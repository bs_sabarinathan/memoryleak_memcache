﻿using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Access.Interface;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Web;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using BrandSystems.Marcom.Core.Utility;
using System.Diagnostics;

namespace Presentation.Controllers
{
    public class PlanningController : ApiController
    {
        ServiceResponse result;

        private static string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
        private static string retFilePath = baseDir + "//" + "SearchLog.txt";

        [Route("api/Planning/GetMilestoneById/{ID}")]
        [HttpGet]
        public ServiceResponse GetMilestoneById(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetMilestoneById(ID); ;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;

        }

        [Route("api/Planning/DeleteMileStone/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteMileStone(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteMileStone(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/InsertCostCenter")]
        [HttpPost]
        public ServiceResponse InsertCostCenter([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.AddCostCenter((int)jobj["EntityID"], (int)jobj["CostcenterID"], (decimal)jobj["PlannedAmount"], (decimal)jobj["RequestedAmount"], (decimal)jobj["ApprovedAllocatedAmount"], (decimal)jobj["ApprovedBudget"], (decimal)jobj["Comited"], (decimal)jobj["Spent"], (DateTimeOffset)jobj["ApprovedbudgetDate"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/DeleteCostCenter/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteCostCenter(int ID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteCostCenter(ID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/InsertFundingRequest")]
        [HttpPost]
        public ServiceResponse InsertFundingRequest([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.CreateFundingRequest((int)jobj["EntityID"], (int)jobj["CostcenterID"], (decimal)jobj["Amount"], (string)jobj["DueDate"], (string)jobj["Comment"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/UpdateFundingRequest")]
        [HttpPut]
        public ServiceResponse UpdateFundingRequest([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateFundingRequest((int)jobj["EntityID"], (int)jobj["CostcenterID"], (int)jobj["State"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/DeleteFundingRequest")]
        [HttpDelete]
        public ServiceResponse DeleteFundingRequest([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteFundingRequest((int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetfundingRequestsByEntityID/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetfundingRequestsByEntityID(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.getfundingRequestsByEntityID(EntityID); ;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/EntityPlannedAmountInsert")]
        [HttpPost]
        public ServiceResponse EntityPlannedAmountInsert([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jr = (JArray)jobj["PlannedAmount"];
                for (int i = 0; i < jr.Count(); i++)
                {
                    result.Response = marcomManager.PlanningManager.EntityPlannedAmountInsert((int)jr[i]["EntityID"], (int)jr[i]["CostCenterID"], (decimal)jr[i]["Amount"], (int)jr[i]["CurrencyType"], (string)jr[i]["Description"]);
                }
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/EntityApprovedPlannedAmountInsert")]
        [HttpPost]
        public ServiceResponse EntityApprovedPlannedAmountInsert([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jr = (JArray)jobj["ApprovePlannedAmount"];
                for (int i = 0; i < jr.Count(); i++)
                {

                    result.Response = marcomManager.PlanningManager.EntityPlannedAmountInsert((int)jr[i]["EntityID"], (int)jr[i]["CostCenterID"], (decimal)jr[i]["Amount"], (int)jr[i]["CurrencyType"], (string)jr[i]["Description"]);
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/ReleaseFund")]
        [HttpPut]
        public ServiceResponse ReleaseFund([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.ReleaseFund((int)jobj["EntityID"], (int)jobj["CostCenterID"], (decimal)jobj["ReleaseFund"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdateFundRequestStatus/{EntityID}/{CostCenterID}/{FundRequestID}/{Status}")]
        [HttpPut]
        public ServiceResponse UpdateFundRequestStatus(int EntityID, int CostCenterID, int FundRequestID, int Status)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateFundRequestStatus(EntityID, CostCenterID, FundRequestID, Status);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/ApprovePlannedAmountUpdate")]
        [HttpPut]
        public ServiceResponse ApprovePlannedAmountUpdate([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jr = (JArray)jobj["ApprovePlannedAmount"];
                JArray jr1 = (JArray)jobj["OldApproveAmount"];
                for (int i = 0; i < jr.Count(); i++)
                {
                    if (((decimal)jr[i]["AvailableAmount"] >= (decimal)jr[i]["Amount"]) || (int)jr[i]["Level"] == 1) // if the level is equal to 1 directly update without validation
                        result.Response = marcomManager.PlanningManager.EntityApprovePlannedAmountInsert((int)jr[i]["EntityID"], (int)jr[i]["CostCenterID"], (decimal)jr[i]["AvailableAmount"], (decimal)jr[i]["Amount"], (decimal)jr1[i], (int)jr[i]["CurrencyType"], (int)jr[i]["Level"]);
                }
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/AdjustApprovePlannedAmount/{EntityID}")]
        [HttpPut]
        public ServiceResponse AdjustApprovePlannedAmount(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.AdjustApproveAllocation(EntityID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/EntityMoneyTransfer")]
        [HttpPut]
        public ServiceResponse EntityMoneyTransfer([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.EntityMoneyTransfer((int)jobj["EntityID"], (int)jobj["FromCostCenterID"], (int)jobj["ToCostCenterID"], (decimal)jobj["Transferfund"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetObjectiveById/{ID}")]
        [HttpGet]
        public ServiceResponse GetObjectiveById(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.SelectObjectiveByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/UpdateObjective")]
        [HttpPut]
        public ServiceResponse UpdateObjective([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateObjective((IObjective)jobj["ObjectiveData"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        //method renamed
        [Route("api/Planning/DeleteObjective1/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteObjective1(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteObjective(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/InsertUnits")]
        [HttpPost]
        public ServiceResponse InsertUnits([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.CreateUnits((int)jobj["ID"], (string)jobj["Caption"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/GetUnitsById/{ID}")]
        [HttpGet]
        public ServiceResponse GetUnitsById(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.SelectUnitsById(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/GetEntityAttributesDetails/{ID}")]
        [HttpGet]
        public ServiceResponse GetEntityAttributesDetails(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntityAttributesDetails(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/DeleteUnits")]
        [HttpDelete]
        public ServiceResponse DeleteUnits([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteUnitsById((int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdateUnits")]
        [HttpPut]
        public ServiceResponse UpdateUnits([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateUnits((IObjectiveUnit)jobj["Units"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/InsertRatings")]
        [HttpPost]
        public ServiceResponse InsertRatings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.CreateRatings((int)jobj["ObjectiveID"], (string)jobj["caption"], (int)jobj["SortOrder"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Planning/GetRatingsById/{ID}")]
        [HttpGet]
        public ServiceResponse GetRatingsById(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.SelectRatings(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/DeleteRatings")]
        [HttpDelete]
        public ServiceResponse DeleteRatings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.SelectRatings((int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetPresentationById/{ID}")]
        [HttpGet]
        public ServiceResponse GetPresentationById(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetPresentationByEntityId(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/InsertPresentation")]
        [HttpPost]
        public ServiceResponse InsertPresentation([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray jr = (JArray)jobj["entityList"];
                int[] entityidList = jr.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.CreatePresentation((int)jobj["EntityID"], (DateTimeOffset)jobj["PublishedOn"], entityidList, (string)jobj["Content"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/UpdatePresentation")]
        [HttpPut]
        public ServiceResponse UpdatePresentation([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdatePresentation((int)jobj["EntityID"], (DateTimeOffset)jobj["PublishedOn"], (string)jobj["Content"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/InsertPublishThisLevel")]
        [HttpPost]
        public ServiceResponse InsertPublishThisLevel([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.PublishThisLevel((int)jobj["EntityID"], (DateTimeOffset)jobj["PublishedOn"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/InsertAttachments")]
        [HttpPost]
        public ServiceResponse InsertAttachments([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.CreateAttachments((int)jobj["EntityID"], (String)jobj["EntityID"], (int)jobj["ActiveVersionNo"], (int)jobj["ActiveFileID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Planning/GetAttachmentsById/{ID}")]
        [HttpGet]
        public ServiceResponse GetAttachmentsById(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetAttachmentsById(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/DeleteAttachments")]
        [HttpDelete]
        public ServiceResponse DeleteAttachments([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteAttachments((int)jobj["ID"], (int)jobj["EntityID"], (string)jobj["Name"], (int)jobj["ActiveVersionNo"], (int)jobj["ActiveFieldID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/InsertEntityPeriod")]
        [HttpPost]
        public ServiceResponse InsertEntityPeriod([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                DateTime StartDate = new DateTime(); DateTime EndDate = new DateTime();
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                string dateformate;
                dateformate = marcomManager.GlobalAdditionalSettings[0].SettingValue.ToString().Replace('m', 'M');

                if (jobj["StartDate"].ToString().Length > 1 && jobj["EndDate"].ToString().Length > 1 && jobj["StartDate"].ToString() != null && jobj["StartDate"].ToString() != "undefined")
                {
                    DateTime startDate = new DateTime();
                    DateTime endDate = new DateTime();
                    //string NewstartDate = (jobj["StartDate"].ToString()) == "" ? null : (DateTime.ParseExact(jobj["StartDate"].ToString(), "dd-MM-yyyy", null)).ToString("yyyy-MM-dd");
                    //string NewendDate = (jobj["EndDate"].ToString()) == "" ? null : (DateTime.ParseExact(jobj["EndDate"].ToString(), "dd-MM-yyyy", null)).ToString("yyyy-MM-dd");
                    //string NewstartDate = (jobj["StartDate"].ToString()) == "" ? null : (DateTime.Parse(jobj["StartDate"].ToString())).ToString("yyyy/MM/dd");
                    //string NewendDate = (jobj["EndDate"].ToString()) == "" ? null : (DateTime.Parse(jobj["EndDate"].ToString())).ToString("yyyy/MM/dd");
                    string NewstartDate = (jobj["StartDate"].ToString()) == "" ? null : ((DateTime.ParseExact(jobj["StartDate"].ToString(), dateformate, CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") == "" ? null : (DateTime.ParseExact(jobj["StartDate"].ToString(), dateformate, CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd"));
                    string NewendDate = (jobj["EndDate"].ToString()) == "" ? null : ((DateTime.ParseExact(jobj["EndDate"].ToString(), dateformate, CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd") == "" ? null : (DateTime.ParseExact(jobj["EndDate"].ToString(), dateformate, CultureInfo.InvariantCulture)).ToString("yyyy-MM-dd"));
                    if (DateTime.TryParse(NewstartDate, out startDate) && DateTime.TryParse(NewendDate, out endDate))
                    {
                        StartDate = startDate;
                        EndDate = endDate;
                    }
                }
                result.Response = marcomManager.PlanningManager.CreateEntityPeriod((int)jobj["EntityID"], StartDate, EndDate, (string)jobj["Description"], (int)jobj["SortOrder"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Planning/GetEntityPeriodById/{ID}")]
        [HttpGet]
        public ServiceResponse GetEntityPeriodById(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntityPeriodById(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/UpdateEntityPeriod")]
        [HttpPut]
        public ServiceResponse UpdateEntityPeriod([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateEntityPeriod((DateTime)jobj["StartDate"], (DateTime)jobj["EndDate"], (string)jobj["Description"], (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = "";
            }
            return result;
        }
        [Route("api/Planning/UpdateEntityPeriodUsingPopUp")]
        [HttpPut]
        public ServiceResponse UpdateEntityPeriodUsingPopUp([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateEntityPeriod(DateTime.ParseExact(jobj["StartDate"].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(jobj["EndDate"].ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture), (string)jobj["Description"], (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = "";
            }
            return result;
        }
        [Route("api/Planning/DeleteEntityPeriod/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteEntityPeriod(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteEntityPeriod(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/InsertEntityColorCode")]
        [HttpPost]
        public ServiceResponse InsertEntityColorCode([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.CreateEntityColorCode((int)jobj["EntitytypeID"], (string)jobj["ColorCode"], (int)jobj["AttributeID"], (int)jobj["OptionID"], (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Planning/GetEntityColorCodeById/{ID}")]
        [HttpGet]
        public ServiceResponse GetEntityColorCodeById(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntityColorCodeById(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/UpdateEntityColorCode")]
        [HttpPut]
        public ServiceResponse UpdateEntityColorCode([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateEntityColorCode((int)jobj["ID"], (int)jobj["EntitytypeID"], (int)jobj["AttributeID"], (int)jobj["OptionID"], (string)jobj["ColorCode"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/DeleteActivityReleation")]
        [HttpDelete]
        public ServiceResponse DeleteActivityReleation([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteActivityReleationTypeHierachy((int)jobj["ParentActivityTypeID"], (int)jobj["ChildActivityTypeID"], (int)jobj["SortOrder"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/DeleteEntityColorCode")]
        [HttpDelete]
        public ServiceResponse DeleteEntityColorCode([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteEntityColorCode((int)jobj["ID"], (int)jobj["EntitytypeID"], (int)jobj["AttributeID"], (int)jobj["OptionID"], (string)jobj["ColorCode"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/CreateMilestone")]
        [HttpPost]
        public ServiceResponse CreateMilestone([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray milestoneAttribue = (JArray)jobj["AttributeData"];
                List<object> listObj = new List<object>();
                listObj = null;
                result.StatusCode = (int)HttpStatusCode.OK;
                IList<IAttributeData> listAttributes = new List<IAttributeData>();
                List<object> objval = new List<object>();
                foreach (var obj in milestoneAttribue)
                {
                    IAttributeData attrData = marcomManager.PlanningManager.AttributeDataservice();
                    attrData.ID = (int)obj["AttributeID"];
                    attrData.TypeID = (int)obj["AttributeTypeID"];
                    if ((dynamic)obj["NodeID"] != null)
                    {
                        if ((dynamic)obj["NodeID"].Count() > 0)
                        {
                            foreach (var mileObj in (dynamic)obj["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = mileObj;
                                objval.Add(valueObj);
                            }
                            attrData.Value = objval;
                        }
                        else
                        {

                            if (obj["NodeID"].Type == JTokenType.Boolean)
                            {
                                attrData.Value = Convert.ToBoolean(obj["NodeID"].ToString());
                            }
                            if (obj["NodeID"].Type == JTokenType.Date)
                            {
                                attrData.Value = (obj["NodeID"].ToString());//Newtonsoft.Json.JsonConvert.DeserializeObject<string>(obj["Value"].ToString());
                            }
                            if (obj["NodeID"].Type == JTokenType.Integer)
                            {
                                attrData.Value = int.Parse(obj["NodeID"].ToString());
                            }
                            else
                                attrData.Value = obj["NodeID"].ToString();

                        }
                        listAttributes.Add(attrData);
                    }
                }
                result.Response = marcomManager.PlanningManager.CreateMilestone((int)jobj["MilestoneEntityTypeID"], (String)jobj["Name"], listAttributes);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/UpdateMilestone")]
        [HttpPost]
        public ServiceResponse UpdateMilestone([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray milestoneAttribue = (JArray)jobj["AttributeData"];
                List<object> listObj = new List<object>();
                listObj = null;
                result.StatusCode = (int)HttpStatusCode.OK;
                IList<IAttributeData> listAttributes = new List<IAttributeData>();
                List<object> objval = new List<object>();
                foreach (var obj in milestoneAttribue)
                {

                    IAttributeData attrData = marcomManager.PlanningManager.AttributeDataservice();
                    attrData.ID = (int)obj["AttributeID"];
                    attrData.TypeID = (int)obj["AttributeTypeID"];
                    if ((dynamic)obj["NodeID"].Count() > 0)
                    {
                        foreach (var mileObj in (dynamic)obj["NodeID"])
                        {
                            object valueObj = new object();
                            valueObj = mileObj;
                            objval.Add(valueObj);
                        }
                        attrData.Value = objval;
                    }
                    else
                    {

                        if (obj["NodeID"].Type == JTokenType.Boolean)
                        {
                            attrData.Value = Convert.ToBoolean(obj["NodeID"].ToString());
                        }
                        if (obj["NodeID"].Type == JTokenType.Date)
                        {
                            attrData.Value = (obj["NodeID"].ToString());//Newtonsoft.Json.JsonConvert.DeserializeObject<string>(obj["Value"].ToString());
                        }
                        if (obj["NodeID"].Type == JTokenType.Integer)
                        {
                            attrData.Value = int.Parse(obj["NodeID"].ToString());
                        }

                        else
                            if (obj["NodeID"].ToString().Contains("/") == true && attrData.TypeID == 5)
                            {
                                var dueDate = obj["NodeID"].ToString().Split('/');
                                var detet = dueDate[2].Substring(0, 4) + "-" + dueDate[1] + "-" + dueDate[0];
                                attrData.Value = detet.ToString();
                            }

                            else
                            {
                                attrData.Value = (obj["NodeID"].ToString());//Newtonsoft.Json.JsonConvert.DeserializeObject<string>(obj["Value"].ToString());
                            }
                    }
                    listAttributes.Add(attrData);
                }
                result.Response = marcomManager.PlanningManager.UpdateMilestone((int)jobj["MilestoneEntityTypeID"], (string)jobj["MilstoneName"], listAttributes, (int)jobj["MilestoneId"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/CreateObjective")]
        [HttpPost]
        public ServiceResponse CreateObjective([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray arrobjAttibutevalues = (JArray)jobj["AttributeData"];


                JArray objnumericarr = (JArray)jobj["ObjectiveNumericData"];
                JArray objNonNumericarr = (JArray)jobj["ObjectiveNonNumericData"];
                JArray objQualitativearr = (JArray)jobj["ObjectiveQualitativeData"];
                JArray objratingarr = (JArray)jobj["ObjectiveRatingData"];
                JArray objconarr = (JArray)jobj["ObjectiveConditonData"];
                JArray arrmembers = (JArray)jobj["ObjectiveMembers"];
                JArray objFulfillData = (JArray)jobj["ObjectiveFulfillConditonValueData"];
                JArray objOwnerData = (JArray)jobj["ObjectiveOwner"];
                JArray arrperiods = (JArray)jobj["Periods"];
                string objectiveInstruction = string.Empty;
                bool objectiveEnableFeedback = false;
                List<string> ratingObjArr = new List<string>();
                DateTime startDate = new DateTime();
                DateTime endDate = new DateTime();
                int objectiveDateRule = 0;
                bool objectiveMandatory = false;
                IObjectiveNumeric objNumeric = marcomManager.PlanningManager.ObjNumericservice();
                IObjectiveNumeric objNonnumericList = marcomManager.PlanningManager.ObjNumericservice();
                IList<IObjectiveRating> objRatingList = new List<IObjectiveRating>();
                IList<IEntityRoleUser> objMembersList = new List<IEntityRoleUser>();
                if (arrmembers != null || objOwnerData != null)
                {
                    if (arrmembers != null && arrmembers.Count > 0)
                    {
                        foreach (var arm in arrmembers)
                        {
                            IEntityRoleUser entrole = marcomManager.PlanningManager.Entityrolesservice();
                            entrole.Roleid = (int)arm["Roleid"];
                            entrole.Userid = (int)arm["Userid"];
                            entrole.IsInherited = false;
                            entrole.InheritedFromEntityid = 0;
                            objMembersList.Add(entrole);
                        }
                    }
                    else if (arrmembers == null || arrmembers.Count == 0)
                    {
                        objMembersList = null;
                    }
                    //foreach (var objOwn in objOwnerData)
                    //{
                    //    IEntityRoleUser entrole = marcomManager.PlanningManager.Entityrolesservice();
                    //    entrole.Roleid = (int)objOwn["Roleid"];
                    //    entrole.Userid = (int)objOwn["Userid"];
                    //    entrole.IsInherited = false;
                    //    entrole.InheritedFromEntityid = 0;
                    //    objMembersList.Add(entrole);
                    //}
                }
                else if (arrmembers == null)
                {
                    objMembersList = null;
                }

                if (objnumericarr != null)
                {
                    foreach (var numericObj in objnumericarr)
                    {
                        if (((string)numericObj["GlobalBaseLine"].ToString()).Length > 8)
                        {
                            numericObj["GlobalBaseLine"] = ((string)numericObj["GlobalBaseLine"].ToString()).Substring(0, 8);
                        }
                        if (((string)numericObj["GlobalTarget"].ToString()).Length > 8)
                        {
                            numericObj["GlobalTarget"] = ((string)numericObj["GlobalTarget"].ToString()).Substring(0, 8);
                        }
                        objNumeric.GlobalBaseLine = (decimal)numericObj["GlobalBaseLine"];
                        objNumeric.GlobalTarget = (decimal)numericObj["GlobalTarget"];
                        objNumeric.Unitid = numericObj["UnitID"] == null ? default(int) : (int)numericObj["UnitID"];
                        objNumeric.UnitValue = numericObj["UnitValue"] == null ? default(int) : numericObj["UnitValue"].ToString().Count() == 0 ? default(int) : (int)numericObj["UnitValue"];
                        objectiveInstruction = (string)numericObj["Instruction"];
                        objectiveEnableFeedback = (bool)numericObj["IsEnableFeedback"];
                        objNonnumericList = null;
                        objRatingList = null;
                    }

                }
                else if (objnumericarr == null)
                {
                    objNumeric = null;
                }
                if (objNonNumericarr != null)
                {
                    foreach (var nonuNumericObj in objNonNumericarr)
                    {
                        if (((string)nonuNumericObj["GlobalBaseLine"]).Length > 8)
                        {
                            nonuNumericObj["GlobalBaseLine"] = ((string)nonuNumericObj["GlobalBaseLine"]).Substring(0, 8);
                        }
                        if (((string)nonuNumericObj["GlobalTarget"]).Length > 8)
                        {
                            nonuNumericObj["GlobalTarget"] = ((string)nonuNumericObj["GlobalTarget"]).Substring(0, 8);
                        }
                        objNonnumericList.GlobalBaseLine = (decimal)nonuNumericObj["GlobalBaseLine"];
                        objNonnumericList.GlobalTarget = (decimal)nonuNumericObj["GlobalTarget"];
                        objNonnumericList.Unitid = nonuNumericObj["UnitID"] == null ? 0 : (int)nonuNumericObj["UnitID"];
                        objNonnumericList.UnitValue = nonuNumericObj["UnitValue"] == null ? 0 : nonuNumericObj["UnitValue"].ToString().Count() == 0 ? 0 : (int)nonuNumericObj["UnitValue"];
                        objectiveInstruction = (string)nonuNumericObj["Instruction"];
                        objectiveEnableFeedback = (bool)nonuNumericObj["IsEnableFeedback"];
                        objNumeric = null;
                        objRatingList = null;
                    }

                }
                else if (objNonNumericarr == null)
                {
                    objNonnumericList = null;
                }
                if (objQualitativearr != null)
                {
                    foreach (var qualitativeObj in objQualitativearr)
                    {
                        objectiveInstruction = (string)qualitativeObj["Instruction"];
                        objectiveEnableFeedback = (bool)qualitativeObj["IsEnableFeedback"];
                        objNumeric = null;
                        objNonnumericList = null;
                        objRatingList = null;
                    }

                }
                IObjectiveRating objrat = marcomManager.PlanningManager.ObjRatingservice();
                if (objratingarr != null)
                {
                    foreach (var ratingObj in objratingarr)
                    {
                        objectiveInstruction = (string)ratingObj["Instruction"];
                        objectiveEnableFeedback = (bool)ratingObj["IsEnableFeedback"];
                        var ratingCount = (dynamic)ratingObj["Ratings"].ToArray();
                        foreach (var ratValue in ratingCount)
                        {
                            ratingObjArr.Add(Convert.ToString(ratValue));
                        }
                        objNumeric = null;
                        objNonnumericList = null;
                    }

                }
                else if (objratingarr == null)
                {
                    objRatingList = null;
                }
                if (objconarr != null)
                {
                    foreach (var condObj in objconarr)
                    {
                        var ste = Convert.ToString(condObj["StartDate"]);
                        startDate = (DateTime)condObj["StartDate"];
                        endDate = (DateTime)condObj["EndDate"];
                        objectiveDateRule = (int)condObj["DateRule"];
                        objectiveMandatory = (bool)condObj["IsMandatory"];
                    }
                }
                IList<IEntityPeriod> listEntityperiods = new List<IEntityPeriod>();
                if (arrperiods != null)
                {
                    foreach (var entityPeriodObj in arrperiods)
                    {
                        if (entityPeriodObj.Count() > 0)
                        {
                            foreach (var arrp in entityPeriodObj)
                            {
                                DateTime P_startDate = new DateTime();
                                DateTime p_endDate = new DateTime();
                                if (DateTime.TryParse(arrp["startDate"].ToString(), out P_startDate) && DateTime.TryParse(arrp["endDate"].ToString(), out p_endDate))
                                {

                                    IEntityPeriod enti = marcomManager.PlanningManager.Entityperiodservice();
                                    enti.Startdate = P_startDate;
                                    enti.EndDate = p_endDate;
                                    enti.Description = (string)arrp["comment"];
                                    enti.SortOrder = (int)arrp["sortorder"];
                                    listEntityperiods.Add(enti);
                                }
                            }
                        }
                    }
                }
                else if (arrperiods == null)
                {
                    listEntityperiods = null;
                }
                IList<IObjectiveFulfillCondtions> objFullfilConditionsList = new List<IObjectiveFulfillCondtions>();
                if (objFulfillData != null)
                {
                    foreach (var obj in objFulfillData)
                    {

                        foreach (var obj2 in obj["ObjectiveFulfillValues"])
                        {
                            IObjectiveFulfillCondtions objcon = marcomManager.PlanningManager.ObjectiveFulfillmentCondtionValuesService();
                            objcon.EntityTypeid = (int)obj2["ObjEntityType"];
                            objcon.ConditionType = (int)obj2["ConditionType"];

                            if (Convert.ToString(obj2["ObjEntityAttributes"]) != "0")
                            {
                                if (obj2["ObjEntityAttributes"] != null)
                                {
                                    string[] attributeData = Convert.ToString(obj2["ObjEntityAttributes"]).Split('_');
                                    int attributeId = Convert.ToInt32(attributeData[0]);
                                    int attributeLevelId = Convert.ToInt32(attributeData[1]);

                                    objcon.Attributeid = attributeId;
                                    objcon.AttributeLevel = attributeLevelId;

                                    var ConditionValue = (dynamic)obj2["ObjAttributeOptionns"].ToArray();
                                    objcon.ObjectiveConditionValue = null;
                                    List<int> objCondVal = new List<int>();
                                    foreach (var conditionObj in ConditionValue)
                                    {
                                        var oe = (int)conditionObj;
                                        objCondVal.Add(oe);
                                    }
                                    objcon.ObjectiveConditionValue = objCondVal;
                                }



                            }

                            objFullfilConditionsList.Add(objcon);
                        }
                    }
                }

                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {

                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                            if ((string)objAttr["Value"] != "-1")
                                entityAttr.SpecialValue = Convert.ToString(objAttr["Value"].ToString());
                            else
                                entityAttr.SpecialValue = "";
                        }
                        else
                        {
                            if (entityAttr.ID != (int)SystemDefinedAttributes.CreationDate)
                            {
                                if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                {
                                    entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Date)
                                {
                                    entityAttr.Value = (objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Integer)
                                {
                                    entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                }
                                else
                                    entityAttr.Value = objAttr["NodeID"].ToString();
                            }
                            else
                            {
                                entityAttr.Value = DateTime.UtcNow.ToString("yyyy-MM-dd");
                            }
                        }
                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.CreateObjective((int)jobj["ObjectiveTypeID"], (String)jobj["ObjectiveName"], (bool)jobj["ObjectiveStatus"], (string)jobj["ObjectiveDescription"], objectiveInstruction, objectiveEnableFeedback, startDate, endDate, objectiveDateRule, objectiveMandatory, objNumeric, objNonnumericList, objRatingList, ratingObjArr, objFullfilConditionsList, objMembersList, listattributevalues, (int)jobj["objEntityTypeId"], listEntityperiods);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Planning/SelectEntityByID/{ID}")]
        [HttpGet]
        public ServiceResponse SelectEntityByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.SelectEntityByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/SelectAllchildEntityObject/{ID}")]
        [HttpGet]
        public ServiceResponse SelectAllchildEntityObject(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.SelectAllchildeEtities(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/SelectObjectiveByID/{ID}")]
        [HttpGet]
        public ServiceResponse SelectObjectiveByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.SelectObjectiveByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/CreateCostcentre")]
        [HttpPost]
        public ServiceResponse CreateCostcentre([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray arrperiods = (JArray)jobj["Periods"];
                JArray arrmembers = (JArray)jobj["EntityMembers"];
                JArray arrobjAttibutevalues = (JArray)jobj["AttributeData"];
                JArray arrentityamountcurrencytype = (JArray)jobj["Entityamountcurrencytype"];
                IList<IEntityRoleUser> listEntityMembers = new List<IEntityRoleUser>();
                if (arrmembers != null && arrmembers.Count > 0)
                {
                    foreach (var arm in arrmembers)
                    {
                        IEntityRoleUser entrole = marcomManager.PlanningManager.Entityrolesservice();
                        //  entrole.Entityid = (int)arm[0];
                        entrole.Roleid = (int)arm["Roleid"];
                        entrole.Userid = (int)arm["Userid"];
                        entrole.IsInherited = ((int)arm["IsInherited"] == 0 ? false : true);
                        entrole.InheritedFromEntityid = (int)arm["InheritedFromEntityid"];
                        listEntityMembers.Add(entrole);
                    }
                }
                else if (arrmembers == null || arrmembers.Count == 0)
                {
                    listEntityMembers = null;
                }
                IList<IEntityPeriod> listEntityperiods = new List<IEntityPeriod>();
                if (arrperiods != null)
                {
                    foreach (var entityPeriodObj in arrperiods)
                    {
                        if (entityPeriodObj.Count() > 0)
                        {
                            foreach (var arrp in entityPeriodObj)
                            {
                                DateTime startDate = new DateTime();
                                DateTime endDate = new DateTime();
                                if (DateTime.TryParse(arrp["startDate"].ToString(), out startDate) && DateTime.TryParse(arrp["endDate"].ToString(), out endDate))
                                {

                                    IEntityPeriod enti = marcomManager.PlanningManager.Entityperiodservice();
                                    enti.Startdate = startDate;
                                    enti.EndDate = endDate;
                                    enti.Description = (string)arrp["comment"];
                                    enti.SortOrder = (int)arrp["sortorder"];
                                    listEntityperiods.Add(enti);
                                }
                            }
                        }
                    }
                }
                else if (arrperiods == null)
                {
                    listEntityperiods = null;
                }
                IList<IEntityAmountCurrencyType> listentityamountcurrencytype = new List<IEntityAmountCurrencyType>();
                if (arrentityamountcurrencytype != null)
                {
                    if (arrentityamountcurrencytype.Count() > 0)
                    {
                        foreach (var item in arrentityamountcurrencytype)
                        {
                            foreach (var i in item)
                            {
                                IEntityAmountCurrencyType entamcur = marcomManager.PlanningManager.EntityAmountCurrencyTypeservice();
                                entamcur.Amount = (decimal)i["amount"];
                                entamcur.Currencytypeid = (int)i["currencytype"];
                                entamcur.Attributeid = (int)i["Attributeid"];
                                listentityamountcurrencytype.Add(entamcur);
                            }
                        }
                    }

                }
                else if (arrentityamountcurrencytype == null)
                {
                    arrentityamountcurrencytype = null;
                }

                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {

                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                        }
                        else
                        {
                            if (entityAttr.ID != (int)SystemDefinedAttributes.CreationDate)
                            {
                                if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                {
                                    entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Date)
                                {
                                    entityAttr.Value = (objAttr["NodeID"].ToString());//Newtonsoft.Json.JsonConvert.DeserializeObject<string>(obj["Value"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Integer)
                                {
                                    entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                }
                                else
                                    entityAttr.Value = objAttr["NodeID"].ToString();
                            }
                            else
                            {
                                entityAttr.Value = DateTime.UtcNow.ToString("yyyy-MM-dd");
                            }
                        }
                        //List<object> listObj = new List<object>();
                        //IAttributeData entityattr = marcomManager.PlanningManager.AttributeDataservice();
                        //entityattr.ID = (int)((objattr[0]).First).First;
                        //entityattr.Caption = (dynamic)((objattr[1]).First).First;
                        //entityattr.TypeID = (int)((objattr[2]).First).First;
                        //if ((dynamic)((objattr[3]).First).First.Count() > 1)
                        //{
                        //    foreach (var obj in (dynamic)((objattr[3]).First).First)
                        //    {
                        //        object valueObj = new object();
                        //        valueObj = obj;
                        //        listObj.Add(valueObj);
                        //    }
                        //    entityattr.Value = listObj;
                        //}
                        //else
                        //{
                        //    entityattr.Value = (dynamic)((objattr[3]).First).First;
                        //}
                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }
                result.Response = marcomManager.PlanningManager.CreateCostcentre((int)jobj["TypeId"], (string)jobj["Name"], (int)jobj["AssignedAmount"], listattributevalues, listEntityMembers, (int)jobj["ParentId"], (int)jobj["CurrencyFormat"], listentityamountcurrencytype, listEntityperiods);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetCostcentre/{ID}")]
        [HttpGet]
        public ServiceResponse GetCostcentre(int ID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetCostcentre(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetCostcentreforEntityCreation/{EntityTypeID}/{FiscalYear}/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetCostcentreforEntityCreation(int EntityTypeID, int FiscalYear, int EntityID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetCostcentreforEntityCreation(EntityTypeID, FiscalYear, EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetGlobalMembers/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetGlobalMembers(int EntityID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetGlobalMembers(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetCostcentreforFinancial/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetCostcentreforFinancial(int EntityID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetCostcentreforFinancial(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/AddCostCenterForFinancial")]
        [HttpPost]
        public ServiceResponse AddCostCenterForFinancial([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrentitycost = (JArray)jobj["EntityCostRelations"];
                IList<IEntityCostReleations> listentitycostcentres = new List<IEntityCostReleations>();
                if (arrentitycost != null)
                {
                    foreach (var arrc in arrentitycost)
                    {
                        IEntityCostReleations entcost = marcomManager.PlanningManager.EntityCostcentrerelationservice();
                        entcost.CostcenterId = (int)arrc["CostcenterId"];
                        entcost.Sortorder = (int)arrc["Sortorder"];
                        entcost.Isassociate = (int)arrc["Isassociate"] == 0 ? false : true;
                        entcost.Isactive = (int)arrc["Isactive"] == 0 ? false : true;
                        entcost.CCName = (string)arrc["costcentername"];
                        listentitycostcentres.Add(entcost);
                    }
                }
                else if (arrentitycost == null)
                {
                    listentitycostcentres = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.AddCostCenterForFinancial((int)jobj["EntityID"], listentitycostcentres, (bool)jobj["isForcetoChild"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Planning/GetEntityFinancialdDetail/{EntityID}/{UserID}/{startRow}/{endRow}/{includedetails}")]
        [HttpGet]
        public ServiceResponse GetEntityFinancialdDetail(string EntityID, int UserID, int startRow, int endRow, bool includedetails)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntityFinancialdDetails(int.Parse(EntityID), UserID, startRow, endRow, includedetails);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetCostcenterBeforeApprovalAmountDetails/{CostCenterID}/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetCostcenterBeforeApprovalAmountDetails(int CostCenterID, int EntityID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetCostcenterBeforeApprovalAmountDetails(CostCenterID, EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/DeleteCostcentreentity/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteCostcentreentity(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteCostcentreentity(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/DeleteCostcentreFinancial/{EntityID}/{CostCenterID}")]
        [HttpDelete]
        public ServiceResponse DeleteCostcentreFinancial(int EntityID, int CostCenterID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteCostcentreFinancial(EntityID, CostCenterID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdateCostcentre")]
        [HttpPut]
        public ServiceResponse UpdateCostcentre([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray arrmembers = (JArray)jobj["EntityMembers"];
                JArray arrobjAttibutevalues = (JArray)jobj["AttributeData"];
                ICostCentreData costdata = marcomManager.PlanningManager.CostcentreDataservice();
                costdata.Id = (int)jobj["ID"];
                costdata.Typeid = (int)jobj["TypeId"];
                costdata.Name = (string)jobj["Name"];
                costdata.AssignedAmount = (decimal)jobj["AssignedAmount"];
                IList<IEntityRoleUser> listEntityMembers = new List<IEntityRoleUser>();
                if (arrmembers != null)
                {
                    foreach (var arm in arrmembers)
                    {
                        IEntityRoleUser entrole = marcomManager.PlanningManager.Entityrolesservice();
                        //  entrole.Entityid = (int)arm[0];
                        entrole.Roleid = (int)arm[0];
                        entrole.Userid = (int)arm[1];
                        entrole.IsInherited = ((int)arm[2] == 0 ? false : true);
                        entrole.InheritedFromEntityid = (int)arm[3];
                        listEntityMembers.Add(entrole);
                    }
                    costdata.EntityMembers = listEntityMembers;

                }
                else if (arrmembers == null)
                {
                    listEntityMembers = null;
                }

                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objattr in arrobjAttibutevalues)
                    {
                        List<object> listObj = new List<object>();
                        IAttributeData entityattr = marcomManager.PlanningManager.AttributeDataservice();
                        entityattr.ID = (int)((objattr[0]).First).First;
                        entityattr.Caption = (dynamic)((objattr[1]).First).First;
                        entityattr.TypeID = (int)((objattr[2]).First).First;
                        if ((dynamic)((objattr[3]).First).First.Count() > 1)
                        {
                            foreach (var obj in (dynamic)((objattr[3]).First).First)
                            {
                                object valueObj = new object();
                                valueObj = obj;
                                listObj.Add(valueObj);
                            }
                            entityattr.Value = listObj;
                        }
                        else
                        {
                            entityattr.Value = (dynamic)((objattr[3]).First).First;
                        }
                        listattributevalues.Add(entityattr);
                    }
                    costdata.EntityAttributeData = listattributevalues;
                }
                //else if (arrobjAttibutevalues == null)
                //{
                //    listattributevalues = null;
                //}
                result.Response = marcomManager.PlanningManager.UpdateCostcentre(costdata);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/DeleteObjective")]
        [HttpDelete]
        public ServiceResponse DeleteObjective([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteObjective((int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/Objectiveentityvalues")]
        [HttpPost]
        public ServiceResponse Objectiveentityvalues([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.Objectiveentityvalues((int)jobj["Objectiveid"], (int)jobj["Entityid"], (int)jobj["PlannedTarget"], (int)jobj["TargetOutcome"], (int)jobj["RatingObjective"], (String)jobj["Comments"], (int)jobj["Status"], (int)jobj["Fulfilment"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/ObjectiveConditionvalues")]
        [HttpPost]
        public ServiceResponse ObjectiveConditionvalues([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.ObjectiveConditionvalues((IList<IObjectiveCondition>)jobj["IObjectiveCondition"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetEntitydescendants/{AttributeID}")]
        [HttpGet]
        public ServiceResponse GetEntitydescendants(int AttributeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntitydescendants(AttributeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/CreateEntity")]
        [HttpPost]
        public ServiceResponse CreateEntity([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrperiods = (JArray)jobj["Periods"];
                JArray arrmembers = (JArray)jobj["EntityMembers"];
                JArray arrentitycost = (JArray)jobj["EntityCostRelations"];
                JArray arrobjentvalues = (JArray)jobj["IObjectiveEntityValue"];
                JArray arrobjAttibutevalues = (JArray)jobj["AttributeData"];
                JArray PredefinedAttrValues = (JArray)jobj["PredefinedAttrValues"];
                JArray AttributeGroupValues = (JArray)jobj["AttributeGroupValues"];
                JArray arrFilesObj = (JArray)jobj["AssetArr"];
                JArray arrentityamountcurrencytype = (JArray)jobj["Entityamountcurrencytype"];   // link assets for sub entities
                int[] assetIdArr = arrFilesObj.Select(jv => (int)jv).ToArray();
                IList<IEntityRoleUser> listEntityMembers = new List<IEntityRoleUser>();
                JArray AttributeGrpInherit = (JArray)jobj["AttributeGrpInherit"];

                int userID = marcomManager.User.Id;
                if (arrmembers.Count > 0)
                {
                    foreach (var arm in arrmembers)
                    {
                        IEntityRoleUser entrole = marcomManager.PlanningManager.Entityrolesservice();
                        entrole.Roleid = (int)arm["Roleid"];
                        entrole.Userid = (int)arm["Userid"];
                        entrole.IsInherited = false;
                        entrole.InheritedFromEntityid = 0;
                        listEntityMembers.Add(entrole);
                    }
                }
                else if (arrmembers.Count == 0)
                {
                    listEntityMembers = null;
                }


                IList<IFundingRequest> listFundrequest = null;

                IList<IEntityPeriod> listEntityperiods = new List<IEntityPeriod>();
                if (arrperiods != null)
                {
                    foreach (var entityPeriodObj in arrperiods)
                    {
                        if (entityPeriodObj.Count() > 0)
                        {
                            foreach (var arrp in entityPeriodObj)
                            {
                                DateTime startDate = new DateTime();
                                DateTime endDate = new DateTime();
                                if (DateTime.TryParse(arrp["startDate"].ToString(), out startDate) && DateTime.TryParse(arrp["endDate"].ToString(), out endDate))
                                {

                                    IEntityPeriod enti = marcomManager.PlanningManager.Entityperiodservice();
                                    enti.Startdate = startDate;
                                    enti.EndDate = endDate;
                                    enti.Description = (string)arrp["comment"];
                                    enti.SortOrder = (int)arrp["sortorder"];
                                    listEntityperiods.Add(enti);
                                }
                            }
                        }
                    }
                }
                else if (arrperiods == null)
                {
                    listEntityperiods = null;
                }
                IList<IEntityAmountCurrencyType> listentityamountcurrencytype = new List<IEntityAmountCurrencyType>();
                if (arrentityamountcurrencytype != null)
                {
                    if (arrentityamountcurrencytype.Count() > 0)
                    {
                        foreach (var item in arrentityamountcurrencytype)
                        {
                            foreach (var i in item)
                            {
                                IEntityAmountCurrencyType entamcur = marcomManager.PlanningManager.EntityAmountCurrencyTypeservice();
                                entamcur.Amount = (decimal)i["amount"];
                                entamcur.Currencytypeid = (int)i["currencytype"];
                                entamcur.Attributeid = (int)i["Attributeid"];
                                listentityamountcurrencytype.Add(entamcur);
                            }
                        }
                    }

                }
                else if (arrentityamountcurrencytype == null)
                {
                    arrentityamountcurrencytype = null;
                }

                IList<IEntityCostReleations> listentitycostcentres = new List<IEntityCostReleations>();
                if (arrentitycost != null)
                {
                    foreach (var arrc in arrentitycost)
                    {
                        IEntityCostReleations entcost = marcomManager.PlanningManager.EntityCostcentrerelationservice();
                        entcost.CostcenterId = (int)arrc["CostcenterId"];
                        entcost.Sortorder = (int)arrc["Sortorder"];
                        entcost.Isassociate = (int)arrc["Isassociate"] == 0 ? false : true;
                        entcost.Isactive = (int)arrc["Isactive"] == 0 ? false : true;
                        listentitycostcentres.Add(entcost);
                    }
                }
                else if (arrentitycost == null)
                {
                    listentitycostcentres = null;
                }
                IList<IObjectiveEntityValue> listobjentityvalues = new List<IObjectiveEntityValue>();
                if (arrobjentvalues != null)
                {
                    foreach (var arrentobj in arrobjentvalues)
                    {
                        IObjectiveEntityValue objentvalues = marcomManager.PlanningManager.ObjEnityvalservice();
                        objentvalues.Objectiveid = (int)arrentobj;
                        listobjentityvalues.Add(objentvalues);
                    }
                }
                else if (arrobjentvalues == null)
                {
                    listobjentityvalues = null;
                }
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {

                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                            if ((string)objAttr["Value"] != "-1")
                                entityAttr.SpecialValue = Convert.ToString(objAttr["Value"].ToString());
                            else
                                entityAttr.SpecialValue = "";
                        }
                        else
                        {
                            if (entityAttr.ID != (int)SystemDefinedAttributes.CreationDate)
                            {
                                if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                {
                                    entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Date)
                                {
                                    entityAttr.Value = (objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Integer)
                                {
                                    entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                }
                                else
                                    entityAttr.Value = objAttr["NodeID"].ToString();
                            }
                            else
                            {
                                entityAttr.Value = DateTime.UtcNow.ToString("yyyy-MM-dd");
                            }
                        }
                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }

                IList<object> PredefinedAttrGroup = new List<object>();

                if (PredefinedAttrValues != null && PredefinedAttrValues.Count > 0)
                {
                    foreach (var items in PredefinedAttrValues)
                    {
                        if (items["PredefinedattrGrpData"].Count() > 0)
                        {
                            int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>((items["PredefinedattrGrpData"]["SelectedIds"]).ToString());
                            PredefinedAttrGroup.Add(new
                            {
                                SelectedIds = Idarr,
                                GroupID = (int)(items["PredefinedattrGrpData"]["GroupID"]),
                                Name = (string)(items["PredefinedattrGrpData"]["Name"])
                            });
                        }
                    }
                }
                else
                    PredefinedAttrGroup = null;


                IList<object> AttrGroupValues = new List<object>();

                if (AttributeGroupValues != null && AttributeGroupValues.Count > 0)
                {
                    foreach (var itm in AttributeGroupValues)
                    {
                        if (itm["attrGrpData"] != null && itm["attrGrpData"].Count() > 0)
                        {
                            foreach (var items in itm["attrGrpData"])
                            {
                                IList<IAttributeData> listattributegrpvalues = new List<IAttributeData>();
                                if (items["AttributeData"] != null)
                                {
                                    foreach (var objAttr in items["AttributeData"])
                                    {
                                        List<object> listObj = new List<object>();
                                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                                        entityAttr.ID = (int)objAttr["AttributeID"];
                                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                                        entityAttr.Level = (int)objAttr["Level"];
                                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                                        {
                                            foreach (var obj in (dynamic)objAttr["NodeID"])
                                            {
                                                object valueObj = new object();
                                                valueObj = (dynamic)obj;
                                                listObj.Add(valueObj);
                                            }
                                            entityAttr.Value = listObj;
                                        }
                                        else
                                        {
                                            if (entityAttr.ID != (int)SystemDefinedAttributes.CreationDate)
                                            {
                                                if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                                {
                                                    entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                                }
                                                if (objAttr["NodeID"].Type == JTokenType.Date)
                                                {
                                                    entityAttr.Value = (objAttr["NodeID"].ToString());//Newtonsoft.Json.JsonConvert.DeserializeObject<string>(obj["Value"].ToString());
                                                }
                                                if (objAttr["NodeID"].Type == JTokenType.Integer)
                                                {
                                                    entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                                }
                                                else
                                                    entityAttr.Value = objAttr["NodeID"].ToString();
                                            }
                                            else
                                            {
                                                entityAttr.Value = DateTime.UtcNow.ToString("yyyy-MM-dd");
                                            }
                                        }

                                        listattributegrpvalues.Add(entityAttr);
                                    }
                                }
                                else if (items["AttributeData"] == null)
                                {
                                    listattributegrpvalues = null;
                                }

                                AttrGroupValues.Add(new
                                {
                                    AttributeData = listattributegrpvalues,
                                    GroupID = (int)itm["GroupID"],
                                    Name = (string)(items["name"]),
                                    AttrGroupID = (int)(items["GroupID"])
                                });
                            }
                        }
                    }
                }
                else
                    AttrGroupValues = null;


                JArray SelectedAssetIdFolderID = (JArray)jobj["SelectedAssetIdFolderID"];
                IList<object> listassetidfolderid = new List<object>();
                IList<object> AssetidlistObj = new List<object>();
                if (SelectedAssetIdFolderID != null)
                {
                    foreach (var objAssetIdFolderId in SelectedAssetIdFolderID)
                    {
                        AssetidlistObj.Add(new
                            {
                                AssetId = (int)objAssetIdFolderId["AssetId"],
                                FolderId = (int)objAssetIdFolderId["FolderId"]
                            });
                    }
                }
                if (SelectedAssetIdFolderID == null) // contains selected AssetIds and corresponding FolderIds from damentityselection
                {
                    AssetidlistObj = null;
                }

                List<int> ObjAttributeGrpInherit = new List<int>();

                if (AttributeGrpInherit != null && AttributeGrpInherit.Count > 0)
                {
                    foreach (var items in AttributeGrpInherit)
                    {
                        ObjAttributeGrpInherit.Add((int)(items["AttributeGroupID"]));
                    }
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                // result.Response = marcomManager.PlanningManager.CreateEntity((int)jobj["ParentId"], (int)jobj["Typeid"], ((int)jobj["Active"] == 0 ? false : true), ((int)jobj["IsLock"] == 0 ? false : true), (String)jobj["Name"], listEntityMembers, listentitycostcentres, listEntityperiods, listFundrequest, listattributevalues, assetIdArr, listobjentityvalues);
                result.Response = marcomManager.PlanningManager.CreateEntity((int)jobj["ParentId"], (int)jobj["Typeid"], (bool)jobj["Active"], (bool)jobj["IsLock"], (String)jobj["Name"], listEntityMembers, listentitycostcentres, listEntityperiods, listFundrequest, listattributevalues, assetIdArr, listobjentityvalues, AssetidlistObj, listentityamountcurrencytype, 0, PredefinedAttrGroup, AttrGroupValues, ObjAttributeGrpInherit);

            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/CreateFundingRequest")]
        [HttpPost]
        public ServiceResponse CreateFundingRequest([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrobjAttibutevalues = (JArray)jobj["AttributeData"];
                JArray financialAttibutevalues = (JArray)jobj["FinancialRequestObj"];
                JArray costCenterRequests = (JArray)jobj["CostCenterRequest"];
                IList<IFundingRequest> listFundrequest = new List<IFundingRequest>();
                if (financialAttibutevalues != null)
                {
                    foreach (var arm in financialAttibutevalues)
                    {

                        IFundingRequest entfundreq = marcomManager.PlanningManager.EntityFundingrequestservice();
                        entfundreq.Entityid = (int)arm["EntityID"];
                        entfundreq.CostCenterid = (int)arm["CostCenterID"];
                        entfundreq.LastUpdatedOn = DateTimeOffset.UtcNow;
                        entfundreq.FundRequestSTATUS = 1;
                        listFundrequest.Add(entfundreq);

                    }
                }
                else if (financialAttibutevalues == null)
                {
                    listFundrequest = null;
                }

                IList<IFundingRequestHolder> listfundrequestattributedata = new List<IFundingRequestHolder>();
                if (costCenterRequests != null)
                {
                    foreach (var cost in costCenterRequests)
                    {

                        IFundingRequestHolder fundreq = marcomManager.PlanningManager.FundingRequestHolderservice();
                        fundreq.CostCenterID = (int)cost["CostCenterID"];

                        DateTime dateTime;
                        if (DateTime.TryParse(cost["DueDate"].ToString(), out dateTime))
                            fundreq.DueDate = (cost["DueDate"].ToString());
                        else
                        {
                            DateTime dt = DateTime.Now;
                            dt.AddDays(7).ToString("dd/MM/yyyy");
                            fundreq.DueDate = dt.ToString();
                        }
                        fundreq.Comment = cost["Description"].ToString();
                        listfundrequestattributedata.Add(fundreq);

                    }
                }
                else if (costCenterRequests == null)
                {
                    listfundrequestattributedata = null;
                }


                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.CreateFundRequest((int)jobj["ParentId"], (int)jobj["Typeid"], ((int)jobj["Active"] == 0 ? false : true), ((int)jobj["IsLock"] == 0 ? false : true), (String)jobj["Name"], listFundrequest, listfundrequestattributedata);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/UpdateEntity")]
        [HttpPut]
        public ServiceResponse UpdateEntity([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrperiods = (JArray)jobj["Periods"];
                JArray arrmembers = (JArray)jobj["EntityMembers"];
                JArray arrentitycost = (JArray)jobj["EntityCostRelations"];
                JArray arrobjentvalues = (JArray)jobj["IObjectiveEntityValue"];
                JArray arrobjAttibutevalues = (JArray)jobj["AttributeData"];
                IEntity entitydata = marcomManager.PlanningManager.EntityService();
                entitydata.Id = (int)jobj["ID"];
                entitydata.Parentid = (int)jobj["ParentId"];
                entitydata.Typeid = (int)jobj["Typeid"];
                entitydata.UniqueKey = (String)jobj["UniqueKey"];
                entitydata.IsLock = (Boolean)jobj["IsLock"];
                entitydata.Name = (String)jobj["Name"];
                IList<IEntityRoleUser> listEntityMembers = new List<IEntityRoleUser>();
                if (arrmembers != null)
                {
                    foreach (var arm in arrmembers)
                    {
                        IEntityRoleUser entrole = marcomManager.PlanningManager.Entityrolesservice();
                        entrole.Roleid = (int)arm[0];
                        entrole.Userid = (int)arm[1];
                        entrole.IsInherited = ((int)arm[2] == 0 ? false : true);
                        entrole.InheritedFromEntityid = (int)arm[3];
                        listEntityMembers.Add(entrole);
                    }
                }
                else if (arrmembers == null)
                {
                    listEntityMembers = null;
                }
                IList<IEntityPeriod> listEntityperiods = new List<IEntityPeriod>();
                if (arrperiods != null)
                {
                    foreach (var arrp in arrperiods)
                    {
                        IEntityPeriod enti = marcomManager.PlanningManager.Entityperiodservice();
                        enti.Startdate = (DateTime)arrp[0];
                        enti.EndDate = (DateTime)arrp[1];
                        enti.Description = (string)arrp[2];
                        enti.SortOrder = (int)arrp[3];
                        listEntityperiods.Add(enti);
                    }
                }
                else if (arrperiods == null)
                {
                    listEntityperiods = null;
                }
                IList<IEntityCostReleations> listentitycostcentres = new List<IEntityCostReleations>();
                if (arrentitycost != null)
                {
                    foreach (var arrc in arrentitycost)
                    {
                        IEntityCostReleations entcost = marcomManager.PlanningManager.EntityCostcentrerelationservice();
                        entcost.CostcenterId = (int)arrc[0];
                        entcost.Sortorder = (int)arrc[1];
                        entcost.Isassociate = (int)arrc[2] == 0 ? false : true;
                        entcost.Isactive = (int)arrc[3] == 0 ? false : true;
                        listentitycostcentres.Add(entcost);
                    }
                }
                else if (arrentitycost == null)
                {
                    listentitycostcentres = null;
                }
                IList<IObjectiveEntityValue> listobjentityvalues = new List<IObjectiveEntityValue>();
                if (arrobjentvalues != null)
                {
                    foreach (var arrentobj in arrobjentvalues)
                    {
                        IObjectiveEntityValue objentvalues = marcomManager.PlanningManager.ObjEnityvalservice();
                        objentvalues.Objectiveid = (int)arrentobj[0];
                        objentvalues.PlannedTarget = (int)arrentobj[1];
                        objentvalues.TargetOutcome = (int)arrentobj[2];
                        objentvalues.RatingObjective = (int)arrentobj[3];
                        objentvalues.Comments = (string)arrentobj[4];
                        objentvalues.Status = (int)arrentobj[5];
                        objentvalues.Fulfilment = (int)arrentobj[6];
                        listobjentityvalues.Add(objentvalues);
                    }
                }
                else if (arrobjentvalues == null)
                {
                    listobjentityvalues = null;
                }
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objattr in arrobjAttibutevalues)
                    {
                        List<object> listObj = new List<object>();
                        IAttributeData entityattr = marcomManager.PlanningManager.AttributeDataservice();
                        entityattr.ID = (int)((objattr[0]).First).First;
                        entityattr.Caption = (dynamic)((objattr[1]).First).First;
                        entityattr.TypeID = (int)((objattr[2]).First).First;
                        if ((dynamic)((objattr[3]).First).First.Count() > 1)
                        {
                            foreach (var obj in (dynamic)((objattr[3]).First).First)
                            {
                                object valueObj = new object();
                                valueObj = obj;
                                listObj.Add(valueObj);
                            }
                            entityattr.Value = listObj;
                        }
                        else
                        {
                            entityattr.Value = (dynamic)((objattr[3]).First).First;
                        }
                        listattributevalues.Add(entityattr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }

                entitydata.Periods = listEntityperiods;
                entitydata.AttributeData = listattributevalues;

                // entitydata.Objectives.Value.Add(listobjentityvalues);
                entitydata.EntityMembers = listEntityMembers;
                entitydata.EntityCostcenters = listentitycostcentres;

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateEntity(entitydata);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/DeleteEntity")]
        [HttpPost]
        public ServiceResponse DeleteEntity([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jEntityID = (JArray)jobj["ID"];
                foreach (var entityId in jEntityID)
                {
                    result.Response = marcomManager.PlanningManager.DeleteEntity((int)entityId);
                }
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/InsertEntityAttributes")]
        [HttpPost]
        public ServiceResponse InsertEntityAttributes([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray arrobjAttibutevalues = (JArray)jobj["IAttributeData"];
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (listattributevalues != null)
                {
                    foreach (var objattr in arrobjAttibutevalues)
                    {
                        IAttributeData entityattr = marcomManager.PlanningManager.AttributeDataservice();
                        entityattr.ID = (int)objattr[0];
                        entityattr.TypeID = (int)objattr[1];
                        entityattr.Caption = (string)objattr[2];
                        entityattr.Value = (dynamic)objattr[3];
                        listattributevalues.Add(entityattr);
                    }

                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }
                result.Response = marcomManager.PlanningManager.InsertEntityAttributes(listattributevalues, (int)jobj["EntityTypeID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetEntityAttributes/{ID}")]
        [HttpGet]
        public ServiceResponse GetEntityAttributes(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntityAttributes(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdateAttributeData")]
        [HttpPost]
        public ServiceResponse UpdateAttributeData([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray arrobjAttibutevalues = (JArray)jobj["IAttributeData"];
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (listattributevalues != null)
                {
                    foreach (var objattr in arrobjAttibutevalues)
                    {
                        IAttributeData entityattr = marcomManager.PlanningManager.AttributeDataservice();
                        entityattr.ID = (int)objattr[0];
                        entityattr.TypeID = (int)objattr[1];
                        entityattr.Caption = (string)objattr[2];
                        entityattr.Value = (dynamic)objattr[3];
                        listattributevalues.Add(entityattr);
                    }

                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }
                result.Response = marcomManager.PlanningManager.UpdateAttributeData(listattributevalues, (int)jobj["EntityID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/Get_EntityIDs/{ID}")]
        [HttpGet]
        public ServiceResponse Get_EntityIDs(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.Get_EntityIDs(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/GetChildTreeNodes/{ParentID}")]
        [HttpGet]
        public ServiceResponse GetChildTreeNodes(int ParentID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetChildTreeNodes(ParentID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/GetParentTreeNodes")]
        [HttpPost]
        public ServiceResponse GetParentTreeNodes([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                // JArray arr = (JArray)jobj["IDArr"];
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetParentTreeNodes(Idarr);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/InsertFilterSettings")]
        [HttpPost]
        public ServiceResponse InsertFilterSettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray filterWhereCondition = (JArray)jobj["WhereConditon"];
                IList<IFiltersettingsValues> listFilterValues = new List<IFiltersettingsValues>();
                string whereCondition = string.Empty;
                foreach (var obj in filterWhereCondition)
                {
                    IFiltersettingsValues filterValues = marcomManager.PlanningManager.FilterSettingsValuesService();
                    filterValues.AttributeId = (int)obj["AttributeID"];
                    filterValues.Level = (int)obj["Level"];
                    filterValues.Value = (int)obj["SelectedValue"];
                    filterValues.AttributeTypeId = (int)obj["AttributeTypeId"];
                    listFilterValues.Add(filterValues);
                    whereCondition += "AttributeID = " + (int)obj["AttributeID"] + " and Value = " + (int)obj["SelectedValue"] + " and ";
                }
                if (whereCondition != "")
                {
                    whereCondition = whereCondition.Remove(whereCondition.Length - 5);
                }
                result.Response = marcomManager.PlanningManager.InsertFilterSettings((string)jobj["FilterName"], (string)jobj["Keyword"], (int)jobj["UserId"], (int)jobj["TypeID"], (string)jobj["entityTypeId"], (int)jobj["IsDetailFilter"], (string)jobj["StarDate"], (string)jobj["EndDate"], whereCondition, listFilterValues, (int)jobj["FilterId"], (string)jobj["EntitymemberId"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetFilterSettings/{TypeID}")]
        [HttpGet]
        public ServiceResponse GetFilterSettings(int TypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetFilterSettings(TypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/GetFilterSettingsForDetail/{TypeID}")]
        [HttpGet]
        public ServiceResponse GetFilterSettingsForDetail(int TypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetFilterSettingsForDetail(TypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/GetFilterSettingValuesByFilertId/{FilterID}")]
        [HttpGet]
        public ServiceResponse GetFilterSettingValuesByFilertId(int FilterID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetFilterSettingValuesByFilertId(FilterID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        //method renamed

        [Route("api/Planning/GetMember/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetMember(string EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;

                result.Response = marcomManager.PlanningManager.GetMember(int.Parse(EntityID));
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/GetFundrequestTaskMember/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetFundrequestTaskMember(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetFundrequestTaskMember(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        //method renamed
        [Route("api/Planning/PutMember/")]
        [HttpPut]
        public ServiceResponse PutMember([FromBody]JObject jobj)
        {
            result = new ServiceResponse();

            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateMember((int)jobj["ID"], (int)jobj["EntityID"], (int)jobj["RoleID"], (int)jobj["Assignee"], (bool)jobj["IsInherited"], (int)jobj["InheritedFromEntityid"], false);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdateMemberInEntity")]
        [HttpPut]
        public ServiceResponse UpdateMemberInEntity([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateMember((int)jobj["ID"], (int)jobj["EntityID"], (int)jobj["RoleID"], (int)jobj["Assignee"], (bool)jobj["IsInherited"], (int)jobj["InheritedFromEntityid"], true);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/Member/{ID}")]
        [HttpDelete]
        public ServiceResponse Member(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteMember(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        //method renamed
        [Route("api/Planning/PostMember")]
        [HttpPost]
        public ServiceResponse PostMember([FromBody] JObject jobj)
        {

            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.InsertMember((int)jobj["EntityID"], (int)jobj["RoleID"], (int)jobj["Assignee"], (bool)jobj["IsInherited"], (int)jobj["InheritedFromEntityid"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Planning/GetMilestoneMetadata/{EntityID}/{EntityTypeId}")]
        [HttpGet]
        public ServiceResponse GetMilestoneMetadata(int EntityId, int EntityTypeId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetMilestoneMetadata(EntityId, EntityTypeId); ;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/GetMilestoneforWidget/{EntityID}/{EntityTypeId}")]
        [HttpGet]
        public ServiceResponse GetMilestoneforWidget(int EntityId, int EntityTypeId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetMilestoneforWidget(EntityId, EntityTypeId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/DeleteFilterSettings/{FilterId}")]
        [HttpDelete]
        public ServiceResponse DeleteFilterSettings(int FilterId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteFilterSettings(FilterId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetEntityPeriod/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetEntityPeriod(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntityPeriod(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/PostEntityPeriod1")]
        [HttpPost]
        //method renamed
        public ServiceResponse PostEntityPeriod1([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateEntityPeriod((int)jobj["ID"], (int)jobj["EntityID"], (string)jobj["StartDate"], (string)jobj["EndDate"], (int)jobj["SortOrder"], (string)jobj["Description"]);
                //result.Response = marcomManager.PlanningManager.UpdateEntityPeriod((int)jobj["ID"], (int)planning.EntityID, (string)planning.StartDate, (string)planning.EndDate, (int)planning.SortOrder, (string)planning.Description);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/EntityPeriod/{ID}")]
        [HttpDelete]
        public ServiceResponse EntityPeriod(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteEntityPeriod(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/PostEntityPeriod")]
        [HttpPost]
        public ServiceResponse PostEntityPeriod([FromBody] JObject jobj)
        {

            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.InsertEntityPeriod((int)jobj["EntityID"], (string)jobj["StartDate"], (string)jobj["EndDate"], (int)jobj["SortOrder"], (string)jobj["Description"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/DeleteRootCostcentre")]
        [HttpPost]
        public ServiceResponse DeleteRootCostcentre([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jEntityID = (JArray)jobj["ID"];
                foreach (var entityId in jEntityID)
                {
                    result.Response = marcomManager.PlanningManager.DeleteRootCostcentre((int)entityId);
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GettingCostcentreFinancialOverview/{EntityId}")]
        [HttpGet]
        public ServiceResponse GettingCostcentreFinancialOverview(int EntityId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GettingCostcentreFinancialOverview(EntityId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdateTotalAssignedAmount")]
        [HttpPost]
        public ServiceResponse UpdateTotalAssignedAmount([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateTotalAssignedAmount((int)jobj["CostcentreId"], (int)jobj["TotalAssignedAmount"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GettingObjectiveUnits")]
        [HttpGet]
        public ServiceResponse GettingObjectiveUnits()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GettingObjectiveUnits();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/AddEntity/{EntityID}/{Name}")]
        [HttpGet]
        public ServiceResponse AddEntity(String EntityID, String Name)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.AddEntity(EntityID, Name);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/UpdateEntityforSearch/{EntityID}/{Name}")]
        [HttpGet]
        public ServiceResponse UpdateEntityforSearch(String EntityID, String Name)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateEntityforSearch(EntityID, Name);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/RemoveEntity")]
        [HttpPost]
        public ServiceResponse RemoveEntity([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jEntityID = (JArray)jobj["ID"];
                foreach (var entityId in jEntityID)
                {
                    result.Response = marcomManager.PlanningManager.RemoveEntity((int)entityId, "");
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/QuickSearch1")]
        [HttpPost]
        public ServiceResponse QuickSearch1([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                List<BrandSystems.Marcom.Core.Planning.SampleData> options = marcomManager.PlanningManager.PerformKeywordSearch((string)jobj["Text"], (string)jobj["Searchtype"]);
                List<object> suggestions = new List<object>();
                List<object> finalSearches = new List<object>();
                if (options != null)
                {
                    foreach (var str in options)
                    {
                        suggestions.Add(new
                        {
                            name = str.Name
                        });
                    }
                }
                finalSearches.Add(new { items = suggestions });
                result.Response = finalSearches[0];
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/QuickLuceneSearch")]
        [HttpPost]
        public ServiceResponse QuickLuceneSearch([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                string tenanturl = HttpContext.Current.Request.Url.Host;
                jobj["tenanthost"] = tenanturl;

                //calling lucene application
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["SearchEngineProvider"]);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Add("X-AuthorizationToken", Guid.NewGuid().ToString());
                        var response = client.PostAsJsonAsync("api/Values/QuickLuceneSearch", jobj).Result;
                        string jsonObStr = response.Content.ReadAsStringAsync().Result;
                        JObject jsonObj = (JObject)JsonConvert.DeserializeObject(jsonObStr);
                        result.Response = jsonObj["Response"];
                        string inputObj = JsonConvert.SerializeObject(jobj);
                        string output = JsonConvert.SerializeObject(jsonObj);

                        ErrorLog.LogFilePath = retFilePath;
                        bool bReturnLog = ErrorLog.CustomExternalRoutine("Input Request : " + Environment.NewLine + inputObj + Environment.NewLine + "OutPut Response is " + Environment.NewLine + output, DateTime.Now);
                    }
                }
                catch
                {
                    result.Response = null;
                }


            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }


        [Route("api/Planning/UpdateSearchEngine")]
        [HttpPost]
        public ServiceResponse UpdateSearchEngine([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateSearchEngine();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }


        [Route("api/Planning/GettingObjectiveSummaryBlockDetails/{ObjectiveId}")]
        [HttpGet]
        public ServiceResponse GettingObjectiveSummaryBlockDetails(int ObjectiveId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GettingObjectiveSummaryBlockDetails(ObjectiveId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GettingObjectiveFulfillmentBlockDetails/{ObjectiveId}")]
        [HttpGet]
        public ServiceResponse GettingObjectiveFulfillmentBlockDetails(int ObjectiveId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GettingObjectiveFulfillmentBlockDetails(ObjectiveId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/DuplicateEntities")]
        [HttpPost]
        public ServiceResponse DuplicateEntities([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                Dictionary<string, bool> duplicateitemslist = new Dictionary<string, bool>();
                JObject duplicatestodo = (JObject)jobj["DuplicateList"];
                JArray arrentitynames = (JArray)jobj["duplicateEntityNames"];
                List<string> listEntityNamesToDuplicate = new List<string>();

                if (arrentitynames != null)
                {
                    foreach (var arm in arrentitynames)
                    {
                        listEntityNamesToDuplicate.Add(arm.ToString());
                    }
                }

                if (duplicatestodo != null)
                {
                    foreach (var obj in duplicatestodo)
                    {
                        duplicateitemslist.Add(obj.Key, (bool)obj.Value);
                    }
                }

                JArray arrEntityIds = (JArray)jobj["EntityID"];
                foreach (var item in arrEntityIds)
                    result.Response = marcomManager.PlanningManager.DuplicateEntity((int)item, (int)jobj["ParentLevel"], (int)jobj["DuplicateTimes"], (bool)jobj["IsDuplicateChild"], duplicateitemslist, listEntityNamesToDuplicate);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/InsertAdditionalObjective")]
        [HttpPost]
        public ServiceResponse InsertAdditionalObjective([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray ObjectiveRating = (JArray)jobj["Ratings"];
                JArray arrmembers = (JArray)jobj["AdditionalObjectiveMembers"];
                List<string> listRating = new List<string>();
                IEntityRoleUser objMembersList = marcomManager.PlanningManager.Entityrolesservice();

                if (arrmembers != null)
                {
                    foreach (var arm in arrmembers)
                    {
                        objMembersList.Roleid = (int)arm["Roleid"];
                        objMembersList.Userid = (int)arm["Userid"];
                        objMembersList.IsInherited = false;
                        objMembersList.InheritedFromEntityid = 0;
                    }
                }
                foreach (var obj in ObjectiveRating)
                {
                    listRating.Add(Convert.ToString(obj));
                }
                if (((string)jobj["PlannedTarget"]).Length > 8)
                {
                    jobj["PlannedTarget"] = ((string)jobj["PlannedTarget"]).Substring(0, 8);
                }
                if (((string)jobj["TargetOutCome"]).Length > 8)
                {
                    jobj["TargetOutCome"] = ((string)jobj["TargetOutCome"]).Substring(0, 8);
                }
                result.Response = marcomManager.PlanningManager.InsertAdditionalObjective((int)jobj["EntityID"], (int)jobj["EntityTypeID"], (int)jobj["ObjectiveTypeID"], (string)jobj["Name"], (string)jobj["Instruction"], (bool)jobj["IsEnableFeedback"], (int)jobj["UnitId"], (decimal)jobj["PlannedTarget"], (decimal)jobj["TargetOutCome"], (int)jobj["RatingObjective"], (string)jobj["Comments"], (int)jobj["Fulfillment"], (int)jobj["ObjectiveStatus"], objMembersList, listRating);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GettingObjectivestoEntitySelect/{EntityID}")]
        [HttpGet]
        public ServiceResponse GettingObjectivestoEntitySelect(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GettingObjectivestoEntitySelect(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdateObjectiveSummaryBlockData")]
        [HttpPost]
        public ServiceResponse UpdateObjectiveSummaryBlockData([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                IList<object> Objratings = new List<object>();
                JArray objratingarr = (JArray)jobj["ObjectiveRatings"];

                if ((int)jobj["ObjectiveTypeId"] == 4)
                {
                    if (objratingarr != null)
                    {
                        foreach (var items in objratingarr)
                        {
                            Objratings.Add(new
                            {
                                RatingCaption = (string)items["RatingCaption"],
                                ObjectiveRatingID = (int)items["ObjectiveRatingID"],
                                IsRemoved = (bool)items["IsRemoved"]
                            });
                        }
                    }
                }
                else
                    Objratings = null;
                result.Response = marcomManager.PlanningManager.UpdateObjectiveSummaryBlockData((int)jobj["ObjectiveID"], (int)jobj["ObjectiveTypeId"], (string)jobj["ObjectiveDescription"], (bool)jobj["EnableCommnets"], (int)jobj["ObjectiveUnits"], (decimal)jobj["GlobalBaseLine"], (decimal)jobj["GlobalTarget"], Objratings);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GettingEditObjectiveFulfillmentDetails/{ObjectiveID}")]
        [HttpGet]
        public ServiceResponse GettingEditObjectiveFulfillmentDetails(int ObjectiveID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GettingEditObjectiveFulfillmentDetails(ObjectiveID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GettingEntityPredefineObjectives/{EntityID}")]
        [HttpGet]
        public ServiceResponse GettingEntityPredefineObjectives(int EntityID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GettingEntityPredefineObjectives(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/InsertPredefineObjectivesforEntity")]
        [HttpPost]
        public ServiceResponse InsertPredefineObjectivesforEntity([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jEntityObjectdata = (JArray)jobj["ObjectiveIDList"];
                List<int> objectiveIdList = new List<int>();
                foreach (var entattrobj in jEntityObjectdata)
                {
                    objectiveIdList.Add((int)entattrobj);
                }
                result.Response = marcomManager.PlanningManager.InsertPredefineObjectivesforEntity(objectiveIdList, (int)jobj["EntityID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/GettingPredefineObjectivesForEntityMetadata")]
        [HttpPost]
        public ServiceResponse GettingPredefineObjectivesForEntityMetadata([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray arrperiods = (JArray)jobj["Periods"];
                JArray arrobjAttibutevalues = (JArray)jobj["AttributeData"];
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                DateTime startDate = new DateTime();
                DateTime endDate = new DateTime();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {

                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                        }
                        else
                        {
                            if (objAttr["NodeID"].Type == JTokenType.Boolean)
                            {
                                entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                            }
                            if (objAttr["NodeID"].Type == JTokenType.Date)
                            {
                                entityAttr.Value = (objAttr["NodeID"].ToString());
                            }
                            if (objAttr["NodeID"].Type == JTokenType.Integer)
                            {
                                entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                            }
                            else
                                entityAttr.Value = objAttr["NodeID"].ToString();
                        }
                        listattributevalues.Add(entityAttr);
                    }
                }
                IList<IEntityPeriod> listEntityperiods = new List<IEntityPeriod>();
                if (arrperiods != null)
                {
                    foreach (var entityPeriodObj in arrperiods)
                    {
                        if (entityPeriodObj.Count() > 0)
                        {
                            for (int i = 0; i < entityPeriodObj.Count(); i++)
                            {
                                IEntityPeriod enti = marcomManager.PlanningManager.Entityperiodservice();
                                if (i == 0)
                                {
                                    startDate = (DateTime)entityPeriodObj[i]["startDate"];//.ToString("MM/dd/yyyy");
                                }
                                enti.Startdate = (DateTime)entityPeriodObj[i]["startDate"];
                                enti.EndDate = (DateTime)entityPeriodObj[i]["endDate"];
                                enti.Description = (string)entityPeriodObj[i]["comment"];
                                enti.SortOrder = (int)entityPeriodObj[i]["sortorder"];
                                if (entityPeriodObj.Count() - 1 == i)
                                {
                                    endDate = (DateTime)entityPeriodObj[i]["endDate"];
                                }
                                listEntityperiods.Add(enti);
                            }
                        }
                    }
                }

                result.Response = marcomManager.PlanningManager.GettingPredefineObjectivesForEntityMetadata(listattributevalues, startDate, endDate, (int)jobj["EntityTypeID"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/LoadPredefineObjectives/{EntityID}")]
        [HttpGet]
        public ServiceResponse LoadPredefineObjectives(int EntityID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.LoadPredefineObjectives(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdatePredefineObjectivesforEntity")]
        [HttpPost]
        public ServiceResponse UpdatePredefineObjectivesforEntity([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;

                if (((string)jobj["PlannedTarget"]).Length > 8)
                {
                    jobj["PlannedTarget"] = ((string)jobj["PlannedTarget"]).Substring(0, 8);
                }
                if (((string)jobj["TargetOutcome"]).Length > 8)
                {
                    jobj["TargetOutcome"] = ((string)jobj["TargetOutcome"]).Substring(0, 8);
                }
                result.Response = marcomManager.PlanningManager.UpdatePredefineObjectivesforEntity((int)jobj["ObjectiveEntityID"], (int)jobj["ObjectiveID"], (int)jobj["EntityID"], (decimal)jobj["PlannedTarget"], (decimal)jobj["TargetOutcome"], (int)jobj["RatingObjective"], (string)jobj["Comments"], (int)jobj["Status"], (int)jobj["Fulfillment"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GettingAddtionalObjectives/{EntityID}")]
        [HttpGet]
        public ServiceResponse GettingAddtionalObjectives(int EntityID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GettingAddtionalObjectives(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdatingObjectiveOverDetails")]
        [HttpPost]
        public ServiceResponse UpdatingObjectiveOverDetails([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdatingObjectiveOverDetails((int)jobj["ObjectiveID"], (string)jobj["ObjectiveName"], (string)jobj["ObjectiveDescription"], (string)jobj["Typeid"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdateAdditionalObjectivesforEntity")]
        [HttpPost]
        public ServiceResponse UpdateAdditionalObjectivesforEntity([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                if (((string)jobj["PlannedTarget"]).Length > 13)
                {
                    jobj["PlannedTarget"] = ((string)jobj["PlannedTarget"]).Substring(0, 13);
                }
                if (((string)jobj["TargetOutcome"]).Length > 13)
                {
                    jobj["TargetOutcome"] = ((string)jobj["TargetOutcome"]).Substring(0, 13);
                }
                result.Response = marcomManager.PlanningManager.UpdateAdditionalObjectivesforEntity((int)jobj["ObjectiveEntityID"], (int)jobj["EntityID"], (int)jobj["ObjectiveTypeID"], (string)jobj["ObjectiveInstruction"], (bool)jobj["IsEnableFeedback"], (int)jobj["UnitID"], (decimal)jobj["PlannedTarget"], (decimal)jobj["TargetOutcome"], (int)jobj["RatingObjective"], (string)jobj["Comments"], (int)jobj["Fulfillment"], (string)jobj["ObjectiveInstruction"], (int)jobj["status"], (string)jobj["Objectivename"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdateObjectiveFulfillmentCondition")]
        [HttpPost]
        public ServiceResponse UpdateObjectiveFulfillmentCondition([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray objFulfillData = (JArray)jobj["ObjectiveFulfillConditonValueData"];
                IList<IObjectiveFulfillCondtions> objFullfilConditionsList = new List<IObjectiveFulfillCondtions>();
                if (objFulfillData != null)
                {
                    foreach (var obj in objFulfillData)
                    {
                        foreach (var obj2 in obj["ObjectiveFulfillValues"])
                        {
                            IObjectiveFulfillCondtions objcon = marcomManager.PlanningManager.ObjectiveFulfillmentCondtionValuesService();
                            objcon.EntityTypeid = (int)obj2["ObjEntityType"];

                            if (obj2["ConditionType"] != null && obj2["ConditionType"].ToString().Length > 0)
                            {
                                objcon.ConditionType = (int)obj2["ConditionType"];
                            }
                            if (Convert.ToString(obj2["ObjEntityAttributes"]) != "0" && Convert.ToString(obj2["ObjEntityAttributes"]) != "")
                            {
                                if (obj2["ObjEntityAttributes"] != null && obj2["ObjEntityAttributes"].ToString().Length > 0)
                                {
                                    string[] attributeData = Convert.ToString(obj2["ObjEntityAttributes"]).Split('_');
                                    int attributeId = Convert.ToInt32(attributeData[0]);
                                    int attributeLevelId = Convert.ToInt32(attributeData[1]);
                                    objcon.Attributeid = attributeId;
                                    objcon.AttributeLevel = attributeLevelId;
                                    var ConditionValue = (dynamic)obj2["ObjAttributeOptionns"].ToArray();
                                    objcon.ObjectiveConditionValue = null;
                                    List<int> objCondVal = new List<int>();
                                    foreach (var conditionObj in ConditionValue)
                                    {
                                        var oe = (int)conditionObj;
                                        objCondVal.Add(oe);
                                    }
                                    objcon.ObjectiveConditionValue = objCondVal;
                                }
                            }
                            objFullfilConditionsList.Add(objcon);
                        }
                    }
                }
                string NewstartDate = "", NewendDate = "";
                if (jobj["ObjectiveStartDate"].ToString().Length > 1 && jobj["ObjectiveEndDate"].ToString().Length > 1 && jobj["ObjectiveStartDate"].ToString() != null && jobj["ObjectiveStartDate"].ToString() != "undefined")
                {
                    NewstartDate = (jobj["ObjectiveStartDate"].ToString()) == "" ? null : (DateTime.Parse((string)jobj["ObjectiveStartDate"]).ToString("yyyy-MM-dd") == "" ? null : DateTime.Parse((string)jobj["ObjectiveStartDate"]).ToString("yyyy-MM-dd"));
                    NewendDate = (jobj["ObjectiveEndDate"].ToString()) == "" ? null : (DateTime.Parse((string)jobj["ObjectiveEndDate"]).ToString("yyyy-MM-dd") == "" ? null : DateTime.Parse((string)jobj["ObjectiveEndDate"]).ToString("yyyy-MM-dd"));
                }
                result.Response = marcomManager.PlanningManager.UpdateObjectiveFulfillmentCondition((int)jobj["ObjectiveID"], NewstartDate, NewendDate, (int)jobj["ObjectiveDateRule"], (bool)jobj["IsMandatory"], objFullfilConditionsList, (string)jobj["ObjectiveFulfillDeatils"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/DeleteObjectiveFulfillment/{ObjectiveID}")]
        [HttpDelete]
        public ServiceResponse DeleteObjectiveFulfillment(int ObjectiveID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteObjectiveFulfillment(ObjectiveID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/GettingAdditionalObjRatings/{ObjectiveID}")]
        [HttpGet]
        public ServiceResponse GettingAdditionalObjRatings(int ObjectiveID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GettingAdditionalObjRatings(ObjectiveID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GettingPredefineObjRatings/{ObjectiveID}")]
        [HttpGet]
        public ServiceResponse GettingPredefineObjRatings(int ObjectiveID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GettingPredefineObjRatings(ObjectiveID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdateImageName")]
        [HttpPost]
        public ServiceResponse UpdateImageName([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateImageName((int)jobj["EntityID"], (int)jobj["AttributeID"], (string)jobj["FileName"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/EntityForecastAmountInsert")]
        [HttpPost]
        public ServiceResponse EntityForecastAmountInsert([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jr = (JArray)jobj["Quarter"];
                for (int i = 0; i < jr.Count(); i++)
                {

                    result.Response = marcomManager.PlanningManager.EntityForeCastInsert((int)jr[i]["EntityID"], (int)jr[i]["CostCenterID"], (Double)jr[i]["Amount"], (int)jr[i]["QuaterID"]);
                }
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetForeCastForCCDetl/{CostcenterId}")]
        [HttpGet]
        public ServiceResponse GetForeCastForCCDetl(int CostcenterId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetForeCastForCCDetl(CostcenterId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/EntityForecastAmountUpdate/{EntityID}")]
        [HttpPost]
        public ServiceResponse EntityForecastAmountUpdate(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.EntityForecastAmountUpdate(EntityID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/FundRequestByID/{ID}/{EntityID}")]
        [HttpDelete]
        public ServiceResponse FundRequestByID(int ID, int EntityID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteFundRequest(ID, EntityID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetAllWorkFlowStepsWithTasks/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetAllWorkFlowStepsWithTasks(int EntityID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetAllWorkFlowStepsWithTasks(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/InsertTaskWithAttachments")]
        [HttpPost]
        public ServiceResponse InsertTaskWithAttachments([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrobjMembers = (JArray)jobj["TaskMembers"];
                JArray arrAttchObj = (JArray)jobj["TaskAttachments"];
                JArray arrFilesObj = (JArray)jobj["TaskFiles"];

                IList<ITaskMember> listTaskMembers = new List<ITaskMember>();
                if (arrobjMembers != null)
                {
                    foreach (var arm in arrobjMembers)
                    {
                        ITaskMember entrole = marcomManager.AccessManager.TaskMemberservice();
                        entrole.Roleid = 4;
                        entrole.Userid = (int)arm["Userid"];
                        entrole.ApprovalRount = 1;
                        entrole.ApprovalStatus = false;
                        listTaskMembers.Add(entrole);
                    }
                }
                else if (arrobjMembers == null)
                {
                    listTaskMembers = null;
                }

                IList<ITask> listTask = new List<ITask>();
                if ((int)jobj["StepID"] != 0)
                {

                    ITask taskval = marcomManager.PlanningManager.TasksService();
                    taskval.StepID = (int)jobj["StepID"];
                    taskval.Name = (string)jobj["Name"];
                    taskval.TaskStatus = 1;
                    taskval.Description = (string)jobj["Description"];
                    taskval.DueDate = (DateTime)jobj["DueDate"];
                    taskval.PredefinedTaskID = (int)jobj["PredefinedTaskID"];
                    listTask.Add(taskval);
                }
                else if ((int)jobj["StepID"] == 0)
                {
                    listTask = null;
                }

                IList<ITaskAttachment> listTaskattachment = new List<ITaskAttachment>();
                if (arrAttchObj != null)
                {
                    foreach (var arm in arrAttchObj)
                    {
                        ITaskAttachment attachval = marcomManager.PlanningManager.TasksAttachmentService();
                        attachval.Name = (string)arm["FileName"] + (string)arm["Extension"];
                        attachval.ActiveFileid = 1;
                        attachval.ActiveVersionNo = 1;
                        attachval.Createdon = DateTime.UtcNow;
                        attachval.Typeid = 4;
                        listTaskattachment.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listTaskattachment = null;
                }

                IList<IFile> listFiles = new List<IFile>();
                if (arrFilesObj != null)
                {
                    foreach (var arm in arrFilesObj)
                    {
                        IFile attachval = marcomManager.CommonManager.Fileservice();
                        attachval.Checksum = (string)arm["Checksum"];
                        attachval.CreatedOn = (DateTime)arm["CreatedOn"];
                        attachval.Extension = (string)arm["Extension"];
                        attachval.MimeType = (string)arm["MimeType"];
                        attachval.Moduleid = (int)arm["ModuleID"];
                        attachval.Name = (string)arm["Name"];
                        attachval.Ownerid = (int)arm["OwnerID"];
                        attachval.Size = (long)arm["Size"];
                        attachval.VersionNo = (int)arm["VersionNo"];
                        attachval.strFileID = (string)arm["FileGuid"];
                        listFiles.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listTaskattachment = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.InsertTaskWithAttachments((int)jobj["ParentId"], (int)jobj["TaskTypeid"], (string)jobj["Name"], (int)jobj["StepID"], listTask, listTaskMembers, listTaskattachment, listFiles);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }

        [Route("api/Planning/GetWorkFlowTaskDetails/{TaskID}")]
        [HttpGet]
        public ServiceResponse GetWorkFlowTaskDetails(int TaskID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetWorkFlowTaskDetails(TaskID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdatingMilestoneStatus")]
        [HttpPost]
        public ServiceResponse UpdatingMilestoneStatus([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdatingMilestoneStatus((int)jobj["MilestoneId"], (int)jobj["MilestoneStatus"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdateEntityActiveStatus")]
        [HttpPut]
        public ServiceResponse UpdateEntityActiveStatus([FromBody]JObject dataobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                int EntityID = (int)dataobj["EntityID"];
                int Status = (int)dataobj["Status"];
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateEntityActiveStatus(EntityID, Status);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;

        }

        [Route("api/Planning/UpdateTaskStatus/{TaskID}/{Status}")]
        [HttpPut]
        public ServiceResponse UpdateTaskStatus(int TaskID, int Status)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateTaskStatus(TaskID, Status);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdateFundingReqStatus/{TaskID}/{Status}/{EntityID}")]
        [HttpPut]
        public ServiceResponse UpdateFundingReqStatus(int TaskID, int Status, int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateTaskStatus(TaskID, Status, EntityID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdatePredefineObjectiveinLineData")]
        [HttpPost]
        public ServiceResponse UpdatePredefineObjectiveinLineData([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdatePredefineObjectiveinLineData((int)jobj["ObjectiveId"], (int)jobj["EntityId"], (int)jobj["PlannedTarget"], (int)jobj["TargetOutcome"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/UpdateAdditionalObjectiveinLineData")]
        [HttpPost]
        public ServiceResponse UpdateAdditionalObjectiveinLineData([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateAdditionalObjectiveinLineData((int)jobj["ObjectiveId"], (string)jobj["Objectivename"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/UpdateUnassignedTaskStatus/{PredefinedTaskID}/{EntityID}")]
        [HttpPut]
        public ServiceResponse UpdateUnassignedTaskStatus(int PredefinedTaskID, int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateUnassignedTaskStatus(PredefinedTaskID, EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetWorkFlowSummary/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetWorkFlowSummary(int EntityID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetWorkFlowSummary(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/DeleteCostcentre")]
        [HttpPost]
        public ServiceResponse DeleteCostcentre([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jEntityID = (JArray)jobj["ID"];
                foreach (var costcentreId in jEntityID)
                {
                    result.Response = marcomManager.PlanningManager.DeleteCostcentre((int)costcentreId);
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/DeleteActivityPredefineObjective")]
        [HttpPost]
        public ServiceResponse DeleteActivityPredefineObjective([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;

                result.Response = marcomManager.PlanningManager.DeleteActivityPredefineObjective((int)jobj["EntityId"], (int)jobj["ObjectiveId"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/DeleteAdditionalObjective")]
        [HttpPost]
        public ServiceResponse DeleteAdditionalObjective([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;

                result.Response = marcomManager.PlanningManager.DeleteAdditionalObjective((int)jobj["ObjectiveId"], (int)jobj["EntityID"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/EntityOwnersList/{EntityID}")]
        [HttpGet]
        public ServiceResponse EntityOwnersList(int EntityID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.EntityOwnersList(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdateObjectiveOwner")]
        [HttpPost]
        public ServiceResponse UpdateObjectiveOwner([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;

                result.Response = marcomManager.PlanningManager.UpdateObjectiveOwner((int)jobj["EntityID"], (int)jobj["UserId"], (int)jobj["RoleId"], (int)jobj["OldUserId"], (int)jobj["EntityTypeID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/InsertTaskMembers")]
        [HttpPost]
        public ServiceResponse InsertTaskMembers([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray arrobjMembers = (JArray)jobj["TaskMembers"];

                IList<ITaskMember> listTaskMembers = new List<ITaskMember>();
                if (arrobjMembers != null)
                {
                    foreach (var arm in arrobjMembers)
                    {
                        ITaskMember entrole = marcomManager.AccessManager.TaskMemberservice();
                        entrole.Roleid = 4;
                        entrole.Userid = (int)arm["Userid"];
                        entrole.ApprovalRount = 1;
                        entrole.ApprovalStatus = false;
                        listTaskMembers.Add(entrole);
                    }
                }
                else if (arrobjMembers == null)
                {
                    listTaskMembers = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.InsertTaskMembers((int)jobj["ParentId"], (int)jobj["TaskID"], listTaskMembers);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }

        [Route("api/Planning/InsertTaskAttachments")]
        [HttpPost]
        public ServiceResponse InsertTaskAttachments([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray arrAttchObj = (JArray)jobj["TaskAttachments"];
                JArray arrFilesObj = (JArray)jobj["TaskFiles"];

                IList<ITaskAttachment> listTaskattachment = new List<ITaskAttachment>();
                if (arrAttchObj != null)
                {
                    foreach (var arm in arrAttchObj)
                    {
                        ITaskAttachment attachval = marcomManager.PlanningManager.TasksAttachmentService();
                        attachval.Name = (string)arm["FileName"] + (string)arm["Extension"];
                        attachval.ActiveFileid = 1;
                        attachval.ActiveVersionNo = 1;
                        attachval.Createdon = DateTime.UtcNow;
                        attachval.Typeid = 4;
                        listTaskattachment.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listTaskattachment = null;
                }

                IList<IFile> listFiles = new List<IFile>();
                if (arrFilesObj != null)
                {
                    foreach (var arm in arrFilesObj)
                    {
                        IFile attachval = marcomManager.CommonManager.Fileservice();
                        attachval.Checksum = (string)arm["Checksum"];
                        attachval.CreatedOn = (DateTime)arm["CreatedOn"];
                        attachval.Extension = (string)arm["Extension"];
                        attachval.MimeType = (string)arm["MimeType"];
                        attachval.Moduleid = (int)arm["ModuleID"];
                        attachval.Name = (string)arm["Name"];
                        attachval.Ownerid = (int)arm["OwnerID"];
                        attachval.Size = (long)arm["Size"];
                        attachval.VersionNo = (int)arm["VersionNo"];
                        attachval.strFileID = (string)arm["FileGuid"];
                        listFiles.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listFiles = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.InsertTaskAttachments((int)jobj["ParentId"], (int)jobj["TaskID"], listTaskattachment, listFiles);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }

        [Route("api/Planning/GetTaskAttachmentFile/{TaskID}")]
        [HttpGet]
        public ServiceResponse GetTaskAttachmentFile(int TaskID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetTaskAttachmentFile(TaskID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/DeleteFileByID/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteFileByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //InsertFile(string Name, int VersionNo, string MimeType, string Extension, long Size, int OwnerID, DateTime CreatedOn, string Checksum, int ModuleID, int EntityID, Guid FileGuid);
                result.Response = marcomManager.PlanningManager.DeleteFileByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdateObjectivestatus")]
        [HttpPost]
        public ServiceResponse UpdateObjectivestatus([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateObjectivestatus((int)jobj["ObjeciveID"], (int)jobj["ObjectiveStatus"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/financialcostcentrestatus/{EntityID}/{CostcentreID}")]
        [HttpGet]
        public ServiceResponse financialcostcentrestatus(int EntityID, int CostcentreID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.financialcostcentrestatus(EntityID, CostcentreID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetEntitiPeriodByIdForGantt/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetEntitiPeriodByIdForGantt(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntitiPeriodByIdForGantt(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = "";
            }
            return result;
        }


        [Route("api/Planning/GetPeriodByIdForGantt/{ID}")]
        [HttpGet]
        public ServiceResponse GetPeriodByIdForGantt(string ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetPeriodByIdForGantt(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = "";
            }
            return result;
        }

        [Route("api/Planning/GetEntitysLinkedToCostCenter/{ID}")]
        [HttpGet]
        public ServiceResponse GetEntitysLinkedToCostCenter(string ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntitysLinkedToCostCenter(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = "";
            }
            return result;
        }
        [Route("api/Planning/DuplicateRootLevelEntities")]
        [HttpPost]
        public ServiceResponse DuplicateRootLevelEntities([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                JArray arrEntityIds = (JArray)jobj["DuplicatesIds"];
                foreach (var item in arrEntityIds)
                    result.Response = marcomManager.PlanningManager.DuplicateEntity((int)item, 0, 1, false);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetFundRequestTaskDetails/{EntityUniqueKey}/{CostCentreID}")]
        [HttpGet]
        public ServiceResponse GetFundRequestTaskDetails(string EntityUniqueKey, int CostCentreID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetFundRequestTaskDetails(EntityUniqueKey, CostCentreID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetNewsfeedFundRequestTaskDetails/{FundID}")]
        [HttpGet]
        public ServiceResponse GetNewsfeedFundRequestTaskDetails(int FundID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetNewsfeedFundRequestTaskDetails(FundID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetMilestoneByEntityID/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetMilestoneByEntityID(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetMilestoneByEntityID(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = "";
            }
            return result;
        }

        [Route("api/Planning/PendingFundRequest/{EntityID}")]
        [HttpGet]
        public ServiceResponse PendingFundRequest(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.PendingFundRequest(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/UpdateLock/{EntityID}/{IsLock}")]
        [HttpGet]
        public ServiceResponse UpdateLock(int EntityID, int IsLock)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateLock(EntityID, IsLock);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/IsLockAvailable")]
        [HttpPost]
        public ServiceResponse IsLockAvailable([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;

                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jEntityID = (JArray)jobj["ID"];
                foreach (var entityId in jEntityID)
                {
                    result.Response = marcomManager.PlanningManager.IsLockAvailable((int)entityId);
                    if (result.Response == true)
                    {
                        return result;
                    }
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdateCostCentreApprovedBudget")]
        [HttpPost]
        public ServiceResponse UpdateCostCentreApprovedBudget([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray jr = (JArray)jobj["CCIDs"];
                int[] entityidList = jr.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateCostCentreApprovedBudget(entityidList);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/EnableDisableWorkFlow/{EntityID}/{IsEnableWorkflow}")]
        [HttpDelete]
        public ServiceResponse EnableDisableWorkFlow(int EntityID, bool IsEnableWorkflow)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.EnableDisableWorkFlow(EntityID, IsEnableWorkflow);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetApprovedBudgetDate/{ListId}")]
        [HttpGet]
        public ServiceResponse GetApprovedBudgetDate(string ListId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetApprovedBudgetDate(ListId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/GetAllPurchaseOrdersByEntityID/{EntityID}")]
        [HttpPut]
        public ServiceResponse GetAllPurchaseOrdersByEntityID(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetAllPurchaseOrdersByEntityID(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }




        [Route("api/Planning/CreateNewPurchaseOrder")]
        [HttpPost]
        public ServiceResponse CreateNewPurchaseOrder([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray arrPurchaseOrderDetail = (JArray)jobj["CostCenterArr"];
                JArray arrobjAttibutevalues = (JArray)jobj["AttributeData"];

                IList<IPurchaseOrder> iilistourchaseorder = new List<IPurchaseOrder>();

                IPurchaseOrder ilistourchaseorder = marcomManager.PlanningManager.PurchaseOrderservice();
                ilistourchaseorder.CreateDate = DateTime.Now;
                ilistourchaseorder.ApproverID = 0;
                ilistourchaseorder.ApprovedDate = null;
                ilistourchaseorder.Description = (string)jobj["Description"];
                ilistourchaseorder.Entityid = (int)jobj["Entityid"];
                ilistourchaseorder.ExpectedSpentDate = null; //(DateTime)jobj["ExpectedSpentDate"];
                ilistourchaseorder.PONumber = (string)jobj["PONumber"];
                ilistourchaseorder.SentDate = null;
                ilistourchaseorder.SupplierID = (int)jobj["SupplierID"];
                ilistourchaseorder.UserID = (int)jobj["UserID"];
                ilistourchaseorder.IsExternal = false;
                iilistourchaseorder.Add(ilistourchaseorder);

                IList<IPurchaseOrderDetail> iilistPurchaseDet = new List<IPurchaseOrderDetail>();
                if (arrPurchaseOrderDetail != null)
                {
                    foreach (var arrc in arrPurchaseOrderDetail)
                    {
                        IPurchaseOrderDetail ilistPurchaseDet = marcomManager.PlanningManager.PurchaseOrderDetailservice();
                        ilistPurchaseDet.CostCentreID = (int)arrc["CostcenterId"];
                        ilistPurchaseDet.Amount = (decimal)arrc["Amount"];
                        ilistPurchaseDet.CurrencyType = (int)arrc["CurrencyType"];
                        iilistPurchaseDet.Add(ilistPurchaseDet);
                    }
                }
                else if (arrPurchaseOrderDetail == null)
                {
                    iilistPurchaseDet = null;
                }

                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {

                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                        }
                        else
                        {
                            if (objAttr["NodeID"].Type == JTokenType.Boolean)
                            {
                                entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                            }
                            if (objAttr["NodeID"].Type == JTokenType.Date)
                            {
                                entityAttr.Value = (objAttr["NodeID"].ToString());
                            }
                            if (objAttr["NodeID"].Type == JTokenType.Integer)
                            {
                                entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                            }
                            else
                                entityAttr.Value = objAttr["NodeID"].ToString();
                        }

                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.CreateNewPurchaseOrder(iilistourchaseorder, iilistPurchaseDet, listattributevalues, (bool)jobj["DirectPurchaseorder"]);

                if (result.Response > 0)
                {

                    JArray jr = (JArray)jobj["CostCenterArr"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        if ((decimal)jr[i]["AvailableAmount"] >= (decimal)jr[i]["Amount"])
                        {
                            marcomManager.PlanningManager.EntityCommittedAmountInsert((int)jobj["Entityid"], (int)jr[i]["CostcenterId"], (decimal)jr[i]["AvailableAmount"], (decimal)jr[i]["Amount"]);
                        }
                    }

                }


            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }

        [Route("api/Planning/GetAllCurrencyType")]
        [HttpGet]
        public ServiceResponse GetAllCurrencyType()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetAllCurrencyType();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetAllSupplier")]
        [HttpGet]
        public ServiceResponse GetAllSupplier()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetAllSupplier();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetCurrencyListFFsettings")]
        [HttpGet]
        public ServiceResponse GetCurrencyListFFsettings()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetCurrencyTypeFFSettings();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/InsertUpdateCurrencyListFFSettings")]
        [HttpPost]
        public ServiceResponse InsertUpdateCurrencyListFFSettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.InsertUpdateCurrencyListFFSettings((int)jobj["Id"], (string)jobj["Name"], (string)jobj["Shorttext"], (string)jobj["Symbol"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Planning/DeleteCurrencyListFFSettings/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteCurrencyListFFSettings(string ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                var ids = ID;
                for (int i = 0; i < ids.Split(',').Length; i++)
                {
                    result.Response = marcomManager.PlanningManager.DeleteCurrencyListFFSettings(Convert.ToInt32(ids.Split(',')[i]));
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/getDivisonIds")]
        [HttpGet]
        public ServiceResponse getDivisonIds()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.getDivisonIds();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;

        }
        [Route("api/Planning/SetDivisonsFFSettings")]
        [HttpPost]
        public ServiceResponse SetDivisonsFFSettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.SetDivisonsFFSettings((int)jobj["DivisionId"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Planning/GetDivisonName")]
        [HttpGet]
        public ServiceResponse GetDivisonName()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetDivisonName();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;

        }

        [Route("api/Planning/EnableDisableWorkFlowStatus/{EntityID}")]
        [HttpGet]
        public ServiceResponse EnableDisableWorkFlowStatus(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.EnableDisableWorkFlowStatus(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/WorkFlowTaskCount/{EntityID}")]
        [HttpGet]
        public ServiceResponse WorkFlowTaskCount(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.WorkFlowTaskCount(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/ApprovePurchaseOrders")]
        [HttpPost]
        public ServiceResponse ApprovePurchaseOrders([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray jr = (JArray)jobj["POIDs"];
                int[] entityidList = jr.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.ApprovePurchaseOrders(entityidList, (int)jobj["EntityID"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/SendPurchaseOrders")]
        [HttpPost]
        public ServiceResponse SendPurchaseOrders([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray jr = (JArray)jobj["POIDs"];
                int[] entityidList = jr.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.SendPurchaseOrders(entityidList, (int)jobj["EntityID"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/RejectPurchaseOrders")]
        [HttpPost]
        public ServiceResponse RejectPurchaseOrders([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray jr = (JArray)jobj["POIDs"];
                int[] entityidList = jr.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.RejectPurchaseOrders(entityidList, (int)jobj["EntityID"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/CreateNewSupplier")]
        [HttpPost]
        public ServiceResponse CreateNewSupplier([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                IList<ISupplier> iilistsupplier = new List<ISupplier>();

                ISupplier ilistsupplier = marcomManager.PlanningManager.Supplierservice();
                ilistsupplier.Address1 = (string)jobj["Address1"];
                ilistsupplier.Address2 = (string)jobj["Address2"];
                ilistsupplier.City = (string)jobj["City"];
                ilistsupplier.CompanyName = (string)jobj["CompanyName"];
                ilistsupplier.Country = (string)jobj["Country"];
                ilistsupplier.CustomerNumber = (string)jobj["CustomerNumber"];
                ilistsupplier.Department = (string)jobj["Department"];
                ilistsupplier.Email = (string)jobj["Email"];
                ilistsupplier.Fax = (string)jobj["Fax"];
                ilistsupplier.Phone = (string)jobj["Phone"];
                ilistsupplier.State = (string)jobj["State"];
                ilistsupplier.Vat = (string)jobj["Vat"];
                ilistsupplier.ZipCode = (string)jobj["ZipCode"];
                iilistsupplier.Add(ilistsupplier);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.CreateNewSupplier(iilistsupplier);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }

        [Route("api/Planning/GetAllInvoiceByEntityID/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetAllInvoiceByEntityID(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetAllInvoiceByEntityID(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetAllSentPurchaseOrdersByEntityID/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetAllSentPurchaseOrdersByEntityID(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetAllSentPurchaseOrdersByEntityID(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }


        [Route("api/Planning/CreateNewInvoice")]
        [HttpPost]
        public ServiceResponse CreateNewInvoice([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray arrPurchaseOrderDetail = (JArray)jobj["CostCenterArr"];
                JArray arrobjAttibutevalues = (JArray)jobj["AttributeData"];

                IList<IInvoice> iilistourchaseorder = new List<IInvoice>();

                IInvoice ilistourchaseorder = marcomManager.PlanningManager.Invoiceservice();
                ilistourchaseorder.CreateDate = DateTime.Now;
                ilistourchaseorder.ApproverID = 0;
                ilistourchaseorder.Description = (string)jobj["Description"];
                ilistourchaseorder.Entityid = (int)jobj["Entityid"];
                ilistourchaseorder.InvoiceNumber = (string)jobj["Invoicenumber"];
                ilistourchaseorder.PONumber = (string)jobj["PONumber"];
                ilistourchaseorder.POID = (string)jobj["POID"];

                ilistourchaseorder.SupplierID = (int)jobj["SupplierID"];
                ilistourchaseorder.UserID = (int)jobj["UserID"];
                iilistourchaseorder.Add(ilistourchaseorder);

                IList<IInvoiceDetail> iilistPurchaseDet = new List<IInvoiceDetail>();
                if (arrPurchaseOrderDetail != null)
                {
                    foreach (var arrc in arrPurchaseOrderDetail)
                    {
                        IInvoiceDetail ilistPurchaseDet = marcomManager.PlanningManager.InvoiceDetailservice();
                        ilistPurchaseDet.CostCentreID = (int)arrc["CostcenterId"];
                        ilistPurchaseDet.Amount = (decimal)arrc["Amount"];
                        ilistPurchaseDet.CurrencyType = (int)arrc["CurrencyType"];
                        iilistPurchaseDet.Add(ilistPurchaseDet);
                    }
                }
                else if (arrPurchaseOrderDetail == null)
                {
                    iilistPurchaseDet = null;
                }

                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {

                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                        }
                        else
                        {
                            if (objAttr["NodeID"].Type == JTokenType.Boolean)
                            {
                                entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                            }
                            if (objAttr["NodeID"].Type == JTokenType.Date)
                            {
                                entityAttr.Value = (objAttr["NodeID"].ToString());
                            }
                            if (objAttr["NodeID"].Type == JTokenType.Integer)
                            {
                                entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                            }
                            else
                                entityAttr.Value = objAttr["NodeID"].ToString();
                        }

                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.CreateNewInvoice(iilistourchaseorder, iilistPurchaseDet, listattributevalues);

                if (result.Response > 0)
                {

                    JArray jr = (JArray)jobj["CostCenterArr"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        if ((decimal)jr[i]["AvailableAmount"] >= (decimal)jr[i]["Amount"])
                        {
                            marcomManager.PlanningManager.EntitySpendAmountInsert((int)jobj["Entityid"], (int)jr[i]["CostcenterId"], (decimal)jr[i]["AvailableAmount"], (decimal)jr[i]["Amount"]);
                        }
                        else
                        {
                            marcomManager.PlanningManager.EntitySpendAmountInsert((int)jobj["Entityid"], (int)jr[i]["CostcenterId"], (decimal)jr[i]["AvailableAmount"], (decimal)jr[i]["Amount"]);

                        }
                    }

                }


            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }
        [Route("api/Planning/CreateInvoiceAndPurchaseOrder")]
        [HttpPost]
        public ServiceResponse CreateInvoiceAndPurchaseOrder([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrPurchaseOrderDetail = (JArray)jobj["CostCenterArr"];

                IList<IInvoice> iilistInvoice = new List<IInvoice>();

                IInvoice ilistInvoioce = marcomManager.PlanningManager.Invoiceservice();
                ilistInvoioce.CreateDate = DateTime.Now;
                ilistInvoioce.ApproverID = (int)jobj["ApproverID"];
                ilistInvoioce.Description = (string)jobj["Description"];
                ilistInvoioce.Entityid = (int)jobj["Entityid"];
                ilistInvoioce.InvoiceNumber = (string)jobj["Invoicenumber"];
                ilistInvoioce.SupplierID = (int)jobj["SupplierID"];
                ilistInvoioce.UserID = (int)jobj["UserID"];
                iilistInvoice.Add(ilistInvoioce);

                IList<IInvoiceDetail> iilistInvoiceDet = new List<IInvoiceDetail>();
                if (arrPurchaseOrderDetail != null)
                {
                    foreach (var arrc in arrPurchaseOrderDetail)
                    {
                        IInvoiceDetail ilistInvoiceDet = marcomManager.PlanningManager.InvoiceDetailservice();
                        ilistInvoiceDet.CostCentreID = (int)arrc["CostcenterId"];
                        ilistInvoiceDet.Amount = (decimal)arrc["Amount"];
                        ilistInvoiceDet.CurrencyType = (int)arrc["CurrencyType"];
                        iilistInvoiceDet.Add(ilistInvoiceDet);
                    }
                }
                else if (arrPurchaseOrderDetail == null)
                {
                    iilistInvoiceDet = null;
                }


                IList<IPurchaseOrder> iilistourchaseorder = new List<IPurchaseOrder>();

                IPurchaseOrder ilistourchaseorder = marcomManager.PlanningManager.PurchaseOrderservice();
                ilistourchaseorder.CreateDate = DateTime.Now;
                ilistourchaseorder.ApproverID = (int)jobj["ApproverID"];
                ilistourchaseorder.ApprovedDate = DateTime.Now;
                ilistourchaseorder.Description = (string)jobj["Description"];
                ilistourchaseorder.Entityid = (int)jobj["Entityid"];
                ilistourchaseorder.ExpectedSpentDate = DateTime.Now;
                ilistourchaseorder.SentDate = DateTime.Now;
                ilistourchaseorder.SupplierID = (int)jobj["SupplierID"];
                ilistourchaseorder.UserID = (int)jobj["UserID"];
                iilistourchaseorder.Add(ilistourchaseorder);

                IList<IPurchaseOrderDetail> iilistPurchaseDet = new List<IPurchaseOrderDetail>();
                if (arrPurchaseOrderDetail != null)
                {
                    foreach (var arrc in arrPurchaseOrderDetail)
                    {
                        IPurchaseOrderDetail ilistPurchaseDet = marcomManager.PlanningManager.PurchaseOrderDetailservice();
                        ilistPurchaseDet.CostCentreID = (int)arrc["CostcenterId"];
                        ilistPurchaseDet.Amount = (decimal)arrc["Amount"];
                        ilistPurchaseDet.CurrencyType = (int)arrc["CurrencyType"];
                        iilistPurchaseDet.Add(ilistPurchaseDet);
                    }
                }
                else if (arrPurchaseOrderDetail == null)
                {
                    iilistPurchaseDet = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.CreateInvoiceAndPurchaseOrder(iilistInvoice, iilistInvoiceDet, iilistourchaseorder, iilistPurchaseDet);

                if (result.Response > 0)
                {

                    JArray jr = (JArray)jobj["CostCenterArr"];

                    //for(int i = 0 ; i < jr.Count() ; i++)
                    //{
                    //    if ((decimal)jr[i]["AvailableAmount"] >= (decimal)jr[i]["Amount"])
                    //    {
                    //        marcomManager.PlanningManager.EntityDirectSpentCommittedAmountInsert((int)jobj["Entityid"], (int)jr[i]["CostcenterId"], (decimal)jr[i]["AvailableAmount"], (decimal)jr[i]["Amount"]);
                    //    }
                    //    else
                    //    {
                    //        marcomManager.PlanningManager.EntityDirectSpentCommittedAmountInsert((int)jobj["Entityid"], (int)jr[i]["CostcenterId"], (decimal)jr[i]["AvailableAmount"], (decimal)jr[i]["Amount"]);

                    //    }
                    //}

                    for (int i = 0; i < jr.Count(); i++)
                    {
                        if ((decimal)jr[i]["AvailableAmount"] >= (decimal)jr[i]["Amount"])
                        {
                            marcomManager.PlanningManager.EntitySpendAmountInsert((int)jobj["Entityid"], (int)jr[i]["CostcenterId"], (decimal)jr[i]["AvailableAmount"], (decimal)jr[i]["Amount"]);
                        }
                        else
                        {
                            marcomManager.PlanningManager.EntitySpendAmountInsert((int)jobj["Entityid"], (int)jr[i]["CostcenterId"], (decimal)jr[i]["AvailableAmount"], (decimal)jr[i]["Amount"]);

                        }
                    }

                }


            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }

        [Route("api/Planning/UpdatePurchaseOrder")]
        [HttpPost]
        public ServiceResponse UpdatePurchaseOrder([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray arrPurchaseOrderDetail = (JArray)jobj["CostCenterArr"];
                JArray arrobjAttibutevalues = (JArray)jobj["AttributeData"];


                IList<IPurchaseOrder> iilistourchaseorder = new List<IPurchaseOrder>();

                IPurchaseOrder ilistourchaseorder = marcomManager.PlanningManager.PurchaseOrderservice();
                ilistourchaseorder.Description = (string)jobj["Description"];
                ilistourchaseorder.SupplierID = (int)jobj["SupplierID"];
                ilistourchaseorder.UserID = (int)jobj["UserID"];
                ilistourchaseorder.Entityid = (int)jobj["Entityid"];
                iilistourchaseorder.Add(ilistourchaseorder);

                IList<IPurchaseOrderDetail> iilistPurchaseDet = new List<IPurchaseOrderDetail>();
                if (arrPurchaseOrderDetail != null)
                {
                    foreach (var arrc in arrPurchaseOrderDetail)
                    {
                        IPurchaseOrderDetail ilistPurchaseDet = marcomManager.PlanningManager.PurchaseOrderDetailservice();
                        ilistPurchaseDet.Id = (int)arrc["PODetailID"];
                        ilistPurchaseDet.CostCentreID = (int)arrc["CostcenterId"];
                        ilistPurchaseDet.Amount = (decimal)arrc["Amount"];
                        ilistPurchaseDet.CurrencyType = (int)arrc["CurrencyType"];
                        iilistPurchaseDet.Add(ilistPurchaseDet);
                    }
                }
                else if (arrPurchaseOrderDetail == null)
                {
                    iilistPurchaseDet = null;
                }

                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {

                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                        }
                        else
                        {
                            if (objAttr["NodeID"].Type == JTokenType.Boolean)
                            {
                                entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                            }
                            if (objAttr["NodeID"].Type == JTokenType.Date)
                            {
                                entityAttr.Value = (objAttr["NodeID"].ToString());
                            }
                            if (objAttr["NodeID"].Type == JTokenType.Integer)
                            {
                                entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                            }
                            else
                                entityAttr.Value = objAttr["NodeID"].ToString();
                        }

                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdatePurchaseOrder((int)jobj["PurchaseOrderID"], iilistourchaseorder, iilistPurchaseDet, listattributevalues);

                if (result.Response > 0)
                {

                    JArray jr = (JArray)jobj["CostCenterArr"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        if ((decimal)jr[i]["AvailableAmount"] >= (decimal)jr[i]["Amount"])
                        {
                            marcomManager.PlanningManager.EntityCommittedAmountInsert((int)jobj["Entityid"], (int)jr[i]["CostcenterId"], (decimal)jr[i]["AvailableAmount"], (decimal)jr[i]["Amount"]);
                        }
                    }

                }


            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }

        [Route("api/Planning/PeriodAvailability/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse PeriodAvailability(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.PeriodAvailability(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/GetPlanningTransactionsByEID")]
        [HttpPost]
        public ServiceResponse GetPlanningTransactionsByEID([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetPlanningTransactionsByEID((JArray)jobj["FinCCObj"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/DeletePlanTransactions")]
        [HttpPost]
        public ServiceResponse DeletePlanTransactions([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeletePlanTransactions((JArray)jobj["PlanObj"], (int)jobj["EntityID"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetEntityAttributesValidationDetails/{ID}/{entityTypeID}")]
        [HttpGet]
        public ServiceResponse GetEntityAttributesValidationDetails(int ID, int entityTypeID)
        {

            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntityAttributesValidationDetails(ID, entityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/GetEntityStatusByEntityID/{entityId}")]
        [HttpGet]
        public ServiceResponse GetEntityStatusByEntityID(int entityId)
        {

            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntityStatusByEntityID(entityId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/UpdateEntityStatus")]
        [HttpPut]
        public ServiceResponse UpdateEntityStatus([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateEntityStatus((int)jobj["EntityID"], (int)jobj["EntityStatusID"], (int)jobj["OnTimeStatus"], (string)jobj["OnTimeComment"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/CheckForMemberAvailabilityForEntity/{EntityID}")]
        [HttpGet]
        public ServiceResponse CheckForMemberAvailabilityForEntity(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.CheckForMemberAvailabilityForEntity(EntityID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/CreateAttributeGroupRecord")]
        [HttpPost]
        public ServiceResponse CreateAttributeGroupRecord([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray arrobjAttibutevalues = (JArray)jobj["AttributeData"];
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {
                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                        }
                        else
                        {
                            if (entityAttr.ID != (int)SystemDefinedAttributes.CreationDate)
                            {
                                if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                {
                                    entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Date)
                                {
                                    entityAttr.Value = (objAttr["NodeID"].ToString());//Newtonsoft.Json.JsonConvert.DeserializeObject<string>(obj["Value"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Integer)
                                {
                                    entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                }
                                else
                                    entityAttr.Value = objAttr["NodeID"].ToString();
                            }
                            else
                            {
                                entityAttr.Value = DateTime.UtcNow.ToString("yyyy-MM-dd");
                            }
                        }

                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.CreateAttributeGroupRecord((int)jobj["AttributeGroupRecordID"], (int)jobj["ParentId"], (int)jobj["Typeid"], (int)jobj["GroupID"], ((int)jobj["IsLock"] == 0 ? false : true), (String)jobj["Name"], (int)jobj["SortOrder"], listattributevalues, false, false, 0);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Planning/GetEntityRelatedDataOnLoad/{EntityID}/{UserID}/{EntityTypeID}/{IsAdmin}/{MilestoneEntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetEntityRelatedDataOnLoad(int EntityID, int UserID, int EntityTypeID, bool IsAdmin, int MilestoneEntityTypeID)
        {
            result = new ServiceResponse();
            List<object> ResultsData = new List<object>();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                ResultsData.Add(marcomManager.MetadataManager.InsertEntityHistory(EntityID, UserID));
                ResultsData.Add(marcomManager.PlanningManager.GetEntityStatusByEntityID(EntityID));
                ResultsData.Add(marcomManager.MetadataManager.GetEntityStatus(EntityTypeID, IsAdmin, EntityID));
                ResultsData.Add(marcomManager.TaskManager.GetOverViewEntityTaskList(EntityID));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = ResultsData;
            }

            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;

            }
            return result;
        }

        [Route("api/Planning/GetEntityRelatedDataOnLoadSet1/{EntityID}/{UserID}/{EntityTypeID}/{IsAdmin}/{MilestoneEntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetEntityRelatedDataOnLoadSet1(int EntityID, int USerID, int EntityTypeID, bool IsAdmin, int MilestoneEntityTypeID)
        {
            result = new ServiceResponse();
            List<object> ResultsData = new List<object>();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                ResultsData.Add(marcomManager.PlanningManager.GetMilestoneMetadata(EntityID, MilestoneEntityTypeID));
                ResultsData.Add(marcomManager.MetadataManager.GetEntityTypeAttributeRelationWithLevelsByID(1, 0));
                ResultsData.Add(marcomManager.MetadataManager.GetValidationDationByEntitytype(MilestoneEntityTypeID));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = ResultsData;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;

            }
            return result;
        }

        [Route("api/Planning/GetDataForBreadcrumLoadWithLocalWorkSpacePath/{EntityID}/{IsWorkspace}")]
        [HttpGet]
        public ServiceResponse GetDataForBreadcrumLoadWithLocalWorkSpacePath(int EntityID, bool IsWorkspace)
        {
            result = new ServiceResponse();
            List<object> ResultsData = new List<object>();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                ResultsData.Add(marcomManager.PlanningManager.PeriodAvailability(EntityID));
                ResultsData.Add(marcomManager.MetadataManager.GetPath(EntityID, IsWorkspace));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = ResultsData;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/GetDataForBreadcrumLoadWithPath/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetDataForBreadcrumLoadWithPath(int EntityID)
        {
            result = new ServiceResponse();
            List<object> ResultsData = new List<object>();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                ResultsData.Add(marcomManager.PlanningManager.PeriodAvailability(EntityID));
                ResultsData.Add(marcomManager.MetadataManager.GetPath(EntityID));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = ResultsData;

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/GetEntityRelatedDataOnLoad_Set2/{EntityID}/{UserID}/{EntityTypeID}/{IsAdmin}/{MilestoneEntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetEntityRelatedDataOnLoad_Set2(int EntityID, int UserID, int EntityTypeID, bool IsAdmin, int MilestoneEntityTypeID)
        {
            result = new ServiceResponse();
            List<object> ResultsData = new List<object>();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                ResultsData.Add(marcomManager.MetadataManager.GetAttributeToAttributeRelationsByIDForEntity(EntityTypeID, EntityID));
                ResultsData.Add(marcomManager.PlanningManager.GetEntityAttributesValidationDetails(EntityID, EntityTypeID));
                ResultsData.Add(marcomManager.PlanningManager.GettingCostcentreFinancialOverview(EntityID));
                ResultsData.Add(marcomManager.PlanningManager.GetEntityAttributesDetails(EntityID));
                ResultsData.Add("");
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = ResultsData;
            }

            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;

            }
            return result;
        }

        [Route("api/Planning/GetEntityRelatedDataOnLoad_detailblock/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetEntityRelatedDataOnLoad_detailblock(int EntityID)
        {
            result = new ServiceResponse();
            List<object> ResultsData = new List<object>();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                //bool bReturnLog = false;
                //// get the base directory
                //string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();

                //// search the file below the current directory
                //string retFilePath = baseDir + "//" + "LogFile-Copy.txt";

                //ErrorLog.LogFilePath = retFilePath;
                ////false for writing log entry to customized text file
                //bReturnLog = ErrorLog.CustomErrorRoutine(false, "Request reach at GetEntityRelatedDataOnLoad_Set2 ************************************************/n" + Environment.NewLine, DateTime.Now);

                //Stopwatch sw = new Stopwatch();
                //sw.Start();
                //bReturnLog = ErrorLog.CustomErrorRoutine(false, "GetEntityAttributesDetails started  at " + Environment.NewLine, DateTime.Now);
                ResultsData.Add(marcomManager.PlanningManager.GetEntityAttributesDetails(EntityID));
                //sw.Stop();
                //bReturnLog = ErrorLog.CustomErrorRoutine(false, "GetEntityAttributesDetails done (total time is " + sw.ElapsedMilliseconds + ") " + Environment.NewLine, DateTime.Now);

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = ResultsData;
            }

            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;

            }
            return result;
        }

        [Route("api/Planning/GetCostCenterRelatedDataOnLoad/{EntityID}/{UserID}/{EntityTypeID}/{IsAdmin}")]
        [HttpGet]
        public ServiceResponse GetCostCenterRelatedDataOnLoad(int EntityID, int UserID, int EntityTypeID, bool IsAdmin)
        {
            result = new ServiceResponse();
            List<object> ResultsData = new List<object>();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                ResultsData.Add(marcomManager.PlanningManager.GetEntityStatusByEntityID(EntityID));
                ResultsData.Add(marcomManager.MetadataManager.GetEntityStatus(EntityTypeID, IsAdmin, EntityID));
                ResultsData.Add(marcomManager.TaskManager.GetOverViewEntityTaskList(EntityID));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = ResultsData;
            }

            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;

            }
            return result;
        }

        [Route("api/Planning/GetCostCenterRelatedDataOnLoad_Set2/{EntityID}/{UserID}/{EntityTypeID}/{IsAdmin}")]
        [HttpGet]
        public ServiceResponse GetCostCenterRelatedDataOnLoad_Set2(int EntityID, int UserID, int EntityTypeID, bool IsAdmin)
        {
            result = new ServiceResponse();
            List<object> ResultsData = new List<object>();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                ResultsData.Add(marcomManager.MetadataManager.GetAttributeToAttributeRelationsByIDForEntity(EntityTypeID, EntityID));
                ResultsData.Add(marcomManager.PlanningManager.GetEntityAttributesDetails(EntityID));
                ResultsData.Add(marcomManager.PlanningManager.GettingCostcentreFinancialOverview(EntityID));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = ResultsData;
            }

            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;

            }
            return result;
        }

        [Route("api/Planning/GetObjectiveRelatedDataOnLoad/{ObjectiveID}")]
        [HttpGet]
        public ServiceResponse GetObjectiveRelatedDataOnLoad(int ObjectiveID)
        {
            result = new ServiceResponse();
            List<object> ResultsData = new List<object>();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                ResultsData.Add(marcomManager.TaskManager.GetOverViewEntityTaskList(ObjectiveID));
                //ResultsData.Add(marcomManager.PlanningManager.GettingObjectiveSummaryBlockDetails((int)planning.ObjectiveID));
                //ResultsData.Add(marcomManager.PlanningManager.GettingObjectiveFulfillmentBlockDetails((int)planning.ObjectiveID));
                //ResultsData.Add(marcomManager.PlanningManager.GettingObjectiveUnits());
                //ResultsData.Add(marcomManager.MetadataManager.GetFulfillmentEntityTypes());
                //ResultsData.Add(marcomManager.PlanningManager.EntityOwnersList((int)planning.ObjectiveID));                  

                result.Response = ResultsData;
            }

            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;

            }
            return result;
        }

        [Route("api/Planning/GetObjectiveRelatedDataOnLoadSet2/{ObjectiveID}")]
        [HttpGet]
        public ServiceResponse GetObjectiveRelatedDataOnLoadSet2(int ObjectiveID)
        {
            result = new ServiceResponse();
            List<object> ResultsData = new List<object>();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //ResultsData.Add(marcomManager.TaskManager.GetOverViewEntityTaskList((int)planning.ObjectiveID));
                ResultsData.Add(marcomManager.PlanningManager.GettingObjectiveSummaryBlockDetails(ObjectiveID));
                ResultsData.Add(marcomManager.PlanningManager.GettingObjectiveFulfillmentBlockDetails(ObjectiveID));
                ResultsData.Add(marcomManager.PlanningManager.GetAllObjectiveUnits(ObjectiveID));
                ResultsData.Add(marcomManager.MetadataManager.GetFulfillmentEntityTypes());
                ResultsData.Add(marcomManager.PlanningManager.EntityOwnersList(ObjectiveID));
                ResultsData.Add(marcomManager.MetadataManager.GetAttributeToAttributeRelationsByIDForEntity(0, ObjectiveID));
                ResultsData.Add(marcomManager.PlanningManager.GetEntityAttributesDetails(ObjectiveID));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = ResultsData;
            }

            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;

            }
            return result;
        }
        [Route("api/Planning/DeleteEntityAttributeGroupRecord/{GroupID}/{GroupRecordID}/{ParentID}")]
        [HttpDelete]
        public ServiceResponse DeleteEntityAttributeGroupRecord(int GroupID, int GroupRecordID, int ParentID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteEntityAttributeGroupRecord(GroupID, GroupRecordID, ParentID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/SaveUploaderImage")]
        [HttpPost]
        public ServiceResponse SaveUploaderImage([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.SaveUploaderImage((string)jobj["imgsourcepath"], (int)jobj["imgwidth"], (int)jobj["imgheight"], (int)jobj["imgX"], (int)jobj["imgY"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/FilterSettingsLoad/{elementNode}/{EntityTypeID}/{FilterType}/{OptionFrom}/{TypeID}")]
        [HttpGet]
        public ServiceResponse FilterSettingsLoad(string elementNode, int EntityTypeID, string FilterType, int OptionFrom, int TypeID)
        {
            result = new ServiceResponse();
            List<object> ResultsData = new List<object>();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                //  ResultsData.Add(marcomManager.MetadataManager.GetOptionsFromXML(elementNode, typeid, marcomManager.User.TenantID));
                ResultsData.Add(marcomManager.MetadataManager.GettingEntityTypeHierarchyForAdminTree(EntityTypeID));
                JArray idarr = new JArray();
                ResultsData.Add(marcomManager.MetadataManager.GettingFilterAttribute(TypeID, FilterType, OptionFrom, idarr));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = ResultsData;
            }

            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;

            }
            return result;
        }

        [Route("api/Planning/GetEntityAttributesDetailsUserDetails/{UserID}")]
        [HttpGet]
        public ServiceResponse GetEntityAttributesDetailsUserDetails(int UserID)
        {

            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntityAttributesDetailsUserDetails(UserID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/GetCurrentDivisionId")]
        [HttpGet]
        public ServiceResponse GetCurrentDivisionId()
        {

            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetCurrentDivisionId();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Planning/GetFundingCostcenterMetadata/{Metadatatype}")]
        [HttpGet]
        public ServiceResponse GetFundingCostcenterMetadata(int Metadatatype)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetFundingCostcenterMetadata(Metadatatype);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/Qtipservicedata/{id}/{timeformat}")]
        [HttpGet]
        public ServiceResponse Qtipservicedata(int id, int timeformat)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                var tree = id;
                result.Response = tree;

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/SaveFinancialDynamicValues")]
        [HttpPost]
        public ServiceResponse SaveFinancialDynamicValues([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray jrnewValue = (JArray)jobj["NewValue"];
                List<object> obj1 = new List<object>();
                foreach (var obj in jrnewValue)
                {
                    if (obj.Type == JTokenType.Boolean)
                    {
                        obj1.Add(Convert.ToBoolean(obj.ToString()));
                    }
                    else if (obj.Type == JTokenType.Date)
                    {
                        obj1.Add(obj.ToString());

                    }
                    else if (obj.Type == JTokenType.Integer)
                    {

                        obj1.Add(int.Parse(obj.ToString()));

                    }
                    else
                        obj1.Add(obj.ToString());
                }


                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.SaveFinancialDynamicValues((int)jobj["FinID"], (int)jobj["AttributetypeID"], (int)jobj["AttributeID"], obj1);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Planning/GetCaptionofPeriod/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetCaptionofPeriod(int EntityTypeID)
        {

            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetCaptionofPeriod(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/MemberAvailable")]
        [HttpPost]
        public ServiceResponse MemberAvailable([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jEntityID = (JArray)jobj["ID"];
                foreach (var entityId in jEntityID)
                {
                    result.Response = marcomManager.MetadataManager.GetOwnerForEntity((int)entityId);
                    if (result.Response == false)
                        return result;
                }
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetFinancialforecastData")]
        [HttpGet]
        public ServiceResponse GetFinancialforecastData()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetFinanncailforecastData();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            };
            return result;

        }
        [Route("api/Planning/UpdateFFData")]
        [HttpPost]
        public ServiceResponse UpdateFFData([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateFFData((string)jobj["FinancialData"], (bool)jobj["Status"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/CallbackSnippetSearch")]
        [HttpPost]
        public ServiceResponse CallbackSnippetSearch([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray jrnewValue = (JArray)jobj["metadata"];
                JArray jrEtypes = (JArray)jobj["ETypeID"];
                int[] entitytypeidList = jrEtypes.Select(jv => (int)jv["Id"]).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                List<BrandSystems.Marcom.Core.Planning.SearchTerm> searchterm = new List<BrandSystems.Marcom.Core.Planning.SearchTerm>();
                BrandSystems.Marcom.Core.Planning.SearchTerm srch = new BrandSystems.Marcom.Core.Planning.SearchTerm();
                if ((bool)jobj["isQuerystringdata"])
                    searchterm = marcomManager.PlanningManager.GetSearchTerm(jrnewValue);
                else
                {
                    foreach (var val in jrnewValue)
                    {
                        srch = new BrandSystems.Marcom.Core.Planning.SearchTerm(); srch.MetadataName = (string)val["AttributeCaption"];
                        JArray jrChildValue = (JArray)val["Values"];
                        if (jrChildValue != null) { foreach (var value in jrChildValue) { srch.MetadataValue.Add((string)value); } }
                        searchterm.Add(srch);
                    }
                }
                string tenanturl = HttpContext.Current.Request.Url.Host, searchtermSerilzed = "";
                if (searchterm.Count > 0) searchtermSerilzed = JsonConvert.SerializeObject(searchterm);
                jobj["tenanthost"] = tenanturl; jobj["userid"] = Convert.ToInt32(GetCookie(Request, "UserId")); jobj["searchterm"] = searchtermSerilzed;
                //result.Response = marcomManager.PlanningManager.CustomSearch((string)jobj["Text"], searchterm, (string)jobj["Searchtype"], entitytypeidList, (int)jobj["PageID"], (bool)jobj["IsGlobalAdmin"], (int)jobj["rowsperpage"], (string)jobj["CurrentScroll"], (bool)jobj["istag"], (string)jobj["orderbyfieldName"], (bool)jobj["desc"]);


                //calling lucene application

                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["SearchEngineProvider"]);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Add("X-AuthorizationToken", Guid.NewGuid().ToString());
                        var response = client.PostAsJsonAsync("api/Values/CallbackSnippetSearch", jobj).Result;
                        string jsonObStr = response.Content.ReadAsStringAsync().Result;
                        JObject jsonObj = (JObject)JsonConvert.DeserializeObject(jsonObStr);
                        result.Response = jsonObj["Response"];

                    }
                }
                catch
                {
                    result.Response = null;
                }

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GetLockStatus/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetLockStatus(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetLockStatus(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            };
            return result;

        }

        [Route("api/Planning/GetEntityDetailsByID/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetEntityDetailsByID(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntityDetailsByID(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/InsertUpdateEntityPeriodLst")]
        [HttpPost]
        public ServiceResponse InsertUpdateEntityPeriodLst([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.InsertUpdateEntityPeriodLst(jobj, (int)jobj["EntityID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/GetAttachmentEditFeature")]
        [HttpGet]
        public ServiceResponse GetAttachmentEditFeature()
        {
            result = new ServiceResponse();
            try
            {
               
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetAttachmentEditFeature();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/updateOverviewStatus")]
        [HttpPut]
        public ServiceResponse updateOverviewStatus([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;

                JArray jrnewValue = (JArray)jobj["metadata"];
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.updateOverviewStatus(jrnewValue);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/CreateCalender")]
        [HttpPost]
        public ServiceResponse CreateCalender([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray saveObj = (JArray)jobj["SelectedEntities"];
                List<int> selectedEntities = new List<int>();
                foreach (int ent in saveObj.ToList())
                {
                    selectedEntities.Add(ent);
                }
                List<int> SelectedTabs = new List<int>();
                foreach (int ent in jobj["SelectedTabs"].ToList())
                {
                    SelectedTabs.Add(ent);
                }

                string CalenderName = Convert.ToString(jobj["CalenderName"]);
                string CalenderDescription = Convert.ToString(jobj["CalenderDescription"]);
                var Calenderowner = jobj["Calenderowner"];
                DateTime? CalenderPublishedOn = new DateTime();


                if (jobj["CalenderPublishedOn"].ToString().Length > 1 && jobj["CalenderPublishedOn"].ToString() != null && jobj["CalenderPublishedOn"].ToString() != "undefined")
                {
                    var dateformat = DateTime.Parse((string)jobj["CalenderPublishedOn"]);
                    CalenderPublishedOn = DateTime.Parse(dateformat.ToString("yyyy-MM-dd"));
                }
                else
                {
                    CalenderPublishedOn = null;
                }

                // CalenderPublishedOn = Convert.ToString(jobj["CalenderPublishedOn"]) != "" ? (DateTime)(jobj["CalenderPublishedOn"]) : DateTime.Now;
                int CalenderVisPeriod = (int)jobj["CalenderVisPeriod"];
                int CalenderVisType = (int)jobj["CalenderVisType"];
                bool CalenderisExternal = (bool)jobj["CalenderisExternal"];
                var CalenderMembers = jobj["CalenderMembers"];
                var calenderOwnerData = jobj["CalenderOwner"];
                var CalenderFulfillData = jobj["CalenderFulfillConditonValueData"];

                IList<IEntityRoleUser> objMembersList = new List<IEntityRoleUser>();
                if (CalenderMembers != null || calenderOwnerData != null)
                {
                    foreach (var arm in CalenderMembers)
                    {
                        IEntityRoleUser entrole = marcomManager.PlanningManager.Entityrolesservice();
                        entrole.Roleid = (int)arm["Roleid"];
                        entrole.Userid = (int)arm["Userid"];
                        entrole.IsInherited = false;
                        entrole.InheritedFromEntityid = 0;
                        objMembersList.Add(entrole);
                    }
                    foreach (var objOwn in calenderOwnerData)
                    {
                        IEntityRoleUser entrole = marcomManager.PlanningManager.Entityrolesservice();
                        entrole.Roleid = (int)objOwn["Roleid"];
                        entrole.Userid = (int)objOwn["Userid"];
                        entrole.IsInherited = false;
                        entrole.InheritedFromEntityid = 0;
                        objMembersList.Add(entrole);
                    }
                }
                else if (CalenderMembers == null)
                {
                    objMembersList = null;
                }
                IList<ICalenderFulfillCondtions> objCalenderFullfilConditionsList = new List<ICalenderFulfillCondtions>();
                if (CalenderFulfillData != null)
                {
                    foreach (var obj in CalenderFulfillData)
                    {

                        foreach (var obj2 in obj["CalenderFulfillValues"])
                        {
                            ICalenderFulfillCondtions objcon = marcomManager.PlanningManager.CalenderFulfillmentCondtionValuesService();
                            objcon.EntityTypeid = obj2["CalEntityType"].Count()==0 ? 0 : (int)obj2["CalEntityType"];
                            objcon.ConditionType = obj2["CalConditionType"].Count() == 0 ? 0 : (int)obj2["CalConditionType"];

                            if ((obj2["CalEntityAttributes"].Count() == 0 ? "0" : Convert.ToString(obj2["CalEntityAttributes"])) != "0")
                            {
                                if (obj2["CalEntityAttributes"].Count() == 0)
                                {
                                    string[] attributeData = Convert.ToString(obj2["CalEntityAttributes"]).Split('_');
                                    int attributeId = 0;
                                    int attributeLevelId = 0;
                                    if (attributeData.Length > 1)
                                    {
                                        attributeId = Convert.ToInt32(attributeData[0]);
                                        attributeLevelId = Convert.ToInt32(attributeData[1]);
                                    }
                                    objcon.Attributeid = attributeId;
                                    objcon.AttributeLevel = attributeLevelId;

                                    var ConditionValue = (dynamic)obj2["CalAttributeOptionns"].ToArray();
                                    objcon.CalenderConditionValue = null;
                                    List<int> objCondVal = new List<int>();
                                    foreach (var conditionObj in ConditionValue)
                                    {
                                        var oe = (int)conditionObj;
                                        objCondVal.Add(oe);
                                    }
                                    objcon.CalenderConditionValue = objCondVal;
                                }
                            }

                            objCalenderFullfilConditionsList.Add(objcon);
                        }
                    }
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.CreateCalender(CalenderName, true, CalenderDescription, objCalenderFullfilConditionsList, objMembersList, selectedEntities, CalenderPublishedOn, CalenderVisPeriod, CalenderVisType, CalenderisExternal, SelectedTabs);

            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/GetEntitiesfrCalender")]
        [HttpPost]
        public ServiceResponse GetEntitiesfrCalender([FromBody]JArray jobj)
        {
            result = new ServiceResponse();
            try
            {


                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray objCalFulfillData = (JArray)jobj[0]["CalenderFulfillValues"];
                IList<ICalenderFulfillCondtions> objCalFullfilConditionsList = new List<ICalenderFulfillCondtions>();
                if (objCalFulfillData != null)
                {
                    foreach (var obj in objCalFulfillData)
                    {

                        //foreach (var obj2 in obj["CalenderFulfillValues"])
                        //{
                        ICalenderFulfillCondtions objcon = marcomManager.PlanningManager.CalenderFulfillmentCondtionValuesService();
                        objcon.EntityTypeid = (int)obj["CalEntityType"];
                        objcon.ConditionType = (int)obj["CalConditionType"];
                        if (Convert.ToString(obj["CalEntityAttributes"]) != "0" && Convert.ToString(obj["CalEntityAttributes"]) != "")
                        {
                            if (obj["CalEntityAttributes"] != null)
                            {
                                string[] attributeData = Convert.ToString(obj["CalEntityAttributes"]).Split('_');
                                int attributeId = Convert.ToInt32(attributeData[0]);
                                int attributeLevelId = Convert.ToInt32(attributeData[1]);
                                objcon.Attributeid = attributeId;
                                objcon.AttributeLevel = attributeLevelId;
                                var ConditionValue = (dynamic)obj["CalAttributeOptionns"].ToArray();
                                objcon.CalenderConditionValue = null;
                                List<int> objCondVal = new List<int>();
                                foreach (var conditionObj in ConditionValue)
                                {
                                    var oe = (int)conditionObj;
                                    objCondVal.Add(oe);
                                }
                                objcon.CalenderConditionValue = objCondVal;
                            }
                        }
                        objCalFullfilConditionsList.Add(objcon);
                        //}
                    }
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntitiesfrCalender(objCalFullfilConditionsList);


            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;

            }
            return result;
        }

        [Route("api/Planning/GetCalenders")]
        [HttpGet]
        public ServiceResponse GetCalenders()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetCalenders();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/GetEntitiesforSelectedCalender/{CalID}")]
        [HttpGet]
        public ServiceResponse GetEntitiesforSelectedCalender(int CalID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //result.Response = marcomManager.CommonManager.GetCustomTabsByTypeID((int)common.TypeID);
                result.Response = marcomManager.PlanningManager.GetEntitiesforSelectedCalender(CalID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/GettingCalenderFulfillmentBlockDetail/{CalenderId}")]
        [HttpGet]
        public ServiceResponse GettingCalenderFulfillmentBlockDetail(int CalenderId)
        {
            result = new ServiceResponse();
            List<object> ResultsData = new List<object>();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                ResultsData.Add(marcomManager.PlanningManager.GettingCalenderFulfillmentBlockDetails(CalenderId));
                ResultsData.Add(marcomManager.PlanningManager.EntityOwnersList(CalenderId));
                ResultsData.Add(marcomManager.PlanningManager.GetCalenderDetailsByID(CalenderId));
                ResultsData.Add(marcomManager.TaskManager.GetOverViewEntityTaskList(CalenderId));
                ResultsData.Add(marcomManager.PlanningManager.GetTabsforCalender(CalenderId));

                result.Response = ResultsData;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/GettingEditCalenderFulfillmentDetails/{CalenderId}")]
        [HttpGet]
        public ServiceResponse GettingEditCalenderFulfillmentDetails(int CalenderId)
        {
            result = new ServiceResponse();
            List<object> ResultsData = new List<object>();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;


                result.Response = marcomManager.PlanningManager.GettingEditCalenderFulfillmentDetails(CalenderId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdateCalenderFulfillmentCondition")]
        [HttpPost]
        public ServiceResponse UpdateCalenderFulfillmentCondition([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                JArray objFulfillData = (JArray)jobj["CalenderFulfillConditonValueData"];

                List<int> selectedEntities = new List<int>();
                foreach (int ent in jobj["selectedentities"].ToList())
                {
                    selectedEntities.Add(ent);
                }



                IList<ICalenderFulfillCondtions> objFullfilConditionsList = new List<ICalenderFulfillCondtions>();
                //var startDatearr = jobj["ObjectiveStartDate"].ToString().Split('/');
                //string startDate = startDatearr[2].Substring(0, 4) + "-" + startDatearr[1] + "-" + startDatearr[0];
                //var endDatearr = jobj["ObjectiveEndDate"].ToString().Split('/');
                //string endDate = endDatearr[2].Substring(0, 4) + "-" + endDatearr[1] + "-" + endDatearr[0];
                if (objFulfillData != null)
                {
                    foreach (var obj in objFulfillData)
                    {

                        foreach (var obj2 in obj["CalenderFulfillValues"])
                        {
                            ICalenderFulfillCondtions objcon = marcomManager.PlanningManager.CalenderFulfillmentCondtionValuesService();
                            objcon.EntityTypeid = (int)obj2["CalEntityType"];

                            if (obj2["CalConditionType"] != null && obj2["CalConditionType"].ToString().Length > 0)
                            {
                                objcon.ConditionType = (int)obj2["CalConditionType"];
                            }
                            if (Convert.ToString(obj2["CalEntityAttributes"]) != "0" && Convert.ToString(obj2["CalEntityAttributes"]) != "")
                            {
                                if (obj2["CalEntityAttributes"] != null && obj2["CalEntityAttributes"].ToString().Length > 0)
                                {

                                    string[] attributeData = Convert.ToString(obj2["CalEntityAttributes"]).Split('_');
                                    int attributeId = Convert.ToInt32(attributeData[0]);
                                    int attributeLevelId = Convert.ToInt32(attributeData[1]);
                                    objcon.Attributeid = attributeId;
                                    objcon.AttributeLevel = attributeLevelId;

                                    var ConditionValue = (dynamic)obj2["CalAttributeOptionns"].ToArray();
                                    objcon.CalenderConditionValue = null;
                                    List<int> objCondVal = new List<int>();
                                    foreach (var conditionObj in ConditionValue)
                                    {
                                        var oe = (int)conditionObj;
                                        objCondVal.Add(oe);
                                    }
                                    objcon.CalenderConditionValue = objCondVal;
                                }
                            }
                            objFullfilConditionsList.Add(objcon);
                        }
                    }
                }
                result.Response = marcomManager.PlanningManager.UpdateCalenderFulfillmentCondition((int)jobj["CalenderID"], objFullfilConditionsList, (string)jobj["CalenderFulfillDeatils"], selectedEntities);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/UpdatingCalenderOverDetails")]
        [HttpPost]
        public ServiceResponse UpdatingCalenderOverDetails([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdatingCalenderOverDetails((int)jobj["CalenderID"], (string)jobj["CalenderName"], (string)jobj["CalenderDescription"], (string)jobj["Typeid"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/SaveCalenderDetails")]
        [HttpPost]
        public ServiceResponse SaveCalenderDetails([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            List<object> ResultsData = new List<object>();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                var d = (string)jobj["PublishedOn"];
                DateTime CalenderPublishedOn = new DateTime();
                CalenderPublishedOn = Convert.ToString(jobj["PublishedOn"]) != "" ? (DateTime)(jobj["PublishedOn"]) : DateTime.Now;
                List<int> tabids = new List<int>();
                var selectedtabs = (JArray)jobj["SelectedTabs"];
                foreach (var id in selectedtabs)
                {
                    tabids.Add(Convert.ToInt32((string)id));
                }

                ResultsData.Add(marcomManager.PlanningManager.SaveCalenderDetails((int)jobj["CalenderID"], (bool)jobj["IsExternal"], (int)jobj["VisibilityPeriod"], (int)jobj["VisibilityType"], CalenderPublishedOn, tabids));
                //ResultsData.Add(marcomManager.PlanningManager.GetCalenderDetailsByID((int)jobj["CalenderID"]));
                result.Response = ResultsData;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/PerformViewMoreSearch")]
        [HttpPost]
        public ServiceResponse PerformViewMoreSearch([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                string tenanturl = HttpContext.Current.Request.Url.Host;
                jobj["tenanthost"] = tenanturl;
                jobj["userid"] = Convert.ToInt32(GetCookie(Request, "UserId"));

                //calling lucene application
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["SearchEngineProvider"]);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Add("X-AuthorizationToken", Guid.NewGuid().ToString());
                        var response = client.PostAsJsonAsync("api/Values/PerformViewMoreSearch", jobj).Result;
                        string jsonObStr = response.Content.ReadAsStringAsync().Result;
                        JObject jsonObj = (JObject)JsonConvert.DeserializeObject(jsonObStr);
                        result.Response = jsonObj["Response"];

                    }
                }
                catch
                {
                    result.Response = null;
                }

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/GetCalendarDetailsbyExternalID")]
        [HttpPost]
        public ServiceResponse GetCalendarDetailsbyExternalID([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            List<object> ResultsData = new List<object>();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetCalendarDetailsbyExternalID((string)jobj["ExternalUrlID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/CreateCmsPageEntity")]
        [HttpPost]
        public ServiceResponse CreateCmsPageEntity([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray arrperiods = (JArray)jobj["Periods"];
                JArray arrmembers = (JArray)jobj["EntityMembers"];
                JArray arrentitycost = (JArray)jobj["EntityCostRelations"];
                JArray arrobjentvalues = (JArray)jobj["IObjectiveEntityValue"];
                JArray arrobjAttibutevalues = (JArray)jobj["AttributeData"];
                JArray arrFilesObj = (JArray)jobj["AssetArr"]; // link assets for sub entities
                JArray arrentityamountcurrencytype = (JArray)jobj["Entityamountcurrencytype"];
                int[] assetIdArr = arrFilesObj.Select(jv => (int)jv).ToArray();

                IList<IEntityRoleUser> listEntityMembers = new List<IEntityRoleUser>();
                if (arrmembers.Count > 0)
                {
                    foreach (var arm in arrmembers)
                    {
                        IEntityRoleUser entrole = marcomManager.PlanningManager.Entityrolesservice();
                        entrole.Roleid = (int)arm["Roleid"];
                        entrole.Userid = (int)arm["Userid"];
                        entrole.IsInherited = false;
                        entrole.InheritedFromEntityid = 0;
                        listEntityMembers.Add(entrole);
                    }
                }
                else if (arrmembers.Count == 0)
                {
                    listEntityMembers = null;
                }

                IList<IFundingRequest> listFundrequest = null;

                IList<IEntityPeriod> listEntityperiods = new List<IEntityPeriod>();
                if (arrperiods != null)
                {
                    foreach (var entityPeriodObj in arrperiods)
                    {
                        if (entityPeriodObj.Count() > 0)
                        {
                            foreach (var arrp in entityPeriodObj)
                            {
                                DateTime startDate = new DateTime();
                                DateTime endDate = new DateTime();
                                if (DateTime.TryParse(arrp["startDate"].ToString(), out startDate) && DateTime.TryParse(arrp["endDate"].ToString(), out endDate))
                                {

                                    IEntityPeriod enti = marcomManager.PlanningManager.Entityperiodservice();
                                    enti.Startdate = startDate;
                                    enti.EndDate = endDate;
                                    enti.Description = (string)arrp["comment"];
                                    enti.SortOrder = (int)arrp["sortorder"];
                                    listEntityperiods.Add(enti);
                                }
                            }
                        }
                    }
                }
                else if (arrperiods == null)
                {
                    listEntityperiods = null;
                }
                IList<IEntityAmountCurrencyType> listentityamountcurrencytype = new List<IEntityAmountCurrencyType>();
                if (arrentityamountcurrencytype != null)
                {
                    if (arrentityamountcurrencytype.Count() > 0)
                    {
                        foreach (var item in arrentityamountcurrencytype)
                        {
                            foreach (var i in item)
                            {
                                IEntityAmountCurrencyType entamcur = marcomManager.PlanningManager.EntityAmountCurrencyTypeservice();
                                entamcur.Amount = (decimal)i["amount"];
                                entamcur.Currencytypeid = (int)i["currencytype"];
                                entamcur.Attributeid = (int)i["Attributeid"];
                                listentityamountcurrencytype.Add(entamcur);
                            }
                        }
                    }

                }
                else if (arrentityamountcurrencytype == null)
                {
                    arrentityamountcurrencytype = null;
                }
                IList<IEntityCostReleations> listentitycostcentres = new List<IEntityCostReleations>();
                if (arrentitycost != null)
                {
                    foreach (var arrc in arrentitycost)
                    {
                        IEntityCostReleations entcost = marcomManager.PlanningManager.EntityCostcentrerelationservice();
                        entcost.CostcenterId = (int)arrc["CostcenterId"];
                        entcost.Sortorder = (int)arrc["Sortorder"];
                        entcost.Isassociate = (int)arrc["Isassociate"] == 0 ? false : true;
                        entcost.Isactive = (int)arrc["Isactive"] == 0 ? false : true;
                        listentitycostcentres.Add(entcost);
                    }
                }
                else if (arrentitycost == null)
                {
                    listentitycostcentres = null;
                }
                IList<IObjectiveEntityValue> listobjentityvalues = new List<IObjectiveEntityValue>();
                if (arrobjentvalues != null)
                {
                    foreach (var arrentobj in arrobjentvalues)
                    {
                        IObjectiveEntityValue objentvalues = marcomManager.PlanningManager.ObjEnityvalservice();
                        objentvalues.Objectiveid = (int)arrentobj;
                        listobjentityvalues.Add(objentvalues);
                    }
                }
                else if (arrobjentvalues == null)
                {
                    listobjentityvalues = null;
                }
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {

                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                        }
                        else
                        {
                            if (entityAttr.ID != (int)SystemDefinedAttributes.CreationDate)
                            {
                                if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                {
                                    entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Date)
                                {
                                    entityAttr.Value = (objAttr["NodeID"].ToString());//Newtonsoft.Json.JsonConvert.DeserializeObject<string>(obj["Value"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Integer)
                                {
                                    entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                }
                                else
                                    entityAttr.Value = objAttr["NodeID"].ToString();
                            }
                            else
                            {
                                entityAttr.Value = DateTime.UtcNow.ToString("yyyy-MM-dd");
                            }
                        }
                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.CreateCmsPageEntity((int)jobj["ParentId"], (int)jobj["Typeid"], ((int)jobj["Active"] == 0 ? false : true), ((int)jobj["IsLock"] == 0 ? false : true), (String)jobj["Name"], listEntityMembers, listentitycostcentres, listEntityperiods, listFundrequest, listattributevalues, (int)jobj["NavID"], (int)jobj["TemplateID"], (string)jobj["PublishedDate"], (string)jobj["PublishedTime"], assetIdArr, listobjentityvalues, null, listentityamountcurrencytype);

            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }

        [Route("api/Planning/getfinancialForecastIds")]
        [HttpGet]
        public ServiceResponse getfinancialForecastIds()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.getfinancialForecastIds();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;

        }
        [Route("api/Planning/updatefinancialforecastsettings")]
        [HttpPost]
        public ServiceResponse updatefinancialforecastsettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateFinancialForecastsettings((int)jobj["Id"], (bool)jobj["IsFinancialforecast"], (int)jobj["ForecastDivision"], (int)jobj["ForecastBasis"], (int)jobj["ForecastLevel"], (int)jobj["Forecastdeadlines"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;

        }

        [Route("api/Planning/GetFinancialForecastsettings")]
        [HttpGet]
        public ServiceResponse GetFinancialForecastsettings()
        {
            result = new ServiceResponse();
            try
            {
               
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetFinancialForecastsettings();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;

        }

        [Route("api/Planning/GetEntityFinancialdForecastHeadings/{EntityID}/{DivisionID}/{Iscc}")]
        [HttpGet]
        public ServiceResponse GetEntityFinancialdForecastHeadings(int EntityID, int DivisionID, bool Iscc)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntityFinancialdForecastHeadings(EntityID, DivisionID, Iscc);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;

        }

        [Route("api/Planning/GetlastUpdatedtime/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetlastUpdatedtime(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetlastUpdatedtime(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;

        }

        //----------------------Added by Suganthi 09 Apr 2015-----------------------
        [Route("api/Planning/getSearchSnippetSourceData")]
        [HttpGet]
        public ServiceResponse getSearchSnippetSourceData()
        {
            result = new ServiceResponse();
            string contents = "";
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                string mappingfilesPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
                mappingfilesPath = mappingfilesPath + "Snippets" + @"\snippetSearchBox.txt";
                contents = File.ReadAllText(mappingfilesPath);
                result.Response = Tuple.Create(contents, marcomManager.DigitalAssetManager.GetDAMEntityTypes());
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;

        }

        [Route("api/Planning/GetCostcentreTreeforPlanCreation/{EntityTypeID}/{FiscalYear}/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetCostcentreTreeforPlanCreation(int EntityTypeID, int FiscalYear, int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetCostcentreTreeforPlanCreation(EntityTypeID, FiscalYear, EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Planning/GetCostCentreAssignedAmount/{CostCentreID}")]
        [HttpGet]
        public ServiceResponse GetCostCentreAssignedAmount(int CostCentreID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetCostCentreAssignedAmount(CostCentreID); ;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/GetCostCentreCurrencyRateById/{EntityId}/{CurrencyId}/{IsCostCentreCreation}")]
        [HttpGet]
        public ServiceResponse GetCostCentreCurrencyRateById(int EntityId, int CurrencyId, bool IsCostCentreCreation)
        {
            result = new ServiceResponse();
            string contents = "";
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetCostCentreCurrencyRateById(EntityId, CurrencyId, IsCostCentreCreation);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/DeleteApprovedAlloc")]
        [HttpPost]
        public ServiceResponse DeleteApprovedAlloc([FromBody]JObject UniqueIdobj)
        {
            result = new ServiceResponse();

            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteApprovedAlloc((string)UniqueIdobj["UniqueId"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }


        [Route("api/Planning/GetApprvdAlloc")]
        [HttpPost]
        public ServiceResponse GetApprvdAlloc([FromBody]JObject EntityId)
        {
            result = new ServiceResponse();

            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetApprvdAlloc((string)EntityId["Planlistids"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/InsertUpdateFinancialTrancation")]
        [HttpPost]
        public ServiceResponse InsertUpdateFinancialTrancation([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray jrnewValue = (JArray)jobj["FinTransData"];
                List<object> obj1 = new List<object>();
                for (int i = 0; i < jrnewValue.Count; i++)
                {
                    obj1.Add(new { EntityID = (int)jrnewValue[i]["EntityID"], CostCentreID = (int)jrnewValue[i]["CostCentreID"], TransactionType = (int)jrnewValue[i]["TransactionType"], PurchaseOrderID = (int)jrnewValue[i]["PurchaseOrderID"], SpentTransactionID = (int)jrnewValue[i]["SpentTransactionID"], ForecastingType = (string)jrnewValue[i]["ForecastingType"], CurrencyType = (int)jrnewValue[i]["CurrencyType"], Amount = (int)jrnewValue[i]["Amount"] });
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.InsertUpdateFinancialTrancation(obj1, (int)jobj["TransactionType"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;

        }

        [Route("api/Planning/UpdateTotalAssigAmtInFinancialTransaction")]
        [HttpPost]
        public ServiceResponse UpdateTotalAssigAmtInFinancialTransaction([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateTotalAssigAmtInFinancialTransaction((decimal)jobj["TotalAssignedAmount"], (int)jobj["CostCentreID"], (int)jobj["currencyType"], (decimal)jobj["currencyRate"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;

        }
        [Route("api/Planning/GetTaskMinMaxValue/{Tasktypeid}")]
        [HttpGet]
        public ServiceResponse GetTaskMinMaxValue(int Tasktypeid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetTaskMinMaxValue(Tasktypeid);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;

        }

        [Route("api/Planning/GetDirectEntityMember/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetDirectEntityMember(string EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;

                result.Response = marcomManager.PlanningManager.GetDirectEntityMember(int.Parse(EntityID));
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Planning/GetDynamicfinancialattributes")]
        [HttpGet]
        public ServiceResponse GetDynamicfinancialattributes()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetDynamicfinancialattributes();

            }
            catch
            {
                result.Response = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/SavePredefinedValuesToAttribtueGroup")]
        [HttpPost]
        public ServiceResponse SavePredefinedValuesToAttribtueGroup([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                JArray jr = (JArray)jobj["SelectedIds"];
                IList<object> searchvalues = new List<object>();

                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["SelectedIds"].ToString());

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.SavePredefinedValuesToAttribtueGroup(Idarr, (int)jobj["GroupID"], (int)jobj["ParentId"], (int)jobj["Typeid"], (int)jobj["GroupID"], ((int)jobj["IsLock"] == 0 ? false : true), (String)jobj["Name"], (int)jobj["SortOrder"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        public ServiceResponse GetEntityOverallStatus()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntityOverallStatus();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        public ServiceResponse GetAllEntityStautsDtls()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetAllEntityStautsDtls();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        public ServiceResponse GetAllEntityEndDate()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetAllEntityEndDate();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/GetEntityTypeStatus/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetEntityTypeStatus(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntityTypeStatus((int)EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }


        public ServiceResponse InsertUpdateEntityTemplate([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.InsertUpdateEntityTemplate(jobj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/GetEntityTemplateDetails")]
        [HttpGet]
        public ServiceResponse GetEntityTemplateDetails()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetEntityTemplateDetails();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        public ServiceResponse UpdateEntityTypeOverallStatus([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.UpdateEntityTypeOverallStatus(jobj);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Planning/DeleteEntityTemplateListById/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteTaskTemplateListById(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.DeleteEntityTemplateListById(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        /// <summary>
        /// Retrieves an individual cookie from the cookies collection
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cookieName"></param>
        /// <returns></returns>
        public string GetCookie(HttpRequestMessage request, string cookieName)
        {
            CookieHeaderValue cookie = request.Headers.GetCookies(cookieName).FirstOrDefault();
            if (cookie != null)
                return cookie[cookieName].Value;

            return null;
        }

        [Route("api/Planning/GetObjectiveUnitsOptionValues/{unitTypeId}")]
        [HttpGet]
        public ServiceResponse GetObjectiveUnitsOptionValues(int unitTypeId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.GetObjectiveUnitsOptionValues(unitTypeId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Planning/copyuploadedImage")]
        [HttpPost]
        public ServiceResponse copyuploadedImage([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                string filename = (string)jobj["filename"];
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.copyuploadedImage(filename);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Planning/savemultipleUploadedImg")]
        [HttpPost]
        public ServiceResponse savemultipleUploadedImg([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.PlanningManager.savemultipleUploadedImg(jobj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
    }
}