﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;
using BrandSystems.Marcom.Core;
using Marcom.API.RequestHeaderDetails;
using Marcom.API.Services;

namespace Marcom.API.Authorization
{
    public class AuthenticateTokenAttribute : System.Web.Http.Filters.AuthorizationFilterAttribute
    {
        public static Guid sessionId;
        // Dictionary to keep control over all current sessions
        public static Dictionary<int, Guid> ImpersonateSession = new Dictionary<int, Guid>();

        // get the base directory
        public static string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "errorlog.txt";
        public static string ApiLogDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "APICall_ErrorLog.txt";
        // search the file below the current directory
        private static string retFilePath = baseDir;
        private static bool bReturnLog = false;
       

        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {

            try
            {
                RequestHeaders ReqHeaderObj; ReqHeaderObj = new RequestHeaders();
                WritingLog(actionContext, ReqHeaderObj);
                if (ReqHeaderObj.AuthorizationTokenPresent)
                {
                    ErrorLog.LogFilePath = retFilePath;
                    bReturnLog = ErrorLog.CustomErrorRoutine(false, "authenticate token started at " + DateTime.Now.ToString(), DateTime.Now);

                    //authenticate token
                    DecryptionAttribute decryptObj; decryptObj = new DecryptionAttribute();
                    string DecryptedAuthTokenTimeStamp = decryptObj.DecryptToken(ReqHeaderObj.AuthorizationToken);
                    string AuthToken = DecryptedAuthTokenTimeStamp.Substring(0, 36); //API key Guid
                    Int32 unixTimestamp = (Int32)(DateTime.UtcNow.AddMinutes(-2).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                    Int32 TokenTimeStamp = Int32.Parse(DecryptedAuthTokenTimeStamp.Substring(36, DecryptedAuthTokenTimeStamp.Length - 36)); //Time stamp
                    bool impersonateuser = false;
                    SessionGenerator GenerateSessionObj; GenerateSessionObj = new SessionGenerator();
                    int userID = MarcomManagerFactory.AuthenticateAPIToken(new Guid(AuthToken), 0); //Pass API TokenID -
                    if (userID == 0)
                    {


                        bReturnLog = ErrorLog.CustomErrorRoutine(false, "UnauthorizedAccessException", DateTime.Now);

                        throw new UnauthorizedAccessException("UnauthorizedAccessException"); 
                    
                    }
                    //impersonate userid
                    if (ReqHeaderObj.ImpersonateUserID != 0)
                    {
                        userID = ReqHeaderObj.ImpersonateUserID;
                        impersonateuser = true;
                    }

                    bReturnLog = ErrorLog.CustomErrorRoutine(false, "GenerateSessionID started for the user " + userID.ToString() + DateTime.Now.ToString(), DateTime.Now);

                    sessionId = GenerateSessionObj.GenerateSessionID(new Guid(AuthToken), userID, actionContext.Request.RequestUri.Host, impersonateuser); //Pass API TokenID and the user id
                    ImpersonateSession[userID] = sessionId;

                    bReturnLog = ErrorLog.CustomErrorRoutine(false, "GenerateSessionID done for the user " + userID.ToString() + DateTime.Now.ToString(), DateTime.Now);


                }
                else
                {
                    ErrorLog.LogFilePath = retFilePath;
                    bReturnLog = ErrorLog.CustomErrorRoutine(false, "Error due to UnauthorizedAccessException", DateTime.Now);

                    APILog.Logging.LogInfo("UnauthorizedAccessException");
                    throw new UnauthorizedAccessException("UnauthorizedAccessException");
                }
            }
            catch (Exception e)
            {

                ErrorLog.LogFilePath = retFilePath;
                bReturnLog = ErrorLog.ErrorRoutine(false, e);

                APILog.Logging.LogException(e, "UnauthorizedAccessException");
                throw new UnauthorizedAccessException("UnauthorizedAccessException"); ;
            }

        }


        public static void WritingLog(System.Web.Http.Controllers.HttpActionContext actionContext, RequestHeaders ReqHeaderObj)
        {
            var request = actionContext.Request;
            var Ip = "";
            try
            {
                if (request.Properties.ContainsKey("MS_HttpContext"))
                {
                    Ip = ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
                }

            ErrorLog.LogFilePath = ApiLogDir;
                bReturnLog = ErrorLog.CustomErrorRoutine(false, "***   Api Error Log Start **** " + DateTime.Now.ToString(), DateTime.Now);
                bReturnLog = ErrorLog.CustomErrorRoutine(false, "ReqHeaderObj --APIUserID -" + ReqHeaderObj.APIUserID.ToString() + "----AuthorizationToken-" + ReqHeaderObj.AuthorizationToken.ToString() + "----ImpersonateUserID-" + ReqHeaderObj.ImpersonateUserID.ToString(), DateTime.Now);
                bReturnLog = ErrorLog.CustomErrorRoutine(false, "IP " + Ip, DateTime.Now);
                bReturnLog = ErrorLog.CustomErrorRoutine(false, "....User ID-" + ReqHeaderObj.ImpersonateUserID.ToString(), DateTime.Now);
                bReturnLog = ErrorLog.CustomErrorRoutine(false, "Method Name " + actionContext.ActionDescriptor.ActionName.ToString(), DateTime.Now);
                bReturnLog = ErrorLog.CustomErrorRoutine(false, "Request data " + actionContext.Request.ToString(), DateTime.Now);
                bReturnLog = ErrorLog.CustomErrorRoutine(false, "Request time " + DateTime.Now.ToString(), DateTime.Now);
            // bReturnLog = ErrorLog.CustomErrorRoutine(false, " Responce time " + , DateTime.Now);

            }
            catch (Exception e) {
                bReturnLog = ErrorLog.CustomErrorRoutine(false, "***   Error block Api Error Log Start **** " + e.ToString(), DateTime.Now);
            }
        }
    }
}