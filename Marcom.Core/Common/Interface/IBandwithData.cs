﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    public interface IBandwithData
    {
        #region Public Properties
        int Id { get; set; }
        int Days { get; set; }
        double Cvalue { get; set; }

        #endregion
    }
}
