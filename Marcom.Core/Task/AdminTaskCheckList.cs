﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Task.Interface;

namespace BrandSystems.Marcom.Core.Task
{
   internal class AdminTaskCheckList : IAdminTaskCheckList
    {
        #region Member Variables

        protected int _id;
        protected int _taskid;
        protected string _name;
        protected int _sortorder;
        #endregion

        #region Constructors
        public AdminTaskCheckList() { }

        public AdminTaskCheckList(int pTaskId, string pName,int pSortorder)
        {
            this._taskid = pTaskId;
            this._name = pName;
            this._sortorder = pSortorder;
          
        }

        public AdminTaskCheckList(int pId)
        {
            this._id = pId;
        }

        #endregion


        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }



        public virtual string NAME
        {
            get { return _name; }
            set
            {
                _name = value;
            }

        }


        public virtual int TaskId
        {
            get { return _taskid; }
            set { _taskid = value; }

        }



        public virtual int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }

        }

       
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            AdminTask castObj = null;
            try
            {
                castObj = (AdminTask)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }
}
