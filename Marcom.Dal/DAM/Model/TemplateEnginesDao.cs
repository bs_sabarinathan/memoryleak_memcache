﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{

    public partial class TemplateEnginesDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _engineType;
        protected string _description;
        protected string _serviceUrl;
        protected string _resourceUrl;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public TemplateEnginesDao() { }

        public TemplateEnginesDao(int pID, int pEngineType, string pDescription, string pServerURL, string pResourceURL)
        {
            this._id = pID;
            this._engineType = pEngineType;
            this._description = pDescription;
            this._serviceUrl = pServerURL;
            this._resourceUrl = pResourceURL;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int EngineType
        {
            get { return _engineType; }
            set { _bIsChanged |= (_engineType != value); _engineType = value; }

        }

        public virtual string Description
        {
            get { return _description; }
            set { _bIsChanged |= (_description != value); _description = value; }

        }

        public virtual string ServerURL
        {
            get { return _serviceUrl; }
            set { _bIsChanged |= (_serviceUrl != value); _serviceUrl = value; }

        }

        public virtual string ResourceURL
        {
            get { return _resourceUrl; }
            set { _resourceUrl = value; }
        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string ID = "ID";
            public const string EngineType = "EngineType";
            public const string Description = "Description";
            public const string ServerURL = "ServerURL";
            public const string ResourceURL = "ResourceURL";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string ID = "ID";
            public const string EngineType = "EngineType";
            public const string Description = "Description";
            public const string ServerURL = "ServerURL";
            public const string ResourceURL = "ResourceURL";
        }


        #endregion


    }

}
