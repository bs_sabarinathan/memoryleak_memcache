﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public partial class PopularTagWordsDao : BaseDao, ICloneable 
    {
          #region Member Variables

		protected int _id;
		protected int _attributeid;
        protected string _usedon;
        protected int _totalhits;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public PopularTagWordsDao() {}

        public PopularTagWordsDao(int pId,  int pAttributeid, string pUsedon,int pTotalHits)
        {
            this._id = pId;
            this._attributeid = pAttributeid;
            this._usedon = pUsedon;
            this._totalhits = pTotalHits;
        }

        public PopularTagWordsDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int AttributeID
		{
			get { return _attributeid; }
			set { _bIsChanged |= (_attributeid != value); _attributeid = value; }
			
		}

          public virtual string UsedOn
        {
            get { return _usedon; }
            set
            {
                if (value != null && value.Length > 10)
                    throw new ArgumentOutOfRangeException("UsedOn", "UsedOn value, cannot contain more than 10 characters");
                _bIsChanged |= (_usedon != value);
                _usedon = value;
            }

        }

        public virtual int TotalHits
        {
            get { return _totalhits; }
            set { _bIsChanged |= (_totalhits != value); _totalhits = value; }
        }

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string PopularTagWordsDao = "PopularTagWordsDao";		
	    	public const string Id = "Id";			
            public const string AttributeID = "AttributeID";
            public const string UsedOn = "UsedOn";
            public const string TotalHits = "TotalHits";
					
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string PopularTagWordsDao = "PopularTagWordsDao";		
            public const string Id = "Id";
            public const string AttributeID = "AttributeID";
            public const string UsedOn = "UsedOn";
            public const string TotalHits = "TotalHits";
		}

		#endregion
    }
}
