﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface IAttributeToAttributeRelations
    {
        int ID
        {
            get;
            set;
        }

        int EntityTypeID
        {
            get;
            set;
        }

        int AttributeTypeID
        {
            get;
            set;
        }

        int AttributeID
        {
            get;
            set;
        }

        int AttributeOptionID
        {
            get;
            set;
        }

        int AttributeLevel
        {
            get;
            set;
        }

        string AttributeRelationID
        {
            get;
            set;
        }
    }

}
