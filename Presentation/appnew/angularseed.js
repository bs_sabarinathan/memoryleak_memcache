﻿'use strict';
angular.module("app", ['ui.select2', 'ngCookies', 'ngResource', '$strap.directives', 'ngSanitize', 'ngGrid', 'ngStorage', 'ui.router', 'ui.bootstrap', 'ui.utils', 'ui.load', 'ui.jq', 'oc.lazyLoad', 'pascalprecht.translate', 'chieffancypants.loadingBar', 'ngAnimate', 'angularUtils.directives.dirPagination', 'ngTagsInput']);
var app = angular.module('app');
app.config(['$compileProvider', function ($compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|mailto|devexpress.reportserver):/);
}]);
