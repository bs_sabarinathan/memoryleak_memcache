﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.ImportXml
{

    /// <summary>
    /// Option object for table 'MM_EntityTaskType'.
    /// </summary>

    internal class EntitytasktypeImport
    {
        #region Member Variables

        protected int _id;
        protected int _newid;
        protected int _entitytypeid;
        protected int _tasktypeid;


        #endregion

        #region Constructors
        public EntitytasktypeImport() { }

        public EntitytasktypeImport(int pId, int pEntitytypeid, int pTasktypeid)
        {
            this._id = pId;
            this._entitytypeid = pEntitytypeid;
            this._tasktypeid = pTasktypeid;
        }

        public EntitytasktypeImport(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int NewId
        {
            get { return _newid; }
            set { _newid = value; }

        }

        public virtual int EntitytypeId
        {
            get { return _entitytypeid; }
            set { _entitytypeid = value; }

        }
        public virtual int TaskTypeId
        {
            get { return _tasktypeid; }
            set { _tasktypeid = value; }

        }




        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntitytasktypeImport castObj = null;
            try
            {
                castObj = (EntitytasktypeImport)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
