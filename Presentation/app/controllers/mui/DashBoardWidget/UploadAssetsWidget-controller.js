﻿(function(ng, app) {
	"use strict";

	function muiDashBoardWidgetuploadAssetWidgetCtrl($scope, $timeout, $http, $compile, $resource, $cookies,$location, $window, UploadedassetswidgetService) {
		$scope.browserchartMetrics = {
			"Count": "1"
		};
		$scope.GlobalChartType = {
			1: "column",
			2: "pie"
		};
		$scope.userimg = parseInt($cookies['UserId']);
		$timeout(function () { drawdetail(); }, 100);
		$scope.UploadedAssetWidgetContentID = $scope.WizardID;
		$scope.NoOfYearnew = $scope.WizardNoOfYear;
		$scope.NoOfMonthnew = $scope.WizardNoOfMonth;

		function drawdetail() {
			$scope.browserchartpoints = [];
			var visualtype = 1;
			var Monthdataupdate = [];
			Monthdataupdate.push({
				"ID": 0,
				"Value": 'All'
			});
			Monthdataupdate.push({
				"ID": 1,
				"Value": 'January'
			});
			Monthdataupdate.push({
				"ID": 2,
				"Value": 'February'
			});
			Monthdataupdate.push({
				"ID": 3,
				"Value": 'March'
			});
			Monthdataupdate.push({
				"ID": 4,
				"Value": 'April'
			});
			Monthdataupdate.push({
				"ID": 5,
				"Value": 'May'
			});
			Monthdataupdate.push({
				"ID": 6,
				"Value": 'June'
			});
			Monthdataupdate.push({
				"ID": 7,
				"Value": 'July'
			});
			Monthdataupdate.push({
				"ID": 8,
				"Value": 'August'
			});
			Monthdataupdate.push({
				"ID": 9,
				"Value": 'September'
			});
			Monthdataupdate.push({
				"ID": 10,
				"Value": 'October'
			});
			Monthdataupdate.push({
				"ID": 11,
				"Value": 'November'
			});
			Monthdataupdate.push({
				"ID": 12,
				"Value": 'December'
			});
			var Browservaluetitle;
			var Browservalueyear = $scope.WizardNoOfYear == undefined ? (new Date()).getFullYear() : $scope.WizardNoOfYear;
			var Browservaluemonth = $scope.WizardNoOfMonth == undefined ? (new Date()).getMonth() + 1 : $scope.WizardNoOfMonth;
			var Browservaluenamemonth;
			if (Browservaluemonth > 0) {
				Browservaluenamemonth = Monthdataupdate[Browservaluemonth].Value;
				Browservaluetitle = 'Upload Asset Summary for ' + Browservaluenamemonth + ' ' + Browservalueyear
			} else {
				Browservaluetitle = 'Upload Asset Summary for ' + Browservalueyear
			}
			var EntityCreationvaluemonth = $scope.WizardNoOfMonth;
			UploadedassetswidgetService.GetAssetCreateationStatistic($scope.NoOfYearnew, $scope.NoOfMonthnew).then(function(GetEntityCreationlistvalue) {
				var EntityCreationResult = GetEntityCreationlistvalue.Response;
				var data = [];
				for (var i = 0; i < GetEntityCreationlistvalue.Response.length; i++) {
					if ($scope.GlobalChartType[1] == "column") {
						if (EntityCreationvaluemonth == 0) {
							var mothname = Monthdataupdate[EntityCreationResult[i].Days].Value;
							data.push({
								y: EntityCreationResult[i].value,
								label: mothname,
								name: Monthdataupdate[EntityCreationResult[i].Days].Value
							});
						} else data.push({
							y: EntityCreationResult[i].value,
							label: EntityCreationResult[i].Days,
							name: EntityCreationResult[i].Days
						});
					}
				}
				$('#' + $scope.UploadedAssetWidgetContentID + '').attr('data-visualType', $scope.GlobalChartType[1]);
				$('#' + $scope.UploadedAssetWidgetContentID + '').attr('data-value', JSON.stringify(data));
				$('#' + $scope.UploadedAssetWidgetContentID + '').attr('data-charttitle', Browservaluetitle);
				RenderBarChart($scope.WizardID);
				$('#' + $scope.UploadedAssetWidgetContentID + '').parents('.box-content').find('.InProgress').remove();
			});
		};

		function RenderBarChart(WizardID) {
			$('#' + WizardID + '').height($('#' + WizardID + '').parents('.box-content').height() - 20);
			var charttitle = $('#' + WizardID + '').attr('data-charttitle');
			var data = JSON.parse($('#' + WizardID + '').attr('data-value'));
			CanvasJS.addColorSet("finColorSet1", ["#BDF2A1", "#9ECF6E", "#DDDD69", "#4DA3CE", "#B08BEB", "#3EA0DD", "#F5A52A", "#23BFAA", "#FAA586", "#EB8CC6"]);
			if ($('#' + WizardID + '').attr('data-visualType') == "column") {
				var chart = new CanvasJS.Chart(WizardID, {
					title: {
						text: charttitle,
						verticalAlign: "top",
						horizontalAlign: "center",
						fontSize: 15,
						fontFamily: "Calibri",
						fontWeight: "normal",
						fontColor: "black",
						fontStyle: "normal",
						borderThickness: 0,
						borderColor: "black",
						cornerRadius: 0,
						backgroundColor: null,
						margin: 0
					},
					colorSet: "finColorSet1",
					height: $('#' + WizardID + '').parents('.box-content').height() - 10,
					axisX: {
						tickColor: "white",
						tickLength: 5,
						labelFontFamily: "sans-serif",
						labelFontColor: "#7F7F7F",
						labelFontSize: 10,
						labelFontWeight: "normal"
					},
					axisY: {
						tickLength: 0,
						labelFontSize: 0,
						lineThickness: 0,
						gridThickness: 0,
						includeZero: true,
					},
					data: [{
						type: "column",
						indexLabel: "{y}",
						indexLabelFontSize: 12,
						indexLabelFontWeight: "normal",
						indexLabelPlacement: "outside",
						indexLabelOrientation: "horizontal",
						indexLabelFontFamily: "sans-serif",
						indexLabelFontColor: "darkgrey",
						legendMarkerColor: "gray",
						showInLegend: false,
						toolTipContent: "{name}:<strong>{y}</strong>",
						dataPoints: data
					}]
				});
			} else {
				var chart = new CanvasJS.Chart(WizardID, {
					title: {
						text: charttitle,
						verticalAlign: "top",
						horizontalAlign: "center",
						fontSize: 15,
						fontFamily: "Calibri",
						fontWeight: "normal",
						fontColor: "black",
						fontStyle: "normal",
						borderThickness: 0,
						borderColor: "black",
						cornerRadius: 0,
						backgroundColor: null,
						margin: 0
					},
					animationEnabled: true,
					height: $('#' + WizardID + '').parents('.box-content').height() - 10,
					legend: {
						verticalAlign: "center",
						horizontalAlign: "right",
						fontSize: 12,
						fontweight: "normal",
						padding: 0,
						fontColor: "darkgrey",
						fontFamily: "sans-serif"
					},
					data: [{
						indexLabelFontSize: 12,
						indexLabelLineThickness: 0,
						indexLabelFontStyle: "italic",
						indexLabelFontWeight: "normal",
						indexLabelFontFamily: "sans-serif",
						indexLabelFontColor: "darkgrey",
						indexLabelLineColor: "darkgrey",
						indexLabelPlacement: "outside",
						type: "pie",
						showInLegend: true,
						toolTipContent: "{name}:<strong>{y}</strong>",
						dataPoints: data
					}]
				});
			}
			chart.render();
		}
		$(document).on("OnChartLoad", function(event, setChartID) {
			if (setChartID != undefined) {
				$('#' + setChartID + '').html('');
				RenderBarChart(setChartID);
			}
		});
		$scope.$on("$destroy", function() {
			RecursiveUnbindAndRemove($("[ng-controller='mui.DashBoardWidget.uploadedAssetsWidgetCtrl']"));
		});
	}
	app.controller("mui.DashBoardWidget.uploadedAssetsWidgetCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$location', '$window', 'UploadedassetswidgetService', muiDashBoardWidgetuploadAssetWidgetCtrl]);
	function UploadedassetswidgetService($http, $q) {
	    $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
	        GetAssetCreateationStatistic: GetAssetCreateationStatistic
	    });
	    function GetAssetCreateationStatistic(year, month) { var request = $http({ method: "get", url: "api/common/GetAssetCreateationStatistic/" + year + "/" + month, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
	    function handleError(response) {
	        if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
	        return ($q.reject(response.data.message));
	    }
	    function handleSuccess(response) { return (response.data); }
	}
	app.service("UploadedassetswidgetService", ['$http', '$q', UploadedassetswidgetService]);
})(angular, app);