﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Access.Interface
{
    /// <summary>
    /// IEntityRoleUser interface for table 'AM_Entity_Role_User'.
    /// </summary>
    public interface ITaskMember
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int TaskID
        {
            get;
            set;

        }

        int Roleid
        {
            get;
            set;

        }

        int ApprovalRount
        {
            get;
            set;

        }

        Boolean ApprovalStatus
        {
            get;
            set;

        }

        int Userid
        {
            get;
            set;

        }

       
        string UserName
        {
            get;
            set;

        }

        string DepartmentName
        {
            get;
            set;

        }

        string Role
        {
            get;
            set;

        }

        string Title
        {
            get;
            set;

        }

        string UserEmail
        {
            get;
            set;

        }




        #endregion
    }
}
