﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Access.Model
{
    public partial class PublishAccessDao :BaseDao, ICloneable   
    {
        #region Member Variables

        protected int _id;
        protected int _role;      
        protected bool _accesspermission;
       
        #endregion

         #region Constructors
        public PublishAccessDao() { }

        public PublishAccessDao(int pID, int pRole)
        {
          this._id = pID;
            this._role = pRole; 
        }

        public PublishAccessDao(bool pAccesspermission)
		{
            this._accesspermission = pAccesspermission; 
		}

        #endregion

          #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int Role
        {
            get { return _role; }
            set { _role = value; }

        }

        public virtual bool AccessPermission
        {
            get { return _accesspermission; }
            set { _accesspermission = value; }

        }
         
        #endregion
        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string ID = "ID";
            public const string Role = "Role";
            public const string Accesspermission = "Accesspermission";
         

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string ID = "ID";

            public const string Role = "Role";
        
            public const string Accesspermission = "Accesspermission";
           
        }

        #endregion
    }
}
