﻿(function (ng, app) {
    "use strict"; function muiadmincmscustomstylesettingsCtrl($scope, AdminService, $timeout, $translate) {
        $scope.ListOfCustomStyle = []; $scope.ClassName = ""; $scope.CssCode = "font-size: 20px; \n" + "font-weight: bold; \n" + "font-family: arial; \n" + "color: #8AC007;'"; $scope.Id = 0; $scope.GetListOfCmsCustomStyle = function () {
            AdminService.GetListOfCmsCustomStyle().then(function (ResCustomStyle) {
                if (ResCustomStyle.Response != null)
                    $scope.ListOfCustomStyle = ResCustomStyle.Response;
            });
        }; $scope.GetListOfCmsCustomStyle(); $scope.InsertUpdateCmsCustomStyle = function () {
            var customfonts = { Id: $scope.Id, ClassName: $scope.ClassName, CssCode: $scope.CssCode }; AdminService.InsertUpdateCmsCustomStyle(customfonts).then(function (Res) {
                if (Res.Response == true) { $scope.GetListOfCmsCustomStyle(); NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption')); }
                else
                    NotifyError($translate.instant('LanguageContents.Res_1144.Caption'));
            }); $("#customStylesEditPopup").modal('hide');
        }
        $scope.EditCustomStyle = function (obj) { $scope.Clear(); $scope.ClassName = obj.ClassName; $scope.CssCode = obj.CssCode; $scope.Id = obj.Id; }
        $scope.DeleteCustomStyle = function (Id) {
            AdminService.DeleteCmsCustomStyle(Id).then(function (resDel) {
                if (resDel.Response == true) { $scope.ListOfCustomStyle = $.grep($scope.ListOfCustomStyle, function (e) { return e.Id != Id }); NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption')); }
                else
                    NotifyError($translate.instant('LanguageContents.Res_1144.Caption'));
            });
        }
        $scope.Clear = function () { $scope.ClassName = ""; $scope.CssCode = "font-size: 20px; \n" + "font-weight: bold; \n" + "font-family: arial; \n" + "color: #8AC007;"; $scope.Id = 0; }
        $scope.$on("$destroy", function () { RecursiveUnbindAndRemove($("[ng-controller='mui.admin.cmscustomstylesettingsCtrl']")); });
    }
    app.controller('mui.admin.cmscustomstylesettingsCtrl', ['$scope', 'AdminService', '$timeout', '$translate', muiadmincmscustomstylesettingsCtrl]);
})(angular, app);