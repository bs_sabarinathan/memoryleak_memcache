using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Access.Model
{

	/// <summary>
	/// GlobalAclDao object for table 'AM_GlobalAclDao'.
	/// </summary>
	public partial class EntityTypeRoleAclDao : BaseDao, ICloneable 
	{
		#region Member Variables

        protected int _id;
		protected int _entityroleid;
		protected int _moduleid;
		protected int _entitytypeid;
		protected int _sortorder;
        protected string _caption;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion

       
		
		#region Constructors
		public EntityTypeRoleAclDao() {}

        public EntityTypeRoleAclDao(int pId, string _pCaption, int pEntityroleid, int pModuleid, int pEntityTypeid, int pSortOrder)
		{
            this._id = pId;
            this._entityroleid = pEntityroleid; 
			this._moduleid = pModuleid; 
			this._entitytypeid = pEntityTypeid; 
			this._sortorder = pSortOrder;
            this._caption = _pCaption;
		}
		
		#endregion
		
		#region Public Properties        
		
        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual string Caption
        {
            get { return _caption; }
            set { _bIsChanged |= (_caption != value); _caption = value; }

        }

        public virtual int EntityRoleID
		{
			get { return _entityroleid; }
			set { _bIsChanged |= (_entityroleid != value); _entityroleid = value; }
			
		}

        public virtual int ModuleID
		{
			get { return _moduleid; }
			set { _bIsChanged |= (_moduleid != value); _moduleid = value; }
			
		}

        public virtual int EntityTypeID
		{
			get { return _entitytypeid; }
			set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }
			
		}

        public virtual int Sortorder
		{
			get { return _sortorder; }
			set { _bIsChanged |= (_sortorder != value); _sortorder = value; }
			
		}
		

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string Id = "ID";
            public const string EntityTypeRoleAclDao = "EntityTypeRoleAclDao";
            public const string EntityRoleID = "EntityRoleID";
            public const string ModuleID = "ModuleID";
            public const string EntityTypeID = "EntityTypeID";
            public const string Sortorder = "Sortorder";
            public const string Caption = "Caption";	
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string Id = "ID";
            public const string EntityTypeRoleAclDao = "EntityTypeRoleAclDao";
            public const string EntityRoleID = "EntityRoleID";
            public const string ModuleID = "ModuleID";
            public const string EntityTypeID = "EntityTypeID";
            public const string Sortorder = "Sortorder";
            public const string Caption = "Caption";			
		}

		#endregion
		
		
	}
	
}
