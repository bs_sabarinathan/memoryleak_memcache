﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.User.Model
{
    public partial class ApprovalRoleUserDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _ID;
        protected int _ApprovalRoleId;
        protected int _UserId;
        
        #endregion

        #region Constructors

        public ApprovalRoleUserDao() { }

        public ApprovalRoleUserDao(int pId, int pApprovalRoleId, int pUserId)
        {
            this._ID = pId;
            this._UserId = pUserId;
            this._ApprovalRoleId = pApprovalRoleId;

        }

        public ApprovalRoleUserDao(int pId)
        {
            this._ID = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public virtual int ApprovalRoleId
        {
            get { return _ApprovalRoleId; }
            set { _ApprovalRoleId = value; }
        }

        public virtual int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string ApprovalRoleUserDao = "ApprovalRoleUserDao";
            public const string ID = "ID";
            public const string ApprovalRoleId = "ApprovalRoleId";
            public const string UserId = "UserId";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string ApprovalRoleUserDao = "ApprovalRoleUserDao";
            public const string ID = "ID";
            public const string ApprovalRoleId = "ApprovalRoleId";
            public const string UserId = "UserId";
        }

        #endregion
    }
}
