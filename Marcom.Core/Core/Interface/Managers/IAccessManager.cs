﻿using System;    
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Access.Interface;
using Newtonsoft.Json.Linq;
using BrandSystems.Marcom.Dal.Access.Model;
using BrandSystems.Marcom.Dal.User.Model;

namespace BrandSystems.Marcom.Core.Interface.Managers
{
    public interface IAccessManager
    {

        #region Instance of Classes In ServiceLayer reference
        /// <summary>
        /// Returns EntityRolesUser class.
        /// </summary>
        ITaskMember TaskMemberservice();

        #endregion

        /// <summary>
        /// Creates the entity user role.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <param name="Roleid">The roleid.</param>
        /// <param name="Userid">The userid.</param>
        /// <param name="IsInherited">if set to <c>true</c> [is inherited].</param>
        /// <param name="InheritedFromEntityid">The inherited from entityid.</param>
        /// <returns>IEntityRoleUser</returns>
        int CreateEntityUserRole(int entityId, int Roleid, int Userid, bool IsInherited, int InheritedFromEntityid);

        /// <summary>
        /// Deletes the entity user role.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <param name="Roleid">The roleid.</param>
        /// <param name="Userid">The userid.</param>
        /// <returns>IEntityRoleUser</returns>
        bool DeleteEntityUserRole(int id);


        /// <summary>
        ///GetRole.
        /// </summary>
        /// <returns>Ilist of IRole</returns>
        IList<IRole> GetRole();

        /// <summary>
        /// Saves/update role.
        /// </summary>
        /// <param name="Caption">The caption.</param>
        /// <param name="Description">The description.</param>
        /// <param name="Id">The id.</param>
        /// <returns>String</returns>
        int SaveUpdateRole(String Caption, String Description, int Id = 0);

        /// <summary>
        /// Deletes the role.
        /// </summary>
        /// <param name="Id">The id.</param>
        /// <returns>String</returns>
        bool DeleteRole(int Id);

        /// <summary>
        /// Saves/update entity ACL.
        /// </summary>
        /// <param name="RoleId">The role id.</param>
        /// <param name="ModuleId">The module id.</param>
        /// <param name="EntityTypeId">The entity type id.</param>
        /// <param name="FeatureId">The feature id.</param>
        /// <param name="AccessRights">The access rights.</param>
        /// <returns>int</returns>
        int SaveUpdateEntityACL(int RoleId, int ModuleId, int EntityTypeId, int FeatureId, string AccessRights);

        /// <summary>
        /// get the global access.
        /// </summary>
        /// <param name="caption">The GlobalRoleid.</param>
        /// <param name="description">The Moduleid.</param>
        /// <param name="roleid">The EntityTypeid.</param>
        /// /// <param name="description">The Featureid.</param>
        /// <param name="roleid">The AccessPermission.</param>
        /// <returns>Ilist of IGlobalAcl</returns>
        IList<object> GetGlobalAcl(int roleID);

        /// <summary>
        /// Saves/update global ACL.
        /// </summary>
        /// <param name="GlobalRoleId">The global role id.</param>
        /// <param name="ModuleId">The module id.</param>
        /// <param name="EntityTypeId">The entity type id.</param>
        /// <param name="FeatureId">The feature id.</param>
        /// <param name="AccessRights">The access rights.</param>
        /// <returns>int</returns>
        bool SaveUpdateGlobalACL(int GlobalRoleId, int ModuleId, int EntityTypeId, int FeatureId, string AccessRights, int ID);

        /// <summary>
        /// Delete global ACL.
        /// </summary>
        /// <param name="ID">ID.</param>
        /// <returns>int</returns>
        bool DeleteGlobalACL(int ID);

        //GetGlobalRole
        /// <summary>
        /// get the global role.
        /// </summary>
        /// <param name="caption">The caption.</param>
        /// <param name="description">The description.</param>
        /// <param name="roleid">The roleid.</param>
        /// <returns>Ilist of GlobalRole</returns>
        IList<IGlobalRole> GetGlobalRole();

        //GetGlobalRole by id
        /// <summary>
        /// get the global role by id
        /// </summary>
        /// <param name="caption">The caption.</param>
        /// <param name="description">The description.</param>
        /// <param name="roleid">The roleid.</param>
        /// <returns>Ilist of GlobalRole</returns>
        IList<IGlobalRole> GetGlobalRoleByID(int ID);
        //Insert/Update global role
        /// <summary>
        /// Insert/Updates the global role.
        /// </summary>
        /// <param name="caption">The caption.</param>
        /// <param name="description">The description.</param>
        /// <param name="roleid">The roleid.</param>
        /// <returns>int</returns>
        bool InsertUpdateGlobalRole(string caption, string description, int id = 0);

        /// <summary>
        /// Deletes the global role.
        /// </summary>
        /// <param name="roleid">The roleid.</param>
        /// <returns>int</returns>
        int DeleteGlobalRole(int roleid);

        /// <summary>
        /// Inserts the global role user.
        /// </summary>
        /// <param name="globalroleid">The globalroleid.</param>
        /// <param name="userid">The userid.</param>
        /// <returns>string</returns>
        int InsertGlobalRoleUser(int globalroleid, int userid);

        /// <summary>
        /// Checks the access.
        /// </summary>
        /// <param name="moduleId">The module id.</param>
        /// <param name="EntitytypeID">The entitytype ID.</param>
        /// <param name="featureId">The feature id.</param>
        /// <param name="AccessPermissionId">The access permission id.</param>
        /// <returns>bool</returns>
        bool CheckAccess(Modules moduleId, int EntitytypeID, FeatureID featureId, OperationId AccessPermissionId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="moduleId"></param>
        /// <param name="featureId"></param>
        /// <param name="AccessPermissionId"></param>
        /// <returns></returns>
        bool CheckUserAccess(int moduleId, int featureId, int AccessPermissionId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="moduleId"></param>
        /// <param name="featureId"></param>
        /// <returns></returns>
        bool CheckUserAccess(int moduleId, int featureId);

        /// <summary>
        /// Tries the access.
        /// </summary>
        /// <param name="moduleId">The module id.</param>
        /// <param name="EntitytypeID">The entitytype ID.</param>
        /// <param name="featureId">The feature id.</param>
        /// <param name="AccessPermissionId">The access permission id.</param>
        void TryAccess(Modules moduleId, int EntitytypeID, FeatureID featureId, OperationId AccessPermissionId);

        void TryAccess(Modules moduleId, FeatureID featureId, OperationId AccessPermissionId);

        void TryAccess(Modules moduleId, FeatureID featureId);
        /// <summary>
        /// Checks the access.
        /// </summary>
        /// <param name="moduleId">The module id.</param>
        /// <param name="EntitytypeID">The entitytype ID.</param>
        /// <param name="featureId">The feature id.</param>
        /// <param name="AccessPermissionId">The access permission id.</param>
        /// <param name="EntityId">The entity id.</param>
        /// <returns>bool</returns>
        bool CheckAccess(Modules moduleId, int EntitytypeID, FeatureID featureId, OperationId AccessPermissionId, int EntityId);

        /// <summary>
        /// Tries the access.
        /// </summary>
        /// <param name="moduleId">The module id.</param>
        /// <param name="EntitytypeID">The entitytype ID.</param>
        /// <param name="featureId">The feature id.</param>
        /// <param name="AccessPermissionId">The access permission id.</param>
        /// <param name="EntityId">The entity id.</param>
        void TryAccess(Modules moduleId, int EntitytypeID, FeatureID featureId, OperationId AccessPermissionId, int EntityId);

        /// <summary>
        /// Delete the global role user.
        /// </summary>
        /// <param name="userid">The userid.</param>
        /// <returns>bool</returns>
        bool DeleteGlobalRoleUser(int userid);

        /// <summary>
        /// Get the global role user By UserID.
        /// </summary>
        /// <param name="userid">The userid.</param>
        /// <returns>int[] </returns>
        int[] GetGlobalRoleUserByID(int userid);


        bool CheckUserIsAdmin();

        bool CheckGlobalAccessAvaiilability(int GlobalRoleID, int ModuleID, int FeatureID);

        bool DeleteOldGlobalAccess(int GlobalRoleID, int ModuleID, int FeatureID);
        void GetApplicationLevelSettings();

        bool InsertUpdateGlobalEntitTypeACL(JArray entitytypeAccessObj);

        IList<object> GetGlobalEntityTypeAcl(int roleID, int moduleID);

        bool DeleteGlobalEntityTypeACL(int ID);

        IList<IRole> GetAllEntityRole();

        IList<IEntityTypeRoleAcl> GetEntityTypeRoleAccess(int EntityTypeID);

        /// <summary>
        /// CHECK THE ENTITY TYPE ACCESS
        /// </summary>
        /// <param name="moduleID"></param>
        /// <param name="EntityTypeID"></param>
        void TryEntityTypeAccess(Modules moduleID, int EntityTypeID);

        /// <summary>
        /// ENTITY TYPE ACCESS
        /// </summary>
        /// <param name="moduleId"></param>
        /// <param name="EntitytypeID"></param>
        /// <returns></returns>
        bool CheckEntityTypeAccessForRootEntityType(Modules moduleId, int EntitytypeID);

        /// <summary>
        /// ENTITY TYPE AND ENTITY ACCESS
        /// </summary>
        /// <param name="moduleID"></param>
        /// <param name="parententityID"></param>
        /// <param name="EntityTypeID"></param>
        void TryEntityTypeAccess(Modules moduleID, int parententityID, int EntityTypeID);

        bool CheckEntityTypeAndEntityRoleAccess(Modules moduleId, int parententityID, int EntitytypeID);

        /// <summary>
        /// ENTITY ACCESSs
        /// </summary>
        /// <param name="currententityID"></param>
        /// <param name="moduleID"></param>
        void TryEntityTypeAccess(int currententityID, Modules moduleID);

        bool CheckEntityRoleAccess(Modules moduleId, int currententityID);
       
        int InsertAssetAccess(int assetaccessID, int userid);

        int[] GetAssetAccessByID(int userid);

        bool DeleteAssetAccessByID(int userid);

        IList<ISuperAdminDetails> GetSuperAdminModule(int Id);

        bool SaveUpdateSuperAdminRoleFeatures(int GlobalRoleID, int MenuID, int FeatureID, bool IsChecked, int GlobalAclId);

        int DeleteGlobalByID(int roleid, int Menu);

        IList<ISuperGlobalAcl> GetSuperglobalacl();

        Dictionary<string, bool> getUserGlobalRoleList(IMarcomManager marcommanager);

        IList GetGlobalRoleUserByRoleID(int taskID);

        bool SaveUpdateNotificationRoleFeatures(int GlobalRoleID, JObject Notificationdata);

        bool SaveUpdateNewsfeedRoleFeatures(int GlobalRoleID,JObject Newsfeedtempdata);

        bool DeleteByGlobalRoleID(int GlobalRoleID);

        IList<GlobalRoleAccess_NewsFeedDao> GetSelectedFeedFilter(int GlobalRoleID);

        IList<GlobalRoleAccess_NotificationDao> GetSelectedNotificationFilter(int GlobalRoleID);

        bool DeleteByGlobalRoleID_Notification(int GlobalRoleID);

        bool Updatepublishaccess(int Role, bool AccessPermission);

        IList Getpublishaccess();

        bool userpublishaccess(int entityid);
    }
}
