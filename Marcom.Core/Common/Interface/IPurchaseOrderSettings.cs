﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{

    public interface IPurchaseOrderSettings
    {
        DateTime CreateDate { get; set; }
        string Prefix { get; set; }
        string DateFormat { get; set; }
        int NoOffDigits { get; set; }
        string DigitFormat { get; set; }
        int ID { get; set; }
    }
}
