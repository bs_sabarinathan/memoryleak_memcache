﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    public interface IWidgetTemplateRoles
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int WidgetTemplateID
        {
            get;
            set;

        }

        int RoleID
        {
            get;
            set;

        }




        #endregion
    }
}
