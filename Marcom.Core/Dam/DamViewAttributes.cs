﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.DAM.Interface;

namespace BrandSystems.Marcom.DAM
{
    public class DamViewAttributes : IDamViewAttribtues
    {
        public int ID { get; set; }
        public string DisplayName { get; set; }
        public int SortOrder
        {
            get;
            set;
        }

        public int Level
        {
            get;
            set;
        }

        public int Type
        {
            get;
            set;
        }

        public bool IsColumn
        {
            get;
            set;
        }

        public bool IsToolTip
        {
            get;
            set;
        }
    }
}
