﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.User.Model
{
   public partial class ApprovalRoleDao: BaseDao, ICloneable
    {
       #region "Member variables"

        protected int _id;
        protected string _caption;
        protected string _description;

        #endregion

        #region "Constructor"

        public ApprovalRoleDao() { }


        public ApprovalRoleDao(int pid, string pCaption, string pDescription) 
        {
            this._id = pid;
            this._caption = pCaption;
            this._description = pDescription;
        }

        #endregion

        #region "Public propertiers"

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }       


        public virtual string Caption
        {
            get { return _caption; }
            set { _caption = value; }

        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string ApprovalRoleDao = "ApprovalRoleDao";
            public const string ID = "ID";
            public const string Caption = "Caption";
            public const string Description = "Description";
            
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string ApprovalRoleDao = "ApprovalRoleDao";
            public const string ID = "ID";
            public const string Caption = "Caption";
            public const string Description = "Description";
            
        }

        #endregion
    }
}
