using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// Feed object for table 'CM_Feed'.
	/// </summary>

    internal class Feed :  IFeed 
	{
		#region Member Variables

		protected int _id;
		protected int _actor;
		protected int _templateid;
		protected DateTimeOffset _happenedon;
		protected DateTimeOffset _commentedupdatedon;
		protected int? _entityid;
		protected string _typename;
		protected string _attributename;
		protected string _fromvalue;
		protected string _tovalue;
		protected IList<IFeedComment> _feedcomment;
        protected int _userid;
        protected int? _associatedentityid;
        protected string _attributegrouprecordname;
        protected int?_version;
        protected int? _phaseID;
        protected string _stepID;
		#endregion
		
		#region Constructors
		public Feed() {}

        public Feed(int pActor, int pTemplateid, DateTimeOffset pHappenedOn, DateTimeOffset pCommentedUpdatedOn, int? pEntityid, string pTypeName, string pAttributeName, string pFromValue, string pToValue, int pUserID, int? pAssocitedEntityID, string pAttributeGroupRecordName, int pversion, int? pphaseID, string pstepID)
		{
			this._actor = pActor; 
			this._templateid = pTemplateid; 
			this._happenedon = pHappenedOn; 
			this._commentedupdatedon = pCommentedUpdatedOn; 
			this._entityid = pEntityid; 
			this._typename = pTypeName; 
			this._attributename = pAttributeName; 
			this._fromvalue = pFromValue; 
			this._tovalue = pToValue;
            this._userid = pUserID;
            this._associatedentityid = pAssocitedEntityID;
            this._attributegrouprecordname = pAttributeGroupRecordName;
            this._version = pversion;
            this._phaseID = pphaseID;
            this._stepID = pstepID;

		}
				
		public Feed(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public int Actor
		{
			get { return _actor; }
			set { _actor = value; }
			
		}
		
		public int Templateid
		{
			get { return _templateid; }
			set { _templateid = value; }
			
		}
		
		public DateTimeOffset HappenedOn
		{
			get { return _happenedon; }
			set { _happenedon = value; }
			
		}
		
		public DateTimeOffset CommentedUpdatedOn
		{
			get { return _commentedupdatedon; }
			set { _commentedupdatedon = value; }
			
		}
		
		public int? Entityid
		{
			get { return _entityid; }
			set { _entityid = value; }
			
		}
        public virtual string AttributeGroupRecordName
        {
            get { return _attributegrouprecordname; }
            set
            {            
                _attributegrouprecordname = value;
            }

        }
		public string TypeName
		{
			get { return _typename; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("TypeName", "TypeName value, cannot contain more than 50 characters");
			  _typename = value; 
			}
			
		}
		
		public string AttributeName
		{
			get { return _attributename; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("AttributeName", "AttributeName value, cannot contain more than 250 characters");
			  _attributename = value; 
			}
			
		}
		
		public string FromValue
		{
			get { return _fromvalue; }
			set 
			{
			  if (value != null && value.Length > 2000)
			    throw new ArgumentOutOfRangeException("FromValue", "FromValue value, cannot contain more than 2000 characters");
			  _fromvalue = value; 
			}
			
		}
		
		public string ToValue
		{
			get { return _tovalue; }
			set 
			{
			  if (value != null && value.Length > 2000)
			    throw new ArgumentOutOfRangeException("ToValue", "ToValue value, cannot contain more than 2000 characters");
			  _tovalue = value; 
			}
			
		}
		
		public IList<IFeedComment> FeedComment
		{
			get { return _feedcomment; }
			set { _feedcomment = value; }
			
		}

        public  int UserID
        {
            get { return _userid; }
            set {_userid = value; }

        }

        public virtual int? AssocitedEntityID
        {
            get { return _associatedentityid; }
            set { _associatedentityid = value; }
        }
        public virtual int? Version
        {
            get { return _version; }
            set { _version = value; }
        }
        public virtual int? PhaseID
        {
            get { return _phaseID; }
            set { _phaseID = value; }
        }
        public virtual string StepID
        {
            get { return _stepID; }
            set { _stepID = value; }
        }
		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			Feed castObj = null;
			try
			{
				castObj = (Feed)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
