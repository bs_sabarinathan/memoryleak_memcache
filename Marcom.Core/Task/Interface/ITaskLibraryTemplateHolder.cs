﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Task.Interface
{

    public interface ITaskLibraryTemplateHolder
    {
        #region Public Properties

        int ID { get; set; }
        string LibraryName { get; set; }
        bool IsActive { get; set; }
        int SortOrder { get; set; }

        string LibraryDescription { get; set; }
        dynamic TaskList { get; set; }
        int EntityParentID { get; set; }

        bool IsGetTasks { get; set; }
        bool IsExpanded { get; set; }
        int TaskCount { get; set; }

        #endregion
    }
}
