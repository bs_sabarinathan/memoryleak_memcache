using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IUserMultiSubscription interface for table 'CM_UserMultiSubscription'.
    /// </summary>
    public interface IUserMultiSubscription
    {
        #region Public Properties

        ISubscriptionType Subscriptionid
        {
            get;
            set;

        }

        int EntityTypeid
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
