using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// INavigation interface for table 'CM_ApplicationUrlTrack'.
    /// </summary>
    public interface IApplicationUrlTrack
    {
        #region Public Properties

        Guid TrackID
        {
            get;
            set;

        }

        string TrackValue
        {
            get;
            set;

        }

        DateTime DOI
        {
            get;
            set;

        }

       
        

        #endregion
    }
}
