(function(ng,app){"use strict";function muiadminassetrolesCtrl($scope,$resource,$location,$timeout,$translate,AccessService,DamService){$scope.listglobalrolesdata=[];$scope.globalrolesData=[];AccessService.GetGlobalRole().then(function(getglobalroles){$scope.listglobalrolesdata=getglobalroles.Response;$scope.globalrolesData=$scope.listglobalrolesdata;});$scope.UpdateUserVisibility=function(roles)
{var IsEnable=$(".checkForCount")[this.$index].checked;if(IsEnable==false){IsEnable=0;}else{IsEnable=1;}
var updateasset={};updateasset.RoleID=roles.Id;updateasset.IsChecked=IsEnable;DamService.UpdateAssetAccessSettings(updateasset).then(function(updateassetvalues){if(updateassetvalues.Response==true)
NotifySuccess($translate.instant('LanguageContents.Res_4782.Caption'))
else
NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));});}
$scope.$on("$destroy",function(){RecursiveUnbindAndRemove($("[ng-controller='mui.admin.assetrolesCtrl']"));});}
app.controller("mui.admin.assetrolesCtrl",['$scope','$resource','$location','$timeout','$translate','AccessService','DamService',muiadminassetrolesCtrl]);})(angular,app);