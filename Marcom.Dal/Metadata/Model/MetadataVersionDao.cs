﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public partial class MetadataVersionDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _name;
        protected string _description;
        protected int _state;
        protected DateTime _startdate;
        protected DateTime _enddate;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors

         public MetadataVersionDao() { }

         public MetadataVersionDao(int pId,string pName, string pDescription, int pState, DateTime pStartDate, DateTime pEndDate)
		{
			this._id = pId; 
			this._name = pName;
            this._description = pDescription; 
			this._state = pState; 
			this._startdate = pStartDate; 
            this._enddate = pEndDate;
		}

        #endregion

         #region Public Properties

         public virtual int ID
         {
             get { return _id; }
             set { _bIsChanged |= (_id != value); _id = value; }

         }

         public virtual string Name
         {
             get { return _name; }
             set
             {
                 if (value != null && value.Length > 250)
                     throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
                 _bIsChanged |= (_name != value);
                 _name = value;
             }

         }

         public virtual string Description
         {
             get { return _description; }
             set
             {
                 if (value != null && value.Length > 500)
                     throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 500 characters");
                 _bIsChanged |= (_description != value);
                 _description = value;
             }

         }

         public virtual int State
         {
             get { return _state; }
             set { _bIsChanged |= (_state != value); _state = value; }

         }

         public virtual DateTime StartDate
         {
             get { return _startdate; }
             set { _bIsChanged |= (_startdate != value); _startdate = value; }

         }

         public virtual DateTime EndDate
         {
             get { return _enddate; }
             set { _bIsChanged |= (_enddate != value); _enddate = value; }

         }


         public virtual bool IsDeleted
         {
             get
             {
                 return _bIsDeleted;
             }
             set
             {
                 _bIsDeleted = value;
             }
         }

         public virtual bool IsChanged
         {
             get
             {
                 return _bIsChanged;
             }
             set
             {
                 _bIsChanged = value;
             }
         }

         #endregion

         #region ICloneable methods

         public virtual object Clone()
         {
             return this.MemberwiseClone();
         }

         #endregion

         #region Public Property and Mapping Constants

         //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
         public static class PropertyNames
         {
             public const string MetadataVersionDao = "MetadataVersionDao";
             public const string ID = "ID";
             public const string Name = "Name";
             public const string Description = "Description";
             public const string State = "State";
             public const string StartDate = "StartDate";
             public const string EndDate = "EndDate";
         }

         //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
         public static class MappingNames
         {
             public const string MetadataVersionDao = "MetadataVersionDao";
             public const string ID = "ID";
             public const string Name = "Name";
             public const string Description = "Description";
             public const string State = "State";
             public const string StartDate = "StartDate";
             public const string EndDate = "EndDate";
         }

         #endregion
    }
}
