﻿(function (ng, app) {
    "use strict";

    function muiadmintasklistlibraryCtrl($scope, $location, $resource, $timeout, $cookies, $window, $compile, $attrs, $sce, $translate, AdminTaskService) {
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath;
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            imagesrcpath = cloudpath;
        }
        var model;
        $scope.GetNonBusinessDaysforDatePicker();
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            $scope[model1] = true;
            model = model1;
        };
        $scope.PeriodCalanderopen = function ($event, item, place) {
            $event.preventDefault();
            $event.stopPropagation();
            if (place == "start") {
                item.calstartopen = true;
                item.calendopen = false;
            } else if (place == "end") {
                item.calstartopen = false;
                item.calendopen = true;
            }
        };
        var loadUploaderAttr = false;
        $scope.PerioddirectiveCalanderopen = function ($event, attrid, place) {
            $event.preventDefault();
            $event.stopPropagation();
            if (place == "start") $scope.fields["PeriodStartDateopen_" + attrid] = true;
            else $scope.fields["PeriodEndDateopen_" + attrid] = true;
        };
        $scope.dynCalanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.fields["DatePart_Calander_Open" + model] = true;
        };

        function GenarateUniqueId() {
            var date = new Date();
            var components = [date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()];
            return components.join('');
        }
        $scope.FileIdForAssetCategory = "";
        $scope.AssetCategory = {};
        $scope.returnObj = [];
        $scope.newTaskid = 0;
        $scope.currenttasklistid = 0;
        $scope.tasklistcurrentindex = -1;
        $scope.ngShowadmintempTaskCreation = false;
        $scope.adminaddfromComputer = false;
        $scope.uploadingInProgress = false;
        $('#assetlistActivityTaskadmin').empty();
        $("#totalAssetActivityAttachProgresstaskadmin").empty();
        $scope.ExpandCollpase = $translate.instant('LanguageContents.Res_4939.Caption');
        var perioddates = [];
        $scope.AddCheckList = function (Index) {
            if ($scope.AdminTaskCheckList[Index].NAME.length > 0) {
                $scope.AdminTaskCheckList.push({
                    ID: 0,
                    NAME: ""
                });
            }
        }
        $scope.RemoveCheckList = function (Index, ID) {
            if ($scope.AdminTaskCheckList.length > 1) {
                bootbox.confirm($translate.instant('LanguageContents.Res_2012.Caption'), function (result) {
                    if (result) {
                        $timeout(function () {
                            if (ID > 0) {
                                AdminTaskService.DeleteAdminTaskCheckListByID(ID).then(function (result) {
                                    if (result.StatusCode == 200) {
                                        $scope.AdminTaskCheckList.splice(Index, 1);
                                    }
                                });
                            } else {
                                $scope.AdminTaskCheckList.splice(Index, 1);
                            }
                        }, 100);
                    }
                });
            } else {
                bootbox.confirm($translate.instant('LanguageContents.Res_2012.Caption'), function (result) {
                    if (result) {
                        $timeout(function () {
                            if (ID > 0) {
                                AdminTaskService.DeleteAdminTaskCheckListByID(ID).then(function (result) {
                                    if (result.StatusCode == 200) {
                                        $scope.AdminTaskCheckList[Index].NAME = "";
                                    }
                                });
                            } else {
                                $scope.AdminTaskCheckList[Index].NAME = "";
                            }
                        }, 100);
                    }
                });
            }
        }
        $scope.TaskLibraryList = [];
        $scope.TaskActionCaption = "";
        var fileUniqueID = '';
        $scope.OwnerList = [];
        $scope.OwnerName = $cookies['Username'];
        $scope.OwnerID = $cookies['UserId'];
        $scope.ownerEmail = $cookies['UserEmail'];
        $scope.OwnerList.push({
            "Roleid": 1,
            "RoleName": "Owner",
            "UserEmail": $scope.ownerEmail,
            "DepartmentName": "-",
            "Title": "-",
            "Userid": parseInt($scope.OwnerID, 10),
            "UserName": $scope.OwnerName,
            "IsInherited": '0',
            "InheritedFromEntityid": '0'
        });
        $scope.globalAttachment = true;
        $scope.UnavailableTaskLibrary = false;
        AdminTaskService.GetTaskList().then(function (TaskListLibraryData) {
            var TaskLibraryList = TaskListLibraryData.Response;
            if (TaskLibraryList.length == 0) {
                $scope.UnavailableTaskLibrary = true;
            } else $scope.TaskLibraryList = TaskListLibraryData.Response;
        });
        $scope.GetAdminTasksbyTaskListID = function (taskListID, taskCount, index) {
            if (taskCount > 0) {
                var currentindex = index;
                var CurrentId = taskListID;
                $scope.currenttasklistid = taskListID;
                $scope.tasklistcurrentindex = index;
                if ($("#tasklist" + CurrentId).attr('data-isload') == 'true') {
                    AdminTaskService.GetAdminTasksByTaskListID(taskListID).then(function (GetMytasksResult) {
                        var taskListData = [];
                        for (var j = 0, taskobj; taskobj = GetMytasksResult.Response[j++];) {
                            if (taskobj.Description == "NULL") taskobj.Description = "";
                            taskListData.push({
                                Id: taskobj.Id,
                                Caption: taskobj.Caption,
                                Description: taskobj.Description,
                                ColorCode: taskobj.colorcode,
                                ShortDesc: taskobj.ShortDescription,
                                SortOder: taskobj.SortOder,
                                TaskListID: taskobj.TaskListID,
                                TaskType: taskobj.TaskType,
                                TaskID: taskobj.TaskID,
                                TaskTypeName: taskobj.TaskTypeName,
                                TotalTaskAttachment: taskobj.TotalTaskAttachment,
                                AttributeData: [],
                                AdminTaskCheckList: []
                            });
                        }
                        $scope.TaskLibraryList[currentindex].TaskList = taskListData;
                    });
                }
                $("#tasklist" + CurrentId).attr('data-isload', 'false');
                if ($("#tasklist" + CurrentId).hasClass('icon-caret-right')) {
                    $("#TaskHolder" + CurrentId).css('display', '');
                    $("#tasklist" + CurrentId).removeClass('icon-caret-right').addClass('icon-caret-down');
                } else {
                    $("#TaskHolder" + CurrentId).css('display', 'none');
                    $("#tasklist" + CurrentId).removeClass('icon-caret-down').addClass('icon-caret-right')
                }
            }
        }
        $scope.ExpandCollapseTasks = function () {
            if ($scope.ExpandCollpase == $translate.instant('LanguageContents.Res_4939.Caption')) {
                var MyTaskParams = {};
                MyTaskParams.libraryObj = $scope.TaskLibraryList;
                AdminTaskService.GetAdminTasksById(MyTaskParams).then(function (GetMytasksResult) {
                    if (GetMytasksResult.Response != null) {
                        var taskObj = GetMytasksResult.Response;
                        for (var i = 0, tasklistObj; tasklistObj = $scope.TaskLibraryList[i++];) {
                            var CurrentId = tasklistObj.ID;
                            var taskListData = [];
                            var taskResObj = $.grep(taskObj, function (e) {
                                return e.TaskListID == tasklistObj.ID;
                            });
                            if (taskResObj.length > 0) {
                                for (var j = 0, taskobj; taskobj = taskResObj[j++];) {
                                    if (taskobj.Description == "NULL") taskobj.Description = "";
                                    taskListData.push({
                                        Id: taskobj.Id,
                                        Caption: taskobj.Caption,
                                        Description: taskobj.Description,
                                        ColorCode: taskobj.colorcode,
                                        ShortDesc: taskobj.ShortDescription,
                                        SortOder: taskobj.SortOder,
                                        TaskListID: taskobj.TaskListID,
                                        TaskType: taskobj.TaskType,
                                        TaskTypeName: taskobj.TaskTypeName,
                                        TotalTaskAttachment: taskobj.TotalTaskAttachment,
                                        AttributeData: [],
                                        AdminTaskCheckList: []
                                    });
                                }
                                $scope.TaskLibraryList[i - 1].TaskList = taskListData;
                                $("#TaskHolder" + CurrentId).css('display', '');
                                $("#tasklist" + CurrentId).removeClass('icon-caret-right').addClass('icon-caret-down');
                            }
                            $scope.ExpandCollpase = $translate.instant('LanguageContents.Res_4940.Caption');
                        }
                    }
                });
            } else {
                $scope.ExpandCollpase = $translate.instant('LanguageContents.Res_4939.Caption');
                for (var i = 0, tasklistObj; tasklistObj = $scope.TaskLibraryList[i++];) {
                    var CurrentId = tasklistObj.ID;
                    if (tasklistObj.TaskCount > 0) {
                        $("#TaskHolder" + CurrentId).css('display', 'none');
                        $("#tasklist" + CurrentId).removeClass('icon-caret-down').addClass('icon-caret-right');
                    }
                }
            }
        }
        $scope.formatResult = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="vertical-align: middle; margin-top: -4px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelection = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="vertical-align: middle; margin-top: -5px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.TaskType = '';
        $scope.TaskTypeListdata = [];
        $scope.tagAllOptionsTasktypelist = {
            multiple: false,
            allowClear: true,
            data: $scope.TaskTypeListdata,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.TaskTypeList = [];
        AdminTaskService.GetTaskTypes().then(function (TaskTypeData) {
            $scope.TaskTypeList = TaskTypeData.Response;
            for (var i = 0; i < $scope.TaskTypeList.length; i++) {
                var el = $scope.TaskTypeList[i];
                if (el.Id != 36) $scope.TaskTypeListdata.push({
                    "id": el.Id,
                    "text": el.Caption,
                    "ShortDescription": el.ShortDescription,
                    "ColorCode": el.ColorCode,
                    "Caption": el.Caption
                });
            }
        });
        $scope.entityeditcontrolclick = function () {
            $timeout(function () {
                $('[class=input-medium]').focus().select()
            }, 10);
        };
        $scope.UpdateTaskList = function (NewVale, IsName, LibraryID) {
            bootbox.alert(NewVale);
        }
        $scope.TaskListID = 0;
        $scope.EnableTaskListAdd = true;
        $scope.EnableTaskListUpdate = false;
        $scope.addTaskList = function () {
            $("#TaskCreationModal").modal('show');
            $scope.TaskListID = 0;
            $scope.Caption = '';
            $scope.Description = '';
            $scope.EnableTaskListAdd = true;
            $scope.EnableTaskListUpdate = false;
            $timeout(function () {
                $('#Caption').focus().select();
            }, 1000);
        };
        $scope.GetTaskByID = function GetTaskByID(TaskListID) {
            var taskObj = $.grep($scope.TaskLibraryList, function (e) {
                return e.ID == TaskListID;
            })[0];
            $scope.TaskListID = TaskListID;
            $scope.Caption = taskObj.LibraryName;
            $scope.Description = taskObj.LibraryDescription;
            $scope.EnableTaskListUpdate = true;
            $scope.EnableTaskListAdd = false;
            $timeout(function () {
                $('#Caption').focus().select();
            }, 1000);
            $("#TaskCreationModal").modal("show");
        };

        function RefreshTaskList(TaskListObj) {
            var taskObj = $.grep($scope.TaskLibraryList, function (e) {
                return e.ID == TaskListObj.Id;
            });
            if (taskObj.length > 0) {
                taskObj[0].LibraryName = TaskListObj.Caption;
                taskObj[0].LibraryDescription = TaskListObj.Description;
            } else {
                $scope.TaskLibraryList.push({
                    "ID": TaskListObj.Id,
                    "TaskCount": 0,
                    "IsExpanded": false,
                    "LibraryName": TaskListObj.Caption,
                    "LibraryDescription": TaskListObj.Description,
                    "SortOrder": TaskListObj.SortOder,
                    "TaskList": []
                });
            }
        }
        $("#ListHolderID").sortable({
            items: ".InProgress",
            axis: "y",
            scroll: true,
            cursor: "move",
            update: function (event, ui) {
                for (var j = 0; j < $scope.TaskLibraryList.length; j++) {
                    var listofitems = $("#TaskHolder" + parseInt($scope.TaskLibraryList[j].ID, 10) + " li");
                    for (var i = 0; i < listofitems.length; i++) {
                        AdminTaskService.UpdateTaskSortOrder(listofitems[i]["id"], parseInt($scope.TaskLibraryList[j].ID, 10), i + 1).then(function (TaskListSortOrderResult) { });
                    }
                }
            }
        }).disableSelection();
        $scope.TaskBriefDetails = {
            taskID: 0,
            taskTypeId: 0,
            taskTypeName: "",
            EntityID: 0,
            taskName: "",
            status: "",
            statusID: 0,
            Description: "",
            taskAttachmentsList: [],
            Totalassetcount: 0,
            Totalassetsize: ""
        };
        $scope.FileList = [];
        $scope.AttachmentFilename = [];
        $scope.SelectedTaskLIstID = 0;

        function refreshModel() {
            $scope.TaskBriefDetails = {
                taskID: 0,
                taskTypeId: 0,
                taskTypeName: "",
                EntityID: 0,
                taskName: "",
                status: "",
                statusID: 0,
                Description: "",
                taskAttachmentsList: [],
            };
            $scope.FileList = [];
            $scope.AttachmentFilename = [];
        }
        $scope.SaveTaskList = function () {
            if ($scope.Caption != "" && $scope.Caption != undefined) {
                if ($('#AddTaskLibrary').hasClass('disabled')) {
                    return;
                }
                $('#AddTaskLibrary').addClass('disabled');
                if ($('#UpdateTaskLibrary').hasClass('disabled')) {
                    return;
                }
                $('#UpdateTaskLibrary').addClass('disabled');
                var addtaskObj = {};
                addtaskObj.ID = $scope.TaskListID;
                addtaskObj.Caption = $scope.Caption;
                addtaskObj.Description = $scope.Description;
                addtaskObj.sortorder = 1;
                AdminTaskService.InsertUpdateTaskList(addtaskObj).then(function (SaveTask) {
                    if (SaveTask.StatusCode == 405) {
                        $('#AddTaskLibrary').removeClass('disabled');
                        $('#UpdateTaskLibrary').removeClass('disabled');
                        NotifyError($translate.instant('LanguageContents.Res_4271.Caption'));
                    } else {
                        $('#AddTaskLibrary').removeClass('disabled');
                        $('#UpdateTaskLibrary').removeClass('disabled');
                        $("#TaskCreationModal").modal('hide');
                        var returnObj = SaveTask.Response.m_Item2;
                        RefreshTaskList(returnObj);
                        NotifySuccess($translate.instant('LanguageContents.Res_4833.Caption'));
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4595.Caption'));
            }
        };
        $scope.AttachmentFilename = [];
        $scope.globalAttachment = true;
        $scope.listPredfWorkflowFilesAttch = [];
        $scope.addAditionalattachments = function () {
            $scope.globalAttachment = false;
            $scope.fnTimeOut();
        }
        $scope.fnTimeOut = function () {
            $('#filelistWorkFlow').empty();
            $('#dragfilesAttachment').show();
            $("#totalProgress").empty();
            $("#totalProgress").append('<span class="pull-left count">0 of 0 Uploaded</span><span class="size">0 B / 0 B</span>' + '<div class="progress progress-striped active">' + '<div style="width: 0%" class="bar"></div>' + '</div>');
            $scope.StrartUpload();
        }

        function validateURL(textval) {
            var urlregex = new RegExp("^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
            return urlregex.test(textval);
        }
        $scope.AttachLink = function () {
            if ($scope.SelectedTaskID > 0) {
                var AttachLink = {};
                AttachLink.EntityID = $scope.SelectedTaskID;
                AttachLink.Name = $scope.addtxtLinkName;
                AttachLink.URL = $scope.addtxtLinkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '');
                AttachLink.LinkType = $scope.addLinkType;
                AttachLink.Description = "";
                AttachLink.ActiveVersionNo = 1;
                AttachLink.TypeID = 9;
                AttachLink.CreatedOn = Date.now();
                AttachLink.OwnerID = $cookies['UserId'];
                AttachLink.OwnerName = $cookies['Username'];
                AttachLink.ModuleID = 1;
                AdminTaskService.InsertLinkInAdminTasks(AttachLink).then(function (AttachResponse) {
                    if (AttachResponse.Response != 0) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4788.Caption'));
                        $scope.addtxtLinkName = "";
                        $scope.addtxtLinkURL = "";
                        $scope.addtxtLinkDesc = "";
                        $scope.addLinkType = 1;
                        $("#addLinkPopup").modal("hide");
                        var taskListResObj = $.grep($scope.TaskLibraryList, function (e) {
                            return e.ID == $scope.SelectedTaskLIstID;
                        });
                        var taskResObj = $.grep(taskListResObj[0].TaskList, function (e) {
                            return e.Id == $scope.SelectedTaskID;
                        });
                        if (taskResObj.length > 0) {
                            taskResObj[0].TotalTaskAttachment = parseInt(taskResObj[0].TotalTaskAttachment) + 1;
                        }
                        ReloadTaskAttachments($scope.SelectedTaskID);
                    }
                });
            } else {
                var linkName = ($scope.addtxtLinkName != undefined || $scope.addtxtLinkName != null) ? $scope.addtxtLinkName : "";
                var url = ($scope.addtxtLinkURL != undefined || $scope.addtxtLinkURL != null) ? $scope.addtxtLinkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '') : "";
                var linkdesc = ($scope.txtLinkDesc != undefined || $scope.txtLinkDesc != null) ? $scope.txtLinkDesc : "";
                $scope.AttachmentFilename.push({
                    "FileName": $scope.addtxtLinkName,
                    "Extension": "Link",
                    "Size": 0,
                    "FileGuid": 0,
                    "FileDescription": linkdesc,
                    "URL": url,
                    "LinkType": $scope.addLinkType,
                    "ID": 0,
                    "LinkID": ($scope.UniqueLinkID + 1)
                });
                $scope.addtxtLinkName = "";
                $scope.addtxtLinkURL = "";
                $scope.addtxtLinkDesc = "";
                $scope.addLinkType = 1;
                $("#addLinkPopup").modal("hide");
            }
        }
        $scope.popMe = function (link) {
            var linktypeObj = $.grep($scope.DefaultSettings.LinkTypes, function (val) {
                return val.Id == parseInt(link.LinkType)
            })[0];
            if (linktypeObj != undefined) {
                var mypage = linktypeObj.Type + link.URL;
                var myname = mypage;
                var w = 1200;
                var h = 800
                var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
                var win = window.open(mypage, myname, winprops)
            }
        };
        $scope.StrartUpload = function () {
            var uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                browse_button: 'pickfilesAlternate',
                drop_element: 'dragfilesAttachment',
                container: 'filescontainer',
                max_file_size: '10000mb',
                url: amazonURL + cloudsetup.BucketName,
                flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                urlstream_upload: true,
                multiple_queues: true,
                file_data_name: 'file',
                multipart: true,
                multipart_params: {
                    'key': '${filename}',
                    'Filename': '${filename}',
                    'acl': 'public-read',
                    'success_action_status': '201',
                    'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                    'policy': cloudsetup.PolicyDocument,
                    'signature': cloudsetup.PolicyDocumentSignature
                }
            });
            uploader.bind('Init', function (up, params) {
                uploader.splice();
            });
            $('#uploadfiles').click(function (e) {
                uploader.start();
                e.preventDefault();
            });
            $('#clearUploader').click(function (e) {
                uploader.destroy();
                $('.moxie-shim').remove();
            });
            uploader.init();
            $('#pickfiles').each(function () {
                var input = new mOxie.FileInput({
                    browse_button: this,
                    multiple: true
                });
                input.onchange = function (event) {
                    uploader.addFile(input.files);
                };
                input.init();
            });
            uploader.bind('FilesAdded', function (up, files) {
                $.each(files, function (i, file) {
                    var ste = file.name.split('.')[file.name.split('.').length - 1];
                    var stes = [];
                    stes = [{
                        ID: file.id,
                        Extension: ste
                    }];
                    $scope.listPredfWorkflowFilesAttch.push({
                        ID: file.id,
                        Name: file.name,
                        Createdon: new Date()
                    });
                    $('#filelistWorkFlow').append('<div id="' + file.id + '" class="attachmentBox" Data-role="Attachment" data-size="' + file.size + '" data-size-uploaded="0" data-status="false">' + '<div class="row-fluid">' + '<div class="span12">' + '<div class="info">' + '<span class="name" >' + '<i class="icon-file-alt"></i>' + file.name + '</span>' + '<span class="pull-right size">0 B / ' + plupload.formatSize(file.size) + '' + '<i class="icon-remove removefile" data-fileid="' + file.id + '"></i>' + '</span>' + '</div>' + '<div class="progress progress-striped active">' + '<div style="width: 0%" class="bar"></div>' + '</div>' + '</div>' + '</div>' + '<div class="row-fluid">' + '<div class="span12">' + '<form id="Form_' + file.id + '" class="form-inline">' + '<select>' + '<option>' + Mimer(ste) + '</option>' + '<option>Image</option>' + '<option>Video</option>' + '<option>Document</option>' + '<option>Zip</option>' + '</select>' + '<input type="text" name="DescVal_' + file.id + '" id="desc_' + file.id + '" placeholder="Description">' + '</form>' + '</div>' + '</div>' + '</div>');
                });
                $('#dragfilesAttachment').hide();
                up.refresh();
                TotalUploadProgress();
            });
            $('#filelistWorkFlow').on('click', '.removefile', function () {
                var fileToRemove = $(this).attr('data-fileid');
                $.each(uploader.files, function (i, file) {
                    if (file.id == fileToRemove) {
                        uploader.removeFile(file);
                        $('#' + file.id).remove();
                    }
                });
                if ($('#filelistWorkFlow .attachmentBox').length == 0) {
                    $('#dragfilesAttachment').show();
                }
                TotalUploadProgress();
            });
            uploader.bind('UploadProgress', function (up, file) {
                $('#' + file.id + " .bar").css("width", file.percent + "%");
                $('#' + file.id + " .size").html(plupload.formatSize(Math.round(file.size * (file.percent / 100))) + ' / ' + plupload.formatSize(file.size));
                $('#' + file.id).attr('data-size-uploaded', Math.round(file.size * (file.percent / 100)));
                TotalUploadProgress();
            });
            uploader.bind('Error', function (up, err) {
                $('#filelistWorkFlow').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                up.refresh();
            });
            uploader.bind('FileUploaded', function (up, file, response) {
                $('#' + file.id).attr('data-status', 'true');
                var fileDescription = $('#Form_' + file.id + '').find('input[name="DescVal_' + file.id + '"]').val();
                SaveFileDetails(file, response.response, fileDescription);
                $scope.globalAttachment = true;
                TotalUploadProgress();
            });
            uploader.bind('BeforeUpload', function (up, file) {
                $.extend(up.settings.multipart_params, {
                    id: file.id.replace("_", ""),
                    size: file.size
                });
                var keyName = file.name;
                var keySplit = keyName.split('/');
                var fileName = keySplit[keySplit.Length - 1];
                var fileext = "." + file.name.split('.').pop();
                var uniqueKey = (TenantFilePath + "DAMFiles/Original/" + file.id + fileext).replace(/\\/g, '/');
                uploader.settings.multipart_params.key = uniqueKey;
                uploader.settings.multipart_params.Filename = uniqueKey;
            });
            uploader.bind('FileUploaded', function (up, file, response) {
                var obj = response;
                $('#' + file.id).attr('data-fileId', response.response);
            });

            function TotalUploadProgress() {
                var TotalSize = 0;
                var TotalCount = $('#filelistWorkFlow .attachmentBox').length;
                var UploadedSize = 0;
                var UploadedCount = 0;
                $('#filelistWorkFlow .attachmentBox').each(function () {
                    TotalSize += parseInt($(this).attr('data-size'));
                    UploadedSize += parseInt($(this).attr('data-size-uploaded'));
                    if ($(this).attr('data-status') == 'true') {
                        UploadedCount += 1;
                    }
                });
                $('#totalProgress .count').html(UploadedCount + ' of ' + TotalCount + ' Uploaded');
                $('#totalProgress .size').html(plupload.formatSize(UploadedSize) + ' / ' + plupload.formatSize(TotalSize));
                $('#totalProgress .bar').css("width", Math.round(((UploadedSize / TotalSize) * 100)) + "%");
            }
        }
        $scope.Clear = function () { }
        $scope.FileList = [];
        $scope.InsertableFileList = [];
        $scope.InsertableAttachmentFilename = [];

        function SaveFileDetails(file, response, fileDescription) {
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");
            $scope.FileList.push({
                "Name": file.name,
                "VersionNo": 1,
                "MimeType": resultArr[1],
                "Extension": extension,
                "OwnerID": $cookies['UserId'],
                "CreatedOn": Date.now(),
                "Checksum": "",
                "ModuleID": 1,
                "EntityID": $routeParams.ID,
                "Size": file.size,
                "FileGuid": resultArr[0],
                "FileDescription": fileDescription
            });
            $scope.InsertableFileList.push({
                "Name": file.name,
                "VersionNo": 1,
                "MimeType": resultArr[1],
                "Extension": extension,
                "OwnerID": $cookies['UserId'],
                "CreatedOn": Date.now(),
                "Checksum": "",
                "ModuleID": 1,
                "EntityID": $routeParams.ID,
                "Size": file.size,
                "FileGuid": resultArr[0],
                "FileDescription": fileDescription
            });
            $scope.AttachmentFilename.push({
                "FileName": file.name,
                "Extension": extension,
                "Size": parseSize(file.size),
                "FileGuid": resultArr[0],
                "FileDescription": fileDescription,
                "URL": "",
                "LinkType": "",
                "LinkID": 0,
                "ID": 0
            });
            $scope.InsertableAttachmentFilename.push({
                "FileName": file.name,
                "Extension": extension,
                "Size": parseSize(file.size),
                "FileGuid": resultArr[0],
                "FileDescription": fileDescription,
                "ID": 0
            });
            if ($scope.SelectedTaskID > 0) {
                var SaveTask = {};
                SaveTask.TaskID = $scope.SelectedTaskID;
                SaveTask.TaskAttachments = $scope.InsertableAttachmentFilename;
                SaveTask.TaskFiles = $scope.InsertableFileList;
                AdminTaskService.InsertTaskAttachments(SaveTask).then(function (SaveTaskResult) {
                    if (SaveTaskResult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4275.Caption'));
                        $scope.globalAttachment = true;
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4388.Caption'));
                        var taskListResObj = $.grep($scope.TaskLibraryList, function (e) {
                            return e.ID == $scope.SelectedTaskLIstID;
                        });
                        var taskResObj = $.grep(taskListResObj[0].TaskList, function (e) {
                            return e.Id == $scope.SelectedTaskID;
                        });
                        if (taskResObj.length > 0) {
                            taskResObj[0].TotalTaskAttachment = parseInt(taskResObj[0].TotalTaskAttachment) + 1;
                            taskResObj[0].AttachmentCollection += $scope.InsertableFileList[0].Name;
                        }
                        $scope.InsertableFileList = [];
                        $scope.InsertableAttachmentFilename = [];
                        ReloadTaskAttachments($scope.SelectedTaskID);
                    }
                });
            }
        }

        function ReloadTaskAttachments(taskID) {
            $scope.AttachmentFilename = [];
            AdminTaskService.GetTaskAttachmentFile(taskID).then(function (TaskDetailList) {
                $scope.attachedFiles = TaskDetailList.Response;
                for (var i = 0; i <= $scope.attachedFiles.length - 1; i++) {
                    $scope.AttachmentFilename.push({
                        "FileName": $scope.attachedFiles[i].Name,
                        "Extension": $scope.attachedFiles[i].Extension,
                        "Size": parseSize($scope.attachedFiles[i].Size),
                        "FileGuid": $scope.attachedFiles[i].Fileguid,
                        "FileDescription": $scope.attachedFiles[i].Description,
                        "ID": $scope.attachedFiles[i].Id,
                        "URL": $scope.attachedFiles[i].linkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', ''),
                        "LinkType": $scope.attachedFiles[i].LinkType
                    });
                }

                $scope.AssetTaskAttachAssetHtml = "";
                $("#assetlistActivityTaskadminexisting").html($compile($scope.AssetTaskAttachAssetHtml)($scope));

                for (var i = 0, asset; asset = $scope.attachedFiles[i++];) {
                    var rec = [];
                    rec = $.grep($scope.AssetTasktypeswithExt.m_Item1, function (e) {
                        return e.Id == asset.AssetTypeid
                    });

                    $scope.AssetTaskAttachAssetHtml += '<div id="Asset_' + asset.Id + '" class="attachmentBox">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="row-fluid">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="span12">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="span2 leftSection">';
                    $scope.AssetTaskAttachAssetHtml += '<i class="icon-ok-sign" style="display:none;"></i>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="fileType-Preview">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="fileType-PreviewDiv">';
                    if (asset.Category == 0) {
                        if (asset.Extension != null) {
                            if (asset.ProcessType > 0) {
                                if (asset.Status == 2) $scope.AssetTaskAttachAssetHtml += '<img id="file_' + asset.Id + '"  src="' + imagesrcpath + 'DAMFiles/Preview/Small_' + asset.Fileguid + '.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 1 || asset.Status == 0) $scope.AssetTaskAttachAssetHtml += '<img class="loadingImg" id=' + asset.ActiveFileID + ' data-extn=' + asset.Extension + ' data-src=' + asset.Fileguid + ' ng-click=\"Editasset(' + asset.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 3) {
                                    $scope.AssetTaskAttachAssetHtml += '<img  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                    $scope.AssetTaskAttachAssetHtml += '<div class="th-Error-preview">';
                                    $scope.AssetTaskAttachAssetHtml += '<span class="errorMsg"><i class="icon-info-sign"></i>';
                                    $scope.AssetTaskAttachAssetHtml += '<span class="errorTxt">Unable to generate Preview</span></span>';
                                    $scope.AssetTaskAttachAssetHtml += '</div>';
                                }
                            } else {
                                $scope.AssetTaskAttachAssetHtml += '<img  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else $scope.AssetTaskAttachAssetHtml += '<img  src="' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 1) {
                        $scope.AssetTaskAttachAssetHtml += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 2) {
                        $scope.AssetTaskAttachAssetHtml += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="progress progress-striped active">';
                    $scope.AssetTaskAttachAssetHtml += '<div style="width: 0%" class="bar"></div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="progressInfo">';
                    $scope.AssetTaskAttachAssetHtml += '<span class="progress-percent"></span>';
                    if (asset.Category == 0) {
                        if (parseSize(asset.Size) == "NaN bytes") {
                            $scope.AssetTaskAttachAssetHtml += '<span class="fileSize">' + (asset.Size) + ' </span>';
                        } else {
                            $scope.AssetTaskAttachAssetHtml += '<span class="fileSize">' + parseSize(asset.Size) + ' </span>';
                        }
                    } else {
                        $scope.AssetTaskAttachAssetHtml += '<span class="fileSize"></span>';
                    }
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="span10 rightSection">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="info">';
                    $scope.AssetTaskAttachAssetHtml += '<span class="name">';
                    if (asset.Category == 0) {
                        $scope.AssetTaskAttachAssetHtml += '<i class="icon-file-alt"></i>';
                    }
                    $scope.AssetTaskAttachAssetHtml += asset.Name;
                    $scope.AssetTaskAttachAssetHtml += '</span>';
                    $scope.AssetTaskAttachAssetHtml += '<span class="size" ng-click="removeasset(' + asset.Assetid + ')">';
                    $scope.AssetTaskAttachAssetHtml += '<i class="icon-remove removefile"></i>';
                    $scope.AssetTaskAttachAssetHtml += '</span>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="formInfo">';
                    $scope.AssetTaskAttachAssetHtml += '<form class="form-float">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="control-group fullSpace">';
                    $scope.AssetTaskAttachAssetHtml += '<label class="control-label"> Asset type </label>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="controls">';
                    $scope.AssetTaskAttachAssetHtml += '<label class="control-label">' + rec[0] != undefined ? rec[0].damCaption : "" + '</label>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="control-group">';
                    $scope.AssetTaskAttachAssetHtml += '<label class="control-label"> Asset name </label>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="controls">';
                    $scope.AssetTaskAttachAssetHtml += '<label class="control-label">' + asset.Name + '</label>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</form>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                }
                $scope.IsAttachmentPresent = true;
                $("#assetlistActivityTaskadminexisting").append($compile($scope.AssetTaskAttachAssetHtml)($scope));
            });
        }

        $scope.removeasset = function (assetid) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2013.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var data = {};
                        data.FolderArr = [assetid];
                        AdminTaskService.deleteAssets(data).then(function (result) {
                            if (result.Response == 0) {
                                NotifyError($translate.instant('LanguageContents.Res_4285.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4100.Caption'));
                                ReloadTaskAttachments($scope.SelectedTaskID);
                                RefreshTaskAttachmentcount();
                            }
                        });
                    }, 100);
                }
            });
        }

        function RefreshTaskAttachmentcount() {
            var taskListResObj = $.grep($scope.TaskLibraryList, function (e) {
                return e.ID == $scope.SelectedTaskLIstID;
            });
            var taskResObj = $.grep(taskListResObj[0].TaskList, function (e) {
                return e.Id == $scope.SelectedTaskID;
            })[0];
            if (taskResObj != undefined) {
                taskResObj.TotalTaskAttachment = taskResObj.TotalTaskAttachment > 0 ? taskResObj.TotalTaskAttachment - 1 : taskResObj.TotalTaskAttachment;
            }
        }

        function parseSize(size) {
            var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"],
				tier = 0;
            while (size >= 1024) {
                size = size / 1024;
                tier++;
            }
            return Math.round(size * 10) / 10 + " " + suffix[tier];
        }
        $scope.ngShowTaskCreation = true;
        $scope.ngShowTaskUpdate = false;
        $scope.atributesRelationList = [];
        $scope.updatetask = false;
        $scope.addNewTask = function (TaskListID) {
            $scope.templateData.PhaseData.AllPhaseList = [];
            $scope.FileIdForAssetCategory = "";
            $scope.AssetCategory = {};
            $scope.addtaskvallid = 0;
            $scope.AssetfileTemplateLoading = "Null";
            $scope.processingsrcobj.processingid = 0;
            $scope.processingsrcobj.processingplace = "task";
            $scope.TaskSave = false;
            $scope.TaskUpdate = false;
            $scope.updatetask = false;
            $scope.AdminTaskCheckList = [{
                ID: 0,
                NAME: ""
            }];
            $scope.TaskActionCaption = "Add task";
            $scope.ngShowTaskCreation = true;
            $scope.ngShowTaskUpdate = false;
            $scope.NewTaskName = "";
            $scope.TaskType = 0;
            $scope.TaskDescription = "";
            $scope.SelectedTaskLIstID = TaskListID;
            $scope.SelectedTaskID = 0;
            $scope.dyn_Cont = '';
            $("#assetlistActivityTaskadmin").html("");
            $("#assetlistActivityTaskadminexisting").html("");
            $("#assetlistActivityTaskadmin").append($compile("")($scope));
            $("#assetlistActivityTaskadminexisting").append($compile("")($scope));

            $('#assetlistActivityTaskadmin').empty();
            $("#admintaskDynamicTaskControls").html("");
            $("#admintaskDynamicTaskControls").append($compile("")($scope));
            $scope.atributesRelationList = [];
            $('#adminTaskDynamic-metadata').html("");
            $scope.ChecklistVisible = false;
            $scope.rytSideApprovalButtonPane = false;
            $("#addTask").modal('show');
            $timeout(function () {
                $('#taskCaption').focus()
            }, 1000);
            refreshModel();

        };
        $scope.SelectedTaskID = 0;
        $scope.EditTask = function (taskID, TaskListID) {
            $scope.rytSideApprovalButtonPane = false;
            $scope.TaskSave = false;
            $scope.TaskUpdate = false;
            $scope.updatetask = true;
            $scope.TaskActionCaption = "Edit task";
            $scope.SelectedTaskID = taskID;
            $scope.SelectedTaskLIstID = TaskListID;
            $scope.ngShowTaskCreation = false;
            $scope.ngShowTaskUpdate = true;
            $scope.dyn_Cont = '';
            $scope.processingsrcobj.processingid = taskID;
            $scope.processingsrcobj.processingplace = "task";
            $timeout(function () {
                $scope.AssetfileTemplateLoading = "task";
                $scope.$broadcast('ReloadAssetView');
            }, 100);
            $scope.approvalflowTempPopup = true;
            $("#assetlistActivityTaskadmin").html("");
            $("#assetlistActivityTaskadminexisting").html("");
            $("#assetlistActivityTaskadmin").append($compile("")($scope));
            $("#assetlistActivityTaskadminexisting").append($compile("")($scope));
            $("#admintaskDynamicTaskControls").html("");
            $("#admintaskDynamicTaskControls").append($compile("")($scope));
            $timeout(function () {
                $('#taskflagname').focus().select();
            }, 1000);
            var taskListResObj = $.grep($scope.TaskLibraryList, function (e) {
                return e.ID == TaskListID;
            });
            var taskResObj = $.grep(taskListResObj[0].TaskList, function (e) {
                return e.Id == taskID;
            });
            if (taskResObj.length > 0) {
                $scope.TaskActionCaption = "Edit task";
                $scope.NewTaskName = taskResObj[0].Caption;
                $scope.TaskDescription = taskResObj[0].Description;
                $scope.SelectedTaskID = taskID;
                $scope.TaskType = (taskResObj[0].TaskID != undefined) ? taskResObj[0].TaskID : taskResObj[0].TaskType;
                $scope.AdminTaskCheckList = [{
                    ID: 0,
                    NAME: ""
                }];
                GetTaskCheckLists(taskID);
                $scope.atributesRelationList = [];
                GetEntityAttributesDetails(taskID);
                ReloadTaskAttachments(taskID);
                GetEntityTaskAttachmentinfo(taskID);
                GetEntityApprovalTaskTemplatedetails(taskID);
            }
            var Tasktype_value = $.grep($scope.TaskTypeList, function (rel) {
                return rel.Id == $scope.TaskType;
            })[0];
            if (Tasktype_value.TaskTypeId == 2) {
                $scope.ChecklistVisible = true;
            }
            if (Tasktype_value.TaskTypeId != 2) $scope.ChecklistVisible = false;
            $("#addTask").modal('show');
            $timeout(function () {
                $('#taskCaption').focus().select();
            }, 1000);
        }

        function GetTaskCheckLists(taskid) {
            AdminTaskService.getAdminTaskchecklist(taskid).then(function (TaskcheckList) {
                if (TaskcheckList.Response.length > 0) {
                    $scope.AdminTaskCheckList = [];
                    $scope.AdminTaskCheckList = TaskcheckList.Response;
                }
            });
        }
        $scope.deleteAttachedFiles = function (fileObject) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2012.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var attachmentDelete = $.grep($scope.AttachmentFilename, function (e) {
                            return e.FileGuid == fileObject.FileGuid
                        });
                        var stringFileName = attachmentDelete[0].FileName;
                        $scope.AttachmentFilename.splice($.inArray(attachmentDelete[0], $scope.AttachmentFilename), 1);
                        var fileToDelete = $.grep($scope.FileList, function (e) {
                            return e.FileGuid == fileObject.FileGuid
                        });
                        $scope.FileList.splice($.inArray(fileToDelete[0], $scope.FileList), 1);
                        if (attachmentDelete[0].Extension != "Link") {
                            if (attachmentDelete[0].ID > 0) {
                                AdminTaskService.DeleteAttachments(attachmentDelete[0].ID).then(function (result) {
                                    if (result != null && result.Response != false) {
                                        var taskListResObj = $.grep($scope.TaskLibraryList, function (e) {
                                            return e.ID == $scope.SelectedTaskLIstID;
                                        });
                                        var taskResObj = $.grep(taskListResObj[0].TaskList, function (e) {
                                            return e.Id == $scope.SelectedTaskID;
                                        });
                                        if (taskResObj.length > 0) {
                                            taskResObj[0].TotalTaskAttachment = parseInt(taskResObj[0].TotalTaskAttachment) - 1;
                                            taskResObj[0].AttachmentCollection = removeValue(taskResObj[0].AttachmentCollection, stringFileName, ',');
                                        }
                                        NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                                    }
                                });
                            }
                        } else {
                            if (attachmentDelete[0].ID > 0) {
                                AdminTaskService.DeleteAdminTaskLinkByID(attachmentDelete[0].ID).then(function (result) {
                                    if (result != null && result.Response != false) {
                                        NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                                        var taskListResObj = $.grep($scope.TaskLibraryList, function (e) {
                                            return e.ID == $scope.SelectedTaskLIstID;
                                        });
                                        var taskResObj = $.grep(taskListResObj[0].TaskList, function (e) {
                                            return e.Id == $scope.SelectedTaskID;
                                        });
                                        if (taskResObj.length > 0) {
                                            taskResObj[0].TotalTaskAttachment = parseInt(taskResObj[0].TotalTaskAttachment) - 1;
                                        }
                                        ReloadTaskAttachments($scope.SelectedTaskID);
                                    }
                                });
                            }
                        }
                    }, 100);
                }
            });
        }

        function removeValue(list, value, separator) {
            separator = separator || ",";
            var values = list.split(separator);
            for (var i = 0; i < values.length; i++) {
                if (values[i] == value) {
                    values.splice(i, 1);
                    return values.join(separator);
                }
            }
            return list;
        }
        var alertcount = 0;
        $scope.Entityamountcurrencytypeitem = [];
        $scope.AttributeData = [];
        $scope.CreateNewTask = function () {
            $scope.addtaskvallid = 0;
            if ($("#assetlistActivityTaskadmin > div").length > 0) {
                $scope.uploadingInProgress = true;
                $("#totalAssetActivityAttachProgresstaskadmin").empty();
                $("#admintaskuploaded").empty();
                $("#totalAssetActivityAttachProgresstaskadmin").append('<span class="pull-left count">0 of 0 Uploaded</span><span class="size">0 B / 0 B</span>' + '<div class="progress progress-striped active">' + '<div style="width: 0%" class="bar"></div>' + '</div>' + '<div id="admintaskuploaded" style="position: absolute; top: 42%; right: -30%;"></div>');
            }
            var percentageflag = false;
            $('div[data-role="formpercentagetotalcontainer"]').children().find('span[data-role="percentageerror"]').each(function (index, value) {
                if (($(this).attr('data-selection') != undefined) && ($(this).attr('data-ispercentage') != undefined) && ($(this).attr('data-isnotfilter') != undefined)) {
                    if (((parseInt($(this).attr('data-selection')) > 1) && ($(this).attr('data-ispercentage') == "true") && ($(this).attr('data-isnotfilter') == "true")) && ($(this).hasClass("result lapse"))) {
                        percentageflag = true;
                        $scope.uploadingInProgress = false;
                    }
                }
            });
            if (percentageflag) {
                return false;
            }
            $("#admintskbtnTemp").click();
            $("#adminTaskDynamicMetadata").removeClass('notvalidate');
            if ($("#adminTaskDynamicMetadata .error").length > 0) {
                $scope.uploadingInProgress = false;
                bootbox.alert($translate.instant('LanguageContents.Res_5716.Caption'));
                return false;
            }
            $scope.AttributeData = [];
            var alertText = "";
            if ($scope.TaskType == "" || $scope.TaskType == undefined || $scope.TaskType == 0) alertText += $translate.instant('LanguageContents.Res_4637.Caption');
            var Tasktype_value = $.grep($scope.TaskTypeList, function (rel) {
                return rel.Id == $scope.TaskType;
            })[0];
            if (Tasktype_value != undefined) if (Tasktype_value.TaskTypeId == "" || Tasktype_value.TaskTypeId == undefined) {
                if (!alertText.contains($translate.instant('LanguageContents.Res_4637.Caption'))) alertText += $translate.instant('LanguageContents.Res_4637.Caption');
            }
            if ($scope.NewTaskName == "") alertText += $translate.instant('LanguageContents.Res_1908.Caption') + "\n";
            if (Tasktype_value == undefined) {
                Tasktype_value = $.grep($scope.TaskTypeList, function (rel) {
                    return rel.Id == $scope.TaskType.id;
                })[0];
                for (var i = 0; i < $scope.AdminTaskCheckList.length; i++) {
                    if ($scope.AdminTaskCheckList.length > 1) {
                        if ($scope.AdminTaskCheckList[i]["NAME"] == "" || $scope.AdminTaskCheckList[i]["NAME"] == null) {
                            if (alertcount == 0) {
                                alertText += $translate.instant('LanguageContents.Res_4588.Caption');
                                alertcount = 1;
                            }
                        }
                    }
                }
            }
            if (alertText == "") {
                $scope.addtaskvallid = 1;
                $scope.TaskSave = true;
                $scope.TaskUpdate = true;
                $scope.AttributeData = [];
                var SaveTask = {};
                SaveTask.TaskType = Tasktype_value.TaskTypeId;
                SaveTask.Typeid = parseInt($scope.TaskType.id);
                SaveTask.Name = $scope.NewTaskName;
                SaveTask.TaskListID = $scope.SelectedTaskLIstID;
                SaveTask.Description = $scope.TaskDescription;
                SaveTask.TaskAttachments = [];
                SaveTask.TaskAttachments = $scope.AttachmentFilename;
                SaveTask.TaskFiles = $scope.FileList;
                if ($("#assetlistActivityTaskadmin > div").length > 0) {
                    $scope.uploadingInProgress = true;
                }
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id != undefined || $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level != undefined) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                        "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
                                        "Value": "-1"
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            var levelCount = $scope.atributesRelationList[i].Levels.length;
                            if (levelCount == 1) {
                                for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                        "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level,
                                        "Value": "-1"
                                    });
                                }
                            } else {
                                if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                    if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                        for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                            $scope.AttributeData.push({
                                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                                "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level,
                                                "Value": "-1"
                                            });
                                        }
                                    } else {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                            "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
                                            "Value": "-1"
                                        });
                                    }
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            var attributeLevelOptions = [];
                            attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID + ""], function (e) {
                                return e.level == (j + 1);
                            }));
                            if (attributeLevelOptions[0] != undefined) {
                                if (attributeLevelOptions[0].selection != undefined) {
                                    for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
                                        var valueMatches = [];
                                        if (attributeLevelOptions[0].selection.length > 1) valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                            return relation.NodeId.toString() === opt;
                                        });
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [opt],
                                            "Level": (j + 1),
                                            "Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
                                        });
                                    }
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                        if ($scope.atributesRelationList[i].IsSpecial == true) {
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            }
                        } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                            if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                                var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID];
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                        if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                        else {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": ($scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID] != null) ? $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                        if (($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TakeDescription) && ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskNotes)) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": ($scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID] != null) ? $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                                for (var k = 0; k < multiselectiObject.length; k++) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": parseInt(multiselectiObject[k], 10),
                                        "Level": 0,
                                        "Value": "-1"
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                        var MyDate = new Date();
                        var MyDateString;
                        if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                            if ($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != undefined) MyDateString = $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID];
                            else MyDateString = "";
                        } else {
                            MyDateString = "";
                        }
                        if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskDueDate) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": MyDateString,
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                        var treenodes = [];
                        treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) {
                            return e.AttributeId == $scope.atributesRelationList[i].AttributeID;
                        });
                        for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": [parseInt(nodeval.id, 10)],
                                "Level": parseInt(nodeval.Level, 10),
                                "Value": "-1"
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": (($scope.fields['MTextSingleLine_' + $scope.atributesRelationList[i].AttributeID]).toString()).replace(/ /g, ''),
                            "Level": 0,
                            "Value": "-1"
                        });
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.ImageFileName,
                            "Level": 0,
                            "Value": "-1"
                        });
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 18) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": "",
                            "Level": 0,
                            "Value": "-1"
                        });
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                        if ($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            for (var j = 0; j < $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID].length; j++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID][j], 10),
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                        if ($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] == "") {
                            $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = "0";
                        }
                        $scope.Entityamountcurrencytypeitem.push({
                            amount: parseFloat($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID].replace(/ /g, '')),
                            currencytype: $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID],
                            Attributeid: $scope.atributesRelationList[i].AttributeID
                        });
                    }
                }
                SaveTask.AttributeData = $scope.AttributeData;
                SaveTask.Periods = [];
                $scope.savesubentityperiods = [];
                for (var m = 0; m < $scope.items.length; m++) {
                    if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                        if ($scope.items[m].startDate.length != 0 && $scope.items[m].endDate.length != 0) {
                            $scope.savesubentityperiods.push({
                                startDate: ($scope.items[m].startDate.length != 0 ? dateFormat($scope.items[m].startDate) : ''),
                                endDate: ($scope.items[m].endDate.length != 0 ? dateFormat($scope.items[m].endDate) : ''),
                                comment: ($scope.items[m].comment.trim().length != 0 ? $scope.items[m].comment : ''),
                                sortorder: 0
                            });
                        }
                    }
                }
                SaveTask.Periods.push($scope.savesubentityperiods);
                var taskResObj = $.grep($scope.AdminTaskCheckList, function (e) {
                    return e.NAME != "";
                });
                SaveTask.Entityamountcurrencytype = [];
                $scope.curram = [];
                for (var m = 0; m < $scope.Entityamountcurrencytypeitem.length; m++) {
                    $scope.curram.push({
                        amount: $scope.Entityamountcurrencytypeitem[m].amount,
                        currencytype: $scope.Entityamountcurrencytypeitem[m].currencytype,
                        Attributeid: $scope.Entityamountcurrencytypeitem[m].Attributeid
                    });
                }
                SaveTask.Entityamountcurrencytype.push($scope.curram);
                SaveTask.AdminTaskCheckList = $scope.AdminTaskCheckList;
                SaveTask.PhaseStepDetails = $scope.templateData.PhaseData.AllPhaseList[0];

                var uploaderNodeID = [];

                var uploaderArr = $.grep(SaveTask.AttributeData, function (e) {
                    return e.AttributeTypeID == 11
                })
                if (uploaderArr.length > 0) {
                    for (var i = 0; i < uploaderArr.length; i++) {
                        uploaderNodeID.push(uploaderArr[i].NodeID);
                    }
                    if (uploaderNodeID.length > 0) {
                        AdminTaskService.savemultipleUploadedImg(uploaderNodeID).then(function (newIDs) {
                            if (newIDs.Response != null) {
                                for (var i = 0; i < newIDs.Response.length; i++) {
                                    var UploadAttr = $.grep(SaveTask.AttributeData, function (e) {
                                        return e.NodeID == newIDs.Response[i].m_Item1;
                                    })[0];
                                    var AttrIndex = SaveTask.AttributeData.indexOf(UploadAttr);
                                    SaveTask.AttributeData[AttrIndex].NodeID = newIDs.Response[i].m_Item2;
                                }
                                saveadmintak(SaveTask);
                            }
                        });
                    }
                }
                else {
                    saveadmintak(SaveTask);
                }

            } else {
                bootbox.alert(alertText);
                alertcount = 0;
                return false;
            }
        }

        function saveadmintak(SaveTaskObj) {
            AdminTaskService.InsertTaskWithAttachments(SaveTaskObj).then(function (SaveTaskResult) {
                if (SaveTaskResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
                    $scope.uploadingInProgress = false;
                } else {
                    if (SaveTaskResult.Response != null && SaveTaskResult.Response.m_Item1 != 0) {
                        var returnObj = SaveTaskResult.Response.m_Item2;
                        $scope.newTaskid = SaveTaskResult.Response.m_Item1;
                        $scope.returnObj = returnObj;
                        UpdateTemplateSortOrder();
                        UpdateTemplatePhaseSortOrder();
                        if ($("#assetlistActivityTaskadmin > div").length == 0) {
                            NotifySuccess($translate.instant('LanguageContents.Res_4820.Caption'));
                            RereshTaskObj(returnObj);
                            $scope.FileList = [];
                            $scope.SelectedTaskLIstID = 0;
                            $scope.AttachmentFilename = [];
                            refreshModel();
                            $scope.newTaskid = 0;
                            $scope.returnObj = [];
                            $('#addTask').modal('hide');
                        } else if ($("#assetlistActivityTaskadmin > div").length > 0) {
                            $("#admintempTaskCreation").click();
                            $scope.uploadingInProgress = true;
                        }
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
                        $scope.TaskSave = false;
                        $scope.TaskUpdate = false;
                    }
                }
            });
        }
        $scope.UpdateTask = function () {
            if ($("#assetlistActivityTaskadmin > div").length > 0) {
                $scope.uploadingInProgress = true;
            }
            $scope.addtaskvallid = 0;
            $scope.AttributeData = [];
            var alertText = "";
            if ($scope.TaskType == "" || $scope.TaskType == undefined || $scope.TaskType == 0) alertText += $translate.instant('LanguageContents.Res_4637.Caption');
            var Tasktype_value = $.grep($scope.TaskTypeList, function (rel) {
                return rel.Id == $scope.TaskType;
            })[0];
            if (Tasktype_value != undefined) if (Tasktype_value.TaskTypeId == "" || Tasktype_value.TaskTypeId == undefined) alertText += $translate.instant('LanguageContents.Res_4637.Caption');
            if ($scope.NewTaskName == "") alertText += $translate.instant('LanguageContents.Res_4587.Caption');
            for (var i = 0; i < $scope.AdminTaskCheckList.length; i++) {
                if ($scope.AdminTaskCheckList.length > 1) {
                    if ($scope.AdminTaskCheckList[i]["NAME"] == "" || $scope.AdminTaskCheckList[i]["NAME"] == null) {
                        alertText += $translate.instant('LanguageContents.Res_4588.Caption');
                    }
                }
            }
            if (alertText == "") {
                $scope.addtaskvallid = 1;
                var SaveTask = {};
                SaveTask.TaskType = Tasktype_value.TaskTypeId;
                SaveTask.Name = $scope.NewTaskName;
                SaveTask.TaskListID = $scope.SelectedTaskLIstID;
                SaveTask.Description = $scope.TaskDescription;
                SaveTask.TaskID = $scope.SelectedTaskID;
                SaveTask.AdminTaskCheckList = $scope.AdminTaskCheckList;
                SaveTask.AttributeData = $scope.AttributeData;
                AdminTaskService.UpdateAdminTask(SaveTask).then(function (SaveTaskResult) {
                    if (SaveTaskResult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
                        $scope.uploadingInProgress = false;
                    } else {
                        if (SaveTaskResult.Response != null && SaveTaskResult.Response.m_Item1 != 0) {
                            var returnObj = SaveTaskResult.Response.m_Item2;
                            $scope.newTaskid = $scope.SelectedTaskID;
                            $scope.returnObj = returnObj;
                            AdminTaskService.InsertUpdateAdminTaskTemplate($scope.templateData.PhaseData.AllPhaseList[0], SaveTaskResult.Response.m_Item2.Id).then(function () {
                                UpdateTemplateSortOrder();
                                UpdateTemplatePhaseSortOrder();
                            });
                            if ($("#assetlistActivityTaskadmin > div").length == 0) {
                                NotifySuccess($translate.instant('LanguageContents.Res_4820.Caption'));
                                RereshTaskObj(returnObj);
                                $scope.FileList = [];
                                $scope.AttributeData = [];
                                $scope.SelectedTaskLIstID = 0;
                                $scope.AttachmentFilename = [];
                                refreshModel();
                                $('#addTask').modal('hide');
                            } else if ($("#assetlistActivityTaskadmin > div").length > 0) {
                                $scope.uploadingInProgress = true;
                                $("#admintempTaskCreation").click();
                            }
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
                            $scope.uploadingInProgress = false;
                        }
                    }
                });
            } else {
                bootbox.alert(alertText);
                return false;
            }
        }

        function RereshTaskObj(taskObj) {
            var taskListResObj = $.grep($scope.TaskLibraryList, function (e) {
                return e.ID == taskObj.TaskListID;
            });
            if (taskListResObj.length > 0) {
                $timeout(function () {
                    GetEntityTaskAttachmentinfo(taskObj.Id);
                }, 100);
                if (taskListResObj[0].TaskList != null) {
                    var taskResObj = $.grep(taskListResObj[0].TaskList, function (e) {
                        return e.Id == taskObj.Id;
                    });
                    if (taskResObj.length > 0) {
                        taskResObj[0].Caption = taskObj.Caption;
                        taskResObj[0].Description = taskObj.Description;
                        taskResObj[0].TaskTypeName = taskObj.TaskTypeName;
                        taskResObj[0].TotalTaskAttachment = $scope.TaskBriefDetails.Totalassetcount;
                        taskResObj[0].SortOder = taskObj.SortOder;
                        taskResObj[0].SortOder = taskObj.SortOder;
                        taskResObj[0].TaskType = taskObj.TaskType;
                        taskResObj[0].AdminTaskCheckList = taskObj.AdminTaskCheckList;
                    } else {
                        taskListResObj[0].TaskList.push({
                            "Id": taskObj.Id,
                            "TaskListID": taskObj.TaskListID,
                            "Caption": taskObj.Caption,
                            "Description": taskObj.Description,
                            "TaskType": taskObj.TaskType,
                            "TaskTypeName": taskObj.TaskTypeName,
                            "TotalTaskAttachment": $scope.TaskBriefDetails.Totalassetcount,
                            "SortOder": taskObj.SortOder,
                            "AttributeData": taskObj.AttributeData,
                            "AdminTaskCheckList": taskObj.AdminTaskCheckList
                        });
                    }
                } else {
                    taskListResObj[0].TaskList.push({
                        "Id": taskObj.Id,
                        "TaskListID": taskObj.TaskListID,
                        "Caption": taskObj.Caption,
                        "Description": taskObj.Description,
                        "TaskType": taskObj.TaskType,
                        "TaskTypeName": taskObj.TaskTypeName,
                        "TotalTaskAttachment": $scope.TaskBriefDetails.Totalassetcount,
                        "SortOder": taskObj.SortOder,
                        "AttributeData": taskObj.AttributeData,
                        "AdminTaskCheckList": taskObj.AdminTaskCheckList
                    });
                }
                taskListResObj[0].TaskCount = taskListResObj[0].TaskList.length;
            }
        }
        $scope.DeleteTaskList = function (tasklistId) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2013.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        AdminTaskService.DeleteSystemTaskList(tasklistId).then(function (result) {
                            if (result != null && result.Response != false) {
                                DeleteRefreshTaskData(tasklistId);
                                NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                            }
                        });
                    }, 100);
                }
            });
        }

        function DeleteRefreshTaskData(TaskListId) {
            var taskListResObj = $.grep($scope.TaskLibraryList, function (e) {
                return e.ID == TaskListId;
            });
            $scope.TaskLibraryList.splice($.inArray(taskListResObj[0], $scope.TaskLibraryList), 1);
        }
        $scope.AddAttachments = function () {
            if ($scope.SelectedTaskID > 0) $scope.globalAttachment = false;
            else $scope.globalAttachment = true;
            $scope.fnTimeOut();
        }
        $scope.AddLinks = function () {
            $scope.addtxtLinkName = "";
            $scope.addtxtLinkURL = "";
            $scope.addtxtLinkDesc = "";
            $scope.addLinkType = 1;
            $("#addLinkPopup").modal("show");
        }
        $scope.DeleteAdminTask = function (taskListID, taskID) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2014.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        AdminTaskService.DeleteAdminTask(taskID).then(function (delresult) {
                            if (delresult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4309.Caption'));
                            } else {
                                var taskListResObj = $.grep($scope.TaskLibraryList, function (e) {
                                    return e.ID == taskListID;
                                });
                                if (taskListResObj[0].TaskList != null) {
                                    var taskResObj = $.grep(taskListResObj[0].TaskList, function (e) {
                                        return e.Id == taskID;
                                    });
                                    if (taskResObj.length > 0) {
                                        taskListResObj[0].TaskList.splice($.inArray(taskResObj[0], taskListResObj[0].TaskList), 1);
                                        taskListResObj[0].TaskCount = taskListResObj[0].TaskList.length;
                                    }
                                }
                                NotifySuccess($translate.instant('LanguageContents.Res_4821.Caption'));
                            }
                        });
                    }, 100);
                }
            });
        }
        $scope.fileSaveObject = {
            "FileFriendlyName": "",
            "FileDescription": "",
            "FileID": 0
        };
        $scope.linkSaveObject = {
            "LinkFriendlyName": "",
            "linkDescription": "",
            "LinkID": 0
        };
        $scope.EditAttachment = function (fileobj) {
            if (fileobj.ID > 0) {
                $scope.ChangeFileFriendlyName(fileobj);
            }
        }
        $scope.ChangeFileFriendlyName = function (fileobj) {
            if (fileobj.Extension != "Link") {
                $('#EditAttachmentFileHolder tbody tr').remove();
                var htmlStr = '';
                htmlStr += ' <tr>';
                htmlStr += '  <td>';
                htmlStr += '     <a target="blank" ng-href="download.aspx?FileID=' + fileobj.FileGuid + '&amp;FileFriendlyName=' + fileobj.FileName + '&amp;Ext=' + fileobj.Extension + '">' + fileobj.FileName + '</a>';
                htmlStr += ' </td>';
                htmlStr += ' <td>';
                htmlStr += '    <input type="text" id="friendlyName_' + fileobj.ID + '" ng-model="fileSaveObject.FileFriendlyName"  placeholder="Friendly Name">';
                htmlStr += ' </td>';
                htmlStr += ' <td>';
                htmlStr += '      <textarea placeholder="Description" id="FileDescriptionVal_' + fileobj.ID + '" ng-model="fileSaveObject.FileDescription" placeholder="File description" rows="3"></textarea>';
                htmlStr += ' </td>';
                var trimFileName = fileobj.FileName.substr(0, fileobj.FileName.lastIndexOf('.')) || fileobj.FileName;
                $scope.fileSaveObject.FileFriendlyName = trimFileName;
                $scope.fileSaveObject.FileDescription = fileobj.FileDescription;
                $scope.fileSaveObject.FileID = fileobj.ID;
                $("#EditAttachmentFileHolder tbody").append($compile(htmlStr)($scope));
                $("#EditAttachementPopup").modal("show");
            } else {
                var url = fileobj.URL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '');
                $scope.txtLinkName = fileobj.FileName;
                $scope.txtLinkURL = url;
                $scope.linkType = fileobj.LinkType;
                $scope.txtLinkDesc = fileobj.FileDescription;
                $scope.linkSaveObject.LinkID = fileobj.ID;
                $("#EditLinkPopup").modal("show");
            }
        }
        $scope.SaveLinkObject = function () {
            var AttachLink = {};
            AttachLink.LinkName = $scope.txtLinkName;
            AttachLink.URL = $scope.txtLinkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '');;
            AttachLink.LinkType = $scope.linkType;
            AttachLink.Description = "";
            AttachLink.LinkID = $scope.linkSaveObject.LinkID;
            AdminTaskService.UpdatetaskAdminLinkDescription(AttachLink).then(function (AttachResponse) {
                if (AttachResponse.Response != false) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4806.Caption'));
                    $scope.txtLinkName = "";
                    $scope.txtLinkURL = "";
                    $scope.txtLinkDesc = "";
                    $scope.linkType = 1;
                    $("#EditLinkPopup").modal("hide");
                    $scope.linkSaveObject = {
                        "LinkFriendlyName": "",
                        "linkDescription": "",
                        "LinkID": 0
                    };
                    var taskListResObj = $.grep($scope.TaskLibraryList, function (e) {
                        return e.ID == $scope.SelectedTaskLIstID;
                    });
                    var taskResObj = $.grep(taskListResObj[0].TaskList, function (e) {
                        return e.Id == $scope.SelectedTaskID;
                    });
                    if (taskResObj.length > 0) {
                        taskResObj[0].TotalTaskAttachment = parseInt(taskResObj[0].TotalTaskAttachment) + 1;
                    }
                    ReloadTaskAttachments($scope.SelectedTaskID);
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                }
            });
        }
        $scope.SaveFileObject = function () {
            var TaskStatusData = {};
            TaskStatusData.FileName = $scope.fileSaveObject.FileFriendlyName;
            TaskStatusData.Description = $scope.fileSaveObject.FileDescription;
            TaskStatusData.FileID = $scope.fileSaveObject.FileID;
            AdminTaskService.UpdatetaskAdminAttachmentDescription(TaskStatusData).then(function (TaskStatusResult) {
                if (TaskStatusResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                } else {
                    if (TaskStatusResult.Response == false) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        $("#EditAttachementPopup").modal("hide");
                    } else {
                        $("#EditAttachementPopup").modal("hide");
                        NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                        $scope.fileSaveObject = {
                            "FileFriendlyName": "",
                            "FileDescription": "",
                            "FileID": 0
                        };
                        ReloadTaskAttachments($scope.SelectedTaskID);
                    }
                }
            });
        }
        $scope.fields = {
            usersID: ''
        };
        $scope.assetfields = {
            usersID: ''
        };
        $scope.Dropdown = {};
        $scope.ShowHideAttributeOnRelation = {};
        $scope.EnableDisableControlsHolder = {};
        $scope.fromAssignedpopup = true;
        $scope.treePreviewObj = {};
        $scope.treeNodeSelectedHolder = [];
        $scope.treesrcdirec = {};
        $scope.staticTreesrcdirec = {};
        $scope.TreeEmptyAttributeObj = {};
        $scope.atributesRelationList = {};
        $scope.optionsLists = [];
        $scope.EnableDisableControlsHolder = {};
        $scope.Inherritingtreelevels = {};
        $scope.InheritingLevelsitems = [];
        $scope.treeTexts = {};
        $scope.treeSources = {};
        $scope.treeSourcesObj = [];
        $scope.UploadAttributeData = [];
        $scope.OptionObj = {};
        $scope.PercentageVisibleSettings = {};
        $scope.DropDownTreePricing = {};
        $scope.listAttributeValidationResult = [];
        $scope.listAttriToAttriResult = [];
        $scope.items = [];
        $scope.treelevels = {};
        $scope.NormalDropdownCaption = {};
        $scope.uploader = {};
        $scope.UploaderCaption = {};
        $scope.NormalMultiDropdownCaption = {};
        $scope.treeSelection = [];
        $scope.normaltreeSources = {};
        $scope.treeTextsObj = [];
        $scope.settreeTexts = function () {
            var keys2 = [];
            angular.forEach($scope.treeTexts, function (key) {
                keys2.push(key);
                $scope.treeTextsObj = keys2;
            });
        }
        $scope.treeNodeSelectedHolder = new Array();
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            var _ref;
            $scope.output = "You selected: " + branch.Caption;
            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
                for (var i = 0, parent; parent = parentArr[i++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == parent.AttributeId && e.id == parent.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(parent);
                    }
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                    if (branch.Children.length > 0) {
                        RemoveRecursiveChildTreenode(branch.Children);
                    }
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
        };
        $scope.renderHtml = function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };
        var treeTextVisbileflag = false;

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == child.AttributeId && e.id == child.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }
            }
        }
        $scope.treesrcdirec = {};
        $scope.my_tree = tree = {};
        $scope.ChecklistVisible = false;
        $scope.ClearModelObject = function (ModelObject) {
            for (var variable in ModelObject) {
                if (typeof ModelObject[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        ModelObject[variable] = "";
                    }
                } else if (typeof ModelObject[variable] === "number") {
                    ModelObject[variable] = null;
                } else if (Array.isArray(ModelObject[variable])) {
                    ModelObject[variable] = [];
                } else if (typeof ModelObject[variable] === "object") {
                    ModelObject[variable] = {};
                }
            }
        }
        $scope.items = [];
        $scope.lastSubmit = [];
        $scope.addNew = function () {
            var ItemCnt = $scope.items.length;
            if (ItemCnt > 0) {
                if ($scope.items[ItemCnt - 1].startDate == null || $scope.items[ItemCnt - 1].startDate.length == 0 || $scope.items[ItemCnt - 1].endDate.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1986.Caption'));
                    return false;
                }
                $scope.items.push({
                    startDate: [],
                    endDate: [],
                    comment: '',
                    sortorder: 0
                });
            }
        };
        $scope.createAdmindynamicControls = function (rootID) {
            var Tasktype_result = $.grep($scope.TaskTypeList, function (rel) {
                return rel.Id == $scope.TaskType;
            })[0];
            if (Tasktype_result != undefined) {
                if (rootID != "" && rootID != undefined) {
                    var Tasktype_value = $.grep($scope.TaskTypeList, function (rel) {
                        return rel.Id == $scope.TaskType;
                    })[0];
                    if (Tasktype_value.TaskTypeId == 2) {
                        $scope.rytSideApprovalButtonPane = false;
                        $scope.ChecklistVisible = true;
                        $scope.AdminTaskCheckList = [{
                            ID: 0,
                            NAME: ""
                        }];
                    } else if (Tasktype_value.TaskTypeId == 3) {
                        $scope.ChecklistVisible = false;
                        $scope.rytSideApprovalButtonPane = true;
                    } else {
                        $scope.ChecklistVisible = false;
                        $scope.rytSideApprovalButtonPane = false;
                        $scope.AdminTaskCheckList = [{
                            ID: 0,
                            NAME: ""
                        }];
                    }
                } else {
                    $scope.AdminTaskCheckList = [{
                        ID: 0,
                        NAME: ""
                    }];
                    $scope.ChecklistVisible = false;
                    $scope.rytSideApprovalButtonPane = false;
                }
                $("#adminTaskDynamic-metadata").unbind();
                $("#adminTaskDynamic-metadata").empty();
                $("#adminTaskDynamic-metadata").html($compile("")($scope));
                $("#admintaskDynamicTaskControls").html("");
                $("#admintaskDynamicTaskControls").append($compile("")($scope));
                $scope.treeNodeSelectedHolder = [];
                $scope.treesrcdirec = {};
                $scope.treePreviewObj = {};
                $scope.PercentageVisibleSettings = {};
                $scope.DropDownTreePricing = {};
                $scope.ClearModelObject($scope.fields);
                $scope.items = [];
                $scope.dyn_Cont = '';
                AdminTaskService.GetEntityTypeAttributeRelationWithLevelsByID(rootID, 0).then(function (entityAttributesRelation) {
                    $scope.atributesRelationList = entityAttributesRelation.Response;
                    for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                        if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                            if ($scope.atributesRelationList[i].AttributeID != 70) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                if ($scope.atributesRelationList[i].InheritFromParent) $scope.atributesRelationList[i].DefaultValue = $scope.atributesRelationList[i].ParentValue[0];
                                if ($scope.atributesRelationList[i].AttributeID !== SystemDefiendAttributes.Name) {
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                                    $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            if ($scope.atributesRelationList[i].IsSpecial == true) {
                                if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                    $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"> <input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.OwnerList[0].UserName;
                                }
                            } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.FiscalYear) {
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,3)\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                if ($scope.atributesRelationList[i].InheritFromParent) $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                                else $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.EntityStatus) { } else {
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,3)\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                if ($scope.atributesRelationList[i].InheritFromParent) $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                                else $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                            var totLevelCnt = $scope.atributesRelationList[i].Levels.length;
                            for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                    return item.Caption
                                };
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                    return item.Caption
                                };
                                if (j == 0) {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                    $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                } else {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                    $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                            if (($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TakeDescription) && ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskNotes)) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea></div></div>";
                                if ($scope.atributesRelationList[i].InheritFromParent) $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                                else $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select  class=\"multiselect\"   data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\"  multiselect-dropdown ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,4)\"   ></select></div></div>";
                            if ($scope.atributesRelationList[i].InheritFromParent) {
                                var defaultmultiselectvalue1 = $scope.atributesRelationList[i].ParentValue.split(',');
                                $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = defaultmultiselectvalue1;
                            } else {
                                var defaultmultiselectvalue = $scope.atributesRelationList[i].DefaultValue.split(',');
                                if ($scope.atributesRelationList[i].DefaultValue != "") {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = defaultmultiselectvalue;
                                } else {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                            $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                            if ($scope.MinValue < 0) {
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                            } else {
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                            }
                            if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                            } else {
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                            }
                            var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                            $scope.items.push({
                                startDate: [],
                                endDate: [],
                                comment: '',
                                sortorder: 0
                            });
                            $scope.dyn_Cont += "    <div class=\"control-group\"><label for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.atributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                            $scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span5\">";
                            //$scope.dyn_Cont += "<input class=\"sdate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"item.startDate\" type=\"text\" name=\"startDate\"   ng-model=\"item.startDate\" ng-click=\"Calanderopen($event,'startDate')\"  datepicker-popup=\"{{format}}\"  is-open=\"startDate\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\" close-text=\"Close\" placeholder=\"-- Start date --\"/><input class=\"edate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-click=\"Calanderopen($event,'endDate')\"  datepicker-popup=\"{{format}}\"  is-open=\"endDate\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" name=\"enddate\" id=\"item.endDate\" ng-model=\"item.endDate\" placeholder=\"-- End date --\"/><input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
                            $scope.dyn_Cont += "<input class=\"sdate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"PeriodCalanderopen($event,item,'start')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calstartopen\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-change=\"changeperioddate_changed(item.startDate,'StartDate')\" ng-model=\"item.startDate\" placeholder=\"-- Start date --\"/><input class=\"edate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-click=\"PeriodCalanderopen($event,item,'end')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calendopen\"  min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  name=\"enddate\" id=\"item.endDate\" ng-change=\"changeperioddate_changed(item.endDate,'EndDate')\" ng-model=\"item.endDate\" placeholder=\"-- End date --\"/><input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
                            $scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
                            $scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
                            $scope.fields["Period_" + $scope.atributesRelationList[i].AttributeID] = "";
                            $scope.dyn_Cont += '</div>';
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                            $scope.fields["Tree_" + $scope.atributesRelationList[i].AttributeID] = [];
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                            if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                treeTextVisbileflag = false;
                                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID])) {
                                    $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = true;
                                } else $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                            } else {
                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                            }
                            $scope.dyn_Cont += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + $scope.atributesRelationList[i].AttributeID + '\" class="control-group treeNode-control-group">';
                            $scope.dyn_Cont += '<label class="control-label">' + $scope.atributesRelationList[i].AttributeCaption + '</label>';
                            $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                            $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '"></div>';
                            $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '">';
                            $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                            $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" accessable="' + $scope.atributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                            $scope.dyn_Cont += '</div></div></div>';
                            $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationList[i].AttributeID + '\">';
                            $scope.dyn_Cont += '<div class="controls">';
                            $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.atributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                            $scope.dyn_Cont += '</div></div>';
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.MTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"MTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                            $scope.fields["MTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                            $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = true;
                            $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = $scope.atributesRelationList[i].DropDownPricing;
                            $scope.dyn_Cont += "<div drowdowntreepercentagemultiselection data-purpose='entity' data-attributeid=" + $scope.atributesRelationList[i].AttributeID.toString() + "></div>";
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                            if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskDueDate) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = false;
                                $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                } else {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                } else {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                                }
                                var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.DateTime_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"text\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event,'calanderopened" + $scope.atributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"calanderopened" + $scope.atributesRelationList[i].AttributeID + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                var param1 = new Date();
                                var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date();
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                                $scope.fields["fields.DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.CurrencyFormatsList;
                            $scope['origninalamountvalue_' + $scope.atributesRelationList[i].AttributeID] = '0';
                            $scope['currRate_' + $scope.atributesRelationList[i].AttributeID] = 1;
                            $scope.setoptions();
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls currenycontrol\"><input type=\"text\"  ng-change=\"Getamountentered(" + $scope.atributesRelationList[i].AttributeID + ")\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  class=\"currencySelector\" id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"GetCostCentreCurrencyRateById(" + $scope.atributesRelationList[i].AttributeID + ")\"><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.ShortName}}</option></select></div>";
                            $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = '0';
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.DefaultSettings.CurrencyFormat.Id;
                            $scope.setFieldKeys();
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                            var totLevelCnt1 = $scope.atributesRelationList[i].Levels.length;
                            for (var j = 0; j < totLevelCnt1; j++) {
                                if (totLevelCnt1 == 1) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                        return item.Caption
                                    };
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                        return item.Caption
                                    };
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                    $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                        return item.Caption
                                    };
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                        return item.Caption
                                    };
                                    if (j == 0) {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                        $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                    } else {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                        if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                            $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                        } else {
                                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                            $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                        }
                                    }
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {

                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            $scope.setoptions();
                            $scope.dyn_Cont += '<div class="control-group"><label class="control-label"';
                            $scope.dyn_Cont += 'for="fields.Uploader_ ' + $scope.atributesRelationList[i].AttributeID + '">' + $scope.atributesRelationList[i].AttributeCaption + ': </label>';
                            $scope.dyn_Cont += '<div id="Uploader" class="controls">';
                            //$scope.dyn_Cont += '<img id="UploaderPreview_' + $scope.atributesRelationList[i].AttributeID + '" class="ng-pristine ng-valid entityImgPreview"';
                            //$scope.dyn_Cont += ' ng-model="fields.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" id="UploaderImageControl" src="" alt="No thumbnail present">';
                            if ($scope.atributesRelationList[i].Value == "" || $scope.atributesRelationList[i].Value == null && $scope.atributesRelationList[i].Value == undefined) {
                                $scope.atributesRelationList[i].Value = "NoThumpnail.jpg";
                            }
                            $scope.dyn_Cont += '<img src="' + imagesrcpath + 'UploadedImages/' + $scope.atributesRelationList[i].Value + '" alt="' + $scope.atributesRelationList[i].Caption + '" id="UploaderPreview_' + $scope.atributesRelationList[i].AttributeID + '"';
                            $scope.dyn_Cont += 'ng-model="fields.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" id="UploaderImageControl" class="ng-pristine ng-valid entityDetailImgPreview">';
                            $scope.dyn_Cont += '<br><a ng-show="EnableDisableControlsHolder.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" ng-model="UploadImage" ng-click="UploadImagefileTaskCreate(' + $scope.atributesRelationList[i].AttributeID + ',true)" class="ng-pristine ng-valid">Select Image</a>';
                            $scope.dyn_Cont += '</div></div>';
                            $scope.fields["Uploader_" + $scope.atributesRelationList[i].AttributeID] = "";
                            $scope.setFieldKeys();
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                            $scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.OptionObj["tagoption_" + $scope.atributesRelationList[i].AttributeID] = [];
                            $scope.setoptions();
                            $scope.tempscope = [];
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\">";
                            $scope.dyn_Cont += "<label class=\"control-label\" for=\"fields.ListTagwords_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label>";
                            if ($scope.atributesRelationList[i].IsReadOnly == true) $scope.dyn_Cont += "<div class=\"controls read-only-disabled\">";
                            else $scope.dyn_Cont += "<div class=\"controls\">";
                            $scope.dyn_Cont += "<directive-tagwords item-attrid = \"" + $scope.atributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + $scope.atributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                            $scope.dyn_Cont += "</div></div>";
                            if ($scope.atributesRelationList[i].InheritFromParent) { } else { }
                            $scope.setFieldKeys();
                        }
                        if ($scope.atributesRelationList[i].IsReadOnly == true) {
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        } else {
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        }
                    }
                    $scope.dyn_Cont += '<input style="display:none" type="submit" id="admintskbtnTemp" class="ng-scope" invisible>';
                    $("#admintaskDynamicTaskControls").append($compile($scope.dyn_Cont)($scope));
                    var mar = ($scope.DecimalSettings.FinancialAutoNumeric.vMax).substr(($scope.DecimalSettings.FinancialAutoNumeric.vMax).indexOf(".") + 1);
                    $scope.mindec = "";
                    if (mar.length == 1) {
                        $("[id^='dTextSingleLine_']").autoNumeric('init', {
                            aSep: ' ',
                            vMin: "0",
                            mDec: "1"
                        });
                    }
                    if (mar.length != 1) {
                        if (mar.length < 5) {
                            $scope.mindec = "0.";
                            for (i = 0; i < mar.length; i++) {
                                $scope.mindec = $scope.mindec + "0";
                            }
                            $("[id^='dTextSingleLine_']").autoNumeric('init', {
                                aSep: ' ',
                                vMin: $scope.mindec
                            });
                        } else {
                            $("[id^='dTextSingleLine_']").autoNumeric('init', {
                                aSep: ' ',
                                vMin: "0",
                                mDec: "0"
                            });
                        }
                    }
                    $("[id^='MTextSingleLine_']").autoNumeric('init', {
                        aSep: ' ',
                        vMin: "0",
                        mDec: "0"
                    });
                    $("[id^='MTextSingleLine_']").keydown(function (event) {
                        if (event.which != 8 && isNaN(String.fromCharCode(event.which)) && (event.keyCode < 96 || event.keyCode > 105) && event.which != 46) {
                            bootbox.alert($translate.instant($translate.instant('LanguageContents.Res_5735.Caption')));
                            event.preventDefault();
                        }
                    });
                    $("#adminTaskDynamicMetadata").scrollTop(0);
                    $scope.rootDisplayName = $scope.fields["TextSingleLine_" + SystemDefiendAttributes.Name];
                    $timeout(function () {
                        HideAttributeToAttributeRelationsOnPageLoad();
                    }, 200);
                    GetValidationList(rootID);
                    $("#adminTaskDynamicMetadata").addClass('notvalidate');

                });
            }
        }

        $timeout(function () {
            StrartUpload_UploaderAttrAdmin();
        }, 100);
        $scope.UploadImagefileTaskCreate = function (attributeId, istaskCreate) {
            $scope.istaskCreate = istaskCreate;
            $scope.UploadAttributeId = attributeId;
            $("#pickfilesUploaderAttrTaskCreate").click();
        }


        $scope.ImageFileName = '';

        function UpdateImagescope(file, response) {

            var resultArr = response.split(",");
            $scope.ImageFileName = resultArr[0] + resultArr[2];
            var PreviewID = "UploaderPreview_" + $scope.UploadAttributeId;
            $('#' + PreviewID).attr('src', imagesrcpath + 'UploadedImages/Temp/' + $scope.ImageFileName);

        }

        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToShow = [];
            if (attrLevel > 0) {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            } else {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }
            if (attributesToShow[0] != undefined) {
                for (var i = 0; i < attributesToShow[0].length; i++) {
                    var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToShow = [];
                var hideAttributeOtherThanSelected = [];
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        } else if (attrType == 12) {
                            if (j == levelcnt) $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }
                    if (attrType == 6) {
                        if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    } else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    } catch (e) { }
                }
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);
                if (attrType == 3) {
                    if ($scope.fields['ListSingleSelection_' + attrID] != null) {
                        optionValue = parseInt(($scope.fields['ListSingleSelection_' + attrID]), 10);
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    if ($scope.fields['ListMultiSelection_' + attrID] != null) {
                        optionValue = $scope.fields['ListMultiSelection_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                } else if (attrType == 7) {
                    if ($scope.fields['Tree_' + attrID] != null) {
                        optionValue = $scope.fields['Tree_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }

        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }
        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad = function (attrID, attributeLevel, attrVal, attrType) {
            try {
                var optionValue = attrVal;
                var attributesToShow = [];
                if (attrType == 3) {
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 7) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6 || attrType == 12) {
                    if (attrVal != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrVal != null) ? parseInt(attrVal, 10) : 0) && e.AttributeLevel == ((attributeLevel != null) ? parseInt(attributeLevel, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }

        function GetValidationList(rootID) {
            AdminTaskService.GetValidationDationByEntitytype(rootID).then(function (GetValidationresult) {
                if (GetValidationresult.Response != null) {
                    $scope.listValidationResult = GetValidationresult.Response;
                    if ($scope.listAttriToAttriResult != null) {
                        for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                            var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                            if (attrRelIDs != undefined) {
                                for (var j = 0; j < attrRelIDs.length; j++) {
                                    if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                        });
                                    } else {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                        });
                                    }
                                }
                            }
                        }
                        $("#adminTaskDynamicMetadata").nod($scope.listValidationResult, {
                            'delay': 200,
                            'submitBtnSelector': '#admintskbtnTemp',
                            'silentSubmit': 'true'
                        });
                        if ($scope.listValidationResult.length != 0) {
                            for (var i = 0 ; i < $scope.listValidationResult.length; i++) {
                                if ($scope.listValidationResult[i][1] == "presence") {
                                    $($scope.listValidationResult[i][0]).parent().addClass('relative');
                                    $("<i class=\"icon-asterisk validationmark margin-right5x\"></i>").insertAfter($scope.listValidationResult[i][0]);
                                }
                            }
                        }
                    }
                }
            });
        }
        var treeTextVisbileflag = false;

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        function GetTreeCheckedNodes(treeobj, attrID) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    $scope.fields["Tree_" + attrID].push(node.id);
                }
                if (node.Children.length > 0) GetTreeCheckedNodes(node.Children, attrID);
            }
        }
        $scope.treeNodeSelectedHolder = [];
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            if ($scope.treeNodeSelectedHolder.length > 0) {
                for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
                    $scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
                }
            }
            $scope.ShowHideAttributeToAttributeRelations(branch.AttributeId, 0, 0, 7);
        };
        $scope.AddDefaultEndDate = function (enddate, startdate, currentindex) {
            if (currentindex != undefined) {
                var enddate1 = null;
                if (currentindex != 0) {
                    enddate1 = $scope.items[currentindex - 1].endDate;
                }
                if (enddate1 != null && enddate1 >= startdate) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1987.Caption'));
                    $scope.items[currentindex].startDate = null;
                    $scope.items[currentindex].endDate = null;
                } else {
                    if (startdate == null) {
                        $scope.items[currentindex].endDate = null;
                    } else {
                        $scope.items[currentindex].endDate = (new Date.create(startdate)).addDays(7);
                    }
                }
            } else {
                if (enddate.toUpperCase().contains("STARTDATE")) {
                    var temp = startdate;
                    startdate = enddate;
                    enddate = temp;
                }
                var objsetenddate = new Date.create($scope.fields[startdate]);
                $scope.fields[enddate] = objsetenddate.addDays(7);
            }
        };

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == child.AttributeId && e.id == child.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }
            }
        }
        $scope.treesrcdirec = {};
        $scope.my_tree = tree = {};
        $scope.LoadDropDownChildLevels = function (attrID, attributeLevel, levelcnt, attrType) {
            if (levelcnt > 0) {
                var currntlevel = attributeLevel + 1;
                for (var j = currntlevel; j <= levelcnt; j++) {
                    $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                    if (attrType == 6) {
                        $scope.fields["DropDown_" + attrID + "_" + j] = "";
                    } else if (attrType == 12) {
                        if (j == levelcnt) $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                        else $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                    }
                }
                if (attrType == 6) {
                    if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                            $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        });
                    }
                } else if (attrType == 12) {
                    if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                            $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        });
                    }
                }
            }
        }

        function LoadTaskMetadata(EntAttrDet, ID, tasktype, fromassignedplace) {
            $scope.StopeUpdateStatusonPageLoad = false;
            $scope.detailsLoader = false;
            $scope.detailsData = true;
            $scope.dyn_Cont = '';
            $scope.dyn_Cont = "";
            $scope.ClearModelObject($scope.fields);
            $scope.attributedata = EntAttrDet;
            $scope.items = [];
            if (tasktype == 2) {
                $scope.processingsrcobj.processingid = ID;
                $scope.processingsrcobj.processingplace = "task";
                $timeout(function () {
                    $scope.AssetfileTemplateLoading = "task";
                    $scope.$broadcast('ReloadAssetView');
                }, 100);
            } else if (tasktype == 3) {
                $scope.processingsrcobj.processingid = ID;
                $scope.processingsrcobj.processingplace = "task";
                $timeout(function () {
                    $scope.AssetfileTemplateLoading = "task";
                    $scope.$broadcast('ReloadAssetView');
                }, 100);
            } else if (tasktype == 31) {
                $scope.processingsrcobj.processingid = ID;
                $scope.processingsrcobj.processingplace = "task";
                $timeout(function () {
                    $scope.AssetfileTemplateLoading = "task";
                    $scope.$broadcast('ReloadAssetView');
                }, 100);
            }
            for (var i = 0; i < $scope.attributedata.length; i++) {
                if ($scope.attributedata[i].TypeID == 6) {
                    $scope.dyn_Cont2 = '';
                    var CaptionObj = $scope.attributedata[i].Caption.split(",");
                    for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                        if (j == 0) {
                            if (CaptionObj[j] != undefined) {
                                $scope.items.push({
                                    caption: $scope.attributedata[i].Lable[j].Label,
                                    level: j + 1
                                });
                                $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                $scope.settreeTexts();
                                $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                            } else {
                                $scope.items.push({
                                    caption: $scope.attributedata[i].Lable[j].Label,
                                    level: j + 1
                                });
                                $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                $scope.settreeTexts();
                                $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                            }
                        } else {
                            if (CaptionObj[j] != undefined) {
                                $scope.items.push({
                                    caption: $scope.attributedata[i].Lable[j].Label,
                                    level: j + 1
                                });
                                $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                $scope.settreeTexts();
                                $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                            } else {
                                $scope.items.push({
                                    caption: $scope.attributedata[i].Lable[j].Label,
                                    level: j + 1
                                });
                                $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                $scope.settreeTexts();
                                $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                            }
                        }
                    }
                    $scope.treelevels["dropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                    $scope.items = [];
                    for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                        var inlineEditabletitile = $scope.treelevels['dropdown_levels_' + $scope.attributedata[i].ID][j].caption;
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
                        } else {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditableoptimizedtreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.DropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                        }
                    }
                } else if ($scope.attributedata[i].TypeID == 12) {
                    $scope.dyn_Cont2 = '';
                    var CaptionObj = $scope.attributedata[i].Caption;
                    for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                        if ($scope.attributedata[i].Lable.length == 1) {
                            var k = j;
                            var treeTexts = [];
                            var fields = [];
                            $scope.items.push({
                                caption: $scope.attributedata[i].Lable[j].Label,
                                level: j + 1
                            });
                            if (k == CaptionObj.length) {
                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                            } else {
                                if (CaptionObj[k] != undefined) {
                                    for (k; k < CaptionObj.length; k++) {
                                        treeTexts.push(CaptionObj[k]);
                                        $scope.settreeTexts();
                                        fields.push(CaptionObj[k]);
                                        $scope.setFieldKeys();
                                    }
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                } else {
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                }
                            }
                        } else {
                            if (j == 0) {
                                if (CaptionObj[j] != undefined) {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.settreeTexts();
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.setFieldKeys();
                                } else {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.settreeTexts();
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.setFieldKeys();
                                }
                            } else {
                                var k = j;
                                if (j == ($scope.attributedata[i].Lable.length - 1)) {
                                    var treeTexts = [];
                                    var fields = [];
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    if (k == CaptionObj.length) {
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    } else {
                                        if (CaptionObj[k] != undefined) {
                                            for (k; k < CaptionObj.length; k++) {
                                                treeTexts.push(CaptionObj[k]);
                                                $scope.settreeTexts();
                                                fields.push(CaptionObj[k]);
                                                $scope.setFieldKeys();
                                            }
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                        } else {
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        }
                                    }
                                } else {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.settreeTexts();
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.setFieldKeys();
                                    } else {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.setFieldKeys();
                                    }
                                }
                            }
                        }
                    }
                    $scope.treelevels["multiselectdropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                    $scope.items = [];
                    for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                        var inlineEditabletitile = $scope.treelevels['multiselectdropdown_levels_' + $scope.attributedata[i].ID][j].caption;
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
                        } else {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditablemultiselecttreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                        }
                    }
                } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                    if ($scope.attributedata[i].Caption != undefined) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                    }
                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                    } else {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext   href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"   data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                    }
                } else if ($scope.attributedata[i].TypeID == 2) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                    if ($scope.attributedata[i].Caption != undefined) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                    }
                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                    } else {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\"  attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"MultiLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\" data-type="' + $scope.attributedata[i].ID + '" data-original-title=\"' + $scope.attributedata[i].Lable + '\">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                    }
                } else if ($scope.attributedata[i].TypeID == 11) {
                    loadUploaderAttr = true;
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    $scope.fields["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                    $scope.UploaderCaption["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                    $scope.setUploaderCaption();
                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group ng-scope\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable.toString() + '</label>';
                    $scope.dyn_Cont += '<div class=\"controls\">';
                    if ($scope.attributedata[i].Caption == "" || $scope.attributedata[i].Caption == null && $scope.attributedata[i].Caption == undefined) {
                        $scope.attributedata[i].Caption = $scope.attributedata[i].Lable;
                    }
                    if ($scope.attributedata[i].Value == "" || $scope.attributedata[i].Value == null && $scope.attributedata[i].Value == undefined) {
                        $scope.attributedata[i].Value = "NoThumpnail.jpg";
                    }
                    $scope.dyn_Cont += '<img src="' + imagesrcpath + 'UploadedImages/' + $scope.attributedata[i].Value + '" alt="' + $scope.attributedata[i].Caption + '"';
                    $scope.dyn_Cont += 'class="entityDetailImgPreview" id="UploaderPreview1_' + $scope.attributedata[i].ID + '">';
                    $scope.dyn_Cont += "<a class='margin-left10x' ng-click='UploadImagefileTaskCreate(" + $scope.attributedata[i].ID + ",false)' attributeTypeID='" + $scope.attributedata[i].TypeID + "'";
                    $scope.dyn_Cont += 'entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="Uploader_' + $scope.attributedata[i].ID + '"';
                    $scope.dyn_Cont += 'my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                    $scope.dyn_Cont += ' class="ng-pristine ng-valid" attributename=\"' + $scope.UploaderCaption["UploaderCaption_" + $scope.attributedata[i].ID] + '\">Select Image';
                    $scope.dyn_Cont += '</a></div></div>';
                } else if ($scope.attributedata[i].TypeID == 3) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                            $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            } else {
                                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            }
                        } else {
                            $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                $scope.dyn_Cont += '<div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span>';
                                $scope.dyn_Cont += '</div></div>';
                            } else {
                                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                $scope.dyn_Cont += '<div class="controls"><a  xeditabledropdown href=\"javascript:;\"';
                                $scope.dyn_Cont += 'attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '"';
                                $scope.dyn_Cont += 'attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"';
                                $scope.dyn_Cont += 'data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                $scope.dyn_Cont += 'attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\"';
                                $scope.dyn_Cont += 'data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a>';
                                $scope.dyn_Cont += '</div></div>';
                            }
                        }
                    } else {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption[0].length > 1) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                } else {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                }
                            }
                        } else {
                            $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            } else {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            }
                        }
                    }
                } else if ($scope.attributedata[i].TypeID == 4) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    if ($scope.attributedata[i].Caption[0] != undefined) {
                        if ($scope.attributedata[i].Caption.length > 1) {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            } else {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            }
                        }
                    } else {
                        $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                        $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        } else {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                        }


                    }
                } else if ($scope.attributedata[i].TypeID == 10) {
                    var inlineEditabletitile = $scope.attributedata[i].Caption;
                    $scope.dyn_Cont += '<div class="period control-group nomargin" data-periodcontainerID="periodcontainerID">';
                    if ($scope.attributedata[i].Value == "-") {
                        $scope.IsStartDateEmpty = true;
                        $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';
                        $scope.dyn_Cont += '</div>';
                    } else {
                        for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {                         
                            var datStartUTCval = "";
                            var datstartval = "";
                            var datEndUTCval = "";
                            var datendval = "";
                            datStartUTCval = $scope.attributedata[i].Value[j].Startdate.substr(0, 10);
                            datstartval = new Date.create(datStartUTCval);
                            datEndUTCval = $scope.attributedata[i].Value[j].EndDate.substr(0, 10);
                            datendval = new Date.create(datEndUTCval);


                            perioddates.push({
                                ID: $scope.attributedata[i].Value[j].Id,
                                value: datendval
                            });

                            $scope.fields["PeriodStartDateopen_" + $scope.attributedata[i].Value[j].Id] = false;

                            $scope.fields["PeriodEndDateopen_" + $scope.attributedata[i].Value[j].Id] = false;

                            $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = dateFormat(new Date.create(dateFormat($scope.attributedata[i].Value[j].Startdate), $scope.format), $scope.format);
                            $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = dateFormat(new Date.create(dateFormat($scope.attributedata[i].Value[j].EndDate), $scope.format), $scope.format);

                            $scope.fields["PeriodStartDate_Dir_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datstartval, $scope.format);
                            $scope.fields["PeriodEndDate_Dir_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datendval, $scope.format);

                            if ($scope.attributedata[i].Value[j].Description == undefined) {
                                $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = "-";
                                $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "";
                            } else {
                                $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                            }
                            $('#fsedateid').css("visibility", "hidden");
                            $scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + $scope.attributedata[i].Value[j].Id + '">';
                            $scope.dyn_Cont += '<div class="inputHolder">';
                            $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label>';
                            $scope.dyn_Cont += '<div class="controls">';
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<span>{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                $scope.dyn_Cont += '<span> to </span><span>{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                            } else {
                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id] = new Date.create();
                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MinValue + 1));
                                } else {
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MaxValue - 1));
                                } else {
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].getDate() + 100000);
                                }
                                var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id]);
                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id] = (temp.MinDate);
                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id] = (temp.MaxDate);
                                $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                $scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                            }
                            $scope.dyn_Cont += '</div></div>';
                            $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                            $scope.dyn_Cont += '<div class="controls">';
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<span>{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                            } else {
                                $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                            }
                            $scope.dyn_Cont += '</div></div></div>';
                            if (j != 0) {
                                $scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + $scope.attributedata[i].Value[j].Id + ')"><i class="icon-remove"></i></a></div>';
                            }
                            $scope.dyn_Cont += '</div>';
                            if (j == ($scope.attributedata[i].Value.length - 1)) {
                                $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';
                                $scope.dyn_Cont += '</div>';
                            }
                        }
                    }
                    $scope.dyn_Cont += ' </div>';
                    $scope.dyn_Cont += '<div class="control-group nomargin">';
                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '<label  data-tempid="startendID" class="control-label" for="label">Start date / End date</label>';
                        $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[Add Start / End Date ]</a>';
                        $scope.dyn_Cont += '<span>[Add Start / End Date ]</span>';
                    } else {
                        if ($scope.attributedata[i].Value == "-") {
                            $scope.dyn_Cont += '<label id="fsedateid"  class="control-label" for="label">' + inlineEditabletitile + '</label>';
                        }
                        $scope.dyn_Cont += '<div class="controls">';
                        $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[Add ' + inlineEditabletitile + ' ]</a>';
                        $scope.dyn_Cont += '</div>';
                    }
                    $scope.dyn_Cont += '</div>';
                } else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
                    var datStartUTCval = "";
                    var datstartval = "";
                    var inlineEditabletitile = $scope.attributedata[i].Caption;
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    if ($scope.attributedata[i].Value != null) {
                        datstartval = new Date.create($scope.attributedata[i].Value);
                        $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat);
                        $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = formatteddateFormat(datstartval, "dd/MM/yyyy");
                    } else {
                        $scope.fields["DateTime_" + $scope.attributedata[i].ID] = '-';
                        $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = '-';
                    }
                    if ($scope.attributedata[i].ID != SystemDefiendAttributes.CreationDate) {
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        } else {
                            $scope.MinValue = $scope.attributedata[i].MinValue;
                            $scope.MaxValue = $scope.attributedata[i].MaxValue;
                            $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = new Date.create();
                            $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = new Date.create();
                            if ($scope.MinValue < 0) {
                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue + 1));
                            } else {
                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue));
                            }
                            if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MaxValue - 1));
                            } else {
                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + 100000);
                            }
                            var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID]);
                            $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = (temp.MinDate);
                            $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = (temp.MaxDate);
                            $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a xeditabletreedatetime data-primaryid="' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-datetime_id=\"DateTime_' + $scope.attributedata[i].ID + '\" data-ng-model=\"DateTime_' + $scope.attributedata[i].ID + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"datetime' + $scope.attributedata[i].ID + '\" href=\"javascript:;\" attributename="datetime">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                        }
                    } else {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                    }
                } else if ($scope.attributedata[i].TypeID == 7) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    $scope.fields["Tree_" + $scope.attributedata[i].ID] = [];
                    $scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                    GetTreeCheckedNodes($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID], $scope.attributedata[i].ID);
                    $scope.staticTreesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class="control-group">';
                    $scope.dyn_Cont += '<label class="control-label">' + $scope.attributedata[i].Lable + ' </label>';
                    $scope.dyn_Cont += '<div class="controls">';
                    $scope.dyn_Cont += '<form class="form-horizontal"><div xeditabletree  editabletypeid="treeType_' + $scope.attributedata[i].ID + '" attributename=\"' + $scope.attributedata[i].Lable + '\" isreadonly="' + $scope.attributedata[i].IsReadOnly + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '"  data-type="treeType_' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"' + $scope.attributedata[i].ID + '\" data-ng-model=\"tree_' + $scope.attributedata[i].ID + '"\    data-original-title=\"' + $scope.attributedata[i].Lable + '\">';
                    if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                        treeTextVisbileflag = false;
                        if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                            $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                        } else $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                    } else {
                        $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                    }
                    $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\" treeplace="detail" node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                    $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';
                    $scope.dyn_Cont += ' </div></form>';
                    $scope.dyn_Cont += '</div></div>';
                } else if ($scope.attributedata[i].TypeID == 8) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                    if ($scope.attributedata[i].Caption != undefined) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html(($scope.attributedata[i].Caption).formatMoney(0, ' ', ' ')).text();
                    }
                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                    } else {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"   data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                    }
                } else if ($scope.attributedata[i].TypeID == 13) {
                    $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.attributedata[i].ID + ""] = $scope.attributedata[i].DropDownPricing;
                    $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.attributedata[i].ID + ""] = true;
                    for (var j = 0, price; price = $scope.attributedata[i].DropDownPricing[j++];) {
                        if (price.selection.length > 0) {
                            var selectiontext = "";
                            var valueMatches = [];
                            if (price.selection.length > 0) valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                return price.selection.indexOf(relation.NodeId.toString()) != -1;
                            });
                            if (valueMatches.length > 0) {
                                selectiontext = "";
                                for (var x = 0, val; val = valueMatches[x++];) {
                                    selectiontext += val.caption;
                                    if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                    else selectiontext += "</br>";
                                }
                            } else selectiontext = "-";
                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + j] = selectiontext;
                        } else {
                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + j] = "-";
                        }
                        $scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = false;
                        $scope.dyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><a  href=\"javascript:;\" xeditablepercentage entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + j + '" editabletypeid="percentagetype' + $scope.attributedata[i].ID + '_' + j + '" data-type="percentagetype' + $scope.attributedata[i].ID + '_' + j + '"  my-qtip2 qtip-content=\"' + price.LevelName + '\"  attributename=\"' + price.LevelName + '\"  ><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + j + ')"></span></a></div></div>';
                    }
                } else if ($scope.attributedata[i].TypeID == 11) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    $scope.fields["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                    $scope.UploaderCaption["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                    $scope.setUploaderCaption();
                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group ng-scope\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable.toString() + '</label>';
                    $scope.dyn_Cont += '<div class=\"controls\">';
                    if ($scope.attributedata[i].Caption == "" || $scope.attributedata[i].Caption == null && $scope.attributedata[i].Caption == undefined) {
                        $scope.attributedata[i].Caption = $scope.attributedata[i].Lable;
                    }
                    if ($scope.attributedata[i].Value == "" || $scope.attributedata[i].Value == null && $scope.attributedata[i].Value == undefined) {
                        $scope.attributedata[i].Value = "NoThumpnail.jpg";
                    }
                    $scope.dyn_Cont += '<img src="' + imagesrcpath + 'UploadedImages/' + $scope.attributedata[i].Value + '" alt="' + $scope.attributedata[i].Caption + '"';
                    $scope.dyn_Cont += 'class="entityDetailImgPreview" id="UploaderPreview1_' + $scope.attributedata[i].ID + '">';
                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '</div></div>';
                    } else {
                        $scope.dyn_Cont += "<a class='margin-left10x' ng-click='UploadImagefileTaskCreate(" + $scope.attributedata[i].ID + ",false)' attributeTypeID='" + $scope.attributedata[i].TypeID + "'";
                        $scope.dyn_Cont += 'entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="Uploader_' + $scope.attributedata[i].ID + '"';
                        $scope.dyn_Cont += 'my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                        $scope.dyn_Cont += ' class="ng-pristine ng-valid" attributename=\"' + $scope.UploaderCaption["UploaderCaption_" + $scope.attributedata[i].ID] + '\">Select Image';
                        $scope.dyn_Cont += '</a></div></div>';
                    }
                } else if ($scope.attributedata[i].TypeID == 18) {
                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">' + $scope.attributedata[i].Caption + '</label></div></div>';
                } else if ($scope.attributedata[i].TypeID == 17) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    if ($scope.attributedata[i].Caption[0] != undefined) {
                        if ($scope.attributedata[i].Caption.length > 0) {
                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                            $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value;
                        }
                    } else {
                        $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                        $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = [];
                    }
                    $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                    } else {
                        if ($scope.Islock_temp == false) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabletagwords href=\"javascript:;\" data-mode=\"inline\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="TagWordsCaption_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.TagWordsCaption_' + $scope.attributedata[i].ID + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" data-original-title="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\"  >{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                        } else if ($scope.Islock_temp == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        }
                    }
                } else if ($scope.attributedata[i].TypeID == 19) {
                    $scope.isfromtask = 1;
                    if ($scope.attributedata[i].Caption[0] != undefined) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Value != null) {
                            $scope['origninalamountvalue_' + $scope.attributedata[i].ID] = $scope.attributedata[i].Value.Amount;
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html((($scope.attributedata[i].Value.Amount).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' '))).text();
                            $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value.Currencytypeid;
                            var currtypeid = $scope.attributedata[i].Value.Currencytypeid;
                            $scope.currtypenameobj = ($.grep($scope.CurrencyFormatsList, function (e) {
                                return e.Id == currtypeid;
                            }));
                            $scope.fields["currtypename_" + $scope.attributedata[i].ID] = $scope.currtypenameobj[0]["ShortName"];
                        } else {
                            $scope['origninalamountvalue_' + $scope.attributedata[i].ID] = 0;
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.DefaultSettings.CurrencyFormat.Id;
                            $scope.fields["currtypename_" + $scope.attributedata[i].ID] = "-";
                        }
                        $scope.currencytypeslist = $scope.CurrencyFormatsList;
                        $scope.setFieldKeys();
                        $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label widthauto ng-binding">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label><span class="va-middle inlineBlock padding-top5x margin-left5x color-info ng-binding">{{fields.currtypename_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        } else {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletextforcurrencyamount   href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"  my-qtip2 qtip-content=\"' + $scope.attributedata[i].Lable + '\"  data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}<span class="margin-left5x">{{fields.currtypename_' + $scope.attributedata[i].ID + '}}</span></a></div></div>';
                        }
                    }
                }
            }
            $("#admintaskDynamicTaskControls").unbind();
            $("#admintaskDynamicTaskControls").empty();
            $("#admintaskDynamicTaskControls").html($compile("")($scope));
            $("#adminTaskDynamic-metadata").unbind();
            $("#adminTaskDynamic-metadata").empty();
            $("#adminTaskDynamic-metadata").html($compile("")($scope));
            $("#adminTaskDynamic-metadata").html($compile($scope.dyn_Cont)($scope));
            HideAttributeToAttributeRelationsOnPageLoad();
            for (i = 0; i < $scope.attributedata.length; i++) {
                if (($scope.attributedata[i].TypeID == 3 || $scope.attributedata[i].TypeID == 4 || $scope.attributedata[i].TypeID == 6 || $scope.attributedata[i].TypeID == 7 || $scope.attributedata[i].TypeID == 12) && $scope.attributedata[i].IsSpecial == false) {
                    if ($scope.attributedata[i].TypeID == 12) {
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null) {
                                if (($scope.attributedata[i].Lable.length - 1) == j) {
                                    var k = $scope.attributedata[i].Value.length - $scope.attributedata[i].Lable.length;
                                    for (k; k < $scope.attributedata[i].Value.length; k++) {
                                        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[k].Nodeid, 12);
                                    }
                                } else {
                                    $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid, 12);
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 6) {
                        for (var j = 0; j < $scope.attributedata[i].Lable.length - 1; j++) {
                            if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null) $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid, 6);
                        }
                    } else if ($scope.attributedata[i].TypeID == 4) {
                        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.attributedata[i].Value, 4);
                    } else if ($scope.attributedata[i].TypeID == 7) {
                        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.fields["Tree_" + $scope.attributedata[i].ID], 7);
                    } else {
                        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.attributedata[i].Value, $scope.attributedata[i].TypeID);
                    }
                }
            }

        }

        $scope.drpdirectiveSource = {};

        $scope.IsSourceformed = function (attrID, levelcnt, attributeLevel, attrType) {
            if (levelcnt > 0) {
                var dropdown_text = '', subid = 0;
                var currntlevel = attributeLevel + 1;
                if (attributeLevel == 1) {
                    var dropdown_text = 'dropdown_text_' + attrID + '_' + attributeLevel;
                    var idtomatch = $scope['dropdown_' + attrID + '_' + attributeLevel] != undefined ? ($scope['dropdown_' + attrID + '_' + attributeLevel].id != undefined ? $scope['dropdown_' + attrID + '_' + attributeLevel].id : $scope['dropdown_' + attrID + '_' + attributeLevel]) : 0;
                    if (idtomatch != 0)
                        $scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel] = $.grep($scope.treeSources["dropdown_" + attrID].Children,
                            function (e) {
                                return e.id == idtomatch;
                            }
                        )[0];
                }
                for (var j = currntlevel; j <= levelcnt; j++) {
                    dropdown_text = 'dropdown_text_' + attrID + '_' + j;
                    subid = $scope['dropdown_' + attrID + '_' + (j)] != undefined ? ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined ? $scope['dropdown_' + attrID + '_' + (j - 1)].id : $scope['dropdown_' + attrID + '_' + (j)]) : 0;
                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = {};
                    if (subid != 0 && subid > 0) {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + (j)] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children != undefined && $scope['dropdown_' + attrID + '_' + (j - 1)] != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = ($.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children, function (e) { return e.id == subid; }))[0];
                        }
                    }
                    else {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + j] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)] != undefined) {
                                var res = [];
                                if ($scope['dropdown_' + attrID + '_' + (j)] > 0)
                                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)]['Children'], function (e) { return e.id == $scope['dropdown_' + attrID + '_' + (j)] })[0];
                                else
                                    $scope['dropdown_' + attrID + '_' + (j)] = 0;
                            }
                        }
                    }
                }
            }
        }

        $scope.BindChildDropdownSource = function (attrID, levelcnt, attributeLevel, attrType) {

            if (levelcnt > 0) {
                if (attrType == 6) {
                    $scope.IsSourceformed(attrID, levelcnt, attributeLevel, attrType);
                }
                var currntlevel = attributeLevel + 1;
                for (var j = currntlevel; j <= levelcnt; j++) {
                    $scope["ddtoption_" + attrID + "_" + j].data.splice(0, $scope["ddtoption_" + attrID + "_" + j].data.length);
                    if (attrType == 6) {
                        $scope["ddtoption_" + attrID + "_" + (attributeLevel + 1)].data.splice(0, $scope["ddtoption_" + attrID + "_" + (attributeLevel + 1)].data.length);
                    } else if (attrType == 12) {
                        if (j == levelcnt) $scope["multiselectdropdown_" + attrID + "_" + j] = [];
                        else $scope["multiselectdropdown_" + attrID + "_" + j] = "";
                    }
                }
                if (attrType == 6) {
                    if ($scope["dropdown_" + attrID + "_" + attributeLevel].id == undefined) {
                        if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel] != null && $scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel] != undefined) {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                                $.each($scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                    $scope["ddtoption_" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                                });
                            }
                        }
                    }
                    else {
                        if ($scope["dropdown_" + attrID + "_" + attributeLevel] != null && $scope["dropdown_" + attrID + "_" + attributeLevel] != undefined) {
                            if ($scope["dropdown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                                $.each($scope["dropdown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                    $scope["ddtoption_" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                                });
                            }
                        }
                    }
                } else if (attrType == 12) {
                    if ($scope["multiselectdropdown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        $.each($scope["multiselectdropdown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                            $scope["ddtoption_" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        });
                    }
                }
            }
        }

        var treeTextVisbileflag = false;

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }
        $scope.deletePeriodDate = function (periodid) {
            var taskID = $scope.SelectedTaskID;
            AdminTaskService.DeleteEntityPeriod(periodid).then(function (deletePerById) {
                if (deletePerById.StatusCode == 200) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                    $("#adminTaskDynamic-metadata div[data-dynperiodid = " + periodid + " ]").html('');
                    perioddates = $.grep(perioddates, function (val) {
                        return val.ID != periodid;
                    });
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4316.Caption'));
                }
            });
        }
        $scope.saveDropdownTree = function (attrID, attributetypeid, entityTypeid, optionarray) {
            if (attributetypeid == 6) {
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.SelectedTaskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                AdminTaskService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        for (var j = 0; j < optionarray.length; j++) {
                            if (optionarray[j] != 0) {
                                $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                            }
                        }
                    }
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 1 || attributetypeid == 2) {
                var updateentityattrib = {};
                if (attrID == 68) {
                    $(window).trigger("UpdateEntityName", optionarray);
                    $("ul li a[data-entityid='" + $scope.SelectedTaskID + "'] span[data-name='text']").text([optionarray]);
                    $("ul li a[data-entityid='" + $scope.SelectedTaskID + "']").attr('data-entityname', optionarray);
                    var Splitarr = [];
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            if ($scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"] != undefined) {
                                if ($scope.SelectedTaskID != $scope.ListViewDetails[i].data.Response.Data[j]["Id"] && $scope.SelectedTaskID == $scope.ListViewDetails[i].data.Response.Data[j]["ParentID"]) {
                                    if (Splitarr.length == 0) {
                                        Splitarr = $scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"].split('!@#');
                                    }
                                    $scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"] = optionarray + "!@#" + Splitarr[1] + "!@#" + Splitarr[2];
                                }
                            } else {
                                break;
                            }
                        }
                    }
                }
                updateentityattrib.EntityID = parseInt($scope.SelectedTaskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                updateentityattrib.AttributetypeID = attributetypeid;
                AdminTaskService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        $scope.fields['SingleLineTextValue_' + attrID] = optionarray;
                        if (attrID == 68) {
                            $('#breadcrumlink').text(optionarray);
                        }
                    }
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 3 || attributetypeid == 4 || attributetypeid == 17) {
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.SelectedTaskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray == "" ? ["0"] : optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                AdminTaskService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                });
                $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, optionarray);
                $scope.treeSelection = [];
            } else if (attributetypeid == 12) {
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.SelectedTaskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                updateentityattrib.AttributetypeID = attributetypeid;
                AdminTaskService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        for (var j = 0; j < optionarray.length; j++) {
                            if (optionarray[j] != 0) {
                                $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                            }
                        }
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    }
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 8) {
                if (!isNaN(optionarray.toString())) {
                    var updateentityattrib = {};
                    updateentityattrib.EntityID = parseInt($scope.SelectedTaskID, 10);
                    updateentityattrib.AttributeID = attrID;
                    updateentityattrib.Level = 0;
                    updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                    updateentityattrib.AttributetypeID = attributetypeid;
                    AdminTaskService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                        if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        else {
                            $scope.fields['SingleLineTextValue_' + attrID] = optionarray == "" ? 0 : parseInt(optionarray).formatMoney(0, ' ', ' ');
                            NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        }
                    });
                    $scope.treeSelection = [];
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                }
            } else if (attributetypeid == 5) {
                var sdate = new Date.create($scope.fields['DateTime_Dir_' + attrID].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                if (sdate == 'NaN-NaN-NaN') {
                    sdate = dateFormat(ConvertStringToDateByFormat($scope.fields['DateTime_Dir_' + periodid].toString(), GlobalUserDateFormat))
                }
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.SelectedTaskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = [sdate] == "" ? ["0"] :[sdate];
                updateentityattrib.AttributetypeID = attributetypeid;
                AdminTaskService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        $scope.fields["DateTime_" + attrID] = dateFormat(sdate, $scope.DefaultSettings.DateFormat);
                    }
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 19) {
                $scope.newamount = optionarray[0];
                $scope.newcurrencytypename = optionarray[2];
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.SelectedTaskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                AdminTaskService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    $scope.fields["SingleLineTextValue_" + attrID] = (parseFloat($scope.newamount)).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    $scope.fields["currtypename_" + attrID] = $scope.newcurrencytypename;
                    $scope.fields["NormalDropDown_" + attrID] = optionarray[1];
                });
                $scope.treeSelection = [];
            }
            $scope.treeSelection = [];
        };
        $scope.UploaderCaptionObj = [];
        $scope.uploadingInProgress = false;
        $scope.setUploaderCaption = function () {
            var keys1 = [];
            angular.forEach($scope.UploderCaption, function (key) {
                keys1.push(key);
                $scope.UploaderCaptionObj = keys1;
            });
        }
        $scope.savePeriodVal = function (attrID, attrTypeID, entityid, periodid) {
            var one_day = 1000 * 60 * 60 * 24;
            if (periodid == 0) {
                var newperiod = {};
                var sdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var edate = new Date.create($scope.fields['PeriodEndDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var maxPeriodDate = new Date.create(Math.max.apply(null, tempperioddates));
                var diffval = (parseInt(edate.getTime() - sdate.getTime()));
                if (diffval < 0) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_1956.Caption'));
                    return false;
                }
                var tempperioddates = [];
                for (var i = 0; i < perioddates.length; i++) {
                    tempperioddates.push(perioddates[i].value)
                }
                var one_day = 1000 * 60 * 60 * 24;
                var maxPeriodDate = new Date.create(Math.max.apply(null, tempperioddates));
                var diffval = (parseInt(sdate.getTime() - maxPeriodDate.getTime()));
                if (diffval < 1) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_1957.Caption'));
                    return false;
                }
                newperiod.EntityID = entityid;
                newperiod.StartDate = dateFormat($scope.fields['PeriodStartDate_Dir_' + periodid]);
                newperiod.EndDate = dateFormat($scope.fields['PeriodEndDate_Dir_' + periodid]);
                newperiod.Description = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                newperiod.SortOrder = 0;
                AdminTaskService.InsertEntityPeriod(newperiod).then(function (resultperiod) {
                    if (resultperiod.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else var newid = resultperiod.Response;
                    if ($scope.IsStartDateEmpty == true) {
                        $scope.IsStartDateEmpty = false;
                        $("[data-tempid=startendID]").remove();
                    }
                    perioddates.push({
                        ID: newid,
                        value: edate
                    });
                    $scope.dyn_Cont = '';
                    $scope.fields["PeriodStartDate_" + newid] = ConvertDateFromStringToString(ConvertDateToString(sdate));
                    $scope.fields["PeriodEndDate_" + newid] = ConvertDateFromStringToString(ConvertDateToString(edate));
                    $scope.fields["PeriodStartDate_Dir_" + newid] = dateFormat(new Date.create(sdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('dd-MM-yyyy'), $scope.format); //dateFormat(new Date.create(sdate.toString()), $scope.DefaultSettings.DateFormat);
                    $scope.fields["PeriodEndDate_Dir_" + newid] = dateFormat(new Date.create(edate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('dd-MM-yyyy'), $scope.format);// dateFormat(new Date.create(edate.toString()), $scope.DefaultSettings.DateFormat);
                    $scope.fields["PeriodDateDesc_Dir_" + newid] = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                    if ($scope.fields['PeriodDateDesc_Dir_' + periodid] == "" || $scope.fields['PeriodDateDesc_Dir_' + periodid] == undefined) {
                        $scope.fields["PeriodDateDesc_" + newid] = "-";
                    } else {
                        $scope.fields["PeriodDateDesc_" + newid] = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                    }
                    $('#fsedateid').css("visibility", "hidden");
                    $scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + newid + '">';
                    $scope.dyn_Cont += '<div class="inputHolder">';
                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Period </label>';
                    $scope.dyn_Cont += '<div class="controls">';
                    $scope.dyn_Cont += '<a  xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodstartdate_id=\"PeriodStartDate_' + newid + '\" data-ng-model=\"PeriodStartDate_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\" data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + newid + '}}</a>';
                    $scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodenddate_id=\"PeriodEndDate_' + newid + '\" data-ng-model=\"PeriodEndDate_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\"  data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + newid + '}}</a>';
                    $scope.dyn_Cont += '</div></div>';
                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Comment Period </label>';
                    $scope.dyn_Cont += '<div class="controls">';
                    $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodenddate_id=\"PeriodDateDesc_' + newid + '\" data-ng-model=\"PeriodDateDesc_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\"  data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + newid + '}}</a>';
                    $scope.dyn_Cont += '</div></div></div>';
                    if (perioddates.length != 1) {
                        $scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + newid + ')"><i class="icon-remove"></i></a></div></div>';
                    }
                    var divcompile = $compile($scope.dyn_Cont)($scope);
                    if ($scope.fromAssignedpopup) {
                        if ($scope.TaskBriefDetails.taskTypeId == 2) $("#taskeditworkAssetdynamicdetail div[data-addperiodid]").append(divcompile);
                        else if ($scope.TaskBriefDetails.taskTypeId == 3) $("#taskeditapprovalworktaskdynamicHolder div[data-addperiodid]").append(divcompile);
                        else if ($scope.TaskBriefDetails.taskTypeId == 31) $("#taskeditreviewtaskdynamicholder div[data-addperiodid]").append(divcompile);
                        else $("#adminTaskDynamic-metadata div[data-addperiodid]").append(divcompile);
                    } else {
                        $("#taskeditunassignedtaskdynamicdataholder div[data-addperiodid]").append(divcompile);
                    }
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                });
            } else {
                var updateperiod = {};
                var temparryStartDate = [];
                $('[data-periodstartdate_id^=PeriodStartDate_]').each(function () {
                    if (parseInt(periodid) < parseInt(this.attributes['data-primaryid'].textContent)) {
                        var sdate;
                        if (this.text != "[Add Start / End Date ]") {
                            sdate = new Date.create(this.text.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                            temparryStartDate.push(sdate);
                        }
                    }
                });
                var temparryEndate = [];
                $('[data-periodenddate_id^=PeriodEndDate_]').each(function () {
                    if (parseInt(periodid) > parseInt(this.attributes['data-primaryid'].textContent)) {
                        var edate = new Date.create(this.text.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                        temparryEndate.push(edate);
                    }
                });
                var sdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                if (sdate == 'NaN-NaN-NaN') {
                    sdate = dateFormat(ConvertStringToDateByFormat($scope.fields['PeriodStartDate_Dir_' + periodid].toString(), GlobalUserDateFormat))
                }
                var edate = new Date.create($scope.fields['PeriodEndDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                if (edate == 'NaN-NaN-NaN') {
                    edate = dateFormat(ConvertStringToDateByFormat($scope.fields['PeriodEndDate_Dir_' + periodid].toString(), GlobalUserDateFormat))
                }
                var diffval = ((parseInt(new Date.create(edate.toString('dd/MM/yyyy')).getTime()) - parseInt((new Date.create(sdate.toString('dd/MM/yyyy')).getTime()))));
                if (diffval < 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1956.Caption'));
                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    return false;
                }
                var maxPeriodEndDate = new Date.create(Math.max.apply(null, temparryEndate));
                var Convertsdate = new Date.create(sdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var diffvalend = (parseInt(Convertsdate.getTime() - maxPeriodEndDate.getTime()));
                if (diffvalend != NaN && !isNaN(diffvalend)) {
                    if (parseInt(diffvalend) < 1) {
                        $scope.fields["PeriodStartDate_Dir_0"] = "";
                        $scope.fields["PeriodEndDate_Dir_0"] = "";
                        $scope.fields["PeriodDateDesc_Dir_0"] = "";
                        $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                        $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                        bootbox.alert($translate.instant('LanguageContents.Res_1992.Caption'));
                        return false;
                    }
                    var minPeroidStartDate = new Date.create(Math.min.apply(null, temparryStartDate));
                    var Convertedate = new Date.create(edate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                    var diffvalstart = (parseInt(Convertedate.getTime() - minPeroidStartDate.getTime()));
                    if (parseInt(diffvalstart) > 1) {
                        $scope.fields["PeriodStartDate_Dir_0"] = "";
                        $scope.fields["PeriodEndDate_Dir_0"] = "";
                        $scope.fields["PeriodDateDesc_Dir_0"] = "";
                        $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                        $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                        bootbox.alert($translate.instant('LanguageContents.Res_1991.Caption'));
                        return false;
                    }
                }
                updateperiod.ID = periodid;
                updateperiod.EntityID = entityid;
                updateperiod.StartDate = dateFormat($scope.fields['PeriodStartDate_Dir_' + periodid]);;
                updateperiod.EndDate = dateFormat($scope.fields['PeriodEndDate_Dir_' + periodid]);;
                updateperiod.SortOrder = 0;
                updateperiod.Description = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                AdminTaskService.PostEntityPeriod(updateperiod).then(function (resultperiod) {
                    if (resultperiod.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        $scope.fields["PeriodStartDate_" + periodid] = ConvertDateFromStringToString(sdate);
                        $scope.fields["PeriodEndDate_" + periodid] = ConvertDateFromStringToString(edate);
                        $scope.fields["PeriodDateDesc_" + periodid] = $scope.fields['PeriodDateDesc_Dir_' + periodid] == "" ? "-" : $scope.fields['PeriodDateDesc_Dir_' + periodid];
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    }
                });
            }
        }
        $scope.savetreeDetail = function (attrID, attributetypeid, entityTypeid) {
            if (attributetypeid == 7) {
                $scope.treeNodeSelectedHolder = [];
                GetTreeObjecttoSave(attrID);
                var updateentityattrib = {};
                updateentityattrib.NewValue = $scope.treeNodeSelectedHolder;
                updateentityattrib.EntityID = parseInt($scope.SelectedTaskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.newTree = $scope.staticTreesrcdirec["Attr_" + attrID];
                updateentityattrib.oldTree = $scope.treesrcdirec["Attr_" + attrID];
                updateentityattrib.AttributetypeID = attributetypeid;
                AdminTaskService.SaveDetailBlockForTreeLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        AdminTaskService.GetAttributeTreeNodeByEntityID(attrID, parseInt($scope.SelectedTaskID, 10)).then(function (GetTree) {
                            $scope.treesrcdirec["Attr_" + attrID] = JSON.parse(GetTree.Response).Children;
                            if ($scope.treeNodeSelectedHolder.length > 0) $scope.TreeEmptyAttributeObj["Attr_" + attrID] = true;
                            else $scope.TreeEmptyAttributeObj["Attr_" + attrID] = false;
                        });
                        $scope.fields["Tree_" + attrID].splice(0, $scope.fields["Tree_" + attrID].length);
                        GetTreeCheckedNodes($scope.treeNodeSelectedHolder, attrID);
                        $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, $scope.fields["Tree_" + attrID]);
                    }
                });
            }
        }
        $scope.saveDropDownTreePricing = function (attrID, attributetypeid, entityTypeid, choosefromParent, inherritfromParent) {
            if (attributetypeid == 13) {
                var NewValue = [];
                NewValue = ReturnSelectedTreeNodes(attrID);
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.SelectedTaskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.NewValue = NewValue;
                updateentityattrib.AttributetypeID = attributetypeid;
                AdminTaskService.UpdateDropDownTreePricing(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        var GetTreeRes;
                        if (choosefromParent) AdminTaskService.GetDropDownTreePricingObjectFromParentDetail(attrID, choosefromParent, choosefromParent, parseInt($scope.SelectedTaskID, 10), 0).then(function (GetTree) {
                            if (GetTree.Response != null) {
                                var result = GetTree.Response;
                                for (var p = 0, price; price = result[p++];) {
                                    var attributeLevelOptions = [];
                                    attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""], function (e) {
                                        return e.level == p;
                                    }));
                                    if (attributeLevelOptions[0] != undefined) {
                                        attributeLevelOptions[0].selection = price.selection;
                                        attributeLevelOptions[0].LevelOptions = price.LevelOptions;
                                        if (price.selection.length > 0) {
                                            var selectiontext = "";
                                            var valueMatches = [];
                                            if (price.selection.length > 0) valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                                return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                            });
                                            if (valueMatches.length > 0) {
                                                selectiontext = "";
                                                for (var x = 0, val; val = valueMatches[x++];) {
                                                    selectiontext += val.caption;
                                                    if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                                    else selectiontext += "</br>";
                                                }
                                            } else selectiontext = "-";
                                            $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = selectiontext;
                                        } else {
                                            $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = "-";
                                        }
                                    }
                                }
                            }
                        });
                        else AdminTaskService.GetDropDownTreePricingObject(attrID, choosefromParent, choosefromParent, parseInt($scope.SelectedTaskID, 10), 0).then(function (GetTree) {
                            if (GetTree.Response != null) {
                                var result = GetTree.Response;
                                for (var p = 0, price; price = result[p++];) {
                                    var attributeLevelOptions = [];
                                    attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""], function (e) {
                                        return e.level == p;
                                    }));
                                    if (attributeLevelOptions[0] != undefined) {
                                        attributeLevelOptions[0].selection = price.selection;
                                        attributeLevelOptions[0].LevelOptions = price.LevelOptions;
                                        if (price.selection.length > 0) {
                                            var selectiontext = "";
                                            var valueMatches = [];
                                            if (price.selection.length > 0) valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                                return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                            });
                                            if (valueMatches.length > 0) {
                                                selectiontext = "";
                                                for (var x = 0, val; val = valueMatches[x++];) {
                                                    selectiontext += val.caption;
                                                    if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                                    else selectiontext += "</br>";
                                                }
                                            } else selectiontext = "-";
                                            $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = selectiontext;
                                        } else {
                                            $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = "-";
                                        }
                                    }
                                }
                            }
                        });
                    }
                });
            }
        }

        function ReturnSelectedTreeNodes(attrID) {
            var selection = [];
            for (var y = 0, price; price = $scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""][y++];) {
                if (price.selection.length > 0) {
                    var matches = [];
                    matches = jQuery.grep(price.LevelOptions, function (relation) {
                        return price.selection.indexOf(relation.NodeId.toString()) != -1;
                    });
                    if (matches.length > 0) for (var z = 0, node; node = matches[z++];) {
                        selection.push({
                            "NodeId": node.NodeId,
                            "Level": price.level,
                            "value": node.value != "" ? node.value : "-1"
                        })
                    }
                }
            }
            return selection;
        }

        function GetTreeObjecttoSave(attributeid) {
            GenerateTreeStructure($scope.staticTreesrcdirec["Attr_" + attributeid]);
        }
        var treeformflag = false;

        function GenerateTreeStructure(treeobj) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == node.AttributeId && e.id == node.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(node);
                    }
                    treeformflag = false;
                    if (ischildSelected(node.Children)) {
                        GenerateTreeStructure(node.Children);
                    } else {
                        GenerateTreeStructure(node.Children);
                    }
                } else GenerateTreeStructure(node.Children);
            }
        }

        function ischildSelected(children) {
            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    treeformflag = true;
                    return treeformflag
                }
            }
            return treeformflag;
        }

        function GetEntityAttributesDetails(taskID) {
            AdminTaskService.GetEntityAttributesDetails(taskID).then(function (TaskDetailList) {
                LoadTaskMetadata(TaskDetailList.Response, $scope.SelectedTaskID, $scope.TaskType, $scope.fromAssignedpopup);
            });
        }
        $scope.AssetfileTemplateLoading = "Null";
        $scope.SetCurrentTabid = function (activecalss, deactiveclass) {
            if (activecalss == "task-Attachments-admin") {
                StartAssetUploadTaskadmin();
            }
            else if (activecalss == "task-MetaData-admin") {
                StrartUpload_UploaderAttrAdmin();
            }
            $("#li" + activecalss + "").removeClass('active');
            $("#" + activecalss + "").removeClass('tab-pane active');
            $("#" + activecalss + "").removeClass('tab-pane');
            $("#li" + activecalss + "").addClass('active');
            $("#" + activecalss + "").addClass('tab-pane active');
            $("#li" + deactiveclass + "").removeClass('active');
            $("#" + deactiveclass + "").removeClass('tab-pane active');
            $("#" + deactiveclass + "").removeClass('tab-pane');
            $("#" + deactiveclass + "").addClass('tab-pane');
        }
        $scope.CloseDampopup = function () {
            $("#addTask").modal('hide');
            $scope.adminaddfromComputer = false;

            $scope.SetCurrentTabid("task-MetaData-admin", "task-Attachments-admin");

        };
        $scope.DAMentitytypes = [];

        function TooltipattachmentName(description) {
            if (description != null & description != "") {
                return description;
            } else {
                return "<label class=\"align-center\">No description</label>";
            }
        };
        $scope.popMe = function (e) {
            var TargetControl = $(e.target);
            var mypage = TargetControl.attr('data-Name');
            if (!(mypage.indexOf("http") == 0)) {
                mypage = "http://" + mypage;
            }
            var myname = TargetControl.attr('data-Name');
            var w = 1200;
            var h = 800
            var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
            var win = window.open(mypage, myname, winprops)
        };
        $scope.dyn_ContAsset = '';
        $scope.DAMtypeswithExt = [];
        $scope.DamExt = [];
        $scope.Fileids = [];

        function GetAllExtensionTypesforDAM() {
            AdminTaskService.GetAllExtensionTypesforDAM().then(function (data) {
                $scope.DAMtypeswithExt = data.Response;
                $scope.AssetTasktypeswithExt = $scope.DAMtypeswithExt;
            });
        }
        var filecount = 0;
        var totalfilecount = 0;
        $timeout(function () {
            $scope.fnTimeOutfrAssetCreationTaskadmin();
        }, 50);
        $scope.fnTimeOutfrAssetCreationTaskadmin = function () {

            $('#pickassetsAlternatetaskadmin').removeAttr('disabled', 'disabled');
            $('#assetlistActivityTaskadmin').empty();
            $("#totalAssetActivityAttachProgresstaskadmin").empty();
            $("#admintaskuploaded").empty();
            $("#totalAssetActivityAttachProgresstaskadmin").append('<span class="pull-left count">0 of 0 Uploaded</span><span class="size">0 B / 0 B</span>' + '<div class="progress progress-striped active">' + '<div style="width: 0%" class="bar"></div>' + '</div>' + '<div id="admintaskuploaded" style="position: absolute; top: 42%; right: -30%;"></div>');
        };

        function applyRemoteData(newFriends) {
            var friends = newFriends;
        }
        var TotalCount = 0;
        var saveBlankAssetsCalled = false;
        function StartAssetUploadTaskadmin() {
            var uploaderAssetadmin = {};
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                $('.moxie-shim').remove();
                uploaderAssetadmin = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickassetsAlternatetaskadmin',
                    container: 'assetscontainerTaskadmin',
                    max_file_size: '10000mb',
                    url: amazonURL + cloudsetup.BucketName,
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    urlstream_upload: true,
                    multiple_queues: true,
                    file_data_name: 'file',
                    multipart: true,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature
                    }
                });
                uploaderAssetadmin.bind('Init', function (up, params) {
                    uploaderAssetadmin.splice();
                });
                uploaderAssetadmin.init();
                uploaderAssetadmin.bind('FilesAdded', function (up, files) {
                    totalfilecount = totalfilecount + files.length;
                    $.each(files, function (i, file) {
                        var damid = 0;
                        $scope.Fileids.push(file.id);
                        var ste = file.name.split('.')[file.name.split('.').length - 1];
                        if (ste != undefined) ste = ste.toLowerCase();
                        var damCount = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                            return e.damExtention == ste
                        }).length;
                        if (damCount > 0) {
                            var selecteddamtype = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                                return e.damExtention == ste
                            });
                        } else {
                            var wildcardExt = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                                return e.damExtention.indexOf("*") != -1
                            });
                            var iswildcard = [];
                            iswildcard = $.grep(wildcardExt, function (e) {
                                return e.damExtention.substring(0, e.damExtention.length - 1).indexOf(ste) != -1
                            });
                            if (iswildcard.length > 0) var selecteddamtype = iswildcard;
                        }
                        $scope.AllDamTypes = [];
                        if (selecteddamtype != undefined && selecteddamtype != null) {
                            damid = selecteddamtype[0].Id;
                            var ownerID = parseInt($scope.OwnerID, 10);
                        }
                        AdminTaskService.GetAssetCategoryTree(damid).then(function (data) {
                            if (data.Response != null) {
                                $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                $scope.AssetCategory["filterval_" + file.id] = '';
                                var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                $("#dynamicAssetTypeTreeInLibrary").html($compile(htmltree)($scope));
                            }
                        });
                        $scope.AssetCategory["treestructure_" + file.id] = '-';
                        $scope.AssetCategory["AssetTypesList_" + file.id] = [];
                        AdminTaskService.GetAssetCategoryPathInAssetEdit(damid).then(function (datapath) {
                            if (datapath.Response != null) {
                                if (datapath.Response[1].AssetTypeIDs.length > 0) {
                                    $scope.AssetCategory["treestructure_" + file.id] = datapath.Response[0].CategoryPath;
                                    var tempval = [];
                                    var tempassty = "";
                                    for (var k = 0; k < datapath.Response[1].AssetTypeIDs.length; k++) {
                                        tempassty = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                                            return e.Id == datapath.Response[1].AssetTypeIDs[k]
                                        });
                                        if (tempassty != undefined && tempassty.length > 0) tempval.push(tempassty[0]);
                                    }
                                    if (tempval.length > 0) $scope.AssetCategory["AssetTypesList_" + file.id] = tempval;
                                } else $scope.fields["assettype_" + fileUniqueID] = '';
                            }
                        });
                        AdminTaskService.GetDamAttributeRelation(damid).then(function (attrreldata) {
                            $scope.dyn_ContAsset = "";
                            if (attrreldata.Response != undefined && attrreldata.Response != null) {
                                $scope.dyn_ContAsset = "";
                                $scope.DAMattributesRelationList = attrreldata.Response;
                                FileAttributes.FileID.push(file.id);
                                FileAttributes.AttributeDataList.push($scope.DAMattributesRelationList);
                                FileAttributes.AssetType.push(damid);
                                for (var i = 0; i < $scope.DAMattributesRelationList.length; i++) {
                                    if ($scope.DAMattributesRelationList[i].TypeID == 1) {
                                        if ($scope.DAMattributesRelationList[i].ID != 70) {
                                            if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.Name) {
                                                $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                                $scope.assetfields["TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                            } else {
                                                $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                                $scope.assetfields["TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                            }
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].TypeID == 3) {
                                        if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.Owner) {
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" dirownernameautopopulate placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.OwnerList[0].UserName;
                                        } else if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.FiscalYear) {
                                            $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"> <option value=\"\"> Select " + $scope.DAMattributesRelationList[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                        } else {
                                            $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"> <option value=\"\"> Select " + $scope.DAMattributesRelationList[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].TypeID == 2) {
                                        $scope.dyn_ContAsset += "<div class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"assetfields.TextMultiLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" name=\"assetfields.TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-model=\"assetfields.TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\" rows=\"3\"></textarea></div></div>";
                                        $scope.assetfields["TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                    } else if ($scope.DAMattributesRelationList[i].TypeID == 4) {
                                        $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = [];
                                        $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                        var defaultmultiselectvalue = $scope.DAMattributesRelationList[i].DefaultValue.split(',');
                                        if ($scope.DAMattributesRelationList[i].DefaultValue != "") {
                                            for (var v = 0, val; val = defaultmultiselectvalue[v++];) {
                                                $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID].push(parseInt(val));
                                            }
                                        } else {
                                            $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = [];
                                        }
                                        $scope.dyn_ContAsset += "<div class=\"control-group attachmentMultiselect\"><label class=\"control-label\" for=\"assetfields.ListMultiSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select class=\"multiselect\" multiple=\"multiple\"  multiselect-dropdown data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  > </select></div></div>";
                                    }
                                }
                            }
                            $scope.assetfields["assetname_" + file.id] = file.name;
                            if (selecteddamtype != undefined && selecteddamtype != null) {
                                var dataid = '';
                                var rec = [];
                                rec = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                                    return e.Id == selecteddamtype[0].Id
                                });
                                if (rec.length > 0) dataid = rec[0].Id;
                                if ($scope.AssetCategory["AssetTypesList_" + file.id].length > 0) {
                                    $scope.assetfields["assettype_" + file.id] = dataid;
                                    $scope.oldfiledetails["AssetType_" + file.id] = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                                        return e.Id == selecteddamtype[0].Id
                                    })[0].Id;
                                }
                            } else {
                                $scope.assetfields["assettype_" + file.id] = '';
                                $scope.oldfiledetails["AssetType_" + file.id] = '';
                            }
                            $('#assetlistActivityTaskadmin').append($compile('<div id="' + file.id + '" class="attachmentBox" Data-role="Attachment" data-size="' + file.size + '" data-size-uploaded="0" data-status="false">' + '<div class="row-fluid">' + '<div class="span12">' + '<div class="span2 leftSection">' + ' <i id="icon_ok' + file.id + '" class="icon-ok-sign" style="display:none;"  ></i>' + ' <div class="fileType-Preview">' + ' <div id="Preview_' + file.id + '" class="fileType-PreviewDiv">' + ' </div>' + '</div>' + ' <div class="progress progress-striped active">' + ' <div style="width: 0%" class="bar"></div>' + '</div>' + ' <div class="progressInfo">' + '  <span class="progress-percent"></span>' + '<span class="fileSize">' + plupload.formatSize(file.size) + '' + '</span>' + '</div>' + '</div>' + ' <div class="span10 rightSection">' + '<div class="info">' + '  <span class="name">' + ' <i class="icon-file-alt"></i>' + file.name + '</span>' + ' <span class="size">' + ' <i class="icon-remove removefile" data-fileid="' + file.id + '"></i>' + ' </span>' + '</div>' + '<div  class="formInfo">' + ' <form class="form-float">' + '<div class=\"control-group fullSpace\">' + '<label class=\"control-label\"> Select asset type </label>' + '<div class=\"controls\">' + '<a ng-click="AssetTypeTreePopUp(\'' + file.id + '\')">{{AssetCategory.treestructure_' + file.id + '}}</a>' + '</div>' + '<div class=\"controls\">' + '<select ui-select2 data-ng-model="assetfields.assettype_' + file.id + '"  ng-change="onDamtypechange(\'' + file.id + '\')" > <option ng-repeat="ndata in  AssetCategory.AssetTypesList_' + file.id + '"  value="{{ndata.Id}}" >{{ndata.damCaption}}</option></select>' + '<span class="imgTypeSection">' + '<span class=\"imgTypeEditIcon\" ng-click="AssetTypeTreePopUp(\'' + file.id + '\')">' + '<img src="assets/img/treeIcon.png"' + '</span>' + '</span>' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Asset name </label>' + '<div class=\"controls\">' + '<input class=\"txtbx\" type="text"  data-ng-model="assetfields.assetname_' + file.id + '" name="DescVal_' + file.id + '" id="desc_' + file.id + '" placeholder="Name">' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Description </label>' + '<div class=\"controls\">' + '<textarea class=\"small-textarea\"  name=\"assetfields.TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" ng-model=\"assetfields.TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" id=\"TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" placeholder="Description" rows="3"></textarea>' + '</div>' + '</div>' + '<div id = "div_' + file.id + '">' + $scope.dyn_ContAsset + '</div>' + '</form>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>')($scope));
                            if (o.Mime.mimes[ste.toLowerCase()] == undefined) {


                                var img = $('<img id="' + file.id + '">');
                                img.attr('src', imagesrcpath + "DAMFiles/StaticPreview_small/" + ste.toUpperCase() + ".jpg");
                                img.attr("onerror", "this.onerror=null; this.src=" + imagesrcpath + "'DAMFiles/Preview/NoPreview.jpg';");
                                img.appendTo('#Preview_' + file.id);
                            } else {
                                if (o.Mime.mimes[ste.toLowerCase()].split('/')[0] == 'image') {
                                    var img = new o.Image();
                                    img.onload = function () {
                                        var li = document.createElement('div');
                                        li.id = this.uid;
                                        document.getElementById('Preview_' + file.id).appendChild(li);
                                        this.embed(li.id, {
                                            width: 80,
                                            height: 80,
                                            crop: true
                                        });
                                    };
                                    img.load(file.getSource());
                                } else {

                                    var img = $('<img id="' + file.id + '">');
                                    img.attr('src', imagesrcpath + "DAMFiles/StaticPreview_small/" + ste.toUpperCase() + ".jpg");
                                    img.appendTo('#Preview_' + file.id);
                                }
                            }
                        });
                    });
                    up.refresh();
                    $timeout(function () {
                        TotalAssetUploadProgress();
                    }, 500);
                });
                uploaderAssetadmin.bind('UploadProgress', function (up, file) {
                    $("#totalAssetActivityAttachProgresstaskadmin").show();
                    $('#' + file.id + " .bar").css("width", file.percent + "%");
                    $('#' + file.id + " .size").html(plupload.formatSize(Math.round(file.size * (file.percent / 100))) + ' / ' + plupload.formatSize(file.size));
                    $('#' + file.id).attr('data-size-uploaded', Math.round(file.size * (file.percent / 100)));
                    TotalAssetUploadProgress();
                });
                uploaderAssetadmin.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploaderAssetadmin.bind('FileUploaded', function (up, file, response) {
                    $('#' + file.id).attr('data-status', 'true');
                    var ind = FileAttributes.FileID.indexOf(file.id);
                    var fileext = "." + file.name.split('.').pop();
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    if ($scope.newTaskid > 0) {
                        $scope.SaveAssetFileDetails(file, response.response, FileAttributes.AttributeDataList[ind]);
                        TotalAssetUploadProgress();
                    }
                });
                uploaderAssetadmin.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKey = (TenantFilePath + "DAMFiles/Original/" + file.id + fileext).replace(/\\/g, '/');;
                    uploaderAssetadmin.settings.multipart_params.key = uniqueKey;
                    uploaderAssetadmin.settings.multipart_params.Filename = uniqueKey;
                });
            }
            else {
                $('.moxie-shim').remove();
                uploaderAssetadmin = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickassetsAlternatetaskadmin',
                    container: 'assetscontainerTaskadmin',
                    max_file_size: '10000mb',
                    url: amazonURL + cloudsetup.BucketName,
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    urlstream_upload: true,
                    multiple_queues: true,
                    file_data_name: 'file',
                    multipart: true,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature
                    }
                });
                uploaderAssetadmin.bind('Init', function (up, params) {
                    uploaderAssetadmin.splice();
                });
                uploaderAssetadmin.init();
                uploaderAssetadmin.bind('FilesAdded', function (up, files) {
                    totalfilecount = totalfilecount + files.length;
                    $.each(files, function (i, file) {
                        var damid = 0;
                        $scope.Fileids.push(file.id);
                        var ste = file.name.split('.')[file.name.split('.').length - 1];
                        if (ste != undefined) ste = ste.toLowerCase();
                        var damCount = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                            return e.damExtention == ste
                        }).length;
                        if (damCount > 0) {
                            var selecteddamtype = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                                return e.damExtention == ste
                            });
                        } else {
                            var wildcardExt = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                                return e.damExtention.indexOf("*") != -1
                            });
                            var iswildcard = [];
                            iswildcard = $.grep(wildcardExt, function (e) {
                                return e.damExtention.substring(0, e.damExtention.length - 1).indexOf(ste) != -1
                            });
                            if (iswildcard.length > 0) var selecteddamtype = iswildcard;
                        }
                        $scope.AllDamTypes = [];
                        if (selecteddamtype != undefined && selecteddamtype != null) {
                            damid = selecteddamtype[0].Id;
                            var ownerID = parseInt($scope.OwnerID, 10);
                        }
                        AdminTaskService.GetAssetCategoryTree(damid).then(function (data) {
                            if (data.Response != null) {
                                $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                $scope.AssetCategory["filterval_" + file.id] = '';
                                var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                $("#dynamicAssetTypeTreeInLibrary").html($compile(htmltree)($scope));
                            }
                        });
                        $scope.AssetCategory["treestructure_" + file.id] = '-';
                        $scope.AssetCategory["AssetTypesList_" + file.id] = [];
                        AdminTaskService.GetAssetCategoryPathInAssetEdit(damid).then(function (datapath) {
                            if (datapath.Response != null) {
                                if (datapath.Response[1].AssetTypeIDs.length > 0) {
                                    $scope.AssetCategory["treestructure_" + file.id] = datapath.Response[0].CategoryPath;
                                    var tempval = [];
                                    var tempassty = "";
                                    for (var k = 0; k < datapath.Response[1].AssetTypeIDs.length; k++) {
                                        tempassty = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                                            return e.Id == datapath.Response[1].AssetTypeIDs[k]
                                        });
                                        if (tempassty != undefined && tempassty.length > 0) tempval.push(tempassty[0]);
                                    }
                                    if (tempval.length > 0) $scope.AssetCategory["AssetTypesList_" + file.id] = tempval;
                                } else $scope.fields["assettype_" + fileUniqueID] = '';
                            }
                        });
                        AdminTaskService.GetDamAttributeRelation(damid).then(function (attrreldata) {
                            $scope.dyn_ContAsset = "";
                            if (attrreldata.Response != undefined && attrreldata.Response != null) {
                                $scope.dyn_ContAsset = "";
                                $scope.DAMattributesRelationList = attrreldata.Response;
                                FileAttributes.FileID.push(file.id);
                                FileAttributes.AttributeDataList.push($scope.DAMattributesRelationList);
                                FileAttributes.AssetType.push(damid);
                                for (var i = 0; i < $scope.DAMattributesRelationList.length; i++) {
                                    if ($scope.DAMattributesRelationList[i].TypeID == 1) {
                                        if ($scope.DAMattributesRelationList[i].ID != 70) {
                                            if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.Name) {
                                                $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                                $scope.assetfields["TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                            } else {
                                                $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                                $scope.assetfields["TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                            }
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].TypeID == 3) {
                                        if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.Owner) {
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" dirownernameautopopulate placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.OwnerList[0].UserName;
                                        } else if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.FiscalYear) {
                                            $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"> <option value=\"\"> Select " + $scope.DAMattributesRelationList[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                        } else {
                                            $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"> <option value=\"\"> Select " + $scope.DAMattributesRelationList[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].TypeID == 2) {
                                        $scope.dyn_ContAsset += "<div class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"assetfields.TextMultiLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" name=\"assetfields.TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-model=\"assetfields.TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\" rows=\"3\"></textarea></div></div>";
                                        $scope.assetfields["TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                    } else if ($scope.DAMattributesRelationList[i].TypeID == 4) {
                                        $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = [];
                                        $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                        var defaultmultiselectvalue = $scope.DAMattributesRelationList[i].DefaultValue.split(',');
                                        if ($scope.DAMattributesRelationList[i].DefaultValue != "") {
                                            for (var v = 0, val; val = defaultmultiselectvalue[v++];) {
                                                $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID].push(parseInt(val));
                                            }
                                        } else {
                                            $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = [];
                                        }
                                        $scope.dyn_ContAsset += "<div class=\"control-group attachmentMultiselect\"><label class=\"control-label\" for=\"assetfields.ListMultiSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select class=\"multiselect\" multiple=\"multiple\"  multiselect-dropdown data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  > </select></div></div>";
                                    }
                                }
                            }
                            $scope.assetfields["assetname_" + file.id] = file.name;
                            if (selecteddamtype != undefined && selecteddamtype != null) {
                                var dataid = '';
                                var rec = [];
                                rec = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                                    return e.Id == selecteddamtype[0].Id
                                });
                                if (rec.length > 0) dataid = rec[0].Id;
                                if ($scope.AssetCategory["AssetTypesList_" + file.id].length > 0) {
                                    $scope.assetfields["assettype_" + file.id] = dataid;
                                    $scope.oldfiledetails["AssetType_" + file.id] = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                                        return e.Id == selecteddamtype[0].Id
                                    })[0].Id;
                                }
                            } else {
                                $scope.assetfields["assettype_" + file.id] = '';
                                $scope.oldfiledetails["AssetType_" + file.id] = '';
                            }
                            $('#assetlistActivityTaskadmin').append($compile('<div id="' + file.id + '" class="attachmentBox" Data-role="Attachment" data-size="' + file.size + '" data-size-uploaded="0" data-status="false">' + '<div class="row-fluid">' + '<div class="span12">' + '<div class="span2 leftSection">' + ' <i id="icon_ok' + file.id + '" class="icon-ok-sign" style="display:none;"  ></i>' + ' <div class="fileType-Preview">' + ' <div id="Preview_' + file.id + '" class="fileType-PreviewDiv">' + ' </div>' + '</div>' + ' <div class="progress progress-striped active">' + ' <div style="width: 0%" class="bar"></div>' + '</div>' + ' <div class="progressInfo">' + '  <span class="progress-percent"></span>' + '<span class="fileSize">' + plupload.formatSize(file.size) + '' + '</span>' + '</div>' + '</div>' + ' <div class="span10 rightSection">' + '<div class="info">' + '  <span class="name">' + ' <i class="icon-file-alt"></i>' + file.name + '</span>' + ' <span class="size">' + ' <i class="icon-remove removefile" data-fileid="' + file.id + '"></i>' + ' </span>' + '</div>' + '<div  class="formInfo">' + ' <form class="form-float">' + '<div class=\"control-group fullSpace\">' + '<label class=\"control-label\"> Select asset type </label>' + '<div class=\"controls\">' + '<a ng-click="AssetTypeTreePopUp(\'' + file.id + '\')">{{AssetCategory.treestructure_' + file.id + '}}</a>' + '</div>' + '<div class=\"controls\">' + '<select ui-select2 data-ng-model="assetfields.assettype_' + file.id + '"  ng-change="onDamtypechange(\'' + file.id + '\')" > <option ng-repeat="ndata in  AssetCategory.AssetTypesList_' + file.id + '"  value="{{ndata.Id}}" >{{ndata.damCaption}}</option></select>' + '<span class="imgTypeSection">' + '<span class=\"imgTypeEditIcon\" ng-click="AssetTypeTreePopUp(\'' + file.id + '\')">' + '<img src="assets/img/treeIcon.png"' + '</span>' + '</span>' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Asset name </label>' + '<div class=\"controls\">' + '<input class=\"txtbx\" type="text"  data-ng-model="assetfields.assetname_' + file.id + '" name="DescVal_' + file.id + '" id="desc_' + file.id + '" placeholder="Name">' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Description </label>' + '<div class=\"controls\">' + '<textarea class=\"small-textarea\"  name=\"assetfields.TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" ng-model=\"assetfields.TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" id=\"TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" placeholder="Description" rows="3"></textarea>' + '</div>' + '</div>' + '<div id = "div_' + file.id + '">' + $scope.dyn_ContAsset + '</div>' + '</form>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>')($scope));
                            if (o.Mime.mimes[ste.toLowerCase()] == undefined) {


                                var img = $('<img id="' + file.id + '">');
                                img.attr('src', imagesrcpath + "DAMFiles/StaticPreview_small/" + ste.toUpperCase() + ".jpg");
                                img.attr("onerror", "this.onerror=null; this.src=" + imagesrcpath + "'DAMFiles/Preview/NoPreview.jpg';");
                                img.appendTo('#Preview_' + file.id);
                            } else {
                                if (o.Mime.mimes[ste.toLowerCase()].split('/')[0] == 'image') {
                                    var img = new o.Image();
                                    img.onload = function () {
                                        var li = document.createElement('div');
                                        li.id = this.uid;
                                        document.getElementById('Preview_' + file.id).appendChild(li);
                                        this.embed(li.id, {
                                            width: 80,
                                            height: 80,
                                            crop: true
                                        });
                                    };
                                    img.load(file.getSource());
                                } else {

                                    var img = $('<img id="' + file.id + '">');
                                    img.attr('src', imagesrcpath + "DAMFiles/StaticPreview_small/" + ste.toUpperCase() + ".jpg");
                                    img.appendTo('#Preview_' + file.id);
                                }
                            }
                        });
                    });
                    up.refresh();
                    $timeout(function () {
                        TotalAssetUploadProgress();
                    }, 500);
                });
                uploaderAssetadmin.bind('UploadProgress', function (up, file) {
                    $("#totalAssetActivityAttachProgresstaskadmin").show();
                    $('#' + file.id + " .bar").css("width", file.percent + "%");
                    $('#' + file.id + " .size").html(plupload.formatSize(Math.round(file.size * (file.percent / 100))) + ' / ' + plupload.formatSize(file.size));
                    $('#' + file.id).attr('data-size-uploaded', Math.round(file.size * (file.percent / 100)));
                    TotalAssetUploadProgress();
                });
                uploaderAssetadmin.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploaderAssetadmin.bind('FileUploaded', function (up, file, response) {
                    $('#' + file.id).attr('data-status', 'true');
                    var ind = FileAttributes.FileID.indexOf(file.id);
                    var fileext = "." + file.name.split('.').pop();
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    if ($scope.newTaskid > 0) {
                        $scope.SaveAssetFileDetails(file, response.response, FileAttributes.AttributeDataList[ind]);
                        TotalAssetUploadProgress();
                    }
                });
                uploaderAssetadmin.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKey = (TenantFilePath + "DAMFiles/Original/" + file.id + fileext).replace(/\\/g, '/');;
                    uploaderAssetadmin.settings.multipart_params.key = uniqueKey;
                    uploaderAssetadmin.settings.multipart_params.Filename = uniqueKey;
                });
            }


            var dom = {
                uploaderAssetadmin: $("#uploaderAssetadmin"),
                uploads: $("ul.uploads")
            };
            $('#admintempTaskCreation').click(function (e) {
                var str = $('#admintaskuploaded').text().replace('Upload Successful.', '');
                $('#admintaskuploaded').text(str);
                if ($("#assetlistActivityTaskadmin > div").length > 0 && $scope.addtaskvallid > 0) {
                    if ($scope.UniqueidsfrBlankAsset.length + $scope.Fileids.length > 0 && $("#assetlistActivityTaskadmin > div").length > 0) {
                        var count = 0;
                        var namecount = 0;
                        $scope.uploadingInProgress = true;
                        angular.forEach($scope.UniqueidsfrBlankAsset, function (key) {
                            if ($scope.assetfields['assettype_' + key] == '' || $scope.assetfields['assettype_' + key] == null || $scope.assetfields['assettype_' + key] == undefined) {
                                count = count + 1;
                            }
                            if ($scope.assetfields['assetname_' + key] == '' || $scope.assetfields['assetname_' + key] == null || $scope.assetfields['assetname_' + key] == undefined) {
                                namecount = namecount + 1;
                            }
                        });
                        angular.forEach($scope.Fileids, function (key) {
                            if ($scope.assetfields['assettype_' + key] == '' || $scope.assetfields['assettype_' + key] == null || $scope.assetfields['assettype_' + key] == undefined) {
                                count = count + 1;
                            }
                            if ($scope.assetfields['assetname_' + key] == '' || $scope.assetfields['assetname_' + key] == null || $scope.assetfields['assetname_' + key] == undefined) {
                                namecount = namecount + 1;
                            }
                        });
                        if (count == 0 && namecount == 0) {
                            $('#CreateNewTaskBtn').attr('disabled', 'disabled');
                            $('#CreateNewTaskBtn').css('pointer-events', 'none');
                            $('#UpdateTaskBtn').attr('disabled', 'disabled');
                            $('#UpdateTaskBtn').css('pointer-events', 'none');
                            $('#pickassetsAlternatetaskadmin').attr('disabled', 'disabled');
                            $('#pickassetsAlternatetaskadmin').css('pointer-events', 'none');
                            if ($scope.UniqueidsfrBlankAsset.length > 0) {
                                if (saveBlankAssetsCalled == false) {
                                    saveBlankAssetsCalled = true;
                                    saveBlankAssets();
                                }
                            }
                            $scope.adminaddfromComputer = true;
                            uploaderAssetadmin.start();
                            e.preventDefault();
                        } else {
                            bootbox.alert($translate.instant('LanguageContents.Res_4594.Caption'));
                            $scope.uploadingInProgress = false;
                            $('#CreateNewTaskBtn').removeAttr('disabled', 'disabled');
                            $('#CreateNewTaskBtn').css('pointer-events', 'auto');
                            $('#UpdateTaskBtn').removeAttr('disabled', 'disabled');
                            $('#UpdateTaskBtn').css('pointer-events', 'auto');
                            return false;
                        }
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_4563.Caption'));
                        $scope.uploadingInProgress = false;
                        return false;
                    }
                }
            });
            $('#assetlistActivityTaskadmin').on('click', '.removefile', function () {
                var fileToRemove = $(this).attr('data-fileid');
                var ind = $scope.UniqueidsfrBlankAsset.indexOf(fileToRemove);
                if (ind != -1) {
                    $('#' + fileToRemove).remove();
                    $scope.UniqueidsfrBlankAsset.splice(ind, 1);
                    totalfilecount = totalfilecount - 1;
                    TotalCount = TotalCount - 1;
                    $('#totalAssetActivityAttachProgresstaskadmin .count').html(0 + ' of ' + $('#assetlistActivityTaskadmin .attachmentBox').length + ' Uploaded');
                } else {
                    $.each(uploaderAssetadmin.files, function (i, file) {
                        if (file.id == fileToRemove) {
                            uploaderAssetadmin.removeFile(file);
                            $('#' + file.id).remove();
                            var ind1 = $scope.Fileids.indexOf(file.id);
                            $scope.Fileids.splice(ind1, 1);
                            totalfilecount = totalfilecount - 1;
                            TotalCount = TotalCount - 1;
                            $('#totalAssetActivityAttachProgresstaskadmin .count').html(0 + ' of ' + $('#assetlistActivityTaskadmin .attachmentBox').length + ' Uploaded');
                        }
                    });
                }
                TotalAssetUploadProgress();
            });


        }
        function TotalAssetUploadProgress() {
            var TotalSize = 0;
            var UploadedSize = 0;
            var UploadedCount = 0;
            $('#assetlistActivityTaskadmin .attachmentBox').each(function () {
                TotalSize += parseInt($(this).attr('data-size'));
                UploadedSize += parseInt($(this).attr('data-size-uploaded'));
                if ($(this).attr('data-status') == 'true') {
                    UploadedCount += 1;
                }
            });
            $('#totalAssetActivityAttachProgresstaskadmin .count').html(UploadedCount + ' of ' + $('#assetlistActivityTaskadmin .attachmentBox').length + ' Uploaded');
            $('#totalAssetActivityAttachProgresstaskadmin .size').html(plupload.formatSize(UploadedSize) + ' / ' + plupload.formatSize(TotalSize));
            $('#totalAssetActivityAttachProgresstaskadmin .bar').css("width", Math.round(((UploadedSize / TotalSize) * 100)) + "%");
            if ((UploadedCount == $('#assetlistActivityTaskadmin .attachmentBox').length) && UploadedCount > 0 && $('#assetlistActivityTaskadmin .attachmentBox').length > 0) {
                $timeout(function () {
                    $('#totalAssetActivityAttachProgresstaskadmin #admintaskuploaded').html('Upload Successful.');
                }, 100);
            }
        }
        $scope.onDamtypechange = function (fileID) {
            var oldAssettype = $scope.oldfiledetails["AssetType_" + fileID];
            var newAssettype = $scope.assetfields['assettype_' + fileID];
            $('#div_' + fileID).html('');
            if (oldAssettype != '') {
                AdminTaskService.GetDamAttributeRelation(oldAssettype).then(function (attrList) {
                    var attrreldata = attrList.Response;
                    var ind = FileAttributes.FileID.indexOf(fileID);
                    FileAttributes.FileID.splice(ind, 1);
                    FileAttributes.AttributeDataList.splice(ind, 1);
                    FileAttributes.AssetType.splice(ind, 1);
                    for (var i = 0; i < attrreldata.length; i++) {
                        if (attrreldata[i].AttributeTypeID == 1) {
                            if (attrreldata[i].ID != 70) {
                                if (attrreldata[i].ID == SystemDefiendAttributes.Name) {
                                    delete $scope.assetfields["TextSingleLine_" + fileID + attrreldata[i].ID];
                                } else {
                                    delete $scope.assetfields["TextSingleLine_" + fileID + attrreldata[i].ID];
                                }
                            }
                        } else if (attrreldata[i].AttributeTypeID == 3) {
                            if (attrreldata[i].ID == SystemDefiendAttributes.Owner) {
                                delete $scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID];
                            } else if (attrreldata[i].ID == SystemDefiendAttributes.FiscalYear) {
                                delete $scope.OptionObj["option_" + fileID + attrreldata[i].ID];
                                delete $scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID];
                            } else {
                                delete $scope.OptionObj["option_" + fileID + attrreldata[i].ID];
                                delete $scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID];
                            }
                        } else if (attrreldata[i].AttributeTypeID == 2) {
                            delete $scope.assetfields["TextMultiLine_" + fileID + attrreldata[i].ID];
                        } else if (attrreldata[i].AttributeTypeID == 4) {
                            delete $scope.OptionObj["option_" + fileID + attrreldata[i].ID];
                            delete $scope.assetfields["ListMultiSelection_" + fileID + attrreldata[i].ID];
                        }
                    }
                });
            }
            AdminTaskService.GetDamAttributeRelation(newAssettype).then(function (attrList) {
                var attrreldata = attrList.Response;
                var newHTML = '';
                FileAttributes.FileID.push(fileID);
                FileAttributes.AttributeDataList.push(attrreldata);
                FileAttributes.AssetType.push(newAssettype);
                for (var i = 0; i < attrreldata.length; i++) {
                    if (attrreldata[i].AttributeTypeID == 1) {
                        if (attrreldata[i].ID != 70) {
                            if (attrreldata[i].ID == SystemDefiendAttributes.Name) {
                                newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + fileID + attrreldata[i].ID + "\" id=\"TextSingleLine_" + fileID + attrreldata[i].ID + "\" placeholder=\"" + attrreldata[i].Caption + "\"></div></div>";
                                $scope.assetfields["TextSingleLine_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
                            } else {
                                newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + fileID + attrreldata[i].ID + "\" id=\"TextSingleLine_" + fileID + attrreldata[i].ID + "\" placeholder=\"" + attrreldata[i].Caption + "\"></div></div>";
                                $scope.assetfields["TextSingleLine_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
                            }
                        }
                    } else if (attrreldata[i].AttributeTypeID == 3) {
                        if (attrreldata[i].ID == SystemDefiendAttributes.Owner) {
                            newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"> <input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.ListSingleSelection_" + fileID + attrreldata[i].ID + "\"  id=\"ListSingleSelection_" + fileID + attrreldata[i].ID + "\" dirownernameautopopulate placeholder=\"" + attrreldata[i].Caption + "\"></div></div>";
                            $scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID] = $scope.OwnerList[0].UserName;
                        } else if (attrreldata[i].ID == SystemDefiendAttributes.FiscalYear) {
                            $scope.OptionObj["option_" + fileID + attrreldata[i].ID] = attrreldata[i].Options;
                            newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].AttributeID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + fileID + attrreldata[i].ID + "\"  id=\"ListSingleSelection_" + fileID + attrreldata[i].ID + "\"> <option value=\"\"> Select " + attrreldata[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + fileID + attrreldata[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                            $scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
                        } else {
                            $scope.OptionObj["option_" + fileID + attrreldata[i].ID] = attrreldata[i].Options;
                            newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + fileID + attrreldata[i].ID + "\"  id=\"ListSingleSelection_" + fileID + attrreldata[i].ID + "\"> <option value=\"\"> Select " + attrreldata[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + fileID + attrreldata[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                            $scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
                        }
                    } else if (attrreldata[i].AttributeTypeID == 2) {
                        if (attrreldata[i].ID != 3) newHTML += "<div class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"assetfields.TextMultiLine_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].ID + "\" name=\"assetfields.TextMultiLine_" + fileID + attrreldata[i].ID + "\" ng-model=\"assetfields.TextMultiLine_" + fileID + attrreldata[i].ID + "\" id=\"TextMultiLine_" + fileID + attrreldata[i].ID + "\" placeholder=\"" + attrreldata[i].Caption + "\" rows=\"3\"></textarea></div></div>";
                        $scope.assetfields["TextMultiLine_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
                    } else if (attrreldata[i].AttributeTypeID == 4) {
                        $scope.OptionObj["option_" + fileID + attrreldata[i].ID] = attrreldata[i].Options;
                        newHTML += "<div class=\"control-group attachmentMultiselect\"><label class=\"control-label\" for=\"assetfields.ListMultiSelection_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"> <select class=\"multiselect\" multiple=\"multiple\"  multiselect-dropdown data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListMultiSelection_" + fileID + attrreldata[i].ID + "\"  id=\"ListMultiSelection_" + fileID + attrreldata[i].ID + "\" ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + fileID + attrreldata[i].ID + "\"  > </select></div></div>";
                        $scope.assetfields["ListMultiSelection_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
                    }
                }
                $scope.oldfiledetails["AssetType_" + fileID] = newAssettype;
                $('#div_' + fileID).append($compile(newHTML)($scope));
            });
        }
        $scope.dyn_ContAsset = '';
        $scope.fieldoptions = [];
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.assetfields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });
        }
        $scope.DAMattributesRelationList = [];
        $scope.AttributeData = [];
        var FileAttributes = {
            "FileID": [],
            "AttributeDataList": [],
            "AssetType": []
        };
        $scope.SaveAsset = [];
        $scope.oldfiledetails = {};
        $scope.SaveAssetFileDetails = function (file, response, fileAttributes) {
            if ($scope.assetfields['assettype_' + file.id] == undefined || $scope.assetfields['assettype_' + file.id] == null || $scope.assetfields['assettype_' + file.id] == '') {
                bootbox.alert($translate.instant('LanguageContents.Res_2511.Caption'));
                $scope.uploadingInProgress = false;
                return false;
            }
            var AssetTypeID = $scope.assetfields['assettype_' + file.id];
            var AssetName = $scope.assetfields['assetname_' + file.id];
            var extension = file.name.substring(file.name.lastIndexOf("."));
            var resultArr = response.split(",");
            var AssetFileGuid = file.id;
            var AssetAttribute = fileAttributes;
            $scope.AttributeData = [];
            $scope.assetfields['TextMultiLine_' + AssetFileGuid + 3] = ($scope.assetfields['TextMultiLine_' + AssetFileGuid + 3] == undefined) ? '' : $scope.assetfields['TextMultiLine_' + AssetFileGuid + 3];
            $scope.AttributeData.push({
                "AttributeID": "3",
                "AttributeCaption": "Description",
                "AttributeTypeID": "2",
                "NodeID": $scope.assetfields['TextMultiLine_' + AssetFileGuid + 3],
                "Level": 0
            });
            for (var i = 0; i < AssetAttribute.length; i++) {
                if (AssetAttribute[i].AttributeTypeID == 3) {
                    if (AssetAttribute[i].ID == SystemDefiendAttributes.Owner) {
                        $scope.AttributeData.push({
                            "AttributeID": AssetAttribute[i].AttributeID,
                            "AttributeCaption": AssetAttribute[i].Caption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                            "Level": 0
                        });
                    } else if ($scope.assetfields['ListSingleSelection_' + AssetFileGuid + AssetAttribute[i].ID] != undefined && $scope.assetfields['ListSingleSelection_' + AssetFileGuid + AssetAttribute[i].ID] != "") {
                        var value = $scope.assetfields['ListSingleSelection_' + AssetFileGuid + AssetAttribute[i].ID];
                        $scope.AttributeData.push({
                            "AttributeID": AssetAttribute[i].AttributeID,
                            "AttributeCaption": AssetAttribute[i].Caption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                            "Level": 0
                        });
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 1) {
                    if (AssetAttribute[i].ID == SystemDefiendAttributes.Name) $scope.entityName = $scope.assetfields['TextSingleLine_' + AssetFileGuid + AssetAttribute[i].ID];
                    else {
                        $scope.AttributeData.push({
                            "AttributeID": AssetAttribute[i].AttributeID,
                            "AttributeCaption": AssetAttribute[i].Caption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": ($scope.assetfields['TextSingleLine_' + AssetFileGuid + AssetAttribute[i].ID] != null) ? $scope.assetfields['TextSingleLine_' + AssetFileGuid + AssetAttribute[i].ID].toString() : "",
                            "Level": 0
                        });
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 2) {
                    $scope.AttributeData.push({
                        "AttributeID": AssetAttribute[i].AttributeID,
                        "AttributeCaption": AssetAttribute[i].Caption,
                        "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                        "NodeID": ($scope.assetfields['TextMultiLine_' + AssetFileGuid + AssetAttribute[i].ID] != null) ? $scope.assetfields['TextMultiLine_' + AssetFileGuid + AssetAttribute[i].ID].toString() : "",
                        "Level": 0
                    });
                } else if (AssetAttribute[i].AttributeTypeID == 5 && AssetAttribute[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                    var MyDate = new Date();
                    var MyDateString;
                    if (AssetAttribute[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        MyDateString = $scope.fields["DatePart_" + AssetAttribute[i].AttributeID];
                    } else {
                        MyDateString = "";
                    }
                    $scope.AttributeData.push({
                        "AttributeID": AssetAttribute[i].AttributeID,
                        "AttributeCaption": AssetAttribute[i].Caption,
                        "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                        "NodeID": ($scope.assetfields['TextMultiLine_' + AssetFileGuid + AssetAttribute[i].ID] != null) ? $scope.assetfields['TextMultiLine_' + AssetFileGuid + AssetAttribute[i].ID].toString() : "",
                        "Level": 0
                    });
                } else if (AssetAttribute[i].AttributeTypeID == 4) {
                    if ($scope.assetfields['ListMultiSelection_' + AssetFileGuid + AssetAttribute[i].ID] != "" && $scope.assetfields['ListMultiSelection_' + AssetFileGuid + AssetAttribute[i].ID] != undefined) {
                        if ($scope.assetfields['ListMultiSelection_' + AssetFileGuid + AssetAttribute[i].ID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + AssetFileGuid + AssetAttribute[i].ID];
                            for (var k = 0; k < multiselectiObject.length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": AssetAttribute[i].AttributeID,
                                    "AttributeCaption": AssetAttribute[i].Caption,
                                    "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                    "NodeID": parseInt(multiselectiObject[k], 10),
                                    "Level": 0
                                });
                            }
                        }
                    }
                }
            }
            $scope.SaveAsset = [];
            $scope.SaveAsset.push({
                "Typeid": AssetTypeID,
                "Active": true,
                "FolderID": 0,
                "AttributeData": $scope.AttributeData,
                "Status": 1,
                "MimeType": resultArr[1],
                "Size": file.size,
                "FileGuid": resultArr[0],
                "Extension": extension,
                "Name": AssetName,
                "VersionNo": 1,
                "FileName": file.name,
                "Description": file.name,
                "EntityID": $scope.newTaskid
            });
            AdminTaskService.SaveAsset($scope.SaveAsset).then(function (result) {
                document.getElementById('icon_ok' + file.id).style.display = 'block';
                filecount = filecount + 1;
                if (result.Response == 0) {
                    NotifyError($translate.instant('LanguageContents.Res_4331.Caption'));
                    $('#CreateNewTask').css('display', 'none');
                    $('#pickassetsAlternatetaskadmin').css('display', 'none');
                    $('#createblankAsset').css('display', 'none');
                    totalfilecount = 0;
                    filecount = 0;
                    $scope.UniqueidsfrBlankAsset = [];
                    saveBlankAssetsCalled = false;
                } else {
                    $scope.SaveAsset = [];
                    TotalCount = TotalCount + 1;
                    if (totalfilecount == filecount) {
                        $('#CreateNewTask').css('display', 'none');
                        $('#pickassetsAlternatetaskadmin').css('display', 'none');
                        $('#createblankAsset').css('display', 'none');
                        $('#pickassetsAlternatetaskadmin').removeAttr('disabled', 'disabled');
                        $('#pickassetsAlternatetaskadmin').css('pointer-events', 'auto');
                        $('#pickassetsAlternatetaskadmin').css('display', 'block');
                        $('#CreateNewTaskBtn').removeAttr('disabled', 'disabled');
                        $('#CreateNewTaskBtn').css('pointer-events', 'auto');
                        $('#UpdateTaskBtn').removeAttr('disabled', 'disabled');
                        $('#UpdateTaskBtn').css('pointer-events', 'auto');
                        totalfilecount = 0;
                        filecount = 0;
                        $scope.UniqueidsfrBlankAsset = [];
                        $scope.Fileids = [];
                        var FileAttributes = {
                            "FileID": [],
                            "AttributeDataList": [],
                            "AssetType": []
                        };
                        NotifySuccess($translate.instant('LanguageContents.Res_4820.Caption'));
                        RereshTaskObj($scope.returnObj);
                        $scope.FileList = [];
                        $scope.SelectedTaskLIstID = 0;
                        $scope.AttachmentFilename = [];
                        refreshModel();
                        $scope.newTaskid = 0;
                        $scope.returnObj = [];
                        $scope.uploadingInProgress = false;
                        $('#assetlistActivityTaskadmin').empty();
                        $("#totalAssetActivityAttachProgresstaskadmin").empty();
                        $('#addTask').modal('hide');
                    }
                }
            });
        }
        $scope.fieldsfrDet = {
            usersID: ''
        };
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fieldsfrDet, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }

        function generateUUID() {
            var d = new Date.create().getTime();
            var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = (d + Math.random() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
            });
            return uuid;
        };
        $scope.tree = {};
        $scope.fieldKeys = [];
        $scope.options = {};
        $scope.fileguid = '';
        $scope.UniqueidsfrBlankAsset = [];
        $scope.processingsrcobj = {
            processingid: $scope.TaskBriefDetails.taskID,
            processingplace: "task",
            processinglock: false
        };

        function GetEntityTaskAttachmentinfo(taskID) {
            AdminTaskService.GetEntityTaskAttachmentinfo(taskID).then(function (TaskAttachmentinfo) {
                if (TaskAttachmentinfo.Response != null) {
                    $scope.TaskBriefDetails.Totalassetcount = TaskAttachmentinfo.Response[0].totalcount;
                    $scope.TaskBriefDetails.Totalassetsize = parseSize(TaskAttachmentinfo.Response[0].totalfilesize);
                }
            });
            if ($scope.currenttasklistid > 0 && $scope.tasklistcurrentindex != -1) {
                ReloadTasklist()
            }
        }

        function ReloadTasklist() {
            if ($scope.currenttasklistid > 0 && $scope.tasklistcurrentindex != -1) {
                AdminTaskService.GetAdminTasksByTaskListID($scope.currenttasklistid).then(function (GetMytasksResult) {
                    var taskListData = [];
                    for (var j = 0, taskobj; taskobj = GetMytasksResult.Response[j++];) {
                        if (taskobj.Description == "NULL") taskobj.Description = "";
                        taskListData.push({
                            Id: taskobj.Id,
                            Caption: taskobj.Caption,
                            Description: taskobj.Description,
                            ColorCode: taskobj.colorcode,
                            ShortDesc: taskobj.ShortDescription,
                            SortOder: taskobj.SortOder,
                            TaskListID: taskobj.TaskListID,
                            TaskType: taskobj.TaskType,
                            TaskID: taskobj.TaskID,
                            TaskTypeName: taskobj.TaskTypeName,
                            TotalTaskAttachment: taskobj.TotalTaskAttachment,
                            AttributeData: [],
                            AdminTaskCheckList: []
                        });
                    }
                    $scope.TaskLibraryList[$scope.tasklistcurrentindex].TaskList = taskListData;
                    $scope.TaskLibraryList[$scope.tasklistcurrentindex].TaskCount = GetMytasksResult.Response.length;
                });
            }
        }
        $scope.$on('Taskassetinfoupdate', function (event) {
            $timeout(function () {
                GetEntityTaskAttachmentinfo($scope.SelectedTaskID);
            }, 100);
        });
        $scope.deleteOne = function (item) {
            $scope.lastSubmit.splice($.inArray(item, $scope.lastSubmit), 1);
            $scope.items.splice($.inArray(item, $scope.items), 1);
        };
        $scope.set_color = function (clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            }
            else
                return '';
        }

        function generateUUID() {
            var d = new Date().getTime();
            var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = (d + Math.random() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
            return uuid;
        };
        $scope.Export = function (taskListID, taskCount, index) {
            $('#ExportTasklistReportPageModel').modal("show");
            $scope.taskLibID = taskListID;
            $scope.filename = "";
            var dt = new Date();
            $scope.filename = dt.getDate() + "/" + dt.getMonth() + 1 + "/" + dt.getFullYear() + ":" + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
            var NewGuid = generateUUID();
            var currentindex = index;
            var CurrentId = taskListID;
            $scope.currenttasklistid = taskListID;
            $scope.tasklistcurrentindex = index;
            var taskObjs = $.grep($scope.TaskLibraryList, function (e) {
                return e.ID == taskListID;
            })[0];
            $scope.Caption = taskObjs.LibraryName;
            AdminTaskService.ExportTaskList(taskListID, NewGuid, $scope.Caption).then(function (GetExportValuesRes) {
                var taskListData = [];
                if (GetExportValuesRes.Response != null) {
                    var a = document.createElement('a'),
						fileid = NewGuid,
						extn = '.zip';
                    var filename = "TaskLibrary " + $scope.filename + extn;
                    a.href = 'DAMDownload.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '';
                    document.body.appendChild(a);
                    a.click();
                    $('#ExportTasklistReportPageModel').modal("hide");
                    NotifySuccess($translate.instant('LanguageContents.Res_4804.Caption'));
                } else {
                    $('#ExportTasklistReportPageModel').modal("hide");
                    NotifyError($translate.instant('LanguageContents.Res_5825.Caption'));
                }
            });
        }
        $scope.ImportPopUp = function (Id, indx) {
            $scope.currenttasklistid = Id;
            $scope.tasklistcurrentindex = indx;
            $("#ImportingVal").click();
            $scope.ImpPopup();
        }
        var imgfileid = '';
        $scope.ImpPopup = function () {
            $('.moxie-shim').remove();
            var uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus',
                browse_button: 'ImportingVal',
                container: 'container',
                max_file_size: '10mb',
                url: 'Handlers/CustomHandler.ashx?Path=Files/ImportExportFiles&typeoffile=Doc',
                flash_swf_url: 'assets/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/js/plupload/Moxie.xap',
                filters: [{
                    title: "Compressed files",
                    extensions: "zip"
                }],
                resize: {
                    width: 320,
                    height: 240,
                    quality: 90
                }
            });
            uploader.bind('Init', function (up, params) {
                $('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
            });
            uploader.init();
            uploader.bind('FilesAdded', function (up, files) {
                up.refresh();
                uploader.start();
            });
            uploader.bind('Error', function (up, err) {
                $('#filelist').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                up.refresh();
            });
            uploader.bind('FileUploaded', function (up, file, response) {
                $scope.FileID = response.response.split(',')[0];
                Importing($scope.FileID);
            });
        };

        function Importing(val) {
            $('#ImportTasklistReportPageModel').modal("show");
            var rst = {};
            rst.FileImport = val;
            rst.LangTypeId = $scope.currenttasklistid;
            AdminTaskService.InsertTaskLibImport(rst).then(function (addoptionsResponse) {
                if (addoptionsResponse.StatusCode == 200 && addoptionsResponse.Response == "Success") {
                    $('#ImportTasklistReportPageModel').modal("hide");
                    if (!$("#tasklist" + $scope.currenttasklistid).hasClass('icon-caret-right')) {
                        $("#TaskHolder" + $scope.currenttasklistid).css('display', 'none');
                        $("#tasklist" + $scope.currenttasklistid).addClass('icon-caret-right');
                    }
                    ReloadTasklist();
                    NotifySuccess($translate.instant('LanguageContents.Res_4804.Caption'));
                } else {
                    ReloadTasklist();
                    $('#ImportTasklistReportPageModel').modal("hide");
                    NotifyError(addoptionsResponse.Response);
                }
            });
        }
        $scope.AssetTypeTreePopUp = function (fileid) {
            $scope.AssetCategory["filterval_" + fileid] = '';
            var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + fileid + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + fileid + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
            $("#dynamicAssetTypeTreeInLibrary").html($compile(htmltree)($scope));
            $scope.FileIdForAssetCategory = fileid;
            $("#AssetCategoryInLibrary").modal('show');
        };
        $scope.AssetTypeCategorySelection = function (branch, parentArr) {
            var i = 0;
            if (branch.ischecked == true) {
                if ($scope.assetfields["assettype_" + $scope.FileIdForAssetCategory].length > 0) {
                    branch.ischecked = false;
                    bootbox.alert($translate.instant('LanguageContents.Res_5115.Caption'));
                    return false;
                } else {
                    $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory] = "";
                    for (i = parentArr.length - 1; i >= 0; i--) {
                        $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory] += parentArr[i].Caption + "/";
                    }
                    if ($scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory].length > 0) $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory] = $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory].substring(0, $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory].length - 1);
                    var tempval = [];
                    var tempasst = "";
                    for (var k = 0; k < parentArr[0].Children.length; k++) {
                        tempasst = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                            return e.Id == parentArr[0].Children[k].id
                        });
                        if (tempasst != undefined && tempasst.length > 0) tempval.push(tempasst[0]);
                    }
                    if (tempval.length > 0) {
                        $scope.AssetCategory["AssetTypesList_" + $scope.FileIdForAssetCategory] = tempval;
                        $scope.assetfields["assettype_" + $scope.FileIdForAssetCategory] = branch.id;
                    }
                }
                $("#AssetCategoryInLibrary").modal('hide');
            } else {
                $('#div_' + $scope.FileIdForAssetCategory).html($compile("")($scope));
                $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory] = "-";
                $scope.assetfields["assettype_" + $scope.FileIdForAssetCategory] = '';
                $scope.AssetCategory["AssetTypesList_" + $scope.FileIdForAssetCategory].splice(0, $scope.AssetCategory["AssetTypesList_" + $scope.FileIdForAssetCategory].length);
            }
        }
        AdminTaskService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
            if (CurrencyListResult.Response != null) $scope.CurrencyFormatsList = CurrencyListResult.Response;
        });
        $scope.Getamountentered = function (atrid) {
            if (1 == $scope.fields["ListSingleSelection_" + atrid]) $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '');
            else $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '') / 1 / $scope['currRate_' + atrid];
        }
        $scope.GetCostCentreCurrencyRateById = function (atrid) {
            AdminTaskService.GetCostCentreCurrencyRateById(0, $scope.fields["ListSingleSelection_" + atrid], true).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope['currRate_' + atrid] = parseFloat(resCurrencyRate.Response[1]);
                    if ($scope['origninalamountvalue_' + atrid] != 0) {
                        $scope.fields["dTextSingleLine_" + atrid] = (parseFloat($scope['origninalamountvalue_' + atrid]) * $scope['currRate_' + atrid]).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    }
                }
            });
        }
        $scope.Getamountenteredforedit = function (atrid) {
            if (1 == $scope['NormalDropDown_' + atrid]) $scope['origninalamountvalue_' + atrid] = $scope['SingleTextValue_' + atrid].replace(/ /g, '');
            else $scope['origninalamountvalue_' + atrid] = $scope['SingleTextValue_' + atrid].replace(/ /g, '') / 1 / $scope.currRate;
        }
        $scope.GetCostCentreCurrencyRateByIdforedit = function (atrid) {
            AdminTaskService.GetCostCentreCurrencyRateById(0, $scope['NormalDropDown_' + atrid], true).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope.currRate = parseFloat(resCurrencyRate.Response[1]);
                    if ($scope['origninalamountvalue_' + atrid] != 0) {
                        $scope['SingleTextValue_' + atrid] = (parseFloat($scope['origninalamountvalue_' + atrid]) * $scope.currRate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    }
                }
            });
        }
        $scope.templateData = {
            "TemplateList": [],
            PhaseData: {
                AllPhaseList: [],
                PhaseList: [{
                    "ID": 0,
                    "Name": "",
                    "Description": "",
                    "Colorcode": ""
                }],
                StepsData: {
                    StepList: [],
                    "StepID": 0,
                    "Name": "",
                    "Description": "",
                    "Stepduration": "(+2 days)",
                    "StepMinApproval": 2,
                    "Steproles": "0",
                    "IsMandatory": 1,
                    "PhaseId": 0
                }
            },
            templateSrcData: {
                "Name": "",
                "Description": "",
                "Templateid": 0,
                "ID": 0
            },
            UserRoles: [],
            phaseTempData: {},
            StepTempData: {},
            SortOrder: {}
        };
        $scope.PhaseDuration = [{
            ID: 1,
            Caption: "+1 days"
        }, {
            ID: 2,
            Caption: "+2 days"
        }, {
            ID: 3,
            Caption: "+3 days"
        }, {
            ID: 4,
            Caption: "+4 days"
        }, {
            ID: 5,
            Caption: "+5 days"
        }, {
            ID: 6,
            Caption: "+6 days"
        }, {
            ID: 7,
            Caption: "+1 week"
        }, {
            ID: 14,
            Caption: "+2 weeks"
        }, {
            ID: 21,
            Caption: "+3 weeks"
        }, {
            ID: 30,
            Caption: "+1 month"
        }];
        $scope.PhaseMinApproval = ["1", "2", "3", "4", "5", "6", "7", "10", "15", "20", "25", "30"];
        $scope.selectedAlltemplates = false;
        $scope.thumbnailActionMenu = false;
        $scope.approvalflowTempPopup = false;
        GetAllTemplates();
        GetAllUserRoles();

        function GetAllTemplates() {
            AdminTaskService.GetAllApprovalFlowTemplates().then(function (Templatesdetails) {
                $scope.templateData.templateSrcData = Templatesdetails.Response;
            });
        }
        $scope.templateData.PhaseData.AllPhaseList = [];

        function getphasewithsteps(phaseid) {
            AdminTaskService.GetAllApprovalFlowPhasesSteps(phaseid).then(function (data) {
                if (data != null) {
                    FormSourceData(data.Response);
                } else {
                }
            });
        }

        function xmlToJson(xml) {
            var obj = {};
            if (xml.nodeType == 1) {
                if (xml.attributes.length > 0) {
                    obj["@attributes"] = {};
                    for (var j = 0; j < xml.attributes.length; j++) {
                        var attribute = xml.attributes.item(j);
                        obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
                    }
                }
            } else if (xml.nodeType == 3) {
                obj = xml.nodeValue;
            }
            if (xml.hasChildNodes()) {
                for (var i = 0; i < xml.childNodes.length; i++) {
                    var item = xml.childNodes.item(i);
                    var nodeName = item.nodeName;
                    if (typeof (obj[nodeName]) == "undefined") {
                        obj[nodeName] = xmlToJson(item);
                    } else {
                        if (typeof (obj[nodeName].push) == "undefined") {
                            var old = obj[nodeName];
                            obj[nodeName] = [];
                            obj[nodeName].push(old);
                        }
                        obj[nodeName].push(xmlToJson(item));
                    }
                }
            }
            return obj;
        }

        function FormSourceData(data) {
            var PhaseData = [],
				steps = [];
            $scope.templateData.PhaseData.AllPhaseList = [];
            for (var i = 0, val; val = data[i++];) {
                steps = [];
                var stepjson = xmlToJson($.parseXML(val.steps));
                if (stepjson != null) {
                    if (stepjson.A.p != null) {
                        if (Object.prototype.toString.call(stepjson.A.p) !== '[object Array]') {
                            steps.push({
                                "Name": stepjson.A.p['Name']["#text"],
                                "ID": stepjson.A.p['ID']["#text"],
                                "MinApproval": stepjson.A.p['MinApproval']["#text"],
                                "Duration": stepjson.A.p['Duration']["#text"],
                                "Description": stepjson.A.p['Description'] == null ? "" : stepjson.A.p['Description']["#text"],
                                "PhaseId": stepjson.A.p['PhaseId']["#text"],
                                "Roles": stepjson.A.p['Roles']["#text"],
                                IsMandatory: Boolean(stepjson.A.p['IsMandatory']["#text"].toString().toLowerCase() == "1" ? true : false),
                                isopened: false
                            });
                        } else {
                            for (var j = 0, st; st = stepjson.A.p[j++];) {
                                steps.push({
                                    "Name": st['Name']["#text"],
                                    "ID": st['ID']["#text"],
                                    "MinApproval": st['MinApproval']["#text"],
                                    "Duration": st['Duration']["#text"],
                                    "Description": st['Description'] == null ? "" : st['Description']["#text"],
                                    "PhaseId": st['PhaseId']["#text"],
                                    "Roles": st['Roles']["#text"],
                                    IsMandatory: st['IsMandatory']["#text"],
                                    isopened: false
                                });
                            }
                        }
                    }
                }
                val.StepsData = steps;
                PhaseData.push(val);
            }
            $scope.templateData.PhaseData.AllPhaseList.push(PhaseData);
        }
        $scope.templateData.templateSrcData.Colorcode = 'ffffff';
        $scope.ColorOptions = {
            preferredFormat: "hex",
            showInput: true,
            showAlpha: false,
            allowEmpty: true,
            showPalette: true,
            showPaletteOnly: false,
            togglePaletteOnly: true,
            togglePaletteMoreText: 'more',
            togglePaletteLessText: 'less',
            showSelectionPalette: true,
            chooseText: "Choose",
            cancelText: "Cancel",
            showButtons: true,
            clickoutFiresChange: true,
            palette: [
				["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
				["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)", "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
				["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)", "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)", "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)", "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
            ]
        };
        $scope.colorchange = function (color) { };
        $scope.checkAllTemplates = function () {
            if ($scope.selectedAlltemplates) {
                $scope.selectedAlltemplates = false;
            } else {
                $scope.selectedAlltemplates = true;
            }
            angular.forEach($scope.templateData.templateSrcData, function (item) {
                item.Selected = $scope.selectedAlltemplates;
            });
        };
        $scope.RemoveCheckallSelection = function (list, $event) {
            var checkbox = $event.target;
            list.Selected = checkbox.checked;
            var TemaplateCollection = $.grep($scope.templateData.templateSrcData, function (e) {
                return e.Selected == true;
            }).length;
            if (TemaplateCollection != $scope.templateData.templateSrcData.length) $scope.selectedAlltemplates = false;
            else $scope.selectedAlltemplates = true;
        }
        $scope.AddNewStep = function (phaseid) {
            $scope.TaskTemplateStepName = '';
            $scope.TaskTemplateStepDescription = '';
            $scope.TaskTemplateSteproles = 0;
            $scope.TaskTemplateStepduration = 1;
            $scope.TaskTemplateStepMinApproval = 1;
            $('#NewTaskTemplateStep').modal('show');
            $scope.templateData.PhaseData.StepsData.PhaseId = phaseid;
            $scope.templateData.PhaseData.StepsData.StepID = GenarateUniqueId();
        };
        $scope.SelectTemplatePopup = function () {
            $('#existingTemplatePopup').modal('show');
        }
        $scope.OpenCreateTaskTemplatePhasePopup = function () {
            $('#NewTaskTemplatePhase').modal('show');
        }
        $scope.OpenCreateTaskTemplateMemberPopup = function () {
            $('#selectApproverPopup').modal('show');
        }
        $scope.dragoverCallback = function (event, index, external, type) {
            $scope.logListEvent('dragged over', event, index, external, type);
            return index > 0;
        };
        $scope.dropCallback = function (event, index, item, external, type, allowedType) {
            $scope.logListEvent('dropped at', event, index, external, type);
            if (external) {
                if (allowedType === 'itemType' && !item.label) return false;
                if (allowedType === 'containerType' && !angular.isArray(item)) return false;
            }
            var Dest_Phase_Details = this.phase;
            var sortorder = {};
            if (allowedType == "itemType") {
                if (Dest_Phase_Details.ID == item.PhaseId) {
                    sortorder.PhaseID = item.PhaseId;
                    sortorder.Phase = true;
                    sortorder.StepID = item.ID;
                    $scope.templateData.SortOrder.push(sortorder);
                } else {
                    sortorder.PhaseID = Dest_Phase_Details.ID;
                    sortorder.Phase = false;
                    sortorder.StepID = item.ID;
                    $scope.templateData.SortOrder.push(sortorder);
                }
            } else {
                sortorder.PhaseID = item.ID;
                $scope.templateData.SortOrder.push(sortorder);
            }
            return item;
        };
        $scope.logEvent = function (message, event) { };
        $scope.logListEvent = function (action, event, index, external, type) {
            var message = external ? 'External ' : '';
            message += type + ' element is ' + action + ' position ' + index;
            $scope.logEvent(message, event);
        };

        function GetAllUserRoles() {
            AdminTaskService.GetApprovalRoles().then(function (Result) {
                $scope.templateData.UserRoles = [];
                if (Result.Response.length != null) {
                    for (var i = 0, role; role = Result.Response[i++];) {
                        $scope.templateData.UserRoles.push({
                            "Id": role.ID,
                            "Caption": role.Caption,
                            "Description": role.Description
                        });
                    }
                }
            });
        }
        $scope.AddStep = function (buttontype) {
            var tempStepID = 0;
            if (buttontype == 1) {
                tempStepID = GenarateUniqueId();
                $scope.templateData.PhaseData.StepsData.Name = $scope.TaskTemplateStepName;
                $scope.templateData.PhaseData.StepsData.Description = $scope.TaskTemplateStepDescription;
                $scope.templateData.PhaseData.StepsData.Stepduration = $scope.TaskTemplateStepduration;
                $scope.templateData.PhaseData.StepsData.StepMinApproval = $scope.TaskTemplateStepMinApproval;
                $scope.templateData.PhaseData.StepsData.Steproles = $scope.TaskTemplateSteproles;
                $scope.templateData.PhaseData.StepsData.IsMandatory = true;
            } else {
                $scope.templateData.PhaseData.StepsData.Name = $scope.step.Name;
                $scope.templateData.PhaseData.StepsData.Description = $scope.step.Description;
                $scope.templateData.PhaseData.StepsData.Stepduration = $scope.step.Duration;
                $scope.templateData.PhaseData.StepsData.StepMinApproval = $scope.step.MinApproval;
                $scope.templateData.PhaseData.StepsData.Steproles = $scope.step.Roles;
                $scope.templateData.PhaseData.StepsData.IsMandatory = true;
            }
            $('#NewTaskTemplateStep').modal('hide');
            NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
            var record = {
                "PhaseId": $scope.templateData.PhaseData.StepsData.PhaseId,
                "Name": $scope.TaskTemplateStepName,
                "Description": $scope.TaskTemplateStepDescription,
                "Duration": $scope.TaskTemplateStepduration,
                "MinApproval": $scope.TaskTemplateStepMinApproval,
                "Roles": $scope.templateData.PhaseData.StepsData.Steproles,
                IsMandatory: true,
                "ID": 0,
                isopened: false,
                tempStepID: tempStepID
            };
            var srcphase = $.grep($scope.templateData.PhaseData.AllPhaseList[0], function (rel) {
                return rel.ID == $scope.templateData.PhaseData.StepsData.PhaseId;
            })[0];
            if (srcphase != null) {
                if ($scope.templateData.PhaseData.StepsData.StepID == 0) {
                    var srcstep = $.grep(srcphase.StepsData, function (rel) {
                        return rel.tempStepID == $scope.templateData.PhaseData.StepsData.tempStepID;
                    })[0];
                } else {
                    var srcstep = $.grep(srcphase.StepsData, function (rel) {
                        return rel.ID == $scope.templateData.PhaseData.StepsData.StepID;
                    })[0];
                }
                if (srcstep != null) {
                    var stepIndex = srcphase.StepsData.indexOf(srcstep);
                    srcphase.StepsData[stepIndex].ID = $scope.templateData.PhaseData.StepsData.StepID;
                    srcphase.StepsData[stepIndex].Name = $scope.TaskTemplateStepName;
                    srcphase.StepsData[stepIndex].Description = $scope.TaskTemplateStepDescription;
                    srcphase.StepsData[stepIndex].MinApproval = $scope.TaskTemplateStepMinApproval;
                    srcphase.StepsData[stepIndex].Duration = $scope.TaskTemplateStepduration;
                    srcphase.StepsData[stepIndex].Roles = $scope.templateData.PhaseData.StepsData.Steproles;
                    srcphase.StepsData[stepIndex].IsMandatory = true;
                    srcphase.StepsData[stepIndex].isopened = false;
                    srcphase.StepsData[stepIndex].tempStepID = $scope.templateData.PhaseData.StepsData.tempStepID;;
                } else {
                    srcphase.StepsData.push(record);
                }
            }
            var phaseMaxduration = 0;
            for (var i = 0; i < srcphase.StepsData.length; i++) {
                phaseMaxduration = parseInt(srcphase.StepsData[i].Duration) > phaseMaxduration ? parseInt(srcphase.StepsData[i].Duration) : phaseMaxduration;
            }
            srcphase.maxduration = phaseMaxduration;
        }
        var length = 0;
        $scope.AddNewPhase = function () {
            var tempPhaseID = GenarateUniqueId();
            var tempStepID = GenarateUniqueId();
            var StepsData = { "Name": "Stage 1", "Description": "Default Stage", "Duration": 1, "ID": 0, "tempStepID": tempStepID, "MinApproval": 1, "Roles": 1, "IsMandatory": true, "PhaseId": 0, "tempPhaseID": tempPhaseID };
            if ($scope.templateData.PhaseData.AllPhaseList.length > 0) {
                length = $scope.templateData.PhaseData.AllPhaseList[0].length;
                var record = { "ColorCode": "", Description: $scope.Phasedescription, ID: 0, Name: $scope.Phasename, EntityID: 0, SortOrder: 0, StepsData: [StepsData], maxduration: 1, tempPhaseID: tempPhaseID };
                $scope.templateData.PhaseData.AllPhaseList[0].push(record);
            } else {
                length = $scope.templateData.PhaseData.AllPhaseList.length;
                var record = [{ "ColorCode": "", Description: $scope.Phasedescription, ID: 0, Name: $scope.Phasename, EntityID: 0, SortOrder: 0, StepsData: [StepsData], maxduration: 1, tempPhaseID: tempPhaseID }];
                $scope.templateData.PhaseData.AllPhaseList.push(record);
            }
            $scope.templateData.PhaseData.StepsData.PhaseId = length;
            $('#NewTaskTemplatePhase').modal('hide');
            $scope.Phasename = "";
            $scope.Phasedescription = "";
            $scope.Phasecolorcode = "";
            $scope.approvalflowTempPopup = true;
            GetAllUserRoles();
        }

        function UpdateTemplateSortOrder() {
            if ($scope.templateData.SortOrder.length > 0) {
                AdminTaskService.UpdateAdminTasklistSortOrder($scope.templateData.SortOrder).then(function (res) {
                    if (res.Response) NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                });
            }
        }

        function UpdateTemplatePhaseSortOrder() {
            if ($scope.templateData.SortOrder.length > 0) {
                AdminTaskService.UpdateAdmintaskPhaseSortOrder($scope.templateData.SortOrder).then(function (res) {
                    if (res.Response) NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                });
            }
        }
        $scope.UpdateStep = function () {
            $scope.templateData.PhaseData.StepsData.StepID = $scope.templateData.StepTempData.ID == (null || undefined) ? 0 : $scope.templateData.StepTempData.ID;
            $('#NewTaskTemplateStep').modal('show');
            var phasedetails = $.grep($scope.templateData.PhaseData.AllPhaseList[0], function (rel) {
                return rel.tempPhaseID == $scope.templateData.StepTempData.tempPhaseID;
            })[0];
            var stepdetails = $.grep(phasedetails.StepsData, function (rel) {
                return rel.ID == $scope.templateData.StepTempData.ID;
            })[0];
            $scope.TaskTemplateStepName = stepdetails.Name;
            $scope.TaskTemplateStepDescription = stepdetails.Description;
            $scope.TaskTemplateStepMinApproval = stepdetails.MinApproval;
            $scope.TaskTemplateStepduration = stepdetails.Duration;
            var roles = stepdetails.Roles;
            $scope.TaskTemplateSteproles = roles;
            $scope.TaskTemplateIsMandatory = true;
            $scope.templateData.PhaseData.StepsData.PhaseId = $scope.templateData.StepTempData.PhaseId;
            $scope.templateData.PhaseData.StepsData.TemplateID = phasedetails.TemplateID;
            $scope.templateData.PhaseData.StepsData.tempStepID = 0;
            if ($scope.templateData.StepTempData.tempStepID != null && $scope.templateData.StepTempData.tempStepID != undefined) {
                $scope.templateData.PhaseData.StepsData.tempStepID = $scope.templateData.StepTempData.tempStepID;
            }
        };
        $scope.DeleteStep = function () {
            var srcphase = $.grep($scope.templateData.PhaseData.AllPhaseList[0], function (rel) {
                return rel.ID == $scope.templateData.StepTempData.PhaseId;
            })[0];
            if (srcphase.StepsData.length > 1) {
                bootbox.confirm($translate.instant('LanguageContents.Res_5736.Caption')+'?', function (result) {
                    if (result) {
                        var stepIndex = srcphase.StepsData.indexOf($scope.templateData.StepTempData);
                        srcphase.StepsData.splice(stepIndex, 1);
                        var phaseMaxduration = 0;
                        for (var i = 0; i < srcphase.StepsData.length; i++) {
                            phaseMaxduration = parseInt(srcphase.StepsData[i].Duration) > phaseMaxduration ? parseInt(srcphase.StepsData[i].Duration) : phaseMaxduration;
                        }
                        srcphase.maxduration = phaseMaxduration;
                        if ($scope.templateData.StepTempData.ID != 0) {
                            AdminTaskService.DeletePhaseStep($scope.templateData.StepTempData.ID, srcphase.EntityID).then(function (result) {
                            })
                        } else {
                            NotifySuccess($translate.instant('LanguageContents.Res_5802.Caption'));
                        }
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_5737.Caption'));
            }
        }
        $scope.menuclick = function (data) {
            $scope.templateData.StepTempData = data;
        }
        $scope.AddExistingTemplate = function () {
            var SelectedTemplates = $.grep($scope.templateData.templateSrcData, function (e) {
                return e.Selected == true;
            });
            if (SelectedTemplates.length == 0) {
                NotifyError($translate.instant('LanguageContents.Res_5826.Caption'));
            } else {
                $scope.templateData.PhaseData.AllPhaseList.push(SelectedTemplates);
                for (var i = 0; i < SelectedTemplates.length; i++) {
                    getphasewithsteps(SelectedTemplates[i].ID);
                }
                $('#existingTemplatePopup').modal('hide');
                $scope.approvalflowTempPopup = true;
            }
        }

        function GetEntityApprovalTaskTemplatedetails(EntityID) {
            AdminTaskService.GetAllAdminApprovalFlowPhasesSteps(EntityID).then(function (TaskDetails) {
                if (TaskDetails.Response != null) {
                    FormSourceData(TaskDetails.Response);
                }
            });
        }
        $scope.showPhaseAdminContext = false;
        $scope.phaseContextmenuAdmin = function (phaseObj) {
            $scope.CurrPhaseObj = phaseObj;
            $scope.showPhaseAdminContext = true;
        }
        $scope.RemovePhase = function (phaseObj) {
            bootbox.confirm($translate.instant('LanguageContents.Res_5738.Caption')+'?', function (result) {
                if (result) {
                    if (phaseObj.ID != 0) {
                        AdminTaskService.RemovePhase(phaseObj.ID, phaseObj.EntityID).then(function (result) {
                            if (result.Response == true) {
                                var srcPhase = $.grep($scope.templateData.PhaseData.AllPhaseList[0], function (e) {
                                    return e.ID == phaseObj.ID
                                })
                                var PhaseIndex = $scope.templateData.PhaseData.AllPhaseList[0].indexOf(srcPhase);
                                $scope.templateData.PhaseData.AllPhaseList[0].splice(PhaseIndex, 1);
                                NotifySuccess($translate.instant('LanguageContents.Res_5803.Caption'));
                            } else {
                                NotifyError($translate.instant('LanguageContents.Res_5804.Caption'));
                            }
                        })
                    } else {
                        var srcPhase = $.grep($scope.templateData.PhaseData.AllPhaseList[0], function (e) {
                            return e.tempPhaseID == phaseObj.tempPhaseID
                        })
                        var PhaseIndex = $scope.templateData.PhaseData.AllPhaseList[0].indexOf(srcPhase[0]);
                        $scope.templateData.PhaseData.AllPhaseList[0].splice(PhaseIndex, 1);
                    }
                }
            })
        }

        function StrartUpload_UploaderAttrAdmin() {
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                $('.moxie-shim').remove();
                var uploader_UplAttr = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAttrTaskCreate',
                    container: 'filescontainerooTaskCreate',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: amazonURL + cloudsetup.BucketName,
                    multi_selection: false,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }
                });
                uploader_UplAttr.bind('Init', function (up, params) { });
                uploader_UplAttr.init();
                uploader_UplAttr.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_UplAttr.start();
                });
                uploader_UplAttr.bind('UploadProgress', function (up, file) { });
                uploader_UplAttr.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_UplAttr.bind('FileUploaded', function (up, file, response) {
                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    if ($scope.istaskCreate) {
                        UpdateImagescope(file, response.response);
                    }
                    else {
                        SaveFileDetailsTaskEdit(file, response.response)
                    }
                });
                uploader_UplAttr.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKey = (TenantFilePath + "UploadedImages/Temp/" + file.id + fileext).replace(/\\/g, "\/");
                    uploader_UplAttr.settings.multipart_params.key = uniqueKey;
                    uploader_UplAttr.settings.multipart_params.Filename = uniqueKey;
                });
            }
            else {

                $('.moxie-shim').remove();
                var uploader_UplAttr = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAttrTaskCreate',
                    container: 'filescontainerooTaskCreate',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: 'Handlers/UploadUploaderImage.ashx?Type=Attachment',
                    chunk: '64Kb',
                    multi_selection: false,
                    multipart_params: {}

                });
                uploader_UplAttr.bind('Init', function (up, params) { });

                uploader_UplAttr.init();
                uploader_UplAttr.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_UplAttr.start();
                });
                uploader_UplAttr.bind('UploadProgress', function (up, file) { });
                uploader_UplAttr.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_UplAttr.bind('FileUploaded', function (up, file, response) {
                    var fileinfo = response.response.split(",");
                    response.response = fileinfo[0].split(".")[0] + "," + GetMIMEType(fileinfo[0]) + "," + '.' + fileinfo[0].split(".")[1];
                    if ($scope.istaskCreate) {
                        UpdateImagescope(file, response.response);
                    }
                    else {
                        SaveFileDetailsTaskEdit(file, response.response)
                    }
                });
                uploader_UplAttr.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
            }
        }



        function SaveFileDetailsTaskEdit(file, response) {
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");
            var uplaodImageObject = {};
            uplaodImageObject.Name = file.name;
            uplaodImageObject.VersionNo = 1;
            uplaodImageObject.MimeType = resultArr[1];
            uplaodImageObject.Extension = extension;
            uplaodImageObject.OwnerID = $cookies['UserId'];
            uplaodImageObject.CreatedOn = Date.now();
            uplaodImageObject.Checksum = "";
            uplaodImageObject.ModuleID = 1;
            uplaodImageObject.taskID = $scope.SelectedTaskID;
            uplaodImageObject.AttributeID = $scope.UploadAttributeId;
            uplaodImageObject.Size = file.size;
            uplaodImageObject.FileName = resultArr[0] + extension;
            var PreviewID = "UploaderPreview1_" + $scope.UploadAttributeId;
            AdminTaskService.copyuploadedImage(uplaodImageObject.FileName).then(function (ImgRes) {
                if (ImgRes.Response != null) {
                    uplaodImageObject.FileName = ImgRes.Response;
                    AdminTaskService.UpdateImageNameforTask(uplaodImageObject).then(function (uplaodImageObjectResult) {
                        $('#UplaodImagediv').modal('hide');
                        if (uplaodImageObjectResult.Response) {
                            NotifySuccess($translate.instant('LanguageContents.Res_4808.Caption'));
                            $('#' + PreviewID).attr('src', imagesrcpath + 'UploadedImages/' + uplaodImageObject.FileName);

                        }
                    });
                }
            })
        }

        setTimeout(GetAllExtensionTypesforDAM, 100);


    }
    app.controller("mui.admin.tasklistlibraryCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$window', '$compile', '$attrs', '$sce', '$translate', 'AdminTaskService', muiadmintasklistlibraryCtrl]);
})(angular, app);