﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Dal.Base;
using NHibernate;

namespace BrandSystems.Marcom.Dal.User
{
   public class UserRepository:GenericRepository
    {
        public UserRepository(ISessionFactory sessionFactory) : base(sessionFactory)
        {
        }
    }
}
