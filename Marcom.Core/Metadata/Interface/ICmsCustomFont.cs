﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface ICmsCustomFont
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        int ProviderID
        {
            get;
            set;

        }

        string KitID
        {
            get;
            set;

        }
        #endregion
    }
}
