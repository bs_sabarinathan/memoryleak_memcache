﻿(function (ng, app) {
    function muiplanningtoolcalendercalendercreationCtrl($scope, $location, $resource, $timeout, $cookies, $window, CalendercreationService, $compile, $translate, $modalInstance) {

        $scope.WizardPosition = 1;
        window["tid_wizard_steps_all_complete_count"] = 0;
        window["tid_wizard_steps_all_complete"] = setInterval(function () {
            KeepAllStepsMarkedComplete();
        }, 25);
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.calender.calendercreationCtrl']"));
        });
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            $scope.enddate1 = false;
            model = model1;
        };
        if ($scope.Isfromcalender_mui == false) {
            $("#stepCal_2").hide();
            $("#tabStep_stepCal_2").hide();
            $("#spanstep2member").text('2');
        }
        $scope.Calender = {
            ngCalenderName: '',
            ngCalenderDescription: '',
            ngCalenderPublishedOn: '',
            ngCalenderVisType: 0,
            ngCalenderisExternal: false
        }
        $scope.OpenCalenderCreationPopup = function () {
            GetAllTabCollections();
            $scope.Calender = {
                ngCalenderName: '',
                ngCalenderDescription: '',
                ngCalenderPublishedOn: '',
                ngCalenderVisPeriod: 1,
                ngCalenderVisType: 0,
                ngCalenderisExternal: false
            }
            $scope.FulfillEntites = '';
            initialSetup();
            ValidCalenderTab1();
            //   $("#CalenderMetadataModel").modal("show");
        };

        function initialSetup() {
            $scope.tempCount = 1;
            var totalWizardSteps = $('#totnumstepstotnumstepstotnumstepstotnumsteps').wizard('totnumsteps').totstep;
            $scope.OwnerList = [];
            $scope.WizardPosition = 1;
            $("#btnWizardCalenderFinish").removeAttr('disabled');
            $('#CalenderWizard').wizard('stepLoaded');
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $('#btnWizardCalenderFinish').hide();
            $('#btnWizardCalenderNext').show();
            $('#btnWizardCalenderPrev').hide();
            $scope.Calender.ngCalenderName = '';
            $scope.Calender.ngCalenderDescription = '';
            $scope.Calender.ngCalenderPublishedOn = '';
            $scope.Calender.ngCalenderVisPeriod = 1;
            $scope.Calender.ngCalenderVisType = 0;
            $scope.Calender.ngCalenderisExternal = false;
            CalendercreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
                $scope.owner = Getowner.Response;
                $scope.QuickInfo1AttributeCaption = $scope.owner.QuickInfo1AttributeCaption;
                $scope.QuickInfo2AttributeCaption = $scope.owner.QuickInfo2AttributeCaption;
                $scope.OwnerList.push({
                    "Roleid": 1,
                    "RoleName": "Owner",
                    "UserEmail": $scope.ownerEmail,
                    "DepartmentName": $scope.owner.Designation,
                    "Title": $scope.owner.Title,
                    "Userid": parseInt($scope.OwnerID, 10),
                    "UserName": $scope.OwnerName,
                    "IsInherited": '0',
                    "InheritedFromEntityid": '0',
                    "QuickInfo1": $scope.owner.QuickInfo1,
                    "QuickInfo2": $scope.owner.QuickInfo2
                });
            });
            CalendercreationService.GetEntityTypeRoleAccess(10).then(function (role) {
                $scope.Roles = role.Response;
            });
            var objOwnername = $cookies['Username'];
            $scope.OwnerName = objOwnername;
            $scope.Calender.ngCalenderOwner = $scope.OwnerName;
        }
        $scope.TabCollectionsList = null;

        function GetAllTabCollections() {
            CalendercreationService.GetAllTabCollections(6).then(function (result) {
                if (result.Response != null) {
                    $scope.TabCollectionsList = result.Response;
                }
            });
        };

        function ValidCalenderTab1() {
            var detailTabValues = [
				['#CalenderName', 'presence', 'Please Enter the Name'],
				['#CalenderDescription', 'presence', 'Please Enter the Description'],
				['#CalenderVisPeriod', 'min-num:0', 'Type in a number that is bigger than 0'],
				['#CalenderVisPeriod', 'presence', 'Please Enter a number']
            ];
            setTimeout(function () {
                $("#CalenderTablMetadata").nod(detailTabValues, {
                    'delay': 200,
                    'submitBtnSelector': '#btnTemp1',
                    'silentSubmit': 'true'
                });
            }, 200);
        }
        $scope.LoadObjectivePopuP = function () {
            $scope.Objective = {};
        }
        var totalWizardSteps = $('#CalenderWizard').wizard('totnumsteps').totstep;
        $('#btnWizardCalenderFinish').hide();
        $('#btnWizardCalenderNext').show();
        $('#btnWizardCalenderPrev').hide();
        window["tid_wizard_steps_all_complete_count"] = 0;
        window["tid_wizard_steps_all_complete"] = setInterval(function () {
            KeepAllStepsMarkedComplete();
        }, 25);
        $scope.UserimageNewTime = new Date().getTime().toString();

        function KeepAllStepsMarkedComplete() {
            $("#CalenderWizard ul.steps").find("li").addClass("complete");
            $("#CalenderWizard ul.steps").find("span.badge").addClass("badge-success");
            window["tid_wizard_steps_all_complete_count"]++;
            if (window["tid_wizard_steps_all_complete_count"] >= 20) {
                clearInterval(window["tid_wizard_steps_all_complete"]);
            }
        }
        $scope.changeTab = function () {
            if ($scope.Calender.ngCalenderPublishedOn == "" || $scope.Calender.ngCalenderPublishedOn == null || $scope.Calender.ngCalenderPublishedOn == undefined) {
                e.stopImmediatePropagation();
                e.stopPropagation();
                bootbox.alert($translate.instant('LanguageContents.Res_5767.Caption') + $translate.instant('LanguageContents.Res_4669.Caption'));
                return false;
            }
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            var currentWizardStep = $('#CalenderWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#CalenderWizard').wizard('totnumsteps').totstep;
            if ($scope.Isfromcalender_mui == false && totalWizardSteps > 1) {
                totalWizardSteps = totalWizardSteps - 1;
                if (currentWizardStep == 3) {
                    $("#stepCal_3").show();
                } else {
                    $("#stepCal_3").hide();
                }
            }
            if (currentWizardStep === 1) {
                $('#btnWizardCalenderNext').show();
                $('#btnWizardCalenderPrev').hide();
                $('#btnWizardCalenderFinish').hide();
            } else if (currentWizardStep === totalWizardSteps) {
                $('#btnWizardCalenderNext').hide();
                $('#btnWizardCalenderPrev').show();
                $('#btnWizardCalenderFinish').show();
            } else {
                $('#btnWizardCalenderNext').show();
                $('#btnWizardCalenderPrev').show();
                $('#btnWizardCalenderFinish').hide();
            }
        }
        $scope.changeTab2 = function (e) {
            $("#btnTemp1").click();
            $("#CalenderTablMetadata").removeClass('notvalidate');
            if ($("#CalenderTablMetadata .error").length > 0) {
                e.stopImmediatePropagation();
                e.stopPropagation();
                return false;
            }
            if ($scope.Calender.ngCalenderPublishedOn == "" || $scope.Calender.ngCalenderPublishedOn == null || $scope.Calender.ngCalenderPublishedOn == undefined) {
                e.stopImmediatePropagation();
                e.stopPropagation();
                bootbox.alert($translate.instant('LanguageContents.Res_5767.Caption') + $translate.instant('LanguageContents.Res_4669.Caption'));
                return false;
            }
        }
        $scope.changePrevCalenderTab = function () {
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            var currentWizardStep = $('#CalenderWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#CalenderWizard').wizard('totnumsteps').totstep;
            $('#CalenderWizard').wizard('previous', '');
            currentWizardStep = $('#CalenderWizard').wizard('selectedItem').step;
            if ($scope.Isfromcalender_mui == false && totalWizardSteps > 1) {
                totalWizardSteps = totalWizardSteps - 1;
                if (currentWizardStep == 2) {
                    $("#stepCal_3").show();
                } else {
                    $("#stepCal_3").hide();
                }
            }
            if (currentWizardStep === 1) {
                $('#btnWizardCalenderPrev').hide();
                $('#btnWizardCalenderFinish').hide();
            }
            if (currentWizardStep < totalWizardSteps) {
                $('#btnWizardCalenderNext').show();
                $('#btnWizardCalenderFinish').hide();
            } else {
                $('#btnWizardCalenderNext').hide();
                $('#btnWizardCalenderFinish').show();
            }
        }
        $scope.changeNextCalendertab = function () {
            setTimeout(function () { $("#btnTemp1").click(); }, 50);
            $("#CalenderTablMetadata").removeClass('notvalidate');
            if ($("#CalenderTablMetadata .error").length > 0) {
                return false;
            }
            if ($scope.Calender.ngCalenderPublishedOn == "" || $scope.Calender.ngCalenderPublishedOn == null || $scope.Calender.ngCalenderPublishedOn == undefined) {
                bootbox.alert($translate.instant('LanguageContents.Res_5767.Caption') + $translate.instant('LanguageContents.Res_4669.Caption'));
                return false;
            }
            if ($scope.WizardPosition == 2) {
                if ($("#CalenderFulfillTab .error").length > 0) {
                    return false;
                }
            }
            var tempval = false;
            if (tempval == true) return false;
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () {
                KeepAllStepsMarkedComplete();
            }, 25);
            $('#CalenderWizard').wizard('next', '');
            var currentWizardStep = $('#CalenderWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#CalenderWizard').wizard('totnumsteps').totstep;
            $scope.WizardPosition = currentWizardStep;
            if ($scope.Isfromcalender_mui == false && currentWizardStep == 2) {
                totalWizardSteps = totalWizardSteps - 1;
                if (currentWizardStep == 2) {
                    $("#stepCal_3").show();
                } else {
                    $("#stepCal_3").hide();
                }
            }
            if (currentWizardStep > 1) {
                $('#btnWizardCalenderPrev').show();
            }
            if (currentWizardStep === totalWizardSteps) {
                $('#btnWizardCalenderFinish').show();
                $('#btnWizardCalenderNext').hide();
            } else {
                $('#btnWizardCalenderFinish').hide();
                $('#btnWizardCalenderNext').show();
            }
        }
        $scope.status = true;
        ValidCalenderTab1();
        $scope.cancelCalenderModal = function () {
            $('#CalenderTablMetadata').nod().destroy();
            $scope.CloseCalenderCreationPopup();
        }
        $scope.ObjectiveUnitsData = [];
        $scope.FulfillmentEntityTypes = [];
        $scope.FulfillmentAttributes = [];
        $scope.FulfillmentAttributeOptions = [];
        $scope.fieldKeys = [];
        $scope.fields = {};
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.creatObjectivefullfilmetadataHtml = '';
        $scope.ObjectiveEntityTypeHtml = '';
        $scope.ObjectiveFullfilAttributes = [];
        $scope.FulfillmentAttributeOptions = {};
        $scope.ObjectiveFullfilOptionsHtml = '';
        $scope.AddMulticonditionHtml = '';
        $scope.RatingsHtml = '';
        $scope.ObjectiveFulfillConditions = [];
        $scope.AutoCompleteSelectedUserObj = { "UserSelection": [] };
        $scope.rowCount = 0;
        $scope.ratingRowsCount = 0;
        $scope.numericObjectiveArr = [];
        var objOwnername = $cookies['Username'];
        var objOwnerid = $cookies['UserId'];
        $scope.OwnerName = objOwnername;
        $scope.OwnerID = objOwnerid;
        $scope.ownerEmail = $cookies['UserEmail'];
        $scope.MemberLists = [];
        $scope.AutoCompleteSelectedObj = [];
        $scope.OwnerList = [];
        $scope.owner = {};
        $scope.contrls = '';
        CalendercreationService.GetEntityTypeRoleAccess(10).then(function (role) {
            $scope.Roles = role.Response;
        });
        $scope.addUsers = function () {
            var result = $.grep($scope.MemberLists, function (e) {
                return e.Userid == parseInt($scope.AutoCompleteSelectedObj[0].Id, 10) && e.Roleid == parseInt($scope.fields['userRoles'], 10);
            });
            if (result.length == 0) {
                var membervalues = $.grep($scope.Roles, function (e) {
                    return e.ID == parseInt($scope.fields['userRoles'])
                })[0];
                if (membervalues != undefined) {
                    if ($scope.AutoCompleteSelectedObj.length > 0) {
                        $scope.MemberLists.push({
                            "TID": $scope.count,
                            "UserEmail": $scope.AutoCompleteSelectedObj[0].Email,
                            "DepartmentName": $scope.AutoCompleteSelectedObj[0].Designation,
                            "Title": $scope.AutoCompleteSelectedObj[0].Title,
                            "Roleid": parseInt(membervalues.ID, 10),
                            "RoleName": membervalues.Caption,
                            "Userid": parseInt($scope.AutoCompleteSelectedObj[0].Id, 10),
                            "UserName": $scope.AutoCompleteSelectedObj[0].FirstName + ' ' + $scope.AutoCompleteSelectedObj[0].LastName,
                            "IsInherited": '0',
                            "InheritedFromEntityid": '0',
                            "FromGlobal": 0,
                            "QuickInfo1": $scope.AutoCompleteSelectedObj[0].QuickInfo1,
                            "QuickInfo2": $scope.AutoCompleteSelectedObj[0].QuickInfo2
                        });
                        $scope.count = $scope.count + 1;
                    }
                    $scope.fields.usersID = '';

                    $scope.AutoCompleteSelectedObj = [];
                }
            } else bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
        };
        $scope.deleteMemberOptions = function (item) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2025.Caption'), function (result) {
                $timeout(function () {
                    if (result) {
                        $scope.MemberLists.splice($.inArray(item, $scope.MemberLists), 1);
                    }
                }, 100);
            });
        };
        $scope.ratingItems = [];
        $scope.ratingItems.push({
            ObjRatingText: ''
        });
        $scope.AddRatings = function () {
            if ($scope.ratingItems[$scope.ratingItems.length - 1].ObjRatingText == "" || $scope.ratingItems[0].ObjRatingText == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_1918.Caption'));
                return false;
            } else {
                $scope.ratingRowsCount = $scope.ratingRowsCount + 1;
                $scope.ratingItems.push({
                    ObjRatingText: ''
                });
            }
        }
        $scope.AddRatings1 = function () {
            $scope.ratingRowsCount = $scope.ratingRowsCount + 1;
            $scope.ratingItems.push({
                ObjRatingText: ''
            });
        };
        $scope.RemoveRatings = function (item) {
            if ($scope.ratingRowsCount < 2) {
                bootbox.alert($translate.instant('LanguageContents.Res_1917.Caption'));
                return false;
            } else {
                $scope.ratingRowsCount = $scope.ratingRowsCount - 1;
                $scope.ratingItems.splice($.inArray(item, $scope.items), 1);
            }
        };
        $scope.numericqunatitativedisplay = true;
        CalendercreationService.GettingObjectiveUnits().then(function (ObjectiveUnitsResult) {
            $scope.ObjectiveUnitsData = ObjectiveUnitsResult.Response;
        });
        if ($('#ObjectiveNumericGlobalbaseline').is(':checked')) {
            $('#ObjectiveNumericGlobaltarget').attr("disabled", false);
        }
        $scope.numglobalchecked = false;
        $scope.numtargetchecked = false;
        $scope.nonglobalchecked = false;
        $scope.nontargetchecked = false;
        $scope.objectiveItems = [];
        $scope.objectiveItems.push({
            ObjEntityType: [],
            ObjEntityAttributes: [],
            ObjAttributeOptionns: [],
            ObjCondition: 0
        });
        $scope.addMultiPleConditions = function () {
            if ($scope.objectiveItems[$scope.rowCount].ObjEntityType.length == 0 || $scope.objectiveItems[$scope.rowCount].ObjEntityAttributes.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_1981.Caption'));
                return false;
            } else {
                $scope.rowCount = $scope.rowCount + 1;
                $scope.objectiveItems.push({
                    ObjEntityType: [],
                    ObjEntityAttributes: [],
                    ObjAttributeOptionns: [],
                    ObjCondition: []
                });
                $scope.FulfillmentAttributes = [];
                $scope.FulfillmentAttributeOptions = {};
            }
        };
        $scope.objectiveItemsduplicate = [];
        $scope.objectiveItemsduplicate.push({
            ObjCondition: []
        });
        $scope.addMultiPleConditionsduplicate = function (item) {
            $scope.objectiveItemsduplicate.push({
                ObjCondition: []
            });
        };
        $scope.removeFulCondition = function (item) {
            if ($scope.rowCount > 0) {
                $scope.rowCount = $scope.rowCount - 1;
                $scope.objectiveItems.splice($.inArray(item, $scope.items), 1);
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1982.Caption'));
                return false;
            }
        };
        FilltFulfillmentEntityTypes("CalEntityType0");

        function FilltFulfillmentEntityTypes(ControllerID) {
            CalendercreationService.GetFulfillmentEntityTypesfrCal().then(function (fulFillmentEntityTypesObj) {
                $scope.FulfillmentEntityTypes = fulFillmentEntityTypesObj.Response;
                var objentities = fulFillmentEntityTypesObj.Response;
                $.each(objentities, function (val, item) {
                    $('#' + ControllerID).append($('<option></option>').val(item.Id).html(item.Caption))
                });
            });
        }
        $scope.LoadCalenderFulfillmentMetadatatbody = function (event) {
            //  $("#CalenderFulfillmentMetadatatbody").click(function (event) {
            var TargetControl = $(event.target);
            var currentUniqueId = TargetControl.parents('div').attr('data-id');
            if (TargetControl.attr('data-role') == 'EntityType') {
                ResetDropDown("CalEntityAttributes" + currentUniqueId, currentUniqueId);
                ResetDropDown("CalAttributeOptions" + currentUniqueId, currentUniqueId);
                CalendercreationService.GetFulfillmentAttribute(TargetControl.val()).then(function (fulFillmentAttributesObj) {
                    if (fulFillmentAttributesObj.Response.length > 0) {
                        FillEntityTypeAttributes("CalEntityAttributes" + currentUniqueId, fulFillmentAttributesObj.Response);
                    }
                });
            } else if (TargetControl.attr('data-role') == 'Attributes') {
                ResetDropDown("CalAttributeOptions" + currentUniqueId, currentUniqueId);
                var AttributeData = TargetControl.val().split("_");
                var entityAttribtueId = parseInt(AttributeData[0], 10);
                var entityAttributeLevel = parseInt(AttributeData[1], 10);
                CalendercreationService.GetFulfillmentAttributeOptions(entityAttribtueId, entityAttributeLevel).then(function (fulfillmentAttributeOptionsObj) {
                    if (fulfillmentAttributeOptionsObj.Response.length > 0) {
                        FillEntityTypeAttributes("CalAttributeOptions" + currentUniqueId, fulfillmentAttributeOptionsObj.Response)
                    }
                });
            } else if (TargetControl.attr('data-role') == 'Equals') { } else if (TargetControl.attr('data-role') == 'Add') {
                if (currentUniqueId > 0 && TargetControl.parents().find('#CalConditionType' + currentUniqueId).val() == "") {
                    bootbox.alert($translate.instant('LanguageContents.Res_4617.Caption'));
                    return false;
                }
                if (TargetControl.parents().find('#CalEntityType' + currentUniqueId).val() == "0" || TargetControl.parents().find('#CalEntityType' + currentUniqueId).val() == null) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1983.Caption'));
                    return false;
                }
                if ($('#CalEntityAttributes' + currentUniqueId).val() != "0" && $('#CalEntityAttributes' + currentUniqueId).val() != null) {
                    if ($('#CalAttributeOptions' + currentUniqueId).val() == null) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1846.Caption'));
                        return false;
                    }
                }
                var UniqueId = parseInt(TargetControl.parents('div').attr('data-id')) + 1;
                var html = '';
                html += "<div  data-control='main' data-Holder='holder' id='container" + UniqueId + "' data-uniquekey='container" + UniqueId + "' data-id='" + UniqueId + "'>";
                html += "<ul class='repeter' >";
                html += "<li class='form-inline'>";
                html += " <label class='control-label' for='CalenderCondition'>" + $translate.instant('LanguageContents.Res_4956.Caption') + ":</label>";
                html += "<select ui-select2 id='CalConditionType" + UniqueId + "' ng-model='dummymodule_ConditionType_" + UniqueId + "'>";
                html += "<option value='' >-- " + $translate.instant('LanguageContents.Res_811.Caption') + "-- </option>";
                html += "<option value='1'>" + $translate.instant('LanguageContents.Res_5027.Caption') + " </option>";
                html += "<option value='2' ng-selected='true'>" + $translate.instant('LanguageContents.Res_812.Caption') + " </option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_807.Caption') + "</label>";
                html += "<select ui-select2 id='CalEntityType" + UniqueId + "' ng-model='dummymodule_EntityType_" + UniqueId + "' data-role='EntityType'>";
                html += "<option value='0' ng-selected='true'>--" + $translate.instant('LanguageContents.Res_39.Caption') + "--</option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_808.Caption') + "</label>";
                html += "<select ui-select2 id='CalEntityAttributes" + UniqueId + "' ng-model='dummymodule_attribute_" + UniqueId + "'  data-role='Attributes'>";
                html += "<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + "--</option>";
                html += "";
                html += "</select>";
                html += "<label>" + $translate.instant('LanguageContents.Res_809.Caption') + "</label>";
                html += "<select ui-select2 multiple='multiple' id='CalAttributeOptions" + UniqueId + "' ng-model='dummymodule_Options_" + UniqueId + "' data-role='Equals'>";
                html += "<option  value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>";
                html += "";
                html += "</select>";
                html += "<button class='btn' data-role='Add' ><i class='icon-plus' data-role='Add'></i></button>&nbsp;<button class='btn' data-role='Remove' ><i class='icon-remove' data-role='Remove'></i></button>";
                html += "";
                html += "</li>";
                html += "</ul>";
                html += "</div>";
                $("#container" + currentUniqueId).after($compile(html)($scope));
                FillEntityTypes("CalEntityType" + UniqueId);
            } else if (TargetControl.attr('data-role') == 'Remove') {
                if (currentUniqueId == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1982.Caption'));
                    return false;
                }
                $("#container" + currentUniqueId).remove();
            }
        };

        function FillEntityTypes(ControllerID) {
            if (ControllerID != undefined) {
                var objentities = $scope.FulfillmentEntityTypes;
                $('#' + ControllerID).html("");
                $('#' + ControllerID).html("<option value='0'>--" + $translate.instant('LanguageContents.Res_39.Caption') + "--</option>");
                $.each(objentities, function (val, item) {
                    $('#' + ControllerID).append($('<option ></option>').val(item.Id).html(item.Caption))
                });
            }
        }

        function FillEntityTypeAttributes(ControllerID, Response) {
            if (ControllerID != undefined) {
                $('#' + ControllerID).html("");
                $('#' + ControllerID).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
                $.each(Response, function (val, item) {
                    if (item.Level != undefined) {
                        var currentId = item.Id.toString() + "_" + item.Level;
                        $('#' + ControllerID).append($("<option></option>").val(currentId).html(item.Caption));
                    } else {
                        $('#' + ControllerID).append($('<option ></option>').val(item.Id).html(item.Caption));
                    }
                });
            }
        }

        function ResetDropDown(ControllerID, currentUniqueId) {
            $('#' + ControllerID).html("");
            $('#' + ControllerID).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
            $('#' + ControllerID).select2("val", 0);
            $('#CalAttributeOptions' + currentUniqueId).html("");
            $('#CalAttributeOptions' + currentUniqueId).html("<option value='0'>-- " + $translate.instant('LanguageContents.Res_39.Caption') + " --</option>");
            $('#CalAttributeOptions' + currentUniqueId).select2("val", "");
        }
        $scope.isselectallEntities = false;
        $scope.selectAllEntities = function () {
            $.each($scope.FulfillEntites, function (index, value) {
                if ($scope.isselectallEntities == false) {
                    $("#entity" + value.EntityID).attr("class", "checkbox checked");
                } else {
                    $("#entity" + value.EntityID).attr("class", "checkbox");
                }
            });
            $scope.isselectallEntities = ($scope.isselectallEntities == false) ? true : false;
        };
        $scope.CalDatavalue = {};
        $scope.objectiveConditionsValues = {};
        $scope.CreateNewCalender = function () {
            //$("#btnWizardCalenderFinish").click(function (event) {
            $("#btnWizardCalenderFinish").attr('disabled', 'disabled');
            collectionObjectiveCondition();
            $scope.CalDatavalue = $scope.ConditionDataArr;
            var CalenderEntity = {};
            $scope.selectedEntities = [];
            if ($scope.EntitiesfrCalfromPlan.length > 0) {
                $scope.selectedEntities = $scope.EntitiesfrCalfromPlan;
            }
            $.each($scope.FulfillEntites, function (index, value) {
                if ($("#entity" + value.EntityID).attr("class") == "checkbox checked" || $("#entity" + value.EntityID).attr("class") == "checked") {
                    $scope.selectedEntities.push(value.EntityID);
                }
            });
            var calendeerPublishedOn = "";
            calendeerPublishedOn = $scope.Calender.ngCalenderPublishedOn;
            CalenderEntity.SelectedEntities = $scope.selectedEntities;
            CalenderEntity.CalenderTypeID = $scope.Calender.ngObjectiveType;
            CalenderEntity.CalenderName = $scope.Calender.ngCalenderName;
            CalenderEntity.CalenderDescription = $scope.Calender.ngCalenderDescription;
            CalenderEntity.Calenderowner = $scope.Calender.ngCalenderOwner;
            CalenderEntity.CalenderPublishedOn = dateFormat(calendeerPublishedOn, "yyyy/MM/dd");
            CalenderEntity.CalenderVisPeriod = $scope.Calender.ngCalenderVisPeriod != null ? $scope.Calender.ngCalenderVisPeriod : 0;
            CalenderEntity.CalenderVisType = $scope.Calender.ngCalenderVisType;
            CalenderEntity.CalenderisExternal = $scope.Calender.ngCalenderisExternal;
            CalenderEntity.SelectedTabs = $scope.selectedTabs;
            if ($scope.CalDatavalue[0].CalEntityType != "0") {
                CalenderEntity.CalenderFulfillConditonValueData = [{
                    "CalenderFulfillValues": $scope.CalDatavalue
                }]
            } else if ($scope.CalDatavalue[0].CalEntityType == "0") {
                CalenderEntity.CalenderFulfillConditonValueData = []
            }
            CalenderEntity.CalenderMembers = $scope.MemberLists;
            CalenderEntity.CalenderOwner = $scope.OwnerList;
            CalendercreationService.CreateCalender(CalenderEntity).then(function (result) {
                if (result.StatusCode == 200) {
                    //  $("#CalenderMetadataModel").modal("hide");
                    $scope.CloseCalenderCreationPopup()
                    $scope.Calender = {
                        ngCalenderName: '',
                        ngCalenderDescription: '',
                        ngCalenderPublishedOn: '',
                        ngCalenderVisPeriod: 1,
                        ngCalenderVisType: 0,
                        ngCalenderisExternal: false
                    }
                    $scope.FulfillEntites = '';
                    NotifySuccess($translate.instant('LanguageContents.Res_4139.Caption'));
                    var IDList = new Array();
                    IDList.push(result.Response);
                    $window.ListofEntityID = IDList;
                    $scope.MemberLists = [];
                    var TrackID = CreateHisory(IDList);
                    $location.path('/mui/calender/detail/section/' + result.Response + '/overview');
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4254.Caption'));
                }
            });
        }
        $scope.ConditionDataArr = [];

        function collectionObjectiveCondition() {
            $scope.ConditionDataArr = [];
            var EntttyTypeArr = new Array();
            var attriArreArr = new Array();
            var optionArr = [];
            $('#CalenderFulfillmentMetadatatbody div[data-Holder="holder"]').each(function (index) {
                if ($('#CalConditionType' + index).val() == '') {
                    var collectionarr = {
                        'CalEntityType': $('#CalEntityType' + index).val(),
                        'CalEntityAttributes': $('#CalEntityAttributes' + index).val(),
                        'CalAttributeOptionns': $('#CalAttributeOptions' + index).val(),
                        'CalConditionType': 0
                    };
                } else {
                    var collectionarr = {
                        'CalEntityType': $('#CalEntityType' + index).val(),
                        'CalEntityAttributes': $('#CalEntityAttributes' + index).val(),
                        'CalAttributeOptionns': $('#CalAttributeOptions' + index).val(),
                        'CalConditionType': $('#CalConditionType' + index).val()
                    };
                }
                $scope.ConditionDataArr.push(collectionarr);
            });
        }
        $scope.SaveObjectiveName = function (objValue) { }
        $scope.FulfillEntites = '';
        $scope.getEntities = function () {
            collectionObjectiveCondition();
            var CalenderFulfill = [];
            var CalDatavaluefrEntities = $scope.ConditionDataArr;
            if (CalDatavaluefrEntities[0].CalEntityType != "0") {
                CalenderFulfill.push({
                    "CalenderFulfillValues": CalDatavaluefrEntities
                });
            } else if (CalDatavaluefrEntities[0].CalEntityType == "0") {
                CalenderFulfill.push({
                    "CalenderFulfillValues": CalDatavaluefrEntities
                });
            }
            CalendercreationService.getEntitiesfrCalender(CalenderFulfill).then(function (result) {
                if (result.StatusCode == 200) {
                    if (result.Response.length > 0) {
                        $('#iconselectallentities').attr("class", "checkbox");
                        $('#availentitiesTable').show();
                        $scope.FulfillEntites = result.Response;
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_4512.Caption'));
                    }
                } else { }
            });
        }
        $scope.selectedEntities = [];
        $scope.selectedTabs = [];
        $scope.selectEntity = function (entityid, e) {
            if (e.target.checked) {
                $scope.selectedEntities.push(entityid);
            } else {
                $scope.selectedEntities.splice($.inArray(entityid, $scope.selectedEntities), 1);
            }
        }
        $scope.selectTabs = function (id, e) {
            if (e.target.checked) {
                $scope.selectedTabs.push(id);
            } else {
                $scope.selectedTabs.splice($.inArray(id, $scope.selectedTabs), 1);
            }
        };
        $scope.PeriodonBlur = function () {
            var d = parseInt($scope.Calender.ngCalenderVisPeriod, 10);
            if (d === parseInt(d, 10) && d > 0) { } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1133.Caption'));
                $scope.Calender.ngCalenderVisPeriod = parseInt(1, 10);
            }
        }

        function SetCursorPosition(ctrID, cursorPosition) {
            var cnt = ctrID.val().length;
            if (cnt > 4) {
                cursorPosition = cursorPosition + 1;
                if (cnt > 5) cursorPosition = cursorPosition - 1;
                if (cnt > 8) cursorPosition = cursorPosition + 1;
                if (cnt > 9) cursorPosition = cursorPosition - 1;
            }
            setTimeout(function () {
                ctrID.caret(cursorPosition);
            }, 0);
        }

        function FinancialValidation(event) {
            if (event.shiftKey == true) {
                event.preventDefault();
            }
            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 189 || event.keyCode == 190 || event.key === "-") {
                return true;
            } else {
                event.preventDefault();
            }
        };

        function CurrencyFormat(srcNumber) {
            var retCommaString = '';
            var getstr = srcNumber + '';
            if (getstr.match(" ")) return getstr;
            var count = 0;
            var commaString = '';
            for (var x = getstr.length - 1; x >= 0; x--) {
                if (count == 3) {
                    commaString = commaString + ' ';
                    count = 0;
                }
                commaString = commaString + getstr.charAt(x);
                count++;
            }
            for (var j = commaString.length - 1; j >= 0; j--) {
                retCommaString = retCommaString + commaString.charAt(j);
            }
            return (retCommaString);
        }

        function getMoney(A) {
            var a = new Number(A);
            var b = a.toFixed(2);
            a = parseInt(a);
            b = (b - a).toPrecision(2);
            b = parseFloat(b).toFixed(2);
            a = a.toLocaleString();
            a = a.replace(",", "");
            a = Number(a);
            a = a.formatMoney(0, ' ', ' ');
            if (a < 1 && a.lastIndexOf('.00') == (a.length - 3)) {
                a = a.substring(0, a.length - 3);
            }
            if (b.substring(0, 1) == '-') {
                b = b.substring(2);
            } else b = b.substring(1);
            return a + b;
        }
        setTimeout(function () {
            if ($scope.Isfromcalender_mui == false) {
                $("#stepCal_2").hide();
                $("#tabStep_stepCal_2").hide();
                $("#spanstep2member").text('2');
            }
            $scope.OpenCalenderCreationPopup();
        }, 300);
        $scope.CloseCalenderCreationPopup = function () {
            $modalInstance.dismiss('cancel');
        }
    }
    app.controller('mui.planningtool.calender.calendercreationCtrl', ['$scope', '$location', '$resource', '$timeout', '$cookies', '$window', 'CalendercreationService', '$compile', '$translate', '$modalInstance', muiplanningtoolcalendercalendercreationCtrl]);
})(angular, app);