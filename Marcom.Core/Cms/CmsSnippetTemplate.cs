﻿using BrandSystems.Marcom.Core.Cms.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Cms
{
    internal class CmsSnippetTemplate : ICmsSnippetTemplate
    {
        protected int _id;
        protected string _snippetcontent;
        protected bool _active;
        protected string _thumbnailguid;
        protected string _createdon;
        protected string _defaultfirstcontent;
        protected string _defaultlastcontent;

        public CmsSnippetTemplate()
        {

        }

        public CmsSnippetTemplate(int pid)
        {
            this._id = pid;
        }

        public CmsSnippetTemplate(int pid, string psnippetcontent, bool pactive, string pthumbnailguid, string pcreatedon, string pdefaultfirstcontent, string pdefaultlastcontent)
        {
            this._id = pid;
            this._snippetcontent = psnippetcontent;
            this._active = pactive;
            this._thumbnailguid = pthumbnailguid;
            this._createdon = pcreatedon;
            this._defaultfirstcontent = pdefaultfirstcontent;
            this._defaultlastcontent = pdefaultlastcontent;
        }
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string SnippetContent
        {
            get { return _snippetcontent; }
            set { _snippetcontent = value; }
        }


        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public string ThumbnailGuid
        {
            get { return _thumbnailguid; }
            set { _thumbnailguid = value; }
        }

        public string CreatedOn
        {
            get { return _createdon; }
            set { _createdon = value; }
        }
        public string DefaultFirstContent
        {
            get { return _defaultfirstcontent; }
            set { _defaultfirstcontent = value; }
        }
        public string DefaultLastContent
        {
            get { return _defaultlastcontent; }
            set { _defaultlastcontent = value; }
        }
    }


}
