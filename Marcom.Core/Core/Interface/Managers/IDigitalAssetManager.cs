﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BrandSystems.Marcom.Dal.Metadata.Model;
using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Core.Dam.Interface;
using System.Collections;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.DAM;
using BrandSystems.Marcom.Core.DAM.Interface;
using BrandSystems.Marcom.Dal.DAM.Model;
using Newtonsoft.Json.Linq;
using BrandSystems.Marcom.Core.Metadata;
namespace BrandSystems.Marcom.Core.Interface.Managers
{
    public interface IDigitalAssetManager
    {

        /// <summary>
        /// Gets the type of the entity.
        /// </summary>
        ///  /// <param name="EntityTypeID">The EntityTypeID.</param>
        /// <returns>List of EntitytypeAttribute</returns>
        IList<IAttribute> GetAttributeDAMCreate(int EntityTypeID);
        IList<IAttribute> GetDAMAttribute(int EntityTypeID);
        IList<IAttribute> GetDAMCommonAttribute();
        string DAMGetAdminSettings(string LogoSettings, string key, int typeid);
        string DAMGetAdminSettings(string LogoSettings);
        bool DAMadminSettingsforRootLevelInsertUpdate(string jsondata, string LogoSettings, string key, int typeid);
        bool DAMadminSettingsforRootLevelInsertUpdate(string jsondata, string LogoSettings);

        IList<IEntityType> GetDAMEntityTypes();

        Tuple<IList, IList> GetDAMExtensions();

        IList<IEntityTypeAttributeRelationwithLevels> GetDamAttributeRelation(int damID);

        /// <summary>
        /// Gets the DAM folder.
        /// </summary>
        /// <param name="attributeID">The EntityID.</param>
        /// <returns>string</returns>
        string GetEntityDamFolder(int entityID);

        int CreateAsset(int parentId, int TypeId, string Name, IList<IAttributeData> listattributevalues, IList<IDamPeriod> listperiodvalues, string FileName, int VersionNo, string MimeType, string Extension, long Size, int EntityID, String FileGuid, string Description, bool IsforDuplicate = false, int Duplicatefilestatus = 0, int LinkedAssetID = 0, string fileAdditionalinfo = null, string strAssetAccess = null, int SourceAssetID = 0, bool IsforAdmin = false, int assetactioncode = 0, bool blnAttach = false, IList<IDAMFile> SourceAssetDAMFile = null, bool fromcrop = false, IList<IAssetAmountCurrencyType> listentityamountcurrencytype = null, bool IsTemplate = false, int ImpersonateUID = 0);



        List<object> GetAssets(int folderid, int entityID, int viewType, int orderby, int pageNo, bool ViewAll = false);

        IAssets GetAssetAttributesDetails(int id);

        List<object> GetDAMViewSettings();

        int CreateFolder(int entityID, int parentNodeID, int Level, int nodeId, int id, string Key, int sortorder, string caption, string description, string colorcode);

        int UpdateFolder(int id, string caption, string description, string colorcode);

        bool DeleteFolder(int[] idArr);

        bool DeleteAssets(int[] idArr);

        DAMFileDao SaveFiletoAsset(int AssetID, int Status, string MimeType, long Size, string FileGuid, DateTime CreatedOn, string Extension, string Name, int VersionNo, string Description, int OwnerID);

        bool UpdateAssetVersion(int AssetID, int fileid);

        string DownloadDamFiles(int[] assetid, bool Isfiles, int sendDamFiles = 0, List<KeyValuePair<string, int>> CroppedList = null, bool Issendmail = false);
        /// <summary>
        /// UpdateAssetAccess
        /// </summary>
        /// <param name="AssetID"></param>
        /// <param name="AccessID"></param>
        /// <returns>success</returns>
        bool UpdateAssetAccess(int AssetID, string AccessID);
        int AssetRoleAccess(int ID, int[] RoleID);
        int DuplicateAssets(int[] assetid);
        int AttachAssetsEntityCreation(int entityID, int[] assetid, IList<object> SelectedAssetIdFolderId = null, bool isalreadyexist=false,int UserID=0);
        /// <summary>
        /// CheckPreviewGenerator
        /// </summary>
        /// <param name="AssetIDs"></param>
        /// <returns>returns string of preview generated ids</returns>
        string CheckPreviewGenerator(string AssetIDs);

        bool SaveDetailBlockForAssets(int AssetID, int AttributeTypeid, int OldValue, List<object> NewValue, int Level);
        IList<IOption> GetOptionDetailListByAssetID(int id, int AssetID);

        /// <summary>
        /// PublishAssets
        /// </summary>
        /// <param name="idArr"></param>
        /// <param name="IsPublished"></param>
        /// <returns>true if ispublish value is updated</returns>
        bool PublishAssets(int[] idArr, bool IsPublished, int PublisherID = 0);

        int CreateBlankAsset(int parentId, int TypeId, string Name, IList<IAttributeData> listattributevalues, int EntityID, int Category, IList<IDamPeriod> listperiodvalues, string url = null, bool IsforDuplicate = false, int LinkedAssetID = 0, string strAssetAccess = null, int SourceAssetID = 0, IList<IAssetAmountCurrencyType> listentityamountcurrencytype = null);

        /// <summary>
        /// Send Mail
        /// </summary>
        /// <param name="proxy"></param>
        /// <param name="idarr"></param>
        /// <returns>true if mail sent</returns>
        bool SendMail(bool IncludeMdata, int[] idarr, string toAddress, string subject, List<KeyValuePair<string, int>> CroppedList = null);

        /// <summary>
        /// MoveFilesToUploadImagesToCreateTask
        /// </summary>used to move the files from dam files to uploadimages folder to create task
        /// <param name="proxy"></param>
        /// <param name="TaskFiles"></param>
        /// <returns>new guid</returns>
        Tuple<IList<BrandSystems.Marcom.Core.Common.Interface.IFile>, int> MoveFilesToUploadImagesToCreateTask(IList<BrandSystems.Marcom.Core.Common.Interface.IFile> TaskFiles, int ParentEntityID);

        bool DeleteAttachmentVersionByAssetID(int fileid);

        /// <summary>
        /// UpdateAssetAccessSettings
        /// </summary>
        /// <param name="RoleID"></param>
        /// <param name="IsChecked"></param>
        /// <returns></returns>
        bool UpdateAssetAccessSettings(int RoleID, bool IsChecked);

        bool UpdateAssetDetails(int AssetID, string AssetName);

        IList<IDamFileProfile> GetProfileFiles();

        bool InsertUpdateProfileFiles(int id, string name, double height, double width, string extension, int DPI, string AssetRole);

        bool DeleteProfilefile(int Id);

        IList<IOptimakerSettings> GetOptimakerSettings();

        IList<ICategory> GetOptmakerCatagories();

        string GetCategoryTreeCollection();

        bool InsertUpdateCatergory(JObject jobj);

        int InsertUpdateOptimakerSettings(int ID, string Name, int CategoryId, string Description, int DocId, int DeptId, int DocType, int DocVersionId, string PreviewImage, int AutoApproval, int TemplateEngineType, int AssetType, string ServerURL, bool EnableHighResPdf);

        bool DeleteOptimakeSetting(int ID);

        bool DeleteOptimakerCategory(int ID);

        IList GetAllOptimakerSettings(int categoryId);

        string GetOptimakerSettingsBaseURL();

        string GetMediaGeneratorSettingsBaseURL();

        int InsertUpdateWordTemplateSettings(int ID, string Name, int CategoryId, string Description, string Worddocpath, string PreviewImage);

        bool DeleteWordtempSetting(int ID);

        IList<IWordTemplateSettings> GetWordTemplateSettings();

        int CreateMediaGeneratorAsset(int entityID, int folderID, int assetTypeID, int createdBy, int docVersionID, string assetName, string jpegname, long jpegsize, int jpegversion, string jpegdesc, string jpegGUID, string highResGUID, string lowResGUID, int docID, int TenantID);

        /// <summary>
        /// GetDAMViewAdminSettings
        /// </summary>
        /// <param name="ViewType"></param>
        /// <param name="DamType"></param>
        /// <returns></returns>
        List<object> GetDAMViewAdminSettings(string ViewType, int DamType);

        /// <summary>
        /// UpdateDamViewStatus
        /// </summary>
        /// <param name="ViewType"></param>
        /// <param name="DamType"></param>
        /// <param name="attributeslist"></param>
        /// <returns></returns>
        bool UpdateDamViewStatus(string ViewType, int DamType, IList<IDamViewAttribtues> attributeslist);

        /// <summary>
        /// GetDAMToolTipSettings
        /// </summary>
        /// <returns>list of thumbnail and list view tool tip settings</returns>
        List<object> GetDAMToolTipSettings();

        List<object> GetStatusFilteredEntityAsset(int folderid, int entityID, int viewType, int orderby, int pageNo, string statusFilter);

        void CreateDOCGeneratedAsset(int entityID, int folderID, int assetTypeID, int createdBy, string intialfilename, string assetname, int TenantID);
        /// <summary>
        /// get published assets
        /// </summary>
        /// <param name="viewType"></param>
        /// <param name="orderby"></param>
        /// <param name="pageNo"></param>
        /// <returns>only published assets list</returns>
        List<object> GetPublishedAssets(int viewType, int orderby, int pageNo);
        /// <summary>
        /// AddFilesFromDAM - add selected published assets to main assets
        /// </summary>
        /// <param name="assetids"></param>
        /// <returns></returns>
        int AddPublishedFilesToDAM(int[] assetids, int EntityID, int FolderID);


        void CreateversionDOCGeneratedAsset(int entityID, int folderID, int assetTypeID, int createdBy, string intialfilename, string assetname, int assetId, int TenantID);
        string getkeyvaluefromwebconfig(string key);
        IList<IDamFileProfile> GetProfileFilesByUser(int UserId);
        IList<DAMFileDao> GetAssetActiveFileinfo(int assetId);
        IList<DAMFiledownloadinfo> CropRescale(int assetId, int TopLeft, int TopRight, int bottomLeft, int BottomRight, int CropFormat, int ScaleWidth, int ScaleHeight, int Dpi, int profileid, string fileformate, int cropfrom);
        IList GetMimeType();
        /// <summary>
        /// GetBreadCrumFolderPath
        /// </summary>
        /// <param name="EntityID"></param>
        /// <param name="CurrentFolderID"></param>
        /// <returns>list of folderpath</returns>
        IList GetBreadCrumFolderPath(int EntityID, int CurrentFolderID);

        IList<object> GetAllFolderStructure(int EntityID);
        bool AddNewsFeedinfo(int assetId, string action, string TypeName);

        bool SaveCropedImageForLightBox(string sourcepath, int imgwidth, int imgheight, int imgX, int imgY);

        List<object> GetMediaAssets(int viewType, int orderbyid, int pageNo, int[] assettypeArr, string filterSchema = null, int totalRecords = 0);

        List<object> GetMediaBankFilterAttributes();

        int MoveAssets(int[] assetid, int folderId, int entityid, int actioncode);
        bool DAMadminSettingsDeleteAttributeRelationAllViews(int typeid, int ID);
        int AttachAssets(int[] assetid, int entityid, int folderId, bool blnAttach);

        List<object> GetSearchAssets(string assetIDs);

        Tuple<List<object>, int> GetCustomFilterAttributes(int typeID);

        Tuple<string, int> GetSearchCriteriaAdminSettings(string LogoSettings, string key, int typeid);

        List<int> AttachAssetsforProofTask(int[] assetid, int entityid, int folderId, bool blnAttach);

        /// <summary>
        /// Creates the proof by ProofHQ.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="taskid">The taskid.</param>
        /// <param name="assetids">int[] assetids.</param>
        /// <returns>bool</returns>
        bool CreateProofTaskWithAttachment(int taskID, int[] assetIds, int Tasktypeid);

        int GetProofIDByTaskID(int taskid);

        int GetUserIDByEmail(string email);


        IAssets ChangeAssettype(int assetTypeID, int damID);
        IList<IEntityTypeAttributeRelationwithLevels> GetAssetAttributueswithTypeID(int assetTypeID);

        int SaveAssetTypeofAsset(int typeid, string Name, IList<IAttributeData> listattributevalues, int AssetID, int oldAssetTypeId, IList<IDamPeriod> listperiodvalues);

        //bool SendMailWithMetaData(int[] idarr, string toAddress, string subject, List<KeyValuePair<string, int>> CroppedList = null);


        bool SendMailWithMetaData(bool IncludeMdata, int[] idarr, string toAddress, string subject, List<KeyValuePair<string, int>> CroppedList = null);

        string AssetMetadata(int[] idarr, bool IncludeMdata);

        string GetLuceneAssetValueDetails(int assetID, int assetTypeid);

        int AddPublishedLinkFiles(int[] assetid, int EntityID, int FolderID);
        string GetDamTreeNode(int attributeID, bool isAdminSettings, int parententityID = 0);
        IDamPeriod Damperiodservice();
        IList<DropDownTreePricing> GetDropDownTreePricingObjectFromParentDetail(int attributeID, bool isinheritfromParent, bool isFetchParent = false, int entityid = 0, int parentid = 0);
        IList<DropDownTreePricing> GetDropDownTreePricingObjectDetail(int attributeID, bool isinheritfromParent, bool isFetchParent = false, int entityid = 0, int parentid = 0);
        bool UpdateDropDownTreePricing(int EntityID, int AttributeTypeid, int AttributeId, IList<IDamTreeValue> NewValue);

        int CreateAssetPeriod(int Assetid, DateTime Startdate, DateTime EndDate, string Description, int SortOrder, int attributeid);
        bool UpdateAssetPeriod(int ID, int EntityID, string StartDate, string EndDate, int SortOrder, string Description, int attributeid);
        bool DeleteAssetPeriod(int ID);
        string GetAttributeTreeNodeFromParent(int attributeID, int entityID, bool isChoosefromParent, bool isinheritfromParent = false);
        string GetDetailAttributeTreeNodeFromParent(int attributeID, int entityID, bool isChoosefromParent);
        string GetAttributeTreeNode(int attributeID, int entityID);
        bool SaveDetailBlockForTreeLevels(int EntityID, int AttributeTypeid, int attributeid, IList<IDamTreeValue> NewValue, JArray jroldTree, JArray jrnewtree);

        List<object> GetLightboxAssets(int[] assetArr, int viewType, int orderbyid, int pageNo, bool ViewAll = false);

        bool UpdateAssetThumpnail(string sourcepath, int imgwidth, int imgheight, int imgX, int imgY, string Preview, string OriginalAssetName, string assetExt, int assetId = 0);

        bool DeleteAssetAccess(int AssetID);

        IList GetlistoflinkedAsset(int EntityID, int[] assetArr);

        List<object> IsActiveLinkEntity(int entityID);

        string[] DownloadAssetWithMetadata(int[] assetid, bool include);
        int CreateReformatProfile(int ProfileID, string Name, int Units, JObject srcValue);
        IList<DamReformatProfileDao> GetReformatProfile(int ProfileID);
        Tuple<bool, string, string, string, int> ReformatRescale(int assetId, int profileid, int Units, int Folderid, JObject srcValue);

        bool UnPublishAssets(int[] idArr, bool IsPublished, int PublisherID = 0);

        int ReformateCreateAsset(int assetid, int EntityID, int FolderID);

        List<object> GetAssetDetsforSelectedAssets(int folderid, int entityID, int viewType, int orderby, int pageNo, int[] selAssets, bool ViewAll = false);

        List<object> GetAssetsbasedonFolderandEntity(int entityID, int folderID, int viewType, int orderby, bool ViewAll = false);

        string GetExternalAssetContent();

        bool InsertUpdateExternalAssetContent(string htmlcontent);

        string GetAssetCategoryTreeCollection();

        bool InsertUpdateAssetCatergory(JObject jobj);

        int DeleteAssetCategory(int ID);

        IList GetAssetTypeAssetCategoryRelation();

        string GetSelectedCategoryTreeCollection(int CategoryID);

        IList InsertUpdateAssetCategoryRelation(int AssetTypeID, int CategoryID);

        string GetAssetCategoryTree(int AsetTypeID);

        List<object> GetAssetCategoryPathInAssetEdit(int AssetTypeID);
        bool UpdateAssetImageName(int entityId, int attributeId, string imageName);

        IList GetAssetTypeFileExtensionRelation();

        bool InsertAssetTypeFileExtension(int ID, int AssetTypeID, string FileExt, string category);

        bool DeleteAssetTypeFileExtension(int ID);

        bool GetAssetSnippetTemplates(JObject jsonXML);


        string getassetSnippetData();
        Tuple<string, int> Get2ImagineTemplateUrl(int entityID, int folderID, int userID, int templateType, string DocID, string serverURL, int assetType);

        dynamic Download2ImagineAsset(int entityID, int folderID, int userID, int docVersionID, string serverURL);

        int saveTemplateEngine(JObject jObj);

        IList<TemplateEnginesDao> getAllTemplateEngines();

        bool deleteTemplateEngine(int ID);

        IAssetAmountCurrencyType AssetAmountCurrencyTypeservice();
        IList<TemplateEnginesDao> getServerUrls();

        OptimakerSettingsDao getCurrentTemplateAssetInfo(int DocID);

        string Get2ImagineResourceUrl(string documentId, string serverURL);

        bool IsFileExist(string filePath);

        string S3Settings();

        string copydeleteTemplateAsset(string tempguid);
        bool writeUploadErrorlog(JObject errorloginfo);
        string CheckAccessToPublish(int Userid,int EntityId);

        bool CreateNewAssetWithFolder(int EntityID, JArray AssetObject, int ImpersonateUID=0);
    }
}
