﻿(function (ng, app) {
    function muiplanningtooldefaultdetailsectionCtrl($scope, $sce, $timeout, $http, $compile, $resource, $location, $window, $stateParams, PlansectionService) {
        $scope.CustomTab = {
            Selection: ""
        };
        setTimeout(function () { $scope.page_resize(); }, 100);
        $scope.TabCollectionsList = null;
        $scope.set_bgcolor = function (clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            }
            else {
                return '';
            }
        }
        $scope.set_color = function (clr) {
            if (clr != null) return {
                'color': "#" + clr.toString().trim()
            }
            else {
                return '';
            }
        }
        $scope.Global = {
            SelectedEntityUniquekey: ""
        };
        $scope.ResultHolderForBreadcrum = [];
        $scope.IsLock = true;
        $scope.EntityLockTask = false;
        $scope.ShowCustomTabOnLoad = false;
        $scope.IsCustomTab = true;
        $scope.urlTaskID = 0;
        if ($stateParams.TaskID !== undefined) {
            $scope.urlTaskID = $stateParams.TaskID;
        }

        var timervar;
        localStorage.clear('customTabName');
        $scope.loadBreadCrum_Edit = function (isOnload) {
            $("#plandetailfilterdiv").css('display', 'none')
            if ($scope.LocalIsWorkspaces) {
                PlansectionService.GetDataForBreadcrumLoadWithLocalWorkSpacePath($stateParams.ID, true).then(function (BreadcrumData) {
                    if (BreadcrumData.Response != null) {
                        $scope.ResultHolderForBreadcrum = BreadcrumData.Response;
                    }
                    if ($scope.ResultHolderForBreadcrum[0] == true) {
                        $("#Objective").css({
                            'display': 'block'
                        });
                    } else {
                        $("#Objective").css({
                            'display': 'none'
                        });
                    }
                    if ($("#EntitiesTree li a[data-entityid=" + $stateParams.ID + "]").length == 0 && $scope.Activity.IsActivitySectionLoad == false) {
                        $scope.loadactivityfromsearchByID($stateParams.ID, 100);
                    }
                    $scope.array = [];
                    var GetPathforbreadcrum = $scope.ResultHolderForBreadcrum[1];
                    var getbc = GetPathforbreadcrum;
                    if (getbc != null && getbc != undefined) {
                        for (var i = 0; i < getbc.length; i++) {
                            if (i == (getbc.length - 1)) {
                                var decodedName = $('<div/>').html(getbc[i].Name).text();
                                $scope.array.push({
                                    "ID": getbc[i].ID,
                                    "Name": decodedName,
                                    "UniqueKey": getbc[i].UniqueKey,
                                    "ColorCode": getbc[i].ColorCode,
                                    "ShortDescription": getbc[i].ShortDescription,
                                    "TypeID": getbc[i].TypeID,
                                    "mystyle": "999999"
                                });
                                $scope.EntityShortText = getbc[i].ShortDescription;
                                $scope.EntityBreadCrumTypeName = getbc[i].TypeName;
                                $scope.EntityColorcode = getbc[i].ColorCode;
                                $scope.RootLevelEntityName = decodedName;
                                $scope.RootLevelEntityNameTemp = getbc[i].Name;
                                $scope.RootLevelEntityTypeID = getbc[i].TypeID;
                                $window.GlobalUniquekey = getbc[i].UniqueKey;
                                $scope.Global.SelectedEntityUniquekey = getbc[i].UniqueKey;
                                $window.SubscriptionEntityTypeID = getbc[i].TypeID;
                                $window.SubscriptionEntityID = getbc[i].ID;
                                $(window).trigger('LoadSubscription', $stateParams.ID);
                                $scope.EntityTypeID = getbc[i].TypeID;
                                if (getbc[i].IsLock == "True") {
                                    $scope.IsLock = true;
                                } else {
                                    $scope.IsLock = false;
                                }
                                if (getbc[i].IsReadOnly == "True") {
                                    $scope.EntityLockTask = true;
                                } else {
                                    $scope.EntityLockTask = false;
                                }
                            } else {
                                var decodedName = $('<div/>').html(getbc[i].Name).text();
                                $scope.array.push({
                                    "ID": getbc[i].ID,
                                    "Name": decodedName,
                                    "UniqueKey": getbc[i].UniqueKey,
                                    "ColorCode": getbc[i].ColorCode,
                                    "ShortDescription": getbc[i].ShortDescription,
                                    "TypeID": getbc[i].TypeID,
                                    "mystyle": "0088CC"
                                });
                            }
                        }
                        GetAllTabCollections();
                        if (isOnload == false) {
                            if ($scope.subview == "overview") {
                                $scope.$broadcast('LoadSectionDetail', $stateParams.ID);
                                $scope.$emit('LoadSectionDetail', $stateParams.ID);
                            } else if ($scope.subview == "financial") {
                                $scope.$broadcast('LoadFinancialDetail', $stateParams.ID);
                            } else if ($scope.subview == "member") {
                                $scope.$broadcast('LoadMembersDetail', $stateParams.ID);
                            } else if ($scope.subview == "objective") {
                                $scope.$broadcast('LoadObjectives', $stateParams.ID);
                            } else if ($scope.subview == "attachment") {
                                $scope.$broadcast('LoadAttachment', $stateParams.ID);
                            } else if ($scope.subview == "presentation") {
                                $scope.$broadcast('LoadPresentations', $stateParams.ID);
                            } else if ($scope.subview == "workflow") {
                                $scope.$broadcast('LoadWorkflow', $stateParams.ID);
                            } else if ($scope.subview == "task") {
                                ResetTaskObjects();
                                $scope.$broadcast('LoadTask', $stateParams.ID);
                            }
                        }
                    }
                });
            } else {
                PlansectionService.GetDataForBreadcrumLoadWithPath($stateParams.ID).then(function (BreadcrumData) {
                    if (BreadcrumData.Response != null) {
                        $scope.ResultHolderForBreadcrum = BreadcrumData.Response;
                    }
                    if ($scope.ResultHolderForBreadcrum[0] == true) {
                        $("#Objective").css({
                            'display': 'block'
                        });
                    } else {
                        $("#Objective").css({
                            'display': 'none'
                        });
                    }
                    if ($("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").length == 0 && $scope.Activity.IsActivitySectionLoad == false) {
                        $scope.loadactivityfromsearchByID($stateParams.ID, 100);
                    }
                    $scope.array = [];
                    var GetPathforbreadcrum = $scope.ResultHolderForBreadcrum[1];
                    var getbc = GetPathforbreadcrum;
                    if (getbc != null && getbc != undefined) {
                        for (var i = 0; i < getbc.length; i++) {
                            if (i == (getbc.length - 1)) {
                                var decodedName = $('<div/>').html(getbc[i].Name).text();
                                $scope.array.push({
                                    "ID": getbc[i].ID,
                                    "Name": decodedName,
                                    "UniqueKey": getbc[i].UniqueKey,
                                    "ColorCode": getbc[i].ColorCode,
                                    "ShortDescription": getbc[i].ShortDescription,
                                    "TypeID": getbc[i].TypeID,
                                    "mystyle": "999999"
                                });
                                $scope.EntityShortText = getbc[i].ShortDescription;
                                $scope.EntityBreadCrumTypeName = getbc[i].TypeName;
                                $scope.EntityColorcode = getbc[i].ColorCode;
                                $scope.RootLevelEntityName = decodedName;
                                $scope.RootLevelEntityNameTemp = getbc[i].Name;
                                $scope.RootLevelEntityTypeID = getbc[i].TypeID;
                                $window.GlobalUniquekey = getbc[i].UniqueKey;
                                $scope.Global.SelectedEntityUniquekey = getbc[i].UniqueKey;
                                $window.SubscriptionEntityTypeID = getbc[i].TypeID;
                                $window.SubscriptionEntityID = getbc[i].ID;
                                $(window).trigger('LoadSubscription', $stateParams.ID);
                                $scope.EntityTypeID = getbc[i].TypeID;
                                if (getbc[i].IsLock == "True") {
                                    $scope.IsLock = true;
                                } else {
                                    $scope.IsLock = false;
                                }
                                if (getbc[i].IsReadOnly == "True") {
                                    $scope.EntityLockTask = true;
                                } else {
                                    $scope.EntityLockTask = false;
                                }
                            } else {
                                var decodedName = $('<div/>').html(getbc[i].Name).text();
                                $scope.array.push({
                                    "ID": getbc[i].ID,
                                    "Name": decodedName,
                                    "UniqueKey": getbc[i].UniqueKey,
                                    "ColorCode": getbc[i].ColorCode,
                                    "ShortDescription": getbc[i].ShortDescription,
                                    "TypeID": getbc[i].TypeID,
                                    "mystyle": "0088CC"
                                });
                            }
                        }
                        GetAllTabCollections();
                        if (isOnload == false) {
                            if ($scope.subview == "overview") {
                                $scope.$broadcast('LoadSectionDetail', $stateParams.ID);
                                $scope.$emit('LoadSectionDetail', $stateParams.ID);
                            } else if ($scope.subview == "financial") {
                                $scope.$broadcast('LoadFinancialDetail', $stateParams.ID);
                            } else if ($scope.subview == "member") {
                                $scope.$broadcast('LoadMembersDetail', $stateParams.ID);
                            } else if ($scope.subview == "objective") {
                                $scope.$broadcast('LoadObjectives', $stateParams.ID);
                            } else if ($scope.subview == "attachment") {
                                $scope.$broadcast('LoadAttachment', $stateParams.ID);
                            } else if ($scope.subview == "presentation") {
                                $scope.$broadcast('LoadPresentations', $stateParams.ID);
                            } else if ($scope.subview == "workflow") {
                                $scope.$broadcast('LoadWorkflow', $stateParams.ID);
                            } else if ($scope.subview == "task") {
                                ResetTaskObjects();
                                $scope.$broadcast('LoadTask', $stateParams.ID);
                            }
                        }
                    }
                });
            }
        }
        $scope.loadBreadCrum_Edit(true);

        function LoadNextRenderContext() {
            // Get the render context local to this controller (and relevant params).
            $scope.thisContext = "";
            if ($scope.subview == null) {
                var a = window.location.hash.split('/');
                for (var i = 0; i < a.length; i++) {
                    if (a[i] == "section") {
                        $scope.thisContext = "DynamicEntity";
                    }

                }
            }
            if ($scope.thisContext == "" || $scope.thisContext == null) {
                if ($location.path().indexOf("costcentre") != -1) {
                    $scope.thisContext = "Costcentre";
                }
                else if ($location.path().indexOf("objective") != -1) {
                    $scope.thisContext = "Objective";
                }
                else if ($location.path().indexOf("calender") != -1) {
                    $scope.thisContext = "Calender";
                }
            }
        }
        if ($scope.EntityTypeID != 6) $scope.ngObjectiveshow = true;
        else $scope.ngObjectiveshow = false;
        $scope.load = function (parameters) {
            $scope.LoadTab = function (tab) {
                $window.CurrentEntityTab = "";
                $window.CurrentEntityTab = tab;
                //$(this).addClass('active');
                localStorage.setItem('lasttab', tab);
                if ($scope.thisContext == "DynamicEntity") {
                    $location.path('/mui/planningtool/default/detail/section/' + $stateParams.ID + '/' + tab + '');
                }
                else if ($scope.thisContext == "Costcentre")
                    $location.path('/mui/planningtool/costcentre/detail/sectionentity/' + $stateParams.ID + '/' + tab + '');
                else if ($scope.thisContext == "Objective")
                    $location.path('/mui/objective/detail/sectionentity/' + $stateParams.ID + '/' + tab + '');
                else if ($scope.thisContext == "Calender")
                    $location.path('/mui/planningtool/calender/detail/sectionentity/' + $stateParams.ID + '/' + tab + '');
            };
        };
        $scope.LoadTab = function (tab) {
            $window.CurrentEntityTab = "";
            $window.CurrentEntityTab = tab;
            //$(this).addClass('active');
            if ($scope.thisContext == "DynamicEntity") {
                $location.path('/mui/planningtool/default/detail/section/' + $stateParams.ID + '/' + tab + '');
            } else if ($scope.thisContext == "Costcentre") {
                $location.path('/mui/planningtool/costcentre/detail/sectionentity/' + $stateParams.ID + '/' + tab + '');
            } else if ($scope.thisContext == "Objective") {
                $location.path('/mui/planningtool/objective/detail/sectionentity/' + $stateParams.ID + '/' + tab + '');
            }
            else if ($scope.thisContext == "Calender") {
                $location.path('/mui/planningtool/calender/detail/sectionentity/' + $stateParams.ID + '/' + tab + '');
            }
        };
        $scope.agroup = [];
        $scope.agroupListTabBlock = [];
        $scope.agroupkeyvaluepairTabBlock = [];
        $scope.AttributeGroupById = 0;

        function GetAttributeGroups() {
            $scope.LanguageContent = $scope.LanguageContents;
            $scope.AttrLock = $scope.IsLock;
            if ($scope.agroup.length > 0) {
                $scope.agroup.splice(0, $scope.agroup.length);
                $scope.agroupListTabBlock.splice(0, $scope.agroupListTabBlock.length);
                $scope.agroupkeyvaluepairTabBlock.splice(0, $scope.agroupkeyvaluepairTabBlock.length);
            }
            PlansectionService.GetEntityTypeAttributeGroupRelation(0, parseInt($stateParams.ID), $scope.AttributeGroupById).then(function (attributeGrpRelation) {
                if (attributeGrpRelation.Response != null) {
                    if (attributeGrpRelation.Response != '') {
                        $scope.agroup = attributeGrpRelation.Response;
                        $scope.agroupListTabBlock = $.grep(attributeGrpRelation.Response, function (e) {
                            return e.LocationType == 3 && e.RepresentationType == true
                        });
                        $scope.agroupkeyvaluepairTabBlock = $.grep(attributeGrpRelation.Response, function (e) {
                            return e.LocationType == 3 && e.RepresentationType == false
                        });
                    }
                    if ($scope.agroupListTabBlock.length > 0) $timeout(function () {
                        $scope.$broadcast("processlistganttintab" + ($scope.agroupListTabBlock[0].ID))
                    }, 200);
                    if ($scope.agroupkeyvaluepairTabBlock.length > 0) $timeout(function () {
                        $scope.$broadcast("process" + ($scope.agroupkeyvaluepairTabBlock[0].ID))
                    }, 200);
                }
            });
        }
        $scope.CustomTabLoad = function (tab, tabUrl, IsSytemDefined, Name, AddEntityID, AttributeGroupId) {
            $scope.ShowCustomTabOnLoad = false;
            $scope.agroup.length = 0;
            $scope.agroupListTabBlock.length = 0;
            $scope.agroupkeyvaluepairTabBlock.length = 0;
            var customObj = { Name: Name, IsSysTab: IsSytemDefined, AttrGroupId: AttributeGroupId };
            localStorage.setItem('customTabName', JSON.stringify(customObj));
            if (IsSytemDefined == false) {
                $scope.AttributeGroupById = AttributeGroupId;
                $scope.CustomTab.Selection = $sce.trustAsResourceUrl("");
                if (AttributeGroupId == 0 || AttributeGroupId == undefined) {
                    $scope.IsCustomTab = true;
                    PlansectionService.GetCustomTabUrlTabsByTypeID(tab, $stateParams.ID).then(function (gettabresult) {
                        if (gettabresult.Response != null) {
                            $scope.CustomTab.Selection = $sce.trustAsResourceUrl(gettabresult.Response);
                            $timeout(function () {
                                $scope.ShowCustomTabOnLoad = true;
                            }, 750);
                        }
                    });
                } else {
                    $timeout(function () {
                        $scope.IsCustomTab = false;
                        GetAttributeGroups();
                        $scope.ShowCustomTabOnLoad = true;
                    }, 200);
                }
                //   $scope.CustomTab.Selection = $sce.trustAsResourceUrl(tabUrl + (AddEntityID == true ? $stateParams.ID : ""));
                $window.CurrentEntityTab = "";
                $window.CurrentEntityTab = tab;
                //$(this).addClass('active');
                if ($scope.thisContext == "DynamicEntity") {
                    $location.path('/mui/planningtool/default/detail/section/customtab/' + tab + '/' + $stateParams.ID + '');
                } else if ($scope.thisContext == "Costcentre") {
                    $location.path('/mui/planningtool/costcentre/detail/sectionentity/customtab/' + tab + '/' + $stateParams.ID + '');
                } else if ($scope.thisContext == "Objective") {
                    $location.path('/mui/objective/detail/sectionentitycustom/customtab/' + tab + '/' + $stateParams.ID + '');
                } else if ($scope.thisContext == "Calender") {
                    $location.path('/mui/planningtool/calender/customtab/sectionentity/' + tab + '/' + $stateParams.ID + '');
                }
            } else {
                $scope.LoadTab(Name.toLowerCase());
            }


        }
        $scope.ActiveEntityTab = {
            TaskActive: ''
        };
        if ($stateParams.TaskID !== undefined) {
            $('#SectionTabs li').removeClass('active');
            $scope.ActiveEntityTab.TaskActive = 'active';
        }
        $scope.BreadCrumOverview = function (ID) {
            var actname = $('#breadcrumlink').text();
            actname = actname.substring(0, actname.length);
            $window.EntityTypeID = ID;
            $window.SubscriptionEntityTypeID = $('#breadcrumlink').attr('data-typeid');
            $window.GlobalUniquekey = $('#breadcrumlink').attr('data-uniquekey');
            $scope.RootLevelEntityName = actname;
            $scope.EntityShortText = $('#breadcrumlink').attr('data-shortdesc');
            $scope.EntityColorcode = $('#breadcrumlink').attr('data-colorcode');
            if ($scope.thisContext == "DynamicEntity") {
                $location.path('mui/planningtool/default/detail/section/' + ID + '/' + $scope.subview + '');
            } else if ($scope.thisContext == "Costcentre") {
                $("#EntitiesTree li.active").removeClass('active')
                $("#EntitiesTree  li[data-CostCentreID=" + $scope.CurrentCostcentreID + "] a[data-entityid=" + ID + "]").parent('li').addClass('active');
                $location.path('mui/planningtool/costcentre/detail/sectionentity/' + ID + '/' + $scope.subview + '');
            } else if ($scope.thisContext == "Objective") {
                $("#EntitiesTree li.active").removeClass('active')
                $("#EntitiesTree  li[data-ObjectiveID=" + $scope.CurrentObjectiveID + "] a[data-entityid=" + ID + "]").parent('li').addClass('active');
                $location.path('mui/planningtool/objective/detail/sectionentity/' + ID + '/' + $scope.subview + '');
            } else if ($scope.thisContext == "Calender") {
                $("#EntitiesTree li.active").removeClass('active')
                $("#EntitiesTree  li[data-ObjectiveID=" + $scope.CurrentObjectiveID + "] a[data-entityid=" + ID + "]").parent('li').addClass('active');
                $location.path('mui/planningtool/calender/detail/sectionentity/' + ID + '/' + $scope.subview + '');
            }
        }

        function GetAllTabCollections() {
            PlansectionService.GetCustomEntityTabsByTypeID(6, $scope.EntityTypeID, $stateParams.ID, parseInt($scope.muiiscalender.CalenderID, 10)).then(function (gettabresult) {
                if (gettabresult.Response != null) {
                    $scope.TabCollectionsList = gettabresult.Response;
                    if ($stateParams.tabID == undefined) {
                        if ($scope.subview == "customtab" || $scope.subview == undefined) {
                            if ($scope.TabCollectionsList[0].IsSytemDefined == false) {
                                $scope.CustomTabLoad($scope.TabCollectionsList[0].Id, $scope.TabCollectionsList[0].ExternalUrl, false, $scope.TabCollectionsList[0].ControleID, $scope.TabCollectionsList[0].AddEntityID, currentObj[0].AttributeGroupID);
                            }
                        } else {
                            $scope.LoadTab($scope.subview.toLowerCase());
                        }
                    } else {
                        var currentObj = $.grep($scope.TabCollectionsList, function (item, i) {
                            return item.Id == $stateParams.tabID;
                        });
                        if (currentObj != null && currentObj.length > 0) $scope.CustomTabLoad(currentObj[0].Id, currentObj[0].ExternalUrl, currentObj[0].IsSytemDefined, currentObj[0].ControleID, currentObj[0].AddEntityID, currentObj[0].AttributeGroupID);
                        else $scope.LoadTab("overview");
                    }
                    setTimeout(preselecttab, 200);
                }
            });
        }

        function preselecttab() {
            var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
            var curTabName = JSON.parse(localStorage.getItem('customTabName'));

            $(".plantabs li a").each(function () {
                try {
                    var src = $(this).attr("data-tabname");
                    var attrGroupId = $(this).attr("data-attrgroupid");
                    if (src.length > 0) {
                        if (curTabName != null && curTabName != undefined) {
                            if (!curTabName.IsSysTab) {
                                if (src == curTabName.Name) {
                                    if (attrGroupId == curTabName.AttrGroupId) {
                                        $(".plantabs li").removeClass('active');
                                        $(this).parents('li').removeClass('active');
                                        $(this).parents('li').addClass("active");
                                    }
                                }
                            }
                            else if (src == pgurl || src == '') {
                                $(".plantabs li").removeClass('active');
                                $(this).parents('li').removeClass('active');
                                $(this).parents('li').addClass("active");
                            }
                        }
                        else {
                            if (src == pgurl || src == '') {
                                $(".plantabs li").removeClass('active');
                                $(this).parents('li').removeClass('active');
                                $(this).parents('li').addClass("active");
                            }
                        }

                    }

                } catch (e) {

                }
            })
        }

        $scope.BindClass = function (index, tab, ID) {
            //if (index == 0 && $scope.subview == null) {
            //    if ($scope.subview == null) {
            //        var tabid = $scope.TabCollectionsList[index].ControleID;
            //        $scope.subview = tabid;
            //    }
            //    return "active";
            //} else if ($scope.subview == tab) {
            //    return "active";
            //} else if (ID == $stateParams.tabID) {
            //    return "active";
            //}
            return "";
        }
        ResetTaskObjects();

        function ResetTaskObjects() {
            $scope.GlobalTaskFilterStatusObj = {
                MulitipleFilterStatus: []
            };
            $scope.groups = [];
            $scope.groupbySubLevelTaskObj = [];
            $scope.TaskExpandedDetails = [];
            $scope.SubLevelTaskExpandedDetails = [];
            $scope.SubLevelTaskLibraryListContainer = {
                SubLevelTaskLibraryList: []
            };
            $scope.RootLevelTaskLibraryListContainer = {
                RootLevelTaskLibraryList: []
            };
            $scope.groupbySubLevelTaskObj = [];
            $scope.ngExpandAll = true;
            $scope.ngExpandCollapseStatus = {
                ngExpandAll: true
            };
            $scope.TaskOrderListObj = {
                TaskSortingStatus: "Reorder task lists",
                TaskSortingStatusID: 1,
                TaskOrderHandle: false,
                SortOrderObj: [],
                UniqueTaskSort: "Reorder task",
                UniqueTaskSortingStatusID: 1,
                UniqueTaskOrderHandle: false,
                UniqueSortOrderObj: []
            };
            $scope.SubLevelEnableObject = {
                Enabled: false,
                SublevelText: "Show task(s) from sub-levels",
                SublevelTextStatus: 1
            };
        }
        $scope.MainTaskLiveUpdateIndex = 0;
        $scope.subTaskLiveUpdateIndex = 0;
        $scope.$on('LiveTaskListUpdate', function (event, taskkist) {
            if (taskkist.length > 0) {
                InitiateTaskLiveupdateAction();
            }
        });

        function InitiateTaskLiveupdateAction() {
            $scope.MainTaskLiveUpdateIndex = 0;
            $scope.subTaskLiveUpdateIndex = 0;
            UpdateTaskData();
            UpdateSubLevelTaskData();
        }
        $scope.$on('RefreshEntitytaskLibrary', function (event, tasklistid) {
            $scope.LiveTaskListIDCollection.TasklistIDList = [{
                "TaskLiStID": tasklistid,
                "EntityID": $stateParams.ID
            }];
            InitiateTaskLiveupdateAction();
        });

        $scope.$on('RefreshEntityTaskListCollection', function (event, taskobj) {
            $scope.LiveTaskListIDCollection.TasklistIDList = [{
                "TaskLiStID": taskobj.TaskListID,
                "EntityID": taskobj.EntityID
            }];
            InitiateTaskLiveupdateAction();
        });

        function UpdateTaskData() {
            var tlObj, tasklistList = [];
            tlObj = $scope.LiveTaskListIDCollection.TasklistIDList[$scope.MainTaskLiveUpdateIndex];
            tasklistList = $.grep($scope.TaskExpandedDetails, function (e) {
                return e.ID == tlObj.TaskLiStID;
            });
            if (tasklistList.length > 0) var promise = $http.get('api/task/GetEntityTaskListDetails/' + tlObj.EntityID + '/' + tlObj.TaskLiStID + '').success(function (result) {
                var data = [];
                data = result.Response;
                if (data != null && data != undefined) {
                    if (data.length > 0) {
                        tasklistList[0].TaskList = [];
                        for (var j = 0, taskdata; taskdata = data[j++];) {
                            tasklistList[0].TaskList.push(taskdata);
                        }
                        try {
                            var taskListResObj = $.grep($scope.groups, function (e) {
                                return e.ID == tlObj.TaskLiStID && e.EntityParentID == tlObj.EntityID;
                            });
                            if (taskListResObj[0].IsGetTasks == true && taskListResObj[0].IsExpanded == true) {
                                if (tasklistList[0].TaskList.length > 0) {
                                    taskListResObj[0].TaskList = tasklistList[0].TaskList;
                                    taskListResObj[0].TaskCount = tasklistList[0].TaskList.length;
                                }
                            }
                        } catch (e) { }
                    }
                }
                $timeout(function () {
                    $scope.MainTaskLiveUpdateIndex = $scope.MainTaskLiveUpdateIndex + 1;
                    if ($scope.MainTaskLiveUpdateIndex <= $scope.LiveTaskListIDCollection.TasklistIDList.length - 1) UpdateTaskData();
                }, 20);
            });
        }

        function UpdateSubLevelTaskData() {
            var tlObj, tasklistList = [];
            tlObj = $scope.LiveTaskListIDCollection.TasklistIDList[$scope.subTaskLiveUpdateIndex];
            tasklistList = $.grep($scope.SubLevelTaskExpandedDetails, function (e) {
                return e.ID == tlObj.TaskLiStID;
            });
            if (tasklistList.length > 0) var promise = $http.get('api/task/GetEntityTaskListDetails/' + tlObj.EntityID + '/' + tlObj.TaskLiStID + '').success(function (result) {
                var data = [];
                data = result.Response;
                if (data.length > 0) {
                    tasklistList[0].TaskList = [];
                    for (var j = 0, taskdata; taskdata = data[j++];) {
                        tasklistList[0].TaskList.push(taskdata);
                    }
                    try {
                        var EntityTaskObj = [];
                        EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function (e) {
                            return e.EntityID == tlObj.EntityID;
                        });
                        if (EntityTaskObj.length > 0) {
                            var taskObjHolder = $.grep(EntityTaskObj[0].TaskListGroup, function (e) {
                                return e.ID == tlObj.TaskLiStID;
                            });
                            if (taskObjHolder.length > 0) {
                                taskObjHolder[0].TaskList = tasklistList[0].TaskList;
                                taskObjHolder[0].TaskCount = tasklistList[0].TaskList.length;;
                            }
                        }
                    } catch (e) { }
                }
                $timeout(function () {
                    $scope.subTaskLiveUpdateIndex = $scope.subTaskLiveUpdateIndex + 1;
                    if ($scope.subTaskLiveUpdateIndex <= $scope.LiveTaskListIDCollection.TasklistIDList.length - 1) UpdateSubLevelTaskData();
                }, 20);
            });
        }


        $scope.$on("$destroy", function () {
            $timeout.cancel(timervar);
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.detail.sectionCtrl']"));
        });
        $timeout(function () {
            $scope.load();
            $('.nav-tabs').tabdrop({
                text: 'More'
            });
        }, 0);
        $(window).on("UpdateEntityName", function (event, optionarray) {
            $scope.RootLevelEntityName = optionarray.toString();
        });


        PlansectionService.GetLockStatus($stateParams.ID).then(function (entitylockstatus) {
            $scope.entitylockstatus.isUpdatedfrmsection = true;
            $scope.IsLock = entitylockstatus.Response.m_Item1;
            $scope.EntityLockTask = entitylockstatus.Response.m_Item2;
            setTimeout(LoadNextRenderContext(), 100);
        });

    }
    app.controller("mui.planningtool.default.detail.sectionCtrl", ['$scope', '$sce', '$timeout', '$http', '$compile', '$resource', '$location', '$window', '$stateParams', 'PlansectionService', muiplanningtooldefaultdetailsectionCtrl]);
})(angular, app)