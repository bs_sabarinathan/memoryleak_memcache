﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public partial class CmsCustomStyleDao : BaseDao, ICloneable 
    {
          #region Member Variables

		protected int _id;
        protected string _classname;
        protected string _csscode;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public CmsCustomStyleDao() {}

        public CmsCustomStyleDao(int pId, string pClassName, string pCssCode)
		{
			this._id = pId;
            this._classname = pClassName;
            this._csscode = pCssCode; 
		}

        public CmsCustomStyleDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual string ClassName
		{
            get { return _classname; }
			set 
			{
			  if (value != null && value.Length > 100)
                  throw new ArgumentOutOfRangeException("Class Name", "Class Name value, cannot contain more than 100 characters");
              _bIsChanged |= (_classname != value);
              _classname = value; 
			}
		}

        public virtual string CssCode
		{
            get { return _csscode; }
            set { _bIsChanged |= (_csscode != value); _csscode = value; }
		}

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string CmsCustomStyleDao = "CmsCustomStyleDao";			
			public const string Id = "Id";
            public const string ClassName = "ClassName";
            public const string CssCode = "CssCode";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string CmsCustomStyleDao = "CmsCustomStyleDao";			
			public const string Id = "Id";
            public const string ClassName = "ClassName";
            public const string CssCode = "CssCode";			
		}

		#endregion
    }
}
