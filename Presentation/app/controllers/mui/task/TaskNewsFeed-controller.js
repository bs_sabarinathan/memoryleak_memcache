﻿(function (ng, app) {
    "use strict";

    function muitaskTasknewsfeedCtrl($scope, $location, $resource, $compile, $timeout, $interval, $cookies, $translate, $sce, TaskNewsFeedService) {
        var divid = 'feeddivforworktaskedit';
        var PagenoforScroll;
        var fundrequestID;
        var divtypeid;
        var timeoutInterval;
        $scope.associatedentityid = 0;
        $scope.EntityUserList = [];
        $scope.PersonalUserIds = [];
        $scope.OwnerID = parseInt($cookies['UserId']);
        $scope.NewTime = new Date();
        $scope.GetEntityMembers = function (taskId) {
            $scope.EntityUserList = [];
            $scope.PersonalUserIds = [];
            TaskNewsFeedService.GetMember(taskId).then(function (member) {
                var IsUnique = true;
                if (member.Response != null) {
                    $.each(member.Response, function (index, value) {
                        if (value.IsInherited == false) {
                            IsUnique = true;
                            if ($scope.EntityUserList.length > 0) {
                                $.each($scope.EntityUserList, function (i, v) {
                                    if (v.Userid == value.Userid) {
                                        IsUnique = false;
                                    }
                                });
                                if (IsUnique == true) $scope.EntityUserList.push(value);
                            } else $scope.EntityUserList.push(value);
                        }
                    });
                }
            });
        }
        $scope.$on('loadNewsFeed', function ($event, obj) {
            $scope.OwnerID = parseInt($cookies['UserId']);
            $scope.NewTime = new Date();
            $scope.CurrentTaskID = obj.ID;
           $scope.associatedentityid = obj.EntityID;
            $scope.GetEntityMembers(obj.EntityID);
            getFeedforTask($scope.CurrentTaskID, 'feeddivforworktaskedit', false);
            timeoutInterval = $interval(function () {
                getFeedforTask($scope.CurrentTaskID, divid, true);
            }, 15000);
        });

        function getFeedforTask(entityid, divid, isLatestnewsfeed) {
            var tempPageno = 0;           
            if (isLatestnewsfeed == false) {
                $('#' + divid).html('');
                PagenoforScroll = 0;
                $scope.temptaskDiv = divid;
                tempPageno = 0;
            } else tempPageno = -1;
            fundrequestID = entityid;
            divtypeid = divid;
            try {
                if (entityid != null && entityid != undefined && entityid != "") {
                    $scope.tempTaskID = entityid;
                    TaskNewsFeedService.getNewsfeedforTask(entityid, tempPageno, isLatestnewsfeed).then(function (getFundingreqNewsFeedResult) {
                        if (getFundingreqNewsFeedResult.Response != null && getFundingreqNewsFeedResult.Response != undefined) {
                            var fundingreqfundingreqfeeddivHtml = '';
                            if (getFundingreqNewsFeedResult.Response.length > 0) {
                                if (!isLatestnewsfeed) {
                                    $scope.TaskEditFund_newsfeed_TEMP = [];
                                }
                                $scope.TaskEditFund_newsfeed_TEMP.push(getFundingreqNewsFeedResult.Response);
                            }
                            for (var i = 0; i < getFundingreqNewsFeedResult.Response.length; i++) {
                                var feedcomCount = getFundingreqNewsFeedResult.Response[i].FeedComment != null ? getFundingreqNewsFeedResult.Response[i].FeedComment.length : 0;
                                var fundingreqfeeddivHtml = '';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li data-parent="NewsParent" data-ID=' + getFundingreqNewsFeedResult.Response[i].FeedId + '>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"> <img src=Handlers/UserImage.ashx?id=' + getFundingreqNewsFeedResult.Response[i].Actor + '&time=' + $scope.NewTime + ' alt="Avatar"></div>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getFundingreqNewsFeedResult.Response[i].UserEmail + '>' + getFundingreqNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + getFundingreqNewsFeedResult.Response[i].FeedText + '</p></div>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getFundingreqNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML1_' + divid + '"  data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedId + '" id="Comment_' + getFundingreqNewsFeedResult.Response[i].FeedId + '" >Comment</a></span>';
                                if (feedcomCount > 1) {
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="mytaskCommentshowHTML"  data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedComment + '"   id="Mytaskfeed_' + getFundingreqNewsFeedResult.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                                } else {
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="mytaskCommentshowHTML"  data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedComment + '"   id="Mytaskfeed_' + getFundingreqNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                                }
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul class="subComment">';
                                if (getFundingreqNewsFeedResult.Response[i].FeedComment != null && getFundingreqNewsFeedResult.Response[i].FeedComment != '') {
                                    var j = 0;
                                    if (getFundingreqNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                        $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                    } else {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getFundingreqNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    }
                                    for (var k = 0; k < getFundingreqNewsFeedResult.Response[i].FeedComment.length; k++) {
                                        $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getFundingreqNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                        if (k == 0) {
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li';
                                        } else {
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li style = "display:none;"';
                                        }
                                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + ' id="feedcomment_' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
                                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt">';
                                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></div></li>';
                                    }
                                }
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul/></li>';
                                $('#' + divid + ' [data-id]').each(function () {
                                    if (parseInt($(this).attr('data-id')) == getFundingreqNewsFeedResult.Response[i].FeedId) {
                                        $(this).remove();
                                    }
                                });
                                if (isLatestnewsfeed == false) {
                                    $('#' + divid).append(fundingreqfeeddivHtml);
                                    $('#' + divid).scrollTop(0);
                                } else {
                                    $('#' + divid).prepend(fundingreqfeeddivHtml);
                                }
                            }
                            commentclick(divid);
                            $('#' + divid).on('click', 'a[data-command="openlink"]', function (event) {
                                var TargetControl = $(this);
                                var mypage = TargetControl.attr('data-Name');
                                var myname = TargetControl.attr('data-Name');
                                var w = 1200;
                                var h = 800
                                var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
                                var win = window.open(mypage, myname, winprops)
                            });
                        }
                    });
                }
            } catch (e) { }
        }

        function commentclick(divtypeid) {
            $('#' + divtypeid).on('click', 'a[data-DynHTML="CommentHTML1_' + divtypeid + '"]', function (event) {
                var commentuniqueid = this.id;
                event.stopImmediatePropagation();
                $(this).hide();
                var fundingreqfeeddivHtml = '';
                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li class="writeNewComment">';
                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newComment"><div class="userAvatar"><img data-role="user-avatar" src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\'></div>';
                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="textarea-wrapper" data-role="textarea"><div contenteditable="true" tabindex="0" class="textarea" id="feedcomment_' + commentuniqueid + '"></div></div>';
                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<button type="submit" class="btn btn-primary"  data-dyncommentbtn="ButtonHTML_' + divtypeid + '" >Comment</button></div></li>';
                var currentobj = $(this).parents('li[data-parent="NewsParent"]').find('ul');
                if ($(this).parents('li[data-parent="NewsParent"]').find('ul li').children().length > 0) $(this).parents('li[data-parent="NewsParent"]').find('ul li:first').before(fundingreqfeeddivHtml);
                else $(this).parents('li[data-parent="NewsParent"]').find('ul').html(fundingreqfeeddivHtml);
                $timeout(function () {
                    $('#feedcomment_' + commentuniqueid).html('').focus();
                }, 10);
                buttonclick(divtypeid);
            });
        }

        function buttonclick(divtypeid) {
            $('#' + divtypeid).on('click', 'button[data-dyncommentbtn="ButtonHTML_' + divtypeid + '"]', function (event) {
                event.stopImmediatePropagation();
                if ($(this).prev().eq(0).text().toString().trim().length > 0) {
                    var addfeedcomment = {};
                    var FeedidforComment = $(this).parents("li:eq(1)").attr('data-id');
                    var myDate = new Date.create();
                    addfeedcomment.FeedID = $(this).parents("li:eq(1)").attr('data-id');
                    addfeedcomment.Actor = parseInt($cookies['UserId']);
                    addfeedcomment.Comment = $(this).prev().eq(0).text();
                    var d = new Date.create();
                    var month = d.getMonth() + 1;
                    var day = d.getDate();
                    var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                    var output = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day + ' ' + hrs;
                    var _this = $(this);
                    TaskNewsFeedService.InsertFeedComment(addfeedcomment).then(function (saveusercomment) {
                        var fundingreqfeeddivHtml = '';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class=\"newsFeed\">';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"><img src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\' alt="Avatar"></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt">';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5> <a href="undefined">' + $cookies['Username'] + '</a></h5></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + saveusercomment.Response + '</p></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">Few seconds ago</span></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></div></div></li>';
                        $('#' + divtypeid).find('li[data-id=' + FeedidforComment + ']').first().find('li:first').before(fundingreqfeeddivHtml)
                        $(".writeNewComment").remove();;
                        $('#Comment_' + addfeedcomment.FeedID).show();
                    });
                }
            });
        }
        $scope.AddApprovalTaskNewsFeedforedit = function () {
            var temp = '';
            var addnewsfeed = {};
            addnewsfeed.Actor = parseInt($cookies['UserId'], 10);
            addnewsfeed.TemplateID = 40;
            addnewsfeed.EntityID = $scope.CurrentTaskID;
            addnewsfeed.TypeName = "Tasks";
            addnewsfeed.AttributeName = "";
            addnewsfeed.FromValue = "";
            addnewsfeed.ToValue = $scope.CommentText;
            addnewsfeed.PersonalUserIds = $scope.PersonalUserIds;
           addnewsfeed.associatedentityid = $scope.associatedentityid;
            TaskNewsFeedService.Feed(addnewsfeed).then(function (savenewsfeed) {
                $scope.CommentText = "";
                $scope.PersonalUserIds = [];
                getFeedforTask($scope.CurrentTaskID, divid, true);
            });
        }
        $('#feeddivforworktaskedit').on('click', 'a[data-DynHTML="mytaskCommentshowHTML"]', function (event) {
            var feedid1 = event.target.attributes["id"].nodeValue;
            var feedid = feedid1.substring(11);
            var feedfilter = $.grep($scope.TaskEditFund_newsfeed_TEMP[0], function (e) {
                return e.FeedId == parseInt(feedid);
            });
            $("#Mytaskfeed_" + feedid).next('div').hide();
            if (feedfilter != '') {
                for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
                    if ($('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
                        $(this)[0].innerHTML = "View " + (feedfilter[0].FeedComment.length - 1) + " more comment(s)";
                        if (i != 0) $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function () { });
                        else $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    } else {
                        $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                        $(this)[0].innerHTML = "Hide comment(s)";
                    }
                }
            }
        });
        $scope.$on('ClearTimerOnClose', function (event) {
            $interval.cancel(timeoutInterval);
        });
    }
    app.controller("mui.task.TaskNewFeedCtrl", ['$scope', '$location', '$resource', '$compile', '$timeout', '$interval', '$cookies', '$translate', '$sce', 'TaskNewsFeedService', muitaskTasknewsfeedCtrl])
})(angular, app);