﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Access.Model
{
    public partial class GlobalEntityTypeAccessDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
		protected int _globalroleid;
		protected int _moduleid;
		protected int _entitytypeid;
		protected bool _accesspermission;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public GlobalEntityTypeAccessDao() {}

        public GlobalEntityTypeAccessDao(int pId, int pGlobalRoleid, int pModuleid, int pEntityTypeid,  bool pAccessPermission)
		{
            this._id = pId;
			this._globalroleid = pGlobalRoleid; 
			this._moduleid = pModuleid; 
			this._entitytypeid = pEntityTypeid; 
			this._accesspermission = pAccessPermission; 
		}
		
		#endregion
		
		#region Public Properties        
		
        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }
        
		public virtual int GlobalRoleid
		{
			get { return _globalroleid; }
			set { _bIsChanged |= (_globalroleid != value); _globalroleid = value; }
			
		}
		
		public virtual int Moduleid
		{
			get { return _moduleid; }
			set { _bIsChanged |= (_moduleid != value); _moduleid = value; }
			
		}
		
		public virtual int EntityTypeid
		{
			get { return _entitytypeid; }
			set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }
			
		}
		
		public virtual bool AccessPermission
		{
			get { return _accesspermission; }
			set { _bIsChanged |= (_accesspermission != value); _accesspermission = value; }
			
		}

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string Id = "Id";
            public const string GlobalEntityTypeAccessDao = "GlobalEntityTypeAccessDao";           
			public const string GlobalRoleid = "GlobalRoleid";			
			public const string Moduleid = "Moduleid";			
			public const string EntityTypeid = "EntityTypeid";			
			public const string AccessPermission = "AccessPermission";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string Id = "Id";
            public const string GlobalEntityTypeAccessDao = "GlobalEntityTypeAccessDao";
			public const string GlobalRoleid = "GlobalRoleid";			
			public const string Moduleid = "Moduleid";			
			public const string EntityTypeid = "EntityTypeid";			
			public const string AccessPermission = "AccessPermission";			
		}

		#endregion
		
    }
}
