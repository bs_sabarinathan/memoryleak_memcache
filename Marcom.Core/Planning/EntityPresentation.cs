using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

	/// <summary>
	/// EntityPresentation object for table 'PM_Entity_Presentation'.
	/// </summary>

    internal class EntityPresentation : IEntityPresentation
    {
        #region Member Variables

        protected int _entityid;
        protected DateTimeOffset _publishedon;
        protected string _content;
        
        
        #endregion

        #region Constructors
        public EntityPresentation() { }

        public EntityPresentation(int pentityid, DateTimeOffset pPublishedOn, string pContent)
        {
            this._entityid = pentityid;
            this._publishedon = pPublishedOn;
            this._content = pContent;
        }

        #endregion

        #region Public Properties

        public virtual int EntityId
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public DateTimeOffset PublishedOn
        {
            get { return _publishedon; }
            set { _publishedon = value; }

        }

        public string Content
        {
            get { return _content; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Content", "Content value, cannot contain more than 1073741823 characters");
                _content = value;
            }

        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityPresentation castObj = null;
            try
            {
                castObj = (EntityPresentation)obj;
            }
            catch (Exception) { return false; }
            return castObj.GetHashCode() == this.GetHashCode();
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {

            return this.GetType().FullName.GetHashCode();

        }
        #endregion

    }	
}
