﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Task.Model
{

    /// <summary>
    /// PurchaseOrderDao object for table 'PM_PurchaseOrder'.
    /// </summary>

    public partial class AdminTaskCheckListDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _taskid;
        protected string _name;
        protected int _sortorder;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public AdminTaskCheckListDao() { }

        public AdminTaskCheckListDao(int pTaskId, string pName,int pSortorder)
        {
            this._taskid = pTaskId;
            this._name = pName;
            this._sortorder = pSortorder;
          
        }

        public AdminTaskCheckListDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }





        public virtual string NAME
        {
            get { return _name; }
            set { _bIsChanged |= (_name != value); _name = value; }

        }

        public virtual int TaskId
        {
            get { return _taskid; }
            set { _bIsChanged |= (_taskid != value); _taskid = value; }

        }


        public virtual int SortOrder
        {
            get { return _sortorder; }
            set { _bIsChanged |= (_sortorder != value); _sortorder = value; }

        }


        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AdminTaskDao = "AdminTaskCheckListDao";
            public const string ID = "ID";
            public const string TaskId = "TaskId";
            public const string NAME = "NAME";
            public const string SortOrder = "SortOrder";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AdminTaskDao = "AdminTaskCheckListDao";
            public const string ID = "ID";
            public const string TaskId = "TaskId";
            public const string NAME = "NAME";
            public const string SortOrder = "SortOrder";
        }

        #endregion


    }

}
