using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    /// <summary>
    /// ISelectableValue interface for table 'MM_SelectableValue'.
    /// </summary>
    public interface ISelectableValue
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }

        int Attributeid
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
