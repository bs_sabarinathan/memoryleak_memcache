﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Dam.Interface;

namespace BrandSystems.Marcom.Core.Dam
{
    public class AssetAccess : IAssetAccess
    {
        #region "Member variables"

        protected int _id;
        protected string _role;       

        #endregion

         #region "Constructor"

        public AssetAccess() { }


        public AssetAccess(int pid,string pRole)
        {
            this._id = pid;
            this._role = pRole;
        }

        #endregion

        #region "Public propertiers"

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public string Role
        {
            get { return _role; }
            set{_role = value; }

        }

        #endregion
    }
}
