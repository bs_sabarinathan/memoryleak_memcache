using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// CostCenterDao object for table 'PM_CostCenter'.
	/// </summary>
	
	public partial class CostCenterDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected string _name;
		protected decimal? _assignedamount;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public CostCenterDao() {}
			
		public CostCenterDao(int pId, string pName, decimal? pAssignedAmount)
		{
			this._id = pId; 
			this._name = pName; 
			this._assignedamount = pAssignedAmount; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual string Name
		{
			get { return _name; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
			  _bIsChanged |= (_name != value); 
			  _name = value; 
			}
			
		}
		
		public virtual decimal? AssignedAmount
		{
			get { return _assignedamount; }
			set { _bIsChanged |= (_assignedamount != value); _assignedamount = value; }
			
		}
		

		public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods
		
		public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string CostCenterDao = "CostCenterDao";			
			public const string Id = "Id";			
			public const string Name = "Name";			
			public const string AssignedAmount = "AssignedAmount";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string CostCenterDao = "CostCenterDao";			
			public const string Id = "Id";			
			public const string Name = "Name";			
			public const string AssignedAmount = "AssignedAmount";			
		}

		#endregion
		
		
	}
	
}
