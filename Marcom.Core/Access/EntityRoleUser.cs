using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Access
{

    /// <summary>
    /// EntityRoleUser object for table 'AM_Entity_Role_User'.
    /// </summary>
    internal class EntityRoleUser : IEntityRoleUser
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected int _roleid;
        protected int _userid;
        protected bool _isinherited;
        protected int _inheritedfromentityid;
        protected string _username;
        protected string _role;
        protected string _departmentname;
        protected string _title;
        protected string _useremail;
        protected string _inheritedfromentityname;
        protected string _quickinfo1;
        protected string _quickinfo2;
        protected string _quickinfo1attributecaption;
        protected string _quickinfo2attributecaption;
        #endregion

        #region Constructors
        public EntityRoleUser() { }

        public EntityRoleUser(int pId, int pEntityid, int pRoleid, int pUserid, bool pIsInherited, int pInheritedFromEntityid, string pusername, string prole, string pdepartmentname, string ptitle, string puseremail, string pinheritedfromentityname, string pquickInfo1, string pquickInfo2, string pquickInfo1attributecaption, string pquickInfo2attributecaption)
        {
            this._id = pId;
            this._entityid = pEntityid;
            this._roleid = pRoleid;
            this._userid = pUserid;
            this._isinherited = pIsInherited;
            this._inheritedfromentityid = pInheritedFromEntityid;
            this._username = pusername;
            this._role = prole;
            this._departmentname = pdepartmentname;
            this._title = ptitle;
            this._useremail = puseremail;
            this._inheritedfromentityname = pinheritedfromentityname;
            this._quickinfo1 = pquickInfo1;
            this._quickinfo2 = pquickInfo2;
            this._quickinfo1 = pquickInfo1attributecaption;
            this._quickinfo2 = pquickInfo2attributecaption;
        }

        #endregion

        #region Public Properties


        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int Entityid
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public int Roleid
        {
            get { return _roleid; }
            set { _roleid = value; }

        }

        public int Userid
        {
            get { return _userid; }
            set { _userid = value; }

        }

        public bool IsInherited
        {
            get { return _isinherited; }
            set { _isinherited = value; }

        }

        public int InheritedFromEntityid
        {
            get { return _inheritedfromentityid; }
            set { _inheritedfromentityid = value; }

        }

        public string UserName
        {
            get { return _username; }
            set { _username = value; }

        }

        public string DepartmentName
        {
            get { return _departmentname; }
            set { _departmentname = value; }

        }

        public string Role
        {
            get { return _role; }
            set { _role = value; }

        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }

        }

        public string UserEmail
        {
            get { return _useremail; }
            set { _useremail = value; }

        }

        public string InheritedFromEntityName
        {
            get { return _inheritedfromentityname; }
            set { _inheritedfromentityname = value; }

        }

        public string QuickInfo1
        {
            get { return _quickinfo1; }
            set { _quickinfo1 = value; }
        }

        public string QuickInfo2
        {
            get { return _quickinfo2; }
            set { _quickinfo2 = value; }
        }
        public string QuickInfo1AttributeCaption
        {
            get { return _quickinfo1attributecaption; }
            set { _quickinfo1attributecaption = value; }
        }

        public string QuickInfo2AttributeCaption
        {
            get { return _quickinfo2attributecaption; }
            set { _quickinfo2attributecaption = value; }
        }

        public String ShortText
        {
            get;
            set;
        }

        public String ColorCode
        {
            get;
            set;
        }

        public int BackendRoleID
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityRoleUser castObj = null;
            try
            {
                castObj = (EntityRoleUser)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
