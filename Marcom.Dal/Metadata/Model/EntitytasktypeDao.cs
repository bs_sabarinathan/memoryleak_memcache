﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

    /// <summary>
    /// EntityTaskTypeDao object for table 'MM_EntityTaskType'.
    /// </summary>

    public partial class EntitytasktypeDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _entitytypeid;
        protected int _tasktypeid;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public EntitytasktypeDao() { }

        public EntitytasktypeDao(int pId, int pEntitytasktypeid, int pTasktypeid)
        {
            this._id = pId;
            this._entitytypeid = pEntitytasktypeid;
            this._tasktypeid = pTasktypeid;
        }

        public EntitytasktypeDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }


        public virtual int EntitytypeId
        {
            get { return _entitytypeid; }
            set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }

        }

        public virtual int TaskTypeId
        {
            get { return _tasktypeid; }
            set { _bIsChanged |= (_tasktypeid != value); _tasktypeid = value; }

        }


        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string EntitytasktypeDao = "EntitytasktypeDao";
            public const string Id = "Id";
            public const string EntitytypeId = "EntitytypeId";
            public const string TaskTypeId = "TaskTypeId";

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string EntitytasktypeDao = "EntitytasktypeDao";
            public const string Id = "Id";
            public const string EntitytypeId = "EntitytypeId";
            public const string TaskTypeId = "TaskTypeId";

        }

        #endregion


    }

}
