﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{
    public class LanguageType : ILanguageType
    {
        #region Member Variables

		protected int _id;
        protected string _name;
        protected string _description;
        protected int _inheritedfrom;
        protected string _addedon;
        protected int _pendingtranslation;
        protected bool _isdefaultlanguage;
        protected string _langKey;
        protected string _langContent;
		
		#endregion
		
		#region Constructors
        public LanguageType() { }

        public LanguageType(int pid, string pName, string pDescription, int pInheritedFrom, string pAddedOn, int pPendingTranslation, bool pIsDefaultLanguage, string pLangKey, string pLangContent)
		{
            this._id = pid;
            this._name = pName;
            this._description = pDescription;
            this._inheritedfrom = pInheritedFrom;
            this._addedon = pAddedOn;
            this._pendingtranslation = pPendingTranslation;
            this._isdefaultlanguage = pIsDefaultLanguage;
            this._langKey = pLangKey;
            this._langContent = pLangContent;
		}
				
	
		#endregion
		
		#region Public Properties
		
		public int ID
		{
			get { return _id; }
			set { _id = value; }
			
		}

        public string Name 
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public int InheritedFrom
        {
            get { return _inheritedfrom; }
            set { _inheritedfrom = value; }
        }

        public string AddedOn
        {
            get { return _addedon; }
            set { _addedon = value; }
        }

        public int PendingTranslation
        {
            get { return _pendingtranslation; }
            set { _pendingtranslation = value; }
        }

        public bool IsDefaultLanguage
        {
            get { return _isdefaultlanguage; }
            set { _isdefaultlanguage = value; }
        }

        public string LangKey
        {
            get { return _langKey; }
            set { _langKey = value; }
        }

        public string LangContent
        {
            get { return _langContent; }
            set { _langContent = value; }
        }
		#endregion 
		
		#region Equals And HashCode Overrides
	
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
    }
}
