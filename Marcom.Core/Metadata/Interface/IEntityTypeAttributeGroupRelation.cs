﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Metadata.Interface
    {
    public interface IEntityTypeAttributeGroupRelation
        {
        #region Public Properties

        int ID
            {
            get;
            set;
            }

        int AttributeGroupID
            {
            get;
            set;

            }
        int EntityTypeID
            {
            get;
            set;

            }

        string Caption
            {
            get;
            set;
            }





        int LocationType
            {
            get;
            set;

            }

        bool RepresentationType
            {
            get;
            set;

            }




        int SortOrder
            {
            get;
            set;

            }

        int PageSize
        {
            get;
            set;
        }
        bool IsTabInEntityCreation
        {
            get;
            set;
        }
        bool IsAttrGrpInheritFromParent
        {
            get;
            set;
        }
        #endregion
        }
    }
