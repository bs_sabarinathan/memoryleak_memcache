using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    /// <summary>
    /// IAttributeSetAttribute interface for table 'MM_AttributeSet_Attribute'.
    /// </summary>
    public interface IAttributeSetAttribute
    {
        #region Public Properties

        int AttributeSetid
        {
            get;
            set;

        }

        int Attributeid
        {
            get;
            set;

        }

        int? SortOrder
        {
            get;
            set;

        }

        int? Validationid
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
