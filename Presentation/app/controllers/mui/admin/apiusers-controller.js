﻿(function (ng, app) {
    "use strict"; function muiadminapiuserCtrl($scope, $location, $resource, $timeout, $translate, AdminService) {
        GetAPIuserDetails(); $scope.APIList = null; function GetAPIuserDetails() { AdminService.GetAPIusersDetails().then(function (res) { $scope.selectedAllAPIusers = false; if (res.Response != null) { $scope.APIList = res.Response; } }); }
$scope.generateGuid=function(){if($('#generateBtn').hasClass('disabled')){return;}
$('#generateBtn').addClass('disabled');var APIcollection=$.grep($scope.APIList,function(e){return e.Selected==true;});if(APIcollection.length==0){bootbox.alert($translate.instant('LanguageContents.Res_154.Caption'));$('#generateBtn').removeClass('disabled');return false;}
else{var objNewGuid={};objNewGuid.APISelected=APIcollection;AdminService.GenerateGuidforSelectedAPI(objNewGuid).then(function(GuidResult){if(GuidResult.StatusCode==405){NotifyError($translate.instant('LanguageContents.Res_4325.Caption'));$('#generateBtn').removeClass('disabled');}
else{if(GuidResult.Response==null){NotifyError($translate.instant('LanguageContents.Res_4325.Caption'));$('#generateBtn').removeClass('disabled');}
else{$('#generateBtn').removeClass('disabled');NotifySuccess($translate.instant('LanguageContents.Res_4429.Caption'));$scope.APIList=null;GetAPIuserDetails();}}});}}
$scope.checkAllAPIUsers=function(){if($scope.selectedAllAPIusers){$scope.selectedAllAPIusers=true;}else{$scope.selectedAllAPIusers=false;}
angular.forEach($scope.APIList,function(item){item.Selected=$scope.selectedAllAPIusers;});};$scope.RemoveCheckallAPISelection=function(list,$event){var checkbox=$event.target;list.Selected=checkbox.checked;var APIcollection=$.grep($scope.APIList,function(e){return e.Selected==true;}).length;if(APIcollection!=$scope.APIList.length)
$scope.selectedAllAPIusers=false;else
$scope.selectedAllAPIusers=true;}}
    app.controller("mui.admin.apiuserCtrl", ['$scope', '$location', '$resource', '$timeout', '$translate', 'AdminService', muiadminapiuserCtrl]);
})(angular, app);