using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// GanttHeaderBarDao object for table 'PM_GanttHeaderBar'.
	/// </summary>
	
	public partial class GanttHeaderBarDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected DateTime _startdate;
		protected DateTime _enddate;
		protected string _colorcode;
		protected string _description;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public GanttHeaderBarDao() {}
			
		public GanttHeaderBarDao(DateTime pStartDate, DateTime pEndDate, string pColorCode, string pDescription)
		{
			this._startdate = pStartDate; 
			this._enddate = pEndDate; 
			this._colorcode = pColorCode; 
			this._description = pDescription; 
		}
				
		public GanttHeaderBarDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual DateTime StartDate
		{
			get { return _startdate; }
			set { _bIsChanged |= (_startdate != value); _startdate = value; }
			
		}
		
		public virtual DateTime EndDate
		{
			get { return _enddate; }
			set { _bIsChanged |= (_enddate != value); _enddate = value; }
			
		}
		
		public virtual string ColorCode
		{
			get { return _colorcode; }
			set 
			{
			  if (value != null && value.Length > 10)
			    throw new ArgumentOutOfRangeException("ColorCode", "ColorCode value, cannot contain more than 10 characters");
			  _bIsChanged |= (_colorcode != value); 
			  _colorcode = value; 
			}
			
		}
		
		public virtual string Description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
			  _bIsChanged |= (_description != value); 
			  _description = value; 
			}
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string GanttHeaderBarDao = "GanttHeaderBarDao";			
			public const string Id = "Id";			
			public const string StartDate = "StartDate";			
			public const string EndDate = "EndDate";			
			public const string ColorCode = "ColorCode";			
			public const string Description = "Description";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string GanttHeaderBarDao = "GanttHeaderBarDao";			
			public const string Id = "Id";			
			public const string StartDate = "StartDate";			
			public const string EndDate = "EndDate";			
			public const string ColorCode = "ColorCode";			
			public const string Description = "Description";			
		}

		#endregion
		
		
	}
	
}
