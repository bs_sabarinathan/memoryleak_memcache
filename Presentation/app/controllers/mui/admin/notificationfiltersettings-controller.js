﻿(function (ng, app) {
    function muiadminnotificationfiltersettingsCntrl($scope, $location, $resource, $timeout, $translate, AdminService) {
        $scope.listofsubscriptionsForAdminSettings = []; function LoadSusbscripitontypes() {
            AdminService.GetAllSubscriptionType().then(function (res) {
                $scope.listofsubscriptionsForAdminSettings = $.grep(res.Response, function (e) {
                    return e.Id != 13;
                }); 
            });
        }
        $timeout(function () { LoadSusbscripitontypes(); }, 10); $scope.UpdateCheckedStatus = function (item, $event) {
            var notificationCheckedstatus = $event.target; if (notificationCheckedstatus.id == "Checkbox_1")
                item.isAppDefault = notificationCheckedstatus.checked; if (notificationCheckedstatus.id == "Checkbox_2")
                    item.isAppMandatory = notificationCheckedstatus.checked; if (notificationCheckedstatus.id == "Checkbox_3")
                        item.isMailDefault = notificationCheckedstatus.checked; if (notificationCheckedstatus.id == "Checkbox_4")
                            item.isMailMandatory = notificationCheckedstatus.checked;
        }
        $scope.SaveAdminNotificationSettings = function () {
            var saveadminsttngs = {}; saveadminsttngs.subscriptionObject = $scope.listofsubscriptionsForAdminSettings; AdminService.InsertAdminNotificationSettings(saveadminsttngs).then(function (res) {
                if (res.StatusCode == 405) { NotifyError($translate.instant('LanguageContents.Res_4329.Caption')); }
                else { NotifySuccess($translate.instant('LanguageContents.Res_4779.Caption')); }
            });
        }
    }
    app.controller("mui.admin.notificationfiltersettingsCntrl", ['$scope', '$location', '$resource', '$timeout', '$translate', 'AdminService', muiadminnotificationfiltersettingsCntrl]);
})(angular, app);