﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{
    class WidgetTemplateRoles
    {
        #region Member Variables

		protected int _id;
        protected int _WidgetTemplateID;
        protected int _RoleID;
		
		
		#endregion
		
		#region Constructors
		public WidgetTemplateRoles() {}

        public WidgetTemplateRoles(int pWidgetTemplateID, int pRoleID)
		{
            this._WidgetTemplateID = pWidgetTemplateID;
            this._RoleID = pRoleID; 
			
		}

        public WidgetTemplateRoles(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}

        public int WidgetTemplateID
		{
            get { return _WidgetTemplateID; }
            set { _WidgetTemplateID = value; }
			
		}

        public int RoleID
		{
            get { return _RoleID; }
            set { _RoleID = value; }
			
		}
		
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            WidgetTemplateRoles castObj = null;
			try
			{
                castObj = (WidgetTemplateRoles)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
    }
}
