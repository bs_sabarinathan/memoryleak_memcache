using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
	/// NotificationTypeDao object for table 'CM_NotificationType'.
	/// </summary>
	
	public partial class NotificationTypeDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected string _caption;
		protected SubscriptionTypeDao _subscriptiontypeid;
		protected string _template;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public NotificationTypeDao() {}
			
		public NotificationTypeDao(string pCaption, SubscriptionTypeDao pSubscriptionTypeid, string pTemplate)
		{
			this._caption = pCaption; 
			this._subscriptiontypeid = pSubscriptionTypeid; 
			this._template = pTemplate; 
		}
				
		public NotificationTypeDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
			  _bIsChanged |= (_caption != value); 
			  _caption = value; 
			}
			
		}
		
		public virtual SubscriptionTypeDao SubscriptionTypeid
		{
			get { return _subscriptiontypeid; }
			set { _bIsChanged |= (_subscriptiontypeid != value); _subscriptiontypeid = value; }
			
		}
		
		public virtual string Template
		{
			get { return _template; }
			set 
			{
			  if (value != null && value.Length > 1000)
			    throw new ArgumentOutOfRangeException("Template", "Template value, cannot contain more than 1000 characters");
			  _bIsChanged |= (_template != value); 
			  _template = value; 
			}
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string NotificationTypeDao = "NotificationTypeDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";			
			public const string SubscriptionTypeid = "SubscriptionTypeid";			
			public const string Template = "Template";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string NotificationTypeDao = "NotificationTypeDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";			
			public const string SubscriptionTypeid = "SubscriptionTypeid";			
			public const string Template = "Template";			
		}

		#endregion
		
		
	}
	
}
