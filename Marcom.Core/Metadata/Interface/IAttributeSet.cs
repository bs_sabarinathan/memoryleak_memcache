using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    /// <summary>
    /// IAttributeSet interface for table 'MM_AttributeSet'.
    /// </summary>
    public interface IAttributeSet
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
