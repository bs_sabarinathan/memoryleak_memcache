﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Task.Model
{
    public class ApprovalStepsDao : BaseDao, ICloneable
    {
        public virtual int ID { get; set; }
        public virtual int PhaseId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual int Duration { get; set; }
        public virtual int MinApproval { get; set; }
        public virtual string Roles { get; set; }
        public virtual Boolean IsMandatory { get; set; }
        public virtual int SortOrder { get; set; }
      


        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }
}
