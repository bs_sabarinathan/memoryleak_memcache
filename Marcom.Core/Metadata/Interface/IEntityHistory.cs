﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    /// <summary>
    /// IEntityType interface for table 'MM_EntityHistory'.
    /// </summary>
    public interface IEntityHistory
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int EntityID
        {
            get;
            set;

        }

        DateTimeOffset VisitedOn
        {
            get;
            set;

        }

        int UserID
        {
            get;
            set;

        }
        string EntityName
        {
            get;
            set;
        }
        string RoleName
        {
            get;
            set;
        }
        string ShortDescription
        {
            get;
            set;
        }

        string ColorCode
        {
            get;
            set;
        }
        #endregion
    }
}
