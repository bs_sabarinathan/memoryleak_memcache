﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace Presentation.Handlers
{
    /// <summary>
    /// Summary description for UserTitleImageHandler
    /// </summary>
    public class UserTitleImageHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            Decimal chunk = context.Request["chunk"] != null ? Decimal.Parse(context.Request["chunk"]) : 0;
            string fileName = context.Request["name"] != null ? context.Request["name"] : string.Empty;
            string TempName = context.Request["TemplateType"];

            string tenanturl = HttpContext.Current.Request.Url.Host;
            BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
            string TenantFilePath = tfp.GetTenantFilePathByHostName(tenanturl);

            //TODO: change as needed for your application
             string uploadPath;
           if(TempName=="media")
               uploadPath = context.Server.MapPath("~/" + TenantFilePath + "/DAMFiles/Templates/MediaGenerator/Images/ThumbNail/");
           else if (TempName == "word")
               uploadPath = context.Server.MapPath("~/" + TenantFilePath + "/DAMFiles/Templates/Word/Images/ThumbNail/");
           else
               uploadPath = context.Server.MapPath("~/assets/img/Temp/");
            //HttpContext.Current.Request.Files[0];
            HttpPostedFile fileUpload = HttpContext.Current.Request.Files[0];

            fileName = Guid.NewGuid().ToString() + new FileInfo(fileName).Extension;

            //fileName = "logo.png";

            //if (File.Exists(uploadPath + fileName))
            //{
            //    System.IO.File.Delete(uploadPath + fileName);
            //}

            using (var fs = new FileStream(Path.Combine(uploadPath, fileName), chunk == 0 ? FileMode.Create : FileMode.Append))
            {
                var buffer = new byte[fileUpload.InputStream.Length];
                fileUpload.InputStream.Read(buffer, 0, buffer.Length);

                fs.Write(buffer, 0, buffer.Length);
            }


            System.Drawing.Image objImage = System.Drawing.Image.FromFile(Path.Combine(uploadPath, fileName));
            var width = objImage.Width.ToString();
            var height = objImage.Height.ToString();

            
            context.Response.Write(fileName + ',' + width + ',' + height);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}