using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.ImportXml
{

	/// <summary>
	/// AttributeType object for table 'MM_AttributeType'.
	/// </summary>

    internal class AttributeTypeImport
	{
		#region Member Variables

        protected int _newid;
		protected int _id;
		protected string _caption;
		protected string _classname;
		protected bool _isselectable;
        protected string _datatype;
        protected string _sqltype;
        protected int _length;
        protected bool _isnullable;
		
		
		#endregion
		
		#region Constructors
		public AttributeTypeImport() {}

        public AttributeTypeImport(string pCaption, string pClassName, bool pIsSelectable, string pDataType, string pSqlType, int pLength, bool pIsNullable, int pNewId)
		{
			this._caption = pCaption; 
			this._classname = pClassName; 
			this._isselectable = pIsSelectable; 
            this._datatype = pDataType;
            this._sqltype = pSqlType;
            this._length = pLength;
            this._isnullable = pIsNullable;
            this._newid = pNewId;

		}

        public AttributeTypeImport(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}

        public int NewId
        {
            get { return _newid; }
            set { _newid = value; }

        }
		
		public string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
			  _caption = value; 
			}
			
		}
		
		public string ClassName
		{
			get { return _classname; }
			set 
			{
			  if (value != null && value.Length > 100)
			    throw new ArgumentOutOfRangeException("ClassName", "ClassName value, cannot contain more than 100 characters");
			  _classname = value; 
			}
			
		}
		
		public bool IsSelectable
		{
			get { return _isselectable; }
			set { _isselectable = value; }
			
		}

        public string DataType
        {
            get { return _datatype; }
            set { _datatype = value; }

        }

        public string SqlType
        {
            get { return _sqltype; }
            set { _sqltype = value; }

        }

        public int Length
        {
            get { return _length; }
            set { _length = value; }

        }

        public bool IsNullable
        {
            get { return _isnullable; }
            set { _isnullable = value; }
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            AttributeTypeImport castObj = null;
			try
			{
                castObj = (AttributeTypeImport)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
