﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{

    public interface IWorkFlowStepsWithTasks
    {
        #region Public Properties

        int StepID { get; set; }
        string StepName { get; set; }
        bool IsActive { get; set; }
        string WorkFloName { get; set; }
        int TotalActiveTasks { get; set; }
        dynamic TaskList
        {
            get;
            set;
        }

        #endregion
    }
}
