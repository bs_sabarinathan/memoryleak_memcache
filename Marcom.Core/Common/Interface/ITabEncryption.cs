﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    public interface ITabEncryption
    {
        int ID
        {
            get;
            set;

        }

        int CustomTabID
        {
            get;
            set;

        }

        string EncryKey
        {
            get;
            set;

        }

        string EncryIV
        {
            get;
            set;

        }

        string Algorithm
        {
            get;
            set;

        }

        string PaddingMode
        {
            get;
            set;

        }

        string CipherMode
        {
            get;
            set;

        }
    }
}
