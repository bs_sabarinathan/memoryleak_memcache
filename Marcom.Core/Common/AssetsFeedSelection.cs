﻿using BrandSystems.Marcom.Core.Common.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Common
{
    internal class AssetsFeedSelection : IAssetsFeedSelection
    {
        public int FeedId { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string UserImage { get; set; }
        public string FeedHappendTime { get; set; }
        public string FeedText { get; set; }
        public int Actor { get; set; }
        public int EntityID { get; set; }
        public int AssetId { get; set; }
        public string Entityname { get; set; }
        public string AssetName { get; set; }
        public int Versionno { get; set; }
        public int ActiveFileID { get; set; }
        public string Assetimagepath { get; set; }
        public int ProcessType { get; set; }
        public int Category { get; set; }
        public int EntitytypeId { get; set; }
        public bool Isobjective { get; set; }
    }


}
