﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public class AttrGroupDynamicAttributesDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected int _sortoder;
        protected int _predefinedId;
        //  protected Iesi.Collections.Generic.ISet<DynamicnewAttributes> _attributes;
        //protected List<KeyValuePair<String, object>> _attributes;

        protected IDictionary _attributes;

        #endregion

        #region Constructors
        public AttrGroupDynamicAttributesDao() { }

        public AttrGroupDynamicAttributesDao(int pId, int pEntityId, int pSortOrder, IDictionary pAttributes, int pPredefinedId)
        {
            this._id = pId;
            this._entityid = pEntityId;
            this._sortoder = pSortOrder;
            this._predefinedId = pPredefinedId;
            this._attributes = new Hashtable();
            this._attributes = pAttributes;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }
        public virtual int EntityId
        {
            get { return _entityid; }
            set { _entityid = value; }
        }

        public virtual int SortOrder
        {
            get { return _sortoder; }
            set { _sortoder = value; }
        }

        public virtual int PredefinedId
        {
            get { return _predefinedId; }
            set { _predefinedId = value; }
        }

        public virtual IDictionary Attributes
        {
            get { return _attributes; }
            set { _attributes = value; }

        }

        #endregion
        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AttrGroupDynamicAttributesDao = "AttrGroupDynamicAttributesDao";
            public const string Id = "Id";
            public const string EntityId = "EntityId";
            public const string SortOrder = "SortOrder";
            public const string Attributes = "Attributes";
            public const string PredefinedId = "PredefinedId";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AttrGroupDynamicAttributesDao = "AttrGroupDynamicAttributesDao";
            public const string Id = "Id";
            public const string EntityId = "EntityId";
            public const string SortOrder = "SortOrder";
            public const string Attributes = "Attributes";
            public const string PredefinedId = "PredefinedId";
        }

        #endregion
    }
}
