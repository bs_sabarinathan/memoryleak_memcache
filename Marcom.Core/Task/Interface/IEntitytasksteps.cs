﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Task.Interface
{
    public interface IEntitytasksteps
    {
        int ID { get; set; }
        string TempID { get; set; }
        int PhaseId { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        int Duration { get; set; }
        int MinApproval { get; set; }
        string Roles { get; set; }
        bool IsMandatory { get; set; }
        int SortOrder { get; set; }
        int Status { get; set; }
    }
}
