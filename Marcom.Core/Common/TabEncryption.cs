﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{
    internal class TabEncryption : ITabEncryption
    {
        #region Member Variables

        protected int _id;
        protected int _cutomtabid;
        protected string _encryKey;
        protected string _encryIV;
        protected string _algorithm;
        protected string _cipherMode;
        protected string _paddingMode;

        #endregion

         #region Constructors

        public TabEncryption() { }

        public TabEncryption(int pId, int pcutomtabid, string pencryKey, string pencryIV, string palgorithm, string ppaddingMode, string pcipherMode)
		{
			this._id = pId;
            this._cutomtabid = pcutomtabid;
            this._encryKey = pencryKey;
            this._encryIV = pencryIV;
            this._algorithm = palgorithm;
            this._paddingMode = ppaddingMode;
            this._cipherMode = pcipherMode; 
		}

        #endregion

        #region Public Properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public int CustomTabID
        {
            get { return _cutomtabid; }
            set {  _cutomtabid = value; }

        }

        public string EncryKey
        {
            get { return _encryKey; }
            set
            {
                if (value != null && value.Length > 1500)
                    throw new ArgumentOutOfRangeException("EncryKey", "EncryKey value, cannot contain more than 1500 characters");
                _encryKey = value;
            }

        }

        public string EncryIV
        {
            get { return _encryIV; }
            set
            {
                if (value != null && value.Length > 1500)
                    throw new ArgumentOutOfRangeException("EncryIV", "EncryIV value, cannot contain more than 1500 characters");
                _encryIV = value;
            }

        }

        public string Algorithm
        {
            get { return _algorithm; }
            set { _algorithm = value; }

        }

        public string PaddingMode
        {
            get { return _paddingMode; }
            set { _paddingMode = value; }

        }

        public string CipherMode
        {
            get { return _cipherMode; }
            set { _cipherMode = value; }

        }


        #endregion 			

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            Navigation castObj = null;
            try
            {
                castObj = (Navigation)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}
