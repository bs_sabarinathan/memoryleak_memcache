﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace BrandSystems.Marcom.Core.Dam
{
    public class AmazonS3FileStorageProvider
    {

        public string AWSAccessKeyID { get; set; }
        public string AWSSecretAccessKey { get; set; }
        public string BucketName { get; set; }
        public string RequestEndpoint { get; set; }
        public string PolicyDocument { get; set; }
        public string PolicyDocumentSignature { get; set; }
        public int FileStorageType { get; set; }

        public string UploaderUrl { get; set; }


    }
}