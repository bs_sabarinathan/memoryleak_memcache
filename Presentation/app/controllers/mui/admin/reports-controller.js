﻿(function (ng, app) {
    "use strict";

    function muiadminreportsCtrl($scope, $location, $resource, $timeout, $cookies, $translate, AdminService) {
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName).replace(/\\/g, "\/");

        $scope.ReportImgPath = "Files/ReportFiles/Images/Preview/";
        if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
            $scope.ReportImgPath = cloudpath + "/Files/ReportFiles/Images/Preview/";
        }
        $scope.myData = [];
        $scope.listdata = [];
        AdminService.GetReportByOID(0).then(function (res) {
            $scope.listdata = res.Response;
            $scope.myData = $scope.listdata;
        });
        $scope.reportcontrolclick = function () {
            $timeout(function () {
                $('[class=input-medium]').focus().select();
            }, 10);
        };
        $scope.save = function (row) {
            if (row.OID > 0 && (row.Caption.length > 0 || row.Preview.length > 0 || row.Description.length > 0) && (row.Caption != '-' || row.Description != '-' || row.Preview != 'NoPreview.jpg')) {
                var savecredential = {};
                if (row.Caption == "") {
                    row.Caption = "-";
                }
                if (row.Description == "") {
                    row.Description = "-";
                }
                savecredential.ID = row.ID;
                savecredential.OID = row.OID;
                savecredential.Name = row.Name;
                savecredential.Caption = row.Caption;
                savecredential.Description = row.Description;
                savecredential.Preview = row.Preview;
                savecredential.CategoryId = row.CategoryId;
                var IsShow;
                if (!row.Show == false) {
                    IsShow = 0;
                } else {
                    IsShow = 1;
                }
                savecredential.Show = IsShow;
                var IsEntityLevel;
                if (!row.EntityLevel == false) {
                    IsEntityLevel = 0;
                } else {
                    IsEntityLevel = 1;
                }
                savecredential.EntityLevel = IsEntityLevel;
                var IsSubLevel;
                if (!row.SubLevel == false) {
                    IsSubLevel = 0;
                } else {
                    IsSubLevel = 1;
                }
                savecredential.SubLevel = IsSubLevel;
                savecredential.ID = row.ID == undefined ? 0 : row.ID;
                AdminService.InsertUpdateReport(savecredential).then(function (res) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                });
                AdminService.GetReportByOID(0).then(function (res) {
                    $scope.listdata = res.Response;
                    $scope.myData = $scope.listdata;
                });
            } else {
                NotifyError($translate.instant('LanguageContents.Res_4373.Caption'));
            }
        }
        $scope.update = function (row, cbno) {
            if (row.OID > 0 && row.Caption.length > 0 && row.Id > 0 && row.Caption != '-' && row.Status == 'Linked') {
                if (row.Caption == "") {
                    row.Caption = "-";
                }
                if (row.Description == "") {
                    row.Description = "-";
                }
                var updatecredential = {};
                updatecredential.ID = row.Id;
                updatecredential.OID = row.OID;
                updatecredential.Name = row.Name;
                updatecredential.Caption = row.Caption;
                updatecredential.Description = row.Description;
                updatecredential.Preview = row.Preview;
                updatecredential.CategoryId = row.CategoryId;
                var IsShowupdate;
                if (cbno == "1") {
                    if (!row.Show == false) {
                        IsShowupdate = 0;
                    } else {
                        IsShowupdate = 1;
                    }
                } else {
                    if (!row.Show == false) {
                        IsShowupdate = 1;
                    } else {
                        IsShowupdate = 0;
                    }
                }
                updatecredential.Show = IsShowupdate;
                var IsEntityLevelupdate;
                if (cbno == "2") {
                    if (!row.EntityLevel == false) {
                        IsEntityLevelupdate = 0;
                    } else {
                        IsEntityLevelupdate = 1;
                    }
                } else {
                    if (!row.EntityLevel == false) {
                        IsEntityLevelupdate = 1;
                    } else {
                        IsEntityLevelupdate = 0;
                    }
                }
                updatecredential.EntityLevel = IsEntityLevelupdate;
                var IsSubLevelupdate;
                if (cbno == "3") {
                    if (!row.SubLevel == false) {
                        IsSubLevelupdate = 0;
                    } else {
                        IsSubLevelupdate = 1;
                    }
                } else {
                    if (!row.SubLevel == false) {
                        IsSubLevelupdate = 1;
                    } else {
                        IsSubLevelupdate = 0;
                    }
                }
                updatecredential.SubLevel = IsSubLevelupdate;
                AdminService.InsertUpdateReport(updatecredential).then(function (updatecredential) {
                    if (updatecredential.Response > 0) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_4372.Caption'));
                    }
                });
                AdminService.GetReportByOID(0).then(function (res) {
                    $scope.listdata = res.Response;
                    $scope.myData = $scope.listdata;
                });
            } else if (row.Caption == '-' && row.Status == 'Linked') {
                NotifyError($translate.instant('LanguageContents.Res_4584.Caption'));
            } else {
                NotifyError($translate.instant('LanguageContents.Res_4584.Caption'));
            }
        }
        $scope.reporteditcontrolclick = function () {
            $timeout(function () {
                $('[class=input-medium]').focus().select();
            }, 10);
        };
        $scope.UploadReportImageID = 0;
        $scope.UploadImagefile = function (OID, reportname) {
            $scope.UploadReportImageID = OID;
            $scope.ngUplaodImagediv = true;
            $scope.tempODI = OID;
            $scope.tempReportName = reportname;
            $scope.tempFile = '';
            $scope.tempResponse = '';
            $("#pickfiles").click();
        };
        $timeout(function () {
            StrartUpload();
        }, 50);
        var countUp = function () {
            StrartUpload();
            $timeout(function () {
                countUp();
            }, 500);
        }

        function StrartUpload() {
            var uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                browse_button: 'pickfiles',
                container: 'filescontainer',
                max_file_size: '10mb',
                flash_swf_url: 'js/plupload.flash.swf',
                silverlight_xap_url: 'js/plupload.silverlight.xap',
                url: 'Handlers/ReportImagePicHandler.ashx?Type=Attachment',
                multi_selection: false,
                filters: [{
                    title: "Image files",
                    extensions: "jpg,gif,png,jpeg"
                }]
            });
            uploader.bind('Init', function (up, params) { });
            uploader.init();
            uploader.bind('FilesAdded', function (up, files) {
                $.each(files, function (i, file) { });
                up.refresh();
                uploader.start();
            });
            uploader.bind('UploadProgress', function (up, file) {
                $('#uploadprogress .bar').css("width", file.percent + "%");
                $('#uploadprogress #barText').html(file.percent + "%");
            });
            uploader.bind('Error', function (up, err) {
                bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                up.refresh();
            });
            uploader.bind('FileUploaded', function (up, file, response) {
                $scope.tempFile = file;
                $scope.tempResponse = response;
                var img = new Image();
                img.src = 'Files/ReportFiles/Images/Temp/' + response.response;
                $('#uploadprogress').hide();
                $('#ReportImage').modal('show');
                $('#ReportImage #CropReportImg').attr('style', '');
                $('#ReportImage #CropReportImg').removeAttr('src').css('visibility', 'hidden');
                $('#ReportImage #CropReportImg').attr('src', 'Files/ReportFiles/Images/Temp/' + response.response.split(',')[0]);
                var w = parseInt(response.response.split(',')[1]);
                var h = parseInt(response.response.split(',')[2]);
                var path = response.response.split(',')[0];
                $timeout(function () {
                    AssigneNewImage(w, h, path);
                }, 200);
            });


        }
        var jcrop_api;

        function AssigneNewImage(w, h, path) {
            $('#ReportImage #CropReportImg').attr('src', 'Files/ReportFiles/Images/Temp/' + path);
            $('#ReportImage #CropReportImg').attr('data-width', w).attr('data-height', h);
            if (w >= h) {
                $('#ReportImage #CropReportImg').css('width', '704px');
                var imgheight = Math.round((h / w) * 704);
                var margine = Math.round((528 - imgheight) / 2);
                if (margine < 0) {
                    margine = 0;
                }
                $('#ReportImage #CropReportImg').parent().css('margin-top', margine + 'px');
            } else {
                $('#ReportImage #CropReportImg').css('height', '528px');
                $('#ReportImage #CropReportImg').parent().css('margin-top', '0px');
            }
            $('#ReportImage #CropReportImg').css('visibility', 'visible');
            initJcrop();
        }

        function initJcrop() {
            if ($('#ReportImage .jcrop-holder').length > 0) {
                jcrop_api.destroy();
            }
            $('#CropReportImg').Jcrop({
                onChange: showPreview,
                onSelect: showPreview,
                onRelease: releaseCheck
            }, function () {
                jcrop_api = this;
                jcrop_api.setOptions({
                    aspectRatio: 4 / 5
                });
                jcrop_api.focus();
                var w = $('#ReportImage #CropReportImg').width();
                var h = $('#ReportImage #CropReportImg').height();
                $scope.OrgimgWidth = w;
                $scope.OrgimgHeight = h;
                var nw = 0;
                var nh = 0;
                if (w <= h) {
                    nw = w;
                    nh = Math.round((w / 4) * 5);
                } else {
                    nh = h;
                    nw = Math.round((h / 4) * 5);
                }
                jcrop_api.animateTo([0, 0, nw, nh]);
            });

            function showPreview(c) {
                var orgw = $('#ReportImage #CropReportImg').attr('data-width');
                var orgh = $('#ReportImage #CropReportImg').attr('data-height');
                $scope.userimgWidth = Math.round((c.w / $scope.OrgimgWidth) * orgw);
                $scope.userimgHeight = Math.round((c.h / $scope.OrgimgHeight) * orgh);
                $scope.userimgX = Math.round((c.x / $scope.OrgimgWidth) * orgw);
                $scope.userimgY = Math.round((c.y / $scope.OrgimgHeight) * orgh);
            };

            function releaseCheck() {
                jcrop_api.setOptions({
                    allowSelect: true
                });
            };
        };
        $scope.UploadReportImage = function () {
            //AdminService.UpdateReportImage().then(function (res) {
            var saveobj = {};
            //saveobj.imgsourcepath = $("#CropReportImg").attr('src');
            var ImgPrvID = $scope.tempResponse.response.split(',')[0];
            saveobj.imgsourcepath = "/Files/ReportFiles/Images/Temp/" + ImgPrvID;
            saveobj.imgwidth = $scope.userimgWidth;
            saveobj.imgheight = $scope.userimgHeight;
            saveobj.imgX = $scope.userimgX;
            saveobj.imgY = $scope.userimgY;
            saveobj.OID = $scope.tempODI;
            saveobj.Preview = ImgPrvID;
            saveobj.ReportName = $scope.tempReportName;
            AdminService.UpdateReportImage(saveobj).then(function (getchangeimageResult) {
                var previewID = "reportPrv_" + $scope.UploadReportImageID;
                if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                    $("#" + previewID).attr('src', cloudpath + "/Files/ReportFiles/Images/Preview/" + ImgPrvID);
                }
                else {
                    $("#" + previewID).attr('src', "Files/ReportFiles/Images/Preview/" + ImgPrvID);
                }
                AdminService.GetReportByOID(0).then(function (res) {
                    $scope.listdata = res.Response;
                    $scope.myData = $scope.listdata;
                });
                NotifySuccess($translate.instant('LanguageContents.Res_4153.Caption'));
            });
            //});
        }
        $scope.UpdateReportValues = function (idcaption, id, oid, name, caption, description, preview, show, CategoryId, EntityLevel, SubLevel, newValue) {
            if (newValue == "") {
                newValue = "-";
            }
            if (idcaption == 1) {
                caption = newValue;
            } else {
                description = newValue;
            }
            var updatereport = {};
            updatereport.OID = oid;
            updatereport.Name = name;
            updatereport.Caption = $scope.DecodedTextName(caption);
            updatereport.Description = $scope.DecodedTextName(description);
            updatereport.Preview = preview;
            updatereport.CategoryId = CategoryId;
            var IsShow;
            if (show == "false") {
                IsShow = 0;
            } else if (show == "true") {
                IsShow = 1;
            } else {
                IsShow = 1;
            }
            updatereport.Show = IsShow;
            var IsEntityLevel;
            if (EntityLevel == "false") {
                IsEntityLevel = 0;
            } else if (EntityLevel == "true") {
                IsEntityLevel = 1;
            } else {
                IsEntityLevel = 1;
            }
            updatereport.EntityLevel = IsEntityLevel;
            var IsSubLevel;
            if (SubLevel == "false") {
                IsSubLevel = 0;
            } else if (SubLevel == "true") {
                IsSubLevel = 1;
            } else {
                IsSubLevel = 1;
            }
            updatereport.SubLevel = IsSubLevel;
            updatereport.ID = id == undefined ? 0 : id;
            AdminService.InsertUpdateReport(updatereport).then(function (res) {
                if (res.Response == true) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4805.Caption'));
                    AdminService.GetReportByOID(0).then(function (res) {
                        $scope.listdata = res.Response;
                        $scope.myData = $scope.listdata;
                    });
                }
            });
        }
        $scope.Clear = function () { }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.reportsCtrl']"));
        });

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.controller("mui.admin.reportsCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$translate', 'AdminService', muiadminreportsCtrl]);
})(angular, app);