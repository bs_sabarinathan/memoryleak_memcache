﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Access.Model
{

    public partial class SuperAdminDetailsDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _MenuID;    
        protected int _IsTopNavigation;
        protected string _Name;
        protected bool _accesspermission;
        protected int _Featureid;

        #endregion

        #region Constructors
        public SuperAdminDetailsDao() { }

        public SuperAdminDetailsDao(int pId, int pMenuID, int pIsTopNavigation, string pName, bool pAccessPermission,int pFeatureid)
        {
            this._id = pId;
            this._MenuID = pMenuID;
            this._IsTopNavigation = pIsTopNavigation;
            this._Name = pName;
            this._accesspermission = pAccessPermission;
            this._Featureid = pFeatureid;

        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int MenuID
        {
            get { return _MenuID; }
            set { _MenuID = value; }

        }

        public virtual int IsTopNavigation
        {
            get { return _IsTopNavigation; }
            set { _IsTopNavigation = value; }

        }

        public virtual string Name
        {
            get { return _Name; }
            set { _Name = value; }

        }
        public virtual bool AccessPermission
        {
            get { return _accesspermission; }
            set { _accesspermission = value; }

        }
        public virtual int FeatureID
        {
            get { return _Featureid; }
            set { _Featureid = value; }

        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string Id = "Id";
            public const string SuperAdminDetailsDao = " SuperAdminDetailsDao";
            public const string MenueID = "MenuID";
            public const string IsTopNavigation = "IsTopNavigation";
            public const string Name = "Name";
            public const string AccessPermission = "AccessPermission";
            public const string FeatureID = "FeatureID";

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string Id = "Id";
            public const string SuperAdminDetailsDao = " SuperAdminDetailsDao";
            public const string MenuID = "MenuID";
            public const string IsTopNavigation = "IsTopNavigation";
            public const string Name = "Name";
            public const string AccessPermission = "AccessPermission";
            public const string FeatureID = "FeatureID";
        }

        #endregion




    }

}
