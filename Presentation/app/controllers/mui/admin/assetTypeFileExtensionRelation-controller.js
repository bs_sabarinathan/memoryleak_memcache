﻿(function (ng, app) {
    "use strict";

    function muiassettypefileextensionrelationCtrl($scope, $resource, $timeout, $compile, $translate, AdminService) {
        $scope.FileExtID = 0;
        $scope.AssettypeFileExt = [];
        $scope.CategoryPath = '-';
        $scope.AssetTypeID = "";
        $scope.XmlAssetTypeList = [];
        $scope.AssetTypesList = [];
        $scope.FileExtension = "";
        $scope.selAssetType = [];
        $scope.tempHolder = '';
        AdminService.GetEntityType(5).then(function (entitytypes) {
            $scope.XmlAssetTypeList = entitytypes.Response;
        });
        $scope.GetAllFileExtension = function () {
            AdminService.GetAssetTypeFileExtensionRelation().then(function (data) {
                if (data.Response != null) {
                    $scope.AssettypeFileExt = data.Response;
                }
            });
        }
        $scope.GetAllFileExtension();
        $scope.AssetTypeTreePopUp = function (fileid) {
            $scope.filterval = '';
            var htmltree = '<category-tree tree-filter=\"filterval\" tree-data=\"AssetCategoryTree\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
            $("#dynamicAssetTypeTree").html($compile(htmltree)($scope));
            $("#AssetCategoryTree").modal('show');
        };
        $scope.AssetTypeCategorySelection = function (branch, parentArr) {
            var i = 0;
            if (branch.ischecked == true) {
                $scope.CategoryPath = "";
                for (i = parentArr.length - 1; i >= 0; i--) {
                    $scope.CategoryPath += parentArr[i].Caption + "/";
                }
                if ($scope.CategoryPath.length > 0) $scope.CategoryPath = $scope.CategoryPath.substring(0, $scope.CategoryPath.length - 1);
                var tempval = [];
                var tempasst = "";
                for (var k = 0; k < parentArr[0].Children.length; k++) {
                    tempasst = $.grep($scope.XmlAssetTypeList, function (e) {
                        return e.Id == parentArr[0].Children[k].id
                    });
                    if (tempasst != undefined && tempasst.length > 0) tempval.push(tempasst[0]);
                }
                if (tempval.length > 0) {
                    $scope.AssetTypesList = tempval;
                    $scope.AssetTypeID = branch.id;
                    if ($scope.tagAllOptions.data.length > 0) $scope.tagAllOptions.data.splice(0, $scope.tagAllOptions.data.length);
                    $.each($scope.AssetTypesList, function (i, el) {
                        $scope.tagAllOptions.data.push({
                            "id": el.Id,
                            "text": el.Caption,
                            "ShortDescription": el.ShortDescription,
                            "ColorCode": el.ColorCode
                        });
                    });
                    $scope.selAssetType.splice(0, $scope.selAssetType.length);
                    var tempTypeHolder = $.grep($scope.AssetTypesList, function (e) {
                        return e.Id == branch.id
                    });
                    $scope.selAssetType.push({
                        "id": tempTypeHolder[0].Id,
                        "text": tempTypeHolder[0].Caption,
                        "ShortDescription": tempTypeHolder[0].ShortDescription,
                        "ColorCode": tempTypeHolder[0].ColorCode
                    });
                    $scope.tempHolder = $scope.selAssetType[0];
                }
                $("#AssetCategoryTree").modal('hide');
            } else {
                $('#div_' + $scope.FileIdForAssetCategory).html($compile("")($scope));
                $scope.selAssetType = [];
                $scope.CategoryPath = "-";
                $scope.AssetTypeID = '';
                $scope.tempHolder = '';
                $scope.AssetTypesList.splice(0, $scope.AssetTypesList.length);
            }
        }
        $scope.EditAssetTypeFileExt = function (AssetTypeID, ID, Ext, CategoryPath) {
            $scope.CategoryPath = CategoryPath;
            $scope.FileExtension = Ext;
            $scope.FileExtID = ID;
            AdminService.GetAssetCategoryTree(AssetTypeID).then(function (data) {
                if (data.Response != null) {
                    $scope.AssetCategoryTree = JSON.parse(data.Response);
                    $scope.filterval = '';
                    var htmltree = '<category-tree tree-filter=\"filterval\" tree-data=\"AssetCategoryTree\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                    $("#dynamicAssetTypeTree").html($compile(htmltree)($scope));
                    AdminService.GetAssetCategoryPathInAssetEdit(AssetTypeID).then(function (data) {
                        if (data.Response != null) {
                            if (data.Response[1].AssetTypeIDs.length > 0) {
                                $scope.AssetTypesList = $.grep($scope.XmlAssetTypeList, function (e) {
                                    return $.inArray(e.Id, data.Response[1].AssetTypeIDs) != -1;
                                })
                            }
                            if ($scope.tagAllOptions.data.length > 0) $scope.tagAllOptions.data.splice(0, $scope.tagAllOptions.data.length);
                            $.each($scope.AssetTypesList, function (i, el) {
                                $scope.tagAllOptions.data.push({
                                    "id": el.Id,
                                    "text": el.Caption,
                                    "ShortDescription": el.ShortDescription,
                                    "ColorCode": el.ColorCode
                                });
                            });
                            $scope.selAssetType.splice(0, $scope.selAssetType.length);
                            var tempTypeHolder = $.grep($scope.AssetTypesList, function (e) {
                                return e.Id == AssetTypeID
                            });
                            $scope.selAssetType.push({
                                "id": tempTypeHolder[0].Id,
                                "text": tempTypeHolder[0].Caption,
                                "ShortDescription": tempTypeHolder[0].ShortDescription,
                                "ColorCode": tempTypeHolder[0].ColorCode
                            });
                            $scope.tempHolder = $scope.selAssetType[0];
                        }
                    });
                    $scope.AssetTypeID = AssetTypeID;
                }
            });
            $timeout(function () {
                $("#addFileExtensionPopup").modal('show');
            }, 100);
        }
        $scope.DeleteAssetTypeFileExt = function (ID) {
            AdminService.DeleteAssetTypeFileExtension(ID).then(function (res) {
                if (res.Response == true) $scope.AssettypeFileExt.splice($scope.AssettypeFileExt.map(function (x) {
                    return x.ID;
                }).indexOf(ID), 1);
            });
        }
        $scope.InsertAssetTypeFileExtension = function () {
            $scope.AssetTypeID = $scope.tempHolder.id;
            var AssetFileExtRelation = {};
            AssetFileExtRelation.ID = $scope.FileExtID;
            AssetFileExtRelation.AssetTypeID = $scope.AssetTypeID;
            AssetFileExtRelation.Category = $scope.CategoryPath;
            AssetFileExtRelation.FileExt = $scope.FileExtension;
            AdminService.InsertAssetTypeFileExtension(AssetFileExtRelation).then(function (res) {
                if (res.Response != null) {
                    $scope.GetAllFileExtension();
                }
            });
            $("#addFileExtensionPopup").modal('hide');
        }
        $scope.ReassignMembersData = [];
        $scope.formatResult = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="vertical-align: middle; margin-top: -4px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelection = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="vertical-align: middle; margin-top: -5px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.tagAllOptions = {
            multiple: false,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.ClearScope = function () {
            $scope.CategoryPath = '-';
            $scope.AssetTypeID = "";
            $scope.AssetTypesList.splice(0, $scope.AssetTypesList.length);
            $scope.FileExtension = "";
            $scope.FileExtID = 0;
            $scope.selAssetType = [];
            $scope.tempHolder = '';
            AdminService.GetAssetCategoryTree(0).then(function (data) {
                if (data.Response != null) {
                    $scope.AssetCategoryTree = JSON.parse(data.Response);
                    $scope.filterval = '';
                    var htmltree = '<category-tree tree-filter=\"filterval\" tree-data=\"AssetCategoryTree\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                    $("#dynamicAssetTypeTree").html($compile(htmltree)($scope));
                }
            });
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.assettypefileextensionrelationCtrl']"));
        });
    }
    app.controller('mui.admin.assettypefileextensionrelationCtrl', ['$scope', '$resource', '$timeout', '$compile', '$translate', 'AdminService', muiassettypefileextensionrelationCtrl]);
})(angular, app);