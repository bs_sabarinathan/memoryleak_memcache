﻿(function (ng, app) {
    function muiadminupdatesCtrl($scope, $location, $resource, $timeout, $cookies, $translate, AdminService) {
        $scope.shownewupdatesection = false; $scope.updateurl = ''; AdminService.GetUpdateSettings().then(function (getupdatesData) {
            var d = $scope.tempvariable; var userid = $cookies['UserId']; if (getupdatesData.Response != null) {
                $scope.UpdatesData = getupdatesData.Response.m_Item1; if ($scope.UpdatesData != null) {
                    if (getupdatesData.Response.m_Item2 != undefined) { $scope.updateurl = getupdatesData.Response.m_Item2 + "?InstanceID=" + $scope.UpdatesData.InstanceId + "&CurrentVersionId=" + $scope.UpdatesData.VersionId + "&CurrentSecId=" + parseInt($cookies['UserId']); }
                    $('#iframenewversion').attr('href', $scope.updateurl)
                    $scope.Client = $scope.UpdatesData.Client; $scope.CurrentVersion = $scope.UpdatesData.CurrentVersion; $scope.License = $scope.UpdatesData.License; $scope.AvailableUpdates = $scope.UpdatesData.AvailableUpdates; $scope.PreviousUpdates = $scope.UpdatesData.PreviousUpdates; $scope.prevFeatures = featurelist($scope.PreviousUpdates); $scope.availfeatures = featurelist($scope.AvailableUpdates);
                }
            }
        }); function featurelist(AvailableUpdates) {
            $scope.availfeatures = []; angular.forEach(AvailableUpdates, function (item) { var features = item.Feature.split('|'); angular.forEach(features, function (subitem) { $scope.availfeatures.push({ "Id": item.Id, "feature": subitem }); }) })
            return $scope.availfeatures;
        }
        $scope.filterfeature = function (item) {
            return function (feature) {
                if (item.Id == feature.Id)
                    return true; else
                    return false;
            }
        }; $scope.redirecttoUpgradeTool = function (toverion) { window.location = $scope.updateurl + '&Type=Upgrade&ToVersion=' + toverion; }; $scope.redirecttoUpgradeToolrevert = function (beforeverion) { window.location = $scope.updateurl + '&Type=Revert Upgrade&ToVersion=' + beforeverion; }; $scope.Downloadpdf = function (docpath, e) {
            if (docpath != '' && docpath != null) { $('#loadingDownloadPageModel').modal("show"); var a = document.createElement('a'), fileid = docpath, extn = 'forUpgradetool'; var filename = docpath; a.href = 'Download.aspx?FileID=' + docpath + '&FileFriendlyName=forUpgradetool&Ext=.pdf'; a.download = docpath; document.body.appendChild(a); $('#loadingDownloadPageModel').modal("hide"); a.click(); }
            else { bootbox.alert($translate.instant('LanguageContents.Res_4212.Caption')); }
        };
    }
    app.controller("mui.admin.updatesCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$translate', 'AdminService', muiadminupdatesCtrl]);
})(angular, app);