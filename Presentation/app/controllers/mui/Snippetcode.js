﻿(function () {
    var b = document.getElementsByTagName('body')[0],
    otherlib = false;
    var _JI;

    if (typeof jQuery != 'undefined') {
        msg = 'This page already using jQuery v' + jQuery.fn.jquery;
        return showMsg();
    } else if (typeof $ == 'function') {
        otherlib = true;
    }

    // more or less stolen form jquery core and adapted by paul irish
    function getScript(url, success) {
        var script = document.createElement('script');
        script.src = url;
        var head = document.getElementsByTagName('head')[0],
        done = false;
        // Attach handlers for all browsers
        script.onload = script.onreadystatechange = function () {
            if (!done && (!this.readyState
            || this.readyState == 'loaded'
            || this.readyState == 'complete')) {
                done = true;
                success();
                script.onload = script.onreadystatechange = null;
                head.removeChild(script);
            }
        };
        head.appendChild(script);
    }
    getScript('http://code.jquery.com/jquery.min.js', function () {
        if (typeof jQuery == 'undefined') {
            msg = 'Sorry, but jQuery wasn\'t able to load';
        } else {
            msg = 'This page is now jQuerified with v' + jQuery.fn.jquery;
            if (otherlib) { msg += ' and noConflict(). Use $jq(), not $().'; }
        }
        return showMsg();
    });
    function showMsg() {


        window.setTimeout(function () {
            if (typeof jQuery == 'undefined') {

            } else {

                if (otherlib) {
                    $jq = jQuery.noConflict();
                    _JI = $jq;

                }
                else
                    _JI = $;
            }
            pagestuff();
        }, 500);
    }

    var TypeList = '';
    var searchresult = '';



    function GenerateGUID() {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
        });
        return uuid;
    };



    var TypeList = '';
    var searchresult = '';
    var selectedoptionkey = -1;



    function pagestuff() {
        _JI(document).ready(function () {



            var counter = 2; drawinitialcontent();

            function drawinitialcontent() {
                var html = "";
                $.ajax({
                    type: "GET",
                    url: '@serviceurl/planning/getSearchSnippetSourceData',
                    contentType: "application/json",
                    crossDomain: true,
                    async: false,
                    success: function (result) {
                        if (result.StatusCode == 200) {
                            html = result.Response.Item1;
                            _JI("alsb-searchbox").append(html);
                            TypeList = result.Response.Item2;
                            var TypeHTML = "";
                            if (TypeList.length > 0) {
                                TypeHTML += "<ul>";
                                _JI.each(TypeList, function (key, value) {
                                    TypeHTML += "<li data-id='" + value.Id + "'><a><input type=\"checkbox\" id='" + value.Id + "' /><span>" + value.Caption + "</span></a></li>"
                                });
                                TypeHTML += "</ul>";
                            }

                            _JI("#searchresult-typeDropdown").append(TypeHTML);
                            setTimeout(function () {
                                initializePagefunctions();
                            }, 100);

                        }
                    },
                    error: function (data) {
                    },

                });
                var htmldiv = "";
                htmldiv += "  <iframe id=\"searchResultScreen-Iframe\"  marginheight=\"0\" align=\"middle\" scrolling=\"auto\" allowfullscreen onload=\"this.width=screen.width;this.height=screen.height;\" src=\"\"></iframe>";
                _JI("alsr-searchresults").append(htmldiv);

                setTimeout(function () {
                    $("#searchResultScreen-Iframe").hide();
                }, 0);
            }

            function performsearchforinput(searchtext) {
                setTimeout(function () {

                    var customval = $('alsb-searchbox').data('token'),
                                 dynToken = eval(customval);

                    var searchinputs = {
                        "Text": searchtext,
                        "Searchtype": "Assetlibrary",
                        "token": dynToken == null ? "" : decodeURIComponent(dynToken.replace(/\+/g, " "))
                    };

                    _JI("#searchresult-dropdown").css("display", "none");

                    var jsonsearchInputs = JSON.stringify(searchinputs);
                    $.ajax({
                        type: "POST",
                        url: '@serviceurl/planning/SnippetQuickWordSearch',
                        contentType: "application/json",
                        data: jsonsearchInputs,
                        crossDomain: true,
                        async: false,
                        success: function (result) {
                            if (result.StatusCode != 499) {
                                if (result.Response.Item2.Email.length > 0) {
                                    searchresult = result.Response.Item1.items;
                                    if (searchresult.length > 0) {
                                        selectedoptionkey = -1;
                                        bindtotextboxdropdowndiv(result.Response.Item1.items, searchtext);
                                    }
                                    else {
                                        _JI("#searchresult-dropdown").css("display", "none");
                                        _JI("#searchresult-typeDropdown").css("display", "none");
                                    }
                                }
                                else {
                                    _JI("#searchresult-dropdown").css("display", "none");
                                    _JI("#searchresult-typeDropdown").css("display", "none");
                                    console.log("Token mismatch");
                                }
                            }
                            else {
                                _JI("#searchresult-dropdown").css("display", "none");
                                _JI("#searchresult-typeDropdown").css("display", "none");
                                console.log("Token mismatch");

                            }
                        },
                        error: function (data) {
                            _JI("#searchresult-dropdown").css("display", "none");
                            _JI("#searchresult-typeDropdown").css("display", "none");
                            console.log("Token mismatch");
                        },

                    });
                }, 50);

            }

            function initializePagefunctions() {
                setTimeout(function () {

                    _JI("#searchresult-input").bind("keyup", function (e) {
                        var key = e.KeyCode || e.which;
                        if (key != 40 && key != 38 && key != 13) {
                            if (_JI("#searchresult-input").val().length == 0) {
                                selectedoptionkey = -1;
                                _JI("#searchresult-input").val("");
                                _JI("#searchresult-dropdown").hide();
                                _JI("#searchresult-dropdown").empty();
                                _JI("#searchresult-dropdown").css("display", "none");

                                performsearchforinput(String.fromCharCode(e.keyCode).toLowerCase());

                            }
                            else {

                                performsearchforinput(_JI("#searchresult-input").val());
                            }
                        }
                    });
                    _JI("#searchresult-typeSection").click(function (event) {
                        var targetCtrl = _JI(event.target);
                        var controlid = targetCtrl[0].id;
                        if (controlid == 'anchorSearchAll' || controlid == 'typeTextmain' || controlid == 'typeText' || controlid == 'imgcaretDown') {

                            _JI("#searchresult-dropdown").css("display", "none");

                            var d = _JI('#searchresult-typeDropdown').css('display');
                            if (d == 'block') {
                                _JI("#searchresult-typeDropdown").css("display", "none");
                            }
                            else {
                                _JI("#searchresult-typeDropdown").css("display", "block");
                            }
                        }
                    });


                    _JI(document).on('focus', "#searchresult-inputSection input[type='text']", function (e) {
                        setTimeout(function () {
                            _JI("#searchresult-typeDropdown").css("display", "none");
                            if (_JI("#searchresult-input").val().length > 0)
                                performsearchforinput(_JI("#searchresult-input").val());
                        }, 100);

                    });

                    _JI(document).on('focusout', "#searchresult-inputSection", function (e) {
                        setTimeout(function () {
                            _JI("#searchresult-dropdown").hide();
                            _JI("#searchresult-dropdown").empty();
                            _JI("#searchresult-typeDropdown").css("display", "none");
                            _JI("#searchresult-dropdown").css("display", "none");
                        }, 100);
                    });



                    //_JI(document).on('click', "#ulsearchdropdown li", function (e) {
                    //    var targetCtrl = _JI(e.target);
                    //    var selValue = targetCtrl[0].text;
                    //    _JI("#searchresult-input").val(selValue);
                    //    _JI("#searchresult-dropdown").css("display", "none");

                    //    var selassetTypeList = [];
                    //    _JI("#searchresult-typeDropdown ul li input[type='checkbox']").each(function () {
                    //        // var list = $(this).find("li input[type='checkbox']");
                    //        if ($(this).is(":checked")) {
                    //            selassetTypeList.push($(this)[0].id);
                    //        }
                    //    });

                    //    _JI("#searchresult-dropdown").css("display", "none");

                    //    var customval = $('alsb-searchbox').data('token'),
                    //                    dynToken = eval(customval);

                    //    var searchobj = {};
                    //    searchobj.nb = 0;
                    //    searchobj.lb = 0;
                    //    searchobj.ST = _JI("#searchresult-input").val();
                    //    searchobj.AT = selassetTypeList.join(',');
                    //    searchobj.Criteria = {}

                    //    $("#searchResultScreen-Iframe").show();

                    //    var loc = "@serviceurl/login.html?token=" + dynToken + "&searchobj=" + JSON.stringify(searchobj) + "";
                    //    //document.getElementById('searchResultScreen-Iframe').setAttribute('src', loc);
                    //    $('#searchResultScreen-Iframe').attr('src', loc);


                    //});


                    _JI("#searchButton").click(function () {
                        var selassetTypeList = [];
                        _JI("#searchresult-typeDropdown ul li input[type='checkbox']").each(function () {
                            // var list = $(this).find("li input[type='checkbox']");
                            if ($(this).is(":checked")) {
                                selassetTypeList.push($(this)[0].id);
                            }
                        });

                        _JI("#searchresult-dropdown").css("display", "none");

                        var customval = $('alsb-searchbox').data('token'),
                     dynToken = eval(customval);
                        var searchobj = {};
                        searchobj.nb = 0;
                        searchobj.lb = 0;
                        searchobj.ST = _JI("#searchresult-input").val();
                        searchobj.AT = selassetTypeList.join(',');
                        searchobj.Criteria = {}

                        $("#searchResultScreen-Iframe").show();

                        var loc = "@serviceurl/login.html?token=" + dynToken + "&searchobj=" + JSON.stringify(searchobj) + "";
                        document.getElementById('searchResultScreen-Iframe').setAttribute('src', loc);

                    });

                    _JI("#imgRemove").click(function () {
                        _JI("#searchresult-input").val("");
                        _JI("#searchresult-dropdown").hide();
                        selectedoptionkey = -1;
                    });

                    _JI(document).mousedown(function (e) {
                        var container = _JI("#searchresult-typeDropdown");
                        var targetelement = _JI(e.target);
                        if (!container.is(e.target) // if the target of the click isn't the container...
                            && container.has(e.target).length === 0 && targetelement[0].id != "searchresult-input") // ... nor a descendant of the container
                        {
                            container.hide();
                        }
                    });


                    _JI(document).on('keyup', "#searchresult-inputSection input[type='text']", function (e) {
                        var key = e.KeyCode || e.which;
                        if (e.keyCode == 13) {
                            _JI("#searchButton").click();
                        }
                        else if (_JI("#searchresult-input").val().length > 0) {
                            var searchtext = _JI("#searchresult-input").val();

                            if (key == 40) {
                                //console.log("im down arrow");
                                if (selectedoptionkey == -1) {
                                    var d = searchresult;
                                    //selectedoptionkey
                                    selectedoptionkey = 0;
                                    _JI("#li_0").addClass("active");
                                    var text = searchresult[selectedoptionkey].name;

                                    _JI("#searchresult-input").val(text);

                                }
                                else {
                                    if (searchresult.length != selectedoptionkey + 1) {
                                        _JI("#li_" + selectedoptionkey).removeClass("active");
                                        selectedoptionkey = selectedoptionkey + 1;
                                        var text = searchresult[selectedoptionkey].name;
                                        _JI("#li_" + selectedoptionkey).addClass("active");
                                        _JI("#searchresult-input").val(text);
                                    }
                                }
                            }
                            else if (key == 38) {
                                if (selectedoptionkey > 0) {
                                    _JI("#li_" + selectedoptionkey).removeClass("active");
                                    selectedoptionkey = selectedoptionkey - 1;
                                    var text = searchresult[selectedoptionkey].name;
                                    _JI("#li_" + selectedoptionkey).addClass("active");
                                    _JI("#searchresult-input").val(text);
                                }

                            }

                            //el.focus();

                        }

                    });

                }, 100);



            }

            function bindtotextboxdropdowndiv(searchlist, searchtext) {
                var searchHTML = "";
                _JI("#searchresult-dropdown").empty();
                if (searchlist.length > 0) {
                    searchHTML += "<ul id=\"ulsearchdropdown\">";
                    _JI.each(searchlist, function (key, value) {
                        var highlightedtext = value.name.replace(searchtext, "<b>" + searchtext + "</b>");
                        searchHTML += "<li id='li_" + key + "'><a id='a_" + key + "'>" + highlightedtext + "</a></li>";

                    });
                    searchHTML += "</ul>";
                }
                _JI("#searchresult-dropdown").css("display", "block");

                _JI("#searchresult-dropdown").append(searchHTML);
                _JI.each(searchlist, function (key, value) {
                    var but = document.getElementById("li_" + key);
                    but.setAttribute("onclick", searchselected);
                    but.onclick = searchselected;
                });
            }
            function searchselected(e) {
                var targetCtrl = _JI(e.target);
                var selValue = targetCtrl[0].text;
                _JI("#searchresult-input").val(selValue);
                _JI("#searchresult-dropdown").css("display", "none");
                var selassetTypeList = [];
                _JI("#searchresult-typeDropdown ul li input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        selassetTypeList.push($(this)[0].id);
                    }
                });
                _JI("#searchresult-dropdown").css("display", "none");
                var customval = $('alsb-searchbox').data('token');
                dynToken = eval(customval);
                var searchobj = {};
                searchobj.nb = 0;
                searchobj.lb = 0;
                searchobj.ST = _JI("#searchresult-input").val();
                searchobj.AT = selassetTypeList.join(',');
                searchobj.Criteria = {}
                $("#searchResultScreen-Iframe").show();
                var loc = "@serviceurl/login.html?token=" + dynToken + "&searchobj=" + JSON.stringify(searchobj) + "";
                $('#searchResultScreen-Iframe').attr('src', loc);
            }

            //// Find all iframes
            var $iframes = $("iframe");

            //// Find & save the aspect ratio for all iframes
            $iframes.each(function () {
                $(this).data("ratio", this.height / this.width)
                  // Remove the hardcoded width & height attributes
                  .removeAttr("width")
                  .removeAttr("height");
            });

            // Resize the iframes when the window is resized
            $(window).resize(function () {
                $iframes.each(function () {
                    // Get the parent container's width
                    var width = $(this).parent().width();
                    $(this).width(width)
                      .height(width * $(this).data("ratio"));
                });
                // Resize to fix all iframes on page load.
            }).resize();

        });


    }


})();