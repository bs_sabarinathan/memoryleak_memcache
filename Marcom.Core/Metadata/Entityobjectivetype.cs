﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class Entityobjectivetype : IEntityObjectiveType
    {
        #region Member Variables

        protected int _id;
        protected int _entitytypeid;
        protected int _objectivetypeid;
        protected int _objectivebasetypeid;
        protected int _unitid;


        #endregion

        #region Constructors
        public Entityobjectivetype() { }

        public Entityobjectivetype(int pId, int pEntitytypeid, int pObjectivetypeid)
        {
            this._id = pId;
            this._entitytypeid = pEntitytypeid;
            this._objectivetypeid = pObjectivetypeid;
        }

        public Entityobjectivetype(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }



        public virtual int EntitytypeId
        {
            get { return _entitytypeid; }
            set { _entitytypeid = value; }

        }
        public virtual int ObjectiveTypeId
        {
            get { return _objectivetypeid; }
            set { _objectivetypeid = value; }

        }

        public virtual int ObjectiveBaseTypeId
        {
            get { return _objectivebasetypeid; }
            set { _objectivebasetypeid = value; }

        }
        public virtual int UnitId
        {
            get { return _unitid; }
            set { _unitid = value; }

        }




        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            Entityobjectivetype castObj = null;
            try
            {
                castObj = (Entityobjectivetype)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}
