﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    internal class FinancialOverview : IFinancialOverview
    {
        public decimal TotalAssignedAmount { get; set; }
        public decimal PlannedAmount { get; set; }
        public decimal InRequests { get; set; }
        public decimal ApprovedBudget { get; set; }
        public decimal BudgetDeviation { get; set; }
        public decimal ApprovedAllocation { get; set; }
        public decimal UnAllocatedAmount { get; set; }
        public decimal Committed { get; set; }
        public decimal Spent { get; set; }
        public decimal AvailabletoSpent { get; set; }
        public decimal EntityTypeId { get; set; }
        public string LastUpdatedon { get; set; }
        public decimal SubAssignedAmount { get; set; }
        public decimal AvailableAssignedAmount { get; set; }
    }
}
