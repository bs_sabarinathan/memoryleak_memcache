﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface IAttributeGroup
    {
        #region Public Properties

        int Id
        {
            get;
            set;
        }

        string Caption
        {
            get;
            set;
        }
        string Description
        {
            get;
            set;
        }

        bool IsPredefined
        {
            get;
            set;
        }

        #endregion
    }
}
