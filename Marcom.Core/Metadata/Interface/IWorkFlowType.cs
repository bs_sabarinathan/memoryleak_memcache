﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    /// <summary>
    /// IAttribute interface for table 'MM_Attribute'.
    /// </summary>
    public interface IWorkFlowType
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }


        #endregion
    }
}
