using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// ICalenderTab interface for table 'PM_CalenderTab'.
    /// </summary>
    public interface ICalenderTab
    {
        #region Public Properties

        int Id
        {
            get;
            set;
        }
        int Calenderid
        {
            get;
            set;

        }

        int CustomTabid
        {
            get;
            set;

        }    
        #endregion
    }
}
