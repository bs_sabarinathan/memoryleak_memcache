﻿(function (ng, app) {
    "use strict"; function muiproofhqcrentialCntrl($scope, $location, $resource, $timeout, $translate, AdminService) {
        AdminService.GetProofHQSSettings().then(function (res) { var result = res.Response; $scope.proofHQusermailid = res.Response[0].UserCredential.UserName; $scope.proofHQpassword = res.Response[0].UserCredential.Password; }); function ValidateEmail(mail) {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) { return true }
            return false;
        }
        $scope.SaveProofRecord = function () {
            if ($scope.proofHQusermailid == undefined) { bootbox.alert($translate.instant('LanguageContents.Res_4666.Caption')); return false; }
            if (!ValidateEmail($scope.proofHQusermailid)) { bootbox.alert($translate.instant('LanguageContents.Res_4920.Caption')); return false; }
            if ($scope.proofHQpassword == undefined) { bootbox.alert($translate.instant('LanguageContents.Res_4667.Caption')); return false; }
            var addproofSettings = {}; addproofSettings.uname = $scope.proofHQusermailid; addproofSettings.password = $scope.proofHQpassword; AdminService.UpdateProofHQSSettings(addproofSettings).then(function (saveProofSettings) {
                if (saveProofSettings.Response == true)
                    NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption')); else
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
            });
        }
    }
    app.controller("mui.admin.muiproofhqcrentialCntrl", ['$scope', '$location', '$resource', '$timeout', '$translate', 'AdminService', muiproofhqcrentialCntrl]);
})(angular, app);