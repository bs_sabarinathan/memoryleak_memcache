using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
	/// UserMultiSubscriptionDao object for table 'CM_UserMultiSubscription'.
	/// </summary>
	
	public partial class UserMultiSubscriptionDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected SubscriptionTypeDao _subscriptionid;
		protected int _entitytypeid;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion

        #region Extensibility Method Definitions

        public override bool Equals(object obj)
        {
            UserMultiSubscriptionDao toCompare = obj as UserMultiSubscriptionDao;
            if (toCompare == null)
            {
                return false;
            }

            if (!Object.Equals(this.EntityTypeid, toCompare.EntityTypeid))
                return false;
            if (!Object.Equals(this.Subscriptionid, toCompare.Subscriptionid))
                return false;

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 13;
            hashCode = (hashCode * 7) + EntityTypeid.GetHashCode();
            hashCode = (hashCode * 7) + Subscriptionid.GetHashCode();
            return hashCode;
        }

        #endregion
		
		#region Constructors
		public UserMultiSubscriptionDao() {}
			
		public UserMultiSubscriptionDao(SubscriptionTypeDao pSubscriptionid, int pEntityTypeid)
		{
			this._subscriptionid = pSubscriptionid; 
			this._entitytypeid = pEntityTypeid; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual SubscriptionTypeDao Subscriptionid
		{
			get { return _subscriptionid; }
			set { _bIsChanged |= (_subscriptionid != value); _subscriptionid = value; }
			
		}
		
		public virtual int EntityTypeid
		{
			get { return _entitytypeid; }
			set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 			
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string UserMultiSubscriptionDao = "UserMultiSubscriptionDao";			
			public const string Subscriptionid = "Subscriptionid";			
			public const string EntityTypeid = "EntityTypeid";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string UserMultiSubscriptionDao = "UserMultiSubscriptionDao";			
			public const string Subscriptionid = "Subscriptionid";			
			public const string EntityTypeid = "EntityTypeid";			
		}

		#endregion
		
		
	}
	
}
