﻿using Alphaleonis.Win32.Filesystem;
using BrandSystems.Marcom.Core.AmazonStorageHelper;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.User.Interface;
using BrandSystems.Marcom.Utility;
using CSRedis;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BrandSystems.Marcom.Core.Utility
{
    public class MarcomCache<T>
    {
        #region Initialization

        private static string RedisHost = ConfigurationManager.AppSettings["RedisServer"].ToString();
        private static string RedisPassword = ConfigurationManager.AppSettings["RedisPassword"].ToString();
        private static TimeSpan CacheExpireTime = TimeSpan.Parse(ConfigurationManager.AppSettings["RedisExpireTime"]);
        private static string AppPath = ConfigurationManager.AppSettings["MarcomPresentation"];
        private static bool IsAmazonEC = ConfigurationManager.AppSettings["AmazonEC"] == "1";

        /*** Redis Initialization ***/
        RedisClient marcomCache = new RedisClient(RedisHost);

        #endregion

        #region Log
        public static string getSystemIP()
        {
            string hostName = Dns.GetHostName(); // Retrive the Name of HOST
            // Get the IP
            string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();
            return myIP;
        }
        public static void writeLog(string UserName, string SessionID, string msgType, string LogText)
        {
            string IP = getSystemIP();
            ClsDb.writeCacheLog(IP, UserName, SessionID, msgType, LogText);
        }
        #endregion

        #region Communicate with CacheServer

        /// <summary>
        /// Get the values from MarcomCache based on the CacheKey
        /// </summary>
        /// <param name="CacheKey"> </param>s
        /// <param name="SessionID"> </param>
        /// <returns></returns>
        private T Get(string CacheKey)
        {
            try
            {
                if (!IsAmazonEC)
                    marcomCache.Auth(RedisPassword); // For CsRedis only, not for ElasticCache 
                string CacheString = marcomCache.Get(CacheKey);
                string status = CacheString == null ? "Failed" : "Success";
                return JsonConvert.DeserializeObject<T>(CacheString);
            }
            catch (Exception Ex)
            {
                string error = " CacheKey :" + CacheKey + " \n Error Message : " + Ex.ToString();
                writeLog("", CacheKey.Split('_')[0], " Failed GetCache", error);
                return default(T);
            }
        }

        /// <summary>
        /// Store (CacheKey,Value) pair to MarcomCache
        /// </summary>
        /// <param name="CacheObj"> </param>
        /// <param name="CacheKey"> </param>
        private void Set(T CacheObj, string CacheKey)
        {
            try
            {
                if (!IsAmazonEC)
                    marcomCache.Auth(RedisPassword); // For CsRedis only, not for ElasticCache 
                marcomCache.Del(CacheKey);
                marcomCache.Set(CacheKey, JsonConvert.SerializeObject(CacheObj)); //Redis Cache
            }
            catch (Exception Ex)
            {
                string error = " CacheKey :" + CacheKey + " \n Error Message : " + Ex.ToString();
                writeLog("", CacheKey.Split('_')[0], "Failed Set/Delete Cache", error);
            }
        }

        /// <summary>
        /// Get the XMLString from MarcomCache based on the CacheKey
        /// </summary>
        /// <param name="CacheKey"> </param>
        /// <returns></returns>
        private string GetXML(string CacheKey)
        {
            try
            {
                if (!IsAmazonEC)
                    marcomCache.Auth(RedisPassword); // For CsRedis only, not for ElasticCache 

                string result = marcomCache.Get(CacheKey);
                string status = (result == null || result == "") ? "Failed" : "Success";

                return result;
            }
            catch (Exception Ex)
            {
                string error = " CacheKey :" + CacheKey + " \n Error Message : " + Ex.ToString();

                writeLog("", CacheKey.Split('_')[0], "Failed to GetXML", error);
                return null;
            }

        }

        /// <summary>
        /// Store (CacheKey,XMLString) pair to MarcomCache
        /// </summary>
        /// <param name="CacheObj"> </param>
        /// <param name="CacheKey"> </param>
        private void SetXML(string CacheObj, string CacheKey)
        {
            try
            {
                if (!IsAmazonEC)
                    marcomCache.Auth(RedisPassword); // For CsRedis only, not for ElasticCache 

                marcomCache.Del(CacheKey);
                marcomCache.Set(CacheKey, CacheObj); // For CsRedis only, not for ElasticCache 
            }
            catch (Exception Ex)
            {
                string error = " CacheKey :" + CacheKey.Split('_')[1] + " \n Error Message : " + Ex.ToString();
                writeLog("", CacheKey.Split('_')[0], "SetXML", error);
            }
        }

        /// <summary>
        ///  Set the Lock status for the MetadataSettings ApplyChanges 
        /// </summary>
        /// <param name="Lock"></param>
        /// <returns></returns>
        private void SetApplyChangesStatus(bool Lock)
        {
            if (!IsAmazonEC)
                marcomCache.Auth(RedisPassword);
            string lockStatus = Lock.ToString();
            marcomCache.Set(cacheKey._MetadataApplyChanges.ToString(), lockStatus);
        }

        /// <summary>
        /// Get the Lock status of the MetadataSettings ApplyChanges 
        /// </summary>
        private bool GetApplyChangesStatus()
        {
            if (!IsAmazonEC)
                marcomCache.Auth(RedisPassword);
            string lockStatus = marcomCache.Get(cacheKey._MetadataApplyChanges.ToString());
            if (lockStatus == null || lockStatus == "")
            {
                return true; //Return as Locked
            }
            return Convert.ToBoolean(lockStatus);
        }

        /// <summary>
        /// Removed the Cached (CacheKey,value) pair based on the SessionID but a Not XMLCache
        /// </summary>
        /// <param name="CacheObj"> </param>
        /// <param name="CacheKey"> </param>
        /// <param name="SessionID"> </param>
        public void Remove(Guid SessionID)
        {
            #region CacheKeys to Remove

            string UserCacheKey = SessionID.ToString() + "_marcomUsersCache";
            string EntitySortorderIdColle = SessionID.ToString() + "_EntitySortorderIdColle";
            string EntityFilterSortorderIDColln = SessionID.ToString() + "_EntityFilterSortorderIDColln";
            string EntitySortorderIdColleHash = SessionID.ToString() + "_EntitySortorderIdColleHash";
            string EntityMainQuery = SessionID.ToString() + "_EntityMainQuery";
            string GeneralColumnDefs = SessionID.ToString() + "_GeneralColumnDefs";

            #endregion

            #region Remove the Cahce

            try
            {
                if (!IsAmazonEC)
                    marcomCache.Auth(RedisPassword); // For CsRedis only, not for ElasticCache 

                marcomCache.Del(UserCacheKey);
                marcomCache.Del(EntitySortorderIdColle);
                marcomCache.Del(EntityFilterSortorderIDColln);
                marcomCache.Del(EntitySortorderIdColleHash);
                marcomCache.Del(EntityMainQuery);
                marcomCache.Del(GeneralColumnDefs);
            }
            catch (Exception Ex)
            {
                string error = " Error Message : " + Ex.ToString();
                writeLog("", SessionID.ToString(), "Failed to Removed the Cache of this Session", error);
            }

            #endregion
        }

        /// <summary>
        /// It will clear all cache 
        /// </summary>
        private void ClearAllCache()
        {
            if (!IsAmazonEC)
                marcomCache.Auth(RedisPassword); // For CsRedis only, not for ElasticCache 
            marcomCache.FlushAll();
        }

        #endregion

        #region MetadataSettings Applychanges

        public static bool SetApplyChangesLock(bool Status)
        {
            try
            {
                MarcomCache<T> Cache = new MarcomCache<T>();
                Cache.SetApplyChangesStatus(Status);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool GetApplyChangesLock()
        {
            try
            {
                MarcomCache<T> Cache = new MarcomCache<T>();
                return Cache.GetApplyChangesStatus();
            }
            catch
            {
                return true; // If failed to get 
            }
        }
        #endregion

        # region NonXML Cache

        public static T GetCache(string key, Guid SessionID)
        {
            MarcomCache<T> Cache = new MarcomCache<T>();
            key = SessionID.ToString() + key;
            return Cache.Get(key);
        }

        public static void SetCache(T CacheObj, string key, Guid SessionID)
        {
            MarcomCache<T> Cache = new MarcomCache<T>();
            key = SessionID.ToString() + key;
            Cache.Set(CacheObj, key);
        }


        public static bool RemoveCache(Guid SessionID)
        {
            try
            {
                MarcomCache<T> Cache = new MarcomCache<T>();
                Cache.Remove(SessionID);
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region XML Cache

        public static bool SaveXDocument(XDocument XDoc, xmlType Type, int TenantID, string metadataVersion = null)
        {

            MarcomCache<T> Cache = new MarcomCache<T>();
            IUser UserInfo = GetUserTenantsInfo(TenantID);
            string CacheKey = UserInfo.TenantName + "_" + Type.ToString();
            try
            {
                if (Type == xmlType.Metadata)
                {
                    CacheKey = CacheKey + "_" + metadataVersion;
                }

                string desireKey = string.Empty;
                switch (Type)
                {
                    case xmlType.Admin:
                        desireKey = UserInfo.TenantPath + "AdminSettings.xml";
                        if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                        {
                            AWSHelper.uploadXdocContent(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey, XDoc);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            XDoc.Save(desireKey);
                        }
                        break;
                    case xmlType.Metadata:
                        desireKey = UserInfo.TenantPath + "MetadataXML/MetadataVersion_V" + metadataVersion + ".xml";
                        if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                        {
                            AWSHelper.uploadXdocContent(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey, XDoc);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            XDoc.Save(desireKey);
                        }

                        break;
                    case xmlType.CurrentWorking:
                        desireKey = UserInfo.TenantPath + "MetadataXML/CurrentMetadataWorking.xml";
                        if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                        {
                            AWSHelper.uploadXdocContent(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey, XDoc);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            XDoc.Save(desireKey);
                        }

                        break;
                    case xmlType.CurrentSyncDB:
                        desireKey = UserInfo.TenantPath + "MetadataXML/CurrentSyncDBXML.xml";
                        if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                        {
                            AWSHelper.uploadXdocContent(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey, XDoc);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            XDoc.Save(desireKey);
                        }

                        break;
                    case xmlType.FutureMetadata:
                        desireKey = UserInfo.TenantPath + "MetadataXML/FutureMetadataWorking.xml";
                        if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                        {
                            AWSHelper.uploadXdocContent(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey, XDoc);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            XDoc.Save(desireKey);
                        }

                        break;
                    case xmlType.FutureSyncDB:
                        desireKey = UserInfo.TenantPath + "MetadataXML/FutureSyncDBXML.xml";
                        if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                        {
                            AWSHelper.uploadXdocContent(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey, XDoc);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            XDoc.Save(desireKey);
                        }

                        break;
                    case xmlType.InitialWorking:
                        desireKey = UserInfo.TenantPath + "MetadataXML/InitialWorkingXML.xml";
                        if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                        {
                            AWSHelper.uploadXdocContent(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey, XDoc);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            XDoc.Save(desireKey);
                        }

                        break;
                }

                Cache.SetXML(XDoc.ToString(), CacheKey);
                return true;
            }
            catch (Exception Ex)
            {
                string error = " CacheKey :" + CacheKey + " \n Error Message : " + Ex.ToString();
                writeLog("", CacheKey, "SaveXML", error);
                return false;
            }
        }

        public static bool SaveXElement(XElement Elements, xmlType Type, int TenantID, string metadataVersion = null)
        {
            MarcomCache<T> Cache = new MarcomCache<T>();
            IUser UserInfo = GetUserTenantsInfo(TenantID);
            string CacheKey = UserInfo.TenantName + "_" + Type.ToString();
            try
            {
                if (Type == xmlType.Metadata)
                {
                    CacheKey = CacheKey + "_" + metadataVersion;
                }
                string desireKey = string.Empty;
                switch (Type)
                {
                    case xmlType.Admin:
                        desireKey = UserInfo.TenantPath + "AdminSettings.xml";
                        if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                        {
                            AWSHelper.uploadXElemContent(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey, Elements);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            Elements.Save(desireKey);
                        }
                        break;
                    case xmlType.Metadata:
                        desireKey = UserInfo.TenantPath + "MetadataXML/MetadataVersion_V" + metadataVersion + ".xml";
                        if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                        {
                            AWSHelper.uploadXElemContent(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey, Elements);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            Elements.Save(desireKey);
                        }
                        break;
                    case xmlType.CurrentWorking:
                        desireKey = UserInfo.TenantPath + "MetadataXML/CurrentMetadataWorking.xml";
                        if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                        {
                            AWSHelper.uploadXElemContent(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey, Elements);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            Elements.Save(desireKey);
                        }
                        break;
                    case xmlType.CurrentSyncDB:
                        desireKey = UserInfo.TenantPath + "MetadataXML/CurrentSyncDBXML.xml";
                        if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                        {
                            AWSHelper.uploadXElemContent(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey, Elements);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            Elements.Save(desireKey);
                        }
                        break;
                    case xmlType.FutureMetadata:
                        desireKey = UserInfo.TenantPath + "MetadataXML/FutureMetadataWorking.xml";
                        if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                        {
                            AWSHelper.uploadXElemContent(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey, Elements);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            Elements.Save(desireKey);
                        }
                        break;
                    case xmlType.FutureSyncDB:
                        desireKey = UserInfo.TenantPath + "MetadataXML/FutureSyncDBXML.xml";
                        if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                        {
                            AWSHelper.uploadXElemContent(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey, Elements);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            Elements.Save(desireKey);
                        }
                        break;
                    case xmlType.InitialWorking:
                        desireKey = UserInfo.TenantPath + "MetadataXML/InitialWorkingXML.xml";
                        if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                        {
                            AWSHelper.uploadXElemContent(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey, Elements);
                        }
                        else
                        {
                            desireKey = AppPath + desireKey;
                            Elements.Save(desireKey);
                        }
                        break;
                }
                Cache.SetXML(Elements.ToString(), CacheKey);
                return true;
            }
            catch (Exception Ex)
            {
                string error = " CacheKey :" + CacheKey + " \n Error Message : " + Ex.ToString();
                writeLog("", CacheKey, "SaveXML", error);
                return false;
            }
        }

        public static XDocument ReadXDocument(xmlType Type, int TenantID, string metadataVersion = null)
        {
            writeLog("", "", "XMLType ", "XMLType : " + Type.ToString());
            MarcomCache<T> Cache = new MarcomCache<T>();
            IUser UserInfo = GetUserTenantsInfo(TenantID);
            string CacheKey = UserInfo.TenantName + "_" + Type.ToString();
            try
            {
                if (Type == xmlType.Metadata)
                {
                    CacheKey = CacheKey + "_" + metadataVersion;
                }
                string CacheString = string.Empty;
                XDocument Result = new XDocument();
                try
                {
                    CacheString = Cache.GetXML(CacheKey);
                }
                catch
                {
                    Result = ReadXDocFromFile(Type, UserInfo, metadataVersion);
                    CacheString = Result.ToString();
                    Cache.SetXML(CacheString, CacheKey);
                }
                if (CacheString == null || CacheString == "")
                {
                    Result = ReadXDocFromFile(Type, UserInfo, metadataVersion);
                    CacheString = Result.ToString();
                    Cache.SetXML(CacheString, CacheKey);
                }
                return XDocument.Parse(CacheString);
            }
            catch (Exception Ex)
            {
                string error = " CacheKey :" + CacheKey + " \n Error Message : " + Ex.ToString();
                writeLog("", CacheKey, "ReadXML", error);
                return null;
            }
        }

        public static XElement ReadXElement(xmlType Type, int TenantID, string metadataVersion = null)
        {
            MarcomCache<T> Cache = new MarcomCache<T>();
            IUser UserInfo = GetUserTenantsInfo(TenantID);
            string CacheKey = UserInfo.TenantName + "_" + Type.ToString();
            try
            {
                if (Type == xmlType.Metadata)
                {
                    CacheKey = CacheKey + "_" + metadataVersion;
                }
                string CacheString = string.Empty;
                XElement Result = null;
                try
                {
                    CacheString = Cache.GetXML(CacheKey);
                }
                catch
                {
                    Result = ReadXElemetFromFile(Type, UserInfo, metadataVersion);
                    CacheString = Result.ToString();
                    Cache.SetXML(CacheString, CacheKey);
                }
                if (CacheString == null || CacheString == "")
                {
                    Result = ReadXElemetFromFile(Type, UserInfo, metadataVersion);
                    CacheString = Result.ToString();
                    Cache.SetXML(CacheString, CacheKey);
                }
                return XElement.Parse(CacheString);
            }
            catch (Exception Ex)
            {
                string error = " CacheKey :" + CacheKey + " \n Error Message : " + Ex.ToString();
                writeLog("", CacheKey, "ReadXML", error);
                return null;
            }
        }

        private static XDocument ReadXDocFromFile(xmlType Type, IUser UserInfo, string metadataVersion)
        {
            XDocument Result = new XDocument();
            string desireKey = string.Empty;
            writeLog("", UserInfo.TenantName, "Try to ReadXML from File", "CacheKey : " + UserInfo.TenantName + "_" + Type.ToString() + " , Status : Started");

            switch (Type)
            {
                case xmlType.Admin:
                    desireKey = UserInfo.TenantPath + "AdminSettings.xml";
                    if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        Result = AWSHelper.ReadAdminXDocument(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XDocument.Load(desireKey);
                    }
                    break;
                case xmlType.Metadata:

                    desireKey = UserInfo.TenantPath + "MetadataXML/MetadataVersion_V" + metadataVersion + ".xml";
                    if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        Result = AWSHelper.ReadAdminXDocument(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XDocument.Load(desireKey);
                    }
                    break;
                case xmlType.CurrentWorking:
                    desireKey = UserInfo.TenantPath + "MetadataXML/CurrentMetadataWorking.xml";
                    if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        Result = AWSHelper.ReadAdminXDocument(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XDocument.Load(desireKey);
                    }
                    break;
                case xmlType.CurrentSyncDB:
                    desireKey = UserInfo.TenantPath + "MetadataXML/CurrentSyncDBXML.xml";
                    if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        Result = AWSHelper.ReadAdminXDocument(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XDocument.Load(desireKey);
                    }
                    break;
                case xmlType.FutureMetadata:
                    desireKey = UserInfo.TenantPath + "MetadataXML/FutureMetadataWorking.xml";
                    if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        Result = AWSHelper.ReadAdminXDocument(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XDocument.Load(desireKey);
                    }
                    break;
                case xmlType.FutureSyncDB:
                    desireKey = UserInfo.TenantPath + "MetadataXML/FutureSyncDBXML.xml";
                    if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        Result = AWSHelper.ReadAdminXDocument(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XDocument.Load(desireKey);
                    }
                    break;
                case xmlType.InitialWorking:
                    desireKey = UserInfo.TenantPath + "MetadataXML/InitialWorkingXML.xml";
                    if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        Result = AWSHelper.ReadAdminXDocument(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XDocument.Load(desireKey);
                    }
                    break;

            }
            writeLog("", UserInfo.TenantName, "Successfully Read the XML", "CacheKey : " + UserInfo.TenantName + "_" + Type.ToString() + " , Status : Successs");
            return Result;
        }

        private static XElement ReadXElemetFromFile(xmlType Type, IUser UserInfo, string metadataVersion)
        {
            XElement Result = null;
            string desireKey = string.Empty;
            writeLog("", UserInfo.TenantName, "Try to ReadXML from File", "CacheKey : " + UserInfo.TenantName + "_" + Type.ToString() + " , Status : Started");

            switch (Type)
            {
                case xmlType.Admin:
                    desireKey = UserInfo.TenantPath + "AdminSettings.xml";
                    if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        Result = AWSHelper.ReadAdminXElement(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XElement.Load(desireKey);
                    }
                    break;
                case xmlType.Metadata:
                    desireKey = UserInfo.TenantPath + "MetadataXML/MetadataVersion_V" + metadataVersion + ".xml";
                    if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        Result = AWSHelper.ReadAdminXElement(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XElement.Load(desireKey);
                    }
                    break;
                case xmlType.CurrentWorking:
                    desireKey = UserInfo.TenantPath + "MetadataXML/CurrentMetadataWorking.xml";
                    if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        Result = AWSHelper.ReadAdminXElement(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XElement.Load(desireKey);
                    }
                    break;
                case xmlType.CurrentSyncDB:
                    desireKey = UserInfo.TenantPath + "MetadataXML/CurrentSyncDBXML.xml";
                    if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        Result = AWSHelper.ReadAdminXElement(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XElement.Load(desireKey);
                    }
                    break;
                case xmlType.FutureMetadata:
                    desireKey = UserInfo.TenantPath + "MetadataXML/FutureMetadataWorking.xml";
                    if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        Result = AWSHelper.ReadAdminXElement(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XElement.Load(desireKey);
                    }
                    break;
                case xmlType.FutureSyncDB:
                    desireKey = UserInfo.TenantPath + "MetadataXML/FutureSyncDBXML.xml";
                    if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        Result = AWSHelper.ReadAdminXElement(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XElement.Load(desireKey);
                    }
                    break;
                case xmlType.InitialWorking:
                    desireKey = UserInfo.TenantPath + "MetadataXML/InitialWorkingXML.xml";
                    if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        Result = AWSHelper.ReadAdminXElement(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey);
                    }
                    else
                    {
                        desireKey = AppPath + desireKey;
                        Result = XElement.Load(desireKey);
                    }
                    break;
            }
            writeLog("", UserInfo.TenantName, "Successfully Read the XML", "CacheKey : " + UserInfo.TenantName + "_" + Type.ToString() + " , Status : Successs");
            return Result;
        }

        /// <summary>
        /// It will Read corresponding File path as XDocument and will return the result
        /// </summary>
        /// <param name="TenantID"></param>
        /// <returns></returns>
        public static XDocument GetXmlWorkingPath(int TenantID)
        {
            try
            {
                XDocument XDoc = new XDocument();

                if (!MarcomManagerFactory.viewOldMetadataVersion[TenantID])
                {
                    if (MarcomManagerFactory.IsWorkingWithCurrentWorkingVersion[TenantID])
                        XDoc = ReadXDocument(xmlType.CurrentWorking, TenantID);
                    else
                        XDoc = ReadXDocument(xmlType.FutureMetadata, TenantID);
                }
                else
                {
                    string metadataVersion = MarcomManagerFactory.oldMetadataVersionNumber[TenantID];
                    XDoc = ReadXDocument(xmlType.Metadata, TenantID, metadataVersion);
                }
                return XDoc;
            }
            catch (Exception Ex)
            {
                writeLog("", "", "GetXmlWorkingPath", "Failed to read, Error Message : " + Ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// It will Read corresponding File path as XDocument and will return the result
        /// </summary>
        /// <param name="TenantID"></param>
        /// <returns></returns>
        public static XDocument GetSynchXMLPath(int TenantID)
        {
            try
            {
                XDocument XDoc = new XDocument();
                if (MarcomManagerFactory.IsWorkingWithCurrentWorkingVersion[TenantID])
                    XDoc = ReadXDocument(xmlType.CurrentSyncDB, TenantID);
                else
                    XDoc = ReadXDocument(xmlType.FutureSyncDB, TenantID);
                return XDoc;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// It will Read corresponding File path as XDocument and will return the result
        /// </summary>
        /// <param name="IsCurrentVersion"></param>
        /// <param name="TenantID"></param>
        /// <returns></returns>
        public static XDocument GetSynchXMLPath(bool IsCurrentVersion, int TenantID)
        {
            try
            {
                XDocument XDoc = new XDocument();
                if (IsCurrentVersion)
                    XDoc = ReadXDocument(xmlType.CurrentSyncDB, TenantID);
                else
                    XDoc = ReadXDocument(xmlType.FutureSyncDB, TenantID);

                return XDoc;
            }
            catch
            {
                return null;

            }
        }

        /// <summary>
        /// It will Read corresponding File path as XDocument and will return the result
        /// </summary>
        /// <param name="TenantID"></param>
        /// <returns></returns>
        public static XDocument GetActiveVersionXmlPath(int TenantID)
        {
            try
            {
                //writeLog("", "", "GetActiveVersionXmlPath", "Reading MetadataVersion XML");
                string Version = MarcomManagerFactory.ActiveMetadataVersionNumber[TenantID].ToString();
                return ReadXDocument(xmlType.Metadata, TenantID, Version);
            }
            catch (Exception Ex)
            {
                writeLog("", "", "GetActiveVersionXmlPath", "Failed to read, Error Message : " + Ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// It will save the Xdocument in to the corresponding File
        /// </summary>
        /// <param name="XDoc"></param>
        /// <param name="TenantID"></param>
        /// <returns></returns>
        public static bool SaveXmlWorkingPath(XDocument XDoc, int TenantID)
        {
            try
            {
                bool result = false;

                if (!MarcomManagerFactory.viewOldMetadataVersion[TenantID])
                {
                    if (MarcomManagerFactory.IsWorkingWithCurrentWorkingVersion[TenantID])
                        result = SaveXDocument(XDoc, xmlType.CurrentWorking, TenantID);
                    else
                        result = SaveXDocument(XDoc, xmlType.FutureMetadata, TenantID);
                }
                else
                {
                    string metadataVersion = MarcomManagerFactory.oldMetadataVersionNumber[TenantID];
                    result = SaveXDocument(XDoc, xmlType.Metadata, TenantID, metadataVersion);
                }
                return result;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// It will save the XElement in to the corresponding File
        /// </summary>
        /// <param name="XDoc"></param>
        /// <param name="TenantID"></param>
        /// <returns></returns>
        public static bool SaveXmlWorkingPath(XElement XDoc, int TenantID)
        {
            try
            {
                bool result = false;

                if (!MarcomManagerFactory.viewOldMetadataVersion[TenantID])
                {
                    if (MarcomManagerFactory.IsWorkingWithCurrentWorkingVersion[TenantID])
                        result = SaveXElement(XDoc, xmlType.CurrentWorking, TenantID);
                    else
                        result = SaveXElement(XDoc, xmlType.FutureMetadata, TenantID);
                }
                else
                {
                    string metadataVersion = MarcomManagerFactory.oldMetadataVersionNumber[TenantID];
                    result = SaveXElement(XDoc, xmlType.Metadata, TenantID, metadataVersion);
                }
                return result;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// It will save the Xdocument in to the corresponding File
        /// </summary>
        /// <param name="XDoc"></param>
        /// <param name="TenantID"></param>
        /// <returns></returns>
        public static bool SaveSynchXMLPath(XDocument XDoc, int TenantID)
        {
            try
            {
                bool result = false;
                if (MarcomManagerFactory.IsWorkingWithCurrentWorkingVersion[TenantID])
                    result = SaveXDocument(XDoc, xmlType.CurrentSyncDB, TenantID);
                else
                    result = SaveXDocument(XDoc, xmlType.FutureSyncDB, TenantID);
                return result;
            }
            catch
            {
                return false;
            }
        }

        public static bool BackUpMetadatXML(XDocument XDoc, string backupPath, int TenantID)
        {
            try
            {
                IUser UserInfo = GetUserTenantsInfo(TenantID);

                if (UserInfo.AwsStorage.storageType == (int)StorageArea.Amazon)
                {
                    string desireKey = backupPath.Replace(AppPath, "");
                    AWSHelper.uploadXdocContent(UserInfo.AwsStorage.S3, UserInfo.AwsStorage.BucketName, desireKey, XDoc);
                }
                else
                {
                    XDoc.Save(backupPath);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        public static void ClearCache()
        {
            MarcomCache<T> cache = new MarcomCache<T>();
            cache.ClearAllCache();
        }

        #region Get TenantsInfo

        public static IUser GetUserTenantsInfo(int TenantID)
        {
            BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
            string TenantFilePath = tfp.GetTenantFilePath(TenantID);
            string TenantHost = tfp.GetTenantHostURLbyTenantID(TenantID);
            string TenantName = tfp.GetTenantNamebyTenantID(TenantID);
            BrandSystems.Marcom.Core.User.Interface.IUser user = new User.User();
            user.TenantID = TenantID;
            user.TenantPath = TenantFilePath;
            user.TenantName = TenantName;
            user.TenantHost = TenantHost;
            user.AwsStorage = tfp.GetTenantAmazonStoragebytenantId(TenantID);

            return user;
        }
        #endregion
    }
}
