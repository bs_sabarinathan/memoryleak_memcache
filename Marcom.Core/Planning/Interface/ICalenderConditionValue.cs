using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// ICalenderConditionValue interface for table 'PM_CalenderConditionValue'.
    /// </summary>
    public interface ICalenderConditionValue
    {
        #region Public Properties

        int Id
        {
            get;
            set;
        }

        int Conditionid
        {
            get;
            set;

        }

        int Value
        {
            get;
            set;

        }
        #endregion
    }
}
