/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

	/// <summary>
    /// Module object for table 'MM_TreeLevel'.
	/// </summary>
	
	internal class TreeLevel : ITreeLevel 
	{
		#region Member Variables

		protected int _id;
        protected int _level;
		protected string _levelname;
		protected int _attributeid;
        protected bool _isPercentage;
		
		
		#endregion
		
		#region Constructors
		public TreeLevel() {}

        public TreeLevel(int pId, int pLevel, string pLevelname, int pAttributeid,bool pIsPercentage)
		{
            this._id = pId;
            this._level = pLevel;
            this._levelname = pLevelname;
            this._attributeid = pAttributeid; 
            this._isPercentage = pIsPercentage;
		}

        public TreeLevel(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}

        public virtual int Level
        {
            get { return _level; }
            set { _level = value; }

        }

        public virtual string LevelName
        {
            get { return _levelname; }
            set { _levelname = value;
            }

        }

        public virtual int AttributeID
        {
            get { return _attributeid; }
            set { _attributeid = value; }

        }
        public virtual bool IsPercentage
        {
            get { return _isPercentage; }
            set { _isPercentage = value; }

        }
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			Module castObj = null;
			try
			{
				castObj = (Module)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
