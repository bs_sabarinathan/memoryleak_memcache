using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IObjectiveUnit interface for table 'PM_Objective_Unit'.
    /// </summary>
    public interface IObjectiveUnit
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
