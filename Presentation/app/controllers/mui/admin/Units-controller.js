﻿(function (ng, app) {
    "use strict";

    function muiadminunitsCtrl($scope, $location, $resource, $timeout, $cookies, $compile, $translate, AdminService) {
        AdminService.GetUnits().then(function (res) {
            $scope.listUnits = res.Response;
            $scope.listunitsData = $scope.listUnits;
        });
        $scope.slno = '<span class="slno">{{row.rowIndex+1}}</span>';
        $scope.editableInPopup = '<a class="iconLink" data-toggle="modal" ng-click="GetUnitsByID(row)" data-toggle="modal" data-target="#unitsModal"><i class="icon-edit"></i></a> '
        $scope.Delete = '<a class="iconLink" data-toggle="modal" ng-click="DeleteUnitsByid(row)" data-toggle="modal"><i class="icon-remove"></i></a> '
        $scope.filterOptions = {
            filterText: ''
        };
        $scope.gridGetUnits = {
            data: 'listunitsData',
            enablePinning: false,
            columnDefs: [{
                field: "SlNo",
                displayName: $translate.instant('LanguageContents.Res_4753.Caption'),
                cellTemplate: $scope.slno,
                width: 60
            }, {
                field: "Caption",
                displayName: $translate.instant('LanguageContents.Res_44.Caption'),
                width: 180
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.editableInPopup,
                width: 30
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.Delete,
                width: 40
            }]
        };
        $scope.GetUnitsByID = function GetUnitsByID(row) {
            $scope.Id = row.entity.Id;
            $scope.Caption = row.entity.Caption;
            $scope.unitsadd = false;
            $scope.unitsupdate = true;
            $timeout(function () {
                $('#Caption').focus().select();
            }, 1000);
            AdminService.GetUnits().then(function (res) {
                $scope.listunits = res.Response;
                $scope.listunitsData = $scope.listunits;
            });
        };
        $scope.addUnits = function () {
            $('#unitsModal').modal('show');
            $scope.slno = 0;
            $scope.Id = 0;
            $scope.Caption = '';
            $scope.unitsadd = true;
            $scope.unitsupdate = false;
            $timeout(function () {
                $('#Caption').focus();
            }, 1000);
        };
        $scope.UpdateUnits = function () {
            if ($scope.Caption == '' || $scope.Caption == undefined) {
                bootbox.alert($translate.instant('LanguageContents.Res_1849.Caption'));
                return true;
            }
            if ($scope.Caption != '') {
                var addunits = {};
                addunits.ID = $scope.Id;
                addunits.Caption = $scope.Caption;
                AdminService.InsertUpdateUnits(addunits).then(function (addunitsvalues) {
                    if (addunitsvalues.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4347.Caption'));
                    } else {
                        AdminService.GetUnits().then(function (GetUnits) {
                            $scope.listUnits = GetUnits.Response;
                            $scope.listunitsData = $scope.listUnits;
                        });
                        $('#unitsModal').modal('hide');
                        NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                    }
                });
            }
        };
        $scope.SaveUnits = function () {
            if ($scope.Caption == '' || $scope.Caption == undefined) {
                bootbox.alert($translate.instant('LanguageContents.Res_1849.Caption'));
                return true;
            }
            if ($scope.Caption != '') {
                var value1 = ($.grep($scope.listunitsData, function (e) {
                    return e.Caption.toLowerCase() == $scope.Caption.toLowerCase();
                }));
                if (value1.length == 0) {
                    var addunits = {};
                    addunits.ID = $scope.Id;
                    addunits.Caption = $scope.Caption;
                    AdminService.InsertUpdateUnits(addunits).then(function (addunitsvalues) {
                        if (addunitsvalues.StatusCode == 405) {
                            NotifyError($translate.instant('LanguageContents.Res_4347.Caption'));
                        } else {
                            AdminService.GetUnits().then(function (getunits) {
                                $scope.listUnits = getunits.Response;
                                $scope.listunitsData = $scope.listUnits;
                            });
                            $('#unitsModal').modal('hide');
                            NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                        }
                    });
                } else {
                    bootbox.confirm($translate.instant('LanguageContents.Res_2018.Caption'), function (result) {
                        if (result) {
                            $timeout(function () {
                                var addunits = {};
                                addunits.ID = $scope.Id;
                                addunits.Caption = $scope.Caption;
                                AdminService.InsertUpdateUnits(addunits).then(function (res) {
                                    if (res.StatusCode == 405) {
                                        NotifyError($translate.instant('LanguageContents.Res_4347.Caption'));
                                    } else {
                                        AdminService.GetUnits().then(function (res) {
                                            $scope.listUnits = res.Response;
                                            $scope.listunitsData = $scope.listUnits;
                                        });
                                        $('#unitsModal').modal('hide');
                                        NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                                    }
                                });
                            }, 100);
                        }
                    });
                    return true;
                }
            }
        };
        $scope.DeleteUnitsByid = function DeleteUnitsByid(row) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2019.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        AdminService.DeleteUnitsByid(row.entity.Id).then(function (res) {
                            if (res.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4312.Caption'));
                            } else {
                                $scope.listunitsdata = res.Response;
                                NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                            }
                            AdminService.GetUnits().then(function (res) {
                                $scope.listUnits = res.Response;
                                $scope.listunitsData = $scope.listUnits;
                            });
                        });
                    }, 100);
                }
            });
        }
    }
    app.controller("mui.admin.unitsCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$compile', '$translate', 'AdminService', muiadminunitsCtrl]);
})(angular, app);