/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.ImportXml
{

    /// <summary>
    /// EntityTypeStatusOptions object for table 'MM_EntityTypeStatus_Options'.
    /// </summary>

    internal class EntityTypeStatusOptionsImport
    {
        #region Member Variables

        protected int _newid;
        protected int _id;
        protected int _entitytypeid;
        protected string _statusoptions;
        protected int _sortorder;
        protected bool _isremoved;
        protected string _colorcode;
        #endregion

        #region Constructors
        public EntityTypeStatusOptionsImport() { }

        public EntityTypeStatusOptionsImport(int pID, int pEntityTypeID, string pStatusOptions, int pSortOrder, bool pIsRemoved, int pNewId)
        {
            this._id = pID;
            this._entitytypeid = pEntityTypeID;
            this._statusoptions = pStatusOptions;
            this._sortorder = pSortOrder;
            this._isremoved = pIsRemoved;
            this._newid = pNewId;
        }

        public EntityTypeStatusOptionsImport(int pID)
        {
            this._id = pID;
        }

        #endregion

        #region Public Properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public int NewId
        {
            get { return _newid; }
            set { _newid = value; }

        }

        public int EntityTypeID
        {
            get { return _entitytypeid; }
            set
            {
                _entitytypeid = value;
            }

        }

        public string StatusOptions
        {
            get { return _statusoptions; }
            set
            {
                _statusoptions = value;
            }

        }
        public int SortOrder
        {
            get { return _sortorder; }
            set
            {
                _sortorder = value;
            }

        }
        public bool IsRemoved
        {
            get { return _isremoved; }
            set
            {
                _isremoved = value;
            }

        }
        public string ColorCode
        {
            get { return _colorcode; }
            set
            {
                _colorcode = value;
            }
        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityTypeStatusOptionsImport castObj = null;
            try
            {
                castObj = (EntityTypeStatusOptionsImport)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.ID);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
