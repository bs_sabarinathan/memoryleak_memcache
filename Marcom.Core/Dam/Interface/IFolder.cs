﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.DAM.Interface
{
    /// <summary>
    /// ITreeNode interface for table 'MM_TreeNode'.

    /// </summary>
    public interface IFolder
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int NodeID
        {
            get;
            set;

        }
        int ParentNodeID
        {
            get;
            set;

        }
        int Level
        {
            get;
            set;

        }

        string KEY
        {
            get;
            set;

        }

        int EntityID
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }

        int SortOrder
        {
            get;
            set;
        }


        string Colorcode
        {
            get;
            set;

        }

        #endregion
    }
}
