using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// UserMailSubscription object for table 'CM_UserMailSubscription'.
	/// </summary>
	
	internal class UserMailSubscription : IUserMailSubscription 
	{
		#region Member Variables

		protected int _id;
		protected int _userid;
		protected DateTimeOffset _lastsenton;
		protected DateTimeOffset _lastupdatedon;
		protected bool _isemailenable;
		protected string _dayname;
        protected TimeSpan _timing;
		protected bool _recapreport;
		
		
		#endregion
		
		#region Constructors
		public UserMailSubscription() {}

        public UserMailSubscription(int pUserid, DateTimeOffset pLastSentOn, DateTimeOffset pLastUpdatedOn, bool pIsEmailEnable, string pDayName, TimeSpan pTiming, bool pRecapReport)
		{
			this._userid = pUserid; 
			this._lastsenton = pLastSentOn; 
			this._lastupdatedon = pLastUpdatedOn; 
			this._isemailenable = pIsEmailEnable; 
			this._dayname = pDayName; 
			this._timing = pTiming; 
			this._recapreport = pRecapReport; 
		}
				
		public UserMailSubscription(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public int Userid
		{
			get { return _userid; }
			set { _userid = value; }
			
		}
		
		public DateTimeOffset LastSentOn
		{
			get { return _lastsenton; }
			set { _lastsenton = value; }
			
		}
		
		public DateTimeOffset LastUpdatedOn
		{
			get { return _lastupdatedon; }
			set { _lastupdatedon = value; }
			
		}
		
		public bool IsEmailEnable
		{
			get { return _isemailenable; }
			set { _isemailenable = value; }
			
		}
		
		public string DayName
		{
			get { return _dayname; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("DayName", "DayName value, cannot contain more than 50 characters");
			  _dayname = value; 
			}
			
		}

        public TimeSpan Timing
		{
			get { return _timing; }
			set { _timing = value; }
			
		}
		
		public bool RecapReport
		{
			get { return _recapreport; }
			set { _recapreport = value; }
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			UserMailSubscription castObj = null;
			try
			{
				castObj = (UserMailSubscription)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
