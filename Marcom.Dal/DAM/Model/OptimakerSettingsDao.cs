﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
    public partial class OptimakerSettingsDao : BaseDao, ICloneable
    {

        #region "Member variables"

        protected int _id;
        protected string _name;
        protected int _categoryid;
        protected string _description;
        protected int _docid;
        protected int _deptid;
        protected int _doctype;
        protected int _docversionid;
        protected string _previewimage;
        protected bool _autoapproval;
        protected int _templateEngineType;
        protected int _assetType;
        protected string _serverUrl;
        protected bool _enableHiResPdf;

        #endregion

        #region "Constructor"

        public OptimakerSettingsDao() { }


        public OptimakerSettingsDao(int pid, string pname, int pcategoryid, string pdescription, int pdocid, int pdepid, int pdoctype, int pdocversionid, string ppreviewimage, bool pautoapproval, int pTemplateEngineType, int pAssetType, string pServerURL, bool pEnableHighResPDF)
        {
            this._id = pid;
            this._name = pname;
            this._categoryid = pcategoryid;
            this._description = pdescription;
            this._docid = pdocid;
            this._deptid = pdepid;
            this._doctype = pdoctype;
            this._docversionid = pdocversionid;
            this._previewimage = ppreviewimage;
            this._autoapproval = pautoapproval;
            this._templateEngineType = pTemplateEngineType;
            this._assetType = pAssetType;
            this._serverUrl = pServerURL;
            this._enableHiResPdf = pEnableHighResPDF;
        }

        #endregion


        #region "Public propertiers"

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }



        public virtual string Name
        {
            get { return _name; }
            set
            {
                _name = value;
            }

        }



        public virtual int CategoryID
        {
            get { return _categoryid; }
            set { _categoryid = value; }

        }


        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }

        }

        public virtual int DocID
        {
            get { return _docid; }
            set { _docid = value; }

        }


        public virtual int DepartmentID
        {
            get { return _deptid; }
            set { _deptid = value; }

        }

        public virtual int DocType
        {
            get { return _doctype; }
            set { _doctype = value; }

        }


        public virtual int DocVersionID
        {
            get { return _docversionid; }
            set { _docversionid = value; }

        }

        public virtual string PreviewImage
        {
            get { return _previewimage; }
            set { _previewimage = value; }

        }

        public virtual bool AutoApproval
        {
            get { return _autoapproval; }
            set { _autoapproval = value; }

        }

        public virtual int TemplateEngineType
        {
            get { return _templateEngineType; }
            set { _templateEngineType = value; }

        }

        public virtual int AssetType
        {
            get { return _assetType; }
            set { _assetType = value; }

        }

        public virtual string ServerURL
        {
            get { return _serverUrl; }
            set { _serverUrl = value; }
        }

        public virtual bool EnableHighResPDF
        {
            get { return _enableHiResPdf; }
            set { _enableHiResPdf = value; }
        }
        #endregion


        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string OptimakerSettingsDao = "OptimakerSettingsDao";
            public const string ID = "ID";
            public const string CategoryID = "CategoryID";
            public const string Description = "Description";
            public const string Name = "Name";
            public const string DocID = "DocID";
            public const string DepartmentID = "DepartmentID";
            public const string DocType = "DocType";
            public const string DocVersionID = "DocVersionID";
            public const string PreviewImage = "PreviewImage";
            public const string AutoApproval = "AutoApproval";
            public const string TemplateEngineType = "TemplateEngineType";
            public const string AssetType = "AssetType";
            public const string ServerURL = "ServerURL";
            public const string EnableHighResPDF = "EnableHighResPDF";

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string OptimakerSettingsDao = "OptimakerSettingsDao";
            public const string ID = "ID";
            public const string CategoryID = "CategoryID";
            public const string Description = "Description";
            public const string Name = "Name";
            public const string DocID = "DocID";
            public const string DepartmentID = "DepartmentID";
            public const string DocType = "DocType";
            public const string DocVersionID = "DocVersionID";
            public const string PreviewImage = "PreviewImage";
            public const string AutoApproval = "AutoApproval";
            public const string TemplateEngineType = "TemplateEngineType";
            public const string AssetType = "AssetType";
            public const string ServerURL = "ServerURL";
            public const string EnableHighResPDF = "EnableHighResPDF";
        }

        #endregion
    }
}
