/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

	internal class RoleFeature : IRoleFeature 
	{
		#region Member Variables

		protected int _id;
		protected string _caption;
		protected string _description;
        protected int _moduleid;
		protected bool _ischecked;
        protected int _globalaclid;
		#endregion
		
		#region Constructors
		public RoleFeature() {}

        public RoleFeature(int pId,string pCaption, string pDescription, int pModuleID, bool pIsChecked,int pGlobalAclID)
		{
            this._id = pId;
			this._caption = pCaption; 
			this._description = pDescription;
            this._moduleid = pModuleID;
            this._ischecked = pIsChecked;
            this._globalaclid = pGlobalAclID;
		}

        public RoleFeature(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
			  _caption = value; 
			}
			
		}
		
		public string Description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
			  _description = value; 
			}
			
		}

        public virtual int ModuleID
        {
            get { return _moduleid; }
            set {  _moduleid = value; }

        }

        public virtual bool IsChecked
        {
            get { return _ischecked; }
            set { _ischecked = value; }

        }
        public virtual int GlobalAclID
        { get { return _globalaclid; }
            set { _globalaclid = value; }
        }
		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			Feature castObj = null;
			try
			{
				castObj = (Feature)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
