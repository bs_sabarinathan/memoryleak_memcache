﻿(function (ng, app) {
    "use strict";

    function muiadminmoduleCtrl($scope, $location, $resource, $timeout, $translate, AdminService) {
        var Version = 1;
        $scope.myData = [];
        $scope.listdata = [];
        AdminService.GetModule().then(function (res) {
            $scope.listdata = res.Response;
            $scope.myData = $scope.listdata;
        });
        $scope.features = [];
        $scope.featuresData = [];
        $scope.GetEntityTypeByID = function GetEntityTypeByID(row) {
            $("#entitytypeattributerelationModal").modal('show');
            AdminService.GetAllModuleFeatures(row.Id).then(function (res) {
                $scope.features = res.Response;
                $scope.featuresData = $scope.features;
            });
        };
        $scope.updateenable = function (row) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2007.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var updatemodule = {};
                        updatemodule.ID = row.Id;
                        updatemodule.Caption = row.Caption;
                        updatemodule.Description = row.Description;
                        var IsEnable;
                        if (!row.IsEnable == false) {
                            IsEnable = 0;
                        } else {
                            IsEnable = 1;
                        }
                        updatemodule.IsEnable = IsEnable;
                        AdminService.Module(updatemodule).then(function (updatemodulesettings) {
                            if (updatemodulesettings.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4363.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                                AdminService.GetModule().then(function (moduledata) {
                                    $scope.listdata = moduledata.Response;
                                    $scope.myData = $scope.listdata;
                                });
                            }
                        });
                    }, 100);
                }
            });
            AdminService.GetModule().then(function (res) {
                $scope.listdata = res.Response;
                $scope.myData = $scope.listdata;
            });
        };
        $scope.updateenablefeature = function (row) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2008.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var updatefeature = {};
                        updatefeature.ID = row.Id;
                        updatefeature.Caption = row.Caption;
                        updatefeature.Description = row.Description;
                        updatefeature.moduleID = row.ModuleID;
                        var IsEnable;
                        if (!row.IsEnable == false) {
                            IsEnable = 0;
                        } else {
                            IsEnable = 1;
                        }
                        updatefeature.IsEnable = IsEnable;
                        AdminService.UpdateFeature(updatefeature).then(function (updatemodulesettings) {
                            if (updatemodulesettings.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4363.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                                AdminService.GetAllModuleFeatures(row.ModuleID).then(function (modulefeatures) {
                                    $scope.features = modulefeatures.Response;
                                    $scope.featuresData = $scope.features;
                                });
                            }
                        });
                    }, 100);
                }
            });
            AdminService.GetAllModuleFeatures(row.ModuleID).then(function (res) {
                $scope.features = res.Response;
                $scope.featuresData = $scope.features;
            });
        };
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.moduleCtrl']"));
        });
    }
    app.controller("mui.admin.moduleCtrl", ['$scope', '$location', '$resource', '$timeout', '$translate', 'AdminService', muiadminmoduleCtrl]);
})(angular, app);