﻿(function (ng, app) {

    // "use strict";
           function muiplanningtoolcostcentredetailsectionfinancialCtrl($scope, $timeout, $http, requestContext, _, $compile, $routeParams, $cookies, $resource,PlanningService) {


		    // --- Define Controller Methods. ------------------- //


		    // --- Define Scope Methods. ------------------------ //

		    // --- Define Controller Variables. ----------------- //

		    // Get the render context local to this controller (and relevant params).
		    var renderContext = requestContext.getRenderContext("mui.planningtool.costcentre.detail.section.financial");


		    // --- Define Scope Variables. ---------------------- //


		    // The subview indicates which view is going to be rendered on the page.
		    $scope.subview = renderContext.getNextSection();




		    // --- Bind To Scope Events. ------------------------ //


		    // I handle changes to the request context.
		    $scope.$on(
                "requestContextChanged",
                function () {

                    // Make sure this change is relevant to this controller.
                    if (!renderContext.isChangeRelevant()) {

                        return;

                    }

                    // Update the view that is being rendered.
                    $scope.subview = renderContext.getNextSection();

                }
            );

		    $scope.$on("$destroy", function () {
		        RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.detail.section.financialCtrl']"));
		    });
		    // --- Initialize. ---------------------------------- //

		    //----------------Costcentre financial holder data binding start--------//
		    $scope.costcentreFinacialHolderResult = {};
		    $scope.financialchartpoints = [];
		    PlanningService.GettingCostcentreFinancialOverview(parseInt($routeParams.ID, 10)).then(function(GetFinancialSummaryBlockResult){
		    //var GetFinancialSummaryBlock = $resource('planning/GettingCostcentreFinancialOverview/:EntityId/', { EntityId: parseInt($routeParams.ID, 10) }, { get: { method: 'GET' } });
		  //  var GetFinancialSummaryBlockResult = GetFinancialSummaryBlock.get(function () {
		        $scope.costcentreFinacialHolderResult = GetFinancialSummaryBlockResult.Response;
		        var costcentreFinancialHtml = '';
		        costcentreFinancialHtml = costcentreFinancialHtml + '<table class="table table-normal-both" id="Table1">';
		        costcentreFinancialHtml = costcentreFinancialHtml + '<thead><tr>';
		        costcentreFinancialHtml = costcentreFinancialHtml + '<th>Total assigned</th>';
		        costcentreFinancialHtml = costcentreFinancialHtml + '<th>Planned amount</th>';
		        costcentreFinancialHtml = costcentreFinancialHtml + '<th>In Requests</th>';
		        costcentreFinancialHtml = costcentreFinancialHtml + '<th>Total Approved Budget</th>';
		        costcentreFinancialHtml = costcentreFinancialHtml + '<th>Total Budget Deviation</th> </tr> </thead>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <tbody><tr>';
		        costcentreFinancialHtml = costcentreFinancialHtml + '<td id="Totalassignedamount" ng-model="ngToatalAssignedAmount">';
		        costcentreFinancialHtml = costcentreFinancialHtml + '<a href=\"javascript:;\" id=\"TotalAssignedAmount\" data-ng-model=\"TotalAssignedAmount"\ data-type=\"text\" data-original-title=\"TotalAssignedAmount\">';
		        costcentreFinancialHtml = costcentreFinancialHtml + $scope.costcentreFinacialHolderResult.TotalAssignedAmount;
		        costcentreFinancialHtml = costcentreFinancialHtml + '</a>';
		        costcentreFinancialHtml = costcentreFinancialHtml + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntityPlannedAmount">' + $scope.costcentreFinacialHolderResult.PlannedAmount + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntityRequestedAmount">' + $scope.costcentreFinacialHolderResult.InRequests + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntityTotalApprovedBudget">' + $scope.costcentreFinacialHolderResult.ApprovedBudget + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntityTotalBudgetDeviation">' + $scope.costcentreFinacialHolderResult.BudgetDeviation + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' </tr></tbody>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <thead><tr >';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <th>Approved allocations</th>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <th>Unallocated</th>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <th>Committed</th>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <th>Spent</th>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <th>Available to spend</th> </tr></thead>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <tbody><tr>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntityApprovedAllocatedAmount">' + $scope.costcentreFinacialHolderResult.ApprovedAllocation + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntityUnAllocatedAmount">' + $scope.costcentreFinacialHolderResult.UnAllocatedAmount + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntityCommitted">' + $scope.costcentreFinacialHolderResult.Committed + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntitySpent">' + $scope.costcentreFinacialHolderResult.Spent + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntityAvailabletoSpent">' + $scope.costcentreFinacialHolderResult.AvailabletoSpent + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' </tr></tbody>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' </table>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <div class="finStatusFooterInfo">';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <div>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <span class="leftLabel">Latest Update:</span><span id="lastupdateddate">'
		        costcentreFinancialHtml = costcentreFinancialHtml + $scope.costcentreFinacialHolderResult.LastUpdatedon + '</span>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' </div>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <div>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' <span class="leftLabel">Funding Method:</span><span>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' Value Manually Entered';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' </span>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' </div>';
		        costcentreFinancialHtml = costcentreFinancialHtml + ' </div>';
		        $('#CostcentreFinancialHolderdiv').html(costcentreFinancialHtml);
		        $('#TotalAssignedAmount').editable({
		            type: 'text',
		            pk: 1,
		            placement: 'right',
		            url: '/post',
		            name: $scope.costcentreFinacialHolderResult.TotalAssignedAmount,
		            title: 'Update Amount'
		        });
		    });
		    $.mockjax({
		        url: '/post',
		        responseTime: 400,
		        response: function (settings) {
		            var updateCostcentreAssignedAmount = {};
		            //var updateCostcentreAssignedAmount = $resource('planning/UpdateTotalAssignedAmount/:EntityID/:TotalAssignedAmount', { EntityID: parseInt($routeParams.ID, 10), TotalAssignedAmount: parseInt(settings.data.value, 10) }, { update: { method: 'PUT' } });
		            //var updateCostcentreAssignedAmountData = new updateCostcentreAssignedAmount();
		            updateCostcentreAssignedAmount.EntityID = parseInt($routeParams.ID, 10);
		            updateCostcentreAssignedAmount.TotalAssignedAmount = parseInt(settings.data.value, 10);
		            PlanningService.UpdateTotalAssignedAmount(updateCostcentreAssignedAmount).then(function (updateCostcentreAssignedAmountResult) {
		           // var updateCostcentreAssignedAmountResult = updateCostcentreAssignedAmount.update(updateCostcentreAssignedAmountData, function () {
		                if (updateCostcentreAssignedAmountResult.Response == true) {
		                   
                        PlanningService.GettingCostcentreFinancialOverview(parseInt($routeParams.ID, 10)).then(function (GetFinancialSummaryBlockResult) {
		                   // var GetFinancialSummaryBlock = $resource('planning/GettingCostcentreFinancialOverview/:EntityId/', { EntityId: parseInt($routeParams.ID, 10) }, { get: { method: 'GET' } });
		                   // var GetFinancialSummaryBlockResult = GetFinancialSummaryBlock.get(function () {
		                        $scope.costcentreFinacialHolderResult = GetFinancialSummaryBlockResult.Response;
		                        var costcentreFinancialHtml = '';
		                        costcentreFinancialHtml = costcentreFinancialHtml + '<table class="table table-normal-both" id="Table1">';
		                        costcentreFinancialHtml = costcentreFinancialHtml + '<thead><tr>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + '<th>Total assigned</th>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + '<th>Planned amount</th>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + '<th>In Requests</th>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + '<th>Total Approved Budget</th>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + '<th>Total Budget Deviation</th> </tr> </thead>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <tbody><tr>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + '<td id="Totalassignedamount" ng-model="ngToatalAssignedAmount">';
		                        costcentreFinancialHtml = costcentreFinancialHtml + '<a href=\"javascript:;\" id=\"TotalAssignedAmount\" data-ng-model=\"TotalAssignedAmount"\ data-type=\"text\" data-original-title=\"TotalAssignedAmount\">';
		                        costcentreFinancialHtml = costcentreFinancialHtml + $scope.costcentreFinacialHolderResult.TotalAssignedAmount;
		                        costcentreFinancialHtml = costcentreFinancialHtml + '</a>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntityPlannedAmount">' + $scope.costcentreFinacialHolderResult.PlannedAmount + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntityRequestedAmount">' + $scope.costcentreFinacialHolderResult.InRequests + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntityTotalApprovedBudget">' + $scope.costcentreFinacialHolderResult.ApprovedBudget + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntityTotalBudgetDeviation">' + $scope.costcentreFinacialHolderResult.BudgetDeviation + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' </tr></tbody>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <thead><tr >';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <th>Approved allocations</th>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <th>Unallocated</th>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <th>Committed</th>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <th>Spent</th>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <th>Available to spend</th> </tr></thead>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <tbody><tr>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntityApprovedAllocatedAmount">' + $scope.costcentreFinacialHolderResult.ApprovedAllocation + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntityUnAllocatedAmount">' + $scope.costcentreFinacialHolderResult.UnAllocatedAmount + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntityCommitted">' + $scope.costcentreFinacialHolderResult.Committed + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntitySpent">' + $scope.costcentreFinacialHolderResult.Spent + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <td id="EntityAvailabletoSpent">' + $scope.costcentreFinacialHolderResult.AvailabletoSpent + $scope.DefaultSettings.CurrencyFormat + ' </td>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' </tr></tbody>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' </table>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <div class="finStatusFooterInfo">';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <div>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <span class="leftLabel">Latest Update:</span><span id="lastupdateddate">'
		                        costcentreFinancialHtml = costcentreFinancialHtml + $scope.costcentreFinacialHolderResult.LastUpdatedon + '</span>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' </div>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <div>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' <span class="leftLabel">Funding Method:</span><span>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' Value Manually Entered';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' </span>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' </div>';
		                        costcentreFinancialHtml = costcentreFinancialHtml + ' </div>';
		                        $('#CostcentreFinancialHolderdiv').html(costcentreFinancialHtml);
		                    });
		                    NotifySuccess($translate.instant('LanguageContents.Res_4805.Caption'));
		                }
		                else
		                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
		            });
		        }
		    });
		    //----------------Costcentre financial holder data binding End--------//


		}

           app.controller("mui.planningtool.costcentre.detail.section.financialCtrl", ['$scope', '$timeout', '$http', 'requestContext', '_', '$compile', '$routeParams', '$cookies', '$resource', 'PlanningService', muiplanningtoolcostcentredetailsectionfinancialCtrl]);
})(angular, app);