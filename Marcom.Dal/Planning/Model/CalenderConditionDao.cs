using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// CalenderConditionDao object for table 'PM_CalenderCondition'.
	/// </summary>
	
	public partial class CalenderConditionDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected int _calenderid;
		protected int? _entitytypeid;
		protected int? _attributeid;
		protected int _conditiontype;
		protected int _sortorder;
        protected int _attributelevel;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public CalenderConditionDao() {}

        public CalenderConditionDao(int pId, int pCalenderid, int? pEntityTypeid, int? pAttributeid, int pConditionType, int pSortOrder, int pAttributeLevel)
		{
			this._id = pId;
            this._calenderid = pCalenderid; 
			this._entitytypeid = pEntityTypeid; 
			this._attributeid = pAttributeid; 
			this._conditiontype = pConditionType; 
			this._sortorder = pSortOrder;
            this._attributelevel = pAttributeLevel;
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual int Calenderid
		{
			get { return _calenderid; }
            set { _bIsChanged |= (_calenderid != value); _calenderid = value; }
			
		}
		
		public virtual int? EntityTypeid
		{
			get { return _entitytypeid; }
			set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }
			
		}
		
		public virtual int? Attributeid
		{
			get { return _attributeid; }
			set { _bIsChanged |= (_attributeid != value); _attributeid = value; }
			
		}
		
		public virtual int ConditionType
		{
			get { return _conditiontype; }
			set { _bIsChanged |= (_conditiontype != value); _conditiontype = value; }
			
		}
		
		public virtual int SortOrder
		{
			get { return _sortorder; }
			set 
			{
			  _bIsChanged |= (_sortorder != value); 
			  _sortorder = value; 
			}
			
		}

        public virtual int AttributeLevel
        {
            get { return _attributelevel; }
            set { _attributelevel = value; }
        }

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string CalenderConditionDao = "CalenderConditionDao";			
			public const string Id = "Id";
            public const string Calenderid = "Calenderid";			
			public const string EntityTypeid = "EntityTypeid";			
			public const string Attributeid = "Attributeid";			
			public const string ConditionType = "ConditionType";			
			public const string SortOrder = "SortOrder";
            public const string AttributeLevel = "AttributeLevel";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string CalenderConditionDao = "CalenderConditionDao";			
			public const string Id = "Id";
            public const string Calenderid = "Calenderid";			
			public const string EntityTypeid = "EntityTypeid";			
			public const string Attributeid = "Attributeid";			
			public const string ConditionType = "ConditionType";			
			public const string SortOrder = "SortOrder";
            public const string AttributeLevel = "AttributeLevel";
		}

		#endregion
		
		
	}
	
}
