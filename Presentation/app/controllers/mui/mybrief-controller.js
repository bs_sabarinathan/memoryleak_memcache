﻿(function (ng, app) {

    "use strict";

    //app.controller(
    //	"mui.mytaskCtrl",
    function muimybriefCtrl($scope, $cookies, $http, $compile, $window, $timeout, $location) {

        $('#myBriefContainer').css('min-height', window.innerHeight - 103);

        //---------------------------  Get the initial feed ------------------------------------

        // --- Define Scope Variables. ---------------------- //


        // --- Bind To Scope Events. ------------------------ //


        // I handle changes to the request context.

        // --- Initialize. ---------------------------------- //

        //Forming Complex data

        // From MDN
        //$(window).bind('beforeunload', function () {

        //    //save info somewhere


        //    return 'are you sure you want to leave?';

        //});



        //$(window).unload(function () {
        //    $route.reload();
        //});


        function setIFrameSrc(iFrameID, userid, usermail, username) {

            $http.get('Appsettings/CommonSetup.json').then(function (resp) {

                var myFrame = $('#' + iFrameID);
                var jsonobj = { "UserID": userid, "UserEmail": usermail, "UserName": username };
                var jsonqrystring = JSON.stringify(jsonobj);
                var url = resp.data.BriefSettings.BriefHostedUrl + '?qry=' + jsonqrystring;
                $(myFrame).attr('src', url);

                $('iframe#externalBriefMiddleware').load(function () {
                    $('#divLoading').css('display', 'none');
                    $('#divFrameHolder').css('display', 'block');
                    console.log("hai loaded");
                });
            });

        }

        $timeout(function () {
            setIFrameSrc("externalBriefMiddleware", $cookies['UserId'], $cookies['UserEmail'], $cookies['Username']);
        }, 10);




        //mytask paging ENDS HERE

        //-------------- user defined functions ---------

        // Get the render context local to this controller (and relevant params).
        var renderContext = requestContext.getRenderContext("mui.mybrief");

        $scope.subview = renderContext.getNextSection();     

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.mytaskCtrl']"));
        });

        //For Setting Color Code - By Madhur 22 Dec 2014
        $scope.set_color = function (clr) {
            if (clr != null)
                return { 'background-color': "#" + clr.toString().trim() };
            else
                return '';
        }
    }
    //);
    app.controller("mui.muimybriefCtrl", ['$scope', '$cookies', '$compile', '$window', '$timeout', '$location', muimybriefCtrl]);

})(angular, app);