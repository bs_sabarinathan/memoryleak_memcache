﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Microsoft.SqlServer.Management.Smo;
using System.Data.SqlClient;
//using Microsoft.SqlServer.Management.Common;
using System.Collections.Specialized;
using System.Configuration;
using NHibernate.Tool.hbm2ddl;
using BrandSystems.Marcom.Core.Managers.Proxy;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Utility
{
    public class DynamicCollection
    {
        private static DynamicCollection _instance = new DynamicCollection();

        public static DynamicCollection Instance
        {
            get { return _instance; }
        }

        #region Member Variables

        protected List<int> _typeids;
        protected List<int> _attributeids;
        protected List<int> _orderbyids;
        protected List<int> _whereids;

        #endregion

        #region Constructors

        public DynamicCollection()
        {
        }

        public DynamicCollection(List<int> TypeIDs, List<int> AttributeIDs, List<int> OrderByIDs, List<int> WhereIDs)
        {
            this._typeids = TypeIDs;
            this._attributeids = AttributeIDs;
            this._orderbyids = OrderByIDs;
            this._whereids = WhereIDs;
        }

        #endregion

        public string GenerateSqlScript(IEntityTypeAttributeRelation ientitytypeattributeRelation)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("SELECT ");

            _attributeids.ForEach(delegate(int attributeid)
            {                
                sb.Append("Attr_" + attributeid + "").Append(", ");
            });

            if (_attributeids.Count > 0)
            {
                var index = sb.ToString().LastIndexOf(", ");
                if (index >= 0)
                {
                    sb = sb.Remove(index, 1);
                }
            }

            sb.Append("FROM (");

            _typeids.ForEach(delegate(int typeid)
            {
                sb.Append("SELECT ");

                _attributeids.ForEach(delegate(int attributeid)
                {
                    sb.Append("Attr_" + attributeid + "").Append(", ");
                });

                if (_attributeids.Count > 0)
                {
                    var attrindex = sb.ToString().LastIndexOf(", ");
                    if (attrindex >= 0)
                    {
                        sb = sb.Remove(attrindex, 1);
                    }
                }

                sb.Append("FROM ").Append("MM_AttributeRecord_" + typeid + " ").Append("UNION ALL ");

            });

            if (_typeids.Count > 0)
            {
                var typeindex = sb.ToString().LastIndexOf("UNION ALL ");
                if (typeindex >= 0)
                {
                    sb = sb.Remove(typeindex, 10);
                }
            }

            sb.Append(") AS tbl ");

            sb.Append("ORDER BY ");

            _orderbyids.ForEach(delegate(int orderybyid)
            {
                sb.Append("Attr_" + orderybyid + "").Append(", ");
            });

            if (_orderbyids.Count > 0)
            {
                var orderybyindex = sb.ToString().LastIndexOf(", ");
                if (orderybyindex >= 0)
                {
                    sb = sb.Remove(orderybyindex, 1);
                }
            }

            _whereids.ForEach(delegate(int whereid)
            {
                sb.Append("Attr_" + whereid + "").Append(", ");
            });

            if (_whereids.Count > 0)
            {
                var whereindex = sb.ToString().LastIndexOf(", ");
                if (whereindex >= 0)
                {
                    sb = sb.Remove(whereindex, 1);
                }
            }

            return sb.ToString();
        }
    }
}
