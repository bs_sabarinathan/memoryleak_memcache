﻿using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Configuration;
using System.IO;
using Presentation.Utility;
using System.Text;
using System.Configuration;
using System.Net.Http.Headers;
using BrandSystems.Marcom.Core.Utility;

namespace Presentation.Controllers
{
    public class CommonController : ApiController
    {
        ServiceResponse result;

        [Route("api/Common/GetAllSubscriptionType")]
        [HttpGet]
        public ServiceResponse GetAllSubscriptionType()
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetAllSubscriptionType();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        //d
        [Route("api/Common/GetUserSubscriptionSettings/{SubscribtionTypeID}")]
        [HttpGet]
        public ServiceResponse GetUserSubscriptionSettings(string SubscribtionTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetUserSubscriptionSettings(SubscribtionTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/UpdateUserSubscriptionSettings/{ID}")]
        [HttpPut]
        public ServiceResponse UpdateUserSubscriptionSettings(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetNotification(0);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        //d

        [Route("api/Common/GetNotification/{flag}")]
        [HttpGet]
        public ServiceResponse GetNotification(int flag)
        {
            result = new ServiceResponse();
            try
            {

                //string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
                //string retFilePath = baseDir + "//" + "ServiceErrorLog.txt";

                //BrandSystems.Marcom.Core.Utility.ErrorLog.LogFilePath = retFilePath;
                //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "<-----------------------Get notification REACHED HERE -------------->", DateTime.Now);

                //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "<-----------------------Keys are STARTS HERE-------------->", DateTime.Now);
                //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, MarcomManagerFactory.PrintMarcomManager(), DateTime.Now);
                //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, MarcomManagerFactory.PrintMarcomUserManager(), DateTime.Now);
                //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "<-----------------------Keys ENDS STARTS HERE-------------->", DateTime.Now);

                //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "<-----------------------Browser sessioncookie is--------------> " + ((string[])(Request.Headers.GetValues("sessioncookie")))[0], DateTime.Now);



                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetNotification(flag);

            }
            catch (Exception ex)
            {

                //string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
                //string retFilePath = baseDir + "//" + "ServiceErrorLog.txt";

                //BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("CommonManager/GetNotification method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;


                //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "<-----------------------CommonManager/GetNotification exception Keys are STARTS HERE-------------->", DateTime.Now);
                //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, MarcomManagerFactory.PrintMarcomManager(), DateTime.Now);
                //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, MarcomManagerFactory.PrintMarcomUserManager(), DateTime.Now);
                //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "<-----------------------CommonManager/GetNotification exception Keys ENDS STARTS HERE-------------->", DateTime.Now);


                //BrandSystems.Marcom.Core.Utility.ErrorLog.LogFilePath = retFilePath;
                //BrandSystems.Marcom.Core.Utility.ErrorLog.ErrorRoutine(false, ex);

            }
            return result;
        }


        //d
        [Route("api/Common/GetNotificationForMypageSettings/{flag}/{pageNo}")]
        [HttpGet]
        public ServiceResponse GetNotificationForMypageSettings(int flag, int pageNo)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetNotification(flag, pageNo);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        //d

        [Route("api/Common/UpdateIsviewedStatusNotification")]
        [HttpPost]
        public ServiceResponse UpdateIsviewedStatusNotification([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdateIsviewedStatusNotification((int)jobj["UserID"], (string)jobj["FirstFiveNotifications"], (int)jobj["Flag"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }


        [Route("api/Common/UpdatetopIsviewedStatusNotification")]
        [HttpPost]
        public ServiceResponse UpdatetopIsviewedStatusNotification()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdatetopIsviewedStatusNotification();

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        //doubt
        [Route("api/Common/InsertNavigation")]
        [HttpPost]
        public ServiceResponse InsertNavigation([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //result.Response = marcomManager.CommonManager.Navigation_Insert((int)jobj["ID"], (int)jobj["TypeID"], (int)jobj["ParentID"], (int)jobj["ModuleID"], (int)jobj["FeatureID"], (string)jobj["Caption"], (string)jobj["Description"], (string)jobj["Url"], (string)jobj["JavaScript"], ((int)jobj["IsActive"] == 0 ? false : true), ((int)jobj["IsPopup"] == 0 ? false : true), ((int)jobj["IsIframe"] == 0 ? false : true), ((int)jobj["IsDynamicPage"] == 0 ? false : true), ((int)jobj["IsExternal"] == 0 ? false : true), ((int)jobj["AddUserName"] == 0 ? false : true), ((int)jobj["AddUserEmail"] == 0 ? false : true), ((int)jobj["IsDefault"] == 0 ? false : true), (string)jobj["ExternalUrl"], (string)jobj["Imageurl"], (int)jobj["GlobalRoleid"], ((int)jobj["AddUserID"] == 0 ? false : true), ((int)jobj["AddLanguageCode"] == 0 ? false : true));
                result.Response = marcomManager.CommonManager.Navigation_Insert((int)jobj["ID"], (int)jobj["TypeID"], (int)jobj["ParentID"], (int)jobj["ModuleID"], (int)jobj["FeatureID"], (string)jobj["Caption"], (string)jobj["Description"], (string)jobj["Url"], (string)jobj["JavaScript"], ((int)jobj["IsActive"] == 0 ? false : true), ((int)jobj["IsPopup"] == 0 ? false : true), ((int)jobj["IsIframe"] == 0 ? false : true), ((int)jobj["IsDynamicPage"] == 0 ? false : true), ((int)jobj["IsExternal"] == 0 ? false : true), ((int)jobj["AddUserName"] == 0 ? false : true), ((int)jobj["AddUserEmail"] == 0 ? false : true), ((int)jobj["IsDefault"] == 0 ? false : true), (string)jobj["ExternalUrl"], (string)jobj["Imageurl"], (int)jobj["GlobalRoleid"], ((int)jobj["AddUserID"] == 0 ? false : true), ((int)jobj["AddLanguageCode"] == 0 ? false : true), (int)jobj["SearchType"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        //d
        [Route("api/Common/UpdateUserSingleEntitySubscription")]
        [HttpPost]
        public ServiceResponse UpdateUserSingleEntitySubscription([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdateUserSingleEntitySubscription((int)jobj["EntityID"], (int)jobj["ID"], (string)jobj["Caption"], (bool)jobj["IsAutomated"], (int)jobj["UserID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/UpdateNavigationByID")]
        [HttpPut]
        public ServiceResponse UpdateNavigationByID([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //result.Response = marcomManager.CommonManager.Navigation_Update((int)jobj["ID"], (int)jobj["TypeID"], (int)jobj["ParentId"], (int)jobj["ModuleID"], (int)jobj["FeatureID"], (string)jobj["Caption"], (string)jobj["Description"], (string)jobj["Url"], (string)jobj["ExternalUrl"], ((int)jobj["IsExternal"] == 0 ? false : true), ((int)jobj["IsDefault"] == 0 ? false : true), ((int)jobj["IsEnable"] == 0 ? false : true), ((int)jobj["AddUserID"] == 0 ? false : true), ((int)jobj["AddLanguageCode"] == 0 ? false : true), ((int)jobj["AddUserEmail"] == 0 ? false : true), ((int)jobj["AddUserName"] == 0 ? false : true), (int)jobj["SortOrder"]);
                result.Response = marcomManager.CommonManager.Navigation_Update((int)jobj["ID"], (int)jobj["TypeID"], (int)jobj["ParentId"], (int)jobj["ModuleID"], (int)jobj["FeatureID"], (string)jobj["Caption"], (string)jobj["Description"], (string)jobj["Url"], (string)jobj["ExternalUrl"], ((int)jobj["IsExternal"] == 0 ? false : true), ((int)jobj["IsDefault"] == 0 ? false : true), ((int)jobj["IsEnable"] == 0 ? false : true), ((int)jobj["AddUserID"] == 0 ? false : true), ((int)jobj["AddLanguageCode"] == 0 ? false : true), ((int)jobj["AddUserEmail"] == 0 ? false : true), ((int)jobj["AddUserName"] == 0 ? false : true), (int)jobj["SortOrder"], (int)jobj["SelectedSearchType"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        //d
        [Route("api/Common/GetNavigationByIDandParentid/{IsParentID}/{UserID}/{Flag}")]
        [HttpGet]
        public ServiceResponse GetNavigationByIDandParentid(int IsParentID, int UserID, int Flag)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.Navigation_Select((IsParentID == 1) ? true : false, UserID, Flag);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        //doubt
        [Route("api/Common/UpdateNavigationSortOrder")]
        [HttpPost]
        public ServiceResponse UpdateNavigationSortOrder([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray SortOrderUpdate = (JArray)jobj["AttributeArray"];
                foreach (var item in SortOrderUpdate)
                {
                    result.Response = marcomManager.CommonManager.UpdateNavigationSortOrder((int)item["Id"], (int)item["SortOrder"]);
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        //d
        [Route("api/Common/DeleteNavigation/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteNavigation(int ID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.Navigation_Delete(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/GetGroupIDForNavigation/{NavigationID}/{UserID}")]
        [HttpGet]
        public ServiceResponse GetGroupIDForNavigation(int NavigationID, int UserID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetGroupIDForNavigation(NavigationID, UserID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetUserDefaultSubscription/{ID}/{Caption}/{IsAutomated}")]
        [HttpGet]
        public ServiceResponse GetUserDefaultSubscription(int ID, string Caption, bool IsAutomated)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetUserDefaultSubscription(ID, Caption, IsAutomated);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/InsertSubscriptionNotificationsettings")]
        [HttpPost]
        public ServiceResponse InsertSubscriptionNotificationsettings([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertSubscriptionNotificationsettings((int)jobj["UserID"], (DateTimeOffset)jobj["LastsentOn"], (DateTimeOffset)jobj["LastupdatedOn"], (TimeSpan)jobj["Timing"], (bool)jobj["IsEmailenable"], (string)jobj["DayName"], (bool)jobj["RecapReport"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/UpdateRecapNotificationsettings")]
        [HttpPut]
        public ServiceResponse UpdateRecapNotificationsettings([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdateRecapNotificationsettings((int)jobj["ID"], (int)jobj["UserID"], (DateTimeOffset)jobj["LastSentOn"], (DateTimeOffset)jobj["LastUpdatedOn"], (bool)jobj["IsEmailEnable"], (string)jobj["DayName"], (TimeSpan)jobj["Timing"], (bool)jobj["RecapReport"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetFeedByID/{EntitiyID}")]
        [HttpGet]
        public ServiceResponse GetFeedByID(int EntitiyID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetFeedByID(EntitiyID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/InsertFeedComment")]
        [HttpPost]
        public ServiceResponse InsertFeedComment([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertFeedComment((int)jobj["FeedID"], (int)jobj["Actor"], (string)jobj["Comment"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/UserMultiSubscription/{EntitiyID}/{UserID}")]
        [HttpGet]
        public ServiceResponse UserMultiSubscription(int EntitiyID, int UserID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UserMultiSubscriptionLoad(EntitiyID, UserID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/PostMultiSubscription")]
        [HttpPost]
        public ServiceResponse PostMultiSubscription([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                //JArray arrLevels = (JArray)jobj["LEVELS"];
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["levels"].ToString());
                //int[] Arr = null;
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.SaveUpdateMultiSubscription(Idarr, (int)jobj["EntitiyID"], (int)jobj["UserID"], (bool)jobj["IsMultiLevel"], (int)jobj["EntityTypeID"], (DateTimeOffset)jobj["SubscribedOn"], (DateTimeOffset)jobj["LastUpdatedOn"], (int)jobj["FilterRoleOption"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        //Method name renamed to PutMultiSubscription
        [Route("api/Common/PutMultiSubscription")]
        [HttpPut]
        public ServiceResponse PutMultiSubscription([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrLevels = (JArray)jobj["LEVELS"];
                int[] Arr = null;
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.SaveUpdateMultiSubscription(Arr, (int)jobj["EntitiyID"], (int)jobj["UserID"], (bool)jobj["IsMultiLevel"], (int)jobj["EntityTypeID"], (DateTimeOffset)jobj["SubscribedOn"], (DateTimeOffset)jobj["LastUpdatedOn"], (int)jobj["filteroption"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/GetUnsubscribeMultiSubscriptionById/{EntitiyID}/{UserID}")]
        [HttpDelete]
        public ServiceResponse GetUnsubscribeMultiSubscriptionById(int EntitiyID, int UserID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UnsubscribeMultiSubscription(EntitiyID, UserID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/CheckUserPermissionForEntity/{EntitiyID}")]
        [HttpGet]
        public ServiceResponse CheckUserPermissionForEntity(int EntitiyID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.CheckUserPermissionForEntity(EntitiyID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/PostFeedTemplate")]
        [HttpPost]
        public ServiceResponse PostFeedTemplate([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.SaveUpdateFeedTemplate((int)jobj["ModuleID"], (int)jobj["FeatureID"], (string)jobj["Template"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/FeedTemplate")]
        [HttpPut]
        public ServiceResponse FeedTemplate([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = (marcomManager.CommonManager.SaveUpdateFeedTemplate((int)jobj["ModuleID"], (int)jobj["FeatureID"], (string)jobj["Template"]) == 0 ? false : true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/PostFeed")]
        [HttpPost]
        public ServiceResponse PostFeed([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray arrlst = (JArray)jobj["PersonalUserIds"];
                int[] userids = arrlst.Select(jv => (int)jv).ToArray();
                result.Response = marcomManager.CommonManager.SaveUpdateFeed((int)jobj["Actor"], (int)jobj["TemplateID"], (int)jobj["EntityID"], (string)jobj["TypeName"], (string)jobj["AttributeName"], (string)jobj["FromValue"], (string)jobj["ToValue"], 0, 0, null, 0, 0, "", userids);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/Feed")]
        [HttpPut]
        public ServiceResponse Feed([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                int[] userids = new int[] { };
                if (jobj["PersonalUserIds"] != null)
                {
                    JArray arrlst = (JArray)jobj["PersonalUserIds"];
                    userids = arrlst.Select(jv => (int)jv).ToArray();
                }
                if (jobj["associatedentityid"] == null)
                {
                    jobj["associatedentityid"] = 0;
                }
                result.Response = (marcomManager.CommonManager.SaveUpdateFeed((int)jobj["Actor"], (int)jobj["TemplateID"], (int)jobj["EntityID"], (string)jobj["TypeName"], (string)jobj["AttributeName"], (string)jobj["FromValue"], (string)jobj["ToValue"], 0, (int)jobj["associatedentityid"], null, 0, 0, "", userids) == 0 ? false : true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/AdminSettingsforRootLevelInsertUpdate")]
        [HttpPost]
        public ServiceResponse AdminSettingsforRootLevelInsertUpdate([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JObject job = JObject.Parse(Convert.ToString(jobj));
                string jsondata = Convert.ToString(jobj["View"]);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.AdminSettingsforRootLevelInsertUpdate(jsondata, (string)jobj["Key"], (int)jobj["TypeId"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/AdminSettingsforReportInsertUpdate")]
        [HttpPost]
        public ServiceResponse AdminSettingsforReportInsertUpdate([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                string jsondata = Convert.ToString(jobj["View"]);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.AdminSettingsforReportInsertUpdate(jsondata, (string)jobj["Key"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/AdminSettingsforGanttViewInsertUpdate")]
        [HttpPost]
        public ServiceResponse AdminSettingsforGanttViewInsertUpdate([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                string jsondata = Convert.ToString(jobj);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.AdminSettingsforGanttViewInsertUpdate(jsondata, (string)jobj["Key"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/AdminSettingsforDetailFilterInsertUpdate")]
        [HttpPost]
        public ServiceResponse AdminSettingsforDetailFilterInsertUpdate([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                string jsondata = Convert.ToString(jobj);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.AdminSettingsforDetailFilterInsertUpdate(jsondata, (string)jobj["Key"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/AdminSettingsforListViewInsertUpdate")]
        [HttpPost]
        public ServiceResponse AdminSettingsforListViewInsertUpdate([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                string jsondata = Convert.ToString(jobj);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.AdminSettingsforListViewInsertUpdate(jsondata, (string)jobj["Key"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/AdminSettingsforRootLevelFilterSettingsInsertUpdate")]
        [HttpPost]
        public ServiceResponse AdminSettingsforRootLevelFilterSettingsInsertUpdate([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                string jsondata = Convert.ToString(jobj);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.AdminSettingsforRootLevelFilterSettingsInsertUpdate(jsondata, (string)jobj["Key"], (int)jobj["EntityTypeID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/AdminSettingsForRootLevelDelete/{Key}/{EntityTypeID}")]
        [HttpDelete]
        public ServiceResponse AdminSettingsForRootLevelDelete(string Key, int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.AdminSettingsForRootLevelDelete(Key, EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/AdminSettingsForRootLevelDeleteByAttributeID/{Key}/{EntityTypeID}/{AttribtueID}")]
        [HttpDelete]
        public ServiceResponse AdminSettingsForRootLevelDeleteByAttributeID(string Key, int EntityTypeID, int AttribtueID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.AdminSettingsForRootLevelDelete(Key, EntityTypeID, AttribtueID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetNavigationConfig")]
        [HttpGet]
        public ServiceResponse GetNavigationConfig()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetNavigationConfig();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetMediabankNavigationConfig")]
        [HttpGet]
        public ServiceResponse GetMediabankNavigationConfig()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetMediabankNavigationConfig();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetNavigationExternalLinksByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetNavigationExternalLinksByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetNavigationExternalLinksByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetAdminSettingselemntnode/{LogoSettings}/{elemntnode}/{typeid}")]
        [HttpGet]
        public ServiceResponse GetAdminSettingselemntnode(string LogoSettings, string elemntnode, int typeid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = JObject.Parse(marcomManager.CommonManager.GetAdminSettingselemntnode(LogoSettings, elemntnode, typeid));
            }
            catch
            {
                return null;
            }
            return result;
        }
        [Route("api/Common/InsertFile")]
        [HttpPost]
        public ServiceResponse InsertFile([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //InsertFile(string Name, int VersionNo, string MimeType, string Extension, long Size, int OwnerID, DateTime CreatedOn, string Checksum, int ModuleID, int EntityID, Guid FileGuid);
                result.Response = marcomManager.CommonManager.InsertFile((string)jobj["Name"], (int)jobj["VersionNo"], (string)jobj["MimeType"], (string)jobj["Extension"], (long)jobj["Size"], (int)jobj["OwnerID"], (DateTime)jobj["CreatedOn"], (string)jobj["Checksum"], (int)jobj["ModuleID"], (int)jobj["EntityID"], (String)jobj["FileGuid"], (string)jobj["Description"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/InsertFileInPlanAttchTab")]
        [HttpPost]
        public ServiceResponse InsertFileInPlanAttchTab([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertFile((string)jobj["Name"], (int)jobj["VersionNo"], (string)jobj["MimeType"], (string)jobj["Extension"], (long)jobj["Size"], (int)jobj["OwnerID"], (DateTime)jobj["CreatedOn"], (string)jobj["Checksum"], (int)jobj["ModuleID"], (int)jobj["EntityID"], (String)jobj["FileGuid"], (string)jobj["Description"], true);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/InsertLink")]
        [HttpPost]
        public ServiceResponse InsertLink([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertLink((int)jobj["EntityID"], (string)jobj["Name"], (string)jobj["URL"], (string)jobj["Description"], (int)jobj["ActiveVersionNo"], (int)jobj["TypeID"], (string)jobj["CreatedOn"], (int)jobj["OwnerID"], (int)jobj["ModuleID"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/DeleteFileByID/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteFileByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.DeleteFileByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/DeleteLinkByID/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteLinkByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.DeleteLinkByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetFileByEntityID/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetFileByEntityID(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetFileByEntityID(EntityID);
            }
            catch
            {
                return null;
            }
            return result;
        }

        [Route("api/Common/GetFilesandLinksByEntityID/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetFilesandLinksByEntityID(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetFilesandLinksByEntityID(EntityID);
            }
            catch
            {
                return null;
            }
            return result;
        }

        [Route("api/Common/GetEnityFeeds/{EntitiyID}/{pageNo}/{Feedsgroupid}")]
        [HttpGet]
        public ServiceResponse GetEnityFeeds(string EntitiyID, int pageNo, string Feedsgroupid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                // string entityids= marcomManager.CommonManager.GetEntityIdsForFeed((int)common.EntitiyID);
                result.StatusCode = (int)HttpStatusCode.OK;
                if (Feedsgroupid == "-1")
                {
                    //comment temp=2
                    int UserID = marcomManager.User.Id;
                    bool isCorporate = false;
                    string FeedGroupTempIds_string = "";
                    int getcorporateroleid = marcomManager.CommonManager.GetCorporateRolebyEntityID(int.Parse(EntitiyID));
                    var UserGlobalRoleId = marcomManager.AccessManager.GetGlobalRoleUserByID(UserID);
                    var EntityRoleIDs = marcomManager.CommonManager.GetEntityRoleByEnitityID(int.Parse(EntitiyID), UserID);
                    if (getcorporateroleid != null || getcorporateroleid != 0)
                    {
                        for (var i = 0; i < EntityRoleIDs.Length; i++)
                        {
                            if (EntityRoleIDs[i] == getcorporateroleid)
                            {
                                FeedGroupTempIds_string = "" + (int)NewsfeedFilter.Comment;
                                isCorporate = true;
                            }
                        }
                    }
                    if (!isCorporate)
                    {
                        if (UserGlobalRoleId.Contains(1))
                        {
                            FeedGroupTempIds_string = "-1";
                        }
                        else
                        {
                            var FeedGroupTempIds = marcomManager.CommonManager.GettingFeedFilter(UserGlobalRoleId);
                            if (FeedGroupTempIds.Length == 0)
                            {
                                FeedGroupTempIds_string = Feedsgroupid;
                            }
                            else
                            {
                                for (var i = 0; i < FeedGroupTempIds.Length; i++)
                                {
                                    if (FeedGroupTempIds_string == "")
                                    {
                                        FeedGroupTempIds_string += "" + FeedGroupTempIds[i];
                                    }
                                    else
                                    {
                                        FeedGroupTempIds_string += "," + FeedGroupTempIds[i];
                                    }
                                }
                            }
                        }
                    }
                    result.Response = marcomManager.CommonManager.GetFeedsByEntityID(EntitiyID, pageNo, int.Parse(EntitiyID), FeedGroupTempIds_string);
                }
                else
                {
                    result.Response = marcomManager.CommonManager.GetFeedsByEntityID(EntitiyID, pageNo, int.Parse(EntitiyID), Feedsgroupid);
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetLastEntityFeeds/{EntitiyID}/{Feedsgroupid}")]
        [HttpGet]
        public ServiceResponse GetLastEntityFeeds(int EntitiyID, string Feedsgroupid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                //string entityids = marcomManager.CommonManager.GetEntityIdsForFeed((int)common.EntitiyID);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetLatestFeedsByEntityID((EntitiyID).ToString(), EntitiyID, Feedsgroupid);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetEnityFeedsForFundingReq/{EntitiyID}/{pageNo}/{islatestfeed}")]
        [HttpGet]
        public ServiceResponse GetEnityFeedsForFundingReq(int EntitiyID, int pageNo, bool islatestfeed)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GettingFeedsByEntityIDandFundingrequest(EntitiyID, pageNo, islatestfeed);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/InsertUserSingleEntitySubscription")]
        [HttpPost]
        public ServiceResponse InsertUserSingleEntitySubscription([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertUserSingleEntitySubscription((int)jobj["UserId"], (int)jobj["EntityId"], (int)jobj["EntitytypeId"], (DateTimeOffset)jobj["SubscribedOn"], (DateTimeOffset)jobj["LastUpdatedOn"], (string)jobj["issubscribe"], (int)jobj["FilterRoleOption"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetEntityTypeforSubscription/{ID}")]
        [HttpGet]
        public ServiceResponse GetEntityTypeforSubscription(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetEntityTypeforSubscription(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetAutoSubscriptionDetails/{UserID}/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetAutoSubscriptionDetails(int UserID, int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetAutoSubscriptionDetails(UserID, EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/GetWidgetTemplateByID/{TemplateID}")]
        [HttpGet]
        public ServiceResponse GetWidgetTemplateByID(int TemplateID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                //return marcomManager.CommonManager.Navigation_Select(((int)common.IsParentID == 1) ? true : false);
                //return marcomManager.CommonManager.WidgetTemplate_Select(TemplateID);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.WidgetTemplate_Select(TemplateID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetWidgetTypesByID/{UserId}/{ISAdmin}/{TypeID}")]
        [HttpGet]
        public ServiceResponse GetWidgetTypesByID(int UserId, bool ISAdmin, int TypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //return marcomManager.CommonManager.WidgetTypes_Select(UserId, ISAdmin, TypeID);
                result.Response = marcomManager.CommonManager.WidgetTypes_Select(UserId, ISAdmin, TypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetWidgetTypeRolesByID/{WidgetTypeID}")]
        [HttpGet]
        public ServiceResponse GetWidgetTypeRolesByID(int WidgetTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //return marcomManager.CommonManager.GetWidgetTypeRolesByID((int)common.WidgetTypeID);
                result.Response = marcomManager.CommonManager.GetWidgetTypeRolesByID(WidgetTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetWidgetTypeDimensionByID/{WidgetTypeID}")]
        [HttpGet]
        public ServiceResponse GetWidgetTypeDimensionByID(int WidgetTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //return marcomManager.CommonManager.Navigation_Select(((int)common.IsParentID == 1) ? true : false);
                //return marcomManager.CommonManager.WidgetTypeDimension_Select(WidgetTypeID);
                result.Response = marcomManager.CommonManager.WidgetTypeDimension_Select(WidgetTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/InsertWidgetTemplate")]
        [HttpPost]
        public ServiceResponse InsertWidgetTemplate([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertWidgetTemplate((string)jobj["TemplateName"], (string)jobj["TemplateDescription"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/UpdateWidgetTemplateByID")]
        [HttpPut]
        public ServiceResponse UpdateWidgetTemplateByID([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.WidgetTemplate_Update((int)jobj["ID"], (string)jobj["TemplateName"], (string)jobj["TemplateDescription"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/InsertWidgetTypeRoles")]
        [HttpPost]
        public ServiceResponse InsertWidgetTypeRoles([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                if ((jobj["widgetTypeID"] != null) && (jobj["roleID"] != null) && (marcomManager.CommonManager.DeleteWidgetTypeRoles((int)jobj["widgetTypeID"]) == true))
                {
                    JArray jobjGlobalRole = (JArray)jobj["roleID"];
                    result.StatusCode = (int)HttpStatusCode.OK;
                    foreach (var val in jobjGlobalRole)
                    {
                        result.Response = marcomManager.CommonManager.InsertWidgetTypeRoles((int)jobj["widgetTypeID"], (int)val);
                    }
                }


            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/DeleteWidgetTypeRoles/{widgetTypeID}")]
        [HttpDelete]
        public ServiceResponse DeleteWidgetTypeRoles(int widgetTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.DeleteWidgetTypeRoles(widgetTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/InsertWidgetTemplateRoles")]
        [HttpPost]
        public ServiceResponse InsertWidgetTemplateRoles([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                if ((jobj["WidgetTemplateID"] != null) && (jobj["roleID"] != null) && (marcomManager.CommonManager.DeleteWidgetTemplateRoles((int)jobj["WidgetTemplateID"]) == true))
                {
                    JArray jobjGlobalRole = (JArray)jobj["roleID"];
                    result.StatusCode = (int)HttpStatusCode.OK;
                    foreach (var val in jobjGlobalRole)
                    {
                        //result.Response = marcomManager.AccessManager.InsertGlobalRoleUser((int)val, (int)jobj["UserID"]);
                        result.Response = marcomManager.CommonManager.InsertWidgetTemplateRoles((int)jobj["WidgetTemplateID"], (int)val);
                    }
                }


            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetWidgetTemplateRolesByTemplateID/{WidgetTemplateID}")]
        [HttpGet]
        public ServiceResponse GetWidgetTemplateRolesByTemplateID(int WidgetTemplateID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetWidgetTemplateRolesByTemplateID(WidgetTemplateID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/DeleteWidgetTemplateRoles/{WidgetTemplateID}")]
        [HttpDelete]
        public ServiceResponse DeleteWidgetTemplateRoles(int WidgetTemplateID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.DeleteWidgetTemplateRoles(WidgetTemplateID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetUserDefaultSubscriptionByUserID")]
        [HttpGet]
        public ServiceResponse GetUserDefaultSubscriptionByUserID()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetUserDefaultSubscriptionByUserID();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/SaveDefaultSubscriptionByUserID")]
        [HttpPost]
        public ServiceResponse SaveDefaultSubscriptionByUserID([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                // int[] Isubarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["UserDefaultSubscription"].ToString());
                string subscriptionTypeIds = jobj["subscriptionTypeId"].ToString();
                string mailSubscriptionTypeIds = jobj["mailsubscriptionTypeId"].ToString();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.SaveSelectedDefaultSubscription(subscriptionTypeIds, mailSubscriptionTypeIds);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetWidgetDetailsByUserID/{UserID}/{IsAdmin}/{GlobalTemplateID}")]
        [HttpGet]
        public ServiceResponse GetWidgetDetailsByUserID(int UserID, bool IsAdmin, int GlobalTemplateID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetWidgetDetailsByUserID(UserID, IsAdmin, GlobalTemplateID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/InsertNotificationByMail")]
        [HttpPost]
        public ServiceResponse InsertNotificationByMail([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.SaveNotificationByMail((string)jobj["ColumnName"], (string)jobj["ColumnValue"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/InsertTaskNotificationByMail")]
        [HttpPost]
        public ServiceResponse InsertTaskNotificationByMail([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.SaveTaskNotificationByMail((string)jobj["ColumnName"], (string)jobj["ColumnValue"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/Widget")]
        [HttpPost]
        public ServiceResponse Widget([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //result.Response = marcomManager.CommonManager.InsertUpdateWidget((string)jobj["templateid"], (string)jobj["widgetid"], (string)jobj["caption"], (string)jobj["description"], (int)jobj["widgettypeid"], (int)jobj["filterid"], (int)jobj["attributeid"], (bool)jobj["isstatic"], ((string)jobj["widgetQuery"]), (jobj["dimensionid"].ToString() == "" ? 0 : (int)jobj["dimensionid"]), ((string)jobj["matrixid"]), (int)jobj["columnval"], (int)jobj["rowval"], (int)jobj["sizeXval"], (int)jobj["sizeYval"], (bool)jobj["isadminpage"], (jobj["visualtypeid"].ToString() == "" ? 0 : (int)jobj["visualtypeid"]), (jobj["NoOfItem"].ToString() == "" ? 0 : (int)jobj["NoOfItem"])); 
                result.Response = marcomManager.CommonManager.InsertUpdateWidget((string)jobj["templateid"], (string)jobj["widgetid"], (string)jobj["caption"], (string)jobj["description"], (int)jobj["widgettypeid"], (int)jobj["filterid"], (int)jobj["attributeid"], (bool)jobj["isstatic"], ((string)jobj["widgetQuery"]), (jobj["dimensionid"].ToString() == "" ? 0 : (int)jobj["dimensionid"]), ((string)jobj["matrixid"]), (int)jobj["columnval"], (int)jobj["rowval"], (int)jobj["sizeXval"], (int)jobj["sizeYval"], (bool)jobj["isadminpage"], (jobj["visualtypeid"].ToString() == "" ? 0 : (int)jobj["visualtypeid"]), (jobj["noofitem"].ToString() == "" ? 0 : (int)jobj["noofitem"]), ((string)jobj["listofEntityID"]), ((string)jobj["listofSelectEntityID"]), (jobj["noofYear"].ToString() == "" ? 0 : (int)jobj["noofYear"]), (jobj["noofMonth"].ToString() == "" ? 0 : (int)jobj["noofMonth"]));

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/WidgetDragEditing")]
        [HttpPost]
        public ServiceResponse WidgetDragEditing([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrwidgetlist = (JArray)jobj["widgetdatalist"];
                IList<IWidget> listwidget = new List<IWidget>();

                if (arrwidgetlist != null)
                {
                    foreach (var arrc in arrwidgetlist)
                    {
                        IWidget tempwidget = marcomManager.CommonManager.Widgetservice();

                        tempwidget.TemplateID = (int)arrc["templateid"];
                        tempwidget.Id = (string)arrc["widgtid"];
                        tempwidget.Column = (int)arrc["grid"]["col"];
                        tempwidget.Row = (int)arrc["grid"]["row"];
                        tempwidget.SizeX = (int)arrc["grid"]["sizex"];
                        tempwidget.SizeY = (int)arrc["grid"]["sizey"];

                        listwidget.Add(tempwidget);
                    }
                }

                result.StatusCode = (int)HttpStatusCode.OK;

                result.Response = marcomManager.CommonManager.WidgetDragEdit(listwidget, (bool)jobj["IsAdminPage"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetWidgetByID/{WidgetID}/{TemplateID}/{IsAdminPage}")]
        [HttpGet]
        public ServiceResponse GetWidgetByID(string WidgetID, string TemplateID, bool IsAdminPage)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetWidgetDetails(WidgetID, TemplateID, IsAdminPage);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/DeleteWidget/{WidgetID}/{TemplateID}/{IsAdminPage}")]
        [HttpDelete]
        public ServiceResponse DeleteWidget(string WidgetID, string TemplateID, bool IsAdminPage)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.DeleteWidget(TemplateID, WidgetID, IsAdminPage);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/InsertUserNotification")]
        [HttpPost]
        public ServiceResponse InsertUserNotification([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UserNotification_Insert((int)jobj["EntityID"], (int)jobj["ActorID"], (DateTimeOffset)jobj["CreatedOn"], (int)jobj["TypeID"], (bool)jobj["IsViewed"], (bool)jobj["IsSentInMail"], (string)jobj["TypeName"], (string)jobj["AttributeName"], (string)jobj["FromValue"], (string)jobj["ToValue"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetDynamicwidgetContentUserID/{UserID}/{WidgetTypeID}/{WidgetID}/{DimensionID}")]
        [HttpGet]
        public ServiceResponse GetDynamicwidgetContentUserID(int UserID, int WidgetTypeID, string WidgetID, int DimensionID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetDynamicwidgetContentUserID(UserID, WidgetTypeID, WidgetID, DimensionID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetSubscriptionByUserId")]
        [HttpGet]
        public ServiceResponse GetSubscriptionByUserId()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetSubscriptionByUserId();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetTaskSubscriptionByUserId")]
        [HttpGet]
        public ServiceResponse GetTaskSubscriptionByUserId()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetTaskSubscriptionByUserId();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetIsSubscribedFromSettings")]
        [HttpGet]
        public ServiceResponse GetIsSubscribedFromSettings()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetIsSubscribedFromSettings();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/EntityTypeForFeeds")]
        [HttpGet]
        public ServiceResponse EntityTypeForFeeds()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                // result.Response = null;
                result.Response = marcomManager.CommonManager.GetNotAssociateEntityTypes();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetPoSSettings/{PoSettings}")]
        [HttpGet]
        public ServiceResponse GetPoSSettings(string PoSettings)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //result.Response = null;
                result.Response = marcomManager.CommonManager.GetPoSSettings(PoSettings);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/InsertPoSettingXML")]
        [HttpPost]
        public ServiceResponse InsertPoSettingXML([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertPoSettingXML((string)jobj["Prefix"], (string)jobj["DateFormat"], (string)jobj["DigitFormat"], (string)jobj["NumberCount"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/InsertUpdateAdditionalSettings")]
        [HttpPost]
        public ServiceResponse InsertUpdateAdditionalSettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertUpdateAdditionalSettings((int)jobj["ID"], (string)jobj["Settingname"], (string)jobj["settingValue"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetAdditionalSettings")]
        [HttpGet]
        public ServiceResponse GetAdditionalSettings()
        {
            result = new ServiceResponse();
            try
            {               
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetAdditionalSettings();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/InsertAdminSettingXML")]
        [HttpPost]
        public ServiceResponse InsertAdminSettingXML([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.Insert_AdminEmail((string)jobj["emailids"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetEmailIds")]
        [HttpGet]
        public ServiceResponse GetEmailIds()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetEmailids();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetLanguageTypes")]
        [HttpGet]
        public ServiceResponse GetLanguageTypes()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetLanguageTypes();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/SaveNewLanguage")]
        [HttpPost]
        public ServiceResponse SaveNewLanguage([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.SaveNewLanguage((int)jobj["InheritedId"], (string)jobj["Name"], (string)jobj["Description"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/GetLanguageExport/{ID}/{languageTypename}/{Filename}")]
        [HttpGet]
        public ServiceResponse GetLanguageExport(int ID, string languageTypename, string Filename)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetLanguageExport(ID, languageTypename, Filename);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/GetLanguageContent/{StartRows}/{NextRows}")]
        [HttpGet]
        public ServiceResponse GetLanguageContent(int StartRows, int NextRows)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetLanguageContent(StartRows, NextRows);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/UpdateLanguageContent")]
        [HttpPost]
        public ServiceResponse UpdateLanguageContent([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdateLanguageContent((int)jobj["LangTypeID"], (int)jobj["ContentID"], (string)jobj["newValue"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }


        [Route("api/Common/GetLanguageSettings/{LangID}")]
        [HttpGet]
        public ServiceResponse GetLanguageSettings(int LangID)
        {
            result = new ServiceResponse();
            try
            {
                Guid sessionapi = new Guid(((string[])(Request.Headers.GetValues("sessioncookie")))[0]);
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,sessionapi);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetLanguageSettings(LangID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetCurrentPONumber")]
        [HttpGet]
        public ServiceResponse GetCurrentPONumber()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetCurrentPONumber();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/UpdateLanguageName")]
        [HttpPost]
        public ServiceResponse UpdateLanguageName([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdateLanguageName((int)jobj["LangTypeID"], (string)jobj["NewValue"], (int)jobj["NameOrDesc"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/SetDefaultLanguage")]
        [HttpPost]
        public ServiceResponse SetDefaultLanguage([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.SetDefaultLanguage((int)jobj["LangID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/LanguageSearch")]
        [HttpPost]
        public ServiceResponse LanguageSearch([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.LanguageSearch((int)jobj["LangID"], (string)jobj["searchtext"], (string)jobj["searchdate"], (int)jobj["StartRowsSearch"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/LanguageSearchs")]
        [HttpPost]
        public ServiceResponse LanguageSearchs([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray jr = (JArray)jobj["filterval"];
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.LanguageSearchs((int)jobj["LangID"], jr[0].ToString());
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetDefaultLangFromXML")]
        [HttpGet]
        public ServiceResponse GetDefaultLangFromXML()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetDefaultLangFromXML();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetUserDefaultTaskNotificationMailSettings")]
        [HttpGet]
        public ServiceResponse GetUserDefaultTaskNotificationMailSettings()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetUserDefaultTaskNotificationMailSettings();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }


        [Route("api/Common/GetSupportText")]
        [HttpGet]
        public ServiceResponse GetSupportText()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetEditorText();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/InsertEditortext")]
        [HttpPost]
        public ServiceResponse InsertEditortext([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray jr = (JArray)jobj["entityList"];
                int[] entityidList = jr.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertEditortext(entityidList, (string)jobj["Content"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }

            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetSSODetails")]
        [HttpGet]
        public ServiceResponse GetSSODetails()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetSSODetails();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/UpdateSSOSettings")]
        [HttpPost]
        public ServiceResponse UpdateSSOSettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray SAMLRoles = (JArray)jobj["SAMLroles"];
                result.Response = marcomManager.CommonManager.UpdateSSOSettings((string)jobj["key"], (string)jobj["iv"], (string)jobj["algo"], (string)jobj["paddingmode"], (string)jobj["ciphermode"], (string)jobj["tokenmode"], (string)jobj["SSOTimeDifference"], (string)jobj["UserGroupsvalue"], (string)jobj["ClientIntranetUrl"], (string)jobj["globalAccessRoles"], SAMLRoles);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/IsActiveEntity/{EntityID}")]
        [HttpGet]
        public ServiceResponse IsActiveEntity(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.IsActiveEntity(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetUnits")]
        [HttpGet]
        public ServiceResponse GetUnits()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetUnits();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetFeedFilter")]
        [HttpGet]
        public ServiceResponse GetFeedFilter()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetFilterGroup();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/InsertUpdateFeedFilterGroup")]
        [HttpPost]
        public ServiceResponse InsertUpdateFeedFilterGroup([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertUpdateFeedFilterGroup((int)jobj["ID"], (string)jobj["FeedGroup"], (string)jobj["Template"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/DeleteFeedGroupByid/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteFeedGroupByid(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                var id = ID;
                result.Response = marcomManager.CommonManager.DeleteFeedGroupByid(id);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetFeedTemplates")]
        [HttpGet]

        public ServiceResponse GetFeedTemplates()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetFeedTemplates();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetUnitsByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetUnitsByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetUnitsByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/InsertUpdateUnits")]
        [HttpPost]
        public ServiceResponse InsertUpdateUnits([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertUpdateUnits((string)jobj["Caption"], (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/DeleteUnitsByid/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteUnitsByid(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                var ids = (string)ID.ToString();
                for (int i = 0; i < ids.Split(',').Length; i++)
                {
                    result.Response = marcomManager.CommonManager.DeleteUnitsByid(Convert.ToInt32(ids.Split(',')[i]));
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetTopNavigation")]
        [HttpGet]
        public ServiceResponse GetTopNavigation()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetTopNavigation();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/InsertAdminNotificationSettings")]
        [HttpPost]
        public ServiceResponse InsertAdminNotificationSettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray subscriptionChkList = (JArray)jobj["subscriptionObject"];
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertAdminNotificationSettings(subscriptionChkList);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/InsertCurrencyFormat")]
        [HttpPost]
        public ServiceResponse InsertCurrencyFormat([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertCurrencyFormat((string)jobj["ShortName"], (string)jobj["Name"], (string)jobj["Symbol"], (int)jobj["Id"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/InsertUpdateGanttHeaderBar")]
        [HttpPost]
        public ServiceResponse InsertUpdateGanttHeaderBar([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                DateTime StartDate = new DateTime(); DateTime EndDate = new DateTime();
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                string NewstartDate = (jobj["Startdate"].ToString()) == "" ? null : (DateTime.Parse(jobj["Startdate"].ToString())).ToString("yyyy/MM/dd");
                string NewendDate = (jobj["EndDate"].ToString()) == "" ? null : (DateTime.Parse(jobj["EndDate"].ToString())).ToString("yyyy/MM/dd");
                DateTime startDate = new DateTime(); DateTime endDate = new DateTime();
                if (DateTime.TryParse(NewstartDate, out startDate) && DateTime.TryParse(NewendDate, out endDate))
                {
                    StartDate = startDate;
                    EndDate = endDate;
                }
                result.Response = marcomManager.CommonManager.InsertUpdateGanttHeaderBar((int)jobj["Id"], (string)jobj["Name"], (string)jobj["Description"], StartDate, EndDate, (string)jobj["ColorCode"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetAllGanttHeaderBar")]
        [HttpGet]
        public ServiceResponse GetAllGanttHeaderBar()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetAllGanttHeaderBar();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/DeleteGanttHeaderBar/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteGanttHeaderBar(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.DeleteGanttHeaderBar(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetUniqueuserhit/{year}/{month}")]
        [HttpGet]
        public ServiceResponse GetUniqueuserhit(int year, int month)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetUniqueuserhit(year, month);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetApplicationhit/{year}/{month}")]
        [HttpGet]
        public ServiceResponse GetApplicationhit(int year, int month)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetApplicationhit(year, month);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetBrowserStatistic/{year}/{month}")]
        [HttpGet]
        public ServiceResponse GetBrowserStatistic(int year, int month)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetBrowserStatistic(year, month);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetBrowserVersionStatistic/{year}/{month}")]
        [HttpGet]
        public ServiceResponse GetBrowserVersionStatistic(int year, int month)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetBrowserVersionStatistic(year, month);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetUserStatistic")]
        [HttpGet]
        public ServiceResponse GetUserStatistic()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetUserStatistic();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetOSStatistic")]
        [HttpGet]
        public ServiceResponse GetOSStatistic()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetOSStatistic();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetstartpageStatistic")]
        [HttpGet]
        public ServiceResponse GetstartpageStatistic()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetstartpageStatistic();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetUserRoleStatistic")]
        [HttpGet]
        public ServiceResponse GetUserRoleStatistic()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetUserRoleStatistic();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetEnityStatistic")]
        [HttpGet]
        public ServiceResponse GetEnityStatistic()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetEnityStatistic();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetEnityCreateationStatistic/{year}/{month}")]
        [HttpGet]
        public ServiceResponse GetEnityCreateationStatistic(int year, int month)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetEnityCreateationStatistic(year, month);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetbandwidthStatistic/{year}/{month}")]
        [HttpGet]
        public ServiceResponse GetbandwidthStatistic(int year, int month)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetbandwidthStatistic(year, month);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetBroadcastMessages")]
        [HttpGet]
        public ServiceResponse GetBroadcastMessages()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetBroadcastMessages();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetBroadcastMessagesbyuser")]
        [HttpGet]
        public ServiceResponse GetBroadcastMessagesbyuser()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetBroadcastMessagesbyuser();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/InsertBroadcastMessages")]
        [HttpPost]
        public ServiceResponse InsertBroadcastMessages([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertBroadcastMessages((int)jobj["userId"], (string)jobj["username"], (string)jobj["broadcastmsg"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/updateBroadcastMessagesbyuser")]
        [HttpPost]
        public ServiceResponse updateBroadcastMessagesbyuser()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.updateBroadcastMessagesbyuser();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetCurrencyconverter")]
        [HttpGet]
        public ServiceResponse GetCurrencyconverter()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.getCurrencyconverterData();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/Insertupdatecurrencyconverter")]
        [HttpPost]
        public ServiceResponse Insertupdatecurrencyconverter([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.Insertupdatecurrencyconverter((DateTime)jobj["Startdate"], (DateTime)jobj["Enddate"], (string)jobj["Currencytype"], (double)jobj["Currencyrate"], (int)jobj["Id"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/DeleteCurrencyconverter/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteCurrencyconverter(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                var ids = (string)ID.ToString();
                for (int i = 0; i < ids.Split(',').Length; i++)
                {
                    result.Response = marcomManager.CommonManager.DeleteCurrencyconverterData(Convert.ToInt32(ids.Split(',')[i]));
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetRatesByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetRatesByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetRatesByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetExchangesratesbyCurrencytype/{ID}")]
        [HttpGet]
        public ServiceResponse GetExchangesratesbyCurrencytype(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetExchangesratesbyCurrencytype(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/InsertEntityAttachmentsVersion")]
        [HttpPost]
        public ServiceResponse InsertEntityAttachmentsVersion([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrAttchObj = (JArray)jobj["Attachments"];
                JArray arrFilesObj = (JArray)jobj["Files"];
                IList<IAttachments> listEntityattachment = new List<IAttachments>();
                if (arrAttchObj != null)
                {
                    foreach (var arm in arrAttchObj)
                    {
                        IAttachments attachval = marcomManager.TaskManager.EntityTasksAttachmentService();
                        attachval.Name = (string)arm["FileName"] + (string)arm["Extension"];
                        attachval.ActiveFileid = 1;
                        attachval.ActiveVersionNo = 1;
                        attachval.Createdon = DateTime.UtcNow;
                        attachval.Typeid = 4;
                        attachval.VersioningFileId = (int)jobj["VersioningFileId"];
                        listEntityattachment.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listEntityattachment = null;
                }

                IList<IFile> listEntityFiles = new List<IFile>();
                if (arrFilesObj != null)
                {
                    foreach (var arm in arrFilesObj)
                    {
                        IFile attachval = marcomManager.CommonManager.Fileservice();
                        attachval.Checksum = (string)arm["Checksum"];
                        attachval.CreatedOn = (DateTime)arm["CreatedOn"];
                        attachval.Extension = (string)arm["Extension"];
                        attachval.MimeType = (string)arm["MimeType"];
                        attachval.Moduleid = (int)arm["ModuleID"];
                        attachval.Name = (string)arm["Name"];
                        attachval.Ownerid = (int)arm["OwnerID"];
                        attachval.Size = (long)arm["Size"];
                        attachval.VersionNo = (int)arm["VersionNo"];
                        attachval.strFileID = (string)arm["FileGuid"];
                        attachval.Description = (string)arm["FileDescription"];
                        listEntityFiles.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listEntityattachment = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertEntityAttachmentsVersion((int)jobj["EntityID"], listEntityattachment, listEntityFiles, (int)jobj["FileId"], (int)jobj["VersioningFileId"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetCustomTabsByTypeID/{TypeID}")]
        [HttpGet]
        public ServiceResponse GetCustomTabsByTypeID(int TypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetCustomTabsByTypeID(TypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/InsertUpdateCustomTab")]
        [HttpPost]
        public ServiceResponse InsertUpdateCustomTab([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertUpdateCustomTab((int)jobj["Id"], (int)jobj["Typeid"], (string)jobj["Name"], (string)jobj["ExternalUrl"], (bool)jobj["AddEntityID"], (bool)jobj["AddLanguageCode"], (bool)jobj["AddUserEmail"], (bool)jobj["AddUserName"], (bool)jobj["AddUserID"], (int)jobj["tabEncryID"], (string)jobj["encryKey"], (string)jobj["encryIV"], (string)jobj["algorithm"], (string)jobj["paddingMode"], (string)jobj["cipherMode"], (string)jobj["EntityTypeIds"], (string)jobj["GlobalIds"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/DeleteCustomtabByID/{ID}/{AttributeTypeID}/{EntityTypeID}")]
        [HttpDelete]
        public ServiceResponse DeleteCustomtabByID(int ID, int AttributeTypeID, int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.DeleteCustomtabByID(ID, AttributeTypeID, EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/UpdateCustomTabSortOrder/{ID}/{SortOrder}")]
        [HttpPut]
        public ServiceResponse UpdateCustomTabSortOrder(int ID, int SortOrder)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdateCustomTabSortOrder(ID, SortOrder);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/InsertUpdateApplicationUrlTrack")]
        [HttpPost]
        public ServiceResponse InsertUpdateApplicationUrlTrack([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertUpdateApplicationUrlTrack((Guid)(jobj)["TrackID"], (string)(jobj)["TrackValue"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetApplicationUrlTrackByID/{TrackID}")]
        [HttpGet]
        public ServiceResponse GetApplicationUrlTrackByID(Guid TrackID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetApplicationUrlTrackByID(TrackID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetCustomEntityTabsByTypeID/{TypeID}/{CalID}/{EntityTypeID}/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetCustomEntityTabsByTypeID(int TypeID, int CalID, int EntityTypeID, int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetCustomEntityTabsByTypeID(TypeID, CalID, EntityTypeID, EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetCustomTabUrlTabsByTypeID/{tabID}/{entityID}")]
        [HttpGet]
        public ServiceResponse GetCustomTabUrlTabsByTypeID(int tabID, int entityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetCustomTabUrlTabsByTypeID(tabID, entityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/UpdateCustomTabSettings")]
        [HttpPost]
        public ServiceResponse UpdateCustomTabSettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdateCustomTabSettings((string)jobj["key"], (string)jobj["iv"], (string)jobj["algo"], (string)jobj["paddingmode"], (string)jobj["ciphermode"], (string)jobj["tokenmode"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetCustomTabSettingDetails")]
        [HttpGet]
        public ServiceResponse GetCustomTabSettingDetails()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetCustomTabSettingDetails();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/GetConvertedcurrencies")]
        [HttpPost]
        public IList<IConvertedcurrencies> GetConvertedcurrencies([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            IList<IConvertedcurrencies> objcurrncy = new List<IConvertedcurrencies>();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray SortOrderUpdate = (JArray)jobj["ConvertedCurrenciesObj"];

                IConvertedcurrencies intobj = null;

                foreach (var item in SortOrderUpdate)
                {
                    result.Response = marcomManager.CommonManager.GetConvertedcurrencies((int)item["Amount"], (int)item["Id"], (string)item["Currency"], (DateTime)item["Duedate"]);
                    intobj = (IConvertedcurrencies)result.Response;
                    objcurrncy.Add(intobj);
                }

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            //  return result;
            return objcurrncy;
        }

        [Route("api/Common/GetUpdateSettings")]
        [HttpGet]
        public ServiceResponse GetUpdateSettings()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetUpdateSettings();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetPasswordPolicyDetails")]
        [HttpGet]
        public ServiceResponse GetPasswordPolicyDetails()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetPasswordPolicyDetails();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/UpdatePasswordPolicy")]
        [HttpPost]
        public ServiceResponse UpdatePasswordPolicy([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdatePasswordPolicy((string)jobj["MinLength"], (string)jobj["MaxLength"], (string)jobj["NumLength"], (string)jobj["UpperLength"], (string)jobj["SpecialLength"], ((string)jobj["SpecialChars"]), ((string)jobj["BarWidth"]), ((string)jobj["MultipleColors"]));
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetPlantabsettings")]
        [HttpGet]
        public ServiceResponse GetPlantabsettings()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetPlantabsettings();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/UpdatePlanTabIds")]
        [HttpPost]
        public ServiceResponse UpdatePlanTabIds([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdatePlanTabsettings((string)jobj["jsondata"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetCustomTabEncryptionByID")]
        [HttpGet]
        public ServiceResponse GetCustomTabEncryptionByID()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetCustomTabEncryptionByID();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetOptimakerAddresspoints")]
        [HttpGet]
        public ServiceResponse GetOptimakerAddresspoints()
        {
            result = new ServiceResponse();
            try
            {
               
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetOptimakerAddresspoints();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/GetAssetFeeds/{AssetID}/{PageNo}")]
        [HttpGet]
        public ServiceResponse GetAssetFeeds(int AssetID, int PageNo)
        {
            result = new ServiceResponse();

            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GettingFeedsByAsset(AssetID, PageNo);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetLastAssetFeeds/{AssetID}")]
        [HttpGet]
        public ServiceResponse GetLastAssetFeeds(int AssetID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GettingLatestFeedsByAsset(AssetID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/FeedAsset")]
        [HttpPost]
        public ServiceResponse FeedAsset([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray arrlst = (JArray)jobj["PersonalUserIds"];
                int[] userids = arrlst.Select(jv => (int)jv).ToArray();
                result.Response = marcomManager.CommonManager.SaveUpdateFeed((int)jobj["Actor"], (int)jobj["TemplateID"], (int)jobj["EntityID"], (string)jobj["TypeName"], (string)jobj["AttributeName"], (string)jobj["FromValue"], (string)jobj["ToValue"], 0, (int)jobj["associatedentityid"], null, (int)jobj["version"], 0, "", userids);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/IsAvailableAsset/{AssetID}")]
        [HttpGet]
        public ServiceResponse IsAvailableAsset(int AssetID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.IsAvailableAsset(AssetID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetCustomTabAccessByID/{TabID}")]
        [HttpGet]
        public ServiceResponse GetCustomTabAccessByID(int TabID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetCustomTabAccessByID(TabID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetAdminSettings/{LogoSettings}/{typeid}")]
        [HttpGet]
        public ServiceResponse GetAdminSettings(string LogoSettings, int typeid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetAdminSettings(LogoSettings, typeid);
            }
            catch
            {
                return null;
            }
            return result;
        }

        [Route("api/Common/GetAdminLayoutSettings/{LogoSettings}/{typeid}")]
        [HttpGet]
        public ServiceResponse GetAdminLayoutSettings(string LogoSettings, int typeid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetAdminLayoutSettings(LogoSettings, typeid);
            }
            catch
            {
                return null;
            }
            return result;
        }

        [Route("api/Common/GetAdminLayoutFinSettings/{LogoSettings}/{typeid}")]
        [HttpGet]
        public ServiceResponse GetAdminLayoutFinSettings(string LogoSettings, int typeid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetAdminLayoutFinSettings(LogoSettings, typeid);
            }
            catch
            {
                return null;
            }
            return result;
        }

        [Route("api/Common/GetAdminLayoutObjectiveSettings/{LogoSettings}/{typeid}")]
        [HttpGet]
        public ServiceResponse GetAdminLayoutObjectiveSettings(string LogoSettings, int typeid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetAdminLayoutObjectiveSettings(LogoSettings, typeid);
            }
            catch
            {
                return null;
            }
            return result;
        }
        [Route("api/Common/LayoutDesign")]
        [HttpPost]
        public ServiceResponse LayoutDesign(string TabType, int TabLocation)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.LayoutSettingsApplyChanges(TabType, TabLocation.ToString());
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetAttributeSearchCriteria/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetAttributeSearchCriteria(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetAttributeSearchCriteria(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/SearchadminSettingsforRootLevelInsertUpdate")]
        [HttpPost]
        public ServiceResponse SearchadminSettingsforRootLevelInsertUpdate([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                string jsondata = Convert.ToString(jobj);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.SearchadminSettingsforRootLevelInsertUpdate(jsondata, (string)jobj["LogoSettings"], (string)jobj["Key"], (int)jobj["TypeId"], (int)jobj["enableAutoExpand"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetCustomEntityTabsfrCalID/{TypeID}/{CalID}")]
        [HttpGet]
        public ServiceResponse GetCustomEntityTabsfrCalID(int TypeID, int CalID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetCustomEntityTabsfrCalID(TypeID, CalID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetCustomEntityTabsByTypeID/{TypeID}")]
        [HttpGet]
        public ServiceResponse GetCustomEntityTabsByTypeID(int TypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetCustomEntityTabsByTypeID(TypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/LayoutSettingsApplyChanges")]
        [HttpPost]
        public ServiceResponse LayoutSettingsApplyChanges([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.LayoutSettingsApplyChanges((string)jobj["tabtypename"], (string)jobj["locationname"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetProofHQSSettings")]
        [HttpGet]
        public ServiceResponse GetProofHQSSettings()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetProofHQSSettings();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/UpdateProofHQSSettings")]
        [HttpPost]
        public ServiceResponse UpdateProofHQSSettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdateProofHQSSettings((string)jobj["uname"], (string)jobj["password"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetCalendarNavigationConfig")]
        [HttpGet]
        public ServiceResponse GetCalendarNavigationConfig()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetCalendarNavigationConfig();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/UpdateExpirytime")]
        [HttpPost]
        public ServiceResponse UpdateExpirytime([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdateExpirytime((string)jobj["Time"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/Getexpirytime")]
        [HttpGet]
        public ServiceResponse Getexpirytime()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.Getexpirytime();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/UpdatePopularTagWordsToShow")]
        [HttpPost]
        public ServiceResponse UpdatePopularTagWordsToShow([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdatePopularTagWordsToShow((int)jobj["TotalTagWords"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetTotalPopularTagWordsToShow")]
        [HttpGet]
        public ServiceResponse GetTotalPopularTagWordsToShow()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetTotalPopularTagWordsToShow();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        //newly added by Lakshanya
        [Route("api/Common/GettingAssetsFeedSelectionDashbord/{Topx}/{FeedTemplateID}/{Newsfeedid}")]
        [HttpGet]
        public ServiceResponse GettingAssetsFeedSelectionDashbord(int Topx, int FeedTemplateID, int Newsfeedid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                // string entityids= marcomManager.CommonManager.GetEntityIdsForFeed((int)common.EntitiyID);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GettingAssetsFeedSelectionDashbord(Topx, FeedTemplateID, Newsfeedid);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetAssetCreateationStatistic/{year}/{month}")]
        [HttpGet]
        public ServiceResponse GetAssetCreateationStatistic(int year, int month)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetAssetCreateationStatistic(year, month);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/InsertLanguageImport")]
        [HttpPost]
        public ServiceResponse InsertLanguageImport([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertLanguageImport((string)jobj["FileImport"], (int)jobj["LangTypeId"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/GetProofInitiatorStatistic")]
        [HttpPost]
        public ServiceResponse GetProofInitiatorStatistic([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetProofInitiatorStatistic();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetSearchtype/{userid}")]
        [HttpGet]
        public ServiceResponse GetSearchtype(int userid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetSearchtype(userid);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetassignedAcess/{userid}")]
        [HttpGet]
        public ServiceResponse GetassignedAcess(int userid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetassignedAcess(userid);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/UpdateThemeSettings")]
        [HttpPost]
        public ServiceResponse UpdateThemeSettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdateThemeSettings((string)jobj["themename"], (int)jobj["themeID"], (JObject)jobj["themeBlockData"], (JObject)jobj["Greyvariation"], (JObject)jobj["objBasecolorvariations"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/RestoreDefaultTheme")]
        [HttpPost]
        public ServiceResponse RestoreDefaultTheme()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.RestoreDefaultTheme();

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetThemeData/{Theme}")]
        [HttpGet]
        public ServiceResponse GetThemeData(int Theme)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetThemeData(Theme);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetThemeValues")]
        [HttpGet]
        public ServiceResponse GetThemeValues()
        {
            result = new ServiceResponse();
            try
            {
               
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetThemeValues();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/InsertNotifydisplaytime")]
        [HttpPost]
        public ServiceResponse InsertNotifydisplaytime([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.Insert_notifydisplaytime((string)jobj["notifydisplaytime"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }


        [Route("api/Common/InsertNotifycycledisplaytime")]
        [HttpPost]
        public ServiceResponse InsertNotifycycledisplaytime([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.Insert_notifycycledisplaytime((string)jobj["notifydisplaytime"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/Getnotifytimesetbyuser")]
        [HttpGet]
        public ServiceResponse Getnotifytimesetbyuser()
        {
            result = new ServiceResponse();
            try
            {
               
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.Getnotifytimesetbyuser();

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/Getnotifyrecycletimesetbyuser")]
        [HttpGet]
        public ServiceResponse Getnotifyrecycletimesetbyuser()
        {
            result = new ServiceResponse();
            try
            {
               
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.Getnotifyrecycletimesetbyuser();

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetTaskLIveUpdateRecords")]
        [HttpGet]
        public ServiceResponse GetTaskLIveUpdateRecords()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetTaskLIveUpdateRecords();
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("CommonManager/GetTaskLIveUpdateRecords method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        // Method already exists
        /*[HttpPost]
        public ServiceResponse RestoreDefaultTheme([FromBody]JObject jobj)           
            {
                result = new ServiceResponse();
                try
                {
                    IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                    result.StatusCode = (int)HttpStatusCode.OK;
                    result.Response = marcomManager.CommonManager.RestoreDefaultTheme();
                }
                catch
                {
                    result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                    result.Response = 0;
                }
                return result;
            }*/
        [Route("api/Common/UpdateTitleSettings")]
        [HttpPost]
        public ServiceResponse UpdateTitleSettings([FromBody]JObject Titlelogojobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                var jobj = (JObject)Titlelogojobj["TitleSettings"];
                var ImageChanged = (JObject)Titlelogojobj["IsImageChanged"];
                var isupdated = marcomManager.CommonManager.UpdateTitleSettings((string)jobj["titlename"], (int)jobj["titleID"], (JObject)jobj["titleBlockData"], ImageChanged);
                if (isupdated)
                {
                    var amdinsettingsjobj = (JObject)Titlelogojobj["AdminSettings"];
                    string jsondata = Convert.ToString(amdinsettingsjobj);
                    result.StatusCode = (int)HttpStatusCode.OK;
                    result.Response = marcomManager.CommonManager.AdminSettingsInsertUpdate(jsondata, (string)amdinsettingsjobj["Key"], (string)amdinsettingsjobj["LogoSettings"]["logo"], (string)amdinsettingsjobj["LogoSettings"]["Applogo"], ImageChanged);
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetTitleLogoSettings/{TitleSettings}")]
        [HttpGet]
        public ServiceResponse GetTitleLogoSettings(string TitleSettings)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetTitleLogoSettings(TitleSettings);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Common/GetAlltableswithmetadata/{Tablename}")]
        [HttpGet]
        public ServiceResponse GetAlltableswithmetadata(string Tablename)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //var data = marcomManager.CommonManager.GetAlltableswithmetadata();
                result.Response = marcomManager.CommonManager.GetAlltableswithmetadata(Tablename);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/ManipulateQueryEditorQuery")]
        [HttpPost]
        public ServiceResponse ManipulateQueryEditorQuery([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.ManipulateQueryEditorQuery((string)jobj["qrybody"], tenantres.tenanturl);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetAlltablesnames")]
        [HttpGet]
        public ServiceResponse GetAlltablesnames()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //var data = marcomManager.CommonManager.GetAlltableswithmetadata();
                result.Response = marcomManager.CommonManager.GetAlltablesnames();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/getsnippet/{token}")]
        [HttpGet]
        public string getsnippet(string tokenobj)
        {
            //StringBuilder result1 = new StringBuilder();
            string contents = "";
            try
            {
                var servicePath = WebConfigurationManager.AppSettings["hosturl"];
                string token = (string)tokenobj;
                string mappingfilesPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
                mappingfilesPath = mappingfilesPath + "app" + @"\controllers\mui\Snippetcode.js";
                contents = File.ReadAllText(mappingfilesPath);
                contents = contents.Replace("@serviceurl", servicePath);

            }
            catch
            {

            }

            return contents;
        }

        [Route("api/Common/getTenantClientPath")]
        [HttpGet]
        public string getTenantClientPath()
        {
            string TenantPath;
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantPath = marcomManager.CommonManager.getTenantClientPath();
            }
            catch
            {
                TenantPath = null;
            }
            return TenantPath;
        }
        [Route("api/Common/getsnippethoturl")]
        [HttpGet]
        public string getsnippethoturl()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
            StringBuilder result1 = new StringBuilder();
            string contents = "";
            try
            {
                var servicePath = WebConfigurationManager.AppSettings["hosturl"];
                contents = servicePath;

            }
            catch
            {

            }
            return contents;
        }
        [Route("api/Common/GetObjectLanguageContent/{LangEditID}")]
        [HttpGet]
        public ServiceResponse GetObjectLanguageContent(int LangEditID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetObjectLanguageContent(LangEditID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetlanguagetypeByID/{languageTypeID}")]
        [HttpGet]
        public ServiceResponse GetlanguagetypeByID(int languageTypeID)
        {
            result = new ServiceResponse();

            try
            {
                IMarcomManager marcomManger = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManger.CommonManager.GetlanguageContentObjectByID(languageTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/UpdateJsonLanguageContent")]
        [HttpPost]
        public ServiceResponse UpdateJsonLanguageContent([FromBody]JObject jobj)
        {
            result = new ServiceResponse();

            try
            {
                string JsonLanguage = (string)jobj["JsonLanguageData"];
                int selectedLangID = (int)jobj["SelectedLangID"];
                string LangKey = (string)jobj["LangKey"];
                int TranslationPending = (int)jobj["TranslationPending"];
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdateJsonLanguageContent(JsonLanguage, selectedLangID, LangKey, TranslationPending);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/InsertNewLanguage")]
        [HttpPost]
        public ServiceResponse InsertNewLanguage([FromBody]JObject jobj)
        {
            result = new ServiceResponse();

            try
            {
                string Name = (string)jobj["Name"];
                string Description = (string)jobj["Description"];
                int InhertedLangID = (int)jobj["InheritedId"];
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.Response = marcomManager.CommonManager.InsertNewLanguage(Name, Description, InhertedLangID);

            }
            catch
            {
                result.Response = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetdefaultlangKey")]
        [HttpGet]
        public ServiceResponse GetdefaultlangKey()
        {
            result = new ServiceResponse();

            try
            {
                IMarcomManager marcomManger = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.Response = marcomManger.CommonManager.GetDefaultLanguageKey();
            }
            catch
            {
                result.Response = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }


        [Route("api/Common/GetModuleID/{EntityId}")]
        [HttpGet]
        public ServiceResponse GetModuleID(int EntityId)
        {
            result = new ServiceResponse();

            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetModuleID(EntityId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/UpdateAccLockPasswordPolicy")]
        [HttpPost]
        public ServiceResponse UpdateAccLockPasswordPolicy([FromBody]JObject jobj)
        {
            result = new ServiceResponse();

            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdateAccLockPasswordPolicy((int)jobj["lockouttime"], (int)jobj["attempts"], (int)jobj["monthlyrepetition"], (int)jobj["repetitiontime"], (int)jobj["monthlyexpiration"], (bool)jobj["expirevalidation"], (bool)jobj["notifyuser"], (bool)jobj["notifyadmin"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/GetAccLockDetails/")]
        [HttpGet]
        public ServiceResponse GetAccLockDetails()
        {
            result = new ServiceResponse();

            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetAccLockDetails();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/getredirectPath/{entityID}")]
        [HttpGet]
        public ServiceResponse getredirectPath(int entityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.Response = marcomManager.CommonManager.getRedirectPath(entityID);
            }
            catch
            {
                result.Response = null;
            }
            return result;
        }


        [Route("api/Common/InsertUpdateDecimalSettings")]
        [HttpPost]
        public ServiceResponse InsertUpdateDecimalSettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.Response = marcomManager.CommonManager.InsertUpdateDecimalSettings((int)jobj["deciamlplaces"], (string)jobj["pagename"]);
                result.StatusCode = (int)HttpStatusCode.OK;
            }
            catch
            {
                result.Response = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetDecimalSettingsValue")]
        [HttpGet]
        public ServiceResponse GetDecimalSettingsValue()
        {
            result = new ServiceResponse();
            try
            {
               
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetDecimalSettingsValue();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetDalimSettings")]
        [HttpGet]
        public ServiceResponse GetDalimSettings()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetDalimSettings();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/UpdateDalimSettings")]
        [HttpPost]
        public ServiceResponse UpdateDalimSettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                JArray TemaplateCollection = new JArray();

                if ((int)jobj["TemaplateCollection"].Count() > 0)
                {
                    TemaplateCollection = (JArray)jobj["TemaplateCollection"][0];
                }
                else
                {
                    TemaplateCollection = (JArray)jobj["TemaplateCollection"];
                }
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdateDalimSettings((string)jobj["dalimcustomerId"], (string)jobj["dalimcustomername"], (string)jobj["dalimdefaultusername"], (string)jobj["dalimdefaultuserpassword"], (string)jobj["dalimorgUnit"], (string)jobj["dalimdefaultprofile"], (string)jobj["dalimprojecttemplatename"], (string)jobj["dalimProtocol"], (string)jobj["externalurl"], (string)jobj["dalimDomain"], TemaplateCollection);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/updateDefaultFolderInfo")]
        [HttpPost]
        public ServiceResponse updateDefaultFolderInfo([FromBody]JObject DefaultFolderObj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.updateDefaultFolderInfo(DefaultFolderObj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }

            return result;
        }

        [Route("api/Common/getDefaultFolderInfo")]
        [HttpGet]
        public ServiceResponse getDefaultFolderInfo()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.getDefaultFolderInfo();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }


        [Route("api/Common/SaveHolidayDetails/")]
        [HttpPost]
        public ServiceResponse SaveHolidayDetails([FromBody]JObject day)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.SaveHolidayDetails((string)day["nonbusinessday"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetNonBusinessDays/")]
        [HttpGet]
        public ServiceResponse GetNonBusinessDays()
        {
            result = new ServiceResponse();

            try
            {
               
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetNonBusinessDays();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Common/InsertHolidayDetails/")]
        [HttpPost]
        public ServiceResponse InsertHolidayDetails([FromBody]JObject holidaydetails)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertHolidayDetails(holidaydetails);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }


        [Route("api/Common/GetHolidaysDetails/")]
        [HttpGet]
        public ServiceResponse GetHolidaysDetails()
        {
            result = new ServiceResponse();

            try
            {
               
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetHolidaysDetails();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/DeleteHoliday/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteHoliday(int ID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.DeleteHoliday(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/UpdateNewFeedconfig")]
        [HttpPost]
        public ServiceResponse UpdateNewFeedconfig([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdateNewFeedconfig(jobj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }

            return result;
        }

        [Route("api/Common/GetNewsFeedConfigInfo")]
        [HttpGet]
        public ServiceResponse GetNewsFeedConfigInfo()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetNewsFeedConfigInfo();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }


        [Route("api/Common/savecloudsettings")]
        [HttpPost]
        public ServiceResponse savecloudsettings(JObject jObj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.savecloudsettings(jObj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }


        [Route("api/Common/getcloudsettings")]
        [HttpGet]
        public ServiceResponse getcloudsettings()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.getcloudsettings();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetNavigationandLanguageConfig")]
        [HttpGet]
        public ServiceResponse GetNavigationandLanguageConfig()
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetNavigationandLanguageConfig();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Common/GetObjectiveTabAdminSettings/{LogoSettings}/{typeid}")]
        [HttpGet]
        public ServiceResponse GetObjectiveTabAdminSettings(string LogoSettings, int typeid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetObjectiveTabAdminSettings(LogoSettings, typeid);
            }
            catch
            {
                return null;
            }
            return result;
        }
        [Route("api/Common/GetAllAttributeGroup/")]
        [HttpGet]
        public ServiceResponse GetAllAttributeGroup()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetAllAttributeGroup();
            }
            catch
            {
                return null;
            }
            return result;
        }

        [Route("api/Common/GetAttrGroupTooltipValues/{GroupID}")]
        [HttpGet]
        public ServiceResponse GetAttrGroupTooltipValues(int GroupID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetAttrGroupTooltipValues(GroupID);
            }
            catch
            {
                return null;
            }
            return result;
        }

        [Route("api/Common/AdminSettingsObjectivetabRootLevelInsertUpdate")]
        [HttpPost]
        public ServiceResponse AdminSettingsObjectivetabRootLevelInsertUpdate([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JObject job = JObject.Parse(Convert.ToString(jobj));
                string jsondata = Convert.ToString(jobj["View"]);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.AdminSettingsObjectivetabRootLevelInsertUpdate(jsondata, (string)jobj["Key"], (int)jobj["TypeId"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/UpdateAttributeGroupTooltip")]
        [HttpPost]
        public ServiceResponse UpdateAttributeGroupTooltip([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JObject job = JObject.Parse(Convert.ToString(jobj));
                string jsondata = Convert.ToString(jobj["View"]);
                int GroupID = Convert.ToInt32(jobj["GroupID"]);
                int AttributeID = Convert.ToInt32(jobj["AttributeID"]);
                bool ToolTip = Convert.ToBoolean(Convert.ToInt32(jobj["ToolTip"]));

                JArray attributedetails = (JArray)jobj["attributedetails"];
                IList<object> attrDeatils = new List<object>();

                if (attributedetails != null)
                {
                    foreach (var item in attributedetails)
                    {
                        attrDeatils.Add(new
                        {
                            ID = (int)item["ID"]
                            //IsToolTip = (int)item["IsToolTip"]
                        });
                    }
                }
                else
                {
                    attrDeatils = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdateAttributeGroupTooltip(GroupID, AttributeID, ToolTip, attrDeatils);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }



        //Financial feature
        [Route("api/Common/GetFinancialViews")]
        [HttpGet]
        public ServiceResponse GetFinancialViews()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.CommonManager.GetFinancialViews();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        //Financial feature
        [Route("api/Common/GetFinancialViewColumns/{viewId}")]
        [HttpGet]
        public ServiceResponse GetFinancialViewColumns(int viewId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.CommonManager.GetFinancialViewColumns(viewId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/InsertFinacialColumns")]
        [HttpPost]
        public ServiceResponse InsertFinacialColumns([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.InsertFinacialColumns(jobj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/UpdateFinancialViews")]
        [HttpPost]
        public ServiceResponse UpdateFinancialViews([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.UpdateFinancialViews(jobj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/DeleteFinancialViews")]
        [HttpPost]
        public ServiceResponse DeleteFinancialViews([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.DeleteFinancialViews(jobj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/EditFinancialView/{viewid}")]
        [HttpGet]
        public ServiceResponse EditFinancialView(int viewid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.EditFinancialView(viewid);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Common/GetListFinancialView")]
        [HttpGet]
        public ServiceResponse GetListFinancialView()
        {
            result = new ServiceResponse();
            try
            {
               
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(GetCookie(Request, "session")));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetListFinancialView();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Common/GetUserRegAdminSettings/{LogoSettings}/{typeid}")]
        [HttpGet]
        public ServiceResponse GetUserRegAdminSettings(string LogoSettings, int typeid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetUserRegAdminSettings(LogoSettings, typeid);
            }
            catch
            {
                return null;
            }
            return result;
        }
        [Route("api/Common/GetUserRegLogoSettings/{TitleSettings}")]
        [HttpGet]
        public ServiceResponse GetUserRegLogoSettings(string TitleSettings)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetUserRegLogoSettings(TitleSettings);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Common/UpdateUserRegSettings")]
        [HttpPost]
        public ServiceResponse UpdateUserRegSettings([FromBody]JObject Titlelogojobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                var jobj = (JObject)Titlelogojobj["UserRegSettings"];
                var ImageChanged = (JObject)Titlelogojobj["IsImageChanged"];
                var isupdated = marcomManager.CommonManager.UpdateUserRegSettings((int)jobj["titleID"], (JObject)jobj["titleBlockData"], ImageChanged);
                if (isupdated)
                {
                    var amdinsettingsjobj = (JObject)Titlelogojobj["AdminSettings"];
                    string jsondata = Convert.ToString(amdinsettingsjobj);
                    result.StatusCode = (int)HttpStatusCode.OK;
                    result.Response = marcomManager.CommonManager.AdminSettingsUserRegInsertUpdate(jsondata, (string)amdinsettingsjobj["Key"], (string)amdinsettingsjobj["LogoSettings"]["logo"], ImageChanged);
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }



        /// <summary>
        /// Retrieves an individual cookie from the cookies collection
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cookieName"></param>
        /// <returns></returns>
        public string GetCookie(HttpRequestMessage request, string cookieName)
        {
            CookieHeaderValue cookie = request.Headers.GetCookies(cookieName).FirstOrDefault();
            if (cookie != null)
                return cookie[cookieName].Value;

            return null;
        }
    }
}