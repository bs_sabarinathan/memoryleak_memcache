﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Access.Model
{

    /// <summary>
    /// EntityRoleUserDao object for table 'AM_Entity_Role_User'.
    /// </summary>
    public partial class TaskMemberDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _taskid;
        protected int _roleid;
        protected int _userid;
        protected int _approvalrount;
        protected Boolean _approvalstatus;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

      

        #region Constructors
        public TaskMemberDao() { }

        public TaskMemberDao(int pId, int pTaskID, int pRoleid, int pUserid, int pApprovalRount,Boolean pApprovalStatus)
        {
            this._id = pId;
            this._taskid = pTaskID;
            this._roleid = pRoleid;
            this._userid = pUserid;
            this._approvalrount = pApprovalRount;
            this._approvalstatus = pApprovalStatus;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int TaskID
        {
            get { return _taskid; }
            set { _bIsChanged |= (_taskid != value); _taskid = value; }

        }

        public virtual int Roleid
        {
            get { return _roleid; }
            set { _bIsChanged |= (_roleid != value); _roleid = value; }

        }

        public virtual int Userid
        {
            get { return _userid; }
            set { _bIsChanged |= (_userid != value); _userid = value; }

        }

        public virtual int ApprovalRount
        {
            get { return _approvalrount; }
            set { _bIsChanged |= (_approvalrount != value); _approvalrount = value; }

        }

        public virtual Boolean ApprovalStatus
        {
            get { return _approvalstatus; }
            set { _bIsChanged |= (_approvalstatus != value); _approvalstatus = value; }

        }

     
        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string Id = "Id";
            public const string TaskMemberDao = "TaskMemberDao";
            public const string TaskID = "TaskID";
            public const string Roleid = "Roleid";
            public const string Userid = "Userid";
            public const string ApprovalRount = "ApprovalRount";
            public const string ApprovalStatus = "ApprovalStatus";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string Id = "Id";
            public const string TaskMemberDao = "TaskMemberDao";
            public const string TaskID = "TaskID";
            public const string Roleid = "Roleid";
            public const string Userid = "Userid";
            public const string ApprovalRount = "ApprovalRount";
            public const string ApprovalStatus = "ApprovalStatus";
        }

        #endregion


    }

}
