﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
    public partial class AssetTypeFileExtensionRelationDao : BaseDao, ICloneable
    {
          #region "Member variables"

        protected int _id;
        protected int _assettypeid;
        protected string _extension;
        protected int _sortorder;
        protected string _assetcategory;

          #endregion

        #region "Constructor"

        public AssetTypeFileExtensionRelationDao() { }


        public AssetTypeFileExtensionRelationDao(int pid, int passettypeid, string pextension, int psortorder, string passetcategory) 
        {
            this._id = pid;
            this._assettypeid = passettypeid;
            this._extension = pextension;
            this._sortorder = psortorder;
            this._assetcategory = passetcategory;
        }

        #endregion


         #region "Public propertiers"

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int AssetTypeID
        {
            get { return _assettypeid; }
            set { _assettypeid = value; }

        }

        public virtual string Extension
        {
            get { return _extension; }
            set { _extension = value; }

        }

        public virtual int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }

        }

        public virtual string AssetCategory
        {
            get { return _assetcategory; }
            set { _assetcategory = value; }

        }

        #endregion


        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion


        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AssetTypeFileExtensionRelationDao = "AssetTypeFileExtensionRelationDao";
            public const string ID = "ID";
            public const string AssetTypeID = "AssetTypeID";
            public const string Extension = "Extension";
            public const string SortOrder = "SortOrder";
            public const string AssetCategory = "AssetCategory";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AssetTypeFileExtensionRelationDao = "AssetTypeFileExtensionRelationDao";
            public const string ID = "ID";
            public const string AssetTypeID = "AssetTypeID";
            public const string Extension = "Extension";
            public const string SortOrder = "SortOrder";
            public const string AssetCategory = "AssetCategory";
        }

        #endregion
    }
}
