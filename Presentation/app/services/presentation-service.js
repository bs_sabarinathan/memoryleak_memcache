﻿(function(ng,app){"use strict";function PresentationService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetEntitydescendants:GetEntitydescendants,GetPresentationById:GetPresentationById,InsertPresentation:InsertPresentation});function GetEntitydescendants(AttributeID){var request=$http({method:"get",url:"api/Planning/GetEntitydescendants/"+AttributeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetPresentationById(ID){var request=$http({method:"get",url:"api/Planning/GetPresentationById/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function InsertPresentation(formobj){var request=$http({method:"post",url:"api/Planning/InsertPresentation/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("PresentationService",['$http','$q',PresentationService]);})(angular,app);