﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using Marcom.API.Services;
using Marcom.API.Services.Versioning;

namespace Marcom.API
{
    public static class WebApiConfig
    {
 
        public static void Register(HttpConfiguration config)
        {
            config.Formatters.Clear();
            config.Formatters.Add(new XmlMediaTypeFormatter());
            config.Formatters.Add(new JsonMediaTypeFormatter());
            config.Formatters.Add(new FormUrlEncodedMediaTypeFormatter());

          
            config.Routes.MapHttpRoute(
                name: "GetApi",
                routeTemplate: "{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
              name: "GetApi1",
              routeTemplate: "{controller}/{action}/"
           );

            config.Routes.MapHttpRoute(
               name: "LockEntityAPI",
               routeTemplate: "{controller}/DuplicateEntities/{id}/{lockstatus}",
               defaults: new { id = RouteParameter.Optional }
          );

             config.Routes.MapHttpRoute(
                 name: "GetUserAccess",
                 routeTemplate: "{controller}/GetUserInfo/{userid}/{entityid}",
                 defaults: new { userid = RouteParameter.Optional, entityid = RouteParameter.Optional }
              );

             config.Routes.MapHttpRoute(
                  name: "GetUserInvolvedEntities",
                  routeTemplate: "{controller}/GetUserInvolvedEntities/{UniqueKey}/{Userid}",
                  defaults: new { userid = RouteParameter.Optional, entityid = RouteParameter.Optional }
               );

             config.Routes.MapHttpRoute(
               name: "GetMyTask",
               routeTemplate: "{controller}/GetMyTask/{FilterByentityID}/{StartRowno}/{MaxRowNo}/{AssignRole}/{UserId}"
                 );

             config.Routes.MapHttpRoute(
                name: "GetEntitiesList",
                routeTemplate: "{controller}/GetEntitiesList/{ID}/{ParentID}/{TypeID}"
                  );

             config.Routes.MapHttpRoute(
                 name: "GetListOfEntityTypeAttributeGroupValues",
                 routeTemplate: "{controller}/GetListOfEntityTypeAttributeGroupValues/{EntityTypeID}/{AttrGrpId}",
                 defaults: new { EntityTypeID = RouteParameter.Optional, AttrGrpId = RouteParameter.Optional }
                   );

             config.Routes.MapHttpRoute(
                 name: "GetEntityTypeAttributeDetailsList",
                 routeTemplate: "{controller}/GetEntityTypeAttributeDetailsList/{EntityTypeID}/{ParentId}",
                 defaults: new { EntityTypeID = RouteParameter.Optional, ParentId = RouteParameter.Optional }
                   );

             config.Routes.MapHttpRoute(
                name: "GetEntityList",
                routeTemplate: "{controller}/GetEntityList/{EntityTypeId}",
                defaults: new { EntityTypeId = RouteParameter.Optional }
                  );

             config.Routes.MapHttpRoute(
                 name: "UpdateObjectiveList",
                 routeTemplate: "{controller}/UpdateObjectiveList/{EntityId}/{ObjectiveID}/{PlannedTrgt}/{TrgtOutcome}",
                 defaults: new { EntityTypeId = RouteParameter.Optional }
                   );


            config.Filters.Add(new ExceptionHandlingAttribute());

            config.Services.Replace(typeof(IHttpControllerSelector),  new ApiVersioningSelector(config));

             
        }
    }
}
