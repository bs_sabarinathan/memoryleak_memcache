﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class TagOption : ITagOption
    {
        #region Member Variables

		protected int _id;
		protected string _caption;
        protected string _text;
		protected int _attributeid;
        protected int _userid;
        protected string _username;
        protected string _createdon;
        protected int _sortorder;
        protected string _usedon;
        protected int _totalhits;
        protected int _populartagstoshow;
		
		#endregion
		
		#region Constructors
		public TagOption() {}

        public TagOption(int pId, string pCaption, int pAttributeid, int pUserid, int pSortorder, string pUsername, string pCreateon, string pUsedon, int pTotalHits, int pPopularTagsToShow)
		{
			this._id = pId; 
			this._caption = pCaption; 
			this._attributeid = pAttributeid;
            this._userid = pUserid;
            this._sortorder = pSortorder;
            this._username = pUsername;
            this._createdon = pCreateon;
            this._usedon = pUsedon;
            this._totalhits = pTotalHits;
            this._populartagstoshow = pPopularTagsToShow;
		}

        public TagOption(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 1000)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 100 characters");
			  _caption = value; 
			}
		}

        public string text
        {
            get { return _text; }
            set
            {
                if (value != null && value.Length > 1000)
                    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 100 characters");
                _text = value;
            }

        }

        public virtual int AttributeID
        {
            get { return _attributeid; }
            set { _attributeid = value; }

        }

        public virtual int UserID
        {
            get { return _userid; }
            set { _userid = value; }

        }

        public string UserName
        {
            get { return _username; }
            set
            {
                if (value != null && value.Length > 1000)
                    throw new ArgumentOutOfRangeException("User Name", "User Name value, cannot contain more than 1000 characters");
                _username = value;
            }

        }

        public string CreatedOn
        {
            get { return _createdon; }
            set
            {
                if (value != null && value.Length > 10)
                    throw new ArgumentOutOfRangeException("CreatedOn", "CreatedOn value, cannot contain more than 10 characters");
                _createdon = value;
            }

        }

        public string UsedOn
        {
            get { return _usedon; }
            set
            {
                if (value != null && value.Length > 10)
                    throw new ArgumentOutOfRangeException("UsedOn", "UsedOn value, cannot contain more than 10 characters");
                _usedon = value;
            }

        }

        public int TotalHits
        {
            get { return _totalhits; }
            set { _totalhits = value; }
        }

        public int PopularTagsToShow
        {
            get { return _populartagstoshow; }
            set { _populartagstoshow = value; }
        }

        public virtual int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }

        }
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            TagOption castObj = null;
			try
			{
                castObj = (TagOption)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
    }
}
