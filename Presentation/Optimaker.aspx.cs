﻿using BrandSystems.Marcom.Core.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Presentation
{
    public partial class Optimaker : System.Web.UI.Page
    {

        public string jpegFilename;
        public string jpegSize;
        public string jpegVersion;
        public string jpegDesc;
        public string jpegGuid;
        public string highResGUID;
        public string lowResGUID;

        protected void Page_Load(object sender, EventArgs e)
        {


            string LogFile = HttpContext.Current.Server.MapPath("~/Logs/Log_optimaker.txt");
            using (FileStream fs = new FileStream(LogFile, FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.WriteLine("Reached Page Load" + e);
                sw.WriteLine("----------------");
            }
            GetPostVariables();
        }

        public void GetPostVariables()
        {
            try
            {
                string docversionid = string.Empty;
                string productionname = string.Empty;
                string entityid = string.Empty;
                string folderid = string.Empty;
                string createdid = string.Empty;
                string DocID = string.Empty;
                string TenantID = string.Empty;

                if (!string.IsNullOrEmpty(Request.Form["docversionid"]))
                {
                    docversionid = Request.Form["docversionid"];
                }
                if (!string.IsNullOrEmpty(Request.Form["productionname"]))
                {
                    productionname = Request.Form["productionname"];
                }
                if (!string.IsNullOrEmpty(Request.Form["marcomid"]))
                {
                    string[] mergeddata = Request.Form["marcomid"].ToString().Split('|');
                    entityid = mergeddata[0];
                    DocID = mergeddata[1];
                    TenantID = mergeddata[2];
                }
                if (!string.IsNullOrEmpty(Request.Form["folderid"]))
                {
                    folderid = Request.Form["folderid"];
                }
                if (!string.IsNullOrEmpty(Request.Form["createdid"]))
                {
                    createdid = Request.Form["createdid"];
                }
                
                /*
                docversionid = "1";
                productionname = "demoprod";
                entityid = "3";
                folderid = "0";
                createdid = "14849";
                */
                //GetOptimakerProductionFiles("88466", "A4 for demo");

                GetOptimakerProductionFiles(docversionid, productionname, int.Parse(TenantID));
                CreateMediaGeneratorTemplateAsset(docversionid, DocID, productionname, TenantID, entityid, folderid, createdid, "33");
            }
            catch (Exception e)
            {
                string LogFile = HttpContext.Current.Server.MapPath("~/Logs/Log_optimaker.txt");
                using (FileStream fs = new FileStream(LogFile, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine("EXCEPTION IN GetPostVariables" + e);
                    sw.WriteLine("----------------");
                }
            }
        }



        public void CreateMediaGeneratorTemplateAsset(string DocversionID, string DocID, string Assetname, string TenantID, string EntityID = "0", string FolderID = "0", string createdby = "0", string assettypeid = "33")
        {
            try
            {
                //Create input Json object
                string mediageneurl;
                mediageneurl = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.LastIndexOf("/")) + "/api/Dam/CreateMediaGeneratorAsset";
                StringBuilder jsonmbdata = new StringBuilder();
                //jsonmbdata.Append("{ \"DocversionID\": \"" + DocversionID + "\", \"AssetName\": \"" + Assetname + "\", \"EntityID\": \"" + EntityID + "\", \"FolderID\": \"" + FolderID + "\", \"CreatedBy\": \"" + createdby + "\", \"AssetTypeID\": \"" + assettypeid + "\", \"jpegFilename\": \"" + jpegFilename + "\", \"jpegsize\": \"" + jpegSize + "\", \"jpegVersion\": \"" + jpegVersion + "\", \"jpegDesc\": \"" + jpegDesc + "\", \"jpegGuid\": \"" + jpegGuid + "\", \"highResGUID\": \"" + highResGUID + "\", \"lowResGUID\": \"" + lowResGUID + "\", \"DocID\": \"" + DocID + "\",}");
                jsonmbdata.Append("{ \"DocversionID\": \"" + DocversionID + "\", \"AssetName\": \"" + Assetname + "\", \"EntityID\": \"" + EntityID + "\", \"FolderID\": \"" + FolderID + "\", \"CreatedBy\": \"" + createdby + "\", \"AssetTypeID\": \"" + assettypeid + "\", \"jpegFilename\": \"" + jpegFilename + "\", \"jpegsize\": \"" + jpegSize + "\", \"jpegVersion\": \"" + jpegVersion + "\", \"jpegDesc\": \"" + jpegDesc + "\", \"jpegGuid\": \"" + jpegGuid + "\", \"highResGUID\": \"" + highResGUID + "\", \"lowResGUID\": \"" + lowResGUID + "\", \"TenantID\": \"" + TenantID + "\", \"DocID\": \"" + DocID + "\",}");

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(mediageneurl);
                request.ContentType = "application/json;charset=\"utf-8\"";
                request.Accept = "application/json";
                request.Method = "POST";


                string LogFile = HttpContext.Current.Server.MapPath("~/Logs/Log_optimaker.txt");
                using (FileStream fs = new FileStream(LogFile, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine("URL" + mediageneurl);
                    sw.WriteLine("URL params" + jsonmbdata.ToString());
                    sw.WriteLine("----------------");
                }

                using (Stream stm = request.GetRequestStream())
                {
                    using (StreamWriter stmw = new StreamWriter(stm))
                    {
                        stmw.Write(jsonmbdata.ToString());
                    }
                }

                WebResponse response = request.GetResponse();

                //READ THE RESPONSE
                Response.Write(((HttpWebResponse)response).StatusDescription);


            }
            catch (WebException webex)
            {
                string LogFile = HttpContext.Current.Server.MapPath("~/Logs/Log_optimaker.txt");
                using (FileStream fs = new FileStream(LogFile, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine("EXCEPTION IN CALLING SERVICE" + webex);
                    sw.WriteLine("----------------");
                }


                WebResponse errResp = webex.Response;
                using (Stream respStream = errResp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(respStream);
                    string text = reader.ReadToEnd();
                }
                throw webex;
            }
            catch (Exception ex)
            {
                string LogFile = HttpContext.Current.Server.MapPath("~/Logs/Log_optimaker.txt");
                using (FileStream fs = new FileStream(LogFile, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine("EXCEPTION IN CreateMediaGeneratorTemplateAsset" + ex);
                    sw.WriteLine("----------------");
                }

            }
        }


        public void GetOptimakerProductionFiles(string DocVersionID, string productionname, int TenantID)
        {
            try
            {
                //string HighresPDF = productionname + "_" + DocVersionID + "_" + Guid.NewGuid();
                //string LowresPDF = productionname + "_" + DocVersionID + "_" + Guid.NewGuid();
                
                TenantSelection tfp = new TenantSelection();
                string TenantFilePath = tfp.GetTenantFilePath(TenantID);

                string HighresPDF = Guid.NewGuid().ToString();
                string LowresPDF = Guid.NewGuid().ToString();
                string prodjpeg = Guid.NewGuid().ToString();

                string DAMHighResPDFFilePath = Path.Combine(HttpContext.Current.Server.MapPath(TenantFilePath + "/DAMFiles/Templates/MediaGenerator/HighResPdf/"), HighresPDF + ".pdf");
                string DAMLowResPDFFilePath = Path.Combine(HttpContext.Current.Server.MapPath(TenantFilePath + "/DAMFiles/Templates/MediaGenerator/LowResPdf/"), productionname + " low res.pdf");
                string DAMJpeg = Path.Combine(HttpContext.Current.Server.MapPath(TenantFilePath + "/DAMFiles/Original/"), prodjpeg + ".jpg");


                string LogFile = HttpContext.Current.Server.MapPath("~/Logs/Log_optimaker.txt");
                using (FileStream fs = new FileStream(LogFile, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine("Tenant Path" + TenantFilePath);
                    sw.WriteLine("Tenant ID" + TenantID.ToString());
                    sw.WriteLine("jpeg path :" + DAMJpeg);
                    sw.WriteLine("----------------");
                }


                //WebClient client = new WebClient();

                string optimakerfilelink = System.Configuration.ConfigurationManager.AppSettings["OptimakerFilesURL"].ToString();
                optimakerfilelink = optimakerfilelink.Replace("[docversionid]", DocVersionID);
               
                //optimakerfilelink = optimakerfilelink.Replace("[filetype]", "25");
                //client.DownloadFile(optimakerfilelink, DAMHighResPDFFilePath);

                //optimakerfilelink = optimakerfilelink.Replace("filetype=25", "filetype=26");
                //client.DownloadFile(optimakerfilelink, DAMLowResPDFFilePath);

                //optimakerfilelink = optimakerfilelink.Replace("filetype=26", "filetype=24");
                //client.DownloadFile(optimakerfilelink, DAMJpeg);


                WebClient clientHighRes = new WebClient();
                optimakerfilelink = optimakerfilelink.Replace("[filetype]", "25");
                clientHighRes.DownloadFileCompleted += new AsyncCompletedEventHandler(HighResCompleted);
                clientHighRes.DownloadFileAsync(new Uri(optimakerfilelink), DAMHighResPDFFilePath);

                WebClient clientLowRes = new WebClient();
                optimakerfilelink = optimakerfilelink.Replace("filetype=25", "filetype=26");
                clientLowRes.DownloadFileCompleted += new AsyncCompletedEventHandler(LowResCompleted);
                clientLowRes.DownloadFileAsync(new Uri(optimakerfilelink), DAMLowResPDFFilePath);

                System.Threading.Thread.Sleep(3000);

                WebClient clientjpeg = new WebClient();
                optimakerfilelink = optimakerfilelink.Replace("filetype=26", "filetype=24");
                //clientjpeg.DownloadFile(optimakerfilelink, DAMJpeg);
                clientjpeg.DownloadFileCompleted += new AsyncCompletedEventHandler(JpegCompleted);
                clientjpeg.DownloadFile(new Uri(optimakerfilelink), DAMJpeg);


                FileInfo jpgfiledetails = new FileInfo(DAMJpeg);
                jpegFilename = Path.GetFileName(DAMJpeg);
                jpegSize = jpgfiledetails.Length.ToString();
                jpegVersion = "1";
                jpegDesc = productionname;
                jpegGuid = prodjpeg;
                highResGUID = HighresPDF + ".pdf";
                lowResGUID = productionname + " low res.pdf";
            }
            catch (Exception e)
            {
                string LogFile = HttpContext.Current.Server.MapPath("~/Logs/Log_optimaker.txt");
                using (FileStream fs = new FileStream(LogFile, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine("EXCEPTION IN GetOptimakerProductionFiles" + e);
                   
                    sw.WriteLine("----------------");
                }
            }

        }

        private void JpegCompleted(object sender, AsyncCompletedEventArgs e)
        {
            //string destDAMJpeg =DAMJpeg.Replace("/temp/", "/Original/");
            //File.Copy(DAMJpeg, destDAMJpeg);
            string LogFile = HttpContext.Current.Server.MapPath("~/Logs/Log_optimaker.txt");
            using (FileStream fs = new FileStream(LogFile, FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.WriteLine(" -- Jpeg Download completed -- ");

            }
        }

        private void LowResCompleted(object sender, AsyncCompletedEventArgs e)
        {
            string LogFile = HttpContext.Current.Server.MapPath("~/Logs/Log_optimaker.txt");
            using (FileStream fs = new FileStream(LogFile, FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.WriteLine(" -- LowRes PDF Download completed -- ");

            }
        }

        private void HighResCompleted(object sender, AsyncCompletedEventArgs e)
        {
            string LogFile = HttpContext.Current.Server.MapPath("~/Logs/Log_optimaker.txt");
            using (FileStream fs = new FileStream(LogFile, FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.WriteLine(" -- HighRes PDF Download completed -- ");

            }
        }

    }
}