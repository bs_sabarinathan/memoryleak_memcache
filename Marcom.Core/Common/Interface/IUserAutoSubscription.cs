using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IUserAutoSubscription interface for table 'CM_UserAutoSubscription'.
    /// </summary>
    public interface IUserAutoSubscription
    {
        #region Public Properties

        int Userid
        {
            get;
            set;

        }

        int Entityid
        {
            get;
            set;

        }

        ISubscriptionType Subscriptionid
        {
            get;
            set;

        }

        int EntityTypeid
        {
            get;
            set;

        }

        bool IsValid
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
