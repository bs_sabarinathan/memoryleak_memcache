using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// MilestoneDao object for table 'PM_Milestone'.
	/// </summary>
	public partial class MilestoneDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected int _entityid;
		protected string _name;
		protected string _description;
		protected int _status;
		protected DateTimeOffset _duedate;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public MilestoneDao() {}
			
		public MilestoneDao(int pEntityid, string pName, string pDescription, int pStatus, DateTimeOffset pDueDate)
		{
			this._entityid = pEntityid; 
			this._name = pName; 
			this._description = pDescription; 
			this._status = pStatus; 
			this._duedate = pDueDate; 
		}
				
		public MilestoneDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual int Entityid
		{
			get { return _entityid; }
			set { _bIsChanged |= (_entityid != value); _entityid = value; }
			
		}		

		public virtual string Name
		{
			get { return _name; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 50 characters");
			  _bIsChanged |= (_name != value); 
			  _name = value; 
			}
			
		}
		
		public virtual string Description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
			  _bIsChanged |= (_description != value); 
			  _description = value; 
			}
			
		}
		
		public virtual int Status
		{
			get { return _status; }
			set { _bIsChanged |= (_status != value); _status = value; }
			
		}
		
		public virtual DateTimeOffset DueDate
		{
			get { return _duedate; }
			set { _bIsChanged |= (_duedate != value); _duedate = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string MilestoneDao = "MilestoneDao";			
			public const string Id = "Id";			
			public const string Entityid = "Entityid";			
			public const string Name = "Name";			
			public const string Description = "Description";			
			public const string Status = "Status";			
			public const string DueDate = "DueDate";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string MilestoneDao = "MilestoneDao";			
			public const string Id = "Id";			
			public const string Entityid = "Entityid";			
			public const string Name = "Name";			
			public const string Description = "Description";			
			public const string Status = "Status";			
			public const string DueDate = "DueDate";			
		}

		#endregion
		
		
	}
	
}
