﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Report.Model
{
    public partial class ReportDao : BaseDao, ICloneable 
    {
        #region Member Variables
        protected int _id;
        protected int _oid;
        protected int _CategoryId;
        protected string _name;
        protected string _caption;
        protected string _description;
        protected string _Preview;
        protected bool _show;
        protected bool _entityLevel;
        protected bool _subLevel;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
		public ReportDao() {}

        public ReportDao(int Poid, string Pname, string pcaption, string pdescription, string pPreview, bool pshow, int pCategoryId, bool pentityLevel, bool psubLevel)
		{
            this._oid = Poid;
            this._name = Pname;
            this._caption = pcaption;
            this._description = pdescription;
            this._Preview = pPreview;
            this._show = pshow;
            this._CategoryId = pCategoryId;
            this._entityLevel = pentityLevel;
            this._subLevel = psubLevel;
		}


        public ReportDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int OID
        {
            get { return _oid; }
            set { _bIsChanged |= (_oid != value); _oid = value; }

        }

        public virtual int CategoryId
        {
            get { return _CategoryId; }
            set { _bIsChanged |= (_CategoryId != value); _CategoryId = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("Name", "Reporturl value, cannot contain more than 100 characters");
                _bIsChanged |= (_name != value);
                _name = value;
            }

        }
        public virtual string Caption
        {
            get { return _caption; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 100 characters");
                _bIsChanged |= (_caption != value);
                _caption = value;
            }

        }
        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 200)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 200 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }
        public virtual string Preview
        {
            get { return _Preview; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("Preview", "Preview value, cannot contain more than 100 characters");
                _bIsChanged |= (_Preview != value);
                _Preview = value;
            }

        }
        public virtual bool Show
        {
            get { return _show; }
            set { _show = value; }

        }

        public virtual bool EntityLevel
        {
            get { return _entityLevel; }
            set { _entityLevel = value; }

        }

        public virtual bool SubLevel
        {
            get { return _subLevel; }
            set { _subLevel = value; }

        }

        #endregion 				

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string ReportDao = "ReportDao";
            public const string Id = "Id";
            public const string OID = "OID";
            public const string Name = "Name";
            public const string Caption = "Caption";
            public const string Description = "Description";
            public const string Preview = "Preview";
            public const string Show = "Show";
            public const string CategoryId = "CategoryId";
            public const string EntityLevel = "EntityLevel";
            public const string SubLevel = "SubLevel";
            
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string ReportDao = "ReportDao";
            public const string Id = "Id";
            public const string OID = "OID";
            public const string Name = "Name";
            public const string Caption = "Caption";
            public const string Description = "Description";
            public const string Preview = "Preview";
            public const string Show = "Show";
            public const string CategoryId = "CategoryId";
            public const string EntityLevel = "EntityLevel";
            public const string SubLevel = "SubLevel";
        }

        #endregion
		
    }
}
