﻿(function(ng,app){"use strict";function MemberService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetUsers:GetUsers,GetEntityTypeRoleAccess:GetEntityTypeRoleAccess,GetMember:GetMember,GetEntityDetailsByID:GetEntityDetailsByID,PostMember:PostMember,UpdateMemberInEntity:UpdateMemberInEntity,Member:Member,PutMember:PutMember});function GetUsers(){var request=$http({method:"get",url:"api/user/GetUsers/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeRoleAccess(EntityTypeID){var request=$http({method:"get",url:"api/access/GetEntityTypeRoleAccess/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetMember(EntityID){var request=$http({method:"get",url:"api/Planning/GetMember/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityDetailsByID(EntityID){var request=$http({method:"get",url:"api/Planning/GetEntityDetailsByID/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function PostMember(dataobj){var request=$http({method:"post",url:"api/Planning/PostMember/",params:{action:"add"},data:dataobj});return(request.then(handleSuccess,handleError));}
function UpdateMemberInEntity(dataobj){var request=$http({method:"put",url:"api/Planning/UpdateMemberInEntity/",params:{action:"update"},data:dataobj});return(request.then(handleSuccess,handleError));}
function Member(ID){var request=$http({method:"delete",url:"api/Planning/Member/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function PutMember(dataobj){var request=$http({method:"put",url:"api/Planning/PutMember/",params:{action:"update"},data:formobj});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("MemberService",['$http','$q',MemberService]);})(angular,app);