﻿using System;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Dal.Planning.Model
{
   internal class EntityOwners : IEntityOwners
    {
      public int EntityId { get; set; }
      public int RoleId { get; set; }
      public int UserId { get; set; }
      public string UserName { get; set; }
      public string UserEmail { get; set; }
    }
}
