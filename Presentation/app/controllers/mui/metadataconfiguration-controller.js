﻿(function (ng, app) {
    "use strict";
    //app.controller(
    //	"mui.metadataconfigurationCtrl",
    function muimetadataconfigurationCtrl($scope) {

        //---------------------------  Get the initial feed ------------------------------------
        // --- Define Scope Variables. ---------------------- //

        // --- Bind To Scope Events. ------------------------ //

        // I handle changes to the request context.

        // --- Initialize. ---------------------------------- //

        //-------------- user defined functions ---------

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.metadataconfigurationCtrl']"));
        });
    }
    //);
    app.controller("mui.metadataconfigurationCtrl", ['$scope', muimetadataconfigurationCtrl]);
})(angular, app);