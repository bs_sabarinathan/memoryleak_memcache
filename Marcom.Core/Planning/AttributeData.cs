﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Metadata;

namespace BrandSystems.Marcom.Core.Planning
{
   public class AttributeData : IAttributeData
    { 
        public int ID { get; set; }
        public int TypeID { get; set; }
        public dynamic Lable { get; set; }
        public dynamic Caption { get; set; }
        public dynamic Value { get; set; }
        public string SpecialValue { get; set; }
        public int Level { get; set; }
        public bool IsSpecial { get; set; }
        public bool IsInheritFromParent { get; set; }
        public bool IsChooseFromParent { get; set; }
        public bool IsReadOnly { get; set; }
        public int AttributeRecordID { get; set; }
        public bool IsLock { get; set; }
        public int SortOrder { get; set; }
        public string specialValue { get; set; }
        public dynamic tree { get; set; }
        public dynamic options { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public bool IsSearchable { get; set; }
        public bool ShowAsColumn { get; set; }
        public bool AttributeGroupTooltip { get; set; }
        public IList<DropDownTreePricing> DropDownPricing { get; set; }
       
    }

    public class TreeDropDownLabel : ITreeDropDownLabel
    {
        public int Level { get; set; }
        public string Label { get; set; }
    }

    public class TreeDropDownCaption : ITreeDropDownCaption
    {
        public int Level { get; set; }
        public string Caption { get; set; }
    }

    public class ValdiationWithAttributeRelationData : IValdiationWithAttributeRelationData
    {
        public bool InheritFromParent { get; set; }
        public bool ChooseFromParentOnly { get; set; }
        public bool IsReadOnly { get; set; }

        public int AttributeID { get; set; }
        public int EntityTypeID { get; set; }
        public int RelationShipID { get; set; }
        public string Name { get; set; }
        public string ValueType { get; set; }
        public string Value { get; set; }
        public string ErrorMessage { get; set; }
    }


}
