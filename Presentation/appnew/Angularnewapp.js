///#source 1 1 /appnew/bower_components/angular/angular.min.js
(function(n,t,i){"use strict";function v(n,t){return t=t||Error,function(){var u=arguments[0],e="["+(n?n+":":"")+u+"] ",o=arguments[1],f=arguments,i,r;for(i=e+o.replace(/\{\d+\}/g,function(n){var t=+n.slice(1,-1);return t+2<f.length?co(f[t+2]):n}),i=i+"\nhttp://errors.angularjs.org/1.3.11/"+(n?n+"/":"")+u,r=2;r<arguments.length;r++)i=i+(r==2?"?":"&")+"p"+(r-2)+"="+encodeURIComponent(co(arguments[r]));return new t(i)}}function di(n){if(n==null||gi(n))return!1;var t=n.length;return n.nodeType===vt&&t?!0:c(n)||o(n)||t===0||typeof t=="number"&&t>0&&t-1 in n}function r(n,t,i){var u,f,e;if(n)if(l(n))for(u in n)u!="prototype"&&u!="length"&&u!="name"&&(!n.hasOwnProperty||n.hasOwnProperty(u))&&t.call(i,n[u],u,n);else if(o(n)||di(n))for(e=typeof n!="object",u=0,f=n.length;u<f;u++)(e||u in n)&&t.call(i,n[u],u,n);else if(n.forEach&&n.forEach!==r)n.forEach(t,i,n);else for(u in n)n.hasOwnProperty(u)&&t.call(i,n[u],u,n);return n}function pe(n){return Object.keys(n).sort()}function nl(n,t,i){for(var r=pe(n),u=0;u<r.length;u++)t.call(i,n[r[u]],r[u]);return r}function we(n){return function(t,i){n(i,t)}}function br(){return++gc}function be(n,t){t?n.$$hashKey=t:delete n.$$hashKey}function a(n){for(var t,u,i,e,f,o=n.$$hashKey,r=1,s=arguments.length;r<s;r++)if(t=arguments[r],t)for(u=Object.keys(t),i=0,e=u.length;i<e;i++)f=u[i],n[f]=t[f];return be(n,o),n}function g(n){return parseInt(n,10)}function ke(n,t){return a(Object.create(n),t)}function s(){}function ct(n){return n}function nt(n){return function(){return n}}function e(n){return typeof n=="undefined"}function u(n){return typeof n!="undefined"}function h(n){return n!==null&&typeof n=="object"}function c(n){return typeof n=="string"}function k(n){return typeof n=="number"}function lt(n){return ni.call(n)==="[object Date]"}function l(n){return typeof n=="function"}function kr(n){return ni.call(n)==="[object RegExp]"}function gi(n){return n&&n.window===n}function nr(n){return n&&n.$evalAsync&&n.$watch}function tl(n){return ni.call(n)==="[object File]"}function il(n){return ni.call(n)==="[object FormData]"}function rl(n){return ni.call(n)==="[object Blob]"}function tr(n){return typeof n=="boolean"}function dr(n){return n&&l(n.then)}function de(n){return!!(n&&(n.nodeName||n.prop&&n.attr&&n.find))}function ul(n){for(var i={},r=n.split(","),t=0;t<r.length;t++)i[r[t]]=!0;return i}function pt(n){return y(n.nodeName||n[0]&&n[0].nodeName)}function ir(n,t){var i=n.indexOf(t);return i>=0&&n.splice(i,1),t}function ti(n,t,i,u){var l,c,f,e,a,s;if(gi(n)||nr(n))throw hi("cpws","Can't copy! Making copies of Window or Scope instances is not supported.");if(t){if(n===t)throw hi("cpi","Can't copy! Source and destination are identical.");if(i=i||[],u=u||[],h(n)){if(c=i.indexOf(n),c!==-1)return u[c];i.push(n);u.push(t)}if(o(n))for(t.length=0,e=0;e<n.length;e++)f=ti(n[e],null,i,u),h(n[e])&&(i.push(n[e]),u.push(f)),t.push(f);else{a=t.$$hashKey;o(t)?t.length=0:r(t,function(n,i){delete t[i]});for(s in n)n.hasOwnProperty(s)&&(f=ti(n[s],null,i,u),h(n[s])&&(i.push(n[s]),u.push(f)),t[s]=f);be(t,a)}}else t=n,n&&(o(n)?t=ti(n,[],i,u):lt(n)?t=new Date(n.getTime()):kr(n)?(t=new RegExp(n.source,n.toString().match(/[^\/]*$/)[0]),t.lastIndex=n.lastIndex):h(n)&&(l=Object.create(Object.getPrototypeOf(n)),t=ti(n,l,i,u)));return t}function at(n,t){var i,u,r;if(o(n))for(t=t||[],i=0,u=n.length;i<u;i++)t[i]=n[i];else if(h(n)){t=t||{};for(r in n)r.charAt(0)==="$"&&r.charAt(1)==="$"||(t[r]=n[r])}return t||n}function et(n,t){if(n===t)return!0;if(n===null||t===null)return!1;if(n!==n&&t!==t)return!0;var f=typeof n,s=typeof t,e,r,u;if(f==s&&f=="object")if(o(n)){if(!o(t))return!1;if((e=n.length)==t.length){for(r=0;r<e;r++)if(!et(n[r],t[r]))return!1;return!0}}else{if(lt(n))return lt(t)?et(n.getTime(),t.getTime()):!1;if(kr(n)&&kr(t))return n.toString()==t.toString();if(nr(n)||nr(t)||gi(n)||gi(t)||o(t))return!1;u={};for(r in n)if(r.charAt(0)!=="$"&&!l(n[r])){if(!et(n[r],t[r]))return!1;u[r]=!0}for(r in t)if(!u.hasOwnProperty(r)&&r.charAt(0)!=="$"&&t[r]!==i&&!l(t[r]))return!1;return!0}return!1}function rr(n,t,i){return n.concat(gu.call(t,i))}function tf(n,t){return gu.call(n,t||0)}function ge(n,t){var i=arguments.length>2?tf(arguments,2):[];return!l(t)||t instanceof RegExp?t:i.length?function(){return arguments.length?t.apply(n,rr(i,arguments,0)):t.apply(n,i)}:function(){return arguments.length?t.apply(n,arguments):t.call(n)}}function no(n,r){var u=r;return typeof n=="string"&&n.charAt(0)==="$"&&n.charAt(1)==="$"?u=i:gi(r)?u="$WINDOW":r&&t===r?u="$DOCUMENT":nr(r)&&(u="$SCOPE"),u}function ur(n,t){return typeof n=="undefined"?i:(k(t)||(t=t?2:null),JSON.stringify(n,no,t))}function to(n){return c(n)?JSON.parse(n):n}function wt(n){n=f(n).clone();try{n.empty()}catch(i){}var t=f("<div>").append(n).html();try{return n[0].nodeType===iu?y(t):t.match(/^(<[^>]+>)/)[1].replace(/^<([\w\-]+)/,function(n,t){return"<"+y(t)})}catch(i){return y(t)}}function io(n){try{return decodeURIComponent(n)}catch(t){}}function ro(n){var i={},f,t;return r((n||"").split("&"),function(n){if(n&&(f=n.replace(/\+/g,"%20").split("="),t=io(f[0]),u(t))){var r=u(f[1])?io(f[1]):!0;ye.call(i,t)?o(i[t])?i[t].push(r):i[t]=[i[t],r]:i[t]=r}}),i}function rf(n){var t=[];return r(n,function(n,i){o(n)?r(n,function(n){t.push(ii(i,!0)+(n===!0?"":"="+ii(n,!0)))}):t.push(ii(i,!0)+(n===!0?"":"="+ii(n,!0)))}),t.length?t.join("&"):""}function gr(n){return ii(n,!0).replace(/%26/gi,"&").replace(/%3D/gi,"=").replace(/%2B/gi,"+")}function ii(n,t){return encodeURIComponent(n).replace(/%40/gi,"@").replace(/%3A/gi,":").replace(/%24/g,"$").replace(/%2C/gi,",").replace(/%3B/gi,";").replace(/%20/g,t?"%20":"+")}function fl(n,t){var i,r,u=fr.length;for(n=f(n),r=0;r<u;++r)if(i=fr[r]+t,c(i=n.attr(i)))return i;return null}function el(n,t){var i,u,f={};r(fr,function(t){var r=t+"app";!i&&n.hasAttribute&&n.hasAttribute(r)&&(i=n,u=n.getAttribute(r))});r(fr,function(t){var f=t+"app",r;!i&&(r=n.querySelector("["+f.replace(":","\\:")+"]"))&&(i=r,u=r.getAttribute(f))});i&&(f.strictDi=fl(i,"strict-di")!==null,t(i,u?[u]:[],f))}function uo(i,u,e){var o;h(e)||(e={});o={strictDi:!1};e=a(o,e);var s=function(){var r,n;if(i=f(i),i.injector()){r=i[0]===t?"document":wt(i);throw hi("btstrpd","App Already Bootstrapped with this Element '{0}'",r.replace(/</,"&lt;").replace(/>/,"&gt;"));}return u=u||[],u.unshift(["$provide",function(n){n.value("$rootElement",i)}]),e.debugInfoEnabled&&u.push(["$compileProvider",function(n){n.debugInfoEnabled(!0)}]),u.unshift("ng"),n=wf(u,e.strictDi),n.invoke(["$rootScope","$rootElement","$compile","$injector",function(n,t,i,r){n.$apply(function(){t.data("$injector",r);i(t)(n)})}]),n},c=/^NG_ENABLE_DEBUG_INFO!/,l=/^NG_DEFER_BOOTSTRAP!/;if(n&&c.test(n.name)&&(e.debugInfoEnabled=!0,n.name=n.name.replace(c,"")),n&&!l.test(n.name))return s();n.name=n.name.replace(l,"");ft.resumeBootstrap=function(n){r(n,function(n){u.push(n)});s()}}function ol(){n.name="NG_ENABLE_DEBUG_INFO!"+n.name;n.location.reload()}function sl(n){var t=ft.element(n).injector();if(!t)throw hi("test","no injector found for element argument to getTestability");return t.get("$$testability")}function eo(n,t){return t=t||"_",n.replace(fo,function(n,i){return(i?t:"")+n.toLowerCase()})}function hl(){var t;uf||(ut=n.jQuery,ut&&ut.fn.on?(f=ut,a(ut.fn,{scope:ri.scope,isolateScope:ri.isolateScope,controller:ri.controller,injector:ri.injector,inheritedData:ri.inheritedData}),t=ut.cleanData,ut.cleanData=function(n){var i,r,u;if(ff)ff=!1;else for(r=0;(u=n[r])!=null;r++)i=ut._data(u,"events"),i&&i.$destroy&&ut(u).triggerHandler("$destroy");t(n)}):f=w,ft.element=f,uf=!0)}function ef(n,t,i){if(!n)throw hi("areq","Argument '{0}' is {1}",t||"?",i||"required");return n}function nu(n,t,i){return i&&o(n)&&(n=n[n.length-1]),ef(l(n),t,"not a function, got "+(n&&typeof n=="object"?n.constructor.name||"Object":typeof n)),n}function li(n,t){if(n==="hasOwnProperty")throw hi("badname","hasOwnProperty is not a valid {0} name",t);}function oo(n,t,i){var r;if(!t)return n;var u=t.split("."),f,e=n,o=u.length;for(r=0;r<o;r++)f=u[r],n&&(n=(e=n)[f]);return!i&&l(n)?ge(e,n):n}function tu(n){var t=n[0],r=n[n.length-1],i=[t];do{if(t=t.nextSibling,!t)break;i.push(t)}while(t!==r);return f(i)}function ot(){return Object.create(null)}function cl(n){function t(n,t,i){return n[t]||(n[t]=i())}var r=v("$injector"),u=v("ng"),i=t(n,"angular",Object);return i.$$minErr=i.$$minErr||v,t(i,"module",function(){var n={};return function(i,f,e){var o=function(n,t){if(n==="hasOwnProperty")throw u("badname","hasOwnProperty is not a valid {0} name",t);};return o(i,"module"),f&&n.hasOwnProperty(i)&&(n[i]=null),t(n,i,function(){function n(n,i,r,u){return u||(u=t),function(){return u[r||"push"]([n,i,arguments]),h}}if(!f)throw r("nomod","Module '{0}' is not available! You either misspelled the module name or forgot to load it. If registering a module ensure that you specify the dependencies as the second argument.",i);var t=[],u=[],o=[],s=n("$injector","invoke","push",u),h={_invokeQueue:t,_configBlocks:u,_runBlocks:o,requires:f,name:i,provider:n("$provide","provider"),factory:n("$provide","factory"),service:n("$provide","service"),value:n("$provide","value"),constant:n("$provide","constant","unshift"),animation:n("$animateProvider","register"),filter:n("$filterProvider","register"),controller:n("$controllerProvider","register"),directive:n("$compileProvider","directive"),config:s,run:function(n){return o.push(n),this}};return e&&s(e),h})}})}function ll(n){var t=[];return JSON.stringify(n,function(n,i){if(i=no(n,i),h(i)){if(t.indexOf(i)>=0)return"<<already seen>>";t.push(i)}return i})}function co(n){return typeof n=="function"?n.toString().replace(/ \{[\s\S]*$/,""):typeof n=="undefined"?"undefined":typeof n!="string"?ll(n):n}function al(t){a(t,{bootstrap:uo,copy:ti,extend:a,equals:et,element:f,forEach:r,injector:wf,noop:s,bind:ge,toJson:ur,fromJson:to,identity:ct,isUndefined:e,isDefined:u,isString:c,isFunction:l,isObject:h,isNumber:k,isElement:de,isArray:o,version:lo,isDate:lt,lowercase:y,uppercase:bi,callbacks:{counter:0},getTestability:sl,$$minErr:v,$$csp:ci,reloadWithDebugInfo:ol});ki=cl(n);try{ki("ngLocale")}catch(i){ki("ngLocale",[]).provider("$locale",sv)}ki("ng",["ngLocale"],["$provide",function(n){n.provider({$$sanitizeUri:fy});n.provider("$compile",rs).directive({a:ah,input:tc,textarea:tc,form:up,script:eb,select:hb,style:lb,option:cb,ngBind:dp,ngBindHtml:nw,ngBindTemplate:gp,ngClass:iw,ngClassEven:uw,ngClassOdd:rw,ngCloak:fw,ngController:ew,ngForm:fp,ngHide:nb,ngIf:sw,ngInclude:hw,ngInit:lw,ngNonBindable:bw,ngPluralize:kw,ngRepeat:dw,ngShow:gw,ngStyle:tb,ngSwitch:ib,ngSwitchWhen:rb,ngSwitchDefault:ub,ngOptions:sb,ngTransclude:fb,ngModel:yw,ngList:aw,ngChange:tw,pattern:lc,ngPattern:lc,required:cc,ngRequired:cc,minlength:vc,ngMinlength:vc,maxlength:ac,ngMaxlength:ac,ngValue:kp,ngModelOptions:ww}).directive({ngInclude:cw}).directive(ar).directive(ic);n.provider({$anchorScroll:ca,$animate:is,$browser:va,$cacheFactory:ya,$controller:wa,$document:ba,$exceptionHandler:ka,$filter:ih,$interpolate:ev,$interval:ov,$http:iv,$httpBackend:uv,$location:av,$log:vv,$parse:ny,$rootScope:uy,$q:ty,$$q:iy,$sce:sy,$sceDelegate:oy,$sniffer:hy,$templateCache:pa,$templateRequest:cy,$$testability:ly,$timeout:ay,$window:vy,$$rAF:ry,$$asyncCallback:la,$$jqLite:ea})}])}function yl(){return++vl}function or(n){return n.replace(pl,function(n,t,i,r){return r?i.toUpperCase():i}).replace(wl,"Moz$1")}function hf(n){return!dl.test(n)}function ao(n){var t=n.nodeType;return t===vt||!t||t===ho}function vo(n,t){var i,o,f,u=t.createDocumentFragment(),e=[],s;if(hf(n))e.push(t.createTextNode(n));else{for(i=i||u.appendChild(t.createElement("div")),o=(gl.exec(n)||["",""])[1].toLowerCase(),f=st[o]||st._default,i.innerHTML=f[1]+n.replace(na,"<$1><\/$2>")+f[2],s=f[0];s--;)i=i.lastChild;e=rr(e,i.childNodes);i=u.firstChild;i.textContent=""}return u.textContent="",u.innerHTML="",r(e,function(n){u.appendChild(n)}),u}function ta(n,i){i=i||t;var r;return(r=kl.exec(n))?[i.createElement(r[1])]:(r=vo(n,i))?r.childNodes:[]}function w(n){if(n instanceof w)return n;var t;if(c(n)&&(n=p(n),t=!0),!(this instanceof w)){if(t&&n.charAt(0)!="<")throw sf("nosel","Looking up elements via selectors is not supported by jqLite! See: http://docs.angularjs.org/api/angular.element");return new w(n)}t?af(this,ta(n)):af(this,n)}function cf(n){return n.cloneNode(!0)}function fu(n,t){var r,i,u;if(t||eu(n),n.querySelectorAll)for(r=n.querySelectorAll("*"),i=0,u=r.length;i<u;i++)eu(r[i])}function yo(n,t,i,f){if(u(f))throw sf("offargs","jqLite#off() does not support the `selector` argument");var e=ou(n),o=e&&e.events,s=e&&e.handle;if(s)if(t)r(t.split(" "),function(t){if(u(i)){var r=o[t];if(ir(r||[],i),r&&r.length>0)return}er(n,t,s);delete o[t]});else for(t in o)t!=="$destroy"&&er(n,t,s),delete o[t]}function eu(n,t){var u=n.ng339,r=u&&ru[u];if(r){if(t){delete r.data[t];return}r.handle&&(r.events.$destroy&&r.handle({},"$destroy"),yo(n));delete ru[u];n.ng339=i}}function ou(n,t){var r=n.ng339,u=r&&ru[r];return t&&!u&&(n.ng339=r=yl(),u=ru[r]={events:{},data:{},handle:i}),u}function lf(n,t,i){if(ao(n)){var f=u(i),e=!f&&t&&!h(t),s=!t,o=ou(n,!e),r=o&&o.data;if(f)r[t]=i;else{if(s)return r;if(e)return r&&r[t];a(r,t)}}}function su(n,t){return n.getAttribute?(" "+(n.getAttribute("class")||"")+" ").replace(/[\n\t]/g," ").indexOf(" "+t+" ")>-1:!1}function hu(n,t){t&&n.setAttribute&&r(t.split(" "),function(t){n.setAttribute("class",p((" "+(n.getAttribute("class")||"")+" ").replace(/[\n\t]/g," ").replace(" "+p(t)+" "," ")))})}function cu(n,t){if(t&&n.setAttribute){var i=(" "+(n.getAttribute("class")||"")+" ").replace(/[\n\t]/g," ");r(t.split(" "),function(n){n=p(n);i.indexOf(" "+n+" ")===-1&&(i+=n+" ")});n.setAttribute("class",p(i))}}function af(n,t){var i,r;if(t)if(t.nodeType)n[n.length++]=t;else if(i=t.length,typeof i=="number"&&t.window!==t){if(i)for(r=0;r<i;r++)n[n.length++]=t[r]}else n[n.length++]=t}function po(n,t){return lu(n,"$"+(t||"ngController")+"Controller")}function lu(n,t,r){var e,u,s;for(n.nodeType==ho&&(n=n.documentElement),e=o(t)?t:[t];n;){for(u=0,s=e.length;u<s;u++)if((r=f.data(n,e[u]))!==i)return r;n=n.parentNode||n.nodeType===of&&n.host}}function wo(n){for(fu(n,!0);n.firstChild;)n.removeChild(n.firstChild)}function bo(n,t){t||fu(n);var i=n.parentNode;i&&i.removeChild(n)}function ia(t,i){if(i=i||n,i.document.readyState==="complete")i.setTimeout(t);else f(i).on("load",t)}function ko(n,t){var i=sr[t.toLowerCase()];return i&&vf[pt(n)]&&i}function ra(n,t){var i=n.nodeName;return(i==="INPUT"||i==="TEXTAREA")&&yf[t]}function ua(n,t){var i=function(i,r){var u,f,s,o;if(i.isDefaultPrevented=function(){return i.defaultPrevented},u=t[r||i.type],f=u?u.length:0,f)for(e(i.immediatePropagationStopped)&&(s=i.stopImmediatePropagation,i.stopImmediatePropagation=function(){i.immediatePropagationStopped=!0;i.stopPropagation&&i.stopPropagation();s&&s.call(i)}),i.isImmediatePropagationStopped=function(){return i.immediatePropagationStopped===!0},f>1&&(u=at(u)),o=0;o<f;o++)i.isImmediatePropagationStopped()||u[o].call(n,i)};return i.elem=n,i}function ea(){this.$get=function(){return a(w,{hasClass:function(n,t){return n.attr&&(n=n[0]),su(n,t)},addClass:function(n,t){return n.attr&&(n=n[0]),cu(n,t)},removeClass:function(n,t){return n.attr&&(n=n[0]),hu(n,t)}})}}function ai(n,t){var r=n&&n.$$hashKey,i;return r?(typeof r=="function"&&(r=n.$$hashKey()),r):(i=typeof n,i=="function"||i=="object"&&n!==null?n.$$hashKey=i+":"+(t||br)():i+":"+n)}function hr(n,t){if(t){var i=0;this.nextUid=function(){return++i}}r(n,this.put,this)}function ha(n){var i=n.toString().replace(ns,""),t=i.match(go);return t?"function("+(t[1]||"").replace(/[\s\r\n]+/," ")+")":"fn"}function pf(n,t,i){var u,e,s,f;if(typeof n=="function"){if(!(u=n.$inject)){if(u=[],n.length){if(t){c(i)&&i||(i=n.name||ha(n));throw ui("strictdi","{0} is not using explicit annotation and cannot be invoked in strict mode",i);}e=n.toString().replace(ns,"");s=e.match(go);r(s[1].split(oa),function(n){n.replace(sa,function(n,t,i){u.push(i)})})}n.$inject=u}}else o(n)?(f=n.length-1,nu(n[f],"fn"),u=n.slice(0,f)):nu(n,"fn",!0);return u}function wf(n,t){function y(n){return function(t,i){if(h(t))r(t,we(n));else return n(t,i)}}function g(n,t){if(li(n,"service"),(l(t)||o(t))&&(t=u.instantiate(t)),!t.$get)throw ui("pget","Provider '{0}' must define $get factory method.",n);return v[n+p]=t}function rt(n,t){return function(){var i=f.invoke(t,this);if(e(i))throw ui("undef","Provider '{0}' must return a value from $get factory method.",n);return i}}function k(n,t,i){return g(n,{$get:i!==!1?rt(n,t):t})}function ut(n,t){return k(n,["$injector",function(n){return n.instantiate(t)}])}function et(n,t){return k(n,nt(t),!1)}function ot(n,t){li(n,"constant");v[n]=t;b[n]=t}function st(n,t){var i=u.get(n+p),r=i.$get;i.$get=function(){var n=f.invoke(r,i);return f.invoke(t,null,{$delegate:n})}}function tt(n){var t=[],i;return r(n,function(n){function f(n){for(var i,r,t=0,f=n.length;t<f;t++)i=n[t],r=u.get(i[0]),r[i[1]].apply(r,i[2])}if(!d.get(n)){d.put(n,!0);try{c(n)?(i=ki(n),t=t.concat(tt(i.requires)).concat(i._runBlocks),f(i._invokeQueue),f(i._configBlocks)):l(n)?t.push(u.invoke(n)):o(n)?t.push(u.invoke(n)):nu(n,"module")}catch(r){o(n)&&(n=n[n.length-1]);r.message&&r.stack&&r.stack.indexOf(r.message)==-1&&(r=r.message+"\n"+r.stack);throw ui("modulerr","Failed to instantiate module {0} due to:\n{1}",n,r.stack||r.message||r);}}}),t}function it(n,i){function r(t,r){if(n.hasOwnProperty(t)){if(n[t]===w)throw ui("cdep","Circular dependency found: {0}",t+" <- "+a.join(" <- "));return n[t]}try{return a.unshift(t),n[t]=w,n[t]=i(t,r)}catch(u){n[t]===w&&delete n[t];throw u;}finally{a.shift()}}function u(n,i,u,f){typeof u=="string"&&(f=u,u=null);for(var c=[],l=pf(n,t,f),e,s=0,h=l.length;s<h;s++){if(e=l[s],typeof e!="string")throw ui("itkn","Incorrect injection token! Expected service name as string, got {0}",e);c.push(u&&u.hasOwnProperty(e)?u[e]:r(e,f))}return o(n)&&(n=n[h]),n.apply(i,c)}function f(n,t,i){var f=Object.create((o(n)?n[n.length-1]:n).prototype||null),r=u(n,f,t,i);return h(r)||l(r)?r:f}return{invoke:u,instantiate:f,get:r,annotate:pf,has:function(t){return v.hasOwnProperty(t+p)||n.hasOwnProperty(t)}}}t=t===!0;var w={},p="Provider",a=[],d=new hr([],!0),v={$provide:{provider:y(g),factory:y(k),service:y(ut),value:y(et),constant:y(ot),decorator:st}},u=v.$injector=it(v,function(n,t){ft.isString(t)&&a.push(t);throw ui("unpr","Unknown provider: {0}",a.join(" <- "));}),b={},f=b.$injector=it(b,function(n,t){var r=u.get(n+p,t);return f.invoke(r.$get,r,i,n)});return r(tt(n),function(n){f.invoke(n||s)}),f}function ca(){var n=!0;this.disableAutoScrolling=function(){n=!1};this.$get=["$window","$location","$rootScope",function(t,i,r){function o(n){var t=null;return Array.prototype.some.call(n,function(n){if(pt(n)==="a")return t=n,!0}),t}function s(){var n=f.yOffset,i,r;return l(n)?n=n():de(n)?(i=n[0],r=t.getComputedStyle(i),n=r.position!=="fixed"?0:i.getBoundingClientRect().bottom):k(n)||(n=0),n}function u(n){var i,r;n?(n.scrollIntoView(),i=s(),i&&(r=n.getBoundingClientRect().top,t.scrollBy(0,r-i))):t.scrollTo(0,0)}function f(){var n=i.hash(),t;n?(t=e.getElementById(n))?u(t):(t=o(e.getElementsByName(n)))?u(t):n==="top"&&u(null):u(null)}var e=t.document;return n&&r.$watch(function(){return i.hash()},function(n,t){(n!==t||n!=="")&&ia(function(){r.$evalAsync(f)})}),f}]}function la(){this.$get=["$$rAF","$timeout",function(n,t){return n.supported?function(t){return n(t)}:function(n){return t(n,0,!1)}}]}function aa(n,t,u,o){function it(n){try{n.apply(null,tf(arguments,1))}finally{if(v--,v===0)while(k.length)try{k.pop()()}catch(t){u.error(t)}}}function wt(n){var t=n.indexOf("#");return t===-1?"":n.substr(t+1)}function bt(n,t){(function i(){r(d,function(n){n()});ht=t(i,n)})()}function lt(){ft();at()}function ft(){l=n.history.state;l=e(l)?null:l;et(l,g)&&(l=g);g=l}function at(){(p!==h.url()||y!==l)&&(p=h.url(),y=l,r(rt,function(n){n(h.url(),l)}))}function yt(n){try{return decodeURIComponent(n)}catch(t){return n}}var h=this,w=t[0],a=n.location,tt=n.history,st=n.setTimeout,pt=n.clearTimeout,b={},v,k,d,ht,rt,ut,g;h.isMock=!1;v=0;k=[];h.$$completeOutstandingRequest=it;h.$$incOutstandingRequestCount=function(){v++};h.notifyWhenNoOutstandingRequests=function(n){r(d,function(n){n()});v===0?n():k.push(n)};d=[];h.addPollFn=function(n){return e(ht)&&bt(100,st),d.push(n),n};var l,y,p=a.href,kt=t.find("base"),ct=null;ft();y=l;h.url=function(t,i,r){var f,u;return e(r)&&(r=null),a!==n.location&&(a=n.location),tt!==n.history&&(tt=n.history),t?(f=y===r,p===t&&(!o.history||f))?h:(u=p&&fi(p)===fi(t),p=t,y=r,!o.history||u&&f?(u||(ct=t),i?a.replace(t):u?a.hash=wt(t):a.href=t):(tt[i?"replaceState":"pushState"](r,"",t),ft(),y=l),h):ct||a.href.replace(/%27/g,"'")};h.state=function(){return l};rt=[];ut=!1;g=null;h.onUrlChange=function(t){if(!ut){if(o.history)f(n).on("popstate",lt);f(n).on("hashchange",lt);ut=!0}return rt.push(t),t};h.$$checkUrlChange=at;h.baseHref=function(){var n=kt.attr("href");return n?n.replace(/^(https?\:)?\/\/[^\/]*/,""):""};var nt={},ot="",vt=h.baseHref();h.cookies=function(n,t){var o,s,r,f,e;if(n)t===i?w.cookie=encodeURIComponent(n)+"=;path="+vt+";expires=Thu, 01 Jan 1970 00:00:00 GMT":c(t)&&(o=(w.cookie=encodeURIComponent(n)+"="+encodeURIComponent(t)+";path="+vt).length+1,o>4096&&u.warn("Cookie '"+n+"' possibly not set or overflowed because it was too large ("+o+" > 4096 bytes)!"));else{if(w.cookie!==ot)for(ot=w.cookie,s=ot.split("; "),nt={},f=0;f<s.length;f++)r=s[f],e=r.indexOf("="),e>0&&(n=yt(r.substring(0,e)),nt[n]===i&&(nt[n]=yt(r.substring(e+1))));return nt}};h.defer=function(n,t){var i;return v++,i=st(function(){delete b[i];it(n)},t||0),b[i]=!0,i};h.defer.cancel=function(n){return b[n]?(delete b[n],pt(n),it(s),!0):!1}}function va(){this.$get=["$window","$log","$sniffer","$document",function(n,t,i,r){return new aa(n,r,t,i)}]}function ya(){this.$get=function(){function t(t,i){function y(n){n!=f&&(r?r==n&&(r=n.n):r=n,c(n.n,n.p),c(n,f),f=n,f.n=null)}function c(n,t){n!=t&&(n&&(n.p=t),t&&(t.n=n))}if(t in n)throw v("$cacheFactory")("iid","CacheId '{0}' is already taken!",t);var s=0,l=a({},i,{id:t}),o={},h=i&&i.capacity||Number.MAX_VALUE,u={},f=null,r=null;return n[t]={put:function(n,t){if(h<Number.MAX_VALUE){var i=u[n]||(u[n]={key:n});y(i)}if(!e(t))return n in o||s++,o[n]=t,s>h&&this.remove(r.key),t},get:function(n){if(h<Number.MAX_VALUE){var t=u[n];if(!t)return;y(t)}return o[n]},remove:function(n){if(h<Number.MAX_VALUE){var t=u[n];if(!t)return;t==f&&(f=t.p);t==r&&(r=t.n);c(t.n,t.p);delete u[n]}delete o[n];s--},removeAll:function(){o={};s=0;u={};f=r=null},destroy:function(){o=null;l=null;u=null;delete n[t]},info:function(){return a({},l,{size:s})}}}var n={};return t.info=function(){var t={};return r(n,function(n,i){t[i]=n.info()}),t},t.get=function(t){return n[t]},t}}function pa(){this.$get=["$cacheFactory",function(n){return n("templates")}]}function rs(n,e){function ft(n,t){var u=/^\s*([@&]|=(\*?))(\??)\s*(\w*)\s*$/,i={};return r(n,function(n,r){var f=n.match(u);if(!f)throw tt("iscp","Invalid isolate scope definition for directive '{0}'. Definition: {... {1}: '{2}' ...}",t,r,n);i[r]={mode:f[1][0],collection:f[2]==="*",optional:f[3]==="?",attrName:f[4]||r}}),i}var w={},b="Directive",k=/^\s*directive\:\s*([\w\-]+)\s+(.*)$/,d=/(([\w\-]+)(?:\:([^;]+))?;?)/,g=ul("ngSrc,ngSrcset,src,srcset"),it=/^(?:(\^\^?)?(\?)?(\^\^?)?)?/,rt=/^(on[a-z]+|formaction)$/,v;this.directive=function st(t,i){return li(t,"directive"),c(t)?(ef(i,"directiveFactory"),w.hasOwnProperty(t)||(w[t]=[],n.factory(t+b,["$injector","$exceptionHandler",function(n,i){var u=[];return r(w[t],function(r,f){try{var e=n.invoke(r);l(e)?e={compile:nt(e)}:!e.compile&&e.link&&(e.compile=nt(e.link));e.priority=e.priority||0;e.index=f;e.name=e.name||t;e.require=e.require||e.controller&&e.name;e.restrict=e.restrict||"EA";h(e.scope)&&(e.$$isolateBindings=ft(e.scope,e.name));u.push(e)}catch(o){i(o)}}),u}])),w[t].push(i)):r(t,we(st)),this};this.aHrefSanitizationWhitelist=function(n){return u(n)?(e.aHrefSanitizationWhitelist(n),this):e.aHrefSanitizationWhitelist()};this.imgSrcSanitizationWhitelist=function(n){return u(n)?(e.imgSrcSanitizationWhitelist(n),this):e.imgSrcSanitizationWhitelist()};v=!0;this.debugInfoEnabled=function(n){return u(n)?(v=n,this):v};this.$get=["$injector","$interpolate","$exceptionHandler","$templateRequest","$parse","$controller","$rootScope","$document","$sce","$animate","$$sanitizeUri",function(n,u,e,nt,ft,st,ht,lt,at,yt,kt){function ni(n,t){try{n.addClass(t)}catch(i){}}function dt(n,t,i,u,e){var s,o;return n instanceof f||(n=f(n)),r(n,function(t,i){t.nodeType==iu&&t.nodeValue.match(/\S+/)&&(n[i]=f(t).wrap("<span><\/span>").parent()[0])}),s=ei(n,t,n,i,u,e),dt.$$addScopeClass(n),o=null,function(t,i,r){var u,c;ef(t,"scope");r=r||{};var e=r.parentBoundTranscludeFn,h=r.transcludeControllers,l=r.futureParentElement;if(e&&e.$$boundTransclude&&(e=e.$$boundTransclude),o||(o=gi(l)),u=o!=="html"?f(si(o,f("<div>").append(n).html())):i?ri.clone.call(n):n,h)for(c in h)u.data("$"+c+"Controller",h[c].instance);return dt.$$addScopeInfo(u,t),i&&i(u,t),s&&s(t,u,u,e),u}}function gi(n){var t=n&&n[0];return t?pt(t)!=="foreignobject"&&t.toString().match(/SVG/)?"svg":"html":"html"}function ei(n,t,r,u,e,o){function b(n,r,u,e){var s,c,l,a,o,w,y,b,v,k;if(p)for(k=r.length,v=new Array(k),o=0;o<h.length;o+=3)y=h[o],v[y]=r[y];else v=r;for(o=0,w=h.length;o<w;)l=v[h[o++]],s=h[o++],c=h[o++],s?(s.scope?(a=n.$new(),dt.$$addScopeInfo(f(l),a)):a=n,b=s.transcludeOnThisElement?ti(n,s.transclude,e,s.elementTranscludeOnThisElement):!s.templateOnThisElement&&e?e:!e&&t?ti(n,t):null,s(c,a,l,u,b)):c&&c(n,l.childNodes,i,e)}for(var h=[],l,a,s,v,y,w,p,c=0;c<n.length;c++)l=new fi,a=oi(n[c],[],l,c===0?u:i,e),s=a.length?yi(a,n[c],l,t,r,null,[],[],o):null,s&&s.scope&&dt.$$addScopeClass(l.$$element),y=s&&s.terminal||!(v=n[c].childNodes)||!v.length?null:ei(v,s?(s.transcludeOnThisElement||!s.templateOnThisElement)&&s.transclude:t),(s||y)&&(h.push(c,s,y),w=!0,p=p||s),o=null;return w?b:null}function ti(n,t,i){return function(r,u,f,e,o){return r||(r=n.$new(!1,o),r.$$transcluded=!0),t(r,u,{parentBoundTranscludeFn:i,transcludeControllers:f,futureParentElement:e})}}function oi(n,t,i,r,u){var it=n.nodeType,rt=i.$attr,o,s,g,nt,tt;switch(it){case vt:ii(t,bt(pt(n)),"E",r,u);for(var a,e,f,l,v,y,w=n.attributes,b=0,ut=w&&w.length;b<ut;b++)g=!1,nt=!1,a=w[b],e=a.name,v=p(a.value),l=bt(e),(y=di.test(l))&&(e=e.replace(bf,"").substr(8).replace(/_(.)/g,function(n,t){return t.toUpperCase()})),tt=l.replace(/(Start|End)$/,""),tr(tt)&&l===tt+"Start"&&(g=e,nt=e.substr(0,e.length-5)+"end",e=e.substr(0,e.length-6)),f=bt(e.toLowerCase()),rt[f]=e,(y||!i.hasOwnProperty(f))&&(i[f]=v,ko(n,f)&&(i[f]=!0)),or(n,t,v,f,y),ii(t,f,"A",r,u,g,nt);if(s=n.className,h(s)&&(s=s.animVal),c(s)&&s!=="")while(o=d.exec(s))f=bt(o[2]),ii(t,f,"C",r,u)&&(i[f]=p(o[3])),s=s.substr(o.index+o[0].length);break;case iu:fr(t,n.nodeValue);break;case so:try{o=k.exec(n.nodeValue);o&&(f=bt(o[1]),ii(t,f,"M",r,u)&&(i[f]=p(o[2])))}catch(ft){}}return t.sort(ur),t}function ai(n,t,i){var r=[],u=0;if(t&&n.hasAttribute&&n.hasAttribute(t)){do{if(!n)throw tt("uterdir","Unterminated attribute, found '{0}' but no matching '{1}' found.",t,i);n.nodeType==vt&&(n.hasAttribute(t)&&u++,n.hasAttribute(i)&&u--);r.push(n);n=n.nextSibling}while(u>0)}else r.push(n);return f(r)}function vi(n,t,i){return function(r,u,f,e,o){return u=ai(u[0],t,i),n(r,u,f,e,o)}}function yi(n,s,a,v,y,w,b,k,d){function fr(n,t,i,r){n&&(i&&(n=vi(n,i,r)),n.require=g.require,n.directiveName=ot,(nt===g||g.$$isolateScope)&&(n=bi(n,{isolateScope:!0})),b.push(n));t&&(i&&(t=vi(t,i,r)),t.require=g.require,t.directiveName=ot,(nt===g||g.$$isolateScope)&&(t=bi(t,{isolateScope:!0})),k.push(t))}function tr(n,t,i,u){var f,s="data",h=!1,l=i,e;if(c(t)){if(e=t.match(it),t=t.substring(e[0].length),e[3]&&(e[1]?e[3]=null:e[1]=e[3]),e[1]==="^"?s="inheritedData":e[1]==="^^"&&(s="inheritedData",l=i.parent()),e[2]==="?"&&(h=!0),f=null,u&&s==="data"&&(f=u[t])&&(f=f.instance),f=f||l[s]("$"+t+"Controller"),!f&&!h)throw tt("ctreq","Controller '{0}', required by directive '{1}', can't be found!",t,n);return f||null}return o(t)&&(f=[],r(t,function(t){f.push(tr(n,t,i,u))})),f}function at(n,t,e,o,h){function ht(n,t,r){var u;return nr(n)||(r=t,t=n,n=i),bt&&(u=d),r||(r=bt?l.parent():l),h(n,t,u,r,ut)}var w,ot,v,it,p,d,g,l,c,rt,y,ut;for(s===e?(c=a,l=a.$$element):(l=f(e),c=new fi(l,a)),nt&&(p=t.$new(!0)),h&&(g=ht,g.$$boundTransclude=h),yt&&(pt={},d={},r(yt,function(n){var r={$scope:n===nt||n.$$isolateScope?p:t,$element:l,$attrs:c,$transclude:g},i;it=n.controller;it=="@"&&(it=c[n.name]);i=st(it,r,!0,n.controllerAs);d[n.name]=i;bt||l.data("$"+n.name+"Controller",i.instance);pt[n.name]=i})),nt&&(dt.$$addScopeInfo(l,p,!0,!(ct&&(ct===nt||ct===nt.$$originalDirective))),dt.$$addScopeClass(l,!0),rt=pt&&pt[nt.name],y=p,rt&&rt.identifier&&nt.bindToController===!0&&(y=rt.instance),r(p.$$isolateBindings=nt.$$isolateBindings,function(n,i){var r=n.attrName,a=n.optional,v=n.mode,e,f,h,s,o,l;switch(v){case"@":c.$observe(r,function(n){y[i]=n});c.$$observers[r].$$scope=t;c[r]&&(y[i]=u(c[r])(t));break;case"=":if(a&&!c[r])return;f=ft(c[r]);s=f.literal?et:function(n,t){return n===t||n!==n&&t!==t};h=f.assign||function(){e=y[i]=f(t);throw tt("nonassign","Expression '{0}' used with directive '{1}' is non-assignable!",c[r],nt.name);};e=y[i]=f(t);o=function(n){return s(n,y[i])||(s(n,e)?h(t,n=y[i]):y[i]=n),e=n};o.$stateful=!0;l=n.collection?t.$watchCollection(c[r],o):t.$watch(ft(c[r],o),null,f.literal);p.$on("$destroy",l);break;case"&":f=ft(c[r]);y[i]=function(n){return f(t,n)}}})),pt&&(r(pt,function(n){n()}),pt=null),w=0,ot=b.length;w<ot;w++)v=b[w],ki(v,v.isolateScope?p:t,l,c,v.require&&tr(v.directiveName,v.require,l,d),g);for(ut=t,nt&&(nt.template||nt.templateUrl===null)&&(ut=p),n&&n(ut,e.childNodes,i,h),w=k.length-1;w>=0;w--)v=k[w],ki(v,v.isolateScope?p:t,l,c,v.require&&tr(v.directiveName,v.require,l,d),g)}var lt,ci,ri,yi;d=d||{};var ni=-Number.MAX_VALUE,ti,yt=d.controllerDirectives,pt,nt=d.newIsolateScopeDirective,ct=d.templateDirective,ei=d.nonTlbTranscludeDirective,di=!1,gi=!1,bt=d.hasElementTranscludeDirective,rt=a.$$element=f(s),g,ot,ht,hi=w,ii=v,kt,ut;for(lt=0,ci=n.length;lt<ci;lt++){if(g=n[lt],ri=g.$$start,yi=g.$$end,ri&&(rt=ai(s,ri,yi)),ht=i,ni>g.priority)break;if((ut=g.scope)&&(g.templateUrl||(h(ut)?(gt("new/isolated scope",nt||ti,g,rt),nt=g):gt("new/isolated scope",nt,g,rt)),ti=ti||g),ot=g.name,!g.templateUrl&&g.controller&&(ut=g.controller,yt=yt||{},gt("'"+ot+"' controller",yt[ot],g,rt),yt[ot]=g),(ut=g.transclude)&&(di=!0,g.$$tlb||(gt("transclusion",ei,g,rt),ei=g),ut=="element"?(bt=!0,ni=g.priority,ht=rt,rt=a.$$element=f(t.createComment(" "+ot+": "+a[ot]+" ")),s=rt[0],ui(y,tf(ht),s),ii=dt(ht,v,ni,hi&&hi.name,{nonTlbTranscludeDirective:ei})):(ht=f(cf(s)).contents(),rt.empty(),ii=dt(ht,v))),g.template)if(gi=!0,gt("template",ct,g,rt),ct=g,ut=l(g.template)?g.template(rt,a):g.template,ut=li(ut),g.replace){if(hi=g,ht=hf(ut)?[]:fs(si(g.templateNamespace,p(ut))),s=ht[0],ht.length!=1||s.nodeType!==vt)throw tt("tplrt","Template for directive '{0}' must have exactly one root element. {1}",ot,"");ui(y,rt,s);var ir={$attr:{}},ur=oi(s,[],ir),er=n.splice(lt+1,n.length-(lt+1));nt&&pi(ur);n=n.concat(ur).concat(er);wi(a,ir);ci=n.length}else rt.html(ut);if(g.templateUrl)gi=!0,gt("template",ct,g,rt),ct=g,g.replace&&(hi=g),at=rr(n.splice(lt,n.length-lt),rt,a,y,di&&ii,b,k,{controllerDirectives:yt,newIsolateScopeDirective:nt,templateDirective:ct,nonTlbTranscludeDirective:ei}),ci=n.length;else if(g.compile)try{kt=g.compile(rt,a,ii);l(kt)?fr(null,kt,ri,yi):kt&&fr(kt.pre,kt.post,ri,yi)}catch(or){e(or,wt(rt))}g.terminal&&(at.terminal=!0,ni=Math.max(ni,g.priority))}return at.scope=ti&&ti.scope===!0,at.transcludeOnThisElement=di,at.elementTranscludeOnThisElement=bt,at.templateOnThisElement=gi,at.transclude=ii,d.hasElementTranscludeDirective=bt,at}function pi(n){for(var t=0,i=n.length;t<i;t++)n[t]=ke(n[t],{$$isolateScope:!0})}function ii(t,r,u,f,o,s,h){var l;if(r===o)return null;if(l=null,w.hasOwnProperty(r))for(var c,v=n.get(r+b),a=0,y=v.length;a<y;a++)try{c=v[a];(f===i||f>c.priority)&&c.restrict.indexOf(u)!=-1&&(s&&(c=ke(c,{$$start:s,$$end:h})),t.push(c),l=c)}catch(p){e(p)}return l}function tr(t){if(w.hasOwnProperty(t))for(var r,u=n.get(t+b),i=0,f=u.length;i<f;i++)if(r=u[i],r.multiElement)return!0;return!1}function wi(n,t){var u=t.$attr,f=n.$attr,i=n.$$element;r(n,function(i,r){r.charAt(0)!="$"&&(t[r]&&t[r]!==i&&(i+=(r==="style"?";":" ")+t[r]),n.$set(r,i,!0,u[r]))});r(t,function(t,r){r=="class"?(ni(i,t),n["class"]=(n["class"]?n["class"]+" ":"")+t):r=="style"?(i.attr("style",i.attr("style")+";"+t),n.style=(n.style?n.style+";":"")+t):r.charAt(0)=="$"||n.hasOwnProperty(r)||(n[r]=t,f[r]=u[r])})}function rr(n,t,i,u,e,o,s,c){var y=[],w,b,k=t[0],v=n.shift(),g=a({},v,{templateUrl:null,transclude:null,replace:null,$$originalDirective:v}),d=l(v.templateUrl)?v.templateUrl(t,i):v.templateUrl,it=v.templateNamespace;return t.empty(),nt(at.getTrustedResourceUrl(d)).then(function(l){var a,rt,ut,st,ft,ct;if(l=li(l),v.replace){if(ut=hf(l)?[]:fs(si(it,p(l))),a=ut[0],ut.length!=1||a.nodeType!==vt)throw tt("tplrt","Template for directive '{0}' must have exactly one root element. {1}",v.name,d);rt={$attr:{}};ui(u,t,a);ft=oi(a,[],rt);h(v.scope)&&pi(ft);n=ft.concat(n);wi(i,rt)}else a=k,t.html(l);for(n.unshift(g),w=yi(n,a,i,e,t,v,o,s,c),r(u,function(n,i){n==a&&(u[i]=t[0])}),b=ei(t[0].childNodes,e);y.length;){var et=y.shift(),ot=y.shift(),lt=y.shift(),ht=y.shift(),nt=t[0];et.$$destroyed||(ot!==k&&(ct=ot.className,c.hasElementTranscludeDirective&&v.replace||(nt=cf(a)),ui(lt,f(ot),nt),ni(f(nt),ct)),st=w.transcludeOnThisElement?ti(et,w.transclude,ht):ht,w(b,et,nt,u,st))}y=null}),function(n,t,i,r,u){var f=u;t.$$destroyed||(y?y.push(t,i,r,f):(w.transcludeOnThisElement&&(f=ti(t,w.transclude,u)),w(b,t,i,r,f)))}}function ur(n,t){var i=t.priority-n.priority;return i!==0?i:n.name!==t.name?n.name<t.name?-1:1:n.index-t.index}function gt(n,t,i,r){if(t)throw tt("multidir","Multiple directives [{0}, {1}] asking for {2} on: {3}",t.name,i.name,n,wt(r));}function fr(n,t){var i=u(t,!0);i&&n.push({priority:0,compile:function(n){var t=n.parent(),r=!!t.length;return r&&dt.$$addBindingClass(t),function(n,t){var u=t.parent();r||dt.$$addBindingClass(u);dt.$$addBindingInfo(u,i.expressions);n.$watch(i,function(n){t[0].nodeValue=n})}}})}function si(n,i){n=y(n||"html");switch(n){case"svg":case"math":var r=t.createElement("div");return r.innerHTML="<"+n+">"+i+"<\/"+n+">",r.childNodes[0].childNodes;default:return i}}function er(n,t){if(t=="srcdoc")return at.HTML;var i=pt(n);if(t=="xlinkHref"||i=="form"&&t=="action"||i!="img"&&(t=="src"||t=="ngSrc"))return at.RESOURCE_URL}function or(n,t,i,r,f){var o=er(n,r),e;if(f=g[r]||f,e=u(i,!0,o,f),e){if(r==="multiple"&&pt(n)==="select")throw tt("selmulti","Binding to the 'multiple' attribute is not supported. Element: {0}",wt(n));t.push({priority:100,compile:function(){return{pre:function(n,t,s){var c=s.$$observers||(s.$$observers={}),h;if(rt.test(r))throw tt("nodomevents","Interpolations for HTML DOM event attributes are disallowed.  Please use the ng- versions (such as ng-click instead of onclick) instead.");(h=s[r],h!==i&&(e=h&&u(h,!0,o,f),i=h),e)&&(s[r]=e(n),(c[r]||(c[r]=[])).$$inter=!0,(s.$$observers&&s.$$observers[r].$$scope||n).$watch(e,function(n,t){r==="class"&&n!=t?s.$updateClass(n,t):s.$set(r,n)}))}}}})}}function ui(n,i,r){var u=i[0],a=i.length,v=u.parentNode,e,y,c,s,w,l;if(n)for(e=0,y=n.length;e<y;e++)if(n[e]==u){n[e++]=r;for(var o=e,h=o+a-1,p=n.length;o<p;o++,h++)h<p?n[o]=n[h]:delete n[o];n.length-=a-1;n.context===u&&(n.context=r);break}for(v&&v.replaceChild(r,u),c=t.createDocumentFragment(),c.appendChild(u),f(r).data(f(u).data()),ut?(ff=!0,ut.cleanData([u])):delete f.cache[u[f.expando]],s=1,w=i.length;s<w;s++)l=i[s],f(l).remove(),c.appendChild(l),delete i[s];i[0]=r;i.length=1}function bi(n,t){return a(function(){return n.apply(null,arguments)},n,t)}function ki(n,t,i,r,u,f){try{n(t,i,r,u,f)}catch(o){e(o,wt(i))}}var fi=function(n,t){if(t)for(var u=Object.keys(t),r,i=0,f=u.length;i<f;i++)r=u[i],this[r]=t[r];else this.$attr={};this.$$element=n};fi.prototype={$normalize:bt,$addClass:function(n){n&&n.length>0&&yt.addClass(this.$$element,n)},$removeClass:function(n){n&&n.length>0&&yt.removeClass(this.$$element,n)},$updateClass:function(n,t){var r=us(n,t),i;r&&r.length&&yt.addClass(this.$$element,r);i=us(t,n);i&&i.length&&yt.removeClass(this.$$element,i)},$set:function(n,t,u,f){var w=this.$$element[0],b=ko(w,n),a=ra(w,n),k=n,h,s,v,l,y;if(b?(this.$$element.prop(n,t),f=b):a&&(this[a]=t,k=a),this[n]=t,f?this.$attr[n]=f:(f=this.$attr[n],f||(this.$attr[n]=f=eo(n,"-"))),h=pt(this.$$element),h==="a"&&n==="href"||h==="img"&&n==="src")this[n]=t=kt(t,n==="src");else if(h==="img"&&n==="srcset"){var o="",d=p(t),g=/\s/.test(d)?/(\s+\d+x\s*,|\s+\d+w\s*,|\s+,|,\s+)/:/(,)/,c=d.split(g),nt=Math.floor(c.length/2);for(s=0;s<nt;s++)v=s*2,o+=kt(p(c[v]),!0),o+=" "+p(c[v+1]);l=p(c[s*2]).split(/\s/);o+=kt(p(l[0]),!0);l.length===2&&(o+=" "+p(l[1]));this[n]=t=o}u!==!1&&(t===null||t===i?this.$$element.removeAttr(f):this.$$element.attr(f,t));y=this.$$observers;y&&r(y[k],function(n){try{n(t)}catch(i){e(i)}})},$observe:function(n,t){var i=this,u=i.$$observers||(i.$$observers=ot()),r=u[n]||(u[n]=[]);return r.push(t),ht.$evalAsync(function(){!r.$$inter&&i.hasOwnProperty(n)&&t(i[n])}),function(){ir(r,t)}}};var hi=u.startSymbol(),ci=u.endSymbol(),li=hi=="{{"||ci=="}}"?ct:function(n){return n.replace(/\{\{/g,hi).replace(/}}/g,ci)},di=/^ngAttr[A-Z]/;return dt.$$addBindingInfo=v?function(n,t){var i=n.data("$binding")||[];o(t)?i=i.concat(t):i.push(t);n.data("$binding",i)}:s,dt.$$addBindingClass=v?function(n){ni(n,"ng-binding")}:s,dt.$$addScopeInfo=v?function(n,t,i,r){var u=i?r?"$isolateScopeNoTemplate":"$isolateScope":"$scope";n.data(u,t)}:s,dt.$$addScopeClass=v?function(n,t){ni(n,t?"ng-isolate-scope":"ng-scope")}:s,dt}]}function bt(n){return or(n.replace(bf,""))}function us(n,t){var u="",e=n.split(/\s+/),o=t.split(/\s+/),i,f,r;n:for(i=0;i<e.length;i++){for(f=e[i],r=0;r<o.length;r++)if(f==o[r])continue n;u+=(u.length>0?" ":"")+f}return u}function fs(n){var t,i;if(n=f(n),t=n.length,t<=1)return n;while(t--)i=n[t],i.nodeType===so&&kc.call(n,t,1);return n}function wa(){var n={},t=!1,r=/^(\S+)(\s+as\s+(\w+))?$/;this.register=function(t,i){li(t,"controller");h(t)?a(n,t):n[t]=i};this.allowGlobals=function(){t=!0};this.$get=["$injector","$window",function(u,f){function e(n,t,i,r){if(!(n&&h(n.$scope)))throw v("$controller")("noscp","Cannot export controller '{0}' as '{1}'! No $scope object provided via `locals`.",r,t);n.$scope[t]=i}return function(s,h,l,v){var p,b,y,w,k;return(l=l===!0,v&&c(v)&&(w=v),c(s)&&(b=s.match(r),y=b[1],w=w||b[3],s=n.hasOwnProperty(y)?n[y]:oo(h.$scope,y,!0)||(t?oo(f,y,!0):i),nu(s,y,!0)),l)?(k=(o(s)?s[s.length-1]:s).prototype,p=Object.create(k||null),w&&e(h,w,p,y||s.name),a(function(){return u.invoke(s,p,h,y),p},{instance:p,identifier:w})):(p=u.instantiate(s,h,y),w&&e(h,w,p,y||s.name),p)}}]}function ba(){this.$get=["$window",function(n){return f(n.document)}]}function ka(){this.$get=["$log",function(n){return function(){n.error.apply(n,arguments)}}]}function df(n,t){var i,r;return c(n)&&(i=n.replace(nv,"").trim(),i&&(r=t("Content-Type"),(r&&r.indexOf(es)===0||tv(i))&&(n=to(i)))),n}function tv(n){var t=n.match(da);return t&&ga[t[0]].test(n)}function os(n){var t=ot(),i,u,f;return n?(r(n.split("\n"),function(n){f=n.indexOf(":");i=y(p(n.substr(0,f)));u=p(n.substr(f+1));i&&(t[i]=t[i]?t[i]+", "+u:u)}),t):t}function ss(n){var t=h(n)?n:i;return function(i){if(t||(t=os(n)),i){var r=t[y(i)];return r===void 0&&(r=null),r}return t}}function hs(n,t,i,u){return l(u)?u(n,t,i):(r(u,function(r){n=r(n,t,i)}),n)}function gf(n){return 200<=n&&n<300}function iv(){var n=this.defaults={transformResponse:[df],transformRequest:[function(n){return h(n)&&!tl(n)&&!rl(n)&&!il(n)?ur(n):n}],headers:{common:{Accept:"application/json, text/plain, */*"},post:at(kf),put:at(kf),patch:at(kf)},xsrfCookieName:"XSRF-TOKEN",xsrfHeaderName:"X-XSRF-TOKEN"},t=!1,f;this.useApplyAsync=function(n){return u(n)?(t=!!n,this):t};f=this.interceptors=[];this.$get=["$httpBackend","$browser","$cacheFactory","$rootScope","$q","$injector",function(s,p,w,b,k,d){function g(t){function c(n){var t=a({},n);return t.data=n.data?hs(n.data,n.headers,n.status,f.transformResponse):n.data,gf(n.status)?t:k.reject(t)}function w(n){var t,i={};return r(n,function(n,r){l(n)?(t=n(),t!=null&&(i[r]=t)):i[r]=n}),i}function b(t){var i=n.headers,u=a({},t.headers),r,f,e;i=a({},i.common,i[y(t.method)]);n:for(r in i){f=y(r);for(e in u)if(y(e)===f)continue n;u[r]=i[r]}return w(u)}var f,s,h;if(!ft.isObject(t))throw v("$http")("badreq","Http request configuration must be an object.  Received: {0}",t);f=a({method:"get",transformRequest:n.transformRequest,transformResponse:n.transformResponse},t);f.headers=b(t);f.method=bi(f.method);var p=function(t){var u=t.headers,f=hs(t.data,ss(u),i,t.transformRequest);return e(f)&&r(u,function(n,t){y(t)==="content-type"&&delete u[t]}),e(t.withCredentials)&&!e(n.withCredentials)&&(t.withCredentials=n.withCredentials),ut(t,f).then(c,c)},o=[p,i],u=k.when(f);for(r(nt,function(n){(n.request||n.requestError)&&o.unshift(n.request,n.requestError);(n.response||n.responseError)&&o.push(n.response,n.responseError)});o.length;)s=o.shift(),h=o.shift(),u=u.then(s,h);return u.success=function(n){return u.then(function(t){n(t.data,t.status,t.headers,f)}),u},u.error=function(n){return u.then(null,function(t){n(t.data,t.status,t.headers,f)}),u},u}function it(){r(arguments,function(n){g[n]=function(t,i){return g(a(i||{},{method:n,url:t}))}})}function rt(){r(arguments,function(n){g[n]=function(t,i,r){return g(a(r||{},{method:n,url:t,data:i}))}})}function ut(r,f){function ut(n,i,r,u){function f(){v(i,n,r,u)}l&&(gf(n)?l.put(a,[n,i,os(r),u]):l.remove(a));t?b.$applyAsync(f):(f(),b.$$phase||b.$apply())}function v(n,t,i,u){t=Math.max(t,0);(gf(t)?y.resolve:y.reject)({data:n,status:t,headers:ss(i),config:r,statusText:u})}function it(n){v(n.data,n.status,at(n.headers()),n.statusText)}function rt(){var n=g.pendingRequests.indexOf(r);n!==-1&&g.pendingRequests.splice(n,1)}var y=k.defer(),w=y.promise,l,c,nt=r.headers,a=et(r.url,r.params),d;return g.pendingRequests.push(r),w.then(rt,rt),(r.cache||n.cache)&&r.cache!==!1&&(r.method==="GET"||r.method==="JSONP")&&(l=h(r.cache)?r.cache:h(n.cache)?n.cache:tt),l&&(c=l.get(a),u(c)?dr(c)?c.then(it,it):o(c)?v(c[1],c[0],at(c[2]),c[3]):v(c,200,{},"OK"):l.put(a,w)),e(c)&&(d=th(r.url)?p.cookies()[r.xsrfCookieName||n.xsrfCookieName]:i,d&&(nt[r.xsrfHeaderName||n.xsrfHeaderName]=d),s(r.method,a,f,ut,nt,r.timeout,r.withCredentials,r.responseType)),w}function et(n,t){if(!t)return n;var i=[];return nl(t,function(n,t){n===null||e(n)||(o(n)||(n=[n]),r(n,function(n){h(n)&&(n=lt(n)?n.toISOString():ur(n));i.push(ii(t)+"="+ii(n))}))}),i.length>0&&(n+=(n.indexOf("?")==-1?"?":"&")+i.join("&")),n}var tt=w("$http"),nt=[];return r(f,function(n){nt.unshift(c(n)?d.get(n):d.invoke(n))}),g.pendingRequests=[],it("get","delete","head","jsonp"),rt("post","put","patch"),g.defaults=n,g}]}function rv(){return new n.XMLHttpRequest}function uv(){this.$get=["$browser","$window","$document",function(n,t,i){return fv(n,rv,n.defer,t.angular.callbacks,i[0])}]}function fv(n,t,f,e,o){function h(n,t,i){var r=o.createElement("script"),u=null;return r.type="text/javascript",r.src=n,r.async=!0,u=function(n){er(r,"load",u);er(r,"error",u);o.body.removeChild(r);r=null;var f=-1,s="unknown";n&&(n.type!=="load"||e[t].called||(n={type:"error"}),s=n.type,f=n.type==="error"?404:200);i&&i(f,s)},uu(r,"load",u),uu(r,"error",u),o.body.appendChild(r),u}return function(o,c,l,a,v,p,w,b){function rt(){g&&g();k&&k.abort()}function it(t,r,u,e,o){tt!==i&&f.cancel(tt);g=k=null;t(r,u,e,o);n.$$completeOutstandingRequest(s)}var d,g,k,nt,tt;if(n.$$incOutstandingRequestCount(),c=c||n.url(),y(o)=="jsonp")d="_"+(e.counter++).toString(36),e[d]=function(n){e[d].data=n;e[d].called=!0},g=h(c.replace("JSON_CALLBACK","angular.callbacks."+d),d,function(n,t){it(a,n,e[d].data,"",t);e[d]=s});else{if(k=t(),k.open(o,c,!0),r(v,function(n,t){u(n)&&k.setRequestHeader(t,n)}),k.onload=function(){var i=k.statusText||"",t="response"in k?k.response:k.responseText,n=k.status===1223?204:k.status;n===0&&(n=t?200:gt(c).protocol=="file"?404:0);it(a,n,t,k.getAllResponseHeaders(),i)},nt=function(){it(a,-1,null,null,"")},k.onerror=nt,k.onabort=nt,w&&(k.withCredentials=!0),b)try{k.responseType=b}catch(ut){if(b!=="json")throw ut;}k.send(l||null)}p>0?tt=f(rt,p):dr(p)&&p.then(rt)}}function ev(){var n="{{",t="}}";this.startSymbol=function(t){return t?(n=t,this):n};this.endSymbol=function(n){return n?(t=n,this):t};this.$get=["$parse","$exceptionHandler","$sce",function(i,r,f){function h(n){return"\\\\\\"+n}function o(o,h,p,w){function et(i){return i.replace(v,n).replace(y,t)}function ht(n){try{return n=ot(n),w&&!u(n)?n:st(n)}catch(t){var i=au("interr","Can't interpolate: {0}\n{1}",o,t.toString());r(i)}}w=!!w;for(var d,nt,b=0,g=[],tt=[],rt=o.length,it,k=[],ut=[];b<rt;)if((d=o.indexOf(n,b))!=-1&&(nt=o.indexOf(t,d+s))!=-1)b!==d&&k.push(et(o.substring(b,d))),it=o.substring(d+s,nt),g.push(it),tt.push(i(it,ht)),b=nt+c,ut.push(k.length),k.push("");else{b!==rt&&k.push(et(o.substring(b)));break}if(p&&k.length>1)throw au("noconcat","Error while interpolating: {0}\nStrict Contextual Escaping disallows interpolations that concatenate multiple expressions when a trusted value is required.  See http://docs.angularjs.org/api/ng.$sce",o);if(!h||g.length){var ft=function(n){for(var t=0,i=g.length;t<i;t++){if(w&&e(n[t]))return;k[ut[t]]=n[t]}return k.join("")},ot=function(n){return p?f.getTrusted(p,n):f.valueOf(n)},st=function(n){if(n==null)return"";switch(typeof n){case"string":break;case"number":n=""+n;break;default:n=ur(n)}return n};return a(function(n){var t=0,i=g.length,u=new Array(i),f;try{for(;t<i;t++)u[t]=tt[t](n);return ft(u)}catch(e){f=au("interr","Can't interpolate: {0}\n{1}",o,e.toString());r(f)}},{exp:o,expressions:g,$$watchDelegate:function(n,t,i){var r;return n.$watchGroup(tt,function(i,u){var f=ft(i);l(t)&&t.call(this,f,i!==u?r:f,n);r=f},i)}})}}var s=n.length,c=t.length,v=new RegExp(n.replace(/./g,h),"g"),y=new RegExp(t.replace(/./g,h),"g");return o.startSymbol=function(){return n},o.endSymbol=function(){return t},o}]}function ov(){this.$get=["$rootScope","$window","$q","$$q",function(n,t,i,r){function e(e,o,s,h){var y=t.setInterval,p=t.clearInterval,a=0,v=u(h)&&!h,l=(v?r:i).defer(),c=l.promise;return s=u(s)?s:0,c.then(null,null,e),c.$$intervalId=y(function(){l.notify(a++);s>0&&a>=s&&(l.resolve(a),p(c.$$intervalId),delete f[c.$$intervalId]);v||n.$apply()},o),f[c.$$intervalId]=l,c}var f={};return e.cancel=function(n){return n&&n.$$intervalId in f?(f[n.$$intervalId].reject("canceled"),t.clearInterval(n.$$intervalId),delete f[n.$$intervalId],!0):!1},e}]}function sv(){this.$get=function(){return{id:"en-us",NUMBER_FORMATS:{DECIMAL_SEP:".",GROUP_SEP:",",PATTERNS:[{minInt:1,minFrac:0,maxFrac:3,posPre:"",posSuf:"",negPre:"-",negSuf:"",gSize:3,lgSize:3},{minInt:1,minFrac:2,maxFrac:2,posPre:"¤",posSuf:"",negPre:"(¤",negSuf:")",gSize:3,lgSize:3}],CURRENCY_SYM:"$"},DATETIME_FORMATS:{MONTH:"January,February,March,April,May,June,July,August,September,October,November,December".split(","),SHORTMONTH:"Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec".split(","),DAY:"Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday".split(","),SHORTDAY:"Sun,Mon,Tue,Wed,Thu,Fri,Sat".split(","),AMPMS:["AM","PM"],medium:"MMM d, y h:mm:ss a",short:"M/d/yy h:mm a",fullDate:"EEEE, MMMM d, y",longDate:"MMMM d, y",mediumDate:"MMM d, y",shortDate:"M/d/yy",mediumTime:"h:mm:ss a",shortTime:"h:mm a"},pluralCat:function(n){return n===1?"one":"other"}}}}function ne(n){for(var t=n.split("/"),i=t.length;i--;)t[i]=gr(t[i]);return t.join("/")}function cs(n,t){var i=gt(n);t.$$protocol=i.protocol;t.$$host=i.hostname;t.$$port=g(i.port)||cv[i.protocol]||null}function ls(n,t){var r=n.charAt(0)!=="/",i;r&&(n="/"+n);i=gt(n);t.$$path=decodeURIComponent(r&&i.pathname.charAt(0)==="/"?i.pathname.substring(1):i.pathname);t.$$search=ro(i.search);t.$$hash=decodeURIComponent(i.hash);t.$$path&&t.$$path.charAt(0)!="/"&&(t.$$path="/"+t.$$path)}function kt(n,t){if(t.indexOf(n)===0)return t.substr(n.length)}function fi(n){var t=n.indexOf("#");return t==-1?n:n.substr(0,t)}function as(n){return n.replace(/(#.+)|#$/,"$1")}function te(n){return n.substr(0,fi(n).lastIndexOf("/")+1)}function lv(n){return n.substring(0,n.indexOf("/",n.indexOf("//")+2))}function ie(n,t){this.$$html5=!0;t=t||"";var r=te(n);cs(n,this);this.$$parse=function(n){var t=kt(r,n);if(!c(t))throw vu("ipthprfx",'Invalid url "{0}", missing path prefix "{1}".',n,r);ls(t,this);this.$$path||(this.$$path="/");this.$$compose()};this.$$compose=function(){var n=rf(this.$$search),t=this.$$hash?"#"+gr(this.$$hash):"";this.$$url=ne(this.$$path)+(n?"?"+n:"")+t;this.$$absUrl=r+this.$$url.substr(1)};this.$$parseLinkUrl=function(u,f){if(f&&f[0]==="#")return this.hash(f.slice(1)),!0;var e,s,o;return(e=kt(n,u))!==i?(s=e,o=(e=kt(t,e))!==i?r+(kt("/",e)||e):n+s):(e=kt(r,u))!==i?o=r+e:r==u+"/"&&(o=r),o&&this.$$parse(o),!!o}}function re(n,t){var i=te(n);cs(n,this);this.$$parse=function(r){function o(n,t,i){var u=/^\/[A-Z]:(\/.*)/,r;return(t.indexOf(i)===0&&(t=t.replace(i,"")),u.exec(t))?n:(r=u.exec(n),r?r[1]:n)}var f=kt(n,r)||kt(i,r),u;f.charAt(0)==="#"?(u=kt(t,f),e(u)&&(u=f)):u=this.$$html5?f:"";ls(u,this);this.$$path=o(this.$$path,u,n);this.$$compose()};this.$$compose=function(){var i=rf(this.$$search),r=this.$$hash?"#"+gr(this.$$hash):"";this.$$url=ne(this.$$path)+(i?"?"+i:"")+r;this.$$absUrl=n+(this.$$url?t+this.$$url:"")};this.$$parseLinkUrl=function(t){return fi(n)==fi(t)?(this.$$parse(t),!0):!1}}function vs(n,t){this.$$html5=!0;re.apply(this,arguments);var i=te(n);this.$$parseLinkUrl=function(r,u){if(u&&u[0]==="#")return this.hash(u.slice(1)),!0;var f,e;return n==fi(r)?f=r:(e=kt(i,r))?f=n+t+e:i===r+"/"&&(f=i),f&&this.$$parse(f),!!f};this.$$compose=function(){var i=rf(this.$$search),r=this.$$hash?"#"+gr(this.$$hash):"";this.$$url=ne(this.$$path)+(i?"?"+i:"")+r;this.$$absUrl=n+t+this.$$url}}function yu(n){return function(){return this[n]}}function ps(n,t){return function(i){return e(i)?this[n]:(this[n]=t(i),this.$$compose(),this)}}function av(){var t="",n={enabled:!1,requireBase:!0,rewriteLinks:!0};this.hashPrefix=function(n){return u(n)?(t=n,this):t};this.html5Mode=function(t){return tr(t)?(n.enabled=t,this):h(t)?(tr(t.enabled)&&(n.enabled=t.enabled),tr(t.requireBase)&&(n.requireBase=t.requireBase),tr(t.rewriteLinks)&&(n.rewriteLinks=t.rewriteLinks),this):n};this.$get=["$rootScope","$browser","$sniffer","$rootElement","$window",function(i,r,u,e,o){function w(n,t,i){var u=s.url(),f=s.$$state;try{r.url(n,t,i);s.$$state=r.state()}catch(e){s.url(u);s.$$state=f;throw e;}}function b(n,t){i.$broadcast("$locationChangeSuccess",s.absUrl(),n,s.$$state,t)}var s,a,y=r.baseHref(),c=r.url(),v,p,l;if(n.enabled){if(!y&&n.requireBase)throw vu("nobase","$location in HTML5 mode requires a <base> tag to be present!");v=lv(c)+(y||"/");a=u.history?ie:vs}else v=fi(c),a=re;s=new a(v,"#"+t);s.$$parseLinkUrl(c,c);s.$$state=r.state();p=/^\s*(javascript|mailto):/i;e.on("click",function(t){var u,c,l;if(n.rewriteLinks&&!t.ctrlKey&&!t.metaKey&&!t.shiftKey&&t.which!=2&&t.button!=2){for(u=f(t.target);pt(u[0])!=="a";)if(u[0]===e[0]||!(u=u.parent())[0])return;(c=u.prop("href"),l=u.attr("href")||u.attr("xlink:href"),h(c)&&c.toString()==="[object SVGAnimatedString]"&&(c=gt(c.animVal).href),p.test(c))||!c||u.attr("target")||t.isDefaultPrevented()||s.$$parseLinkUrl(c,l)&&(t.preventDefault(),s.absUrl()!=r.url()&&(i.$apply(),o.angular["ff-684208-preventDefault"]=!0))}});s.absUrl()!=c&&r.url(s.absUrl(),!0);l=!0;r.onUrlChange(function(n,t){i.$evalAsync(function(){var r=s.absUrl(),u=s.$$state,f;(s.$$parse(n),s.$$state=t,f=i.$broadcast("$locationChangeStart",n,r,t,u).defaultPrevented,s.absUrl()===n)&&(f?(s.$$parse(r),s.$$state=u,w(r,!1,u)):(l=!1,b(r,u)))});i.$$phase||i.$digest()});return i.$watch(function(){var t=as(r.url()),e=as(s.absUrl()),n=r.state(),o=s.$$replace,f=t!==e||s.$$html5&&u.history&&n!==s.$$state;(l||f)&&(l=!1,i.$evalAsync(function(){var r=s.absUrl(),u=i.$broadcast("$locationChangeStart",r,t,s.$$state,n).defaultPrevented;s.absUrl()===r&&(u?(s.$$parse(t),s.$$state=n):(f&&w(r,o,n===s.$$state?null:s.$$state),b(t,n)))}));s.$$replace=!1}),s}]}function vv(){var n=!0,t=this;this.debugEnabled=function(t){return u(t)?(n=t,this):n};this.$get=["$window",function(i){function f(n){return n instanceof Error&&(n.stack?n=n.message&&n.stack.indexOf(n.message)===-1?"Error: "+n.message+"\n"+n.stack:n.stack:n.sourceURL&&(n=n.message+"\n"+n.sourceURL+":"+n.line)),n}function u(n){var t=i.console||{},u=t[n]||t.log||s,e=!1;try{e=!!u.apply}catch(o){}return e?function(){var n=[];return r(arguments,function(t){n.push(f(t))}),u.apply(t,n)}:function(n,t){u(n,t==null?"":t)}}return{log:u("log"),info:u("info"),warn:u("warn"),error:u("error"),debug:function(){var i=u("debug");return function(){n&&i.apply(t,arguments)}}()}}]}function yt(n,t){if(n==="__defineGetter__"||n==="__defineSetter__"||n==="__lookupGetter__"||n==="__lookupSetter__"||n==="__proto__")throw it("isecfld","Attempting to access a disallowed field in Angular expressions! Expression: {0}",t);return n}function ht(n,t){if(n)if(n.constructor===n)throw it("isecfn","Referencing Function in Angular expressions is disallowed! Expression: {0}",t);else if(n.window===n)throw it("isecwindow","Referencing the Window in Angular expressions is disallowed! Expression: {0}",t);else if(n.children&&(n.nodeName||n.prop&&n.attr&&n.find))throw it("isecdom","Referencing DOM nodes in Angular expressions is disallowed! Expression: {0}",t);else if(n===Object)throw it("isecobj","Referencing Object in Angular expressions is disallowed! Expression: {0}",t);return n}function bv(n,t){if(n)if(n.constructor===n)throw it("isecfn","Referencing Function in Angular expressions is disallowed! Expression: {0}",t);else if(n===yv||n===pv||n===wv)throw it("isecff","Referencing call, apply or bind in Angular expressions is disallowed! Expression: {0}",t);}function fe(n){return n.constant}function lr(n,t,i,r,u){var o,f,s,e;for(ht(n,u),ht(t,u),o=i.split("."),s=0;o.length>1;s++)f=yt(o.shift(),u),e=s===0&&t&&t[f]||n[f],e||(e={},n[f]=e),n=ht(e,u);return f=yt(o.shift(),u),ht(n[f],u),n[f]=r,r}function pi(n){return n=="constructor"}function ks(n,t,r,u,f,e,o){yt(n,e);yt(t,e);yt(r,e);yt(u,e);yt(f,e);var s=function(n){return ht(n,e)},h=o||pi(n)?s:ct,c=o||pi(t)?s:ct,l=o||pi(r)?s:ct,a=o||pi(u)?s:ct,v=o||pi(f)?s:ct;return function(e,o){var s=o&&o.hasOwnProperty(n)?o:e;return s==null?s:(s=h(s[n]),!t)?s:s==null?i:(s=c(s[t]),!r)?s:s==null?i:(s=l(s[r]),!u)?s:s==null?i:(s=a(s[u]),!f)?s:s==null?i:v(s[f])}}function dv(n,t){return function(i,r){return n(i,r,ht,t)}}function gv(n,t,u){var o=t.expensiveChecks,a=o?bs:ws,e=a[n],f,c,s,l,h;return e?e:(f=n.split("."),c=f.length,t.csp?e=c<6?ks(f[0],f[1],f[2],f[3],f[4],u,o):function(n,t){var r=0,e;do e=ks(f[r++],f[r++],f[r++],f[r++],f[r++],u,o)(n,t),t=i,n=e;while(r<c);return e}:(s="",o&&(s+="s = eso(s, fe);\nl = eso(l, fe);\n"),l=o,r(f,function(n,t){yt(n,u);var i=(t?"s":'((l&&l.hasOwnProperty("'+n+'"))?l:s)')+"."+n;(o||pi(n))&&(i="eso("+i+", fe)",l=!0);s+="if(s == null) return undefined;\ns="+i+";\n"}),s+="return s;",h=new Function("s","l","eso","fe",s),h.toString=nt(s),l&&(h=dv(h,u)),e=h),e.sharedGetter=!0,e.assign=function(t,i,r){return lr(t,r,n,i,n)},a[n]=e,e)}function ee(n){return l(n.valueOf)?n.valueOf():ds.call(n)}function ny(){var n=ot(),t=ot();this.$get=["$filter","$sniffer",function(i,f){function w(n){var t=n;return n.sharedGetter&&(t=function(t,i){return n(t,i)},t.literal=n.literal,t.constant=n.constant,t.assign=n.assign),t}function c(n,t){for(var i,r=0,u=n.length;r<u;r++)i=n[r],i.constant||(i.inputs?c(i.inputs,t):t.indexOf(i)===-1&&t.push(i));return t}function e(n,t){return n==null||t==null?n===t:typeof n=="object"&&(n=ee(n),typeof n=="object")?!1:n===t||n!==n&&t!==t}function o(n,t,i,r){var u=r.$$inputs||(r.$$inputs=c(r.inputs,[])),f,h,o,s,l;if(u.length===1)return h=e,u=u[0],n.$watch(function(n){var t=u(n);return e(t,h)||(f=r(n),h=t&&ee(t)),f},t,i);for(o=[],s=0,l=u.length;s<l;s++)o[s]=e;return n.$watch(function(n){for(var i,s=!1,t=0,h=u.length;t<h;t++)i=u[t](n),(s||(s=!e(i,o[t])))&&(o[t]=i&&ee(i));return s&&(f=r(n)),f},t,i)}function a(n,t,i,r){var f,e;return f=n.$watch(function(n){return r(n)},function(n,i,r){e=n;l(t)&&t.apply(this,arguments);u(n)&&r.$$postDigest(function(){u(e)&&f()})},i)}function v(n,t,i,f){function s(n){var t=!0;return r(n,function(n){u(n)||(t=!1)}),t}var e,o;return e=n.$watch(function(n){return f(n)},function(n,i,r){o=n;l(t)&&t.call(this,n,i,r);s(n)&&r.$$postDigest(function(){s(o)&&e()})},i)}function b(n,t,i,r){var u;return u=n.$watch(function(n){return r(n)},function(){l(t)&&t.apply(this,arguments);u()},i)}function h(n,t){if(!t)return n;var r=n.$$watchDelegate,f=r!==v&&r!==a,i=f?function(i,r){var u=n(i,r);return t(u,i,r)}:function(i,r){var f=n(i,r),e=t(f,i,r);return u(f)?e:f};return n.$$watchDelegate&&n.$$watchDelegate!==o?i.$$watchDelegate=n.$$watchDelegate:t.$stateful||(i.$$watchDelegate=o,i.inputs=[n]),i}var y={csp:f.csp,expensiveChecks:!1},p={csp:f.csp,expensiveChecks:!0};return function(r,u,f){var e,k,c,l;switch(typeof r){case"string":if(c=r=r.trim(),l=f?t:n,e=l[c],!e){r.charAt(0)===":"&&r.charAt(1)===":"&&(k=!0,r=r.substring(2));var d=f?p:y,g=new ue(d),nt=new yi(g,i,d);e=nt.parse(r);e.constant?e.$$watchDelegate=b:k?(e=w(e),e.$$watchDelegate=e.literal?v:a):e.inputs&&(e.$$watchDelegate=o);l[c]=e}return h(e,u);case"function":return h(r,u);default:return h(s,u)}}}]}function ty(){this.$get=["$rootScope","$exceptionHandler",function(n,t){return gs(function(t){n.$evalAsync(t)},t)}]}function iy(){this.$get=["$browser","$exceptionHandler",function(n,t){return gs(function(t){n.defer(t)},t)}]}function gs(n,t){function k(n,t,i){function u(t){return function(i){r||(r=!0,t.call(n,i))}}var r=!1;return[u(t),u(i)]}function y(){this.$$state={status:0}}function s(n,t){return function(i){t.call(n,i)}}function g(n){var e,r,f,u,o;for(f=n.pending,n.processScheduled=!1,n.pending=i,u=0,o=f.length;u<o;++u){r=f[u][0];e=f[u][n.status];try{l(e)?r.resolve(e(n.value)):n.status===1?r.resolve(n.value):r.reject(n.value)}catch(s){r.reject(s);t(s)}}}function c(t){!t.processScheduled&&t.pending&&(t.processScheduled=!0,n(function(){g(t)}))}function u(){this.promise=new y;this.resolve=s(this,this.resolve);this.reject=s(this,this.reject);this.notify=s(this,this.notify)}function tt(n){var i=new u,f=0,t=o(n)?[]:{};return r(n,function(n,r){f++;w(n).then(function(n){t.hasOwnProperty(r)||(t[r]=n,--f||i.resolve(t))},function(n){t.hasOwnProperty(r)||i.reject(n)})}),f===0&&i.resolve(t),i.promise}var a=v("$q",TypeError),d=function(){return new u},f;y.prototype={then:function(n,t,i){var r=new u;return this.$$state.pending=this.$$state.pending||[],this.$$state.pending.push([r,n,t,i]),this.$$state.status>0&&c(this.$$state),r.promise},"catch":function(n){return this.then(null,n)},"finally":function(n,t){return this.then(function(t){return p(t,!0,n)},function(t){return p(t,!1,n)},t)}};u.prototype={resolve:function(n){this.promise.$$state.status||(n===this.promise?this.$$reject(a("qcycle","Expected promise to be resolved with value other than itself '{0}'",n)):this.$$resolve(n))},$$resolve:function(n){var i,r=k(this,this.$$resolve,this.$$reject);try{(h(n)||l(n))&&(i=n&&n.then);l(i)?(this.promise.$$state.status=-1,i.call(n,r[0],r[1],this.notify)):(this.promise.$$state.value=n,this.promise.$$state.status=1,c(this.promise.$$state))}catch(u){r[1](u);t(u)}},reject:function(n){this.promise.$$state.status||this.$$reject(n)},$$reject:function(n){this.promise.$$state.value=n;this.promise.$$state.status=2;c(this.promise.$$state)},notify:function(i){var r=this.promise.$$state.pending;this.promise.$$state.status<=0&&r&&r.length&&n(function(){for(var u,f,n=0,e=r.length;n<e;n++){f=r[n][0];u=r[n][3];try{f.notify(l(u)?u(i):i)}catch(o){t(o)}}})}};var nt=function(n){var t=new u;return t.reject(n),t.promise},e=function(n,t){var i=new u;return t?i.resolve(n):i.reject(n),i.promise},p=function(n,t,i){var r=null;try{l(i)&&(r=i())}catch(u){return e(u,!1)}return dr(r)?r.then(function(){return e(n,t)},function(n){return e(n,!1)}):e(n,t)},w=function(n,t,i,r){var f=new u;return f.resolve(n),f.promise.then(t,i,r)};return f=function b(n){function i(n){t.resolve(n)}function r(n){t.reject(n)}if(!l(n))throw a("norslvr","Expected resolverFn, got '{0}'",n);if(!(this instanceof b))return new b(n);var t=new u;return n(i,r),t.promise},f.defer=d,f.reject=nt,f.when=w,f.all=tt,f}function ry(){this.$get=["$window","$timeout",function(n,t){var i=n.requestAnimationFrame||n.webkitRequestAnimationFrame,f=n.cancelAnimationFrame||n.webkitCancelAnimationFrame||n.webkitCancelRequestAnimationFrame,r=!!i,u=r?function(n){var t=i(n);return function(){f(t)}}:function(n){var i=t(n,16.66,!1);return function(){t.cancel(i)}};return u.supported=r,u}]}function uy(){var i=10,u=v("$rootScope"),n=null,t=null;this.digestTtl=function(n){return arguments.length&&(i=n),i};this.$get=["$injector","$exceptionHandler","$parse","$browser",function(f,o,c,a){function p(){this.$id=br();this.$$phase=this.$parent=this.$$watchers=this.$$nextSibling=this.$$prevSibling=this.$$childHead=this.$$childTail=null;this.$root=this;this.$$destroyed=!1;this.$$listeners={};this.$$listenerCount={};this.$$isolateBindings=null}function d(n){if(v.$$phase)throw u("inprog","{0} already in progress",v.$$phase);v.$$phase=n}function k(){v.$$phase=null}function g(n,t,i){do n.$$listenerCount[i]-=t,n.$$listenerCount[i]===0&&delete n.$$listenerCount[i];while(n=n.$parent)}function nt(){}function tt(){while(b.length)try{b.shift()()}catch(n){o(n)}t=null}function it(){t===null&&(t=a.defer(function(){v.$apply(tt)}))}p.prototype={constructor:p,$new:function(n,t){function r(){i.$$destroyed=!0}var i;return t=t||this,n?(i=new p,i.$root=this.$root):(this.$$ChildScope||(this.$$ChildScope=function(){this.$$watchers=this.$$nextSibling=this.$$childHead=this.$$childTail=null;this.$$listeners={};this.$$listenerCount={};this.$id=br();this.$$ChildScope=null},this.$$ChildScope.prototype=this),i=new this.$$ChildScope),i.$parent=t,i.$$prevSibling=t.$$childTail,t.$$childHead?(t.$$childTail.$$nextSibling=i,t.$$childTail=i):t.$$childHead=t.$$childTail=i,(n||t!=this)&&i.$on("$destroy",r),i},$watch:function(t,i,r){var u=c(t);if(u.$$watchDelegate)return u.$$watchDelegate(this,i,r,u);var o=this,f=o.$$watchers,e={fn:i,last:nt,get:u,exp:t,eq:!!r};return n=null,l(i)||(e.fn=s),f||(f=o.$$watchers=[]),f.unshift(e),function(){ir(f,e);n=null}},$watchGroup:function(n,t){function c(){o=!1;h?(h=!1,t(i,i,u)):t(i,f,u)}var f=new Array(n.length),i=new Array(n.length),e=[],u=this,o=!1,h=!0,s;return n.length?n.length===1?this.$watch(n[0],function(n,r,u){i[0]=n;f[0]=r;t(i,n===r?i:f,u)}):(r(n,function(n,t){var r=u.$watch(n,function(n,r){i[t]=n;f[t]=r;o||(o=!0,u.$evalAsync(c))});e.push(r)}),function(){while(e.length)e.shift()()}):(s=!0,u.$evalAsync(function(){s&&t(i,i,u)}),function(){s=!1})},$watchCollection:function(n,t){function y(n){var c,o,y,t,s,v;if(i=n,!e(i)){if(h(i))if(di(i))for(r!==l&&(r=l,f=r.length=0,u++),c=i.length,f!==c&&(u++,r.length=f=c),v=0;v<c;v++)s=r[v],t=i[v],y=s!==s&&t!==t,y||s===t||(u++,r[v]=t);else{r!==a&&(r=a={},f=0,u++);c=0;for(o in i)i.hasOwnProperty(o)&&(c++,t=i[o],s=r[o],o in r?(y=s!==s&&t!==t,y||s===t||(u++,r[o]=t)):(f++,r[o]=t,u++));if(f>c){u++;for(o in r)i.hasOwnProperty(o)||(f--,delete r[o])}}else r!==i&&(r=i,u++);return u}}function b(){var n,r;if(v?(v=!1,t(i,i,s)):t(i,o,s),p)if(h(i))if(di(i))for(o=new Array(i.length),n=0;n<i.length;n++)o[n]=i[n];else{o={};for(r in i)ye.call(i,r)&&(o[r]=i[r])}else o=i}y.$stateful=!0;var s=this,i,r,o,p=t.length>1,u=0,w=c(n,y),l=[],a={},v=!0,f=0;return this.$watch(w,b)},$digest:function(){var r,e,s,g,it,h,rt=i,ut,f,ft=this,c=[],p,b;d("$digest");a.$$checkUrlChange();this===v&&t!==null&&(a.defer.cancel(t),tt());n=null;do{for(h=!1,f=ft;y.length;){try{b=y.shift();b.scope.$eval(b.expression,b.locals)}catch(ot){o(ot)}n=null}n:do{if(g=f.$$watchers)for(it=g.length;it--;)try{if(r=g[it],r)if((e=r.get(f))===(s=r.last)||(r.eq?et(e,s):typeof e=="number"&&typeof s=="number"&&isNaN(e)&&isNaN(s))){if(r===n){h=!1;break n}}else h=!0,n=r,r.last=r.eq?ti(e,null):e,r.fn(e,s===nt?e:s,f),rt<5&&(p=4-rt,c[p]||(c[p]=[]),c[p].push({msg:l(r.exp)?"fn: "+(r.exp.name||r.exp.toString()):r.exp,newVal:e,oldVal:s}))}catch(ot){o(ot)}if(!(ut=f.$$childHead||f!==ft&&f.$$nextSibling))while(f!==ft&&!(ut=f.$$nextSibling))f=f.$parent}while(f=ut);if((h||y.length)&&!rt--){k();throw u("infdig","{0} $digest() iterations reached. Aborting!\nWatchers fired in the last 5 iterations: {1}",i,c);}}while(h||y.length);for(k();w.length;)try{w.shift()()}catch(ot){o(ot)}},$destroy:function(){var n,t;if(!this.$$destroyed&&(n=this.$parent,this.$broadcast("$destroy"),this.$$destroyed=!0,this!==v)){for(t in this.$$listenerCount)g(this,this.$$listenerCount[t],t);n.$$childHead==this&&(n.$$childHead=this.$$nextSibling);n.$$childTail==this&&(n.$$childTail=this.$$prevSibling);this.$$prevSibling&&(this.$$prevSibling.$$nextSibling=this.$$nextSibling);this.$$nextSibling&&(this.$$nextSibling.$$prevSibling=this.$$prevSibling);this.$destroy=this.$digest=this.$apply=this.$evalAsync=this.$applyAsync=s;this.$on=this.$watch=this.$watchGroup=function(){return s};this.$$listeners={};this.$parent=this.$$nextSibling=this.$$prevSibling=this.$$childHead=this.$$childTail=this.$root=this.$$watchers=null}},$eval:function(n,t){return c(n)(this,t)},$evalAsync:function(n,t){v.$$phase||y.length||a.defer(function(){y.length&&v.$digest()});y.push({scope:this,expression:n,locals:t})},$$postDigest:function(n){w.push(n)},$apply:function(n){try{return d("$apply"),this.$eval(n)}catch(t){o(t)}finally{k();try{v.$digest()}catch(t){o(t);throw t;}}},$applyAsync:function(n){function i(){t.$eval(n)}var t=this;n&&b.push(i);it()},$on:function(n,t){var r=this.$$listeners[n],i,u;r||(this.$$listeners[n]=r=[]);r.push(t);i=this;do i.$$listenerCount[n]||(i.$$listenerCount[n]=0),i.$$listenerCount[n]++;while(i=i.$parent);return u=this,function(){var i=r.indexOf(t);i!==-1&&(r[i]=null,g(u,1,n))}},$emit:function(n){var s=[],u,r=this,e=!1,t={name:n,targetScope:r,stopPropagation:function(){e=!0},preventDefault:function(){t.defaultPrevented=!0},defaultPrevented:!1},h=rr([t],arguments,1),i,f;do{for(u=r.$$listeners[n]||s,t.currentScope=r,i=0,f=u.length;i<f;i++){if(!u[i]){u.splice(i,1);i--;f--;continue}try{u[i].apply(null,h)}catch(c){o(c)}}if(e)return t.currentScope=null,t;r=r.$parent}while(r);return t.currentScope=null,t},$broadcast:function(n){var r=this,t=r,e=r,u={name:n,targetScope:r,preventDefault:function(){u.defaultPrevented=!0},defaultPrevented:!1},h,f,i,s;if(!r.$$listenerCount[n])return u;for(h=rr([u],arguments,1);t=e;){for(u.currentScope=t,f=t.$$listeners[n]||[],i=0,s=f.length;i<s;i++){if(!f[i]){f.splice(i,1);i--;s--;continue}try{f[i].apply(null,h)}catch(c){o(c)}}if(!(e=t.$$listenerCount[n]&&t.$$childHead||t!==r&&t.$$nextSibling))while(t!==r&&!(e=t.$$nextSibling))t=t.$parent}return u.currentScope=null,u}};var v=new p,y=v.$$asyncQueue=[],w=v.$$postDigestQueue=[],b=v.$$applyAsyncQueue=[];return v}]}function fy(){var n=/^\s*(https?|ftp|mailto|tel|file):/,t=/^\s*((https?|ftp|file|blob):|data:image\/)/;this.aHrefSanitizationWhitelist=function(t){return u(t)?(n=t,this):n};this.imgSrcSanitizationWhitelist=function(n){return u(n)?(t=n,this):t};this.$get=function(){return function(i,r){var f=r?t:n,u;return(u=gt(i).href,u!==""&&!u.match(f))?"unsafe:"+u:i}}}function ey(n){if(n==="self")return n;if(c(n)){if(n.indexOf("***")>-1)throw dt("iwcard","Illegal sequence *** in string matcher.  String: {0}",n);return n=nf(n).replace("\\*\\*",".*").replace("\\*","[^:/.?&;]*"),new RegExp("^"+n+"$")}if(kr(n))return new RegExp("^"+n.source+"$");throw dt("imatcher",'Matchers may only be "self", string patterns or RegExp objects');}function nh(n){var t=[];return u(n)&&r(n,function(n){t.push(ey(n))}),t}function oy(){this.SCE_CONTEXTS=rt;var n=["self"],t=[];this.resourceUrlWhitelist=function(t){return arguments.length&&(n=nh(t)),n};this.resourceUrlBlacklist=function(n){return arguments.length&&(t=nh(n)),t};this.$get=["$injector",function(r){function s(n,t){return n==="self"?th(t):!!n.exec(t.href)}function h(i){for(var e=gt(i.toString()),f=!1,r=0,u=n.length;r<u;r++)if(s(n[r],e)){f=!0;break}if(f)for(r=0,u=t.length;r<u;r++)if(s(t[r],e)){f=!1;break}return f}function f(n){var t=function(n){this.$$unwrapTrustedValue=function(){return n}};return n&&(t.prototype=new n),t.prototype.valueOf=function(){return this.$$unwrapTrustedValue()},t.prototype.toString=function(){return this.$$unwrapTrustedValue().toString()},t}function c(n,t){var r=u.hasOwnProperty(n)?u[n]:null;if(!r)throw dt("icontext","Attempted to trust a value in invalid context. Context: {0}; Value: {1}",n,t);if(t===null||t===i||t==="")return t;if(typeof t!="string")throw dt("itype","Attempted to trust a non-string value in a content requiring a string: Context: {0}",n);return new r(t)}function l(n){return n instanceof e?n.$$unwrapTrustedValue():n}function a(n,t){if(t===null||t===i||t==="")return t;var r=u.hasOwnProperty(n)?u[n]:null;if(r&&t instanceof r)return t.$$unwrapTrustedValue();if(n===rt.RESOURCE_URL){if(h(t))return t;throw dt("insecurl","Blocked loading resource from url not allowed by $sceDelegate policy.  URL: {0}",t.toString());}else if(n===rt.HTML)return o(t);throw dt("unsafe","Attempting to use an unsafe value in a safe context.");}var o=function(){throw dt("unsafe","Attempting to use an unsafe value in a safe context.");},e,u;return r.has("$sanitize")&&(o=r.get("$sanitize")),e=f(),u={},u[rt.HTML]=f(e),u[rt.CSS]=f(e),u[rt.URL]=f(e),u[rt.JS]=f(e),u[rt.RESOURCE_URL]=f(u[rt.URL]),{trustAs:c,getTrusted:a,valueOf:l}}]}function sy(){var n=!0;this.enabled=function(t){return arguments.length&&(n=!!t),n};this.$get=["$parse","$sceDelegate",function(t,i){var u;if(n&&si<8)throw dt("iequirks","Strict Contextual Escaping does not support Internet Explorer version < 11 in quirks mode.  You can fix this by adding the text <!doctype html> to the top of your HTML document.  See http://docs.angularjs.org/api/ng.$sce for more information.");u=at(rt);u.isEnabled=function(){return n};u.trustAs=i.trustAs;u.getTrusted=i.getTrusted;u.valueOf=i.valueOf;n||(u.trustAs=u.getTrusted=function(n,t){return t},u.valueOf=ct);u.parseAs=function(n,i){var r=t(i);return r.literal&&r.constant?r:t(i,function(t){return u.getTrusted(n,t)})};var f=u.parseAs,e=u.getTrusted,o=u.trustAs;return r(rt,function(n,t){var i=y(t);u[or("parse_as_"+i)]=function(t){return f(n,t)};u[or("get_trusted_"+i)]=function(t){return e(n,t)};u[or("trust_as_"+i)]=function(t){return o(n,t)}}),u}]}function hy(){this.$get=["$window","$document",function(n,t){var s={},h=g((/android (\d+)/.exec(y((n.navigator||{}).userAgent))||[])[1]),v=/Boxee/i.test((n.navigator||{}).userAgent),u=t[0]||{},i,r=u.body&&u.body.style,f=!1,o=!1,l,a;if(r){for(a in r)if(l=/^(Moz|webkit|ms)(?=[A-Z])/.exec(a)){i=l[0];i=i.substr(0,1).toUpperCase()+i.substr(1);break}i||(i="WebkitOpacity"in r&&"webkit");f=!!("transition"in r||i+"Transition"in r);o=!!("animation"in r||i+"Animation"in r);!h||f&&o||(f=c(u.body.style.webkitTransition),o=c(u.body.style.webkitAnimation))}return{history:!!(n.history&&n.history.pushState&&!(h<4)&&!v),hasEvent:function(n){if(n==="input"&&si<=11)return!1;if(e(s[n])){var t=u.createElement("div");s[n]="on"+n in t}return s[n]},csp:ci(),vendorPrefix:i,transitions:f,animations:o,android:h}}]}function cy(){this.$get=["$templateCache","$http","$q",function(n,t,i){function r(u,f){function h(n){if(!f)throw tt("tpload","Failed to load template: {0}",u);return i.reject(n)}var e,s;return r.totalPendingRequests++,e=t.defaults&&t.defaults.transformResponse,o(e)?e=e.filter(function(n){return n!==df}):e===df&&(e=null),s={cache:n,transformResponse:e},t.get(u,s).finally(function(){r.totalPendingRequests--}).then(function(n){return n.data},h)}return r.totalPendingRequests=0,r}]}function ly(){this.$get=["$rootScope","$browser","$location",function(n,t,i){var u={};return u.findBindings=function(n,t,i){var f=n.getElementsByClassName("ng-binding"),u=[];return r(f,function(n){var f=ft.element(n).data("$binding");f&&r(f,function(r){if(i){var f=new RegExp("(^|\\s)"+nf(t)+"(\\s|\\||$)");f.test(r)&&u.push(n)}else r.indexOf(t)!=-1&&u.push(n)})}),u},u.findModels=function(n,t,i){for(var u=["ng-","data-ng-","ng\\:"],r=0;r<u.length;++r){var e=i?"=":"*=",o="["+u[r]+"model"+e+'"'+t+'"]',f=n.querySelectorAll(o);if(f.length)return f}},u.getLocation=function(){return i.url()},u.setLocation=function(t){t!==i.url()&&(i.url(t),n.$digest())},u.whenStable=function(n){t.notifyWhenNoOutstandingRequests(n)},u}]}function ay(){this.$get=["$rootScope","$browser","$q","$$q","$exceptionHandler",function(n,t,i,r,f){function o(o,s,h){var v=u(h)&&!h,c=(v?r:i).defer(),l=c.promise,a;return a=t.defer(function(){try{c.resolve(o())}catch(t){c.reject(t);f(t)}finally{delete e[l.$$timeoutId]}v||n.$apply()},s),l.$$timeoutId=a,e[a]=c,l}var e={};return o.cancel=function(n){return n&&n.$$timeoutId in e?(e[n.$$timeoutId].reject("canceled"),delete e[n.$$timeoutId],t.defer.cancel(n.$$timeoutId)):!1},o}]}function gt(n){var t=n;return si&&(b.setAttribute("href",t),t=b.href),b.setAttribute("href",t),{href:b.href,protocol:b.protocol?b.protocol.replace(/:$/,""):"",host:b.host,search:b.search?b.search.replace(/^\?/,""):"",hash:b.hash?b.hash.replace(/^#/,""):"",hostname:b.hostname,port:b.port,pathname:b.pathname.charAt(0)==="/"?b.pathname:"/"+b.pathname}}function th(n){var t=c(n)?gt(n):n;return t.protocol===oe.protocol&&t.host===oe.host}function vy(){this.$get=nt(n)}function ih(n){function t(u,f){if(h(u)){var e={};return r(u,function(n,i){e[i]=t(i,n)}),e}return n.factory(u+i,f)}var i="Filter";this.register=t;this.$get=["$injector",function(n){return function(t){return n.get(t+i)}}];t("currency",rh);t("date",sh);t("filter",yy);t("json",tp);t("limitTo",ip);t("lowercase",hh);t("number",uh);t("orderBy",lh);t("uppercase",ch)}function yy(){return function(n,t,i){if(!o(n))return n;var r,u;switch(typeof t){case"function":r=t;break;case"boolean":case"number":case"string":u=!0;case"object":r=py(t,i,u);break;default:return n}return n.filter(r)}}function py(n,t,i){var r=h(n)&&"$"in n;return t===!0?t=et:l(t)||(t=function(n,t){return h(n)||h(t)?!1:(n=y(""+n),t=y(""+t),n.indexOf(t)!==-1)}),function(u){return r&&!h(u)?ei(u,n.$,t,!1):ei(u,n,t,i)}}function ei(n,t,i,r,u){var a=typeof n,h=typeof t,f,s,e,c;if(h==="string"&&t.charAt(0)==="!")return!ei(n,t.substring(1),i,r);if(o(n))return n.some(function(n){return ei(n,t,i,r)});switch(a){case"object":if(r){for(f in n)if(f.charAt(0)!=="$"&&ei(n[f],t,i,!0))return!0;return u?!1:ei(n,t,i,!1)}if(h==="object"){for(f in t)if((s=t[f],!l(s))&&(e=f==="$",c=e?n:n[f],!ei(c,s,i,e,e)))return!1;return!0}return i(n,t);case"function":return!1;default:return i(n,t)}}function rh(n){var t=n.NUMBER_FORMATS;return function(n,i,r){return e(i)&&(i=t.CURRENCY_SYM),e(r)&&(r=t.PATTERNS[1].maxFrac),n==null?n:fh(n,t.PATTERNS[1],t.GROUP_SEP,t.DECIMAL_SEP,r).replace(/\u00A4/g,i)}}function uh(n){var t=n.NUMBER_FORMATS;return function(n,i){return n==null?n:fh(n,t.PATTERNS[0],t.GROUP_SEP,t.DECIMAL_SEP,i)}}function fh(n,t,i,r,u){var l,v,k,s,c;if(!isFinite(n)||h(n))return"";l=n<0;n=Math.abs(n);var a=n+"",o="",w=[],b=!1;if(a.indexOf("e")!==-1&&(v=a.match(/([\d\.]+)e(-?)(\d+)/),v&&v[2]=="-"&&v[3]>u+1?n=0:(o=a,b=!0)),b)u>0&&n<1&&(o=n.toFixed(u),n=parseFloat(o));else{k=(a.split(se)[1]||"").length;e(u)&&(u=Math.min(Math.max(t.minFrac,k),t.maxFrac));n=+(Math.round(+(n.toString()+"e"+u)).toString()+"e"+-u);s=(""+n).split(se);c=s[0];s=s[1]||"";var f,y=0,p=t.lgSize,d=t.gSize;if(c.length>=p+d)for(y=c.length-p,f=0;f<y;f++)(y-f)%d==0&&f!==0&&(o+=i),o+=c.charAt(f);for(f=y;f<c.length;f++)(c.length-f)%p==0&&f!==0&&(o+=i),o+=c.charAt(f);while(s.length<u)s+="0";u&&u!=="0"&&(o+=r+s.substr(0,u))}return n===0&&(l=!1),w.push(l?t.negPre:t.posPre,o,l?t.negSuf:t.posSuf),w.join("")}function pu(n,t,i){var r="";for(n<0&&(r="-",n=-n),n=""+n;n.length<t;)n="0"+n;return i&&(n=n.substr(n.length-t)),r+n}function d(n,t,i,r){return i=i||0,function(u){var f=u["get"+n]();return(i>0||f>-i)&&(f+=i),f===0&&i==-12&&(f=12),pu(f,t,r)}}function wu(n,t){return function(i,r){var u=i["get"+n](),f=bi(t?"SHORT"+n:n);return r[f][u]}}function wy(n){var t=-1*n.getTimezoneOffset(),i=t>=0?"+":"";return i+(pu(Math[t>0?"floor":"ceil"](t/60),2)+pu(Math.abs(t%60),2))}function eh(n){var t=new Date(n,0,1).getDay();return new Date(n,0,(t<=4?5:12)-t)}function by(n){return new Date(n.getFullYear(),n.getMonth(),n.getDate()+(4-n.getDay()))}function oh(n){return function(t){var i=eh(t.getFullYear()),r=by(t),u=+r-+i,f=1+Math.round(u/6048e5);return pu(f,n)}}function ky(n,t){return n.getHours()<12?t.AMPMS[0]:t.AMPMS[1]}function sh(n){function i(n){var i;if(i=n.match(t)){var r=new Date(0),u=0,f=0,e=i[8]?r.setUTCFullYear:r.setFullYear,o=i[8]?r.setUTCHours:r.setHours;i[9]&&(u=g(i[9]+i[10]),f=g(i[9]+i[11]));e.call(r,g(i[1]),g(i[2])-1,g(i[3]));var s=g(i[4]||0)-u,h=g(i[5]||0)-f,c=g(i[6]||0),l=Math.round(parseFloat("0."+(i[7]||0))*1e3);return o.call(r,s,h,c,l),r}return n}var t=/^(\d{4})-?(\d\d)-?(\d\d)(?:T(\d\d)(?::?(\d\d)(?::?(\d\d)(?:\.(\d+))?)?)?(Z|([+-])(\d\d):?(\d\d))?)?$/;return function(t,u,f){var h="",e=[],o,s;if(u=u||"mediumDate",u=n.DATETIME_FORMATS[u]||u,c(t)&&(t=np.test(t)?g(t):i(t)),k(t)&&(t=new Date(t)),!lt(t))return t;while(u)s=gy.exec(u),s?(e=rr(e,s,1),u=e.pop()):(e.push(u),u=null);return f&&f==="UTC"&&(t=new Date(t.getTime()),t.setMinutes(t.getMinutes()+t.getTimezoneOffset())),r(e,function(i){o=dy[i];h+=o?o(t,n.DATETIME_FORMATS):i.replace(/(^'|'$)/g,"").replace(/''/g,"'")}),h}}function tp(){return function(n,t){return e(t)&&(t=2),ur(n,t)}}function ip(){return function(n,t){return(k(n)&&(n=n.toString()),!o(n)&&!c(n))?n:(t=Math.abs(Number(t))===Infinity?Number(t):g(t),t?t>0?n.slice(0,t):n.slice(t):c(n)?"":[])}}function lh(n){return function(t,i,r){function h(n,t){for(var u,r=0;r<i.length;r++)if(u=i[r](n,t),u!==0)return u;return 0}function u(n,t){return t?function(t,i){return n(i,t)}:n}function e(n){switch(typeof n){case"number":case"boolean":case"string":return!0;default:return!1}}function s(n){return n===null?"null":typeof n.valueOf=="function"&&(n=n.valueOf(),e(n))?n:typeof n.toString=="function"&&(n=n.toString(),e(n))?n:""}function f(n,t){var i=typeof n,r=typeof t;return i===r&&i==="object"&&(n=s(n),t=s(t)),i===r?(i==="string"&&(n=n.toLowerCase(),t=t.toLowerCase()),n===t)?0:n<t?-1:1:i<r?-1:1}return di(t)?(i=o(i)?i:[i],i.length===0&&(i=["+"]),i=i.map(function(t){var r=!1,i=t||ct,e;if(c(t)){if((t.charAt(0)=="+"||t.charAt(0)=="-")&&(r=t.charAt(0)=="-",t=t.substring(1)),t==="")return u(f,r);if(i=n(t),i.constant)return e=i(),u(function(n,t){return f(n[e],t[e])},r)}return u(function(n,t){return f(i(n),i(t))},r)}),gu.call(t).sort(u(h,r))):t}}function oi(n){return l(n)&&(n={link:n}),n.restrict=n.restrict||"AC",nt(n)}function rp(n,t){n.$name=t}function vh(n,t,u,f,e){var o=this,s=[],h=o.$$parentForm=n.parent().controller("form")||vr;o.$error={};o.$$success={};o.$pending=i;o.$name=e(t.name||t.ngForm||"")(u);o.$dirty=!1;o.$pristine=!0;o.$valid=!0;o.$invalid=!1;o.$submitted=!1;h.$addControl(o);o.$rollbackViewValue=function(){r(s,function(n){n.$rollbackViewValue()})};o.$commitViewValue=function(){r(s,function(n){n.$commitViewValue()})};o.$addControl=function(n){li(n.$name,"input");s.push(n);n.$name&&(o[n.$name]=n)};o.$$renameControl=function(n,t){var i=n.$name;o[i]===n&&delete o[i];o[t]=n;n.$name=t};o.$removeControl=function(n){n.$name&&o[n.$name]===n&&delete o[n.$name];r(o.$pending,function(t,i){o.$setValidity(i,null,n)});r(o.$error,function(t,i){o.$setValidity(i,null,n)});r(o.$$success,function(t,i){o.$setValidity(i,null,n)});ir(s,n)};ec({ctrl:this,$element:n,set:function(n,t,i){var r=n[t],u;r?(u=r.indexOf(i),u===-1&&r.push(i)):n[t]=[i]},unset:function(n,t,i){var r=n[t];r&&(ir(r,i),r.length===0&&delete n[t])},parentForm:h,$animate:f});o.$setDirty=function(){f.removeClass(n,wi);f.addClass(n,ku);o.$dirty=!0;o.$pristine=!1;h.$setDirty()};o.$setPristine=function(){f.setClass(n,wi,ku+" "+he);o.$dirty=!1;o.$pristine=!0;o.$submitted=!1;r(s,function(n){n.$setPristine()})};o.$setUntouched=function(){r(s,function(n){n.$setUntouched()})};o.$setSubmitted=function(){f.addClass(n,he);o.$submitted=!0;h.$setSubmitted()}}function le(n){n.$formatters.push(function(t){return n.$isEmpty(t)?t:t.toString()})}function cp(n,t,i,r,u,f){yr(n,t,i,r,u,f);le(r)}function yr(n,t,i,r,u,f){var c=y(t[0].type),s,o,e,h;if(!u.android){s=!1;t.on("compositionstart",function(){s=!0});t.on("compositionend",function(){s=!1;o()})}if(o=function(n){if(e&&(f.defer.cancel(e),e=null),!s){var u=t.val(),o=n&&n.type;c==="password"||i.ngTrim&&i.ngTrim==="false"||(u=p(u));(r.$viewValue!==u||u===""&&r.$$hasNativeValidators)&&r.$setViewValue(u,o)}},u.hasEvent("input"))t.on("input",o);else{h=function(n,t,i){e||(e=f.defer(function(){e=null;t&&t.value===i||o(n)}))};t.on("keydown",function(n){var t=n.keyCode;t===91||15<t&&t<19||37<=t&&t<=40||h(n,this,this.value)});if(u.hasEvent("paste"))t.on("paste cut",h)}t.on("change",o);r.$render=function(){t.val(r.$isEmpty(r.$viewValue)?"":r.$viewValue)}}function lp(n,t){var i;if(lt(n))return n;if(c(n)&&(ce.lastIndex=0,i=ce.exec(n),i)){var r=+i[1],s=+i[2],u=0,f=0,e=0,o=0,h=eh(r),l=(s-1)*7;return t&&(u=t.getHours(),f=t.getMinutes(),e=t.getSeconds(),o=t.getMilliseconds()),new Date(r,0,h.getDate()+l,u,f,e,o)}return NaN}function bu(n,t){return function(i,u){var e,f;if(lt(i))return i;if(c(i)){if(i.charAt(0)=='"'&&i.charAt(i.length-1)=='"'&&(i=i.substring(1,i.length-1)),ep.test(i))return new Date(i);if(n.lastIndex=0,e=n.exec(i),e)return e.shift(),f=u?{yyyy:u.getFullYear(),MM:u.getMonth()+1,dd:u.getDate(),HH:u.getHours(),mm:u.getMinutes(),ss:u.getSeconds(),sss:u.getMilliseconds()/1e3}:{yyyy:1970,MM:1,dd:1,HH:0,mm:0,ss:0,sss:0},r(e,function(n,i){i<t.length&&(f[t[i]]=+n)}),new Date(f.yyyy,f.MM-1,f.dd,f.HH,f.mm,f.ss||0,f.sss*1e3||0)}return NaN}}function pr(n,t,r,f){return function(o,s,h,c,l,a,v){function k(n){return n&&!(n.getTime&&n.getTime()!==n.getTime())}function d(n){return u(n)?lt(n)?n:r(n):i}var p,y,w,b;gh(o,s,h,c);yr(o,s,h,c,l,a);p=c&&c.$options&&c.$options.timezone;c.$$parserName=n;c.$parsers.push(function(n){if(c.$isEmpty(n))return null;if(t.test(n)){var u=r(n,y);return p==="UTC"&&u.setMinutes(u.getMinutes()-u.getTimezoneOffset()),u}return i});c.$formatters.push(function(n){if(n&&!lt(n))throw du("datefmt","Expected `{0}` to be a date",n);if(k(n)){if(y=n,y&&p==="UTC"){var t=6e4*y.getTimezoneOffset();y=new Date(y.getTime()+t)}return v("date")(n,f,p)}return y=null,""});(u(h.min)||h.ngMin)&&(c.$validators.min=function(n){return!k(n)||e(w)||r(n)>=w},h.$observe("min",function(n){w=d(n);c.$validate()}));(u(h.max)||h.ngMax)&&(c.$validators.max=function(n){return!k(n)||e(b)||r(n)<=b},h.$observe("max",function(n){b=d(n);c.$validate()}))}}function gh(n,t,r,u){var f=t[0],e=u.$$hasNativeValidators=h(f.validity);e&&u.$parsers.push(function(n){var r=t.prop(pc)||{};return r.badInput&&!r.typeMismatch?i:n})}function ap(n,t,r,f,o,s){var h,c;gh(n,t,r,f);yr(n,t,r,f,o,s);f.$$parserName="number";f.$parsers.push(function(n){return f.$isEmpty(n)?null:hp.test(n)?parseFloat(n):i});f.$formatters.push(function(n){if(!f.$isEmpty(n)){if(!k(n))throw du("numfmt","Expected `{0}` to be a number",n);n=n.toString()}return n});(r.min||r.ngMin)&&(f.$validators.min=function(n){return f.$isEmpty(n)||e(h)||n>=h},r.$observe("min",function(n){u(n)&&!k(n)&&(n=parseFloat(n,10));h=k(n)&&!isNaN(n)?n:i;f.$validate()}));(r.max||r.ngMax)&&(f.$validators.max=function(n){return f.$isEmpty(n)||e(c)||n<=c},r.$observe("max",function(n){u(n)&&!k(n)&&(n=parseFloat(n,10));c=k(n)&&!isNaN(n)?n:i;f.$validate()}))}function vp(n,t,i,r,u,f){yr(n,t,i,r,u,f);le(r);r.$$parserName="url";r.$validators.url=function(n,t){var i=n||t;return r.$isEmpty(i)||op.test(i)}}function yp(n,t,i,r,u,f){yr(n,t,i,r,u,f);le(r);r.$$parserName="email";r.$validators.email=function(n,t){var i=n||t;return r.$isEmpty(i)||sp.test(i)}}function pp(n,t,i,r){e(i.name)&&t.attr("name",br());var u=function(n){t[0].checked&&r.$setViewValue(i.value,n&&n.type)};t.on("click",u);r.$render=function(){var n=i.value;t[0].checked=n==r.$viewValue};i.$observe("value",r.$render)}function nc(n,t,i,r,f){var e;if(u(r)){if(e=n(r),!e.constant)throw v("ngModel")("constexpr","Expected constant expression for `{0}`, but saw `{1}`.",i,r);return e(t)}return f}function wp(n,t,i,r,u,f,e,o){var s=nc(o,n,"ngTrueValue",i.ngTrueValue,!0),h=nc(o,n,"ngFalseValue",i.ngFalseValue,!1),c=function(n){r.$setViewValue(t[0].checked,n&&n.type)};t.on("click",c);r.$render=function(){t[0].checked=r.$viewValue};r.$isEmpty=function(n){return n===!1};r.$formatters.push(function(n){return et(n,s)});r.$parsers.push(function(n){return n?s:h})}function ae(n,t){return n="ngClass"+n,["$animate",function(i){function f(n,t){var f=[],i,u,r;n:for(i=0;i<n.length;i++){for(u=n[i],r=0;r<t.length;r++)if(u==t[r])continue n;f.push(u)}return f}function u(n){if(o(n))return n;if(c(n))return n.split(" ");if(h(n)){var t=[];return r(n,function(n,i){n&&(t=t.concat(i.split(" ")))}),t}return n}return{restrict:"AC",link:function(e,o,s){function l(n){var t=c(n,1);s.$addClass(t)}function v(n){var t=c(n,-1);s.$removeClass(t)}function c(n,t){var i=o.data("$classCounts")||{},u=[];return r(n,function(n){(t>0||i[n])&&(i[n]=(i[n]||0)+t,i[n]===+(t>0)&&u.push(n))}),o.data("$classCounts",i),u.join(" ")}function y(n,t){var r=f(t,n),u=f(n,t);r=c(r,1);u=c(u,-1);r&&r.length&&i.addClass(o,r);u&&u.length&&i.removeClass(o,u)}function a(n){var i,r;(t===!0||e.$index%2===t)&&(i=u(n||[]),h?et(n,h)||(r=u(h),y(r,i)):l(i));h=at(n)}var h;e.$watch(s[n],a,!0);s.$observe("class",function(){a(e.$eval(s[n]))});n!=="ngClass"&&e.$watch("$index",function(i,r){var o=i&1,f;o!==(r&1)&&(f=u(e.$eval(s[n])),o===t?l(f):v(f))})}}}]}function ec(n){function l(n,r,e){r===i?a("$pending",n,e):v("$pending",n,e);tr(r)?r?(u(t.$error,n,e),o(t.$$success,n,e)):(o(t.$error,n,e),u(t.$$success,n,e)):(u(t.$error,n,e),u(t.$$success,n,e));t.$pending?(f(fc,!0),t.$valid=t.$invalid=i,s("",null)):(f(fc,!1),t.$valid=oc(t.$error),t.$invalid=!t.$valid,s("",t.$valid));var h;h=t.$pending&&t.$pending[n]?i:t.$error[n]?!1:t.$$success[n]?!0:null;s(n,h);c.$setValidity(n,h,t)}function a(n,i,r){t[n]||(t[n]={});o(t[n],i,r)}function v(n,r,f){t[n]&&u(t[n],r,f);oc(t[n])&&(t[n]=i)}function f(n,t){t&&!r[n]?(h.addClass(e,n),r[n]=!0):!t&&r[n]&&(h.removeClass(e,n),r[n]=!1)}function s(n,t){n=n?"-"+eo(n,"-"):"";f(wr+n,t===!0);f(rc+n,t===!1)}var t=n.ctrl,e=n.$element,r={},o=n.set,u=n.unset,c=n.parentForm,h=n.$animate;r[rc]=!(r[wr]=e.hasClass(wr));t.$setValidity=l}function oc(n){if(n)for(var t in n)return!1;return!0}var yc=/^\/(.+)\/([a-z]*)$/,pc="validity",y=function(n){return c(n)?n.toLowerCase():n},ye=Object.prototype.hasOwnProperty,bi=function(n){return c(n)?n.toUpperCase():n},wc=function(n){return c(n)?n.replace(/[A-Z]/g,function(n){return String.fromCharCode(n.charCodeAt(0)|32)}):n},bc=function(n){return c(n)?n.replace(/[a-z]/g,function(n){return String.fromCharCode(n.charCodeAt(0)&-33)}):n},o,p,nf,ci,fr,fo,uf,ff,lo,ri,sr,vf,yf,ts,is,bf,au,ys,it,vi,yi,ws,bs,ds,dt,rt,tt,b,oe,se,hh,ch,ah,ar,vr,he;"i"!=="I".toLowerCase()&&(y=wc,bi=bc);var si,f,ut,gu=[].slice,kc=[].splice,dc=[].push,ni=Object.prototype.toString,hi=v("ng"),ft=n.angular||(n.angular={}),ki,gc=0;si=t.documentMode;s.$inject=[];ct.$inject=[];o=Array.isArray;p=function(n){return c(n)?n.trim():n};nf=function(n){return n.replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g,"\\$1").replace(/\x08/g,"\\x08")};ci=function(){if(u(ci.isActive_))return ci.isActive_;var n=!!(t.querySelector("[ng-csp]")||t.querySelector("[data-ng-csp]"));if(!n)try{new Function("")}catch(i){n=!0}return ci.isActive_=n};fr=["ng-","data-ng-","ng:","x-ng-"];fo=/[A-Z]/g;uf=!1;var vt=1,iu=3,so=8,ho=9,of=11;lo={full:"1.3.11",major:1,minor:3,dot:11,codeName:"spiffy-manatee"};w.expando="ng339";var ru=w.cache={},vl=1,uu=function(n,t,i){n.addEventListener(t,i,!1)},er=function(n,t,i){n.removeEventListener(t,i,!1)};w._data=function(n){return this.cache[n[this.expando]]||{}};var pl=/([\:\-\_]+(.))/g,wl=/^moz([A-Z])/,bl={mouseleave:"mouseout",mouseenter:"mouseover"},sf=v("jqLite");var kl=/^<(\w+)\s*\/?>(?:<\/\1>|)$/,dl=/<|&#?\w+;/,gl=/<([\w:]+)/,na=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,st={option:[1,'<select multiple="multiple">',"<\/select>"],thead:[1,"<table>","<\/table>"],col:[2,"<table><colgroup>","<\/colgroup><\/table>"],tr:[2,"<table><tbody>","<\/tbody><\/table>"],td:[3,"<table><tbody><tr>","<\/tr><\/tbody><\/table>"],_default:[0,"",""]};st.optgroup=st.option;st.tbody=st.tfoot=st.colgroup=st.caption=st.thead;st.th=st.td;ri=w.prototype={ready:function(i){function r(){u||(u=!0,i())}var u=!1;if(t.readyState==="complete")setTimeout(r);else{this.on("DOMContentLoaded",r);w(n).on("load",r)}},toString:function(){var n=[];return r(this,function(t){n.push(""+t)}),"["+n.join(", ")+"]"},eq:function(n){return n>=0?f(this[n]):f(this[this.length+n])},length:0,push:dc,sort:[].sort,splice:[].splice};sr={};r("multiple,selected,checked,disabled,readOnly,required,open".split(","),function(n){sr[y(n)]=n});vf={};r("input,select,option,textarea,button,form,details".split(","),function(n){vf[n]=!0});yf={ngMinlength:"minlength",ngMaxlength:"maxlength",ngMin:"min",ngMax:"max",ngPattern:"pattern"};r({data:lf,removeData:eu},function(n,t){w[t]=n});r({data:lf,inheritedData:lu,scope:function(n){return f.data(n,"$scope")||lu(n.parentNode||n,["$isolateScope","$scope"])},isolateScope:function(n){return f.data(n,"$isolateScope")||f.data(n,"$isolateScopeNoTemplate")},controller:po,injector:function(n){return lu(n,"$injector")},removeAttr:function(n,t){n.removeAttribute(t)},hasClass:su,css:function(n,t,i){if(t=or(t),u(i))n.style[t]=i;else return n.style[t]},attr:function(n,t,r){var f=y(t),e;if(sr[f])if(u(r))r?(n[t]=!0,n.setAttribute(t,f)):(n[t]=!1,n.removeAttribute(f));else return n[t]||(n.attributes.getNamedItem(t)||s).specified?f:i;else if(u(r))n.setAttribute(t,r);else if(n.getAttribute)return e=n.getAttribute(t,2),e===null?i:e},prop:function(n,t,i){if(u(i))n[t]=i;else return n[t]},text:function(){function n(n,t){if(e(t)){var i=n.nodeType;return i===vt||i===iu?n.textContent:""}n.textContent=t}return n.$dv="",n}(),val:function(n,t){if(e(t)){if(n.multiple&&pt(n)==="select"){var i=[];return r(n.options,function(n){n.selected&&i.push(n.value||n.text)}),i.length===0?null:i}return n.value}n.value=t},html:function(n,t){if(e(t))return n.innerHTML;fu(n,!0);n.innerHTML=t},empty:wo},function(n,t){w.prototype[t]=function(t,r){var u,s,e=this.length,f,l,o,c;if(n!==wo&&(n.length==2&&n!==su&&n!==po?t:r)===i){if(h(t)){for(u=0;u<e;u++)if(n===lf)n(this[u],t);else for(s in t)n(this[u],s,t[s]);return this}for(f=n.$dv,l=f===i?Math.min(e,1):e,o=0;o<l;o++)c=n(this[o],t,r),f=f?f+c:c;return f}for(u=0;u<e;u++)n(this[u],t,r);return this}});r({removeData:eu,on:function fa(n,t,i,r){var h,c,o;if(u(r))throw sf("onargs","jqLite#on() does not support the `selector` or `eventData` parameters");if(ao(n)){var s=ou(n,!0),f=s.events,e=s.handle;for(e||(e=s.handle=ua(n,f)),h=t.indexOf(" ")>=0?t.split(" "):[t],c=h.length;c--;)t=h[c],o=f[t],o||(f[t]=[],t==="mouseenter"||t==="mouseleave"?fa(n,bl[t],function(n){var r=this,i=n.relatedTarget;i&&(i===r||r.contains(i))||e(n,t)}):t!=="$destroy"&&uu(n,t,e),o=f[t]),o.push(i)}},off:yo,one:function(n,t,i){n=f(n);n.on(t,function r(){n.off(t,i);n.off(t,r)});n.on(t,i)},replaceWith:function(n,t){var i,u=n.parentNode;fu(n);r(new w(t),function(t){i?u.insertBefore(t,i.nextSibling):u.replaceChild(t,n);i=t})},children:function(n){var t=[];return r(n.childNodes,function(n){n.nodeType===vt&&t.push(n)}),t},contents:function(n){return n.contentDocument||n.childNodes||[]},append:function(n,t){var r=n.nodeType,i,u,f;if(r===vt||r===of)for(t=new w(t),i=0,u=t.length;i<u;i++)f=t[i],n.appendChild(f)},prepend:function(n,t){if(n.nodeType===vt){var i=n.firstChild;r(new w(t),function(t){n.insertBefore(t,i)})}},wrap:function(n,t){t=f(t).eq(0).clone()[0];var i=n.parentNode;i&&i.replaceChild(t,n);t.appendChild(n)},remove:bo,detach:function(n){bo(n,!0)},after:function(n,t){var u=n,e=n.parentNode,i,f,r;for(t=new w(t),i=0,f=t.length;i<f;i++)r=t[i],e.insertBefore(r,u.nextSibling),u=r},addClass:cu,removeClass:hu,toggleClass:function(n,t,i){t&&r(t.split(" "),function(t){var r=i;e(r)&&(r=!su(n,t));(r?cu:hu)(n,t)})},parent:function(n){var t=n.parentNode;return t&&t.nodeType!==of?t:null},next:function(n){return n.nextElementSibling},find:function(n,t){return n.getElementsByTagName?n.getElementsByTagName(t):[]},clone:cf,triggerHandler:function(n,t,i){var u,f,e,o=t.type||t,h=ou(n),c=h&&h.events,l=c&&c[o];l&&(u={preventDefault:function(){this.defaultPrevented=!0},isDefaultPrevented:function(){return this.defaultPrevented===!0},stopImmediatePropagation:function(){this.immediatePropagationStopped=!0},isImmediatePropagationStopped:function(){return this.immediatePropagationStopped===!0},stopPropagation:s,type:o,target:n},t.type&&(u=a(u,t)),f=at(l),e=i?[u].concat(i):[u],r(f,function(t){u.isImmediatePropagationStopped()||t.apply(n,e)}))}},function(n,t){w.prototype[t]=function(t,i,r){for(var o,s=0,h=this.length;s<h;s++)e(o)?(o=n(this[s],t,i,r),u(o)&&(o=f(o))):af(o,n(this[s],t,i,r));return u(o)?o:this};w.prototype.bind=w.prototype.on;w.prototype.unbind=w.prototype.off});hr.prototype={put:function(n,t){this[ai(n,this.nextUid)]=t},get:function(n){return this[ai(n,this.nextUid)]},remove:function(n){var t=this[n=ai(n,this.nextUid)];return delete this[n],t}};var go=/^function\s*[^\(]*\(\s*([^\)]*)\)/m,oa=/,/,sa=/^\s*(_?)(\S+?)\1\s*$/,ns=/((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg,ui=v("$injector");wf.$$annotate=pf;ts=v("$animate");is=["$provide",function(n){this.$$selectors={};this.register=function(t,i){var r=t+"-animation";if(t&&t.charAt(0)!=".")throw ts("notcsel","Expecting class selector starting with '.' got '{0}'.",t);this.$$selectors[t.substr(1)]=r;n.factory(r,i)};this.classNameFilter=function(n){return arguments.length===1&&(this.$$classNameFilter=n instanceof RegExp?n:null),this.$$classNameFilter};this.$get=["$$q","$$asyncCallback","$rootScope",function(n,t,i){function v(t){var r,u=n.defer();return u.promise.$$cancelFn=function(){r&&r()},i.$$postDigest(function(){r=t(function(){u.resolve()})}),u.promise}function y(n,t){var i=[],u=[],f=ot();return r((n.attr("class")||"").split(/\s+/),function(n){f[n]=!0}),r(t,function(n,t){var r=f[t];n===!1&&r?u.push(t):n!==!0||r||i.push(t)}),i.length+u.length>0&&[i.length?i:null,u.length?u:null]}function l(n,t,i){for(var f,r=0,u=t.length;r<u;++r)f=t[r],n[f]=i}function u(){return e||(e=n.defer(),t(function(){e.resolve();e=null})),e.promise}function h(n,t){if(ft.isObject(t)){var i=a(t.from||{},t.to||{});n.css(i)}}var e;return{animate:function(n,t,i){return h(n,{from:t,to:i}),u()},enter:function(n,t,i,r){return h(n,r),i?i.after(n):t.prepend(n),u()},leave:function(n){return n.remove(),u()},move:function(n,t,i,r){return this.enter(n,t,i,r)},addClass:function(n,t,i){return this.setClass(n,t,[],i)},$$addClassImmediately:function(n,t,i){return n=f(n),t=c(t)?t:o(t)?t.join(" "):"",r(n,function(n){cu(n,t)}),h(n,i),u()},removeClass:function(n,t,i){return this.setClass(n,[],t,i)},$$removeClassImmediately:function(n,t,i){return n=f(n),t=c(t)?t:o(t)?t.join(" "):"",r(n,function(n){hu(n,t)}),h(n,i),u()},setClass:function(n,t,i,r){var c=this,e="$$animateClasses",h=!1,u,s;return n=f(n),u=n.data(e),u?r&&u.options&&(u.options=ft.extend(u.options||{},r)):(u={classes:{},options:r},h=!0),s=u.classes,t=o(t)?t:t.split(" "),i=o(i)?i:i.split(" "),l(s,t,!0),l(s,i,!1),h&&(u.promise=v(function(t){var r=n.data(e),i;n.removeData(e);r&&(i=y(n,r.classes),i&&c.$$setClassImmediately(n,i[0],i[1],r.options));t()}),n.data(e,u)),u.promise},$$setClassImmediately:function(n,t,i,r){return t&&this.$$addClassImmediately(n,t),i&&this.$$removeClassImmediately(n,i),h(n,r),u()},enabled:s,cancel:s}}]}];tt=v("$compile");rs.$inject=["$provide","$$sanitizeUriProvider"];bf=/^((?:x|data)[\:\-_])/i;var es="application/json",kf={"Content-Type":es+";charset=utf-8"},da=/^\[|^\{(?!\{)/,ga={"[":/]$/,"{":/}$/},nv=/^\)\]\}',?\n/;au=v("$interpolate");var hv=/^([^\?#]*)(\?([^#]*))?(#(.*))?$/,cv={http:80,https:443,ftp:21},vu=v("$location");ys={$$html5:!1,$$replace:!1,absUrl:yu("$$absUrl"),url:function(n){if(e(n))return this.$$url;var t=hv.exec(n);return(t[1]||n==="")&&this.path(decodeURIComponent(t[1])),(t[2]||t[1]||n==="")&&this.search(t[3]||""),this.hash(t[5]||""),this},protocol:yu("$$protocol"),host:yu("$$host"),port:yu("$$port"),path:ps("$$path",function(n){return n=n!==null?n.toString():"",n.charAt(0)=="/"?n:"/"+n}),search:function(n,t){switch(arguments.length){case 0:return this.$$search;case 1:if(c(n)||k(n))n=n.toString(),this.$$search=ro(n);else if(h(n))n=ti(n,{}),r(n,function(t,i){t==null&&delete n[i]}),this.$$search=n;else throw vu("isrcharg","The first argument of the `$location#search()` call must be a string or an object.");break;default:e(t)||t===null?delete this.$$search[n]:this.$$search[n]=t}return this.$$compose(),this},hash:ps("$$hash",function(n){return n!==null?n.toString():""}),replace:function(){return this.$$replace=!0,this}};r([vs,re,ie],function(n){n.prototype=Object.create(ys);n.prototype.state=function(t){if(!arguments.length)return this.$$state;if(n!==ie||!this.$$html5)throw vu("nostate","History API state support is available only in HTML5 mode and only in browsers supporting HTML5 History API");return this.$$state=e(t)?null:t,this}});it=v("$parse");var yv=Function.prototype.call,pv=Function.prototype.apply,wv=Function.prototype.bind;vi=ot();r({"null":function(){return null},"true":function(){return!0},"false":function(){return!1},undefined:function(){}},function(n,t){n.constant=n.literal=n.sharedGetter=!0;vi[t]=n});vi["this"]=function(n){return n};vi["this"].sharedGetter=!0;var cr=a(ot(),{"+":function(n,t,r,f){return(r=r(n,t),f=f(n,t),u(r))?u(f)?r+f:r:u(f)?f:i},"-":function(n,t,i,r){return i=i(n,t),r=r(n,t),(u(i)?i:0)-(u(r)?r:0)},"*":function(n,t,i,r){return i(n,t)*r(n,t)},"/":function(n,t,i,r){return i(n,t)/r(n,t)},"%":function(n,t,i,r){return i(n,t)%r(n,t)},"===":function(n,t,i,r){return i(n,t)===r(n,t)},"!==":function(n,t,i,r){return i(n,t)!==r(n,t)},"==":function(n,t,i,r){return i(n,t)==r(n,t)},"!=":function(n,t,i,r){return i(n,t)!=r(n,t)},"<":function(n,t,i,r){return i(n,t)<r(n,t)},">":function(n,t,i,r){return i(n,t)>r(n,t)},"<=":function(n,t,i,r){return i(n,t)<=r(n,t)},">=":function(n,t,i,r){return i(n,t)>=r(n,t)},"&&":function(n,t,i,r){return i(n,t)&&r(n,t)},"||":function(n,t,i,r){return i(n,t)||r(n,t)},"!":function(n,t,i){return!i(n,t)},"=":!0,"|":!0}),kv={n:"\n",f:"\f",r:"\r",t:"\t",v:"\v","'":"'",'"':'"'},ue=function(n){this.options=n};ue.prototype={constructor:ue,lex:function(n){var t,r;for(this.text=n,this.index=0,this.tokens=[];this.index<this.text.length;)if(t=this.text.charAt(this.index),t==='"'||t==="'")this.readString(t);else if(this.isNumber(t)||t==="."&&this.isNumber(this.peek()))this.readNumber();else if(this.isIdent(t))this.readIdent();else if(this.is(t,"(){}[].,;:?"))this.tokens.push({index:this.index,text:t}),this.index++;else if(this.isWhitespace(t))this.index++;else{var i=t+this.peek(),u=i+this.peek(2),o=cr[t],f=cr[i],e=cr[u];o||f||e?(r=e?u:f?i:t,this.tokens.push({index:this.index,text:r,operator:!0}),this.index+=r.length):this.throwError("Unexpected next character ",this.index,this.index+1)}return this.tokens},is:function(n,t){return t.indexOf(n)!==-1},peek:function(n){var t=n||1;return this.index+t<this.text.length?this.text.charAt(this.index+t):!1},isNumber:function(n){return"0"<=n&&n<="9"&&typeof n=="string"},isWhitespace:function(n){return n===" "||n==="\r"||n==="\t"||n==="\n"||n==='\v'||n===" "},isIdent:function(n){return"a"<=n&&n<="z"||"A"<=n&&n<="Z"||"_"===n||n==="$"},isExpOperator:function(n){return n==="-"||n==="+"||this.isNumber(n)},throwError:function(n,t,i){i=i||this.index;var r=u(t)?"s "+t+"-"+this.index+" ["+this.text.substring(t,i)+"]":" "+i;throw it("lexerr","Lexer Error: {0} at column{1} in expression [{2}].",n,r,this.text);},readNumber:function(){for(var n="",r=this.index,t,i;this.index<this.text.length;){if(t=y(this.text.charAt(this.index)),t=="."||this.isNumber(t))n+=t;else if(i=this.peek(),t=="e"&&this.isExpOperator(i))n+=t;else if(this.isExpOperator(t)&&i&&this.isNumber(i)&&n.charAt(n.length-1)=="e")n+=t;else if(!this.isExpOperator(t)||i&&this.isNumber(i)||n.charAt(n.length-1)!="e")break;else this.throwError("Invalid exponent");this.index++}this.tokens.push({index:r,text:n,constant:!0,value:Number(n)})},readIdent:function(){for(var t=this.index,n;this.index<this.text.length;){if(n=this.text.charAt(this.index),!(this.isIdent(n)||this.isNumber(n)))break;this.index++}this.tokens.push({index:t,text:this.text.slice(t,this.index),identifier:!0})},readString:function(n){var f=this.index,t,r,o;this.index++;for(var i="",e=n,u=!1;this.index<this.text.length;){if(t=this.text.charAt(this.index),e+=t,u)t==="u"?(r=this.text.substring(this.index+1,this.index+5),r.match(/[\da-f]{4}/i)||this.throwError("Invalid unicode escape [\\u"+r+"]"),this.index+=4,i+=String.fromCharCode(parseInt(r,16))):(o=kv[t],i=i+(o||t)),u=!1;else if(t==="\\")u=!0;else{if(t===n){this.index++;this.tokens.push({index:f,text:e,constant:!0,value:i});return}i+=t}this.index++}this.throwError("Unterminated quote",f)}};yi=function(n,t,i){this.lexer=n;this.$filter=t;this.options=i};yi.ZERO=a(function(){return 0},{sharedGetter:!0,constant:!0});yi.prototype={constructor:yi,parse:function(n){this.text=n;this.tokens=this.lexer.lex(n);var t=this.statements();return this.tokens.length!==0&&this.throwError("is an unexpected token",this.tokens[0]),t.literal=!!t.literal,t.constant=!!t.constant,t},primary:function(){var n,t,i;for(this.expect("(")?(n=this.filterChain(),this.consume(")")):this.expect("[")?n=this.arrayDeclaration():this.expect("{")?n=this.object():this.peek().identifier&&(this.peek().text in vi)?n=vi[this.consume().text]:this.peek().identifier?n=this.identifier():this.peek().constant?n=this.constant():this.throwError("not a primary expression",this.peek());t=this.expect("(","[",".");)t.text==="("?(n=this.functionCall(n,i),i=null):t.text==="["?(i=n,n=this.objectIndex(n)):t.text==="."?(i=n,n=this.fieldAccess(n)):this.throwError("IMPOSSIBLE");return n},throwError:function(n,t){throw it("syntax","Syntax Error: Token '{0}' {1} at column {2} of the expression [{3}] starting at [{4}].",t.text,n,t.index+1,this.text,this.text.substring(t.index));},peekToken:function(){if(this.tokens.length===0)throw it("ueoe","Unexpected end of expression: {0}",this.text);return this.tokens[0]},peek:function(n,t,i,r){return this.peekAhead(0,n,t,i,r)},peekAhead:function(n,t,i,r,u){if(this.tokens.length>n){var e=this.tokens[n],f=e.text;if(f===t||f===i||f===r||f===u||!t&&!i&&!r&&!u)return e}return!1},expect:function(n,t,i,r){var u=this.peek(n,t,i,r);return u?(this.tokens.shift(),u):!1},consume:function(n){if(this.tokens.length===0)throw it("ueoe","Unexpected end of expression: {0}",this.text);var t=this.expect(n);return t||this.throwError("is unexpected, expecting ["+n+"]",this.peek()),t},unaryFn:function(n,t){var i=cr[n];return a(function(n,r){return i(n,r,t)},{constant:t.constant,inputs:[t]})},binaryFn:function(n,t,i,r){var u=cr[t];return a(function(t,r){return u(t,r,n,i)},{constant:n.constant&&i.constant,inputs:!r&&[n,i]})},identifier:function(){for(var n=this.consume().text;this.peek(".")&&this.peekAhead(1).identifier&&!this.peekAhead(2,"(");)n+=this.consume().text+this.consume().text;return gv(n,this.options,this.text)},constant:function(){var n=this.consume().value;return a(function(){return n},{constant:!0,literal:!0})},statements:function(){for(var n=[];;)if(this.tokens.length>0&&!this.peek("}",")",";","]")&&n.push(this.filterChain()),!this.expect(";"))return n.length===1?n[0]:function(t,i){for(var u,r=0,f=n.length;r<f;r++)u=n[r](t,i);return u}},filterChain:function(){for(var n=this.expression(),t;t=this.expect("|");)n=this.filter(n);return n},filter:function(n){var u=this.$filter(this.consume().text),t,r,f;if(this.peek(":"))for(t=[],r=[];this.expect(":");)t.push(this.expression());return f=[n].concat(t||[]),a(function(f,e){var s=n(f,e),o;if(r){for(r[0]=s,o=t.length;o--;)r[o+1]=t[o](f,e);return u.apply(i,r)}return u(s)},{constant:!u.$stateful&&f.every(fe),inputs:!u.$stateful&&f})},expression:function(){return this.assignment()},assignment:function(){var n=this.ternary(),t,i;return(i=this.expect("="))?(n.assign||this.throwError("implies assignment but ["+this.text.substring(0,i.index)+"] can not be assigned to",i),t=this.ternary(),a(function(i,r){return n.assign(i,t(i,r),r)},{inputs:[n,t]})):n},ternary:function(){var n=this.logicalOR(),t,r,i;return(r=this.expect("?"))&&(t=this.assignment(),this.consume(":"))?(i=this.assignment(),a(function(r,u){return n(r,u)?t(r,u):i(r,u)},{constant:n.constant&&t.constant&&i.constant})):n},logicalOR:function(){for(var n=this.logicalAND(),t;t=this.expect("||");)n=this.binaryFn(n,t.text,this.logicalAND(),!0);return n},logicalAND:function(){for(var n=this.equality(),t;t=this.expect("&&");)n=this.binaryFn(n,t.text,this.equality(),!0);return n},equality:function(){for(var n=this.relational(),t;t=this.expect("==","!=","===","!==");)n=this.binaryFn(n,t.text,this.relational());return n},relational:function(){for(var n=this.additive(),t;t=this.expect("<",">","<=",">=");)n=this.binaryFn(n,t.text,this.additive());return n},additive:function(){for(var n=this.multiplicative(),t;t=this.expect("+","-");)n=this.binaryFn(n,t.text,this.multiplicative());return n},multiplicative:function(){for(var n=this.unary(),t;t=this.expect("*","/","%");)n=this.binaryFn(n,t.text,this.unary());return n},unary:function(){var n;return this.expect("+")?this.primary():(n=this.expect("-"))?this.binaryFn(yi.ZERO,n.text,this.unary()):(n=this.expect("!"))?this.unaryFn(n.text,this.unary()):this.primary()},fieldAccess:function(n){var t=this.identifier();return a(function(r,u,f){var e=f||n(r,u);return e==null?i:t(e)},{assign:function(i,r,u){var f=n(i,u);return f||n.assign(i,f={},u),t.assign(f,r)}})},objectIndex:function(n){var t=this.text,r=this.expression();return this.consume("]"),a(function(u,f){var e=n(u,f),o=r(u,f);return(yt(o,t),!e)?i:ht(e[o],t)},{assign:function(i,u,f){var o=yt(r(i,f),t),e=ht(n(i,f),t);return e||n.assign(i,e={},f),e[o]=u}})},functionCall:function(n,t){var e=[],f,r;if(this.peekToken().text!==")")do e.push(this.expression());while(this.expect(","));return this.consume(")"),f=this.text,r=e.length?[]:null,function(o,h){var a=t?t(o,h):u(t)?i:o,c=n(o,h,a)||s,l,v;if(r)for(l=e.length;l--;)r[l]=ht(e[l](o,h),f);return ht(a,f),bv(c,f),v=c.apply?c.apply(a,r):c(r[0],r[1],r[2],r[3],r[4]),ht(v,f)}},arrayDeclaration:function(){var n=[];if(this.peekToken().text!=="]")do{if(this.peek("]"))break;n.push(this.expression())}while(this.expect(","));return this.consume("]"),a(function(t,i){for(var u=[],r=0,f=n.length;r<f;r++)u.push(n[r](t,i));return u},{literal:!0,constant:n.every(fe),inputs:n})},object:function(){var i=[],t=[],n;if(this.peekToken().text!=="}")do{if(this.peek("}"))break;n=this.consume();n.constant?i.push(n.value):n.identifier?i.push(n.text):this.throwError("invalid key",n);this.consume(":");t.push(this.expression())}while(this.expect(","));return this.consume("}"),a(function(n,r){for(var f={},u=0,e=t.length;u<e;u++)f[i[u]]=t[u](n,r);return f},{literal:!0,constant:t.every(fe),inputs:t})}};ws=ot();bs=ot();ds=Object.prototype.valueOf;dt=v("$sce");rt={HTML:"html",CSS:"css",URL:"url",RESOURCE_URL:"resourceUrl",JS:"js"};tt=v("$compile");b=t.createElement("a");oe=gt(n.location.href);ih.$inject=["$provide"];rh.$inject=["$locale"];uh.$inject=["$locale"];se=".";var dy={yyyy:d("FullYear",4),yy:d("FullYear",2,0,!0),y:d("FullYear",1),MMMM:wu("Month"),MMM:wu("Month",!0),MM:d("Month",2,1),M:d("Month",1,1),dd:d("Date",2),d:d("Date",1),HH:d("Hours",2),H:d("Hours",1),hh:d("Hours",2,-12),h:d("Hours",1,-12),mm:d("Minutes",2),m:d("Minutes",1),ss:d("Seconds",2),s:d("Seconds",1),sss:d("Milliseconds",3),EEEE:wu("Day"),EEE:wu("Day",!0),a:ky,Z:wy,ww:oh(2),w:oh(1)},gy=/((?:[^yMdHhmsaZEw']+)|(?:'(?:[^']|'')*')|(?:E+|y+|M+|d+|H+|h+|m+|s+|a|Z|w+))(.*)/,np=/^\-?\d+$/;sh.$inject=["$locale"];hh=nt(y);ch=nt(bi);lh.$inject=["$parse"];ah=nt({restrict:"E",compile:function(n,t){if(!t.href&&!t.xlinkHref&&!t.name)return function(n,t){if(t[0].nodeName.toLowerCase()==="a"){var i=ni.call(t.prop("href"))==="[object SVGAnimatedString]"?"xlink:href":"href";t.on("click",function(n){t.attr(i)||n.preventDefault()})}}}});ar={};r(sr,function(n,t){if(n!="multiple"){var i=bt("ng-"+t);ar[i]=function(){return{restrict:"A",priority:100,link:function(n,r,u){n.$watch(u[i],function(n){u.$set(t,!!n)})}}}}});r(yf,function(n,t){ar[t]=function(){return{priority:100,link:function(n,i,r){if(t==="ngPattern"&&r.ngPattern.charAt(0)=="/"){var u=r.ngPattern.match(yc);if(u){r.$set("ngPattern",new RegExp(u[1],u[2]));return}}n.$watch(r[t],function(n){r.$set(t,n)})}}}});r(["src","srcset","href"],function(n){var t=bt("ng-"+n);ar[t]=function(){return{priority:99,link:function(i,r,u){var e=n,f=n;n==="href"&&ni.call(r.prop("href"))==="[object SVGAnimatedString]"&&(f="xlinkHref",u.$attr[f]="xlink:href",e=null);u.$observe(t,function(t){if(!t){n==="href"&&u.$set(f,null);return}u.$set(f,t);si&&e&&r.prop(e,u[f])})}}}});vr={$addControl:s,$$renameControl:rp,$removeControl:s,$setValidity:s,$setDirty:s,$setPristine:s,$setSubmitted:s};he="ng-submitted";vh.$inject=["$element","$attrs","$scope","$animate","$interpolate"];var yh=function(n){return["$timeout",function(t){return{name:"form",restrict:n?"EAC":"E",controller:vh,compile:function(n){return n.addClass(wi).addClass(wr),{pre:function(n,r,u,f){var o,s,e;if(!("action"in u)){o=function(t){n.$apply(function(){f.$commitViewValue();f.$setSubmitted()});t.preventDefault()};uu(r[0],"submit",o);r.on("$destroy",function(){t(function(){er(r[0],"submit",o)},0,!1)})}s=f.$$parentForm;e=f.$name;e&&(lr(n,null,e,f,e),u.$observe(u.name?"name":"ngForm",function(t){e!==t&&(lr(n,null,e,i,e),e=t,lr(n,null,e,f,e),s.$$renameControl(f,e))}));r.on("$destroy",function(){s.$removeControl(f);e&&lr(n,null,e,i,e);a(f,vr)})}}}}}]},up=yh(),fp=yh(!0),ep=/\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/,op=/^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/,sp=/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i,hp=/^\s*(\-|\+)?(\d+|(\d*(\.\d*)))\s*$/,ph=/^(\d{4})-(\d{2})-(\d{2})$/,wh=/^(\d{4})-(\d\d)-(\d\d)T(\d\d):(\d\d)(?::(\d\d)(\.\d{1,3})?)?$/,ce=/^(\d{4})-W(\d\d)$/,bh=/^(\d{4})-(\d\d)$/,kh=/^(\d\d):(\d\d)(?::(\d\d)(\.\d{1,3})?)?$/,dh={text:cp,date:pr("date",ph,bu(ph,["yyyy","MM","dd"]),"yyyy-MM-dd"),"datetime-local":pr("datetimelocal",wh,bu(wh,["yyyy","MM","dd","HH","mm","ss","sss"]),"yyyy-MM-ddTHH:mm:ss.sss"),time:pr("time",kh,bu(kh,["HH","mm","ss","sss"]),"HH:mm:ss.sss"),week:pr("week",ce,lp,"yyyy-Www"),month:pr("month",bh,bu(bh,["yyyy","MM"]),"yyyy-MM"),number:ap,url:vp,email:yp,radio:pp,checkbox:wp,hidden:s,button:s,submit:s,reset:s,file:s};var tc=["$browser","$sniffer","$filter","$parse",function(n,t,i,r){return{restrict:"E",require:["?ngModel"],link:{pre:function(u,f,e,o){o[0]&&(dh[y(e.type)]||dh.text)(u,f,e,o[0],t,n,i,r)}}}}],bp=/^(true|false|\d+)$/,kp=function(){return{restrict:"A",priority:100,compile:function(n,t){return bp.test(t.ngValue)?function(n,t,i){i.$set("value",n.$eval(i.ngValue))}:function(n,t,i){n.$watch(i.ngValue,function(n){i.$set("value",n)})}}}},dp=["$compile",function(n){return{restrict:"AC",compile:function(t){return n.$$addBindingClass(t),function(t,r,u){n.$$addBindingInfo(r,u.ngBind);r=r[0];t.$watch(u.ngBind,function(n){r.textContent=n===i?"":n})}}}}],gp=["$interpolate","$compile",function(n,t){return{compile:function(r){return t.$$addBindingClass(r),function(r,u,f){var e=n(u.attr(f.$attr.ngBindTemplate));t.$$addBindingInfo(u,e.expressions);u=u[0];f.$observe("ngBindTemplate",function(n){u.textContent=n===i?"":n})}}}}],nw=["$sce","$parse","$compile",function(n,t,i){return{restrict:"A",compile:function(r,u){var f=t(u.ngBindHtml),e=t(u.ngBindHtml,function(n){return(n||"").toString()});return i.$$addBindingClass(r),function(t,r,u){i.$$addBindingInfo(r,u.ngBindHtml);t.$watch(e,function(){r.html(n.getTrustedHtml(f(t))||"")})}}}}],tw=nt({restrict:"A",require:"ngModel",link:function(n,t,i,r){r.$viewChangeListeners.push(function(){n.$eval(i.ngChange)})}});var iw=ae("",!0),rw=ae("Odd",0),uw=ae("Even",1),fw=oi({compile:function(n,t){t.$set("ngCloak",i);n.removeClass("ng-cloak")}}),ew=[function(){return{restrict:"A",scope:!0,controller:"@",priority:500}}],ic={},ow={blur:!0,focus:!0};r("click dblclick mousedown mouseup mouseover mouseout mousemove mouseenter mouseleave keydown keyup keypress submit focus blur copy cut paste".split(" "),function(n){var t=bt("ng-"+n);ic[t]=["$parse","$rootScope",function(i,r){return{restrict:"A",compile:function(u,f){var e=i(f[t],null,!0);return function(t,i){i.on(n,function(i){var u=function(){e(t,{$event:i})};ow[n]&&r.$$phase?t.$evalAsync(u):t.$apply(u)})}}}}]});var sw=["$animate",function(n){return{multiElement:!0,transclude:"element",priority:600,terminal:!0,restrict:"A",$$tlb:!0,link:function(i,r,u,f,e){var h,s,o;i.$watch(u.ngIf,function(i){i?s||e(function(i,f){s=f;i[i.length++]=t.createComment(" end ngIf: "+u.ngIf+" ");h={clone:i};n.enter(i,r.parent(),r)}):(o&&(o.remove(),o=null),s&&(s.$destroy(),s=null),h&&(o=tu(h.clone),n.leave(o).then(function(){o=null}),h=null))})}}}],hw=["$templateRequest","$anchorScroll","$animate","$sce",function(n,t,i,r){return{restrict:"ECA",priority:400,terminal:!0,transclude:"element",controller:ft.noop,compile:function(f,e){var s=e.ngInclude||e.src,h=e.onload||"",o=e.autoscroll;return function(f,e,c,l,a){var w=0,v,y,p,b=function(){y&&(y.remove(),y=null);v&&(v.$destroy(),v=null);p&&(i.leave(p).then(function(){y=null}),y=p,p=null)};f.$watch(r.parseAsResourceUrl(s),function(r){var c=function(){u(o)&&(!o||f.$eval(o))&&t()},s=++w;r?(n(r,!0).then(function(n){var t,u;s===w&&(t=f.$new(),l.template=n,u=a(t,function(n){b();i.enter(n,null,e).then(c)}),v=t,p=u,v.$emit("$includeContentLoaded",r),f.$eval(h))},function(){s===w&&(b(),f.$emit("$includeContentError",r))}),f.$emit("$includeContentRequested",r)):(b(),l.template=null)})}}}}],cw=["$compile",function(n){return{restrict:"ECA",priority:-400,require:"ngInclude",link:function(i,r,u,f){if(/SVG/.test(r[0].toString())){r.empty();n(vo(f.template,t).childNodes)(i,function(n){r.append(n)},{futureParentElement:r});return}r.html(f.template);n(r.contents())(i)}}}],lw=oi({priority:450,compile:function(){return{pre:function(n,t,i){n.$eval(i.ngInit)}}}}),aw=function(){return{restrict:"A",priority:100,require:"ngModel",link:function(n,t,u,f){var s=t.attr(u.$attr.ngList)||", ",h=u.ngTrim!=="false",c=h?p(s):s,l=function(n){if(!e(n)){var t=[];return n&&r(n.split(c),function(n){n&&t.push(h?p(n):n)}),t}};f.$parsers.push(l);f.$formatters.push(function(n){return o(n)?n.join(s):i});f.$isEmpty=function(n){return!n||!n.length}}}},wr="ng-valid",rc="ng-invalid",wi="ng-pristine",ku="ng-dirty",ve="ng-untouched",uc="ng-touched",fc="ng-pending",du=new v("ngModel"),vw=["$scope","$exceptionHandler","$attrs","$element","$parse","$animate","$timeout","$rootScope","$q","$interpolate",function(n,t,f,o,h,c,a,v,y,p){var tt,d;this.$viewValue=Number.NaN;this.$modelValue=Number.NaN;this.$$rawModelValue=i;this.$validators={};this.$asyncValidators={};this.$parsers=[];this.$formatters=[];this.$viewChangeListeners=[];this.$untouched=!0;this.$touched=!1;this.$pristine=!0;this.$dirty=!1;this.$valid=!0;this.$invalid=!1;this.$error={};this.$$success={};this.$pending=i;this.$name=p(f.name||"",!1)(n);var b=h(f.ngModel),it=b.assign,nt=b,rt=it,g=null,w=this;this.$$setOptions=function(n){if(w.$options=n,n&&n.getterSetter){var t=h(f.ngModel+"()"),i=h(f.ngModel+"($$$p)");nt=function(n){var i=b(n);return l(i)&&(i=t(n)),i};rt=function(n){l(b(n))?i(n,{$$$p:w.$modelValue}):it(n,w.$modelValue)}}else if(!b.assign)throw du("nonassign","Expression '{0}' is non-assignable. Element: {1}",f.ngModel,wt(o));};this.$render=s;this.$isEmpty=function(n){return e(n)||n===""||n===null||n!==n};tt=o.inheritedData("$formController")||vr;d=0;ec({ctrl:this,$element:o,set:function(n,t){n[t]=!0},unset:function(n,t){delete n[t]},parentForm:tt,$animate:c});this.$setPristine=function(){w.$dirty=!1;w.$pristine=!0;c.removeClass(o,ku);c.addClass(o,wi)};this.$setDirty=function(){w.$dirty=!0;w.$pristine=!1;c.removeClass(o,wi);c.addClass(o,ku);tt.$setDirty()};this.$setUntouched=function(){w.$touched=!1;w.$untouched=!0;c.setClass(o,ve,uc)};this.$setTouched=function(){w.$touched=!0;w.$untouched=!1;c.setClass(o,uc,ve)};this.$rollbackViewValue=function(){a.cancel(g);w.$viewValue=w.$$lastCommittedViewValue;w.$render()};this.$validate=function(){if(!k(w.$modelValue)||!isNaN(w.$modelValue)){var t=w.$$lastCommittedViewValue,n=w.$$rawModelValue,r=w.$$parserName||"parse",u=w.$error[r]?!1:i,f=w.$valid,e=w.$modelValue,o=w.$options&&w.$options.allowInvalid;w.$$runValidators(u,n,t,function(t){o||f===t||(w.$modelValue=t?n:i,w.$modelValue!==e&&w.$$writeModelToScope())})}};this.$$runValidators=function(n,t,u,f){function c(n){var t=w.$$parserName||"parse";if(n===i)e(t,null);else if(e(t,n),!n)return r(w.$validators,function(n,t){e(t,null)}),r(w.$asyncValidators,function(n,t){e(t,null)}),!1;return!0}function l(){var n=!0;return(r(w.$validators,function(i,r){var f=i(t,u);n=n&&f;e(r,f)}),!n)?(r(w.$asyncValidators,function(n,t){e(t,null)}),!1):!0}function a(){var n=[],f=!0;r(w.$asyncValidators,function(r,o){var s=r(t,u);if(!dr(s))throw du("$asyncValidators","Expected asynchronous validator to return a promise but got '{0}' instead.",s);e(o,i);n.push(s.then(function(){e(o,!0)},function(){f=!1;e(o,!1)}))});n.length?y.all(n).then(function(){o(f)},s):o(!0)}function e(n,t){h===d&&w.$setValidity(n,t)}function o(n){h===d&&f(n)}d++;var h=d;if(!c(n)){o(!1);return}if(!l()){o(!1);return}a()};this.$commitViewValue=function(){var n=w.$viewValue;(a.cancel(g),w.$$lastCommittedViewValue!==n||n===""&&w.$$hasNativeValidators)&&(w.$$lastCommittedViewValue=n,w.$pristine&&this.$setDirty(),this.$$parseAndValidate())};this.$$parseAndValidate=function(){function s(){w.$modelValue!==o&&w.$$writeModelToScope()}var h=w.$$lastCommittedViewValue,t=h,u=e(t)?i:!0,r,o,f;if(u)for(r=0;r<w.$parsers.length;r++)if(t=w.$parsers[r](t),e(t)){u=!1;break}k(w.$modelValue)&&isNaN(w.$modelValue)&&(w.$modelValue=nt(n));o=w.$modelValue;f=w.$options&&w.$options.allowInvalid;w.$$rawModelValue=t;f&&(w.$modelValue=t,s());w.$$runValidators(u,t,w.$$lastCommittedViewValue,function(n){f||(w.$modelValue=n?t:i,s())})};this.$$writeModelToScope=function(){rt(n,w.$modelValue);r(w.$viewChangeListeners,function(n){try{n()}catch(i){t(i)}})};this.$setViewValue=function(n,t){w.$viewValue=n;(!w.$options||w.$options.updateOnDefault)&&w.$$debounceViewValueCommit(t)};this.$$debounceViewValueCommit=function(t){var r=0,f=w.$options,i;f&&u(f.debounce)&&(i=f.debounce,k(i)?r=i:k(i[t])?r=i[t]:k(i["default"])&&(r=i["default"]));a.cancel(g);r?g=a(function(){w.$commitViewValue()},r):v.$$phase?w.$commitViewValue():n.$apply(function(){w.$commitViewValue()})};n.$watch(function(){var t=nt(n);if(t!==w.$modelValue){w.$modelValue=w.$$rawModelValue=t;for(var u=w.$formatters,f=u.length,r=t;f--;)r=u[f](r);w.$viewValue!==r&&(w.$viewValue=w.$$lastCommittedViewValue=r,w.$render(),w.$$runValidators(i,t,r,s))}return t})}],yw=["$rootScope",function(n){return{restrict:"A",require:["ngModel","^?form","^?ngModelOptions"],controller:vw,priority:1,compile:function(t){return t.addClass(wi).addClass(ve).addClass(wr),{pre:function(n,t,i,r){var u=r[0],f=r[1]||vr;u.$$setOptions(r[2]&&r[2].$options);f.$addControl(u);i.$observe("name",function(n){u.$name!==n&&f.$$renameControl(u,n)});n.$on("$destroy",function(){f.$removeControl(u)})},post:function(t,i,r,u){var f=u[0];if(f.$options&&f.$options.updateOn)i.on(f.$options.updateOn,function(n){f.$$debounceViewValueCommit(n&&n.type)});i.on("blur",function(){f.$touched||(n.$$phase?t.$evalAsync(f.$setTouched):t.$apply(f.$setTouched))})}}}}}],pw=/(\s+|^)default(\s+|$)/,ww=function(){return{restrict:"A",controller:["$scope","$attrs",function(n,t){var r=this;this.$options=n.$eval(t.ngModelOptions);this.$options.updateOn!==i?(this.$options.updateOnDefault=!1,this.$options.updateOn=p(this.$options.updateOn.replace(pw,function(){return r.$options.updateOnDefault=!0," "}))):this.$options.updateOnDefault=!0}]}};var bw=oi({terminal:!0,priority:1e3}),kw=["$locale","$interpolate",function(n,t){var i=/{}/g,u=/^when(Minus)?(.+)$/;return{restrict:"EA",link:function(f,e,o){function d(n){e.text(n||"")}var c=o.count,p=o.$attr.when&&e.attr(o.$attr.when),l=o.offset||0,s=f.$eval(p)||{},a={},w=t.startSymbol(),b=t.endSymbol(),k=w+c+"-"+l+b,v=ft.noop,h;r(o,function(n,t){var i=u.exec(t),r;i&&(r=(i[1]?"-":"")+y(i[2]),s[r]=e.attr(o.$attr[t]))});r(s,function(n,r){a[r]=t(n.replace(i,k))});f.$watch(c,function(t){var i=parseFloat(t),r=isNaN(i);r||i in s||(i=n.pluralCat(i-l));i===h||r&&isNaN(h)||(v(),v=f.$watch(a[i],d),h=i)})}}}],dw=["$parse","$animate",function(n,u){var o="$$NG_REMOVED",e=v("ngRepeat"),s=function(n,t,i,r,u,f,e){n[i]=r;u&&(n[u]=f);n.$index=t;n.$first=t===0;n.$last=t===e-1;n.$middle=!(n.$first||n.$last);n.$odd=!(n.$even=(t&1)==0)},h=function(n){return n.clone[0]},c=function(n){return n.clone[n.clone.length-1]};return{restrict:"A",multiElement:!0,transclude:"element",priority:1e3,terminal:!0,$$tlb:!0,compile:function(l,a){var b=a.ngRepeat,ut=t.createComment(" end ngRepeat: "+b+" "),v=b.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+track\s+by\s+([\s\S]+?))?\s*$/),k,p,d,g,it,rt,w;if(!v)throw e("iexp","Expected expression in form of '_item_ in _collection_[ track by _id_]' but got '{0}'.",b);var nt=v[1],ft=v[2],y=v[3],tt=v[4];if(v=nt.match(/^(?:(\s*[\$\w]+)|\(\s*([\$\w]+)\s*,\s*([\$\w]+)\s*\))$/),!v)throw e("iidexp","'_item_' in '_item_ in _collection_' should be an identifier or '(_key_, _value_)' expression, but got '{0}'.",nt);if(k=v[3]||v[1],p=v[2],y&&(!/^[$a-zA-Z_][$a-zA-Z0-9_]*$/.test(y)||/^(null|undefined|this|\$index|\$first|\$middle|\$last|\$even|\$odd|\$parent|\$root|\$id)$/.test(y)))throw e("badident","alias '{0}' is invalid --- must be a valid JS identifier which is not a reserved name.",y);return w={$id:ai},tt?d=n(tt):(it=function(n,t){return ai(t)},rt=function(n){return n}),function(n,t,l,a,v){d&&(g=function(t,i,r){return p&&(w[p]=t),w[k]=i,w.$index=r,d(n,w)});var nt=ot();n.$watchCollection(ft,function(l){var a,wt,ct=t[0],et,lt=ot(),st,ft,ht,d,pt,tt,w,at,vt,yt,bt;if(y&&(n[y]=l),di(l))tt=l,pt=g||it;else{pt=g||rt;tt=[];for(yt in l)l.hasOwnProperty(yt)&&yt.charAt(0)!="$"&&tt.push(yt);tt.sort()}for(st=tt.length,at=new Array(st),a=0;a<st;a++)if(ft=l===tt?a:tt[a],ht=l[ft],d=pt(ft,ht,a),nt[d])w=nt[d],delete nt[d],lt[d]=w,at[a]=w;else if(lt[d]){r(at,function(n){n&&n.scope&&(nt[n.id]=n)});throw e("dupes","Duplicates in a repeater are not allowed. Use 'track by' expression to specify unique keys. Repeater: {0}, Duplicate key: {1}, Duplicate value: {2}",b,d,ht);}else at[a]={id:d,scope:i,clone:i},lt[d]=!0;for(bt in nt){if(w=nt[bt],vt=tu(w.clone),u.leave(vt),vt[0].parentNode)for(a=0,wt=vt.length;a<wt;a++)vt[a][o]=!0;w.scope.$destroy()}for(a=0;a<st;a++)if(ft=l===tt?a:tt[a],ht=l[ft],w=at[a],w.scope){et=ct;do et=et.nextSibling;while(et&&et[o]);h(w)!=et&&u.move(tu(w.clone),null,f(ct));ct=c(w);s(w.scope,a,k,ht,p,ft,st)}else v(function(n,t){w.scope=t;var i=ut.cloneNode(!1);n[n.length++]=i;u.enter(n,null,f(ct));ct=i;w.clone=n;lt[w.id]=w;s(w.scope,a,k,ht,p,ft,st)});nt=lt})}}}}],sc="ng-hide",hc="ng-hide-animate",gw=["$animate",function(n){return{restrict:"A",multiElement:!0,link:function(t,i,r){t.$watch(r.ngShow,function(t){n[t?"removeClass":"addClass"](i,sc,{tempClasses:hc})})}}}],nb=["$animate",function(n){return{restrict:"A",multiElement:!0,link:function(t,i,r){t.$watch(r.ngHide,function(t){n[t?"addClass":"removeClass"](i,sc,{tempClasses:hc})})}}}],tb=oi(function(n,t,i){n.$watchCollection(i.ngStyle,function(n,i){i&&n!==i&&r(i,function(n,i){t.css(i,"")});n&&t.css(n)})}),ib=["$animate",function(n){return{restrict:"EA",require:"ngSwitch",controller:["$scope",function(){this.cases={}}],link:function(i,u,f,e){var l=f.ngSwitch||f.on,c=[],h=[],o=[],s=[],a=function(n,t){return function(){n.splice(t,1)}};i.$watch(l,function(i){for(var l,v,u=0,f=o.length;u<f;++u)n.cancel(o[u]);for(o.length=0,u=0,f=s.length;u<f;++u)l=tu(h[u].clone),s[u].$destroy(),v=o[u]=n.leave(l),v.then(a(o,u));h.length=0;s.length=0;(c=e.cases["!"+i]||e.cases["?"])&&r(c,function(i){i.transclude(function(r,u){var f,e;s.push(u);f=i.element;r[r.length++]=t.createComment(" end ngSwitchWhen: ");e={clone:r};h.push(e);n.enter(r,f.parent(),f)})})})}}}],rb=oi({transclude:"element",priority:1200,require:"^ngSwitch",multiElement:!0,link:function(n,t,i,r,u){r.cases["!"+i.ngSwitchWhen]=r.cases["!"+i.ngSwitchWhen]||[];r.cases["!"+i.ngSwitchWhen].push({transclude:u,element:t})}}),ub=oi({transclude:"element",priority:1200,require:"^ngSwitch",multiElement:!0,link:function(n,t,i,r,u){r.cases["?"]=r.cases["?"]||[];r.cases["?"].push({transclude:u,element:t})}}),fb=oi({restrict:"EAC",link:function(n,t,i,r,u){if(!u)throw v("ngTransclude")("orphan","Illegal use of ngTransclude directive in the template! No parent directive that requires a transclusion found. Element: {0}",wt(t));u(function(n){t.empty();t.append(n)})}}),eb=["$templateCache",function(n){return{restrict:"E",terminal:!0,compile:function(t,i){if(i.type=="text/ng-template"){var r=i.id,u=t[0].text;n.put(r,u)}}}}],ob=v("ngOptions"),sb=nt({restrict:"A",terminal:!0}),hb=["$compile","$parse",function(n,h){var c=/^\s*([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+group\s+by\s+([\s\S]+?))?\s+for\s+(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+([\s\S]+?)(?:\s+track\s+by\s+([\s\S]+?))?$/,l={$setViewValue:s};return{restrict:"E",require:["select","?ngModel"],controller:["$element","$scope","$attrs",function(n,t,i){var r=this,f={},e=l,o,u;r.databound=i.ngModel;r.init=function(n,t,i){e=n;o=t;u=i};r.addOption=function(t,i){li(t,'"option value"');f[t]=!0;e.$viewValue==t&&(n.val(t),u.parent()&&u.remove());i&&i[0].hasAttribute("selected")&&(i[0].selected=!0)};r.removeOption=function(n){this.hasOption(n)&&(delete f[n],e.$viewValue===n&&this.renderUnknownOption(n))};r.renderUnknownOption=function(t){var i="? "+ai(t)+" ?";u.val(i);n.prepend(u);n.val(i);u.prop("selected",!0)};r.hasOption=function(n){return f.hasOwnProperty(n)};t.$on("$destroy",function(){r.renderUnknownOption=s})}],link:function(s,l,a,v){function ot(n,t,i,r){i.$render=function(){var n=i.$viewValue;r.hasOption(n)?(b.parent()&&b.remove(),t.val(n),n===""&&nt.prop("selected",!0)):e(n)&&nt?t.val(""):r.renderUnknownOption(n)};t.on("change",function(){n.$apply(function(){b.parent()&&b.remove();i.$setViewValue(t.val())})})}function st(n,t,i){var f;i.$render=function(){var n=new hr(i.$viewValue);r(t.find("option"),function(t){t.selected=u(n.get(t.value))})};n.$watch(function(){et(f,i.$viewValue)||(f=at(i.$viewValue),i.$render())});t.on("change",function(){n.$apply(function(){var n=[];r(t.find("option"),function(t){t.selected&&n.push(t.value)});i.$setViewValue(n)})})}function ht(t,f,e){function a(n,i,r){return ft[ht]=r,v&&(ft[v]=i),n(t,ft)}function pt(){t.$apply(function(){var u=nt(t)||[],n,i;p?(n=[],r(f.val(),function(t){t=l?it[t]:t;n.push(at(t,u[t]))})):(i=l?it[f.val()]:f.val(),n=at(i,u[i]));e.$setViewValue(n);st()})}function at(n,t){if(n==="?")return i;if(n==="")return null;var r=d?d:lt;return a(r,n,t)}function bt(){var n=nt(t),i,r,f,u;if(n&&o(n)){for(i=new Array(n.length),r=0,f=n.length;r<f;r++)i[r]=a(et,r,n[r]);return i}if(n){i={};for(u in n)n.hasOwnProperty(u)&&(i[u]=a(et,u,n[u]))}return i}function kt(n){var t,i;if(p)if(l&&o(n))for(t=new hr([]),i=0;i<n.length;i++)t.put(a(l,null,n[i]),!0);else t=new hr(n);else l&&(n=a(l,null,n));return function(i,r){var f;return f=l?l:d?d:lt,p?u(t.remove(a(f,i,r))):n===a(f,i,r)}}function ot(){tt||(t.$$postDigest(st),tt=!0)}function b(n,t,i){n[t]=n[t]||0;n[t]+=i?1:-1}function st(){tt=!1;var lt={"":[]},bt=[""],c,o,n,d,g,s,ii=e.$viewValue,dt=nt(t)||[],gt=v?pe(dt):dt,ot,yt,ri,pt,st,i,ht={},ni,ui=kt(ii),wt=!1,h,ct,at,ti;for(it={},i=0;pt=gt.length,i<pt;i++)(ot=i,v&&(ot=gt[i],ot.charAt(0)==="$"))||(yt=dt[ot],c=a(vt,ot,yt)||"",(o=lt[c])||(o=lt[c]=[],bt.push(c)),ni=ui(ot,yt),wt=wt||ni,at=a(et,ot,yt),at=u(at)?at:"",ti=l?l(t,ft):v?gt[i]:i,l&&(it[ti]=ot),o.push({id:ti,label:at,selected:ni}));for(p||(y||ii===null?lt[""].unshift({id:"",label:"",selected:!wt}):wt||lt[""].unshift({id:"?",label:"",selected:!0})),st=0,ri=bt.length;st<ri;st++){for(c=bt[st],o=lt[c],w.length<=st?(d={element:ut.clone().attr("label",c),label:o.label},g=[d],w.push(g),f.append(d.element)):(g=w[st],d=g[0],d.label!=c&&d.element.attr("label",d.label=c)),h=null,i=0,pt=o.length;i<pt;i++)n=o[i],(s=g[i+1])?(h=s.element,s.label!==n.label&&(b(ht,s.label,!1),b(ht,n.label,!0),h.text(s.label=n.label),h.prop("label",s.label)),s.id!==n.id&&h.val(s.id=n.id),h[0].selected!==n.selected&&(h.prop("selected",s.selected=n.selected),si&&h.prop("selected",s.selected))):(n.id===""&&y?ct=y:(ct=rt.clone()).val(n.id).prop("selected",n.selected).attr("selected",n.selected).prop("label",n.label).text(n.label),g.push(s={element:ct,label:n.label,id:n.id,selected:n.selected}),b(ht,n.label,!0),h?h.after(ct):d.element.append(ct),h=ct);for(i++;g.length>i;)n=g.pop(),b(ht,n.label,!1),n.element.remove()}while(w.length>st){for(o=w.pop(),i=1;i<o.length;++i)b(ht,o[i].label,!1);o[0].element.remove()}r(ht,function(n,t){n>0?k.addOption(t):n<0&&k.removeOption(t)})}var s;if(!(s=g.match(c)))throw ob("iexp","Expected expression in form of '_select_ (as _label_)? for (_key_,)?_value_ in _collection_' but got '{0}'. Element: {1}",g,wt(f));var et=h(s[2]||s[1]),ht=s[4]||s[6],ct=/ as /.test(s[0])&&s[1],d=ct?h(ct):null,v=s[5],vt=h(s[3]||""),lt=h(s[2]?s[1]:ht),nt=h(s[7]),yt=s[8],l=yt?h(s[8]):null,it={},w=[[{element:f,label:""}]],ft={};y&&(n(y)(t),y.removeClass("ng-scope"),y.remove());f.empty();f.on("change",pt);e.$render=st;t.$watchCollection(nt,ot);t.$watchCollection(bt,ot);p&&t.$watchCollection(function(){return e.$modelValue},ot)}if(v[1]){for(var k=v[0],w=v[1],p=a.multiple,g=a.ngOptions,y=!1,nt,tt=!1,rt=f(t.createElement("option")),ut=f(t.createElement("optgroup")),b=rt.clone(),d=0,it=l.children(),ft=it.length;d<ft;d++)if(it[d].value===""){nt=y=it.eq(d);break}k.init(w,y,b);p&&(w.$isEmpty=function(n){return!n||n.length===0});g?ht(s,l,w):p?st(s,l,w):ot(s,l,w,k)}}}}],cb=["$interpolate",function(n){var t={addOption:s,removeOption:s};return{restrict:"E",priority:100,compile:function(i,r){if(e(r.value)){var u=n(i.text(),!0);u||r.$set("value",i.text())}return function(n,i,r){var e="$selectController",o=i.parent(),f=o.data(e)||o.parent().data(e);f&&f.databound||(f=t);u?n.$watch(u,function(n,t){r.$set("value",n);t!==n&&f.removeOption(t);f.addOption(n,i)}):f.addOption(r.value,i);i.on("$destroy",function(){f.removeOption(r.value)})}}}}],lb=nt({restrict:"E",terminal:!1}),cc=function(){return{restrict:"A",require:"?ngModel",link:function(n,t,i,r){r&&(i.required=!0,r.$validators.required=function(n,t){return!i.required||!r.$isEmpty(t)},i.$observe("required",function(){r.$validate()}))}}},lc=function(){return{restrict:"A",require:"?ngModel",link:function(n,t,r,u){if(u){var f,o=r.ngPattern||r.pattern;r.$observe("pattern",function(n){if(c(n)&&n.length>0&&(n=new RegExp("^"+n+"$")),n&&!n.test)throw v("ngPattern")("noregexp","Expected {0} to be a RegExp but was {1}. Element: {2}",o,n,wt(t));f=n||i;u.$validate()});u.$validators.pattern=function(n){return u.$isEmpty(n)||e(f)||f.test(n)}}}}},ac=function(){return{restrict:"A",require:"?ngModel",link:function(n,t,i,r){if(r){var u=-1;i.$observe("maxlength",function(n){var t=g(n);u=isNaN(t)?-1:t;r.$validate()});r.$validators.maxlength=function(n,t){return u<0||r.$isEmpty(n)||t.length<=u}}}}},vc=function(){return{restrict:"A",require:"?ngModel",link:function(n,t,i,r){if(r){var u=0;i.$observe("minlength",function(n){u=g(n)||0;r.$validate()});r.$validators.minlength=function(n,t){return r.$isEmpty(t)||t.length>=u}}}}};if(n.angular.bootstrap){console.log("WARNING: Tried to load angular more than once.");return}hl();al(ft);f(t).ready(function(){el(t,uo)})})(window,document);window.angular.$$csp()||window.angular.element(document).find("head").prepend('<style type="text/css">@charset "UTF-8";[ng\\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide:not(.ng-hide-animate){display:none !important;}ng\\:form{display:block;}<\/style>');
/*
//# sourceMappingURL=angular.min.js.map
*/
///#source 1 1 /appnew/bower_components/angular-animate/angular-animate.min.js
(function(n,t,i){"use strict";t.module("ngAnimate",["ng"]).directive("ngAnimateChildren",function(){var n="$$ngAnimateChildren";return function(i,r,u){var f=u.ngAnimateChildren;t.isString(f)&&f.length===0?r.data(n,!0):i.$watch(f,function(t){r.data(n,!!t)})}}).factory("$$animateReflow",["$$rAF","$document",function(n,t){var i=t[0].body;return function(t){return n(function(){var n=i.offsetWidth+1;t()})}}]).config(["$provide","$animateProvider",function(r,u){function c(n){for(var i,t=0;t<n.length;t++)if(i=n[t],i.nodeType==k)return i}function y(n){return n&&t.element(n)}function l(n){return t.element(c(n))}function p(n,t){return c(n)==c(t)}var s=t.noop,f=t.forEach,w=u.$$selectors,a=t.isArray,b=t.isString,d=t.isObject,k=1,e="$$ngAnimateState",g="$$ngAnimateChildren",v="ng-animate",h={running:!0},o;r.decorator("$animate",["$delegate","$$q","$injector","$sniffer","$rootElement","$$asyncCallback","$rootScope","$document","$templateRequest","$$jqLite",function(n,i,r,k,nt,tt,it,rt,ut,ft){function ct(n,t){var i=n.data(e)||{};return t&&(i.running=!0,i.structural=!0,n.data(e,i)),i.disabled||i.running&&i.structural}function ot(n){var t,r=i.defer();return r.promise.$$cancelFn=function(){t&&t()},it.$$postDigest(function(){t=n(function(){r.resolve()})}),r.promise}function st(n){if(d(n))return n.tempClasses&&b(n.tempClasses)&&(n.tempClasses=n.tempClasses.split(/\s+/)),n}function wt(n,t,i){var e,o,r,u;return i=i||{},e={},f(i,function(n,t){f(t.split(" "),function(t){e[t]=n})}),o=Object.create(null),f((n.attr("class")||"").split(/\s+/),function(n){o[n]=!0}),r=[],u=[],f(t&&t.classes||[],function(n,t){var i=o[t],f=e[t]||{};n===!1?(i||f.event=="addClass")&&u.push(t):n===!0&&(i&&f.event!="removeClass"||r.push(t))}),r.length+u.length>0&&[r.join(" "),u.join(" ")]}function bt(n){var t,i,f;if(n){var u=[],e={},o=n.substr(1).split(".");for((k.transitions||k.animations)&&u.push(r.get(w[""])),t=0;t<o.length;t++)i=o[t],f=w[i],f&&!e[i]&&(u.push(r.get(f)),e[i]=!0);return u}}function kt(n,i,r,u){function p(n,t){var i=n[t],r=n["before"+t.charAt(0).toUpperCase()+t.substr(1)];if(i||r)return t=="leave"&&(r=i,i=null),d.push({event:t,fn:i}),k.push({event:t,fn:r}),!0}function g(t,i,h){function a(n){if(i){if((i[n]||s)(),++l<c.length)return;i=null}h()}var c=[],l;f(t,function(n){n.fn&&c.push(n)});l=0;f(c,function(t,f){var s=function(){a(f)};switch(t.event){case"setClass":i.push(t.fn(n,e,o,s,u));break;case"animate":i.push(t.fn(n,r,u.from,u.to,s));break;case"addClass":i.push(t.fn(n,e||r,s,u));break;case"removeClass":i.push(t.fn(n,o||r,s,u));break;default:i.push(t.fn(n,s,u))}});i&&i.length===0&&h()}var w=n[0],e,o;if(w){u&&(u.to=u.to||{},u.from=u.from||{});a(r)&&(e=r[0],o=r[1],e?o?r=e+" "+o:(r=e,i="addClass"):(r=o,i="removeClass"));var h=i=="setClass",nt=h||i=="addClass"||i=="removeClass"||i=="animate",tt=n.attr("class"),b=tt+" "+r;if(pt(b)){var c=s,l=[],k=[],v=s,y=[],d=[],it=(" "+b).replace(/\s+/g,".");return f(bt(it),function(n){var t=p(n,i);!t&&h&&(p(n,"addClass"),p(n,"removeClass"))}),{node:w,event:i,className:r,isClassBased:nt,isSetClassOperation:h,applyStyles:function(){u&&n.css(t.extend(u.from||{},u.to||{}))},before:function(n){c=n;g(k,l,function(){c=s;n()})},after:function(n){v=n;g(d,y,function(){v=s;n()})},cancel:function(){l&&(f(l,function(n){(n||s)(!0)}),c(!0));y&&(f(y,function(n){(n||s)(!0)}),v(!0))}}}}}function ht(n,i,r,u,h,c,l,a){function ft(t){var u="$animate:"+t;w&&w[u]&&w[u].length>0&&tt(function(){r.triggerHandler(u,{event:n,className:i})})}function ot(){ft("before")}function st(){ft("after")}function at(){ft("close");a()}function g(){g.hasBeenRun||(g.hasBeenRun=!0,c())}function nt(){if(!nt.hasBeenRun){y&&y.applyStyles();nt.hasBeenRun=!0;l&&l.tempClasses&&f(l.tempClasses,function(n){o.removeClass(r,n)});var t=r.data(e);t&&(y&&y.isClassBased?et(r,i):(tt(function(){var t=r.data(e)||{};ut==t.index&&et(r,i,n)}),r.data(e,t)));at()}}var ht=s,y=kt(r,n,i,l),w,k,lt,rt,ut;if(!y||(n=y.event,i=y.className,w=t.element._data(y.node),w=w&&w.events,u||(u=h?h.parent():r.parent()),dt(r,u)))return g(),ot(),st(),nt(),ht;var b=r.data(e)||{},p=b.active||{},it=b.totalActive||0,ct=b.last,d=!1;if(it>0){if(k=[],y.isClassBased)ct.event=="setClass"?(k.push(ct),et(r,i)):p[i]&&(rt=p[i],rt.event==n?d=!0:(k.push(rt),et(r,i)));else if(n=="leave"&&p["ng-leave"])d=!0;else{for(lt in p)k.push(p[lt]);b={};et(r,!0)}k.length>0&&f(k,function(n){n.cancel()})}if(!y.isClassBased||y.isSetClassOperation||n=="animate"||d||(d=n=="addClass"==r.hasClass(i)),d)return g(),ot(),st(),at(),ht;if(p=b.active||{},it=b.totalActive||0,n=="leave")r.one("$destroy",function(){var i=t.element(this),r=i.data(e),n;r&&(n=r.active["ng-leave"],n&&(n.cancel(),et(i,"ng-leave")))});return o.addClass(r,v),l&&l.tempClasses&&f(l.tempClasses,function(n){o.addClass(r,n)}),ut=yt++,it++,p[i]=y,r.data(e,{last:y,active:p,index:ut,totalActive:it}),ot(),y.before(function(t){var u=r.data(e);t=t||!u||!u.active[i]||y.isClassBased&&u.active[i].event!=n;g();t===!0?nt():(st(),y.after(nt))}),y.cancel}function at(n){var i=c(n),r;i&&(r=t.isFunction(i.getElementsByClassName)?i.getElementsByClassName(v):i.querySelectorAll("."+v),f(r,function(n){n=t.element(n);var i=n.data(e);i&&i.active&&f(i.active,function(n){n.cancel()})}))}function et(n,t){if(p(n,nt))h.disabled||(h.running=!1,h.structural=!1);else if(t){var i=n.data(e)||{},r=t===!0;!r&&i.active&&i.active[t]&&(i.totalActive--,delete i.active[t]);(r||!i.totalActive)&&(o.removeClass(n,v),n.removeData(e))}}function dt(n,i){var u,f,c,o,r,s;if(h.disabled)return!0;if(p(n,nt))return h.running;do{if(i.length===0)break;if(o=p(i,nt),r=o?h:i.data(e)||{},r.disabled)return!0;o&&(c=!0);u!==!1&&(s=i.data(g),t.isDefined(s)&&(u=s));f=f||r.running||r.last&&!r.last.isClassBased}while(i=i.parent());return!c||!u&&f}o=ft;nt.data(e,h);var vt=it.$watch(function(){return ut.totalPendingRequests},function(n){n===0&&(vt(),it.$$postDigest(function(){it.$$postDigest(function(){h.running=!1})}))}),yt=0,lt=u.classNameFilter(),pt=lt?function(n){return lt.test(n)}:function(){return!0};return{animate:function(n,t,i,r,u){return r=r||"ng-inline-animate",u=st(u)||{},u.from=i?t:null,u.to=i?i:t,ot(function(t){return ht("animate",r,l(n),null,null,s,u,t)})},enter:function(i,r,u,f){return f=st(f),i=t.element(i),r=y(r),u=y(u),ct(i,!0),n.enter(i,r,u),ot(function(n){return ht("enter","ng-enter",l(i),r,u,s,f,n)})},leave:function(i,r){return r=st(r),i=t.element(i),at(i),ct(i,!0),ot(function(t){return ht("leave","ng-leave",l(i),null,null,function(){n.leave(i)},r,t)})},move:function(i,r,u,f){return f=st(f),i=t.element(i),r=y(r),u=y(u),at(i),ct(i,!0),n.move(i,r,u),ot(function(n){return ht("move","ng-move",l(i),r,u,s,f,n)})},addClass:function(n,t,i){return this.setClass(n,t,[],i)},removeClass:function(n,t,i){return this.setClass(n,[],t,i)},setClass:function(i,r,u,o){var h,v,s,y;return(o=st(o),h="$$animateClasses",i=t.element(i),i=l(i),ct(i))?n.$$setClassImmediately(i,r,u,o):(s=i.data(h),y=!!s,s||(s={},s.classes={}),v=s.classes,r=a(r)?r:r.split(" "),f(r,function(n){n&&n.length&&(v[n]=!0)}),u=a(u)?u:u.split(" "),f(u,function(n){n&&n.length&&(v[n]=!1)}),y)?(o&&s.options&&(s.options=t.extend(s.options||{},o)),s.promise):(i.data(h,s={classes:v,options:o}),s.promise=ot(function(t){var l=i.parent(),f=c(i),o=f.parentNode,u,s,r;if(!o||o.$$NG_REMOVED||f.$$NG_REMOVED){t();return}return u=i.data(h),i.removeData(h),s=i.data(e)||{},r=wt(i,u,s.active),r?ht("setClass",r,i,l,null,function(){r[0]&&n.$$addClassImmediately(i,r[0]);r[1]&&n.$$removeClassImmediately(i,r[1])},u.options,t):t()}))},cancel:function(n){n.$$cancelFn()},enabled:function(n,t){switch(arguments.length){case 2:if(n)et(t);else{var i=t.data(e)||{};i.disabled=!0;t.data(e,i)}break;case 1:h.disabled=!n;break;default:n=!h.disabled}return!!n}}}]);u.register("",["$window","$sniffer","$timeout","$$animateReflow",function(r,u,e,h){function nt(){w||(w=h(function(){g=[];w=null;p={}}))}function tt(n,t){w&&w();g.push(t);w=h(function(){f(g,function(n){n()});g=[];w=null;p={}})}function fi(n,i){var u=c(n),r;(n=t.element(u),at.push(n),r=Date.now()+i,r<=dt)||(e.cancel(kt),dt=r,kt=e(function(){ei(at);at=[]},i,!1))}function ei(n){f(n,function(n){var t=n.data(y);t&&f(t.closeAnimationFns,function(n){n()})})}function vt(n,t){var i=t?p[t]:null;if(!i){var u=0,e=0,o=0,s=0;f(n,function(n){var t,f,h,c,i;n.nodeType==k&&(t=r.getComputedStyle(n)||{},f=t[d+pt],u=Math.max(it(f),u),h=t[d+ct],e=Math.max(it(h),e),c=t[v+ct],s=Math.max(it(t[v+ct]),s),i=it(t[v+pt]),i>0&&(i*=parseInt(t[v+ti],10)||1),o=Math.max(i,o))});i={total:0,transitionDelay:e,transitionDuration:u,animationDelay:s,animationDuration:o};t&&(p[t]=i)}return i}function it(n){var t=0,i=b(n)?n.split(/\s*,\s*/):[];return f(i,function(n){t=Math.max(parseFloat(n)||0,t)}),t}function oi(n){var i=n.parent(),t=i.data(wt);return t||(i.data(wt,++bt),t=bt),t+"-"+c(n).getAttribute("class")}function si(n,t,i,r){var h=["ng-enter","ng-leave","ng-move"].indexOf(i)>=0,l=oi(t),u=l+" "+i,a=p[u]?++p[u].total:0,f={},s;if(a>0){var e=i+"-stagger",v=l+" "+e,w=!p[v];w&&o.addClass(t,e);f=vt(t,v);w&&o.removeClass(t,e)}o.addClass(t,i);var b=t.data(y)||{},k=vt(t,u),d=k.transitionDuration,g=k.animationDuration;if(h&&d===0&&g===0)return o.removeClass(t,i),!1;var nt=r||h&&d>0,tt=g>0&&f.animationDelay>0&&f.animationDuration===0,it=b.closeAnimationFns||[];return t.data(y,{stagger:f,cacheKey:u,running:b.running||0,itemIndex:a,blockTransition:nt,closeAnimationFns:it}),s=c(t),nt&&(yt(s,!0),r&&t.css(r)),tt&&gt(s,!0),!0}function hi(n,t,i,r,u){function ct(){var n,r;t.off(ft,at);o.removeClass(t,l);o.removeClass(t,b);d&&e.cancel(d);et(t,i);n=c(t);for(r in v)n.style.removeProperty(v[r])}function at(n){n.stopPropagation();var t=n.originalEvent||n,i=t.$manualTimeStamp||t.timeStamp||Date.now(),u=parseFloat(t.elapsedTime.toFixed(ri));Math.max(i-wt,0)>=ut&&u>=it&&r()}var a=c(t),h=t.data(y),l,b,nt,tt,rt,ut,k;if(a.getAttribute("class").indexOf(i)==-1||!h){r();return}l="";b="";f(i.split(" "),function(n,t){var i=(t>0?" ":"")+n;l+=i+"-active";b+=i+"-pending"});var v=[],g=h.itemIndex,p=h.stagger,w=0;g>0&&(nt=0,p.transitionDelay>0&&p.transitionDuration===0&&(nt=p.transitionDelay*g),tt=0,p.animationDelay>0&&p.animationDuration===0&&(tt=p.animationDelay*g,v.push(ot+"animation-play-state")),w=Math.round(Math.max(nt,tt)*100)/100);w||(o.addClass(t,l),h.blockTransition&&yt(a,!1));var pt=h.cacheKey+" "+l,s=vt(t,pt),it=Math.max(s.transitionDuration,s.animationDuration);if(it===0){o.removeClass(t,l);et(t,i);r();return}!w&&u&&Object.keys(u).length>0&&(s.transitionDuration||(t.css("transition",s.animationDuration+"s linear all"),v.push("transition")),t.css(u));rt=Math.max(s.transitionDelay,s.animationDelay);ut=rt*lt;v.length>0&&(k=a.getAttribute("style")||"",k.charAt(k.length-1)!==";"&&(k+=";"),a.setAttribute("style",k+" "));var wt=Date.now(),ft=ht+" "+st,bt=(rt+it)*ui,kt=(w+bt)*lt,d;w>0&&(o.addClass(t,b),d=e(function(){d=null;s.transitionDuration>0&&yt(a,!1);s.animationDuration>0&&gt(a,!1);o.addClass(t,l);o.removeClass(t,b);u&&(s.transitionDuration===0&&t.css("transition",s.animationDuration+"s linear all"),t.css(u),v.push("transition"))},w*lt,!1));t.on(ft,at);return h.closeAnimationFns.push(function(){ct();r()}),h.running++,fi(t,kt),ct}function yt(n,t){n.style[d+ni]=t?"none":""}function gt(n,t){n.style[v+ii]=t?"paused":""}function rt(n,t,i,r){if(si(n,t,i,r))return function(n){n&&et(t,i)}}function ut(n,t,i,r,u){if(t.data(y))return hi(n,t,i,r,u);et(t,i);r()}function ft(n,t,i,r,u){var e=rt(n,t,i,u.from),f;if(!e){nt();r();return}return f=e,tt(t,function(){f=ut(n,t,i,r,u.to)}),function(n){(f||s)(n)}}function et(n,t){o.removeClass(n,t);var i=n.data(y);i&&(i.running&&i.running--,i.running&&i.running!==0||n.removeData(y))}function l(n,t){var i="";return n=a(n)?n:n.split(/\s+/),f(n,function(n,r){n&&n.length>0&&(i+=(r>0?" ":"")+n+t)}),i}var ot="",d,st,v,ht;n.ontransitionend===i&&n.onwebkittransitionend!==i?(ot="-webkit-",d="WebkitTransition",st="webkitTransitionEnd transitionend"):(d="transition",st="transitionend");n.onanimationend===i&&n.onwebkitanimationend!==i?(ot="-webkit-",v="WebkitAnimation",ht="webkitAnimationEnd animationend"):(v="animation",ht="animationend");var pt="Duration",ni="Property",ct="Delay",ti="IterationCount",ii="PlayState",wt="$$ngAnimateKey",y="$$ngAnimateCSS3Data",ri=3,ui=1.5,lt=1e3,p={},bt=0,g=[],w;var kt=null,dt=0,at=[];return{animate:function(n,t,i,r,u,f){return f=f||{},f.from=i,f.to=r,ft("animate",n,t,u,f)},enter:function(n,t,i){return i=i||{},ft("enter",n,"ng-enter",t,i)},leave:function(n,t,i){return i=i||{},ft("leave",n,"ng-leave",t,i)},move:function(n,t,i){return i=i||{},ft("move",n,"ng-move",t,i)},beforeSetClass:function(n,t,i,r,u){u=u||{};var e=l(i,"-remove")+" "+l(t,"-add"),f=rt("setClass",n,e,u.from);if(f)return tt(n,r),f;nt();r()},beforeAddClass:function(n,t,i,r){r=r||{};var u=rt("addClass",n,l(t,"-add"),r.from);if(u)return tt(n,i),u;nt();i()},beforeRemoveClass:function(n,t,i,r){r=r||{};var u=rt("removeClass",n,l(t,"-remove"),r.from);if(u)return tt(n,i),u;nt();i()},setClass:function(n,t,i,r,u){u=u||{};i=l(i,"-remove");t=l(t,"-add");var f=i+" "+t;return ut("setClass",n,f,r,u.to)},addClass:function(n,t,i,r){return r=r||{},ut("addClass",n,l(t,"-add"),i,r.to)},removeClass:function(n,t,i,r){return r=r||{},ut("removeClass",n,l(t,"-remove"),i,r.to)}}}])}])})(window,window.angular);
/*
//# sourceMappingURL=angular-animate.min.js.map
*/
///#source 1 1 /appnew/bower_components/angular-cookies/angular-cookies.min.js
(function(n,t,i){"use strict";t.module("ngCookies",["ng"]).factory("$cookies",["$rootScope","$browser",function(n,r){function c(){var n,e,o,s;for(n in f)h(u[n])&&r.cookies(n,i);for(n in u)e=u[n],t.isString(e)||(e=""+e,u[n]=e),e!==f[n]&&(r.cookies(n,e),s=!0);if(s){s=!1;o=r.cookies();for(n in u)u[n]!==o[n]&&(h(o[n])?delete u[n]:u[n]=o[n],s=!0)}}var u={},f={},e,o=!1,s=t.copy,h=t.isUndefined;return r.addPollFn(function(){var t=r.cookies();e!=t&&(e=t,s(t,f),s(t,u),o&&n.$apply())})(),o=!0,n.$watch(c),u}]).factory("$cookieStore",["$cookies",function(n){return{get:function(i){var r=n[i];return r?t.fromJson(r):r},put:function(i,r){n[i]=t.toJson(r)},remove:function(t){delete n[t]}}}])})(window,window.angular);
/*
//# sourceMappingURL=angular-cookies.min.js.map
*/
///#source 1 1 /appnew/bower_components/angular-resource/angular-resource.min.js
(function(n,t,i){"use strict";function e(n){return n!=null&&n!==""&&n!=="hasOwnProperty"&&f.test("."+n)}function o(n,t){var f,u,o,s;if(!e(t))throw r("badmember",'Dotted member path "@{0}" is invalid.',t);for(f=t.split("."),u=0,o=f.length;u<o&&n!==i;u++)s=f[u],n=n!==null?n[s]:i;return n}function u(n,i){i=i||{};t.forEach(i,function(n,t){delete i[t]});for(var r in n)!n.hasOwnProperty(r)||r.charAt(0)==="$"&&r.charAt(1)==="$"||(i[r]=n[r]);return i}var r=t.$$minErr("$resource"),f=/^(\.[a-zA-Z_$][0-9a-zA-Z_$]*)+$/;t.module("ngResource",["ng"]).provider("$resource",function(){var n=this;this.defaults={stripTrailingSlashes:!0,actions:{get:{method:"GET"},save:{method:"POST"},query:{method:"GET",isArray:!0},remove:{method:"DELETE"},"delete":{method:"DELETE"}}};this.$get=["$http","$q",function(f,e){function p(n){return w(n,!0).replace(/%26/gi,"&").replace(/%3D/gi,"=").replace(/%2B/gi,"+")}function w(n,t){return encodeURIComponent(n).replace(/%40/gi,"@").replace(/%3A/gi,":").replace(/%24/g,"$").replace(/%2C/gi,",").replace(/%20/g,t?"%20":"+")}function a(t,i){this.template=t;this.defaults=h({},n.defaults,i);this.urlParams={}}function v(p,w,b,k){function nt(n,t){var i={};return t=h({},w,t),s(t,function(t,r){c(t)&&(t=t());i[r]=t&&t.charAt&&t.charAt(0)=="@"?o(n,t.substr(1)):t}),i}function tt(n){return n.resource}function d(n){u(n||{},this)}var g=new a(p,k);return b=h({},n.defaults.actions,b),d.prototype.toJSON=function(){var n=h({},this);return delete n.$promise,delete n.$resolved,n},s(b,function(n,o){var a=/^(POST|PUT|PATCH)$/i.test(n.method);d[o]=function(v,p,w,b){var ot={},it,rt,ft,ut;switch(arguments.length){case 4:ft=b;rt=w;case 3:case 2:if(c(p)){if(c(v)){rt=v;ft=p;break}rt=p;ft=w}else{ot=v;it=p;rt=w;break}case 1:c(v)?rt=v:a?it=v:ot=v;break;case 0:break;default:throw r("badargs","Expected up to 4 arguments [params, data, success, error], got {0} arguments",arguments.length);}var st=this instanceof d,k=st?it:n.isArray?[]:new d(it),et={},ht=n.interceptor&&n.interceptor.response||tt,ct=n.interceptor&&n.interceptor.responseError||i;return(s(n,function(n,t){t!="params"&&t!="isArray"&&t!="interceptor"&&(et[t]=y(n))}),a&&(et.data=it),g.setUrlParams(et,h({},nt(it,n.params||{}),ot),n.url),ut=f(et).then(function(i){var f=i.data,e=k.$promise;if(f){if(t.isArray(f)!==!!n.isArray)throw r("badcfg","Error in resource configuration for action `{0}`. Expected response to contain an {1} but got an {2}",o,n.isArray?"array":"object",t.isArray(f)?"array":"object");n.isArray?(k.length=0,s(f,function(n){typeof n=="object"?k.push(new d(n)):k.push(n)})):(u(f,k),k.$promise=e)}return k.$resolved=!0,i.resource=k,i},function(n){return k.$resolved=!0,(ft||l)(n),e.reject(n)}),ut=ut.then(function(n){var t=ht(n);return(rt||l)(t,n.headers),t},ct),!st)?(k.$promise=ut,k.$resolved=!1,k):ut};d.prototype["$"+o]=function(n,t,i){c(n)&&(i=t,t=n,n={});var r=d[o].call(this,n,this,t,i);return r.$promise||r}}),d.bind=function(n){return v(p,h({},w,n),b)},d}var l=t.noop,s=t.forEach,h=t.extend,y=t.copy,c=t.isFunction;return a.prototype={setUrlParams:function(n,i,u){var e=this,f=u||e.template,o,h,c=e.urlParams={};s(f.split(/\W/),function(n){if(n==="hasOwnProperty")throw r("badname","hasOwnProperty is not a valid parameter name.");!new RegExp("^\\d+$").test(n)&&n&&new RegExp("(^|[^\\\\]):"+n+"(\\W|$)").test(f)&&(c[n]=!0)});f=f.replace(/\\:/g,":");i=i||{};s(e.urlParams,function(n,r){o=i.hasOwnProperty(r)?i[r]:e.defaults[r];t.isDefined(o)&&o!==null?(h=p(o),f=f.replace(new RegExp(":"+r+"(\\W|$)","g"),function(n,t){return h+t})):f=f.replace(new RegExp("(/?):"+r+"(\\W|$)","g"),function(n,t,i){return i.charAt(0)=="/"?i:t+i})});e.defaults.stripTrailingSlashes&&(f=f.replace(/\/+$/,"")||"/");f=f.replace(/\/\.(?=\w+($|\?))/,".");n.url=f.replace(/\/\\\./,"/.");s(i,function(t,i){e.urlParams[i]||(n.params=n.params||{},n.params[i]=t)})}},v}]})})(window,window.angular);
/*
//# sourceMappingURL=angular-resource.min.js.map
*/
///#source 1 1 /appnew/bower_components/angular-sanitize/angular-sanitize.min.js
(function(n,t){"use strict";function g(){this.$get=["$$sanitizeUri",function(n){return function(t){var i=[];return at(t,k(i,function(t,i){return!/^unsafe/.test(n(t,i))})),i.join("")}}]}function nt(n){var i=[],r=k(i,t.noop);return r.chars(n),i.join("")}function i(n){for(var i={},r=n.split(","),t=0;t<r.length;t++)i[r[t]]=!0;return i}function at(n,i){function g(n,f,e,o){if(f=t.lowercase(f),a[f])while(r.last()&&v[r.last()])w("",r.last());l[f]&&r.last()==f&&w("",f);o=s[f]||!!o;o||r.push(f);var h={};e.replace(tt,function(n,t,i,r,f){var e=i||r||f||"";h[t]=u(e)});i.start&&i.start(f,h,o)}function w(n,u){var f=0,e;if(u=t.lowercase(u),u)for(f=r.length-1;f>=0;f--)if(r[f]==u)break;if(f>=0){for(e=r.length-1;e>=f;e--)i.end&&i.end(r[e]);r.length=f}}typeof n!="string"&&(n=n===null||typeof n=="undefined"?"":""+n);var c,p,h,r=[],k=n,b;for(r.last=function(){return r[r.length-1]};n;){if(b="",p=!0,r.last()&&y[r.last()]?(n=n.replace(new RegExp("(.*)<\\s*\\/\\s*"+r.last()+"[^>]*>","i"),function(n,t){return t=t.replace(ut,"$1").replace(ft,"$1"),i.chars&&i.chars(u(t)),""}),w("",r.last())):(n.indexOf("<!--")===0?(c=n.indexOf("--",4),c>=0&&n.lastIndexOf("-->",c)===c&&(i.comment&&i.comment(n.substring(4,c)),n=n.substring(c+3),p=!1)):o.test(n)?(h=n.match(o),h&&(n=n.replace(h[0],""),p=!1)):rt.test(n)?(h=n.match(e),h&&(n=n.substring(h[0].length),h[0].replace(e,w),p=!1)):it.test(n)&&(h=n.match(f),h?(h[4]&&(n=n.substring(h[0].length),h[0].replace(f,g)),p=!1):(b+="<",n=n.substring(1))),p&&(c=n.indexOf("<"),b+=c<0?n:n.substring(0,c),n=c<0?"":n.substring(c),i.chars&&i.chars(u(b)))),n==k)throw d("badparse","The sanitizer was unable to parse the following block of html: {0}",n);k=n}w()}function u(n){if(!n)return"";var i=vt.exec(n),u=i[1],f=i[3],t=i[2];return t&&(r.innerHTML=t.replace(/</g,"&lt;"),t="textContent"in r?r.textContent:r.innerText),u+t+f}function b(n){return n.replace(/&/g,"&amp;").replace(et,function(n){var t=n.charCodeAt(0),i=n.charCodeAt(1);return"&#"+((t-55296)*1024+(i-56320)+65536)+";"}).replace(ot,function(n){return"&#"+n.charCodeAt(0)+";"}).replace(/</g,"&lt;").replace(/>/g,"&gt;")}function k(n,i){var u=!1,r=t.bind(n,n.push);return{start:function(n,f,e){n=t.lowercase(n);!u&&y[n]&&(u=n);u||p[n]!==!0||(r("<"),r(n),t.forEach(f,function(u,f){var e=t.lowercase(f),o=n==="img"&&e==="src"||e==="background";lt[e]===!0&&(w[e]!==!0||i(u,o))&&(r(" "),r(f),r('="'),r(b(u)),r('"'))}),r(e?"/>":">"))},end:function(n){n=t.lowercase(n);u||p[n]!==!0||(r("<\/"),r(n),r(">"));n==u&&(u=!1)},chars:function(n){u||r(b(n))}}}var d=t.$$minErr("$sanitize"),f=/^<((?:[a-zA-Z])[\w:-]*)((?:\s+[\w:-]+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)\s*(>?)/,e=/^<\/\s*([\w:-]+)[^>]*>/,tt=/([\w:-]+)(?:\s*=\s*(?:(?:"((?:[^"])*)")|(?:'((?:[^'])*)')|([^>\s]+)))?/g,it=/^</,rt=/^<\//,ut=/<!--(.*?)-->/g,o=/<!DOCTYPE([^>]*?)>/i,ft=/<!\[CDATA\[(.*?)]]>/g,et=/[\uD800-\uDBFF][\uDC00-\uDFFF]/g,ot=/([^\#-~| |!])/g,s=i("area,br,col,hr,img,wbr"),h=i("colgroup,dd,dt,li,p,tbody,td,tfoot,th,thead,tr"),c=i("rp,rt"),l=t.extend({},c,h),a=t.extend({},h,i("address,article,aside,blockquote,caption,center,del,dir,div,dl,figure,figcaption,footer,h1,h2,h3,h4,h5,h6,header,hgroup,hr,ins,map,menu,nav,ol,pre,script,section,table,ul")),v=t.extend({},c,i("a,abbr,acronym,b,bdi,bdo,big,br,cite,code,del,dfn,em,font,i,img,ins,kbd,label,map,mark,q,ruby,rp,rt,s,samp,small,span,strike,strong,sub,sup,time,tt,u,var")),st=i("animate,animateColor,animateMotion,animateTransform,circle,defs,desc,ellipse,font-face,font-face-name,font-face-src,g,glyph,hkern,image,linearGradient,line,marker,metadata,missing-glyph,mpath,path,polygon,polyline,radialGradient,rect,set,stop,svg,switch,text,title,tspan,use"),y=i("script,style"),p=t.extend({},s,a,v,l,st),w=i("background,cite,href,longdesc,src,usemap,xlink:href"),ht=i("abbr,align,alt,axis,bgcolor,border,cellpadding,cellspacing,class,clear,color,cols,colspan,compact,coords,dir,face,headers,height,hreflang,hspace,ismap,lang,language,nohref,nowrap,rel,rev,rows,rowspan,rules,scope,scrolling,shape,size,span,start,summary,target,title,type,valign,value,vspace,width"),ct=i("accent-height,accumulate,additive,alphabetic,arabic-form,ascent,attributeName,attributeType,baseProfile,bbox,begin,by,calcMode,cap-height,class,color,color-rendering,content,cx,cy,d,dx,dy,descent,display,dur,end,fill,fill-rule,font-family,font-size,font-stretch,font-style,font-variant,font-weight,from,fx,fy,g1,g2,glyph-name,gradientUnits,hanging,height,horiz-adv-x,horiz-origin-x,ideographic,k,keyPoints,keySplines,keyTimes,lang,marker-end,marker-mid,marker-start,markerHeight,markerUnits,markerWidth,mathematical,max,min,offset,opacity,orient,origin,overline-position,overline-thickness,panose-1,path,pathLength,points,preserveAspectRatio,r,refX,refY,repeatCount,repeatDur,requiredExtensions,requiredFeatures,restart,rotate,rx,ry,slope,stemh,stemv,stop-color,stop-opacity,strikethrough-position,strikethrough-thickness,stroke,stroke-dasharray,stroke-dashoffset,stroke-linecap,stroke-linejoin,stroke-miterlimit,stroke-opacity,stroke-width,systemLanguage,target,text-anchor,to,transform,type,u1,u2,underline-position,underline-thickness,unicode,unicode-range,units-per-em,values,version,viewBox,visibility,width,widths,x,x-height,x1,x2,xlink:actuate,xlink:arcrole,xlink:role,xlink:show,xlink:title,xlink:type,xml:base,xml:lang,xml:space,xmlns,xmlns:xlink,y,y1,y2,zoomAndPan"),lt=t.extend({},w,ct,ht),r=document.createElement("pre"),vt=/^(\s*)([\s\S]*?)(\s*)$/;t.module("ngSanitize",[]).provider("$sanitize",g);t.module("ngSanitize").filter("linky",["$sanitize",function(n){var i=/((ftp|https?):\/\/|(www\.)|(mailto:)?[A-Za-z0-9._%+-]+@)\S*[^\s.;,(){}<>"”’]/,r=/^mailto:/;return function(u,f){function l(n){n&&o.push(nt(n))}function a(n,i){o.push("<a ");t.isDefined(f)&&o.push('target="',f,'" ');o.push('href="',n.replace(/"/g,"&quot;"),'">');l(i);o.push("<\/a>")}if(!u)return u;for(var e,s=u,o=[],h,c;e=s.match(i);)h=e[0],e[2]||e[4]||(h=(e[3]?"http://":"mailto:")+h),c=e.index,l(s.substr(0,c)),a(h,e[0].replace(r,"")),s=s.substring(c+e[0].length);return l(s),n(o.join(""))}}])})(window,window.angular);
/*
//# sourceMappingURL=angular-sanitize.min.js.map
*/
///#source 1 1 /assets/js/ng-grid/ng-grid.min.js
(function(n,t){"use strict";var r=6,p=4,f="asc",w="desc",e="_ng_field_",o="_ng_depth_",u="_ng_hidden_",s="_ng_column_",l=/CUSTOM_FILTERS/g,b=/COL_FIELD/g,tt=/DISPLAY_CELL_TEMPLATE/g,it=/EDITABLE_CELL_TEMPLATE/g,h=/<.+>/,d;n.ngGrid={};n.ngGrid.i18n={};var a=angular.module("ngGrid.services",[]),i=angular.module("ngGrid.directives",[]),k=angular.module("ngGrid.filters",[]);angular.module("ngGrid",["ngGrid.services","ngGrid.directives","ngGrid.filters"]);d=function(n,t,i,u){var s,v,h,c;if(n.selectionProvider.selectedItems===undefined)return!0;var f=i.which||i.keyCode,e,y=!1,p=!1,l=n.selectionProvider.lastClickedRow.rowIndex,o=n.columns.filter(function(n){return n.visible}),a=n.columns.filter(function(n){return n.pinned});if(n.col&&(e=o.indexOf(n.col)),f!=37&&f!=38&&f!=39&&f!=40&&f!=9&&f!=13)return!0;if(n.enableCellSelection){f==9&&i.preventDefault();var w=n.showSelectionCheckbox?n.col.index==1:n.col.index==0,k=n.$index==1||n.$index==0,d=n.$index==n.renderedColumns.length-1||n.$index==n.renderedColumns.length-2,b=o.indexOf(n.col)==o.length-1,g=a.indexOf(n.col)==a.length-1;f==37||f==9&&i.shiftKey?(s=0,w||(e-=1),k?w&&f==9&&i.shiftKey?(s=u.$canvas.width(),e=o.length-1,p=!0):s=u.$viewport.scrollLeft()-n.col.width:a.length>0&&(s=u.$viewport.scrollLeft()-o[e].width),u.$viewport.scrollLeft(s)):f!=39&&(f!=9||i.shiftKey)||(d?b&&f==9&&!i.shiftKey?(u.$viewport.scrollLeft(0),e=n.showSelectionCheckbox?1:0,y=!0):u.$viewport.scrollLeft(u.$viewport.scrollLeft()+n.col.width):g&&u.$viewport.scrollLeft(0),b||(e+=1))}return v=n.configGroups.length>0?u.rowFactory.parsedData.filter(function(n){return!n.isAggRow}):u.filteredRows,h=0,l!=0&&(f==38||f==13&&i.shiftKey||f==9&&i.shiftKey&&p)?h=-1:l!=v.length-1&&(f==40||f==13&&!i.shiftKey||f==9&&y)&&(h=1),h&&(c=v[l+h],c.beforeSelectionChange(c,i)&&(c.continueSelection(i),n.$emit("ngGridEventDigestGridParent"),n.selectionProvider.lastClickedRow.renderedRowIndex>=n.renderedRows.length-r-2?u.$viewport.scrollTop(u.$viewport.scrollTop()+n.rowHeight):n.selectionProvider.lastClickedRow.renderedRowIndex<=r+2&&u.$viewport.scrollTop(u.$viewport.scrollTop()-n.rowHeight))),n.enableCellSelection&&setTimeout(function(){n.domAccessProvider.focusCellElement(n,n.renderedColumns.indexOf(o[e]))},3),!1};String.prototype.trim||(String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g,"")});Array.prototype.indexOf||(Array.prototype.indexOf=function(n){var i=this.length>>>0,t=Number(arguments[1])||0;for(t=t<0?Math.ceil(t):Math.floor(t),t<0&&(t+=i);t<i;t++)if(t in this&&this[t]===n)return t;return-1});Array.prototype.filter||(Array.prototype.filter=function(n){var i=Object(this),e=i.length>>>0,r,f,t,u;if(typeof n!="function")throw new TypeError;for(r=[],f=arguments[1],t=0;t<e;t++)t in i&&(u=i[t],n.call(f,u,t,i)&&r.push(u));return r});k.filter("checkmark",function(){return function(n){return n?"✔":"✘"}});k.filter("ngColumns",function(){return function(n){return n.filter(function(n){return!n.isAggCol})}});a.factory("$domUtilityService",["$utilityService",function(n){var i={},r={},u=function(){var n=t("<div><\/div>");n.appendTo("body");n.height(100).width(100).css("position","absolute").css("overflow","scroll");n.append('<div style="height: 400px; width: 400px;"><\/div>');i.ScrollH=n.height()-n[0].clientHeight;i.ScrollW=n.width()-n[0].clientWidth;n.empty();n.attr("style","");n.append('<span style="font-family: Verdana, Helvetica, Sans-Serif; font-size: 14px;"><strong>M<\/strong><\/span>');i.LetterW=n.children().first().width();n.remove()};return i.eventStorage={},i.AssignGridContainers=function(n,r,u){u.$root=t(r);u.$topPanel=u.$root.find(".ngTopPanel");u.$groupPanel=u.$root.find(".ngGroupPanel");u.$headerContainer=u.$topPanel.find(".ngHeaderContainer");n.$headerContainer=u.$headerContainer;u.$headerScroller=u.$topPanel.find(".ngHeaderScroller");u.$headers=u.$headerScroller.children();u.$viewport=u.$root.find(".ngViewport");u.$canvas=u.$viewport.find(".ngCanvas");u.$footerPanel=u.$root.find(".ngFooterPanel");n.$watch(function(){return u.$viewport.scrollLeft()},function(n){return u.$headerContainer.scrollLeft(n)});i.UpdateGridLayout(n,u)},i.getRealWidth=function(n){var i=0,r=n.parents().andSelf().not(":visible");return t.swap(r[0],{visibility:"hidden",display:"block"},function(){i=n.outerWidth()}),i},i.UpdateGridLayout=function(n,t){var r=t.$viewport.scrollTop();t.elementDims.rootMaxW=t.$root.width();t.$root.is(":hidden")&&(t.elementDims.rootMaxW=i.getRealWidth(t.$root));t.elementDims.rootMaxH=t.$root.height();t.refreshDomSizes();n.adjustScrollTop(r,!0)},i.numberOfGrids=0,i.BuildStyles=function(r,u,f){var p=u.config.rowHeight,e=u.$styleSheet,o=u.gridId,l,v=r.columns,a=0,c,s,h,y;for(e||(e=t("#"+o),e[0]||(e=t("<style id='"+o+"' type='text/css' rel='stylesheet' />").appendTo(u.$root))),e.empty(),c=r.totalRowWidth(),l="."+o+" .ngCanvas { width: "+c+"px; }."+o+" .ngRow { width: "+c+"px; }."+o+" .ngCanvas { width: "+c+"px; }."+o+" .ngHeaderScroller { width: "+(c+i.ScrollH)+"px}",s=0;s<v.length;s++)h=v[s],h.visible!==!1&&(y=h.pinned?u.$viewport.scrollLeft()+a:a,l+="."+o+" .col"+s+" { width: "+h.width+"px; left: "+y+"px; height: "+p+"px }."+o+" .colt"+s+" { width: "+h.width+"px; }",a+=h.width);n.isIe?e[0].styleSheet.cssText=l:e[0].appendChild(document.createTextNode(l));u.$styleSheet=e;f&&(r.adjustScrollLeft(u.$viewport.scrollLeft()),i.digest(r))},i.setColLeft=function(t,i,u){var f,o,e;u.$styleSheet&&(f=r[t.index],f||(f=r[t.index]=new RegExp(".col"+t.index+" { width: [0-9]+px; left: [0-9]+px")),o=u.$styleSheet.html(),e=o.replace(f,".col"+t.index+" { width: "+t.width+"px; left: "+i+"px"),n.isIe?setTimeout(function(){u.$styleSheet.html(e)}):u.$styleSheet.html(e))},i.setColLeft.immediate=1,i.RebuildGrid=function(n,t){i.UpdateGridLayout(n,t);t.config.maintainColumnRatios&&t.configureColumnWidths();n.adjustScrollLeft(t.$viewport.scrollLeft());i.BuildStyles(n,t,!0)},i.digest=function(n){n.$root.$$phase||n.$digest()},i.ScrollH=17,i.ScrollW=17,i.LetterW=10,u(),i}]);a.factory("$sortService",["$parse",function(n){var t={};return t.colSortFnCache={},t.guessSortFn=function(n){var i=typeof n;switch(i){case"number":return t.sortNumber;case"boolean":return t.sortBool;case"string":return n.match(/^-?[£$¤]?[\d,.]+%?$/)?t.sortNumberStr:t.sortAlpha;default:return Object.prototype.toString.call(n)==="[object Date]"?t.sortDate:t.basicSort}},t.basicSort=function(n,t){return n==t?0:n<t?-1:1},t.sortNumber=function(n,t){return n-t},t.sortNumberStr=function(n,t){var i,r,u=!1,f=!1;return(i=parseFloat(n.replace(/[^0-9.-]/g,"")),isNaN(i)&&(u=!0),r=parseFloat(t.replace(/[^0-9.-]/g,"")),isNaN(r)&&(f=!0),u&&f)?0:u?1:f?-1:i-r},t.sortAlpha=function(n,t){var i=n.toLowerCase(),r=t.toLowerCase();return i==r?0:i<r?-1:1},t.sortDate=function(n,t){var i=n.getTime(),r=t.getTime();return i==r?0:i<r?-1:1},t.sortBool=function(n,t){return n&&t?0:n||t?n?1:-1:0},t.sortData=function(i,r){if(r&&i){var s=i.fields.length,u=i.fields,e,o,h=r.slice(0);r.sort(function(r,c){for(var l=0,a=0,p,v,y;l==0&&a<s;)e=i.columns[a],o=i.directions[a],p=t.getSortFn(e,h),v=n(u[a])(r),y=n(u[a])(c),(v||v==0)&&(y||y==0)?l=p(v,y):y||v?v?y||(l=-1):l=1:l=0,a++;return o===f?l:0-l})}},t.Sort=function(n,i){t.isSorting||(t.isSorting=!0,t.sortData(n,i),t.isSorting=!1)},t.getSortFn=function(i,r){var u=undefined,f;if(t.colSortFnCache[i.field])u=t.colSortFnCache[i.field];else if(i.sortingAlgorithm!=undefined)u=i.sortingAlgorithm,t.colSortFnCache[i.field]=i.sortingAlgorithm;else{if(f=r[0],!f)return u;u=t.guessSortFn(n(i.field)(f));u?t.colSortFnCache[i.field]=u:u=t.sortAlpha}return u},t}]);a.factory("$utilityService",["$parse",function(i){var u=/function (.{1,})\(/,r={visualLength:function(n){var i=document.getElementById("testDataLength");return i||(i=document.createElement("SPAN"),i.id="testDataLength",i.style.visibility="hidden",document.body.appendChild(i)),t(i).css("font",t(n).css("font")),i.innerHTML=t(n).text(),i.offsetWidth},forIn:function(n,t){for(var i in n)n.hasOwnProperty(i)&&t(n[i],i)},evalProperty:function(n,t){return i(t)(n)},endsWith:function(n,t){return!n||!t||typeof n!="string"?!1:n.indexOf(t,n.length-t.length)!==-1},isNullOrUndefined:function(n){return n===undefined||n===null?!0:!1},getElementsByClassName:function(n){for(var u,r=[],f=new RegExp("\\b"+n+"\\b"),i=document.getElementsByTagName("*"),t=0;t<i.length;t++)u=i[t].className,f.test(u)&&r.push(i[t]);return r},newId:function(){var n=(new Date).getTime();return function(){return n+=1}}(),seti18n:function(t,i){var r=n.ngGrid.i18n[i];for(var u in r)t.i18n[u]=r[u]},getInstanceType:function(n){var t=u.exec(n.constructor.toString());return t&&t.length>1?t[1]:""},ieVersion:function(){for(var n=3,t=document.createElement("div"),i=t.getElementsByTagName("i");t.innerHTML="<!--[if gt IE "+ ++n+"]><i><\/i><![endif]-->",i[0];);return n>4?n:undefined}()};return t.extend(r,{isIe:function(){return r.ieVersion!==undefined}()}),r}]);var g=function(n,t,i,r){var f=this;f.rowIndex=0;f.offsetTop=f.rowIndex*i;f.entity=n;f.label=n.gLabel;f.field=n.gField;f.depth=n.gDepth;f.parent=n.parent;f.children=n.children;f.aggChildren=n.aggChildren;f.aggIndex=n.aggIndex;f.collapsed=r;f.isAggRow=!0;f.offsetLeft=n.gDepth*25;f.aggLabelFilter=n.aggLabelFilter;f.toggleExpand=function(){f.collapsed=f.collapsed?!1:!0;f.orig&&(f.orig.collapsed=f.collapsed);f.notifyChildren()};f.setExpand=function(n){f.collapsed=n;f.notifyChildren()};f.notifyChildren=function(){for(var i,r,e=Math.max(t.aggCache.length,f.children.length),n=0;n<e;n++)f.aggChildren[n]&&(f.aggChildren[n].entity[u]=f.collapsed,f.collapsed&&f.aggChildren[n].setExpand(f.collapsed)),f.children[n]&&(f.children[n][u]=f.collapsed),n>f.aggIndex&&t.aggCache[n]&&(i=t.aggCache[n],r=30*f.children.length,i.offsetTop=f.collapsed?i.offsetTop-r:i.offsetTop+r);t.renderedChange()};f.aggClass=function(){return f.collapsed?"ngAggArrowCollapsed":"ngAggArrowExpanded"};f.totalChildren=function(){if(f.aggChildren.length>0){var n=0,t=function(i){i.aggChildren.length>0?angular.forEach(i.aggChildren,function(n){t(n)}):n+=i.children.length};return t(f),n}return f.children.length};f.copy=function(){var n=new g(f.entity,t,i,r);return n.orig=f,n}},c=function(n,i,r,u,e,o){var s=this,a=n.colDef,p=500,v=0,y=null;s.colDef=n.colDef;s.width=a.width;s.groupIndex=0;s.isGroupedBy=!1;s.minWidth=a.minWidth?a.minWidth:50;s.maxWidth=a.maxWidth?a.maxWidth:9e3;s.enableCellEdit=n.enableCellEdit||a.enableCellEdit;s.headerRowHeight=n.headerRowHeight;s.displayName=a.displayName||a.field;s.index=n.index;s.isAggCol=n.isAggCol;s.cellClass=a.cellClass;s.sortPriority=undefined;s.cellFilter=a.cellFilter?a.cellFilter:"";s.field=a.field;s.aggLabelFilter=a.cellFilter||a.aggLabelFilter;s.visible=o.isNullOrUndefined(a.visible)||a.visible;s.sortable=!1;s.resizable=!1;s.pinnable=!1;s.pinned=n.enablePinning&&a.pinned;s.originalIndex=s.index;s.groupable=o.isNullOrUndefined(a.groupable)||a.groupable;n.enableSort&&(s.sortable=o.isNullOrUndefined(a.sortable)||a.sortable);n.enableResize&&(s.resizable=o.isNullOrUndefined(a.resizable)||a.resizable);n.enablePinning&&(s.pinnable=o.isNullOrUndefined(a.pinnable)||a.pinnable);s.sortDirection=undefined;s.sortingAlgorithm=a.sortFn;s.headerClass=a.headerClass;s.cursor=s.sortable?"pointer":"default";s.headerCellTemplate=a.headerCellTemplate||e.get("headerCellTemplate.html");s.cellTemplate=a.cellTemplate||e.get("cellTemplate.html").replace(l,s.cellFilter?"|"+s.cellFilter:"");s.enableCellEdit&&(s.cellEditTemplate=e.get("cellEditTemplate.html"),s.editableCellTemplate=a.editableCellTemplate||e.get("editableCellTemplate.html"));a.cellTemplate&&!h.test(a.cellTemplate)&&(s.cellTemplate=t.ajax({type:"GET",url:a.cellTemplate,async:!1}).responseText);s.enableCellEdit&&a.editableCellTemplate&&!h.test(a.editableCellTemplate)&&(s.editableCellTemplate=t.ajax({type:"GET",url:a.editableCellTemplate,async:!1}).responseText);a.headerCellTemplate&&!h.test(a.headerCellTemplate)&&(s.headerCellTemplate=t.ajax({type:"GET",url:a.headerCellTemplate,async:!1}).responseText);s.colIndex=function(){var n=s.pinned?"pinned ":"";return n+="col"+s.index+" colt"+s.index,s.cellClass&&(n+=" "+s.cellClass),n};s.groupedByClass=function(){return s.isGroupedBy?"ngGroupedByIcon":"ngGroupIcon"};s.toggleVisible=function(){s.visible=!s.visible};s.showSortButtonUp=function(){return s.sortable?s.sortDirection===w:s.sortable};s.showSortButtonDown=function(){return s.sortable?s.sortDirection===f:s.sortable};s.noSortVisible=function(){return!s.sortDirection};s.sort=function(t){if(!s.sortable)return!0;var i=s.sortDirection===f?w:f;return s.sortDirection=i,n.sortCallback(s,t),!1};s.gripClick=function(){v++;v===1?y=setTimeout(function(){v=0},p):(clearTimeout(y),n.resizeOnDataCallback(s),v=0)};s.gripOnMouseDown=function(n){return n.ctrlKey&&!s.pinned?(s.toggleVisible(),u.BuildStyles(i,r),!0):(n.target.parentElement.style.cursor="col-resize",s.startMousePosition=n.clientX,s.origWidth=s.width,t(document).mousemove(s.onMouseMove),t(document).mouseup(s.gripOnMouseUp),!1)};s.onMouseMove=function(n){var f=n.clientX-s.startMousePosition,t=f+s.origWidth;return s.width=t<s.minWidth?s.minWidth:t>s.maxWidth?s.maxWidth:t,u.BuildStyles(i,r),!1};s.gripOnMouseUp=function(n){return t(document).off("mousemove",s.onMouseMove),t(document).off("mouseup",s.gripOnMouseUp),n.target.parentElement.style.cursor="default",i.adjustScrollLeft(0),u.digest(i),!1};s.copy=function(){var t=new c(n,i,r,u,e);return t.isClone=!0,t.orig=s,t};s.setVars=function(n){s.orig=n;s.width=n.width;s.groupIndex=n.groupIndex;s.isGroupedBy=n.isGroupedBy;s.displayName=n.displayName;s.index=n.index;s.isAggCol=n.isAggCol;s.cellClass=n.cellClass;s.cellFilter=n.cellFilter;s.field=n.field;s.aggLabelFilter=n.aggLabelFilter;s.visible=n.visible;s.sortable=n.sortable;s.resizable=n.resizable;s.pinnable=n.pinnable;s.pinned=n.pinned;s.originalIndex=n.originalIndex;s.sortDirection=n.sortDirection;s.sortingAlgorithm=n.sortingAlgorithm;s.headerClass=n.headerClass;s.headerCellTemplate=n.headerCellTemplate;s.cellTemplate=n.cellTemplate;s.cellEditTemplate=n.cellEditTemplate}},v=function(n){this.outerHeight=null;this.outerWidth=null;t.extend(this,n)},rt=function(n){var t=this,r,i;t.selectInputElement=function(n){var t=n.nodeName.toLowerCase();(t=="input"||t=="textarea")&&n.select()};t.focusCellElement=function(t,i){var e,o,f,u;t.selectionProvider.lastClickedRow&&(e=i!=undefined?i:r,o=t.selectionProvider.lastClickedRow.clone?t.selectionProvider.lastClickedRow.clone.elm:t.selectionProvider.lastClickedRow.elm,e!=undefined&&o&&(f=angular.element(o[0].children).filter(function(){return this.nodeType!=8}),u=Math.max(Math.min(t.renderedColumns.length-1,e),0),n.config.showSelectionCheckbox&&angular.element(f[u]).scope()&&angular.element(f[u]).scope().col.index==0&&(u=1),f[u]&&f[u].children[0].focus(),r=e))};i=function(n,t){n.css({"-webkit-touch-callout":t,"-webkit-user-select":t,"-khtml-user-select":t,"-moz-user-select":t=="none"?"-moz-none":t,"-ms-user-select":t,"user-select":t})};t.selectionHandlers=function(t,r){var u=!1;r.bind("keydown",function(f){if(f.keyCode==16)return i(r,"none",f),!0;if(!u){u=!0;var e=d(t,r,f,n);return u=!1,e}return!0});r.bind("keyup",function(n){return n.keyCode==16&&i(r,"text",n),!0})}},ut=function(i,r,u,f){var e=this;e.colToMove=undefined;e.groupToMove=undefined;e.assignEvents=function(){if(i.config.jqueryUIDraggable&&!i.config.enablePinning)i.$groupPanel.droppable({addClasses:!1,drop:function(n){e.onGroupDrop(n)}});else{i.$groupPanel.on("mousedown",e.onGroupMouseDown).on("dragover",e.dragOver).on("drop",e.onGroupDrop);i.$headerScroller.on("mousedown",e.onHeaderMouseDown).on("dragover",e.dragOver);if(i.config.enableColumnReordering&&!i.config.enablePinning)i.$headerScroller.on("drop",e.onHeaderDrop);if(i.config.enableRowReordering)i.$viewport.on("mousedown",e.onRowMouseDown).on("dragover",e.dragOver).on("drop",e.onRowDrop)}r.$watch("renderedColumns",function(){f(e.setDraggables)})};e.dragStart=function(n){n.dataTransfer.setData("text","")};e.dragOver=function(n){n.preventDefault()};e.setDraggables=function(){if(i.config.jqueryUIDraggable)i.$root.find(".ngHeaderSortColumn").draggable({helper:"clone",appendTo:"body",stack:"div",addClasses:!1,start:function(n){e.onHeaderMouseDown(n)}}).droppable({drop:function(n){e.onHeaderDrop(n)}});else{var n=i.$root.find(".ngHeaderSortColumn");angular.forEach(n,function(n){n.setAttribute("draggable","true");n.addEventListener&&n.addEventListener("dragstart",e.dragStart)});navigator.userAgent.indexOf("MSIE")!=-1&&i.$root.find(".ngHeaderSortColumn").bind("selectstart",function(){return this.dragDrop(),!1})}};e.onGroupMouseDown=function(n){var r=t(n.target),u;r[0].className!="ngRemoveGroup"?(u=angular.element(r).scope(),u&&(i.config.jqueryUIDraggable||(r.attr("draggable","true"),this.addEventListener&&this.addEventListener("dragstart",e.dragStart),navigator.userAgent.indexOf("MSIE")!=-1&&r.bind("selectstart",function(){return this.dragDrop(),!1})),e.groupToMove={header:r,groupName:u.group,index:u.$index})):e.groupToMove=undefined};e.onGroupDrop=function(n){n.stopPropagation();var u,f;e.groupToMove?(u=t(n.target).closest(".ngGroupElement"),u.context.className=="ngGroupPanel"?(r.configGroups.splice(e.groupToMove.index,1),r.configGroups.push(e.groupToMove.groupName)):(f=angular.element(u).scope(),f&&e.groupToMove.index!=f.$index&&(r.configGroups.splice(e.groupToMove.index,1),r.configGroups.splice(f.$index,0,e.groupToMove.groupName))),e.groupToMove=undefined,i.fixGroupIndexes()):e.colToMove&&(r.configGroups.indexOf(e.colToMove.col)==-1&&(u=t(n.target).closest(".ngGroupElement"),u.context.className=="ngGroupPanel"||u.context.className=="ngGroupPanelDescription ng-binding"?r.groupBy(e.colToMove.col):(f=angular.element(u).scope(),f&&r.removeGroup(f.$index))),e.colToMove=undefined);r.$$phase||r.$apply()};e.onHeaderMouseDown=function(n){var i=t(n.target).closest(".ngHeaderSortColumn"),r=angular.element(i).scope();r&&(e.colToMove={header:i,col:r.col})};e.onHeaderDrop=function(n){if(e.colToMove&&!e.colToMove.col.pinned){var o=t(n.target).closest(".ngHeaderSortColumn"),f=angular.element(o).scope();if(f){if(e.colToMove.col==f.col)return;r.columns.splice(e.colToMove.col.index,1);r.columns.splice(f.col.index,0,e.colToMove.col);i.fixColumnIndexes();u.BuildStyles(r,i,!0);e.colToMove=undefined}}};e.onRowMouseDown=function(n){var i=t(n.target).closest(".ngRow"),r=angular.element(i).scope();r&&(i.attr("draggable","true"),u.eventStorage.rowToMove={targetRow:i,scope:r})};e.onRowDrop=function(n){var e=t(n.target).closest(".ngRow"),r=angular.element(e).scope(),f;if(r){if(f=u.eventStorage.rowToMove,f.scope.row==r.row)return;i.changeRowOrder(f.scope.row,r.row);i.searchProvider.evalFilter();u.eventStorage.rowToMove=undefined;u.digest(r.$root)}};e.assignGridEventHandlers=function(){i.config.tabIndex===-1?(i.$viewport.attr("tabIndex",u.numberOfGrids),u.numberOfGrids++):i.$viewport.attr("tabIndex",i.config.tabIndex);t(n).resize(function(){u.RebuildGrid(r,i)});t(i.$root.parent()).on("resize",function(){u.RebuildGrid(r,i)})};e.assignGridEventHandlers();e.assignEvents()},ft=function(n,t){n.maxRows=function(){return Math.max(t.config.totalServerItems,t.data.length)};n.multiSelect=t.config.enableRowSelection&&t.config.multiSelect;n.selectedItemCount=t.selectedItemCount;n.maxPages=function(){return Math.ceil(n.maxRows()/n.pagingOptions.pageSize)};n.pageForward=function(){var i=n.pagingOptions.currentPage;t.config.totalServerItems>0?n.pagingOptions.currentPage=Math.min(i+1,n.maxPages()):n.pagingOptions.currentPage++};n.pageBackward=function(){var t=n.pagingOptions.currentPage;n.pagingOptions.currentPage=Math.max(t-1,1)};n.pageToFirst=function(){n.pagingOptions.currentPage=1};n.pageToLast=function(){var t=n.maxPages();n.pagingOptions.currentPage=t};n.cantPageForward=function(){var i=n.pagingOptions.currentPage,r=n.maxPages();return t.config.totalServerItems>0?!(i<r):t.data.length<1};n.cantPageToLast=function(){return t.config.totalServerItems>0?n.cantPageForward():!0};n.cantPageBackward=function(){var t=n.pagingOptions.currentPage;return!(t>1)}},et=function(i,f,e,o,s,l,a,w,b){var g={aggregateTemplate:undefined,afterSelectionChange:function(){},beforeSelectionChange:function(){return!0},checkboxCellTemplate:undefined,checkboxHeaderTemplate:undefined,columnDefs:undefined,data:[],dataUpdated:function(){},enableCellEdit:!1,enableCellSelection:!1,enableColumnResize:!1,enableColumnReordering:!1,enableColumnHeavyVirt:!1,enablePaging:!1,enablePinning:!1,enableRowReordering:!1,enableRowSelection:!0,enableSorting:!0,enableHighlighting:!1,excludeProperties:[],filterOptions:{filterText:"",useExternalFilter:!1},footerRowHeight:55,footerTemplate:undefined,groups:[],groupsCollapsedByDefault:!0,headerRowHeight:30,headerRowTemplate:undefined,jqueryUIDraggable:!1,jqueryUITheme:!1,keepLastSelected:!0,maintainColumnRatios:undefined,menuTemplate:undefined,multiSelect:!0,pagingOptions:{pageSizes:[250,500,1e3],pageSize:250,currentPage:1},pinSelectionCheckbox:!1,plugins:[],primaryKey:undefined,rowHeight:30,rowTemplate:undefined,selectedItems:[],selectWithCheckboxOnly:!1,showColumnMenu:!1,showFilter:!1,showFooter:!1,showGroupPanel:!1,showSelectionCheckbox:!1,sortInfo:{fields:[],columns:[],directions:[]},tabIndex:-1,totalServerItems:0,useExternalSorting:!1,i18n:"en",virtualizationThreshold:50},k=this,d;k.maxCanvasHt=0;k.config=t.extend(g,n.ngGrid.config,f);k.config.showSelectionCheckbox=k.config.showSelectionCheckbox&&k.config.enableColumnHeavyVirt===!1;k.config.enablePinning=k.config.enablePinning&&k.config.enableColumnHeavyVirt===!1;k.config.selectWithCheckboxOnly=k.config.selectWithCheckboxOnly&&k.config.showSelectionCheckbox!==!1;k.config.pinSelectionCheckbox=k.config.enablePinning;typeof f.columnDefs=="string"&&(k.config.columnDefs=i.$eval(f.columnDefs));k.rowCache=[];k.rowMap=[];k.gridId="ng"+a.newId();k.$root=null;k.$groupPanel=null;k.$topPanel=null;k.$headerContainer=null;k.$headerScroller=null;k.$headers=null;k.$viewport=null;k.$canvas=null;k.rootDim=k.config.gridDim;k.data=[];k.lateBindColumns=!1;k.filteredRows=[];d=function(n){var i=k.config[n],r=k.gridId+n+".html",u;i&&!h.test(i)?l.put(r,t.ajax({type:"GET",url:i,async:!1}).responseText):i?l.put(r,i):(u=n+".html",l.put(r,l.get(u)))};d("rowTemplate");d("aggregateTemplate");d("headerRowTemplate");d("checkboxCellTemplate");d("checkboxHeaderTemplate");d("menuTemplate");d("footerTemplate");typeof k.config.data=="object"&&(k.data=k.config.data);k.calcMaxCanvasHeight=function(){return k.config.groups.length>0?k.rowFactory.parsedData.filter(function(n){return!n[u]}).length*k.config.rowHeight:k.filteredRows.length*k.config.rowHeight};k.elementDims={scrollW:0,scrollH:0,rowIndexCellW:25,rowSelectedCellW:25,rootMaxW:0,rootMaxH:0};k.setRenderedRows=function(n){i.renderedRows.length=n.length;for(var t=0;t<n.length;t++)!i.renderedRows[t]||n[t].isAggRow||i.renderedRows[t].isAggRow?(i.renderedRows[t]=n[t].copy(),i.renderedRows[t].collapsed=n[t].collapsed,n[t].isAggRow||i.renderedRows[t].setVars(n[t])):i.renderedRows[t].setVars(n[t]),i.renderedRows[t].rowIndex=n[t].rowIndex,i.renderedRows[t].offsetTop=n[t].offsetTop,i.renderedRows[t].selected=n[t].selected,n[t].renderedRowIndex=t;k.refreshDomSizes();i.$emit("ngGridEventRows",n)};k.minRowsToRender=function(){var n=i.viewportDimHeight()||1;return Math.floor(n/k.config.rowHeight)};k.refreshDomSizes=function(){var n=new v;n.outerWidth=k.elementDims.rootMaxW;n.outerHeight=k.elementDims.rootMaxH;k.rootDim=n;k.maxCanvasHt=k.calcMaxCanvasHeight()};k.buildColumnDefsFromData=function(){k.config.columnDefs=[];var n=k.data[0];if(!n){k.lateBoundColumns=!0;return}a.forIn(n,function(n,t){k.config.excludeProperties.indexOf(t)==-1&&k.config.columnDefs.push({field:t})})};k.buildColumns=function(){var n=k.config.columnDefs,t=[],r;n||(k.buildColumnDefsFromData(),n=k.config.columnDefs);k.config.showSelectionCheckbox&&t.push(new c({colDef:{field:"✔",width:k.elementDims.rowSelectedCellW,sortable:!1,resizable:!1,groupable:!1,headerCellTemplate:l.get(i.gridId+"checkboxHeaderTemplate.html"),cellTemplate:l.get(i.gridId+"checkboxCellTemplate.html"),pinned:k.config.pinSelectionCheckbox},index:0,headerRowHeight:k.config.headerRowHeight,sortCallback:k.sortData,resizeOnDataCallback:k.resizeOnData,enableResize:k.config.enableColumnResize,enableSort:k.config.enableSorting,enablePinning:k.config.enablePinning},i,k,o,l,a));n.length>0&&(r=k.config.showSelectionCheckbox?k.config.groups.length+1:k.config.groups.length,i.configGroups.length=0,angular.forEach(n,function(n,u){u+=r;var f=new c({colDef:n,index:u,headerRowHeight:k.config.headerRowHeight,sortCallback:k.sortData,resizeOnDataCallback:k.resizeOnData,enableResize:k.config.enableColumnResize,enableSort:k.config.enableSorting,enablePinning:k.config.enablePinning,enableCellEdit:k.config.enableCellEdit},i,k,o,l,a),e=k.config.groups.indexOf(n.field);e!=-1&&(f.isGroupedBy=!0,i.configGroups.splice(e,0,f),f.groupIndex=i.configGroups.length);t.push(f)}),i.columns=t)};k.configureColumnWidths=function(){var u=k.config.columnDefs,f=k.config.showSelectionCheckbox?i.configGroups.length+1:i.configGroups.length,c=u.length+f,t=[],r=[],e=0,n=0,s,h;n+=k.config.showSelectionCheckbox?25:0;angular.forEach(u,function(u,o){var h,s,c;if(o+=f,h=!1,s=undefined,a.isNullOrUndefined(u.width)?u.width="*":(h=isNaN(u.width)?a.endsWith(u.width,"%"):!1,s=h?u.width:parseInt(u.width,10)),isNaN(s)){if(s=u.width,s=="auto"){i.columns[o].width=u.minWidth;n+=i.columns[o].width;c=i.columns[o];w(function(){k.resizeOnData(c,!0)});return}if(s.indexOf("*")!=-1){u.visible!==!1&&(e+=s.length);u.index=o;t.push(u);return}if(h){u.index=o;r.push(u);return}throw'unable to parse column width, use percentage ("10%","20%", etc...) or "*" to use remaining width of grid';}else u.visible!==!1&&(n+=i.columns[o].width=parseInt(u.width,10))});t.length>0&&(k.config.maintainColumnRatios===!1?angular.noop():k.config.maintainColumnRatios=!0,s=k.rootDim.outerWidth-n,h=Math.floor(s/e),angular.forEach(t,function(t){var u=t.width.length,r;i.columns[t.index].width=h*u;r=1;k.maxCanvasHt>i.viewportDimHeight()&&(r+=o.ScrollW);i.columns[t.index].width-=r;t.visible!==!1&&(n+=i.columns[t.index].width)}));r.length>0&&angular.forEach(r,function(n){var t=n.width;i.columns[n.index].width=Math.floor(k.rootDim.outerWidth*(parseInt(t.slice(0,-1),10)/100))})};k.init=function(){i.selectionProvider=new ht(k,i,b);i.domAccessProvider=new rt(k);k.rowFactory=new ot(k,i,o,l,a);k.searchProvider=new st(i,k,s);k.styleProvider=new ct(i,k);i.$watch("configGroups",function(n){var t=[];angular.forEach(n,function(n){t.push(n.field||n)});k.config.groups=t;k.rowFactory.filteredRowsChanged();i.$emit("ngGridEventGroups",n)},!0);i.$watch("columns",function(n){o.BuildStyles(i,k,!0);i.$emit("ngGridEventColumns",n)},!0);i.$watch(function(){return f.i18n},function(n){a.seti18n(i,n)});k.maxCanvasHt=k.calcMaxCanvasHeight();k.config.sortInfo.fields&&k.config.sortInfo.fields.length>0&&(k.getColsFromFields(),k.sortActual())};k.resizeOnData=function(n){var r=n.minWidth,u=a.getElementsByClassName("col"+n.index);angular.forEach(u,function(n,i){var u,f,e;i===0?(f=t(n).find(".ngHeaderText"),u=a.visualLength(f)+10):(e=t(n).find(".ngCellText"),u=a.visualLength(e)+10);u>r&&(r=u)});n.width=n.longest=Math.min(n.maxWidth,r+7);o.BuildStyles(i,k,!0)};k.lastSortedColumns=[];k.changeRowOrder=function(n,t){var r=k.rowCache.indexOf(n),u=k.rowCache.indexOf(t);k.rowCache.splice(r,1);k.rowCache.splice(u,0,n);i.$emit("ngGridEventChangeOrder",k.rowCache)};k.sortData=function(n,r){var u,e,f;r&&r.shiftKey&&k.config.sortInfo?(u=k.config.sortInfo.columns.indexOf(n),u===-1?(k.config.sortInfo.columns.length==1&&(k.config.sortInfo.columns[0].sortPriority=1),k.config.sortInfo.columns.push(n),n.sortPriority=k.config.sortInfo.columns.length,k.config.sortInfo.fields.push(n.field),k.config.sortInfo.directions.push(n.sortDirection),k.lastSortedColumns.push(n)):k.config.sortInfo.directions[u]=n.sortDirection):(e=t.isArray(n),k.config.sortInfo.columns.length=0,k.config.sortInfo.fields.length=0,k.config.sortInfo.directions.length=0,f=function(n){k.config.sortInfo.columns.push(n);k.config.sortInfo.fields.push(n.field);k.config.sortInfo.directions.push(n.sortDirection);k.lastSortedColumns.push(n)},e?(k.clearSortingData(),angular.forEach(n,function(n,t){n.sortPriority=t+1;f(n)})):(k.clearSortingData(n),n.sortPriority=undefined,f(n)));k.sortActual();k.searchProvider.evalFilter();i.$emit("ngGridEventSorted",k.config.sortInfo)};k.getColsFromFields=function(){k.config.sortInfo.columns?k.config.sortInfo.columns.length=0:k.config.sortInfo.columns=[];angular.forEach(i.columns,function(n){var t=k.config.sortInfo.fields.indexOf(n.field);return t!=-1&&(n.sortDirection=k.config.sortInfo.directions[t]||"asc",k.config.sortInfo.columns.push(n)),!1})};k.sortActual=function(){if(!k.config.useExternalSorting){var n=k.data.slice(0);angular.forEach(n,function(n,t){var r=k.rowMap[t],i;r!=undefined&&(i=k.rowCache[i],i!=undefined&&(n.preSortSelected=i.selected,n.preSortIndex=t))});e.Sort(k.config.sortInfo,n);angular.forEach(n,function(n,t){k.rowCache[t].entity=n;k.rowCache[t].selected=n.preSortSelected;k.rowMap[n.preSortIndex]=t;delete n.preSortSelected;delete n.preSortIndex})}};k.clearSortingData=function(n){n?(angular.forEach(k.lastSortedColumns,function(t){n.index!=t.index&&(t.sortDirection="",t.sortPriority=null)}),k.lastSortedColumns[0]=n,k.lastSortedColumns.length=1):(angular.forEach(k.lastSortedColumns,function(n){n.sortDirection="";n.sortPriority=null}),k.lastSortedColumns=[])};k.fixColumnIndexes=function(){for(var n=0;n<i.columns.length;n++)i.columns[n].visible!==!1&&(i.columns[n].index=n)};k.fixGroupIndexes=function(){angular.forEach(i.configGroups,function(n,t){n.groupIndex=t+1})};i.elementsNeedMeasuring=!0;i.columns=[];i.renderedRows=[];i.renderedColumns=[];i.headerRow=null;i.rowHeight=k.config.rowHeight;i.jqueryUITheme=k.config.jqueryUITheme;i.showSelectionCheckbox=k.config.showSelectionCheckbox;i.enableCellSelection=k.config.enableCellSelection;i.footer=null;i.selectedItems=k.config.selectedItems;i.multiSelect=k.config.multiSelect;i.showFooter=k.config.showFooter;i.footerRowHeight=i.showFooter?k.config.footerRowHeight:0;i.showColumnMenu=k.config.showColumnMenu;i.showMenu=!1;i.configGroups=[];i.gridId=k.gridId;i.enablePaging=k.config.enablePaging;i.pagingOptions=k.config.pagingOptions;i.i18n={};a.seti18n(i,k.config.i18n);i.adjustScrollLeft=function(n){for(var t,l,a,f=0,e=0,v=i.columns.length,s=[],h=!k.config.enableColumnHeavyVirt,r=0,c=function(n){h?s.push(n):i.renderedColumns[r]?i.renderedColumns[r].setVars(n):i.renderedColumns[r]=n.copy();r++},u=0;u<v;u++)t=i.columns[u],t.visible!==!1&&(l=t.width+f,t.pinned?(c(t),a=u>0?n+e:n,o.setColLeft(t,a,k),e+=t.width):l>=n&&f<=n+k.rootDim.outerWidth&&c(t),f+=t.width);h&&(i.renderedColumns=s)};k.prevScrollTop=0;k.prevScrollIndex=0;i.adjustScrollTop=function(n,t){var u,f,e;if(k.prevScrollTop!==n||t){if(n>0&&k.$viewport[0].scrollHeight-n<=k.$viewport.outerHeight()&&i.$emit("ngGridEventScroll"),u=Math.floor(n/k.config.rowHeight),k.filteredRows.length>k.config.virtualizationThreshold){if(k.prevScrollTop<n&&u<k.prevScrollIndex+p)return;if(k.prevScrollTop>n&&u>k.prevScrollIndex-p)return;f=new y(Math.max(0,u-r),u+k.minRowsToRender()+r)}else e=i.configGroups.length>0?k.rowFactory.parsedData.length:k.data.length,f=new y(0,Math.max(e,k.minRowsToRender()+r));k.prevScrollTop=n;k.rowFactory.UpdateViewableRange(f);k.prevScrollIndex=u}};i.toggleShowMenu=function(){i.showMenu=!i.showMenu};i.toggleSelectAll=function(n){i.selectionProvider.toggleSelectAll(n)};i.totalFilteredItemsLength=function(){return k.filteredRows.length};i.showGroupPanel=function(){return k.config.showGroupPanel};i.topPanelHeight=function(){return k.config.showGroupPanel===!0?k.config.headerRowHeight+32:k.config.headerRowHeight};i.viewportDimHeight=function(){return Math.max(0,k.rootDim.outerHeight-i.topPanelHeight()-i.footerRowHeight-2)};i.groupBy=function(n){if(!(k.data.length<1)&&n.groupable&&n.field){n.sortDirection||n.sort({shiftKey:i.configGroups.length>0?!0:!1});var t=i.configGroups.indexOf(n);t==-1?(n.isGroupedBy=!0,i.configGroups.push(n),n.groupIndex=i.configGroups.length):i.removeGroup(t);k.$viewport.scrollTop(0);o.digest(i)}};i.removeGroup=function(n){var t=i.columns.filter(function(t){return t.groupIndex==n+1})[0];t.isGroupedBy=!1;t.groupIndex=0;i.columns[n].isAggCol&&(i.columns.splice(n,1),i.configGroups.splice(n,1),k.fixGroupIndexes());i.configGroups.length===0&&(k.fixColumnIndexes(),o.digest(i));i.adjustScrollLeft(0)};i.togglePin=function(n){for(var u=n.index,t=0,r=0;r<i.columns.length;r++){if(!i.columns[r].pinned)break;t++}n.pinned&&(t=Math.max(n.originalIndex,t-1));n.pinned=!n.pinned;i.columns.splice(u,1);i.columns.splice(t,0,n);k.fixColumnIndexes();o.BuildStyles(i,k,!0);k.$viewport.scrollLeft(k.$viewport.scrollLeft()-n.width)};i.totalRowWidth=function(){for(var r=0,t=i.columns,n=0;n<t.length;n++)t[n].visible!==!1&&(r+=t[n].width);return r};i.headerScrollerDim=function(){var t=i.viewportDimHeight(),r=k.maxCanvasHt,u=r>t,n=new v;return n.autoFitHeight=!0,n.outerWidth=i.totalRowWidth(),u?n.outerWidth+=k.elementDims.scrollW:r-t<=k.elementDims.scrollH&&(n.outerWidth+=k.elementDims.scrollW),n};k.init()},y=function(n,t){this.topRow=n;this.bottomRow=t},nt=function(n,t,i,r,u){var f=this,e=t.enableRowSelection;f.jqueryUITheme=t.jqueryUITheme;f.rowClasses=t.rowClasses;f.entity=n;f.selectionProvider=i;f.selected=i.getSelection(n);f.cursor=e?"pointer":"default";f.setSelection=function(n){f.selectionProvider.setSelection(f,n);f.selectionProvider.lastClickedRow=f};f.continueSelection=function(n){f.selectionProvider.ChangeSelection(f,n)};f.ensureEntity=function(n){f.entity!=n&&(f.entity=n,f.selected=f.selectionProvider.getSelection(f.entity))};f.toggleSelected=function(n){if(!e&&!t.enableCellSelection)return!0;var i=n.target||n;return i.type=="checkbox"&&i.parentElement.className!="ngSelectionCell ng-scope"?!0:t.selectWithCheckboxOnly&&i.type!="checkbox"?(f.selectionProvider.lastClickedRow=f,!0):(f.beforeSelectionChange(f,n)&&f.continueSelection(n),!1)};f.rowIndex=r;f.offsetTop=f.rowIndex*t.rowHeight;f.rowDisplayIndex=0;f.alternatingRowClass=function(){var n=f.rowIndex%2==0;return{ngRow:!0,selected:f.selected,even:n,odd:!n,"ui-state-default":f.jqueryUITheme&&n,"ui-state-active":f.jqueryUITheme&&!n}};f.beforeSelectionChange=t.beforeSelectionChangeCallback;f.afterSelectionChange=t.afterSelectionChangeCallback;f.getProperty=function(n){return u.evalProperty(f.entity,n)};f.copy=function(){return f.clone=new nt(n,t,i,r,u),f.clone.isClone=!0,f.clone.elm=f.elm,f.clone.orig=f,f.clone};f.setVars=function(n){n.clone=f;f.entity=n.entity;f.selected=n.selected}},ot=function(n,t,i,f,h){var l=this;l.aggCache={};l.parentCache=[];l.dataChanged=!0;l.parsedData=[];l.rowConfig={};l.selectionProvider=t.selectionProvider;l.rowHeight=30;l.numberOfAggregates=0;l.groupedData=undefined;l.rowHeight=n.config.rowHeight;l.rowConfig={enableRowSelection:n.config.enableRowSelection,rowClasses:n.config.rowClasses,selectedItems:t.selectedItems,selectWithCheckboxOnly:n.config.selectWithCheckboxOnly,beforeSelectionChangeCallback:n.config.beforeSelectionChange,afterSelectionChangeCallback:n.config.afterSelectionChange,jqueryUITheme:n.config.jqueryUITheme,enableCellSelection:n.config.enableCellSelection,rowHeight:n.config.rowHeight};l.renderedRange=new y(0,n.minRowsToRender()+r);l.buildEntityRow=function(n,t){return new nt(n,l.rowConfig,l.selectionProvider,t,h)};l.buildAggregateRow=function(t,i){var r=l.aggCache[t.aggIndex];return r||(r=new g(t,l,l.rowConfig.rowHeight,n.config.groupsCollapsedByDefault),l.aggCache[t.aggIndex]=r),r.rowIndex=i,r.offsetTop=i*l.rowConfig.rowHeight,r};l.UpdateViewableRange=function(n){l.renderedRange=n;l.renderedChange()};l.filteredRowsChanged=function(){n.lateBoundColumns&&n.filteredRows.length>0&&(n.config.columnDefs=undefined,n.buildColumns(),n.lateBoundColumns=!1,t.$evalAsync(function(){t.adjustScrollLeft(0)}));l.dataChanged=!0;n.config.groups.length>0&&l.getGrouping(n.config.groups);l.UpdateViewableRange(l.renderedRange)};l.renderedChange=function(){var f,i,r,t;if(!l.groupedData||n.config.groups.length<1){l.renderedChangeNoGroups();n.refreshDomSizes();return}for(l.wasGrouped=!0,l.parentCache=[],f=0,i=l.parsedData.filter(function(n){return n.isAggRow?n.parent&&n.parent.collapsed?!1:!0:(n[u]||(n.rowIndex=f++),!n[u])}),l.totalRows=i.length,r=[],t=l.renderedRange.topRow;t<l.renderedRange.bottomRow;t++)i[t]&&(i[t].offsetTop=t*n.config.rowHeight,r.push(i[t]));n.setRenderedRows(r)};l.renderedChangeNoGroups=function(){for(var i=[],t=l.renderedRange.topRow;t<l.renderedRange.bottomRow;t++)n.filteredRows[t]&&(n.filteredRows[t].rowIndex=t,n.filteredRows[t].offsetTop=t*n.config.rowHeight,i.push(n.filteredRows[t]));n.setRenderedRows(i)};l.fixRowCache=function(){var i=n.data.length,r=i-n.rowCache.length,t;if(r<0)n.rowCache.length=n.rowMap.length=i;else for(t=n.rowCache.length;t<i;t++)n.rowCache[t]=n.rowFactory.buildEntityRow(n.data[t],t)};l.parseGroupData=function(n){var r,i,t;if(n.values)for(r=0;r<n.values.length;r++)l.parentCache[l.parentCache.length-1].children.push(n.values[r]),l.parsedData.push(n.values[r]);else for(i in n)if(i==e||i==o||i==s)continue;else n.hasOwnProperty(i)&&(t=l.buildAggregateRow({gField:n[e],gLabel:i,gDepth:n[o],isAggRow:!0,_ng_hidden_:!1,children:[],aggChildren:[],aggIndex:l.numberOfAggregates,aggLabelFilter:n[s].aggLabelFilter},0),l.numberOfAggregates++,t.parent=l.parentCache[t.depth-1],t.parent&&(t.parent.collapsed=!1,t.parent.aggChildren.push(t)),l.parsedData.push(t),l.parentCache[t.depth]=t,l.parseGroupData(n[i]))};l.getGrouping=function(r){var v,d,a,p,w;l.aggCache=[];l.numberOfAggregates=0;l.groupedData={};var b=n.filteredRows,nt=r.length,k=t.columns;for(v=0;v<b.length;v++){if(d=b[v].entity,!d)return;for(b[v][u]=n.config.groupsCollapsedByDefault,a=l.groupedData,p=0;p<r.length;p++){var g=r[p],tt=k.filter(function(n){return n.field==g})[0],y=h.evalProperty(d,g);y=y?y.toString():"null";a[y]||(a[y]={});a[e]||(a[e]=g);a[o]||(a[o]=p);a[s]||(a[s]=tt);a=a[y]}a.values||(a.values=[]);a.values.push(b[v])}for(w=0;w<r.length;w++)!k[w].isAggCol&&w<=nt&&k.splice(0,0,new c({colDef:{field:"",width:25,sortable:!1,resizable:!1,headerCellTemplate:'<div class="ngAggHeader"><\/div>',pinned:n.config.pinSelectionCheckbox},enablePinning:n.config.enablePinning,isAggCol:!0,headerRowHeight:n.config.headerRowHeight},t,n,i,f,h));i.BuildStyles(t,n,!0);n.fixColumnIndexes();t.adjustScrollLeft(0);l.parsedData.length=0;l.parseGroupData(l.groupedData);l.fixRowCache()};n.config.groups.length>0&&n.filteredRows.length>0&&l.getGrouping(n.config.groups)},st=function(n,i,r){var u=this,f=[];u.extFilter=i.config.filterOptions.useExternalFilter;n.showFilter=i.config.showFilter;n.filterText="";u.fieldMap={};u.evalFilter=function(){var t=function(n){for(var i,c,a,s,v,y,t,w,h,d,l=0,p=f.length;l<p;l++){if(i=f[l],!i.column){for(a in n)if(n.hasOwnProperty(a)){if(s=u.fieldMap[a],!s)continue;if(v=null,y=null,s&&s.cellFilter&&(y=s.cellFilter.split(":"),v=r(y[0])),t=n[a],t!=null&&(typeof v=="function"?(w=v(typeof t=="object"?e(t,s.field):t,y[1]).toString(),c=i.regex.test(w)):c=i.regex.test(typeof t=="object"?e(t,s.field).toString():t.toString()),t&&c))return!0}return!1}if(h=u.fieldMap[i.columnDisplay],!h)return!1;var b=h.cellFilter.split(":"),k=h.cellFilter?r(b[0]):null,o=n[i.column]||n[h.field.split(".")[0]];if(o==null||(typeof k=="function"?(d=k(typeof o=="object"?e(o,h.field):o,b[1]).toString(),c=i.regex.test(d)):c=i.regex.test(typeof o=="object"?e(o,h.field).toString():o.toString()),!o||!c))return!1}return!0},n;for(i.filteredRows=f.length===0?i.rowCache:i.rowCache.filter(function(n){return t(n.entity)}),n=0;n<i.filteredRows.length;n++)i.filteredRows[n].rowIndex=n;i.rowFactory.filteredRowsChanged()};var e=function(n,t){var r,i,u,f;if(typeof n!="object"||typeof t!="string")return n;if(r=t.split("."),i=n,r.length>1){for(u=1,f=r.length;u<f;u++)if(i=i[r[u]],!i)return n;return i}return n},o=function(n,t){try{return new RegExp(n,t)}catch(i){return new RegExp(n.replace(/(\^|\$|\(|\)|\<|\>|\[|\]|\{|\}|\\|\||\.|\*|\+|\?)/g,"\\$1"))}},s=function(n){var c,e,r,i,u,s,h;if(f=[],c=t.trim(n))for(e=c.split(";"),r=0;r<e.length;r++)i=e[r].split(":"),i.length>1?(u=t.trim(i[0]),s=t.trim(i[1]),u&&s&&f.push({column:u,columnDisplay:u.replace(/\s+/g,"").toLowerCase(),regex:o(s,"i")})):(h=t.trim(i[0]),h&&f.push({column:"",regex:o(h,"i")}))};n.$watch(function(){return i.config.filterOptions.filterText},function(t){n.filterText=t});n.$watch("filterText",function(t){u.extFilter||(n.$emit("ngGridEventFilter",t),s(t),u.evalFilter())});u.extFilter||n.$watch("columns",function(n){for(var t,i=0;i<n.length;i++)t=n[i],t.field&&(u.fieldMap[t.field.split(".")[0]]=t),t.displayName&&(u.fieldMap[t.displayName.toLowerCase().replace(/\s+/g,"")]=t)})},ht=function(n,t,i){var r=this;r.multi=n.config.multiSelect;r.selectedItems=n.config.selectedItems;r.selectedIndex=n.config.selectedIndex;r.lastClickedRow=undefined;r.ignoreSelectedItemChanges=!1;r.pKeyParser=i(n.config.primaryKey);r.ChangeSelection=function(i,u){var l=u.which||u.keyCode,a=l===40||l===38,v,e,f,o,h,s,y,c;if(!u||u.keyCode&&!a||u.ctrlKey||u.shiftKey||r.toggleSelectAll(!1,!0),u&&u.shiftKey&&!u.keyCode&&r.multi&&n.config.enableRowSelection){if(r.lastClickedRow){if(v=t.configGroups.length>0?n.rowFactory.parsedData.filter(function(n){return!n.isAggRow}):n.filteredRows,e=i.rowIndex,f=r.lastClickedRow.rowIndex,r.lastClickedRow=i,e==f)return!1;for(e<f?(e=e^f,f=e^f,e=e^f,e--):f++,o=[];f<=e;f++)o.push(v[f]);if(o[o.length-1].beforeSelectionChange(o,u)){for(h=0;h<o.length;h++)s=o[h],y=s.selected,s.selected=!y,s.clone&&(s.clone.selected=s.selected),c=r.selectedItems.indexOf(s.entity),c===-1?r.selectedItems.push(s.entity):r.selectedItems.splice(c,1);o[o.length-1].afterSelectionChange(o,u)}return!0}}else r.multi?(!u.keyCode||a)&&r.setSelection(i,!i.selected):r.lastClickedRow==i?r.setSelection(r.lastClickedRow,n.config.keepLastSelected?!0:!i.selected):(r.lastClickedRow&&r.setSelection(r.lastClickedRow,!1),r.setSelection(i,!i.selected));return r.lastClickedRow=i,!0};r.getSelection=function(t){var i=!1,u;return n.config.primaryKey?(u=r.pKeyParser(t),angular.forEach(r.selectedItems,function(n){u==r.pKeyParser(n)&&(i=!0)})):i=r.selectedItems.indexOf(t)!==-1,i};r.setSelection=function(t,i){if(n.config.enableRowSelection){if(i)r.selectedItems.indexOf(t.entity)===-1&&(!r.multi&&r.selectedItems.length>0&&r.toggleSelectAll(!1,!0),r.selectedItems.push(t.entity));else{var u=r.selectedItems.indexOf(t.entity);u!=-1&&r.selectedItems.splice(u,1)}t.selected=i;t.orig&&(t.orig.selected=i);t.clone&&(t.clone.selected=i);t.afterSelectionChange(t)}};r.toggleSelectAll=function(t,i){var f,u;if(i||n.config.beforeSelectionChange(n.filteredRows,t)){for(f=r.selectedItems.length,f>0&&(r.selectedItems.length=0),u=0;u<n.filteredRows.length;u++)n.filteredRows[u].selected=t,n.filteredRows[u].clone&&(n.filteredRows[u].clone.selected=t),t&&r.selectedItems.push(n.filteredRows[u].entity);i||n.config.afterSelectionChange(n.filteredRows,t)}}},ct=function(n,t){n.headerCellStyle=function(n){return{height:n.headerRowHeight+"px"}};n.rowStyle=function(t){var i={top:t.offsetTop+"px",height:n.rowHeight+"px"};return t.isAggRow&&(i.left=t.offsetLeft),i};n.canvasStyle=function(){return{height:t.maxCanvasHt.toString()+"px"}};n.headerScrollerStyle=function(){return{height:t.config.headerRowHeight+"px"}};n.topPanelStyle=function(){return{width:t.rootDim.outerWidth+"px",height:n.topPanelHeight()+"px"}};n.headerStyle=function(){return{width:t.rootDim.outerWidth+"px",height:t.config.headerRowHeight+"px"}};n.groupPanelStyle=function(){return{width:t.rootDim.outerWidth+"px",height:"32px"}};n.viewportStyle=function(){return{width:t.rootDim.outerWidth+"px",height:n.viewportDimHeight()+"px"}};n.footerStyle=function(){return{width:t.rootDim.outerWidth+"px",height:n.footerRowHeight+"px"}}};i.directive("ngCellHasFocus",["$domUtilityService",function(n){var t=function(t,i){t.isFocused=!0;n.digest(t);var u=angular.element(i[0].children).filter(function(){return this.nodeType!=8}),r=angular.element(u[0].children[0]);r.length>0&&(angular.element(r).focus(),t.domAccessProvider.selectInputElement(r[0]),angular.element(r).bind("blur",function(){return t.isFocused=!1,n.digest(t),!0}))};return function(n,i){var r=!1;n.editCell=function(){setTimeout(function(){t(n,i)},0)};i.bind("mousedown",function(){return i.focus(),!0});i.bind("focus",function(){return r=!0,!0});i.bind("blur",function(){return r=!1,!0});i.bind("keydown",function(u){return r&&u.keyCode!=37&&u.keyCode!=38&&u.keyCode!=39&&u.keyCode!=40&&u.keyCode!=9&&!u.shiftKey&&u.keyCode!=13&&t(n,i),u.keyCode==27&&i.focus(),!0})}}]);i.directive("ngCellText",function(){return function(n,t){t.bind("mouseover",function(n){n.preventDefault();t.css({cursor:"text"})});t.bind("mouseleave",function(n){n.preventDefault();t.css({cursor:"default"})})}});i.directive("ngCell",["$compile","$domUtilityService",function(n,t){return{scope:!1,compile:function(){return{pre:function(t,i){var r,f=t.col.cellTemplate.replace(b,"$eval('row.entity.' + col.field)"),u;t.col.enableCellEdit?(r=t.col.cellEditTemplate,r=r.replace(tt,f),r=r.replace(it,t.col.editableCellTemplate.replace(b,"$eval('row.entity.' + col.field)"))):r=f;u=n(r)(t);t.enableCellSelection&&u[0].className.indexOf("ngSelectionCell")==-1&&(u[0].setAttribute("tabindex",0),u.addClass("ngCellElement"));i.append(u)},post:function(n,i){n.enableCellSelection&&n.domAccessProvider.selectionHandlers(n,i);n.$on("ngGridEventDigestCell",function(){t.digest(n)})}}}}}]);i.directive("ngGridFooter",["$compile","$templateCache",function(n,t){return{scope:!1,compile:function(){return{pre:function(i,r){r.children().length===0&&r.append(n(t.get(i.gridId+"footerTemplate.html"))(i))}}}}}]);i.directive("ngGridMenu",["$compile","$templateCache",function(n,t){return{scope:!1,compile:function(){return{pre:function(i,r){r.children().length===0&&r.append(n(t.get(i.gridId+"menuTemplate.html"))(i))}}}}}]);i.directive("ngGrid",["$compile","$filter","$templateCache","$sortService","$domUtilityService","$utilityService","$timeout","$parse",function(n,i,r,u,f,e,o,s){return{scope:!0,compile:function(){return{pre:function(h,c,l){var w=t(c),y=h.$eval(l.ngGrid),a,p;return y.gridDim=new v({outerHeight:t(w).height(),outerWidth:t(w).width()}),a=new et(h,y,u,f,i,r,e,o,s),typeof y.columnDefs=="string"?h.$parent.$watch(y.columnDefs,function(n){if(!n){a.refreshDomSizes();a.buildColumns();return}a.lateBoundColumns=!1;h.columns=[];a.config.columnDefs=n;a.buildColumns();a.configureColumnWidths();a.eventProvider.assignEvents();f.RebuildGrid(h,a)},!0):a.buildColumns(),typeof y.data=="string"&&(p=function(n){a.data=t.extend([],n);a.rowFactory.fixRowCache();angular.forEach(a.data,function(n,t){var i=a.rowMap[t]||t;a.rowCache[i]&&a.rowCache[i].ensureEntity(n);a.rowMap[i]=t});a.searchProvider.evalFilter();a.configureColumnWidths();a.refreshDomSizes();a.config.sortInfo.fields.length>0&&(a.getColsFromFields(),a.sortActual(),a.searchProvider.evalFilter(),h.$emit("ngGridEventSorted",a.config.sortInfo));h.$emit("ngGridEventData",a.gridId)},h.$parent.$watch(y.data,p),h.$parent.$watch(y.data+".length",function(){p(h.$eval(y.data))})),a.footerController=new ft(h,a),c.addClass("ngGrid").addClass(a.gridId.toString()),y.enableHighlighting||c.addClass("unselectable"),y.jqueryUITheme&&c.addClass("ui-widget"),c.append(n(r.get("gridTemplate.html"))(h)),f.AssignGridContainers(h,c,a),a.eventProvider=new ut(a,h,f,o),y.selectRow=function(n,t){a.rowCache[n]&&(a.rowCache[n].clone&&a.rowCache[n].clone.setSelection(t?!0:!1),a.rowCache[n].setSelection(t?!0:!1))},y.selectItem=function(n,t){y.selectRow(a.rowMap[n],t)},y.selectAll=function(n){h.toggleSelectAll(n)},y.groupBy=function(n){if(n)h.groupBy(h.columns.filter(function(t){return t.field==n})[0]);else{var i=t.extend(!0,[],h.configGroups);angular.forEach(i,h.groupBy)}},y.sortBy=function(n){var t=h.columns.filter(function(t){return t.field==n})[0];t&&t.sort()},y.gridId=a.gridId,y.ngGrid=a,y.$gridScope=h,y.$gridServices={SortService:u,DomUtilityService:f},h.$on("ngGridEventDigestGrid",function(){f.digest(h.$parent)}),h.$on("ngGridEventDigestGridParent",function(){f.digest(h.$parent)}),h.$evalAsync(function(){h.adjustScrollLeft(0)}),angular.forEach(y.plugins,function(n){typeof n=="function"&&(n=n.call(this));n.init(h.$new(),a,y.$gridServices);y.plugins[e.getInstanceType(n)]=n}),y.init=="function"&&y.init(a,h),null}}}}}]);i.directive("ngHeaderCell",["$compile",function(n){return{scope:!1,compile:function(){return{pre:function(t,i){i.append(n(t.col.headerCellTemplate)(t))}}}}}]);i.directive("ngIf",[function(){return{transclude:"element",priority:1e3,terminal:!0,restrict:"A",compile:function(n,t,i){return function(n,t,r){var f,u;n.$watch(r.ngIf,function(r){f&&(f.remove(),f=undefined);u&&(u.$destroy(),u=undefined);r&&(u=n.$new(),i(u,function(n){f=n;t.after(n)}))})}}}}]);i.directive("ngInput",["$parse",function(n){return function(t,i,r){var f=n(t.$eval(r.ngInput)),e=f.assign,u=f(t.row.entity);i.val(u);i.bind("keyup",function(){var n=i.val();t.$root.$$phase||t.$apply(function(){e(t.row.entity,n)})});i.bind("keydown",function(n){switch(n.keyCode){case 37:case 38:case 39:case 40:n.stopPropagation();break;case 27:t.$root.$$phase||t.$apply(function(){e(t.row.entity,u);i.val(u);i.blur()})}return!0})}}]);i.directive("ngRow",["$compile","$domUtilityService","$templateCache",function(n,t,i){return{scope:!1,compile:function(){return{pre:function(r,u){if(r.row.elm=u,r.row.clone&&(r.row.clone.elm=u),r.row.isAggRow){var f=i.get(r.gridId+"aggregateTemplate.html");f=r.row.aggLabelFilter?f.replace(l,"| "+r.row.aggLabelFilter):f.replace(l,"");u.append(n(f)(r))}else u.append(n(i.get(r.gridId+"rowTemplate.html"))(r));r.$on("ngGridEventDigestRow",function(){t.digest(r)})}}}}}]);i.directive("ngViewport",[function(){return function(n,t){var i,u,r=0;t.bind("scroll",function(t){var f=t.target.scrollLeft,e=t.target.scrollTop;return n.$headerContainer&&n.$headerContainer.scrollLeft(f),n.adjustScrollLeft(f),n.adjustScrollTop(e),n.$root.$$phase||n.$digest(),u=f,r=r,i=!1,!0});t.bind("mousewheel DOMMouseScroll",function(){return i=!0,t.focus(),!0});n.enableCellSelection||n.domAccessProvider.selectionHandlers(n,t)}}]);n.ngGrid.i18n.en={ngAggregateLabel:"items",ngGroupPanelDescription:"Drag a column header here and drop it to group by that column.",ngSearchPlaceHolder:"Search...",ngMenuText:"Choose Columns:",ngShowingItemsLabel:"Showing Items:",ngTotalItemsLabel:"Total Items:",ngSelectedItemsLabel:"Selected Items:",ngPageSizeLabel:"Page Size:",ngPagerFirstTitle:"First Page",ngPagerNextTitle:"Next Page",ngPagerPrevTitle:"Previous Page",ngPagerLastTitle:"Last Page"};n.ngGrid.i18n.fr={ngAggregateLabel:"articles",ngGroupPanelDescription:"Faites glisser un en-tête de colonne ici et déposez-le vers un groupe par cette colonne.",ngSearchPlaceHolder:"Recherche...",ngMenuText:"Choisir des colonnes:",ngShowingItemsLabel:"Articles Affichage des:",ngTotalItemsLabel:"Nombre total d'articles:",ngSelectedItemsLabel:"Éléments Articles:",ngPageSizeLabel:"Taille de page:",ngPagerFirstTitle:"Première page",ngPagerNextTitle:"Page Suivante",ngPagerPrevTitle:"Page précédente",ngPagerLastTitle:"Dernière page"};n.ngGrid.i18n.ge={ngAggregateLabel:"artikel",ngGroupPanelDescription:"Ziehen Sie eine Spaltenüberschrift hier und legen Sie es der Gruppe nach dieser Spalte.",ngSearchPlaceHolder:"Suche...",ngMenuText:"Spalten auswählen:",ngShowingItemsLabel:"Zeige Artikel:",ngTotalItemsLabel:"Meiste Artikel:",ngSelectedItemsLabel:"Ausgewählte Artikel:",ngPageSizeLabel:"Größe Seite:",ngPagerFirstTitle:"Erste Page",ngPagerNextTitle:"Nächste Page",ngPagerPrevTitle:"Vorherige Page",ngPagerLastTitle:"Letzte Page"};n.ngGrid.i18n.sp={ngAggregateLabel:"Artículos",ngGroupPanelDescription:"Arrastre un encabezado de columna aquí y soltarlo para agrupar por esa columna.",ngSearchPlaceHolder:"Buscar...",ngMenuText:"Elegir columnas:",ngShowingItemsLabel:"Artículos Mostrando:",ngTotalItemsLabel:"Artículos Totales:",ngSelectedItemsLabel:"Artículos Seleccionados:",ngPageSizeLabel:"Tamaño de Página:",ngPagerFirstTitle:"Primera Página",ngPagerNextTitle:"Página Siguiente",ngPagerPrevTitle:"Página Anterior",ngPagerLastTitle:"Última Página"};n.ngGrid.i18n["zh-cn"]={ngAggregateLabel:"条目",ngGroupPanelDescription:"拖曳表头到此处以进行分组",ngSearchPlaceHolder:"搜索...",ngMenuText:"数据分组与选择列：",ngShowingItemsLabel:"当前显示条目：",ngTotalItemsLabel:"条目总数：",ngSelectedItemsLabel:"选中条目：",ngPageSizeLabel:"每页显示数：",ngPagerFirstTitle:"回到首页",ngPagerNextTitle:"下一页",ngPagerPrevTitle:"上一页",ngPagerLastTitle:"前往尾页"};n.ngGrid.i18n["zh-tw"]={ngAggregateLabel:"筆",ngGroupPanelDescription:"拖拉表頭到此處以進行分組",ngSearchPlaceHolder:"搜尋...",ngMenuText:"選擇欄位：",ngShowingItemsLabel:"目前顯示筆數：",ngTotalItemsLabel:"總筆數：",ngSelectedItemsLabel:"選取筆數：",ngPageSizeLabel:"每頁顯示：",ngPagerFirstTitle:"第一頁",ngPagerNextTitle:"下一頁",ngPagerPrevTitle:"上一頁",ngPagerLastTitle:"最後頁"};angular.module("ngGrid").run(["$templateCache",function(n){n.put("aggregateTemplate.html",'<div ng-click="row.toggleExpand()" ng-style="rowStyle(row)" class="ngAggregate">    <span class="ngAggregateText">{{row.label CUSTOM_FILTERS}} ({{row.totalChildren()}} {{AggItemsLabel}})<\/span>    <div class="{{row.aggClass()}}"><\/div><\/div>');n.put("cellEditTemplate.html",'<div ng-cell-has-focus ng-dblclick="editCell()">\t<div ng-if="!isFocused">\tDISPLAY_CELL_TEMPLATE\t<\/div>\t<div ng-if="isFocused">\tEDITABLE_CELL_TEMPLATE\t<\/div><\/div>');n.put("cellTemplate.html",'<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{COL_FIELD CUSTOM_FILTERS}}<\/span><\/div>');n.put("checkboxCellTemplate.html",'<div class="ngSelectionCell"><input tabindex="-1" class="ngSelectionCheckbox" type="checkbox" ng-checked="row.selected" /><\/div>');n.put("checkboxHeaderTemplate.html",'<input class="ngSelectionHeader" type="checkbox" ng-show="multiSelect" ng-model="allSelected" ng-change="toggleSelectAll(allSelected)"/>');n.put("editableCellTemplate.html",'<input ng-class="\'colt\' + col.index" ng-input="COL_FIELD" />');n.put("footerTemplate.html",'<div ng-show="showFooter" class="ngFooterPanel" ng-class="{\'ui-widget-content\': jqueryUITheme, \'ui-corner-bottom\': jqueryUITheme}" ng-style="footerStyle()">    <div class="ngTotalSelectContainer" >        <div class="ngFooterTotalItems" ng-class="{\'ngNoMultiSelect\': !multiSelect}" >            <span class="ngLabel">{{i18n.ngTotalItemsLabel}} {{maxRows()}}<\/span><span ng-show="filterText.length > 0" class="ngLabel">({{i18n.ngShowingItemsLabel}} {{totalFilteredItemsLength()}})<\/span>        <\/div>        <div class="ngFooterSelectedItems" ng-show="multiSelect">            <span class="ngLabel">{{i18n.ngSelectedItemsLabel}} {{selectedItems.length}}<\/span>        <\/div>    <\/div>    <div class="ngPagerContainer" style="float: right; margin-top: 10px;" ng-show="enablePaging" ng-class="{\'ngNoMultiSelect\': !multiSelect}">        <div style="float:left; margin-right: 10px;" class="ngRowCountPicker">            <span style="float: left; margin-top: 3px;" class="ngLabel">{{i18n.ngPageSizeLabel}}<\/span>            <select style="float: left;height: 27px; width: 100px" ng-model="pagingOptions.pageSize" >                <option ng-repeat="size in pagingOptions.pageSizes">{{size}}<\/option>            <\/select>        <\/div>        <div style="float:left; margin-right: 10px; line-height:25px;" class="ngPagerControl" style="float: left; min-width: 135px;">            <button class="ngPagerButton" ng-click="pageToFirst()" ng-disabled="cantPageBackward()" title="{{i18n.ngPagerFirstTitle}}"><div class="ngPagerFirstTriangle"><div class="ngPagerFirstBar"><\/div><\/div><\/button>            <button class="ngPagerButton" ng-click="pageBackward()" ng-disabled="cantPageBackward()" title="{{i18n.ngPagerPrevTitle}}"><div class="ngPagerFirstTriangle ngPagerPrevTriangle"><\/div><\/button>            <input class="ngPagerCurrent" type="number" style="width:50px; height: 24px; margin-top: 1px; padding: 0 4px;" ng-model="pagingOptions.currentPage"/>            <button class="ngPagerButton" ng-click="pageForward()" ng-disabled="cantPageForward()" title="{{i18n.ngPagerNextTitle}}"><div class="ngPagerLastTriangle ngPagerNextTriangle"><\/div><\/button>            <button class="ngPagerButton" ng-click="pageToLast()" ng-disabled="cantPageToLast()" title="{{i18n.ngPagerLastTitle}}"><div class="ngPagerLastTriangle"><div class="ngPagerLastBar"><\/div><\/div><\/button>        <\/div>    <\/div><\/div>');n.put("gridTemplate.html",'<div class="ngTopPanel" ng-class="{\'ui-widget-header\':jqueryUITheme, \'ui-corner-top\': jqueryUITheme}" ng-style="topPanelStyle()">    <div class="ngGroupPanel" ng-show="showGroupPanel()" ng-style="groupPanelStyle()">        <div class="ngGroupPanelDescription" ng-show="configGroups.length == 0">{{i18n.ngGroupPanelDescription}}<\/div>        <ul ng-show="configGroups.length > 0" class="ngGroupList">            <li class="ngGroupItem" ng-repeat="group in configGroups">                <span class="ngGroupElement">                    <span class="ngGroupName">{{group.displayName}}                        <span ng-click="removeGroup($index)" class="ngRemoveGroup">x<\/span>                    <\/span>                    <span ng-hide="$last" class="ngGroupArrow"><\/span>                <\/span>            <\/li>        <\/ul>    <\/div>    <div class="ngHeaderContainer" ng-style="headerStyle()">        <div class="ngHeaderScroller" ng-style="headerScrollerStyle()" ng-include="gridId + \'headerRowTemplate.html\'"><\/div>    <\/div>    <div ng-grid-menu><\/div><\/div><div class="ngViewport" unselectable="on" ng-viewport ng-class="{\'ui-widget-content\': jqueryUITheme}" ng-style="viewportStyle()">    <div class="ngCanvas" ng-style="canvasStyle()">        <div ng-style="rowStyle(row)" ng-repeat="row in renderedRows" ng-click="row.toggleSelected($event)" ng-class="row.alternatingRowClass()" ng-row><\/div>    <\/div><\/div><div ng-grid-footer><\/div>');n.put("headerCellTemplate.html",'<div class="ngHeaderSortColumn {{col.headerClass}}" ng-style="{\'cursor\': col.cursor}" ng-class="{ \'ngSorted\': !noSortVisible }">    <div ng-click="col.sort($event)" ng-class="\'colt\' + col.index" class="ngHeaderText">{{col.displayName}}<\/div>    <div class="ngSortButtonDown" ng-show="col.showSortButtonDown()" style="margin-top:9px;"><\/div>    <div class="ngSortButtonUp" ng-show="col.showSortButtonUp()" style="margin-top:9px;"><\/div>    <div class="ngSortPriority">{{col.sortPriority}}<\/div>    <div ng-class="{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }" ng-click="togglePin(col)" ng-show="col.pinnable"><\/div><\/div><div ng-show="col.resizable" class="ngHeaderGrip" ng-click="col.gripClick($event)" ng-mousedown="col.gripOnMouseDown($event)"><\/div>');n.put("headerRowTemplate.html",'<div ng-style="{ height: col.headerRowHeight }" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" class="ngHeaderCell" ng-header-cell><\/div>');n.put("menuTemplate.html",'<div ng-show="showColumnMenu || showFilter"  class="ngHeaderButton" ng-click="toggleShowMenu()">    <div class="ngHeaderButtonArrow"><\/div><\/div><div ng-show="showMenu" class="ngColMenu">    <div ng-show="showFilter">        <input placeholder="{{i18n.ngSearchPlaceHolder}}" type="text" ng-model="filterText"/>    <\/div>    <div ng-show="showColumnMenu">        <span class="ngMenuText">{{i18n.ngMenuText}}<\/span>        <ul class="ngColList">            <li class="ngColListItem" ng-repeat="col in columns | ngColumns">                <label><input ng-disabled="col.pinned" type="checkbox" class="ngColListCheckbox" ng-model="col.visible"/>{{col.displayName}}<\/label>\t\t\t\t<a title="Group By" ng-class="col.groupedByClass()" ng-show="col.groupable && col.visible" ng-click="groupBy(col)"><\/a>\t\t\t\t<span class="ngGroupingNumber" ng-show="col.groupIndex > 0">{{col.groupIndex}}<\/span>                      <\/li>        <\/ul>    <\/div><\/div>');n.put("rowTemplate.html",'<div ng-style="{ \'cursor\': row.cursor }" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" class="ngCell {{col.cellClass}}" ng-cell><\/div>')}])})(window,jQuery);
/*
//# sourceMappingURL=ng-grid.min.js.map
*/
///#source 1 1 /appnew/bower_components/ngstorage/ngStorage.min.js
"use strict";(function(){function n(n){return["$rootScope","$window",function(t,i){for(var f=i[n]||(console.warn("This browser does not support Web Storage!"),{}),r={$default:function(n){for(var t in n)angular.isDefined(r[t])||(r[t]=n[t]);return r},$reset:function(n){for(var t in r)"$"===t[0]||delete r[t];return r.$default(n)}},u,o,s=0,e;s<f.length;s++)(e=f.key(s))&&"ngStorage-"===e.slice(0,10)&&(r[e.slice(10)]=angular.fromJson(f.getItem(e)));return u=angular.copy(r),t.$watch(function(){o||(o=setTimeout(function(){if(o=null,!angular.equals(r,u)){angular.forEach(r,function(n,t){angular.isDefined(n)&&"$"!==t[0]&&f.setItem("ngStorage-"+t,angular.toJson(n));delete u[t]});for(var n in u)f.removeItem("ngStorage-"+n);u=angular.copy(r)}},100))}),"localStorage"===n&&i.addEventListener&&i.addEventListener("storage",function(n){"ngStorage-"===n.key.slice(0,10)&&(n.newValue?r[n.key.slice(10)]=angular.fromJson(n.newValue):delete r[n.key.slice(10)],u=angular.copy(r),t.$apply())}),r}]}angular.module("ngStorage",[]).factory("$localStorage",n("localStorage")).factory("$sessionStorage",n("sessionStorage"))})();
/*
//# sourceMappingURL=ngStorage.min.js.map
*/
///#source 1 1 /appnew/bower_components/angular-ui-utils/ui-utils.min.js
function uiUploader(n){"use strict";function r(n){for(var i=0;i<n.length;i++)t.files.push(n[i])}function u(){return t.files}function i(n){t.options=n;for(var i=0;i<t.files.length;i++){if(t.activeUploads==t.options.concurrency)break;t.files[i].active||s(t.files[i],t.options.url)}}function f(n){t.files.splice(t.files.indexOf(n),1)}function e(){t.files.splice(0,t.files.length)}function o(n){var i=["n/a","bytes","KiB","MiB","GiB","TB","PB","EiB","ZiB","YiB"],t=+Math.floor(Math.log(n)/Math.log(1024));return(n/Math.pow(1024,t)).toFixed(t?1:0)+" "+i[isNaN(n)?0:t+1]}function s(n,r){var u,f,e,s="";if(t.activeUploads+=1,n.active=!0,u=new window.XMLHttpRequest,f=new window.FormData,u.open("POST",r),u.upload.onloadstart=function(){},u.upload.onprogress=function(i){if(i.lengthComputable){n.loaded=i.loaded;n.humanSize=o(i.loaded);t.options.onProgress(n)}},u.onload=function(){t.activeUploads-=1;i(t.options);t.options.onCompleted(n)},u.onerror=function(){},s)for(e in s)s.hasOwnProperty(e)&&f.append(e,s[e]);return f.append("file",n,n.name),u.send(f),u}var t=this;return t.files=[],t.options={},t.activeUploads=0,n.info("uiUploader loaded"),{addFiles:r,getFiles:u,files:t.files,startUpload:i,removeFile:f,removeAll:e}}angular.module("ui.alias",[]).config(["$compileProvider","uiAliasConfig",function(n,t){"use strict";t=t||{};angular.forEach(t,function(t,i){angular.isString(t)&&(t={replace:!0,template:t});n.directive(i,function(){return t})})}]);angular.module("ui.event",[]).directive("uiEvent",["$parse",function(n){"use strict";return function(t,i,r){var u=t.$eval(r.uiEvent);angular.forEach(u,function(r,u){var f=n(r);i.bind(u,function(n){var i=Array.prototype.slice.call(arguments);i=i.splice(1);f(t,{$event:n,$params:i});t.$$phase||t.$apply()})})}}]);angular.module("ui.format",[]).filter("format",function(){"use strict";return function(n,t){var i=n,r,u;return angular.isString(i)&&t!==undefined&&(angular.isArray(t)||angular.isObject(t)||(t=[t]),angular.isArray(t)?(r=t.length,u=function(n,i){return i=parseInt(i,10),i>=0&&i<r?t[i]:n},i=i.replace(/\$([0-9]+)/g,u)):angular.forEach(t,function(n,t){i=i.split(":"+t).join(n)})),i}});angular.module("ui.highlight",[]).filter("highlight",function(){"use strict";return function(n,t,i){return n&&(t||angular.isNumber(t))?(n=n.toString(),t=t.toString(),i?n.split(t).join('<span class="ui-match">'+t+"<\/span>"):n.replace(new RegExp(t,"gi"),'<span class="ui-match">$&<\/span>')):n}});angular.module("ui.include",[]).directive("uiInclude",["$http","$templateCache","$anchorScroll","$compile",function(n,t,i,r){"use strict";return{restrict:"ECA",terminal:!0,compile:function(u,f){var o=f.uiInclude||f.src,s=f.fragment||"",h=f.onload||"",e=f.autoscroll;return function(u,f){function v(){var v=++l,y=u.$eval(o),p=u.$eval(s);y?n.get(y,{cache:t}).success(function(n){if(v===l){c&&c.$destroy();c=u.$new();var t;t=p?angular.element("<div/>").html(n).find(p):angular.element("<div/>").html(n).contents();f.html(t);r(t)(c);angular.isDefined(e)&&(!e||u.$eval(e))&&i();c.$emit("$includeContentLoaded");u.$eval(h)}}).error(function(){v===l&&a()}):a()}var l=0,c,a=function(){c&&(c.$destroy(),c=null);f.html("")};u.$watch(s,v);u.$watch(o,v)}}}}]);angular.module("ui.indeterminate",[]).directive("uiIndeterminate",[function(){"use strict";return{compile:function(n,t){return!t.type||t.type.toLowerCase()!=="checkbox"?angular.noop:function(n,t,i){n.$watch(i.uiIndeterminate,function(n){t[0].indeterminate=!!n})}}}}]);angular.module("ui.inflector",[]).filter("inflector",function(){"use strict";function n(n){return n=n.replace(/([A-Z])|([\-|\_])/g,function(n,t){return" "+(t||"")}),n.replace(/\s\s+/g," ").trim().toLowerCase().split(" ")}function t(n){var t=[];return angular.forEach(n,function(n){t.push(n.charAt(0).toUpperCase()+n.substr(1))}),t}var i={humanize:function(i){return t(n(i)).join(" ")},underscore:function(t){return n(t).join("_")},variable:function(i){return i=n(i),i[0]+t(i.slice(1)).join("")}};return function(n,t){return t!==!1&&angular.isString(n)?(t=t||"humanize",i[t](n)):n}});angular.module("ui.jq",[]).value("uiJqConfig",{}).directive("uiJq",["uiJqConfig","$timeout",function(n,t){"use strict";return{restrict:"A",compile:function(i,r){if(!angular.isFunction(i[r.uiJq]))throw new Error('ui-jq: The "'+r.uiJq+'" function does not exist');var u=n&&n[r.uiJq];return function(n,i,r){function e(){t(function(){i[r.uiJq].apply(i,f)},0,!1)}var f=[];r.uiOptions?(f=n.$eval("["+r.uiOptions+"]"),angular.isObject(u)&&angular.isObject(f[0])&&(f[0]=angular.extend({},u,f[0]))):u&&(f=[u]);r.ngModel&&i.is("select,input,textarea")&&i.bind("change",function(){i.trigger("input")});r.uiRefresh&&n.$watch(r.uiRefresh,function(){e()});e()}}}}]);angular.module("ui.keypress",[]).factory("keypressHelper",["$parse",function(n){"use strict";var t={8:"backspace",9:"tab",13:"enter",27:"esc",32:"space",33:"pageup",34:"pagedown",35:"end",36:"home",37:"left",38:"up",39:"right",40:"down",45:"insert",46:"delete"},i=function(n){return n.charAt(0).toUpperCase()+n.slice(1)};return function(r,u,f,e){var o,s=[];o=u.$eval(e["ui"+i(r)]);angular.forEach(o,function(t,i){var r,u;u=n(t);angular.forEach(i.split(" "),function(n){r={expression:u,keys:{}};angular.forEach(n.split("-"),function(n){r.keys[n]=!0});s.push(r)})});f.bind(r,function(n){var e=!!(n.metaKey&&!n.ctrlKey),o=!!n.altKey,h=!!n.ctrlKey,f=!!n.shiftKey,i=n.keyCode;r==="keypress"&&!f&&i>=97&&i<=122&&(i=i-32);angular.forEach(s,function(r){var s=r.keys[t[i]]||r.keys[i.toString()],c=!!r.keys.meta,l=!!r.keys.alt,a=!!r.keys.ctrl,v=!!r.keys.shift;s&&c===e&&l===o&&a===h&&v===f&&u.$apply(function(){r.expression(u,{$event:n})})})})}}]);angular.module("ui.keypress").directive("uiKeydown",["keypressHelper",function(n){"use strict";return{link:function(t,i,r){n("keydown",t,i,r)}}}]);angular.module("ui.keypress").directive("uiKeypress",["keypressHelper",function(n){"use strict";return{link:function(t,i,r){n("keypress",t,i,r)}}}]);angular.module("ui.keypress").directive("uiKeyup",["keypressHelper",function(n){"use strict";return{link:function(t,i,r){n("keyup",t,i,r)}}}]);angular.module("ui.mask",[]).value("uiMaskConfig",{maskDefinitions:{"9":/\d/,A:/[a-zA-Z]/,"*":/[a-zA-Z0-9]/},clearOnBlur:!0}).directive("uiMask",["uiMaskConfig","$parse",function(n,t){"use strict";return{priority:100,require:"ngModel",restrict:"A",compile:function(){var i=n;return function(n,r,u,f){function vt(n){return angular.isDefined(n)?(ti(n),!l)?lt():(bt(),kt(),!0):lt()}function yt(n){angular.isDefined(n)&&(c=n,l&&a())}function pt(n){return l?(e=p(n||""),s=ut(e),f.$setValidity("mask",s),s&&e.length?w(e):undefined):n}function wt(n){return l?(e=p(n||""),s=ut(e),f.$viewValue=e.length?w(e):"",f.$setValidity("mask",s),e===""&&u.required&&f.$setValidity("required",!f.$error.required),s?e:undefined):n}function lt(){return l=!1,dt(),angular.isDefined(ht)?r.attr("placeholder",ht):r.removeAttr("placeholder"),angular.isDefined(ct)?r.attr("maxlength",ct):r.removeAttr("maxlength"),r.val(f.$modelValue),f.$viewValue=f.$modelValue,!1}function bt(){e=tt=p(f.$modelValue||"");g=nt=w(e);s=ut(e);var n=s&&e.length?g:"";u.maxlength&&r.attr("maxlength",o[o.length-1]*2);r.attr("placeholder",c);r.val(n);f.$viewValue=n}function kt(){v||(r.bind("blur",at),r.bind("mousedown mouseup",b),r.bind("input keyup click focus",a),v=!0)}function dt(){v&&(r.unbind("blur",at),r.unbind("mousedown",b),r.unbind("mouseup",b),r.unbind("input",a),r.unbind("keyup",a),r.unbind("click",a),r.unbind("focus",a),v=!1)}function ut(n){return n.length?n.length>=d:!0}function p(n){var i="",t=k.slice();return n=n.toString(),angular.forEach(st,function(t){n=n.replace(t,"")}),angular.forEach(n.split(""),function(n){t.length&&t[0].test(n)&&(i+=n,t.shift())}),i}function w(n){var t="",i=o.slice();return angular.forEach(c.split(""),function(r,u){n.length&&u===i[0]?(t+=n.charAt(0)||"_",n=n.substr(1),i.shift()):t+=r}),t}function gt(n){var t=u.placeholder;return typeof t!="undefined"&&t[n]?t[n]:"_"}function ni(){return c.replace(/[_]+/g,"_").replace(/([^_]+)([a-zA-Z0-9])([^_])/g,"$1$2_$3").split("_")}function ti(n){var t=0,i,r;o=[];k=[];c="";typeof n=="string"&&(d=0,i=!1,r=n.split(""),angular.forEach(r,function(n,r){h.maskDefinitions[n]?(o.push(t),c+=gt(r),k.push(h.maskDefinitions[n]),t++,i||d++):n==="?"?i=!0:(c+=n,t++)}));o.push(o.slice().pop()+1);st=ni();l=o.length>1?!0:!1}function at(){h.clearOnBlur&&(it=0,y=0);s&&e.length!==0||(h.clearOnBlur&&(g="",r.val("")),n.$apply(function(){f.$setViewValue("")}))}function b(n){n.type==="mousedown"?r.bind("mouseout",ft):r.unbind("mouseout",ft)}function ft(){y=ot(this);r.unbind("mouseout",ft)}function a(t){var u,s,g;if(t=t||{},u=t.which,s=t.type,u!==16&&u!==91){var c=r.val(),v=nt,b,e=p(c),lt=tt,rt=!1,i=ii(this)||0,ut=it||0,ft=i-ut,h=o[0],l=o[e.length]||o.slice().shift(),a=y||0,at=ot(this)>0,st=a>0,ht=c.length>v.length||a&&c.length>v.length-a,k=c.length<v.length||a&&c.length===v.length-a,vt=u>=37&&u<=40&&t.shiftKey,yt=u===37,ct=u===8||s!=="keyup"&&k&&ft===-1,pt=u===46||s!=="keyup"&&k&&ft===0&&!st,d=(yt||ct||s==="click")&&i>h;if(y=ot(this),!vt&&(!at||s!=="click"&&s!=="keyup")){if(s==="input"&&k&&!st&&e===lt){while(ct&&i>h&&!et(i))i--;while(pt&&i<l&&o.indexOf(i)===-1)i++;g=o.indexOf(i);e=e.substring(0,g)+e.substring(g+1);rt=!0}for(b=w(e),nt=b,tt=e,r.val(b),rt&&n.$apply(function(){f.$setViewValue(e)}),ht&&i<=h&&(i=h+1),d&&i--,i=i>l?l:i<h?h:i;!et(i)&&i>h&&i<l;)i+=d?-1:1;(d&&i<l||ht&&!et(ut))&&i++;it=i;ri(this,i)}}}function et(n){return o.indexOf(n)>-1}function ii(n){if(!n)return 0;if(n.selectionStart!==undefined)return n.selectionStart;if(document.selection){n.focus();var t=document.selection.createRange();return t.moveStart("character",n.value?-n.value.length:0),t.text.length}return 0}function ri(n,t){if(!n)return 0;if(n.offsetWidth!==0&&n.offsetHeight!==0)if(n.setSelectionRange)n.focus(),n.setSelectionRange(t,t);else if(n.createTextRange){var i=n.createTextRange();i.collapse(!0);i.moveEnd("character",t);i.moveStart("character",t);i.select()}}function ot(n){return n?n.selectionStart!==undefined?n.selectionEnd-n.selectionStart:document.selection?document.selection.createRange().text.length:0:0}var l=!1,v=!1,o,k,c,st,d,e,g,s,ht=u.placeholder,ct=u.maxlength,nt,tt,it,y,h={},rt;u.uiOptions?(h=n.$eval("["+u.uiOptions+"]"),angular.isObject(h[0])&&(h=function(n,t){for(var i in n)Object.prototype.hasOwnProperty.call(n,i)&&(t[i]===undefined?t[i]=angular.copy(n[i]):angular.extend(t[i],n[i]));return t}(i,h[0]))):h=i;u.$observe("uiMask",vt);u.$observe("placeholder",yt);rt=!1;u.$observe("modelViewValue",function(n){n==="true"&&(rt=!0)});n.$watch(u.ngModel,function(i){if(rt&&i){var r=t(u.ngModel);r.assign(n,f.$viewValue)}});f.$formatters.push(pt);f.$parsers.push(wt);r.bind("mousedown mouseup",b);Array.prototype.indexOf||(Array.prototype.indexOf=function(n){var u,r,t,i;if(this===null)throw new TypeError;if((u=Object(this),r=u.length>>>0,r===0)||(t=0,arguments.length>1&&(t=Number(arguments[1]),t!==t?t=0:t!==0&&t!==Infinity&&t!==-Infinity&&(t=(t>0||-1)*Math.floor(Math.abs(t)))),t>=r))return-1;for(i=t>=0?t:Math.max(r-Math.abs(t),0);i<r;i++)if(i in u&&u[i]===n)return i;return-1})}}}}]);angular.module("ui.reset",[]).value("uiResetConfig",null).directive("uiReset",["uiResetConfig",function(n){"use strict";var t=null;return n!==undefined&&(t=n),{require:"ngModel",link:function(n,i,r,u){var f;f=angular.element('<a class="ui-reset" />');i.wrap('<span class="ui-resetwrap" />').after(f);f.bind("click",function(i){i.preventDefault();n.$apply(function(){r.uiReset?u.$setViewValue(n.$eval(r.uiReset)):u.$setViewValue(t);u.$render()})})}}}]);angular.module("ui.route",[]).directive("uiRoute",["$location","$parse",function(n,t){"use strict";return{restrict:"AC",scope:!0,compile:function(i,r){var u;if(r.uiRoute)u="uiRoute";else if(r.ngHref)u="ngHref";else if(r.href)u="href";else throw new Error("uiRoute missing a route or href property on "+i[0]);return function(i,r,f){function o(t){var r=t.indexOf("#");r>-1&&(t=t.substr(r+1));e=function(){s(i,n.path().indexOf(t)>-1)};e()}function h(t){var r=t.indexOf("#");r>-1&&(t=t.substr(r+1));e=function(){var r=new RegExp("^"+t+"$",["i"]);s(i,r.test(n.path()))};e()}var s=t(f.ngModel||f.routeModel||"$uiRoute").assign,e=angular.noop;switch(u){case"uiRoute":f.uiRoute?h(f.uiRoute):f.$observe("uiRoute",h);break;case"ngHref":f.ngHref?o(f.ngHref):f.$observe("ngHref",o);break;case"href":o(f.href)}i.$on("$routeChangeSuccess",function(){e()});i.$on("$stateChangeSuccess",function(){e()})}}}}]);angular.module("ui.scroll.jqlite",["ui.scroll"]).service("jqLiteExtras",["$log","$window",function(n,t){"use strict";return{registerFor:function(n){var i,u,s,r,f,e,o;return u=angular.element.prototype.css,n.prototype.css=function(n,t){var i,r;return r=this,i=r[0],(!i||i.nodeType===3||i.nodeType===8||!i.style)?void 0:u.call(r,n,t)},e=function(n){return n&&n.document&&n.location&&n.alert&&n.setInterval},o=function(n,t,i){var r,u,s,o,f;return r=n[0],f={top:["scrollTop","pageYOffset","scrollLeft"],left:["scrollLeft","pageXOffset","scrollTop"]}[t],u=f[0],o=f[1],s=f[2],e(r)?angular.isDefined(i)?r.scrollTo(n[s].call(n),i):o in r?r[o]:r.document.documentElement[u]:angular.isDefined(i)?r[u]=i:r[u]},t.getComputedStyle?(r=function(n){return t.getComputedStyle(n,null)},i=function(n,t){return parseFloat(t)}):(r=function(n){return n.currentStyle},i=function(n,t){var f,e,o,s,i,u,r;return f=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,s=new RegExp("^("+f+")(?!px)[a-z%]+$","i"),s.test(t)?(r=n.style,e=r.left,i=n.runtimeStyle,u=i&&i.left,i&&(i.left=r.left),r.left=t,o=r.pixelLeft,r.left=e,u&&(i.left=u),o):parseFloat(t)}),s=function(n,t){var f,c,l,a,v,u,o,s,y,p,w,b,h;return e(n)?(f=document.documentElement[{height:"clientHeight",width:"clientWidth"}[t]],{base:f,padding:0,border:0,margin:0}):(h={width:[n.offsetWidth,"Left","Right"],height:[n.offsetHeight,"Top","Bottom"]}[t],f=h[0],o=h[1],s=h[2],u=r(n),w=i(n,u["padding"+o])||0,b=i(n,u["padding"+s])||0,c=i(n,u["border"+o+"Width"])||0,l=i(n,u["border"+s+"Width"])||0,a=u["margin"+o],v=u["margin"+s],y=i(n,a)||0,p=i(n,v)||0,{base:f,padding:w+b,border:c+l,margin:y+p})},f=function(n,t,i){var e,u,f;return u=s(n,t),u.base>0?{base:u.base-u.padding-u.border,outer:u.base,outerfull:u.base+u.margin}[i]:(e=r(n),f=e[t],(f<0||f===null)&&(f=n.style[t]||0),f=parseFloat(f)||0,{base:f-u.padding-u.border,outer:f,outerfull:f+u.padding+u.border+u.margin}[i])},angular.forEach({before:function(n){var t,u,f,e,o,i,r;if(o=this,u=o[0],e=o.parent(),t=e.contents(),t[0]===u)return e.prepend(n);for(f=i=1,r=t.length-1;1<=r?i<=r:i>=r;f=1<=r?++i:--i)if(t[f]===u){angular.element(t[f-1]).after(n);return}throw new Error("invalid DOM structure "+u.outerHTML);},height:function(n){var t;return t=this,angular.isDefined(n)?(angular.isNumber(n)&&(n=n+"px"),u.call(t,"height",n)):f(this[0],"height","base")},outerHeight:function(n){return f(this[0],"height",n?"outerfull":"outer")},offset:function(n){var u,t,i,r,f,e;if(f=this,arguments.length){if(n===void 0)return f;throw new Error("offset setter method is not implemented");}if(u={top:0,left:0},r=f[0],t=r&&r.ownerDocument,t)return i=t.documentElement,r.getBoundingClientRect!=null&&(u=r.getBoundingClientRect()),e=t.defaultView||t.parentWindow,{top:u.top+(e.pageYOffset||i.scrollTop)-(i.clientTop||0),left:u.left+(e.pageXOffset||i.scrollLeft)-(i.clientLeft||0)}},scrollTop:function(n){return o(this,"top",n)},scrollLeft:function(n){return o(this,"left",n)}},function(t,i){if(!n.prototype[i])return n.prototype[i]=t})}}}]).run(["$log","$window","jqLiteExtras",function(n,t,i){"use strict";if(!t.jQuery)return i.registerFor(angular.element)}]);angular.module("ui.scroll",[]).directive("uiScrollViewport",["$log",function(){"use strict";return{controller:["$scope","$element",function(n,t){return this.viewport=t,this}]}}]).directive("uiScroll",["$log","$injector","$rootScope","$timeout",function(n,t,i,r){"use strict";return{require:["?^uiScrollViewport"],transclude:"element",priority:1e3,terminal:!0,compile:function(u,f,e){return function(u,f,o,s){var c,p,oi,b,ot,h,tt,k,si,hi,v,st,vt,yt,d,it,pt,rt,a,wt,bt,ht,kt,g,ct,dt,lt,at,y,w,ci,nt,gt,ut,ni,li,ti,ii,ai,vi,yi,ri,ui,ft,fi,l,et,ei;if(lt=n.debug||n.log,at=o.uiScroll.match(/^\s*(\w+)\s+in\s+([\w\.]+)\s*$/),!at)throw new Error("Expected uiScroll in form of '_item_ in _datasource_' but got '"+o.uiScroll+"'");if(ct=at[1],st=at[2],kt=function(n){return angular.isObject(n)&&n.get&&angular.isFunction(n.get)},wt=function(n,t){var i;return n?(i=t.match(/^([\w]+)\.(.+)$/),!i||i.length!==3)?n[t]:wt(n[i[1]],i[2]):null},v=wt(u,st),!kt(v)&&(v=t.get(st),!kt(v)))throw new Error(""+st+" is not a valid datasource");return k=Math.max(3,+o.bufferSize||10),tt=function(){return l.outerHeight()*Math.max(.1,+o.padding||.1)},li=function(n){var t;return(t=n[0].scrollHeight)!=null?t:n[0].document.documentElement.scrollHeight},c=null,e(vi=u.$new(),function(n){var i,e,o,t,r,u;if(t=n[0].localName,t==="dl")throw new Error("ui-scroll directive does not support <"+n[0].localName+"> as a repeating tag: "+n[0].outerHTML);return t!=="li"&&t!=="tr"&&(t="div"),u=s[0]&&s[0].viewport?s[0].viewport:angular.element(window),u.css({"overflow-y":"auto",display:"block"}),o=function(n){var i,t,r;switch(n){case"tr":return r=angular.element("<table><tr><td><div><\/div><\/td><\/tr><\/table>"),i=r.find("div"),t=r.find("tr"),t.paddingHeight=function(){return i.height.apply(i,arguments)},t;default:return t=angular.element("<"+n+"><\/"+n+">"),t.paddingHeight=t.height,t}},e=function(n,t,i){return t[{top:"before",bottom:"after"}[i]](n),{paddingHeight:function(){return n.paddingHeight.apply(n,arguments)},insert:function(t){return n[{top:"after",bottom:"before"}[i]](t)}}},r=e(o(t),f,"top"),i=e(o(t),f,"bottom"),vi.$destroy(),c={viewport:u,topPadding:r.paddingHeight,bottomPadding:i.paddingHeight,append:i.insert,prepend:r.insert,bottomDataPos:function(){return li(u)-i.paddingHeight()},topDataPos:function(){return r.paddingHeight()}}}),l=c.viewport,et=l.scope()||i,angular.isDefined(o.topVisible)&&(ui=function(n){return et[o.topVisible]=n}),angular.isDefined(o.topVisibleElement)&&(ri=function(n){return et[o.topVisibleElement]=n}),angular.isDefined(o.topVisibleScope)&&(fi=function(n){return et[o.topVisibleScope]=n}),yi=function(n){return ui&&ui(n.scope[ct]),ri&&ri(n.element),fi&&fi(n.scope),v.topVisible?v.topVisible(n):void 0},dt=angular.isDefined(o.isLoading)?function(n){return et[o.isLoading]=n,v.loading?v.loading(n):void 0}:function(n){if(v.loading)return v.loading(n)},ut=0,a=1,y=1,h=[],w=[],d=!1,b=!1,g=!1,nt=function(n,t){for(var i,r=i=n;n<=t?i<t:i>t;r=n<=t?++i:--i)h[r].scope.$destroy(),h[r].element.remove();return h.splice(n,t-n)},ci=function(){return ut++,a=1,y=1,nt(0,h.length),c.topPadding(0),c.bottomPadding(0),w=[],d=!1,b=!1,p(ut,!1)},ot=function(){return l.scrollTop()+l.outerHeight()},ft=function(){return l.scrollTop()},ti=function(){return!d&&c.bottomDataPos()<ot()+tt()},si=function(){var i,u,f,e,o,r,n,l,t,s;for(i=0,n=0,u=t=s=h.length-1;s<=0?t<=0:t>=0;u=s<=0?++t:--t)if(f=h[u],o=f.element.offset().top,r=l!==o,l=o,r&&(e=f.element.outerHeight(!0)),c.bottomDataPos()-i-e>ot()+tt())r&&(i+=e),n++,d=!1;else{if(r)break;n++}if(n>0)return c.bottomPadding(c.bottomPadding()+i),nt(h.length-n,h.length),y-=n,lt("clipped off bottom "+n+" bottom padding "+c.bottomPadding())},ii=function(){return!b&&c.topDataPos()>ft()-tt()},hi=function(){var u,f,e,t,n,o,i,r,s;for(i=0,n=0,r=0,s=h.length;r<s;r++)if(u=h[r],e=u.element.offset().top,t=o!==e,o=e,t&&(f=u.element.outerHeight(!0)),c.topDataPos()+i+f<ft()-tt())t&&(i+=f),n++,b=!1;else{if(t)break;n++}if(n>0)return c.topPadding(c.topPadding()+i),nt(0,n),a+=n,lt("clipped off top "+n+" top padding "+c.topPadding())},yt=function(n,t,i){return g||(g=!0,dt(!0)),w.push(t)===1?pt(n,i):void 0},bt=function(n){return n.displayTemp=n.css("display"),n.css("display","none")},ai=function(n){if(n.hasOwnProperty("displayTemp"))return n.css("display",n.displayTemp)},ht=function(n,t){var i,f,r;return i=u.$new(),i[ct]=t,f=n>a,i.$index=n,f&&i.$index--,r={scope:i},e(i,function(t){return r.element=t,f?n===y?(bt(t),c.append(t),h.push(r)):(h[n-a].element.after(t),h.splice(n-a+1,0,r)):(bt(t),c.prepend(t),h.unshift(r))}),{appended:f,wrapper:r}},oi=function(n,t){var i;return n?c.bottomPadding(Math.max(0,c.bottomPadding()-t.element.outerHeight(!0))):(i=c.topPadding()-t.element.outerHeight(!0),i>=0?c.topPadding(i):l.scrollTop(l.scrollTop()+t.element.outerHeight(!0)))},vt=function(n,t,i){var r,e,o,u,a,s,f,v,l;if(lt("top {actual="+c.topDataPos()+" visible from="+ft()+" bottom {visible through="+ot()+" actual="+c.bottomDataPos()+"}"),ti()?yt(n,!0,t):ii()&&yt(n,!1,t),i&&i(n),w.length===0){for(s=0,l=[],f=0,v=h.length;f<v;f++)if(r=h[f],o=r.element.offset().top,u=a!==o,a=o,u&&(e=r.element.outerHeight(!0)),u&&c.topDataPos()+s+e<ft())l.push(s+=e);else{u&&yi(r);break}return l}},p=function(n,t,i,u){return i&&i.length?r(function(){var h,r,c,e,o,s,l,a;for(e=[],o=0,l=i.length;o<l;o++)r=i[o],f=r.wrapper.element,ai(f),h=f.offset().top,c!==h&&(e.push(r),c=h);for(s=0,a=e.length;s<a;s++)r=e[s],oi(r.appended,r.wrapper);return vt(n,t,u)}):vt(n,t,u)},rt=function(n,t,i){return p(n,t,i,function(){return w.shift(),w.length===0?(g=!1,dt(!1)):pt(n,t)})},pt=function(n,t){var i;return i=w[0],i?h.length&&!ti()?rt(n,t):v.get(y,k,function(i){var f,u,r,e;if(!n||n===ut){if(u=[],i.length<k&&(d=!0,c.bottomPadding(0)),i.length>0)for(hi(),r=0,e=i.length;r<e;r++)f=i[r],u.push(ht(++y,f));return rt(n,t,u)}}):h.length&&!ii()?rt(n,t):v.get(a-k,k,function(i){var u,f,r,e;if(!n||n===ut){if(f=[],i.length<k&&(b=!0,c.topPadding(0)),i.length>0)for(h.length&&si(),u=r=e=i.length-1;e<=0?r<=0:r>=0;u=e<=0?++r:--r)f.unshift(ht(--a,i[u]));return rt(n,t,f)}})},gt=function(){if(!i.$$phase&&!g)return p(null,!1),u.$apply()},l.bind("resize",gt),ni=function(){if(!i.$$phase&&!g)return p(null,!0),u.$apply()},l.bind("scroll",ni),ei=function(n){var t,i;return t=l[0].scrollTop,i=l[0].scrollHeight-l[0].clientHeight,t===0&&!b||t===i&&!d?n.preventDefault():void 0},l.bind("mousewheel",ei),u.$watch(v.revision,function(){return ci()}),it=v.scope?v.scope.$new():u.$new(),u.$on("$destroy",function(){return it.$destroy(),l.unbind("resize",gt),l.unbind("scroll",ni),l.unbind("mousewheel",ei)}),it.$on("update.items",function(n,t,i){var u,f,r,e,o;if(angular.isFunction(t))for(f=function(n){return t(n.scope)},r=0,e=h.length;r<e;r++)u=h[r],f(u);else 0<=(o=t-a-1)&&o<h.length&&(h[t-a-1].scope[ct]=i);return null}),it.$on("delete.items",function(n,t){var i,u,r,s,c,f,e,o,l,v,w,b;if(angular.isFunction(t)){for(r=[],f=0,l=h.length;f<l;f++)u=h[f],r.unshift(u);for(c=function(n){if(t(n.scope))return nt(r.length-1-i,r.length-i),y--},i=e=0,v=r.length;e<v;i=++e)s=r[i],c(s)}else 0<=(b=t-a-1)&&b<h.length&&(nt(t-a-1,t-a),y--);for(i=o=0,w=h.length;o<w;i=++o)u=h[i],u.scope.$index=a+i;return p(null,!1)}),it.$on("insert.item",function(n,t,i){var r,u,f,e,o;if(u=[],angular.isFunction(t))throw new Error("not implemented - Insert with locator function");else 0<=(o=t-a-1)&&o<h.length&&(u.push(ht(t,i)),y++);for(r=f=0,e=h.length;f<e;r=++f)i=h[r],i.scope.$index=a+r;return p(null,!1,u)})}}}}]);angular.module("ui.scrollfix",[]).directive("uiScrollfix",["$window",function(n){"use strict";function t(){if(angular.isDefined(n.pageYOffset))return n.pageYOffset;var t=document.compatMode&&document.compatMode!=="BackCompat"?document.documentElement:document.body;return t.scrollTop}return{require:"^?uiScrollfixTarget",link:function(i,r,u,f){function s(){var n=f?o[0].scrollTop:t();!r.hasClass("ui-scrollfix")&&n>u.uiScrollfix?r.addClass("ui-scrollfix"):r.hasClass("ui-scrollfix")&&n<u.uiScrollfix&&r.removeClass("ui-scrollfix")}var e=r[0].offsetTop,o=f&&f.$element||angular.element(n);u.uiScrollfix?typeof u.uiScrollfix=="string"&&(u.uiScrollfix.charAt(0)==="-"?u.uiScrollfix=e-parseFloat(u.uiScrollfix.substr(1)):u.uiScrollfix.charAt(0)==="+"&&(u.uiScrollfix=e+parseFloat(u.uiScrollfix.substr(1)))):u.uiScrollfix=e;o.on("scroll",s);i.$on("$destroy",function(){o.off("scroll",s)})}}}]).directive("uiScrollfixTarget",[function(){"use strict";return{controller:["$element",function(n){this.$element=n}]}}]);angular.module("ui.showhide",[]).directive("uiShow",[function(){"use strict";return function(n,t,i){n.$watch(i.uiShow,function(n){n?t.addClass("ui-show"):t.removeClass("ui-show")})}}]).directive("uiHide",[function(){"use strict";return function(n,t,i){n.$watch(i.uiHide,function(n){n?t.addClass("ui-hide"):t.removeClass("ui-hide")})}}]).directive("uiToggle",[function(){"use strict";return function(n,t,i){n.$watch(i.uiToggle,function(n){n?t.removeClass("ui-hide").addClass("ui-show"):t.removeClass("ui-show").addClass("ui-hide")})}}]);angular.module("ui.unique",[]).filter("unique",["$parse",function(n){"use strict";return function(t,i){if(i===!1)return t;if((i||angular.isUndefined(i))&&angular.isArray(t)){var r=[],f=angular.isString(i)?n(i):function(n){return n},u=function(n){return angular.isObject(n)?f(n):n};angular.forEach(t,function(n){for(var i=!1,t=0;t<r.length;t++)if(angular.equals(u(r[t]),u(n))){i=!0;break}i||r.push(n)});t=r}return t}}]);angular.module("ui.uploader",[]).service("uiUploader",uiUploader);uiUploader.$inject=["$log"];angular.module("ui.validate",[]).directive("uiValidate",function(){"use strict";return{restrict:"A",require:"ngModel",link:function(n,t,i,r){function o(t){if(angular.isString(t)){n.$watch(t,function(){angular.forEach(u,function(n){n(r.$modelValue)})});return}if(angular.isArray(t)){angular.forEach(t,function(t){n.$watch(t,function(){angular.forEach(u,function(n){n(r.$modelValue)})})});return}angular.isObject(t)&&angular.forEach(t,function(t,i){angular.isString(t)&&n.$watch(t,function(){u[i](r.$modelValue)});angular.isArray(t)&&angular.forEach(t,function(t){n.$watch(t,function(){u[i](r.$modelValue)})})})}var e,u={},f=n.$eval(i.uiValidate);f&&(angular.isString(f)&&(f={validator:f}),angular.forEach(f,function(t,i){e=function(u){var f=n.$eval(t,{$value:u});return angular.isObject(f)&&angular.isFunction(f.then)?(f.then(function(){r.$setValidity(i,!0)},function(){r.$setValidity(i,!1)}),u):f?(r.$setValidity(i,!0),u):(r.$setValidity(i,!1),u)};u[i]=e;r.$formatters.push(e);r.$parsers.push(e)}),i.uiValidateWatch&&o(n.$eval(i.uiValidateWatch)))}}});angular.module("ui.utils",["ui.event","ui.format","ui.highlight","ui.include","ui.indeterminate","ui.inflector","ui.jq","ui.keypress","ui.mask","ui.reset","ui.route","ui.scrollfix","ui.scroll","ui.scroll.jqlite","ui.showhide","ui.unique","ui.validate"]);
/*
//# sourceMappingURL=ui-utils.min.js.map
*/
///#source 1 1 /assets/js/Bootstrap/ui-bootstrap-tpls-0.11.0.js
angular.module("ui.bootstrap", ["ui.bootstrap.tpls", "ui.bootstrap.transition", "ui.bootstrap.collapse", "ui.bootstrap.accordion", "ui.bootstrap.alert", "ui.bootstrap.bindHtml", "ui.bootstrap.buttons", "ui.bootstrap.carousel", "ui.bootstrap.position", "ui.bootstrap.datepicker", "ui.bootstrap.dropdownToggle", "ui.bootstrap.modal", "ui.bootstrap.pagination", "ui.bootstrap.tooltip", "ui.bootstrap.popover", "ui.bootstrap.progressbar", "ui.bootstrap.rating", "ui.bootstrap.tabs", "ui.bootstrap.timepicker", "ui.bootstrap.typeahead"]);
angular.module("ui.bootstrap.tpls", ["template/accordion/accordion-group.html", "template/accordion/accordion.html", "template/alert/alert.html", "template/carousel/carousel.html", "template/carousel/slide.html", "template/datepicker/datepicker.html", "template/datepicker/popup.html", "template/modal/backdrop.html", "template/modal/window.html", "template/pagination/pager.html", "template/pagination/pagination.html", "template/tooltip/tooltip-html-unsafe-popup.html", "template/tooltip/tooltip-popup.html", "template/popover/popover.html", "template/progressbar/bar.html", "template/progressbar/progress.html", "template/rating/rating.html", "template/tabs/tab.html", "template/tabs/tabset-titles.html", "template/tabs/tabset.html", "template/timepicker/timepicker.html", "template/typeahead/typeahead-match.html", "template/typeahead/typeahead-popup.html"]);
angular.module('ui.bootstrap.transition', [])

/**
 * $transition service provides a consistent interface to trigger CSS 3 transitions and to be informed when they complete.
 * @param  {DOMElement} element  The DOMElement that will be animated.
 * @param  {string|object|function} trigger  The thing that will cause the transition to start:
 *   - As a string, it represents the css class to be added to the element.
 *   - As an object, it represents a hash of style attributes to be applied to the element.
 *   - As a function, it represents a function to be called that will cause the transition to occur.
 * @return {Promise}  A promise that is resolved when the transition finishes.
 */
.factory('$transition', ['$q', '$timeout', '$rootScope', function ($q, $timeout, $rootScope) {

    var $transition = function (element, trigger, options) {
        options = options || {};
        var deferred = $q.defer();
        var endEventName = $transition[options.animation ? "animationEndEventName" : "transitionEndEventName"];

        var transitionEndHandler = function (event) {
            $rootScope.$apply(function () {
                element.unbind(endEventName, transitionEndHandler);
                deferred.resolve(element);
            });
        };

        if (endEventName) {
            element.bind(endEventName, transitionEndHandler);
        }

        // Wrap in a timeout to allow the browser time to update the DOM before the transition is to occur
        $timeout(function () {
            if (angular.isString(trigger)) {
                element.addClass(trigger);
            } else if (angular.isFunction(trigger)) {
                trigger(element);
            } else if (angular.isObject(trigger)) {
                element.css(trigger);
            }
            //If browser does not support transitions, instantly resolve
            if (!endEventName) {
                deferred.resolve(element);
            }
        });

        // Add our custom cancel function to the promise that is returned
        // We can call this if we are about to run a new transition, which we know will prevent this transition from ending,
        // i.e. it will therefore never raise a transitionEnd event for that transition
        deferred.promise.cancel = function () {
            if (endEventName) {
                element.unbind(endEventName, transitionEndHandler);
            }
            deferred.reject('Transition cancelled');
        };

        return deferred.promise;
    };

    // Work out the name of the transitionEnd event
    var transElement = document.createElement('trans');
    var transitionEndEventNames = {
        'WebkitTransition': 'webkitTransitionEnd',
        'MozTransition': 'transitionend',
        'OTransition': 'oTransitionEnd',
        'transition': 'transitionend'
    };
    var animationEndEventNames = {
        'WebkitTransition': 'webkitAnimationEnd',
        'MozTransition': 'animationend',
        'OTransition': 'oAnimationEnd',
        'transition': 'animationend'
    };
    function findEndEventName(endEventNames) {
        for (var name in endEventNames) {
            if (transElement.style[name] !== undefined) {
                return endEventNames[name];
            }
        }
    }
    $transition.transitionEndEventName = findEndEventName(transitionEndEventNames);
    $transition.animationEndEventName = findEndEventName(animationEndEventNames);
    return $transition;
}]);

angular.module('ui.bootstrap.collapse', ['ui.bootstrap.transition'])

// The collapsible directive indicates a block of html that will expand and collapse
.directive('collapse', ['$transition', function ($transition) {
    // CSS transitions don't work with height: auto, so we have to manually change the height to a
    // specific value and then once the animation completes, we can reset the height to auto.
    // Unfortunately if you do this while the CSS transitions are specified (i.e. in the CSS class
    // "collapse") then you trigger a change to height 0 in between.
    // The fix is to remove the "collapse" CSS class while changing the height back to auto - phew!
    var fixUpHeight = function (scope, element, height) {
        // We remove the collapse CSS class to prevent a transition when we change to height: auto
        element.removeClass('collapse');
        element.css({ height: height });
        // It appears that  reading offsetWidth makes the browser realise that we have changed the
        // height already :-/
        var x = element[0].offsetWidth;
        element.addClass('collapse');
    };

    return {
        link: function (scope, element, attrs) {

            var isCollapsed;
            var initialAnimSkip = true;
            scope.$watch(function () { return element[0].scrollHeight; }, function (value) {
                //The listener is called when scollHeight changes
                //It actually does on 2 scenarios: 
                // 1. Parent is set to display none
                // 2. angular bindings inside are resolved
                //When we have a change of scrollHeight we are setting again the correct height if the group is opened
                if (element[0].scrollHeight !== 0) {
                    if (!isCollapsed) {
                        if (initialAnimSkip) {
                            fixUpHeight(scope, element, element[0].scrollHeight + 'px');
                        } else {
                            fixUpHeight(scope, element, 'auto');
                        }
                    }
                }
            });

            scope.$watch(attrs.collapse, function (value) {
                if (value) {
                    collapse();
                } else {
                    expand();
                }
            });


            var currentTransition;
            var doTransition = function (change) {
                if (currentTransition) {
                    currentTransition.cancel();
                }
                currentTransition = $transition(element, change);
                currentTransition.then(
                  function () { currentTransition = undefined; },
                  function () { currentTransition = undefined; }
                );
                return currentTransition;
            };

            var expand = function () {
                if (initialAnimSkip) {
                    initialAnimSkip = false;
                    if (!isCollapsed) {
                        fixUpHeight(scope, element, 'auto');
                    }
                } else {
                    doTransition({ height: element[0].scrollHeight + 'px' })
                    .then(function () {
                        // This check ensures that we don't accidentally update the height if the user has closed
                        // the group while the animation was still running
                        if (!isCollapsed) {
                            fixUpHeight(scope, element, 'auto');
                        }
                    });
                }
                isCollapsed = false;
            };

            var collapse = function () {
                isCollapsed = true;
                if (initialAnimSkip) {
                    initialAnimSkip = false;
                    fixUpHeight(scope, element, 0);
                } else {
                    fixUpHeight(scope, element, element[0].scrollHeight + 'px');
                    doTransition({ 'height': '0' });
                }
            };
        }
    };
}]);

angular.module('ui.bootstrap.accordion', ['ui.bootstrap.collapse'])

.constant('accordionConfig', {
    closeOthers: true
})

.controller('AccordionController', ['$scope', '$attrs', 'accordionConfig', function ($scope, $attrs, accordionConfig) {

    // This array keeps track of the accordion groups
    this.groups = [];

    // Ensure that all the groups in this accordion are closed, unless close-others explicitly says not to
    this.closeOthers = function (openGroup) {
        var closeOthers = angular.isDefined($attrs.closeOthers) ? $scope.$eval($attrs.closeOthers) : accordionConfig.closeOthers;
        if (closeOthers) {
            angular.forEach(this.groups, function (group) {
                if (group !== openGroup) {
                    group.isOpen = false;
                }
            });
        }
    };

    // This is called from the accordion-group directive to add itself to the accordion
    this.addGroup = function (groupScope) {
        var that = this;
        this.groups.push(groupScope);

        groupScope.$on('$destroy', function (event) {
            that.removeGroup(groupScope);
        });
    };

    // This is called from the accordion-group directive when to remove itself
    this.removeGroup = function (group) {
        var index = this.groups.indexOf(group);
        if (index !== -1) {
            this.groups.splice(this.groups.indexOf(group), 1);
        }
    };

}])

// The accordion directive simply sets up the directive controller
// and adds an accordion CSS class to itself element.
.directive('accordion', function () {
    return {
        restrict: 'EA',
        controller: 'AccordionController',
        transclude: true,
        replace: false,
        templateUrl: 'template/accordion/accordion.html'
    };
})

// The accordion-group directive indicates a block of html that will expand and collapse in an accordion
.directive('accordionGroup', ['$parse', '$transition', '$timeout', function ($parse, $transition, $timeout) {
    return {
        require: '^accordion',         // We need this directive to be inside an accordion
        restrict: 'EA',
        transclude: true,              // It transcludes the contents of the directive into the template
        replace: true,                // The element containing the directive will be replaced with the template
        templateUrl: 'template/accordion/accordion-group.html',
        scope: { heading: '@' },        // Create an isolated scope and interpolate the heading attribute onto this scope
        controller: ['$scope', function ($scope) {
            this.setHeading = function (element) {
                this.heading = element;
            };
        }],
        link: function (scope, element, attrs, accordionCtrl) {
            var getIsOpen, setIsOpen;

            accordionCtrl.addGroup(scope);

            scope.isOpen = false;

            if (attrs.isOpen) {
                getIsOpen = $parse(attrs.isOpen);
                setIsOpen = getIsOpen.assign;

                scope.$watch(
                  function watchIsOpen() { return getIsOpen(scope.$parent); },
                  function updateOpen(value) { scope.isOpen = value; }
                );

                scope.isOpen = getIsOpen ? getIsOpen(scope.$parent) : false;
            }

            scope.$watch('isOpen', function (value) {
                if (value) {
                    accordionCtrl.closeOthers(scope);
                }
                if (setIsOpen) {
                    setIsOpen(scope.$parent, value);
                }
            });
        }
    };
}])

// Use accordion-heading below an accordion-group to provide a heading containing HTML
// <accordion-group>
//   <accordion-heading>Heading containing HTML - <img src="..."></accordion-heading>
// </accordion-group>
.directive('accordionHeading', function () {
    return {
        restrict: 'EA',
        transclude: true,   // Grab the contents to be used as the heading
        template: '',       // In effect remove this element!
        replace: true,
        require: '^accordionGroup',
        compile: function (element, attr, transclude) {
            return function link(scope, element, attr, accordionGroupCtrl) {
                // Pass the heading to the accordion-group controller
                // so that it can be transcluded into the right place in the template
                // [The second parameter to transclude causes the elements to be cloned so that they work in ng-repeat]
                accordionGroupCtrl.setHeading(transclude(scope, function () { }));
            };
        }
    };
})

// Use in the accordion-group template to indicate where you want the heading to be transcluded
// You must provide the property on the accordion-group controller that will hold the transcluded element
// <div class="accordion-group">
//   <div class="accordion-heading" ><a ... accordion-transclude="heading">...</a></div>
//   ...
// </div>
.directive('accordionTransclude', function () {
    return {
        require: '^accordionGroup',
        link: function (scope, element, attr, controller) {
            scope.$watch(function () { return controller[attr.accordionTransclude]; }, function (heading) {
                if (heading) {
                    element.html('');
                    element.append(heading);
                }
            });
        }
    };
});

angular.module("ui.bootstrap.alert", []).directive('alert', function () {
    return {
        restrict: 'EA',
        templateUrl: 'template/alert/alert.html',
        transclude: true,
        replace: true,
        scope: {
            type: '=',
            close: '&'
        },
        link: function (scope, iElement, iAttrs, controller) {
            scope.closeable = "close" in iAttrs;
        }
    };
});

angular.module('ui.bootstrap.bindHtml', [])

  .directive('bindHtmlUnsafe', function () {
      return function (scope, element, attr) {
          element.addClass('ng-binding').data('$binding', attr.bindHtmlUnsafe);
          scope.$watch(attr.bindHtmlUnsafe, function bindHtmlUnsafeWatchAction(value) {
              element.html(value || '');
          });
      };
  });
angular.module('ui.bootstrap.buttons', [])

  .constant('buttonConfig', {
      activeClass: 'active',
      toggleEvent: 'click'
  })

  .directive('btnRadio', ['buttonConfig', function (buttonConfig) {
      var activeClass = buttonConfig.activeClass || 'active';
      var toggleEvent = buttonConfig.toggleEvent || 'click';

      return {

          require: 'ngModel',
          link: function (scope, element, attrs, ngModelCtrl) {

              //model -> UI
              ngModelCtrl.$render = function () {
                  element.toggleClass(activeClass, angular.equals(ngModelCtrl.$modelValue, scope.$eval(attrs.btnRadio)));
              };

              //ui->model
              element.bind(toggleEvent, function () {
                  if (!element.hasClass(activeClass)) {
                      scope.$apply(function () {
                          ngModelCtrl.$setViewValue(scope.$eval(attrs.btnRadio));
                          ngModelCtrl.$render();
                      });
                  }
              });
          }
      };
  }])

  .directive('btnCheckbox', ['buttonConfig', function (buttonConfig) {

      var activeClass = buttonConfig.activeClass || 'active';
      var toggleEvent = buttonConfig.toggleEvent || 'click';

      return {
          require: 'ngModel',
          link: function (scope, element, attrs, ngModelCtrl) {

              function getTrueValue() {
                  var trueValue = scope.$eval(attrs.btnCheckboxTrue);
                  return angular.isDefined(trueValue) ? trueValue : true;
              }

              function getFalseValue() {
                  var falseValue = scope.$eval(attrs.btnCheckboxFalse);
                  return angular.isDefined(falseValue) ? falseValue : false;
              }

              //model -> UI
              ngModelCtrl.$render = function () {
                  element.toggleClass(activeClass, angular.equals(ngModelCtrl.$modelValue, getTrueValue()));
              };

              //ui->model
              element.bind(toggleEvent, function () {
                  scope.$apply(function () {
                      ngModelCtrl.$setViewValue(element.hasClass(activeClass) ? getFalseValue() : getTrueValue());
                      ngModelCtrl.$render();
                  });
              });
          }
      };
  }]);
/**
* @ngdoc overview
* @name ui.bootstrap.carousel
*
* @description
* AngularJS version of an image carousel.
*
*/
angular.module('ui.bootstrap.carousel', ['ui.bootstrap.transition'])
.controller('CarouselController', ['$scope', '$timeout', '$transition', '$q', function ($scope, $timeout, $transition, $q) {
    var self = this,
      slides = self.slides = [],
      currentIndex = -1,
      currentTimeout, isPlaying;
    self.currentSlide = null;

    /* direction: "prev" or "next" */
    self.select = function (nextSlide, direction) {
        var nextIndex = slides.indexOf(nextSlide);
        //Decide direction if it's not given
        if (direction === undefined) {
            direction = nextIndex > currentIndex ? "next" : "prev";
        }
        if (nextSlide && nextSlide !== self.currentSlide) {
            if ($scope.$currentTransition) {
                $scope.$currentTransition.cancel();
                //Timeout so ng-class in template has time to fix classes for finished slide
                $timeout(goNext);
            } else {
                goNext();
            }
        }
        function goNext() {
            //If we have a slide to transition from and we have a transition type and we're allowed, go
            if (self.currentSlide && angular.isString(direction) && !$scope.noTransition && nextSlide.$element) {
                //We shouldn't do class manip in here, but it's the same weird thing bootstrap does. need to fix sometime
                nextSlide.$element.addClass(direction);
                var reflow = nextSlide.$element[0].offsetWidth; //force reflow

                //Set all other slides to stop doing their stuff for the new transition
                angular.forEach(slides, function (slide) {
                    angular.extend(slide, { direction: '', entering: false, leaving: false, active: false });
                });
                angular.extend(nextSlide, { direction: direction, active: true, entering: true });
                angular.extend(self.currentSlide || {}, { direction: direction, leaving: true });

                $scope.$currentTransition = $transition(nextSlide.$element, {});
                //We have to create new pointers inside a closure since next & current will change
                (function (next, current) {
                    $scope.$currentTransition.then(
                      function () { transitionDone(next, current); },
                      function () { transitionDone(next, current); }
                    );
                }(nextSlide, self.currentSlide));
            } else {
                transitionDone(nextSlide, self.currentSlide);
            }
            self.currentSlide = nextSlide;
            currentIndex = nextIndex;
            //every time you change slides, reset the timer
            restartTimer();
        }
        function transitionDone(next, current) {
            angular.extend(next, { direction: '', active: true, leaving: false, entering: false });
            angular.extend(current || {}, { direction: '', active: false, leaving: false, entering: false });
            $scope.$currentTransition = null;
        }
    };

    /* Allow outside people to call indexOf on slides array */
    self.indexOfSlide = function (slide) {
        return slides.indexOf(slide);
    };

    $scope.next = function () {
        var newIndex = (currentIndex + 1) % slides.length;

        //Prevent this user-triggered transition from occurring if there is already one in progress
        if (!$scope.$currentTransition) {
            return self.select(slides[newIndex], 'next');
        }
    };

    $scope.prev = function () {
        var newIndex = currentIndex - 1 < 0 ? slides.length - 1 : currentIndex - 1;

        //Prevent this user-triggered transition from occurring if there is already one in progress
        if (!$scope.$currentTransition) {
            return self.select(slides[newIndex], 'prev');
        }
    };

    $scope.select = function (slide) {
        self.select(slide);
    };

    $scope.isActive = function (slide) {
        return self.currentSlide === slide;
    };

    $scope.slides = function () {
        return slides;
    };

    $scope.$watch('interval', restartTimer);
    function restartTimer() {
        if (currentTimeout) {
            $timeout.cancel(currentTimeout);
        }
        function go() {
            if (isPlaying) {
                $scope.next();
                restartTimer();
            } else {
                $scope.pause();
            }
        }
        var interval = +$scope.interval;
        if (!isNaN(interval) && interval >= 0) {
            currentTimeout = $timeout(go, interval);
        }
    }
    $scope.play = function () {
        if (!isPlaying) {
            isPlaying = true;
            restartTimer();
        }
    };
    $scope.pause = function () {
        if (!$scope.noPause) {
            isPlaying = false;
            if (currentTimeout) {
                $timeout.cancel(currentTimeout);
            }
        }
    };

    self.addSlide = function (slide, element) {
        slide.$element = element;
        slides.push(slide);
        //if this is the first slide or the slide is set to active, select it
        if (slides.length === 1 || slide.active) {
            self.select(slides[slides.length - 1]);
            if (slides.length == 1) {
                $scope.play();
            }
        } else {
            slide.active = false;
        }
    };

    self.removeSlide = function (slide) {
        //get the index of the slide inside the carousel
        var index = slides.indexOf(slide);
        slides.splice(index, 1);
        if (slides.length > 0 && slide.active) {
            if (index >= slides.length) {
                self.select(slides[index - 1]);
            } else {
                self.select(slides[index]);
            }
        } else if (currentIndex > index) {
            currentIndex--;
        }
    };
}])

/**
 * @ngdoc directive
 * @name ui.bootstrap.carousel.directive:carousel
 * @restrict EA
 *
 * @description
 * Carousel is the outer container for a set of image 'slides' to showcase.
 *
 * @param {number=} interval The time, in milliseconds, that it will take the carousel to go to the next slide.
 * @param {boolean=} noTransition Whether to disable transitions on the carousel.
 * @param {boolean=} noPause Whether to disable pausing on the carousel (by default, the carousel interval pauses on hover).
 *
 * @example
<example module="ui.bootstrap">
  <file name="index.html">
    <carousel>
      <slide>
        <img src="http://placekitten.com/150/150" style="margin:auto;">
        <div class="carousel-caption">
          <p>Beautiful!</p>
        </div>
      </slide>
      <slide>
        <img src="http://placekitten.com/100/150" style="margin:auto;">
        <div class="carousel-caption">
          <p>D'aww!</p>
        </div>
      </slide>
    </carousel>
  </file>
  <file name="demo.css">
    .carousel-indicators {
      top: auto;
      bottom: 15px;
    }
  </file>
</example>
 */
.directive('carousel', [function () {
    return {
        restrict: 'EA',
        transclude: true,
        replace: true,
        controller: 'CarouselController',
        require: 'carousel',
        templateUrl: 'template/carousel/carousel.html',
        scope: {
            interval: '=',
            noTransition: '=',
            noPause: '='
        }
    };
}])

/**
 * @ngdoc directive
 * @name ui.bootstrap.carousel.directive:slide
 * @restrict EA
 *
 * @description
 * Creates a slide inside a {@link ui.bootstrap.carousel.directive:carousel carousel}.  Must be placed as a child of a carousel element.
 *
 * @param {boolean=} active Model binding, whether or not this slide is currently active.
 *
 * @example
<example module="ui.bootstrap">
  <file name="index.html">
<div ng-controller="CarouselDemoCtrl">
  <carousel>
    <slide ng-repeat="slide in slides" active="slide.active">
      <img ng-src="{{slide.image}}" style="margin:auto;">
      <div class="carousel-caption">
        <h4>Slide {{$index}}</h4>
        <p>{{slide.text}}</p>
      </div>
    </slide>
  </carousel>
  <div class="row-fluid">
    <div class="span6">
      <ul>
        <li ng-repeat="slide in slides">
          <button class="btn btn-mini" ng-class="{'btn-info': !slide.active, 'btn-success': slide.active}" ng-disabled="slide.active" ng-click="slide.active = true">select</button>
          {{$index}}: {{slide.text}}
        </li>
      </ul>
      <a class="btn" ng-click="addSlide()">Add Slide</a>
    </div>
    <div class="span6">
      Interval, in milliseconds: <input type="number" ng-model="myInterval">
      <br />Enter a negative number to stop the interval.
    </div>
  </div>
</div>
  </file>
  <file name="script.js">
function CarouselDemoCtrl($scope) {
  $scope.myInterval = 5000;
  var slides = $scope.slides = [];
  $scope.addSlide = function() {
    var newWidth = 200 + ((slides.length + (25 * slides.length)) % 150);
    slides.push({
      image: 'http://placekitten.com/' + newWidth + '/200',
      text: ['More','Extra','Lots of','Surplus'][slides.length % 4] + ' '
        ['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
    });
  };
  for (var i=0; i<4; i++) $scope.addSlide();
}
  </file>
  <file name="demo.css">
    .carousel-indicators {
      top: auto;
      bottom: 15px;
    }
  </file>
</example>
*/

.directive('slide', ['$parse', function ($parse) {
    return {
        require: '^carousel',
        restrict: 'EA',
        transclude: true,
        replace: true,
        templateUrl: 'template/carousel/slide.html',
        scope: {
        },
        link: function (scope, element, attrs, carouselCtrl) {
            //Set up optional 'active' = binding
            if (attrs.active) {
                var getActive = $parse(attrs.active);
                var setActive = getActive.assign;
                var lastValue = scope.active = getActive(scope.$parent);
                scope.$watch(function parentActiveWatch() {
                    var parentActive = getActive(scope.$parent);

                    if (parentActive !== scope.active) {
                        // we are out of sync and need to copy
                        if (parentActive !== lastValue) {
                            // parent changed and it has precedence
                            lastValue = scope.active = parentActive;
                        } else {
                            // if the parent can be assigned then do so
                            setActive(scope.$parent, parentActive = lastValue = scope.active);
                        }
                    }
                    return parentActive;
                });
            }

            carouselCtrl.addSlide(scope, element);
            //when the scope is destroyed then remove the slide from the current slides array
            scope.$on('$destroy', function () {
                carouselCtrl.removeSlide(scope);
            });

            scope.$watch('active', function (active) {
                if (active) {
                    carouselCtrl.select(scope);
                }
            });
        }
    };
}]);

angular.module('ui.bootstrap.position', [])

/**
 * A set of utility methods that can be use to retrieve position of DOM elements.
 * It is meant to be used where we need to absolute-position DOM elements in
 * relation to other, existing elements (this is the case for tooltips, popovers,
 * typeahead suggestions etc.).
 */
  .factory('$position', ['$document', '$window', function ($document, $window) {

      function getStyle(el, cssprop) {
          if (el.currentStyle) { //IE
              return el.currentStyle[cssprop];
          } else if ($window.getComputedStyle) {
              return $window.getComputedStyle(el)[cssprop];
          }
          // finally try and get inline style
          return el.style[cssprop];
      }

      /**
       * Checks if a given element is statically positioned
       * @param element - raw DOM element
       */
      function isStaticPositioned(element) {
          return (getStyle(element, "position") || 'static') === 'static';
      }

      /**
       * returns the closest, non-statically positioned parentOffset of a given element
       * @param element
       */
      var parentOffsetEl = function (element) {
          var docDomEl = $document[0];
          var offsetParent = element.offsetParent || docDomEl;
          while (offsetParent && offsetParent !== docDomEl && isStaticPositioned(offsetParent)) {
              offsetParent = offsetParent.offsetParent;
          }
          return offsetParent || docDomEl;
      };

      return {
          /**
           * Provides read-only equivalent of jQuery's position function:
           * http://api.jquery.com/position/
           */
          position: function (element) {
              var elBCR = this.offset(element);
              var offsetParentBCR = { top: 0, left: 0 };
              var offsetParentEl = parentOffsetEl(element[0]);
              if (offsetParentEl != $document[0]) {
                  offsetParentBCR = this.offset(angular.element(offsetParentEl));
                  offsetParentBCR.top += offsetParentEl.clientTop - offsetParentEl.scrollTop;
                  offsetParentBCR.left += offsetParentEl.clientLeft - offsetParentEl.scrollLeft;
              }

              return {
                  width: element.prop('offsetWidth'),
                  height: element.prop('offsetHeight'),
                  top: elBCR.top - offsetParentBCR.top,
                  left: elBCR.left - offsetParentBCR.left
              };
          },

          /**
           * Provides read-only equivalent of jQuery's offset function:
           * http://api.jquery.com/offset/
           */
          offset: function (element) {
              var boundingClientRect = element[0].getBoundingClientRect();
              return {
                  width: element.prop('offsetWidth'),
                  height: element.prop('offsetHeight'),
                  top: boundingClientRect.top + ($window.pageYOffset || $document[0].body.scrollTop || $document[0].documentElement.scrollTop),
                  left: boundingClientRect.left + ($window.pageXOffset || $document[0].body.scrollLeft || $document[0].documentElement.scrollLeft)
              };
          }
      };
  }]);

angular.module('ui.bootstrap.datepicker', ['ui.bootstrap.position'])

.constant('datepickerConfig', {
    dayFormat: 'dd',
    monthFormat: 'MMMM',
    yearFormat: 'yyyy',
    dayHeaderFormat: 'EEE',
    dayTitleFormat: 'MMMM yyyy',
    monthTitleFormat: 'yyyy',
    showWeeks: true,
    startingDay: 0,
    yearRange: 20,
    minDate: null,
    maxDate: null
})

.controller('DatepickerController', ['$scope', '$attrs', 'dateFilter', 'datepickerConfig', function ($scope, $attrs, dateFilter, dtConfig) {
    var format = {
        day: getValue($attrs.dayFormat, dtConfig.dayFormat),
        month: getValue($attrs.monthFormat, dtConfig.monthFormat),
        year: getValue($attrs.yearFormat, dtConfig.yearFormat),
        dayHeader: getValue($attrs.dayHeaderFormat, dtConfig.dayHeaderFormat),
        dayTitle: getValue($attrs.dayTitleFormat, dtConfig.dayTitleFormat),
        monthTitle: getValue($attrs.monthTitleFormat, dtConfig.monthTitleFormat)
    },
    startingDay = getValue($attrs.startingDay, dtConfig.startingDay),
    yearRange = getValue($attrs.yearRange, dtConfig.yearRange);

    this.minDate = dtConfig.minDate ? new Date(dtConfig.minDate) : null;
    this.maxDate = dtConfig.maxDate ? new Date(dtConfig.maxDate) : null;

    function getValue(value, defaultValue) {
        return angular.isDefined(value) ? $scope.$parent.$eval(value) : defaultValue;
    }

    function getDaysInMonth(year, month) {
        return new Date(year, month, 0).getDate();
    }

    function getDates(startDate, n) {
        var dates = new Array(n);
        var current = startDate, i = 0;
        while (i < n) {
            dates[i++] = new Date(current);
            current.setDate(current.getDate() + 1);
        }
        return dates;
    }

    function makeDate(date, format, isSelected, isSecondary) {
        return { date: date, label: dateFilter(date, format), selected: !!isSelected, secondary: !!isSecondary };
    }

    this.modes = [
      {
          name: 'day',
          getVisibleDates: function (date, selected) {
              var year = date.getFullYear(), month = date.getMonth(), firstDayOfMonth = new Date(year, month, 1);
              var difference = startingDay - firstDayOfMonth.getDay(),
              numDisplayedFromPreviousMonth = (difference > 0) ? 7 - difference : -difference,
              firstDate = new Date(firstDayOfMonth), numDates = 0;

              if (numDisplayedFromPreviousMonth > 0) {
                  firstDate.setDate(-numDisplayedFromPreviousMonth + 1);
                  numDates += numDisplayedFromPreviousMonth; // Previous
              }
              numDates += getDaysInMonth(year, month + 1); // Current
              numDates += (7 - numDates % 7) % 7; // Next

              var days = getDates(firstDate, numDates), labels = new Array(7);
              for (var i = 0; i < numDates; i++) {
                  var dt = new Date(days[i]);
                  days[i] = makeDate(dt, format.day, (selected && selected.getDate() === dt.getDate() && selected.getMonth() === dt.getMonth() && selected.getFullYear() === dt.getFullYear()), dt.getMonth() !== month);
              }
              for (var j = 0; j < 7; j++) {
                  labels[j] = dateFilter(days[j].date, format.dayHeader);
              }
              return { objects: days, title: dateFilter(date, format.dayTitle), labels: labels };
          },
          compare: function (date1, date2) {
              return (new Date(date1.getFullYear(), date1.getMonth(), date1.getDate()) - new Date(date2.getFullYear(), date2.getMonth(), date2.getDate()));
          },
          split: 7,
          step: { months: 1 }
      },
      {
          name: 'month',
          getVisibleDates: function (date, selected) {
              var months = new Array(12), year = date.getFullYear();
              for (var i = 0; i < 12; i++) {
                  var dt = new Date(year, i, 1);
                  months[i] = makeDate(dt, format.month, (selected && selected.getMonth() === i && selected.getFullYear() === year));
              }
              return { objects: months, title: dateFilter(date, format.monthTitle) };
          },
          compare: function (date1, date2) {
              return new Date(date1.getFullYear(), date1.getMonth()) - new Date(date2.getFullYear(), date2.getMonth());
          },
          split: 3,
          step: { years: 1 }
      },
      {
          name: 'year',
          getVisibleDates: function (date, selected) {
              var years = new Array(yearRange), year = date.getFullYear(), startYear = parseInt((year - 1) / yearRange, 10) * yearRange + 1;
              for (var i = 0; i < yearRange; i++) {
                  var dt = new Date(startYear + i, 0, 1);
                  years[i] = makeDate(dt, format.year, (selected && selected.getFullYear() === dt.getFullYear()));
              }
              return { objects: years, title: [years[0].label, years[yearRange - 1].label].join(' - ') };
          },
          compare: function (date1, date2) {
              return date1.getFullYear() - date2.getFullYear();
          },
          split: 5,
          step: { years: yearRange }
      }
    ];

    this.isDisabled = function (date, mode) {
        var currentMode = this.modes[mode || 0];
        return ((this.minDate && currentMode.compare(date, this.minDate) < 0) || (this.maxDate && currentMode.compare(date, this.maxDate) > 0) || ($scope.dateDisabled && $scope.dateDisabled({ date: date, mode: currentMode.name })));
    };
}])

.directive('datepicker', ['dateFilter', '$parse', 'datepickerConfig', '$log', function (dateFilter, $parse, datepickerConfig, $log) {
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: 'template/datepicker/datepicker.html',
        scope: {
            dateDisabled: '&'
        },
        require: ['datepicker', '?^ngModel'],
        controller: 'DatepickerController',
        link: function (scope, element, attrs, ctrls) {
            var datepickerCtrl = ctrls[0], ngModel = ctrls[1];

            if (!ngModel) {
                return; // do nothing if no ng-model
            }

            // Configuration parameters
            var mode = 0, selected = new Date(), showWeeks = datepickerConfig.showWeeks;

            if (attrs.showWeeks) {
                scope.$parent.$watch($parse(attrs.showWeeks), function (value) {
                    showWeeks = !!value;
                    updateShowWeekNumbers();
                });
            } else {
                updateShowWeekNumbers();
            }

            if (attrs.min) {
                scope.$parent.$watch($parse(attrs.min), function (value) {
                    datepickerCtrl.minDate = value ? new Date(value) : null;
                    refill();
                });
            }
            if (attrs.max) {
                scope.$parent.$watch($parse(attrs.max), function (value) {
                    datepickerCtrl.maxDate = value ? new Date(value) : null;
                    refill();
                });
            }

            function updateShowWeekNumbers() {
                scope.showWeekNumbers = mode === 0 && showWeeks;
            }

            // Split array into smaller arrays
            function split(arr, size) {
                var arrays = [];
                while (arr.length > 0) {
                    arrays.push(arr.splice(0, size));
                }
                return arrays;
            }

            function refill(updateSelected) {
                var date = null, valid = true;

                if (ngModel.$modelValue) {
                    date = new Date(ngModel.$modelValue);

                    if (isNaN(date)) {
                        valid = false;
                        //$log.error('Datepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.');
                    } else if (updateSelected) {
                        selected = date;
                    }
                }
                ngModel.$setValidity('date', valid);

                var currentMode = datepickerCtrl.modes[mode], data = currentMode.getVisibleDates(selected, date);
                angular.forEach(data.objects, function (obj) {
                    obj.disabled = datepickerCtrl.isDisabled(obj.date, mode);
                });

                ngModel.$setValidity('date-disabled', (!date || !datepickerCtrl.isDisabled(date)));

                scope.rows = split(data.objects, currentMode.split);
                scope.labels = data.labels || [];
                scope.title = data.title;
            }

            function setMode(value) {
                mode = value;
                updateShowWeekNumbers();
                refill();
            }

            ngModel.$render = function () {
                refill(true);
            };

            scope.select = function (date) {
                if (mode === 0) {
                    var dt = new Date(ngModel.$modelValue);
                    dt.setFullYear(date.getFullYear(), date.getMonth(), date.getDate());
                    ngModel.$setViewValue(dt);
                    refill(true);
                } else {
                    selected = date;
                    setMode(mode - 1);
                }
            };
            scope.move = function (direction) {
                var step = datepickerCtrl.modes[mode].step;
                selected.setMonth(selected.getMonth() + direction * (step.months || 0));
                selected.setFullYear(selected.getFullYear() + direction * (step.years || 0));
                refill();
            };
            scope.toggleMode = function () {
                setMode((mode + 1) % datepickerCtrl.modes.length);
            };
            scope.getWeekNumber = function (row) {
                return (mode === 0 && scope.showWeekNumbers && row.length === 7) ? getISO8601WeekNumber(row[0].date) : null;
            };

            function getISO8601WeekNumber(date) {
                var checkDate = new Date(date);
                checkDate.setDate(checkDate.getDate() + 4 - (checkDate.getDay() || 7)); // Thursday
                var time = checkDate.getTime();
                checkDate.setMonth(0); // Compare with Jan 1
                checkDate.setDate(1);
                return Math.floor(Math.round((time - checkDate) / 86400000) / 7) + 1;
            }
        }
    };
}])

.constant('datepickerPopupConfig', {
    dateFormat: 'yyyy-MM-dd',
    closeOnDateSelection: true
})

.directive('datepickerPopup', ['$compile', '$parse', '$document', '$position', 'dateFilter', 'datepickerPopupConfig',
function ($compile, $parse, $document, $position, dateFilter, datepickerPopupConfig) {
    return {
        restrict: 'EA',
        require: 'ngModel',
        link: function (originalScope, element, attrs, ngModel) {

            var closeOnDateSelection = angular.isDefined(attrs.closeOnDateSelection) ? scope.$eval(attrs.closeOnDateSelection) : datepickerPopupConfig.closeOnDateSelection;
            var dateFormat = attrs.datepickerPopup || datepickerPopupConfig.dateFormat;

            // create a child scope for the datepicker directive so we are not polluting original scope
            var scope = originalScope.$new();
            originalScope.$on('$destroy', function () {
                scope.$destroy();
            });

            var getIsOpen, setIsOpen;
            if (attrs.isOpen) {
                getIsOpen = $parse(attrs.isOpen);
                setIsOpen = getIsOpen.assign;

                originalScope.$watch(getIsOpen, function updateOpen(value) {
                    scope.isOpen = !!value;
                });
            }
            scope.isOpen = getIsOpen ? getIsOpen(originalScope) : false; // Initial state

            function setOpen(value) {
                if (setIsOpen) {
                    setIsOpen(originalScope, !!value);
                } else {
                    scope.isOpen = !!value;
                }
            }

            var documentClickBind = function (event) {
                if (scope.isOpen && event.target !== element[0]) {
                    scope.$apply(function () {
                        setOpen(false);
                    });
                }
            };

            var elementFocusBind = function () {
                scope.$apply(function () {
                    setOpen(true);
                });
            };

            // popup element used to display calendar
            var popupEl = angular.element('<datepicker-popup-wrap><datepicker></datepicker></datepicker-popup-wrap>');
            popupEl.attr({
                'ng-model': 'date',
                'ng-change': 'dateSelection()'
            });
            var datepickerEl = popupEl.find('datepicker');
            if (attrs.datepickerOptions) {
                datepickerEl.attr(angular.extend({}, originalScope.$eval(attrs.datepickerOptions)));
            }

            // TODO: reverse from dateFilter string to Date object
            function parseDate(viewValue) {
                if (!viewValue) {
                    ngModel.$setValidity('date', true);
                    return null;
                } else if (angular.isDate(viewValue)) {
                    ngModel.$setValidity('date', true);
                    return viewValue;
                } else if (angular.isString(viewValue)) {
                    var date = new Date(viewValue);
                    if (isNaN(date)) {
                        ngModel.$setValidity('date', false);
                        return undefined;
                    } else {
                        ngModel.$setValidity('date', true);
                        return date;
                    }
                } else {
                    ngModel.$setValidity('date', false);
                    return undefined;
                }
            }
            ngModel.$parsers.unshift(parseDate);

            // Inner change
            scope.dateSelection = function () {
                ngModel.$setViewValue(scope.date);
                ngModel.$render();

                if (closeOnDateSelection) {
                    setOpen(false);
                }
            };

            element.bind('input change keyup', function () {
                scope.$apply(function () {
                    updateCalendar();
                });
            });

            // Outter change
            ngModel.$render = function () {
                var date = ngModel.$viewValue ? dateFilter(ngModel.$viewValue, dateFormat) : '';
                element.val(date);

                updateCalendar();
            };

            function updateCalendar() {
                scope.date = ngModel.$modelValue;
                updatePosition();
            }

            function addWatchableAttribute(attribute, scopeProperty, datepickerAttribute) {
                if (attribute) {
                    originalScope.$watch($parse(attribute), function (value) {
                        scope[scopeProperty] = value;
                    });
                    datepickerEl.attr(datepickerAttribute || scopeProperty, scopeProperty);
                }
            }
            addWatchableAttribute(attrs.min, 'min');
            addWatchableAttribute(attrs.max, 'max');
            if (attrs.showWeeks) {
                addWatchableAttribute(attrs.showWeeks, 'showWeeks', 'show-weeks');
            } else {
                scope.showWeeks = true;
                datepickerEl.attr('show-weeks', 'showWeeks');
            }
            if (attrs.dateDisabled) {
                datepickerEl.attr('date-disabled', attrs.dateDisabled);
            }

            function updatePosition() {
                scope.position = $position.position(element);
                scope.position.top = scope.position.top + element.prop('offsetHeight');
            }

            var documentBindingInitialized = false, elementFocusInitialized = false;
            scope.$watch('isOpen', function (value) {
                if (value) {
                    updatePosition();
                    $document.bind('click', documentClickBind);
                    if (elementFocusInitialized) {
                        element.unbind('focus', elementFocusBind);
                    }
                    element[0].focus();
                    documentBindingInitialized = true;
                } else {
                    if (documentBindingInitialized) {
                        $document.unbind('click', documentClickBind);
                    }
                    element.bind('focus', elementFocusBind);
                    elementFocusInitialized = true;
                }

                if (setIsOpen) {
                    setIsOpen(originalScope, value);
                }
            });

            var $setModelValue = $parse(attrs.ngModel).assign;

            scope.today = function () {
                var today = new Date();
                var dates_exist = $.grep(scope.tempholidays, function (e) {
                    return (today.toString(scope.format)) == (e);
                });
                if (dates_exist != null) {
                    if (dates_exist.length > 0) {
                        bootbox.alert("Selected date should not same as non businessday");
                        return false;
                    }
                    else {
                $setModelValue(originalScope, new Date());
                    }
                }
               
            };
            scope.clear = function () {
                $setModelValue(originalScope, null);
            };

            element.after($compile(popupEl)(scope));
        }
    };
}])

.directive('datepickerPopupWrap', [function () {
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        templateUrl: 'template/datepicker/popup.html',
        link: function (scope, element, attrs) {
            element.bind('click', function (event) {
                event.preventDefault();
                event.stopPropagation();
            });
        }
    };
}]);

/*
 * dropdownToggle - Provides dropdown menu functionality in place of bootstrap js
 * @restrict class or attribute
 * @example:
   <li class="dropdown">
     <a class="dropdown-toggle">My Dropdown Menu</a>
     <ul class="dropdown-menu">
       <li ng-repeat="choice in dropChoices">
         <a ng-href="{{choice.href}}">{{choice.text}}</a>
       </li>
     </ul>
   </li>
 */

angular.module('ui.bootstrap.dropdownToggle', []).directive('dropdownToggle', ['$document', '$location', function ($document, $location) {
    var openElement = null,
        closeMenu = angular.noop;
    return {
        restrict: 'CA',
        link: function (scope, element, attrs) {
            scope.$watch('$location.path', function () { closeMenu(); });
            element.parent().bind('click', function () { closeMenu(); });
            element.bind('click', function (event) {

                var elementWasOpen = (element === openElement);

                event.preventDefault();
                event.stopPropagation();

                if (!!openElement) {
                    closeMenu();
                }

                if (!elementWasOpen) {
                    element.parent().addClass('open');
                    openElement = element;
                    closeMenu = function (event) {
                        if (event) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        $document.unbind('click', closeMenu);
                        element.parent().removeClass('open');
                        closeMenu = angular.noop;
                        openElement = null;
                    };
                    $document.bind('click', closeMenu);
                }
            });
        }
    };
}]);
angular.module('ui.bootstrap.modal', [])

/**
 * A helper, internal data structure that acts as a map but also allows getting / removing
 * elements in the LIFO order
 */
  .factory('$$stackedMap', function () {
      return {
          createNew: function () {
              var stack = [];

              return {
                  add: function (key, value) {
                      stack.push({
                          key: key,
                          value: value
                      });
                  },
                  get: function (key) {
                      for (var i = 0; i < stack.length; i++) {
                          if (key == stack[i].key) {
                              return stack[i];
                          }
                      }
                  },
                  keys: function () {
                      var keys = [];
                      for (var i = 0; i < stack.length; i++) {
                          keys.push(stack[i].key);
                      }
                      return keys;
                  },
                  top: function () {
                      return stack[stack.length - 1];
                  },
                  remove: function (key) {
                      var idx = -1;
                      for (var i = 0; i < stack.length; i++) {
                          if (key == stack[i].key) {
                              idx = i;
                              break;
                          }
                      }
                      return stack.splice(idx, 1)[0];
                  },
                  removeTop: function () {
                      return stack.splice(stack.length - 1, 1)[0];
                  },
                  length: function () {
                      return stack.length;
                  }
              };
          }
      };
  })

/**
 * A helper directive for the $modal service. It creates a backdrop element.
 */
  .directive('modalBackdrop', ['$modalStack', '$timeout', function ($modalStack, $timeout) {
      return {
          restrict: 'EA',
          replace: true,
          templateUrl: 'template/modal/backdrop.html',
          link: function (scope, element, attrs) {

              //trigger CSS transitions
              $timeout(function () {
                  scope.animate = true;
              });

              scope.close = function (evt) {
                  var modal = $modalStack.getTop();
                  if (modal && modal.value.backdrop && modal.value.backdrop != 'static') {
                      evt.preventDefault();
                      evt.stopPropagation();
                      $modalStack.dismiss(modal.key, 'backdrop click');
                  }
              };
          }
      };
  }])

  .directive('modalWindow', ['$timeout', function ($timeout) {
      return {
          restrict: 'EA',
          scope: {
              index: '@'
          },
          replace: true,
          transclude: true,
          templateUrl: 'template/modal/window.html',
          link: function (scope, element, attrs) {
              scope.windowClass = attrs.windowClass || '';

              //trigger CSS transitions
              $timeout(function () {
                  scope.animate = true;
              });
          }
      };
  }])

  .factory('$modalStack', ['$document', '$compile', '$rootScope', '$$stackedMap',
    function ($document, $compile, $rootScope, $$stackedMap) {

        var backdropjqLiteEl, backdropDomEl;
        var backdropScope = $rootScope.$new(true);
        var body = $document.find('body').eq(0);
        var openedWindows = $$stackedMap.createNew();
        var $modalStack = {};

        function backdropIndex() {
            var topBackdropIndex = -1;
            var opened = openedWindows.keys();
            for (var i = 0; i < opened.length; i++) {
                if (openedWindows.get(opened[i]).value.backdrop) {
                    topBackdropIndex = i;
                }
            }
            return topBackdropIndex;
        }

        $rootScope.$watch(backdropIndex, function (newBackdropIndex) {
            backdropScope.index = newBackdropIndex;
        });

        function removeModalWindow(modalInstance) {

            var modalWindow = openedWindows.get(modalInstance).value;

            //clean up the stack
            openedWindows.remove(modalInstance);

            //remove window DOM element
            modalWindow.modalDomEl.remove();

            //remove backdrop if no longer needed
            if (backdropIndex() == -1) {
                backdropDomEl.remove();
                backdropDomEl = undefined;
            }

            //destroy scope
            modalWindow.modalScope.$destroy();
        }

        $document.bind('keydown', function (evt) {
            var modal;

            if (evt.which === 27) {
                modal = openedWindows.top();
                if (modal && modal.value.keyboard) {
                    $rootScope.$apply(function () {
                        $modalStack.dismiss(modal.key);
                    });
                }
            }
        });

        $modalStack.open = function (modalInstance, modal) {

            openedWindows.add(modalInstance, {
                deferred: modal.deferred,
                modalScope: modal.scope,
                backdrop: modal.backdrop,
                keyboard: modal.keyboard
            });

            var angularDomEl = angular.element('<div modal-window></div>');
            angularDomEl.attr('window-class', modal.windowClass);
            angularDomEl.attr('index', openedWindows.length() - 1);
            angularDomEl.html(modal.content);

            var modalDomEl = $compile(angularDomEl)(modal.scope);
            openedWindows.top().value.modalDomEl = modalDomEl;
            body.append(modalDomEl);

            if (backdropIndex() >= 0 && !backdropDomEl) {
                backdropjqLiteEl = angular.element('<div modal-backdrop></div>');
                backdropDomEl = $compile(backdropjqLiteEl)(backdropScope);
                body.append(backdropDomEl);
            }
        };

        $modalStack.close = function (modalInstance, result) {
            var modal = openedWindows.get(modalInstance);
            if (modal) {
                modal.value.deferred.resolve(result);
                removeModalWindow(modalInstance);
            }
        };

        $modalStack.dismiss = function (modalInstance, reason) {
            var modalWindow = openedWindows.get(modalInstance).value;
            if (modalWindow) {
                modalWindow.deferred.reject(reason);
                removeModalWindow(modalInstance);
            }
        };

        $modalStack.getTop = function () {
            return openedWindows.top();
        };

        return $modalStack;
    }])

  .provider('$modal', function () {

      var $modalProvider = {
          options: {
              backdrop: true, //can be also false or 'static'
              keyboard: true
          },
          $get: ['$injector', '$rootScope', '$q', '$http', '$templateCache', '$controller', '$modalStack',
            function ($injector, $rootScope, $q, $http, $templateCache, $controller, $modalStack) {

                var $modal = {};

                function getTemplatePromise(options) {
                    return options.template ? $q.when(options.template) :
                      $http.get(options.templateUrl, { cache: $templateCache }).then(function (result) {
                          return result.data;
                      });
                }

                function getResolvePromises(resolves) {
                    var promisesArr = [];
                    angular.forEach(resolves, function (value, key) {
                        if (angular.isFunction(value) || angular.isArray(value)) {
                            promisesArr.push($q.when($injector.invoke(value)));
                        }
                    });
                    return promisesArr;
                }

                $modal.open = function (modalOptions) {

                    var modalResultDeferred = $q.defer();
                    var modalOpenedDeferred = $q.defer();

                    //prepare an instance of a modal to be injected into controllers and returned to a caller
                    var modalInstance = {
                        result: modalResultDeferred.promise,
                        opened: modalOpenedDeferred.promise,
                        close: function (result) {
                            $modalStack.close(modalInstance, result);
                        },
                        dismiss: function (reason) {
                            $modalStack.dismiss(modalInstance, reason);
                        }
                    };

                    //merge and clean up options
                    modalOptions = angular.extend({}, $modalProvider.options, modalOptions);
                    modalOptions.resolve = modalOptions.resolve || {};

                    //verify options
                    if (!modalOptions.template && !modalOptions.templateUrl) {
                        throw new Error('One of template or templateUrl options is required.');
                    }

                    var templateAndResolvePromise =
                      $q.all([getTemplatePromise(modalOptions)].concat(getResolvePromises(modalOptions.resolve)));


                    templateAndResolvePromise.then(function resolveSuccess(tplAndVars) {

                        var modalScope = (modalOptions.scope || $rootScope).$new();
                        modalScope.$close = modalInstance.close;
                        modalScope.$dismiss = modalInstance.dismiss;

                        var ctrlInstance, ctrlLocals = {};
                        var resolveIter = 1;

                        //controllers
                        if (modalOptions.controller) {
                            ctrlLocals.$scope = modalScope;
                            ctrlLocals.$modalInstance = modalInstance;
                            angular.forEach(modalOptions.resolve, function (value, key) {
                                ctrlLocals[key] = tplAndVars[resolveIter++];
                            });

                            ctrlInstance = $controller(modalOptions.controller, ctrlLocals);
                        }

                        $modalStack.open(modalInstance, {
                            scope: modalScope,
                            deferred: modalResultDeferred,
                            content: tplAndVars[0],
                            backdrop: modalOptions.backdrop,
                            keyboard: modalOptions.keyboard,
                            windowClass: modalOptions.windowClass
                        });

                    }, function resolveError(reason) {
                        modalResultDeferred.reject(reason);
                    });

                    templateAndResolvePromise.then(function () {
                        modalOpenedDeferred.resolve(true);
                    }, function () {
                        modalOpenedDeferred.reject(false);
                    });

                    return modalInstance;
                };

                return $modal;
            }]
      };

      return $modalProvider;
  });
angular.module('ui.bootstrap.pagination', [])

.controller('PaginationController', ['$scope', '$attrs', '$parse', '$interpolate', function ($scope, $attrs, $parse, $interpolate) {
    var self = this;

    this.init = function (defaultItemsPerPage) {
        if ($attrs.itemsPerPage) {
            $scope.$parent.$watch($parse($attrs.itemsPerPage), function (value) {
                self.itemsPerPage = parseInt(value, 10);
                $scope.totalPages = self.calculateTotalPages();
            });
        } else {
            this.itemsPerPage = defaultItemsPerPage;
        }
    };

    this.noPrevious = function () {
        return this.page === 1;
    };
    this.noNext = function () {
        return this.page === $scope.totalPages;
    };

    this.isActive = function (page) {
        return this.page === page;
    };

    this.calculateTotalPages = function () {
        return this.itemsPerPage < 1 ? 1 : Math.ceil($scope.totalItems / this.itemsPerPage);
    };

    this.getAttributeValue = function (attribute, defaultValue, interpolate) {
        return angular.isDefined(attribute) ? (interpolate ? $interpolate(attribute)($scope.$parent) : $scope.$parent.$eval(attribute)) : defaultValue;
    };

    this.render = function () {
        this.page = parseInt($scope.page, 10) || 1;
        $scope.pages = this.getPages(this.page, $scope.totalPages);
    };

    $scope.selectPage = function (page) {
        if (!self.isActive(page) && page > 0 && page <= $scope.totalPages) {
            $scope.page = page;
            $scope.onSelectPage({ page: page });
        }
    };

    $scope.$watch('totalItems', function () {
        $scope.totalPages = self.calculateTotalPages();
    });

    $scope.$watch('totalPages', function (value) {
        if ($attrs.numPages) {
            $scope.numPages = value; // Readonly variable
        }

        if (self.page > value) {
            $scope.selectPage(value);
        } else {
            self.render();
        }
    });

    $scope.$watch('page', function () {
        self.render();
    });
}])

.constant('paginationConfig', {
    itemsPerPage: 10,
    boundaryLinks: false,
    directionLinks: true,
    firstText: 'First',
    previousText: 'Previous',
    nextText: 'Next',
    lastText: 'Last',
    rotate: true
})

.directive('pagination', ['$parse', 'paginationConfig', function ($parse, config) {
    return {
        restrict: 'EA',
        scope: {
            page: '=',
            totalItems: '=',
            onSelectPage: ' &',
            numPages: '='
        },
        controller: 'PaginationController',
        templateUrl: 'template/pagination/pagination.html',
        replace: true,
        link: function (scope, element, attrs, paginationCtrl) {

            // Setup configuration parameters
            var maxSize,
            boundaryLinks = paginationCtrl.getAttributeValue(attrs.boundaryLinks, config.boundaryLinks),
            directionLinks = paginationCtrl.getAttributeValue(attrs.directionLinks, config.directionLinks),
            firstText = paginationCtrl.getAttributeValue(attrs.firstText, config.firstText, true),
            previousText = paginationCtrl.getAttributeValue(attrs.previousText, config.previousText, true),
            nextText = paginationCtrl.getAttributeValue(attrs.nextText, config.nextText, true),
            lastText = paginationCtrl.getAttributeValue(attrs.lastText, config.lastText, true),
            rotate = paginationCtrl.getAttributeValue(attrs.rotate, config.rotate);

            paginationCtrl.init(config.itemsPerPage);

            if (attrs.maxSize) {
                scope.$parent.$watch($parse(attrs.maxSize), function (value) {
                    maxSize = parseInt(value, 10);
                    paginationCtrl.render();
                });
            }

            // Create page object used in template
            function makePage(number, text, isActive, isDisabled) {
                return {
                    number: number,
                    text: text,
                    active: isActive,
                    disabled: isDisabled
                };
            }

            paginationCtrl.getPages = function (currentPage, totalPages) {
                var pages = [];

                // Default page limits
                var startPage = 1, endPage = totalPages;
                var isMaxSized = (angular.isDefined(maxSize) && maxSize < totalPages);

                // recompute if maxSize
                if (isMaxSized) {
                    if (rotate) {
                        // Current page is displayed in the middle of the visible ones
                        startPage = Math.max(currentPage - Math.floor(maxSize / 2), 1);
                        endPage = startPage + maxSize - 1;

                        // Adjust if limit is exceeded
                        if (endPage > totalPages) {
                            endPage = totalPages;
                            startPage = endPage - maxSize + 1;
                        }
                    } else {
                        // Visible pages are paginated with maxSize
                        startPage = ((Math.ceil(currentPage / maxSize) - 1) * maxSize) + 1;

                        // Adjust last page if limit is exceeded
                        endPage = Math.min(startPage + maxSize - 1, totalPages);
                    }
                }

                // Add page number links
                for (var number = startPage; number <= endPage; number++) {
                    var page = makePage(number, number, paginationCtrl.isActive(number), false);
                    pages.push(page);
                }

                // Add links to move between page sets
                if (isMaxSized && !rotate) {
                    if (startPage > 1) {
                        var previousPageSet = makePage(startPage - 1, '...', false, false);
                        pages.unshift(previousPageSet);
                    }

                    if (endPage < totalPages) {
                        var nextPageSet = makePage(endPage + 1, '...', false, false);
                        pages.push(nextPageSet);
                    }
                }

                // Add previous & next links
                if (directionLinks) {
                    var previousPage = makePage(currentPage - 1, previousText, false, paginationCtrl.noPrevious());
                    pages.unshift(previousPage);

                    var nextPage = makePage(currentPage + 1, nextText, false, paginationCtrl.noNext());
                    pages.push(nextPage);
                }

                // Add first & last links
                if (boundaryLinks) {
                    var firstPage = makePage(1, firstText, false, paginationCtrl.noPrevious());
                    pages.unshift(firstPage);

                    var lastPage = makePage(totalPages, lastText, false, paginationCtrl.noNext());
                    pages.push(lastPage);
                }

                return pages;
            };
        }
    };
}])

.constant('pagerConfig', {
    itemsPerPage: 10,
    previousText: '« Previous',
    nextText: 'Next »',
    align: true
})

.directive('pager', ['pagerConfig', function (config) {
    return {
        restrict: 'EA',
        scope: {
            page: '=',
            totalItems: '=',
            onSelectPage: ' &',
            numPages: '='
        },
        controller: 'PaginationController',
        templateUrl: 'template/pagination/pager.html',
        replace: true,
        link: function (scope, element, attrs, paginationCtrl) {

            // Setup configuration parameters
            var previousText = paginationCtrl.getAttributeValue(attrs.previousText, config.previousText, true),
            nextText = paginationCtrl.getAttributeValue(attrs.nextText, config.nextText, true),
            align = paginationCtrl.getAttributeValue(attrs.align, config.align);

            paginationCtrl.init(config.itemsPerPage);

            // Create page object used in template
            function makePage(number, text, isDisabled, isPrevious, isNext) {
                return {
                    number: number,
                    text: text,
                    disabled: isDisabled,
                    previous: (align && isPrevious),
                    next: (align && isNext)
                };
            }

            paginationCtrl.getPages = function (currentPage) {
                return [
                  makePage(currentPage - 1, previousText, paginationCtrl.noPrevious(), true, false),
                  makePage(currentPage + 1, nextText, paginationCtrl.noNext(), false, true)
                ];
            };
        }
    };
}]);

/**
 * The following features are still outstanding: animation as a
 * function, placement as a function, inside, support for more triggers than
 * just mouse enter/leave, html tooltips, and selector delegation.
 */
angular.module('ui.bootstrap.tooltip', ['ui.bootstrap.position', 'ui.bootstrap.bindHtml'])

/**
 * The $tooltip service creates tooltip- and popover-like directives as well as
 * houses global options for them.
 */
.provider('$tooltip', function () {
    // The default options tooltip and popover.
    var defaultOptions = {
        placement: 'top',
        animation: true,
        popupDelay: 0
    };

    // Default hide triggers for each show trigger
    var triggerMap = {
        'mouseenter': 'mouseleave',
        'click': 'click',
        'focus': 'blur'
    };

    // The options specified to the provider globally.
    var globalOptions = {};

    /**
     * `options({})` allows global configuration of all tooltips in the
     * application.
     *
     *   var app = angular.module( 'App', ['ui.bootstrap.tooltip'], function( $tooltipProvider ) {
     *     // place tooltips left instead of top by default
     *     $tooltipProvider.options( { placement: 'left' } );
     *   });
     */
    this.options = function (value) {
        angular.extend(globalOptions, value);
    };

    /**
     * This allows you to extend the set of trigger mappings available. E.g.:
     *
     *   $tooltipProvider.setTriggers( 'openTrigger': 'closeTrigger' );
     */
    this.setTriggers = function setTriggers(triggers) {
        angular.extend(triggerMap, triggers);
    };

    /**
     * This is a helper function for translating camel-case to snake-case.
     */
    function snake_case(name) {
        var regexp = /[A-Z]/g;
        var separator = '-';
        return name.replace(regexp, function (letter, pos) {
            return (pos ? separator : '') + letter.toLowerCase();
        });
    }

    /**
     * Returns the actual instance of the $tooltip service.
     * TODO support multiple triggers
     */
    this.$get = ['$window', '$compile', '$timeout', '$parse', '$document', '$position', '$interpolate', function ($window, $compile, $timeout, $parse, $document, $position, $interpolate) {
        return function $tooltip(type, prefix, defaultTriggerShow) {
            var options = angular.extend({}, defaultOptions, globalOptions);

            /**
             * Returns an object of show and hide triggers.
             *
             * If a trigger is supplied,
             * it is used to show the tooltip; otherwise, it will use the `trigger`
             * option passed to the `$tooltipProvider.options` method; else it will
             * default to the trigger supplied to this directive factory.
             *
             * The hide trigger is based on the show trigger. If the `trigger` option
             * was passed to the `$tooltipProvider.options` method, it will use the
             * mapped trigger from `triggerMap` or the passed trigger if the map is
             * undefined; otherwise, it uses the `triggerMap` value of the show
             * trigger; else it will just use the show trigger.
             */
            function getTriggers(trigger) {
                var show = trigger || options.trigger || defaultTriggerShow;
                var hide = triggerMap[show] || show;
                return {
                    show: show,
                    hide: hide
                };
            }

            var directiveName = snake_case(type);

            var startSym = $interpolate.startSymbol();
            var endSym = $interpolate.endSymbol();
            var template =
              '<' + directiveName + '-popup ' +
                'title="' + startSym + 'tt_title' + endSym + '" ' +
                'content="' + startSym + 'tt_content' + endSym + '" ' +
                'placement="' + startSym + 'tt_placement' + endSym + '" ' +
                'animation="tt_animation()" ' +
                'is-open="tt_isOpen"' +
                '>' +
              '</' + directiveName + '-popup>';

            return {
                restrict: 'EA',
                scope: true,
                link: function link(scope, element, attrs) {
                    var tooltip = $compile(template)(scope);
                    var transitionTimeout;
                    var popupTimeout;
                    var $body;
                    var appendToBody = angular.isDefined(options.appendToBody) ? options.appendToBody : false;
                    var triggers = getTriggers(undefined);
                    var hasRegisteredTriggers = false;

                    // By default, the tooltip is not open.
                    // TODO add ability to start tooltip opened
                    scope.tt_isOpen = false;

                    function toggleTooltipBind() {
                        if (!scope.tt_isOpen) {
                            showTooltipBind();
                        } else {
                            hideTooltipBind();
                        }
                    }

                    // Show the tooltip with delay if specified, otherwise show it immediately
                    function showTooltipBind() {
                        if (scope.tt_popupDelay) {
                            popupTimeout = $timeout(show, scope.tt_popupDelay);
                        } else {
                            scope.$apply(show);
                        }
                    }

                    function hideTooltipBind() {
                        scope.$apply(function () {
                            hide();
                        });
                    }

                    // Show the tooltip popup element.
                    function show() {
                        var position,
                            ttWidth,
                            ttHeight,
                            ttPosition;

                        // Don't show empty tooltips.
                        if (!scope.tt_content) {
                            return;
                        }

                        // If there is a pending remove transition, we must cancel it, lest the
                        // tooltip be mysteriously removed.
                        if (transitionTimeout) {
                            $timeout.cancel(transitionTimeout);
                        }

                        // Set the initial positioning.
                        tooltip.css({ top: 0, left: 0, display: 'block' });

                        // Now we add it to the DOM because need some info about it. But it's not 
                        // visible yet anyway.
                        if (appendToBody) {
                            $body = $body || $document.find('body');
                            $body.append(tooltip);
                        } else {
                            element.after(tooltip);
                        }

                        // Get the position of the directive element.
                        position = appendToBody ? $position.offset(element) : $position.position(element);

                        // Get the height and width of the tooltip so we can center it.
                        ttWidth = tooltip.prop('offsetWidth');
                        ttHeight = tooltip.prop('offsetHeight');

                        // Calculate the tooltip's top and left coordinates to center it with
                        // this directive.
                        switch (scope.tt_placement) {
                            case 'right':
                                ttPosition = {
                                    top: position.top + position.height / 2 - ttHeight / 2,
                                    left: position.left + position.width
                                };
                                break;
                            case 'bottom':
                                ttPosition = {
                                    top: position.top + position.height,
                                    left: position.left + position.width / 2 - ttWidth / 2
                                };
                                break;
                            case 'left':
                                ttPosition = {
                                    top: position.top + position.height / 2 - ttHeight / 2,
                                    left: position.left - ttWidth
                                };
                                break;
                            default:
                                ttPosition = {
                                    top: position.top - ttHeight,
                                    left: position.left + position.width / 2 - ttWidth / 2
                                };
                                break;
                        }

                        ttPosition.top += 'px';
                        ttPosition.left += 'px';

                        // Now set the calculated positioning.
                        tooltip.css(ttPosition);

                        // And show the tooltip.
                        scope.tt_isOpen = true;
                    }

                    // Hide the tooltip popup element.
                    function hide() {
                        // First things first: we don't show it anymore.
                        scope.tt_isOpen = false;

                        //if tooltip is going to be shown after delay, we must cancel this
                        $timeout.cancel(popupTimeout);

                        // And now we remove it from the DOM. However, if we have animation, we 
                        // need to wait for it to expire beforehand.
                        // FIXME: this is a placeholder for a port of the transitions library.
                        if (angular.isDefined(scope.tt_animation) && scope.tt_animation()) {
                            transitionTimeout = $timeout(function () { tooltip.remove(); }, 500);
                        } else {
                            tooltip.remove();
                        }
                    }

                    /**
                     * Observe the relevant attributes.
                     */
                    attrs.$observe(type, function (val) {
                        scope.tt_content = val;
                    });

                    attrs.$observe(prefix + 'Title', function (val) {
                        scope.tt_title = val;
                    });

                    attrs.$observe(prefix + 'Placement', function (val) {
                        scope.tt_placement = angular.isDefined(val) ? val : options.placement;
                    });

                    attrs.$observe(prefix + 'Animation', function (val) {
                        scope.tt_animation = angular.isDefined(val) ? $parse(val) : function () { return options.animation; };
                    });

                    attrs.$observe(prefix + 'PopupDelay', function (val) {
                        var delay = parseInt(val, 10);
                        scope.tt_popupDelay = !isNaN(delay) ? delay : options.popupDelay;
                    });

                    attrs.$observe(prefix + 'Trigger', function (val) {

                        if (hasRegisteredTriggers) {
                            element.unbind(triggers.show, showTooltipBind);
                            element.unbind(triggers.hide, hideTooltipBind);
                        }

                        triggers = getTriggers(val);

                        if (triggers.show === triggers.hide) {
                            element.bind(triggers.show, toggleTooltipBind);
                        } else {
                            element.bind(triggers.show, showTooltipBind);
                            element.bind(triggers.hide, hideTooltipBind);
                        }

                        hasRegisteredTriggers = true;
                    });

                    attrs.$observe(prefix + 'AppendToBody', function (val) {
                        appendToBody = angular.isDefined(val) ? $parse(val)(scope) : appendToBody;
                    });

                    // if a tooltip is attached to <body> we need to remove it on
                    // location change as its parent scope will probably not be destroyed
                    // by the change.
                    if (appendToBody) {
                        scope.$on('$locationChangeSuccess', function closeTooltipOnLocationChangeSuccess() {
                            if (scope.tt_isOpen) {
                                hide();
                            }
                        });
                    }

                    // Make sure tooltip is destroyed and removed.
                    scope.$on('$destroy', function onDestroyTooltip() {
                        if (scope.tt_isOpen) {
                            hide();
                        } else {
                            tooltip.remove();
                        }
                    });
                }
            };
        };
    }];
})

.directive('tooltipPopup', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: { content: '@', placement: '@', animation: '&', isOpen: '&' },
        templateUrl: 'template/tooltip/tooltip-popup.html'
    };
})

.directive('tooltip', ['$tooltip', function ($tooltip) {
    return $tooltip('tooltip', 'tooltip', 'mouseenter');
}])

.directive('tooltipHtmlUnsafePopup', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: { content: '@', placement: '@', animation: '&', isOpen: '&' },
        templateUrl: 'template/tooltip/tooltip-html-unsafe-popup.html'
    };
})

.directive('tooltipHtmlUnsafe', ['$tooltip', function ($tooltip) {
    return $tooltip('tooltipHtmlUnsafe', 'tooltip', 'mouseenter');
}]);

/**
 * The following features are still outstanding: popup delay, animation as a
 * function, placement as a function, inside, support for more triggers than
 * just mouse enter/leave, html popovers, and selector delegatation.
 */
angular.module('ui.bootstrap.popover', ['ui.bootstrap.tooltip'])
.directive('popoverPopup', function () {
    return {
        restrict: 'EA',
        replace: true,
        scope: { title: '@', content: '@', placement: '@', animation: '&', isOpen: '&' },
        templateUrl: 'template/popover/popover.html'
    };
})
.directive('popover', ['$compile', '$timeout', '$parse', '$window', '$tooltip', function ($compile, $timeout, $parse, $window, $tooltip) {
    return $tooltip('popover', 'popover', 'click');
}]);


angular.module('ui.bootstrap.progressbar', ['ui.bootstrap.transition'])

.constant('progressConfig', {
    animate: true,
    autoType: false,
    stackedTypes: ['success', 'info', 'warning', 'danger']
})

.controller('ProgressBarController', ['$scope', '$attrs', 'progressConfig', function ($scope, $attrs, progressConfig) {

    // Whether bar transitions should be animated
    var animate = angular.isDefined($attrs.animate) ? $scope.$eval($attrs.animate) : progressConfig.animate;
    var autoType = angular.isDefined($attrs.autoType) ? $scope.$eval($attrs.autoType) : progressConfig.autoType;
    var stackedTypes = angular.isDefined($attrs.stackedTypes) ? $scope.$eval('[' + $attrs.stackedTypes + ']') : progressConfig.stackedTypes;

    // Create bar object
    this.makeBar = function (newBar, oldBar, index) {
        var newValue = (angular.isObject(newBar)) ? newBar.value : (newBar || 0);
        var oldValue = (angular.isObject(oldBar)) ? oldBar.value : (oldBar || 0);
        var type = (angular.isObject(newBar) && angular.isDefined(newBar.type)) ? newBar.type : (autoType) ? getStackedType(index || 0) : null;

        return {
            from: oldValue,
            to: newValue,
            type: type,
            animate: animate
        };
    };

    function getStackedType(index) {
        return stackedTypes[index];
    }

    this.addBar = function (bar) {
        $scope.bars.push(bar);
        $scope.totalPercent += bar.to;
    };

    this.clearBars = function () {
        $scope.bars = [];
        $scope.totalPercent = 0;
    };
    this.clearBars();
}])

.directive('progress', function () {
    return {
        restrict: 'EA',
        replace: true,
        controller: 'ProgressBarController',
        scope: {
            value: '=percent',
            onFull: '&',
            onEmpty: '&'
        },
        templateUrl: 'template/progressbar/progress.html',
        link: function (scope, element, attrs, controller) {
            scope.$watch('value', function (newValue, oldValue) {
                controller.clearBars();

                if (angular.isArray(newValue)) {
                    // Stacked progress bar
                    for (var i = 0, n = newValue.length; i < n; i++) {
                        controller.addBar(controller.makeBar(newValue[i], oldValue[i], i));
                    }
                } else {
                    // Simple bar
                    controller.addBar(controller.makeBar(newValue, oldValue));
                }
            }, true);

            // Total percent listeners
            scope.$watch('totalPercent', function (value) {
                if (value >= 100) {
                    scope.onFull();
                } else if (value <= 0) {
                    scope.onEmpty();
                }
            }, true);
        }
    };
})

.directive('progressbar', ['$transition', function ($transition) {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            width: '=',
            old: '=',
            type: '=',
            animate: '='
        },
        templateUrl: 'template/progressbar/bar.html',
        link: function (scope, element) {
            scope.$watch('width', function (value) {
                if (scope.animate) {
                    element.css('width', scope.old + '%');
                    $transition(element, { width: value + '%' });
                } else {
                    element.css('width', value + '%');
                }
            });
        }
    };
}]);
angular.module('ui.bootstrap.rating', [])

.constant('ratingConfig', {
    max: 5,
    stateOn: null,
    stateOff: null
})

.controller('RatingController', ['$scope', '$attrs', '$parse', 'ratingConfig', function ($scope, $attrs, $parse, ratingConfig) {

    this.maxRange = angular.isDefined($attrs.max) ? $scope.$parent.$eval($attrs.max) : ratingConfig.max;
    this.stateOn = angular.isDefined($attrs.stateOn) ? $scope.$parent.$eval($attrs.stateOn) : ratingConfig.stateOn;
    this.stateOff = angular.isDefined($attrs.stateOff) ? $scope.$parent.$eval($attrs.stateOff) : ratingConfig.stateOff;

    this.createDefaultRange = function (len) {
        var defaultStateObject = {
            stateOn: this.stateOn,
            stateOff: this.stateOff
        };

        var states = new Array(len);
        for (var i = 0; i < len; i++) {
            states[i] = defaultStateObject;
        }
        return states;
    };

    this.normalizeRange = function (states) {
        for (var i = 0, n = states.length; i < n; i++) {
            states[i].stateOn = states[i].stateOn || this.stateOn;
            states[i].stateOff = states[i].stateOff || this.stateOff;
        }
        return states;
    };

    // Get objects used in template
    $scope.range = angular.isDefined($attrs.ratingStates) ? this.normalizeRange(angular.copy($scope.$parent.$eval($attrs.ratingStates))) : this.createDefaultRange(this.maxRange);

    $scope.rate = function (value) {
        if ($scope.readonly || $scope.value === value) {
            return;
        }

        $scope.value = value;
    };

    $scope.enter = function (value) {
        if (!$scope.readonly) {
            $scope.val = value;
        }
        $scope.onHover({ value: value });
    };

    $scope.reset = function () {
        $scope.val = angular.copy($scope.value);
        $scope.onLeave();
    };

    $scope.$watch('value', function (value) {
        $scope.val = value;
    });

    $scope.readonly = false;
    if ($attrs.readonly) {
        $scope.$parent.$watch($parse($attrs.readonly), function (value) {
            $scope.readonly = !!value;
        });
    }
}])

.directive('rating', function () {
    return {
        restrict: 'EA',
        scope: {
            value: '=',
            onHover: '&',
            onLeave: '&'
        },
        controller: 'RatingController',
        templateUrl: 'template/rating/rating.html',
        replace: true
    };
});

/**
 * @ngdoc overview
 * @name ui.bootstrap.tabs
 *
 * @description
 * AngularJS version of the tabs directive.
 */

angular.module('ui.bootstrap.tabs', [])

.directive('tabs', function () {
    return function () {
        throw new Error("The `tabs` directive is deprecated, please migrate to `tabset`. Instructions can be found at http://github.com/angular-ui/bootstrap/tree/master/CHANGELOG.md");
    };
})

.controller('TabsetController', ['$scope', '$element',
function TabsetCtrl($scope, $element) {

    var ctrl = this,
      tabs = ctrl.tabs = $scope.tabs = [];

    ctrl.select = function (tab) {
        angular.forEach(tabs, function (tab) {
            tab.active = false;
        });
        tab.active = true;
    };

    ctrl.addTab = function addTab(tab) {
        tabs.push(tab);
        if (tabs.length === 1 || tab.active) {
            ctrl.select(tab);
        }
    };

    ctrl.removeTab = function removeTab(tab) {
        var index = tabs.indexOf(tab);
        //Select a new tab if the tab to be removed is selected
        if (tab.active && tabs.length > 1) {
            //If this is the last tab, select the previous tab. else, the next tab.
            var newActiveIndex = index == tabs.length - 1 ? index - 1 : index + 1;
            ctrl.select(tabs[newActiveIndex]);
        }
        tabs.splice(index, 1);
    };
}])

/**
 * @ngdoc directive
 * @name ui.bootstrap.tabs.directive:tabset
 * @restrict EA
 *
 * @description
 * Tabset is the outer container for the tabs directive
 *
 * @param {boolean=} vertical Whether or not to use vertical styling for the tabs.
 * @param {string=} direction  What direction the tabs should be rendered. Available:
 * 'right', 'left', 'below'.
 *
 * @example
<example module="ui.bootstrap">
  <file name="index.html">
    <tabset>
      <tab heading="Vertical Tab 1"><b>First</b> Content!</tab>
      <tab heading="Vertical Tab 2"><i>Second</i> Content!</tab>
    </tabset>
    <hr />
    <tabset vertical="true">
      <tab heading="Vertical Tab 1"><b>First</b> Vertical Content!</tab>
      <tab heading="Vertical Tab 2"><i>Second</i> Vertical Content!</tab>
    </tabset>
  </file>
</example>
 */
.directive('tabset', function () {
    return {
        restrict: 'EA',
        transclude: true,
        replace: true,
        require: '^tabset',
        scope: {},
        controller: 'TabsetController',
        templateUrl: 'template/tabs/tabset.html',
        compile: function (elm, attrs, transclude) {
            return function (scope, element, attrs, tabsetCtrl) {
                scope.vertical = angular.isDefined(attrs.vertical) ? scope.$parent.$eval(attrs.vertical) : false;
                scope.type = angular.isDefined(attrs.type) ? scope.$parent.$eval(attrs.type) : 'tabs';
                scope.direction = angular.isDefined(attrs.direction) ? scope.$parent.$eval(attrs.direction) : 'top';
                scope.tabsAbove = (scope.direction != 'below');
                tabsetCtrl.$scope = scope;
                tabsetCtrl.$transcludeFn = transclude;
            };
        }
    };
})

/**
 * @ngdoc directive
 * @name ui.bootstrap.tabs.directive:tab
 * @restrict EA
 *
 * @param {string=} heading The visible heading, or title, of the tab. Set HTML headings with {@link ui.bootstrap.tabs.directive:tabHeading tabHeading}.
 * @param {string=} select An expression to evaluate when the tab is selected.
 * @param {boolean=} active A binding, telling whether or not this tab is selected.
 * @param {boolean=} disabled A binding, telling whether or not this tab is disabled.
 *
 * @description
 * Creates a tab with a heading and content. Must be placed within a {@link ui.bootstrap.tabs.directive:tabset tabset}.
 *
 * @example
<example module="ui.bootstrap">
  <file name="index.html">
    <div ng-controller="TabsDemoCtrl">
      <button class="btn btn-small" ng-click="items[0].active = true">
        Select item 1, using active binding
      </button>
      <button class="btn btn-small" ng-click="items[1].disabled = !items[1].disabled">
        Enable/disable item 2, using disabled binding
      </button>
      <br />
      <tabset>
        <tab heading="Tab 1">First Tab</tab>
        <tab select="alertMe()">
          <tab-heading><i class="icon-bell"></i> Alert me!</tab-heading>
          Second Tab, with alert callback and html heading!
        </tab>
        <tab ng-repeat="item in items"
          heading="{{item.title}}"
          disabled="item.disabled"
          active="item.active">
          {{item.content}}
        </tab>
      </tabset>
    </div>
  </file>
  <file name="script.js">
    function TabsDemoCtrl($scope) {
      $scope.items = [
        { title:"Dynamic Title 1", content:"Dynamic Item 0" },
        { title:"Dynamic Title 2", content:"Dynamic Item 1", disabled: true }
      ];

      $scope.alertMe = function() {
        setTimeout(function() {
          alert("You've selected the alert tab!");
        });
      };
    };
  </file>
</example>
 */

/**
 * @ngdoc directive
 * @name ui.bootstrap.tabs.directive:tabHeading
 * @restrict EA
 *
 * @description
 * Creates an HTML heading for a {@link ui.bootstrap.tabs.directive:tab tab}. Must be placed as a child of a tab element.
 *
 * @example
<example module="ui.bootstrap">
  <file name="index.html">
    <tabset>
      <tab>
        <tab-heading><b>HTML</b> in my titles?!</tab-heading>
        And some content, too!
      </tab>
      <tab>
        <tab-heading><i class="icon-heart"></i> Icon heading?!?</tab-heading>
        That's right.
      </tab>
    </tabset>
  </file>
</example>
 */
.directive('tab', ['$parse', '$http', '$templateCache', '$compile',
function ($parse, $http, $templateCache, $compile) {
    return {
        require: '^tabset',
        restrict: 'EA',
        replace: true,
        templateUrl: 'template/tabs/tab.html',
        transclude: true,
        scope: {
            heading: '@',
            onSelect: '&select', //This callback is called in contentHeadingTransclude
            //once it inserts the tab's content into the dom
            onDeselect: '&deselect'
        },
        controller: function () {
            //Empty controller so other directives can require being 'under' a tab
        },
        compile: function (elm, attrs, transclude) {
            return function postLink(scope, elm, attrs, tabsetCtrl) {
                var getActive, setActive;
                if (attrs.active) {
                    getActive = $parse(attrs.active);
                    setActive = getActive.assign;
                    scope.$parent.$watch(getActive, function updateActive(value) {
                        scope.active = !!value;
                    });
                    scope.active = getActive(scope.$parent);
                } else {
                    setActive = getActive = angular.noop;
                }

                scope.$watch('active', function (active) {
                    setActive(scope.$parent, active);
                    if (active) {
                        tabsetCtrl.select(scope);
                        scope.onSelect();
                    } else {
                        scope.onDeselect();
                    }
                });

                scope.disabled = false;
                if (attrs.disabled) {
                    scope.$parent.$watch($parse(attrs.disabled), function (value) {
                        scope.disabled = !!value;
                    });
                }

                scope.select = function () {
                    if (!scope.disabled) {
                        scope.active = true;
                    }
                };

                tabsetCtrl.addTab(scope);
                scope.$on('$destroy', function () {
                    tabsetCtrl.removeTab(scope);
                });
                if (scope.active) {
                    setActive(scope.$parent, true);
                }


                //We need to transclude later, once the content container is ready.
                //when this link happens, we're inside a tab heading.
                scope.$transcludeFn = transclude;
            };
        }
    };
}])

.directive('tabHeadingTransclude', [function () {
    return {
        restrict: 'A',
        require: '^tab',
        link: function (scope, elm, attrs, tabCtrl) {
            scope.$watch('headingElement', function updateHeadingElement(heading) {
                if (heading) {
                    elm.html('');
                    elm.append(heading);
                }
            });
        }
    };
}])

.directive('tabContentTransclude', ['$compile', '$parse', function ($compile, $parse) {
    return {
        restrict: 'A',
        require: '^tabset',
        link: function (scope, elm, attrs) {
            var tab = scope.$eval(attrs.tabContentTransclude);

            //Now our tab is ready to be transcluded: both the tab heading area
            //and the tab content area are loaded.  Transclude 'em both.
            tab.$transcludeFn(tab.$parent, function (contents) {
                angular.forEach(contents, function (node) {
                    if (isTabHeading(node)) {
                        //Let tabHeadingTransclude know.
                        tab.headingElement = node;
                    } else {
                        elm.append(node);
                    }
                });
            });
        }
    };
    function isTabHeading(node) {
        return node.tagName && (
          node.hasAttribute('tab-heading') ||
          node.hasAttribute('data-tab-heading') ||
          node.tagName.toLowerCase() === 'tab-heading' ||
          node.tagName.toLowerCase() === 'data-tab-heading'
        );
    }
}])

.directive('tabsetTitles', ['$http', function ($http) {
    return {
        restrict: 'A',
        require: '^tabset',
        templateUrl: 'template/tabs/tabset-titles.html',
        replace: true,
        link: function (scope, elm, attrs, tabsetCtrl) {
            if (!scope.$eval(attrs.tabsetTitles)) {
                elm.remove();
            } else {
                //now that tabs location has been decided, transclude the tab titles in
                tabsetCtrl.$transcludeFn(tabsetCtrl.$scope.$parent, function (node) {
                    elm.append(node);
                });
            }
        }
    };
}])

;


angular.module('ui.bootstrap.timepicker', [])

.constant('timepickerConfig', {
    hourStep: 1,
    minuteStep: 1,
    showMeridian: true,
    meridians: ['AM', 'PM'],
    readonlyInput: false,
    mousewheel: true
})

.directive('timepicker', ['$parse', '$log', 'timepickerConfig', function ($parse, $log, timepickerConfig) {
    return {
        restrict: 'EA',
        require: '?^ngModel',
        replace: true,
        scope: {},
        templateUrl: 'template/timepicker/timepicker.html',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) {
                return; // do nothing if no ng-model
            }

            var selected = new Date(), meridians = timepickerConfig.meridians;

            var hourStep = timepickerConfig.hourStep;
            if (attrs.hourStep) {
                scope.$parent.$watch($parse(attrs.hourStep), function (value) {
                    hourStep = parseInt(value, 10);
                });
            }

            var minuteStep = timepickerConfig.minuteStep;
            if (attrs.minuteStep) {
                scope.$parent.$watch($parse(attrs.minuteStep), function (value) {
                    minuteStep = parseInt(value, 10);
                });
            }

            // 12H / 24H mode
            scope.showMeridian = timepickerConfig.showMeridian;
            if (attrs.showMeridian) {
                scope.$parent.$watch($parse(attrs.showMeridian), function (value) {
                    scope.showMeridian = !!value;

                    if (ngModel.$error.time) {
                        // Evaluate from template
                        var hours = getHoursFromTemplate(), minutes = getMinutesFromTemplate();
                        if (angular.isDefined(hours) && angular.isDefined(minutes)) {
                            selected.setHours(hours);
                            refresh();
                        }
                    } else {
                        updateTemplate();
                    }
                });
            }

            // Get scope.hours in 24H mode if valid
            function getHoursFromTemplate() {
                var hours = parseInt(scope.hours, 10);
                var valid = (scope.showMeridian) ? (hours > 0 && hours < 13) : (hours >= 0 && hours < 24);
                if (!valid) {
                    return undefined;
                }

                if (scope.showMeridian) {
                    if (hours === 12) {
                        hours = 0;
                    }
                    if (scope.meridian === meridians[1]) {
                        hours = hours + 12;
                    }
                }
                return hours;
            }

            function getMinutesFromTemplate() {
                var minutes = parseInt(scope.minutes, 10);
                return (minutes >= 0 && minutes < 60) ? minutes : undefined;
            }

            function pad(value) {
                return (angular.isDefined(value) && value.toString().length < 2) ? '0' + value : value;
            }

            // Input elements
            var inputs = element.find('input'), hoursInputEl = inputs.eq(0), minutesInputEl = inputs.eq(1);

            // Respond on mousewheel spin
            var mousewheel = (angular.isDefined(attrs.mousewheel)) ? scope.$eval(attrs.mousewheel) : timepickerConfig.mousewheel;
            if (mousewheel) {

                var isScrollingUp = function (e) {
                    if (e.originalEvent) {
                        e = e.originalEvent;
                    }
                    //pick correct delta variable depending on event
                    var delta = (e.wheelDelta) ? e.wheelDelta : -e.deltaY;
                    return (e.detail || delta > 0);
                };

                hoursInputEl.bind('mousewheel wheel', function (e) {
                    scope.$apply((isScrollingUp(e)) ? scope.incrementHours() : scope.decrementHours());
                    e.preventDefault();
                });

                minutesInputEl.bind('mousewheel wheel', function (e) {
                    scope.$apply((isScrollingUp(e)) ? scope.incrementMinutes() : scope.decrementMinutes());
                    e.preventDefault();
                });
            }

            scope.readonlyInput = (angular.isDefined(attrs.readonlyInput)) ? scope.$eval(attrs.readonlyInput) : timepickerConfig.readonlyInput;
            if (!scope.readonlyInput) {

                var invalidate = function (invalidHours, invalidMinutes) {
                    ngModel.$setViewValue(null);
                    ngModel.$setValidity('time', false);
                    if (angular.isDefined(invalidHours)) {
                        scope.invalidHours = invalidHours;
                    }
                    if (angular.isDefined(invalidMinutes)) {
                        scope.invalidMinutes = invalidMinutes;
                    }
                };

                scope.updateHours = function () {
                    var hours = getHoursFromTemplate();

                    if (angular.isDefined(hours)) {
                        selected.setHours(hours);
                        refresh('h');
                    } else {
                        invalidate(true);
                    }
                };

                hoursInputEl.bind('blur', function (e) {
                    if (!scope.validHours && scope.hours < 10) {
                        scope.$apply(function () {
                            scope.hours = pad(scope.hours);
                        });
                    }
                });

                scope.updateMinutes = function () {
                    var minutes = getMinutesFromTemplate();

                    if (angular.isDefined(minutes)) {
                        selected.setMinutes(minutes);
                        refresh('m');
                    } else {
                        invalidate(undefined, true);
                    }
                };

                minutesInputEl.bind('blur', function (e) {
                    if (!scope.invalidMinutes && scope.minutes < 10) {
                        scope.$apply(function () {
                            scope.minutes = pad(scope.minutes);
                        });
                    }
                });
            } else {
                scope.updateHours = angular.noop;
                scope.updateMinutes = angular.noop;
            }

            ngModel.$render = function () {
                var date = ngModel.$modelValue ? new Date(ngModel.$modelValue) : null;

                if (isNaN(date)) {
                    ngModel.$setValidity('time', false);
                    //$log.error('Timepicker directive: "ng-model" value must be a Date object, a number of milliseconds since 01.01.1970 or a string representing an RFC2822 or ISO 8601 date.');
                } else {
                    if (date) {
                        selected = date;
                    }
                    makeValid();
                    updateTemplate();
                }
            };

            // Call internally when we know that model is valid.
            function refresh(keyboardChange) {
                makeValid();
                ngModel.$setViewValue(new Date(selected));
                updateTemplate(keyboardChange);
            }

            function makeValid() {
                ngModel.$setValidity('time', true);
                scope.invalidHours = false;
                scope.invalidMinutes = false;
            }

            function updateTemplate(keyboardChange) {
                var hours = selected.getHours(), minutes = selected.getMinutes();

                if (scope.showMeridian) {
                    hours = (hours === 0 || hours === 12) ? 12 : hours % 12; // Convert 24 to 12 hour system
                }
                scope.hours = keyboardChange === 'h' ? hours : pad(hours);
                scope.minutes = keyboardChange === 'm' ? minutes : pad(minutes);
                scope.meridian = selected.getHours() < 12 ? meridians[0] : meridians[1];
            }

            function addMinutes(minutes) {
                var dt = new Date(selected.getTime() + minutes * 60000);
                selected.setHours(dt.getHours(), dt.getMinutes());
                refresh();
            }

            scope.incrementHours = function () {
                addMinutes(hourStep * 60);
            };
            scope.decrementHours = function () {
                addMinutes(-hourStep * 60);
            };
            scope.incrementMinutes = function () {
                addMinutes(minuteStep);
            };
            scope.decrementMinutes = function () {
                addMinutes(-minuteStep);
            };
            scope.toggleMeridian = function () {
                addMinutes(12 * 60 * ((selected.getHours() < 12) ? 1 : -1));
            };
        }
    };
}]);

angular.module('ui.bootstrap.typeahead', ['ui.bootstrap.position', 'ui.bootstrap.bindHtml'])

/**
 * A helper service that can parse typeahead's syntax (string provided by users)
 * Extracted to a separate service for ease of unit testing
 */
  .factory('typeaheadParser', ['$parse', function ($parse) {

      //                      00000111000000000000022200000000000000003333333333333330000000000044000
      var TYPEAHEAD_REGEXP = /^\s*(.*?)(?:\s+as\s+(.*?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+(.*)$/;

      return {
          parse: function (input) {

              var match = input.match(TYPEAHEAD_REGEXP), modelMapper, viewMapper, source;
              if (!match) {
                  throw new Error(
                    "Expected typeahead specification in form of '_modelValue_ (as _label_)? for _item_ in _collection_'" +
                      " but got '" + input + "'.");
              }

              return {
                  itemName: match[3],
                  source: $parse(match[4]),
                  viewMapper: $parse(match[2] || match[1]),
                  modelMapper: $parse(match[1])
              };
          }
      };
  }])

  .directive('typeahead', ['$compile', '$parse', '$q', '$timeout', '$document', '$position', 'typeaheadParser',
    function ($compile, $parse, $q, $timeout, $document, $position, typeaheadParser) {

        var HOT_KEYS = [9, 13, 27, 38, 40];

        return {
            require: 'ngModel',
            link: function (originalScope, element, attrs, modelCtrl) {

                //SUPPORTED ATTRIBUTES (OPTIONS)

                //minimal no of characters that needs to be entered before typeahead kicks-in
                var minSearch = originalScope.$eval(attrs.typeaheadMinLength) || 1;

                //minimal wait time after last character typed before typehead kicks-in
                var waitTime = originalScope.$eval(attrs.typeaheadWaitMs) || 0;

                //should it restrict model values to the ones selected from the popup only?
                var isEditable = originalScope.$eval(attrs.typeaheadEditable) !== false;

                //binding to a variable that indicates if matches are being retrieved asynchronously
                var isLoadingSetter = $parse(attrs.typeaheadLoading).assign || angular.noop;

                //a callback executed when a match is selected
                var onSelectCallback = $parse(attrs.typeaheadOnSelect);

                var inputFormatter = attrs.typeaheadInputFormatter ? $parse(attrs.typeaheadInputFormatter) : undefined;

                //INTERNAL VARIABLES

                //model setter executed upon match selection
                var $setModelValue = $parse(attrs.ngModel).assign;

                //expressions used by typeahead
                var parserResult = typeaheadParser.parse(attrs.typeahead);


                //pop-up element used to display matches
                var popUpEl = angular.element('<typeahead-popup></typeahead-popup>');
                popUpEl.attr({
                    matches: 'matches',
                    active: 'activeIdx',
                    select: 'select(activeIdx)',
                    query: 'query',
                    position: 'position'
                });
                //custom item template
                if (angular.isDefined(attrs.typeaheadTemplateUrl)) {
                    popUpEl.attr('template-url', attrs.typeaheadTemplateUrl);
                }

                //create a child scope for the typeahead directive so we are not polluting original scope
                //with typeahead-specific data (matches, query etc.)
                var scope = originalScope.$new();
                originalScope.$on('$destroy', function () {
                    scope.$destroy();
                });

                var resetMatches = function () {
                    scope.matches = [];
                    scope.activeIdx = -1;
                };

                var getMatchesAsync = function (inputValue) {

                    var locals = { $viewValue: inputValue };
                    isLoadingSetter(originalScope, true);
                    $q.when(parserResult.source(scope, locals)).then(function (matches) {

                        //it might happen that several async queries were in progress if a user were typing fast
                        //but we are interested only in responses that correspond to the current view value
                        if (inputValue === modelCtrl.$viewValue) {
                            if (matches.length > 0) {

                                scope.activeIdx = 0;
                                scope.matches.length = 0;

                                //transform labels
                                for (var i = 0; i < matches.length; i++) {
                                    locals[parserResult.itemName] = matches[i];
                                    scope.matches.push({
                                        label: parserResult.viewMapper(scope, locals),
                                        model: matches[i]
                                    });
                                }

                                scope.query = inputValue;
                                //position pop-up with matches - we need to re-calculate its position each time we are opening a window
                                //with matches as a pop-up might be absolute-positioned and position of an input might have changed on a page
                                //due to other elements being rendered
                                scope.position = $position.position(element);
                                scope.position.top = scope.position.top + element.prop('offsetHeight');

                            } else {
                                resetMatches();
                            }
                            isLoadingSetter(originalScope, false);
                        }
                    }, function () {
                        resetMatches();
                        isLoadingSetter(originalScope, false);
                    });
                };

                resetMatches();

                //we need to propagate user's query so we can higlight matches
                scope.query = undefined;

                //Declare the timeout promise var outside the function scope so that stacked calls can be cancelled later 
                var timeoutPromise;

                //plug into $parsers pipeline to open a typeahead on view changes initiated from DOM
                //$parsers kick-in on all the changes coming from the view as well as manually triggered by $setViewValue
                modelCtrl.$parsers.unshift(function (inputValue) {

                    resetMatches();
                    if (inputValue && inputValue.length >= minSearch) {
                        if (waitTime > 0) {
                            if (timeoutPromise) {
                                $timeout.cancel(timeoutPromise);//cancel previous timeout
                            }
                            timeoutPromise = $timeout(function () {
                                getMatchesAsync(inputValue);
                            }, waitTime);
                        } else {
                            getMatchesAsync(inputValue);
                        }
                    }

                    if (isEditable) {
                        return inputValue;
                    } else {
                        modelCtrl.$setValidity('editable', false);
                        return undefined;
                    }
                });

                modelCtrl.$formatters.push(function (modelValue) {

                    var candidateViewValue, emptyViewValue;
                    var locals = {};

                    if (inputFormatter) {

                        locals['$model'] = modelValue;
                        return inputFormatter(originalScope, locals);

                    } else {

                        //it might happen that we don't have enough info to properly render input value
                        //we need to check for this situation and simply return model value if we can't apply custom formatting
                        locals[parserResult.itemName] = modelValue;
                        candidateViewValue = parserResult.viewMapper(originalScope, locals);
                        locals[parserResult.itemName] = undefined;
                        emptyViewValue = parserResult.viewMapper(originalScope, locals);

                        return candidateViewValue !== emptyViewValue ? candidateViewValue : modelValue;
                    }
                });

                scope.select = function (activeIdx) {
                    //called from within the $digest() cycle
                    var locals = {};
                    var model, item;

                    locals[parserResult.itemName] = item = scope.matches[activeIdx].model;
                    model = parserResult.modelMapper(originalScope, locals);
                    $setModelValue(originalScope, model);
                    modelCtrl.$setValidity('editable', true);

                    onSelectCallback(originalScope, {
                        $item: item,
                        $model: model,
                        $label: parserResult.viewMapper(originalScope, locals)
                    });

                    resetMatches();

                    //return focus to the input element if a mach was selected via a mouse click event
                    element[0].focus();
                };

                //bind keyboard events: arrows up(38) / down(40), enter(13) and tab(9), esc(27)
                element.bind('keydown', function (evt) {

                    //typeahead is open and an "interesting" key was pressed
                    if (scope.matches.length === 0 || HOT_KEYS.indexOf(evt.which) === -1) {
                        return;
                    }

                    evt.preventDefault();

                    if (evt.which === 40) {
                        scope.activeIdx = (scope.activeIdx + 1) % scope.matches.length;
                        scope.$digest();

                    } else if (evt.which === 38) {
                        scope.activeIdx = (scope.activeIdx ? scope.activeIdx : scope.matches.length) - 1;
                        scope.$digest();

                    } else if (evt.which === 13 || evt.which === 9) {
                        scope.$apply(function () {
                            scope.select(scope.activeIdx);
                        });

                    } else if (evt.which === 27) {
                        evt.stopPropagation();

                        resetMatches();
                        scope.$digest();
                    }
                });

                // Keep reference to click handler to unbind it.
                var dismissClickHandler = function (evt) {
                    if (element[0] !== evt.target) {
                        resetMatches();
                        scope.$digest();
                    }
                };

                $document.bind('click', dismissClickHandler);

                originalScope.$on('$destroy', function () {
                    $document.unbind('click', dismissClickHandler);
                });

                element.after($compile(popUpEl)(scope));
            }
        };

    }])

  .directive('typeaheadPopup', function () {
      return {
          restrict: 'E',
          scope: {
              matches: '=',
              query: '=',
              active: '=',
              position: '=',
              select: '&'
          },
          replace: true,
          templateUrl: 'template/typeahead/typeahead-popup.html',
          link: function (scope, element, attrs) {

              scope.templateUrl = attrs.templateUrl;

              scope.isOpen = function () {
                  return scope.matches.length > 0;
              };

              scope.isActive = function (matchIdx) {
                  return scope.active == matchIdx;
              };

              scope.selectActive = function (matchIdx) {
                  scope.active = matchIdx;
              };

              scope.selectMatch = function (activeIdx) {
                  scope.select({ activeIdx: activeIdx });
              };
          }
      };
  })

  .directive('typeaheadMatch', ['$http', '$templateCache', '$compile', '$parse', function ($http, $templateCache, $compile, $parse) {
      return {
          restrict: 'E',
          scope: {
              index: '=',
              match: '=',
              query: '='
          },
          link: function (scope, element, attrs) {
              var tplUrl = $parse(attrs.templateUrl)(scope.$parent) || 'template/typeahead/typeahead-match.html';
              $http.get(tplUrl, { cache: $templateCache }).success(function (tplContent) {
                  element.replaceWith($compile(tplContent.trim())(scope));
              });
          }
      };
  }])

  .filter('typeaheadHighlight', function () {

      function escapeRegexp(queryToEscape) {
          return queryToEscape.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
      }

      return function (matchItem, query) {
          return query ? matchItem.replace(new RegExp(escapeRegexp(query), 'gi'), '<strong>$&</strong>') : matchItem;
      };
  });
angular.module("template/accordion/accordion-group.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/accordion/accordion-group.html",
      "<div class=\"accordion-group\">\n" +
      "  <div class=\"accordion-heading\" ><a class=\"accordion-toggle\" ng-click=\"isOpen = !isOpen\" accordion-transclude=\"heading\">{{heading}}</a></div>\n" +
      "  <div class=\"accordion-body\" collapse=\"!isOpen\">\n" +
      "    <div class=\"accordion-inner\" ng-transclude></div>  </div>\n" +
      "</div>");
}]);

angular.module("template/accordion/accordion.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/accordion/accordion.html",
      "<div class=\"accordion\" ng-transclude></div>");
}]);

angular.module("template/alert/alert.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/alert/alert.html",
      "<div class='alert' ng-class='type && \"alert-\" + type'>\n" +
      "    <button ng-show='closeable' type='button' class='close' ng-click='close()'>&times;</button>\n" +
      "    <div ng-transclude></div>\n" +
      "</div>\n" +
      "");
}]);

angular.module("template/carousel/carousel.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/carousel/carousel.html",
      "<div ng-mouseenter=\"pause()\" ng-mouseleave=\"play()\" class=\"carousel\">\n" +
      "    <ol class=\"carousel-indicators\" ng-show=\"slides().length > 1\">\n" +
      "        <li ng-repeat=\"slide in slides()\" ng-class=\"{active: isActive(slide)}\" ng-click=\"select(slide)\"></li>\n" +
      "    </ol>\n" +
      "    <div class=\"carousel-inner\" ng-transclude></div>\n" +
      "    <a ng-click=\"prev()\" class=\"carousel-control left\" ng-show=\"slides().length > 1\">&lsaquo;</a>\n" +
      "    <a ng-click=\"next()\" class=\"carousel-control right\" ng-show=\"slides().length > 1\">&rsaquo;</a>\n" +
      "</div>\n" +
      "");
}]);

angular.module("template/carousel/slide.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/carousel/slide.html",
      "<div ng-class=\"{\n" +
      "    'active': leaving || (active && !entering),\n" +
      "    'prev': (next || active) && direction=='prev',\n" +
      "    'next': (next || active) && direction=='next',\n" +
      "    'right': direction=='prev',\n" +
      "    'left': direction=='next'\n" +
      "  }\" class=\"item\" ng-transclude></div>\n" +
      "");
}]);

angular.module("template/datepicker/datepicker.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/datepicker/datepicker.html",
      "<table>\n" +
      "  <thead>\n" +
      "    <tr class=\"text-center\">\n" +
      "      <th><button type=\"button\" class=\"btn pull-left\" ng-click=\"move(-1)\"><i class=\"icon-chevron-left\"></i></button></th>\n" +
      "      <th colspan=\"{{rows[0].length - 2 + showWeekNumbers}}\"><button type=\"button\" class=\"btn btn-block\" ng-click=\"toggleMode()\"><strong>{{title}}</strong></button></th>\n" +
      "      <th><button type=\"button\" class=\"btn pull-right\" ng-click=\"move(1)\"><i class=\"icon-chevron-right\"></i></button></th>\n" +
      "    </tr>\n" +
      "    <tr class=\"text-center\" ng-show=\"labels.length > 0\">\n" +
      "      <th ng-show=\"showWeekNumbers\">#</th>\n" +
      "      <th ng-repeat=\"label in labels\">{{label}}</th>\n" +
      "    </tr>\n" +
      "  </thead>\n" +
      "  <tbody>\n" +
      "    <tr ng-repeat=\"row in rows\">\n" +
      "      <td ng-show=\"showWeekNumbers\" class=\"text-center\"><em>{{ getWeekNumber(row) }}</em></td>\n" +
      "      <td ng-repeat=\"dt in row\" class=\"text-center\">\n" +
      "        <button type=\"button\" style=\"width:100%;\" class=\"btn\" ng-class=\"{'btn-info': dt.selected}\" ng-click=\"select(dt.date)\" ng-disabled=\"dt.disabled\"><span ng-class=\"{muted: dt.secondary}\">{{dt.label}}</span></button>\n" +
      "      </td>\n" +
      "    </tr>\n" +
      "  </tbody>\n" +
      "</table>\n" +
      "");
}]);

angular.module("template/datepicker/popup.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/datepicker/popup.html",
      "<ul class=\"dropdown-menu dp-con\" ng-style=\"{display: (isOpen && 'block') || 'none', top: position.top+'px', left: position.left+'px'}\" class=\"dropdown-menu dp-con\">\n" +
      "	<li ng-transclude></li>\n" +
      "	<li class=\"divider\"></li>\n" +
      "	<li style=\"padding: 0 9px;\">\n" +
      "		<span class=\"btn-group\">\n" +
      "			<button class=\"btn btn-small btn-info inlineBlock\" ng-click=\"today()\">Today</button>\n" +
      "			<button class=\"btn btn-small inlineBlock\" ng-click=\"showWeeks = ! showWeeks\" ng-class=\"{active: showWeeks}\">Weeks</button>\n" +
      //"			<button class=\"btn btn-small btn-danger inlineBlock\" ng-click=\"clear()\">Clear</button>\n" +
      "		</span>\n" +
      "		<button class=\"btn btn-small btn-success pull-right\" ng-click=\"isOpen = false\">Close</button>\n" +
      "	</li>\n" +
      "</ul>");
}]);

angular.module("template/modal/backdrop.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/modal/backdrop.html",
      "<div class=\"modal-backdrop fade\" ng-class=\"{in: animate}\" ng-style=\"{'z-index': 1040 + index*10}\" ng-click=\"close($event)\"></div>");
}]);

angular.module("template/modal/window.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/modal/window.html",
      "<div class=\"modal fade {{ windowClass }}\" ng-class=\"{in: animate}\" ng-style=\"{'z-index': 1050 + index*10}\" ng-transclude></div>");
}]);

angular.module("template/pagination/pager.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/pagination/pager.html",
      "<div class=\"pager\">\n" +
      "  <ul>\n" +
      "    <li ng-repeat=\"page in pages\" ng-class=\"{disabled: page.disabled, previous: page.previous, next: page.next}\"><a ng-click=\"selectPage(page.number)\">{{page.text}}</a></li>\n" +
      "  </ul>\n" +
      "</div>\n" +
      "");
}]);

angular.module("template/pagination/pagination.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/pagination/pagination.html",
      "<div class=\"pagination\"><ul>\n" +
      "  <li ng-repeat=\"page in pages\" ng-class=\"{active: page.active, disabled: page.disabled}\"><a ng-click=\"selectPage(page.number)\">{{page.text}}</a></li>\n" +
      "  </ul>\n" +
      "</div>\n" +
      "");
}]);

angular.module("template/tooltip/tooltip-html-unsafe-popup.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/tooltip/tooltip-html-unsafe-popup.html",
      "<div class=\"tooltip {{placement}}\" ng-class=\"{ in: isOpen(), fade: animation() }\">\n" +
      "  <div class=\"tooltip-arrow\"></div>\n" +
      "  <div class=\"tooltip-inner\" ng-bind-html-unsafe=\"content\"></div>\n" +
      "</div>\n" +
      "");
}]);

angular.module("template/tooltip/tooltip-popup.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/tooltip/tooltip-popup.html",
      "<div class=\"tooltip {{placement}}\" ng-class=\"{ in: isOpen(), fade: animation() }\">\n" +
      "  <div class=\"tooltip-arrow\"></div>\n" +
      "  <div class=\"tooltip-inner\" ng-bind=\"content\"></div>\n" +
      "</div>\n" +
      "");
}]);

angular.module("template/popover/popover.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/popover/popover.html",
      "<div class=\"popover {{placement}}\" ng-class=\"{ in: isOpen(), fade: animation() }\">\n" +
      "  <div class=\"arrow\"></div>\n" +
      "\n" +
      "  <div class=\"popover-inner\">\n" +
      "      <h3 class=\"popover-title\" ng-bind=\"title\" ng-show=\"title\"></h3>\n" +
      "      <div class=\"popover-content\" ng-bind=\"content\"></div>\n" +
      "  </div>\n" +
      "</div>\n" +
      "");
}]);

angular.module("template/progressbar/bar.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/progressbar/bar.html",
      "<div class=\"bar\" ng-class='type && \"bar-\" + type'></div>");
}]);

angular.module("template/progressbar/progress.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/progressbar/progress.html",
      "<div class=\"progress\"><progressbar ng-repeat=\"bar in bars\" width=\"bar.to\" old=\"bar.from\" animate=\"bar.animate\" type=\"bar.type\"></progressbar></div>");
}]);

angular.module("template/rating/rating.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/rating/rating.html",
      "<span ng-mouseleave=\"reset()\">\n" +
      "	<i ng-repeat=\"r in range\" ng-mouseenter=\"enter($index + 1)\" ng-click=\"rate($index + 1)\" ng-class=\"$index < val && (r.stateOn || 'icon-star') || (r.stateOff || 'icon-star-empty')\"></i>\n" +
      "</span>");
}]);

angular.module("template/tabs/pane.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/tabs/pane.html",
      "<div class=\"tab-pane\" ng-class=\"{active: selected}\" ng-show=\"selected\" ng-transclude></div>\n" +
      "");
}]);

angular.module("template/tabs/tab.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/tabs/tab.html",
      "<li ng-class=\"{active: active, disabled: disabled}\">\n" +
      "  <a ng-click=\"select()\" tab-heading-transclude>{{heading}}</a>\n" +
      "</li>\n" +
      "");
}]);

angular.module("template/tabs/tabs.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/tabs/tabs.html",
      "<div class=\"tabbable\">\n" +
      "  <ul class=\"nav nav-tabs\">\n" +
      "    <li ng-repeat=\"pane in panes\" ng-class=\"{active:pane.selected}\">\n" +
      "      <a ng-click=\"select(pane)\">{{pane.heading}}</a>\n" +
      "    </li>\n" +
      "  </ul>\n" +
      "  <div class=\"tab-content\" ng-transclude></div>\n" +
      "</div>\n" +
      "");
}]);

angular.module("template/tabs/tabset-titles.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/tabs/tabset-titles.html",
      "<ul class=\"nav {{type && 'nav-' + type}}\" ng-class=\"{'nav-stacked': vertical}\">\n" +
      "</ul>\n" +
      "");
}]);

angular.module("template/tabs/tabset.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/tabs/tabset.html",
      "\n" +
      "<div class=\"tabbable\" ng-class=\"{'tabs-right': direction == 'right', 'tabs-left': direction == 'left', 'tabs-below': direction == 'below'}\">\n" +
      "  <div tabset-titles=\"tabsAbove\"></div>\n" +
      "  <div class=\"tab-content\">\n" +
      "    <div class=\"tab-pane\" \n" +
      "         ng-repeat=\"tab in tabs\" \n" +
      "         ng-class=\"{active: tab.active}\"\n" +
      "         tab-content-transclude=\"tab\">\n" +
      "    </div>\n" +
      "  </div>\n" +
      "  <div tabset-titles=\"!tabsAbove\"></div>\n" +
      "</div>\n" +
      "");
}]);

angular.module("template/timepicker/timepicker.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/timepicker/timepicker.html",
      "<table class=\"form-inline\">\n" +
      "	<tr class=\"text-center\">\n" +
      "		<td><a ng-click=\"incrementHours()\" class=\"btn btn-link\"><i class=\"icon-chevron-up\"></i></a></td>\n" +
      "		<td>&nbsp;</td>\n" +
      "		<td><a ng-click=\"incrementMinutes()\" class=\"btn btn-link\"><i class=\"icon-chevron-up\"></i></a></td>\n" +
      "		<td ng-show=\"showMeridian\"></td>\n" +
      "	</tr>\n" +
      "	<tr>\n" +
      "		<td class=\"control-group\" ng-class=\"{'error': invalidHours}\"><input type=\"text\" ng-model=\"hours\" ng-change=\"updateHours()\" class=\"span1 text-center\" ng-mousewheel=\"incrementHours()\" ng-readonly=\"readonlyInput\" maxlength=\"2\" /></td>\n" +
      "		<td>:</td>\n" +
      "		<td class=\"control-group\" ng-class=\"{'error': invalidMinutes}\"><input type=\"text\" ng-model=\"minutes\" ng-change=\"updateMinutes()\" class=\"span1 text-center\" ng-readonly=\"readonlyInput\" maxlength=\"2\"></td>\n" +
      "		<td ng-show=\"showMeridian\"><button type=\"button\" ng-click=\"toggleMeridian()\" class=\"btn text-center\">{{meridian}}</button></td>\n" +
      "	</tr>\n" +
      "	<tr class=\"text-center\">\n" +
      "		<td><a ng-click=\"decrementHours()\" class=\"btn btn-link\"><i class=\"icon-chevron-down\"></i></a></td>\n" +
      "		<td>&nbsp;</td>\n" +
      "		<td><a ng-click=\"decrementMinutes()\" class=\"btn btn-link\"><i class=\"icon-chevron-down\"></i></a></td>\n" +
      "		<td ng-show=\"showMeridian\"></td>\n" +
      "	</tr>\n" +
      "</table>");
}]);

angular.module("template/typeahead/typeahead-match.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/typeahead/typeahead-match.html",
      "<a tabindex=\"-1\" bind-html-unsafe=\"match.label | typeaheadHighlight:query\"></a>");
}]);

angular.module("template/typeahead/typeahead-popup.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/typeahead/typeahead-popup.html",
      "<ul class=\"typeahead dropdown-menu\" ng-style=\"{display: isOpen()&&'block' || 'none', top: position.top+'px', left: position.left+'px'}\">\n" +
      "    <li ng-repeat=\"match in matches\" ng-class=\"{active: isActive($index) }\" ng-mouseenter=\"selectActive($index)\" ng-click=\"selectMatch($index)\">\n" +
      "        <typeahead-match index=\"$index\" match=\"match\" query=\"query\" template-url=\"templateUrl\"></typeahead-match>\n" +
      "    </li>\n" +
      "</ul>");
}]);

angular.module("template/typeahead/typeahead.html", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/typeahead/typeahead.html",
      "<ul class=\"typeahead dropdown-menu\" ng-style=\"{display: isOpen()&&'block' || 'none', top: position.top+'px', left: position.left+'px'}\">\n" +
      "    <li ng-repeat=\"match in matches\" ng-class=\"{active: isActive($index) }\" ng-mouseenter=\"selectActive($index)\">\n" +
      "        <a tabindex=\"-1\" ng-click=\"selectMatch($index)\" ng-bind-html-unsafe=\"match.label | typeaheadHighlight:query\"></a>\n" +
      "    </li>\n" +
      "</ul>");
}]);
///#source 1 1 /appnew/bower_components/oclazyload/dist/ocLazyLoad.min.js
(function(){"use strict";function v(t){var i=[];return angular.forEach(t.requires,function(t){n.indexOf(t)===-1&&i.push(t)}),i}function e(n){try{return angular.module(n)}catch(t){if(/No module/.test(t)||t.message.indexOf("$injector:nomod")>-1)return!1}}function y(n){try{return angular.module(n)}catch(t){(/No module/.test(t)||t.message.indexOf("$injector:nomod")>-1)&&(t.message='The module "'+n+'" that you are trying to load does not exist. '+t.message);throw t;}}function u(n,t,i,r){var o,c,u,f,l,h,e,a;if(t)for(o=0,c=t.length;o<c;o++)if(u=t[o],angular.isArray(u)){if(n!==null)if(n.hasOwnProperty(u[0]))f=n[u[0]];else throw new Error("unsupported provider "+u[0]);if(l=p(u,i),u[1]!=="invoke")l&&angular.isDefined(f)&&f[u[1]].apply(f,u[2]);else if(h=function(n){var t=s.indexOf(i+"-"+n);(t===-1||r)&&(t===-1&&s.push(i+"-"+n),angular.isDefined(f)&&f[u[1]].apply(f,u[2]))},angular.isFunction(u[2][0]))h(u[2][0]);else if(angular.isArray(u[2][0]))for(e=0,a=u[2][0].length;e<a;e++)angular.isFunction(u[2][0][e])&&h(u[2][0][e])}}function c(t,i,e){var l,s,h,a,v,y;if(i){for(a=[],l=i.length-1;l>=0;l--)if(s=i[l],typeof s!="string"&&(s=o(s)),s&&f.indexOf(s)===-1){if(v=n.indexOf(s)===-1,h=angular.module(s),v&&(n.push(s),c(t,h.requires,e)),h._runBlocks.length>0)for(r[s]=[];h._runBlocks.length>0;)r[s].push(h._runBlocks.shift());angular.isDefined(r[s])&&(v||e.rerun)&&(a=a.concat(r[s]));u(t,h._invokeQueue,s,e.reconfig);u(t,h._configBlocks,s,e.reconfig);i.pop();f.push(s)}y=t.getInstanceInjector();angular.forEach(a,function(n){y.invoke(n)})}}function p(n,t){var r=n[2][0],u=n[1],e=!1,f;if(angular.isUndefined(i[t])&&(i[t]={}),angular.isUndefined(i[t][u])&&(i[t][u]=[]),f=function(n){e=!0;i[t][u].push(n)},angular.isString(r)&&i[t][u].indexOf(r)===-1)f(r);else if(angular.isObject(r))angular.forEach(r,function(n){angular.isString(n)&&i[t][u].indexOf(n)===-1&&f(n)});else return!1;return e}function o(n){var t=null;return angular.isString(n)?t=n:angular.isObject(n)&&n.hasOwnProperty("name")&&angular.isString(n.name)&&(t=n.name),t}function w(i){var e;if(t.length===0){var o=[i],f=["ng:app","ng-app","x-ng-app","data-ng-app"],s=/\sng[:\-]app(:\s*([\w\d_]+);?)?\s/,r=function(n){return n&&o.push(n)};angular.forEach(f,function(n){f[n]=!0;r(document.getElementById(n));n=n.replace(":","\\:");i[0].querySelectorAll&&(angular.forEach(i[0].querySelectorAll("."+n),r),angular.forEach(i[0].querySelectorAll("."+n+"\\:"),r),angular.forEach(i[0].querySelectorAll("["+n+"]"),r))});angular.forEach(o,function(n){if(t.length===0){var u=" "+i.className+" ",r=s.exec(u);r?t.push((r[2]||"").replace(/\s+/g,",")):angular.forEach(n.attributes,function(n){t.length===0&&f[n.name]&&t.push(n.value)})}})}if(t.length===0)throw"No module found during bootstrap, unable to init ocLazyLoad";e=function e(t){if(n.indexOf(t)===-1){n.push(t);var i=angular.module(t);u(null,i._invokeQueue,t);u(null,i._configBlocks,t);angular.forEach(i.requires,e)}};angular.forEach(t,function(n){e(n)})}var n=["ng"],t=[],i={},s=[],f=[],r={},h=angular.module("oc.lazyLoad",["ng"]),a=angular.noop,l;h.provider("$ocLazyLoad",["$controllerProvider","$provide","$compileProvider","$filterProvider","$injector","$animateProvider",function(t,i,r,u,s,h){var k={},g={$controllerProvider:t,$compileProvider:r,$filterProvider:u,$provide:i,$injector:s,$animateProvider:h},nt=document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0],l,p,b,d=!1,tt=!1;w(angular.element(window.document));this.$get=["$log","$q","$templateCache","$http","$rootElement","$rootScope","$cacheFactory","$interval",function(t,i,r,u,s,h,w,it){var ot,rt=w("ocLazyLoad"),st=!1,ft=!1,et,ut;return d||(t={},t.error=angular.noop,t.warn=angular.noop,t.info=angular.noop),g.getInstanceInjector=function(){return ot?ot:ot=s.data("$injector")||angular.injector()},a=function(n,i){tt&&h.$broadcast(n,i);d&&t.info(n,i)},et=function(n,t,r){var e=i.defer(),u,c,l=function(n){var t=(new Date).getTime();return n.indexOf("?")>=0?n.substring(0,n.length-1)==="&"?n+"_dc="+t:n+"&_dc="+t:n+"?_dc="+t},h,o,f,s,a,v,y,p,w;angular.isUndefined(rt.get(t))&&rt.put(t,e.promise);switch(n){case"css":u=document.createElement("link");u.type="text/css";u.rel="stylesheet";u.href=r.cache===!1?l(t):t;break;case"js":u=document.createElement("script");u.src=r.cache===!1?l(t):t;break;default:e.reject(new Error('Requested type "'+n+'" is not known. Could not inject "'+t+'"'))}return u.onload=u.onreadystatechange=function(){u.readyState&&!/^c|loade/.test(u.readyState)||c||(u.onload=u.onreadystatechange=null,c=1,e.resolve())},u.onerror=function(){e.reject(new Error("Unable to load "+t))},u.async=r.serie?0:1,h=nt.lastChild,r.insertBefore&&(o=angular.element(r.insertBefore),o&&o.length>0&&(h=o[0])),nt.insertBefore(u,h),n=="css"&&(st||(f=navigator.userAgent.toLowerCase(),/iP(hone|od|ad)/.test(navigator.platform)?(s=navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/),a=parseFloat([parseInt(s[1],10),parseInt(s[2],10),parseInt(s[3]||0,10)].join(".")),ft=a<6):f.indexOf("android")>-1?(v=parseFloat(f.slice(f.indexOf("android")+8)),ft=v<4.4):f.indexOf("safari")>-1&&f.indexOf("chrome")==-1&&(y=parseFloat(f.match(/version\/([\.\d]+)/i)[1]),ft=y<6)),ft&&(p=1e3,w=it(function(){try{u.sheet.cssRules;it.cancel(w);u.onload()}catch(n){--p<=0&&u.onerror()}},20))),e.promise},angular.isUndefined(l)&&(l=function(n,t,r){var u=[];angular.forEach(n,function(n){u.push(et("js",n,r))});i.all(u).then(function(){t()},function(n){t(n)})},l.ocLazyLoadLoader=!0),angular.isUndefined(p)&&(p=function(n,t,r){var u=[];angular.forEach(n,function(n){u.push(et("css",n,r))});i.all(u).then(function(){t()},function(n){t(n)})},p.ocLazyLoadLoader=!0),angular.isUndefined(b)&&(b=function(n,t,f){var e=[];return angular.forEach(n,function(n){var t=i.defer();e.push(t.promise);u.get(n,f).success(function(i){angular.isString(i)&&i.length>0&&angular.forEach(angular.element(i),function(n){n.nodeName==="SCRIPT"&&n.type==="text/ng-template"&&r.put(n.id,n.innerHTML)});angular.isUndefined(rt.get(n))&&rt.put(n,!0);t.resolve()}).error(function(i){t.reject(new Error('Unable to load template file "'+n+'": '+i))})}),i.all(e).then(function(){t()},function(n){t(n)})},b.ocLazyLoadLoader=!0),ut=function(n,r){var f=[],e=[],o=[],u=[],s=null,v,h,c,a;return angular.extend(r||{},n),v=function(n){s=rt.get(n);angular.isUndefined(s)||r.cache===!1?/\.(css|less)[^\.]*$/.test(n)&&f.indexOf(n)===-1?f.push(n):/\.(htm|html)[^\.]*$/.test(n)&&e.indexOf(n)===-1?e.push(n):o.indexOf(n)===-1&&o.push(n):s&&u.push(s)},r.serie?v(r.files.shift()):angular.forEach(r.files,function(n){v(n)}),f.length>0&&(h=i.defer(),p(f,function(n){angular.isDefined(n)&&p.hasOwnProperty("ocLazyLoadLoader")?(t.error(n),h.reject(n)):h.resolve()},r),u.push(h.promise)),e.length>0&&(c=i.defer(),b(e,function(n){angular.isDefined(n)&&b.hasOwnProperty("ocLazyLoadLoader")?(t.error(n),c.reject(n)):c.resolve()},r),u.push(c.promise)),o.length>0&&(a=i.defer(),l(o,function(n){angular.isDefined(n)&&l.hasOwnProperty("ocLazyLoadLoader")?(t.error(n),a.reject(n)):a.resolve()},r),u.push(a.promise)),r.serie&&r.files.length>0?i.all(u).then(function(){return ut(n,r)}):i.all(u)},{getModuleConfig:function(n){if(!angular.isString(n))throw new Error("You need to give the name of the module to get");return k[n]?k[n]:null},setModuleConfig:function(n){if(!angular.isObject(n))throw new Error("You need to give the module config object to set");return k[n.name]=n,n},getModules:function(){return n},isLoaded:function(t){var u=function(t){var i=n.indexOf(t)>-1;return i||(i=!!e(t)),i},i,r;if(angular.isString(t)&&(t=[t]),angular.isArray(t)){for(i=0,r=t.length;i<r;i++)if(!u(t[i]))return!1;return!0}throw new Error("You need to define the module(s) name(s)");},load:function(r,u){var p=this,s=null,w=[],d=[],h=i.defer(),l,k,a,b;return(angular.isUndefined(u)&&(u={}),angular.isArray(r))?(angular.forEach(r,function(n){n&&d.push(p.load(n,u))}),i.all(d).then(function(){h.resolve(r)},function(n){h.reject(n)}),h.promise):(l=o(r),typeof r=="string"?(s=p.getModuleConfig(r),s||(s={files:[r]},l=null)):typeof r=="object"&&(s=p.setModuleConfig(r)),s===null?(k='Module "'+l+'" is not configured, cannot load.',t.error(k),h.reject(new Error(k))):angular.isDefined(s.template)&&(angular.isUndefined(s.files)&&(s.files=[]),angular.isString(s.template)?s.files.push(s.template):angular.isArray(s.template)&&s.files.concat(s.template)),w.push=function(n){this.indexOf(n)===-1&&Array.prototype.push.apply(this,arguments)},angular.isDefined(l)&&e(l)&&n.indexOf(l)!==-1&&(w.push(l),angular.isUndefined(s.files)))?(h.resolve(),h.promise):(a={},angular.extend(a,u,s),b=function b(n){var r,h,c,u,f=[],s;if(r=o(n),r===null)return i.when();try{h=y(r)}catch(l){return s=i.defer(),t.error(l.message),s.reject(l),s.promise}return c=v(h),angular.forEach(c,function(i){if(typeof i=="string"){var o=p.getModuleConfig(i);if(o===null){w.push(i);return}i=o}if(e(i.name)){typeof n!="string"&&(u=i.files.filter(function(n){return p.getModuleConfig(i.name).files.indexOf(n)<0}),u.length!==0&&t.warn('Module "',r,'" attempted to redefine configuration for dependency. "',i.name,'"\n Additional Files Loaded:',u),f.push(ut(i.files,a).then(function(){return b(i)})));return}typeof i=="object"&&(i.hasOwnProperty("name")&&i.name&&(p.setModuleConfig(i),w.push(i.name)),i.hasOwnProperty("css")&&i.css.length!==0&&angular.forEach(i.css,function(n){et("css",n,a)}));i.hasOwnProperty("files")&&i.files.length!==0&&i.files&&f.push(ut(i,a).then(function(){return b(i)}))}),i.all(f)},ut(s,a).then(function(){l===null?h.resolve(r):(w.push(l),b(l).then(function(){try{f=[];c(g,w,a)}catch(n){t.error(n.message);h.reject(n);return}h.resolve(r)},function(n){h.reject(n)}))},function(n){h.reject(n)}),h.promise)}}}];this.config=function(n){if(angular.isDefined(n.jsLoader)||angular.isDefined(n.asyncLoader)){if(!angular.isFunction(n.jsLoader||n.asyncLoader))throw"The js loader needs to be a function";l=n.jsLoader||n.asyncLoader}if(angular.isDefined(n.cssLoader)){if(!angular.isFunction(n.cssLoader))throw"The css loader needs to be a function";p=n.cssLoader}if(angular.isDefined(n.templatesLoader)){if(!angular.isFunction(n.templatesLoader))throw"The template loader needs to be a function";b=n.templatesLoader}angular.isDefined(n.modules)&&(angular.isArray(n.modules)?angular.forEach(n.modules,function(n){k[n.name]=n}):k[n.modules.name]=n.modules);angular.isDefined(n.debug)&&(d=n.debug);angular.isDefined(n.events)&&(tt=n.events)}}]);h.directive("ocLazyLoad",["$ocLazyLoad","$compile","$animate","$parse",function(n,t,i,r){return{restrict:"A",terminal:!0,priority:1e3,compile:function(u){var f=u[0].innerHTML;return u.html(""),function(u,e,o){var s=r(o.ocLazyLoad);u.$watch(function(){return s(u)||o.ocLazyLoad},function(r){angular.isDefined(r)&&n.load(r).then(function(){i.enter(t(f)(u),null,e)})},!0)}}}}]);l=angular.bootstrap;angular.bootstrap=function(n,i,r){return t=i.slice(),l(n,i,r)};Array.prototype.indexOf||(Array.prototype.indexOf=function(n,t){var r,f,u,i;if(this==null)throw new TypeError('"this" is null or not defined');if((f=Object(this),u=f.length>>>0,u===0)||(i=+t||0,Math.abs(i)===Infinity&&(i=0),i>=u))return-1;for(r=Math.max(i>=0?i:u-Math.abs(i),0);r<u;){if(r in f&&f[r]===n)return r;r++}return-1})})();
/*
//# sourceMappingURL=ocLazyLoad.min.js.map
*/
///#source 1 1 /appnew/bower_components/angular-translate/angular-translate.min.js
angular.module("pascalprecht.translate",["ng"]).run(["$translate",function(n){var i=n.storageKey(),t=n.storage(),r=function(){var r=n.preferredLanguage();angular.isString(r)?n.use(r):t.put(i,n.use())};t?t.get(i)?n.use(t.get(i))["catch"](r):r():angular.isString(n.preferredLanguage())&&n.use(n.preferredLanguage())}]);angular.module("pascalprecht.translate").provider("$translate",["$STORAGE_KEY",function(n){var r={},u,a=[],v,t,p,i,s,h,w=n,d,f,st,g=[],nt=!1,e,tt="translate-cloak",it,b,k,ht=!1,rt=".",c,vt="2.5.2",ct=function(){var i=window.navigator,r=["language","browserLanguage","systemLanguage","userLanguage"],n,t;if(angular.isArray(i.languages))for(n=0;n<i.languages.length;n++)if(t=i.languages[n],t&&t.length)return t;for(n=0;n<r.length;n++)if(t=i[r[n]],t&&t.length)return t;return null},ut,y,ft,et,ot;ct.displayName="angular-translate/service: getFirstBrowserLanguage";ut=function(){return(ct()||"").split("-").join("_")};ut.displayName="angular-translate/service: getLocale";var o=function(n,t){for(var i=0,r=n.length;i<r;i++)if(n[i]===t)return i;return-1},lt=function(){return this.replace(/^\s+|\s+$/g,"")},at=function(n){for(var i=[],h=angular.lowercase(n),u=0,c=a.length,f,t,e,s,r;u<c;u++)i.push(angular.lowercase(a[u]));if(o(i,h)>-1)return n;if(v)for(t in v)if(e=!1,s=Object.prototype.hasOwnProperty.call(v,t)&&angular.lowercase(t)===angular.lowercase(n),t.slice(-1)==="*"&&(e=t.slice(0,-1)===n.slice(0,t.length-1)),(s||e)&&(f=v[t],o(i,angular.lowercase(f))>-1))return f;return(r=n.split("_"),r.length>1&&o(i,angular.lowercase(r[0]))>-1)?r[0]:n},l=function(n,t){if(!n&&!t)return r;if(n&&!t){if(angular.isString(n))return r[n]}else angular.isObject(r[n])||(r[n]={}),angular.extend(r[n],y(t));return this};this.translations=l;this.cloakClassName=function(n){return n?(tt=n,this):tt};y=function(n,t,i,r){var u,e,o,f;t||(t=[]);i||(i={});for(u in n)Object.prototype.hasOwnProperty.call(n,u)&&(f=n[u],angular.isObject(f)?y(f,t.concat(u),i,u):(e=t.length?""+t.join(rt)+rt+u:u,t.length&&u===r&&(o=""+t.join(rt),i[o]="@:"+e),i[e]=f));return i};this.addInterpolation=function(n){return g.push(n),this};this.useMessageFormatInterpolation=function(){return this.useInterpolation("$translateMessageFormatInterpolation")};this.useInterpolation=function(n){return st=n,this};this.useSanitizeValueStrategy=function(n){return nt=n,this};this.preferredLanguage=function(n){return ft(n),this};ft=function(n){return n&&(u=n),u};this.translationNotFoundIndicator=function(n){return this.translationNotFoundIndicatorLeft(n),this.translationNotFoundIndicatorRight(n),this};this.translationNotFoundIndicatorLeft=function(n){return n?(b=n,this):b};this.translationNotFoundIndicatorRight=function(n){return n?(k=n,this):k};this.fallbackLanguage=function(n){return et(n),this};et=function(n){return n?(angular.isString(n)?(p=!0,t=[n]):angular.isArray(n)&&(p=!1,t=n),angular.isString(u)&&o(t,u)<0&&t.push(u),this):p?t[0]:t};this.use=function(n){if(n){if(!r[n]&&!e)throw new Error("$translateProvider couldn't find translationTable for langKey: '"+n+"'");return i=n,this}return i};ot=function(n){if(!n)return d?d+w:w;w=n};this.storageKey=ot;this.useUrlLoader=function(n,t){return this.useLoader("$translateUrlLoader",angular.extend({url:n},t))};this.useStaticFilesLoader=function(n){return this.useLoader("$translateStaticFilesLoader",n)};this.useLoader=function(n,t){return e=n,it=t||{},this};this.useLocalStorage=function(){return this.useStorage("$translateLocalStorage")};this.useCookieStorage=function(){return this.useStorage("$translateCookieStorage")};this.useStorage=function(n){return h=n,this};this.storagePrefix=function(n){return n?(d=n,this):n};this.useMissingTranslationHandlerLog=function(){return this.useMissingTranslationHandler("$translateMissingTranslationHandlerLog")};this.useMissingTranslationHandler=function(n){return f=n,this};this.usePostCompiling=function(n){return ht=!!n,this};this.determinePreferredLanguage=function(n){var t=n&&angular.isFunction(n)?n():ut();return u=a.length?at(t):t,this};this.registerAvailableLanguageKeys=function(n,t){return n?(a=n,t&&(v=t),this):a};this.useLoaderCache=function(n){return n===!1?c=undefined:n===!0?c=!0:typeof n=="undefined"?c="$translationCache":n&&(c=n),this};this.$get=["$log","$injector","$rootScope","$q",function(n,a,v,d){var yt,pt=a.get(st||"$translateDefaultInterpolation"),ct=!1,kt={},ut={},dt,wt,rt=function(n,r,f){var c,e,s;return angular.isArray(n)?(c=function(n){for(var i={},u=[],e=function(n){var t=d.defer(),u=function(r){i[n]=r;t.resolve([n,r])};return rt(n,r,f).then(u,u),t.promise},t=0,o=n.length;t<o;t++)u.push(e(n[t]));return d.all(u).then(function(){return i})},c(n)):(e=d.defer(),n&&(n=lt.apply(n)),s=function(){var n=u?ut[u]:ut[i],r,f;return dt=0,h&&!n&&(r=yt.get(w),n=ut[r],t&&t.length&&(f=o(t,r),dt=f===0?1:0,o(t,u)<0&&t.push(u))),n}(),s?s.then(function(){fi(n,r,f).then(e.resolve,e.reject)},e.reject):fi(n,r,f).then(e.resolve,e.reject),e.promise)},ii=function(n){return b&&(n=[b,n].join(" ")),k&&(n=[n,k].join(" ")),n},ni=function(n){i=n;v.$emit("$translateChangeSuccess",{language:n});h&&yt.put(rt.storageKey(),i);pt.setLocale(i);angular.forEach(kt,function(n,t){kt[t].setLocale(i)});v.$emit("$translateChangeEnd",{language:n})},bt=function(n){var i,t,r;if(!n)throw"No language key specified for loading.";return i=d.defer(),v.$emit("$translateLoadingStart",{language:n}),ct=!0,t=c,typeof t=="string"&&(t=a.get(t)),r=angular.extend({},it,{key:n,$http:angular.extend({},{cache:t},it.$http)}),a.get(e)(r).then(function(t){var r={};v.$emit("$translateLoadingSuccess",{language:n});angular.isArray(t)?angular.forEach(t,function(n){angular.extend(r,y(n))}):angular.extend(r,y(t));ct=!1;i.resolve({key:n,table:r});v.$emit("$translateLoadingEnd",{language:n})},function(n){v.$emit("$translateLoadingError",{language:n});i.reject(n);v.$emit("$translateLoadingEnd",{language:n})}),i.promise},oi,gt,si;if(h&&(yt=a.get(h),!yt.get||!yt.put))throw new Error("Couldn't use storage '"+h+"', missing get() or put() method!");angular.isFunction(pt.useSanitizeValueStrategy)&&pt.useSanitizeValueStrategy(nt);g.length&&angular.forEach(g,function(n){var t=a.get(n);t.setLocale(u||i);angular.isFunction(t.useSanitizeValueStrategy)&&t.useSanitizeValueStrategy(nt);kt[t.getInterpolationIdentifier()]=t});var hi=function(n){var t=d.defer();return Object.prototype.hasOwnProperty.call(r,n)?t.resolve(r[n]):ut[n]?ut[n].then(function(n){l(n.key,n.table);t.resolve(n.table)},t.reject):t.reject(),t.promise},ci=function(n,t,r,u){var f=d.defer();return hi(n).then(function(e){Object.prototype.hasOwnProperty.call(e,t)?(u.setLocale(n),f.resolve(u.interpolate(e[t],r)),u.setLocale(i)):f.reject()},f.reject),f.promise},li=function(n,t,u,f){var o,e=r[n];return e&&Object.prototype.hasOwnProperty.call(e,t)&&(f.setLocale(n),o=f.interpolate(e[t],u),f.setLocale(i)),o},ti=function(n){if(f){var t=a.get(f)(n,i);return t!==undefined?t:n}return n},ri=function(n,i,r,u){var f=d.defer(),e;return n<t.length?(e=t[n],ci(e,i,r,u).then(f.resolve,function(){ri(n+1,i,r,u).then(f.resolve)})):f.resolve(ti(i)),f.promise},ui=function(n,i,r,u){var f,e;return n<t.length&&(e=t[n],f=li(e,i,r,u),f||(f=ui(n+1,i,r,u))),f},ai=function(n,t,i){return ri(wt>0?wt:dt,n,t,i)},vi=function(n,t,i){return ui(wt>0?wt:dt,n,t,i)},fi=function(n,u,e){var o=d.defer(),h=i?r[i]:r,l=e?kt[e]:pt,s,c;return h&&Object.prototype.hasOwnProperty.call(h,n)?(s=h[n],s.substr(0,2)==="@:"?rt(s.substr(2),u,e).then(o.resolve,o.reject):o.resolve(l.interpolate(s,u))):(f&&!ct&&(c=ti(n)),i&&t&&t.length?ai(n,u,l).then(function(n){o.resolve(n)},function(n){o.reject(ii(n))}):f&&!ct&&c?o.resolve(c):o.reject(ii(n))),o.promise},ei=function(n,u,e){var o,h=i?r[i]:r,l=e?kt[e]:pt,s,c;return h&&Object.prototype.hasOwnProperty.call(h,n)?(s=h[n],o=s.substr(0,2)==="@:"?ei(s.substr(2),u,e):l.interpolate(s,u)):(f&&!ct&&(c=ti(n)),i&&t&&t.length?(dt=0,o=vi(n,u,l)):o=f&&!ct&&c?c:ii(n)),o};if(rt.preferredLanguage=function(n){return n&&ft(n),u},rt.cloakClassName=function(){return tt},rt.fallbackLanguage=function(n){if(n!==undefined&&n!==null){if(et(n),e&&t&&t.length)for(var i=0,r=t.length;i<r;i++)ut[t[i]]||(ut[t[i]]=bt(t[i]));rt.use(rt.use())}return p?t[0]:t},rt.useFallbackLanguage=function(n){if(n!==undefined&&n!==null)if(n){var i=o(t,n);i>-1&&(wt=i)}else wt=0},rt.proposedLanguage=function(){return s},rt.storage=function(){return yt},rt.use=function(n){var t,u;return n?(t=d.defer(),v.$emit("$translateChangeStart",{language:n}),u=at(n),u&&(n=u),r[n]||!e||ut[n]?(t.resolve(n),ni(n)):(s=n,ut[n]=bt(n).then(function(i){return l(i.key,i.table),t.resolve(i.key),ni(i.key),s===n&&(s=undefined),i},function(n){s===n&&(s=undefined);v.$emit("$translateChangeError",{language:n});t.reject(n);v.$emit("$translateChangeEnd",{language:n})})),t.promise):i},rt.storageKey=function(){return ot()},rt.isPostCompilingEnabled=function(){return ht},rt.refresh=function(n){function h(){f.resolve();v.$emit("$translateRefreshEnd",{language:n})}function c(){f.reject();v.$emit("$translateRefreshEnd",{language:n})}var f,o,s,u,a;if(!e)throw new Error("Couldn't refresh translation table, no loader registered!");if(f=d.defer(),v.$emit("$translateRefreshStart",{language:n}),n)r[n]?bt(n).then(function(t){l(t.key,t.table);n===i&&ni(i);h()},c):c();else{if(o=[],s={},t&&t.length)for(u=0,a=t.length;u<a;u++)o.push(bt(t[u])),s[t[u]]=!0;i&&!s[i]&&o.push(bt(i));d.all(o).then(function(n){angular.forEach(n,function(n){r[n.key]&&delete r[n.key];l(n.key,n.table)});i&&ni(i);h()})}return f.promise},rt.instant=function(n,e,o){var a,c,y,s,h,l,p,v;if(n===null||angular.isUndefined(n))return n;if(angular.isArray(n)){for(a={},c=0,y=n.length;c<y;c++)a[n[c]]=rt.instant(n[c],e,o);return a}if(angular.isString(n)&&n.length<1)return n;for(n&&(n=lt.apply(n)),h=[],u&&h.push(u),i&&h.push(i),t&&t.length&&(h=h.concat(t)),l=0,p=h.length;l<p;l++)if(v=h[l],r[v]&&typeof r[v][n]!="undefined"&&(s=ei(n,e,o)),typeof s!="undefined")break;return s||s===""||(s=pt.interpolate(n,e),f&&!ct&&(s=ti(n))),s},rt.versionInfo=function(){return vt},rt.loaderCache=function(){return c},e&&(angular.equals(r,{})&&rt.use(rt.use()),t&&t.length))for(oi=function(n){return l(n.key,n.table),v.$emit("$translateChangeEnd",{language:n.key}),n},gt=0,si=t.length;gt<si;gt++)ut[t[gt]]=bt(t[gt]).then(oi);return rt}]}]);angular.module("pascalprecht.translate").factory("$translateDefaultInterpolation",["$interpolate",function(n){var t={},u,f="default",i=null,r={escaped:function(n){var i={};for(var t in n)Object.prototype.hasOwnProperty.call(n,t)&&(i[t]=angular.element("<div><\/div>").text(n[t]).html());return i}},e=function(n){return angular.isFunction(r[i])?r[i](n):n};return t.setLocale=function(n){u=n},t.getInterpolationIdentifier=function(){return f},t.useSanitizeValueStrategy=function(n){return i=n,this},t.interpolate=function(t,r){return i&&(r=e(r)),n(t)(r||{})},t}]);angular.module("pascalprecht.translate").constant("$STORAGE_KEY","NG_TRANSLATE_LANG_KEY");angular.module("pascalprecht.translate").directive("translate",["$translate","$q","$interpolate","$compile","$parse","$rootScope",function(n,t,i,r,u,f){return{restrict:"AE",scope:!0,compile:function(t,e){var o=e.translateValues?e.translateValues:undefined,s=e.translateInterpolation?e.translateInterpolation:undefined,h=t[0].outerHTML.match(/translate-value-+/i),c="^(.*)("+i.startSymbol()+".*"+i.endSymbol()+")(.*)",l="^(.*)"+i.startSymbol()+"(.*)"+i.endSymbol()+"(.*)";return function(t,a,v){var b,d,w,nt;t.interpolateParams={};t.preText="";t.postText="";var y={},k=function(n){if(angular.equals(n,"")||!angular.isDefined(n)){var r=a.text().match(c);angular.isArray(r)?(t.preText=r[1],t.postText=r[3],y.translate=i(r[2])(t.$parent),watcherMatches=a.text().match(l),angular.isArray(watcherMatches)&&watcherMatches[2]&&watcherMatches[2].length&&t.$watch(watcherMatches[2],function(n){y.translate=n;p()})):y.translate=a.text().replace(/^\s+|\s+$/g,"")}else y.translate=n;p()},tt=function(n){v.$observe(n,function(t){y[n]=t;p()})};v.$observe("translate",function(n){k(n)});for(b in v)v.hasOwnProperty(b)&&b.substr(0,13)==="translateAttr"&&tt(b);if(v.$observe("translateDefault",function(n){t.defaultText=n}),o&&v.$observe("translateValues",function(n){n&&t.$parent.$watch(function(){angular.extend(t.interpolateParams,u(n)(t.$parent))})}),h){d=function(n){v.$observe(n,function(i){var r=angular.lowercase(n.substr(14,1))+n.substr(15);t.interpolateParams[r]=i})};for(w in v)Object.prototype.hasOwnProperty.call(v,w)&&w.substr(0,14)==="translateValue"&&w!=="translateValues"&&d(w)}var p=function(){for(var n in y)y.hasOwnProperty(n)&&y[n]&&it(n,y[n],t,t.interpolateParams)},it=function(t,i,r,u){n(i,u,s).then(function(n){g(n,r,!0,t)},function(n){g(n,r,!1,t)})},g=function(t,i,u,f){var s;if(f==="translate"){u||typeof i.defaultText=="undefined"||(t=i.defaultText);a.html(i.preText+t+i.postText);var h=n.isPostCompilingEnabled(),o=typeof e.translateCompile!="undefined",c=o&&e.translateCompile!=="false";(h&&!o||c)&&r(a.contents())(i)}else u||typeof i.defaultText=="undefined"||(t=i.defaultText),s=v.$attr[f].substr(15),a.attr(s,t)};t.$watch("interpolateParams",p,!0);nt=f.$on("$translateChangeSuccess",p);a.text().length&&k("");p();t.$on("$destroy",nt)}}}}]);angular.module("pascalprecht.translate").directive("translateCloak",["$rootScope","$translate",function(n,t){return{compile:function(i){var r=function(){i.addClass(t.cloakClassName())},u=function(){i.removeClass(t.cloakClassName())},f=n.$on("$translateChangeEnd",function(){u();f();f=null});return r(),function(n,i,f){f.translateCloak&&f.translateCloak.length&&f.$observe("translateCloak",function(n){t(n).then(u,r)})}}}}]);angular.module("pascalprecht.translate").filter("translate",["$parse","$translate",function(n,t){var i=function(i,r,u){return angular.isObject(r)||(r=n(r)(this)),t.instant(i,r,u)};return i.$stateful=!0,i}]);
/*
//# sourceMappingURL=angular-translate.min.js.map
*/
///#source 1 1 /appnew/bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js
angular.module("pascalprecht.translate").factory("$translateStaticFilesLoader",["$q","$http",function(n,t){return function(i){if(!i||!angular.isString(i.prefix)||!angular.isString(i.suffix))throw new Error("Couldn't load static files, no prefix or suffix specified!");var r=n.defer();return t(angular.extend({url:[i.prefix,i.key,i.suffix].join(""),method:"GET",params:""},i.$http)).success(function(n){r.resolve(n)}).error(function(){r.reject(i.key)}),r.promise}}]);
/*
//# sourceMappingURL=angular-translate-loader-static-files.min.js.map
*/
///#source 1 1 /appnew/bower_components/angular-ui-router/release/angular-ui-router.min.js
typeof module!="undefined"&&typeof exports!="undefined"&&module.exports===exports&&(module.exports="ui.router"),function(n,t,i){"use strict";function w(n,t){return r(new(r(function(){},{prototype:n})),t)}function k(n){return f(arguments,function(t){t!==n&&f(t,function(t,i){n.hasOwnProperty(i)||(n[i]=t)})}),n}function bt(n,t){var r=[];for(var i in n.path){if(n.path[i]!==t.path[i])break;r.push(n.path[i])}return r}function p(n){if(Object.keys)return Object.keys(n);var i=[];return t.forEach(n,function(n,t){i.push(t)}),i}function h(n,t){if(Array.prototype.indexOf)return n.indexOf(t,Number(arguments[2])||0);var r=n.length>>>0,i=Number(arguments[2])||0;for(i=i<0?Math.ceil(i):Math.floor(i),i<0&&(i+=r);i<r;i++)if(i in n&&n[i]===t)return i;return-1}function it(n,t,i,u){var o=bt(i,u),f,c={},l=[],s,e;for(s in o)if(o[s].params&&(f=p(o[s].params),f.length))for(e in f)h(l,f[e])>=0||(l.push(f[e]),c[f[e]]=n[f[e]]);return r({},c,t)}function d(n,t,i){var f,r,u;if(!i){i=[];for(f in n)i.push(f)}for(r=0;r<i.length;r++)if(u=i[r],n[u]!=t[u])return!1;return!0}function g(n,t){var i={};return f(n,function(n){i[n]=t[n]}),i}function b(n){var i={},r=Array.prototype.concat.apply(Array.prototype,Array.prototype.slice.call(arguments,1));for(var t in n)h(r,t)==-1&&(i[t]=n[t]);return i}function nt(n,t){var r=l(n),i=r?[]:{};return f(n,function(n,u){t(n,u)&&(i[r?i.length:u]=n)}),i}function y(n,t){var i=l(n)?[]:{};return f(n,function(n,r){i[r]=t(n,r)}),i}function rt(n,t){var s=1,c=2,o={},l=[],a=o,y=r(n.when(o),{$$promises:o,$$values:o});this.study=function(o){function tt(n,i){if(g[i]!==c){if(d.push(i),g[i]===s){d.splice(0,h(d,i));throw new Error("Cyclic dependency: "+d.join(" -> "));}if(g[i]=s,e(n))w.push(i,[function(){return t.get(n)}],l);else{var r=t.annotate(n);f(r,function(n){n!==i&&o.hasOwnProperty(n)&&tt(o[n],n)});w.push(i,n,r)}d.pop();g[i]=c}}function it(n){return v(n)&&n.then&&n.$$promises}if(!v(o))throw new Error("'invocables' must be an object");var nt=p(o||{}),w=[],d=[],g={};return f(o,tt),o=d=g=null,function(e,o,s){function d(){--ft||(rt||k(c,o.$$values),h.$$values=c,h.$$promises=h.$$promises||!0,delete h.$$inheritedValues,g.resolve(c))}function tt(n){h.$$failure=n;g.reject(n)}function et(i,r,o){function v(n){l.reject(n);tt(n)}function y(){if(!u(h.$$failure))try{l.resolve(t.invoke(r,s,c));l.promise.then(function(n){c[i]=n;d()},v)}catch(n){v(n)}}var l=n.defer(),a=0;f(o,function(n){p.hasOwnProperty(n)&&!e.hasOwnProperty(n)&&(a++,p[n].then(function(t){c[n]=t;--a||y()},v))});a||y();p[i]=l.promise}var l,ut;if(it(e)&&s===i&&(s=o,o=e,e=null),e){if(!v(e))throw new Error("'locals' must be an object");}else e=a;if(o){if(!it(o))throw new Error("'parent' must be a promise returned by $resolve.resolve()");}else o=y;var g=n.defer(),h=g.promise,p=h.$$promises={},c=r({},e),ft=1+w.length/3,rt=!1;if(u(o.$$failure))return tt(o.$$failure),h;for(o.$$inheritedValues&&k(c,b(o.$$inheritedValues,nt)),r(p,o.$$promises),o.$$values?(rt=k(c,b(o.$$values,nt)),h.$$inheritedValues=b(o.$$values,nt),d()):(o.$$inheritedValues&&(h.$$inheritedValues=b(o.$$inheritedValues,nt)),o.then(d,tt)),l=0,ut=w.length;l<ut;l+=3)e.hasOwnProperty(w[l])?d():et(w[l],w[l+1],w[l+2]);return h}};this.resolve=function(n,t,i,r){return this.study(n)(t,i,r)}}function ut(n,t,i){this.fromConfig=function(n,t,i){return u(n.template)?this.fromString(n.template,t):u(n.templateUrl)?this.fromUrl(n.templateUrl,t):u(n.templateProvider)?this.fromProvider(n.templateProvider,t,i):null};this.fromString=function(n,t){return o(n)?n(t):n};this.fromUrl=function(i,r){return o(i)&&(i=i(r)),i==null?null:n.get(i,{cache:t,headers:{Accept:"text/html"}}).then(function(n){return n.data})};this.fromProvider=function(n,t,r){return i.invoke(n,null,r||{params:t})}}function a(n,t,u){function nt(t,i,r,u){if(g.push(t),d[t])return d[t];if(!/^\w+(-+\w+)*(?:\[\])?$/.test(t))throw new Error("Invalid parameter name '"+t+"' in pattern '"+n+"'");if(b[t])throw new Error("Duplicate parameter name '"+t+"' in pattern '"+n+"'");return b[t]=new c.Param(t,i,r,u),b[t]}function tt(n,t,i){var r=["",""],u=n.replace(/[\\\[\]\^$*+?.()|{}]/g,"\\$&");if(!t)return u;switch(i){case!1:r=["(",")"];break;case!0:r=["?(",")?"];break;default:r=["("+i+"|",")?"]}return u+r[0]+t+r[1]}function it(i,r){var f,u,e,s,h;return f=i[2]||i[3],h=t.params[f],e=n.substring(o,i.index),u=r?i[4]:i[4]||(i[1]=="*"?".*":null),s=c.type(u||"string")||w(c.type("string"),{pattern:new RegExp(u)}),{id:f,regexp:u,segment:e,type:s,cfg:h}}var f,l,e,s,k;t=r({params:{}},v(t)?t:{});var a=/([:*])([\w\[\]]+)|\{([\w\[\]]+)(?:\:((?:[^{}\\]+|\\.|\{(?:[^{}\\]+|\\.)*\})+))?\}/g,rt=/([:]?)([\w\[\]-]+)|\{([\w\[\]-]+)(?:\:((?:[^{}\\]+|\\.|\{(?:[^{}\\]+|\\.)*\})+))?\}/g,y="^",o=0,h,p=this.segments=[],d=u?u.params:{},b=this.params=u?u.params.$$new():new c.ParamSet,g=[];for(this.source=n;h=a.exec(n);){if(f=it(h,!1),f.segment.indexOf("?")>=0)break;l=nt(f.id,f.type,f.cfg,"path");y+=tt(f.segment,l.type.pattern.source,l.squash);p.push(f.segment);o=a.lastIndex}if(e=n.substring(o),s=e.indexOf("?"),s>=0){if(k=this.sourceSearch=e.substring(s),e=e.substring(0,s),this.sourcePath=n.substring(0,o+s),k.length>0)for(o=0;h=rt.exec(k);)f=it(h,!0),l=nt(f.id,f.type,f.cfg,"search"),o=a.lastIndex}else this.sourcePath=n,this.sourceSearch="";y+=tt(e)+(t.strict===!1?"/?":"")+"$";p.push(e);this.regexp=new RegExp(y,t.caseInsensitive?"i":i);this.prefix=p[0];this.$$paramNames=g}function s(n){r(this,n)}function ft(){function rt(n){return n!=null?n.toString().replace(/\//g,"%2F"):n}function ht(n){return n!=null?n.toString().replace(/%2F/g,"/"):n}function ct(n){return this.pattern.test(n)}function lt(){return{strict:g,caseInsensitive:d}}function ot(n){return o(n)||l(n)&&o(n[n.length-1])}function st(){while(it.length){var i=it.shift();if(i.pattern)throw new Error("You cannot override a type's .pattern at runtime.");t.extend(n[i.name],b.invoke(i.def))}}function k(n){r(this,n||{})}c=this;var d=!1,g=!0,tt=!1;var n={},ut=!0,it=[],b,et={string:{encode:rt,decode:ht,is:ct,pattern:/[^/]*/},int:{encode:rt,decode:function(n){return parseInt(n,10)},is:function(n){return u(n)&&this.decode(n.toString())===n},pattern:/\d+/},bool:{encode:function(n){return n?1:0},decode:function(n){return parseInt(n,10)!==0},is:function(n){return n===!0||n===!1},pattern:/0|1/},date:{encode:function(n){return this.is(n)?[n.getFullYear(),("0"+(n.getMonth()+1)).slice(-2),("0"+n.getDate()).slice(-2)].join("-"):i},decode:function(n){if(this.is(n))return n;var t=this.capture.exec(n);return t?new Date(t[1],t[2]-1,t[3]):i},is:function(n){return n instanceof Date&&!isNaN(n.valueOf())},equals:function(n,t){return this.is(n)&&this.is(t)&&n.toISOString()===t.toISOString()},pattern:/[0-9]{4}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[1-2][0-9]|3[0-1])/,capture:/([0-9]{4})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])/},json:{encode:t.toJson,decode:t.fromJson,is:t.isObject,equals:t.equals,pattern:/[^/]*/},any:{encode:t.identity,decode:t.identity,is:t.identity,equals:t.equals,pattern:/.*/}};ft.$$getDefaultValue=function(n){if(!ot(n.value))return n.value;if(!b)throw new Error("Injectable functions cannot be called at configuration time");return b.invoke(n.value)};this.caseInsensitive=function(n){return u(n)&&(d=n),d};this.strictMode=function(n){return u(n)&&(g=n),g};this.defaultSquashPolicy=function(n){if(!u(n))return tt;if(n!==!0&&n!==!1&&!e(n))throw new Error("Invalid squash policy: "+n+". Valid policies: false, true, arbitrary-string");return tt=n,n};this.compile=function(n,t){return new a(n,r(lt(),t))};this.isMatcher=function(n){if(!v(n))return!1;var t=!0;return f(a.prototype,function(i,r){o(i)&&(t=t&&u(n[r])&&o(n[r]))}),t};this.type=function(t,i,f){if(!u(i))return n[t];if(n.hasOwnProperty(t))throw new Error("A type named '"+t+"' has already been defined.");return n[t]=new s(r({name:t},i)),f&&(it.push({name:t,def:f}),ut||st()),this};f(et,function(t,i){n[i]=new s(r({name:i},t))});n=w(n,{});this.$get=["$injector",function(t){return b=t,ut=!1,st(),f(et,function(t,i){n[i]||(n[i]=new s(t))}),this}];this.Param=function(t,f,o,c){function it(n){var t=v(n)?p(n):[],i=h(t,"value")===-1&&h(t,"type")===-1&&h(t,"squash")===-1&&h(t,"array")===-1;return i&&(n={value:n}),n.$$fn=ot(n.value)?n.value:function(){return n.value},n}function rt(i,r,u){if(i.type&&r)throw new Error("Param '"+t+"' has two type configurations.");return r?r:i.type?i.type instanceof s?i.type:new s(i.type):u==="config"?n.any:n.string}function ut(){var n={array:c==="search"?"auto":!1},i=t.match(/\[\]$/)?{array:!0}:{};return r(n,i,o).array}function ft(n,t){var i=n.squash;if(!t||i===!1)return!1;if(!u(i)||i==null)return tt;if(i===!0||e(i))return i;throw new Error("Invalid squash policy: '"+i+"'. Valid policies: false, true, or arbitrary string");}function et(n,t,r,u){var f,o,s=[{from:"",to:r||t?i:""},{from:null,to:r||t?i:""}];return f=l(n.replace)?n.replace:[],e(u)&&f.push({from:u,to:i}),o=y(f,function(n){return n.from}),nt(s,function(n){return h(o,n.from)===-1}).concat(f)}function st(){if(!b)throw new Error("Injectable functions cannot be called at configuration time");return b.invoke(o.$$fn)}function ht(n){function t(n){return function(t){return t.from===n}}function i(n){var i=y(nt(d.replace,t(n)),function(n){return n.to});return i.length?i[0]:n}return n=i(n),u(n)?d.type.decode(n):st()}function ct(){return"{Param:"+t+" "+f+" squash: '"+k+"' optional: "+w+"}"}var d=this,a;o=it(o);f=rt(o,f,c);a=ut();f=a?f.$asArray(a,c==="search"):f;f.name!=="string"||a||c!=="path"||o.value!==i||(o.value="");var w=o.value!==i,k=ft(o,w),g=et(o,a,w,k);r(this,{id:t,type:f,location:c,array:a,squash:k,replace:g,isOptional:w,value:ht,dynamic:i,config:o,toString:ct})};k.prototype={$$new:function(){return w(this,r(new k,{$$parent:this}))},$$keys:function(){for(var t=[],i=[],n=this,r=p(k.prototype);n;)i.push(n),n=n.$$parent;return i.reverse(),f(i,function(n){f(p(n),function(n){h(t,n)===-1&&h(r,n)===-1&&t.push(n)})}),t},$$values:function(n){var t={},i=this;return f(i.$$keys(),function(r){t[r]=i[r].value(n&&n[r])}),t},$$equals:function(n,t){var i=!0,r=this;return f(r.$$keys(),function(u){var f=n&&n[u],e=t&&t[u];r[u].type.equals(f,e)||(i=!1)}),i},$$validates:function(n){var t=!0,u,i,r,e=this;return f(this.$$keys(),function(f){r=e[f];i=n[f];u=!i&&r.isOptional;t=t&&(u||!!r.type.is(i))}),t},$$parent:i};this.ParamSet=k}function et(n,f){function p(n){var t=/^\^((?:\\[^a-zA-Z0-9]|[^\\\[\]\^$*+?.()|{}]+)*)/.exec(n.source);return t!=null?t[1].replace(/\\(.)/g,"$1"):""}function w(n,t){return n.replace(/\$(\$|\d{1,2})/,function(n,i){return t[i==="$"?0:Number(i)]})}function v(n,t,i){if(!i)return!1;var r=n.invoke(t,t,{$match:i});return u(r)?r:!0}function y(r,u,f,o){function b(n,t,i){return v==="/"?n:t?v.slice(0,-1)+n:i?v.slice(1)+n:n}function p(n){function o(n){var t=n(f,r);return t?(e(t)&&r.replace().url(t),!0):!1}var u,c,t;if(!n||!n.defaultPrevented){if(u=l&&r.url()===l,l=i,u)return!0;for(c=s.length,t=0;t<c;t++)if(o(s[t]))return;h&&o(h)}}function w(){return c=c||u.$on("$locationChangeSuccess",p)}var v=o.baseHref(),y=r.url(),l;return a||w(),{sync:function(){p()},listen:function(){return w()},update:function(n){if(n){y=r.url();return}r.url()!==y&&(r.url(y),r.replace())},push:function(n,t,u){r.url(n.format(t||{}));l=u&&u.$$avoidResync?r.url():i;u&&u.replace&&r.replace()},href:function(i,u,f){var o,e,h,s;return i.validates(u)?(o=n.html5Mode(),t.isObject(o)&&(o=o.enabled),e=i.format(u),f=f||{},o||e===null||(e="#"+n.hashPrefix()+e),e=b(e,o,f.absolute),!f.absolute||!e)?e:(h=!o&&e?"/":"",s=r.port(),s=s===80||s===443?"":":"+s,[r.protocol(),"://",r.host(),s,h,e].join("")):null}}}var s=[],h=null,a=!1,c;this.rule=function(n){if(!o(n))throw new Error("'rule' must be a function");return s.push(n),this};this.otherwise=function(n){if(e(n)){var t=n;n=function(){return t}}else if(!o(n))throw new Error("'rule' must be a function");return h=n,this};this.when=function(n,t){var i,u=e(t),c,s,h;if(e(n)&&(n=f.compile(n)),!u&&!o(t)&&!l(t))throw new Error("invalid 'handler' in when()");c={matcher:function(n,t){return u&&(i=f.compile(t),t=["$match",function(n){return i.format(n)}]),r(function(i,r){return v(i,t,n.exec(r.path(),r.search()))},{prefix:e(n.prefix)?n.prefix:""})},regex:function(n,t){if(n.global||n.sticky)throw new Error("when() RegExp must not be global or sticky");return u&&(i=t,t=["$match",function(n){return w(i,n)}]),r(function(i,r){return v(i,t,n.exec(r.path()))},{prefix:p(n)})}};s={matcher:f.isMatcher(n),regex:n instanceof RegExp};for(h in s)if(s[h])return this.rule(c[h](n,t));throw new Error("invalid 'what' in when()");};this.deferIntercept=function(n){n===i&&(n=!0);a=n};this.$get=y;y.$inject=["$location","$rootScope","$injector","$browser"]}function ot(n,s){function ot(n){return n.indexOf(".")===0||n.indexOf("^")===0}function k(n,t){var s;if(!n)return i;var h=e(n),u=h?n:n.name,c=ot(u);if(c){if(!t)throw new Error("No reference point given for path '"+u+"'");t=k(t);for(var r=u.split("."),f=0,l=r.length,o=t;f<l;f++){if(r[f]===""&&f===0){o=t;continue}if(r[f]==="^"){if(!o.parent)throw new Error("Path '"+u+"' not valid for state '"+t.name+"'");o=o.parent;continue}break}r=r.slice(f).join(".");u=o.name+(o.name&&r?".":"")+r}return(s=tt[u],s&&(h||!h&&(s===n||s.self===n)))?s:i}function st(n,t){rt[n]||(rt[n]=[]);rt[n].push(t)}function ht(n){for(var t=rt[n]||[];t.length;)ut(t.shift())}function ut(t){var i,u,r;if(t=w(t,{self:t,resolve:t.resolve||{},toString:function(){return this.name}}),i=t.name,!e(i)||i.indexOf("@")>=0)throw new Error("State must have a valid name");if(tt.hasOwnProperty(i))throw new Error("State '"+i+"'' is already defined");if(u=i.indexOf(".")!==-1?i.substring(0,i.lastIndexOf(".")):e(t.parent)?t.parent:v(t.parent)&&e(t.parent.name)?t.parent.name:"",u&&!tt[u])return st(u,t.self);for(r in b)o(b[r])&&(t[r]=b[r](t,b.$delegates[r]));return tt[i]=t,!t[ft]&&t.url&&n.when(t.url,["$match","$stateParams",function(n,i){a.$current.navigable==t&&d(n,i)||a.transitionTo(t,n,{inherit:!0,location:!1})}]),ht(i),t}function ct(n){return n.indexOf("*")>-1}function lt(n){var t=n.split("."),i=a.$current.name.split("."),r,u;if(t[0]==="**"&&(i=i.slice(h(i,t[1])),i.unshift("**")),t[t.length-1]==="**"&&(i.splice(h(i,t[t.length-2])+1,Number.MAX_VALUE),i.push("**")),t.length!=i.length)return!1;for(r=0,u=t.length;r<u;r++)t[r]==="*"&&(i[r]="*");return i.join("")===t.join("")}function at(n,t){return e(n)&&!u(t)?b[n]:!o(t)||!e(n)?this:(b[n]&&!b.$delegates[n]&&(b.$delegates[n]=b[n]),b[n]=t,this)}function vt(n,t){return v(n)?t=n:t.name=n,ut(t),this}function et(n,s,h,c,v,b,rt){function ht(t,i,r,u){var e=n.$broadcast("$stateNotFound",t,i,r),f;return e.defaultPrevented?(rt.update(),et):e.retry?u.$retry?(rt.update(),ot):(f=a.transition=s.when(e.retry),f.then(function(){return f!==a.transition?ut:(t.options.$retry=!0,a.transitionTo(t.to,t.toParams,t.options))},function(){return et}),rt.update(),f):null}function at(n,i,r,u,e,a){var w=r?i:g(n.params.$$keys(),i),y={$stateParams:w},p;return e.resolve=v.resolve(n.resolve,y,e.resolve,n),p=[e.resolve.then(function(n){e.globals=n})],u&&p.push(u),f(n.views,function(i,r){var u=i.resolve&&i.resolve!==n.resolve?i.resolve:{};u.$template=[function(){return h.load(r,{view:i,locals:y,params:w,notify:a.notify})||""}];p.push(v.resolve(u,y,e.resolve,n).then(function(f){if(o(i.controllerProvider)||l(i.controllerProvider)){var s=t.extend({},u,y);f.$$controller=c.invoke(i.controllerProvider,null,s)}else f.$$controller=i.controller;f.$$state=n;f.$$controllerAs=i.controllerAs;e[r]=f}))}),s.all(p).then(function(){return e})}var ut=s.reject(new Error("transition superseded")),st=s.reject(new Error("transition prevented")),et=s.reject(new Error("transition aborted")),ot=s.reject(new Error("transition failed"));return nt.locals={resolve:null,globals:{$stateParams:{}}},a={params:{},current:nt.self,$current:nt,transition:null},a.reload=function(){return a.transitionTo(a.current,b,{reload:!0,inherit:!1,notify:!0})},a.go=function(n,t,i){return a.transitionTo(n,t,r({inherit:!0,relative:a.$current},i))},a.transitionTo=function(t,i,f){var d,vt,et,tt,ct;i=i||{};f=r({location:!0,inherit:!1,relative:null,notify:!0,reload:!1,$retry:!1},f||{});var l=a.$current,p=a.params,lt=l.path,bt,e=k(t,f.relative);if(!u(e)){if(d={to:t,toParams:i,options:f},vt=ht(d,l.self,p,f),vt)return vt;if(t=d.to,i=d.toParams,f=d.options,e=k(t,f.relative),!u(e)){if(!f.relative)throw new Error("No such state '"+t+"'");throw new Error("Could not resolve '"+t+"' from state '"+f.relative+"'");}}if(e[ft])throw new Error("Cannot transition to abstract state '"+t+"'");if(f.inherit&&(i=it(b,i||{},a.$current,e)),!e.params.$$validates(i))return ot;i=e.params.$$values(i);t=e;var v=t.path,o=0,h=v[o],y=nt.locals,pt=[];if(!f.reload)while(h&&h===lt[o]&&h.ownParams.$$equals(i,p))y=pt[o]=h.locals,o++,h=v[o];if(yt(t,l,y,f))return t.self.reloadOnSearch!==!1&&rt.update(),a.transition=null,s.when(a.current);if(i=g(t.params.$$keys(),i||{}),f.notify&&n.$broadcast("$stateChangeStart",t.self,i,l.self,p).defaultPrevented)return rt.update(),st;for(et=s.when(y),tt=o;tt<v.length;tt++,h=v[tt])y=pt[tt]=w(y),et=at(h,i,h===t,et,y,f);return ct=a.transition=et.then(function(){var r,u,e;if(a.transition!==ct)return ut;for(r=lt.length-1;r>=o;r--)e=lt[r],e.self.onExit&&c.invoke(e.self.onExit,e.self,e.locals.globals),e.locals=null;for(r=o;r<v.length;r++)u=v[r],u.locals=pt[r],u.self.onEnter&&c.invoke(u.self.onEnter,u.self,u.locals.globals);return a.transition!==ct?ut:(a.$current=t,a.current=t.self,a.params=i,wt(a.params,b),a.transition=null,f.location&&t.navigable&&rt.push(t.navigable.url,t.navigable.locals.globals.$stateParams,{$$avoidResync:!0,replace:f.location==="replace"}),f.notify&&n.$broadcast("$stateChangeSuccess",t.self,i,l.self,p),rt.update(!0),a.current)},function(r){return a.transition!==ct?ut:(a.transition=null,bt=n.$broadcast("$stateChangeError",t.self,i,l.self,p,r),bt.defaultPrevented||rt.update(),s.reject(r))})},a.is=function(n,t,f){f=r({relative:a.$current},f||{});var e=k(n,f.relative);return u(e)?a.$current!==e?!1:t?d(e.params.$$values(t),b):!0:i},a.includes=function(n,t,f){if(f=r({relative:a.$current},f||{}),e(n)&&ct(n)){if(!lt(n))return!1;n=a.$current.name}var o=k(n,f.relative);return u(o)?u(a.$current.includes[o.name])?t?d(o.params.$$values(t),b,p(t)):!0:!1:i},a.href=function(n,t,f){var e,o;return(f=r({lossy:!0,inherit:!0,absolute:!1,relative:a.$current},f||{}),e=k(n,f.relative),!u(e))?null:(f.inherit&&(t=it(b,t||{},a.$current,e)),o=e&&f.lossy?e.navigable:e,!o||o.url===i||o.url===null)?null:rt.href(o.url,g(e.params.$$keys(),t||{}),{absolute:f.absolute})},a.get=function(n,t){if(arguments.length===0)return y(p(tt),function(n){return tt[n].self});var i=k(n,t||a.$current);return i&&i.self?i.self:null},a}function yt(n,t,i,r){if(n===t&&(i===t.locals&&!r.reload||n.self.reloadOnSearch===!1))return!0}var nt,tt={},a,rt={},ft="abstract",b={parent:function(n){if(u(n.parent)&&n.parent)return k(n.parent);var t=/^(.+)\.[^.]+$/.exec(n.name);return t?k(t[1]):nt},data:function(n){return n.parent&&n.parent.data&&(n.data=n.self.data=r({},n.parent.data,n.data)),n.data},url:function(n){var t=n.url,i={params:n.params||{}};if(e(t))return t.charAt(0)=="^"?s.compile(t.substring(1),i):(n.parent.navigable||nt).url.concat(t,i);if(!t||s.isMatcher(t))return t;throw new Error("Invalid url '"+t+"' in state '"+n+"'");},navigable:function(n){return n.url?n:n.parent?n.parent.navigable:null},ownParams:function(n){var t=n.url&&n.url.params||new c.ParamSet;return f(n.params||{},function(n,i){t[i]||(t[i]=new c.Param(i,null,n,"config"))}),t},params:function(n){return n.parent&&n.parent.params?r(n.parent.params.$$new(),n.ownParams):new c.ParamSet},views:function(n){var t={};return f(u(n.views)?n.views:{"":n},function(i,r){r.indexOf("@")<0&&(r+="@"+n.parent.name);t[r]=i}),t},path:function(n){return n.parent?n.parent.path.concat(n):[]},includes:function(n){var t=n.parent?r({},n.parent.includes):{};return t[n.name]=!0,t},$delegates:{}};nt=ut({name:"",url:"^",views:null,abstract:!0});nt.navigable=null;this.decorator=at;this.state=vt;this.$get=et;et.$inject=["$rootScope","$q","$view","$injector","$resolve","$stateParams","$urlRouter","$location","$urlMatcherFactory"]}function st(){function n(n,t){return{load:function(i,u){var f;return u=r({template:null,controller:null,view:null,locals:null,notify:!0,async:!0,params:{}},u),u.view&&(f=t.fromConfig(u.view,u.params,u.locals)),f&&u.notify&&n.$broadcast("$viewContentLoading",u),f}}}this.$get=n;n.$inject=["$rootScope","$templateFactory"]}function kt(){var n=!1;this.useAnchorScroll=function(){n=!0};this.$get=["$anchorScroll","$timeout",function(t,i){return n?t:function(n){i(function(){n[0].scrollIntoView()},0,!1)}}]}function ht(n,i,r,u){function s(){return i.has?function(n){return i.has(n)?i.get(n):null}:function(n){try{return i.get(n)}catch(t){return null}}}function h(n,t){var r=function(){return{enter:function(n,t,i){t.after(n);i()},leave:function(n,t){n.remove();t()}}},i;return e?{enter:function(n,t,i){var r=e.enter(n,null,t,i);r&&r.then&&r.then(i)},leave:function(n,t){var i=e.leave(n,t);i&&i.then&&i.then(t)}}:f?(i=f&&f(t,n),{enter:function(n,t,r){i.enter(n,null,t);r()},leave:function(n,t){i.leave(n);t()}}):r()}var o=s(),f=o("$animator"),e=o("$animate");return{restrict:"ECA",terminal:!0,priority:400,transclude:"element",compile:function(i,f,e){return function(i,f,o){function b(){c&&(c.remove(),c=null);s&&(s.$destroy(),s=null);l&&(p.leave(l,function(){c=null}),c=l,l=null)}function v(h){var c,v=lt(i,o,f,u),d=v&&n.$current&&n.$current.locals[v],k;(h||d!==y)&&(c=i.$new(),y=n.$current.locals[v],k=e(c,function(n){p.enter(n,f,function(){s&&s.$emit("$viewContentAnimationEnded");(t.isDefined(a)&&!a||i.$eval(a))&&r(n)});b()}),l=k,s=c,s.$emit("$viewContentLoaded"),s.$eval(w))}var c,l,s,y,w=o.onload||"",a=o.autoscroll,p=h(o,i);i.$on("$stateChangeSuccess",function(){v(!1)});i.$on("$viewContentLoading",function(){v(!1)});v(!0)}}}}function ct(n,t,i,r){return{restrict:"ECA",priority:-400,compile:function(u){var f=u.html();return function(u,e,o){var c=i.$current,l=lt(u,o,e,r),s=c&&c.locals[l],a,h;s&&(e.data("$uiView",{name:l,state:s.$$state}),e.html(s.$template?s.$template:f),a=n(e.contents()),s.$$controller&&(s.$scope=u,h=t(s.$$controller,s),s.$$controllerAs&&(u[s.$$controllerAs]=h),e.data("$ngControllerController",h),e.children().data("$ngControllerController",h)),a(u))}}}}function lt(n,t,i,r){var u=r(t.uiView||t.name||"")(n),f=i.inheritedData("$uiView");return u.indexOf("@")>=0?u:u+"@"+(f?f.state.name:"")}function dt(n,t){var r=n.match(/^\s*({[^}]*})\s*$/),i;if(r&&(n=t+"("+r[1]+")"),i=n.replace(/\n/g," ").match(/^([^(]+?)\s*(\((.*)\))?$/),!i||i.length!==4)throw new Error("Invalid state ref '"+n+"'");return{state:i[1],paramExpr:i[3]||null}}function at(n){var t=n.parent().inheritedData("$uiView");if(t&&t.state&&t.state.name)return t.state}function vt(n,i){var r=["location","inherit","reload"];return{restrict:"A",require:["?^uiSrefActive","?^uiSrefActiveEq"],link:function(u,f,e,o){var s=dt(e.uiSref,n.current.name),h=null,w=at(f)||n.$current,c=null,b=f.prop("tagName")==="A",v=f[0].nodeName==="FORM",k=v?"action":"href",y=!0,l={relative:w,inherit:!0},p=u.$eval(e.uiSrefOpts)||{},a;(t.forEach(r,function(n){n in p&&(l[n]=p[n])}),a=function(i){if(i&&(h=t.copy(i)),y){c=n.href(s.state,h,l);var r=o[1]||o[0];if(r&&r.$$setStateInfo(s.state,h),c===null)return y=!1,!1;e.$set(k,c)}},s.paramExpr&&(u.$watch(s.paramExpr,function(n){n!==h&&a(n)},!0),h=t.copy(u.$eval(s.paramExpr))),a(),v)||f.bind("click",function(t){var e=t.which||t.button,r,u;e>1||t.ctrlKey||t.metaKey||t.shiftKey||f.attr("target")||(r=i(function(){n.go(s.state,h,l)}),t.preventDefault(),u=b&&!c?1:0,t.preventDefault=function(){u--<=0&&i.cancel(r)})})}}}function tt(n,t,i){return{restrict:"A",controller:["$scope","$element","$attrs",function(t,r,u){function s(){h()?r.addClass(o):r.removeClass(o)}function h(){return typeof u.uiSrefActiveEq!="undefined"?f&&n.is(f.name,e):f&&n.includes(f.name,e)}var f,e,o;o=i(u.uiSrefActiveEq||u.uiSrefActive||"",!1)(t);this.$$setStateInfo=function(t,i){f=n.get(t,at(r));e=i;s()};t.$on("$stateChangeSuccess",s)}]}}function yt(n){var t=function(t){return n.is(t)};return t.$stateful=!0,t}function pt(n){var t=function(t){return n.includes(t)};return t.$stateful=!0,t}var u=t.isDefined,o=t.isFunction,e=t.isString,v=t.isObject,l=t.isArray,f=t.forEach,r=t.extend,wt=t.copy,c;t.module("ui.router.util",["ng"]);t.module("ui.router.router",["ui.router.util"]);t.module("ui.router.state",["ui.router.router","ui.router.util"]);t.module("ui.router",["ui.router.state"]);t.module("ui.router.compat",["ui.router"]);rt.$inject=["$q","$injector"];t.module("ui.router.util").service("$resolve",rt);ut.$inject=["$http","$templateCache","$injector"];t.module("ui.router.util").service("$templateFactory",ut);a.prototype.concat=function(n,t){var i={caseInsensitive:c.caseInsensitive(),strict:c.strictMode(),squash:c.defaultSquashPolicy()};return new a(this.sourcePath+n+this.sourceSearch,r(i,t),this)};a.prototype.toString=function(){return this.source};a.prototype.exec=function(n,t){function a(n){function t(n){return n.split("").reverse().join("")}function i(n){return n.replace(/\\-/,"-")}var r=t(n).split(/-(?!\\)/),u=y(r,t);return y(u,i).reverse()}var o=this.regexp.exec(n),f,u;if(!o)return null;t=t||{};var s=this.parameters(),l=s.length,c=this.segments.length-1,h={},i,e,r;if(c!==o.length-1)throw new Error("Unbalanced capture group in route '"+this.source+"'");for(i=0;i<c;i++){for(r=s[i],f=this.params[r],u=o[i+1],e=0;e<f.replace;e++)f.replace[e].from===u&&(u=f.replace[e].to);u&&f.array===!0&&(u=a(u));h[r]=f.value(u)}for(;i<l;i++)r=s[i],h[r]=this.params[r].value(t[r]);return h};a.prototype.parameters=function(n){return u(n)?this.params[n]||null:this.$$paramNames};a.prototype.validates=function(n){return this.params.$$validates(n)};a.prototype.format=function(n){function g(n){return encodeURIComponent(n).replace(/-/g,function(n){return"%5C%"+n.charCodeAt(0).toString(16).toUpperCase()})}var s,w;n=n||{};var h=this.segments,c=this.parameters(),b=this.params;if(!this.validates(n))return null;for(var a=!1,k=h.length-1,d=c.length,i=h[0],r=0;r<d;r++){var nt=r<k,o=c[r],u=b[o],v=u.value(n[o]),p=u.isOptional&&u.type.equals(u.value(),v),f=p?u.squash:!1,t=u.type.encode(v);if(nt)s=h[r+1],f===!1?(t!=null&&(i+=l(t)?y(t,g).join("-"):encodeURIComponent(t)),i+=s):f===!0?(w=i.match(/\/$/)?/\/?(.*)/:/(.*)/,i+=s.match(w)[1]):e(f)&&(i+=f+s);else{if(t==null||p&&f!==!1)continue;l(t)||(t=[t]);t=y(t,encodeURIComponent).join("&"+o+"=");i+=(a?"&":"?")+(o+"="+t);a=!0}}return i};s.prototype.is=function(){return!0};s.prototype.encode=function(n){return n};s.prototype.decode=function(n){return n};s.prototype.equals=function(n,t){return n==t};s.prototype.$subPattern=function(){var n=this.pattern.toString();return n.substr(1,n.length-2)};s.prototype.pattern=/.*/;s.prototype.toString=function(){return"{Type:"+this.name+"}"};s.prototype.$asArray=function(n,t){function r(n,t){function r(n,t){return function(){return n[t].apply(n,arguments)}}function f(n){return l(n)?n:u(n)?[n]:[]}function o(n){switch(n.length){case 0:return i;case 1:return t==="auto"?n[0]:n;default:return n}}function s(n){return!n}function e(n,t){return function(i){i=f(i);var r=y(i,n);return t===!0?nt(r,s).length===0:o(r)}}function h(n){return function(t,i){var u=f(t),e=f(i),r;if(u.length!==e.length)return!1;for(r=0;r<u.length;r++)if(!n(u[r],e[r]))return!1;return!0}}this.encode=e(r(n,"encode"));this.decode=e(r(n,"decode"));this.is=e(r(n,"is"),!0);this.equals=h(r(n,"equals"));this.pattern=n.pattern;this.$arrayMode=t}if(!n)return this;if(n==="auto"&&!t)throw new Error("'auto' array mode is for query parameters only");return new r(this,n)};t.module("ui.router.util").provider("$urlMatcherFactory",ft);t.module("ui.router.util").run(["$urlMatcherFactory",function(){}]);et.$inject=["$locationProvider","$urlMatcherFactoryProvider"];t.module("ui.router.router").provider("$urlRouter",et);ot.$inject=["$urlRouterProvider","$urlMatcherFactoryProvider"];t.module("ui.router.state").value("$stateParams",{}).provider("$state",ot);st.$inject=[];t.module("ui.router.state").provider("$view",st);t.module("ui.router.state").provider("$uiViewScroll",kt);ht.$inject=["$state","$injector","$uiViewScroll","$interpolate"];ct.$inject=["$compile","$controller","$state","$interpolate"];t.module("ui.router.state").directive("uiView",ht);t.module("ui.router.state").directive("uiView",ct);vt.$inject=["$state","$timeout"];tt.$inject=["$state","$stateParams","$interpolate"];t.module("ui.router.state").directive("uiSref",vt).directive("uiSrefActive",tt).directive("uiSrefActiveEq",tt);yt.$inject=["$state"];pt.$inject=["$state"];t.module("ui.router.state").filter("isState",yt).filter("includedByState",pt)}(window,window.angular);
/*
//# sourceMappingURL=angular-ui-router.min.js.map
*/
///#source 1 1 /appnew/bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie.min.js
angular.module("pascalprecht.translate").factory("$translateCookieStorage",["$cookieStore",function(n){return{get:function(t){return n.get(t)},set:function(t,i){n.put(t,i)},put:function(t,i){n.put(t,i)}}}]);
/*
//# sourceMappingURL=angular-translate-storage-cookie.min.js.map
*/
///#source 1 1 /appnew/bower_components/angular-translate-storage-local/angular-translate-storage-local.min.js
angular.module("pascalprecht.translate").factory("$translateLocalStorage",["$window","$translateCookieStorage",function(n,t){var u=function(){var t;return{get:function(i){return t||(t=n.localStorage.getItem(i)),t},set:function(i,r){t=r;n.localStorage.setItem(i,r)},put:function(i,r){t=r;n.localStorage.setItem(i,r)}}}(),i="localStorage"in n,r;if(i){r="pascalprecht.translate.storageTest";try{n.localStorage!==null?(n.localStorage.setItem(r,"foo"),n.localStorage.removeItem(r),i=!0):i=!1}catch(f){i=!1}}return i?u:t}]);
/*
//# sourceMappingURL=angular-translate-storage-local.min.js.map
*/
///#source 1 1 /assets/js/Angularjs/angular-strap.min.js
angular.module("$strap.config",[]).value("$strapConfig",{});angular.module("$strap.filters",["$strap.config"]);angular.module("$strap.directives",["$strap.config"]);angular.module("$strap",["$strap.filters","$strap.directives","$strap.config"]);angular.module("$strap.directives").directive("bsAlert",["$parse","$timeout","$compile",function(n,t,i){return{restrict:"A",link:function(t,r,u){var o=n(u.bsAlert),s=o.assign,f=o(t),e;u.bsAlert?t.$watch(u.bsAlert,function(n,e){f=n;r.html((n.title?"<strong>"+n.title+"<\/strong>&nbsp;":"")+n.content||"");!n.closed||r.hide();i(r.contents())(t);(n.type||e.type)&&(e.type&&r.removeClass("alert-"+e.type),n.type&&r.addClass("alert-"+n.type));(angular.isUndefined(u.closeButton)||u.closeButton!=="0"&&u.closeButton!=="false")&&r.prepend('<button type="button" class="close" data-dismiss="alert">&times;<\/button>')},!0):(angular.isUndefined(u.closeButton)||u.closeButton!=="0"&&u.closeButton!=="false")&&r.prepend('<button type="button" class="close" data-dismiss="alert">&times;<\/button>');r.addClass("alert").alert();r.hasClass("fade")&&(r.removeClass("in"),setTimeout(function(){r.addClass("in")}));e=u.ngRepeat&&u.ngRepeat.split(" in ").pop();r.on("close",function(n){var i;e?(n.preventDefault(),r.removeClass("in"),i=function(){r.trigger("closed");t.$parent&&t.$parent.$apply(function(){for(var r=e.split("."),n=t.$parent,i=0;i<r.length;++i)n&&(n=n[r[i]]);n&&n.splice(t.$index,1)})},$.support.transition&&r.hasClass("fade")?r.on($.support.transition.end,i):i()):f&&(n.preventDefault(),r.removeClass("in"),i=function(){r.trigger("closed");t.$apply(function(){f.closed=!0})},$.support.transition&&r.hasClass("fade")?r.on($.support.transition.end,i):i())})}}}]);angular.module("$strap.directives").directive("bsButton",["$parse","$timeout",function(n){return{restrict:"A",require:"?ngModel",link:function(t,i,r,u){var f,e;if(u&&(i.parent('[data-toggle="buttons-checkbox"], [data-toggle="buttons-radio"]').length||i.attr("data-toggle","button"),f=!!t.$eval(r.ngModel),f&&i.addClass("active"),t.$watch(r.ngModel,function(n,t){var r=!!n,u=!!t;r!==u?$.fn.button.Constructor.prototype.toggle.call(e):r&&!f&&i.addClass("active")})),!i.hasClass("btn"))i.on("click.button.data-api",function(){i.button("toggle")});i.button();e=i.data("button");e.toggle=function(){if(!u)return $.fn.button.Constructor.prototype.toggle.call(this);var r=i.parent('[data-toggle="buttons-radio"]');r.length?(i.siblings("[ng-model]").each(function(i,r){n($(r).attr("ng-model")).assign(t,!1)}),t.$digest(),u.$modelValue||(u.$setViewValue(!u.$modelValue),t.$digest())):t.$apply(function(){u.$setViewValue(!u.$modelValue)})}}}}]).directive("bsButtonsCheckbox",["$parse",function(){return{restrict:"A",require:"?ngModel",compile:function(n){n.attr("data-toggle","buttons-checkbox").find("a, button").each(function(n,t){$(t).attr("bs-button","")})}}}]).directive("bsButtonsRadio",["$timeout",function(n){return{restrict:"A",require:"?ngModel",compile:function(t,i){return t.attr("data-toggle","buttons-radio"),i.ngModel||t.find("a, button").each(function(n,t){$(t).attr("bs-button","")}),function(t,i,r,u){if(u){n(function(){i.find("[value]").button().filter('[value="'+u.$viewValue+'"]').addClass("active")});i.on("click.button.data-api",function(n){t.$apply(function(){u.$setViewValue($(n.target).closest("button").attr("value"))})});t.$watch(r.ngModel,function(n,u){if(n!==u){var f=i.find('[value="'+t.$eval(r.ngModel)+'"]');f.length&&f.button("toggle")}})}}}}}]);angular.module("$strap.directives").directive("bsButtonSelect",["$parse","$timeout",function(n){return{restrict:"A",require:"?ngModel",link:function(t,i,r,u){var s=n(r.bsButtonSelect),c=s.assign,f,h,e,o;u&&(i.text(t.$eval(r.ngModel)),t.$watch(r.ngModel,function(n){i.text(n)}));i.bind("click",function(){f=s(t);h=u?t.$eval(r.ngModel):i.text();e=f.indexOf(h);o=e>f.length-2?f[0]:f[e+1];t.$apply(function(){i.text(o);u&&u.$setViewValue(o)})})}}}]);angular.module("$strap.directives").directive("bsDatepicker",["$timeout","$strapConfig",function(n,t){var i=/(iP(a|o)d|iPhone)/g.test(navigator.userAgent),r=function(n){return n=n||"en",{"/":"[\\/]","-":"[-]",".":"[.]"," ":"[\\s]",dd:"(?:(?:[0-2]?[0-9]{1})|(?:[3][01]{1}))",d:"(?:(?:[0-2]?[0-9]{1})|(?:[3][01]{1}))",mm:"(?:[0]?[1-9]|[1][012])",m:"(?:[0]?[1-9]|[1][012])",DD:"(?:"+$.fn.datepicker.dates[n].days.join("|")+")",D:"(?:"+$.fn.datepicker.dates[n].daysShort.join("|")+")",MM:"(?:"+$.fn.datepicker.dates[n].months.join("|")+")",M:"(?:"+$.fn.datepicker.dates[n].monthsShort.join("|")+")",yyyy:"(?:(?:[1]{1}[0-9]{1}[0-9]{1}[0-9]{1})|(?:[2]{1}[0-9]{3}))(?![[0-9]])",yy:"(?:(?:[0-9]{1}[0-9]{1}))(?![[0-9]])"}},u=function(n,t){var u=n,f=r(t),i;return i=0,angular.forEach(f,function(n,t){u=u.split(t).join("${"+i+"}");i++}),i=0,angular.forEach(f,function(n){u=u.split("${"+i+"}").join(n);i++}),new RegExp("^"+u+"$",["i"])};return{restrict:"A",require:"?ngModel",link:function(n,r,f,e){var s=angular.extend({autoclose:!0},t.datepicker||{}),h=f.dateType||s.type||"date",l;angular.forEach(["format","weekStart","calendarWeeks","startDate","endDate","daysOfWeekDisabled","autoclose","startView","minViewMode","todayBtn","todayHighlight","keyboardNavigation","language","forceParse"],function(n){angular.isDefined(f[n])&&(s[n]=f[n])});var o=s.language||"en",a=f.dateFormat||s.format||$.fn.datepicker.dates[o]&&$.fn.datepicker.dates[o].format||"mm/dd/yyyy",c=i?"yyyy-mm-dd":a,v=u(c,o);if(e&&(e.$formatters.unshift(function(n){return h==="date"&&angular.isString(n)&&n?$.fn.datepicker.DPGlobal.parseDate(n,$.fn.datepicker.DPGlobal.parseFormat(a),o):n}),e.$parsers.unshift(function(n){return n?h==="date"&&angular.isDate(n)?(e.$setValidity("date",!0),n):angular.isString(n)&&v.test(n)?(e.$setValidity("date",!0),i)?new Date(n):h==="string"?n:$.fn.datepicker.DPGlobal.parseDate(n,$.fn.datepicker.DPGlobal.parseFormat(c),o):(e.$setValidity("date",!1),undefined):(e.$setValidity("date",!0),null)}),e.$render=function(){if(i){var n=e.$viewValue?$.fn.datepicker.DPGlobal.formatDate(e.$viewValue,$.fn.datepicker.DPGlobal.parseFormat(c),o):"";return r.val(n),n}return e.$viewValue||r.val(""),r.datepicker("update",e.$viewValue)}),i)r.prop("type","date").css("-webkit-appearance","textfield");else{if(e)r.on("changeDate",function(t){n.$apply(function(){e.$setViewValue(h==="string"?r.val():t.date)})});r.datepicker(angular.extend(s,{format:c,language:o}));n.$on("$destroy",function(){var n=r.data("datepicker");n&&(n.picker.remove(),r.data("datepicker",null))})}if(l=r.siblings('[data-toggle="datepicker"]'),l.length)l.on("click",function(){r.trigger("focus")})}}}]);angular.module("$strap.directives").directive("bsDropdown",["$parse","$compile","$timeout",function(n,t,i){var r=function(n,t){return t||(t=['<ul class="dropdown-menu" role="menu" aria-labelledby="drop1">',"<\/ul>"]),angular.forEach(n,function(n,i){if(n.divider)return t.splice(i+1,0,'<li class="divider"><\/li>');var u="<li"+(n.submenu&&n.submenu.length?' class="dropdown-submenu"':"")+'><a tabindex="-1" ng-href="'+(n.href||"")+'"'+(n.click?'" ng-click="'+n.click+'"':"")+(n.target?'" target="'+n.target+'"':"")+(n.method?'" data-method="'+n.method+'"':"")+">"+(n.text||"")+"<\/a>";n.submenu&&n.submenu.length&&(u+=r(n.submenu).join("\n"));u+="<\/li>";t.splice(i+1,0,u)}),t};return{restrict:"EA",scope:!0,link:function(u,f,e){var s=n(e.bsDropdown),o=s(u);i(function(){!angular.isArray(o);var n=angular.element(r(o).join(""));n.insertAfter(f);t(f.next("ul.dropdown-menu"))(u)});f.addClass("dropdown-toggle").attr("data-toggle","dropdown")}}}]);angular.module("$strap.directives").factory("$modal",["$rootScope","$compile","$http","$timeout","$q","$templateCache","$strapConfig",function(n,t,i,r,u,f,e){return function(o){function s(o){var h=angular.extend({show:!0},e.modal,o),s=h.scope?h.scope:n.$new(),c=h.template;return u.when(f.get(c)||i.get(c,{cache:!0}).then(function(n){return n.data})).then(function(n){var u=c.replace(".html","").replace(/[\/|\.|:]/g,"-")+"-"+s.$id,i=$('<div class="modal hide" tabindex="-1"><\/div>').attr("id",u).addClass("fade").html(n);h.modalClass&&i.addClass(h.modalClass);$("body").append(i);r(function(){t(i)(s)});s.$modal=function(n){i.modal(n)};angular.forEach(["show","hide"],function(n){s[n]=function(){i.modal(n)}});s.dismiss=s.hide;angular.forEach(["show","shown","hide","hidden"],function(n){i.on(n,function(t){s.$emit("modal-"+n,t)})});i.on("shown",function(){$("input[autofocus]",i).first().trigger("focus")});i.on("hidden",function(){h.persist||s.$destroy()});return s.$on("$destroy",function(){i.remove()}),i.modal(h),i})}return new s(o)}}]).directive("bsModal",["$q","$modal",function(n,t){return{restrict:"A",scope:!0,link:function(i,r,u){var f={template:i.$eval(u.bsModal),persist:!0,show:!1,scope:i};angular.forEach(["modalClass","backdrop","keyboard"],function(n){angular.isDefined(u[n])&&(f[n]=u[n])});n.when(t(f)).then(function(n){r.attr("data-target","#"+n.attr("id")).attr("data-toggle","modal")})}}}]);angular.module("$strap.directives").directive("bsNavbar",["$location",function(n){return{restrict:"A",link:function(t,i){t.$watch(function(){return n.path()},function(n){$("li[data-match-route]",i).each(function(t,i){var r=angular.element(i),u=r.attr("data-match-route"),f=new RegExp("^"+u+"$",["i"]);f.test(n)?r.addClass("active"):r.removeClass("active")})})}}}]);angular.module("$strap.directives").directive("bsPopover",["$parse","$compile","$http","$timeout","$q","$templateCache",function(n,t,i,r,u,f){$("body").on("keyup",function(n){n.keyCode===27&&$(".popover.in").each(function(){$(this).popover("hide")})});return{restrict:"A",scope:!0,link:function(r,e,o){var c=n(o.bsPopover),l=c.assign,s=c(r),h={};angular.isObject(s)&&(h=s);u.when(h.content||f.get(s)||i.get(s,{cache:!0})).then(function(n){if(angular.isObject(n)&&(n=n.data),!!o.unique)e.on("show",function(){$(".popover.in").each(function(){var n=$(this),t=n.data("popover");t&&!t.$element.is(e)&&n.popover("hide")})});!o.hide||r.$watch(o.hide,function(n,t){n?i.hide():n!==t&&i.show()});e.popover(angular.extend({},h,{content:n,html:!0}));var i=e.data("popover");i.hasContent=function(){return this.getTitle()||n};i.getPosition=function(){var n=$.fn.popover.Constructor.prototype.getPosition.apply(this,arguments);return t(this.$tip)(r),r.$digest(),this.$tip.data("popover",this),n};r.$popover=function(n){i(n)};angular.forEach(["show","hide"],function(n){r[n]=function(){i[n]()}});r.dismiss=r.hide;angular.forEach(["show","shown","hide","hidden"],function(n){e.on(n,function(t){r.$emit("popover-"+n,t)})})})}}}]);angular.module("$strap.directives").directive("bsSelect",["$timeout",function(n){return{restrict:"A",require:"?ngModel",link:function(t,i,r,u){var f=t.$eval(r.bsSelect)||{};n(function(){i.selectpicker(f);i.next().removeClass("ng-scope")});u&&t.$watch(r.ngModel,function(n,t){n!==t&&i.selectpicker("refresh")})}}}]);angular.module("$strap.directives").directive("bsTabs",["$parse","$compile","$timeout",function(n,t,i){return{restrict:"A",require:"?ngModel",priority:0,scope:!0,template:'<div class="tabs"><ul class="nav nav-tabs"><li ng-repeat="pane in panes" ng-class="{active:pane.active}"><a data-target="#{{pane.id}}" data-index="{{$index}}" data-toggle="tab">{{pane.title}}<\/a><\/li><\/ul><div class="tab-content" ng-transclude><\/div>',replace:!0,transclude:!0,compile:function(){return function(t,r,u,f){var s=n(u.bsTabs),v=s.assign,y=s(t);t.panes=[];var l=r.find("ul.nav-tabs"),h=r.find("div.tab-content"),a=0,o,c,e;if(i(function(){h.find("[data-title], [data-tab]").each(function(n){var i=angular.element(this);o="tab-"+t.$id+"-"+n;c=i.data("title")||i.data("tab");e=!e&&i.hasClass("active");i.attr("id",o).addClass("tab-pane");u.fade&&i.addClass("fade");t.panes.push({id:o,title:c,content:this.innerHTML,active:e})});t.panes.length&&!e&&(h.find(".tab-pane:first-child").addClass("active"+(u.fade?" in":"")),t.panes[0].active=!0)}),f){r.on("show",function(n){var i=$(n.target);t.$apply(function(){f.$setViewValue(i.data("index"))})});t.$watch(u.ngModel,function(n){angular.isUndefined(n)||(a=n,setTimeout(function(){var t=$(l[0].querySelectorAll("li")[n*1]);t.hasClass("active")||t.children("a").tab("show")}))})}}}}}]);angular.module("$strap.directives").directive("bsTimepicker",["$timeout",function(n){var t="((?:(?:[0-1][0-9])|(?:[2][0-3])|(?:[0-9])):(?:[0-5][0-9])(?::[0-5][0-9])?(?:\\s?(?:am|AM|pm|PM))?)";return{restrict:"A",require:"?ngModel",link:function(i,r,u,f){var s,e,o;if(f){r.on("changeTime.timepicker",function(){n(function(){f.$setViewValue(r.val())})});s=new RegExp("^"+t+"$",["i"]);f.$parsers.unshift(function(n){if(!n||s.test(n))return f.$setValidity("time",!0),n;f.$setValidity("time",!1);return})}if(r.attr("data-toggle","timepicker"),r.parent().addClass("bootstrap-timepicker"),r.timepicker(),e=r.data("timepicker"),o=r.siblings('[data-toggle="timepicker"]'),o.length)o.on("click",$.proxy(e.showWidget,e))}}}]);angular.module("$strap.directives").directive("bsTooltip",["$parse","$compile",function(n){return{restrict:"A",scope:!0,link:function(t,i,r){var f=n(r.bsTooltip),o=f.assign,u=f(t),e;if(t.$watch(r.bsTooltip,function(n,t){n!==t&&(u=n)}),!!r.unique)i.on("show",function(){$(".tooltip.in").each(function(){var n=$(this),t=n.data("tooltip");t&&!t.$element.is(i)&&n.tooltip("hide")})});i.tooltip({title:function(){return angular.isFunction(u)?u.apply(null,arguments):u},html:!0});e=i.data("tooltip");e.show=function(){var n=$.fn.tooltip.Constructor.prototype.show.apply(this,arguments);return this.tip().data("tooltip",this),n};t._tooltip=function(n){i.tooltip(n)};t.hide=function(){i.tooltip("hide")};t.show=function(){i.tooltip("show")};t.dismiss=t.hide}}}]);angular.module("$strap.directives").directive("bsTypeahead",["$parse",function(n){return{restrict:"A",require:"?ngModel",link:function(t,i,r,u){var o=n(r.bsTypeahead),s=o.assign,f=o(t),e;t.$watch(r.bsTypeahead,function(n,t){n!==t&&(f=n)});i.attr("data-provide","typeahead");i.typeahead({source:function(){return angular.isFunction(f)?f.apply(null,arguments):f},minLength:r.minLength||1,items:r.items,updater:function(n){return u&&t.$apply(function(){u.$setViewValue(n)}),t.$emit("typeahead-updated",n),n}});e=i.data("typeahead");e.lookup=function(){var n;return(this.query=this.$element.val()||"",this.query.length<this.options.minLength)?this.shown?this.hide():this:(n=$.isFunction(this.source)?this.source(this.query,$.proxy(this.process,this)):this.source,n?this.process(n):this)};!r.matchAll||(e.matcher=function(){return!0});r.minLength==="0"&&setTimeout(function(){i.on("focus",function(){i.val().length===0&&setTimeout(i.typeahead.bind(i,"lookup"),200)})})}}}]);
/*
//# sourceMappingURL=angular-strap.min.js.map
*/
///#source 1 1 /assets/js/Angularjs/AngularUI/select2.js
/**
 * Enhanced Select2 Dropmenus
 *
 * @AJAX Mode - When in this mode, your value will be an object (or array of objects) of the data used by Select2
 *     This change is so that you do not have to do an additional query yourself on top of Select2's own query
 * @params [options] {object} The configuration options passed to $.fn.select2(). Refer to the documentation
 */
angular.module('ui.select2', []).value('uiSelect2Config', {}).directive('uiSelect2', ['uiSelect2Config', '$timeout', function (uiSelect2Config, $timeout) {
  var options = {};
  if (uiSelect2Config) {
    angular.extend(options, uiSelect2Config);
  }
  return {
    require: 'ngModel',
    priority: 1,
    compile: function (tElm, tAttrs) {
      var watch,
        repeatOption,
        repeatAttr,
        isSelect = tElm.is('select'),
        isMultiple = angular.isDefined(tAttrs.multiple);

      // Enable watching of the options dataset if in use
      if (tElm.is('select')) {
        repeatOption = tElm.find( 'optgroup[ng-repeat], optgroup[data-ng-repeat], option[ng-repeat], option[data-ng-repeat]');

        if (repeatOption.length) {
          repeatAttr = repeatOption.attr('ng-repeat') || repeatOption.attr('data-ng-repeat');
          watch = jQuery.trim(repeatAttr.split('|')[0]).split(' ').pop();
        }
      }

      return function (scope, elm, attrs, controller) {
        // instance-specific options
        var opts = angular.extend({}, options, scope.$eval(attrs.uiSelect2));

        /*
        Convert from Select2 view-model to Angular view-model.
        */
        var convertToAngularModel = function(select2_data) {
          var model;
          if (opts.simple_tags) {
            model = [];
            angular.forEach(select2_data, function(value, index) {
              model.push(value.id);
            });
          } else {
            model = select2_data;
          }
          return model;
        };

        /*
        Convert from Angular view-model to Select2 view-model.
        */
        var convertToSelect2Model = function(angular_data) {
          var model = [];
          if (!angular_data) {
            return model;
          }

          if (opts.simple_tags) {
            model = [];
            angular.forEach(
              angular_data,
              function(value, index) {
                model.push({'id': value, 'text': value});
              });
          } else {
            model = angular_data;
          }
          return model;
        };

        if (isSelect) {
          // Use <select multiple> instead
          delete opts.multiple;
          delete opts.initSelection;
        } else if (isMultiple) {
          opts.multiple = true;
        }

        if (controller) {
          // Watch the model for programmatic changes
           scope.$watch(tAttrs.ngModel, function(current, old) {
            if (!current) {
              return;
            }
            if (current === old) {
              return;
            }
            controller.$render();
          }, true);
          controller.$render = function () {
            if (isSelect) {
              elm.select2('val', controller.$viewValue);
            } else {
              if (opts.multiple) {
                var viewValue = controller.$viewValue;
                if (angular.isString(viewValue)) {
                  viewValue = viewValue.split(',');
                }
                elm.select2(
                  'data', convertToSelect2Model(viewValue));
              } else {
                if (angular.isObject(controller.$viewValue)) {
                  elm.select2('data', controller.$viewValue);
                } else if (!controller.$viewValue) {
                  elm.select2('data', null);
                } else {
                  elm.select2('val', controller.$viewValue);
                }
              }
            }
          };

          // Watch the options dataset for changes
          if (watch) {
            scope.$watch(watch, function (newVal, oldVal, scope) {
              if (angular.equals(newVal, oldVal)) {
                return;
              }
              // Delayed so that the options have time to be rendered
              $timeout(function () {
                elm.select2('val', controller.$viewValue);
                // Refresh angular to remove the superfluous option
                elm.trigger('change');
                if(newVal && !oldVal && controller.$setPristine) {
                  controller.$setPristine(true);
                }
              });
            });
          }

          // Update valid and dirty statuses
          controller.$parsers.push(function (value) {
            var div = elm.prev();
            div
              .toggleClass('ng-invalid', !controller.$valid)
              .toggleClass('ng-valid', controller.$valid)
              .toggleClass('ng-invalid-required', !controller.$valid)
              .toggleClass('ng-valid-required', controller.$valid)
              .toggleClass('ng-dirty', controller.$dirty)
              .toggleClass('ng-pristine', controller.$pristine);
            return value;
          });

          if (!isSelect) {
            // Set the view and model value and update the angular template manually for the ajax/multiple select2.
            elm.bind("change", function (e) {
              e.stopImmediatePropagation();
              
              if (scope.$$phase || scope.$root.$$phase) {
                return;
              }
              scope.$apply(function () {
                controller.$setViewValue(
                  convertToAngularModel(elm.select2('data')));
              });
            });

            if (opts.initSelection) {
              var initSelection = opts.initSelection;
              opts.initSelection = function (element, callback) {
                initSelection(element, function (value) {
                  var isPristine = controller.$pristine;
                  controller.$setViewValue(convertToAngularModel(value));
                  callback(value);
                  if (isPristine) {
                    controller.$setPristine();
                  }
                  elm.prev().toggleClass('ng-pristine', controller.$pristine);
                });
              };
            }
          }
        }

        elm.bind("$destroy", function() {
          elm.select2("destroy");
        });

        attrs.$observe('disabled', function (value) {
          elm.select2('enable', !value);
        });

        attrs.$observe('readonly', function (value) {
          elm.select2('readonly', !!value);
        });

        if (attrs.ngMultiple) {
          scope.$watch(attrs.ngMultiple, function(newVal) {
            attrs.$set('multiple', !!newVal);
            elm.select2(opts);
          });
        }

        // Initialize the plugin late so that the injected DOM does not disrupt the template compiler
        $timeout(function () {
          elm.select2(opts);

          // Set initial value - I'm not sure about this but it seems to need to be there
          elm.select2('data', controller.$modelValue);
          // important!
          controller.$render();

          // Not sure if I should just check for !isSelect OR if I should check for 'tags' key
          if (!opts.initSelection && !isSelect) {
            var isPristine = controller.$pristine;
            controller.$setViewValue(
              convertToAngularModel(elm.select2('data'))
            );
            if (isPristine) {
              controller.$setPristine();
            }
            elm.prev().toggleClass('ng-pristine', controller.$pristine);
          }
        });
      };
    }
  };
}]);

///#source 1 1 /assets/js/Angularjs/ng-tags-input.min.js
/*! ngTagsInput v2.0.1 License: MIT */!function(){"use strict";function a(){var a={};return{on:function(b,c){return b.split(" ").forEach(function(b){a[b]||(a[b]=[]),a[b].push(c)}),this},trigger:function(b,c){return angular.forEach(a[b],function(a){a.call(null,c)}),this}}}function b(a,b){return a=a||[],a.length>0&&!angular.isObject(a[0])&&a.forEach(function(c,d){a[d]={},a[d][b]=c}),a}function c(a,b,c){for(var d=null,e=0;e<a.length;e++)if(a[e][c].toLowerCase()===b[c].toLowerCase()){d=a[e];break}return d}function d(a,b,c){var d=b.replace(/([.?*+^$[\]\\(){}|-])/g,"\\$1");return a.replace(new RegExp(d,"gi"),c)}var e={backspace:8,tab:9,enter:13,escape:27,space:32,up:38,down:40,comma:188},f=angular.module("ngTagsInput",[]);f.directive("tagsInput",["$timeout","$document","tagsInputConfig",function(d,f,g){function h(a,b){var d,e,f,g={};return d=function(b){return b[a.displayProperty]},e=function(b,c){b[a.displayProperty]=c},f=function(b){var e=d(b);return e.length>=a.minLength&&e.length<=(a.maxLength||e.length)&&a.allowedTagsPattern.test(e)&&!c(g.items,b,a.displayProperty)},g.items=[],g.addText=function(a){var b={};return e(b,a),g.add(b)},g.add=function(c){var h=d(c).trim();return a.replaceSpacesWithDashes&&(h=h.replace(/\s/g,"-")),e(c,h),f(c)?(g.items.push(c),b.trigger("tag-added",{$tag:c})):b.trigger("invalid-tag",{$tag:c}),c},g.remove=function(a){var c=g.items.splice(a,1)[0];return b.trigger("tag-removed",{$tag:c}),c},g.removeLast=function(){var b,c=g.items.length-1;return a.enableEditingLastTag||g.selected?(g.selected=null,b=g.remove(c)):g.selected||(g.selected=g.items[c]),b},g}return{restrict:"E",require:"ngModel",scope:{tags:"=ngModel",onTagAdded:"&",onTagRemoved:"&"},replace:!1,transclude:!0,templateUrl:"ngTagsInput/tags-input.html",controller:["$scope","$attrs","$element",function(b,c,d){g.load("tagsInput",b,c,{placeholder:[String,"Add a tag"],tabindex:[Number],removeTagSymbol:[String,String.fromCharCode(215)],replaceSpacesWithDashes:[Boolean,!0],minLength:[Number,3],maxLength:[Number],addOnEnter:[Boolean,!0],addOnSpace:[Boolean,!1],addOnComma:[Boolean,!0],addOnBlur:[Boolean,!0],allowedTagsPattern:[RegExp,/.+/],enableEditingLastTag:[Boolean,!1],minTags:[Number],maxTags:[Number],displayProperty:[String,"text"],allowLeftoverText:[Boolean,!1],addFromAutocompleteOnly:[Boolean,!1]}),b.events=new a,b.tagList=new h(b.options,b.events),this.registerAutocomplete=function(){var a=d.find("input");return a.on("keydown",function(a){b.events.trigger("input-keydown",a)}),{addTag:function(a){return b.tagList.add(a)},focusInput:function(){a[0].focus()},getTags:function(){return b.tags},getOptions:function(){return b.options},on:function(a,c){return b.events.on(a,c),this}}}}],link:function(a,c,g,h){var i=[e.enter,e.comma,e.space,e.backspace],j=a.tagList,k=a.events,l=a.options,m=c.find("input");k.on("tag-added",a.onTagAdded).on("tag-removed",a.onTagRemoved).on("tag-added",function(){a.newTag.text=""}).on("tag-added tag-removed",function(){h.$setViewValue(a.tags)}).on("invalid-tag",function(){a.newTag.invalid=!0}).on("input-change",function(){j.selected=null,a.newTag.invalid=null}).on("input-focus",function(){h.$setValidity("leftoverText",!0)}).on("input-blur",function(){l.addFromAutocompleteOnly||(l.addOnBlur&&j.addText(a.newTag.text),h.$setValidity("leftoverText",l.allowLeftoverText?!0:!a.newTag.text))}),a.newTag={text:"",invalid:null},a.getDisplayText=function(a){return a[l.displayProperty].trim()},a.track=function(a){return a[l.displayProperty]},a.newTagChange=function(){k.trigger("input-change",a.newTag.text)},a.$watch("tags",function(c){a.tags=b(c,l.displayProperty),j.items=a.tags}),a.$watch("tags.length",function(a){h.$setValidity("maxTags",angular.isUndefined(l.maxTags)||a<=l.maxTags),h.$setValidity("minTags",angular.isUndefined(l.minTags)||a>=l.minTags)}),m.on("keydown",function(b){if(!b.isImmediatePropagationStopped||!b.isImmediatePropagationStopped()){var c,d,f=b.keyCode,g=b.shiftKey||b.altKey||b.ctrlKey||b.metaKey,h={};if(!g&&-1!==i.indexOf(f))if(h[e.enter]=l.addOnEnter,h[e.comma]=l.addOnComma,h[e.space]=l.addOnSpace,c=!l.addFromAutocompleteOnly&&h[f],d=!c&&f===e.backspace&&0===a.newTag.text.length,c)j.addText(a.newTag.text),a.$apply(),b.preventDefault();else if(d){var k=j.removeLast();k&&l.enableEditingLastTag&&(a.newTag.text=k[l.displayProperty]),a.$apply(),b.preventDefault()}}}).on("focus",function(){a.hasFocus||(a.hasFocus=!0,k.trigger("input-focus"),a.$apply())}).on("blur",function(){d(function(){var b=f.prop("activeElement"),d=b===m[0],e=c[0].contains(b);(d||!e)&&(a.hasFocus=!1,k.trigger("input-blur"))})}),c.find("div").on("click",function(){m[0].focus()})}}}]),f.directive("autoComplete",["$document","$timeout","$sce","tagsInputConfig",function(a,f,g,h){function i(a,d){var e,g,h,i={};return g=function(a,b){return a.filter(function(a){return!c(b,a,d.tagsInput.displayProperty)})},i.reset=function(){h=null,i.items=[],i.visible=!1,i.index=-1,i.selected=null,i.query=null,f.cancel(e)},i.show=function(){i.selected=null,i.visible=!0},i.load=function(c,j){return c.length<d.minLength?void i.reset():(f.cancel(e),void(e=f(function(){i.query=c;var e=a({$query:c});h=e,e.then(function(a){e===h&&(a=b(a.data||a,d.tagsInput.displayProperty),a=g(a,j),i.items=a.slice(0,d.maxResultsToShow),i.items.length>0?i.show():i.reset())})},d.debounceDelay,!1)))},i.selectNext=function(){i.select(++i.index)},i.selectPrior=function(){i.select(--i.index)},i.select=function(a){0>a?a=i.items.length-1:a>=i.items.length&&(a=0),i.index=a,i.selected=i.items[a]},i.reset(),i}function j(a){return a.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;")}return{restrict:"E",require:"^tagsInput",scope:{source:"&"},templateUrl:"ngTagsInput/auto-complete.html",link:function(b,c,f,k){var l,m,n,o,p,q=[e.enter,e.tab,e.escape,e.up,e.down];h.load("autoComplete",b,f,{debounceDelay:[Number,100],minLength:[Number,3],highlightMatchedText:[Boolean,!0],maxResultsToShow:[Number,10]}),n=b.options,m=k.registerAutocomplete(),n.tagsInput=m.getOptions(),l=new i(b.source,n),o=function(a){return a[n.tagsInput.displayProperty]},b.suggestionList=l,b.addSuggestion=function(){var a=!1;return l.selected&&(m.addTag(l.selected),l.reset(),m.focusInput(),a=!0),a},b.highlight=function(a){var b=o(a);return b=j(b),n.highlightMatchedText&&(b=d(b,j(l.query),"<em>$&</em>")),g.trustAsHtml(b)},b.track=function(a){return o(a)},m.on("tag-added invalid-tag",function(){l.reset()}).on("input-change",function(a){a?l.load(a,m.getTags()):l.reset()}).on("input-keydown",function(a){var c,d;if(-1!==q.indexOf(a.keyCode)){var f=!1;a.stopImmediatePropagation=function(){f=!0,a.stopPropagation()},a.isImmediatePropagationStopped=function(){return f},l.visible&&(c=a.keyCode,d=!1,c===e.down?(l.selectNext(),d=!0):c===e.up?(l.selectPrior(),d=!0):c===e.escape?(l.reset(),d=!0):(c===e.enter||c===e.tab)&&(d=b.addSuggestion()),d&&(a.preventDefault(),a.stopImmediatePropagation(),b.$apply()))}}).on("input-blur",function(){l.reset()}),p=function(){l.visible&&(l.reset(),b.$apply())},a.on("click",p),b.$on("$destroy",function(){a.off("click",p)})}}}]),f.directive("tiTranscludeAppend",function(){return function(a,b,c,d,e){e(function(a){b.append(a)})}}),f.directive("tiAutosize",function(){return{restrict:"A",require:"ngModel",link:function(a,b,c,d){var e,f,g=3;e=angular.element('<span class="input"></span>'),e.css("display","none").css("visibility","hidden").css("width","auto").css("white-space","pre"),b.parent().append(e),f=function(a){var d,f=a;return angular.isString(f)&&0===f.length&&(f=c.placeholder),f&&(e.text(f),e.css("display",""),d=e.prop("offsetWidth"),e.css("display","none")),b.css("width",d?d+g+"px":""),a},d.$parsers.unshift(f),d.$formatters.unshift(f),c.$observe("placeholder",function(a){d.$modelValue||f(a)})}}}),f.provider("tagsInputConfig",function(){var a={},b={};this.setDefaults=function(b,c){return a[b]=c,this},this.setActiveInterpolation=function(a,c){return b[a]=c,this},this.$get=["$interpolate",function(c){var d={};return d[String]=function(a){return a},d[Number]=function(a){return parseInt(a,10)},d[Boolean]=function(a){return"true"===a.toLowerCase()},d[RegExp]=function(a){return new RegExp(a)},{load:function(e,f,g,h){f.options={},angular.forEach(h,function(h,i){var j,k,l,m,n;j=h[0],k=h[1],l=d[j],m=function(){var b=a[e]&&a[e][i];return angular.isDefined(b)?b:k},n=function(a){f.options[i]=a?l(a):m()},b[e]&&b[e][i]?g.$observe(i,function(a){n(a)}):n(g[i]&&c(g[i])(f.$parent))})}}}]}),f.run(["$templateCache",function(a){a.put("ngTagsInput/tags-input.html",'<div class="host" tabindex="-1" ti-transclude-append=""><div class="tags" ng-class="{focused: hasFocus}"><ul class="tag-list"><li class="tag-item" ng-repeat="tag in tagList.items track by track(tag)" ng-class="{ selected: tag == tagList.selected }"><span>{{getDisplayText(tag)}}</span> <a class="remove-button" ng-click="tagList.remove($index)">{{options.removeTagSymbol}}</a></li></ul><input class="input" tabindex="{{options.tabindex}}" ng-model="newTag.text" ng-change="newTagChange()" ng-trim="false" ng-class="{\'invalid-tag\': newTag.invalid}" ti-autosize=""></div></div>'),a.put("ngTagsInput/auto-complete.html",'<div class="autocomplete" ng-show="suggestionList.visible"><ul class="suggestion-list"><li class="suggestion-item" ng-repeat="item in suggestionList.items track by track(item)" ng-class="{selected: item == suggestionList.selected}" ng-click="addSuggestion()" ng-mouseenter="suggestionList.select($index)" ng-bind-html="highlight(item)"></li></ul></div>')}])}();
