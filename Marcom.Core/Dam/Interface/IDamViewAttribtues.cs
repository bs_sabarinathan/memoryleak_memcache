﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.DAM.Interface
{
    /// <summary>
    /// IDamViewAttribtues
    /// </summary>
    public interface IDamViewAttribtues
    {
        #region Public Properties
        int ID { get; set; }
        string DisplayName { get; set; }
        int SortOrder { get; set; }
        int Level { get; set; }
        int Type { get; set; }
        bool IsColumn { get; set; }
        bool IsToolTip { get; set; }
        #endregion
    }
}
