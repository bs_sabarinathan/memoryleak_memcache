

angular.module('app')
    /**
   * jQuery plugin config use ui-jq directive , config the js and css files that required
   * key: function name of the jQuery plugin
   * value: array of the css js file located
   */
  .constant('JQ_CONFIG', {

      screenfull: ['appnew/bower_components/screenfull/dist/screenfull.min.js'],
      TouchSpin: ['appnew/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',
                          'appnew/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'],
      tagsinput: ['appnew/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.js',
                          'appnew/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'],
      modalpopup: ['appnew/bower_components/bootstrap/dist/js/bootstrap.js'],
      bootbox: ['assets/js/UtilityFramework/bootbox.js'],
      tooltip: ['appnew/bower_components/Qtip/css/qtip2.css', 'appnew/bower_components/Qtip/plugin/jquery-qtip.js'],
      Plupload: ['assets/js/Plupload/browserplus-min.js', 'assets/js/Plupload/plupload.full.js'],

      videodev: ['assets/js/UtilityFramework/videodev.js', 'assets/css/video-js.css'],
      JqueryCustom: ['assets/js/Jquery/jquery-ui-1.10.3.custom.js'],
      Jqueryappear: ['assets/js/Scroll/jquery.appear.js'],
      AdjustWidth: ['assets/js/InHouse/jquery.AdjustWidth.js'],
      wizard: ['assets/js/Bootstrap/Fuelux/wizard.js'],
      checkbox: ['assets/js/Bootstrap/Fuelux/checkbox.js'],
      Canvas: ['assets/js/CanvasJS/canvasjs.min.js'],
      Html2Canvas: ['assets/js/html2canvas.js'],
      select2: ['assets/js/Select2/select2.js', 'assets/css/select2.css'],
      Pickcolor: ['assets/js/Pick-a-color/pick-a-color-1.1.4.js', 'assets/css/pick-a-color-1.1.4.css'],
      tinycolor: ['assets/js/Pick-a-color/tinycolor-0.9.14.min.js'],
      date: ['assets/js/UtilityFramework/date.js'],
      lodash: ['assets/js/UtilityFramework/lodash.js'],
      placeholders: ['assets/js/UtilityFramework/placeholders.min.js'],
      autoNumeric: ['assets/js/Jquery/Plugin/autoNumeric.js'],
      Redactor: ['assets/js/Redactor/redactor.js', 'assets/css/redactor.css'],
      jquerymask: ['assets/js/Jquery/Plugin/jquery.mask.min.js'],
      bootstrapeditable: ['assets/js/Bootstrap/Xeditable/bootstrap-editable.js', 'assets/css/bootstrap-editable.css'],
      datepicker: ['assets/js/Bootstrap/DatePicker/bootstrap-datepicker.js', 'assets/css/bootstrap-datepicker.css'],
      bootstrapnotify: ['assets/js/Bootstrap/bootstrap-notify.js', 'assets/css/bootstrap-notify.css'],
      scroll: ['assets/js/UtilityFramework/jquery-paged-scroll.js'],
      nod: ['assets/js/Nod/nod.js'],
      tablednd: ['assets/js/Jquery/Plugin/jquery.tablednd.js'],
      tipTip: ['assets/js/Jquery/Plugin/jquery.tipTip.minified.js', 'assets/css/tipTip.css'],
      jqueryQtip: ['assets/js/UtilityFramework/jquery.qtip.js'],
      Jcrop: ['assets/js/Jcrop/jquery.Jcrop.min.js', 'assets/css/jquery.Jcrop.css'],
      Gridster: ['assets/js/Gridster/jquery.gridster.js', 'assets/css/jquery.gridster.css'],
      multiselect: ['assets/js/UtilityFramework/bootstrap-multiselect.js', 'assets/css/bootstrap-multiselect.css', 'assets/css/multiselect.css'],
      multiselectpartial: ['assets/js/Bootstrap/bootstrap-multiselectpartial.js', 'assets/css/bootstrap-multiselect.css', 'assets/css/multiselect.css'],
      passwordstrength: ['assets/js/Jquery/Plugin/jquery.password-strength.js'],
      spectrum: ['assets/js/UtilityFramework/spectrum.js', 'assets/css/spectrum.css'],
      bootstrapbutton: ['assets/js/Bootstrap/bootstrap-button.js'],
      tpls: ['assets/js/Bootstrap/ui-bootstrap-tpls-0.11.0.js'],
      fontawesome: ['assets/css/font-awesome.css'],
      dashboard: ['assets/css/_dashboard.cs'],
      hack: ['assets/css/hack.css'],
      pace: ['assets/css/pace.css'],
      abn_tree: ['assets/css/abn_tree.css'],
      simple: ['assets/css/simple.css'],
      codemirror: ['assets/js/codemirror.js']


  }
  )
  // oclazyload config
  .config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
      // We configure ocLazyLoad to use the lib script.js as the async loader
      $ocLazyLoadProvider.config({
          debug: true,
          events: true,
          modules: [

              {
                  name: 'ui.grid',
                  files: [
                      'appnew/bower_components/angular-ui-grid/ui-grid.min.js',
                      'appnew/bower_components/angular-ui-grid/ui-grid.min.css',
                      'appnew/bower_components/angular-ui-grid/ui-grid.bootstrap.css'
                  ]
              },
              {
                  name: 'ui.select',
                  files: [
                      'appnew/bower_components/angular-ui-select/dist/select.min.js',
                      'appnew/bower_components/angular-ui-select/dist/select.min.css'
                  ]
              },

              {
                  name: 'toaster',
                  files: [
                      'appnew/bower_components/angularjs-toaster/toaster.js',
                      'appnew/bower_components/angularjs-toaster/toaster.css'
                  ]
              },
              {
                  name: 'smart-table',
                  files: [
                      'appnew/bower_components/angular-smart-table/dist/smart-table.min.js'
                  ]
              },
              {
                  name: 'dndLists',
                  files: [
                      'assets/js/Angularjs/angular-drag-and-drop-lists.js'
                  ]
              },
              {
                  name: 'ui.bootstrap',
                  files: [
                      'assets/js/Bootstrap/ui-bootstrap-tpls-0.11.0.js'
                  ]
              },
              {
                  name: 'angucomplete-alt',
                  files: [
                      'assets/css/angucomplete-alt.css',
                      'assets/js/Angularjs/angucomplete-alt.js'
                  ]
              },
                {
                    name: 'ngGrid',
                    files: [
                        'assets/js/ng-grid/ng-grid.js',
                        'assets/css/ng-grid.css'
                    ]
                },
                {
                    name: 'angular-accordion',
                    files: [
                        'assets/js/Angularjs/angular-accordion.js'
                    ]
                },
                {
                    name: 'adminalternate-directive',
                    files: [
                        'app/directives/adminalternate-directive.js'
                    ]
                }
          ]
      });
  }]);
