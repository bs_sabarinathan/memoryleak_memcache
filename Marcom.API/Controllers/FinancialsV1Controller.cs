﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BrandSystems.Marcom.Core.Report.Interface;
using BrandSystems.Marcom.Metadata.Interface;
using Marcom.API.Authorization;
using Marcom.API.Models;
using Marcom.API.Services;
using Newtonsoft.Json.Linq;
using Marcom.API.Services.Financials;
using Marcom.API.Models.Financials;
using System.Data;


namespace Marcom.API.Controllers
{
    [AuthenticateTokenAttribute]
    public class FinancialsV1Controller : ApiController
    {
        #region MemberVariables

        private FinancialsRepositoryV1 FinancialRepository;

        #endregion

        #region ConstructorLogic
        /// <summary>
        /// Constructer logic
        /// </summary>
        public FinancialsV1Controller()
        {
            this.FinancialRepository = new FinancialsRepositoryV1();
        }

        #endregion

        #region  API-Actions

        /// <summary>
        /// Fetch Purchase Order details for an Entity 
        /// </summary>
        [HttpGet]
        [Queryable]
        public dynamic GetPODetailsByEntityID(int ID)
        {
            try
            {
                return FinancialRepository.GetPODetailsByEntity(ID);
           }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        ///  Create new Purchase orders
        /// </summary>
        [HttpPost]
        public HttpResponseMessage CreatePurchaseOrders(JObject json)
        {
            try
            {
                bool createdstatus = false;
                if (FinancialRepository.IsExternalSourceAllowed() == true)
                {
                    createdstatus = FinancialRepository.CreatePO(json);
                }
                else
                {
                    var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.MethodNotAllowed, "Not linked with external source.");
                    return response;
                }

                if (createdstatus == true)
                {
                    var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.OK, "Created Successfully");
                    return response;
                }
                else
                {
                    var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.InternalServerError, "Error in PO creation");
                    return response;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        ///  Update existing Purchase orders
        /// </summary>
        [HttpPut]
        public HttpResponseMessage UpdatePurchaseOrders(JObject json)
        {
            try
            {
                bool createdstatus = false;
                if (FinancialRepository.IsExternalSourceAllowed() == true)
                {
                    createdstatus = FinancialRepository.UpdatePO(json);
                }
                else
                {
                    var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.MethodNotAllowed, "Not linked with external source.");
                    return response;
                }

                if (createdstatus == true)
                {
                    var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.OK, "Updated Successfully");
                    return response;
                }
                else
                {
                    var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.InternalServerError, "Error in PO updation");
                    return response;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        ///  Get Spent transactions for an Entity
        /// </summary>
        [HttpGet]
        [Queryable]
        public dynamic GetSpentDetailsByEntityID(int ID)
        {
            try
            {
                return FinancialRepository.GetSpentDetailsByEntity(ID);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        ///  Create new Spent transactions
        /// </summary>
        [HttpPost]
        public HttpResponseMessage CreateSpentTransactions(JObject json)
        {
            try
            {
                bool createdstatus = false;
                if (FinancialRepository.IsExternalSourceAllowed() == true)
                {
                    createdstatus = FinancialRepository.CreateSpentTransactions(json);
                }
                else
                {
                    var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.MethodNotAllowed, "Not linked with external source.");
                    return response;
                }

                if (createdstatus == true)
                {
                    var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.OK, "Created Successfully");
                    return response;
                }
                else
                {
                    var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.InternalServerError, "Error in Spent creation");
                    return response;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        ///  Update existing Spent transactions
        /// </summary>
        [HttpPut]
        public HttpResponseMessage UpdateSpentTransactions(JObject json)
        {
            try
            {
                bool createdstatus = false;
                if (FinancialRepository.IsExternalSourceAllowed() == true)
                {

                    createdstatus = FinancialRepository.UpdateSpentTransactions(json);
                }
                else
                {
                    var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.MethodNotAllowed, "Not linked with external source.");
                    return response;
                }
                if (createdstatus == true)
                {
                    var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.OK, "Updated Successfully");
                    return response;
                }
                else
                {
                    var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.InternalServerError, "Error in Updation");
                    return response;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Fetch Financial metadata details for an Entity 
        /// </summary>
        [HttpGet]
        [Queryable]
        public dynamic GetFinancialsByEntityID(int ID)
        {
            try
            {
                return FinancialRepository.GetFinanceDetailsByEntity(ID);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        ///  Update client specific financial metadata
        /// </summary>
        [HttpPut]
        public HttpResponseMessage UpdateFinancialsMetadata(JObject json)
        {
            try
            {
                bool createdstatus = false;
                if (FinancialRepository.IsExternalSourceAllowed() == true)
                {

                    createdstatus = FinancialRepository.UpdateFinancialMetadata(json);
                }
                else
                {
                    var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.MethodNotAllowed, "Not linked with external source.");
                    return response;
                }
                if (createdstatus == true)
                {
                    var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.OK, "Updated Successfully");
                    return response;
                }
                else
                {
                    var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.InternalServerError, "Error in Financial updation");
                    return response;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPut]
        [Queryable]
        public dynamic CurrencyConverter(JObject json)
        {
            try
            {
                return FinancialRepository.GetConvertedCurrencies(json);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
      
        //public JObject CurrencyConverter(JObject curr)
        //{

        //}


        #endregion

    }
}
