﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Managers.Proxy;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Access.Interface;
using BrandSystems.Marcom.Core.Planning;
using System.Collections;
using Newtonsoft.Json.Linq;

namespace BrandSystems.Marcom.Core.Interface.Managers
{
    public interface IPlanningManager
    {
        /// <summary>
        /// Intializes the imilestone.
        /// </summary>
        /// <param name="strBody">accepts string body containing imilestone objects </param>
        /// <returns>IMilestone</returns>
        IMilestone IntializeImilestone(string strBody);

        /// <summary>
        /// Creates the milestone.
        /// </summary>
        /// <param name="entityTypeId">The Entity Type Id.</param>
        /// <param name="name">The Name</param>
        /// <param name="attributes">List of attributes Data</param>
        /// <returns>last inserted id</returns>
        int CreateMilestone(int milestoneTypeId, string name, IList<IAttributeData> entityattributedata);


        /// <summary>
        /// Gets the milestone.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>IMilestone</returns>
        IMilestone GetMilestone(long id);

        /// <summary>
        /// Updates the milestone.
        /// </summary>
        /// <param name="milestoneTypeID">The MilestoneTypID</param>
        /// <param name="milestoneName">The MilestoneName</param>
        /// <param name="milestoneObj">The List of Milstone AttributeData</param>
        /// <param name="entityId">The EntityID</param>
        /// <returns>bool</returns>
        bool UpdateMilestone(int milestoneTypeID, string milstoneName, IList<IAttributeData> milestoneObj, int entityId);


        /// <summary>
        /// Updates the milestone.
        /// </summary>
        /// <param name="Id">The id.</param>
        /// <param name="Entityid">The entityid.</param>
        /// <param name="Name">The name.</param>
        /// <param name="Description">The description.</param>
        /// <param name="Status">The status.</param>
        /// <param name="DueDate">The due date.</param>
        /// <returns>bool</returns>
        bool UpdateMilestone(int Id, int Entityid, string Name, string Description, int Status, DateTimeOffset DueDate);

        /// <summary>
        /// Deletes the mile stone.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>bool</returns>
        bool DeleteMileStone(int id);

        /// <summary>
        /// Gets the milestone by id.
        /// </summary>
        /// <param name="id"> id.</param>
        /// <returns>IMilestone</returns>
        IList<IAttributeData> GetMilestoneById(int id);


        /// <summary>
        /// Deletes the activity releation type hierachy.
        /// </summary>
        /// <param name="parentactivityTypeId">The parentactivity type id.</param>
        /// <param name="childactivityTypeId">The childactivity type id.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <returns>bool</returns>
        bool DeleteActivityReleationTypeHierachy(int parentactivityTypeId, int childactivityTypeId, int sortOrder);

        /// <summary>
        /// Creates the entity color code.
        /// </summary>
        /// <param name="entitytypeId">The entitytype id.</param>
        /// <param name="colorCode">The color code.</param>
        /// <param name="attributeid">The attributeid.</param>
        /// <param name="optionid">The optionid.</param>
        /// <param name="Id">The id.</param>
        /// <returns>last inserted id</returns>
        int CreateEntityColorCode(int entitytypeId, string colorCode, int attributeid = 0, int optionid = 0, int Id = 0);

        int ownerchange(int EntityID);

        /// <summary>
        /// Deletes the entity color code.
        /// </summary>
        /// <param name="entitycolorcode">The entitycolorcode.</param>
        /// <returns>bool</returns>
        bool DeleteEntityColorCode(IEntityColorCode entitycolorcode);

        /// <summary>
        /// Deletes the entity color code.
        /// </summary>
        /// <param name="Id">The id.</param>
        /// <param name="EntityTypeid">The entity typeid.</param>
        /// <param name="Attributeid">The attributeid.</param>
        /// <param name="Optionid">The optionid.</param>
        /// <param name="ColorCode">The color code.</param>
        /// <returns>bool</returns>
        bool DeleteEntityColorCode(int Id, int EntityTypeid, int Attributeid, int Optionid, string ColorCode);

        /// <summary>
        /// Gets the entityPresentation code by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>
        /// IEntityPresentation
        /// </returns>
        IEntityPresentation GetPresentationByEntityId(int id);


        /// <summary>
        /// Creates the presentation.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <param name="PublishedOn">The published on.</param>
        /// <param name="content">The content.</param>
        /// <returns>IEntityPresentation</returns>
        int CreatePresentation(int entityId, DateTimeOffset PublishedOn, int[] entityList, string content = null);

        /// <summary>
        /// Adds the cost center.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <param name="costcenterId">The costcenter id.</param>
        /// <param name="plannedAmount">The planned amount.</param>
        /// <param name="requestedAmount">The requested amount.</param>
        /// <param name="approvedallocatedAmount">The approvedallocated amount.</param>
        /// <param name="approvedBudget">The approved budget.</param>
        /// <param name="commited">The commited.</param>
        /// <param name="spent">The spent.</param>
        /// <param name="approvedbudgetDate">The approvedbudget date.</param>
        /// <returns>int</returns>
        int AddCostCenter(int entityId, int costcenterId, decimal plannedAmount, decimal requestedAmount, decimal approvedallocatedAmount, decimal approvedBudget, decimal commited, decimal spent, DateTimeOffset approvedbudgetDate);

        /// <summary>
        /// Deletes the cost center.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <param name="costcenterId">The costcenter id.</param>
        /// <returns>bool</returns>
        bool DeleteCostCenter(int id);

        /// <summary>
        /// Creates the funding request.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <param name="costcenterId">The costcenter id.</param>
        /// <param name="amount">The amount.</param>
        /// <returns>last inserted id</returns>
        int CreateFundingRequest(int entityId, int costcenterId, decimal amount, string duedate, string comment);

        /// <summary>
        /// Updates the funding request.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <param name="costcenterId">The costcenter id.</param>
        /// <param name="state">The state.</param>
        /// <returns>bool</returns>
        bool UpdateFundingRequest(int entityId, int costcenterId, int state);

        /// <summary>
        /// Deletes the funding request.
        /// </summary>
        /// <param name="Id">The id.</param>
        /// <returns>bool</returns>
        bool DeleteFundingRequest(int Id);

        /// <summary>
        /// get funding request.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>IFundingRequests</returns>
        IList<IFundingRequest> getfundingRequestsByEntityID(int EntityId);


        /// <summary>
        /// Selects the objective by ID.
        /// </summary>
        /// <param name="ID">The ID.</param>
        /// <returns>IObjective</returns>
        IObjective SelectObjectiveByID(int ID);

        /// <summary>
        /// Updates the objective.
        /// </summary>
        /// <param name="objectiveData">The objective data.</param>
        /// <returns>bool</returns>
        bool UpdateObjective(IObjective objectiveData);

        /// <summary>
        /// Creates the units.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="caption">The caption.</param>
        /// <returns>last inserted id</returns>
        int CreateUnits(int id, string caption);

        /// <summary>
        /// Selects the units by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>IObjectiveUnit</returns>
        IObjectiveUnit SelectUnitsById(int id);

        /// <summary>
        /// Updateunitses the specified units.
        /// </summary>
        /// <param name="units">The units.</param>
        /// <returns>bool</returns>
        bool UpdateUnits(IObjectiveUnit units);

        /// <summary>
        /// Deletes the units by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>bool</returns>
        bool DeleteUnitsById(int id);

        /// <summary>
        /// Creates the ratings.
        /// </summary>
        /// <param name="objectiveid">The objectiveid.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <returns>IObjectiveRating</returns>
        int CreateRatings(int objectiveid, String caption, int sortOrder);

        /// <summary>
        /// Selects the ratings.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>IObjectiveRating</returns>
        IObjectiveRating SelectRatings(int id);

        /// <summary>
        /// Deletes the ratings.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>bool</returns>
        bool DeleteRatings(int id);

        /// <summary>
        /// Updates the presentation.
        /// </summary>
        /// <param name="presentation">The presentation.</param>
        /// <returns>IEntityPresentation</returns>
        bool UpdatePresentation(IEntityPresentation presentation);

        /// <summary>
        /// Updates the presentation.
        /// </summary>
        /// <param name="EntityId">The entity id.</param>
        /// <param name="PublishedOn">The published on.</param>
        /// <param name="Content">The content.</param>
        /// <returns>IEntityPresentation</returns>
        bool UpdatePresentation(int EntityId, DateTimeOffset PublishedOn, string Content);

        /// <summary>
        /// Publishes the this level.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <param name="PublishedOn">The published on.</param>
        /// <returns>bool</returns>
        bool PublishThisLevel(int entityId, DateTimeOffset PublishedOn);

        /// <summary>
        /// Creates the attachments.
        /// </summary>
        /// <param name="Entityid">The entityid.</param>
        /// <param name="Name">The name.</param>
        /// <param name="ActiveVersionNo">The active version no.</param>
        /// <param name="ActiveFileid">The active fileid.</param>
        /// <returns>last inserted id</returns>
        int CreateAttachments(int Entityid, String Name, int ActiveVersionNo, int ActiveFileid);

        /// <summary>
        /// Gets the attachments by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>IList<IAttachments></returns>
        IList<IAttachments> GetAttachmentsById(int id);

        /// <summary>
        /// Deletes the attachments.
        /// </summary>
        /// <param name="Attachments">The attachments.</param>
        /// <returns>bool</returns>
        bool DeleteAttachments(IAttachments Attachments);

        /// <summary>
        /// Deletes the attachments.
        /// </summary>
        /// <param name="Id">The id.</param>
        /// <param name="Entityid">The entityid.</param>
        /// <param name="Name">The name.</param>
        /// <param name="ActiveVersionNo">The active version no.</param>
        /// <param name="ActiveFileid">The active fileid.</param>
        /// <returns>bool</returns>
        bool DeleteAttachments(int Id, int Entityid, string Name, int ActiveVersionNo, int ActiveFileid);

        /// <summary>
        /// Creates the entity period.
        /// </summary>
        /// <param name="Entityid">The entityid.</param>
        /// <param name="Startdate">The startdate.</param>
        /// <param name="EndDate">The end date.</param>
        /// <param name="Description">The description.</param>
        /// <param name="SortOrder">The sort order.</param>
        /// <returns>int</returns>
        int CreateEntityPeriod(int Entityid, DateTime Startdate, DateTime EndDate, string Description, int SortOrder);

        /// <summary>
        /// Gets the entity period by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>IEntityPeriod</returns>
        IEntityPeriod GetEntityPeriodById(int id);

        /// <summary>
        /// Updates the entity period.
        /// </summary>
        /// <param name="EntityPeriod">The entity period.</param>
        /// <returns>bool</returns>
        bool UpdateEntityPeriod(IEntityPeriod EntityPeriod);

        /// <summary>
        /// Updates the entity period.
        /// </summary>
        /// <param name="Entityid">The entityid.</param>
        /// <param name="Startdate">The startdate.</param>
        /// <param name="EndDate">The end date.</param>
        /// <param name="Description">The description.</param>
        /// <param name="SortOrder">The sort order.</param>
        /// <returns>IEntityPeriod</returns>
        string UpdateEntityPeriod(DateTime Startdate, DateTime EndDate, string Description, int Id);

        /// <summary>
        /// Deletes the entity period.
        /// </summary>
        /// <param name="EntityPeriod">The entity period.</param>
        /// <returns>bool</returns>
        bool DeleteEntityPeriod(IEntityPeriod EntityPeriod);

        /// <summary>
        /// Deletes the entity period.
        /// </summary>
        /// <param name="Entityid">The entityid.</param>
        /// <param name="Startdate">The startdate.</param>
        /// <param name="EndDate">The end date.</param>
        /// <param name="Description">The description.</param>
        /// <param name="SortOrder">The sort order.</param>
        /// <returns>bool</returns>
        bool DeleteEntityPeriod(int Entityid, DateTime Startdate, DateTime EndDate, string Description, int SortOrder);

        /// <summary>
        /// Gets the entity color code by id.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>IEntityColorCode</returns>
        IEntityColorCode GetEntityColorCodeById(int id);

        /// <summary>
        /// Updates the entity color code.
        /// </summary>
        /// <param name="EntityColorCode">The entity color code.</param>
        /// <returns>bool</returns>
        bool UpdateEntityColorCode(IEntityColorCode EntityColorCode);

        /// <summary>
        /// Updates the entity color code.
        /// </summary>
        /// <param name="Id">The id.</param>
        /// <param name="EntityTypeid">The entity typeid.</param>
        /// <param name="Attributeid">The attributeid.</param>
        /// <param name="Optionid">The optionid.</param>
        /// <param name="ColorCode">The color code.</param>
        /// <returns>IEntityColorCode</returns>
        bool UpdateEntityColorCode(int Id, int EntityTypeid, int Attributeid, int Optionid, string ColorCode);

        /// <summary>
        /// Notification  milestone update.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <param name="entityName">Name of the entity.</param>
        /// <param name="entityAttributeId">The entity attribute id.</param>
        /// <param name="entityAttributeOldValue">The entity attribute old value.</param>
        /// <param name="attributeValue">The attribute value.</param>
        /// <param name="parentId">The parent id.</param>
        /// <returns>bool</returns>
        bool Notification_MilestoneUpdated(int entityId = 0, string entityName = "", int entityAttributeId = 0, string entityAttributeOldValue = "", string attributeValue = "", int parentId = 0);

        /// <summary>
        /// Notification milestone delete.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_MilestoneDelete(int entityId = 0);

        /// <summary>
        /// Notification milestone create.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_MilestoneCreate(int entityId);

        /// <summary>
        /// Notification additional objective create.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_AdditionalObjectiveCreate(int entityId);

        /// <summary>
        /// Notification cost center add.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_CostCenterAdd(int entityId);

        /// <summary>
        /// Notification entity attachment create.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_EntityAttachmentCreated(int entityId);

        /// <summary>
        /// Notification entity date insert.
        /// </summary>
        /// <param name="newval">The newval.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_EntityDateInsert(string newval, int entityId);

        /// <summary>
        /// Notification entity date delete.
        /// </summary>
        /// <param name="oldval">The oldval.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_EntityDateDelete(string oldval, int entityId);

        /// <summary>
        /// Notification task created.
        /// </summary>
        /// <param name="entitytypename">The entitytypename.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_TaskCreated(string entitytypename, int entityId);

        /// <summary>
        /// Notification entity create.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_EntityCreate(int entityId);

        /// <summary>
        /// Notification entity update.
        /// </summary>
        /// <param name="oldvalue">The oldvalue.</param>
        /// <param name="newvalue">The newvalue.</param>
        /// <param name="entityId">The entity id.</param>
        /// <param name="attributeName">Name of the attribute.</param>
        /// <returns>bool</returns>
        bool Notification_EntityUpdated(string oldvalue, string newvalue, int entityId, string attributeName);

        /// <summary>
        /// Notification entity delete.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_EntityDeleted(int entityId);

        /// <summary>
        /// Notification entity comment add.
        /// </summary>
        /// <param name="comment">The comment.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_EntityCommentAdded(string comment, int entityId);

        /// <summary>
        /// Notification task metadata update.
        /// </summary>
        /// <param name="attributename">The attributename.</param>
        /// <param name="oldvalue">The oldvalue.</param>
        /// <param name="newvalue">The newvalue.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_TaskMetadataUpdated(string attributename, string oldvalue, string newvalue, int entityId);

        /// <summary>
        /// Notification task member add.
        /// </summary>
        /// <param name="EntityTypeName">Name of the entity type.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_TaskMemberAdded(string EntityTypeName, int entityId);

        /// <summary>
        /// Notification task status changed.
        /// </summary>
        /// <param name="EntityTypeName">Name of the entity type.</param>
        /// <param name="Entitystate">The entitystate.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_TaskStatusChanged(string EntityTypeName, string Entitystate, int entityId);

        /// <summary>
        /// Notification entity state changed.
        /// </summary>
        /// <param name="oldvalue">The oldvalue.</param>
        /// <param name="newvalue">The newvalue.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_EntityStateChanged(string oldvalue, string newvalue, int entityId);

        /// <summary>
        /// Notification entity member add.
        /// </summary>
        /// <param name="users">The users.</param>
        /// <param name="role">The role.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_EntityMemberAdded(string users, string role, int entityId);

        /// <summary>
        /// Notification entity attachment delete.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_EntityAttachmentDeleted(int entityId);

        /// <summary>
        /// Notification entity member role update.
        /// </summary>
        /// <param name="users">The users.</param>
        /// <param name="oldvalue">The oldvalue.</param>
        /// <param name="newvalue">The newvalue.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_EntityMemberRoleUpdated(string users, string oldvalue, string newvalue, int entityId);

        /// <summary>
        /// Notification entity member removed.
        /// </summary>
        /// <param name="users">The users.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_EntityMemberRemoved(string users, int entityId);

        /// <summary>
        /// Notification entity duplicated.
        /// </summary>
        /// <param name="entitytypename">The entitytypename.</param>
        /// <param name="countofnos">The countofnos.</param>
        /// <param name="sublevels">The sublevels.</param>
        /// <param name="entityId">The entity id.</param>
        /// <param name="entityName">Name of the entity.</param>
        /// <returns>bool</returns>
        bool Notification_EntityDuplicated(string entitytypename, string countofnos, string sublevels, int entityId, string entityName);

        /// <summary>
        /// Notification fund request created.
        /// </summary>
        /// <param name="RequestedAmount">The requested amount.</param>
        /// <param name="CostcenterName">Name of the costcenter.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_FundRequestCreated(float RequestedAmount, string CostcenterName, int entityId);

        /// <summary>
        /// Notification released funds.
        /// </summary>
        /// <param name="ReleaseAmount">The release amount.</param>
        /// <param name="PathTemplate">The path template.</param>
        /// <param name="CostcenterName">Name of the costcenter.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_ReleasedFunds(float ReleaseAmount, string PathTemplate, string CostcenterName, int entityId);

        /// <summary>
        /// Notification costcenter assigned amount changed.
        /// </summary>
        /// <param name="oldval">The oldval.</param>
        /// <param name="newval">The newval.</param>
        /// <param name="CostcenterName">Name of the costcenter.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_CostcenterAssignedAmountChanged(string oldval, string newval, string CostcenterName, int entityId);

        /// <summary>
        /// Notification entity plan budget updated.
        /// </summary>
        /// <param name="oldval">The oldval.</param>
        /// <param name="newval">The newval.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>        
        bool Notification_EntityPlanBudgetUpdated(string oldval, string newval, int entityId);

        /// <summary>
        /// Notification entity approved allocated updated.
        /// </summary>
        /// <param name="oldval">The oldval.</param>
        /// <param name="newval">The newval.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_EntityApprovedAllocatedUpdated(string oldval, string newval, int entityId);

        /// <summary>
        /// Notification funding request statechanged.
        /// </summary>
        /// <param name="FundingRequestState">State of the funding request.</param>
        /// <param name="CostcenterName">Name of the costcenter.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_FundingRequestStatechanged(string FundingRequestState, string CostcenterName, int entityId);

        /// <summary>
        /// Notification funding request deleted.
        /// </summary>
        /// <param name="costcentername">The costcentername.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_FundingRequestDeleted(string costcentername, int entityId);

        /// <summary>
        /// Notification  cost center deleted.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <param name="entityName">Name of the entity.</param>
        /// <returns>bool</returns>
        bool Notification_CostCenterDeleted(int entityId, string entityName);

        /// <summary>
        /// Notification  money transferred.
        /// </summary>
        /// <param name="Amount">The amount.</param>
        /// <param name="FromCostcenterName">Name of from costcenter.</param>
        /// <param name="ToCostCenterName">Name of to cost center.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_MoneyTransferred(float Amount, string FromCostcenterName, string ToCostCenterName, int entityId);

        /// <summary>
        /// Notification insert cost center.
        /// </summary>
        /// <param name="CostcenterName">Name of the costcenter.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_InsertCostCenter(string CostcenterName, int entityId);

        /// <summary>
        /// Notification enable disable workflow.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_EnableDisableWorkflow(string state, int entityId);

        /// <summary>
        /// Notification entity commit budget updated.
        /// </summary>
        /// <param name="OldValue">The old value.</param>
        /// <param name="NewValue">The new value.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_EntityCommitBudgetUpdated(string OldValue, string NewValue, int entityId);

        /// <summary>
        /// Notification entity spent budget updated.
        /// </summary>
        /// <param name="OldValue">The old value.</param>
        /// <param name="NewValue">The new value.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_EntitySpentBudgetUpdated(string OldValue, string NewValue, int entityId);

        /// <summary>
        /// Notification cost center approved budget updated.
        /// </summary>
        /// <param name="CostcenterName">Name of the costcenter.</param>
        /// <param name="entityId">The entity id.</param>
        /// <returns>bool</returns>
        bool Notification_CostCenterApprovedBudgetUpdated(string CostcenterName, int entityId);

        /// <summary>
        /// Creates the objective.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="typeId">The type id.</param>
        /// <param name="name">The name.</param>
        /// <param name="isEnableFeedback">The is enable feedback.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="dateRule">The date rule.</param>
        /// <param name="isMandatory">The is mandatory.</param>
        /// <param name="numeric">The numeric.</param>
        /// <param name="ratings">Additional attributes if any.</param>
        /// <param name="conditions">Additional attributes if any.</param>
        /// <returns>Last inserted Objective ID</returns>
        // int CreateObjective(int typeId, String name, IObjectiveNumeric numeric, IList<IObjectiveRating> ratings, IList<IObjectiveCondition> conditions,IList<IEntityRoleUser> entityMembers, IList<IAttributeData> entityattributedata);

        /// <summary>
        /// Creates the objective.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="typeId">The type id.</param>
        /// <param name="name">The name.</param>
        /// <param name="instruction">The instruction.</param>
        /// <param name="isEnableFeedback">The is enable feedback.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="dateRule">The date rule.</param>
        /// <param name="isMandatory">The is mandatory.</param>
        /// <param name="numeric">Additional  attributes if any</param>
        /// <param name="ratings">Additional  attributes if any</param>
        /// <param name="conditions">Additional attributes if any</param>
        /// <returns>Last inserted Objective ID</returns>
        //int CreateObjective(int typeId, String name, IObjectiveNumeric numeric, IList<IObjectiveRating> ratings, IList<IObjectiveCondition> conditions,IList<IEntityRoleUser> entityMembers,IList<IAttributeData> entityattributedata);

        /// <summary>
        /// Creates the objective.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The id.</param>
        /// <param name="typeId">The type id.</param>
        /// <param name="name">The name.</param>
        /// <param name="objDescription">The Description.</param>
        /// <param name="instruction">The instruction.</param>
        /// <param name="isEnableFeedback">The is enable feedback.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="dateRule">The date rule.</param>
        /// <param name="isMandatory">The is mandatory.</param>
        /// <param name="objNumeric">The objNumeric.</param>
        /// <param name="objRatings">The objRatings.</param>
        /// <param name="ratingObjArr">The Ratings Caption List </param>
        /// <param name="objFullfilConditions">The objFullfilConditions.</param>
        /// <param name="objEntityMembers">The MembersList.</param>
        /// <returns>Last inserted Objective ID</returns>
        int CreateObjective(int typeId, String name, bool objStatus, string objDescription, string objInstruction, bool objIsEnableFeedback, DateTime objStartDate, DateTime objEndDate, int objDateRule, bool objMandatory, IObjectiveNumeric objNumeric, IObjectiveNumeric objNonNumeric, IList<IObjectiveRating> objRatings, List<string> ratingObjArr, IList<IObjectiveFulfillCondtions> objFullfilConditions, IList<IEntityRoleUser> objEntityMembers, IList<IAttributeData> objectiveattributedata, int objEntityTypeId, IList<IEntityPeriod> listEntityperiods);


        /// <summary>
        /// Deletes the objective.
        /// </summary>
        /// <param name="objective">The objective.</param>
        /// <returns>True (or) False</returns>
        bool DeleteObjective(int objectiveId);

        /// <summary>
        /// Selecting all children and parent entities based on unique-key.
        /// </summary>
        /// <param name="ID">EntityID</param>
        /// <param name="TypeID">EntityTypeID</param>
        /// <returns>IList<IEntity></returns>
        IList<IEntity> SelectAllchildeEtities(int id);

        /// <summary>
        /// Selecting particular Entity.
        /// </summary>
        /// <param name="ID">EntityID</param>
        /// <param name="TypeID">TypeID</param>
        /// <returns>IEntity</returns>
        IEntity SelectEntityByID(int id);
        /// <summary>
        /// Creating Costcentre entity.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The id.</param>
        /// <param name="name">The Name.</param>
        /// <param name="assignedAmount">The Assignedamount for costcentre.</param>
        /// <param name="entityattributedata">The IList<IAttributeData> AttributeData</param>
        /// <param name="entityMembers">The EntityMembers.</param>
        /// <returns>Last Inserted Costcentre ID</returns>
        int CreateCostcentre(int typeId, string name, int assignedAmount, IList<IAttributeData> entityattributedata, IList<IEntityRoleUser> entityMembers, int ParentID, int CurrencyFormat, IList<IEntityAmountCurrencyType> listentityamountcurrencytype, IList<IEntityPeriod> listEntityperiods);

        /// <summary>
        /// Getting Costcentre 
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The id.</param>
        /// <returns>ICostCentreData</returns>
        ICostCentreData GetCostcentre(int id);

        /// <summary>
        /// Getting Costcentre for entity creation
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The id.</param>
        /// <returns>ICostCentreData</returns>
        IList GetCostcentreforEntityCreation(int EntityTypeID, int fiscalyear = 0, int entityid = 0);

        /// <summary>
        /// Getting Member for Entity Creation
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The entityid.</param>
        /// <returns>IMemberData</returns>
        IList GetGlobalMembers(int entityid = 0);


        /// <summary>
        /// add Costcentre 
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="entityId">The entityid.</param>
        /// <param name="entityCostcentres">The IEntityCostReleations.</param>
        /// <returns>ICostCentreData</returns>
        bool AddCostCenterForFinancial(int entityId, IList<IEntityCostReleations> entityCostcentres, bool isForceful);

        /// <summary>
        /// Getting Costcentre for Financial 
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The id.</param>
        /// <returns>ICostCentreData</returns>
        IList<object> GetCostcentreforFinancial(int entityid);

        /// <summary>
        /// Getting Entity Financial Details 
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The entityid.</param>
        /// <returns>IList</returns>
        Tuple<IList, IList, IList, IList, IList, int, IList<IFinancialMetadataAttributewithValues>, Tuple<List<object>>> GetEntityFinancialdDetails(int entityid, int userID, int startRow, int endRow, bool includedetails);

        /// <summary>
        /// Getting Cost Center Amount  Details 
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The costCenterId.</param>
        /// <returns>IList</returns>
        IList GetCostcenterBeforeApprovalAmountDetails(int costCenterId, int entityid);

        /// <summary>
        /// Update Planned Amount in Financial
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The entityid.</param>
        /// <param name="CostCenterId">The CostCenter ID</param>
        /// <param name="Amount">Planned Amount</param>
        /// <returns>True (or) False</returns>
        bool EntityPlannedAmountInsert(int entityID, int CostcenterId, Decimal PlannedAmount, int currencyType, string description);

        /// <summary>
        /// Update Planned Amount in Financial
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The entityid.</param>
        /// <param name="CostCenterId">The CostCenter ID</param>
        /// <param name="Amount">Planned Amount</param>
        /// <returns>True (or) False</returns>
        bool EntityApprovePlannedAmountInsert(int entityID, int CostcenterId, Decimal AvailableAmount, Decimal PlannedAmount, Decimal ApprovedAllocatedAmount, int currencyType,int level);

        /// <summary>
        /// AdjustApprove Allocation Amount
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The entityid.</param>
        /// <param name="Amount">Approve Planned Updation</param>
        /// <returns>Bool</returns>
        bool AdjustApproveAllocation(int entityID);

        /// <summary>
        /// Transfer Money from one entityCostcenter into another costcenter in Financial
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The entityid.</param>
        /// <param name="CostCenterId">The FromCostCenter ID</param>
        /// <param name="CostCenterId">The ToCostCenter ID</param>
        /// <param name="Amount">Transfer Amount</param>
        /// <returns>True (or) False</returns>
        bool EntityMoneyTransfer(int entityID, int FromCostcenterId, int ToCostCenterId, Decimal TransferAmount);

        /// <summary>
        /// Update Approved Allocated Amount in Financial
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The entityid.</param>
        /// <param name="CostCenterId">The CostCenter ID</param>
        /// <param name="Amount">Release Amount</param>
        /// <returns>True (or) False</returns>
        bool ReleaseFund(int entityID, int CostcenterId, Decimal ReleaseAmount);

        /// <summary>
        /// Update Status  in Financial
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The entityid.</param>
        /// <param name="CostCenterId">The CostCenter ID</param>
        /// <param name="Status">the Status</param>
        /// <returns>Bool</returns>
        bool UpdateFundRequestStatus(int entityID, int CostcenterId, int FundRequestID, int status);

        /// <summary>
        /// Update Request Amount in Financial
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The entityid.</param>
        /// <param name="CostCenterId">The CostCenter ID</param>
        /// <param name="Amount">Request Amount</param>
        /// <returns>Bool</returns>
        bool EntityRequestAmountInsert(int entityID, int CostcenterId, Decimal RequestAmount);

        /// <summary>
        /// Updating Costcentre 
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="costcentredata">The CostcentreData.</param>
        /// <returns>True (or) False</returns>
        bool UpdateCostcentre(ICostCentreData costcentredata);
        /// <summary>
        /// Deleting Costcentre entity.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The id.</param>
        /// <returns>true (or) False</returns>
        bool DeleteCostcentreentity(int costcenterId);
        /// <summary>
        /// Deleting Costcentre Relation in Financial.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The Entityid.</param>
        /// <param name="id">The CostCenterid.</param>
        /// <returns>true (or) False</returns>
        bool DeleteCostcentreFinancial(int entityID, int costcenterId);
        /// <summary>
        /// Creating Objective entityvalues.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="objectiveId">The ObjectiveId.</param>
        /// <param name="entityId">The EntityId.</param>
        /// <param name="plannedTarget">The PlannedTarget.</param>
        /// <param name="targetOutcome">The TargetOutcome.</param>
        /// <param name="ratingObjective">The RatingObjective.</param>
        /// <param name="comments">The Comments.</param>
        /// <param name="status">The Status.</param>
        /// <param name="fullfilment">The Fulfilment.</param>
        /// <returns>IObjectiveEntityValue</returns>
        IObjectiveEntityValue Objectiveentityvalues(int objectiveId, int entityId, int plannedTarget, int targetOutcome, int ratingObjective, string comments, int status, int fullfilment);
        /// <summary>
        /// Creating Objective Condition.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="objCondition">The TotalObjectiveCondition.</param>
        /// <returns>IList<IObjectiveCondition></returns>
        IList<IObjectiveCondition> ObjectiveConditionvalues(IList<IObjectiveCondition> objCondition);

        /// <summary>
        /// Gets the tree node.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <returns>List of ITreeNode</returns>
        string GetEntitydescendants(int attributeID);

        /// <summary>
        /// Creates the Entity
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The id.</param>
        /// <param name="parentId">The parent id.</param>
        /// <param name="typeId">The type id.</param>
        /// <param name="active">The active.</param>
        /// <param name="uniqueKey">The unique key.</param>
        /// <param name="isLock">The is lock.</param>
        /// <param name="name">The name.</param>
        /// <param name="entityMembers">The entity members.</param>
        /// <param name="entityObjectvalues">The entity ObjectiveEntityValues.</param>
        /// <param name="entityCostcenters">The entity costcenters.</param>
        /// <param name="periods">The periods.</param>
        /// <param name="attributes"> The attributes</param>
        /// <returns>Lastinserted Entity Id value</returns>
        int CreateEntity(int parentId, int typeId, Boolean active, Boolean isLock, string name, IList<IEntityRoleUser> entityMembers, IList<IEntityCostReleations> entityCostcentres, IList<IEntityPeriod> entityPeriods, IList<IFundingRequest> listFundrequest, IList<IAttributeData> entityattributedata, int[] assetIdArr = null, IList<IObjectiveEntityValue> entityObjectvalues = null, IList<object> attributes = null, IList<IEntityAmountCurrencyType> listentityamountcurrencytype = null, int ApiOwnerID = 0, IList<object> PredefinedAttrGroup = null, IList<object> AttrGroupValues = null, List<int> IsAttributeGrpDirectInherit = null);

        /// <summary>
        /// Creates the Entity.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The id.</param>
        /// <param name="parentId">The parent id.</param>
        /// <param name="typeId">The type id.</param>
        /// <param name="active">The active.</param>
        /// <param name="uniqueKey">The unique key.</param>
        /// <param name="isLock">The is lock.</param>
        /// <param name="name">The name.</param>
        /// <param name="attributes"> The attributes</param>
        /// <returns>Lastinserted Entity Id value</returns>
        int CreateFundRequest(int parentId, int typeId, Boolean active, Boolean isLock, string name, IList<IFundingRequest> listFundrequest, IList<IFundingRequestHolder> entityattributedata);


        /// <summary>
        /// Updates the Entity.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="entitydata">The entitydata.</param>
        /// <returns>True (or) False</returns>
        bool UpdateEntity(IEntity entitydata);

        /// <summary>
        /// Getting EntityAttribute values By Entityname
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="entityTypeId">The EntityTypeID</param>
        /// <returns>IList<IDynamicAttributes></returns>
        IList<IDynamicAttributes> GetEntityAttributes(int entityId);
        /// <summary>
        /// Inserting EntityAttribute values By Entityname
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="entityTypeId">The EntityTypeID</param>
        /// <returns>int</returns>
        int InsertEntityAttributes(IList<IAttributeData> attributes, int entityTypeId);

        /// <summary>
        /// Deleting Entity
        /// </summary>
        /// <param name="entityId">The EntityID</param>
        /// <returns>True (or) False</returns>
        bool DeleteEntity(int entityId);


        #region Instance of Classes In ServiceLayer reference
        /// <summary>
        /// Returns EntityRolesUser class.
        /// </summary>
        IEntityRoleUser Entityrolesservice();

        /// <summary>
        /// Returns PurchaseOrder class.
        /// </summary>
        IPurchaseOrder PurchaseOrderservice();

        /// <summary>
        /// Returns PurchaseOrderDetail class.
        /// </summary>
        IPurchaseOrderDetail PurchaseOrderDetailservice();

        /// <summary>
        /// Returns Invoice class.
        /// </summary>
        IInvoice Invoiceservice();

        /// <summary>
        /// Returns InvoiceDetail class.
        /// </summary>
        IInvoiceDetail InvoiceDetailservice();

        /// <summary>
        /// Returns Supplier class.
        /// </summary>
        ISupplier Supplierservice();

        /// <summary>
        /// Returns EntityCostcentrerelation class.
        /// </summary>
        IEntityCostReleations EntityCostcentrerelationservice();

        /// <summary>
        /// Returns Entitypresentation class.
        /// </summary>
        IEntityPresentation Entitypresentationservice();

        /// <summary>
        /// Returns Entityperiod class.
        /// </summary>
        IEntityPeriod Entityperiodservice();

        IEntityAmountCurrencyType EntityAmountCurrencyTypeservice();

        /// <summary>
        /// Returns Financial class.
        /// </summary>
        IFinancial Entityfinanicalservice();

        /// <summary>
        /// Returns FundingRequest class.
        /// </summary>
        IFundingRequest EntityFundingrequestservice();

        /// <summary>
        /// Returns Objective class.
        /// </summary>
        IObjective Objectiveservice();

        /// <summary>
        /// Returns Costcentre class.
        /// </summary>
        ICostCenter Costcentreservice();

        /// <summary>
        /// Returns AttributeData class.
        /// </summary>
        IAttributeData AttributeDataservice();

        /// <summary>
        /// Returns FundingRequestHolder class.
        /// </summary>
        IFundingRequestHolder FundingRequestHolderservice();

        /// <summary>
        /// Returns ObjectiveNumeric class.
        /// </summary>
        IObjectiveNumeric ObjNumericservice();

        /// <summary>
        /// Returns ObjectiveRating class.
        /// </summary>
        IObjectiveRating ObjRatingservice();

        /// <summary>
        /// Returns ObjectiveCondition class.
        /// </summary>
        IObjectiveCondition Objectiveconditionservice();

        /// <summary>
        /// Returns ObjectiveEntityValues class.
        /// </summary>
        IObjectiveEntityValue ObjEnityvalservice();

        /// <summary>
        /// Returns BaseEntity class.
        /// </summary>
        IBaseEntity Baseentityservice();

        ICostCentreData CostcentreDataservice();

        IEntity EntityService();

        IFiltersettingsValues FilterSettingsValuesService();

        /// <summary>
        /// Returns IObjectiveFulfillCondition class.
        /// </summary>
        IObjectiveFulfillCondtions ObjectiveFulfillmentCondtionValuesService();

        ICalenderFulfillCondtions CalenderFulfillmentCondtionValuesService();


        /// <summary>
        /// Returns Task class.
        /// </summary>
        ITask TasksService();

        /// <summary>
        /// Returns TaskAttachment class.
        /// </summary>
        ITaskAttachment TasksAttachmentService();

        #endregion
        /// <summary>
        /// Updating EntityAttribute values
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="IList<IAttributeData> attributes">The AttributesData</param>
        /// <param name="enityId">The EnityID</param>
        /// <returns>True (or) False</returns>
        bool UpdateAttributeData(IList<IAttributeData> attributes, int entityId);

        /// <summary>
        /// Gets the attributes details by entityID.
        /// </summary>
        /// <param name="Id">The entityId.</param>
        /// <returns>
        /// Ilist
        /// </returns>
        IList<IAttributeData> GetEntityAttributesDetails(int id);


        IList Get_EntityIDs(int ActivityListID, int CostCenterID = 0, int ObjectiveID = 0, bool IsGlobalAdmin = false, int FilterID = 0, string PublishDate = null, int UserID = 0);


        /// <summary>
        /// GetChildTreeNodes.
        /// </summary>
        /// <param name="Id">The ParentID.</param>
        /// <returns>
        /// IList
        /// </returns>
        IList GetChildTreeNodes(int ParentID);


        /// <summary>
        /// GetParentTreeNodes.
        /// </summary>
        /// <param name="IdArr">The IdArr.</param>
        /// <returns>
        /// IList
        /// </returns>
        IList GetParentTreeNodes(int[] IdArr);




        /// <summary>
        /// Inserting FilterSettings values for ActivityListLevel
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="filterName">The FilterName</param>
        /// <param name="keyword">The Keyword</param>
        /// <param name="userId">The UserID</param>
        /// <param name="entityTypeId">The EntityTypeID</param>
        /// <param name="startDate">The StartDate</param>
        /// <param name="endDate">The EndDate</param>
        /// <param name="whereCondition">The WhereCondition</param>
        /// <returns>int</returns>
        int InsertFilterSettings(string filterName, string keyword, int userId, int typeId, string entityTypeId, int IsDetailFilter, string startDate, string endDate, string whereCondition, IList<IFiltersettingsValues> filterAttributes, int filterId = 0, string entitymemberId = "");

        /// <summary>
        /// GetFilterSettings.
        /// </summary>
        /// <returns>
        /// IList<FilterSettings>
        /// </returns>
        IList<IFilterSettings> GetFilterSettings(int typeId);
        IList GetApprovedBudgetDate(string ListId);

        IList<IFilterSettings> GetFilterSettingsForDetail(int typeId);

        /// <summary>
        /// GetFilterSettingsValues.
        /// </summary>
        /// <returns>
        /// IFilterSettings
        /// </returns>
        IFilterSettings GetFilterSettingValuesByFilertId(int filterId);

        /// <summary>
        /// Getting All Milestones based on EntityId.
        /// </summary>
        /// <param name="entityId">The EntityId</param>
        /// <param name="entitytypeId">The MileStoneTypeId</param>
        /// <returns>IList of IMilestoneMetadata</returns>

        IList<IMilestoneMetadata> GetMilestoneMetadata(int entityId, int entitytypeId);

        /// <summary>
        /// Getting All Milestones based on EntityId.
        /// </summary>
        /// <param name="entityId">The EntityId</param>
        /// <param name="entitytypeId">The MileStoneTypeId</param>
        /// <returns>IList of IMilestoneMetadata</returns>

        IList<IMilestoneMetadata> GetMilestoneforWidget(int entityId, int entitytypeId);
        /// <summary>
        /// Get member.
        /// </summary>
        /// <param name="EntityID">The EntityID.</param>
        /// <returns>IList of IEntityRoleUser</returns>
        IList<IEntityRoleUser> GetMember(int EntityID);

        /// <summary>
        /// GetFundrequestTaskMember.
        /// </summary>
        /// <param name="ID">The ID.</param>
        /// <returns>IList of IEntityRoleUser</returns>
        IList<ITaskMember> GetFundrequestTaskMember(int EntityID);

        /// <summary>
        /// Inserting member
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="int EntityID">The EntityID</param>
        /// <param name="int RoleID">The RoleID</param>
        /// <param name="int Assignee">The Assignee</param>
        /// <param name="bool IsInherited">The IsInherited</param>
        /// <param name="int InheritedFromEntityid">The InheritedFromEntityid</param>
        /// <returns>int</returns>
        int InsertMember(int EntityID, int RoleID, int Assignee, bool IsInherited, int InheritedFromEntityid);

        /// <summary>
        /// Deleting Entity
        /// </summary>
        /// <param name="entityId">The EntityID</param>
        /// <param name="Assignee">The Assignee</param>
        /// <returns>True (or) False</returns>
        bool DeleteMember(int ID);


        /// <summary>
        /// Updating member
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="int EntityID">The EntityID</param>
        /// <param name="int RoleID">The RoleID</param>
        /// <param name="int Assignee">The Assignee</param>
        /// <param name="bool IsInherited">The IsInherited</param>
        /// <param name="int InheritedFromEntityid">The InheritedFromEntityid</param>
        /// <returns>int</returns>
        bool UpdateMember(int ID, int EntityID, int RoleID, int Assignee, bool IsInherited, int InheritedFromEntityid, bool IsPlanEntity = false);

        /// <summary>
        /// Delete FilterSettings and Values.
        /// </summary>
        /// <returns>
        /// True or False
        /// </returns>
        bool DeleteFilterSettings(int filterId);


        /// <summary>
        /// Get Entity Period.
        /// </summary>
        /// <param name="EntityID">The EntityID.</param>
        /// <returns>IList of IEntityPeriod</returns>
        IList<IEntityPeriod> GetEntityPeriod(int EntityID);

        /// <summary>
        /// Inserting Entity Period
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="int EntityID">The EntityID</param>
        /// <param name="string StartDate">The StartDate</param>
        /// <param name="string EndDate">The EndDate</param>
        /// <param name="int SortOrder">The SortOrder</param>
        /// <param name="string Description">The Description</param>
        /// <returns>int</returns>
        int InsertEntityPeriod(int EntityID, string StartDate, string EndDate, int SortOrder, string Description);

        /// <summary>
        /// Deleting Entity Period
        /// </summary>
        /// <param name="entityId">The EntityID</param>
        /// <param name="Assignee">The Assignee</param>
        /// <returns>True (or) False</returns>
        bool DeleteEntityPeriod(int ID);


        /// <summary>
        /// Updating Entity Period
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="int EntityID">The EntityID</param>
        /// <param name="string StartDate">The StartDate</param>
        /// <param name="string EndDate">The EndDate</param>
        /// <param name="int SortOrder">The SortOrder</param>
        /// <param name="string Description">The Description</param>
        /// <returns>int</returns>
        bool UpdateEntityPeriod(int ID, int EntityID, string StartDate, string EndDate, int SortOrder, string Description);


        /// <summary>
        /// Deleting RootLevelcostCentre
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="costcentreId">The CostcentreID</param>
        /// <returns>True (or) False</returns>
        bool DeleteRootCostcentre(int costcentreId);

        /// <summary>
        /// Getting CostcentreFinancialSummaryBlockDetails
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="costCentreId">The CostcentreID</param>
        /// <returns>IFinancialOverview</returns>
        IFinancialOverview GettingCostcentreFinancialOverview(int costCentreId);

        /// <summary>
        /// Updating Costcentre Assigned Amount
        /// </summary>
        /// <param name="costcenreId">The CostcentreID</param>
        /// <param name="totalAssignedAmount">The Total Assigned Amount</param>
        /// <returns>int</returns>
        string UpdateTotalAssignedAmount(int costcentreId, int totalAssignedAmount);

        /// <summary>
        /// Getting All Units.
        /// </summary>
        /// <returns>List of IObjectiveUnit</returns>
        IList<IObjectiveUnit> GettingObjectiveUnits();
        bool AddEntity(string EntityID, string name);
        bool UpdateEntityforSearch(string EntityID, string name);
        bool RemoveEntity(int EntityID, string name);
        bool UpdateSearchEngine();


        /// <summary>
        /// Getting Objective & Assignments Type Block
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="objectiveId">The ObjectiveID</param>
        /// <returns>IObjectiveSummaryDeatils</returns>
        IObjectiveSummaryDetails GettingObjectiveSummaryBlockDetails(int objectiveId);

        /// <summary>
        /// Getting Objective & Assignments Fulfillment Block
        /// </summary>
        /// <param name="objectiveId">The ObjectiveID</param>
        /// <returns>IObjectiveFulfullConditions</returns>
        IObjectiveFulfillCondtions GettingObjectiveFulfillmentBlockDetails(int objectiveId);

        /// <summary>
        /// Duplicating the entites
        /// </summary>
        /// <param name="proxy"></param>
        /// <param name="entityId"></param>
        /// <returns>EntityID</returns>
        ArrayList DuplicateEntity(int entityID, int parentID, int DuplicateTimes, bool IsDuplicateChild, Dictionary<string, bool> duplicateitems = null, List<string> listEntityNamesToDuplicate = null);

        /// <summary>
        /// Inserting Additional Objective & Assignments 
        /// </summary>
        /// <param name="entityId">The EntityID</param>
        /// <param name="entityTypeId">The EntityTypeId</param>
        /// <param name="objectiveTye">The ObjectiveTypeId</param>
        /// <param name="name">The Name</param>
        /// <param name="instruction">The Instruction</param>
        /// <param name="isEnablefeeback">The IsEnableFeedback</param>
        /// <param name="untiId">The UnitId</param>
        /// <param name="plannedTarget">The PlannedTarget</param>
        /// <param name="targetOutcome">The TargetOutcome</param>
        /// <param name="ratingObjective">The RatingObjective</param>
        /// <param name="comments">The Comments</param>
        /// <param name="fulFillment">The Fulfillment</param>
        /// <param name="objectiveStatus">The ObjectiveStatus</param>
        /// <param name="entityMembers">The Entity Users</param>
        /// <param name="ratings">The Ratings</param>
        /// <returns>Last Inserted Additional EntityId</returns>
        int InsertAdditionalObjective(int entityId, int entityTypeId, int objectiveTypeId, string name, string instruction, bool isEnableFeedback, int unitId, decimal plannedTarget, decimal targetOutCome, int ratingObjective, string comments, int fulFillment, int objectiveStatus, IEntityRoleUser entityMembers, List<string> ratings = null);



        /// <summary>
        /// Getting Objectives for Activity Entity Select 
        /// </summary>
        /// <param name="entityId">The EntityID</param>
        /// <returns>IObjectivesToEntitySelect</returns>
        IList<IObjectivesToEntitySelect> GettingObjectivestoEntitySelect(int entityId);

        /// <summary>
        /// Updating Objective & Assignments Summary Block
        /// </summary>
        /// <param name="objectiveId">The ObjectiveID</param>
        /// <param name="instruction">The Instruction</param>
        /// <param name="isEnableComments">The EnableComments</param>
        /// <param name="unitId">The UnitID</param>
        /// <param name="globalBaseline">The GlobaleBaseLine</param>
        /// <param name="globalTarget">The GlobalTarget</param>
        /// <returns>True or False</returns>
        bool UpdateObjectiveSummaryBlockData(int objectiveId, int objectiveTypeId, string instruction, bool isEnableComments, int unitId = 0, decimal globalBaseline = 0, decimal globalTarget = 0, IList<object> Objratings = null);

        /// <summary>
        /// Getting Objective & Assignments Fulfillment Block
        /// </summary>
        /// <param name="objectiveId">The ObjectiveID</param>
        /// <returns>IList of IObjectiveFulfullConditions</returns>
        IList<IObjectiveFulfillCondtions> GettingEditObjectiveFulfillmentDetails(int objectiveId);

        /// <summary>
        /// Getting Entity Predefine Objectie AttributeDetails
        /// </summary>
        /// <param name="entityId">The EntityID</param>
        /// <returns>List of IEntityPredefineObjectiveAttributes</returns>
        List<IEntityPredefineObjectiveAttributes> GettingEntityPredefineObjectives(int entityId);

        /// <summary>
        /// Inserting predefined Objectives for Entity
        /// </summary>
        /// <param name="objectiveId">The ObjectiveID</param>
        /// <param name="entityId">The EntityID</param>
        /// <returns>List of Last of Inserted Objective IDs</returns>
        List<int> InsertPredefineObjectivesforEntity(List<int> objectiveId, int entityId);

        /// <summary>
        /// Getting Entity Predefine Objectie AttributeDetails
        /// </summary>
        /// <param name="attribteDate">The IList of AttributeData</param>
        /// <param name="startDate">The StartDate</param>
        /// <param name="enddate">The EndDate</param>
        /// <param name="entityTypeId">The EntityTypeID</param>
        /// <returns>List of IEntityPredefineObjectiveAttributes</returns>
        List<IEntityPredefineObjectiveAttributes> GettingPredefineObjectivesForEntityMetadata(IList<IAttributeData> attributeData, DateTime startDate, DateTime endDate, int entityTypeID);

        /// <summary>
        /// Getting Entity Predefine Objecties
        /// </summary>
        /// <param name="entityId">The EntityID</param>
        /// <returns>IList of IPredefineObjectives</returns>
        IList<object> LoadPredefineObjectives(int entityId);

        /// <summary>
        /// Updating predefined Objectives for Entity
        /// </summary>
        /// <param name="objectiveEntityId">The ObjectiveEntityID</param>
        /// <param name="objectiveId">The ObjectiveID</param>
        /// <param name="entityId">The EntityID</param>
        /// <param name="plannedTarget">The PlannedTarget</param>
        /// <param name="targetOutCome">The TargetOutcome</param>
        /// <param name="ratingObjective">The RatignObjective</param>
        /// <param name="comments">The Comments</param>
        /// <param name="status">The Status</param>
        /// <param name="fulfilled">The FulFilled</param>
        /// <returns>True or False</returns>
        bool UpdatePredefineObjectivesforEntity(int objectiveEntityId, int objectiveId, int entityId, decimal plannedTarget = 0, decimal targetOutcome = 0, int ratingObjective = 0, string comments = null, int status = 0, int fulfillment = 0);

        /// <summary>
        /// Getting Entity Additional Objecties
        /// </summary>
        /// <param name="entityId">The EntityID</param>
        /// <returns>IList of IPredefineObjectives</returns>
        IList<IPredefineObjectives> GettingAddtionalObjectives(int entityId);

        /// <summary>
        /// Inserting and Updating Mandatoy Objective condition satisfied Entities
        /// </summary>
        /// <param name="objectiveId">The ObjectiveID</param>
        /// <param name="objectiveName">The ObjectiveName</param>
        /// <param name="objectiveDescription">The ObjectiveDescription</param>
        /// <returns>True or False</returns>
        bool UpdatingObjectiveOverDetails(int objectiveId, string objectiveName, string objectiveDescription, string Typeid);

        /// <summary>
        /// Updating Additional Objective & Assignments 
        /// </summary>
        /// <param name="objectiveEntityId">The ObjectiveEntityID</param>
        /// <param name="entityId">The EntityID</param>
        /// <param name="objectiveTye">The ObjectiveTypeId</param>
        /// <param name="instruction">The Instruction</param>
        /// <param name="isEnablefeeback">The IsEnableFeedback</param>
        /// <param name="untiId">The UnitId</param>
        /// <param name="plannedTarget">The PlannedTarget</param>
        /// <param name="targetOutcome">The TargetOutcome</param>
        /// <param name="ratingObjective">The RatingObjective</param>
        /// <param name="comments">The Comments</param>
        /// <param name="fulFillment">The Fulfillment</param>
        /// <returns>True or False</returns>
        bool UpdateAdditionalObjectivesforEntity(int objectiveEntityId, int entityId, int objectiveTypeId, string instruction, bool isEnableFeedback, int unitId, decimal plannedTarget, decimal targetOutCome, int ratingObjective, string comments, int fulFillment, string instructions, int Objstatus, string Objectivename);


        /// <summary>
        /// Reinserting Objective Fulfillment Conditions
        /// </summary>
        /// <param name="objectiveId">The ObjectiveID</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="dateRule">The date rule.</param>
        /// <param name="isMandatory">The is mandatory.</param>
        /// <param name="objFullfilConditions">The objFullfilConditions.</param>
        /// <returns>Last inserted Condition ID</returns>
        int UpdateObjectiveFulfillmentCondition(int objectiveId, string objStartDate, string objEndDate, int objDateRule, bool objMandatory, IList<IObjectiveFulfillCondtions> objFullfilConditions, string ObjectiveFulfillDeatils);
        //
        /// <summary>
        /// Deleting Objective Fulfillment Conditions 
        /// </summary>
        /// <param name="objectiveId">The ObjectiveID</param>
        /// <returns>True or False</returns>
        bool DeleteObjectiveFulfillment(int objectiveId);

        /// <summary>
        /// Getting Additional Objective Ratings
        /// </summary>
        /// <param name="objectiveId">The ObjectiveID</param>
        /// <returns>IList of Additional Ratings</returns>
        IList<IAddtionalObjectiveRating> GettingAdditionalObjRatings(int objectiveId);

        /// <summary>
        /// Getting Predefine Objective Ratings
        /// </summary>
        /// <param name="objectiveId">The ObjectiveID</param>
        /// <returns>IList of Objective Ratings</returns>
        IList<IObjectiveRating> GettingPredefineObjRatings(int objectiveId);

        bool EntityForeCastInsert(int entityID, int CostcenterId, Double QuarterAmount, int Quater);

        /// <summary>
        /// Updating Entity Image Name 
        /// </summary>
        /// <param name="entityId">The EntityID</param>
        /// <param name="attributeId">The AttributeID</param>
        /// <param name="imageName">The ImageName</param>
        ///  <param name="attribtueData">The AttributeData</param>
        /// <returns>True or False</returns>
        bool UpdateImageName(int entityId, int attributeId, string imageName);


        IList GetForeCastForCCDetl(int CostcenterId);

        bool EntityForecastAmountUpdate(int EntityID);

        bool DeleteFundRequest(int id, int entityID);

        /// <summary>
        /// Getting WorkFlowSteps with Tasks
        /// </summary>
        ///  <param name="proxy"></param>
        /// <param name="EntityTypeID">The EntityTypeID</param>
        /// <returns>IList of IWorkFlowStepsWithTasks</returns>
        IList<IWorkFlowStepsWithTasks> GetAllWorkFlowStepsWithTasks(int entityID);

        int InsertTaskWithAttachments(int parentEntityID, int taskTypeID, string TaskName, int StepID, IList<ITask> TaskList, IList<ITaskMember> TaskMembers, IList<ITaskAttachment> TaskAttachments, IList<BrandSystems.Marcom.Core.Common.Interface.IFile> TaskFiles);

        /// <summary>
        /// Getting Task details
        /// </summary>
        ///  <param name="proxy"></param>
        /// <param name="TaskID">The TaskID</param>
        /// <returns>IList of ITask</returns>
        IList<ITask> GetWorkFlowTaskDetails(int taskID);

        /// <summary>
        /// Updating Milestone status 
        /// </summary>
        /// <param name="entityId">The EntityID</param>
        /// <param name="status">The status</param>
        /// <returns>True or False</returns>
        bool UpdatingMilestoneStatus(int entityId, int status);


        /// <summary>
        /// Updating Entity Active status 
        /// </summary>
        ///  <param name="proxy"></param>
        /// <param name="entityId">The EntityID</param>
        /// <param name="status">The status</param>
        /// <returns>True or False</returns>
        bool UpdateEntityActiveStatus(int entityId, int status);

        /// <summary>
        /// Updating Task status 
        /// </summary>
        ///  <param name="proxy"></param>
        /// <param name="entityId">The TaskID</param>
        /// <param name="status">The Status</param>
        /// <returns>True or False</returns>
        int UpdateTaskStatus(int taskID, int status, int entityID = 0);


        /// <summary>
        /// Updating Unassigned Task status 
        /// </summary>
        ///  <param name="proxy"></param>
        /// <param name="entityId">The TaskID</param>
        /// <returns>True or False</returns>
        bool UpdateUnassignedTaskStatus(int predefinedTaskID, int EntityID);
        /// <summary>
        /// Updating Predefine In-Line Edit Planned Target and TargetOutcome 
        /// </summary>
        /// <param name="objectiveId">The ObjectievID</param>
        /// <param name="entityId">The EntityID</param>
        /// <param name="plannedTarget">The PlannedTarget</param>
        /// <param name="targetOutcome">The TargetOutcome</param>
        /// <returns>True or False</returns>
        bool UpdatePredefineObjectiveinLineData(int objectiveId, int entityId, int plannedTaget, int targetOutcome);

        /// <summary>
        /// Updating Additional In-Line Edit Planned Target and TargetOutcome 
        /// </summary>
        /// <param name="entityId">The EntityID</param>
        /// <param name="plannedTarget">The PlannedTarget</param>
        /// <param name="targetOutcome">The TargetOutcome</param>
        /// <returns>True or False</returns>
        bool UpdateAdditionalObjectiveinLineData(int entityId, string Objectivename);


        IList<IWorkFlowOverView> GetWorkFlowSummary(int entityID);

        /// <summary>
        /// Deleting Entity
        /// </summary>
        /// <param name="costcentreId">The CostcentreID</param>
        /// <returns>One (or) Two</returns>
        int DeleteCostcentre(int costcentreId);
        /// <summary>
        /// Deleting Activity Predefine Objective
        /// </summary>
        /// <param name="entityId">The EntityID</param>
        /// <param name="objectiveId">The ObjectiveID</param>
        /// <returns>True (or) False</returns>
        bool DeleteActivityPredefineObjective(int entityId, int objectiveId);

        /// <summary>
        /// Deleting Activity Additional Objective
        /// </summary>
        /// <param name="objectiveId">The ObjectiveID</param>
        /// <returns>True (or) False</returns>
        bool DeleteAdditionalObjective(int objectiveId, int entityID);

        /// <summary>
        /// Getting Entity OWners List
        /// </summary>
        /// <param name="entityId">The EntityID</param>
        /// <returns>IList of IEntityOwners</returns>
        IList<IEntityOwners> EntityOwnersList(int entityId);

        /// <summary>
        /// Updating Objective OwnerDetails
        /// </summary>
        /// <param name="entitId">The EntityID</param>
        /// <param name="userId">The UserID</param>
        /// <param name="roleId">The RoleID</param>
        /// <param name="oldOwnerId">The Old Objective Ownerid</param>
        /// <returns>True (or) False</returns>
        bool UpdateObjectiveOwner(int entityId, int userId, int roleId, int oldOwnerId, int entitytypeid);

        bool InsertTaskMembers(int parentEntityID, int TaskID, IList<ITaskMember> TaskMembers);

        bool InsertTaskAttachments(int parentEntityID, int TaskID, IList<ITaskAttachment> TaskAttachments, IList<BrandSystems.Marcom.Core.Common.Interface.IFile> TaskFiles);

        IList<BrandSystems.Marcom.Core.Common.Interface.IFile> GetTaskAttachmentFile(int taskID);

        /// <summary>
        /// DeleteFileByID.
        /// </summary>
        /// <param name="proxy">ID Parameter</param>
        /// <returns>bool</returns>
        bool DeleteFileByID(int ID);

        /// <summary>
        /// Updating Objective Status
        /// </summary>
        /// <param name="objectiveId">The ObjectiveId</param>
        /// <param name="objectiveStatus">The ObjectiveStatus</param>
        /// <returns>True (or) False</returns>
        bool UpdateObjectivestatus(int objectiveId, int objectiveStatus);


        int financialcostcentrestatus(int costcentreid, int entityID);

        string GetEntitiPeriodByIdForGantt(int EntityID);
        IList GetPeriodByIdForGantt(string ID);
        IList GetEntitysLinkedToCostCenter(string ID);

        /// <summary>
        /// Getting Task details
        /// </summary>
        ///  <param name="proxy"></param>
        /// <param name="TaskID">The TaskID</param>
        /// <returns>IList of ITask</returns>
        IList<ITask> GetFundRequestTaskDetails(string entityUniqueKey, int CostcentreID);

        /// Getting Task details
        /// </summary>
        ///  <param name="proxy"></param>
        /// <param name="TaskID">The TaskID</param>
        /// <returns>IList of ITask</returns>
        IList<ITask> GetNewsfeedFundRequestTaskDetails(int fundID);

        string GetMilestoneByEntityID(int EntityID);

        bool PendingFundRequest(int EntityID);

        bool UpdateLock(int EntityID, int IsLock);

        bool IsLockAvailable(int EntityIDs);


        /// <summary>
        ///Update Approved Budget
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="Costcentreid">The Costcentre id.</param>
        /// <returns>true/false</returns>
        bool UpdateCostCentreApprovedBudget(int[] costcentreList);

        Tuple<IList<IPurchaseOrder>, bool> GetAllPurchaseOrdersByEntityID(int entityid);

        int CreateNewPurchaseOrder(IList<IPurchaseOrder> listpurchaseOrder, IList<IPurchaseOrderDetail> POdetailList, IList<IAttributeData> entityattributedata, bool DirectPO);


        /// <summary>
        /// Getting All CurrencyTypes.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <returns>List of ICurrencyType</returns>
        IList<ICurrencyType> GetAllCurrencyType();

        /// <summary>
        /// Update Commit Amount in Financial
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The entityid.</param>
        /// <param name="CostCenterId">The CostCenter ID</param>
        /// <param name="Amount">Commit Amount</param>
        /// <returns>Bool</returns>
        bool EntityCommittedAmountInsert(int entityID, int CostcenterId, Decimal AvailableAmount, Decimal CommitAmount);
        bool EntityDirectSpentCommittedAmountInsert(int entityID, int CostcenterId, Decimal AvailableAmount, Decimal CommitAmount);
        bool EntitySpendAmountInsert(int entityID, int CostcenterId, Decimal AvailableAmount, Decimal CommitAmount);

        /// <summary>
        /// Getting All Supplier.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <returns>List of ISupplier</returns>
        IList<ISupplier> GetAllSupplier();

        bool ApprovePurchaseOrders(int[] POIDArr, int entityID = 0);

        bool SendPurchaseOrders(int[] POIDArr, int entityID = 0);

        bool RejectPurchaseOrders(int[] POIDArr, int entityID = 0);


        int CreateNewSupplier(IList<ISupplier> listSupplier);


        /// <summary>
        /// Delete Workflow Tasks
        /// </summary>
        /// <param name="entityId">The EntityID.</param>
        /// <returns>true/false</returns>
        bool EnableDisableWorkFlow(int entityId, bool IsEnableWorkflow);

        /// <summary>
        /// Enable & Disable WorkFlow Status
        /// </summary>
        /// <param name="entityId">The EntityID.</param>
        /// <returns>True or False</returns>
        bool EnableDisableWorkFlowStatus(int entityId);
        /// <summary>
        /// WorkFlow Tasks Count
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <returns>Work Tasks Count</returns>
        int WorkFlowTaskCount(int entityId);

        Tuple<IList<IInvoice>, bool> GetAllInvoiceByEntityID(int entityid);

        IList<IPurchaseOrder> GetAllSentPurchaseOrdersByEntityID(int entityid);

        int CreateNewInvoice(IList<IInvoice> listpurchaseOrder, IList<IInvoiceDetail> POdetailList, IList<IAttributeData> entityattributedata);

        int CreateInvoiceAndPurchaseOrder(IList<IInvoice> listInvoice, IList<IInvoiceDetail> InvoicedetailList, IList<IPurchaseOrder> listpurchaseOrder, IList<IPurchaseOrderDetail> POdetailLis);

        int UpdatePurchaseOrder(int POID, IList<IPurchaseOrder> listpurchaseOrder, IList<IPurchaseOrderDetail> POdetailList, IList<IAttributeData> entityattributedata);

        bool PeriodAvailability(int EntityTypeID);

        IList<IPlanning> GetPlanningTransactionsByEID(JArray jObject);

        bool DeletePlanTransactions(JArray planObj, int entityID = 0);

        List<IEntityPredefineObjectiveAttributes> GettingPredefineTemplatesForEntityMetadata(IList<IAttributeData> attributeData, int entityTypeID, int entityID);

        IList<Gantt> getReportData(string listIDS);






        /// <summary>
        /// Get Currencytype
        /// </summary>
        /// <returns>IList</returns>

        IList<ICurrencyType> GetCurrencyTypeFFSettings();

        /// <summary>
        /// Added Currency type the Financial forecast settings.
        /// </summary>
        /// <param name="id">The currencytypeID</param>
        /// <param name="name">The currencytypeName</param>
        /// <param name="ShortName">The currencytype Short text</param>
        /// <param name="Symbol">The Symbol</param>
        /// <returns>bool</returns>


        bool InsertUpdateCurrencyListFFSettings(int id, string Name, string ShortName, string Symbol);

        // bool UpdateFinancialForecastSettings(int id, string Name, string ShortName, string Symbol);


        /// <summary>
        /// Deleted Currency type the Financial forecast settings.
        /// </summary>
        /// <param name="id">The currencytypeID</param>
        /// <returns>bool</returns>

        bool DeleteCurrencyListFFSettings(int id);


        /// <summary>
        /// Get divisons for Financial forecast settings.
        /// </summary>
        /// <returns>Object</returns>

        Object getDivisonIds();


        /// <summary>
        /// Sets Divison name by id in  Financial forecast settings.
        /// </summary>
        /// <param name="Divisonid">The DivisonId</param>
        /// <returns>bool</returns>

        bool SetDivisonsFFSettings(int DivisonId);

        /// <summary>
        /// Gets Divison name in  Financial forecast settings.
        /// </summary>
        /// <returns>String</returns>
        string GetDivisonName();

        IList<IValdiationWithAttributeRelationData> GetEntityAttributesValidationDetails(int id, int entityTypeID);


        /// <summary>
        /// Get the entity status by entity id
        /// </summary>
        /// <param name="proxy"></param>
        /// <param name="entityID"></param>
        /// <returns></returns>
        IEntityStatus GetEntityStatusByEntityID(int entityID);

        /// <summary>
        /// Update the Entity status
        /// </summary>
        /// <param name="proxy"></param>
        /// <param name="entityID"></param>
        /// <param name="statusID"></param>
        /// <param name="intimeID"></param>
        /// <param name="comment"></param>
        /// <returns></returns>
        bool UpdateEntityStatus(int entityID, int statusID, int intimeID, string comment);



        bool CheckForMemberAvailabilityForEntity(int EntityID);

        int CreateAttributeGroupRecord(int AttributeGroupRecordID, int parentId, int typeId, int GroupID, Boolean isLock, string name, int SortOrder, IList<IAttributeData> entityattributedata, bool IsFromEntityCreation = false, bool IsFromPredefinedAttrGroup = false, int SelectedPredefinedId = 0);

        bool UpdateImageNameFromAttribtueGroup(int entityId, int attributeId, string imageName, int GroupID, int GroupRecordID);

        bool DeleteEntityAttributeGroupRecord(int GroupID, int GroupRecordID, int ParentID);

        bool SaveUploaderImage(string sourcepath, int imgwidth, int imgheight, int imgX, int imgY);
        void notificationForAddMember(int costcenterid, int entityid);
        IList<IAttributeData> GetEntityAttributesDetailsUserDetails(int UserID);

        int GetCurrentDivisionId();

        /// <summary>
        /// Getting  FinancialMetada attribute details
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The financialresult.</param>
        /// <returns>IFinancialMetadataAttributewithValues<</returns>
        IList<IFinancialMetadataAttributewithValues> GetFundingCostcenterMetadata(int metadataType);

        bool SaveFinancialDynamicValues(int finID, int AttributeTypeid, int attributeid, List<object> NewValue);

        string GetCaptionofPeriod(int entityId);

        /// <summary>
        /// Create New Purchase Order from the API
        /// </summary>
        /// <param name="proxy"></param>
        /// <param name="EntityID">The EntityID</param>
        /// <param name="CostCenterID">The Cost Center ID </param>
        /// <param name="IPurchaseOrder">The IPurchaseOrder</param>
        /// <param name="IList<IAttributeData>">The client specific attribute IDs and values</param>
        /// <returns>Purchase order ID</returns>
        int InsertUpdatePO(int EntityID, int CostCenterID, IPurchaseOrder ipurchaseOrder, IList<IAttributeData> MetadataValues);


        /// <summary>
        /// Create New Spent Transaction Order from the API
        /// </summary>
        /// <param name="proxy"></param>
        /// <param name="EntityID">The EntityID</param>
        /// <param name="CostCenterID">The Cost Center ID </param>
        /// <param name="IInvoice">The IInvoice</param>
        /// <param name="IList<IAttributeData>">The client specific attribute IDs and values</param>
        /// <returns>Spent Transaction order ID</returns>
        int InsertApiSpentTransaction(int EntityID, int CostCenterID, IInvoice iInvoiceObj, IList<IAttributeData> MetadataValues);

        /// <summary>
        /// Create New Spent Transaction Order from the API
        /// </summary>
        /// <param name="proxy"></param>
        /// <param name="EntityID">The EntityID</param>
        /// <param name="CostCenterID">The Cost Center ID </param>
        /// <param name="IInvoice">The IInvoice</param>
        /// <param name="IList<IAttributeData>">The client specific attribute IDs and values</param>
        /// <returns>Spent Transaction order ID</returns>
        int UpdateApiSpentTransaction(int EntityID, int CostCenterID, ArrayList UpdateAttributes, IInvoice iInvoiceObj, IList<IAttributeData> MetadataValues);

        List<bool> GetFinanncailforecastData();
        bool UpdateFFData(string financialdata, bool status);

        bool UpdateFinancialMetadata(int EntityId, int CostCenterID, IList<IAttributeData> MetadataValues);

        int UpdateApiPO(int EntityID, int CostCenterID, ArrayList jattributes, IPurchaseOrder ipurchaseOrder, IList<IAttributeData> MetadataValues);

        /// <summary>
        /// Getting API Entity Financial Details 
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="id">The entityid.</param>
        /// <returns>IList</returns>
        IList<IFinancialDetail> GetApiEntityFinancialdDetails(int entityid, int userID, int costcenterid = 0);

        decimal GetOverviewFinancialAmount(int entityId);

        /// <summary>
        /// GetLockStatus
        /// </summary>
        /// <param name="EntityID"></param>
        /// <returns>Lock status</returns>
        Tuple<bool, bool> GetLockStatus(int EntityID);

        /// <summary>
        /// GetEntityLevelAccess
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="EntityID"></param>
        /// <returns>string of user access and entityrole access to convert into json</returns>
        string GetEntityLevelAccess(int UserID, int EntityID);

        IEntity GetEntityDetailsByID(int EntityID);

        string InsertUpdateEntityPeriodLst(JObject jobj, int EntityID);

        bool GetAttachmentEditFeature();

        bool updateOverviewStatus(JArray statusObject);

        int CreateImportedAttributeGroupRecord(int AttributeGroupRecordID, int parentId, int typeId, int GroupID, Boolean isLock, string name, int SortOrder, IList<IAttributeData> entityattributedata);

        BrandSystems.Marcom.Core.Planning.SearchResult CustomSearch(string Text, List<BrandSystems.Marcom.Core.Planning.SearchTerm> SearchTerm, string searchtype, int PageID, bool IsGlobalAdmin, int rowsPerPage, string currentscroll, bool istag = false);


        BrandSystems.Marcom.Core.Planning.SearchResult CustomSearch(string Text, List<BrandSystems.Marcom.Core.Planning.SearchTerm> SearchTerm, string searchtype, int[] entitytypes, int PageID, bool IsGlobalAdmin, int rowsPerPage, string currentscroll, bool istag = false, string orderbyfieldname = "Name", bool desc = false);


        IList<IEntity> GetEntitiesfrCalender(IList<ICalenderFulfillCondtions> objCalFullfilConditionsList);

        int CreateCalender(string CalenderName, bool status, string CalenderDescription, IList<ICalenderFulfillCondtions> objCalenderFullfilConditionsList, IList<IEntityRoleUser> objMembersList, List<int> selectedEntities, DateTime? CalenderPublishedOn, int CalenderVisPeriod, int CalenderVisType, bool CalenderisExternal, List<int> selectedTabs);

        IList<ICalender> GetCalenders();

        IList<int> GetEntitiesforSelectedCalender(int calenderID);

        ICalenderFulfillCondtions GettingCalenderFulfillmentBlockDetails(int calenderID);

        // dynamic GettingEditCalenderFulfillmentDetails(int p);

        Tuple<IList<ICalenderFulfillCondtions>, IList<int>> GettingEditCalenderFulfillmentDetails(int calenderId);

        //int UpdateCalenderFulfillmentCondition(int calenderId, IList<ICalenderFulfillCondtions> calFullfilConditions, string CalFulfillDeatils);


        //dynamic UpdateCalenderFulfillmentCondition(int p1, IList<IObjectiveFulfillCondtions> objFullfilConditionsList, string p2);

        int UpdateCalenderFulfillmentCondition(int p1, IList<ICalenderFulfillCondtions> objFullfilConditionsList, string p2, List<int> selectedEntities);
        bool UpdatingCalenderOverDetails(int calId, string calName, string calDescription, string Typeid);

        ICalender GetCalenderDetailsByID(int p);

        int SaveCalenderDetails(int calID, bool isExternal, int VisibilityPeriod, int VisibilityType, DateTime CalenderPublishedOn, List<int> selectedtabs);

        IList<int> GetTabsforCalender(int CalenderID);

        int CreateCmsPageEntity(int parentId, int typeId, Boolean active, Boolean isLock, string name, IList<IEntityRoleUser> entityMembers, IList<IEntityCostReleations> entityCostcentres, IList<IEntityPeriod> entityPeriods, IList<IFundingRequest> listFundrequest, IList<IAttributeData> entityattributedata, int NavID, int TemplateID, string PublishedDate, string PublishedTime, int[] assetIdArr = null, IList<IObjectiveEntityValue> entityObjectvalues = null, IList<object> attributes = null, IList<IEntityAmountCurrencyType> listentityamountcurrencytype = null);



        int GetCalendarDetailsbyExternalID(string ExternalUrlID);

        int UpdateFinancialForecastsettings(int Id, bool IsFinancialforecast, int ForecastDivision, int ForecastBasis, int ForecastLevel, int Forecastdeadlines);

        Tuple<object, object> getfinancialForecastIds();

        IFinancialForecastSettings GetFinancialForecastsettings();

        Object GetEntityFinancialdForecastHeadings(int entityID, int DivisionID, bool Iscc);

        Tuple<string, string> GetlastUpdatedtime(int entityID);

        bool RemoveEntityAsync(int EntityID);

        List<SampleData> PerformKeywordSearch(string Text, string searchcategory);

        bool UpdateEntityforSearch(string EntityIDs, string name, string searchtype = "");

        bool UpdateEntityforSearchAsync(string EntityIDs, string name, string searchtype = "");

        bool UpdateEntityforSearchAsync(int TaskId, string name, string searchtype = "");

        bool UpdateEntityforSearchAsync(int MilestoneId, string MilestoneName, int EntityId, string searchtype = "");

        bool UpdateEntityforSearchAsync(int EntityID, int GroupRecordID, string EntityName, string GroupRecordName, int GroupID, string searchtype = "");

        bool UpdateEntityAsyncForDam(int AssetID, string NewValue, string searchtype = "");

        bool AddEntityAsyncDam(int AssetID, int FileID, string searchtype = "");

        bool AddEntityAsyncDamNew(int AssetID, int FileID, string searchtype = "");

        decimal GetCostCentreAssignedAmount(int CostCentreID);

        string GetCostcentreTreeforPlanCreation(int EntityTypeID, int fiscalyear = 0, int entityid = 0);

        /// <summary>
        /// GetSearchTerm
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>List of SearchTerm from JObject</returns>
        List<SearchTerm> GetSearchTerm(JArray obj);

        /// <summary>
        /// Get the Currency rate for the selected currency type
        /// </summary>
        /// <param name="currencyType"></param>
        /// <returns></returns>
        List<string> GetCostCentreCurrencyRateById(int EntityId, int CurrencyId = 0, bool IsCostCentreCreation = false);

        string DeleteApprovedAlloc(string UniqueId);
        string GetApprvdAlloc(string EntityId);

        /// <summary>
        /// INSERT UPDATE ALL THE FINANCIAL TRANCATION ACCORDING TO THE CURRENCY TYPE SELECTED
        /// </summary>
        /// <returns>BOOL</returns>
        bool InsertUpdateFinancialTrancation(List<object> FinancialTranction, int TransactionType);

        /// <summary>
        /// ON CHANGE OF TOTAL ASSIGNED AMOUNT IN COST CENTRE WE NEED TO UPDATE FINANCIAL TRACTION ACCORDING TO THE CURRECY TYPE
        /// </summary>
        /// <returns></returns>
        bool UpdateTotalAssigAmtInFinancialTransaction(decimal TotalAssignAmount, int CostCentreID, int currencyType, decimal currencyRate);

        IList GetCustomGlobalRoleMembers(int[] roleids, int typeid);

        IList GetTaskMinMaxValue(int Tasktypeid);

        IList<IEntityRoleUser> GetDirectEntityMember(int EntityID);

		IList<ICurrencyType> GetCurrencyListFFsettings();
        bool AddEntityAsync(int TaskId, string Name, string Desc, string searchtype = "");
        IList<object> GetDynamicfinancialattributes();

        IList<object> GetObjectiveUnitsOptionValues(int unitType);

        IList<object> GetAllObjectiveUnits(int ObjectiveId);

        bool SavePredefinedValuesToAttribtueGroup(int[] SelectedPreDefinedIDs, int GroupID, int EntityID, int typeId, int AttrGroupID, Boolean isLock, string name, int SortOrder, bool IsFromEntityCreation = false);

        JObject InsertPreDefinedAttributeGroupDataFromMiddleWare(string AttributeGroupData, string AttributeGroupHeaderCaption, int AttributeGroupID);

        JObject GetEntityTypeAttributeDetailsListToMiddleWare(int EntityTypeID, int ParentId);


        JObject GetEntityListToMiddleWare(string EntityId);

        IList GetEntityOverallStatus();
        IList GetAllEntityStautsDtls();
        IList GetEntityTypeStatus(int EntityTypeID);

        int InsertUpdateEntityTemplate(JObject jObj);
        List<object> GetEntityTemplateDetails();

        bool UpdateEntityTypeOverallStatus(JObject jObj);
        IList GetAllEntityEndDate();
        bool DeleteEntityTemplateListById(int templateID);

        JObject GetObjectiveListToMiddleWare(string EntityId);
        bool UpdateObjectiveListInMiddleWare(int EntityId, int ObjectiveID, decimal PlannedTrgt, decimal TrgtOutcome);

        string copyuploadedImage(string filename);

        List<Tuple<string, string>> savemultipleUploadedImg(JObject ImgArr);

        JObject GetEntityFinancialListToMiddleWare(string EntityId);


    }

}
