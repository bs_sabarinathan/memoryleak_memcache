﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Task
{
   public class SourceDestinationMember
    {
       public int Id { get; set; }
       public int EntityID { get; set; }
       public int RoleID { get; set; }
       public string RoleName { get; set; }
       public int ActualRoleId { get; set; }
       public bool Status { get; set; }
       public int UserID { get; set; }
       public string UserName { get; set; }
       public bool IstaskOwner { get; set; }
    }
}
