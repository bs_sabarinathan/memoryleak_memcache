﻿(function (ng, app) {
    "use strict";

    function AssetCreationService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            GetAllExtensionTypesforDAM: GetAllExtensionTypesforDAM,
            S3Settings: S3Settings,
            GetAssetCategoryTree: GetAssetCategoryTree,
            GetAssetCategoryPathInAssetEdit: GetAssetCategoryPathInAssetEdit,
            GetDamAttributeRelation: GetDamAttributeRelation,
            SaveAsset: SaveAsset,
            SaveBlankAsset: SaveBlankAsset,
            savemultipleUploadedImg: savemultipleUploadedImg,
            writeUploadErrorlog: writeUploadErrorlog
        });

        function GetAllExtensionTypesforDAM() {
            var request = $http({
                method: "get",
                url: "api/dam/GetDAMExtensions/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function S3Settings() {
            var request = $http({
                method: "get",
                url: "api/dam/S3Settings",
                params: {
                    action: "Get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAssetCategoryTree(AsetTypeID) {
            var request = $http({
                method: "Get",
                url: "api/dam/GetAssetCategoryTree/" + AsetTypeID,
                params: {
                    action: "Get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAssetCategoryPathInAssetEdit(AssetTypeID) {
            var request = $http({
                method: "Get",
                url: "api/dam/GetAssetCategoryPathInAssetEdit/" + AssetTypeID,
                params: {
                    action: "Get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDamAttributeRelation(damid) {
            var request = $http({
                method: "get",
                url: "api/dam/GetDamAttributeRelation/" + damid,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveAsset(SaveAsset) {
            var request = $http({
                method: "post",
                url: "api/dam/CreateAsset/",
                params: {
                    action: "add"
                },
                data: {
                    saveAsset: SaveAsset
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveBlankAsset(jobj) {
            var request = $http({
                method: "post",
                url: "api/dam/CreateBlankAsset/",
                params: {
                    action: "add"
                },
                data: {
                    saveAsset: jobj
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function savemultipleUploadedImg(uplImgArr) {
            var request = $http({
                method: "post",
                url: "api/Planning/savemultipleUploadedImg/",
                params: {
                    action: "add"
                },
                data: {
                    uplImgArr: uplImgArr
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function writeUploadErrorlog(loginfo) {
            var request = $http({
                method: "post",
                url: "api/dam/writeUploadErrorlog/",
                params: {
                    action: "add"
                },
                data: loginfo
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.service("AssetCreationService", ['$http', '$q', AssetCreationService]);
})(angular, app);