﻿(function (ng, app) {
    "use strict"; function muiadminCtrl($scope, AdminService) {
        $scope.$on("$destroy", function () { RecursiveUnbindAndRemove($("[ng-controller='mui.adminCtrl']")); }); $scope.ChangeAcive = function (index) {
            $(".accordion-heading.active").removeClass('active'); if (index == 0) { $("#SuperAdminTab").addClass('active'); }
if(index==1){$("#GeneralTab").addClass('active');}
else if(index==2){$("#UserTab").addClass('active');}
else if(index==3){$("#PlanningTab").addClass('active');}
else if(index==4||index==5||index==6||index==7){if(index==5){$scope.financialAttrType=3;}
else if(index==6){$scope.financialAttrType=1;}
else if(index==7){$scope.financialAttrType=2;}
$("#FinancialTab").addClass('active');}
else if(index==8){$("#DashBoardTab").addClass('active');}
else if(index==9){$("#ObjectiveTab").addClass('active');}
else if(index==10){$("#TaskTab").addClass('active');}
else if(index==11){$("#ReportTabe").addClass('active');}
else if(index==12){$("#DamTab").addClass('active');}}
$scope.GeneralMenuList=[];AdminService.GetGlobalRoleUserByID($scope.UserId).then(function(getroleuserbyid){$scope.RolesOfUsers=getroleuserbyid.Response;if(getroleuserbyid.Response!=null){$scope.resultsUser=Math.min.apply(Math,$scope.RolesOfUsers)
AdminService.GetSuperglobalacl().then(function(objgetgetsuperglobalacl){$scope.resul=objgetgetsuperglobalacl.Response;if(objgetgetsuperglobalacl.Response!=null){$scope.GeneralMenuList=($.grep(objgetgetsuperglobalacl.Response,function(e){return e.GlobalRoleid==$scope.resultsUser&&e.AccessPermission==true&&e.Featureid!=0;}));if($scope.GeneralMenuList.length>0){var CollapsingId=$('#'+$scope.GeneralMenuList[0].Featureid).parents(".accordion-body").attr('id');$("#adminTreeMenu").find('.collapsed').removeClass('collapsed').height(0)
$("#"+CollapsingId).removeClass('collapse');$("#"+CollapsingId).addClass('in collapse');}}});}});}
app.controller("mui.adminCtrl",['$scope','AdminService',muiadminCtrl]);})(angular,app);