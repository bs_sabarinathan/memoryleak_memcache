using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
    /// MailFooter object for table 'CM_MailFooter'.
	/// </summary>
	
	internal class MailFooter : IMailFooter
	{
		#region Member Variables

		protected int _id;        
        protected string _body;
        protected string _description;
		
		
		#endregion
		
		#region Constructors
		public MailFooter() {}

        public MailFooter(int pid,  string pBody, string pDescription)
		{
            this._id = pid;            
            this._body = pBody;
            this._description = pDescription; 
		}

        public MailFooter(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}

       

        public string Body
		{
			get { return _body; }
            set { _body = value; }
			
		}

        public string description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 4000)
                  throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 100 characters");
              _description = value; 
			}
			
		}
		
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			FeedComment castObj = null;
			try
			{
				castObj = (FeedComment)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
