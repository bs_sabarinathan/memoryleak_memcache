﻿(function (ng, app) {
    "use strict";

    function muiassetreformatCtrl($window, $location, $timeout, $scope, $cookies, $resource, AssetReformatService, $sce, $compile, $translate, $modalInstance, params) {
        var timerObj = {};
        timerObj.openpopuptimer = $timeout(function () { openAssetReformatPopup(params.thubmnailGuid, params.assetid, params.folderid, params.EntityID, params.fromisassetlibrary); }, 100);

        function openAssetReformatPopup(thubmnailGuid, assetid, folderid, EntityID, fromisassetlibrary) {
            $scope.mimes = {};
            var timeoutvar1 = $timeout(function () {
            $scope.profileCollection = [{
                "ID": "-1",
                "Name": "No change"
            }];
            }, 100);
            $scope.Unitsshortname = "px";
            $scope.ProfileResData = [];
            $scope.AssetEditID = assetid;
            $scope.convertWidth = 0;
            $scope.convertHeight = 0;
            $scope.imageunitshortFormat = 'px';
            $scope.Reformatestatus = false;
            $scope.ReformateAssetId = 0;
            $scope.fromisassetlibrary = fromisassetlibrary;
            var Cusratiocheckbox = null;
            var keeporginalcheckbox = null;
            var Aspetratiocheckbox = null;
            $scope.AssetReformat = { "TomailAddress": [] };
            var timeoutvar = $timeout(function () {
                $("#wizardStep2").addClass("displayNone");
                $('#btnSaveRescale').removeClass("displayNone");
                $("#wizardStep1").removeClass("displayNone");
                $('#Rotate90').removeClass("active");
                $('#Rotate180').removeClass("active");
                $('#Rotate270').removeClass("active");
                $('#Reasseteditimage').css('height', '0px');
                $scope.perviousUnitsshortname = "px";
            }, 300);
            $scope.AssetPreviewObj = {
                AssetId: assetid,
                AssetCeatedOn: "",
                AssetUpdatedOn: "",
                PublisherID: 0,
                PublishedOn: "",
                VersionCeatedOn: "",
                EntityID: EntityID,
                Fileguid: thubmnailGuid,
                folderid: folderid
            };
            $scope.ReformateAssetObj = {
                AssetId: 0,
                Fileguid: "",
                Extension: "",
                EntityID: 0,
                FileName: "",
                status: false
            };
            $scope.select2Options = {
                tags: [],
                multiple: true,
                simple_tags: false,
                minimumInputLength: 1,
                formatResult: function (item) {
                    return item.text;
                },
                formatSelection: function (item) {
                    return item.text;
                }
            }
            $scope.ReformateAssetObj.EntityID = EntityID;
            var jcrop_api;
            var wRatio;
            var hRatio;
            GetMimeTypevalue();
            var imageBaseUrlcloud = (cloudsetup.storageType == clientFileStoragetype.local) ? TenantFilePath : (cloudsetup.Uploaderurl + "/" + cloudsetup.BucketName + "/" + TenantFilePath).replace(/\\/g, "\/");
            $scope.reobj = {
                Crop: {
                    "keeporiginal": false,
                    "Cropenable": false,
                    "Topleftx": 0,
                    "Toplefty": 0,
                    "Bottomrightx": 0,
                    "Bottomrighty": 0,
                    "Aspetratiow": 0,
                    "Aspetratioh": 0,
                    "corpWidth": 0,
                    "corpHeight": 0
                },
                CropResize: {
                    "preserveAspetratio": false,
                    "CropResizeenable": false,
                    "Height": 0,
                    "Width": 0,
                    "Resizeper": 100,
                    "corpWidth": 0,
                    "corpHeight": 0
                },
                Rotate: {
                    "Rotateenable": false,
                    "Rotate": 0
                },
                Formate: {
                    "Formateenable": false,
                    "mime": $scope.Activefileformat,
                    "Dpi": $scope.orgDpi,
                    "Quality": 100
                },
                Roundcorner: {
                    "Roundcornerenable": false,
                    "Topleft": 0,
                    "Topright": 0,
                    "Bottomleft": 0,
                    "Bottomright": 0,
                    "cornercolour": ""
                },
            };
            var timeoutUnitsCollection = $timeout(function () {
            $scope.UnitsCollection = [{
                "ID": "1",
                "Name": "Pixels",
                "shortname": "px"
            }, {
                "ID": "2",
                "Name": "Inches",
                "shortname": "inch"
            }, {
                "ID": "3",
                "Name": "Millimeter",
                "shortname": "mm"
            }];
            }, 100);
            $scope.QualityCollection = [{
                "ID": "100"
            }, {
                "ID": "90"
            }, {
                "ID": "80"
            }, {
                "ID": "70"
            }, {
                "ID": "60"
            }, {
                "ID": "50"
            }, {
                "ID": "40"
            }, {
                "ID": "30"
            }, {
                "ID": "20"
            }, {
                "ID": "10"
            }];
            timerObj.setquality = $timeout(function () {
                $scope.profprofile = -1;
                $scope.profUnits = 1;
                $scope.reobj.Formate.Quality = 100;
            }, 50);

            function Getdefaultvalue() {
                $scope.reobj = {
                    Crop: {
                        "keeporiginal": false,
                        "Cropenable": false,
                        "Topleftx": 0,
                        "Toplefty": 0,
                        "Bottomrightx": 0,
                        "Bottomrighty": 0,
                        "Aspetratiow": 0,
                        "Aspetratioh": 0,
                        "corpWidth": 0,
                        "corpHeight": 0
                    },
                    CropResize: {
                        "preserveAspetratio": false,
                        "CropResizeenable": false,
                        "Height": 0,
                        "Width": 0,
                        "Resizeper": 100,
                        "corpWidth": 0,
                        "corpHeight": 0
                    },
                    Rotate: {
                        "Rotateenable": false,
                        "Rotate": 0
                    },
                    Formate: {
                        "Formateenable": false,
                        "mime": $scope.Activefileformat,
                        "Dpi": $scope.orgDpi,
                        "Quality": 100
                    },
                    Roundcorner: {
                        "Roundcornerenable": false,
                        "Topleft": 0,
                        "Topright": 0,
                        "Bottomleft": 0,
                        "Bottomright": 0,
                        "cornercolour": "ffffff"
                    }
                };
                $scope.profprofile = -1;
                $scope.profUnits = 1;
                $scope.reobj.Formate.Quality = 100;
                $scope.Unitsshortname = "px";
                $scope.reobj.CropResize.CropResizeenable = false;
                $('#preserveacceptration').removeClass('disabled');
                $scope.reobj.Crop.Cropenable = false;
                $('#enabledisableproportion').removeClass('disabled');
                $('#CusratioIcon1').removeClass('disabled');
            }
            $scope.colorchange = function (color) { }
            timerObj.getreformatprofile = $timeout(function () {
                AssetReformatService.GetReformatProfile(-1).then(function (result) {
                    if (result.Response == 0) { } else {
                        $scope.ProfileResData = $.grep(result.Response, function (e) {
                            return e.ID != -1;
                        });
                        if ($scope.ProfileResData != undefined) {
                            for (var k = 0, data; data = $scope.ProfileResData[k++];) {
                                $scope.profileCollection.push({
                                    "ID": data["ID"],
                                    "Name": data["Name"]
                                });
                            }
                            $scope.ProfileSrcData = JSON.parse($scope.ProfileResData[0].ProfileSrcValue).ActionSrcValue.ProfileSrcValue;
                            if ($scope.ProfileSrcData != undefined) {
                                $scope.reobj.Crop.Cropenable = false;
                                $('#enabledisableproportion').removeClass('disabled');
                                $('#CusratioIcon1').removeClass('disabled');
                                $scope.reobj.CropResize.CropResizeenable = false;
                                $('#preserveacceptration').removeClass('disabled');
                            }
                        } else {
                            $scope.Unitsshortname = "px";
                            $scope.ProfileResData = [];
                            $scope.profprofile = -1;
                            $scope.profUnits = 1;
                        }
                    }
                });
            }, 100);
            $scope.profilechange = function (profile) {
                $('#preserveacceptration').removeClass('checked');
                $('#CusratioIcon1').removeClass('checked');
                if (jcrop_api != undefined) jcrop_api.release();
                if (profile == -1) {
                    Getdefaultvalue();
                    $scope.cropopen();
                } else if ($scope.ProfileResData != undefined && profile > 0) {
                    var currProfileResData = $.grep($scope.ProfileResData, function (e) {
                        return e.ID == profile;
                    });
                    $scope.profprofile = currProfileResData[0].ID;
                    $scope.profprofilename = currProfileResData[0].Name;
                    $scope.profUnits = currProfileResData[0].Units;
                    if ($scope.profUnits > 0) {
                        var taskActionSrc = $.grep($scope.UnitsCollection, function (e) {
                            return e.ID == $scope.profUnits;
                        });
                        $scope.Unitsshortname = taskActionSrc[0].shortname;
                    }
                    $scope.ProfileSrcData = JSON.parse(currProfileResData[0].ProfileSrcValue).ActionSrcValue.ProfileSrcValue;
                    if ($scope.ProfileSrcData != undefined) {
                        $scope.reobj.Crop = $scope.ProfileSrcData.Crop;
                        $scope.reobj.CropResize = $scope.ProfileSrcData.CropResize;
                        $scope.reobj.Formate = $scope.ProfileSrcData.Formate;
                        $scope.reobj.Roundcorner = $scope.ProfileSrcData.Roundcorner;
                        if ($scope.ProfileSrcData.Crop.Cropenable) {
                            $scope.reobj.Crop.Cropenable = true;
                            $('#enabledisableproportion').addClass('disabled');
                            $('#CusratioIcon1').addClass('disabled');
                        } else {
                            $scope.reobj.Crop.Cropenable = false;
                            $('#enabledisableproportion').removeClass('disabled');
                            $('#CusratioIcon1').removeClass('disabled');
                        }
                        if ($scope.ProfileSrcData.CropResize.CropResizeenable) {
                            $scope.reobj.CropResize.CropResizeenable = true;
                            $('#preserveacceptration').addClass('disabled');
                        } else {
                            $scope.reobj.CropResize.CropResizeenable = false;
                            $('#preserveacceptration').removeClass('disabled');
                        }
                    }
                    if ($scope.Unitsshortname == 'px') {
                        $scope.reobj.Crop.Topleftx = 0;
                        $scope.reobj.Crop.Toplefty = 0;
                        $scope.reobj.Crop.Bottomrightx = $scope.orgWidth;
                        $scope.reobj.Crop.Bottomrighty = $scope.orgHeight;
                    } else {
                        $scope.reobj.Crop.Topleftx = 0;
                        $scope.reobj.Crop.Toplefty = 0;
                        $scope.reobj.Crop.Bottomrightx = ChangeUnit($scope.reobj.Formate.Dpi, $scope.orgWidth, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.Crop.Bottomrighty = ChangeUnit($scope.reobj.Formate.Dpi, $scope.orgHeight, $scope.Unitsshortname).toFixed(1);
                    }
                    if ($scope.reobj.Crop.Aspetratiow > 0 || $scope.reobj.Crop.Aspetratioh > 0) {
                        $scope.Cusratio = true
                        $scope.Customratiospan = true;
                        initJcrop($scope.reobj.Crop.Aspetratiow, $scope.reobj.Crop.Aspetratioh);
                        Resizeperset();
                    } else {
                        $scope.Cusratio = false
                    }
                    $scope.reobj.Formate.Quality = 100;
                }
            }

            function GetMimeTypevalue() {
                AssetReformatService.GetMimeType().then(function (data) {
                    if (data.Response != null) {
                        $scope.mimes = data.Response;
                    }
                });
            }
            GetAssetAttribuuteDetails(assetid);
            GetAssetActiveFileinfo(assetid);
            $scope.RestCropvalues = function () {
                if (jcrop_api != undefined) jcrop_api.release();
                if ($scope.ProfileResData != undefined && $scope.profprofile > 0) {
                    var currProfileResData = $.grep($scope.ProfileResData, function (e) {
                        return e.ID == $scope.profprofile;
                    });
                    $scope.ProfileSrcData = JSON.parse(currProfileResData[0].ProfileSrcValue).ActionSrcValue.ProfileSrcValue;
                    if ($scope.ProfileSrcData != undefined) {
                        $scope.reobj.Crop = $scope.ProfileSrcData.Crop;
                        if ($scope.reobj.Crop.Aspetratiow > 0 || $scope.reobj.Crop.Aspetratioh > 0) {
                            $scope.Cusratio = true
                            $scope.Customratiospan = true;
                            initJcrop($scope.reobj.Crop.Aspetratiow, $scope.reobj.Crop.Aspetratioh);
                        }
                    }
                } else {
                    $scope.reobj.Crop = {
                        "keeporiginal": false,
                        "Cropenable": false,
                        "Topleftx": 0,
                        "Toplefty": 0,
                        "Bottomrightx": 0,
                        "Bottomrighty": 0,
                        "Aspetratiow": 0,
                        "Aspetratioh": 0,
                        "corpWidth": 0,
                        "corpHeight": 0
                    };
                    $scope.Cusratio = false;
                    $scope.Customratiospan = false;
                    if ($scope.Unitsshortname == 'px') {
                        $scope.reobj.Crop.Bottomrightx = $scope.orgWidth;
                        $scope.reobj.Crop.Bottomrighty = $scope.orgHeight;
                        $scope.reobj.Crop.Topleftx = 0;
                        $scope.reobj.Crop.Toplefty = 0;
                    } else {
                        $scope.reobj.Crop.Topleftx = 0;
                        $scope.reobj.Crop.Toplefty = 0;
                        $scope.reobj.Crop.Bottomrightx = ChangeUnit($scope.reobj.Formate.Dpi, $scope.orgWidth, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.Crop.Bottomrighty = ChangeUnit($scope.reobj.Formate.Dpi, $scope.orgHeight, $scope.Unitsshortname).toFixed(1);
                    }
                    if (jcrop_api != undefined) {
                        jcrop_api.release();
                        jcrop_api.setSelect([0, 0, $scope.orgWidth, $scope.orgHeight]);
                    }
                }
            }
            $scope.RestCropResize = function () {
                if ($scope.ProfileResData != undefined && $scope.profprofile > 0) {
                    var currProfileResData = $.grep($scope.ProfileResData, function (e) {
                        return e.ID == $scope.profprofile;
                    });
                    $scope.ProfileSrcData = JSON.parse(currProfileResData[0].ProfileSrcValue).ActionSrcValue.ProfileSrcValue;
                    if ($scope.ProfileSrcData != undefined) {
                        $scope.reobj.CropResize = $scope.ProfileSrcData.CropResize;
                    }
                } else {
                    $scope.reobj.CropResize = {
                        "preserveAspetratio": false,
                        "CropResizeenable": false,
                        "Height": 0,
                        "Width": 0,
                        "Resizeper": 100,
                        "corpWidth": 0,
                        "corpHeight": 0
                    };
                    if ($scope.Unitsshortname == 'px') {
                        $scope.reobj.CropResize.corpWidth = $scope.orgWidth;
                        $scope.reobj.CropResize.corpHeight = $scope.orgHeight;
                        $scope.reobj.CropResize.Width = $scope.orgWidth;
                        $scope.reobj.CropResize.Height = $scope.orgHeight;
                        $scope.reobj.CropResize.Resizeper = 100;
                    } else {
                        $scope.reobj.CropResize.corpWidth = ChangeUnit($scope.reobj.Formate.Dpi, $scope.orgWidth, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.CropResize.corpHeight = ChangeUnit($scope.reobj.Formate.Dpi, $scope.orgHeight, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.CropResize.Width = ChangeUnit($scope.reobj.Formate.Dpi, $scope.orgWidth, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.CropResize.Height = ChangeUnit($scope.reobj.Formate.Dpi, $scope.orgHeight, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.CropResize.Resizeper = 100;
                    }
                }
            }
            $scope.RestRotate = function () {
                $scope.reobj.Rotate = {
                    "Rotateenable": false,
                    "Rotate": 0
                };
                $('#Rotate90').removeClass("active");
                $('#Rotate180').removeClass("active");
                $('#Rotate270').removeClass("active");
            }
            $scope.RestFormate = function () {
                if ($scope.ProfileResData != undefined && $scope.profprofile > 0) {
                    var currProfileResData = $.grep($scope.ProfileResData, function (e) {
                        return e.ID == $scope.profprofile;
                    });
                    $scope.ProfileSrcData = JSON.parse(currProfileResData[0].ProfileSrcValue).ActionSrcValue.ProfileSrcValue;
                    if ($scope.ProfileSrcData != undefined) {
                        $scope.reobj.Formate = $scope.ProfileSrcData.Formate;
                        $scope.reobj.Formate.Quality = 100;
                    }
                } else $scope.reobj.Formate = {
                    "Formateenable": false,
                    "mime": $scope.Activefileformat,
                    "Dpi": $scope.orgDpi,
                    "Quality": 100
                };
            }
            $scope.RestRoundcorner = function () {
                if ($scope.ProfileResData != undefined && $scope.profprofile > 0) {
                    var currProfileResData = $.grep($scope.ProfileResData, function (e) {
                        return e.ID == $scope.profprofile;
                    });
                    $scope.ProfileSrcData = JSON.parse(currProfileResData[0].ProfileSrcValue).ActionSrcValue.ProfileSrcValue;
                    if ($scope.ProfileSrcData != undefined) {
                        $scope.reobj.Roundcorner = $scope.ProfileSrcData.Roundcorner;
                    }
                } else $scope.reobj.Roundcorner = {
                    "Roundcornerenable": false,
                    "Topleft": 0,
                    "Topright": 0,
                    "Bottomleft": 0,
                    "Bottomright": 0,
                    "cornercolour": "ffffff"
                };
            }
            $scope.Unitschange = function (Units, result) {
                if (result) {
                    if (Units == 2 || Units == 3 || Units == 1) {
                        if ($scope.reobj.Formate.Dpi == 0) {
                            bootbox.alert($translate.instant('LanguageContents.Res_1118.Caption'));
                            return false;
                        }
                    }
                }
                if (Units > 0) {
                    var taskActionSrc = $.grep($scope.UnitsCollection, function (e) {
                        return e.ID == Units;
                    });
                    $scope.Unitsshortname = taskActionSrc[0].shortname;
                }
                if (Units == 3) {
                    $scope.convertWidth = pixels_dpi_to_mm($scope.orgWidth, $scope.reobj.Formate.Dpi).toFixed(1);
                    $scope.convertHeight = pixels_dpi_to_mm($scope.orgHeight, $scope.reobj.Formate.Dpi).toFixed(1);
                } else if (Units == 2) {
                    $scope.convertWidth = pixels_dpi_to_inch($scope.orgWidth, $scope.reobj.Formate.Dpi).toFixed(1);
                    $scope.convertHeight = pixels_dpi_to_inch($scope.orgHeight, $scope.reobj.Formate.Dpi).toFixed(1);
                } else if (Units == 1 || Units == 0) {
                    $scope.convertWidth = $scope.orgWidth;
                    $scope.convertHeight = $scope.orgHeight;
                }
                $scope.reobj.CropResize.Resizeper = 100;
                if ($scope.perviousUnitsshortname != $scope.Unitsshortname) {
                    if ($scope.Unitsshortname == 'px') {
                        $scope.reobj.Crop.Topleftx = $scope.TopXValue;
                        $scope.reobj.Crop.Toplefty = $scope.TopYValue;
                        $scope.reobj.Crop.Bottomrightx = $scope.ButtonXValue;
                        $scope.reobj.Crop.Bottomrighty = $scope.ButtonYValue;
                        $scope.reobj.Crop.corpWidth = $scope.corpWidth;
                        $scope.reobj.Crop.corpHeight = $scope.corpHeight;
                        $scope.reobj.CropResize.corpWidth = $scope.corpWidth;
                        $scope.reobj.CropResize.corpHeight = $scope.corpHeight;
                        $scope.reobj.CropResize.Width = $scope.corpWidth;
                        $scope.reobj.CropResize.Height = $scope.corpHeight;
                        $scope.reobj.Roundcorner.Topleft = ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.Roundcorner.Topleft, $scope.perviousUnitsshortname);
                        $scope.reobj.Roundcorner.Topright = ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.Roundcorner.Topright, $scope.perviousUnitsshortname);
                        $scope.reobj.Roundcorner.Bottomleft = ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.Roundcorner.Bottomleft, $scope.perviousUnitsshortname);
                        $scope.reobj.Roundcorner.Bottomright = ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.Roundcorner.Bottomright, $scope.perviousUnitsshortname);
                    } else {
                        $scope.reobj.Crop.Topleftx = ChangeUnit($scope.reobj.Formate.Dpi, $scope.TopXValue, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.Crop.Toplefty = ChangeUnit($scope.reobj.Formate.Dpi, $scope.TopYValue, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.Crop.Bottomrightx = ChangeUnit($scope.reobj.Formate.Dpi, $scope.ButtonXValue, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.Crop.Bottomrighty = ChangeUnit($scope.reobj.Formate.Dpi, $scope.ButtonYValue, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.Crop.corpWidth = ChangeUnit($scope.reobj.Formate.Dpi, $scope.corpWidth, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.Crop.corpHeight = ChangeUnit($scope.reobj.Formate.Dpi, $scope.corpHeight, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.CropResize.corpWidth = ChangeUnit($scope.reobj.Formate.Dpi, $scope.corpWidth, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.CropResize.corpHeight = ChangeUnit($scope.reobj.Formate.Dpi, $scope.corpHeight, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.CropResize.Width = ChangeUnit($scope.reobj.Formate.Dpi, $scope.corpWidth, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.CropResize.Height = ChangeUnit($scope.reobj.Formate.Dpi, $scope.corpHeight, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.Roundcorner.Topleft = ChangeUnit($scope.reobj.Formate.Dpi, $scope.reobj.Roundcorner.Topleft, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.Roundcorner.Topright = ChangeUnit($scope.reobj.Formate.Dpi, $scope.reobj.Roundcorner.Topright, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.Roundcorner.Bottomleft = ChangeUnit($scope.reobj.Formate.Dpi, $scope.reobj.Roundcorner.Bottomleft, $scope.Unitsshortname).toFixed(1);
                        $scope.reobj.Roundcorner.Bottomright = ChangeUnit($scope.reobj.Formate.Dpi, $scope.reobj.Roundcorner.Bottomright, $scope.Unitsshortname).toFixed(1);
                    }
                    $scope.perviousUnitsshortname = $scope.Unitsshortname;
                }
            }
            $scope.crop = function () {
                var ID = 0;
                if ($scope.AssetEditID != undefined) ID = $scope.AssetEditID;
                else ID = $scope.AssetEditID;
                if (ID !== undefined) {
                    timerObj.getassetactivefileinfo = $timeout(function () {
                        AssetReformatService.GetAssetActiveFileinfo(ID).then(function (result) {
                            if (result.Response != "") {
                                var generatedids = result.Response;
                                var Activefileexn = result.Response[0].Extension;
                                $scope.Activefileformat = Activefileexn.substring(1, Activefileexn.length);
                                var addinfo = (result.Response[0].Additionalinfo != "") ? JSON.parse(result.Response[0].Additionalinfo) : "";
                                if (addinfo != undefined && addinfo != "") {
                                    $scope.orgWidth = addinfo.Width;
                                    $scope.orgHeight = addinfo.Height;
                                    $scope.orgDpi = addinfo.Dpi;
                                    $scope.convertHeight = addinfo.Height;
                                    $scope.convertWidth = addinfo.Width;
                                    $scope.reobj.CropResize.corpWidth = addinfo.Width;
                                    $scope.reobj.CropResize.corpHeight = addinfo.Height;
                                    $scope.reobj.CropResize.Width = addinfo.Width;
                                    $scope.reobj.CropResize.Height = addinfo.Height;
                                    $scope.reobj.Formate.Dpi = addinfo.Dpi;
                                    $scope.reobj.Formate.mime = $scope.Activefileformat;
                                } else {
                                    $scope.orgWidth = 0;
                                    $scope.orgHeight = 0;
                                    $scope.orgDpi = 0;
                                }
                            }
                            initJcrop(0, 0);
                            if (jcrop_api != undefined) {
                                jcrop_api.release();
                                jcrop_api.setSelect([0, 0, $scope.orgWidth, $scope.orgHeight]);
                            }
                            GetMimeTypevalue();
                        });
                    }, 100);
                }
            }
            $scope.cropopen = function () {
                Cropdefault();
                $scope.CropUnit = 1;
                $scope.crop();
            }

            function initJcrop(WidthRatio, HeightRatio) {
                $('.jcrop-holder').remove();
               // $('.jcrop-holder').not(':last').remove();
                $('#Reasseteditimage').removeAttr('style');
                if (jcrop_api != undefined) {
                    jcrop_api.release();
                    jcrop_api.destroy();
                    $('#Reasseteditimage').removeAttr('style');
                }
                $('#Reasseteditimage').css('height', '400px');
                var originalImgHeight = jQuery('#ReImgContainer #Reasseteditimage').height();
                var originalImgWidth = jQuery('#ReImgContainer #Reasseteditimage').width();
                var orgWidth = $scope.orgWidth;
                var orgHeight = $scope.orgHeight;
                $('#Reasseteditimage').Jcrop({
                    boxWidth: orgWidth,
                    boxHeight: orgHeight,
                    onChange: showPreview,
                    onSelect: showPreview,
                    trueSize: [orgWidth, orgHeight],
                    setSelect: [0, 0, originalImgWidth, originalImgHeight],
                    onRelease: releaseCheck
                }, function () {
                    $('.jcrop-holder').not(':last').remove();
                    jcrop_api = this;
                    jcrop_api.setOptions({
                        aspectRatio: WidthRatio / HeightRatio
                    });
                    jcrop_api.focus();
                    var w = $scope.orgWidth;
                    var h = $scope.orgHeight;
                    jcrop_api.animateTo([0, 0, w, h]);
                });

                function showPreview(c) {
                    $scope.PreviewHeight = $('#ReImgContainer #Reasseteditimage').height();
                    $scope.PreviewWidth = $('#ReImgContainer #Reasseteditimage').width();
                    var Width = (c.w).toFixed(0);
                    var Height = (c.h).toFixed(0);
                    var topx = (c.x).toFixed(0);
                    var topy = (c.y).toFixed(0);
                    var btnx = (c.x2).toFixed(0);
                    var btny = (c.y2).toFixed(0);
                    if (Width < 0) {
                        Width = Width * (-1);
                    }
                    if (Height < 0) {
                        Height = Height * (-1);
                    }
                    $(document).trigger("OncorpImageChange", c);
                    assgincropvalues(c);
                };

                function releaseCheck() {
                    jcrop_api.setOptions({
                        allowSelect: true
                    });
                };
            };
            $scope.CropCancel = function () {
                $('.jcrop-holder').remove();
               // $('.jcrop-holder').not(':last').remove();
                $('#Reasseteditimage').removeAttr('style');
                if (jcrop_api != undefined) {
                    jcrop_api.release();
                    jcrop_api.destroy();
                    $('#Reasseteditimage').removeAttr('style');
                    $('#Reasseteditimage').attr('src', ' ');
                }
                $('#Reasseteditimage').val();
                $("#CropTopleftx").unbind('focusout');
                $("#CropToplefty").unbind('focusout');
                $("#CropBottomrightx").unbind('focusout');
                $("#CropBottomrighty").unbind('focusout');
                $("#CropResizeHeight").unbind('focusout');
                $("#CropResizeWidth").unbind('focusout');
                $("#ratiow").unbind('focusout');
                $("#ratioh").unbind('focusout');
                $("#ratioh").unbind('focusout');
                $scope.reobj.Crop.Topleftx = 0, $scope.reobj.Crop.Toplefty = 0, $scope.reobj.Crop.Bottomrightx = 0, $scope.reobj.Crop.Bottomrighty = 0;
                Getdefaultvalue();
                Cropdefault();
                $timeout(function () {
                    $modalInstance.dismiss('cancel');
                }, 100)
            }

            function gcd(a, b) {
                return (b == 0) ? a : gcd(b, a % b);
            }

            function Cropdefault() {
                wRatio = 0;
                hRatio = 0;
                $scope.reobj.Crop.Aspetratiow = 0;
                $scope.reobj.Crop.Aspetratioh = 0;
                $scope.CustomdownOptionDiv = false;
                $scope.CustomDownloadProfileOptionDiv = false
                $scope.CustomdownFormat = 0;
                $scope.CustomDownloadbtn = true;
                $scope.CustomImgDiv = true;
                $scope.CustomCancelshow = true;
                $scope.Cusratio = false;
                Cusratiocheckbox = null;
                Aspetratiocheckbox = null;
                keeporginalcheckbox = null;
                $('#CusratioIcon1').removeClass('checked');
                $scope.DamProfileType = {};
                $scope.Customratiospan = false;
                $scope.orgWidth = 0;
                $scope.orgHeight = 0;
                $scope.orgDpi = 0;
                $scope.corpWidth = 0;
                $scope.corpHeight = 0;
                $scope.ProfileWidth = 0;
                $scope.ProfileHeight = 0;
                $scope.Profileformate = "";
                $scope.ProfileDpi = 0;
                $scope.CropFormat = 0;
                $scope.ProfileType = 0;
                $scope.profileid = 0;
                $scope.CusDpi = 0;
                $scope.CusFileFormat = "-";
                $scope.CusWidth = 0;
                $scope.CusHeight = 0;
                $scope.CropApplybtn = false;
                $scope.CustomOptionDiv = false;
                $scope.ProfileOptionDiv = false;
                $scope.cropImgDiv = false;
                $scope.CustomImgDiv = false;
                $scope.validcropinput = true;
                $scope.mimes = {};
                $scope.orgSize = 0;
                $('.jcrop-holder').remove();
                //$('.jcrop-holder').not(':last').remove();
                $('#Reasseteditimage').removeAttr('style');
                if (jcrop_api != undefined) {
                    jcrop_api.release();
                    jcrop_api.destroy();
                    $('#Reasseteditimage').removeAttr('style');
                }
            }
            $(document).on("OncorpImageChange", function (event, c) {
                assgincropvalues(c);
            });

            function GetAssetAttribuuteDetails(assetid) {
                $scope.assettypeidfrbtn = 0;
                Cropdefault();
                $scope.RoleIdsaved = [];
                $scope.editable = '';
                if (assetid !== undefined) {
                    AssetReformatService.GetAttributeDetails(assetid).then(function (attrList) {
                        $scope.OlAssetID = assetid;
                        $scope.oldAssetTypeID = attrList.Response.AssetTypeid;
                        $scope.assettypeidfrbtn = attrList.Response.AssetTypeid;
                        $scope.AssetPublish = attrList.Response.IsPublish;
                        var ID = assetid;
                        $scope.StopeUpdateStatusonPageLoad = false;
                        $scope.detailsLoader = false;
                        $scope.detailsData = true;
                        $scope.Assetdyn_Cont = '';
                        $scope.attributedata = attrList.Response.AttributeData;
                        $scope.AssetPreviewObj.AssetId = assetid;
                        $scope.AssetPreviewObj.AssetCeatedOn = attrList.Response.StrCreatedDate != null ? dateFormat(attrList.Response.StrCreatedDate, $scope.DefaultSettings.DateFormat) : "";
                        $scope.AssetPreviewObj.AssetUpdatedOn = attrList.Response.StrUpdatedOn != "0001-01-01" ? dateFormat(attrList.Response.StrUpdatedOn, $scope.DefaultSettings.DateFormat) : "-";
                        $scope.AssetlastUpdatedOn = $scope.AssetPreviewObj.AssetUpdatedOn;
                        $scope.AssetPreviewObj.PublisherID = attrList.Response.PublisherID;
                        $scope.AssetPreviewObj.PublishedOn = attrList.Response.StrPublishedOn != "" ? dateFormat(attrList.Response.StrPublishedOn, $scope.DefaultSettings.DateFormat) : "-";
                        $scope.ddlGlobalRole = [];
                        $scope.assetFolderid = attrList.Response.FolderID;
                        $scope.assetEntityId = attrList.Response.EntityID;
                        $scope.ProductionEntityID = attrList.Response.ProductionEntityID;
                        $scope.AssetActiveFileID = attrList.Response.ActiveFileID;
                        try {
                            $scope.assetEditLinkDetails.folderName = attrList.Response.folderName;
                            $scope.assetEditLinkDetails.publishedon = attrList.Response.PublishLinkDetails[0].publishedON;
                            $scope.assetEditLinkDetails.publishedby = attrList.Response.PublishLinkDetails[0].publishedby;
                            $scope.assetEditLinkDetails.linkdetails = attrList.Response.PublishLinkDetails[0].LinkUserDetail;
                        } catch (e) { }
                        $scope.assetfiles = attrList.Response.Files;
                        $scope.ActiveVersion = $.grep(attrList.Response.Files, function (e) {
                            return e.ID == attrList.Response.ActiveFileID;
                        })[0];
                        if ($scope.ActiveVersion != '' && $scope.ActiveVersion != undefined) {
                            $scope.AssetPreviewObj.VersionCeatedOn = $scope.ActiveVersion.StrCreatedDate != "" ? dateFormat($scope.ActiveVersion.StrCreatedDate, $scope.DefaultSettings.DateFormat) : "-";
                            $scope.EditversionID = $scope.ActiveVersion.ID;
                            $scope.Editverionfileguid = $scope.ActiveVersion.Fileguid;
                            $scope.ProcessType = $scope.ActiveVersion.ProcessType;
                            $scope.videofileguid = imageBaseUrlcloud + "DAMFiles/Preview/" + $scope.ActiveVersion.Fileguid + '.mp4';
                            if ($scope.ProcessType == 3) {
                                if ($scope.ActiveVersion.Status == 2) {
                                    $('#videoHolder').html('');
                                    $scope.posterpath = imageBaseUrlcloud + "DAMFiles/Preview/Enlarge_" + $scope.ActiveVersion.Fileguid + '.jpg?' + generateUniqueTracker() + '';
                                    timerObj.activeversioninfo = $timeout(function () {
                                        var HTML = '<video id="video1_' + $scope.ActiveVersion.Fileguid + '" class="video-js" controls preload="auto"  poster=\"' + imageBaseUrlcloud + 'DAMFiles/Preview/Enlarge_' + $scope.ActiveVersion.Fileguid + '.jpg\"><source src=\"' + $scope.videofileguid.toString() + '\" type="video/mp4"></video>'
                                        $('#videoHolder').html(HTML);
                                        vjs('video1_' + $scope.ActiveVersion.Fileguid + '');
                                    }, 100);
                                } else if ($scope.ActiveVersion.Status == 1 || $scope.ActiveVersion.Status == 0) {
                                    $('#videoHolder').html('');
                                    $scope.posterpath = "assets/img/loading.gif";
                                    timerObj.previewTimer = $timeout(function () {
                                        CheckPreviewGeneratorForVersion()
                                    }, 5000);
                                    var HTML = '<div class="loadicon-large"><span><img src="' + $scope.posterpath + '" class="noBorder" />Loading...</span></div>'
                                    $('#videoHolder').html(HTML);
                                } else if ($scope.ActiveVersion.Status == 3) {
                                    $scope.posterpath = imageBaseUrlcloud + "DAMFiles/StaticPreview_small/" + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + ".png";
                                    var HTML = '<img src="' + $scope.posterpath + '" />'
                                    $('#videoHolder').html(HTML);
                                }
                            }
                            $scope.Category = attrList.Response.Category;
                            if (attrList.Response.Category == 0) {
                                $scope.IsAttachment = true;
                                $scope.IsImgInfo = true;
                                $scope.IsSize = true;
                                $scope.IsResolution = true;
                                $scope.IsDPI = true;
                            } else $scope.IsAttachment = false;
                            $scope.IsImgInfo = false;
                            $scope.ActiveVersionNo = $scope.ActiveVersion.VersionNo;
                            $scope.filepathfrview = imageBaseUrlcloud + "DAMFiles/Original/" + $scope.ActiveVersion.Fileguid + $scope.ActiveVersion.Extension;
                            $scope.filedownloadpath = "DAMDownload.aspx?FileID=" + $scope.ActiveVersion.Fileguid + "&amp;FileFriendlyName=" + $scope.ActiveVersion.Name + "&amp;Ext=" + $scope.ActiveVersion.Extension;
                            if ($scope.ProcessType == 1 || $scope.ProcessType == 2 || $scope.ProcessType == 4) {
                                if ($scope.ActiveVersion.Status == 2) {
                                    $scope.fileguid = imageBaseUrlcloud + "DAMFiles/Preview/Enlarge_" + $scope.ActiveVersion.Fileguid + '.jpg?' + generateUniqueTracker() + '';
                                    $scope.onerrorfileguid = imageBaseUrlcloud + "DAMFiles/StaticPreview_small/" + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + ".png";
                                    $scope.cropopen();
                                } else if ($scope.ActiveVersion.Status == 1 || $scope.ActiveVersion.Status == 0) {
                                    timerObj.previewTimer = $timeout(function () {
                                        CheckPreviewGeneratorForVersion()
                                    }, 5000);
                                    $scope.fileguid = "assets/img/loading.gif";
                                }
                                if ($scope.ActiveVersion.Status == 3) {
                                    $scope.fileguid = imageBaseUrlcloud + "DAMFiles/StaticPreview_small/" + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + ".png";
                                }
                            } else {
                                $scope.fileguid = imageBaseUrlcloud + "DAMFiles/StaticPreview_small/" + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + ".png";
                            }
                        } else {
                            if (attrList.Response.Category == 1) {
                                $scope.fileguid = imageBaseUrlcloud + "DAMFiles/StaticPreview_small/BLANK.png";
                                $scope.Category = attrList.Response.Category;
                            } else if (attrList.Response.Category == 2) {
                                $scope.fileguid = imageBaseUrlcloud + "DAMFiles/StaticPreview_small/LINK.png";
                                $scope.Category = attrList.Response.Category;
                            }
                            $scope.filedownloadpath = '';
                        }
                    });
                }
            }
            $scope.Rotatevalue = function (value) {
                $scope.reobj.Rotate.Rotate = value;
            }

            function pixels_dpi_to_cm(pixels, dpi) {
                var output;
                output = pixels / dpi;
                output = output * 2.54;
                return output;
            }

            function pixels_dpi_to_mm(pixels, dpi) {
                var output;
                output = pixels / dpi;
                output = output * 25.4;
                return output;
            }

            function pixels_dpi_to_inch(pixels, dpi) {
                var output;
                output = pixels / dpi;
                return output;
            }

            function ChangeUnit(intdpi, Exitingvalue, Convertype) {
                var output;
                if (Convertype == "inch") {
                    output = Exitingvalue / intdpi;
                } else if (Convertype == "mm") {
                    output = Exitingvalue / intdpi;
                    output = output * 25.4;
                }
                return output;
            }

            function ConvertToPixels(intdpi, Exitingvalue, ExistingType) {
                var output;
                if (ExistingType == "inch") {
                    output = intdpi * Exitingvalue;
                } else if (ExistingType == "mm") {
                    output = (Exitingvalue * intdpi) / 25.4
                }
                return output;
            }

            function GetAssetActiveFileinfo(assetid) {
                timerObj.previewTimer = $timeout(function () {
                    AssetReformatService.GetAssetActiveFileinfo(assetid).then(function (result) {
                        if (result.Response != "") {
                            $scope.IsImgInfo = true;
                            var generatedids = result.Response;
                            var Activefileexn = result.Response[0].Extension;
                            $scope.Activefileformat = Activefileexn.substring(1, Activefileexn.length);
                            var addinfo = (result.Response[0].Additionalinfo != "") ? JSON.parse(result.Response[0].Additionalinfo) : "";
                            if (result.Response[0].Size != undefined) {
                                $scope.orgSize = parseSize(result.Response[0].Size);
                                $scope.IsSize = true;
                            }
                            if (addinfo != undefined && addinfo != "") {
                                $scope.orgWidth = addinfo.Width;
                                $scope.orgHeight = addinfo.Height;
                                $scope.orgDpi = addinfo.Dpi;
                                timerObj.unitschange = $timeout(function () {
                                    $scope.Unitschange(1);
                                }, 10);
                                $scope.IsDimensioninfo = true;
                            } else {
                                $scope.orgWidth = 0;
                                $scope.orgHeight = 0;
                                $scope.orgDpi = 0;
                            }
                        } else {
                            $scope.IsImgInfo = false;
                        }
                    });
                }, 50);
            }

            function parseSize(bytes) {
                var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"];
                var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
                if (bytes == 0) return '0 Byte';
                var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
                return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
            }
            $scope.selectCusratio = function (event) {
                Cusratiocheckbox = event.target;
                if (Cusratiocheckbox.checked) {
                    $scope.Cusratio = true
                    $scope.Customratiospan = true;
                    var abGcd = gcd($scope.orgWidth, $scope.orgHeight);
                    wRatio = $scope.orgWidth / abGcd;
                    hRatio = $scope.orgHeight / abGcd;
                    $scope.reobj.Crop.Aspetratiow = wRatio;
                    $scope.reobj.Crop.Aspetratioh = hRatio;
                    initJcrop($scope.reobj.Crop.Aspetratiow, $scope.reobj.Crop.Aspetratioh);
                } else {
                    $scope.Cusratio = false
                    $scope.Customratiospan = false;
                    $scope.reobj.Crop.Aspetratiow = 0;
                    $scope.reobj.Crop.Aspetratioh = 0;
                    initJcrop(0, 0);
                    if (jcrop_api != undefined) {
                        jcrop_api.release();
                        jcrop_api.setSelect([0, 0, $scope.orgWidth, $scope.orgHeight]);
                    }
                }
            }
            $scope.selectkeeporiginal = function (event) {
                keeporginalcheckbox = event.target;
                if (keeporginalcheckbox.checked) {
                    $scope.Cusratio = true;
                    $('#CusratioIcon1').addClass('disabled');
                    $scope.Customratiospan = true;
                    var abGcd = gcd($scope.orgWidth, $scope.orgHeight);
                    wRatio = $scope.orgWidth / abGcd;
                    hRatio = $scope.orgHeight / abGcd;
                    $scope.reobj.Crop.Aspetratiow = wRatio;
                    $scope.reobj.Crop.Aspetratioh = hRatio;
                    $scope.reobj.Crop.keeporiginal = true;
                    initJcrop($scope.reobj.Crop.Aspetratiow, $scope.reobj.Crop.Aspetratioh);
                } else {
                    $scope.reobj.Crop.Aspetratiow = 0;
                    $scope.reobj.Crop.Aspetratioh = 0;
                    $scope.Cusratio = false;
                    $scope.reobj.Crop.keeporiginal = false;
                    $scope.Customratiospan = false;
                    initJcrop(0, 0);
                    if (jcrop_api != undefined) {
                        jcrop_api.release();
                        jcrop_api.setSelect([0, 0, $scope.orgWidth, $scope.orgHeight]);
                    }
                }
            }
            $scope.selectpreserveAspetratio = function (event) {
                Aspetratiocheckbox = event.target;
                if (Aspetratiocheckbox.checked) {
                    $scope.reobj.CropResize.preserveAspetratio = true;
                    var abGcd = gcd($scope.orgWidth, $scope.orgHeight);
                    wRatio = $scope.orgWidth / abGcd;
                    hRatio = $scope.orgHeight / abGcd;
                    $scope.orgwRatio = wRatio;
                    $scope.orghRatio = hRatio;
                } else {
                    $scope.reobj.CropResize.preserveAspetratio = false;
                }
            }
            $("#CropResizeHeight").focusout(function () {
                var re;
                if ($scope.Unitsshortname == 'px') re = /^[0-9]+$/;
                else re = /^[0-9.]+$/;
                Resizeperset();
                if (re.test($scope.reobj.CropResize.Height)) {
                    if ($scope.reobj.CropResize.preserveAspetratio) {
                        var height = $scope.reobj.CropResize.Height;
                        var radioheightvalue = Math.round((height / $scope.orghRatio)).toFixed(0);
                        $scope.reobj.CropResize.Width = (radioheightvalue * $scope.orgwRatio).toFixed(0);
                        $scope.reobj.CropResize.corpHeight = $scope.reobj.CropResize.Height;
                        $scope.reobj.CropResize.corpWidth = $scope.reobj.CropResize.Width;
                    } else {
                        $scope.reobj.CropResize.corpHeight = $scope.reobj.CropResize.Height;
                        $scope.reobj.CropResize.corpWidth = $scope.reobj.CropResize.Width;
                    }
                } else { }
            });
            $("#CropResizeWidth").focusout(function () {
                var re;
                if ($scope.Unitsshortname == 'px') re = /^[0-9]+$/;
                else re = /^[0-9.]+$/;
                Resizeperset();
                if (re.test($scope.reobj.CropResize.Width)) {
                    if ($scope.reobj.CropResize.preserveAspetratio) {
                        var Width = $scope.reobj.CropResize.Width;
                        var radioWidthvalue = Math.round((Width / $scope.orgwRatio)).toFixed(0);
                        $scope.reobj.CropResize.Height = (radioWidthvalue * $scope.orghRatio).toFixed(0);
                        $scope.reobj.CropResize.corpHeight = $scope.reobj.CropResize.Height;
                        $scope.reobj.CropResize.corpWidth = $scope.reobj.CropResize.Width;
                    } else {
                        $scope.reobj.CropResize.corpHeight = $scope.reobj.CropResize.Height;
                        $scope.reobj.CropResize.corpWidth = $scope.reobj.CropResize.Width;
                    }
                } else { }
            });

            function Resizeperset() {
                if ($scope.orgWidth > $scope.orgHeight) {
                    var height;
                    if ($scope.Unitsshortname == 'px') {
                        height = $scope.reobj.CropResize.Height;
                    } else {
                        height = ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.CropResize.Height, $scope.Unitsshortname).toFixed(0);
                    }
                    $scope.reobj.CropResize.Resizeper = Math.round(height / $scope.orgHeight * 100).toFixed(0);
                } else if ($scope.orgWidth < $scope.orgHeight) {
                    var Width;
                    if ($scope.Unitsshortname == 'px') {
                        Width = $scope.reobj.CropResize.corpWidth;
                    } else {
                        Width = ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.CropResize.corpWidth, $scope.Unitsshortname).toFixed(0);
                    }
                    $scope.reobj.CropResize.Resizeper = Math.round(Width / $scope.orgWidth * 100).toFixed(0);
                }
            }

            function Resizepersetnew() {
                if ($scope.orgWidth > $scope.orgHeight) {
                    var height;
                    var Width;
                    if ($scope.Unitsshortname == 'px') {
                        height = $scope.orgHeight;
                        Width = $scope.orgWidth;
                        $scope.reobj.CropResize.Height = Math.round(($scope.reobj.CropResize.Resizeper / 100) * height).toFixed(0);
                        $scope.reobj.CropResize.Width = Math.round(($scope.reobj.CropResize.Resizeper / 100) * Width).toFixed(0);
                        $scope.reobj.CropResize.corpWidth = $scope.reobj.CropResize.Width;
                        $scope.reobj.CropResize.corpHeight = $scope.reobj.CropResize.Height;
                    } else {
                        height = $scope.convertHeight;
                        Width = $scope.convertWidth;
                        $scope.reobj.CropResize.Height = (($scope.reobj.CropResize.Resizeper / 100) * height).toFixed(1);
                        $scope.reobj.CropResize.Width = (($scope.reobj.CropResize.Resizeper / 100) * Width).toFixed(1);
                        $scope.reobj.CropResize.corpWidth = $scope.reobj.CropResize.Width;
                        $scope.reobj.CropResize.corpHeight = $scope.reobj.CropResize.Height;
                    }
                } else if ($scope.orgWidth < $scope.orgHeight) {
                    var Width;
                    if ($scope.Unitsshortname == 'px') {
                        height = $scope.orgHeight;
                        Width = $scope.orgWidth;
                        $scope.reobj.CropResize.Height = Math.round(($scope.reobj.CropResize.Resizeper / 100) * height).toFixed(0);
                        $scope.reobj.CropResize.Width = Math.round(($scope.reobj.CropResize.Resizeper / 100) * Width).toFixed(0);
                        $scope.reobj.CropResize.corpWidth = $scope.reobj.CropResize.Width;
                        $scope.reobj.CropResize.corpHeight = $scope.reobj.CropResize.Height;
                    } else {
                        height = $scope.convertHeight;
                        Width = $scope.convertWidth;
                        $scope.reobj.CropResize.Height = (($scope.reobj.CropResize.Resizeper / 100) * height).toFixed(1);
                        $scope.reobj.CropResize.Width = (($scope.reobj.CropResize.Resizeper / 100) * Width).toFixed(1);
                        $scope.reobj.CropResize.corpWidth = $scope.reobj.CropResize.Width;
                        $scope.reobj.CropResize.corpHeight = $scope.reobj.CropResize.Height;
                    }
                }
            }
            $("#ratiow").focusout(function () {
                var re = /^[0-9]+$/;
                if (re.test($scope.reobj.Crop.Aspetratiow)) {
                    initJcrop($scope.reobj.Crop.Aspetratiow, $scope.reobj.Crop.Aspetratioh);
                    $scope.Cusratio = true
                    $scope.Customratiospan = true;
                } else {
                    $scope.reobj.Crop.Aspetratiow = wRatio;
                    bootbox.alert($translate.instant('LanguageContents.Res_1136.Caption'));
                    return false;
                }
            });
            $("#ratioh").focusout(function () {
                var re = /^[0-9]+$/;
                if (re.test($scope.reobj.Crop.Aspetratioh)) {
                    initJcrop($scope.reobj.Crop.Aspetratiow, $scope.reobj.Crop.Aspetratioh);
                    $scope.Cusratio = true
                    $scope.Customratiospan = true;
                } else {
                    $scope.reobj.Crop.Aspetratioh = hRatio;
                    bootbox.alert($translate.instant('LanguageContents.Res_1136.Caption'));
                    return false;
                }
            });
            $("#CropTopleftx").focusout(function () {
                var re;
                var msg;
                if ($scope.Unitsshortname == 'px') {
                    re = /^[0-9]+$/;
                    msg = "plese enter the valid Input [0-9]";
                } else {
                    re = /^[0-9.]+$/;
                    msg = "plese enter the valid Input [0-9.]";
                }
                if (re.test($scope.reobj.Crop.Topleftx)) {
                    if (jcrop_api != undefined) {
                        if ($scope.Unitsshortname == 'px') jcrop_api.setSelect([$scope.reobj.Crop.Topleftx, $scope.reobj.Crop.Toplefty, $scope.reobj.Crop.Bottomrightx, $scope.reobj.Crop.Bottomrighty]);
                        else jcrop_api.setSelect([ConvertToPixels($scope.orgDpi, $scope.reobj.Crop.Topleftx, $scope.Unitsshortname).toFixed(0), ConvertToPixels($scope.orgDpi, $scope.reobj.Crop.Toplefty, $scope.Unitsshortname).toFixed(0), ConvertToPixels($scope.orgDpi, $scope.reobj.Crop.Bottomrightx, $scope.Unitsshortname).toFixed(0), ConvertToPixels($scope.orgDpi, $scope.reobj.Crop.Bottomrighty, $scope.Unitsshortname).toFixed(0)]);
                    }
                } else {
                    $scope.reobj.Crop.Topleftx = 0;
                    bootbox.alert(msg);
                    return false;
                }
            });
            $("#CropToplefty").focusout(function () {
                var re;
                var msg;
                if ($scope.Unitsshortname == 'px') {
                    re = /^[0-9]+$/;
                    msg = "plese enter the valid Input [0-9]";
                } else {
                    re = /^[0-9.]+$/;
                    msg = "plese enter the valid Input [0-9]";
                }
                if (re.test($scope.reobj.Crop.Topleftx)) {
                    if (jcrop_api != undefined) {
                        if ($scope.Unitsshortname == 'px') jcrop_api.setSelect([$scope.reobj.Crop.Topleftx, $scope.reobj.Crop.Toplefty, $scope.reobj.Crop.Bottomrightx, $scope.reobj.Crop.Bottomrighty]);
                        else jcrop_api.setSelect([ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.Crop.Topleftx, $scope.Unitsshortname).toFixed(0), ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.Crop.Toplefty, $scope.Unitsshortname).toFixed(0), ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.Crop.Bottomrightx, $scope.Unitsshortname).toFixed(0), ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.Crop.Bottomrighty, $scope.Unitsshortname).toFixed(0)]);
                    }
                } else {
                    $scope.reobj.Crop.Toplefty = 0;
                    bootbox.alert(msg);
                    return false;
                }
            });
            $("#CropBottomrightx").focusout(function () {
                var re;
                var msg;
                if ($scope.Unitsshortname == 'px') {
                    re = /^[0-9]+$/;
                    msg = "plese enter the valid Input [0-9]";
                } else {
                    re = /^[0-9.]+$/;
                    msg = "plese enter the valid Input [0-9.]";
                }
                if (re.test($scope.reobj.Crop.Topleftx)) {
                    if (jcrop_api != undefined) {
                        if ($scope.Unitsshortname == 'px') jcrop_api.setSelect([$scope.reobj.Crop.Topleftx, $scope.reobj.Crop.Toplefty, $scope.reobj.Crop.Bottomrightx, $scope.reobj.Crop.Bottomrighty]);
                        else jcrop_api.setSelect([ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.Crop.Topleftx, $scope.Unitsshortname).toFixed(0), ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.Crop.Toplefty, $scope.Unitsshortname).toFixed(0), ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.Crop.Bottomrightx, $scope.Unitsshortname).toFixed(0), ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.Crop.Bottomrighty, $scope.Unitsshortname).toFixed(0)]);
                    }
                } else {
                    var originalImgHeight = jQuery('#ReImgContainer #Reasseteditimage').height();
                    var originalImgWidth = jQuery('#ReImgContainer #Reasseteditimage').width();
                    $scope.reobj.Crop.Bottomrightx = originalImgWidth;
                    bootbox.alert(msg);
                    return false;
                }
            });
            $("#CropBottomrighty").focusout(function () {
                var re;
                var msg;
                if ($scope.Unitsshortname == 'px') {
                    re = /^[0-9]+$/;
                    msg = "plese enter the valid Input [0-9]";
                } else {
                    re = /^[0-9.]+$/;
                    msg = "plese enter the valid Input [0-9.]";
                }
                if (re.test($scope.reobj.Crop.Topleftx)) {
                    if (jcrop_api != undefined) {
                        if ($scope.Unitsshortname == 'px') jcrop_api.setSelect([$scope.reobj.Crop.Topleftx, $scope.reobj.Crop.Toplefty, $scope.reobj.Crop.Bottomrightx, $scope.reobj.Crop.Bottomrighty]);
                        else jcrop_api.setSelect([ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.Crop.Topleftx, $scope.Unitsshortname).toFixed(0), ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.Crop.Toplefty, $scope.Unitsshortname).toFixed(0), ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.Crop.Bottomrightx, $scope.Unitsshortname).toFixed(0), ConvertToPixels($scope.reobj.Formate.Dpi, $scope.reobj.Crop.Bottomrighty, $scope.Unitsshortname).toFixed(0)]);
                    }
                } else {
                    var originalImgHeight = jQuery('#ReImgContainer #Reasseteditimage').height();
                    var originalImgWidth = jQuery('#ReImgContainer #Reasseteditimage').width();
                    $scope.reobj.Crop.Bottomrighty = originalImgHeight;
                    bootbox.alert(msg);
                    return false;
                }
            });
            $("#CropResizeper").focusout(function () {
                var re;
                var msg;
                re = /^[0-9]+$/;
                msg = "plese enter the valid Input [0-9]";
                if (re.test($scope.reobj.CropResize.Resizeper)) {
                    Resizepersetnew();
                } else {
                    bootbox.alert(msg);
                    return false;
                }
            });

            function assgincropvalues(c) {
                $scope.corpWidth = 0;
                $scope.corpHeight = 0;
                $scope.TopXValue = Math.round(c.x).toFixed(0);
                $scope.TopYValue = Math.round(c.y).toFixed(0);
                $scope.ButtonXValue = Math.round(c.x2).toFixed(0);
                $scope.ButtonYValue = Math.round(c.y2).toFixed(0);
                $scope.corpWidth = Math.round(c.w).toFixed(0);
                $scope.corpHeight = Math.round(c.h).toFixed(0);
                if ($scope.Unitsshortname == 'px') {
                    $scope.reobj.Crop.Topleftx = $scope.TopXValue;
                    $scope.reobj.Crop.Toplefty = $scope.TopYValue;
                    $scope.reobj.Crop.Bottomrightx = $scope.ButtonXValue;
                    $scope.reobj.Crop.Bottomrighty = $scope.ButtonYValue;
                    $scope.reobj.Crop.corpWidth = $scope.corpWidth;
                    $scope.reobj.Crop.corpHeight = $scope.corpHeight;
                    $scope.reobj.CropResize.corpWidth = $scope.corpWidth;
                    $scope.reobj.CropResize.corpHeight = $scope.corpHeight;
                    $scope.reobj.CropResize.Width = $scope.corpWidth;
                    $scope.reobj.CropResize.Height = $scope.corpHeight;
                } else {
                    $scope.reobj.Crop.Topleftx = ChangeUnit($scope.reobj.Formate.Dpi, $scope.TopXValue, $scope.Unitsshortname).toFixed(1);
                    $scope.reobj.Crop.Toplefty = ChangeUnit($scope.reobj.Formate.Dpi, $scope.TopYValue, $scope.Unitsshortname).toFixed(1);
                    $scope.reobj.Crop.Bottomrightx = ChangeUnit($scope.reobj.Formate.Dpi, $scope.ButtonXValue, $scope.Unitsshortname).toFixed(1);
                    $scope.reobj.Crop.Bottomrighty = ChangeUnit($scope.reobj.Formate.Dpi, $scope.ButtonYValue, $scope.Unitsshortname).toFixed(1);
                    $scope.reobj.Crop.corpWidth = ChangeUnit($scope.reobj.Formate.Dpi, $scope.corpWidth, $scope.Unitsshortname).toFixed(1);
                    $scope.reobj.Crop.corpHeight = ChangeUnit($scope.reobj.Formate.Dpi, $scope.corpHeight, $scope.Unitsshortname).toFixed(1);
                    $scope.reobj.CropResize.corpWidth = ChangeUnit($scope.reobj.Formate.Dpi, $scope.corpWidth, $scope.Unitsshortname).toFixed(1);
                    $scope.reobj.CropResize.corpHeight = ChangeUnit($scope.reobj.Formate.Dpi, $scope.corpHeight, $scope.Unitsshortname).toFixed(1);
                    $scope.reobj.CropResize.Width = ChangeUnit($scope.reobj.Formate.Dpi, $scope.corpWidth, $scope.Unitsshortname).toFixed(1);
                    $scope.reobj.CropResize.Height = ChangeUnit($scope.reobj.Formate.Dpi, $scope.corpHeight, $scope.Unitsshortname).toFixed(1);
                }
                Resizeperset();
            }
            $scope.SaveReformatRescaleval = function () {
                if ($scope.profUnits == 2 || $scope.profUnits == 3 || $scope.profUnits == 1) {
                    if ($scope.reobj.Formate.Dpi == 0) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1118.Caption'));
                        return false;
                    }
                }
                if ($('#btnSaveRescale').hasClass('disabled')) {
                    return;
                }
                $('#btnSaveRescale').addClass('disabled');
                $scope.CropprocessFlag = true;
                var SaveRescale = {};
                SaveRescale.AssetId = $scope.AssetEditID;
                SaveRescale.ProfileID = $scope.profprofile;
                SaveRescale.Units = $scope.profUnits;
                SaveRescale.Folderid = $scope.AssetPreviewObj.folderid;
                SaveRescale.srcInputObj = $scope.reobj;
                $scope.SaveReformatRescale = [];
                $scope.SaveReformatRescale.push({
                    "AssetId": $scope.AssetEditID,
                    "ProfileID": SaveRescale.ProfileID,
                    "Units": $scope.profUnits,
                    "Folderid": $scope.AssetPreviewObj.folderid,
                    "srcInputObj": {
                        "ActionSrcValue": SaveRescale
                    }
                });
                var SaveReformatRescale = {
                    SaveReformatRescale: $scope.SaveReformatRescale[0]
                };
                AssetReformatService.ReformatRescale(SaveReformatRescale).then(function (SaveRescaleResult) {
                    if (SaveRescaleResult.StatusCode == 405) {
                        $('#btnSaveRescale').removeClass('disabled');
                        NotifyError($translate.instant('LanguageContents.Res_1134.Caption'));
                        $scope.CropprocessFlag = false;
                        $scope.ReformateAssetObj.status = false;
                    } else {
                        if (SaveRescaleResult.Response != null) {
                            if (SaveRescaleResult.Response.m_Item1 == false) {
                                $('#btnSaveRescale').removeClass('disabled');
                                if (SaveRescaleResult.Response.m_Item2 != null) {
                                    NotifyError(SaveRescaleResult.Response.m_Item2);
                                } else NotifyError($translate.instant('LanguageContents.Res_1134.Caption'));
                                $scope.CropprocessFlag = false;
                                $scope.ReformateAssetObj.status = false;
                            } else {
                                $scope.CropprocessFlag = false;
                                NotifySuccess($translate.instant('LanguageContents.Res_1135.Caption'));
                                $('#btnSaveRescale').removeClass('disabled');
                                $scope.ReformateAssetObj.Fileguid = SaveRescaleResult.Response.m_Item2;
                                $scope.ReformateAssetObj.Extension = SaveRescaleResult.Response.m_Item3;
                                $scope.ReformateAssetObj.FileName = SaveRescaleResult.Response.m_Item4;
                                $scope.ReformateAssetObj.AssetId = SaveRescaleResult.Response.m_Item5
                                $scope.ReformateAssetObj.status = true;
                                Cropdefault();
                                $scope.fileguid = imageBaseUrlcloud + "DAMFiles/Preview/Enlarge_" + $scope.ReformateAssetObj.Fileguid + '.jpg?' + generateUniqueTracker() + '';
                                $scope.Reformatestatus = true;
                                $('.jcrop-holder').remove();
                               // $('.jcrop-holder').not(':last').remove();
                                $('#Reasseteditimage').removeAttr('style');
                                if (jcrop_api != undefined) {
                                    jcrop_api.release();
                                    jcrop_api.destroy();
                                    $('#Reasseteditimage').removeAttr('style');
                                }
                                $("#wizardStep1").addClass("displayNone");
                                $('#btnSaveRescale').addClass("displayNone");
                                $("#wizardStep2").removeClass("displayNone");
                            }
                        } else {
                            $('#btnSaveRescale').removeClass('disabled');
                            NotifyError($translate.instant('LanguageContents.Res_1134.Caption'));
                            $scope.CropprocessFlag = false;
                            $scope.ReformateAssetObj.status = false;
                        }
                    }
                });
            }
            $scope.Proceed = function () {
                if ($('#RadioDownload').is(':checked')) {
                    $scope.Download();
                } else if ($('#RadioAddToLightBox').is(':checked')) {
                    $scope.Addtolightbox();
                } else if ($('#RadioNewAsset').is(':checked')) {
                    $scope.SaveReformatAsset();
                } else if ($('#RadioSendMail').is(':checked')) {
                    $scope.openmailpopup();
                } else if ($('#RadioRevert').is(':checked')) {
                    $('.jcrop-holder').remove();
                    //$('.jcrop-holder').not(':last').remove();
                    $('#Reasseteditimage').removeAttr('style');
                    $('#Reasseteditimage').attr('src', ' ');
                    if (jcrop_api != undefined) {
                        jcrop_api.release();
                        jcrop_api.destroy();
                        $('#Reasseteditimage').removeAttr('style');
                        $('#Reasseteditimage').attr('src', ' ');
                    }
                    timerObj.startreformatcallback = $timeout(function () {
                        openAssetReformatPopup($scope.AssetPreviewObj.Fileguid, $scope.AssetPreviewObj.AssetId, $scope.AssetPreviewObj.folderid, $scope.AssetPreviewObj.EntityID);
                    }, 100);
                } else if ($('#RadioDownloadMetadata').is(':checked')) {
                    $scope.DownloadWithMetadata(false);
                } else if ($('#RadioDownloadPackage').is(':checked')) {
                    $scope.DownloadWithMetadata(true);
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'));
                }
            }
            $scope.Download = function () {
                $scope.CropprocessFlag = true;
                var filename = $scope.ReformateAssetObj.FileName;
                var actualfilename = "";
                for (var i = 0; i < filename.length; i++) {
                    if ((filename[i] >= '0' && filename[i] <= '9') || (filename[i] >= 'A' && filename[i] <= 'z' || (filename[i] == '.' || filename[i] == '_') || (filename[i] == ' '))) {
                        actualfilename = actualfilename + (filename[i]);
                    }
                }
                blockUIForDownload();
                location.href = 'DAMDownload.aspx?FileID=' + $scope.ReformateAssetObj.Fileguid + '&FileFriendlyName=' + actualfilename + '&Ext=' + $scope.ReformateAssetObj.Extension + '&token=' + $('#reformatdownload_token_value_id').val();
                $('#RadioDownload')[0].checked = false;
                $('#RadioRevert')[0].checked = true;
                $scope.CropprocessFlag = true;
            }
            $scope.Addtolightbox = function () {
                $scope.damlightbox.lightboxselection.push({
                    "AssetId": $scope.ReformateAssetObj.AssetId
                });
                NotifySuccess($translate.instant('LanguageContents.Res_4066.Caption'));
                $('#RadioAddToLightBox')[0].checked = false;
                $('#RadioRevert')[0].checked = true;
                $timeout(function () { $scope.closereformatpopup() }, 100);
            }
            $scope.openmailpopup = function () {
                $scope.sendReformatMailSubject = "";
                $scope.AssetReformat.TomailAddress = [];
                $('#sendNewCropAssetPopup').modal("show");
                $scope.includemdata = false;
            }
            $scope.AssetReformat.TomailAddress = [];
            $scope.includemdata = false;
            $scope.selectmetadata = function (e) {
                var chk = e.target;
                if (chk.checked) $scope.includemdata = true;
                else $scope.includemdata = false;
            }
            $scope.SendAssetWitMwtadata = function () {
                $timeout(function () {
                $scope.MailprocessMailFlag = true;
                var assetSelection = [];
                var emailvalid = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                var assetSelection = [],
					toArray = new Array(),
					mailSubject = $scope.sendReformatMailSubject != "" ? $scope.sendReformatMailSubject : "Marcom Media Bank";
                    for (var i = 0, mail; mail = $scope.AssetReformat.TomailAddress[i++];) {
                    if (mail.text.match(emailvalid)) {
                        toArray.push(mail.text);
                    } else {
                        $scope.MailprocessMailFlag = false;
                        bootbox.alert($translate.instant('LanguageContents.Res_1137.Caption'));
                        return false;
                    }
                }
                assetSelection.push({
                    "AssetId": $scope.ReformateAssetObj.AssetId
                });
                if (assetSelection.length > 0 && toArray.length > 0) {
                    var srcObj = {};
                    srcObj.Toaddress = toArray.join(",");
                    srcObj.subject = mailSubject;
                    srcObj.AssetArr = assetSelection;
                    srcObj.metadata = $scope.includemdata;
                    timerObj.sendmailmetadata = $timeout(function () {
                        AssetReformatService.SendMailWithMetaData(srcObj).then(function (result) {
                            if (result.Response == null) {
                                $scope.sendReformatMailSubject = "";
                                $scope.MailprocessMailFlag = false;
                                    $scope.AssetReformat.TomailAddress = [];
                                NotifyError($translate.instant('LanguageContents.Res_4349.Caption'));
                            } else {
                                $scope.sendReformatMailSubject = "";
                                $scope.MailprocessMailFlag = false;
                                    $scope.AssetReformat.TomailAddress = [];
                                NotifySuccess($translate.instant('LanguageContents.Res_4472.Caption'));
                                $('#RadioSendMail')[0].checked = false;
                                $('#RadioRevert')[0].checked = true;
                                $('#sendNewCropAssetPopup').modal("hide");
                                $timeout(function () { $scope.closereformatpopup() }, 100);
                            }
                        });
                    }, 100);
                } else {
                    $scope.sendReformatMailSubject = "";
                    $scope.MailprocessMailFlag = false;
                        $scope.AssetReformat.TomailAddress = [];
                    bootbox.alert($translate.instant('LanguageContents.Res_1138.Caption'));
                }
                },100);
            }
            $scope.SaveReformatAsset = function () {
                var SaveAsset = {};
                SaveAsset.AssetId = $scope.ReformateAssetObj.AssetId;
                SaveAsset.EntityID = $scope.AssetPreviewObj.EntityID;
                SaveAsset.FolderID = $scope.AssetPreviewObj.folderid;
                $scope.SaveReformatAsset = [];
                $scope.SaveReformatAsset.push({
                    "AssetId": $scope.ReformateAssetObj.AssetId,
                    "EntityID": $scope.AssetPreviewObj.EntityID,
                    "FolderID": $scope.AssetPreviewObj.folderid
                });
                var SaveReformatAsset = {
                    SaveReformatAsset: $scope.SaveReformatAsset[0]
                };
                AssetReformatService.SaveReformatAsset(SaveReformatAsset).then(function (SaveAssetResult) {
                    if (SaveAssetResult.Response == 0) {
                        NotifyError($translate.instant('LanguageContents.Res_5827.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                        $('#RadioNewAsset')[0].checked = false;
                        $('#RadioRevert')[0].checked = true;
                        timerObj.callreformatstartup = $timeout(function () {
                            $scope.$emit('CallbackStartReformatclose', $scope.AssetPreviewObj.AssetId, $scope.AssetPreviewObj.folderid);
                        }, 100);
                        $timeout(function () { $scope.closereformatpopup() }, 100);
                    }
                });
            }
            $("input[id*='ratio']").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
            $("input[id*='Round']").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
            $("input[id*='FormateDpi']").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
            $("input[id*='Crop']").keypress(function (event) {
                return isNumber(event)
            });

            function isNumber(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode != 45 && (charCode != 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57)) return false;
                return true;
            }
            $scope.reobj.Formate.Dpi = "";
            $("#FormateDpi").focusout(function () {
                if ($scope.reobj.Formate.Dpi == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1118.Caption'));
                }
            });
            var fileDownloadCheckTimer;

            function blockUIForDownload() {
                var token = new Date().getTime();
                $('#reformatdownload_token_value_id').val(token);
                $scope.CropprocessFlag = true;
                fileDownloadCheckTimer = setInterval(function () {
                    var cookieValue = $.cookie('fileDownloadToken');
                    if (cookieValue == token) finishDownload();
                }, 1000);
            }

            function finishDownload() {
                window.clearInterval(fileDownloadCheckTimer);
                $.removeCookie('fileDownloadToken');
                $scope.CropprocessFlag = false;
                // $('#reformatAssetPopup').modal('hide');
                $timeout(function () { $scope.closereformatpopup() }, 100);

            }
            $scope.DownloadWithMetadata = function (result) {
                var arr = [];
                arr[0] = $scope.ReformateAssetObj.AssetId;
                var include = result;
                AssetReformatService.DownloadAssetWithMetadata(arr, result).then(function (result) {
                    if (result.Response == null) {
                        NotifyError($translate.instant('LanguageContents.Res_4317.Caption'));
                    } else {
                        if (arr.length == 1) {
                            if (include == false) {
                                var fileid = result.Response[1],
									extn = '.xml';
                                if (result.Response[0] == null || result.Response[0] == "") var filename = "MediabankAssets" + extn;
                                else var filename = result.Response[0];
                            } else {
                                var fileid = result.Response[1],
									extn = '.zip';
                                //var filename = fileid + extn;
                                var filename = "package";
                            }
                        } else {
                            var fileid = result.Response[1],
								extn = '.zip';
                            var filename = "package";
                        }
                        blockUIForDownload();
                        if ((parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) && include) {
                            location.href = 'DAMDownload.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#reformatdownload_token_value_id').val();
                        }
                        else
                        { location.href = 'DownloadAssetMetadata.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#reformatdownload_token_value_id').val(); }
                          
                        if (include == true) NotifySuccess($translate.instant('LanguageContents.Res_5805.Caption'));
                        else NotifySuccess($translate.instant('LanguageContents.Res_5806.Caption'));
                    }
                });
            }
        }
       
        $scope.closereformatpopup = function () {
            $modalInstance.dismiss('cancel');
        }
        $scope.$on("$destroy", function () {
            $timeout.cancel(timerObj);
        });
    }
    app.controller("mui.muiassetreformatCtrl", ['$window', '$location', '$timeout', '$scope', '$cookies', '$resource', 'AssetReformatService', '$sce', '$compile', '$translate', '$modalInstance', 'params', muiassetreformatCtrl]);
})(angular, app);