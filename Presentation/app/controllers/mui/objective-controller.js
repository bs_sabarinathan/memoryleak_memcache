﻿(function (ng, app) {
    "use strict"; function muiplanningtoolobjectiveCtrl($scope) {
        $scope.$on("$destroy", function () { RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.objectiveCtrl']")); });
    }
    app.controller("mui.planningtool.objectiveCtrl", ['$scope', muiplanningtoolobjectiveCtrl]);
})(angular, app);