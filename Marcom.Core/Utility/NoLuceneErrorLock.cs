﻿using Lucene.Net.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Utility
{

    /// <summary> Use this {@link LockFactory} to disable locking entirely.
    /// This LockFactory is used when you call {@link FSDirectory#setDisableLocks}.
    /// Only one instance of this lock is created.  You should call {@link
    /// #GetNoLockFactory()} to get the instance.
    /// 
    /// </summary>
    /// <seealso cref="LockFactory">
    /// </seealso>

    public class NoLuceneErrorLock : LockFactory
    {

        // Single instance returned whenever makeLock is called.
        private static NoLock singletonLock = new NoLock();
        private static NoLockFactory singleton = new NoLockFactory();

        public static NoLockFactory GetNoLockFactory()
        {
            return singleton;
        }

        public override Lock MakeLock(System.String lockName)
        {
            return singletonLock;
        }

        public override void ClearLock(System.String lockName)
        {
        }

    }


    class NoLock : Lock
    {
        public override bool Obtain()
        {
            return true;
        }

        public override void Release()
        {
        }

        public override bool IsLocked()
        {
            return false;
        }

        public override System.String ToString()
        {
            return "NoLock";
        }
    }
}
