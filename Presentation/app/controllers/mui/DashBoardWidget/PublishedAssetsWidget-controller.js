﻿(function (ng, app) {
    "use strict";

    function muiDashBoardWidgetPublishedAssetsWidgetCtrl($scope, $timeout, $http, $compile, $resource, $cookies, $location, $window, PublishedassetswidgetService) {
        $scope.PublishedEntityFeedsdivID = "PublishedEntityFeedsdiv_" + $scope.WizardID;
        $scope.PublishedFeedContentdivID = "PublishedFeedContentdiv_" + $scope.WizardID;
        $scope.WizardFeedsgroupid = '-1';
        $timeout(function () { LoadNewsFeedPublished(); }, 100);
        if (cloudsetup.storageType == undefined) {
            var imageBaseUrlcloud = TenantFilePath;
        }
        else if (cloudsetup != null) {
            var imageBaseUrlcloud = (cloudsetup.storageType == clientFileStoragetype.local) ? TenantFilePath : (cloudsetup.Uploaderurl + "/" + cloudsetup.BucketName + "/" + TenantFilePath).replace(/\\/g, "\/");
        }
        function LoadNewsFeedPublished() {
            try {
                $('#' + $scope.PublishedEntityFeedsdivID + '').html('');
                var feeddivHtml = '';
                $scope.PublishedEntityFeedsResultset = [];
                $scope.TopxNoOfItem = $scope.WizardNoOfItem;
                PublishedassetswidgetService.GettingAssetsFeedSelectionDashbord($scope.TopxNoOfItem, 105, 0).then(function (PublishedgetEntityNewsFeedResult) {
                    $scope.PublishedEntityFeedsResultset = PublishedgetEntityNewsFeedResult.Response;
                    for (var i = 0; i < PublishedgetEntityNewsFeedResult.Response.length; i++) {
                        feeddivHtml = feeddivHtml + '<li>';
                        feeddivHtml = feeddivHtml + '    <div class="newsFeed">';
                        feeddivHtml = feeddivHtml + '        <div class="userAvatar">';
                        feeddivHtml = feeddivHtml + '            <div class="AssetImgContainer">';
                        if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                            feeddivHtml = feeddivHtml + '               <img src="' + imageBaseUrlcloud + (PublishedgetEntityNewsFeedResult.Response[i].Assetimagepath).replace(TenantFilePath,"") + '" onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                        }
                        else {
                            feeddivHtml = feeddivHtml + '               <img src="' + PublishedgetEntityNewsFeedResult.Response[i].Assetimagepath + '" onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                        }
                        feeddivHtml = feeddivHtml + '           </div>';
                        feeddivHtml = feeddivHtml + '        </div>';
                        feeddivHtml = feeddivHtml + '        <div class="cmnt">';
                        feeddivHtml = feeddivHtml + '           <div class="cmntHeader">';
                        feeddivHtml = feeddivHtml + '               <h5>' + PublishedgetEntityNewsFeedResult.Response[i].AssetName + '</h5>';
                        feeddivHtml = feeddivHtml + '            </div>';
                        feeddivHtml = feeddivHtml + '            <div class="cmntContent">';
                        if (PublishedgetEntityNewsFeedResult.Response[i].Versionno > 1) {
                            if (PublishedgetEntityNewsFeedResult.Response[i].Isobjective == true)
                                feeddivHtml = feeddivHtml + '<p>' + PublishedgetEntityNewsFeedResult.Response[i].UserName + ' published  ' + PublishedgetEntityNewsFeedResult.Response[i].AssetName + ' Version ' + PublishedgetEntityNewsFeedResult.Response[i].Versionno + ' from <a href="#mui/objective/detail/section/' + PublishedgetEntityNewsFeedResult.Response[i].EntityID + '/overview">' + PublishedgetEntityNewsFeedResult.Response[i].Entityname + '</a></p>';
                            else if(PublishedgetEntityNewsFeedResult.Response[i].EntitytypeId == 35)
                                feeddivHtml = feeddivHtml + '<p>' + PublishedgetEntityNewsFeedResult.Response[i].UserName + ' published  ' + PublishedgetEntityNewsFeedResult.Response[i].AssetName + ' Version ' + PublishedgetEntityNewsFeedResult.Response[i].Versionno + ' from <a href="#mui/calender/detail/section/' + PublishedgetEntityNewsFeedResult.Response[i].EntityID + '/overview">' + PublishedgetEntityNewsFeedResult.Response[i].Entityname + '</a></p>';
                            else if(PublishedgetEntityNewsFeedResult.Response[i].EntitytypeId == 5)
                                feeddivHtml = feeddivHtml + '<p>' + PublishedgetEntityNewsFeedResult.Response[i].UserName + ' published  ' + PublishedgetEntityNewsFeedResult.Response[i].AssetName + ' Version ' + PublishedgetEntityNewsFeedResult.Response[i].Versionno + ' from <a href="#mui/planningtool/costcentre/detail/section/' + PublishedgetEntityNewsFeedResult.Response[i].EntityID + '/overview">' + PublishedgetEntityNewsFeedResult.Response[i].Entityname + '</a></p>';
                                            else
                            feeddivHtml = feeddivHtml + '<p>' + PublishedgetEntityNewsFeedResult.Response[i].UserName + ' published  ' + PublishedgetEntityNewsFeedResult.Response[i].AssetName + ' Version ' + PublishedgetEntityNewsFeedResult.Response[i].Versionno + ' from <a href="#mui/planningtool/default/detail/section/' + PublishedgetEntityNewsFeedResult.Response[i].EntityID + '/overview">' + PublishedgetEntityNewsFeedResult.Response[i].Entityname + '</a></p>';
                        } else {
                            if (PublishedgetEntityNewsFeedResult.Response[i].Isobjective == true)
                                feeddivHtml = feeddivHtml + '<p>' + PublishedgetEntityNewsFeedResult.Response[i].UserName + ' published  ' + PublishedgetEntityNewsFeedResult.Response[i].AssetName + '  from <a href="#mui/objective/detail/section/' + PublishedgetEntityNewsFeedResult.Response[i].EntityID + '/overview">' + PublishedgetEntityNewsFeedResult.Response[i].Entityname + '</a></p>';
                            else if(PublishedgetEntityNewsFeedResult.Response[i].EntitytypeId == 35)
                                feeddivHtml = feeddivHtml + '<p>' + PublishedgetEntityNewsFeedResult.Response[i].UserName + ' published  ' + PublishedgetEntityNewsFeedResult.Response[i].AssetName + '  from <a href="#mui/calender/detail/section/' + PublishedgetEntityNewsFeedResult.Response[i].EntityID + '/overview">' + PublishedgetEntityNewsFeedResult.Response[i].Entityname + '</a></p>';
                            else if(PublishedgetEntityNewsFeedResult.Response[i].EntitytypeId == 5)
                                feeddivHtml = feeddivHtml + '<p>' + PublishedgetEntityNewsFeedResult.Response[i].UserName + ' published  ' + PublishedgetEntityNewsFeedResult.Response[i].AssetName + '  from <a href="#mui/planningtool/costcentre/detail/section/' + PublishedgetEntityNewsFeedResult.Response[i].EntityID + '/overview">' + PublishedgetEntityNewsFeedResult.Response[i].Entityname + '</a></p>';
                                        else
                            feeddivHtml = feeddivHtml + '<p>' + PublishedgetEntityNewsFeedResult.Response[i].UserName + ' published  ' + PublishedgetEntityNewsFeedResult.Response[i].AssetName + '  from <a href="#mui/planningtool/default/detail/section/' + PublishedgetEntityNewsFeedResult.Response[i].EntityID + '/overview">' + PublishedgetEntityNewsFeedResult.Response[i].Entityname + '</a></p>';
                        }
                        feeddivHtml = feeddivHtml + '            </div>';
                        feeddivHtml = feeddivHtml + '            <div class="cmntFooter">';
                        feeddivHtml = feeddivHtml + '                <span class="cmntTime">' + PublishedgetEntityNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                        feeddivHtml = feeddivHtml + '            </div>';
                        feeddivHtml = feeddivHtml + '        </div>';
                        feeddivHtml = feeddivHtml + '    </div>';
                        feeddivHtml = feeddivHtml + '</li>';
                    }
                    $('#' + $scope.PublishedEntityFeedsdivID + '').append(feeddivHtml);
                    $('#' + $scope.PublishedEntityFeedsdivID + '').parents('.box-content').find('.InProgress').remove();
                });
            } catch (e) { }
        }

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.DashBoardWidget.PublishedAssetsWidgetCtrl']"));
        });
    }
    app.controller("mui.DashBoardWidget.PublishedAssetsWidgetCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$location', '$window', 'PublishedassetswidgetService', muiDashBoardWidgetPublishedAssetsWidgetCtrl]);
    function PublishedassetswidgetService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GettingAssetsFeedSelectionDashbord: GettingAssetsFeedSelectionDashbord
        });
        function GettingAssetsFeedSelectionDashbord(Topx, FeedTemplateID, Newsfeedid) { var request = $http({ method: "get", url: "api/common/GettingAssetsFeedSelectionDashbord/" + Topx + "/" + FeedTemplateID + "/" + Newsfeedid, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("PublishedassetswidgetService", ['$http', '$q', PublishedassetswidgetService]);
})(angular, app);