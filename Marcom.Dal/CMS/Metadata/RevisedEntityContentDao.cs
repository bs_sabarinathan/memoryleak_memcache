﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.CMS.Model
{
    public partial class RevisedEntityContentDao : BaseDao, ICloneable
    {
        #region Member variables

        protected int _id;
        protected int _entityid;
        protected bool _active;
        protected string _content;
        protected string _createdon;

        #endregion

        #region Constructors

        public RevisedEntityContentDao()
        {

        }

        public RevisedEntityContentDao(int pid)
        {
            this._id = pid;
        }

        public RevisedEntityContentDao(int pid, int pentityid, bool pactive, string pcontent, string pcreateon)
        {
            this._id = pid;
            this._entityid = pentityid;
            this._active = pactive;
            this._content = pcontent;
            this._createdon = pcreateon;
        }

        #endregion

        #region Public properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public virtual int EntityID
        {
            get { return _entityid; }
            set { _entityid = value; }
        }


        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }


        public virtual string Content
        {
            get { return _content; }
            set { _content = value; }
        }

        public virtual string CreatedOn
        {
            get { return _createdon; }
            set { _createdon = value; }
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string RevisedEntityContentDao = "RevisedEntityContentDao";
            public const string ID = "ID";
            public const string EntityID = "EntityID";
            public const string Active = "Active";
            public const string Content = "Content";
            public const string CreatedOn = "CreatedOn";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string RevisedEntityContentDao = "RevisedEntityContentDao";
            public const string ID = "ID";
            public const string EntityID = "EntityID";
            public const string Active = "Active";
            public const string Content = "Content";
            public const string CreatedOn = "CreatedOn";
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

    }
}
