﻿(function (ng, app) {
    function muiDAMassettaskCtrl($window, $location, $route, $timeout, $scope, $cookies, $resource, requestContext, _, $compile, DamService, $routeParams, $translate, TaskService, MetadataService, PlanningService) {
        var model;
        $scope.GetNonBusinessDaysforDatePicker();
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            model = model1;
            $scope.TaskDueDate1 = false;
        };
        $scope.dynCalanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = false;
            $scope.TaskDueDate1 = true;
        };
        var TenantFilePath1 = TenantFilePath.replace(/\\/g, "\/")
        $scope.showAttach = "Dam";
        $scope.freezeTaskList = false;
        $scope.ShowDamAssetApprovalType = 0;
        $scope.AssetTaskList = 0;
        $scope.newAssetTaskid = 0;
        $scope.TaskAssetID = 0;
        $scope.AssetTaskAttachAssetHtml = "";
        $scope.AssetTaskTypeObj = {
            "AssetTaskType": 0
        };
        $scope.$on("Cleasetaskpopup", function (event, fromPlace, tasktype, tasklist, assetid) {
            $scope.ShowDamAssetApprovalType = 0;
            $scope.AssetTaskList = tasklist;
            $scope.TaskAssetID = assetid;
            $scope.showAttach = fromPlace;
            if (fromPlace == "Task") {
                if ($scope.AssetTaskList > 0)
                    $scope.freezeTaskList = true;
            }
            $scope.AssetTaskTypeObj.AssetTaskType = tasktype;
            if (tasktype == 32)
                $scope.ShowDamAssetApprovalType = 1;
            else if (tasktype == 36)
                $scope.ShowDamAssetApprovalType = 2;
            else if (tasktype == 37)
                $scope.ShowDamAssetApprovalType = 3;
            else
                $scope.ShowDamAssetApprovalType = 0;
            LoadThumbnailBlocks();
            ResetVariables();
            $scope.userid = parseInt($.cookie('UserId'), 10);
            $scope.userisProofInitiator = false;
            $scope.EntityID = ($scope.DamAssetTaskselectionFiles.AssetFiles[0] != undefined) ? $scope.DamAssetTaskselectionFiles.AssetFiles[0].EntityID : parseInt($routeParams.ID, 10);
            PlanningService.GetMember($scope.EntityID).then(function (member) {
                $scope.memberList = member.Response;
                var loggedinUserDets = $.grep($scope.memberList, function (e) {
                    return e.Userid == $scope.userid
                });
                var loggedinUserRoles = $.grep(loggedinUserDets, function (e) {
                    return e.BackendRoleID == EntityRoles.ProofInitiator
                });
                if (loggedinUserRoles.length > 0) {
                    $scope.userisProofInitiator = true;
                    MetadataService.GetTaskTypes().then(function (TaskTypeData) {
                        $scope.TaskTypeList = TaskTypeData.Response;
                    });
                } else if ($scope.userisProofInitiator == false && $scope.TaskTypeList.length > 0) {
                    var DelTaskType = [],
                        data = [];
                    DelTaskType = $.grep($scope.TaskTypeList, function (e) {
                        return e.TaskTypeId == 36;
                    });
                    if (DelTaskType.length > 0) {
                        $scope.TaskTypeList.splice($.inArray(DelTaskType[0], $scope.TaskTypeList), 1);
                    }
                    DelTaskType = [];
                    DelTaskType = $.grep($scope.TaskTypeList, function (e) {
                        return e.TaskTypeId == 37;
                    });
                    if (DelTaskType.length > 0) {
                        $scope.TaskTypeList.splice($.inArray(DelTaskType[0], $scope.TaskTypeList), 1);
                    }
                }
            });
            LoadTaskList();
            if (fromPlace == "Assetedit" || fromPlace == "Dam" || fromPlace == "marcommediabank") {
                GetentityMembers($scope.DamAssetTaskselectionFiles.AssetFiles[0].EntityID);
            }
        });
        $scope.AssetTasktypeswithExt = [];
        $scope.DamExt = [];
        $scope.Fileids = [];
        $timeout(function () { GetAllExtensionTypesforDAM(); }, 100);

        function GetAllExtensionTypesforDAM() {
            DamService.GetAllExtensionTypesforDAM().then(function (data) {
                $scope.AssetTasktypeswithExt = data.Response;
            });
        }

        function LoadThumbnailBlocks() {
            $timeout(function () {
                var html = '';
                $scope.AssetTaskAttachAssetHtml = "";
                $("#assettaskentityattachassetdata").html(html);
                LoadThumbnailView();
                $scope.DamAssetTaskselectionFiles.AssetFiles = [];
                $scope.DamAssetTaskselectionFiles.ThumbnailSettings = [];
                $scope.DamAssetTaskselectionFiles.AssetDynamicData = [];
            }, 100);
        }

        function LoadThumbnailView() {
            if ($scope.DamAssetTaskselectionFiles.AssetFiles != null) {
                $scope.addfromGenAttachments = true;
                var assets = [];
                assets = $scope.DamAssetTaskselectionFiles.AssetFiles;
                for (var i = 0, asset; asset = assets[i++];) {
                    var rec = [];
                    rec = $.grep($scope.AssetTasktypeswithExt.m_Item1, function (e) {
                        return e.Id == asset.AssetTypeid
                    });
                    var remainAssetFilesRecord = [];
                    remainAssetFilesRecord = $.grep($scope.DamAssetTaskselectionFiles.AssetSelectionFiles, function (e) {
                        return e.AssetID == asset.AssetUniqueID;
                    });
                    if (remainAssetFilesRecord.length == 0) {
                        if (asset.AssetUniqueID != undefined) {
                            $scope.DamAssetTaskselectionFiles.AssetSelectionFiles.push(asset.AssetUniqueID);
                        }
                    }
                    $scope.AssetTaskAttachAssetHtml += '<div id="Asset_' + asset.AssetUniqueID + '" class="attachmentBox">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="row-fluid">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="span12">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="span2 leftSection">';
                    $scope.AssetTaskAttachAssetHtml += '<i class="icon-ok-sign" style="display:none;"></i>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="fileType-Preview">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="fileType-PreviewDiv">';
                    if (asset.Category == 0) {
                        if (asset.Extension != null) {
                            if (asset.ProcessType > 0) {
                                if (asset.Status == 2)
                                    $scope.AssetTaskAttachAssetHtml += '<img id="file_' + asset.AssetUniqueID + '" src="' + TenantFilePath + 'DAMFiles/Preview/Small_' + asset.FileGuid + '.jpg?' + generateUniqueTracker() + '" alt="Image" onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.png?' + generateUniqueTracker() + '\'"/>';
                                else if (asset.Status == 1 || asset.Status == 0)
                                    $scope.AssetTaskAttachAssetHtml += '<img class="loadingImg" id=' + asset.ActiveFileID + ' data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset(' + asset.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 3) {
                                    $scope.AssetTaskAttachAssetHtml += '<img  src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.png?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                    $scope.AssetTaskAttachAssetHtml += '<div class="th-Error-preview">';
                                    $scope.AssetTaskAttachAssetHtml += '<span class="errorMsg"><i class="icon-info-sign"></i>';
                                    $scope.AssetTaskAttachAssetHtml += '<span class="errorTxt">Unable to generate Preview</span></span>';
                                    $scope.AssetTaskAttachAssetHtml += '</div>';
                                }
                            } else {
                                $scope.AssetTaskAttachAssetHtml += '<img  src="' + TenantFilePath1 + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.png?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else
                            $scope.AssetTaskAttachAssetHtml += '<img  src="' + TenantFilePath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 1) {
                        $scope.AssetTaskAttachAssetHtml += '<img id="file_' + asset.AssetUniqueID + '"  src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/BLANK.png?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 2) {
                        $scope.AssetTaskAttachAssetHtml += '<img id="file_' + asset.AssetUniqueID + '"  src="' + TenantFilePath + 'DAMFiles/StaticPreview_small/LINK.png?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="progress progress-striped active">';
                    $scope.AssetTaskAttachAssetHtml += '<div style="width: 0%" class="bar"></div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="progressInfo">';
                    $scope.AssetTaskAttachAssetHtml += '<span class="progress-percent"></span>';
                    if (asset.Category == 0) {
                        if (parseSize(asset.Size) == "NaN bytes") {
                            $scope.AssetTaskAttachAssetHtml += '<span class="fileSize">' + (asset.Size) + ' </span>';
                        } else {
                            $scope.AssetTaskAttachAssetHtml += '<span class="fileSize">' + parseSize(asset.Size) + ' </span>';
                        }
                    } else {
                        $scope.AssetTaskAttachAssetHtml += '<span class="fileSize"></span>';
                    }
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="span10 rightSection">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="info">';
                    $scope.AssetTaskAttachAssetHtml += '<span class="name">';
                    if (asset.Category == 0) {
                        $scope.AssetTaskAttachAssetHtml += '<i class="icon-file-alt"></i>';
                    }
                    $scope.AssetTaskAttachAssetHtml += asset.AssetName;
                    $scope.AssetTaskAttachAssetHtml += '</span>';
                    $scope.AssetTaskAttachAssetHtml += '<span class="size" ng-click="removeasset(' + asset.AssetUniqueID + ')">';
                    $scope.AssetTaskAttachAssetHtml += '<i class="icon-remove removefile"></i>';
                    $scope.AssetTaskAttachAssetHtml += '</span>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="formInfo">';
                    $scope.AssetTaskAttachAssetHtml += '<form class="form-float">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="control-group fullSpace">';
                    $scope.AssetTaskAttachAssetHtml += '<label class="control-label"> Asset type </label>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="controls">';
                    $scope.AssetTaskAttachAssetHtml += '<label class="control-label">' + rec[0] != undefined ? rec[0].damCaption : "" + '</label>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="control-group">';
                    $scope.AssetTaskAttachAssetHtml += '<label class="control-label"> Asset name </label>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="controls">';
                    $scope.AssetTaskAttachAssetHtml += '<label class="control-label">' + asset.AssetName + '</label>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</form>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                }
                $("#assettaskentityattachassetdata").append($compile($scope.AssetTaskAttachAssetHtml)($scope));
            } else { }
        }
        $scope.removeasset = function (assetid) {
            $("#assettaskentityattachassetdata").children("#Asset_" + assetid + "").remove();
            if ($("#assettaskentityattachassetdata > div").length == 0) {
                $scope.addfromGenAttachments = false;
                $scope.AssetTaskAttachAssetHtml = '';
            } else {
                $scope.AssetTaskAttachAssetHtml = '';
                $scope.AssetTaskAttachAssetHtml = $("#assettaskentityattachassetdata").html();
            }
            $scope.DamAssetTaskselectionFiles.AssetFiles.splice($.inArray(assetid, $scope.DamAssetTaskselectionFiles.AssetFiles), 1);
            $scope.DamAssetTaskselectionFiles.AssetSelectionFiles.splice($.inArray(assetid, $scope.DamAssetTaskselectionFiles.AssetSelectionFiles), 1);
            var remainRecord = [];
            var remainRecorddata = [];
            remainRecord = $.grep($scope.DamAssetTaskselectionFiles.AssetFiles, function (e) {
                return e.AssetUniqueID == assetid;
            });
            if (remainRecord.length > 0) {
                $scope.DamAssetTaskselectionFiles.AssetFiles.splice($.inArray(remainRecord[0], $scope.DamAssetTaskselectionFiles.AssetFiles), 1);
            }
            remainRecorddata = $.grep($scope.DamAssetTaskselectionFiles.AssetDynamicData, function (e) {
                return e.ID == assetid;
            });
            if (remainRecorddata.length > 0) {
                $scope.DamAssetTaskselectionFiles.AssetDynamicData.splice($.inArray(remainRecorddata[0], $scope.DamAssetTaskselectionFiles.AssetDynamicData), 1);
            }
        }

        function GenereteDetailBlock(asset) {
            var attrRelation = [];
            attrRelation = $.grep($scope.DamAssetTaskselectionFiles.ThumbnailSettings, function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            var htmldata = '';
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    var cls = attr.ID == 68 ? "th-infoMain" : "th-infoSub";
                    if ($scope.DamAssetTaskselectionFiles.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.DamAssetTaskselectionFiles.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = [];
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined)
                                htmldata += '<span class="' + cls.toString() + '">' + data[0]["" + attr.ID + ""] + '</span>';
                        } else
                            htmldata += '<span class="' + cls.toString() + '">-</span>';
                    } else
                        htmldata += '<span class="' + cls.toString() + '">-</span>';
                }
            }
            return htmldata;
        }
        $scope.tasktypeobj = {
            tasktype: 2
        };
        $scope.$on("OpenTree", function (event, fromPlace, tasktype) {
            $scope.showAttach = fromPlace;
            $scope.AssetTaskTypeObj.AssetTaskType = tasktype;
            $scope.$broadcast('PopulateTree', fromPlace, 2);
        });
        $scope.TaskTypeFilter = function (item) {
            if ($scope.ShowDamAssetApprovalType > 0)
                if (item.TaskTypeId == 32 || item.TaskTypeId == 36 || item.TaskTypeId == 37)
                    return false;
                else
                    return true;
            else {
                if (item.TaskTypeId == 32 || item.TaskTypeId == 36 || item.TaskTypeId == 37)
                    return false;
                else
                    return true;
                return true;
            }
        }
        $scope.Close = function () {
            $scope.DisableIsAdminTask = true;
            $scope.TaskActionCaption = "Add task";
            $scope.TaskMembersList = "";
            $scope.NewTaskName = "";
            $scope.TaskDueDate = new Date.create();
            $scope.TaskDueDate = null;
            $scope.TaskDescription = "";
            $scope.SelectedTaskID = 0;
            $scope.dyn_Cont = '';
            $scope.DamAssetTaskselectionFiles.AssetFiles = [];
            $scope.DamAssetTaskselectionFiles.ThumbnailSettings = [];
            $scope.DamAssetTaskselectionFiles.AssetDynamicData = [];
            $scope.DamAssetTaskselectionFiles.AssetSelectionFiles = [];
            $("#damtaskDynamicTaskControls").html("");
            $("#damtaskDynamicTaskControls").append($compile("")($scope));
            $scope.AdminTaskCheckList = [{
                ID: 0,
                NAME: ""
            }];
            $("#DynamicTaskControls").html("");
        }

        function ResetVariables() {
            $scope.ngShowUnassigned = false;
            $scope.ngShowTaskCreation = true;
            $scope.DisableIsAdminTask = false;
            if ($scope.ShowDamAssetApprovalType > 0)
                $scope.DisableIsAdminTask = true;
            $scope.TaskActionCaption = "Add task";
            $scope.TaskMembersList = "";
            $scope.NewTaskName = "";
            $scope.TaskDueDate = new Date.create();
            $scope.TaskDueDate = null;
            $scope.TaskDescription = "";
            $scope.SelectedTaskID = 0;
            $scope.dyn_Cont = '';
            $("#damtaskDynamicTaskControls").html("");
            $("#damtaskDynamicTaskControls").append($compile("")($scope));
            $scope.AdminTaskCheckList = [{
                ID: 0,
                NAME: ""
            }];
            $("#DynamicTaskControls").html("");
            $scope.createdynamicControls($scope.AssetTaskTypeObj.AssetTaskType);
            $("#loadworktask").modal('show');
            refreshModel();
            $timeout(function () {
                $('#addtaskcaption').focus();
            }, 1000);
        }

        function LoadTaskList() {
            TaskService.GetEntityTaskListWithoutTasks($routeParams.ID).then(function (TaskListLibraryData) {
                $scope.TaskLibraryList = TaskListLibraryData.Response;
                if ($scope.TaskLibraryList.length == 1) {
                    $timeout(function () {
                        $scope.AssetTaskList = $scope.TaskLibraryList[0].ID
                    }, 100);
                }
            });
        }
        $scope.fields = {
            usersID: ''
        };
        $scope.Dropdown = {};
        $scope.ShowHideAttributeOnRelation = {};
        $scope.EnableDisableControlsHolder = {};
        $scope.optionsLists = [];
        $scope.EnableDisableControlsHolder = {};
        $scope.Inherritingtreelevels = {};
        $scope.InheritingLevelsitems = [];
        $scope.treeTexts = {};
        $scope.treeSources = {};
        $scope.DropDownTreeOptionValues = {};
        $scope.treeSourcesObj = [];
        $scope.UploadAttributeData = [];
        $scope.OptionObj = {};

        function refreshModel() {
            $scope.TaskBriefDetails = {
                taskID: 0,
                taskListUniqueID: 0,
                taskTypeId: 0,
                taskTypeName: "",
                EntityID: 0,
                ownerId: 0,
                taskName: "",
                dueIn: 0,
                dueDate: "",
                status: "",
                statusID: 0,
                taskOwner: "",
                Description: "",
                taskmembersList: [],
                taskAttachmentsList: [],
                taskProgressCount: "",
                totalTaskMembers: [],
                TaskMembersRoundTripGroup: [],
                taskCheckList: [],
                WorkTaskInprogressStatus: ""
            };
        }
        $scope.TaskTypeList = [];
        MetadataService.GetTaskTypes().then(function (TaskTypeData) {
            $scope.TaskTypeList = TaskTypeData.Response;
        });
        $scope.EntityMemberList = [];
        $scope.EntityDistinctMembers = [];
        PlanningService.GetMember($routeParams.ID).then(function (EntityMemberList) {
            $scope.EntityMemberList = EntityMemberList.Response;
            $scope.EntityDistinctMembers = [];
            var dupes = {};
            if ($scope.EntityMemberList != null) {
                for (var i = 0, el; el = $scope.EntityMemberList[i++];) {
                    if (el.IsInherited != true) {
                        if (!dupes[el.Userid]) {
                            dupes[el.Userid] = true;
                            $scope.EntityDistinctMembers.push(el);
                        }
                    }
                }
            }
        });
        GetentityMembers($routeParams.ID);

        function GetentityMembers(entityID) {
            $scope.EntityMemberList = [];
            PlanningService.GetMember(entityID).then(function (EntityMemberList) {
                $scope.EntityMemberList = EntityMemberList.Response;
                $scope.EntityDistinctMembers = [];
                var dupes = {};
                if ($scope.EntityMemberList != null) {
                    for (var i = 0, el; el = $scope.EntityMemberList[i++];) {
                        if (el.IsInherited != true) {
                            if (!dupes[el.Userid]) {
                                dupes[el.Userid] = true;
                                $scope.EntityDistinctMembers.push(el);
                            }
                        }
                    }
                }
            });
        }
        $scope.fields = {
            usersID: ''
        };
        $scope.assetfields = {
            usersID: ''
        };
        $scope.EnableDisableControlsHolder = {};
        $scope.atributesRelationList = {};
        $scope.optionsLists = [];
        $scope.EnableDisableControlsHolder = {};
        $scope.Inherritingtreelevels = {};
        $scope.InheritingLevelsitems = [];
        $scope.treeTexts = {};
        $scope.treeSources = {};
        $scope.treeSourcesObj = [];
        $scope.UploadAttributeData = [];
        $scope.OptionObj = {};
        $scope.PercentageVisibleSettings = {};
        $scope.DropDownTreePricing = {};
        $scope.listAttributeValidationResult = [];
        $scope.listAttriToAttriResult = [];
        $scope.items = [];
        $scope.treeSources = {};
        $scope.treelevels = {};
        $scope.NormalDropdownCaption = {};
        $scope.uploader = {};
        $scope.UploaderCaption = {};
        $scope.NormalMultiDropdownCaption = {};
        $scope.treeTexts = {};
        $scope.treeSelection = [];
        $scope.normaltreeSources = {};
        $scope.treeNodeSelectedHolder = new Array();
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            var _ref;
            $scope.output = "You selected: " + branch.Caption;
            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
                for (var i = 0, parent; parent = parentArr[i++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == parent.AttributeId && e.id == parent.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(parent);
                    }
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                    if (branch.Children.length > 0) {
                        RemoveRecursiveChildTreenode(branch.Children);
                    }
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
        };
        $scope.renderHtml = function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };
        var treeTextVisbileflag = false;

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == child.AttributeId && e.id == child.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }
            }
        }
        $scope.treesrcdirec = {};
        $scope.my_tree = tree = {};
        $scope.ChecklistVisible = false;
        $scope.ClearModelObject = function (ModelObject) {
            for (var variable in ModelObject) {
                if (typeof ModelObject[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        ModelObject[variable] = "";
                    }
                } else if (typeof ModelObject[variable] === "number") {
                    ModelObject[variable] = null;
                } else if (Array.isArray(ModelObject[variable])) {
                    ModelObject[variable] = [];
                } else if (typeof ModelObject[variable] === "object") {
                    ModelObject[variable] = {};
                }
            }
        }
        $scope.createdynamicControls = function (rootID) {
            $scope.fields = {
                usersID: ''
            };
            if (rootID != "" && rootID != undefined) {
                var Tasktype_value = $.grep($scope.TaskTypeList, function (rel) {
                    return rel.Id == $scope.AssetTaskTypeObj.AssetTaskType;
                })[0];
                if (Tasktype_value != undefined) {
                    if (Tasktype_value.TaskTypeId == 2) {
                        $scope.ChecklistVisible = true;
                        $scope.AdminTaskCheckList = [{
                            ID: 0,
                            NAME: ""
                        }];
                    } else {
                        $scope.ChecklistVisible = false;
                        $scope.AdminTaskCheckList = [{
                            ID: 0,
                            NAME: ""
                        }];
                    }
                }
            } else {
                $scope.AdminTaskCheckList = [{
                    ID: 0,
                    NAME: ""
                }];
                $scope.ChecklistVisible = false;
            }
            $("#damtaskDynamicTaskControls").html("");
            $("#damtaskDynamicTaskControls").append($compile("")($scope));
            $scope.treeNodeSelectedHolder = [];
            $scope.treesrcdirec = {};
            $scope.treePreviewObj = {};
            $scope.PercentageVisibleSettings = {};
            $scope.DropDownTreePricing = {};
            $scope.ClearModelObject($scope.fields);
            $scope.dyn_Cont = '';
            $scope.ShowHideAttributeOnRelation = {};
            $scope.EnableDisableControlsHolder = {};
            $scope.OptionObj = {};
            $scope.items = [];
            if (rootID != null && rootID != "") {
                MetadataService.GetEntityTypeAttributeRelationWithLevelsByID(rootID, 0).then(function (entityAttributesRelation) {
                    $scope.atributesRelationList = entityAttributesRelation.Response;
                    for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                        if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                            if ($scope.atributesRelationList[i].AttributeID != 70) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                if ($scope.atributesRelationList[i].InheritFromParent)
                                    $scope.atributesRelationList[i].DefaultValue = $scope.atributesRelationList[i].ParentValue[0];
                                if ($scope.atributesRelationList[i].AttributeID !== SystemDefiendAttributes.Name) {
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                                    $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            if ($scope.atributesRelationList[i].IsSpecial == true) {
                                if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                    $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"> <input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.OwnerList[0].UserName;
                                }
                            } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.FiscalYear) {
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,3)\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                if ($scope.atributesRelationList[i].InheritFromParent)
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                                else
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.EntityStatus) { } else {
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,3)\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                if ($scope.atributesRelationList[i].InheritFromParent)
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                                else
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                            var totLevelCnt = $scope.atributesRelationList[i].Levels.length;
                            for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                    return item.Caption
                                };
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                    return item.Caption
                                };
                                if (j == 0) {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                    $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                } else {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                    $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                            if (($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TakeDescription) && ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskNotes)) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea></div></div>";
                                if ($scope.atributesRelationList[i].InheritFromParent)
                                    $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                                else
                                    $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select  class=\"multiselect\"   data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\"  multiselect-dropdown ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,4)\"   ></select></div></div>";
                            if ($scope.atributesRelationList[i].InheritFromParent) {
                                var defaultmultiselectvalue1 = $scope.atributesRelationList[i].ParentValue.split(',');
                                $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = defaultmultiselectvalue1;
                            } else {
                                var defaultmultiselectvalue = $scope.atributesRelationList[i].DefaultValue.split(',');
                                if ($scope.atributesRelationList[i].DefaultValue != "") {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = defaultmultiselectvalue;
                                } else {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                            $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                            if ($scope.MinValue < 0) {
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                            } else {
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                            }
                            if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                            } else {
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                            }
                            var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                            $scope.items.push({
                                startDate: [],
                                endDate: [],
                                comment: '',
                                sortorder: 0
                            });
                            $scope.dyn_Cont += "    <div class=\"control-group\"><label for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.atributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                            $scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span5\">";
                            $scope.dyn_Cont += "<input class=\"sdate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + "item.startDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open" + "item.startDate" + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-change=\"changeperioddate_changed(item.startDate,'StartDate')\" ng-model=\"item.startDate\" placeholder=\"-- Start date --\"/><input class=\"edate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" name=\"enddate\" id=\"item.endDate\" ng-change=\"changeperioddate_changed(item.endDate,'EndDate')\" ng-model=\"item.endDate\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + "item.endDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open" + "item.endDate" + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- End date --\"/><input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
                            $scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
                            $scope.fields["DatePart_Calander_Open" + "item.startDate"] = false;
                            $scope.fields["DatePart_Calander_Open" + "item.endDate"] = false;
                            $scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
                            $scope.fields["Period_" + $scope.atributesRelationList[i].AttributeID] = "";
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                            $scope.fields["Tree_" + $scope.atributesRelationList[i].AttributeID] = [];
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                            if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                treeTextVisbileflag = false;
                                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID])) {
                                    $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = true;
                                } else
                                    $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                            } else {
                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                            }
                            $scope.dyn_Cont += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + $scope.atributesRelationList[i].AttributeID + '\" class="control-group treeNode-control-group">';
                            $scope.dyn_Cont += '<label class="control-label">' + $scope.atributesRelationList[i].AttributeCaption + '</label>';
                            $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                            $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '"></div>';
                            $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '">';
                            $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                            $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" accessable="' + $scope.atributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                            $scope.dyn_Cont += '</div></div></div>';
                            $scope.dyn_Cont += '<div class="control-group staticTreeGroup margin-top0x" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationList[i].AttributeID + '\">';
                            $scope.dyn_Cont += '<div class="controls">';
                            $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.atributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                            $scope.dyn_Cont += '</div></div>';
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                            $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                            $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = true;
                            $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = $scope.atributesRelationList[i].DropDownPricing;
                            $scope.dyn_Cont += "<div drowdowntreepercentagemultiselection data-purpose='entity' data-attributeid=" + $scope.atributesRelationList[i].AttributeID.toString() + "></div>";
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                            if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskDueDate) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = false;
                                $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                } else {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                } else {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                                }
                                var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.DateTime_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-change=\"setTimeout(changeduedate_changed(fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "," + $scope.atributesRelationList[i].AttributeID + "),3000)\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"fields.DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                var param1 = new Date.create();
                                var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                                $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                            var totLevelCnt1 = $scope.atributesRelationList[i].Levels.length;
                            for (var j = 0; j < totLevelCnt1; j++) {
                                if (totLevelCnt1 == 1) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                        return item.Caption
                                    };
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                        return item.Caption
                                    };
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                    $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                        return item.Caption
                                    };
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                        return item.Caption
                                    };
                                    if (j == 0) {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                        $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                    } else {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                        if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                            $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                        } else {
                                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                            $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                        }
                                    }
                                }
                            }
                        }
                        if ($scope.atributesRelationList[i].IsReadOnly == true) {
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        } else {
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        }
                    }
                    $scope.dyn_Cont += '<input style="display:none" type="submit" id="damtskbtnTemp" class="ng-scope" invisible>';
                    $("#damtaskDynamicTaskControls").append($compile($scope.dyn_Cont)($scope));
                    $("#DamTaskDynamicMetadata").scrollTop(0);
                    $scope.rootDisplayName = $scope.fields["TextSingleLine_" + SystemDefiendAttributes.Name];
                    $timeout(function () {
                        HideAttributeToAttributeRelationsOnPageLoad();
                    }, 200);
                    GetValidationList(rootID);
                    $("#DamTaskDynamicMetadata").addClass('notvalidate');
                });
            }
        }
        $scope.AddDefaultEndDate = function (enddate, startdate, currentindex) {
            if (currentindex != undefined) {
                var enddate1 = null;
                if (currentindex != 0) {
                    enddate1 = $scope.items[currentindex - 1].endDate;
                }
                if (enddate1 != null && enddate1 >= startdate) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1987.Caption'));
                    $scope.items[currentindex].startDate = null;
                    $scope.items[currentindex].endDate = null;
                } else {
                    if (startdate == null) {
                        $scope.items[currentindex].endDate = null;
                    } else {
                        $scope.items[currentindex].endDate = (new Date.create(startdate)).addDays(7);
                    }
                }
            } else {
                if (enddate.toUpperCase().contains("STARTDATE")) {
                    var temp = startdate;
                    startdate = enddate;
                    enddate = temp;
                }
                var objsetenddate = new Date.create($scope.fields[startdate]);
                $scope.fields[enddate] = objsetenddate.addDays(7);
            }
        };

        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToShow = [];
            if (attrLevel > 0) {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            } else {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }
            if (attributesToShow[0] != undefined) {
                for (var i = 0; i < attributesToShow[0].length; i++) {
                    var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToShow = [];
                var hideAttributeOtherThanSelected = [];
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        } else if (attrType == 12) {
                            if (j == levelcnt)
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }
                    if (attrType == 6) {
                        if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    } else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    } catch (e) { }
                }
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);
                if (attrType == 3) {
                    if ($scope.fields['ListSingleSelection_' + attrID] != null) {
                        optionValue = parseInt(($scope.fields['ListSingleSelection_' + attrID]), 10);
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    if ($scope.fields['ListMultiSelection_' + attrID] != null) {
                        optionValue = $scope.fields['ListMultiSelection_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                } else if (attrType == 7) {
                    if ($scope.fields['Tree_' + attrID] != null) {
                        optionValue = $scope.fields['Tree_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }

        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }
        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad = function (attrID, attributeLevel, attrVal, attrType) {
            try {
                var optionValue = attrVal;
                var attributesToShow = [];
                if (attrType == 3) {
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 7) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6 || attrType == 12) {
                    if (attrVal != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrVal != null) ? parseInt(attrVal, 10) : 0) && e.AttributeLevel == ((attributeLevel != null) ? parseInt(attributeLevel, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }

        function GetValidationList(rootID) {
            MetadataService.GetValidationDationByEntitytype(rootID).then(function (GetValidationresult) {
                if (GetValidationresult.Response != null) {
                    $scope.listValidationResult = GetValidationresult.Response;
                    if ($scope.listAttriToAttriResult != null) {
                        for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                            var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                            if (attrRelIDs != undefined) {
                                for (var j = 0; j < attrRelIDs.length; j++) {
                                    if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                        });
                                    } else {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                        });
                                    }
                                }
                            }
                        }
                        $("#DamTaskDynamicMetadata").nod($scope.listValidationResult, {
                            'delay': 200,
                            'submitBtnSelector': '#damtskbtnTemp',
                            'silentSubmit': 'true'
                        });
                    }
                }
            });
        }
        var treeTextVisbileflag = false;

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        function GetTreeCheckedNodes(treeobj, attrID) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    $scope.fields["Tree_" + attrID].push(node.id);
                }
                if (node.Children.length > 0)
                    GetTreeCheckedNodes(node.Children, attrID);
            }
        }
        $scope.treeNodeSelectedHolder = [];
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            if ($scope.treeNodeSelectedHolder.length > 0) {
                for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
                    $scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
                }
            }
            $scope.ShowHideAttributeToAttributeRelations(branch.AttributeId, 0, 0, 7);
        };

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == child.AttributeId && e.id == child.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }
            }
        }
        $scope.treesrcdirec = {};
        $scope.my_tree = tree = {};
        $scope.LoadDropDownChildLevels = function (attrID, attributeLevel, levelcnt, attrType) {
            if (levelcnt > 0) {
                var currntlevel = attributeLevel + 1;
                for (var j = currntlevel; j <= levelcnt; j++) {
                    $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                    if (attrType == 6) {
                        $scope.fields["DropDown_" + attrID + "_" + j] = "";
                    } else if (attrType == 12) {
                        if (j == levelcnt)
                            $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                        else
                            $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                    }
                }
                if (attrType == 6) {
                    if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                            $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        });
                    }
                } else if (attrType == 12) {
                    if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                            $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        });
                    }
                }
            }
        }
        
        $scope.drpdirectiveSource = {};

        $scope.IsSourceformed = function (attrID, levelcnt, attributeLevel, attrType) {
            if (levelcnt > 0) {
                var dropdown_text = '', subid = 0;
                var currntlevel = attributeLevel + 1;
                if (attributeLevel == 1) {
                    var dropdown_text = 'dropdown_text_' + attrID + '_' + attributeLevel;
                    var idtomatch = $scope['dropdown_' + attrID + '_' + attributeLevel] != undefined ? ($scope['dropdown_' + attrID + '_' + attributeLevel].id != undefined ? $scope['dropdown_' + attrID + '_' + attributeLevel].id : $scope['dropdown_' + attrID + '_' + attributeLevel]) : 0;
                    if (idtomatch != 0)
                        $scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel] = $.grep($scope.treeSources["dropdown_" + attrID].Children,
                            function (e) {
                                return e.id == idtomatch;
                            }
                        )[0];
                }
                for (var j = currntlevel; j <= levelcnt; j++) {
                    dropdown_text = 'dropdown_text_' + attrID + '_' + j;
                    subid = $scope['dropdown_' + attrID + '_' + (j)] != undefined ? ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined ? $scope['dropdown_' + attrID + '_' + (j - 1)].id : $scope['dropdown_' + attrID + '_' + (j)]) : 0;
                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = {};
                    if (subid != 0 && subid > 0) {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + (j)] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children != undefined && $scope['dropdown_' + attrID + '_' + (j - 1)] != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = ($.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children, function (e) { return e.id == subid; }))[0];
                        }
                    }
                    else {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + j] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)] != undefined) {
                                var res = [];
                                if ($scope['dropdown_' + attrID + '_' + (j)] > 0)
                                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)]['Children'], function (e) { return e.id == $scope['dropdown_' + attrID + '_' + (j)] })[0];
                                else
                                    $scope['dropdown_' + attrID + '_' + (j)] = 0;
                            }
                        }
                    }
                }
            }
        }

        $scope.BindChildDropdownSource = function (attrID, levelcnt, attributeLevel, attrType) {

            if (levelcnt > 0) {
                var currntlevel = attributeLevel + 1;
                for (var j = currntlevel; j <= levelcnt; j++) {
                    $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].length);
                    if (attrType == 6) {
                        $scope["dropdown_" + attrID + "_" + j] = 0;
                        $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].length);
                    } else if (attrType == 12) {
                        if (j == levelcnt) $scope["multiselectdropdown_" + attrID + "_" + j] = [];
                        else $scope["multiselectdropdown_" + attrID + "_" + j] = "";
                    }
                }
                if (attrType == 6) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {
                        var children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["dropdown_" + attrID + "_" + attributeLevel]) })[0].Children;

                        if (children != undefined) {
                            var subleveloptions = [];
                            $.each(children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                } else if (attrType == 12) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {

                        var sublevel_res = [];
                        if ($scope["multiselectdropdown_" + attrID + "_" + attributeLevel] != undefined && $scope["multiselectdropdown_" + attrID + "_" + attributeLevel] > 0)
                            sublevel_res = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["multiselectdropdown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (sublevel_res != undefined) {
                            var subleveloptions = [];
                            $.each(sublevel_res, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                }
            }
        }

        var treeTextVisbileflag = false;

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        function GetadminTaskCheckLists(taskID) {
            $scope.AdminTaskCheckList = [];
            TaskService.getTaskchecklist(taskID).then(function (TaskDetailList) {
                $scope.AdminTaskCheckList = TaskDetailList.Response;
                if ($scope.AdminTaskCheckList.length == 0)
                    $scope.AdminTaskCheckList.push({
                        ID: 0,
                        NAME: ""
                    });
            });
        }
        $scope.AddCheckList = function (Index) {
            if ($scope.SelectedTaskID != 0)
                $scope.AdminTaskCheckList.push({
                    Id: 0,
                    Name: "",
                    SortOrder: 0,
                    Status: false,
                    UserName: "",
                    UserId: 0,
                    CompletedOnValue: "",
                    OwnerId: 0,
                    OwnerName: "",
                    CompletedOn: ""
                });
            else
                $scope.AdminTaskCheckList.push({
                    ID: 0,
                    NAME: ""
                });
        }
        $scope.RemoveCheckList = function (Index, ID) {
            if ($scope.AdminTaskCheckList.length > 1) {
                bootbox.confirm($translate.instant('LanguageContents.Res_2012.Caption'), function (result) {
                    if (result) {
                        $timeout(function () {
                            if (ID > 0) {
                                TaskService.DeleteEntityCheckListByID(ID).then(function (result) {
                                    if (result.StatusCode == 200) {
                                        $scope.AdminTaskCheckList.splice(Index, 1);
                                    }
                                });
                            } else {
                                $scope.AdminTaskCheckList.splice(Index, 1);
                            }
                        }, 100);
                    }
                });
            } else {
                bootbox.confirm($translate.instant('LanguageContents.Res_2012.Caption'), function (result) {
                    if (result) {
                        $timeout(function () {
                            if (ID > 0) {
                                TaskService.DeleteEntityCheckListByID(ID).then(function (result) {
                                    if (result.StatusCode == 200) {
                                        $scope.AdminTaskCheckList[Index].NAME = "";
                                    }
                                });
                            } else {
                                $scope.AdminTaskCheckList[Index].NAME = "";
                            }
                        }, 100);
                    }
                });
            }
        }
        $scope.formatSelection = function (item) {
            return item.UserName;
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.ReassignMembersData = [];
        $scope.formatResult = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<img src="Handlers/UserImage.ashx?id=' + item.id + '">';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.UserName + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.OwnerList = [];
        $scope.OwnerList.push({
            "Roleid": 1,
            "RoleName": "Owner",
            "UserEmail": $scope.ownerEmail,
            "DepartmentName": "-",
            "Title": "-",
            "Userid": parseInt($scope.OwnerID, 10),
            "UserName": $scope.OwnerName,
            "IsInherited": '0',
            "InheritedFromEntityid": '0'
        });
        $scope.CreateNewTask = function () {
            var filemoved = false;
            var repeater = 0;
            var repeater = 0;
            if ($scope.AssetTaskList == 0 && $scope.TaskLibraryList.length > 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_5553.Caption'));
                return;
            }
            if ($scope.AssetTaskList != null && $scope.AssetTaskList != undefined && $scope.AssetTaskList != 0) {
                $timeout(function () {
                    $scope.CreateAttachmentTask();
                }, 20);
            } else {
                if ($scope.TaskLibraryList != null)
                    if ($scope.TaskLibraryList.length == 0)
                        bootbox.alert($translate.instant('LanguageContents.Res_4566.Caption'));
                    else
                        bootbox.alert($translate.instant('LanguageContents.Res_4613.Caption'));
                else
                    bootbox.alert($translate.instant('LanguageContents.Res_4613.Caption'));
            }
        };
        $scope.AttributeData = [];
        $scope.CreateUnassignedNewTask = function () {
            $scope.addtaskvallid = 0;
            if ($('#ngDamShowUnassigned').hasClass('disabled')) {
                return;
            }
            $('#ngDamShowUnassigned').addClass('disabled');
            var dateval = new Date.create();
            dateval = new Date.create(dateFormat(dateval, $scope.DefaultSettings.DateFormat));
            var alertText = "";
            $scope.AttributeData = [];
            var taskMembersAvailable = [];
            if ($scope.TaskMembersList == undefined || $scope.TaskMembersList == "")
                taskMembersAvailable = [];
            else
                taskMembersAvailable = $scope.TaskMembersList;
            if ($scope.AssetTaskTypeObj.AssetTaskType == "" || $scope.AssetTaskTypeObj.AssetTaskType == undefined || $scope.AssetTaskTypeObj.AssetTaskType == 0)
                alertText += $translate.instant('LanguageContents.Res_4637.Caption');
            var Tasktype_value = $.grep($scope.TaskTypeList, function (rel) {
                return rel.Id == $scope.AssetTaskTypeObj.AssetTaskType;
            })[0];
            if (Tasktype_value != undefined)
                if (Tasktype_value.TaskTypeId == "" || Tasktype_value.TaskTypeId == undefined) {
                    if (!alertText.contains("Please select the task type\n"))
                        alertText += $translate.instant('LanguageContents.Res_4637.Caption');
                }
            if ($scope.NewTaskName == "")
                alertText += $translate.instant('LanguageContents.Res_4587.Caption');
            if ($scope.TaskDueDate != undefined && new Date.create(dateFormat($scope.TaskDueDate, $scope.DefaultSettings.DateFormat)) < dateval)
                alertText += ($translate.instant('LanguageContents.Res_1909.Caption'));
            if (taskMembersAvailable.length > 0) {
                if ($scope.TaskDueDate == "" || $scope.TaskDueDate == undefined || $scope.TaskDueDate == null)
                    alertText += $translate.instant('LanguageContents.Res_4223.Caption');
            }
            if (alertText == "") {
                $scope.addtaskvallid = 1;
                $scope.AttributeData = [];
                var Tasktype_value = $.grep($scope.TaskTypeList, function (rel) {
                    return rel.Id == $scope.AssetTaskTypeObj.AssetTaskType;
                })[0];
                var SaveTask = {};
                SaveTask.TaskType = Tasktype_value.TaskTypeId;
                SaveTask.Typeid = $scope.AssetTaskTypeObj.AssetTaskType;
                SaveTask.Name = $scope.NewTaskName;
                SaveTask.TaskListID = $scope.SelectedTaskLIstID;
                SaveTask.TaskID = $scope.SelectedUnassignedTaskID;
                SaveTask.Description = $scope.TaskDescription;
                SaveTask.Note = $scope.TaskNote;
                SaveTask.TaskAttachments = [];
                SaveTask.TaskAttachments = $scope.AttachmentFilename;
                SaveTask.TaskFiles = $scope.FileList;
                SaveTask.ParentEntityID = $scope.TaskGlobalEntityID;
                SaveTask.DueDate = $scope.TaskDueDate != undefined ? $scope.TaskDueDate : "";
                SaveTask.TaskMembers = [];
                var taskOwnerObj = [];
                taskOwnerObj = $scope.OwnerList[0];
                SaveTask.TaskMembers = $scope.TaskMembersList != undefined ? $scope.TaskMembersList : [];
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                    "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
                                    "Value": "-1"
                                });
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            var levelCount = $scope.atributesRelationList[i].Levels.length;
                            if (levelCount == 1) {
                                for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                        "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level,
                                        "Value": "-1"
                                    });
                                }
                            } else {
                                if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                    if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                        for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                            $scope.AttributeData.push({
                                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                                "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level,
                                                "Value": "-1"
                                            });
                                        }
                                    } else {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                            "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
                                            "Value": "-1"
                                        });
                                    }
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            var attributeLevelOptions = [];
                            attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID + ""], function (e) {
                                return e.level == (j + 1);
                            }));
                            if (attributeLevelOptions[0] != undefined) {
                                if (attributeLevelOptions[0].selection != undefined) {
                                    for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
                                        var valueMatches = [];
                                        if (attributeLevelOptions[0].selection.length > 1)
                                            valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                                return relation.NodeId.toString() === opt;
                                            });
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [opt],
                                            "Level": (j + 1),
                                            "Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
                                        });
                                    }
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                        if ($scope.atributesRelationList[i].IsSpecial == true) {
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            }
                        } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                            if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                                var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID];
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                        if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name)
                            $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                        else {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": ($scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID] != null) ? $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                        if (($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TakeDescription) && ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskNotes)) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": ($scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID] != null) ? $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                                for (var k = 0; k < multiselectiObject.length; k++) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": parseInt(multiselectiObject[k], 10),
                                        "Level": 0,
                                        "Value": "-1"
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                        var MyDate = new Date.create();
                        var MyDateString;
                        if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                            if ($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                var datestring = formatteddateFormat(($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID]), "yyyy/MM/dd");
                                MyDateString = (datestring).toString("yyyy/MM/dd");
                            } else
                                MyDateString = "";
                        } else {
                            MyDateString = "";
                        }
                        if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskDueDate) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": MyDateString,
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                        var treenodes = [];
                        treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) {
                            return e.AttributeId == $scope.atributesRelationList[i].AttributeID;
                        });
                        for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": [parseInt(nodeval.id, 10)],
                                "Level": parseInt(nodeval.Level, 10),
                                "Value": "-1"
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                            "Level": 0,
                            "Value": "-1"
                        });
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 18) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": "",
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                }
                SaveTask.AttributeData = $scope.AttributeData;
                var taskResObj = $.grep($scope.AdminTaskCheckList, function (e) {
                    return e.Name != null;
                });
                SaveTask.AdminTaskCheckList = $scope.AdminTaskCheckList;
                TaskService.InsertUnassignedEntityTaskWithAttachments(SaveTask).then(function (SaveTaskResult) {
                    if (SaveTaskResult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
                        $('#ngDamShowUnassigned').removeClass('disabled');
                    } else {
                        if (SaveTaskResult.Response != null && SaveTaskResult.Response.m_Item1 != 0) {
                            $('#ngDamShowUnassigned').removeClass('disabled');
                            var returnObj = SaveTaskResult.Response.m_Item2;
                            NotifySuccess($translate.instant('LanguageContents.Res_4820.Caption'));
                            if ($scope.TaskActionFor == 1)
                                RereshTaskObj(returnObj);
                            else
                                RereshTaskObj(returnObj);
                            $scope.FileList = [];
                            $scope.SelectedTaskLIstID = 0;
                            $scope.SelectedUnassignedTaskID = 0;
                            $scope.AttachmentFilename = [];
                            refreshModel();
                            $('#addTask').modal('hide');
                        } else {
                            $('#ngDamShowUnassigned').removeClass('disabled');
                            NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
                        }
                    }
                });
            } else {
                $('#ngDamShowUnassigned').removeClass('disabled');
                bootbox.alert(alertText);
                return false;
            }
        }
        $scope.AttributeData = [];
        $scope.CreateAttachmentTask = function () {
            $scope.addtaskvallid = 0;
            var percentageflag = false;
            $('div[data-role="formpercentagetotalcontainer"]').children().find('span[data-role="percentageerror"]').each(function (index, value) {
                if (($(this).attr('data-selection') != undefined) && ($(this).attr('data-ispercentage') != undefined) && ($(this).attr('data-isnotfilter') != undefined)) {
                    if (((parseInt($(this).attr('data-selection')) > 1) && ($(this).attr('data-ispercentage') == "true") && ($(this).attr('data-isnotfilter') == "true")) && ($(this).hasClass("result lapse"))) {
                        percentageflag = true;
                    }
                }
            });
            if (percentageflag) {
                return false;
            }
            $("#damtskbtnTemp").click();
            $("#DamTaskDynamicMetadata").removeClass('notvalidate');
            if ($("#DamTaskDynamicMetadata .error").length > 0) {
                return false;
            }
            $scope.IsNotVersioning = true;
            if ($('#DamCreatedamNewTaskBtn').hasClass('disabled')) {
                return;
            }
            $('#DamCreatedamNewTaskBtn').addClass('disabled');
            var dateval = new Date.create();
            dateval = new Date.create(dateFormat(dateval, $scope.DefaultSettings.DateFormat));
            var alertText = "";
            $scope.AttributeData = [];
            var taskMembersAvailable = [];
            if ($scope.TaskMembersList == undefined || $scope.TaskMembersList == "")
                taskMembersAvailable = [];
            else
                taskMembersAvailable = $scope.TaskMembersList;
            if ($scope.AssetTaskTypeObj.AssetTaskType == "" || $scope.AssetTaskTypeObj.AssetTaskType == undefined || $scope.AssetTaskTypeObj.AssetTaskType == 0)
                alertText += $translate.instant('LanguageContents.Res_4637.Caption');
            var Tasktype_value = $.grep($scope.TaskTypeList, function (rel) {
                return rel.Id == $scope.AssetTaskTypeObj.AssetTaskType;
            })[0];
            if (Tasktype_value != undefined)
                if (Tasktype_value.TaskTypeId == "" || Tasktype_value.TaskTypeId == undefined) {
                    if (!alertText.contains($translate.instant('LanguageContents.Res_4637.Caption')))
                        alertText += $translate.instant('LanguageContents.Res_4637.Caption');
                }
            if ($scope.NewTaskName == "")
                alertText += $translate.instant('LanguageContents.Res_4587.Caption');
            if ($scope.TaskDueDate != undefined && dateDiffBetweenDates($scope.TaskDueDate) < 0)
                alertText += ($translate.instant('LanguageContents.Res_1909.Caption'));
            if (alertText == "") {
                $scope.AttributeData = [];
                var SaveTask = {};
                if (Tasktype_value != undefined) {
                    SaveTask.TaskType = Tasktype_value.TaskTypeId;
                } else {
                    SaveTask.TaskType = $scope.AssetTaskTypeObj.AssetTaskType;
                }
                var AssetidforAssetapproval = 0;
                if ($scope.AssetTaskTypeObj.AssetTaskType == 32 || $scope.AssetTaskTypeObj.AssetTaskType == 36 || $scope.AssetTaskTypeObj.AssetTaskType == 37) {
                    if ($scope.DamAssetTaskselectionFiles.AssetSelectionFiles.length == 1)
                        $scope.TaskAssetID = $scope.DamAssetTaskselectionFiles.AssetSelectionFiles[0];
                    else {
                        if ($("#assettaskentityattachassetdata > div").length == 1 && $scope.DamAssetTaskselectionFiles.AssetSelectionFiles.length > 1) {
                            $scope.TaskAssetID = $scope.DamAssetTaskselectionFiles.AssetSelectionFiles[$scope.DamAssetTaskselectionFiles.AssetSelectionFiles.length - 1];
                            for (var i = 0; i < $scope.DamAssetTaskselectionFiles.AssetSelectionFiles.length; i++) {
                                if ($scope.TaskAssetID != $scope.DamAssetTaskselectionFiles.AssetSelectionFiles[i] && $scope.TaskAssetID > 0) {
                                    $scope.removeasset($scope.DamAssetTaskselectionFiles.AssetSelectionFiles[i]);
                                }
                            }
                        } else {
                            if ($("#assettaskentityattachassetdata > div").length > 1)
                                bootbox.alert($translate.instant('LanguageContents.Res_4918.Caption'));
                            else if ($("#assettaskentityattachassetdata > div").length == 0)
                                bootbox.alert($translate.instant('LanguageContents.Res_5756.Caption'));
                            $('#DamCreatedamNewTaskBtn').removeClass('disabled');
                            return false;
                        }
                    }
                }
                SaveTask.Typeid = $scope.AssetTaskTypeObj.AssetTaskType;
                SaveTask.Name = $scope.NewTaskName;
                SaveTask.TaskListID = $scope.AssetTaskList;
                SaveTask.Description = $scope.TaskDescription;
                SaveTask.TaskAttachments = [];
                SaveTask.TaskAttachments = $scope.AttachmentFilename;
                SaveTask.TaskFiles = $scope.FileList;
                SaveTask.ParentEntityID = $routeParams.ID;
                SaveTask.AssetID = ($scope.AssetTaskTypeObj.AssetTaskType == 32 || $scope.AssetTaskTypeObj.AssetTaskType == 36 || $scope.AssetTaskTypeObj.AssetTaskType == 37) ? $scope.TaskAssetID : 0;
                SaveTask.DueDate = $scope.TaskDueDate != null ? $scope.TaskDueDate : "";
                SaveTask.TaskMembers = [];
                var taskOwnerObj = [];
                taskOwnerObj = $scope.OwnerList[0];
                if ($scope.TaskMembersList == undefined || $scope.TaskMembersList == "")
                    $scope.TaskMembersList = [];
                else
                    SaveTask.TaskMembers = $scope.TaskMembersList;
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                    "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
                                    "Value": "-1"
                                });
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 18) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": "",
                            "Level": 0,
                            "Value": "-1"
                        });
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            var levelCount = $scope.atributesRelationList[i].Levels.length;
                            if (levelCount == 1) {
                                for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                        "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level,
                                        "Value": "-1"
                                    });
                                }
                            } else {
                                if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                    if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                        for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                            $scope.AttributeData.push({
                                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                                "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level,
                                                "Value": "-1"
                                            });
                                        }
                                    } else {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                            "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
                                            "Value": "-1"
                                        });
                                    }
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            var attributeLevelOptions = [];
                            attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID + ""], function (e) {
                                return e.level == (j + 1);
                            }));
                            if (attributeLevelOptions[0] != undefined) {
                                if (attributeLevelOptions[0].selection != undefined) {
                                    for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
                                        var valueMatches = [];
                                        if (attributeLevelOptions[0].selection.length > 1)
                                            valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                                return relation.NodeId.toString() === opt;
                                            });
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [opt],
                                            "Level": (j + 1),
                                            "Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
                                        });
                                    }
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                        if ($scope.atributesRelationList[i].IsSpecial == true) {
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            }
                        } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                            if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                                var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID];
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                        if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name)
                            $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                        else {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": ($scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID] != null) ? $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                        if (($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TakeDescription) && ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskNotes)) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": ($scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID] != null) ? $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                                for (var k = 0; k < multiselectiObject.length; k++) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": parseInt(multiselectiObject[k], 10),
                                        "Level": 0,
                                        "Value": "-1"
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                        var MyDate = new Date.create();
                        var MyDateString;
                        if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                            if ($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                var datestring = formatteddateFormat(($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID]), "yyyy/MM/dd");
                                MyDateString = (datestring).toString("yyyy/MM/dd");
                            } else
                                MyDateString = "";
                        } else {
                            MyDateString = "";
                        }
                        if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskDueDate) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": MyDateString,
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                        var treenodes = [];
                        treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) {
                            return e.AttributeId == $scope.atributesRelationList[i].AttributeID;
                        });
                        for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": [parseInt(nodeval.id, 10)],
                                "Level": parseInt(nodeval.Level, 10),
                                "Value": "-1"
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                }
                SaveTask.AttributeData = $scope.AttributeData;
                SaveTask.Periods = [];
                $scope.savesubentityperiods = [];
                for (var m = 0; m < $scope.items.length; m++) {
                    if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                        if ($scope.items[m].startDate.length != 0 && $scope.items[m].endDate.length != 0) {
                            $scope.savesubentityperiods.push({
                                startDate: ($scope.items[m].startDate.length != 0 ? dateFormat($scope.items[m].startDate) : ''),
                                endDate: ($scope.items[m].endDate.length != 0 ? dateFormat($scope.items[m].endDate) : ''),
                                comment: ($scope.items[m].comment.trim().length != 0 ? $scope.items[m].comment : ''),
                                sortorder: 0
                            });
                        }
                    }
                }
                SaveTask.Periods.push($scope.savesubentityperiods);
                var taskResObj = $.grep($scope.AdminTaskCheckList, function (e) {
                    return e.NAME != "";
                });
                SaveTask.AdminTaskCheckList = $scope.AdminTaskCheckList;
                TaskService.InsertEntityTaskWithAttachments(SaveTask).then(function (SaveTaskResult) {
                    if (SaveTaskResult.StatusCode == 405) {
                        $('#DamCreatedamNewTaskBtn').removeClass('disabled');
                        NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
                    } else {
                        if (SaveTaskResult.Response != null && SaveTaskResult.Response.m_Item1 != 0) {
                            $('#DamCreatedamNewTaskBtn').removeClass('disabled');
                            var returnObj = SaveTaskResult.Response.m_Item2;
                            $scope.newAssetTaskid = SaveTaskResult.Response.m_Item1;
                            if ($("#assettaskentityattachassetdata > div").length > 0) {
                                var attchasset = {};
                                attchasset.AssetArr = $scope.DamAssetTaskselectionFiles.AssetSelectionFiles;
                                attchasset.EntityID = $scope.newAssetTaskid;
                                attchasset.FolerId = 0;
                                DamService.AttachAssetsforProofTask(attchasset).then(function (result) {
                                    if (result.Response == 0) {
                                        var assetArr = result.Response;
                                        $('#DamCreatedamNewTaskBtn').removeClass('disabled');
                                        NotifyError($translate.instant('LanguageContents.Res_4327.Caption'));
                                    } else {
                                        var assetArr = result.Response;
                                        $scope.DamAssetTaskselectionFiles.AssetSelectionFiles = [];
                                        $scope.AssetTaskAttachAssetHtml = '';
                                        NotifySuccess($translate.instant('LanguageContents.Res_4820.Caption'));
                                        $('#loadworktask').modal('hide');
                                        if ($scope.AssetTaskTypeObj.AssetTaskType == 36 || $scope.AssetTaskTypeObj.AssetTaskType == 37)
                                            $timeout(function () {
                                                CreateProofTask($scope.newAssetTaskid, assetArr, $scope.AssetTaskTypeObj.AssetTaskType);
                                            }, 20);
                                        if ($scope.showAttach == "Task") {
                                            $scope.LiveTaskListIDCollection.push({
                                                "TaskLiStID": $scope.AssetTaskList,
                                                "EntityID": $routeParams.ID
                                            });
                                            $scope.$emit('LiveTaskListUpdate', $scope.LiveTaskListIDCollection);
                                        }
                                        $scope.DamAssetTaskselectionFiles.AssetFiles = [];
                                        $scope.DamAssetTaskselectionFiles.ThumbnailSettings = [];
                                        $scope.DamAssetTaskselectionFiles.AssetDynamicData = [];
                                        $scope.DamAssetTaskselectionFiles.AssetSelectionFiles = [];
                                        if ($scope.showAttach == "Dam") {
                                            $timeout(function () {
                                                $scope.$emit('RefreshAssetSelectionforTask', $scope.AssetTaskList);
                                                $scope.AssetTaskList = 0;
                                                refreshModel();
                                            }, 10);
                                        }
                                    }
                                });
                            }
                        } else {
                            $('#DamCreatedamNewTaskBtn').removeClass('disabled');
                            NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
                        }
                    }
                });
            } else {
                $('#DamCreatedamNewTaskBtn').removeClass('disabled');
                bootbox.alert(alertText);
                return false;
            }
        }

        function CreateProofTask(taskid, assetarr, tasktypeid) {
            var proofdetails = {};
            proofdetails.AssetArr = assetarr;
            proofdetails.TaskID = $scope.newAssetTaskid;
            proofdetails.Tasktypeid = tasktypeid;
            DamService.CreateproofTask(proofdetails).then(function (result) {
                if (result.Response == true) { }
            });
        }

        function parseSize(size) {
            var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"],
                tier = 0;
            while (size >= 1024) {
                size = size / 1024;
                tier++;
            }
            return Math.round(size * 10) / 10 + " " + suffix[tier];
        }

        function dateDiffBetweenDates(dateTo) {
            var _MS_PER_DAY = 1000 * 60 * 60 * 24;
            var dateToday = new Date.create();
            var utcDateSet = Date.UTC(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate());
            var utcToday = Date.UTC(dateToday.getFullYear(), dateToday.getMonth(), dateToday.getDate());
            return Math.floor((utcDateSet - utcToday) / _MS_PER_DAY);
        }
        $scope.LoadTreeData = function () {
            $scope.$broadcast('PopulateTree', "Dam", 32);
        }
        $scope.$on("CallbackDamFiles", function (event, attachments, files, assetId) {
            $scope.TaskAssetID = assetId;
            $scope.AttachmentFilename = attachments;
            $scope.FileList = files;
        });
        $scope.changeduedate_changed = function (duedate, ID) {
            if (duedate != null) {
                var test = isValidDate(duedate.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(duedate, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert($translate.instant('LanguageContents.Res_5747.Caption'));
                            $scope.fields["DatePart_" + ID] = "";
                        }
                    }
                } else {
                    $scope.fields["DatePart_" + ID] = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_5748.Caption'));
                }
            }
        }
        $scope.changeperioddate_changed = function (date, datetype) {
            if (date != null) {
                var test = isValidDate(date.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(date, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert($translate.instant('LanguageContents.Res_5747.Caption'));
                            if (datetype == "StartDate")
                                $scope.items[0].startDate = "";
                            else
                                $scope.item[0].endDate = "";
                        }
                    }
                } else {
                    if (datetype == "StartDate")
                        $scope.items[0].startDate = "";
                    else
                        $scope.item[0].endDate = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_5748.Caption'));
                }
            }
        }

        function isValidDate(dateval, dateformat) {
            var formatlen;
            var defaultdateVal = [];
            defaultdateVal = dateval.length;
            formatlen = dateformat.length;
            if (formatlen == defaultdateVal || defaultdateVal > formatlen)
                return true;
            else
                return false;
        };
    }
    app.controller("mui.DAM.assettaskCtrl", ['$window', '$location', '$route', '$timeout', '$scope', '$cookies', '$resource', 'requestContext', '_', '$compile', 'DamService', '$routeParams', '$translate', 'TaskService', 'MetadataService', 'PlanningService', muiDAMassettaskCtrl]);
})(angular, app);