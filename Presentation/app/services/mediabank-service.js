﻿(function (ng, app) {
    "use strict";

    function MediabankService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            GetAllExtensionTypesforDAM: GetAllExtensionTypesforDAM,
            GetCustomFilterAttributes: GetCustomFilterAttributes,
            GetDAMViewSettings: GetDAMViewSettings,
            GetDAMToolTipSettings: GetDAMToolTipSettings,
            GetMediaAssets: GetMediaAssets,
            DownloadFiles: DownloadFiles,
            GetAttributeDetails: GetAttributeDetails,
            AddPublishedFilesToDAM: AddPublishedFilesToDAM,
            AddPublishedLinkFiles: AddPublishedLinkFiles,
            DownloadAssetWithMetadata: DownloadAssetWithMetadata,
            SendMailWithMetaData: SendMailWithMetaData,
            UnPublishAssets: UnPublishAssets,
            GetLockStatus: GetLockStatus,
            LinkpublishedfilestoCMS: LinkpublishedfilestoCMS
        });

        function GetAllExtensionTypesforDAM() {
            var request = $http({
                method: "get",
                url: "api/dam/GetDAMExtensions/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCustomFilterAttributes(typeid) {
            var request = $http({
                method: "get",
                url: "api/dam/GetCustomFilterAttributes/" + typeid,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDAMViewSettings() {
            var request = $http({
                method: "get",
                url: "api/dam/GetDAMViewSettings/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDAMToolTipSettings() {
            var request = $http({
                method: "get",
                url: "api/dam/GetDAMToolTipSettings/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetMediaAssets(jobj) {
            var request = $http({
                method: "post",
                ignoreLoadingBar : true,
                url: "api/dam/GetMediaAssets/",
                params: {
                    action: "add"
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DownloadFiles(arrObj) {
            var request = $http({
                method: "post",
                url: "api/dam/DownloadDamZip/",
                params: {
                    action: "add"
                },
                data: {
                    AssetArr: arrObj
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeDetails(damid) {
            var request = $http({
                method: "get",
                ignoreLoadingBar : true,
                url: "api/dam/GetAssetAttributesDetails/" + damid,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function AddPublishedFilesToDAM(jobj) {
            var request = $http({
                method: "post",
                url: "api/dam/AddPublishedFilesToDAM/",
                params: {
                    action: "add"
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }
        function AddPublishedLinkFiles(jobj) {
            var request = $http({
                method: "post",
                url: "api/dam/AddPublishedLinkFiles/",
                params: {
                    action: "add"
                },
                data: jobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DownloadAssetWithMetadata(assetid, result) {
            var request = $http({
                method: "post",
                url: "api/dam/DownloadAssetWithMetadata/",
                params: {
                    action: "add"
                },
                data: {
                    assetid: assetid,
                    result: result
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SendMailWithMetaData(srcObj) {
            var request = $http({
                method: "post",
                url: "api/dam/SendMailWithMetaData/",
                params: {
                    action: "add"
                },
                data: srcObj
            });
            return (request.then(handleSuccess, handleError));
        }

        function UnPublishAssets(arrObj) {
            var request = $http({
                method: "post",
                url: "api/dam/UnPublishAssets/",
                params: {
                    action: "add"
                },
                data: {
                    AssetArr: arrObj
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetLockStatus(EntityID) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetLockStatus/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function LinkpublishedfilestoCMS(jobj) {
            var request = $http({
                method: "post",
                url: "api/cms/LinkpublishedfilestoCMS/", params: { action: "add" }, data: jobj
            }); return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.service("MediabankService", ['$http', '$q', MediabankService]);
})(angular, app);