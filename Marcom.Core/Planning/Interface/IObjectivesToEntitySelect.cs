﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
   public interface IObjectivesToEntitySelect
    {
       int ObjectiveID { get; set; }
       string ObjectiveName { get; set; }
       string ObjectiveOwner { get; set; }
       int ObjectiveTypeId { get; set; }
       string ObjectiveDescription { get; set; }
    }
}
