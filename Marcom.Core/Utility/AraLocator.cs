﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Utility
{

    [Serializable]
    public class ArrayLocation
    {
        private int start;
        private int end;
        public virtual int Start
        {
            get
            {
                return start;
            }
            set
            {
                this.start = value;
            }
        }
        public virtual int End
        {
            get
            {
                return end;
            }
            set
            {
                this.end = value;
            }
        }

    }
}


