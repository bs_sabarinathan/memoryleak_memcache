﻿(function (ng, app) {
    function muiadmincustomviewCtrl($scope, $location, $resource, $timeout, $cookies, $compile, $translate, AdminService) {
        $scope.listdata = {};
        $scope.Createdon = 0;
        $scope.executionTimevalue = 0;
        $scope.executionTime = false;
        $scope.noResult = false;
        $scope.noResultmsg = "";
        $scope.Selecttablename = "";
        $scope.ngdbschema = false;
        $scope.ngdbtable = true;
        $scope.reportschemaview = false;
        $scope.tablewithselectColumns = [];
        $timeout(function () {
            ValidateCustomView();
            $("#Customview").addClass('notvalidate');
        }, 300);

        function ValidateCustomView() {
            var viewValues = [
				['#Name', 'presence', 'Please enter the Name'],
				['#code1', 'presence', 'Please enter the Query']
            ];
            $("#Customview").nod(viewValues, {
                'delay': 200,
                'submitBtnSelector': '#btnTemp1',
                'silentSubmit': 'true'
            });
        }
        $timeout(function () {
            displayschemastatus(0);
        }, 300);

        function displayschemastatus(status) {
            AdminService.GetReportViewSchemaResponse().then(function (getreportschemarespview) {
                if (getreportschemarespview.Response != 0) {
                    $scope.reportschemaview = true;
                } else {
                    $scope.reportschemaview = false;
                }
                if (getreportschemarespview.Response == 1) {
                    $("#reportschemaerrormessageview").html($translate.instant('LanguageContents.Res_1817.Caption'));
                    if (status == 1) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1814.Caption'))
                    }
                } else if (getreportschemarespview.Response == 2) {
                    $("#reportschemaerrormessageview").html($translate.instant('LanguageContents.Res_1816.Caption'));
                    if (status == 1) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1815.Caption'))
                    }
                } else if (getreportschemarespview.Response == 3) {
                    $("#reportschemaerrormessageview").html($translate.instant('LanguageContents.Res_1816.Caption'));
                    if (status == 1) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1815.Caption'))
                    }
                }
            });
        }
        $("#Name").focusout(function () {
            var re = /^[a-zA-Z_]+$/;
            if (re.test($scope.Name)) { } else {
                NotifyError($translate.instant('LanguageContents.Res_4643.Caption'));
                return false;
            }
        });
        $scope.editableInPopup = '<a class="iconLink" data-toggle="modal" ng-click="GetViewByID(row)" data-target="#sqlEditorPopup"><i class="icon-edit"></i></a> '
        $scope.delete = '<a class="iconLink" data-toggle="modal" ng-click="DeleteViewByID(row)"><i class="icon-remove"></i></a> '
        $scope.DeleteViewByID = function DeleteViewByID(row) {
            var answer = confirm($translate.instant('LanguageContents.Res_2050.Caption'));
            if (answer) {
                AdminService.DeleteView(row.entity.ID).then(function (deleteview) {
                    if (deleteview.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4314.Caption'));
                        displayschemastatus(1);
                    } else {
                        if (deleteview.Response == true) {
                            $scope.listdata.splice(row.rowIndex, 1);
                            $scope.parentViewList = $scope.listdata;
                        }
                        NotifySuccess($translate.instant('LanguageContents.Res_4897.Caption'));
                        displayschemastatus(1);
                    }
                });
            } else { }
        }
        $scope.GetViewByID = function GetViewByID(row) {
            $scope.AddView = false;
            $scope.EditView = true;
            $scope.RowIndex = row.rowIndex;
            $scope.ViewID = row.entity.ID;
            $scope.Name = row.entity.Name;
            $scope.Description = row.entity.Description;
            $scope.Query = row.entity.Query;
            $scope.Createdon = row.entity.Createdon;
            $scope.EnableUpdate = true;
            $scope.EnableValidate = false;
            $scope.EnableAdd = false;
            $scope.isDisabled = true;
            $scope.Namereadonly = true;
            $scope.Queryreadonly = false;
        };

        function Validatequery() {
            $scope.EnableAdd = false;
            $scope.EnableUpdate = false;
            $scope.EnableValidate = true;
        }
        $("#code1").mouseover(function () {
            $scope.EnableUpdate = false;
            $scope.EnableAdd = false;
            $scope.EnableValidate = true;
        });
        $("#code1").mouseout(function () {
            $scope.EnableAdd = false;
            $scope.EnableUpdate = false;
            $scope.EnableValidate = true;
        });
        $("#code1").focusout(function () {
            $scope.EnableAdd = false;
            $scope.EnableUpdate = false;
            $scope.EnableValidate = true;
        });
        AdminService.GetViews().then(function (getviews) {
            $scope.listdata = getviews.Response;
            $scope.parentViewList = $scope.listdata;
        });
        $scope.orderbyname = false;
        $scope.addView = function () {
            $("#Customview").addClass('notvalidate');
            $("#sqlEditorPopup").modal("show");
            $scope.EditView = false;
            $scope.AddView = true;
            $scope.EnableAdd = false;
            $scope.EnableUpdate = false;
            $scope.EnableValidate = true;
            $scope.Name = "";
            $scope.Description = "";
            $scope.Query = "";
            $scope.ViewID = 0;
            $scope.Namereadonly = false;
            $scope.Queryreadonly = false;
            $scope.resultblockObj = [];
        };
        $scope.slno = '<span class="slno">{{row.rowIndex+1}}</span>';
        $scope.filterOptions = {
            filterText: ''
        };
        $scope.gridOptions = {
            data: 'parentViewList',
            enablePinning: false,
            filterOptions: $scope.filterOptions,
            columnDefs: [{
                field: "SlNo",
                displayName: $translate.instant('LanguageContents.Res_4753.Caption'),
                cellTemplate: $scope.slno,
                width: 60
            }, {
                field: "Id",
                displayName: $translate.instant('LanguageContents.Res_69.Caption'),
                width: 60,
                visible: false
            }, {
                field: "Name",
                displayName: $translate.instant('LanguageContents.Res_1.Caption')
            }, {
                field: "Description",
                displayName: $translate.instant('LanguageContents.Res_22.Caption')
            }, {
                field: "Createdon",
                displayName: $translate.instant('LanguageContents.Res_1161.Caption'),
                width: 150
            }, {
                field: "Createdby",
                displayName: $translate.instant('LanguageContents.Res_1162.Caption'),
                width: 150
            }, {
                field: "Query",
                displayName: $translate.instant('LanguageContents.Res_5028.Caption'),
                width: 10,
                visible: false
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.editableInPopup,
                width: 30
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.delete,
                width: 40
            }]
        };
        $scope.saveView = function () {
            var isinvalid = validquery($scope.Query);
            if (isinvalid == true) {
                bootbox.alert($translate.instant('LanguageContents.Res_4677.Caption'));
                return false;
            } else {
                var addView = {};
                addView.ViewID = 0;
                addView.Name = $scope.Name;
                addView.Description = $scope.Description;
                addView.Query = $scope.Query;
                AdminService.InsertCustomViews(addView).then(function (getaddView) {
                    if (getaddView.Response.m_Item1 > 0) {
                        var MyDate = new Date();
                        var MyDateString;
                        $scope.ViewID = getaddView.Response;
                        MyDateString = MyDate.getFullYear() + '-' + ('0' + (MyDate.getMonth() + 1)).slice(-2) + '-' + ('0' + (MyDate.getDate())).slice(-2);
                        $scope.listdata.push({
                            "ID": getaddView.Response.m_Item1,
                            "Name": 'CV_' + $scope.Name,
                            "Description": $scope.Description,
                            "Createdon": MyDateString,
                            "Createdby": $scope.Username,
                            "Query": $scope.Query
                        });
                        $scope.parentViewList = $scope.listdata;
                        NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                        displayschemastatus(1);
                        $scope.Query = "";
                    } else {
                        if (getaddView.Response.m_Item2 === "morethanone") {
                            NotifyError($translate.instant('LanguageContents.Res_1163.Caption'));
                        } else {
                            var errormsg = getaddView.Response.m_Item2;
                            var res = errormsg.replace("<br/>", " ");
                            NotifyError(getaddView.Response.m_Item2);
                        }
                        displayschemastatus(1);
                    }
                });
                $('#sqlEditorPopup').modal('hide');
            }
        };
        $scope.validateView = function () {
            $("#btnTemp1").click();
            $("#Customview").removeClass('notvalidate');
            if ($("#Customview .error").length > 0) {
                return false;
            }
            var isinvalid = validquery($scope.Query);
            if (isinvalid == true) {
                bootbox.alert($translate.instant('LanguageContents.Res_4677.Caption'));
                return false;
            } else {
                var validateView = {};
                validateView.ViewID = $scope.ViewID == undefined ? 0 : $scope.ViewID;
                validateView.Name = $scope.Name;
                validateView.Query = $scope.Query;
                AdminService.GetViewsValidate(validateView).then(function (getvalidateView) {
                    if (getvalidateView.Response == "1") {
                        NotifyError($translate.instant('LanguageContents.Res_4899.Caption'));
                    } else if (getvalidateView.Response == "2") {
                        if ($scope.ViewID > 0) {
                            $scope.EnableUpdate = true;
                        } else {
                            $scope.EnableAdd = true;
                        }
                        $scope.Namereadonly = true;
                        $scope.EnableValidate = false;
                        NotifySuccess($translate.instant('LanguageContents.Res_4678.Caption'));
                    } else if (getvalidateView.Response == "3") {
                        $scope.EnableUpdate = false;
                        $scope.EnableAdd = false;
                        $scope.Queryreadonly = false;
                        $scope.EnableValidate = true;
                        NotifyError($translate.instant('LanguageContents.Res_4677.Caption'));
                    } else if (getvalidateView.Response == "4") {
                        $scope.EnableUpdate = false;
                        $scope.EnableAdd = false;
                        $scope.Queryreadonly = false;
                        $scope.EnableValidate = true;
                        NotifyError($translate.instant('LanguageContents.Res_1163.Caption'));
                    } else {
                        $scope.EnableUpdate = false;
                        $scope.EnableAdd = false;
                        $scope.Queryreadonly = false;
                        bootbox.alert(getvalidateView.Response);
                    }
                });
            }
        };
        $scope.updateView = function () {
            var isinvalid = validquery($scope.Query);
            if (isinvalid == true) {
                bootbox.alert($translate.instant('LanguageContents.Res_4677.Caption'));
                return false;
            } else {
                var updateView = {};
                updateView.ViewID = $scope.ViewID;
                updateView.Name = $scope.Name;
                updateView.Description = $scope.Description;
                updateView.Query = $scope.Query;
                AdminService.CustomViews_Update(updateView).then(function (updateaddView) {
                    if (updateaddView.Response != null) {
                        if (updateaddView.Response.m_Item1) {
                            $scope.listdata[$scope.RowIndex].Name = $scope.Name;
                            $scope.listdata[$scope.RowIndex].Description = $scope.Description;
                            $scope.listdata[$scope.RowIndex].Query = $scope.Query;
                            var MyDate = new Date();
                            var MyDateString;
                            MyDateString = MyDate.getFullYear() + '-' + ('0' + (MyDate.getMonth() + 1)).slice(-2) + '-' + ('0' + (MyDate.getDate())).slice(-2);
                            $scope.listdata[$scope.RowIndex].Createdon = MyDateString;
                            $scope.listdata[$scope.RowIndex].Createdby = $scope.Username;
                            $scope.parentViewList = $scope.listdata;
                            NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                            displayschemastatus(1);
                            $scope.Query = "";
                        } else {
                            if (updateaddView.Response.m_Item2 == "morethanone") {
                                NotifyError($translate.instant('LanguageContents.Res_1163.Caption'));
                            } else {
                                var errormsg = updateaddView.Response.m_Item2;
                                var res = errormsg.replace("<br/>", " ");
                                NotifyError(res);
                            }
                            displayschemastatus(1);
                        }
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                        displayschemastatus(1);
                    }
                })
                $('#sqlEditorPopup').modal('hide');
            }
        };
        $scope.ReportServerPushSchema = function ReportServerPushSchema() {
            AdminService.pushviewSchema().then(function (result) {
                if (result.Response == 1) {
                    $scope.reportschemaview = true;
                    $("#reportschemaerrormessage").html($translate.instant('LanguageContents.Res_1817.Caption'));
                    bootbox.alert($translate.instant('LanguageContents.Res_1817.Caption'));
                } else if (result.Response == 2) {
                    $scope.reportschemaview = true;
                    $("#reportschemaerrormessage").html($translate.instant('LanguageContents.Res_1816.Caption'));
                    bootbox.alert($translate.instant('LanguageContents.Res_1816.Caption'));
                } else if (result.Response == 3) {
                    $scope.reportschemaview = true;
                    $("#reportschemaerrormessage").html($translate.instant('LanguageContents.Res_1816.Caption'));
                    bootbox.alert($translate.instant('LanguageContents.Res_1816.Caption'));
                } else {
                    $scope.reportschemaview = false;
                    NotifySuccess($translate.instant('LanguageContents.Res_4782.Caption'));
                }
            });
        }
        $scope.tablewithColumns = [];
        var cancelTimer = $timeout(function () { loadtables(); }, 500);

        function loadtables() {
            var tablenames = [];
            AdminService.GetAlltablesnames().then(function (getTablenames) {
                var resultTablename = getTablenames.Response;
                for (var i = 0, res; res = getTablenames.Response[i++];) {
                    $scope.tablewithColumns.push({
                        id: res.id,
                        "TableName": res.NAME,
                        "Columns": [],
                        "isExpand": false
                    });
                }
            });
        }
        $('#sqlEditorPopup').on('shown', function () {
            $timeout(function () {
                var testeditor1 = CodeMirror.fromTextArea('code1', {
                    height: "228px",
                    lineNumbers: true,
                    parserfile: "/parsesql.js",
                    stylesheet: "../../../assets/css/sqlcolors.css",
                    path: "../../../assets/js/",
                    textWrapping: false,
                    onChange: updateTextArea,
                    onupdate: updatemirroe
                });


                function updateTextArea() {
                    Validatequery();
                    //testeditor1.save();
                    $scope.Query = testeditor1.getCode();
                    if ($scope.Query.length > 0) {
                        $('#sqeditorBlock').removeClass('error');
                        $('#sqeditorBlock .icon-warning-sign').addClass('displayNone');
                    } else {
                        $('#sqeditorBlock').addClass('error');
                        $('#sqeditorBlock .icon-warning-sign').removeClass('displayNone');
                    }
                }

                function updatemirroe() {
                    Validatequery();
                }
            }, 1000);
        });
        $scope.getcolumninfo = function (tablename, id) {
            var tablewithmetadata = [];
            AdminService.GetAlltableswithmetadata(tablename).then(function (getroles) {
                var result = getroles.Response;
                var columns = $.grep($scope.tablewithColumns, function (rel) {
                    return rel.id == id;
                });
                if (columns.length > 0) {
                    columns[0].Columns = result;
                }
            });
        }
        $scope.resultblockObj = [];
        $scope.editQuery = function () {
            if ($scope.Query != undefined && $scope.Query.length > 0) {
                if ($('#RunQuery').hasClass('disabled')) {
                    return;
                }
                $('#RunQuery').addClass('disabled');
                var isinvalid = validquery($scope.Query);
                if (isinvalid == true) {
                    bootbox.alert($translate.instant('LanguageContents.Res_4677.Caption'));
                    $('#RunQuery').removeClass('disabled');
                    return false;
                } else {
                    var viewschemaview = {};
                    viewschemaview.qrybody = $scope.Query;
                    AdminService.ManipulateQueryEditorQuery(viewschemaview).then(function (result) {
                        $scope.resultblockObj = [];
                        if (result.Response != null) {
                            $('#RunQuery').removeClass('disabled');
                            $scope.executionTime = true;
                            $scope.noResult = false;
                            if (result.Response.m_Item1.length > 0 && result.Response.m_Item2 == "success") {
                                for (var i = 0, item; item = result.Response.m_Item1[i++];) {
                                    var values = [],
										column = [],
										columncollection = [];
                                    for (var j = 0, val; val = item[j++];) {
                                        values = [];
                                        for (var k = 0, data; data = val[k++];) {
                                            if (j == 1) column.push({
                                                name: data.ColumnName,
                                                Id: GenerateGUID()
                                            });
                                            values.push({
                                                res: data.ColumnValue,
                                                Id: GenerateGUID()
                                            });
                                        }
                                        columncollection.push(values);
                                    }
                                    $scope.resultblockObj.push({
                                        Id: GenerateGUID(),
                                        Columns: column,
                                        Values: columncollection
                                    });
                                }
                            } else if (result.Response.m_Item2 != "success") {
                                $scope.executionTime = false;
                                $scope.noResult = true;
                                $scope.noResultmsg = result.Response.m_Item2;
                            }
                            if (result.Response.m_Item3 != null && result.Response.m_Item3 != undefined) {
                                $scope.executionTimevalue = result.Response.m_Item3;
                            }
                        }
                    });
                }
            } else { }
        }
        $scope.close = function () {
            $scope.Name = "";
            $scope.Description = "";
            $scope.Query = "";
            $scope.ViewID = 0;
            $scope.executionTime = false;
            $scope.noResult = false;
            $scope.noResultmsg = "";
            $scope.Namereadonly = false;
            $scope.Queryreadonly = false;
            $scope.resultblockObj = [];
            $scope.Selecttablename = "";
            $scope.tablewithselectColumns = [];
        }
        $scope.showdbschema = function (tablename, id) {
            $scope.Selecttablename = tablename;
            var pervcolumnsval = $.grep($scope.tablewithColumns, function (rel) {
                return rel.id == id;
            });
            if (pervcolumnsval.length > 0) {
                if (pervcolumnsval[0].Columns.length > 0) {
                    $scope.tablewithselectColumns = pervcolumnsval[0].Columns;
                    $scope.ngdbschema = true;
                } else {
                    var tablewithmetadata = [];
                    AdminService.GetAlltableswithmetadata(tablename).then(function (getroles) {
                        var result = getroles.Response;
                        var columns = $.grep($scope.tablewithColumns, function (rel) {
                            return rel.id == id;
                        });
                        if (columns.length > 0) {
                            columns[0].Columns = result;
                            $scope.tablewithselectColumns = columns[0].Columns;
                            $scope.ngdbschema = true;
                        }
                    });
                }
            }
        }
        $scope.hidebschema = function () {
            $scope.ngdbschema = false;
        }
        $scope.showtablelist = function (value) {
            if (value == 1) $scope.ngdbtable = true;
            else $scope.ngdbtable = false;
        }

        function validquery(query) {
            var isinvalid = false;
            var str = query;
            var str1 = str.toUpperCase();
            var arr = ["CREATE ", "DROP ", "TRUNCATE ", "ALTER ", "INSERT ", "UPDATE ", "DELETE "];
            for (var i = arr.length - 1; i >= 0; --i) {
                if (str1.indexOf(arr[i]) != -1) {
                    isinvalid = true;
                }
            }
            return isinvalid
        }
        $scope.$on("$destroy", function () {
            $timeout.cancel(cancelTimer);
        });
    }
    app.controller("mui.admin.customviewCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$compile', '$translate', 'AdminService', muiadmincustomviewCtrl]);
})(angular, app);