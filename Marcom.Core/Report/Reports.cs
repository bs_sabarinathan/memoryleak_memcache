﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using BrandSystems.Marcom.Core.Report.Interface;
using System.Security.Cryptography;
using BrandSystems.Marcom.Core.Access;
namespace BrandSystems.Marcom.Core.Report
{
    /// <summary>
    /// User object for table 'RM_Report'.
    /// </summary>
    namespace BrandSystems.Marcom.Core.Report
    {
        internal class Reports : IReports, ICloneable
        {
            #region Member Variables

            protected int _id;
            protected int _oid;
            protected int _CategoryId;
            protected string _name;
            protected string _caption;
            protected string _description;
            protected string _preview;
            protected bool _show;
            protected bool _entityLevel;
            protected bool _subLevel;
            protected string _status;




            #endregion

            #region Constructors
            public Reports() { }

            public Reports(int poid, string pname, string pcaption, string pdescription, string ppreview, bool pshow, string pstatus, int pCategoryId, bool pentityLevel, bool psubLevel)
            {
                this._oid = poid;
                this._name = pname;
                this._caption = pcaption;
                this._description = pdescription;
                this._preview = ppreview;
                this._show = pshow;
                this._status = pstatus;
                this._CategoryId = pCategoryId;
                this._entityLevel = pentityLevel;
                this._subLevel = psubLevel;
            }

            public Reports(int pId)
            {
                this._id = pId;
            }

            #endregion

            #region Public Properties

            public int Id
            {
                get { return _id; }
                set { _id = value; }

            }

            public int OID
            {
                get { return _oid; }
                set { _oid = value; }

            }

            public int CategoryId
            {
                get { return _CategoryId; }
                set { _CategoryId = value; }

            }

            public string Name
            {
                get { return _name; }
                set
                {
                     _name = value;
                }

            }

            public string Caption
            {
                get { return _caption; }
                set
                {
                    
                    _caption = value;
                }

            }



            public string Description
            {
                get { return _description; }
                set
                {
                   
                    _description = value;
                }

            }

            public string Preview
            {
                get { return _preview; }
                set
                {
                    _preview = value;
                }

            }

            public bool Show
            {
                get { return _show; }
                set
                {
                    _show = value;
                }
            }

            public string Status
            {

                get { return _status; }
                set
                {
                     _status = value;
                }

            }

            public bool EntityLevel
            {
                get { return _entityLevel; }
                set
                {
                    _entityLevel = value;
                }
            }

            public bool SubLevel
            {
                get { return _subLevel; }
                set
                {
                    _subLevel = value;
                }
            }
            #endregion

            #region Equals And HashCode Overrides
            /// <summary>
            /// local implementation of Equals based on unique value members
            /// </summary>
            public override bool Equals(object obj)
            {
                if (this == obj) return true;
                Reports castObj = null;
                try
                {
                    castObj = (Reports)obj;
                }
                catch (Exception) { return false; }
                return (castObj != null) &&
                    (this._id == castObj.Id);
            }
            /// <summary>
            /// local implementation of GetHashCode based on unique value members
            /// </summary>
            public override int GetHashCode()
            {


                int hash = 57;
                hash = 27 * hash * _id.GetHashCode();
                return hash;
            }


            #endregion

            #region ICloneable methods

            public virtual object Clone()
            {
                return this.MemberwiseClone();
            }

            #endregion

        }

    }
}
