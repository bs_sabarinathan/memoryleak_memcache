﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
    class AssetRoleAccessDao : BaseDao, ICloneable
    {
        #region "Member variables"

        protected int _id;
        protected int _roleID;
        protected int _assetID;

        #endregion

        #region "Constructor"

        public AssetRoleAccessDao() { }


        public AssetRoleAccessDao(int pid, int pRoleID,int pAssetID) 
        {
            this._id = pid;
            this._roleID = pRoleID;
            this._assetID = pAssetID;
        }

        #endregion

        #region "Public propertiers"

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }


        public virtual int RoleID
        {
            get { return _roleID; }
            set { _roleID = value; }
        }

        public virtual int AssetID
        {
            get { return _assetID; }
            set { _assetID = value; }
        }


        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AssetRoleAccessDao = "AssetRoleAccessDao";
            public const string ID = "ID";
            public const string RoleID = "RoleID";
            public const string AssetID = "AssetID";

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AssetRoleAccessDao = "AssetRoleAccessDao";
            public const string ID = "ID";
            public const string RoleID = "RoleID";
            public const string AssetID = "AssetID";

        }

        #endregion
    }
}
