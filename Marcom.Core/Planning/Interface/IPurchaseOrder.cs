﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IPurchaseOrder interface for table 'PM_PurchaseOrder'.
    /// </summary>
    public interface IPurchaseOrder
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Entityid
        {
            get;
            set;

        }

        DateTime CreateDate
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }

        string PONumber
        {
            get;
            set;
        }


        int SupplierID
        {
            get;
            set;

        }

        DateTime? ExpectedSpentDate
        {
            get;
            set;

        }

        DateTime? ApprovedDate
        {
            get;
            set;

        }

        int? ApproverID
        {
            get;
            set;

        }

        DateTime? SentDate
        {
            get;
            set;

        }


        int Status
        {
            get;
            set;

        }

        int UserID
        {
            get;
            set;

        }

        bool IsExternal
        {
            get;
            set;
        }


        string RequesterName { get; set; }
        string SupplierName { get; set; }
        string ApproverName { get; set; }
        int CostCentreID { get; set; }
        decimal amount { get; set; }
        string currencytypeName { get; set; }
        int currencytype { get; set; }
        string strAppprovalDate { get; set; }
        string strSendDate { get; set; }
        string strCreatedDate { get; set; }
        string strAmount { get; set; }
        string strStatusName { get; set; }
        dynamic purchaseOrderDetail { get; set; }
        bool isApproveSendAuthority { get; set; }
        IList<IFinancialMetadataAttributewithValues> DynamciData { get; set; }

        #endregion
    }
}
