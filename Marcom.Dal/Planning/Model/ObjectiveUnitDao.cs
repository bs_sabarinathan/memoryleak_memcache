using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// ObjectiveUnitDao object for table 'PM_Objective_Unit'.
	/// </summary>
	
	public partial class ObjectiveUnitDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected string _caption;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public ObjectiveUnitDao() {}
			
		public ObjectiveUnitDao(int pId, string pCaption)
		{
			this._id = pId; 
			this._caption = pCaption; 
		}
				
		public ObjectiveUnitDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
			  _bIsChanged |= (_caption != value); 
			  _caption = value; 
			}
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string ObjectiveUnitDao = "ObjectiveUnitDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string ObjectiveUnitDao = "ObjectiveUnitDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";			
		}

		#endregion
		
		
	}
	
}
