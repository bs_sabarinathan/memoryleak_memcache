///#source 1 1 /app/controllers/mui/planningtool/default/detail/section/member-controller.js
(function (ng, app) {
    "use strict"; function muiplanningtooldefaultdetailmemberCtrl($scope, $timeout, $window, $http, $compile, $resource, $stateParams, $translate, MemberService) {
        function sortOn(collection, name) {
            collection.sort(function (a, b) {
                if (a[name] <= b[name]) { return (-1); }
                return (1);
            });
        }
        function GetLevelfromUniqueKey(UniqueKey) { var substr = UniqueKey.split('.'); return substr.length; }
        $scope.groupBy = function (attribute, ISglobal) {
            $scope.groups = []; var GroupEntityID = "_Inavlid_EntityID"; var groupValue = "_INVALID_GROUP_VALUE_"; if (ISglobal == false) { sortOn($scope.memberList, attribute); } else { sortOn($scope.memberList, "InheritedFromEntityid"); sortOn($scope.memberList, attribute); }
            for (var i = 0, friend; friend = $scope.memberList[i++];) {
                if (ISglobal == false) {
                    $scope.ShowMemberEntityHeader = ISglobal; if (friend["IsInherited"] === false) {
                        if (friend["InheritedFromEntityid"] !== GroupEntityID) { var NewEntityGroup = { Name: $scope.DecodedTextName(friend.InheritedFromEntityName), ColorCode: friend.ColorCode, ShortText: friend.ShortText, Groups: [] }; GroupEntityID = friend["InheritedFromEntityid"]; $scope.groups.push(NewEntityGroup); }
                        if (GroupEntityID == friend["InheritedFromEntityid"]) {
                            if (friend[attribute] !== groupValue) { var group = { label: friend[attribute], friends: [], rolename: friend.Role }; groupValue = group.label; NewEntityGroup.Groups.push(group); }
                            group.friends.push(friend);
                        }
                    }
                } else if (ISglobal == true) {
                    $scope.ShowMemberEntityHeader = ISglobal; if (friend["InheritedFromEntityid"] !== GroupEntityID) { var NewEntityGroup = { Name: $scope.DecodedTextName(friend.InheritedFromEntityName), ColorCode: friend.ColorCode, ShortText: friend.ShortText, Groups: [] }; GroupEntityID = friend["InheritedFromEntityid"]; $scope.groups.push(NewEntityGroup); }
                    if (GroupEntityID == friend["InheritedFromEntityid"]) {
                        if (friend[attribute] !== groupValue) { var group = { label: friend[attribute], friends: [], rolename: friend.Role }; groupValue = group.label; NewEntityGroup.Groups.push(group) }
                        group.friends.push(friend);
                    }
                }
            }
        }; $scope.UserimageNewTime = new Date().getTime().toString(); $scope.memberList = []; $scope.groups = []; $scope.$on('LoadMembersDetail', function (event, ID) { $scope.EntityID = ID; $scope.load(); $scope.GlobalAccessStatus = { GlobalText: "Show global access", GlobalStatus: 0 }; }); $scope.AutoCompleteSelectedObj = []; $scope.EntityID = $stateParams.ID; $scope.IsInherited = true
        $scope.InheritedFromEntityid = $stateParams.ID; $scope.UserLists = {}; $scope.Roles = {}; $scope.userCanAddProofInitiator = false; $scope.Checkroles = function (Roleid) { if (Roleid == EntityRoles.ProofInitiator) { if ($scope.loggedinUserRoles != null) { if ($scope.loggedinUserRoles.length > 0) return true; else return false; } else return false; } else { return true; } }
        $scope.AutoCompleteSelectedUserObj = {"UserSelection":[]};
        $scope.load = function (parameters) {
            $scope.AutoCompleteSelectedObj = []; $scope.EntityID = $stateParams.ID; $scope.IsInherited = true
            $scope.InheritedFromEntityid = $stateParams.ID; $scope.UserLists = {}; $scope.Roles = {}; $scope.userid = parseInt($.cookie('UserId'), 10); MemberService.GetMember($scope.EntityID).then(function (member) { var resp = member.Response; $scope.memberList = member.Response; $scope.loggedinUserDets = $.grep($scope.memberList, function (e) { return e.Userid == $scope.userid }); $scope.loggedinUserRoles = $.grep($scope.loggedinUserDets, function (e) { return e.BackendRoleID == EntityRoles.Owner || e.BackendRoleID == EntityRoles.Editer }); $scope.groupBy('Roleid', false); $scope.QuickInfo1AttributeCaption = $scope.memberList[0].QuickInfo1AttributeCaption; $scope.QuickInfo2AttributeCaption = $scope.memberList[0].QuickInfo2AttributeCaption; MemberService.GetUsers($scope.EntityID).then(function (UserbyNamelist) { $scope.UserLists = UserbyNamelist.Response; }); }); MemberService.GetEntityDetailsByID($scope.EntityID).then(function (GetEntityresult) { if (GetEntityresult.Response != null) { var levelid = GetEntityresult.Response.Level; $scope.ShowGlobalAccess = levelid > 0 ? true : false; MemberService.GetEntityTypeRoleAccess(GetEntityresult.Response.Typeid).then(function (role) { $scope.Roles = role.Response; }); } });
        }; $scope.ddlrole = '';
        $scope.addMember = function () {
            $timeout(function () { $('#ddluser').focus(); }, 1000)
            $("#memberModal").modal('show');
        };
        $scope.addEntityMember = function () {
            try {
                if ($scope.ddlrole == '' || $scope.ddlrole.length == 0) { bootbox.alert($translate.instant('LanguageContents.Res_1906.Caption')); $('#addentitymemberID').removeClass('disabled'); return false; } else { $('#addentitymemberID').addClass('disabled'); }
                var userval = $('#ddluser').val(); if (userval.length == 0) { bootbox.alert("Please enter the user name"); $('#addentitymemberID').removeClass('disabled'); return false; }
                var membervalues = $.grep($scope.Roles, function (e) { return e.ID == parseInt($scope.ddlrole, 10) })[0]; var InsertMember = {}; InsertMember.EntityID = $scope.EntityID; InsertMember.RoleID = membervalues.ID; InsertMember.Assignee = parseInt($scope.AutoCompleteSelectedObj[0].Id, 10); InsertMember.IsInherited = false; InsertMember.InheritedFromEntityid = 0; var result = $.grep($scope.memberList, function (e) { return (e.Userid == parseInt($scope.AutoCompleteSelectedObj[0].Id, 10) && e.Roleid == membervalues.ID) && (e.IsInherited == false); }); if (result.length == 0) {
                    MemberService.PostMember(InsertMember).then(function (insertmember) {
                        MemberService.GetMember($scope.EntityID).then(function (member) {
                            var resp = member.Response; $scope.memberList = member.Response; var GlobalAccessToshow = ($scope.GlobalAccessStatus.GlobalStatus == 1 ? true : false)
                            $scope.groupBy('Roleid', GlobalAccessToshow);
                        }); $('#ddlrole').select2('val', ''); $scope.ddlrole = ''; $('#addentitymemberID').removeClass('disabled'); $scope.ddlUser = ''; $scope.AutoCompleteSelectedObj = []; NotifySuccess($translate.instant('LanguageContents.Res_4479.Caption'));
                    });
                } else { $('#ddlrole').select2('val', ''); $scope.ddlrole = ''; $scope.ddlUser = ''; $scope.AutoCompleteSelectedObj = []; bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption')); $('#addentitymemberID').removeClass('disabled'); }
            } catch (e) { $('#addentitymemberID').removeClass('disabled'); }
        }; $scope.updateEntityMember = function (ID) { var updateMembersinfo = {}; updateMembersinfo.ID = ID; updateMembersinfo.EntityID = $scope.EntityID; updateMembersinfo.RoleID = $scope.ddlrole; updateMembersinfo.Assignee = parseInt($scope.AutoCompleteSelectedObj[0].Id, 10); updateMembersinfo.IsInherited = $scope.IsInherited; updateMembersinfo.InheritedFromEntityid = $scope.InheritedFromEntityid; MemberService.UpdateMemberInEntity(updateMembersinfo).then(function (updatemember) { }); }; $scope.deleteEntityMember = function (ID, Name) {
            if ($scope.IsLock == true) { return false; }
            bootbox.confirm($translate.instant('LanguageContents.Res_1851.Caption') + " " + Name + " ?", function (result) {
                if (result) {
                    $timeout(function () {
                        MemberService.Member(ID).then(function (deletemember) {
                            MemberService.GetMember($scope.EntityID).then(function (member) {
                                var resp = member.Response; $scope.memberList = member.Response; var GlobalAccessToshow = ($scope.GlobalAccessStatus.GlobalStatus == 1 ? true : false)
                                $scope.groupBy('Roleid', GlobalAccessToshow);
                            });
                        });
                    }, 100);
                }
            });
        }; $scope.GlobalAccessStatus = { GlobalText: "Show global access", GlobalStatus: 0 }; $scope.toggleAutoUserAccess = function () { if ($scope.GlobalAccessStatus.GlobalStatus == 0) { $scope.GlobalAccessStatus.GlobalStatus = 1; $scope.GlobalAccessStatus.GlobalText = "Hide global access"; $scope.groupBy('Roleid', true); } else { $scope.GlobalAccessStatus.GlobalStatus = 0; $scope.GlobalAccessStatus.GlobalText = "Show global access"; $scope.groupBy('Roleid', false); } }
        $scope.CloseAddmember = function () { $('#ddlrole').select2('val', ''); $scope.ddlrole = ''; $('#addentitymemberID').removeClass('disabled'); $scope.ddlUser = ''; $scope.AutoCompleteSelectedObj = []; }
        $scope.$on("$destroy", function () { RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.detail.memberCtrl']")); }); $scope.MemberAddIsLock = true; $timeout(function () { $scope.load(); }, 0); $scope.set_color = function (clr) { if (clr != null) return { 'background-color': "#" + clr.toString().trim() }; else return ''; }
    }
    app.controller("mui.planningtool.default.detail.memberCtrl", ['$scope', '$timeout', '$window', '$http', '$compile', '$resource', '$stateParams', '$translate', 'MemberService', muiplanningtooldefaultdetailmemberCtrl]);
})(angular, app);
///#source 1 1 /app/services/member-service.js
(function(ng,app){"use strict";function MemberService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetUsers:GetUsers,GetEntityTypeRoleAccess:GetEntityTypeRoleAccess,GetMember:GetMember,GetEntityDetailsByID:GetEntityDetailsByID,PostMember:PostMember,UpdateMemberInEntity:UpdateMemberInEntity,Member:Member,PutMember:PutMember});function GetUsers(){var request=$http({method:"get",url:"api/user/GetUsers/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeRoleAccess(EntityTypeID){var request=$http({method:"get",url:"api/access/GetEntityTypeRoleAccess/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetMember(EntityID){var request=$http({method:"get",url:"api/Planning/GetMember/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityDetailsByID(EntityID){var request=$http({method:"get",url:"api/Planning/GetEntityDetailsByID/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function PostMember(dataobj){var request=$http({method:"post",url:"api/Planning/PostMember/",params:{action:"add"},data:dataobj});return(request.then(handleSuccess,handleError));}
function UpdateMemberInEntity(dataobj){var request=$http({method:"put",url:"api/Planning/UpdateMemberInEntity/",params:{action:"update"},data:dataobj});return(request.then(handleSuccess,handleError));}
function Member(ID){var request=$http({method:"delete",url:"api/Planning/Member/"+ID,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function PutMember(dataobj){var request=$http({method:"put",url:"api/Planning/PutMember/",params:{action:"update"},data:formobj});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("MemberService",['$http','$q',MemberService]);})(angular,app);
