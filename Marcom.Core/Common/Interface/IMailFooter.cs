using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IMailFooter interface for table 'CM_MailFooter'.
    /// </summary>
    public interface IMailFooter
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        
        string Body
        {
            get;
            set;

        }

        string description
        {
            get;
            set;

        }
        

        #endregion
    }
}
