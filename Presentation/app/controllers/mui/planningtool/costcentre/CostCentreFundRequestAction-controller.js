﻿(function (ng, app) {
    "use strict";
    function muiplanningtoolcostcentreCostCentreFundRequestActionCtrl($scope, $location, $resource, $timeout, $stateParams, $cookies, $window, $compile, $translate, CCfundingrequestService, $modalInstance, params) {
        var NewsFeedUniqueTimerForCostcentre = null;
        $scope.temptaskDiv = "";
        $scope.tempTaskID = 0;
        $scope.globalAttachment = true;
        $scope.fundReqIDforFeedforCost = 0;
        $scope.listPredfWorkflowFilesAttch = [];

        $scope.CCFundRegObj = {
            "OwnerName": $cookies['Username'],
            "OwnerID": $cookies['UserId'],
            "ownerEmail": $cookies['UserEmail']
        };

        $scope.OwnerList = [];
        $scope.DefaultImageSettings = {
            'ImageSpan': new Date.create().getTime().toString(),
        };
        $scope.OwnerList.push({
            "Roleid": 1,
            "RoleName": "Owner",
            "UserEmail": $scope.CCFundRegObj.OwnerName,
            "DepartmentName": "-",
            "Title": "-",
            "Userid": parseInt($scope.CCFundRegObj.OwnerID, 10),
            "UserName": $scope.CCFundRegObj.OwnerName,
            "IsInherited": '0',
            "InheritedFromEntityid": '0'
        });
        $scope.groups = [];
        $scope.UserimageNewTime = new Date().getTime().toString();
        $scope.CCEntityID = 0;
        $scope.GetEntityMembers = function () {
            CCfundingrequestService.GetMember(params.EntityID).then(function (member) {
                $scope.PersonalUserIds = [];
                $scope.EntityUserList = [];
                var IsUnique = true;
                if (member.Response != null) {
                    $.each(member.Response, function (index, value) {
                        if (value.IsInherited == false) {
                            IsUnique = true;
                            if ($scope.EntityUserList.length > 0) {
                                $.each($scope.EntityUserList, function (i, v) {
                                    if (v.Userid == value.Userid) {
                                        IsUnique = false;
                                    }
                                });
                                if (IsUnique == true) $scope.EntityUserList.push(value);
                            } else $scope.EntityUserList.push(value);
                        }
                    });
                }
            });
        }
        $scope.CurrentStepID = 0;
        $scope.CurrentTaskName = "";
        $scope.CurrentTaskType = 0;
        $scope.CurrentPrdefinedTaskID = 0;
        $scope.TaskByIDDetail = [];
        $scope.TaskBriefDetails = {
            taskID: 0,
            taskTypeId: 0,
            taskTypeName: "",
            EntityID: 0,
            ownerId: 0,
            stepID: 0,
            taskName: "",
            dueIn: 0,
            dueDate: "",
            status: "",
            statusID: 0,
            taskOwner: "",
            taskOwnerFundRequest: "",
            Description: "",
            taskmembersList: [],
            taskAttachmentsList: [],
            CostCentreName: "",
            InRequests: 0,
            PreviousAllApproved: 0,
            TotalApprovedForthis: 0,
            CCAllocatedAmount: 0,
            CCAvailable: 0,
            AfterApproval: 0,
            TotalEntittApproveAmount: 0,
            IsCCOwner: false,
            RequestDate: ""
        };
        $scope.EntityUniqueidToReturn = "";
        $scope.ReturnStatus = 0;
        $scope.FromEntityName = "";
        $scope.FromEntityTypeName = "";
        $("#feedFundingRequestModal").on("onCostCentreFundingRequestsAction", function (event, cid, entityUniqueKey, UniquekeyToBindStatus, EntityName, EntityType) {
            if (NewsFeedUniqueTimerForCostcentre != undefined) $timeout.cancel(NewsFeedUniqueTimerForCostcentre);
            $('#WorkTaskModal').on('hide.bs.modal', function () {
                if (NewsFeedUniqueTimerForCostcentre != undefined) $timeout.cancel(NewsFeedUniqueTimerForCostcentre);
            });
            $scope.FromEntityName = EntityName;
            $scope.FromEntityTypeName = EntityType;
            $scope.EntityUniqueidToReturn = UniquekeyToBindStatus;
            CCfundingrequestService.GetFundRequestTaskDetails(entityUniqueKey, cid).then(function (TaskDetailList) {
                $scope.TaskByIDDetail = TaskDetailList.Response;
                $scope.tempTaskID = parseInt(TaskDetailList.Response[0].Id);
                $scope.fundReqIDforFeedforCost = parseInt(TaskDetailList.Response[0].Id);
                feedforfundingrequest(parseInt(TaskDetailList.Response[0].Id), false);
                BindTaskDetails($scope.TaskByIDDetail, 7, "Funding Request");
            });
            var commentbuttinid = "costcentrefundingreqfeedcomment";
            $('#' + commentbuttinid).empty();
            var temp = '';
            if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
                temp = 'costcentrefeedtextholder';
                document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
            }
        });
        $("#feedFundingRequestModal").on("onNewsfeedCostCentreFundingRequestsAction", function (event, FundReqID) {
            LoadFinancialDetailPopup(FundReqID);
        });
        function OnNewsfeedCostCentreFundingRequestsAction(FundReqID) {
            $timeout(function () {
                LoadFinancialDetailPopup(FundReqID);
            }, 100);
        };
        OnNewsfeedCostCentreFundingRequestsAction(params.ID);
        $("#feedFundingRequestModal").on("TopMyTaskCostCentreFundingRequestsAction", function (event, FundReqID) {
            LoadFinancialDetailPopup(FundReqID);
        });

        function LoadFinancialDetailPopup(FundReqID) {
            if (NewsFeedUniqueTimerForCostcentre != undefined) $timeout.cancel(NewsFeedUniqueTimerForCostcentre);
            $('#feedFundingRequestModal').on('hide.bs.modal', function () {
                if (NewsFeedUniqueTimerForCostcentre != undefined) $timeout.cancel(NewsFeedUniqueTimerForCostcentre);
            });
            $scope.FromEntityName = "";
            $scope.FromEntityTypeName = "";
            CCfundingrequestService.GetNewsfeedFundRequestTaskDetails(FundReqID).then(function (TaskDetailList) {
                $scope.TaskByIDDetail = TaskDetailList.Response;
                $scope.tempTaskID = parseInt(TaskDetailList.Response[0].Id);
                $scope.fundReqIDforFeedforCost = parseInt(TaskDetailList.Response[0].Id);
                feedforfundingrequest(parseInt(TaskDetailList.Response[0].Id), false);
                $scope.CCEntityID = parseInt(TaskDetailList.Response[0].EntityId);
                params.EntityID = $scope.CCEntityID;
                $scope.GetEntityMembers();
                CCfundingrequestService.GetLockStatus(FundReqID).then(function (entitylockstatus) {
                    $scope.IsLock = entitylockstatus.Response.m_Item1;
                    //$timeout(function () {
                    BindTaskDetails($scope.TaskByIDDetail, 7, "Funding Request");
                    //  }, 10);
                });
            });
            var commentbuttinid = "costcentrefundingreqfeedcomment";
            $('#' + commentbuttinid).empty();
            var temp = '';
            if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
                temp = 'costcentrefeedtextholder';
                document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
            }
            // $timeout(function () {
            $scope.TimerforcallbackForFundReqCost(FundReqID, true);
            // }, 30000);
        }

        function refreshModel() {
            $scope.TaskBriefDetails = {
                taskID: 0,
                taskTypeId: 0,
                taskTypeName: "",
                EntityID: 0,
                ownerId: 0,
                stepID: 0,
                taskName: "",
                dueIn: 0,
                dueDate: "",
                status: "",
                statusID: 0,
                taskOwner: "",
                taskOwnerFundRequest: "",
                Description: "",
                taskmembersList: [],
                taskAttachmentsList: []
            };
        }
        $scope.TaskMemberList = [];
        $scope.ShowCompleteBtn = false;
        $scope.ShowApproveBtn = false;
        $scope.ShowRejectedBtn = false;
        $scope.ShowWithdrawBtn = false;
        $scope.ShowUnabletoCompleteBtn = false;
        $scope.attachedFiles = [];

        function BindTaskDetails(TaskObject, taskTypeId, taskTypeName) {
            $scope.ShowCompleteBtn = false;
            $scope.ShowApproveBtn = false;
            $scope.ShowRejectedBtn = false;
            $scope.ShowWithdrawBtn = false;
            $scope.ShowUnabletoCompleteBtn = false;
            $scope.TaskBriefDetails.IsCCOwner = false;
            for (var i = 0; i < TaskObject.length; i++) {
                $scope.listPredfWorkflowFilesAttch = TaskObject[i].TaskAttachment;
                var isThisMemberPresent = $.grep(TaskObject[i].TaskMembers, function (e) {
                    return (e.Userid == parseInt($cookies['UserId']) && e.Roleid == 4);
                });
                if (taskTypeId == 2) {
                    if (TaskObject[i].Status == 8 && isThisMemberPresent.length === 1) {
                        if (isThisMemberPresent[0].Roleid == 1) {
                            $scope.ShowWithdrawBtn = true;
                        } else {
                            $scope.ShowCompleteBtn = true;
                            $scope.ShowUnabletoCompleteBtn = true;
                        }
                    } else if (TaskObject[i].Status == 8 && isThisMemberPresent.length > 1) {
                        $scope.ShowCompleteBtn = true;
                        $scope.ShowWithdrawBtn = true;
                    } else if (TaskObject[i].Status == 8 && isThisMemberPresent.length > 1) {
                        $scope.ShowCompleteBtn = true;
                        $scope.ShowWithdrawBtn = true;
                    }
                } else if (taskTypeId == 3) {
                    if (TaskObject[i].Status == 8 && isThisMemberPresent.length === 1) {
                        if (isThisMemberPresent[0].Roleid == 1) {
                            $scope.ShowWithdrawBtn = true;
                        } else {
                            if ($scope.IsLock != true) {
                                $scope.ShowApproveBtn = true;
                                $scope.ShowRejectedBtn = true;
                            }
                        }
                    } else if (TaskObject[i].Status == 8 && isThisMemberPresent.length > 1) {
                        if ($scope.IsLock != true) {
                            $scope.ShowApproveBtn = true;
                            $scope.ShowWithdrawBtn = true;
                        }
                    }
                } else if (taskTypeId == 7) {
                    if (TaskObject[i].Status == 8 && isThisMemberPresent.length === 1) {
                        if (isThisMemberPresent[0].Roleid == 1) {
                            $scope.ShowWithdrawBtn = false;
                            if ($scope.IsLock != true) {
                                $scope.ShowApproveBtn = true;
                                $scope.ShowRejectedBtn = true;
                            }
                        } else {
                            if ($scope.IsLock != true) {
                                $scope.ShowApproveBtn = true;
                                $scope.ShowRejectedBtn = true;
                            }
                        }
                    } else if (TaskObject[i].Status == 8 && isThisMemberPresent.length > 1) {
                        if ($scope.IsLock != true) {
                            $scope.ShowApproveBtn = true;
                            $scope.ShowRejectedBtn = true;
                            $scope.ShowWithdrawBtn = false;
                        }
                    }
                }
                $scope.FromEntityName = TaskObject[i].ParentEntityName;
                $scope.FromEntityTypeName = TaskObject[i].ParentEntityTypeName;
                $scope.TaskBriefDetails.taskName = TaskObject[i].Name;
                $scope.TaskBriefDetails.taskTypeId = taskTypeId;
                $scope.TaskBriefDetails.taskTypeName = taskTypeName;
                var dateval = "";
                var datStartUTCval = "";
                var datstartval = "";
                if (TaskObject[i].DueDate != undefined) dateval = TaskObject[i].DueDate;
                datStartUTCval = dateval.substr(0, 10);
                datstartval = new Date(datStartUTCval);
                $scope.TaskBriefDetails.dueDate = dateFormat(datstartval, $scope.DefaultSettings.DateFormat);
                $scope.TaskBriefDetails.dueIn = TaskObject[i].Duedates;
                $scope.TaskBriefDetails.taskOwner = TaskObject[i].TaskOwnerName;
                if (TaskObject[i].TaskMembers.length > 0 && TaskObject[i].TaskMembers != null) {
                    var res = $.grep(TaskObject[i].TaskMembers, function (e) {
                        return e.Roleid == 1;
                    });
                    if (res.length > 0) {
                        $scope.TaskBriefDetails.taskOwnerFundRequest = res[0].UserName;
                        $scope.TaskBriefDetails.ownerId = res[0].Userid;
                    } else $scope.TaskBriefDetails.taskOwnerFundRequest = TaskObject[i].TaskOwnerName.split(",")[0];
                } else $scope.TaskBriefDetails.taskOwnerFundRequest = TaskObject[i].TaskOwnerName.split(",")[0];
                $scope.TaskBriefDetails.status = TaskObject[i].TaskStatusName;
                $scope.TaskBriefDetails.statusID = TaskObject[i].Status;
                $scope.TaskBriefDetails.taskID = TaskObject[i].Id;
                if (TaskObject[i].MemberID != 0) $scope.TaskBriefDetails.ownerId = TaskObject[i].MemberID;
                $scope.TaskBriefDetails.EntityID = TaskObject[i].EntityId;
                $scope.TaskBriefDetails.stepID = TaskObject[i].StepID;
                if (TaskObject[i].fundrequestTask[i] != undefined) {
                    $scope.TaskBriefDetails.Description = TaskObject[i].fundrequestTask[i].Description;
                    $scope.TaskBriefDetails.InRequests = parseInt(TaskObject[i].fundrequestTask[i].RequestAmount, 10).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    $scope.TaskBriefDetails.RequestDate = dateFormat(TaskObject[i].fundrequestTask[i].RequestDate, $scope.DefaultSettings.DateFormat);
                    $scope.TaskBriefDetails.CostCentreName = TaskObject[i].fundrequestTask[i].CostCenterName;
                    $scope.TaskBriefDetails.PreviousAllApproved = parseInt(TaskObject[i].fundrequestTask[i].CCPreviousAllApprovedAmount, 10).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    $scope.TaskBriefDetails.TotalApprovedForthis = parseInt(TaskObject[i].fundrequestTask[i].CCTotalApprovedAmountThisLevel, 10).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    $scope.TaskBriefDetails.CCAllocatedAmount = parseInt(TaskObject[i].fundrequestTask[i].CCAssignedAmount, 10).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    $scope.TaskBriefDetails.CCAvailable = parseInt(TaskObject[i].fundrequestTask[i].CCAvailableAmount, 10).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    $scope.TaskBriefDetails.AfterApproval = (parseInt(TaskObject[i].fundrequestTask[i].CCAvailableAmount, 10) - parseInt(TaskObject[i].fundrequestTask[i].RequestAmount, 10)).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    $scope.TaskBriefDetails.TotalEntittApproveAmount = (parseInt(TaskObject[i].fundrequestTask[i].CCTotalApprovedAmountThisLevel, 10) + parseInt(TaskObject[i].fundrequestTask[i].RequestAmount, 10)).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    //  $scope.TaskBriefDetails.IsCCOwner = false;
                    $scope.TaskBriefDetails.IsCCOwner = $scope.ShowApproveBtn;
                }
                var membersWithoutOwner = $.grep(TaskObject[i].TaskMembers, function (e) {
                    return e.Roleid != 1;
                });
                $scope.TaskBriefDetails.taskmembersList = membersWithoutOwner;
                $scope.TaskMemberList = TaskObject[i].TaskMembers;
                $scope.groupBy('Roleid', false);
                SeperateUsers();
            }
        }
        $scope.Activemembers = [];

        function SeperateUsers() {
            var MemberList = $.grep($scope.TaskMemberList, function (e) {
                return e.Roleid != 1;
            });
            var UniqueTaskMembers = [];
            var dupes = {};
            $.each(MemberList, function (i, el) {
                if (!dupes[el.Userid]) {
                    dupes[el.Userid] = true;
                    UniqueTaskMembers.push(el);
                }
            });
            var UniqueEntityMembers = [];
            dupes = {};
            if ($scope.EntityMemberList != undefined) {
                $.each($scope.EntityMemberList, function (i, el) {
                    if (el.IsInherited != true) {
                        if (!dupes[el.Userid]) {
                            dupes[el.Userid] = true;
                            UniqueEntityMembers.push(el);
                        }
                    }
                });
            }
            $scope.RemainMembers = [];
            $.each(UniqueEntityMembers, function (key, value) {
                var MemberList = $.grep(UniqueTaskMembers, function (e) {
                    return e.Userid == value.Userid;
                });
                if (MemberList.length == 0) {
                    $scope.RemainMembers.push(value);
                }
            });
        }

        function GetUserSelectedAll() {
            var IDList = [];
            $('#AdditionalTaskMembers  > tbody input:checked').each(function () {
                IDList.push({
                    "Userid": ($(this).attr('data-userid')),
                    "Id": ($(this).attr('data-id')),
                    "TaskID": ($(this).attr('data-taskid')),
                    "Roleid": 4,
                    "UserName": ($(this).attr('data-username')),
                    "UserEmail": ($(this).attr('data-UserEmail')),
                    "DepartmentName": ($(this).attr('data-departmentname')),
                    "Role": ($(this).attr('data-role')),
                    "Title": ($(this).attr('data-title'))
                });
            });
            return IDList
        }
        $scope.AddTaskAdditionalMembers = function () {
            var memberList = [];
            var SaveTask = {};
            memberList = GetUserSelectedAll();
            if (memberList.length > 0) {
                for (var i = 0; i < memberList.length; i++) {
                    $scope.TaskMemberList.push(memberList[i]);
                }
                var SaveTask = {};
                SaveTask.ParentId = params.EntityID;
                SaveTask.TaskID = $scope.TaskBriefDetails.taskID;
                SaveTask.TaskMembers = memberList;
                CCfundingrequestService.InsertTaskMembers(SaveTask).then(function (SaveTaskResult) {
                    if (SaveTaskResult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4820.Caption'));
                        $scope.groupBy('Roleid', false);
                        SeperateUsers();
                        $('#AdditionalTaskMembers > tbody input:checked').each(function () {
                            $(this).next('i').removeClass('checked');
                        });
                        $('#AddTaskMemberModal').modal('hide');
                    }
                });
            }
        }

        function sortOn(collection, name) {
            collection.sort(function (a, b) {
                if (a[name] <= b[name]) {
                    return (-1);
                }
                return (1);
            });
        }
        $scope.groupBy = function (attribute, ISglobal) {
            $scope.TaskMembergroups = [];
            sortOn($scope.TaskMemberList, attribute);
            var groupValue = "_INVALID_GROUP_VALUE_";
            for (var i = 0; i < $scope.TaskMemberList.length; i++) {
                var friend = $scope.TaskMemberList[i];
                if (friend[attribute] !== groupValue) {
                    var group = {
                        label: friend[attribute],
                        friends: [],
                        rolename: friend.Role == "Owner" ? "Task Owner" : "Task Assignee"
                    };
                    groupValue = group.label;
                    $scope.TaskMembergroups.push(group);
                }
                group.friends.push(friend);
            }
        };
        $scope.TaskMembergroups = [];

        $scope.$on("$destroy", function () {
            $timeout.cancel(NewsFeedUniqueTimerForCostcentre);
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.CostCentreFundRequestActionCtrl']"));
        });
        $scope.addAditionalattachments = function () {
            $scope.globalAttachment = false;
            $scope.fnTimeOut();
        }
        $(window).on("onMakeStatus", function (event, step) {
            CCfundingrequestService.UpdateEntityActiveStatus(parseInt(params.EntityID, 10), step).then(function (TaskStatusData) {
                if (TaskStatusData.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                    cacheReault();
                }
            });
        });
        var entid = 0;
        var divid = '';
        $scope.Fund_newsfeed_TEMP = [];
        var divtypeid = '';
        var fundrequestID = 0;
        $scope.userimgsrc = '';
        $scope.commentuserimgsrc = '';

        function feedforfundingrequest(entityid, isLatestnewsfeed) {
            var tempPageno = 0;
            if (isLatestnewsfeed == false) {
                divid = 'fundingrequestfeeddivFinReq';
                $('#' + divid).html('');
                PagenoforScroll = 0;
                $scope.temptaskDiv = divid;
                $scope.TaskFeedReloadOnScroll();
                tempPageno = 0;
                entid = entityid;
            } else {
                tempPageno = -1;
            }
            fundrequestID = entityid;
            divtypeid = divid;
            try {
                CCfundingrequestService.GetEnityFeedsForFundingReq(entityid, tempPageno, isLatestnewsfeed).then(function (getFundingreqNewsFeedResult) {
                    var fundingreqfundingreqfeeddivHtml = '';
                    $scope.Fund_newsfeed_TEMP = [];
                    if (!isLatestnewsfeed) {
                        $scope.Fund_newsfeed_TEMP.push(getFundingreqNewsFeedResult.Response);
                    } else {
                        if (getFundingreqNewsFeedResult.Response != null) $scope.Fund_newsfeed_TEMP.push(getFundingreqNewsFeedResult.Response);
                    }
                    if (getFundingreqNewsFeedResult.Response != null) {
                        for (var i = 0; i < getFundingreqNewsFeedResult.Response.length; i++) {
                            var feedcomCount = getFundingreqNewsFeedResult.Response[i].FeedComment != null ? getFundingreqNewsFeedResult.Response[i].FeedComment.length : 0;
                            var fundingreqfeeddivHtml = '';
                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li data-parent="NewsParent" data-ID=' + getFundingreqNewsFeedResult.Response[i].FeedId + '>';
                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"> <img src=Handlers/UserImage.ashx?id=' + getFundingreqNewsFeedResult.Response[i].Actor + '&time=' + $scope.NewTime + ' alt="Avatar"></div>';
                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getFundingreqNewsFeedResult.Response[i].UserEmail + '>' + getFundingreqNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + getFundingreqNewsFeedResult.Response[i].FeedText + '</p></div>';
                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getFundingreqNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedId + '" id="Comment_' + getFundingreqNewsFeedResult.Response[i].FeedId + '" >Comment</a></span>';
                            if (feedcomCount > 1) {
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedComment + '"   id="' + getFundingreqNewsFeedResult.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                            } else {
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedComment + '"   id="' + getFundingreqNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                            }
                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul class="subComment">';
                            if (getFundingreqNewsFeedResult.Response[i].FeedComment != null && getFundingreqNewsFeedResult.Response[i].FeedComment != '') {
                                var j = 0;
                                if (getFundingreqNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                    $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                } else {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getFundingreqNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                }
                                for (var k = 0; k < getFundingreqNewsFeedResult.Response[i].FeedComment.length; k++) {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getFundingreqNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    if (k == 0) {
                                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li';
                                    } else {
                                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li style = "display:none;"';
                                    }
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + ' id="feedcomment_' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt">';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></div></li>';
                                }
                            }
                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</ul></li>';
                            $('#' + divid + ' [data-id]').each(function () {
                                if (parseInt($(this).attr('data-id')) == getFundingreqNewsFeedResult.Response[i].FeedId) {
                                    $(this).remove();
                                }
                            });
                            if (isLatestnewsfeed == false) {
                                $('#' + divid).append(fundingreqfeeddivHtml);
                                $('#' + divid).scrollTop(0);
                            } else {
                                $('#' + divid).prepend(fundingreqfeeddivHtml);
                            }
                        }
                    }
                });
            } catch (e) { }
        }
        $scope.TimerforcallbackForFundReqCost = function (entityid, stat) {
            feedforfundingrequest(parseInt(entityid), true);
            NewsFeedUniqueTimerForCostcentre = $timeout(function () {
                $scope.TimerforcallbackForFundReqCost(parseInt(entityid), true);
            }, 30000);
        }
        var PagenoforScroll = 0;
        $scope.TaskFeedReloadOnScroll = function () {
            try {
                $('#' + $scope.temptaskDiv).scroll(function () {
                    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                        PagenoforScroll += 2;
                        CCfundingrequestService.GetEnityFeedsForFundingReq($scope.tempTaskID, PagenoforScroll, false).then(function (latestNewsFeedResult) {
                            var fundingreqfundingreqfeeddivHtml = '';
                            if (latestNewsFeedResult.Response.length > 0) {
                                for (var i = 0; i < latestNewsFeedResult.Response.length; i++) {
                                    var feedcomCount = latestNewsFeedResult.Response[i].FeedComment != null ? latestNewsFeedResult.Response[i].FeedComment.length : 0;
                                    var fundingreqfeeddivHtml = '';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li data-parent="NewsParent" data-ID=' + latestNewsFeedResult.Response[i].FeedId + '>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"> <img src=Handlers/UserImage.ashx?id=' + latestNewsFeedResult.Response[i].Actor + '&time=' + $scope.NewTime + ' alt="Avatar"></div>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + latestNewsFeedResult.Response[i].UserEmail + '>' + latestNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + latestNewsFeedResult.Response[i].FeedText + '</p></div>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + latestNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML"  data-commnetid="' + latestNewsFeedResult.Response[i].FeedId + '" id="Comment_' + latestNewsFeedResult.Response[i].FeedId + '" >Comment</a></span>';
                                    if (feedcomCount > 1) {
                                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + latestNewsFeedResult.Response[i].FeedComment + '"   id="' + latestNewsFeedResult.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                                    } else {
                                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="CommentshowHTML"  data-commnetid="' + latestNewsFeedResult.Response[i].FeedComment + '"   id="' + latestNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                                    }
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul class="subComment">';
                                    if (latestNewsFeedResult.Response[i].FeedComment != null && latestNewsFeedResult.Response[i].FeedComment != '') {
                                        var j = 0;
                                        if (latestNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                            $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                        } else {
                                            $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + latestNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                        }
                                        for (var k = 0; k < latestNewsFeedResult.Response[i].FeedComment.length; k++) {
                                            $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + latestNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                            if (k == 0) {
                                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li';
                                            } else {
                                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li style = "display:none;"';
                                            }
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + ' id="feedcomment_' + latestNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt">';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + latestNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + latestNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + latestNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + latestNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></div></li>';
                                        }
                                    }
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</ul></li>';
                                    $('#' + $scope.temptaskDiv).append(fundingreqfeeddivHtml);
                                }
                            } else PagenoforScroll -= 2;
                        });
                    }
                });
            } catch (e) { }
        }
        $scope.AddfundingreqNewsFeed = function () {
            if (entid != 0) {
                if ($("#costcentrefeedtextholder").text() != "") {
                    return false;
                }
                var addnewsfeed = {};
                addnewsfeed.Actor = parseInt($cookies['UserId'], 10);
                addnewsfeed.TemplateID = 40;
                addnewsfeed.EntityID = parseInt(entid);
                addnewsfeed.TypeName = "fundingrequest";
                addnewsfeed.AttributeName = "";
                addnewsfeed.FromValue = "";
                addnewsfeed.ToValue = $('#costcentrefundingreqfeedcomment').text();
                addnewsfeed.PersonalUserIds = $scope.PersonalUserIds;
                CCfundingrequestService.Feed(addnewsfeed).then(function (savenewsfeed) { });
                $('#costcentrefundingreqfeedcomment').empty();
                if (document.getElementById('costcentrefundingreqfeedcomment').innerHTML.replace(/(<([^>]+)>)/g, '').replace(/ /g, '').length == 0) {
                    document.getElementById('costcentrefundingreqfeedcomment').innerHTML = '<span id="costcentrefeedtextholder" class=\'placeholder\'>Write a comment...</span>';
                }
                 $scope.PersonalUserIds = [];
                $timeout(function () {
                    feedforfundingrequest($scope.fundReqIDforFeedforCost, true);
                }, 2000);
            }
        };
        $('body').on('click', 'a[data-DynHTML="CommentshowHTML"]', function (event) {
            var feedid1 = event.target.attributes["id"].nodeValue;
            var feedid = feedid1;
            //var feedfilter = $.grep($scope.Fund_newsfeed_TEMP[0], function (e) {
            //    return e.FeedId == parseInt(feedid);
            //});
            var feedfilter = [];
            for (var j = 0; j < $scope.Fund_newsfeed_TEMP.length; j++) {
                for (var k = 0; k < $scope.Fund_newsfeed_TEMP[j].length; k++) {
                    if ($scope.Fund_newsfeed_TEMP[j][k].FeedId == parseInt(feedid)) {
                        feedfilter = $.grep($scope.Fund_newsfeed_TEMP[j], function (e) {
                            return e.FeedId == parseInt(feedid);
                        });
                    }
                }
            }

            $("#" + feedid).next('div').hide();
            if (feedfilter != '') {
                for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
                    if ($('#fundingrequestfeeddivFinReq #feedcomment_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
                        $(this)[0].innerHTML = "View " + (feedfilter[0].FeedComment.length - 1) + " more comment(s)";
                        if (i != 0) $('#fundingrequestfeeddivFinReq #feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function () {$(this).clearQueue(); });
                        else $('#fundingrequestfeeddivFinReq #feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { $(this).clearQueue();});
                    } else {
                        $('#fundingrequestfeeddivFinReq #feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () {$(this).clearQueue(); });
                        $(this)[0].innerHTML = "Hide comment(s)";
                    }
                }
            }
        });
        $('body').on('click', 'a[data-DynHTML="CommentHTML"]', function (event) {
            var commentuniqueid = this.id;
            event.stopImmediatePropagation();
            $(this).hide();
            var fundingreqfeeddivHtml = '';
            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li class="writeNewComment">';
            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newComment"><div class="userAvatar"><img data-role="user-avatar" src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\'></div>';
            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="textarea-wrapper" data-role="textarea"><div contenteditable="true" tabindex="0" class="textarea" id="feedcomment_' + commentuniqueid + '"></div></div>';
            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<button type="submit" class="btn btn-primary"  data-dyncommentbtn="ButtonHTML">' + $translate.instant('LanguageContents.Res_1204.Caption') + '</button></div></li>';
            var currentobj = $(this).parents('li[data-parent="NewsParent"]').find('ul');
            if ($(this).parents('li[data-parent="NewsParent"]').find('ul').children().length > 0) $(this).parents('li[data-parent="NewsParent"]').find('ul li:first').before(fundingreqfeeddivHtml);
            else $(this).parents('li[data-parent="NewsParent"]').find('ul').html(fundingreqfeeddivHtml);
            $timeout(function () {
                $('#feedcomment_' + commentuniqueid).html('').focus();
            }, 10);
        });
        $('body').on('click', 'a[data-id="costcenterlink"]', function (event) {
            event.stopImmediatePropagation();
            var entityid = $(this).attr('data-costcentreid');
            CCfundingrequestService.IsActiveEntity($(this).attr('data-costcentreid')).then(function (result) {
                if (result.Response == true) {
                    $('#CostCentreFundingRequestApprovalModal').modal('hide');
                    //$("#feedFundingRequestModal").modal("hide");
                    $location.path('/mui/planningtool/costcentre/detail/section/' + entityid + '/overview');
                   // $scope.Closeccfundreqpopup();
                    if ($('.FundingRequestPopup').length != 0) {
                        $('.FundingRequestPopup').hide()
                    }
                    else {
                        $scope.Closeccfundreqpopup();
                    }
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });
        $('body').on('click', 'button[data-dyncommentbtn="ButtonHTML"]', function (event) {
            event.stopImmediatePropagation();
            if ($(this).prev().eq(0).text().toString().trim().length > 0) {
                var addfeedcomment = {};
                var FeedidforComment = $(this).parents("li:eq(1)").attr('data-id');
                var myDate = new Date();
                addfeedcomment.FeedID = $(this).parents("li:eq(1)").attr('data-id');
                addfeedcomment.Actor = parseInt($cookies['UserId']);
                addfeedcomment.Comment = $(this).prev().eq(0).text();
                var d = new Date();
                var month = d.getMonth() + 1;
                var day = d.getDate();
                var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                var output = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day + ' ' + hrs;
                var _this = $(this);
                CCfundingrequestService.InsertFeedComment(addfeedcomment).then(function (saveusercomment) {
                    var fundingreqfeeddivHtml = '';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class=\"newsFeed\">';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"><img src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\' alt="Avatar"></div>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt">';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5> <a href="undefined">' + $cookies['Username'] + '</a></h5></div>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + saveusercomment.Response + '</p></div>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">Few seconds ago</span></div>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></div></div></li>';
                    $('#' + divtypeid).find('li[data-id=' + FeedidforComment + ']').first().find('li:first').before(fundingreqfeeddivHtml)
                    $(".writeNewComment").remove();;
                    $('#Comment_' + addfeedcomment.FeedID).show();                    
                });
            }
        });
        $scope.UpdateTaskStatus = function (taskID, StatusID) {
            CCfundingrequestService.UpdateTaskStatus(taskID, StatusID).then(function (TaskStatusData) {
                if (TaskStatusData.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
                } else {
                    if (TaskStatusData.Response == 2) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1901.Caption'));
                    } else if (TaskStatusData.Response == 1) {
                        $scope.ReturnStatus = StatusID;
                        NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                        $('#CostCentreFundingRequestApprovalModal').modal('hide');
                        //$("#feedFundingRequestModal").modal("hide");
                        // $(window).trigger("onCostcentreFinancialAccept", [$scope.EntityUniqueidToReturn, StatusID]);
                        $scope.$emit('onCostcentreFinancialAccept', $scope.EntityUniqueidToReturn, StatusID);
                        $scope.$emit('onMyfundingRequestkAction');
                        $scope.Closeccfundreqpopup();
                    } else {
                        $scope.ReturnStatus = StatusID;
                        NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                        $('#CostCentreFundingRequestApprovalModal').modal('hide');
                        //$("#feedFundingRequestModal").modal("hide");
                        $("#feedFundingRequestModal").trigger("onMyfundingRequestkAction");
                        //$(window).trigger("onCostcentreFinancialAccept", [$scope.EntityUniqueidToReturn, StatusID]);
                        $scope.$emit('onCostcentreFinancialAccept', $scope.EntityUniqueidToReturn, StatusID);
                        $scope.$emit('onMyfundingRequestkAction');
                        $scope.Closeccfundreqpopup();
                    }
                }
            });
        }

        $scope.RetrieveName = function (userid) {
            var MemberList = $.grep($scope.EntityMemberList, function (e) {
                return e.Userid == userid;
            });
            if (MemberList.length > 0) return MemberList[0].UserName;
            else return "";
        }
        $scope.Closeccfundreqpopup = function () {
            $modalInstance.dismiss('cancel');
        }
    }
    app.controller("mui.planningtool.costcentre.CostCentreFundRequestActionCtrl", ['$scope', '$location', '$resource', '$timeout', '$stateParams', '$cookies', '$window', '$compile', '$translate', 'CCfundingrequestService', '$modalInstance', 'params', muiplanningtoolcostcentreCostCentreFundRequestActionCtrl]);
    function CCfundingrequestService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetEnityFeedsForFundingReq: GetEnityFeedsForFundingReq,
            Feed: Feed,
            IsActiveEntity: IsActiveEntity,
            InsertFeedComment: InsertFeedComment,
            GetFundRequestTaskDetails: GetFundRequestTaskDetails,
            GetNewsfeedFundRequestTaskDetails: GetNewsfeedFundRequestTaskDetails,
            GetLockStatus: GetLockStatus,
            InsertTaskMembers: InsertTaskMembers,
            UpdateEntityActiveStatus: UpdateEntityActiveStatus,
            UpdateTaskStatus: UpdateTaskStatus,
            GetMember: GetMember
        });
        function GetEnityFeedsForFundingReq(EntityID, pageNo, islatestfeed) { var request = $http({ method: "get", ignoreLoadingBar: true, url: "api/common/GetEnityFeedsForFundingReq/" + EntityID + "/" + pageNo + "/" + islatestfeed, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function Feed(addnewsfeed) { var request = $http({ method: "put", url: "api/common/Feed/", params: { action: "update", }, data: addnewsfeed }); return (request.then(handleSuccess, handleError)); }
        function IsActiveEntity(EntityID) { var request = $http({ method: "get", url: "api/common/IsActiveEntity/" + EntityID, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function InsertFeedComment(addfeedcomment) { var request = $http({ method: "post", url: "api/common/InsertFeedComment/", params: { action: "add", }, data: addfeedcomment }); return (request.then(handleSuccess, handleError)); }
        function GetFundRequestTaskDetails(EntityUniqueKey, CostCentreID) { var request = $http({ method: "get", url: "api/Planning/GetFundRequestTaskDetails/" + EntityUniqueKey + "/" + CostCentreID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetNewsfeedFundRequestTaskDetails(FundID) { var request = $http({ method: "get", url: "api/Planning/GetNewsfeedFundRequestTaskDetails/" + FundID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetLockStatus(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetLockStatus/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function InsertTaskMembers(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertTaskMembers/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateEntityActiveStatus(dataobj) { var request = $http({ method: "put", url: "api/Planning/UpdateEntityActiveStatus/", params: { action: "update" }, data: dataobj }); return (request.then(handleSuccess, handleError)); }
        function UpdateTaskStatus(EntityID, Status) { var request = $http({ method: "put", url: "api/Planning/UpdateTaskStatus/" + EntityID + "/" + Status, params: { action: "update" } }); return (request.then(handleSuccess, handleError)); }
        function GetMember(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetMember/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("CCfundingrequestService", ['$http', '$q', CCfundingrequestService]);
})(angular, app);