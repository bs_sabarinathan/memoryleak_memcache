﻿(function (ng, app) {
    "use strict";

    function muiadminfinancialreportsettings($scope, $resource, $location, $timeout, $http, $translate, AdminService) {
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName).replace(/\\/g, "\/");
        $scope.reportJSONObject = [];
        $scope.reportname = "";
        $scope.FinancialFotrecastObj = {
            ID: 0,
            ReportAction: $translate.instant('LanguageContents.Res_4056.Caption'),
            description: "",
            imageurl: "NoPreview.jpg"
        };
        $scope.entitytyesource = [];
        $scope.attributesource = [];
        $scope.attributewiseAttributesource = [];
        $scope.blockWiseAttributeRelation = {};
        $scope.Uniqueattributesource = [];
        if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {

            $scope.reportPreviewImg = cloudpath + "/Files/ReportFiles/Images/Preview/Temp/";
        }
        else {
            $scope.reportPreviewImg = "/Files/ReportFiles/Images/Preview/Temp/";
        }
        AdminService.GetAllEntityTypeAttributeRelationWithLevels().then(function (result) {
            var data = result.Response;
            getuniqueEntitytypes(data);
            $scope.attributesource = data;
            var attrivutecoll = [];
            attrivutecoll = $.grep(data, function (e) {
                return e.AttributeTypeID != 0;
            });
            $scope.attributewiseAttributesource = attrivutecoll;
            getuniqueAttributes(data);
        }).then(function (response) {
            $timeout(function () {
                LoadFinancialReportSettings();
            }, 100);
        });
        $timeout(function () {
            StrartUpload();
        }, 50);
        var countUp = function () {
            StrartUpload();
            $timeout(function () {
                countUp();
            }, 500);
        }

        function getuniqueEntitytypes(source) {
            var unique = {};
            $scope.entitytyesource = [];
            for (var i = 0, entitytype; entitytype = source[i++];) {
                if (typeof (unique[entitytype.EntityTypeID]) == "undefined") {
                    $scope.entitytyesource.push(entitytype);
                }
                unique[entitytype.EntityTypeID] = 0;
            }
        }

        function getuniqueAttributes(source) {
            var unique = {};
            $scope.Uniqueattributesource = [];
            for (var i = 0, entitytype; entitytype = source[i++];) {
                if (typeof (unique[entitytype.strAttributeID]) == "undefined") {
                    $scope.Uniqueattributesource.push(entitytype);
                }
                unique[entitytype.strAttributeID] = 0;
            }
        }

        function generateUUID() {
            var d = new Date().getTime();
            var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = (d + Math.random() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
            });
            return uuid;
        };
        $scope.getOptions = function (num) {
            return $scope.blockWiseAttributeRelation["Rel_" + num];
        };
        $scope.UpdateAttributeRelation = function (uuid, entitytypeIDArr) {
            RetreiveEntitytyewiseAttributesRelation(uuid, [entitytypeIDArr]);
        }

        function RetreiveEntitytyewiseAttributesRelation(uuid, entitytypeIDArr) {
            if (entitytypeIDArr != null) {
                if (entitytypeIDArr[0] != null) {
                    var matches = [];
                    matches = jQuery.grep($scope.attributewiseAttributesource, function (relation) {
                        return entitytypeIDArr[0].indexOf(relation.EntityTypeID.toString()) != -1;
                    });
                    $scope.blockWiseAttributeRelation["Rel_" + uuid] = [];
                    for (var i = 0, rel; rel = matches[i++];) {
                        if (rel.AttributeTypeID == 3 || rel.AttributeTypeID == 4) {
                            var blockvals = [];
                            blockvals = $.grep($scope.blockWiseAttributeRelation["Rel_" + uuid], function (e) {
                                return e.AttributeTypeID == rel.AttributeTypeID && e.AttributeTypeID == 3 && e.AttributeTypeID == 4 && e.Level == rel.Level && e.strAttributeID == rel.strAttributeID;
                            });
                            if (blockvals.length == 0) {
                                $scope.blockWiseAttributeRelation["Rel_" + uuid].push(rel);
                            }
                        }
                    }
                } else {
                    $scope.blockWiseAttributeRelation["Rel_" + uuid] = [];
                }
            } else {
                $scope.blockWiseAttributeRelation["Rel_" + uuid] = [];
            }
        }
        $scope.blocktypes = [{
            ID: 1,
            Name: 'summary'
        }, {
            ID: 2,
            Name: 'Attribute wise'
        }, {
            ID: 3,
            Name: 'Detail'
        }];
        $scope.columns = [{
            ID: 1,
            Caption: 'Total Number of MA & ME'
        }, {
            ID: -1,
            Caption: 'Total Planned Budget'
        }, {
            ID: -2,
            Caption: 'Total In Requests'
        }, {
            ID: -3,
            Caption: 'Total Approved Allocation'
        }, {
            ID: -4,
            Caption: 'Approved Budget'
        }, {
            ID: -5,
            Caption: 'Budget Deviation'
        }, {
            ID: -6,
            Caption: 'Commited'
        }, {
            ID: -7,
            Caption: 'Spent'
        }, {
            ID: -8,
            Caption: 'Available To Spend'
        }];
        $scope.Summarycolumns = [{
            id: 1,
            caption: 'Total Number of Items',
            isselected: '0'
        }, {
            id: 2,
            caption: 'Total Planned Budget',
            isselected: '0'
        }, {
            id: 3,
            caption: 'Total In Requests',
            isselected: '0'
        }, {
            id: 4,
            caption: 'Total Approved Allocation',
            isselected: '0'
        }, {
            id: 5,
            caption: 'Approved Budget',
            isselected: '0'
        }, {
            id: 6,
            caption: 'Budget Deviation',
            isselected: '0'
        }, {
            id: 7,
            caption: 'Commited',
            isselected: '0'
        }, {
            id: 8,
            caption: 'Spent',
            isselected: '0'
        }, {
            id: 9,
            caption: 'Available To Spend',
            isselected: '0'
        }];
        $scope.Attributewisecolumns = [{
            id: 1,
            caption: 'Options',
            isselected: '0'
        }, {
            id: 2,
            caption: 'Planned',
            isselected: '0'
        }, {
            id: 3,
            caption: 'In Requests',
            isselected: '0'
        }, {
            id: 4,
            caption: 'Approved allocation',
            isselected: '0'
        }, {
            id: 5,
            caption: 'Approved Budget',
            isselected: '0'
        }, {
            id: 6,
            caption: 'Budget Deviation',
            isselected: '0'
        }, {
            id: 7,
            caption: 'Commited',
            isselected: '0'
        }, {
            id: 8,
            caption: 'Spent',
            isselected: '0'
        }, {
            id: 9,
            caption: 'Available To Spend',
            isselected: '0'
        }];
        $scope.Detailwisecolumns = [{
            id: -2,
            caption: 'Total Planned Budget',
            isselected: '0'
        }, {
            id: -3,
            caption: 'Total In Requests',
            isselected: '0'
        }, {
            id: -4,
            caption: 'Total Approved Allocation',
            isselected: '0'
        }, {
            id: -5,
            caption: 'Approved Budget',
            isselected: '0'
        }, {
            id: -6,
            caption: 'Budget Deviation',
            isselected: '0'
        }, {
            id: -7,
            caption: 'Commited',
            isselected: '0'
        }, {
            id: -8,
            caption: 'Spent',
            isselected: '0'
        }, {
            id: -9,
            caption: 'Available To Spend',
            isselected: '0'
        }, {
            id: -200,
            caption: 'Entity Name',
            isselected: '0'
        }, {
            id: -100,
            caption: 'ID#',
            isselected: '0'
        }, {
            id: 69,
            caption: 'Owner',
            isselected: '0'
        }];
        $scope.FinancialReportSettings = [];

        function LoadFinancialReportSettings() {
            AdminService.GetFinancialReportSettings().then(function (res) {
                $scope.FinancialReportSettings = [];
                $scope.FinancialReportSettings = res.Response;
            });
        }

        function LoadFinancialReportSettingsXML() {
            AdminService.GetReportJSONData($scope.FinancialFotrecastObj.ID).then(function (res) {
                $scope.reportJSONObject = [];
                $scope.ReportAttributes = {
                    blockWiseAttributeRelation: {}
                };
                var rst = [];
                rst = JSON.parse(res.Response);
                if (rst.root != null) {
                    if (rst.root.block != null) {
                        for (var i = 0, jsonData; jsonData = rst.root.block[i++];) {
                            var uuid = generateUUID();
                            jsonData.uuid = uuid;
                            if (jsonData.Type == 1 || jsonData.Type == 3) {
                                if (jsonData.EntityType != null || jsonData.EntityType != '') {
                                    jsonData.EntityType = typeof jsonData.EntityType === "string" ? jsonData.EntityType.split(',') : [];
                                    RetreiveEntitytyewiseAttributesRelation(uuid, jsonData.EntityType);
                                } else {
                                    jsonData.EntityType = [];
                                    $scope.blockWiseAttributeRelation["Rel_" + uuid] = [];
                                }
                            } else {
                                if (jsonData.EntityType != null || jsonData.EntityType != '') {
                                    jsonData.EntityType = typeof jsonData.EntityType === "string" ? jsonData.EntityType.split(',') : 0;
                                    RetreiveEntitytyewiseAttributesRelation(uuid, [jsonData.EntityType]);
                                } else {
                                    jsonData.EntityType = 0;
                                    $scope.blockWiseAttributeRelation["Rel_" + uuid] = [];
                                }
                            }
                            if (jsonData.AttributeID != undefined) {
                                if (jsonData.AttributeID != null || jsonData.AttributeID != '') jsonData.AttributeID = typeof jsonData.AttributeID === "string" ? jsonData.AttributeID.toString() : 0;
                                else jsonData.AttributeID = '0';
                            }
                            jsonData.Type = parseInt(jsonData.Type);
                            if (jsonData.charts != undefined) {
                                for (var j = 0, chartsObj; chartsObj = jsonData.charts[j++];) {
                                    if (chartsObj.chart != undefined) {
                                        for (var k = 0, chartdata; chartdata = chartsObj.chart[k++];) {
                                            chartdata.chartType = typeof chartdata.chartType === "string" ? parseInt(chartdata.chartType) : 0;
                                            if (jsonData.Type == 1 || jsonData.Type == 3) {
                                                if (chartdata.columns != null || chartdata.columns != '') chartdata.columns = typeof chartdata.columns === "string" ? chartdata.columns.split(',') : [];
                                                else chartdata.columns = [];
                                            } else {
                                                if (chartdata.chartType == 3) {
                                                    if (chartdata.columns != null || chartdata.columns != '') chartdata.columns = typeof chartdata.columns === "string" ? chartdata.columns.split(',') : 0;
                                                    else chartdata.columns = 0;
                                                } else {
                                                    if (chartdata.columns != null || chartdata.columns != '') chartdata.columns = typeof chartdata.columns === "string" ? chartdata.columns.split(',') : [];
                                                    else chartdata.columns = [];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $timeout(function () {
                        $scope.reportJSONObject = rst;
                        $scope.reportname = $scope.reportJSONObject.root.caption;
                    }, 10);
                }
            });
        }

        function FormatJsonData(JsonObject) {
            var rst = [];
            rst = JsonObject;
            if (rst.root.block != null) {
                for (var i = 0, jsonData; jsonData = rst.root.block[i++];) {
                    if (jsonData.Type == 1 || jsonData.Type == 3) {
                        if (jsonData.EntityType != null || jsonData.EntityType != '') jsonData.EntityType = typeof jsonData.EntityType === "string" ? jsonData.EntityType.split(',') : [];
                        else jsonData.EntityType = [];
                    } else {
                        if (jsonData.EntityType != null || jsonData.EntityType != '') jsonData.EntityType = typeof jsonData.EntityType === "string" ? jsonData.EntityType.split(',') : 0;
                        else jsonData.EntityType = 0;
                    }
                    if (jsonData.AttributeID != undefined) {
                        if (jsonData.AttributeID != null || jsonData.AttributeID != '') jsonData.AttributeID = typeof jsonData.AttributeID === "string" ? parseInt(jsonData.AttributeID) : '0';
                        else jsonData.AttributeID = '0';
                    }
                    jsonData.Type = parseInt(jsonData.Type);
                    if (jsonData.charts != undefined) {
                        for (var j = 0, chartsObj; chartsObj = jsonData.charts[j++];) {
                            if (chartsObj.chart != undefined) {
                                for (var k = 0, chartdata; chartdata = chartsObj.chart[k++];) {
                                    if (chartdata.columns != null || chartdata.columns != '') chartdata.columns = typeof chartdata.columns === "string" ? chartdata.columns.split(',') : [];
                                    else chartdata.columns = [];
                                    chartdata.chartType = typeof chartdata.chartType === "string" ? parseInt(chartdata.chartType) : 0;
                                }
                            }
                        }
                    }
                }
                $timeout(function () {
                    $scope.reportJSONObject = rst;
                    $scope.reportname = $scope.reportJSONObject.root.caption;
                }, 10);
            }
        }
        $scope.EntitytypeTypewiseAttributeFilter = function (entitytypeId) {
            return function (block) {
                var attrEntitytypeid = [block.EntityTypeID.toString()];
                var matches = jQuery.grep(entitytypeId, function (eid) {
                    return attrEntitytypeid.indexOf(eid) != -1;
                });
                if (matches.length > 0) return true;
                else return false;
            }
        }
        $scope.addNewReportBlock = function () {
            var blockid = $scope.reportJSONObject.root.block != null ? $scope.reportJSONObject.root.block.length + 1 : 1;
            var block = {
                "caption": "",
                "charts": [],
                "columns": [],
                "EntityType": [],
                "id": "" + blockid + "",
                "Type": 1
            };
            var columns = [{
                "column": $scope.Summarycolumns
            }];
            block.columns = columns;
            block.uuid = generateUUID();
            if ($scope.reportJSONObject.root.block != null) {
                $scope.reportJSONObject.root.block.push(block);
            } else {
                $scope.reportJSONObject.root["block"] = [block];
            }
        }
        $scope.changeBlockColumns = function (blocktype, blockcolumns, blocks) {
            var blockvals = $.grep($scope.blocktypes, function (e) {
                return e.ID == blocktype
            })[0];
            blockcolumns[0].column = [];
            if (blockvals.ID == 1) {
                blockcolumns[0].column = $scope.Summarycolumns;
                if (blocks.charts != undefined) {
                    for (var i = 0, charts; charts = blocks.charts[i++];) {
                        charts.columns = [];
                    }
                }
                if (blocks.AttributeID != undefined) blocks.AttributeID = '';
            } else if (blockvals.ID == 2) {
                blockcolumns[0].column = $scope.Attributewisecolumns;
                if (blocks.AttributeID == undefined) blocks.AttributeID = '';
                if (blocks.charts != undefined) {
                    for (var i = 0, charts; charts = blocks.charts[i++];) {
                        charts.columns = [];
                    }
                }
            }
            RetreiveEntitytyewiseAttributesRelation(blocks.uuid, [blocks.EntityType]);
        }
        $scope.addReportBlockChart = function (blocks) {
            var len = 1;
            if (blocks.charts[0] != undefined) {
                len = blocks.charts[0] != undefined ? blocks.charts[0].chart.length + 1 : 1;
                var chartdata = {
                    "chartType": 0,
                    "columns": [],
                    "id": len.toString()
                };
                if (blocks.charts != null || blocks.charts.length > 0) {
                    if (blocks.charts[0] != undefined) {
                        if (blocks.charts[0].chart != undefined) blocks.charts[0].chart.push(chartdata);
                        else {
                            var chart = [{
                                "chartType": 0,
                                "columns": [],
                                "id": "1"
                            }];
                            blocks.charts["chart"] = chart;
                        }
                    } else {
                        var chart = [{
                            "chartType": 0,
                            "columns": [],
                            "id": "1"
                        }];
                        blocks.charts["chart"] = chart;
                    }
                }
            } else {
                var chartsdata = [{
                    "chart": [{
                        "chartType": 0,
                        "columns": [],
                        "id": "1"
                    }]
                }];
                blocks["charts"] = chartsdata;
            }
        }
        $scope.deletechartblock = function (chart, chartblock) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2013.Caption'), function (result) {
                if (result) $timeout(function () {
                    chartblock.splice($.inArray(chart, chartblock), 1);
                }, 100);
            });
        }
        $scope.deleteblock = function (block, blocks) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2043.Caption'), function (result) {
                if (result) $timeout(function () {
                    blocks.splice($.inArray(block, blocks), 1);
                }, 100);
            });
        }
        $scope.DeleteReportById = function (id) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2005.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        AdminService.DeletefinancialreportByID(id).then(function (res) {
                            if (res.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4305.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                                $scope.reportJSONObject = [];
                                $scope.reportname = "";
                                LoadFinancialReportSettings();
                            }
                        });
                    }, 100);
                }
            });
        }
        $scope.refreshAttributeselection = function (blocksAttributeID, blocksType) {
            if (blocksType != undefined) if (blocksType == 2) $timeout(function () {
                blocksAttributeID = '';
            }, 10);
        }
        $scope.RemoveCheckColumnSelection = function (column, $event, charts) {
            var checkbox = $event.target;
            column.isselected = checkbox.checked == true ? '1' : '0';
            var chartlen = (charts != undefined || charts != null) ? charts.length : 0;
            if (chartlen > 0) {
                for (var j = 0, chartsObj; chartsObj = charts[j++];) {
                    if (chartsObj.chart != undefined) {
                        for (var k = 0, chartdata; chartdata = chartsObj.chart[k++];) {
                            if (checkbox.checked) { } else {
                                if (chartdata.columns.indexOf(column.id) > -1) {
                                    chartdata.columns.splice($.inArray(column.id, chartdata.columns), 1);
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.updateReportBlock = function () {
            var flag = true;
            var alertText = "";
            var rst = [];
            if ($scope.reportJSONObject.root.caption != "" && $scope.reportJSONObject.root.caption != undefined) {
                var inserttasktempparams = {};
                rst = $scope.reportJSONObject;
                if (rst.root.block != null) {
                    for (var i = 0, jsonData; jsonData = rst.root.block[i++];) {
                        if (jsonData.EntityType != null) {
                            if (jsonData.Type == 1 || jsonData.Type == 3) {
                                jsonData.EntityType = typeof jsonData.EntityType === "object" ? jsonData.EntityType.join(',') : jsonData.EntityType.toString();
                            } else if (jsonData.Type == 2) {
                                jsonData.EntityType = jsonData.EntityType.toString();
                            } else jsonData.EntityType = "";
                        } else jsonData.EntityType = "";
                        if (jsonData.EntityType === "") {
                            alertText = $translate.instant('LanguageContents.Res_4621.Caption');
                            bootbox.alert(alertText);
                            return false;
                        }
                        if (jsonData.caption == "") {
                            alertText = $translate.instant('LanguageContents.Res_4583.Caption');
                            bootbox.alert(alertText);
                            return false;
                        }
                        if (jsonData.AttributeID != undefined) {
                            if (jsonData.AttributeID != null || jsonData.AttributeID != '') jsonData.AttributeID = (typeof jsonData.AttributeID === "number" || typeof jsonData.AttributeID === "string") ? jsonData.AttributeID.toString() : "";
                            else jsonData.AttributeID = "";
                        }
                        jsonData.Type = jsonData.Type;
                        if (jsonData.columns != undefined) {
                            var emptycolumn = [];
                            emptycolumn = $.grep(jsonData.columns[0].column, function (e) {
                                return e.caption == '' || e.caption == null;
                            });
                            if (emptycolumn.length > 0) {
                                alertText = $translate.instant('LanguageContents.Res_4596.Caption');
                                bootbox.alert(alertText);
                                return false;
                            }
                        }
                        if (jsonData.columns != undefined) {
                            for (var j = 0, columnsObj; columnsObj = jsonData.columns[j++];) {
                                if (columnsObj.column != undefined) {
                                    var emptycolumn = [];
                                    emptycolumn = $.grep(columnsObj.column, function (e) {
                                        return e.isselected == '0';
                                    });
                                    if (emptycolumn.length == columnsObj.column.length) {
                                        alertText = $translate.instant('LanguageContents.Res_4611.Caption');
                                        bootbox.alert(alertText);
                                        return false;
                                    }
                                }
                            }
                        }
                        if (jsonData.Type == 3) {
                            if (jsonData.columns != undefined) {
                                for (var j = 0, columnsObj; columnsObj = jsonData.columns[j++];) {
                                    if (columnsObj.column != undefined) {
                                        columnsObj.column = $.grep(columnsObj.column, function (e) {
                                            return e.isselected == '1';
                                        });
                                    }
                                }
                            }
                        }
                        if (jsonData.charts != undefined) {
                            for (var j = 0, chartsObj; chartsObj = jsonData.charts[j++];) {
                                if (chartsObj.chart != undefined) {
                                    for (var k = 0, chartdata; chartdata = chartsObj.chart[k++];) {
                                        chartdata.chartType = chartdata.chartType.toString();
                                        if (jsonData.Type == 1 || jsonData.Type == 3) {
                                            if (chartdata.columns != null) chartdata.columns = typeof chartdata.columns === "object" ? chartdata.columns.join(',') : "";
                                            else chartdata.columns = '';
                                        } else {
                                            if (chartdata.chartType != undefined || chartdata.chartType != null) {
                                                if (chartdata.chartType == 3) {
                                                    if (chartdata.columns != null) chartdata.columns = chartdata.columns.toString();
                                                    else chartdata.columns = '';
                                                } else {
                                                    if (chartdata.columns != null) chartdata.columns = typeof chartdata.columns === "object" ? chartdata.columns.join(',') : "";
                                                    else chartdata.columns = '';
                                                }
                                            } else {
                                                chartdata.columns = '';
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                inserttasktempparams.reportBlockData = rst;
                inserttasktempparams.ReportID = $scope.FinancialFotrecastObj.ID;
                inserttasktempparams.Name = $scope.reportJSONObject.root.caption;
                inserttasktempparams.ReportImage = $scope.FinancialFotrecastObj.imageurl;
                inserttasktempparams.ReportDescription = $scope.FinancialFotrecastObj.description;
                AdminService.insertupdatefinancialreportsettings(inserttasktempparams).then(function (result) {
                    if (result != null && result != 0) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                        if ($scope.FinancialFotrecastObj.ID > 0) {
                            LoadFinancialReportSettingsXML();
                            LoadFinancialReportSettings();
                        } else {
                            $scope.reportJSONObject = [];
                            $scope.reportname = "";
                            LoadFinancialReportSettings();
                        }
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4689.Caption'));
            }
        }
        $scope.GetFinancialReportBlock = function (report) {
            report.IsSelected = true;
            $scope.FinancialFotrecastObj.ID = report.ID;
            $scope.FinancialFotrecastObj.ReportAction = $translate.instant('LanguageContents.Res_5023.Caption');
            $scope.FinancialFotrecastObj.description = $scope.DecodedTextName(report.Description);
            $scope.FinancialFotrecastObj.imageurl = report.ReportImage;
            LoadFinancialReportSettingsXML();
        }

        function unselectallReportEdit(id) {
            var reportResObj = $.grep($scope.FinancialReportSettings, function (e) {
                return e.ID != id;
            });
            for (var i = 0, reportData; reportData = reportResObj[i++];) {
                reportData.IsSelected = false;
            }
        }
        $scope.AddNewReport = function () {
            $scope.FinancialFotrecastObj.ID = 0;
            $scope.FinancialFotrecastObj.ReportAction = $translate.instant('LanguageContents.Res_4056.Caption');
            $scope.FinancialFotrecastObj.description = "";
            $scope.FinancialFotrecastObj.imageurl = "NoPreview.jpg";
            var dt = {
                "root": {
                    "block": [{
                        "caption": "",
                        "charts": [{
                            "chart": [{
                                "chartType": "2",
                                "columns": "2,3,4,5,6,7,8",
                                "id": "1"
                            }]
                        }],
                        "columns": [{
                            "column": $scope.Summarycolumns
                        }],
                        "EntityType": "",
                        "id": "1",
                        "Type": 1
                    }],
                    "caption": "",
                    "noOfColumn": "1"
                }
            };
            FormatJsonData(dt);
            RemoveCheckBoxSelection();
            $timeout(function () {
                $('#reportCaption').focus().select();
            }, 1000);
        }
        $scope.idFilter = function (location) {
            return (location.id > 1 || location.id < -1);
        };

        function RemoveCheckBoxSelection() {
            $('#financialReportSettings > tbody input:checkbox').each(function () {
                $(this).next('i').removeClass('checked');
                $(this).prop('checked', false);
            });
        }
        $scope.UploadReportImageID = 0;
        $scope.UploadImagefile = function () {
            $scope.UploadReportImageID = 1;
            $("#financialsettingspickfiles").click();
        };

        function StrartUpload() {
            var uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                browse_button: 'financialsettingspickfiles',
                container: 'financialsettingsfilescontainer',
                max_file_size: '10mb',
                flash_swf_url: 'js/plupload.flash.swf',
                silverlight_xap_url: 'js/plupload.silverlight.xap',
                url: 'Handlers/ReportImagePicHandler.ashx?Type=Attachment',
                multi_selection: false,
                filters: [{
                    title: "Image files",
                    extensions: "jpg,gif,png"
                }]
            });
            uploader.bind('Init', function (up, params) { });
            uploader.init();
            uploader.bind('FilesAdded', function (up, files) {
                $.each(files, function (i, file) { });
                up.refresh();
                uploader.start();
                $('#financialsettingspickfiles').hide();
                $('#financialsettingsuploadprogress').show();
                $('#financialsettingsuploadprogress .bar').css('width', '0');
                $('#financialsettingsuploadprogress #financialsettingsbarText').html('0 %');
            });
            uploader.bind('financialsettingsuploadprogress', function (up, file) {
                $('#financialsettingsuploadprogress .bar').css("width", file.percent + "%");
                $('#financialsettingsuploadprogress #financialsettingsbarText').html(file.percent + "%");
            });
            uploader.bind('Error', function (up, err) {
                bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                up.refresh();
            });
            uploader.bind('FileUploaded', function (up, file, response) {
                $scope.tempFile = file;
                $scope.tempResponse = response;
                var img = new Image();
                img.src = 'Files/ReportFiles/Images/Temp/' + response.response;
                $('#financialsettingspickfiles').show();
                $('#financialsettingsuploadprogress').hide();
                $('#financialsettingsReportImage').modal('show');
                $('#financialsettingsReportImage #financialsettingsCropReportImg').removeAttr('src').css('visibility', 'hidden');
                $('#financialsettingsReportImage #financialsettingsCropReportImg').attr('style', '');
                var w = parseInt(response.response.split(',')[1]);
                var h = parseInt(response.response.split(',')[2]);
                var path = response.response.split(',')[0];
                $timeout(function () {
                    AssigneNewImage(w, h, path);
                }, 200);
            });
        }
        var jcrop_api;

        function AssigneNewImage(w, h, path) {
            $('#financialsettingsReportImage #financialsettingsCropReportImg').attr('src', 'Files/ReportFiles/Images/Temp/' + path);
            $('#financialsettingsReportImage #financialsettingsCropReportImg').attr('data-width', w).attr('data-height', h);
            if (w >= h) {
                $('#financialsettingsReportImage #financialsettingsCropReportImg').css('width', '704px');
                var imgheight = Math.round((h / w) * 704);
                var margine = Math.round((528 - imgheight) / 2);
                if (margine < 0) {
                    margine = 0;
                }
                $('#financialsettingsReportImage #financialsettingsCropReportImg').parent().css('margin-top', margine + 'px');
            } else {
                $('#financialsettingsReportImage #financialsettingsCropReportImg').css('height', '528px');
                $('#financialsettingsReportImage #financialsettingsCropReportImg').parent().css('margin-top', '0px');
            }
            $('#financialsettingsReportImage #financialsettingsCropReportImg').css('visibility', 'visible');
            initJcrop();
        }

        function initJcrop() {
            if ($('#financialsettingsReportImage .jcrop-holder').length > 0) {
                jcrop_api.destroy();
            }
            $('#financialsettingsCropReportImg').Jcrop({
                onSelect: showPreview,
                onRelease: releaseCheck
            }, function () {
                jcrop_api = this;
                jcrop_api.setOptions({
                    aspectRatio: 4 / 5
                });
                jcrop_api.focus();
                var w = $('#financialsettingsReportImage #financialsettingsCropReportImg').width();
                var h = $('#financialsettingsReportImage #financialsettingsCropReportImg').height();
                $scope.OrgimgWidth = w;
                $scope.OrgimgHeight = h;
                var nw = 0;
                var nh = 0;
                if (w <= h) {
                    nw = w;
                    nh = Math.round((w / 4) * 5);
                } else {
                    nh = h;
                    nw = Math.round((h / 4) * 5);
                }
                jcrop_api.animateTo([0, 0, nw, nh]);
            });

            function showPreview(c) {
                var orgw = $('#financialsettingsReportImage #financialsettingsCropReportImg').attr('data-width');
                var orgh = $('#financialsettingsReportImage #financialsettingsCropReportImg').attr('data-height');
                $scope.userimgWidth = Math.round((c.w / $scope.OrgimgWidth) * orgw);
                $scope.userimgHeight = Math.round((c.h / $scope.OrgimgHeight) * orgh);
                $scope.userimgX = Math.round((c.x / $scope.OrgimgWidth) * orgw);
                $scope.userimgY = Math.round((c.y / $scope.OrgimgHeight) * orgh);
            };

            function releaseCheck() {
                jcrop_api.setOptions({
                    allowSelect: true
                });
            };
        };
        $scope.UploadReportImage = function () {
            var res = {};
            res.imgsourcepath = $("#financialsettingsCropReportImg").attr('src');
            res.imgwidth = $scope.userimgWidth;
            res.imgheight = $scope.userimgHeight;
            res.imgX = $scope.userimgX;
            res.imgY = $scope.userimgY;
            res.Preview = $scope.tempResponse.response.split(',')[0];
            AdminService.UpdateFinancialSettingsReportImage(res).then(function (result) {
                $scope.FinancialFotrecastObj.imageurl = $scope.tempResponse.response.split(',')[0];
                $scope.OrgimgWidth = 0;
                $scope.OrgimgHeight = 0;
            });
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.controller("mui.admin.financialreportsettings", ['$scope', '$resource', '$location', '$timeout', '$http', '$translate', 'AdminService', muiadminfinancialreportsettings]);
})(angular, app);