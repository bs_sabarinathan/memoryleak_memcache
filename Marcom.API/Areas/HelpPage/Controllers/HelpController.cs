using System;
using System.Web.Http;
using System.Web.Mvc;
using Marcom.API.Areas.HelpPage.ModelDescriptions;
using Marcom.API.Areas.HelpPage.Models;
using System.Text;
using Newtonsoft.Json;

namespace Marcom.API.Areas.HelpPage.Controllers
{
    /// <summary>
    /// The controller that will handle requests for the help page.
    /// </summary>
    public class HelpController : Controller
    {
        private const string ErrorViewName = "Error";

        public HelpController()
            : this(GlobalConfiguration.Configuration)
        {
        }

        public HelpController(HttpConfiguration config)
        {
            Configuration = config;
        }

        public HttpConfiguration Configuration { get; private set; }

        public ActionResult Index()
        {
            ViewBag.DocumentationProvider = Configuration.Services.GetDocumentationProvider();
            return View(Configuration.Services.GetApiExplorer().ApiDescriptions);
        }

        public ActionResult Api(string apiId)
        {
            if (!String.IsNullOrEmpty(apiId))
            {
                HelpPageApiModel apiModel = Configuration.GetHelpPageApiModel(apiId);
                CustomizeHelpPage(ref apiModel);
                if (apiModel != null)
                {
                    return View(apiModel);
                }
            }

            return View(ErrorViewName);
        }

        public ActionResult ResourceModel(string modelName)
        {
            if (!String.IsNullOrEmpty(modelName))
            {
                ModelDescriptionGenerator modelDescriptionGenerator = Configuration.GetModelDescriptionGenerator();
                ModelDescription modelDescription;
                if (modelDescriptionGenerator.GeneratedModels.TryGetValue(modelName, out modelDescription))
                {
                    return View(modelDescription);
                }
            }

            return View(ErrorViewName);
        }

        public HelpPageApiModel CustomizeHelpPage(ref HelpPageApiModel apiModel)
        {
            if (apiModel.ApiDescription.ID == "GETapi/FinancialsV1/GetPODetailsByEntityID/{ID}")
            {
                apiModel.SampleResponses.Clear();
                System.Net.Http.Headers.MediaTypeHeaderValue mediatyp = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                mediatyp.MediaType = "application/json";
                StringBuilder samplevalue = new StringBuilder();
                samplevalue.Append("{'attributes' :  \n");
                samplevalue.Append("[{ 'attributeID' : 5,'attributename' : 'Order no.', 'IsSystemDefined' : true }]\n");
                samplevalue.Append("'PODetails' : [{'5' : '1992029', 'CostCentreID': 10292, 'Entityid': 12132}]}");
                apiModel.SampleResponses.Add(mediatyp, samplevalue.ToString());
                apiModel.UriParameters[0].Documentation = "Entity ID should be provided";
                return apiModel;
            }
            if (apiModel.ApiDescription.ID == "GETapi/FinancialsV1/GetSpentDetailsByEntityID/{ID}")
            {
                apiModel.SampleResponses.Clear();
                System.Net.Http.Headers.MediaTypeHeaderValue mediatyp = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                mediatyp.MediaType = "application/json";
                StringBuilder samplevalue = new StringBuilder();
                samplevalue.Append("{'attributes' :  \n");
                samplevalue.Append("[{ 'attributeID' : 5,'attributename' : 'Invoice No', 'IsSystemDefined' : true }]\n");
                samplevalue.Append("'PODetails' : [{'5' : '1992029', 'CostCentreID': 10292, 'Entityid': 12132}]}");
                apiModel.SampleResponses.Add(mediatyp, samplevalue.ToString());
                apiModel.UriParameters[0].Documentation = "Entity ID should be provided";
                return apiModel;
            }
            if (apiModel.ApiDescription.ID == "POSTapi/FinancialsV1/CreatePurchaseOrders")
            {
                apiModel.SampleRequests.Clear();
                System.Net.Http.Headers.MediaTypeHeaderValue mediatyp = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                mediatyp.MediaType = "application/json";
                StringBuilder samplevalue = new StringBuilder();
                samplevalue.Append("{ \"PurchaseOrders\" : [ {   \n");
                samplevalue.Append("\"5\" : 10300, \n");
                samplevalue.Append("\"6\" : 90, \n");
                samplevalue.Append("\"7\" : \"2014-06-12\", \n");
                samplevalue.Append("\"8\" : \"Supplier Name\", \n");
                samplevalue.Append("\"9\" : \"Description\",");
                samplevalue.Append("\"10\" : \"Requested by name\", \n");
                samplevalue.Append("\"11\" : \"2014-06-12\", \n");
                samplevalue.Append("\"12\" : \"2014-06-12\", \n");
                samplevalue.Append("\"13\" : \"Vinod Rao\", \n");
                samplevalue.Append("\"25\" : \"PO client spec info\",\n");
                samplevalue.Append("\"CostCentreID\" : 300, \n");
                samplevalue.Append("\"EntityID\" : 301 \n");
                samplevalue.Append("} ] }\n"); 
                apiModel.SampleRequests.Add(mediatyp, samplevalue.ToString());
                 return apiModel;
            }
            if (apiModel.ApiDescription.ID == "PUTapi/FinancialsV1/UpdatePurchaseOrders")
            {
                apiModel.SampleRequests.Clear();
                System.Net.Http.Headers.MediaTypeHeaderValue mediatyp = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                mediatyp.MediaType = "application/json";
                StringBuilder samplevalue = new StringBuilder();
                samplevalue.Append("{ \"PurchaseOrders\" : [ {   \n");
                samplevalue.Append("\"5\" : 10300, \n");
                samplevalue.Append("\"8\" : \"Revised Suppplier name\", \n");
                samplevalue.Append("\"CostCentreID\" : 300, \n");
                samplevalue.Append("\"EntityID\" : 301 \n");
                samplevalue.Append("} ] }\n"); 
                apiModel.SampleRequests.Add(mediatyp, samplevalue.ToString());
                return apiModel;
            }
            if (apiModel.ApiDescription.ID == "POSTapi/FinancialsV1/CreateSpentTransactions")
            {
                apiModel.SampleRequests.Clear();
                System.Net.Http.Headers.MediaTypeHeaderValue mediatyp = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                mediatyp.MediaType = "application/json";
                StringBuilder samplevalue = new StringBuilder();
                samplevalue.Append("{ \"SpentTransactions\" : [ {   \n");
                samplevalue.Append("\"15\" : \"INV-9001\", \n");
                samplevalue.Append("\"16\" : \"2014-06-21\", \n");
                samplevalue.Append("\"17\" : \"PO-10299\", \n");
                samplevalue.Append("\"18\" : \"Supplier Name\", \n");
                samplevalue.Append("\"19\" : \"Description\",");
                samplevalue.Append("\"20\" : \"100.60\", \n");
                samplevalue.Append("\"27\" : \"Spent client spec info\",\n");
                samplevalue.Append("\"CostCentreID\" : 300, \n");
                samplevalue.Append("\"EntityID\" : 301 \n");
                samplevalue.Append("} ] }\n"); 
                apiModel.SampleRequests.Add(mediatyp, samplevalue.ToString());
                return apiModel;
            }
            if (apiModel.ApiDescription.ID == "PUTapi/FinancialsV1/UpdateSpentTransactions")
            {
                apiModel.SampleRequests.Clear();
                System.Net.Http.Headers.MediaTypeHeaderValue mediatyp = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                mediatyp.MediaType = "application/json";
                StringBuilder samplevalue = new StringBuilder();
                samplevalue.Append("{ \"SpentTransactions\" : [ {   \n");
                samplevalue.Append("\"15\" : \"INV-9001\", \n");
                samplevalue.Append("\"17\" : \"PO-10299\", \n");
                samplevalue.Append("\"18\" : \"Revised Supplier Name\", \n");
                samplevalue.Append("\"CostCentreID\" : 300, \n");
                samplevalue.Append("\"EntityID\" : 301 \n");
                samplevalue.Append("} ] }\n");
                apiModel.SampleRequests.Add(mediatyp, samplevalue.ToString());
                return apiModel;
            }
            if (apiModel.ApiDescription.ID == "PUTapi/FinancialsV1/UpdateFinancialsMetadata")
            {
                apiModel.SampleRequests.Clear();
                System.Net.Http.Headers.MediaTypeHeaderValue mediatyp = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                mediatyp.MediaType = "application/json";
                StringBuilder samplevalue = new StringBuilder();
                samplevalue.Append("{ \"FinancialDetails\" : [ {   \n");
                samplevalue.Append("\"21\" : \"client specific metadata\", \n");
                samplevalue.Append("\"22\" : \"client specific metadata\", \n"); 
                samplevalue.Append("\"CostCentreID\" : 300, \n");
                samplevalue.Append("\"EntityID\" : 301 \n");
                samplevalue.Append("} ] }\n");
                apiModel.SampleRequests.Add(mediatyp, samplevalue.ToString());
                return apiModel;
            }
            if (apiModel.ApiDescription.ID == "GETapi/FinancialsV1/GetFinancialsByEntityID/{ID}")
            {
                apiModel.SampleResponses.Clear();
                System.Net.Http.Headers.MediaTypeHeaderValue mediatyp = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                mediatyp.MediaType = "application/json";
                StringBuilder samplevalue = new StringBuilder();
                samplevalue.Append("{ \"FinancialDetails\" : [ {   \n");
                samplevalue.Append("\"1\" : \"100.00\", \n");
                samplevalue.Append("\"2\" : \"50.50\", \n");
                samplevalue.Append("\"3\" : \"200.00\", \n");
                samplevalue.Append("\"4\" : \"Cost centre name\", \n");
                samplevalue.Append("\"21\" : \"client specific metadata\", \n");
                samplevalue.Append("\"22\" : \"client specific metadata\", \n");
                samplevalue.Append("\"CostCentreID\" : 300, \n");
                samplevalue.Append("\"EntityID\" : 301 \n");
                samplevalue.Append("} ] }\n");
                apiModel.SampleResponses.Add(mediatyp, samplevalue.ToString());
                return apiModel;
            }
            return apiModel;
        }
    }
}