﻿(function(ng, app) {
	"use strict";

	function muiDashBoardWidgetTopxActivityWidgetCtrl($scope, $timeout, $http, $compile, $resource, $cookies, $location, $window, TopxactivitywidgetService) {
		$scope.TopxActivitydivID = "Wizarddiv_" + $scope.WizardID;
		$scope.TopxActivitys = [];
		$scope.TopxNoOfItem = $scope.WizardNoOfItem;
		$timeout(function () { LoadTopxActivity(); }, 100);

		function LoadTopxActivity() {
			TopxactivitywidgetService.GetTopActivityByID(parseInt($cookies['UserId']), $scope.TopxNoOfItem).then(function(Gettopxactivity) {
				$scope.TopxActivitys = Gettopxactivity.Response;
				$('#' + $scope.TopxActivitydivID + '').parents('.box-content').find('.InProgress').remove();
			});
		}
	
		$scope.$on("$destroy", function() {
			RecursiveUnbindAndRemove($("[ng-controller='mui.DashBoardWidget.TopxActivityWidgetCtrl']"));
		});
		$scope.set_color = function(clr) {
			if (clr != null) return {
				'background-color': "#" + clr.toString().trim()
			};
			else return '';
		}
	}
	app.controller("mui.DashBoardWidget.TopxActivityWidgetCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies','$location', '$window', 'TopxactivitywidgetService', muiDashBoardWidgetTopxActivityWidgetCtrl]);
	function TopxactivitywidgetService($http, $q) {
	    $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
	        GetTopActivityByID: GetTopActivityByID
	    });
	    function GetTopActivityByID(UserID, Topx) { var request = $http({ method: "get", url: "api/Metadata/GetTopActivityByID/" + UserID + "/" + Topx, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
	    function handleError(response) {
	        if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
	        return ($q.reject(response.data.message));
	    }
	    function handleSuccess(response) { return (response.data); }
	}
	app.service("TopxactivitywidgetService", ['$http', '$q', TopxactivitywidgetService]);
})(angular, app);