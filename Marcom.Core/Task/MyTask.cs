﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Task.Interface;

namespace BrandSystems.Marcom.Core.Task
{
    public class MyTask:IMyTask
    {
        public int TaskId
        {
            get;
            set;
        }

        public int TaskTypeId
        {
            get;
            set;
        }

        public string TaskName
        {
            get;
            set;
        }

        public string TaskDescription
        {
            get;
            set;
        }

        public int EntityId
        {
            get;
            set;
        }

        public string EntityName
        {
            get;
            set;
        }

        public IList Path
        {
            get;
            set;
        }

        public int RoleId
        {
            get;
            set;
        }

        public string DueDate
        {
            get;
            set;
        }

        public int Noofdays
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public int UserID
        {
            get;
            set;
        }

        public int IsAdminTask
        {
            get;
            set;

        }

        public int TaskStatus
        {
            get;
            set;
        }
        public string ProgressCount
        {
            get;
            set;
        }
    }
}
