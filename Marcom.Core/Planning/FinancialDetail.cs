﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{


    public class FinancialDetail : IFinancialDetail
    {
        #region Member Variables
        public int FinId { get; set; }
        public decimal Spent { get; set; }
        public decimal InRequest { get; set; }
        public decimal ApprovedBudget { get; set; }
        public int CostCenterOwnerID { get; set; }
        public string EntityName { get; set; }
        public string CostCenterName { get; set; }
        public string UniqueKey { get; set; }
        public string ApprovedBudgetDate { get; set; }
        public decimal NonResAlloc { get; set; }
        public int EntityID { get; set; }
        public int ParentID { get; set; }
        public decimal ApprovedAllocatedAmount { get; set; }
        public decimal NonResPlan { get; set; }
        public decimal PlannedAmount { get; set; }
        public decimal SubAllocated { get; set; }
        public int TypeId { get; set; }
        public decimal SubPlanned { get; set; }
        public decimal Commited { get; set; }
        public decimal available { get; set; }
        public int CostCenterID { get; set; }
        public decimal BudgetDeviation { get; set; }
        public IList<IFinancialDetail> CostCenterList { get; set; }
        public IList<IFinancialMetadataAttributewithValues> DynamicData { get; set; }

        #endregion




    }

}
