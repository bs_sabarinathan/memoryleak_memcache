﻿(function (ng, app) {
    "use strict";

    function muiadminTemplateEnginesCtrl($scope, $location, $resource, $timeout, $translate, AdminService) {
        $scope.templateEngineList = [];
        getAllTemplateEngines();
        $scope.templEnginType = 0;
        $scope.ResourceUrl = "";
        $scope.clearFilter = function () {
            $scope.templEngineFilter = "";
            //$scope.tempEngfilter();
        };
        //$scope.filteringTempEngEventHandler = function (e) {
        //    $timeout(function () {
        //        $('.footable').trigger('footable_resize');
        //    }, 500);
        //};
        //$scope.tempEngfilter = function () {
        //    $scope.templEnginFilter = $scope.templEngineFilter;
        //    $timeout(function () {
        //        $("#templateEngineTbl").footable();
        //        $("#templateEngineTbl").trigger('footable_initialized');
        //        $("#templateEngineTbl").trigger('footable_resize');
        //        $("#templateEngineTbl").data('footable').redraw();
        //    }, 10);
        //}
        $scope.sortEnable = function sortEnable(columnID) {
            $scope.tempColID = columnID;
            if ($scope.colName == columnID) {
                if ($scope.IsSortAsc != true) {
                    $scope.IsSortAsc = true;
                    $scope.IsSortDsc = true;
                } else {
                    $scope.IsSortAsc = false;
                }
            } else {
                $scope.IsSortAsc = true;
            }
            $scope.colName = columnID;
        }

        function setSort() {
            if ($scope.tempColID != undefined) {
                $("#" + $scope.tempColID).click();
                if ($scope.tempColID.contains('th0')) $("#" + $scope.tempColID).click();
                if (!$scope.tempColID.contains('th0') && ($scope.IsSortAsc == false || $scope.IsSortDsc == true)) {
                    $("#" + $scope.tempColID).click();
                }
            }
        }
        $scope.OpenTempEngineDetailsPopUp = function () {
            ClearTemplateEngineDetails();
            $scope.showResourceUrl = false;
            $scope.buttonText = $translate.instant('LanguageContents.Res_15.Caption');
            $scope.popupName = ($translate.instant('LanguageContents.Res_5562.Caption'));
            $('#tempEngineDetails').modal('show');
        }

        function ClearTemplateEngineDetails() {
            $scope.templEnginType = 0;
            $scope.Templatedesc = "";
            $scope.ServerUrl = "";
            $scope.ResourceUrl = "";
        }
        $scope.saveUpdateTemplateEngine = function (ID) {
            var templEngine = {};
            templEngine.ID = ID == undefined ? 0 : ID;
            templEngine.templateEngineType = $scope.templEnginType;
            templEngine.Description = $scope.Templatedesc;
            templEngine.ServerUrl = $scope.ServerUrl;
            templEngine.ResourceUrl = $scope.ResourceUrl;
            AdminService.saveTemplateEngine(templEngine).then(function (tempEnginresult) {
                var templEngineID = tempEnginresult.Response;
                if (templEngineID != 0) {
                    if (ID == undefined) {
                        NotifySuccess($translate.instant('LanguageContents.Res_5560.Caption'));
                    }
                    if (ID != undefined) {
                        NotifySuccess($translate.instant('LanguageContents.Res_5568.Caption'));
                    }
                } else {
                    if (ID == undefined) {
                        NotifyError($translate.instant('LanguageContents.Res_5561.Caption'));
                    }
                    if (ID != undefined) {
                        NotifyError($translate.instant('LanguageContents.Res_5569.Caption'));
                    }
                }
                $("#tempEngineDetails").modal('hide');
                $scope.tempEngineID = 0;
                getAllTemplateEngines();
                $timeout(function () {
                    setSort();
                }, 50);
            })
        }
        $scope.tempEngineEdit = function (ID) {
            $scope.buttonText = $translate.instant('LanguageContents.Res_64.Caption');
            $scope.popupName = ($translate.instant('LanguageContents.Res_5562.Caption'));
            var edittemplEngine = $.grep($scope.templateEngineList, function (e) {
                return e.ID == ID
            })[0];
            var templEngine = {};
            $scope.tempEngineID = ID;
            if (edittemplEngine.EngineType == ($translate.instant('LanguageContents.Res_5566.Caption'))) {
                $scope.templEnginType = 1;
            }
            if (edittemplEngine.EngineType == ($translate.instant('LanguageContents.Res_5567.Caption'))) {
                $scope.templEnginType = 2;
            }
            $scope.Templatedesc = edittemplEngine.Description;
            $scope.ServerUrl = edittemplEngine.ServerURL;
            if ($scope.templEnginType == 2) {
                $scope.ResourceUrl = edittemplEngine.ResourceURL;
                $scope.showResourceUrl = true;
            }
            if ($scope.templEnginType != 2) {
                $scope.ResourceUrl = "";
                $scope.showResourceUrl = false;
            }
            $("#tempEngineDetails").modal('show');
        }
        $scope.tempEngineDelete = function (ID) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2050.Caption'), function (result) {
                if (result) {
                    AdminService.deleteTemplateEngine(ID).then(function (result) {
                        if (result.Response == true) {
                            var Dyndata = $.grep($scope.templateEngineList, function (e) {
                                return e.ID == ID
                            })[0];
                            if (Dyndata != undefined) {
                                $scope.templateEngineList.splice($.inArray(Dyndata, $scope.templateEngineList), 1);
                            }
                            //$scope.tempEngfilter();
                            NotifySuccess($translate.instant('LanguageContents.Res_5570.Caption'));
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_5571.Caption'));
                        }
                    });
                }
            });
        }

        function getAllTemplateEngines() {
            AdminService.getAllTemplateEngines().then(function (templEnginResult) {
                var templateEngines = templEnginResult.Response;
                if (templateEngines != null) {
                    for (var i = 0; i < templateEngines.length; i++) {
                        if (templateEngines[i].EngineType == 1) {
                            templateEngines[i].EngineType = ($translate.instant('LanguageContents.Res_5566.Caption'));
                        } else if (templateEngines[i].EngineType == 2) {
                            templateEngines[i].EngineType = ($translate.instant('LanguageContents.Res_5567.Caption'));
                        }
                    }
                }
                $scope.templateEngineList = templateEngines;
            })
        }
        $scope.getResourceUrl = function getResourceUrl(serverUrl) {
            if ($scope.templEnginType == 2) {
                if (serverUrl.contains('https://a1.2imagine.eu')) {
                    $scope.ResourceUrl = 'http://a1.2imagine.eu/2Imagine.Core/Handlers/ResourceProvider.ashx?sessionId=[sessionId]&uri=[uri]';
                    $scope.showResourceUrl = true;
                } else if (serverUrl.contains('https://s1.2imagine.eu')) {
                    $scope.ResourceUrl = 'http://s1.2imagine.eu/2Imagine.Core/Handlers/ResourceProvider.ashx?sessionId=[sessionId]&uri=[uri]';
                    $scope.showResourceUrl = true;
                } else {
                    $scope.showResourceUrl = false;
                }
            } else {
                $scope.showResourceUrl = false;
            }
        }
    }
    app.controller("mui.admin.TemplateEnginesCtrl", ['$scope', '$location', '$resource', '$timeout', '$translate', 'AdminService', muiadminTemplateEnginesCtrl]);
})(angular, app);