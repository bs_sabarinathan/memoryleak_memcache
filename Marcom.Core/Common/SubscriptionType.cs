using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// SubscriptionType object for table 'CM_SubscriptionType'.
	/// </summary>
	
	internal class SubscriptionType : ISubscriptionType 
	{
		#region Member Variables

		protected int _id;
		protected string _caption;
        protected bool _isappdefault;
        protected bool _isappmandatory;
        protected bool _ismaildefault;
        protected bool _ismailmandatory;
		
		
		#endregion
		
		#region Constructors
		public SubscriptionType() {}

        public SubscriptionType(string pCaption, bool pisAppDefault, bool pisAppMandatory, bool pisMailDefault, bool pisMailMandatory)
		{
			this._caption = pCaption;
            this._isappdefault = pisAppDefault;
            this._isappmandatory = pisAppMandatory;
            this._ismaildefault = pisMailDefault;
            this._ismailmandatory = pisMailMandatory;  
		}
				
		public SubscriptionType(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public string Caption
		{
			get { return _caption; }
			set 
			{
			 
			  _caption = value; 
			}
			
		}

        public virtual bool isAppDefault
        {
            get { return _isappdefault; }
            set {  _isappdefault = value; }

        }
        public virtual bool isAppMandatory
        {
            get { return _isappmandatory; }
            set {  _isappmandatory = value; }

        }
        public virtual bool isMailDefault
        {
            get { return _ismaildefault; }
            set {_ismaildefault = value; }

        }
        public virtual bool isMailMandatory
        {
            get { return _ismailmandatory; }
            set { _ismailmandatory = value; }

        }

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			SubscriptionType castObj = null;
			try
			{
				castObj = (SubscriptionType)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
