﻿(function (ng, app) {
    "use strict";
    function muicustomsearchresultCntrl($scope, $cookies, $resource, $compile, $window, $timeout, $location, $sce, $translate, CustomsearchresultService, $modal) {

        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var TenantFilePath1 = TenantFilePath.replace(/\\/g, "\/")
        var imagesrcpath = TenantFilePath;
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            TenantFilePath1 = cloudpath;
            imagesrcpath = cloudpath;
        }
        $scope.PageDetails = { PageNo: 1, 'Expanded': true };
        $scope.objectivetypes = [];
        $scope.isExpanded = false;

        var lastRoute = $location.path();
        var currentpath = lastRoute.replace("/mui/", '');

        $scope.isResultEmpty = false;

        $scope.Customsearchattributedata = [];
        $scope.SelectedTagWords = [];
        $scope.clearTagwords = false;
        $scope.CustomsearchAssetTypes = { "AssetTypes": [] };
        $scope.customAssetTypes = [];

        $timeout(function () {
            $scope.RowsPerPageSearch = (Math.floor($(".Customsearchresultcontent").width() / 176) * 3); //172 is the default height of the thumnail box.so here we hardcoded

        }, 50);

        $scope.fields = {};
        $scope.PageType = ["Production(s)", "Task(s)", "Asset(s)"];

        $timeout(function () { Loadresult(); }, 100);

        $timeout(function () {
            GetCustomFilterAttributes(0);
        }, 50);

        function GetCustomFilterAttributes(typeid) {
            CustomsearchresultService.GetCustomFilterAttributes(typeid)
                .then(
                    function (data) {
                        var res = data.Response;
                        if (res != undefined)
                            LoadCustomAttributes(res);

                    }
                );
        }

        $scope.selectedAssettypes = [];

        if ($scope.searchassetlibrary == true || $scope.searchassetlibrary == false) {

            CustomsearchresultService.GetAllExtensionTypesforDAM()
                .then(function (data) {
                    $scope.customAssetTypes = data.Response;

                    $scope.CustomsearchAssetTypes.AssetTypes = $scope.customAssetTypes.m_Item1;

                    $('#custom_assettype').html("");
                    var html = "";
                    $scope.CustomsearchAssetTypes["FilterExpand"] = false;

                    html += ' <div class="sr-dv-filterGroup">';
                    html += ' <div class="sr-dv-filterGroupHeader"> ';
                    html += '                       <h4 ng-click=\'makeAssettypeexpandcollapse()\'>' + $translate.instant('LanguageContents.Res_2507.Caption') + '</h4> ';
                    html += '                       <span class="clearfilter"> ';
                    html += '                            <span>|</span> ';
                    html += '                            <a href="javascript:void(0);" ng-click="customTypePerformClear()">' + $translate.instant('LanguageContents.Res_414.Caption') + '</a> ';
                    html += '                        </span> ';
                    html += "                        <i ng-click=\'makeAssettypeexpandcollapse()\' ng-class=\'{\"icon-collapse-alt\" :CustomsearchAssetTypes.FilterExpand,\"icon-expand-alt\": !CustomsearchAssetTypes.FilterExpand}\'></i> ";
                    html += '</div> ';
                    if ($scope.customAssetTypes.m_Item1 != null) {
                        if ($scope.customAssetTypes.m_Item1.length > 0) {
                            for (var l = 0, type; type = $scope.customAssetTypes.m_Item1[l++];) {
                                type["Isselectd"] = false;
                            }
                            $scope.CustomsearchAssetTypes["FilterText"] = "";
                            $scope.CustomsearchAssetTypes["AssetTypeFilterOption"] = $scope.customAssetTypes.m_Item1;
                            html += ' <div class="sr-dv-filterGroupItems" ng-show="CustomsearchAssetTypes.FilterExpand"> ';
                            html += '  <input class="searchBy" ng-model="CustomsearchAssetTypes.FilterText" type="text" placeholder="Search by Asset type" /> ';
                            html += '  <ul>';
                            html += ' <li ng-repeat="item in CustomsearchAssetTypes.AssetTypeFilterOption | filter:CustomsearchAssetTypes.FilterText"> ';
                            html += '  <label class="checkbox checkbox-custom "><input data-ng-model="item.Isselectd" ng-click=\"FilterbyAssettype($event,item.Id,item.damCaption)\" type="checkbox"><i ng-class="CustomsearchTypeoptionClass(item.Id)" class="cutomfiltercheck"></i>{{item.damCaption}}</label>';
                            html += ' </li> ';
                            html += ' </ul>';
                            html += '</div>';
                        }
                    }
                    html += " </div>";
                    html += ' <hr class="sr-dv-filtergroupSeparator" />';
                    $("#custom_assettype").html($compile(html)($scope));
                });
        }

        $scope.makeAssettypeexpandcollapse = function () {
            $scope.CustomsearchAssetTypes["FilterExpand"] = !$scope.CustomsearchAssetTypes["FilterExpand"];
        }

        $scope.customTypePerformClear = function () {

            $scope.CustomsearchAssetTypes["FilterText"] = "";
            var remainRecord = [];

            $scope.selectedAssettypes = [];

            $scope.PageNo = 1;
            if ($scope.selectedAttributes.length > 0)
                GetFilteredAssetsData();
            else
                Loadresult();

        }

        $scope.CustomsearchTypeoptionClass = function (id) {
            var selctionRecord = [];
            selctionRecord = $.grep($scope.selectedAssettypes, function (e) { return e.Id == id; });
            if (selctionRecord != undefined) {
                if (selctionRecord.length > 0)
                    return "checkbox checked";
                else
                    return "checkbox";
            }
            return "checkbox";
        }

        var isfilterRequest = true;
        $scope.assettypefiltercheckbox = false;

        $scope.FilterbyAssettype = function (event, typeid, name) {

            //if (isfilterRequest) {
            //    $scope.assettypefiltercheckbox = true;
            //    isfilterRequest = false;


            //$('#mediasloadingDownloadPageModel').modal('show');
            var checkbox = event.target;
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.selectedAssettypes, function (e) { return e.Id == typeid; });
                if (remainRecord.length == 0) {
                    $scope.selectedAssettypes.push({ "Id": typeid, "Name": name });
                }
            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.selectedAssettypes, function (e) { return e.Id == typeid; });
                if (remainRecord.length > 0) {
                    $scope.selectedAssettypes.splice($.inArray(remainRecord[0], $scope.selectedAssettypes), 1);
                }
            }
            $scope.PageDetails.PageNo = 1;
            if ($scope.selectedAssettypes.length > 0)
                GetFilteredAssetsData();
            else
                Loadresult();
            //}
        }

        $scope.CustomFilterbyAssettype = function (typeid) {

            var remainRecord = [];
            remainRecord = $.grep($scope.selectedAssettypes, function (e) { return e.Id == typeid; });
            if (remainRecord.length > 0) {
                $scope.selectedAssettypes.splice($.inArray(remainRecord[0], $scope.selectedAssettypes), 1);
            }

            if ($scope.selectedAssettypes.length == 0) {
                $scope.searchassetlibrary = false;
                $location.path("/marcommediabank");
            }

            $scope.PageDetails.PageNo = 1;
            if ($scope.selectedAssettypes.length > 0)
                GetFilteredAssetsData();
            else
                Loadresult();

        }


        $scope.AttributeOptions = { "Attributes": [] };


        $scope.SearchTypeOptions = [{ "Name": "All", "ActualName": "All" }, { "Name": "Productions", "ActualName": "Productions" },
            { "Name": "Tasks", "ActualName": "Task" }, { "Name": "Attachments", "ActualName": "Attachments" },
             { "Name": "Asset library", "ActualName": "Assetlibrary" }, { "Name": "Pages", "ActualName": "Pages" }, { "Name": "Tags", "ActualName": "Tags" }];

        $scope.$watch('searchObject.CurrentSearchType', function (v) {
            $scope.searchObject.SearchText = v;
        });

        $scope.btntypetracker = function (option) {


            $scope.searchObject.SearchText = option.Lable;
            $scope.searchObject.CurrentSearchType = option.Lable;
            $scope.searchObject.ActualSearchTerm = option.Actual_Type;
            $scope.searchObject.SearchTypeID = option.id;

            GetCustomFilterAttributes(option.id);

            $timeout(function () {
                $scope.selectedAttributes = [];
                $scope.PageDetails.PageNo = 1;
                if ($scope.selectedAttributes.length > 0)
                    GetFilteredAssetsData();
                else
                    Loadresult();

            }, 50)
        }

        $scope.RemoveSearchText = function () {

            if ($scope.searchassetlibrary == true && $scope.selectedAssettypes.length == 0) {
                $location.path("/marcommediabank");
            }

            $scope.CustomSearchText = "";
            $scope.searchtext = "";
            $scope.PageDetails.PageNo = 1;
            $scope.$emit("clearsearchtextforcefully");
            $timeout(function () { GetFilteredAssetsData(); }, 100);

        }

        $scope.RemoveSearchType = function () {
            if ($scope.searchObject.SearchText != "All") {
                $scope.searchObject.SearchText = "All";
                $scope.searchObject.CurrentSearchType = "All";
                $scope.searchObject.ActualSearchTerm = "All";
            }
        }

        $scope.makeexpandcollapse = function (id) {
            $scope.AttributeOptions["FilterExpand_" + id + ""] = !$scope.AttributeOptions["FilterExpand_" + id + ""];
        }

        $scope.PerformClear = function (id) {
            $scope.AttributeOptions["FilterText_" + id + ""] = "";
            var remainRecord = [];
            remainRecord = $.grep($scope.selectedAttributes, function (e) { return e.AttributeID == id; });
            if (remainRecord.length > 0) {
                $scope.selectedAttributes.splice($.inArray(remainRecord[0], $scope.selectedAttributes), 1);
            }
            var selctionRecord = [];
            selctionRecord = $.grep($scope.AttributeOptions.Attributes, function (e) { return e.AttributeID == id; })[0];
            if (selctionRecord != undefined) {
                for (var j = 0, attr; attr = selctionRecord.Options[j++];) {
                    attr["Isselectd"] = false;
                }
            }
            if (remainRecord.length > 0) {
                if (remainRecord[0].AttributeTypeID == 17) {
                    $timeout(function () {
                        $scope.clearTagwords = true;
                    }, 100);

                }
            }
            $scope.PageDetails.PageNo = 1;
            if ($scope.selectedAttributes.length > 0)
                GetFilteredAssetsData();
            else
                Loadresult();
        }

        $scope.optionClass = function (attrid, optionid) {
            var selctionRecord = [];
            selctionRecord = $.grep($scope.selectedAttributes, function (e) { return e.AttributeID == attrid; });
            if (selctionRecord != undefined) {
                if (selctionRecord.length > 0) {
                    var valueIds = [];
                    valueIds = $.grep(selctionRecord[0].ValuesIDArr, function (e) { return e == optionid; });
                    if (valueIds != undefined)
                        if (valueIds.length > 0)
                            return "checkbox checked";
                        else
                            return "checkbox";
                    else
                        return "checkbox";
                }
            }
            return "checkbox";
        }
        CustomsearchresultService.GetObjectiveEntityType(false).then(function (ResultPlansList) {
            if (ResultPlansList.Response != null && ResultPlansList.Response.length > 0) {
                var objtyperesult = ResultPlansList.Response;
                for (var i = 0; i < objtyperesult.length ; i++) {
                    $scope.objectivetypes.push(objtyperesult[i].Id);
                }
            }
            else {
            }
        });
        function LoadCustomAttributes(res) {
            $scope.AttributeOptions.Attributes = res;
            $("#customsearch-AssetAttributes").html("");
            var html = "";
            for (var i = 0, type; type = res[i++];) {
                if (type.AttributeTypeID != 17) {
                    $scope.AttributeOptions["FilterExpand_" + type.AttributeID + ""] = false;
                    html += ' <div class="sr-dv-filterGroup">';
                    html += ' <div class="sr-dv-filterGroupHeader"> ';
                    html += '                       <h4 ng-click=\'makeexpandcollapse(' + type.AttributeID + ')\'>' + type.AttributeCaption + '</h4> ';
                    html += '                       <span class="clearfilter"> ';
                    html += '                            <span>|</span> ';
                    html += '                            <a href="javascript:void(0);" ng-click="PerformClear(' + type.AttributeID + ')">Clear</a> ';
                    html += '                        </span> ';
                    html += "                        <i ng-click=\"makeexpandcollapse(" + type.AttributeID + ")\"  ng-class=\'{\"icon-collapse-alt\" : AttributeOptions.FilterExpand_" + type.AttributeID + ",\"icon-expand-alt\": !AttributeOptions.FilterExpand_" + type.AttributeID + "}\'></i> ";
                    html += '</div> ';
                    if (type.Options != null) {
                        if (type.Options.length > 0) {
                            for (var l = 0, opt; opt = type.Options[l++];) {
                                opt["Isselectd"] = false;
                            }
                            $scope.AttributeOptions["FilterText_" + type.AttributeID + ""] = "";
                            $scope.AttributeOptions["FilterOption_" + type.AttributeID + ""] = type.Options;
                            $scope.AttributeOptions["FilterProp_" + type.AttributeID + ""] = type;
                            html += ' <div class="sr-dv-filterGroupItems" ng-show="AttributeOptions.FilterExpand_' + type.AttributeID + '"> ';
                            html += '  <input class="searchBy" ng-model="AttributeOptions.FilterText_' + type.AttributeID + '" type="text" placeholder="Search by ' + type.AttributeCaption + '" /> ';
                            html += '  <ul>';
                            html += ' <li ng-repeat="item in AttributeOptions.FilterOption_' + type.AttributeID + ' | filter:AttributeOptions.FilterText_' + type.AttributeID + ' " "> ';
                            html += '  <label class="checkbox checkbox-custom "><input data-ng-model="item.Isselectd" attributecaption="{{AttributeOptions.FilterProp_' + type.AttributeID + '.AttributeCaption}}" optioncaption="{{item.Caption}}" ng-click=\"FilterbyAttributeSelection($event,' + $scope.AttributeOptions["FilterProp_" + type.AttributeID + ""].AttributeID + ',' + $scope.AttributeOptions["FilterProp_" + type.AttributeID + ""].AttributeTypeID + ',item.Id,' + $scope.AttributeOptions["FilterProp_" + type.AttributeID + ""].Level + ')\" type="checkbox"><i ng-class="optionClass(' + type.AttributeID + ',item.Id)"></i>{{item.Caption}}</label>';
                            html += ' </li> ';
                            html += ' </ul>';
                            html += '</div>';
                        }
                    }
                    html += " </div>";
                    // html += ' <hr class="sr-dv-filtergroupSeparator" />';
                }

                if (type.AttributeTypeID == 17) {




                    $scope.fields["ListTagwords_load" + type.AttributeID] = [];
                    $scope.tempscope = [];
                    $scope.AttributeOptions["FilterExpand_" + type.AttributeID + ""] = false;
                    html += ' <div class="sr-dv-filterGroup">';
                    html += ' <div class="sr-dv-filterGroupHeader"> ';
                    html += '                       <h4 ng-click=\'makeexpandcollapse(' + type.AttributeID + ')\'>' + type.AttributeCaption + '</h4> ';
                    html += '                       <span class="clearfilter"> ';
                    html += '                            <span>|</span> ';
                    html += '                            <a href="javascript:void(0);" ng-click="PerformClear(' + type.AttributeID + ')">Clear</a> ';
                    html += '                        </span> ';
                    html += "                        <i ng-click=\"makeexpandcollapse(" + type.AttributeID + ")\"  ng-class=\'{\"icon-collapse-alt\" : AttributeOptions.FilterExpand_" + type.AttributeID + ",\"icon-expand-alt\": !AttributeOptions.FilterExpand_" + type.AttributeID + "}\'></i> ";
                    html += '</div> ';
                    if (type.Options != null) {
                        if (type.Options.length > 0) {
                            for (var l = 0, opt; opt = type.Options[l++];) {
                                opt["Isselectd"] = false;
                                if ($scope.searchObject.ActualSearchTerm == "Tags")
                                    if (type.Caption != undefined) {
                                        if (type.Caption.toLowerCase() == $scope.searchtext) {
                                            $scope.tempscope.push({ "Id": type.Id, "Caption": type.Caption.toLowerCase() });
                                        }
                                    }

                            }
                            $scope.AttributeOptions["FilterText_" + type.AttributeID + ""] = "";
                            $scope.AttributeOptions["FilterOption_" + type.AttributeID + ""] = type.Options;
                            $scope.AttributeOptions["FilterProp_" + type.AttributeID + ""] = type;
                            html += ' <div class="sr-dv-filterGroupItems" ng-show="AttributeOptions.FilterExpand_' + type.AttributeID + '"> ';
                            $scope.tempscope = [];
                            html += "<div class=\"tagwordsDiv\">";
                            html += "<directive-tagwords item-attrid = \"" + type.AttributeID + l + "\" item-cleartag=\"clearTagwords\" searchfrtag=\"alert(" + type.AttributeID + ",optionid,isselected)\"  item-show-hide-progress =\"null\" item-tagword-id=\"SelectedTagWords\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                            html += "</div>";
                            html += '</div>';
                        }
                    }
                    html += " </div>";

                }
                html += ' <hr class="sr-dv-filtergroupSeparator" />';

            }

            $("#customsearch-AssetAttributes").html($compile(html)($scope));
        }

        $scope.CustomCountObjects = { "Totalcount": 0, "Production": 0, "Task": 0, "Asset": 0 };

        $scope.Pathinfo = {};

        $scope.CustomSearchText = $scope.searchtext != undefined ? $scope.searchtext : "";

        $scope.PagingData = {};

        function CreateProductionRecord(obj) {
            var entitytypeObj = {}, attributerelations = [], attrwithValues = {}, CurrentPath = [], html = '';

            $($.parseXML(obj.Entitytypedetails)).find('p').each(function () {
                entitytypeObj.colorcode = $(this).attr('c'), entitytypeObj.shortdesc = $(this).attr('sd'), entitytypeObj.typeid = $(this).attr('t');
                entitytypeObj.ParentID = $(this).attr('p'), entitytypeObj.IsTask = $(this).attr('IsT'), entitytypeObj.ModuleId = $(this).attr('m');
            });
            $($.parseXML(obj.AttributeDetails)).find('p').each(function () {
                attributerelations.push({
                    "Id": $(this).attr('id'), "AttrID": $(this).attr('aid'),
                    "Caption": $(this).attr('l'), "EntityTypeID": $(this).attr('t'),
                    "MetadadaCaption": $(this).attr('C')
                });
            });

            $($.parseXML(obj.Path)).find('p').each(function () {
                CurrentPath.push({ "ID": $(this).attr('i'), "TypeID": $(this).attr('t'), "Name": $(this).attr('n'), "ParentID": $(this).attr('p'), "IsTask": $(this).attr('IsT') });
            });

            var htmlpath = '<ul class="breadcrumb">';
            for (var p = 0, pa; pa = CurrentPath[p++];) {
                htmlpath += '<li data-highlightclass="highlight-circle" data-keyword="' + $scope.searchtext + '"  class="active keywordmatchtrext">';
                if (obj.SearchType == "Productions") {
                    var Isobjective = false;
                    for (var i = 0; i < $scope.objectivetypes.length ; i++) {
                        if ($scope.objectivetypes[i] == pa.TypeID) {
                            Isobjective = true;
                        }
                    }
                    if (pa.TypeID != 1)
                        htmlpath += '<a  ng-click="IsActiveEntity(' + pa.ID + ',' + pa.TypeID + ',' + pa.ParentID + ',' + pa.IsTask + ',' + entitytypeObj.ModuleId + ',$event)" href="JavaScript: void(0);"  data-IsObjective=' + Isobjective + ' >' + pa.Name + ' </a>';
                    else
                        htmlpath += '<a  ng-click="IsActiveEntity(' + pa.ParentID + ',' + pa.TypeID + ',' + pa.ParentID + ',' + pa.IsTask + ',' + entitytypeObj.ModuleId + ',$event)" href="JavaScript: void(0);" data-IsObjective=' + Isobjective + ' >' + pa.Name + ' </a>';

                }
                if (obj.SearchType == "Task") {
                    htmlpath += '<a  ng-click="IsActiveEntity(' + pa.ID + ',' + pa.TypeID + ',' + pa.ParentID + ',' + pa.IsTask + ',' + entitytypeObj.ModuleId + ',$event)" href="JavaScript: void(0);">' + pa.Name + ' </a>';
                }
                if (obj.SearchType == "CmsNavigation") {
                    htmlpath += '<a  ng-click="IsActiveEntity(' + pa.ID + ',' + pa.TypeID + ',' + pa.ParentID + ',' + pa.IsTask + ',' + entitytypeObj.ModuleId + ',$event)" href="JavaScript: void(0);">' + pa.Name + ' </a>';
                }
                if (p < CurrentPath.length)
                    htmlpath += '<span class="divider">/</span>';
                htmlpath += ' </li>';
            }
            htmlpath += '</ul>';

            var htmlsearchAttrtempl = '';

            for (var a = 0, val; val = obj.AttributeValues[a++];) {
                attrwithValues["Attr_" + val.AttributeId] = val.MatchedValues;
                var src_str = val.MatchedValues;
                var words = $scope.searchtext;
                //words = words.replace(/(\s+)/, "(<[^>]+>)*$1(<[^>]+>)*");

                var settings = { caseSensitive: false, wordsOnly: false };

                if (words.constructor === String) {
                    words = [words];
                }
                words = jQuery.grep(words, function (word, i) {
                    return word != '';
                });
                words = jQuery.map(words, function (word, i) {
                    return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
                });
                if (words.length == 0) { return this; };

                var flag = settings.caseSensitive ? "" : "i";
                var patternstr = "(" + words.join("|") + ")";
                if (settings.wordsOnly) {
                    pattern = "\\b" + patternstr + "\\b";
                }
                var pattern = new RegExp(patternstr, flag);

                //var pattern = new RegExp("(" + term + ")", "gi");

                src_str = src_str.replace(pattern, "<mark>$1</mark>");
                src_str = src_str.replace(/(<mark>[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/, "$1</mark>$2<mark>$4<mark>$5</mark>$6<mark>$7</mark>");
                if (src_str.contains("mark")) {
                    htmlsearchAttrtempl += ' <div>';
                    htmlsearchAttrtempl += '<b>' + val.AttributeCaption + ':</b> ' + src_str + '';
                    htmlsearchAttrtempl += '</div>';
                }

            }
            var colorcode = entitytypeObj.colorcode != null ? entitytypeObj.colorcode : "", shortdesc = entitytypeObj.shortdesc != null ? entitytypeObj.shortdesc : "";
            html += '<li> ';
            html += '<span class="eicon-l pull-left color-white" style="background-color: #' + colorcode + '">' + shortdesc + '</span>';
            html += '<div class="block">';
            html += '<span class="arrow-left"></span>';
            html += '<span class="name">' + obj.Name + '</span>';
            html += '<div class="body">';
            html += '' + htmlpath + '';
            html += htmlsearchAttrtempl;
            html += '</div>';
            html += '</div>';
            html += '</li>';
            if (obj.SearchType == "Productions")
                $("#productionsearch-results").append(
                                       $compile(html)($scope));
            else if (obj.SearchType == "Task")
                $("#Tasks-results").append(
                                 $compile(html)($scope));
            else if (obj.SearchType == "CmsNavigation")
                $("#CMS-results").append(
                                 $compile(html)($scope));


        }

        $scope.AssetChkClass = {};
        $scope.AssetSelection = {};
        $scope.AssetSelectionClass = {};
        function LoadAssetThimbnailView(res) {
            var html = '';
            if ($scope.PageDetails.PageNo == 1) {
                $("#Assets-results").html(
                                      $compile(html)($scope));
                $("#Assets-library-results").html(
                                     $compile(html)($scope));
                $scope.AssetSearchObj.AssetSearchfiles = [];
                $scope.AssetSearchObj.AssetDynamicData = [];
                $scope.AssetSelectionClass = {};
            }

            if (res != null) {
                var assets = [];
                assets = res.AssetDetails;
                if (assets != null) {
                    for (var a = 0, fil; fil = assets[a++];) {
                        $scope.AssetSearchObj.AssetSearchfiles.push(fil);
                    }
                }

                var DynamicData = res.attrRelation, data;
                if (DynamicData != null) {
                    for (var d = 0, dt; dt = assets[d++];) {
                        data = DynamicData[dt.AssetUniqueID];
                        if (data != null)
                            $scope.AssetSearchObj.AssetDynamicData.push({ "AssetID": dt.AssetUniqueID, "Data": data });
                    }
                }

                for (var i = 0, asset; asset = assets[i++];) {
                    $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                    var assetselection = "";
                    if (IsAssetSelected(asset.AssetUniqueID)) {
                        assetselection = "th-Selection selected";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                    }

                    else {
                        assetselection = $scope.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-Selection selected" : "th-Selection";
                        $scope.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                    }
                    $scope.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                    html = "";
                    html += '<div class="th-Box"  data-tooltiptype="assetthumbnailview" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '>';
                    html += '<div>';
                    html += '<ul id="thumbnailActionMenu" style="display: none;" class="dropdown-menu contexMenu">';
                    html += '<li><a tabindex="-1" href="javascript:void(0)" ng-click="PerformAssetOperation(2)">Copy</a></li></ul>';
                    html += '</div>';
                    // html += '<div ng-class=\"AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-Selection">';
                    html += '<div id="select_' + i + '"  class="th-Selection">';
                    html += '<div class="th-chkBx">';
                    html += '<i class="icon-stop"></i>';
                    html += '<label class="checkbox checkbox-custom">';
                    html += '<input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + asset.AssetUniqueID + '" type="checkbox" />';
                    html += '<i  id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i>';
                    html += '</label>';
                    html += '</div>';
                    html += '<div class="th-options">';
                    html += '<i class="icon-stop"></i>';
                    html += '<i id="optionsIcon_' + i + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" context="customsearchActionMenu"></i>';
                    html += '</div>';
                    // html += '</div>';
                    html += '<div class="th-Block" >';
                    html += '<div class="th-ImgBlock">';
                    html += '<div  class="th-ImgContainer" damthumbnailviewtooltip data-tooltiptype="assetthumbnailview" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '  ng-click=\"Editasset(' + asset.AssetUniqueID + ')\">';

                    if (asset.Category == 0) {
                        if (asset.Extension != null) {

                            html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid.toLowerCase() + ' src="' + imagesrcpath + 'DAMFiles/Preview/Small_' + asset.FileGuid.toLowerCase() + '.jpg" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg\'" />';
                            //if (asset.ProcessType > 0) {
                            //    if (asset.Status == 2)
                            //        html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="DAMFiles/Preview/Small_' + asset.FileGuid + '.jpg" alt="Image"   onerror="this.onerror=null;this.src=\'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length) + '.png\'" />';
                            //    else if (asset.Status == 1 || asset.Status == 0)
                            //        html += '<img class="loadingImg" id=' + asset.ActiveFileID + '  data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'DAMFiles/Original/NoPreview1.jpg\'" />';
                            //    else if (asset.Status == 3) {
                            //        html += '<img src="DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length) + '.png' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + '  onerror="this.onerror=null;this.src=\'DAMFiles/Original/NoPreview1.jpg\'" />';

                            //    }
                            //}
                            //else {
                            //    html += '<img src="DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length) + '.png' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + '  onerror="this.onerror=null;this.src=\'DAMFiles/Original/NoPreview1.jpg\'" />';
                            //}
                        }
                        else
                            html += '<img src="' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid.toLowerCase() + ' onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                    }
                    else if (asset.Category == 1) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid.toLowerCase() + ' src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/BLANK.jpg" alt="Image"   onerror="this.onerror=null;this.src=\'DAMFiles/Original/NoPreview1.jpg\'" />';
                    }
                    else if (asset.Category == 2) {
                        html += '<img id="file_' + asset.AssetUniqueID + '"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid.toLowerCase() + ' src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/LINK.jpg" alt="Image"   onerror="this.onerror=null;this.src=\'DAMFiles/Original/NoPreview1.jpg\'" />';
                    }

                    html += '</div>';
                    html += '</div>';
                    html += '<div class="th-DetailBlock">';
                    html += '<div class="th-ActionButtonContainer">';
                    html += '<span>';
                    html += '<i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i>';
                    html += '</span>';
                    html += '</div>';
                    html += '<div class="th-detail-eIconContainer">';
                    html += '<span class="th-eIcon" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                    html += '</div>';
                    html += '<div class="th-Details">';
                    html += GenereteAssetDetailBlock(asset);
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';

                    if (asset.IsPublish == "0") {
                        $("#Assets-results").append(
                                       $compile(html)($scope));
                    }
                    else {
                        $("#Assets-library-results").append(
                                      $compile(html)($scope));
                        $scope.assetlibraryaccess.push(asset);
                    }

                }



            }
        }

        $scope.assetlibraryaccess = [];

        function GenereteAssetDetailBlock(asset) {
            var attrRelation = [];
            attrRelation = $.grep($scope.SearchAssetLibrarySettings.ThumbnailSettings, function (e) {
                return e.assetType == asset.AssetTypeid;
            });

            var html = '';
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {

                    var cls = attr.ID == 68 ? "th-infoMain" : "th-infoSub";
                    if ($scope.AssetSearchObj.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetSearchObj.AssetDynamicData, function (e) {
                            return e.AssetID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = {}; attrval = data[0].Data;
                            if (attrval != undefined) {

                                if (attrval[attr.ID] != "" && attrval[attr.ID] != null && attrval[attr.ID] != undefined)
                                    if (attrval[attr.ID] != '1900-01-01')
                                        html += '<span class="' + cls.toString() + '">' + attrval[attr.ID] + '</span>';
                            }
                        }
                    }
                }
            }
            return html;
        }

        $scope.LoadAssetthumbnailViewMetadata = function (qtip_id, assetid, backcolor, shortdesc) {
            $scope.tempEntityTypeTreeText = '';
            if (assetid != null) {
                CustomsearchresultService.GetAttributeDetails(assetid)
                  .then(
                      function (attrList) {
                          $scope.ActiveVersion = $.grep(attrList.Response.Files, function (e) { return e.ID == attrList.Response.ActiveFileID; })[0];
                          var html = '';
                          html += '<div class="th-AssetInfoTooltip">';
                          html += DrawThumbnailBlock(attrList);
                          html += '</div> ';

                          $('#' + qtip_id).html(html);
                      });
            }
        };

        function DrawThumbnailBlock(attrList) {
            var tooltipattrs = [];
            var html = '';
            $scope.dyn_Cont = '';
            refreshdata();
            $scope.Customsearchattributedata = [];
            if (attrList.Response.AttributeData != null) {
                if ($scope.SearchAssetLibrarySettings.ThumbnailSettings.length > 0) {
                    tooltipattrs = $.grep($scope.SearchAssetLibrarySettings.ThumbnailSettings, function (e) { return e.assetType == attrList.Response.AssetTypeid });
                }
                if (tooltipattrs.length > 0) {
                    for (var l = 0; l < tooltipattrs.length; l++) {
                        var isexist = $.grep(attrList.Response.AttributeData, function (e) { return e.ID == tooltipattrs[l].ID })[0];
                        if (isexist != null)
                            $scope.Customsearchattributedata.push(isexist);
                    }

                }
                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Asset name</span><span class="th-AssetInfoValue">' + attrList.Response.Name + '</span></div>';

                for (var i = 0; i < $scope.Customsearchattributedata.length; i++) {
                    if ($scope.Customsearchattributedata[i].TypeID == 1 && $scope.Customsearchattributedata[i].IsSpecial == false) {  // singleLine and Multiline TextBoxes
                        $scope.fields["SingleLineTextValue_" + $scope.Customsearchattributedata[i].ID] = "-";
                        if ($scope.Customsearchattributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.Customsearchattributedata[i].ID] = $('<div />').html($scope.Customsearchattributedata[i].Caption).text();
                        }
                        //html += '<div  class=\"control-group\"><label class=\"control-label\"\>' + $scope.Customsearchattributedata[i].Lable + '</label><div class=\"controls\"> <label class="control-label">' + $scope.fields["SingleLineTextValue_" + $scope.Customsearchattributedata[i].ID] + '</label></div></div>';
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.Customsearchattributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.Customsearchattributedata[i].ID] + '</span></div>';
                    }
                    else if ($scope.Customsearchattributedata[i].TypeID == 1 && $scope.Customsearchattributedata[i].IsSpecial == true) {
                        $scope.fields["SingleLineTextValue_" + $scope.Customsearchattributedata[i].ID] = "-";
                        if ($scope.Customsearchattributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.Customsearchattributedata[i].ID] = $('<div />').html($scope.Customsearchattributedata[i].Value).text();
                        }
                        //html += '<div class=\"control-group\"><label class=\"control-label\"\>' + 'Asset Name' + '</label><div class=\"controls\"> <label class="control-label">' + $scope.fields["SingleLineTextValue_" + $scope.Customsearchattributedata[i].ID] + '</label></div></div>';
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Asset Name</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.Customsearchattributedata[i].ID] + '</span></div>';
                    }
                    else if ($scope.Customsearchattributedata[i].TypeID == 2) {
                        $scope.fields["SingleLineTextValue_" + $scope.Customsearchattributedata[i].ID] = "-";
                        if ($scope.Customsearchattributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.Customsearchattributedata[i].ID] = $('<div />').html($scope.Customsearchattributedata[i].Caption).text();
                        }
                        //html += '<div class=\"control-group\"><label class=\"control-label\"\>' + $scope.Customsearchattributedata[i].Lable + '</label><div class=\"controls\"> <label class="control-label">' + $scope.fields["SingleLineTextValue_" + $scope.Customsearchattributedata[i].ID] + '</label></div></div>';
                        html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.Customsearchattributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["SingleLineTextValue_" + $scope.Customsearchattributedata[i].ID] + '</span></div>';
                    }
                    else if ($scope.Customsearchattributedata[i].TypeID == 3) {
                        if ($scope.Customsearchattributedata[i].ID == SystemDefiendAttributes.Owner && $scope.Customsearchattributedata[i].IsSpecial == true) {
                            if ($scope.Customsearchattributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.Customsearchattributedata[i].ID] = $scope.Customsearchattributedata[i].Caption[0];
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.Customsearchattributedata[i].ID] = $scope.Customsearchattributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                //html += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.Customsearchattributedata[i].Lable + '</label> <div class="controls"> <label class="control-label">' + $scope.Customsearchattributedata[i].Caption[0] + '</label></div></div>';
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.Customsearchattributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.Customsearchattributedata[i].Caption[0] + '</span></div>';
                            }
                            else {
                                $scope.fields["NormalDropDown_" + $scope.Customsearchattributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.Customsearchattributedata[i].ID] = $scope.Customsearchattributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                //html += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.Customsearchattributedata[i].Lable + '</label>';
                                //html += '<div class="controls"><span>' + $scope.Customsearchattributedata[i].Lable + '</span>';
                                //html += '</div></div>';
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.Customsearchattributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.Customsearchattributedata[i].Lable + '</span></div>';
                            }
                        } else {
                            if ($scope.Customsearchattributedata[i].Caption[0] != undefined) {
                                if ($scope.Customsearchattributedata[i].Caption[0].length > 1) {
                                    $scope.fields["NormalDropDown_" + $scope.Customsearchattributedata[i].ID] = $scope.Customsearchattributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.Customsearchattributedata[i].ID] = $scope.Customsearchattributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    //html += '<div  class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.Customsearchattributedata[i].Lable + '</label> <div class="controls"> <label class="control-label">' + $scope.Customsearchattributedata[i].Caption[0] + '</label></div></div>';
                                    html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.Customsearchattributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.Customsearchattributedata[i].Caption[0] + '</span></div>';
                                }
                            }
                            else {
                                $scope.fields["NormalDropDown_" + $scope.Customsearchattributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.Customsearchattributedata[i].ID] = $scope.Customsearchattributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                //html += '<div  class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.Customsearchattributedata[i].Lable + '</label> <div class="controls"> <label class="control-label">' + $scope.fields["NormalDropDown_" + $scope.Customsearchattributedata[i].ID] + '</label></div></div>';
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.Customsearchattributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["NormalDropDown_" + $scope.Customsearchattributedata[i].ID] + '</span></div>';
                            }
                        }
                    }
                    else if ($scope.Customsearchattributedata[i].TypeID == 4) {
                        if ($scope.Customsearchattributedata[i].Caption[0] != undefined) {
                            if ($scope.Customsearchattributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.Customsearchattributedata[i].ID] = $scope.Customsearchattributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.Customsearchattributedata[i].ID] = $scope.Customsearchattributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                //html += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.Customsearchattributedata[i].Lable + '</label> <div class="controls"> <label class="control-label">' + $scope.Customsearchattributedata[i].Caption + '</label></div></div>';
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.Customsearchattributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.Customsearchattributedata[i].Caption + '</span></div>';
                            }
                        }
                        else {

                            $scope.fields["NormalMultiDropDown_" + $scope.Customsearchattributedata[i].ID] = "-";

                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.Customsearchattributedata[i].ID] = $scope.Customsearchattributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            //html += '<div  class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.Customsearchattributedata[i].Lable + '</label> <div class="controls"> <label class="control-label">' + $scope.fields["NormalMultiDropDown_" + $scope.Customsearchattributedata[i].ID] + '</label></div></div>';
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.Customsearchattributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["NormalMultiDropDown_" + $scope.Customsearchattributedata[i].ID] + '</span></div>';
                        }
                    }
                    else if ($scope.Customsearchattributedata[i].TypeID == 17) {
                        if ($scope.Customsearchattributedata[i].Caption[0] != undefined) {
                            if ($scope.Customsearchattributedata[i].Caption.length > 1) {
                                $scope.fields["TagWordsCaption_" + $scope.Customsearchattributedata[i].ID] = $scope.Customsearchattributedata[i].Caption;

                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.Customsearchattributedata[i].ID] = $scope.Customsearchattributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                //html += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.Customsearchattributedata[i].Lable + '</label> <div class="controls"> <label class="control-label">' + $scope.Customsearchattributedata[i].Caption + '</label></div></div>';
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.Customsearchattributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.Customsearchattributedata[i].Caption + '</span></div>';
                            }
                        }
                        else {

                            $scope.fields["TagWordsCaption_" + $scope.Customsearchattributedata[i].ID] = "-";

                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.Customsearchattributedata[i].ID] = $scope.Customsearchattributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            //html += '<div  class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.Customsearchattributedata[i].Lable + '</label> <div class="controls"> <label class="control-label">' + $scope.fields["TagWordsCaption_" + $scope.Customsearchattributedata[i].ID] + '</label></div></div>';
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.Customsearchattributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["TagWordsCaption_" + $scope.Customsearchattributedata[i].ID] + '</span></div>';
                        }
                    }
                    else if ($scope.Customsearchattributedata[i].TypeID == 10) {
                        var inlineEditabletitile = $scope.Customsearchattributedata[i].Caption;

                        if ($scope.Customsearchattributedata[i].Value == "-") {
                            $scope.fields["PeriodStartEndDate_" + $scope.Customsearchattributedata[i].ID] = '-';
                            $scope.fields["PeriodDateDesc_" + $scope.Customsearchattributedata[i].ID] = "-";
                            //html += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.Customsearchattributedata[i].Lable + '</label>';
                            //html += '<div class="controls">';
                            ////+ $scope.fields["PeriodStartDate_" + $scope.Customsearchattributedata[i].Value[j].Id] +
                            //html += '<label class=\"control-label\" for=\"label\">' + $scope.fields["PeriodStartEndDate_" + $scope.Customsearchattributedata[i].ID];
                            //html += '</label>';
                            //html += '</div>';
                            //html += '</div>';
                            //html += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                            //html += '<div class="controls">';
                            //html += '<label class=\"control-label\" for=\"label\">' + $scope.fields["PeriodDateDesc_" + $scope.Customsearchattributedata[i].ID] + '</label>';
                            //html += '</div>';
                            //html += '</div>';
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.Customsearchattributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodStartEndDate_" + $scope.Customsearchattributedata[i].ID] + '</span></div>';
                            html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Comment ' + inlineEditabletitile + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodDateDesc_" + $scope.Customsearchattributedata[i].ID] + '</span></div>';
                        }
                        else {
                            for (var j = 0; j < $scope.Customsearchattributedata[i].Value.length; j++) {

                                var datStartUTCval = "";
                                var datstartval = "";
                                var datEndUTCval = "";
                                var datendval = "";

                                datStartUTCval = $scope.Customsearchattributedata[i].Value[j].Startdate.substr(6, ($scope.Customsearchattributedata[i].Value[j].Startdate.indexOf('+') - 6));
                                datstartval = new Date(parseInt(datStartUTCval));

                                datEndUTCval = $scope.Customsearchattributedata[i].Value[j].EndDate.substr(6, ($scope.Customsearchattributedata[i].Value[j].EndDate.indexOf('+') - 6));
                                datendval = new Date(parseInt(datEndUTCval));
                                $scope.fields["PeriodStartDate_" + $scope.Customsearchattributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                $scope.fields["PeriodEndDate_" + $scope.Customsearchattributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));
                                if ($scope.Customsearchattributedata[i].Value[j].Description == undefined) {

                                    $scope.fields["PeriodDateDesc_" + $scope.Customsearchattributedata[i].Value[j].Id] = "-";
                                } else {
                                    $scope.fields["PeriodDateDesc_" + $scope.Customsearchattributedata[i].Value[j].Id] = $scope.Customsearchattributedata[i].Value[j].Description;

                                }
                                //html += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.Customsearchattributedata[i].Lable + '</label>';
                                //html += '<div class="controls">';
                                ////+ $scope.fields["PeriodStartDate_" + $scope.Customsearchattributedata[i].Value[j].Id] +
                                //html += '<label class=\"control-label\" for=\"label\">' + $scope.fields["PeriodStartDate_" + $scope.Customsearchattributedata[i].Value[j].Id];
                                //html += ' to ' + $scope.fields["PeriodEndDate_" + $scope.Customsearchattributedata[i].Value[j].Id] + '</label>';
                                //html += '</div>';
                                //html += '</div>';
                                //html += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                                //html += '<div class="controls">';
                                //html += '<label class=\"control-label\" for=\"label\">' + $scope.fields["PeriodDateDesc_" + $scope.Customsearchattributedata[i].Value[j].Id] + '</label>';
                                //html += '</div>';
                                //html += '</div>';
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + $scope.Customsearchattributedata[i].Lable + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodStartDate_" + $scope.Customsearchattributedata[i].Value[j].Id] + ' to ' + $scope.fields["PeriodEndDate_" + $scope.Customsearchattributedata[i].Value[j].Id] + '</span></div>';
                                html += '<div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Comment ' + inlineEditabletitile + '</span><span class="th-AssetInfoValue">' + $scope.fields["PeriodDateDesc_" + $scope.Customsearchattributedata[i].Value[j].Id] + '</span></div>';
                            }
                        }

                    }

                }
            }
            return html;
        }

        function refreshdata() {
            $scope.ShowHideAttributeOnRelation = {};
            $scope.treelevels = {};
            $scope.fieldoptions = [];
            $scope.OwnerList = [];
            $scope.owner = {};
            $scope.NormalDropdownCaption = {};
            $scope.NormalDropdownCaptionObj = [];
            $scope.normaltreeSources = {};
            $scope.setNormalDropdownCaption = function () {
                var keys1 = [];

                angular.forEach($scope.NormalDropdownCaption, function (key) {
                    keys1.push(key);
                    $scope.NormalDropdownCaptionObj = keys1;
                });
            }
            $scope.NormalMultiDropdownCaption = {};
            $scope.NormalMultiDropdownCaptionObj = [];
            $scope.setNormalMultiDropdownCaption = function () {
                var keys1 = [];

                angular.forEach($scope.NormalMultiDropdownCaption, function (key) {
                    keys1.push(key);
                    $scope.NormalMultiDropdownCaptionObj = keys1;
                });
            }
            $scope.setFieldKeys = function () {
                var keys = [];
                angular.forEach($scope.fields, function (key) {
                    keys.push(key);
                    $scope.fieldKeys = keys;
                });
            }
            $scope.fields = { usersID: '' };

            $scope.UserId = $cookies['UserId'];
        }

        var productionsearchtypes = { "Productions": ["Productions", "CmsNavigation", "Task"], "Assets": ["Attachments", "Assetlibrary"] };
        $scope.CountValeObj = { "Production": 0, "Task": 0, "Attachments": 0, "AssetLibrary": 0, "CMS": 0 };

        function cleartheSarchHistory() {
            $(".noResultFound").text("Your search did not match any item.");
            $(".customsearchresult").text("Total 0 hit");
            $scope.isResultEmpty = true;
            $scope.CountValeObj.Production = 0;
            $scope.CountValeObj.Task = 0;
            $scope.CountValeObj.Attachments = 0;
            $scope.CountValeObj.AssetLibrary = 0;
            $scope.CountValeObj.CMS = 0;
            $scope.ScrollInputs.Productions.Isdatafired = false;
            $scope.ScrollInputs.Task.Isdatafired = false;
            $scope.ScrollInputs.Assets.Isdatafired = false;
            $scope.ScrollInputs.Assetlibrary.Isdatafired = false;
            $scope.ScrollInputs.Pages.Isdatafired = false;

        }

        $scope.CurrentScroll = "Productions";
        $scope.CurrentScrollPageNo = 1;
        $scope.IsAllLaded = false;
        function Loadresult() {

            $scope.IsAllLaded = false;

            if ($scope.searchObject.ActualSearchTerm != "All" && $scope.searchObject.ActualSearchTerm != "Tags") {
                $scope.CurrentScroll = $scope.searchObject.ActualSearchTerm == "Tags" ? "All" : $scope.searchObject.ActualSearchTerm;
            }
            else {
                $scope.CurrentScroll = "Productions";
            }

            $(window).AdjustHeightWidth();
            //var SearchResult = $resource('planning/PerformViewMoreSearch', { get: { method: 'GET' } });
            //var search = new SearchResult();
            var search = {};
            if ($scope.searchtext == "") {
                search.Text = "";
            }
            else {
                search.Text = $scope.searchtext;
            }
            search.SearchTerm = {};
            search.ETypeID = 0;
            search.istag = $scope.searchObject.ActualSearchTerm == "Tags" ? true : false;
            search.Searchtype = $scope.searchObject.ActualSearchTerm == "Tags" ? "All" : $scope.searchObject.ActualSearchTerm;
            search.PageID = $scope.PageDetails.PageNo;
            search.IsGlobalAdmin = IsGlobalAdmin;
            search.metadata = $scope.selectedAttributes;
            search.rowsperpage = $scope.RowsPerPageSearch;
            search.CurrentScroll = $scope.CurrentScroll;
            if ($scope.searchtext != "" || $scope.searchtext != undefined) {
                $scope.search = $scope.searchtext;
            }
            else {
                $scope.search = "";
            }

            CustomsearchresultService.PerformViewMoreSearch(search).then(function (searchresponse) {
                $scope.resultfortypesfound = [];
                $scope.array = [];
                if (searchresponse.Response != null) {
                    $scope.PagingData["Page_" + $scope.PageDetails.PageNo] = searchresponse.Response.SearchResultEntity;
                    $scope.resultfortypesfound = searchresponse.Response.SearchResultEntity;
                }
                var dyn_Contresult = "";
                if ($scope.PageDetails.PageNo == 1) {
                    $("#productionsearch-results").html(
                             $compile('')($scope));
                    $("#CMS-results").html(
                             $compile('')($scope));
                    $("#Tasks-results").html(
                          $compile('')($scope));
                    $("#Assets-results").html(
                             $compile('')($scope));
                    $("#Assets-library-results").html(
                             $compile('')($scope));
                }

                if (searchresponse.Response == false) {
                    cleartheSarchHistory();
                }
                else if (searchresponse.Response == null) {
                    cleartheSarchHistory();
                }
                else {

                    if (searchresponse.Response.SearchResultEntity == undefined) {
                        cleartheSarchHistory();
                    }
                    else {

                        if (searchresponse.Response == false) {
                            cleartheSarchHistory();
                        }

                        if (searchresponse.Response.TotalPages != 0) {

                            $scope.CustomCountObjects.Totalcount = searchresponse.Response.TotalPages;

                            var ht_Count = "hit";
                            if ($scope.CustomCountObjects.Totalcount > 1)
                                ht_Count = "hit(s)";
                            else
                                ht_Count = "hit";
                            $(".customsearchresult").text("Total " + $scope.CustomCountObjects.Totalcount + " " + ht_Count + "");

                            MarkLoaded(searchresponse.Response.SearchResultEntity.length > 0 ? true : false);

                            if ($scope.searchObject.ActualSearchTerm == "All" || $scope.searchObject.ActualSearchTerm == "Tags") {
                                $scope.CountValeObj.Production = searchresponse.Response.ProductionCount;
                                $scope.CountValeObj.Task = searchresponse.Response.TaskCount;
                                $scope.CountValeObj.Attachments = searchresponse.Response.AttachmentCount;
                                $scope.CountValeObj.AssetLibrary = searchresponse.Response.AssetLibraryCount;
                                $scope.CountValeObj.CMS = searchresponse.Response.CMSCount;
                            }
                            else {
                                if ($scope.searchObject.ActualSearchTerm == "Productions") $scope.CountValeObj.Production = searchresponse.Response.ProductionCount; else $scope.CountValeObj.Production = 0;
                                if ($scope.searchObject.ActualSearchTerm == "Task") $scope.CountValeObj.Task = searchresponse.Response.TaskCount; else $scope.CountValeObj.Task = 0;
                                if ($scope.searchObject.ActualSearchTerm == "Attachments") $scope.CountValeObj.Attachments = searchresponse.Response.AttachmentCount; else $scope.CountValeObj.Attachments = 0;
                                if ($scope.searchObject.ActualSearchTerm == "Assetlibrary") $scope.CountValeObj.AssetLibrary = searchresponse.Response.AssetLibraryCount; else $scope.CountValeObj.AssetLibrary = 0;
                                if ($scope.searchObject.ActualSearchTerm == "CmsNavigation") $scope.CountValeObj.CMS = searchresponse.Response.CMSCount; else $scope.CountValeObj.CMS = 0;
                            }

                            var valueMatches = jQuery.grep(searchresponse.Response.SearchResultEntity, function (relation) {
                                return productionsearchtypes.Productions.indexOf(relation.SearchType) != -1;
                            });
                            for (var i = 0, val; val = valueMatches[i++];) {
                                CreateProductionRecord(val);
                            }

                            var assetMatches = jQuery.grep(searchresponse.Response.SearchResultEntity, function (relation) {
                                return productionsearchtypes.Assets.indexOf(relation.SearchType) != -1;
                            });
                            if (assetMatches != undefined) {
                                var arr = {};
                                arr = RetreiveAssetDatafromXML(assetMatches);
                                if (arr.AssetDetails.length > 0) {
                                    LoadAssetThimbnailView(arr);
                                }
                            }
                            $(".noResultFound").text("");
                            $scope.isResultEmpty = false;
                            if (($scope.searchObject.ActualSearchTerm == "All" || $scope.searchObject.ActualSearchTerm == "Tags") && $scope.CurrentScroll != "fullyloaded") {
                                checktoloadnext();
                            }
                        }
                        else {
                            cleartheSarchHistory();
                        }
                    }
                }
            });
            loadScrollSettings();
        }

        function MarkLoaded(isContainData) {

            if ($scope.CurrentScroll == "Productions") {
                if (isContainData) {
                    if (!$scope.ScrollInputs.Productions.Isdatafired)
                        $scope.ScrollInputs.Productions.Isdatafired = true;
                }
            }
            else if ($scope.CurrentScroll == "Task") {
                if (isContainData) {
                    if (!$scope.ScrollInputs.Task.Isdatafired)
                        $scope.ScrollInputs.Task.Isdatafired = true;
                }
            }
            else if ($scope.CurrentScroll == "Attachments") {
                if (isContainData) {
                    if (!$scope.ScrollInputs.Assets.Isdatafired)
                        $scope.ScrollInputs.Assets.Isdatafired = true;
                }
            }
            else if ($scope.CurrentScroll == "Assetlibrary") {
                if (isContainData) {
                    if (!$scope.ScrollInputs.Assetlibrary.Isdatafired)
                        $scope.ScrollInputs.Assetlibrary.Isdatafired = true;
                }
            }
            else if ($scope.CurrentScroll == "CmsNavigation") {
                if (isContainData) {
                    if (!$scope.ScrollInputs.Pages.Isdatafired)
                        $scope.ScrollInputs.Pages.Isdatafired = true;
                }
            }

        }

        function checktoloadnext() {
            if (($scope.searchObject.ActualSearchTerm == "All" || $scope.searchObject.ActualSearchTerm == "Tags") && $scope.CurrentScroll != "fullyloaded") {
                if ($scope.CurrentScroll == "Productions") {
                    if ($scope.CountValeObj.Production - ($scope.ScrollInputs.Productions.CurrentScrollCount * $scope.RowsPerPageSearch) >= 0) {
                        //call next page of record
                        $scope.ScrollInputs.Productions.CurrentScrollCount = $scope.ScrollInputs.Productions.CurrentScrollCount + 1;
                        $scope.CurrentScrollPageNo = $scope.ScrollInputs.Productions.CurrentScrollCount;
                        //checkListItemContents();
                    }
                    else {
                        if ($scope.CountValeObj.Task > 0) {
                            $scope.CurrentScrollPageNo = $scope.ScrollInputs.Task.CurrentScrollCount;
                            $scope.CurrentScroll = "Task";
                            checkListItemContents();
                        }
                        else if ($scope.CountValeObj.CMS > 0) {
                            $scope.CurrentScroll = "CmsNavigation";
                            $scope.CurrentScrollPageNo = $scope.ScrollInputs.Pages.CurrentScrollCount;
                            checkListItemContents();
                        }
                        else if ($scope.CountValeObj.Attachments > 0) {
                            $scope.CurrentScrollPageNo = $scope.ScrollInputs.Assets.CurrentScrollCount;
                            $scope.CurrentScroll = "Attachments";
                            checkListItemContents();
                        }
                        else if ($scope.CountValeObj.AssetLibrary > 0) {
                            $scope.CurrentScrollPageNo = $scope.ScrollInputs.Assetlibrary.CurrentScrollCount;
                            $scope.CurrentScroll = "Assetlibrary";
                            checkListItemContents();
                        }

                        else {
                            $scope.IsAllLaded = true;
                        }
                        $scope.ScrollInputs.Productions.isLoaded = true;


                    }
                }
                else if ($scope.CurrentScroll == "Task") {
                    if ($scope.CountValeObj.Task - ($scope.ScrollInputs.Task.CurrentScrollCount * $scope.RowsPerPageSearch) >= 0) {
                        //call next page of record
                        $scope.ScrollInputs.Task.CurrentScrollCount = $scope.ScrollInputs.Task.CurrentScrollCount + 1;
                        $scope.CurrentScrollPageNo = $scope.ScrollInputs.Task.CurrentScrollCount;
                        //checkListItemContents();
                    }
                    else {
                        $scope.ScrollInputs.Task.isLoaded = true;

                        if ($scope.CountValeObj.CMS > 0) {
                            $scope.CurrentScroll = "CmsNavigation";
                            $scope.CurrentScrollPageNo = $scope.ScrollInputs.Pages.CurrentScrollCount;
                            checkListItemContents();
                        }
                        else if ($scope.CountValeObj.Attachments > 0) {
                            $scope.CurrentScroll = "Attachments";
                            $scope.CurrentScrollPageNo = $scope.ScrollInputs.Assets.CurrentScrollCount;
                            checkListItemContents();
                        }
                        else if ($scope.CountValeObj.AssetLibrary > 0) {
                            $scope.CurrentScrollPageNo = $scope.ScrollInputs.Assetlibrary.CurrentScrollCount;
                            $scope.CurrentScroll = "Assetlibrary";
                            checkListItemContents();
                        }
                        else {
                            $scope.IsAllLaded = true;
                        }

                    }
                }
                else if ($scope.CurrentScroll == "CmsNavigation") {
                    if ($scope.CountValeObj.CMS - ($scope.ScrollInputs.Pages.CurrentScrollCount * $scope.RowsPerPageSearch) >= 0) {
                        //call next page of record
                        $scope.ScrollInputs.Pages.CurrentScrollCount = $scope.ScrollInputs.Pages.CurrentScrollCount + 1;
                        $scope.CurrentScrollPageNo = $scope.ScrollInputs.Pages.CurrentScrollCount;
                        //checkListItemContents();
                    }
                    else {
                        $scope.ScrollInputs.Pages.isLoaded = true;

                        if ($scope.CountValeObj.Attachments > 0) {
                            $scope.CurrentScrollPageNo = $scope.ScrollInputs.Assets.CurrentScrollCount;
                            $scope.CurrentScroll = "Attachments";
                            checkListItemContents();
                        }
                        else if ($scope.CountValeObj.AssetLibrary > 0) {
                            $scope.CurrentScrollPageNo = $scope.ScrollInputs.Assetlibrary.CurrentScrollCount;
                            $scope.CurrentScroll = "Assetlibrary";
                            checkListItemContents();
                        }
                        else {
                            $scope.IsAllLaded = true;
                        }

                    }
                }
                else if ($scope.CurrentScroll == "Attachments") {
                    if ($scope.CountValeObj.Attachments - ($scope.ScrollInputs.Assets.CurrentScrollCount * $scope.RowsPerPageSearch) >= 0) {
                        //call next page of record
                        $scope.ScrollInputs.Assets.CurrentScrollCount = $scope.ScrollInputs.Assets.CurrentScrollCount + 1;
                        $scope.CurrentScrollPageNo = $scope.ScrollInputs.Assets.CurrentScrollCount;
                        //checkListItemContents();
                    }
                    else {
                        $scope.ScrollInputs.Assets.isLoaded = true;
                        if ($scope.CountValeObj.AssetLibrary > 0) {
                            $scope.CurrentScrollPageNo = $scope.ScrollInputs.Assetlibrary.CurrentScrollCount;
                            $scope.CurrentScroll = "Assetlibrary";
                            checkListItemContents();
                        }
                        else {
                            $scope.IsAllLaded = true;
                        }
                    }
                }
                else if ($scope.CurrentScroll == "Assetlibrary") {
                    if ($scope.CountValeObj.AssetLibrary - ($scope.ScrollInputs.Assetlibrary.CurrentScrollCount * $scope.RowsPerPageSearch) >= 0) {
                        //call next page of record
                        $scope.ScrollInputs.Assetlibrary.CurrentScrollCount = $scope.ScrollInputs.Assetlibrary.CurrentScrollCount + 1;
                        $scope.CurrentScrollPageNo = $scope.ScrollInputs.Assetlibrary.CurrentScrollCount;
                        //checkListItemContents();
                    }
                    else {
                        $scope.ScrollInputs.Assetlibrary.isLoaded = true;
                        $scope.CurrentScroll = "fullyloaded";
                        $scope.IsAllLaded = true;
                    }
                }
            }

        }

        $scope.ScrollInputs = {
            "Productions": { "isLoaded": false, "CurrentScrollCount": 1, "Isdatafired": false }, "Task": { "isLoaded": false, "CurrentScrollCount": 1, "Isdatafired": false },
            "Pages": { "isLoaded": false, "CurrentScrollCount": 1, "Isdatafired": false }, "Assets": { "isLoaded": false, "CurrentScrollCount": 1, "Isdatafired": false }, "Assetlibrary": { "isLoaded": false, "CurrentScrollCount": 1, "Isdatafired": false }
        };

        function RetreiveAssetDatafromXML(res) {
            var assetobj = [], attributerelations = [], result = {}, attributes = [], htmlsearchAttrtempl = '', attributewithValues = {};
            for (var i = 0, obj; obj = res[i++];) {
                htmlsearchAttrtempl = '';
                $($.parseXML(obj.AssetDetails)).find('p').each(function () {
                    assetobj.push({
                        "FileUniqueID": $(this).attr('FileUniqueID'), "FileName": $(this).attr('FileName'), "Extension": $(this).attr('Extension'), "Size": $(this).attr('Size'),
                        "VersionNo": $(this).attr('VersionNo'), "ProcessType": $(this).attr('ProcessType'), "OwnerID": $(this).attr('OwnerID'), "CreatedOn": $(this).attr('CreatedOn'), "FileGuid": $(this).attr('FileGuid'),
                        "Description": $(this).attr('Description'), "AssetID": $(this).attr('AssetID'), "FolderID": $(this).attr('FolderID'), "EntityID": $(this).attr('EntityID'),
                        "AssetUniqueID": $(this).attr('AssetUniqueID'), "AssetName": $(this).attr('AssetName'), "LinkURL": $(this).attr('LinkURL'), "MimeType": $(this).attr('MimeType'),
                        "AssetTypeid": $(this).attr('AssetTypeid'), "ActiveFileID": $(this).attr('ActiveFileID'), "ColorCode": $(this).attr('ColorCode'), "ShortDescription": $(this).attr('ShortDescription'),
                        "Status": $(this).attr('Status'), "Category": $(this).attr('Category'), "IsPublish": $(this).attr('IsPublish'), "LinkedAssetID": $(this).attr('LinkedAssetID')
                    });
                });
                var tempAttributes = [];

                var tempFiles = {};
                $($.parseXML(obj.AssetValueDetails)).find('p').each(function () {
                    var assetID = $(this).attr('ID');
                    $.each(this.attributes, function (i, attrib) {
                        var name = attrib.name.replace("Attr_", ""), value = attrib.value, matchtxt;
                        matchtxt = matchHighlightText($scope.searchtext, value);
                        tempFiles[name] = value;
                    });
                    attributewithValues[assetID] = tempFiles;
                });
            }
            result = { "AssetDetails": assetobj, "attrRelation": attributewithValues }
            return result;
        }

        function matchHighlightText(sub, source) {
            var src_str = source;
            var term = sub, htmlsearchAttrtempl = "";
            term = term.replace(/(\s+)/, "(<[^>]+>)*$1(<[^>]+>)*");
            var pattern = new RegExp("(" + term + ")", "gi");
            src_str = src_str.replace(pattern, "<mark>$1</mark>");
            src_str = src_str.replace(/(<mark>[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/, "$1</mark>$2<mark>$4<mark>$5</mark>$6<mark>$7</mark>");
            return src_str;
        }

        $scope.attributewiseoption = function (item) {
            if (item) {
                if (item.AttributeTypeID != 17)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        $scope.alert = function (attrid, optionid, isselected) {
            if (isselected) {
                var remainRecord = [];
                remainRecord = $.grep($scope.selectedAttributes, function (e) { return e.AttributeID == attrid; });
                var optionCaption = $.grep($scope.AttributeOptions["FilterOption_" + attrid + ""], function (e) { return e.Id == optionid })[0].Caption;
                var attrCaption = $scope.AttributeOptions["FilterProp_" + attrid + ""].AttributeCaption;
                var attrType = $scope.AttributeOptions["FilterProp_" + attrid + ""].AttributeTypeID;
                if (remainRecord.length == 0) {
                    $scope.selectedAttributes.push({ "AttributeID": attrid, "AttributeTypeID": attrType, "AttributeCaption": attrCaption, "Values": ['' + optionCaption.toString() + ''], "ValuesIDArr": [optionid] });
                }
                else {
                    //update the values
                    var valueRecords = [], valueIds = [];
                    valueRecords = $.grep(remainRecord[0].Values, function (e) { return e == optionCaption; });
                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) { return e == optionid; });
                    if (valueRecords.length == 0) {
                        remainRecord[0].Values.push(optionCaption);
                    }
                    if (valueIds.length == 0) {
                        remainRecord[0].ValuesIDArr.push(optionid);
                    }
                }
            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.selectedAttributes, function (e) { return e.AttributeID == attrid; });

                var optionCaption = $.grep($scope.AttributeOptions["FilterOption_" + attrid + ""], function (e) { return e.Id == optionid })[0].Caption;
                var attrCaption = $scope.AttributeOptions["FilterProp_" + attrid + ""].AttributeCaption;
                var attrType = $scope.AttributeOptions["FilterProp_" + attrid + ""].AttributeTypeID;

                if (remainRecord.length > 0) {
                    var valueRecords = [], valueIds = [];
                    valueRecords = $.grep(remainRecord[0].Values, function (e) { return e == optionCaption; });
                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) { return e == optionid; });
                    remainRecord[0].Values.splice($.inArray(valueRecords[0], remainRecord[0].Values), 1);
                    remainRecord[0].ValuesIDArr.splice($.inArray(valueIds[0], remainRecord[0].ValuesIDArr), 1);
                    if (remainRecord[0].Values.length == 0) {
                        $scope.selectedAttributes.splice($.inArray(remainRecord[0], $scope.selectedAttributes), 1);
                    }
                }
            }
            $scope.PageDetails.PageNo = 1;
            if ($scope.selectedAttributes.length > 0)
                GetFilteredAssetsData();
            else
                Loadresult();
            //alert("aid=" + aid + "-----" + "id=" + id);

        }

        $scope.selectedAttributes = [];
        $scope.FilterbyAttributeSelection = function (event, attrid, attrtype, optionid, attrlevel) {
            var checkbox = event.target;
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.selectedAttributes, function (e) { return e.AttributeID == attrid; });
                if (remainRecord.length == 0) {
                    $scope.selectedAttributes.push({ "AttributeID": attrid, "AttributeTypeID": attrtype, "AttributeCaption": $(checkbox).attr('attributecaption'), "Values": ['' + $(checkbox).attr('optioncaption').toString() + ''], "ValuesIDArr": [optionid] });
                }
                else {
                    //update the values
                    var valueRecords = [], valueIds = [];
                    valueRecords = $.grep(remainRecord[0].Values, function (e) { return e == $(checkbox).attr('optioncaption'); });
                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) { return e == optionid; });
                    if (valueRecords.length == 0) {
                        remainRecord[0].Values.push($(checkbox).attr('optioncaption'));
                    }
                    if (valueIds.length == 0) {
                        remainRecord[0].ValuesIDArr.push(optionid);
                    }
                }
            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.selectedAttributes, function (e) { return e.AttributeID == attrid; });
                if (remainRecord.length > 0) {
                    var valueRecords = [], valueIds = [];
                    valueRecords = $.grep(remainRecord[0].Values, function (e) { return e == $(checkbox).attr('optioncaption'); });
                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) { return e == optionid; });
                    remainRecord[0].Values.splice($.inArray(valueRecords[0], remainRecord[0].Values), 1);
                    remainRecord[0].ValuesIDArr.splice($.inArray(valueIds[0], remainRecord[0].ValuesIDArr), 1);
                    if (remainRecord[0].Values.length == 0) {
                        $scope.selectedAttributes.splice($.inArray(remainRecord[0], $scope.selectedAttributes), 1);
                    }
                }
            }
            $scope.PageDetails.PageNo = 1;
            if ($scope.selectedAttributes.length > 0)
                GetFilteredAssetsData();
            else
                Loadresult();
        }

        $scope.RemoveCustomAttributefromFilter = function (attrid, valueCaption) {
            var remainRecord = [];
            remainRecord = $.grep($scope.selectedAttributes, function (e) { return e.AttributeID == attrid; });
            if (remainRecord.length > 0) {
                var valueRecords = [], valueIds = [];
                valueRecords = $.grep(remainRecord[0].Values, function (e) { return e == valueCaption; });
                remainRecord[0].Values.splice($.inArray(valueRecords[0], remainRecord[0].Values), 1);
                if (remainRecord[0].Values.length == 0) {
                    $scope.selectedAttributes.splice($.inArray(remainRecord[0], $scope.selectedAttributes), 1);
                }
                var attrValue = {};
                attrValue = $.grep($scope.AttributeOptions["FilterOption_" + attrid + ""], function (e) { return e.Caption == valueCaption; })[0];
                if (attrValue != undefined) {
                    attrValue["Isselectd"] = false;
                    var valueIds = [];
                    valueIds = $.grep(remainRecord[0].ValuesIDArr, function (e) { return e == attrValue.Id; });
                    remainRecord[0].ValuesIDArr.splice($.inArray(valueIds[0], remainRecord[0].ValuesIDArr), 1);
                }

                $scope.PageDetails.PageNo = 1;
                $timeout(function () { GetFilteredAssetsData(); }, 100);
            }

        }

        function GetFilteredAssetsData() {

            $(window).AdjustHeightWidth();

            $scope.IsAllLaded = false;

            $scope.ScrollInputs = {
                "Productions": { "isLoaded": false, "CurrentScrollCount": 1 }, "Task": { "isLoaded": false, "CurrentScrollCount": 1 },
                "Pages": { "isLoaded": false, "CurrentScrollCount": 1 }, "Assets": { "isLoaded": false, "CurrentScrollCount": 1 }, "Assetlibrary": { "isLoaded": false, "CurrentScrollCount": 1 }
            };


            if ($scope.searchObject.ActualSearchTerm != "All" && $scope.searchObject.ActualSearchTerm != "Tags") {
                $scope.CurrentScroll = $scope.searchObject.ActualSearchTerm == "Tags" ? "All" : $scope.searchObject.ActualSearchTerm;
            }
            else {
                $scope.CurrentScroll = "Productions";
            }

            // var SearchResult = $resource('planning/PerformViewMoreSearch', { get: { method: 'GET' } });
            var search = {};
            //  var search = new SearchResult();
            if ($scope.searchtext == "") {
                search.Text = "";
            }
            else {
                search.Text = $scope.searchtext;
            }
            if ($scope.searchassetlibrary == true) {
                var assetypes = [];
                if ($scope.selectedAssettypes.length > 0)
                    assetypes = $scope.selectedAssettypes;
                else
                    assetypes = $scope.customAssetTypes.m_Item1;
            }

            search.SearchTerm = {};
            if ($scope.searchassetlibrary == true) {
                search.ETypeID = assetypes;
                search.orderbyfieldName = "Name";
                search.desc = true;
                search.isQuerystringdata = false;
            }
            else {
                search.ETypeID = 0;
            }
            search.istag = $scope.searchObject.ActualSearchTerm == "Tags" ? true : false;
            search.Searchtype = $scope.searchObject.ActualSearchTerm == "Tags" ? "All" : $scope.searchObject.ActualSearchTerm;
            search.PageID = $scope.PageDetails.PageNo;
            search.IsGlobalAdmin = IsGlobalAdmin;
            search.metadata = $scope.selectedAttributes;
            search.rowsperpage = $scope.RowsPerPageSearch;
            if ($scope.searchtext != "" || $scope.searchtext != undefined) {
                $scope.search = $scope.searchtext;
            }
            else {
                $scope.search = "";
            }
            search.CurrentScroll = $scope.CurrentScroll;
            if ($scope.searchassetlibrary == true && $scope.selectedAssettypes.length > 0) {
                CustomsearchresultService.CallbackSnippetSearch(search).then(function (searchresponce) {
                    $scope.resultfortypesfound = [];
                    $scope.array = [];
                    $scope.resultfortypesfound = searchresponse.Response.SearchResultEntity;
                    if ($scope.PageDetails.PageNo == 1) {
                        $("#productionsearch-results").html(
                                 $compile('')($scope));
                        $("#CMS-results").html(
                                 $compile('')($scope));
                        $("#Tasks-results").html(
                             $compile('')($scope));
                        $("#Assets-results").html(
                                 $compile('')($scope));
                        $("#Assets-library-results").html(
                                 $compile('')($scope));
                    }
                    if (searchresponse.Response.SearchResultEntity == undefined) {
                        cleartheSarchHistory();
                    }

                    if (searchresponse.Response == false) {
                        cleartheSarchHistory();
                    }
                    else {
                        if (searchresponse.Response.TotalPages == 0) {
                            $("#productionsearch-results").html(
                                   $compile('')($scope));
                            $("#CMS-results").html(
                                     $compile('')($scope));
                            $("#Assets-results").html(
                                     $compile('')($scope));
                            $("#Assets-library-results").html(
                                     $compile('')($scope));
                            $("#Tasks-results").html($compile('')($scope));
                            $(".noResultFound").text("Your search did not match any item.");
                            $(".customsearchresult").text("Total 0 hit");
                            $scope.isResultEmpty = true;
                        }
                        if (searchresponse.Response.TotalPages != 0) {
                            $scope.CustomCountObjects.Totalcount = searchresponse.Response.TotalPages;

                            var ht_Count = "hit";
                            if ($scope.CustomCountObjects.Totalcount > 1)
                                ht_Count = "hit(s)";
                            else
                                ht_Count = "hit";
                            $(".customsearchresult").text("Total " + $scope.CustomCountObjects.Totalcount + " " + ht_Count + "");

                            if ($scope.searchObject.ActualSearchTerm == "All" || $scope.searchObject.ActualSearchTerm == "Tags") {
                                $scope.CountValeObj.Production = searchresponse.Response.ProductionCount;
                                $scope.CountValeObj.Task = searchresponse.Response.TaskCount;
                                $scope.CountValeObj.Attachments = searchresponse.Response.AttachmentCount;
                                $scope.CountValeObj.AssetLibrary = searchresponse.Response.AssetLibraryCount;
                                $scope.CountValeObj.CMS = searchresponse.Response.CMSCount;
                            }
                            else {
                                if ($scope.searchObject.ActualSearchTerm == "Productions") $scope.CountValeObj.Production = searchresponse.Response.ProductionCount; else $scope.CountValeObj.Production = 0;
                                if ($scope.searchObject.ActualSearchTerm == "Task") $scope.CountValeObj.Task = searchresponse.Response.TaskCount; else $scope.CountValeObj.Task = 0;
                                if ($scope.searchObject.ActualSearchTerm == "Attachments") $scope.CountValeObj.Attachments = searchresponse.Response.AttachmentCount; else $scope.CountValeObj.Attachments = 0;
                                if ($scope.searchObject.ActualSearchTerm == "Assetlibrary") $scope.CountValeObj.AssetLibrary = searchresponse.Response.AssetLibraryCount; else $scope.CountValeObj.AssetLibrary = 0;
                                if ($scope.searchObject.ActualSearchTerm == "CmsNavigation") $scope.CountValeObj.CMS = searchresponse.Response.CMSCount; else $scope.CountValeObj.CMS = 0;
                            }


                            MarkLoaded(searchresponse.Response.SearchResultEntity.length > 0 ? true : false);

                            var valueMatches = jQuery.grep(searchresponse.Response.SearchResultEntity, function (relation) {
                                return productionsearchtypes.Productions.indexOf(relation.SearchType) != -1;
                            });
                            for (var i = 0, val; val = valueMatches[i++];) {
                                CreateProductionRecord(val);
                            }

                            var assetMatches = jQuery.grep(searchresponse.Response.SearchResultEntity, function (relation) {
                                return productionsearchtypes.Assets.indexOf(relation.SearchType) != -1;
                            });
                            if (assetMatches != undefined) {
                                var arr = {};
                                arr = RetreiveAssetDatafromXML(assetMatches);
                                if (arr.AssetDetails.length > 0) {
                                    LoadAssetThimbnailView(arr);
                                }
                            }

                            $(".noResultFound").text("");
                            $scope.isResultEmpty = false;
                            if (($scope.searchObject.ActualSearchTerm == "All" || $scope.searchObject.ActualSearchTerm == "Tags") && $scope.CurrentScroll != "fullyloaded") {
                                checktoloadnext();
                            }
                        }
                        else {
                            cleartheSarchHistory();
                        }
                    }
                });
            }
            else {
                CustomsearchresultService.PerformViewMoreSearch(search).then(function (searchresponse) {
                    $scope.resultfortypesfound = [];
                    $scope.array = [];
                    if (searchresponse.Response != null)
                        $scope.resultfortypesfound = searchresponse.Response.SearchResultEntity;
                    if ($scope.PageDetails.PageNo == 1) {
                        $("#productionsearch-results").html(
                                 $compile('')($scope));
                        $("#CMS-results").html(
                                 $compile('')($scope));
                        $("#Tasks-results").html(
                             $compile('')($scope));
                        $("#Assets-results").html(
                                 $compile('')($scope));
                        $("#Assets-library-results").html(
                                 $compile('')($scope));
                    }
                    if (searchresponse.Response == null) {
                        cleartheSarchHistory();
                    }
                    else if (searchresponse.Response.SearchResultEntity == undefined) {
                        cleartheSarchHistory();
                    }

                    else if (searchresponse.Response == false) {
                        cleartheSarchHistory();
                    }
                    else {
                        if (searchresponse.Response.TotalPages == 0) {
                            $("#productionsearch-results").html(
                                   $compile('')($scope));
                            $("#CMS-results").html(
                                     $compile('')($scope));
                            $("#Assets-results").html(
                                     $compile('')($scope));
                            $("#Assets-library-results").html(
                                     $compile('')($scope));
                            $("#Tasks-results").html($compile('')($scope));
                            $(".noResultFound").text("Your search did not match any item.");
                            $(".customsearchresult").text("Total 0 hit");
                            $scope.isResultEmpty = true;
                        }
                        if (searchresponse.Response.TotalPages != 0) {
                            $scope.CustomCountObjects.Totalcount = searchresponse.Response.TotalPages;

                            var ht_Count = "hit";
                            if ($scope.CustomCountObjects.Totalcount > 1)
                                ht_Count = "hit(s)";
                            else
                                ht_Count = "hit";
                            $(".customsearchresult").text("Total " + $scope.CustomCountObjects.Totalcount + " " + ht_Count + "");

                            if ($scope.searchObject.ActualSearchTerm == "All" || $scope.searchObject.ActualSearchTerm == "Tags") {
                                $scope.CountValeObj.Production = searchresponse.Response.ProductionCount;
                                $scope.CountValeObj.Task = searchresponse.Response.TaskCount;
                                $scope.CountValeObj.Attachments = searchresponse.Response.AttachmentCount;
                                $scope.CountValeObj.AssetLibrary = searchresponse.Response.AssetLibraryCount;
                                $scope.CountValeObj.CMS = searchresponse.Response.CMSCount;
                            }
                            else {
                                if ($scope.searchObject.ActualSearchTerm == "Productions") $scope.CountValeObj.Production = searchresponse.Response.ProductionCount; else $scope.CountValeObj.Production = 0;
                                if ($scope.searchObject.ActualSearchTerm == "Task") $scope.CountValeObj.Task = searchresponse.Response.TaskCount; else $scope.CountValeObj.Task = 0;
                                if ($scope.searchObject.ActualSearchTerm == "Attachments") $scope.CountValeObj.Attachments = searchresponse.Response.AttachmentCount; else $scope.CountValeObj.Attachments = 0;
                                if ($scope.searchObject.ActualSearchTerm == "Assetlibrary") $scope.CountValeObj.AssetLibrary = searchresponse.Response.AssetLibraryCount; else $scope.CountValeObj.AssetLibrary = 0;
                                if ($scope.searchObject.ActualSearchTerm == "CmsNavigation") $scope.CountValeObj.CMS = searchresponse.Response.CMSCount; else $scope.CountValeObj.CMS = 0;
                            }


                            MarkLoaded(searchresponse.Response.SearchResultEntity.length > 0 ? true : false);

                            var valueMatches = jQuery.grep(searchresponse.Response.SearchResultEntity, function (relation) {
                                return productionsearchtypes.Productions.indexOf(relation.SearchType) != -1;
                            });
                            for (var i = 0, val; val = valueMatches[i++];) {
                                CreateProductionRecord(val);
                            }

                            var assetMatches = jQuery.grep(searchresponse.Response.SearchResultEntity, function (relation) {
                                return productionsearchtypes.Assets.indexOf(relation.SearchType) != -1;
                            });
                            if (assetMatches != undefined) {
                                var arr = {};
                                arr = RetreiveAssetDatafromXML(assetMatches);
                                if (arr.AssetDetails.length > 0) {
                                    LoadAssetThimbnailView(arr);
                                }
                            }

                            $(".noResultFound").text("");
                            $scope.isResultEmpty = false;
                            if (($scope.searchObject.ActualSearchTerm == "All" || $scope.searchObject.ActualSearchTerm == "Tags") && $scope.CurrentScroll != "fullyloaded") {
                                checktoloadnext();
                            }
                        }
                        else {
                            cleartheSarchHistory();
                        }
                    }

                });
            }
        }

        //Returns Decoded Name
        $scope.DecodedTaskName = function (TaskName) {
            if (TaskName != null & TaskName != "") {
                return $('<div />').html(TaskName).text();
            }
            else {
                return "";
            }
        };

        $scope.AssetSearchObj = { "AssetSearchfiles": [], "AssetDynamicData": [], "ThumbnailSettings": [] };

        $scope.Editasset = function (AssetID) {

            var rem = $.grep($scope.assetlibraryaccess, function (e) { return e.AssetUniqueID == AssetID });
            if (rem.length > 0) {
                $scope.SearchAsset.Access = 1;
            }
            else
                $scope.SearchAsset.Access = 0;

            $timeout(function () {
                $scope.$emit('callBackAssetEditinMUI', AssetID, false, false, 'ViewType');
            }, 10);
        }

        $timeout(function () { CheckPreviewGenerator(); }, 5000);
        function CheckPreviewGenerator() {
            getPreviewIDs();
        }

        function getPreviewIDs() {
            var assetids = [];
            for (var i = 0; i < $('.loadingImg').length; i++) {
                assetids.push($('.loadingImg')[i].id);
            }
            if (assetids.length > 0) {
                var data = {};
                data.AssetIDs = assetids;
                CustomsearchresultService.CheckPreviewGeneratorID(data)
                .then(function (result) {
                    if (result.Response != null) {
                        var generatedids = result.Response.split(',');
                        for (var j = 0; j < generatedids.length; j++) {
                            if (generatedids[j] != "") {
                                var src = $('#' + generatedids[j]).attr('data-src');
                                //var extn = $('#' + generatedids[j]).attr('data-extn');
                                try {
                                    var upd = $.grep($scope.AssetSearchObj.AssetSearchfiles, function (e) { return e.ActiveFileID == generatedids[j] });
                                    if (upd.length > 0) {
                                        $.grep($scope.AssetSearchObj.AssetSearchfiles, function (e) { return e.ActiveFileID == generatedids[j] })[0].Status = 2;
                                    }
                                } catch (e) {

                                }
                                var extn = '.jpg';
                                if ($('#AssetEditpopup').length > 0 && $('#AssetEditpopup').css('display') == 'block') {
                                    if (GlobalAssetEditID == generatedids[j]) {
                                        $('#asseteditimage').attr('src', imagesrcpath + 'DAMFiles/Preview/Big_' + src + '' + extn);
                                    }
                                }
                                $('#' + generatedids[j]).removeClass('loadingImg');
                                $('#' + generatedids[j]).attr('src', imagesrcpath + 'DAMFiles/Preview/Small_' + src + '' + extn);
                            }
                        }
                    }
                });
            }
            PreviewGeneratorTimer = $timeout(function () { CheckPreviewGenerator(); }, 3000);
        }

        var loading = false;
        // When the DOM is ready, initialize document.
        function loadScrollSettings() {

            $(window).scroll(function () {

                if (loading && $scope.IsAllLaded) {
                    return;
                }

                if ($(window).scrollTop() > ($(document).height() - $(window).height() - 700)) {
                    loading = true;
                    if ($scope.searchObject.ActualSearchTerm == "All" || $scope.searchObject.ActualSearchTerm == "Tags") {
                        if ($scope.CurrentScroll != "fullyloaded")
                            checkListItemContents();
                        else
                            $scope.IsAllLaded = true;
                    }
                    else {
                        if (($scope.RowsPerPageSearch * $scope.PageDetails.PageNo) <= $scope.CustomCountObjects.Totalcount)
                            checkListItemContents();
                        else
                            $scope.IsAllLaded = true;
                    }
                }

            });


        }

        function checkListItemContents() {

            if (!$scope.IsAllLaded) {

                //$scope.CurrentScrollPageNo = $scope.CurrentScrollPageNo + 1;
                $scope.PageDetails.PageNo = $scope.PageDetails.PageNo + 1;
                $(window).AdjustHeightWidth();
                //var SearchResult = $resource('planning/PerformViewMoreSearch', { get: { method: 'GET' } });
                //var search = new SearchResult();
                var search = {};
                if ($scope.searchtext == "") {
                    search.Text = "";
                }
                else {
                    search.Text = $scope.searchtext;
                }
                search.SearchTerm = {};
                search.ETypeID = 0;
                search.istag = $scope.searchObject.ActualSearchTerm == "Tags" ? true : false;
                search.Searchtype = $scope.searchObject.ActualSearchTerm == "Tags" ? "All" : $scope.searchObject.ActualSearchTerm;
                search.PageID = ($scope.searchObject.ActualSearchTerm == "All" || $scope.searchObject.ActualSearchTerm == "Tags") ? $scope.CurrentScrollPageNo : $scope.PageDetails.PageNo;
                search.IsGlobalAdmin = IsGlobalAdmin;
                search.metadata = $scope.selectedAttributes;
                search.rowsperpage = $scope.RowsPerPageSearch;
                if ($scope.searchtext != "" || $scope.searchtext != undefined) {
                    $scope.search = $scope.searchtext;
                }
                else {
                    $scope.search = "";
                }
                search.CurrentScroll = $scope.CurrentScroll;
                $scope.resultfortypesfound = [];
                $scope.array = [];
                var dyn_Contresult = "";
                CustomsearchresultService.PerformViewMoreSearch(search).then(function (searchresponse) {
                    if (searchresponse.Response != false && searchresponse.Response != null) {
                        $scope.resultfortypesfound = [];
                        loading = false;
                        $scope.array = [];
                        $scope.PagingData["Page_" + $scope.PageDetails.PageNo] = searchresponse.Response.SearchResultEntity;
                        $scope.resultfortypesfound = searchresponse.Response.SearchResultEntity;
                        var dyn_Contresult = "";
                        if (searchresponse.Response.TotalPages != 0) {

                            MarkLoaded(searchresponse.Response.SearchResultEntity.length > 0 ? true : false);

                            var valueMatches = jQuery.grep(searchresponse.Response.SearchResultEntity, function (relation) {
                                return productionsearchtypes.Productions.indexOf(relation.SearchType) != -1;
                            });
                            for (var i = 0, val; val = valueMatches[i++];) {
                                CreateProductionRecord(val);
                            }

                            var assetMatches = jQuery.grep(searchresponse.Response.SearchResultEntity, function (relation) {
                                return productionsearchtypes.Assets.indexOf(relation.SearchType) != -1;
                            });
                            if (assetMatches != undefined) {
                                var arr = {};
                                arr = RetreiveAssetDatafromXML(assetMatches);
                                if (arr.AssetDetails.length > 0) {
                                    LoadAssetThimbnailView(arr);
                                }
                            }

                            checktoloadnext(); //check to load 

                        }
                    }
                });
            }
        }
        $scope.SetAssetActionId = function (assetid) {
            if ($scope.AssetSelectionFiles.length > 0) {
                var asset = [], data = [];
                asset = $.grep($scope.AssetSelectionFiles, function (e) {
                    return e.AssetId == assetid;
                });
                if (asset.length == 0) {
                    $scope.AssetSelectionFiles.push({ "AssetId": assetid });
                }
            }
            else {
                $scope.AssetSelectionFiles.push({ "AssetId": assetid });
            }
        }

        function IsAssetSelected(assetid) {

            var remainRecord = [];

            remainRecord = $.grep($scope.AssetSelectionFiles, function (e) { return e.AssetId == assetid; });
            if (remainRecord.length > 0) {
                return true;
            }
            return false;

        }

        $scope.AssetSelectionFiles = [];
        $scope.checkSelection = function (assetid, activefileid, event) {


            var checkbox = event.target;

            //if ($scope.FilterStatus == 1)
            $scope.AssetSelectionClass["asset_" + assetid] = checkbox.checked == true ? "th-Selection selected" : "th-Selection";
            //else if ($scope.FilterStatus == 2)
            //    $scope.AssetSelectionClass["asset_" + assetid] = checkbox.checked == true ? "th-sum-Selection selected" : "th-sum-Selection";
            //else if ($scope.FilterStatus == 3)
            //    $scope.AssetSelectionClass["asset_" + assetid] = checkbox.checked == true ? "li-Selection selected" : "li-Selection";

            var res = [];
            res = $.grep($scope.AssetSearchObj.AssetSearchfiles, function (e) {
                return e.AssetUniqueID == assetid;
            });

            //if (res.length > 0) {
            //    DownloadAssets(checkbox, assetid, res[0].FileName, res[0].Extension, res[0].FileGuid);
            //}


            if (checkbox.checked) {
                var remainRecord = [];

                remainRecord = $.grep($scope.AssetSelectionFiles, function (e) { return e.AssetId == assetid; });
                if (remainRecord.length == 0) {
                    if (res[0] != undefined)
                        $scope.AssetSelectionFiles.push({ "AssetId": assetid });
                    else
                        $scope.AssetSelectionFiles.push({ "AssetId": assetid });
                }

            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetSelectionFiles, function (e) { return e.AssetId == assetid; });
                if (remainRecord.length > 0) {
                    $scope.AssetSelectionFiles.splice($.inArray(remainRecord[0], $scope.AssetSelectionFiles), 1);

                }

            }


        }

        $scope.damlightbox.lightboxselection = [];
        $scope.Addtolightbox = function () {
            if ($scope.AssetSelectionFiles.length > 0) {
                for (var i = 0, assetid; assetid = $scope.AssetSelectionFiles[i++];) {
                    var remainRecord = [];

                    var asset = [], data = [];
                    asset = $.grep($scope.AssetSearchObj.AssetSearchfiles, function (e) {
                        return e.AssetUniqueID == assetid.AssetId;
                    });
                    data = $.grep($scope.AssetSearchObj.AssetDynamicData, function (e) {
                        return e.AssetID == assetid.AssetId;
                    });
                    if (asset[0].FileGuid != null)
                        if (asset.length > 0)
                            $scope.damlightbox.lightboxselection.push({ "AssetRawData": asset[0], "EntityID": asset[0].EntityID, "ShortDesc": asset[0].ShortDescription, "ColorCode": asset[0].ColorCode, "AssetId": assetid.AssetId, "AssetName": asset[0].Name, "Description": asset[0].Description != null ? asset[0].Description : "-", "Guid": asset[0].FileGuid, "Ext": asset[0].Extension, "AssetStatus": asset[0].Status, "FileSize": asset[0].Size, "DynamicData": data, "MimeType": asset[0].MimeType, "Extension": asset[0].Extension.substring(1, asset[0].Extension.length).toLowerCase(), "Category": asset[0].Category, "Iscropped": false });
                }

                $timeout(function () { $scope.AssetSelectionFiles = []; }, 200);
                NotifySuccess($translate.instant('LanguageContents.Res_4066.Caption'));
                //$scope.DownloadFileObj.DownloadFilesArr = [];
                //$scope.AssetFilesObj.AssetTaskObj = [];

            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }

        }

        $scope.DownloadWithMetadata = function (result) {
            var arr = [];
            for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                arr[i] = $scope.AssetSelectionFiles[i].AssetId;
            }
            var include = result;
            CustomsearchresultService.DownloadAssetWithMetadata(arr, result)
            .then(function (result) {
                if (result.Response == null) {
                    NotifyError($translate.instant('LanguageContents.Res_4317.Caption'));
                    // $('#loadingDownloadPageModel').modal("hide");
                }
                else {
                    if (arr.length == 1) {
                        if (include == false) {
                            var fileid = result.Response[1], extn = '.xml';

                            if (result.Response[0] == null || result.Response[0] == "")
                                var filename = "MediabankAssets" + extn;
                            else
                                var filename = result.Response[0];

                        }
                        else {
                            var fileid = result.Response[1], extn = '.zip';
                            var filename = fileid + extn;
                        }
                    }
                    else {
                        var fileid = result.Response[1], extn = '.zip';
                        var filename = fileid + extn;
                    }
                    blockUIForDownload();
                    if ((parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) && include) {
                        location.href = 'DAMDownload.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#download_token_value_id').val();
                    } else {
                        location.href = 'DownloadAssetMetadata.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#download_token_value_id').val();
                    }
                    $timeout(function () { clearassetSelection(); }, 100);
                }
            });
        }

        function clearassetSelection() {
            for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                $('#chkBoxIcon_' + $scope.AssetSelectionFiles[i].AssetId).attr("class", "checkbox");
            }
            $scope.AssetSelectionFiles.splice(0, $scope.AssetSelectionFiles.length);
        }

        $scope.DownloadFileObj = [];
        function DownloadAssets(checkbox, assetid, name, ext, guid) {
            var arr = [];
            for (var i = 0; i < $scope.AssetSelectionFiles.length; i++) {
                arr[i] = $scope.AssetSelectionFiles[i].AssetId;
                $scope.DownloadFileObj[i].AssetId = $scope.AssetSelectionFiles[i].AssetId;
                $scope.DownloadFileObj.push({ AssetId: assetid, FileName: name, Fileguid: guid, Extension: ext });
            }
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.DownloadFileObj, function (e) { return e.AssetId == assetid; });
                if (remainRecord.length == 0) {
                    $scope.DownloadFileObj.push({ AssetId: assetid, FileName: name, Fileguid: guid, Extension: ext });
                }
            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.DownloadFileObj, function (e) { return e.AssetId == assetid; });
                if (remainRecord.length > 0) {
                    $scope.DownloadFileObj.splice($.inArray(remainRecord[0], $scope.DownloadFileObj), 1);
                }
            }

        }

        $scope.DownloadAssetsFiles = function () {
            if ($scope.AssetSelectionFiles.length > 0) {
                for (var i = 0, assetid; assetid = $scope.AssetSelectionFiles[i++];) {
                    //var remainRecord = [];

                    var asset = [], data = [];
                    asset = $.grep($scope.AssetSearchObj.AssetSearchfiles, function (e) {
                        return e.AssetUniqueID == assetid.AssetId;
                    });
                    //data = $.grep($scope.AssetSearchObj.AssetDynamicData, function (e) {
                    //    return e.AssetID == assetid.AssetId;
                    //});

                    $scope.DownloadFileObj.push({ AssetId: assetid.AssetId, FileName: asset[0].FileName, Fileguid: asset[0].FileGuid, Extension: asset[0].Extension });
                }
                download();
            }
            else {
                alert("Please Select");
            }

        }

        $scope.download = function () {

            for (var i = 0, assetid; assetid = $scope.AssetSelectionFiles[i++];) {
                //var remainRecord = [];

                var asset = [], data = [];
                asset = $.grep($scope.AssetSearchObj.AssetSearchfiles, function (e) {
                    return e.AssetUniqueID == assetid.AssetId;
                });
                $scope.DownloadFileObj.push({ AssetId: assetid.AssetId, FileName: asset[0].FileName, Fileguid: asset[0].FileGuid, Extension: asset[0].Extension });
            }

            if ($scope.DownloadFileObj.length == 0) {

                bootbox.alert($translate.instant('LanguageContents.Res_4610.Caption'));
            }

            if ($scope.DownloadFileObj.length == 1) {

                var arr = $scope.DownloadFileObj;
                var ID = 0;
                if ($scope.DownloadFileObj[0].AssetId != undefined) {
                    ID = $scope.DownloadFileObj[0].AssetId;
                }
                $timeout(function () {
                    var fileid = $scope.DownloadFileObj[0].Fileguid, extn = $scope.DownloadFileObj[0].Extension;
                    var filename = $scope.DownloadFileObj[0].FileName + extn;
                    //blockUIForDownload();
                    location.href = 'DAMDownload.aspx?FileID=' + $scope.DownloadFileObj[0].Fileguid + '&FileFriendlyName=' + $scope.DownloadFileObj[0].FileName + '&Ext=' + $scope.DownloadFileObj[0].Extension + '&token=' + $('#searchdownload_token_value_id').val();
                    //refreshDownLoadScope(arr);
                }, 100);
            }
            else if ($scope.DownloadFileObj.length > 1) {
                // returns all the extension types and related DAM types in the system.
                var arr = $scope.DownloadFileObj;
                $timeout(function () {
                    CustomsearchresultService.DownloadFiles(arr)
                                .then(function (result) {
                                    if (result.Response == null) {
                                        NotifyError($translate.instant('LanguageContents.Res_4317.Caption'));
                                        $('#mediasloadingDownloadPageModel').modal("hide");
                                    }
                                    else {
                                        $scope.DownloadFileObj = [];
                                        var fileid = result.Response, extn = '.zip';
                                        var filename = result.Response + extn;
                                        blockUIForDownload();
                                        location.href = 'DAMDownload.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#mediasdownload_token_value_id').val();
                                        //refreshDownLoadScope(arr);
                                    }
                                });

                }, 100);
            }

        }

        $scope.PerformAssetOperation = function () {

            if ($scope.AssetSelectionFiles.length > 0) {
                for (var i = 0, assetid; assetid = $scope.AssetSelectionFiles[i++];) {
                    var remainRecord = [];

                    $scope.damclipboard.assetselection.push({ "AssetId": assetid.AssetId });
                }
                $timeout(function () { $scope.AssetSelectionFiles = []; }, 200);
                NotifySuccess($translate.instant('LanguageContents.Res_1139.Caption'));
                //$scope.DownloadFileObj.DownloadFilesArr = [];
                //$scope.AssetFilesObj.AssetTaskObj = [];

            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }
        }

        $scope.sendMetadatMailSubject = "";
        //$scope.mediabank.mailtags = [];

        $scope.opensendpopup = function () {
            $scope.sendMetadatMailSubject = "";
            $scope.customsearch.mailtags = [];
            $('#sendsearchMailPopup').modal("show");
            $scope.includemdata = false;
        }

        $scope.includemdata = false;
        $scope.selectmetadata = function (e) {
            var chk = e.target;
            if (chk.checked)
                $scope.includemdata = true;
            else
                $scope.includemdata = false;

        }

        $scope.customsendmailOptions = {
            tags: [],
            multiple: true,
            simple_tags: false,
            minimumInputLength: 1,
            formatResult: function (item) {
                return item.text;
            },
            formatSelection: function (item) {
                return item.text;
            }
        }

        $scope.SendAssetWitMwtadata = function () {

            $scope.MailprocessMailFlag = true;

            var emailvalid = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var assetSelection = [], toArray = new Array(), mailSubject = $scope.sendMetadatMailSubject != "" ? $scope.sendMetadatMailSubject : "Marcom Media Bank";

            for (var i = 0, mail; mail = $scope.customsearch.mailtags[i++];) {
                if (mail.text.match(emailvalid)) {
                    toArray.push(mail.text);
                    //toArray.push($scope.customsearch.mailtags);
                }
                else {
                    $scope.MailprocessMailFlag = false;
                    return false;
                }
            }
            for (var i = 0, assetid; assetid = $scope.AssetSelectionFiles[i++];) {
                var remainRecord = [];

                assetSelection.push({ "AssetId": assetid.AssetId });
            }
            //assetSelection.push({ "AssetId": $scope.AssetEditID });
            if ($scope.AssetSelectionFiles.length > 0) {

                var srcObj = {};
                srcObj.Toaddress = toArray.join(",");
                srcObj.subject = mailSubject;
                srcObj.AssetArr = assetSelection;
                srcObj.metadata = $scope.includemdata;
                $timeout(function () {
                    CustomsearchresultService.SendMailWithMetaData(srcObj)
                                .then(function (result) {
                                    if (result.Response == null) {
                                        $scope.sendMetadatMailSubject = "";
                                        $scope.MailprocessMailFlag = false;
                                        $scope.customsearch.mailtags = [];
                                        NotifyError($translate.instant('LanguageContents.Res_4349.Caption'));
                                    }
                                    else {
                                        $scope.sendMetadatMailSubject = "";
                                        $scope.MailprocessMailFlag = false;
                                        $scope.customsearch.mailtags = [];
                                        NotifySuccess($translate.instant('LanguageContents.Res_4472.Caption'));
                                        $('#sendsearchMailPopup').modal("hide");
                                    }
                                });

                }, 100);
            }

            else {
                $scope.MailprocessMailFlag = false;
                bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'));
            }
        }

        function refreshDownLoadScope(arr) {
            for (var i = 0, item; item = arr[i++];) {

                $scope.AssetSelectionClass["asset_" + item.AssetId] = "th-Selection";
                $scope.AssetSelection["asset_" + item.AssetId] = false;
                $scope.AssetChkClass["asset_" + item.AssetId + ""] = "checkbox";

            }
        }

        var fileDownloadCheckTimer;
        function blockUIForDownload() {
            var token = new Date().getTime(); //use the current timestamp as the token value  
            $('#searchdownload_token_value_id').val(token);
            $('#searchloadingDownloadPageModel').modal("show");
            fileDownloadCheckTimer = setInterval(function () {
                var cookieValue = $.cookie('fileDownloadToken');
                finishDownload();
            }, 1000);
        }

        function finishDownload() {
            window.clearInterval(fileDownloadCheckTimer);
            $.removeCookie('fileDownloadToken'); //clears this cookie value  
            $('#searchloadingDownloadPageModel').modal("hide");
        }

        $scope.$on("$destroy", function () {
            $(window).unbind('scroll');
            $timeout.cancel(PreviewGeneratorTimer);
        });

        $scope.IsActiveEntity = function (EntityID, TypeId, parentId, isTask, ModuleId, event) {
            var isobjective = $(event.target).attr('data-isobjective');
            if (isTask == 1) {
                var entityid = parentId;
                var taskid = EntityID;
                var typeid = TypeId;

                CustomsearchresultService.IsActiveEntity(taskid).then(function (result) {
                    if (result.Response == true) {
                        var obj = {};
                        obj.TaskId = taskid;
                        obj.EntityId = entityid;
                        obj.TaskTypeId = TypeId;
                        $scope.$emit("pingMUITaskEdit", obj);
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                    }
                });


            }

            else {
                CustomsearchresultService.IsActiveEntity(EntityID).then(function (result) {
                    if (result.Response == true) {
                        if (ModuleId == 6) {
                            CustomsearchresultService.GetCmsEntitiesByID(EntityID).then(function (resNavID) {
                                if (resNavID.Response != null) {
                                    $scope.GlobalNavigationID.NavigationID = resNavID.Response[0].NavID.toString();
                                    $cookies["GlobalNavigationID"] = resNavID.Response[0].NavID.toString();
                                    $location.path('/mui/cms/detail/list/' + EntityID + '/entitycontent');
                                }
                            });
                        }
                        else {
                            if (TypeId == 5) {

                                $location.path('/mui/planningtool/costcentre/detail/section/' + EntityID + '/overview');



                            }
                            else if (TypeId == 7) {
                                $("#feedFundingRequestModal").modal("show");
                                $('#feedFundingRequestModal').trigger("onNewsfeedCostCentreFundingRequestsAction", [EntityID]);
                            }
                            else if (TypeId == 10) {
                                $location.path('/mui/objective/detail/section/' + EntityID + '/overview');
                            }
                            else if (TypeId == 35) {
                                $location.path('/mui/calender/detail/section/' + EntityID + '/overview');
                            }
                            else if (isobjective == "true") {
                                $location.path('/mui/objective/detail/section/' + EntityID + '/overview');
                            }
                            else {
                                $location.path('/mui/planningtool/default/detail/section/' + EntityID + '/overview');
                            }
                        }
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1866.Caption'));
                    }
                });
            }
        }

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.customsearchresultCntrl']"));
        });
        // --- Initialize. ---------------------------------- //

        // ...
    }
    app.controller("mui.customsearchresultCntrl", ['$scope', '$cookies', '$resource', '$compile', '$window', '$timeout', '$location', '$sce', '$translate', 'CustomsearchresultService', '$modal', muicustomsearchresultCntrl]);
    app.directive('buttonsRadio', function () {
        return {
            restrict: 'E',
            scope: { model: '=', options: '=', changeselection: "&" },
            controller: function ($scope) {
                $scope.activate = function (option) {
                    $scope.model = option.Lable;
                    $scope.changeselection({ option: option });
                };


                $scope.Returnclass = function (model) {
                    if ($scope.model == model.Lable) {
                        return "btn btn-small btn-primary";
                    }
                    else
                        return "btn btn-small";
                }
            },
            template: "<button type='button' class='{{Returnclass(option)}}' " +
                        "ng-class='{active: option.Lable == model}'" +
                        "ng-repeat='option in options' " +
                        "ng-click='activate(option)'>{{option.Lable}} " +
                      "</button>"
        };
    });

    function CustomsearchresultService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            Search: Search,
            PerformViewMoreSearch: PerformViewMoreSearch,
            IsActiveEntity: IsActiveEntity,
            GetCmsEntitiesByID: GetCmsEntitiesByID,
            GetCustomFilterAttributes: GetCustomFilterAttributes,
            GetAllExtensionTypesforDAM: GetAllExtensionTypesforDAM,
            GetAttributeDetails: GetAttributeDetails,
            DownloadFiles: DownloadFiles,
            SendMailWithMetaData: SendMailWithMetaData,
            CheckPreviewGeneratorID: CheckPreviewGeneratorID,
            DownloadAssetWithMetadata: DownloadAssetWithMetadata,
            CallbackSnippetSearch: CallbackSnippetSearch,
            GetObjectiveEntityType: GetObjectiveEntityType,
        });
        function Search(formobj) { var request = $http({ method: "post", url: "api/Planning/Search/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function PerformViewMoreSearch(formobj) { var request = $http({ method: "post", url: "api/Planning/PerformViewMoreSearch/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function CallbackSnippetSearch(formobj) { var request = $http({ method: "post", url: "api/Planning/CallbackSnippetSearch/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function IsActiveEntity(EntityID) { var request = $http({ method: "get", url: "api/common/IsActiveEntity/" + EntityID, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function GetCmsEntitiesByID(CmsEntityID) { var request = $http({ method: "get", url: "api/cms/GetCmsEntitiesByID/" + CmsEntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetCustomFilterAttributes(typeid) { var request = $http({ method: "get", url: "api/dam/GetCustomFilterAttributes/" + typeid, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetAllExtensionTypesforDAM() { var request = $http({ method: "get", url: "api/dam/GetDAMExtensions/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetAttributeDetails(damid) { var request = $http({ method: "get", ignoreLoadingBar: true, url: "api/dam/GetAssetAttributesDetails/" + damid, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DownloadFiles(arrObj) { var request = $http({ method: "post", url: "api/dam/DownloadDamZip/", params: { action: "add" }, data: { AssetArr: arrObj } }); return (request.then(handleSuccess, handleError)); }
        function SendMailWithMetaData(srcObj) { var request = $http({ method: "post", url: "api/dam/SendMailWithMetaData/", params: { action: "add" }, data: srcObj }); return (request.then(handleSuccess, handleError)); }
        function CheckPreviewGeneratorID(Assetids) { var request = $http({ method: "post", url: "api/dam/CheckPreviewGenerator/", params: { action: "add" }, data: { AssetIDs: Assetids } }); return (request.then(handleSuccess, handleError)); }
        function DownloadAssetWithMetadata(assetid, result) { var request = $http({ method: "post", url: "api/dam/DownloadAssetWithMetadata/", params: { action: "add" }, data: { assetid: assetid, result: result } }); return (request.then(handleSuccess, handleError)); }
        function GetObjectiveEntityType(IsAdmin) {

            var request = $http({
                method: "get",
                url: "api/Metadata/GetObjectiveEntityType/" + IsAdmin,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        } function handleSuccess(response) { return (response.data); }
    } app.service("CustomsearchresultService", ['$http', '$q', CustomsearchresultService]);

})(angular, app);