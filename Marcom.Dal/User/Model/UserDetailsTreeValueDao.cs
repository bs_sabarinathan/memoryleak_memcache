using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.User.Model
{

	/// <summary>
    /// TreeLevelDao object for table 'MM_TreeValue'.
	/// </summary>
	
	public partial class UserDetailsTreeValueDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
        protected int _userid;
		protected int _attributeid;
        protected int _Nodeid;
        protected int _level;
		
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public UserDetailsTreeValueDao() {}

        public UserDetailsTreeValueDao(int pId, int pUserID, int pAttributeid, int pNodeid,int pLevel)
		{
			this._id = pId; 
            this._userid = pUserID;
			this._attributeid = pAttributeid; 
            this._Nodeid = pNodeid;
            this._level = pLevel;
		}

        public UserDetailsTreeValueDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int UserID
		{
            get { return _userid; }
            set { _userid = value; }
		}
       
        public virtual int Attributeid
		{
			get { return _attributeid; }
            set { _attributeid = value; }

        }
        public virtual int Nodeid
        {
            get { return _Nodeid; }
            set { _Nodeid = value; }

		}
        public virtual int Level
        {
            get { return _level ; }
            set { _level = value; }

        }
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string UserDetailsTreeValueDao = "UserDetailsTreeValueDao";			
			public const string Id = "Id";
            public const string UserID = "UserID";
            public const string Attributeid = "Attributeid";
            public const string Nodeid = "Nodeid";
            public const string Level = "Level";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string UserDetailsTreeValueDao = "UserDetailsTreeValueDao";
            public const string Id = "Id";
            public const string UserID = "UserID";
            public const string Attributeid = "Attributeid";
            public const string Nodeid = "Nodeid";
            public const string Level = "Level";
		}

		#endregion
		
		
	}
	
}
