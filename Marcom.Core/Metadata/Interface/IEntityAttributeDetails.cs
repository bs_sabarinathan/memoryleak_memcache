﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface IEntityAttributeDetails
    {
        #region Public Properties
        int EntityId { get; set; }
        bool IsLock { get; set; }
        string ActivityName { get; set; }
        string Owner { get; set; }
        #endregion

    }
}
