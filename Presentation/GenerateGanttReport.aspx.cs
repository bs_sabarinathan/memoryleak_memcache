﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using System.IO;
using System.Collections;
using System.Data;
using System.Diagnostics;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Web;
using BrandSystems.Marcom.Core.Task.Interface;
using BrandSystems.Marcom.Core.Managers.Proxy;
using Presentation.Utility;

namespace Presentation
{
    public partial class GenerateGanttReport : System.Web.UI.Page
    {
        private int RowNo = 5;
        private int ColumnNo = 2;
        private int LastColumnNo = 0;
        private System.DateTime CalenderStartDate = new System.DateTime(System.DateTime.Now.Year - 1, 1, 1);
        private System.DateTime CalenderEndDate = new System.DateTime(System.DateTime.Now.Year + 1, 12, 31);


        private bool IsObjectiveOrCostCenterPresent = false;
        private int FilterID = 0;
        private string ListOfEntityID = "";
        private int TypeID = 0;
        private string GlobalAccess;
        protected void Page_Load(object sender, System.EventArgs e)
        {

            TenantResolver tenantres = new TenantResolver(System.Web.HttpContext.Current.Request.Url.Host.ToString());
            Guid userSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);
            IMarcomManager managers = MarcomManagerFactory.GetMarcomManager(null, userSession);
            string FileID = Request.QueryString["xid"];

            try
            {

                if (FileID == null)
                {


                    string something = HttpContext.Current.Request.QueryString["EID"];


                    ListOfEntityID = Request.QueryString["EID"];

                    IList<BrandSystems.Marcom.Core.Planning.Gantt> GV = new List<BrandSystems.Marcom.Core.Planning.Gantt>();

                    int intUserID = 0;

                    intUserID = 12;

                    string NewGuid = Guid.NewGuid().ToString();

                    dynamic fullpath = Server.MapPath("Reports/") + intUserID + "_" + NewGuid + ".xlsx";

                    FileInfo newFile = new FileInfo(fullpath);

                    ExcelPackage pck = new ExcelPackage(newFile);
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Monthly View");


                    List<string> AttributeList = new List<string>();
                    AttributeList.Add("ID#");
                    // need to check with prbhu
                    AttributeList.Add("Type");
                    AttributeList.Add("Status");

                    GenerateDynamicColumn(ws, AttributeList);

                    LastColumnNo = GenerateMonthlyViewHeader(ws, ColumnNo);
                    ws.View.FreezePanes(RowNo, 5);

                    GV = managers.PlanningManager.getReportData(ListOfEntityID);

                    GenerateGanttView(ws, GV);

                    //    'log("Page_Load", "Generation done")

                    ws.Column(1).Width = 30;
                    ws.View.ShowGridLines = false;



                    pck.Workbook.Properties.Title = "Monthly Gantt View";
                    pck.Workbook.Properties.Author = "Marcom Plarform";
                    pck.Workbook.Properties.Subject = "Monthly Gantt View";
                    pck.Workbook.Properties.Keywords = "Monthly Gantt View";

                    pck.Save();

                    string strFriendlyName = "Gantt-view-(Monthly)-" + System.DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx";

                    dynamic name = System.IO.Path.GetFileName(fullpath);
                    name = name.Replace(System.IO.Path.GetFileName(fullpath), strFriendlyName);
                    dynamic ext = System.IO.Path.GetExtension(fullpath);
                    string type = "";
                    type = "application/vnd.ms-excel";
                    Response.AppendHeader("content-disposition", "attachment; filename=\"" + strFriendlyName + "\"");
                    if (!string.IsNullOrEmpty(type))
                    {
                        Response.ContentType = type;
                        Response.WriteFile(fullpath);
                        Response.End();
                    }
                }

            }
            catch (Exception ex)
            {
                //Log("Page_Load Exception", ex.Message + Constants.vbNewLine + ex.StackTrace);
            }

        }

        public bool GenerateDynamicColumn(ExcelWorksheet ws, List<string> ColumnName)
        {

            foreach (var item in ColumnName)
            {


                var _with1 = ws.Cells[3, ColumnNo, 4, ColumnNo];
                _with1.Merge = true;
                _with1.Value = item.ToString();
                _with1.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with1.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with1.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with1.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with1.Style.Font.Name = "Calibri";
                _with1.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with1.Style.Font.Size = 10;
                _with1.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                _with1.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                ws.Column(ColumnNo).Width = 18;

                ColumnNo = ColumnNo + 1;

            }



            return true;
        }

        public int GenerateMonthlyViewHeader(ExcelWorksheet ws, int StartColumnNo)
        {



            int Column = StartColumnNo;
            int StartColumn = Column;
            ws.Row(2).Height = 12;
            ws.Row(3).Height = 10;
            ws.Row(4).Height = 10;







            DateTime startDate = new DateTime(CalenderStartDate.Year, CalenderStartDate.Month, CalenderStartDate.Day);
            DateTime stopDate = new DateTime(CalenderEndDate.Year, CalenderEndDate.Month, CalenderEndDate.Day);
            int interval = 1;

            DateTime dateTime = startDate;

            int DaysInMonth = System.DateTime.DaysInMonth(dateTime.Year, dateTime.Month);
            bool OnlyForFirstMonth = true;




            while (dateTime <= stopDate)
            {
                var _with1 = ws.Cells[3, Column];
                _with1.Value = new System.DateTime(dateTime.Year, dateTime.Month, dateTime.Day);
                _with1.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with1.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with1.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));


                _with1.Style.Font.Name = "Calibri";
                _with1.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with1.Style.Font.Size = 6;
                _with1.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                _with1.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                _with1.Style.Numberformat.Format = "ddd";

                var _with2 = ws.Cells[4, Column];
                _with2.Value = new System.DateTime(dateTime.Year, dateTime.Month, dateTime.Day);
                _with2.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with2.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with2.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with2.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with2.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with2.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with2.Style.Font.Name = "Calibri";
                _with2.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with2.Style.Font.Size = 6;
                _with2.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                _with2.Style.Numberformat.Format = "dd";

                int CurrentMonthinDays = System.DateTime.DaysInMonth(dateTime.Year, dateTime.Month);
                DateTime EndMonthDate = new DateTime(dateTime.Year, dateTime.Month, CurrentMonthinDays);
                dateTime += TimeSpan.FromDays(interval);
                Column = Column + 1;

                if (((EndMonthDate == dateTime | stopDate == dateTime | startDate == dateTime) & (stopDate >= dateTime)))
                {
                    if ((stopDate == dateTime) & EndMonthDate != dateTime)
                    {
                        CurrentMonthinDays = stopDate.Day;
                    }

                    if ((OnlyForFirstMonth == true))
                    {
                        if ((startDate.Day != 1 & startDate.Month == dateTime.Month & startDate.Year == dateTime.Year))
                        {
                            CurrentMonthinDays = (CurrentMonthinDays - startDate.Day + 1);
                            OnlyForFirstMonth = false;
                        }
                    }






                    var _with3 = ws.Cells[2, Column - (CurrentMonthinDays - 1), 2, Column];
                    _with3.Merge = true;
                    if (CurrentMonthinDays > 5)
                    {
                        _with3.Value = new System.DateTime(dateTime.Year, dateTime.Month, dateTime.Day);
                    }
                    _with3.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    _with3.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                    _with3.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    _with3.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                    _with3.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    _with3.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                    _with3.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    _with3.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));



                    _with3.Style.Font.Name = "Calibri";
                    _with3.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                    _with3.Style.Font.Size = 8;
                    _with3.Style.Font.Bold = true;




                    if ((CurrentMonthinDays < 15))
                    {
                        if ((dateTime.Month == CalenderEndDate.Month))
                        {
                            _with3.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }
                        else
                        {
                            _with3.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        }



                    }
                    else
                    {
                        _with3.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }


                    // .Style.HorizontalAlignment = ExcelHorizontalAlignment.Center



                    _with3.Style.Numberformat.Format = "mmm yyyy";


                    if (dateTime.Month % 2 == 0)
                    {
                        _with3.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _with3.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(242, 242, 242));
                    }
                }
                else
                {
                    // If they select Last day of the month in Start Date. this will get executed. 

                    if ((startDate == EndMonthDate))
                    {

                        var _with4 = ws.Cells[2, Column - 1, 2, Column - 1];
                        _with4.Merge = true;

                        _with4.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        _with4.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                        _with4.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        _with4.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                        _with4.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        _with4.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                        _with4.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        _with4.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));



                        _with4.Style.Font.Name = "Calibri";
                        _with4.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                        _with4.Style.Font.Size = 8;
                        _with4.Style.Font.Bold = true;
                        _with4.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        _with4.Style.Numberformat.Format = "mmm yyyy";


                        if (startDate.Month % 2 == 0)
                        {
                            _with4.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            _with4.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(242, 242, 242));
                        }

                    }
                }

            }

            var _with5 = ws.Column(StartColumn);
            _with5.ColumnMax = Column - 1;
            _with5.Width = 3.5;


            return Column - 1;
        }

        public bool GenerateGanttView(ExcelWorksheet ws, IList<BrandSystems.Marcom.Core.Planning.Gantt> GV)
        {

            foreach (BrandSystems.Marcom.Core.Planning.Gantt Item in GV)
            {

                var _with1 = ws.Cells[RowNo, 1];
                //.Value = Item.Name
                _with1.Value = HttpUtility.HtmlDecode(Item.Name);
                _with1.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with1.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with1.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with1.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with1.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));




                if (Item.Level == 0)
                {
                    _with1.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    _with1.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                }

                _with1.Style.Font.Name = "Calibri";

                _with1.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with1.Style.Font.Size = 10;

                _with1.Style.Indent = Item.Level + 1;

                int StartDynColNo = 2;

                var _with2 = ws.Cells[RowNo, StartDynColNo];

                _with2.Value = Item.ID;

                _with2.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with2.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with2.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with2.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with2.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with2.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with2.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with2.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));


                if (Item.Level == 0)
                {
                    _with2.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    _with2.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                }

                _with2.Style.Font.Name = "Calibri";
                _with2.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with2.Style.Font.Size = 10;


                _with2.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                _with2.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;

                var _with12 = ws.Cells[RowNo, StartDynColNo];




                _with12.Value = Item.TypeName;

                _with12.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with12.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with12.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with12.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with12.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with12.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with12.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with12.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));


                if (Item.Level == 0)
                {
                    _with12.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    _with12.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                }
                _with12.Style.Font.Name = "Calibri";
                _with12.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with12.Style.Font.Size = 10;


                _with12.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                _with12.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;

                var _with4 = ws.Cells[RowNo, StartDynColNo];

                _with4.Value = Item.Status;

                _with4.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                _with4.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with4.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                _with4.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with4.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                _with4.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                _with4.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                _with4.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));


                if (Item.Level == 0)
                {
                    _with4.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    _with4.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                }

                _with4.Style.Font.Name = "Calibri";
                _with4.Style.Font.Color.SetColor(System.Drawing.Color.FromArgb(51, 51, 51));
                _with4.Style.Font.Size = 10;


                _with4.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                _with4.Style.VerticalAlignment = ExcelVerticalAlignment.Center;


                StartDynColNo = StartDynColNo + 1;

                if ((Item.ParentID == 0))
                {
                    var _with5 = ws.Cells[RowNo, ColumnNo, RowNo, LastColumnNo];
                    _with5.Merge = true;
                    _with5.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    _with5.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                    _with5.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    _with5.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                    _with5.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    _with5.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));


                    if (Item.Level == 0)
                    {
                        _with5.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        _with5.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));
                    }

                    _with5.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    _with5.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(217, 217, 217));

                }
                else
                {

                    int StartColumnNo = ColumnNo;
                    int EndColumnNo = ColumnNo;


                    switch (new System.DateTime(CalenderStartDate.Year, CalenderStartDate.Month, CalenderStartDate.Day).DayOfWeek)
                    {
                        // need to take StartDate

                        case System.DayOfWeek.Monday:
                            EndColumnNo = EndColumnNo + 6;
                            break;
                        case System.DayOfWeek.Tuesday:
                            EndColumnNo = EndColumnNo + 5;
                            break;
                        case System.DayOfWeek.Wednesday:
                            EndColumnNo = EndColumnNo + 4;
                            break;
                        case System.DayOfWeek.Thursday:
                            EndColumnNo = EndColumnNo + 3;
                            break;
                        case System.DayOfWeek.Friday:
                            EndColumnNo = EndColumnNo + 2;
                            break;
                        case System.DayOfWeek.Saturday:
                            EndColumnNo = EndColumnNo + 1;
                            break;
                        case System.DayOfWeek.Sunday:
                            EndColumnNo = EndColumnNo + 0;
                            break;
                    }


                    bool IsOdd = true;

                    while (StartColumnNo <= LastColumnNo)
                    {
                        var _with6 = ws.Cells[RowNo, StartColumnNo, RowNo, EndColumnNo];
                        _with6.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        _with6.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                        _with6.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        _with6.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                        _with6.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        _with6.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                        _with6.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        _with6.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                        if (IsOdd)
                        {
                            _with6.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            _with6.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(242, 242, 242));
                            IsOdd = false;
                        }
                        else
                        {
                            IsOdd = true;
                        }


                        StartColumnNo = EndColumnNo + 1;
                        EndColumnNo = StartColumnNo + 6;

                        if (EndColumnNo > LastColumnNo)
                        {
                            EndColumnNo = LastColumnNo;
                        }
                    }





                    foreach (var bar in Item.StartEndDate.Split(new string[] { " ~~!~~ " }, StringSplitOptions.None))
                    {

                        if (bar.Length > 0)
                        {


                            DateTime StartDate = DateTime.Parse(bar.Split(':')[0]);
                            DateTime EndDate = DateTime.Parse(bar.Split(':')[1]);
                            string Startdatecomment = bar.Split(':')[2].Replace("~~!~~", "");


                            int BarStartColumn = StartDate.Subtract(CalenderStartDate).Days + ColumnNo;
                            int BarEndColumn = EndDate.Subtract(StartDate).Days + BarStartColumn;

                            if (BarStartColumn < ColumnNo)
                            {
                                BarStartColumn = ColumnNo;
                            }

                            if (BarEndColumn > LastColumnNo)
                            {
                                BarEndColumn = LastColumnNo;
                            }



                            if (BarStartColumn < LastColumnNo & BarEndColumn > ColumnNo)
                            {

                                try
                                {


                                    var _with7 = ws.Cells[RowNo, BarStartColumn, RowNo, BarEndColumn];
                                    _with7.Merge = true;
                                    _with7.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    _with7.Style.Border.Left.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                                    _with7.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    _with7.Style.Border.Right.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                                    _with7.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    _with7.Style.Border.Top.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                                    _with7.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    _with7.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.FromArgb(165, 165, 165));

                                    _with7.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    _with7.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(HexStrToBase10Int(Item.ColorCode)));




                                    dynamic Comment = ws.Comments.Add(ws.Cells[RowNo, BarStartColumn, RowNo, BarEndColumn], "Name: " + HttpUtility.HtmlDecode(Item.Name) + Environment.NewLine + "Start: " + StartDate.ToString("yyyy-MM-dd") + Environment.NewLine + "End: " + EndDate.ToString("yyyy-MM-dd") + Environment.NewLine + "Start/End Comment: " + HttpUtility.HtmlDecode(Startdatecomment.ToString()) + Environment.NewLine, "Marcom Platform");
                                    Comment.Font.FontName = "Calibri";
                                    Comment.Font.Size = 10;
                                    Comment.From.Column = 1;
                                    Comment.From.Row = 1;
                                    Comment.To.Column = 3;
                                    Comment.To.Row = 8;
                                    Comment.BackgroundColor = System.Drawing.Color.FromArgb(231, 242, 245);

                                }
                                catch (Exception ex)
                                {

                                }
                            }

                        }

                    }

                }


                RowNo = RowNo + 1;


            }



            return false;


        }

        protected int HexStrToBase10Int(string hex)
        {
            int base10value = 0;


            try
            {
                return (Convert.ToInt32(hex, 16));
            }
            catch
            { }


            return base10value;
        }



    }


}