using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// CalenderEntityValueDao object for table 'PM_CalenderEntityValue'.
	/// </summary>
	
	public partial class CalenderEntityValueDao : BaseDao, ICloneable 
	{
		#region Member Variables

        protected int _id;
		protected int _calenderid;
		protected int _entityid;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public CalenderEntityValueDao() {}

        public CalenderEntityValueDao(int pId, int pCalenderid, int pEntityid)
		{
            this._id = pId;
            this._calenderid = pCalenderid; 
			this._entityid = pEntityid; 			
		}
		
		#endregion
		
		#region Public Properties
		
        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }
        }
		public virtual int Calenderid
		{
			get { return _calenderid; }
            set { _bIsChanged |= (_calenderid != value); _calenderid = value; }
			
		}
		
		public virtual int Entityid
		{
			get { return _entityid; }
			set { _bIsChanged |= (_entityid != value); _entityid = value; }
			
		}
        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string CalenderEntityValueDao = "CalenderEntityValueDao";			
            public const string Id = "Id";
            public const string Calenderid = "Calenderid";			
			public const string Entityid = "Entityid";						
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string CalenderEntityValueDao = "CalenderEntityValueDao";			
            public const string Id = "Id";
            public const string Calenderid = "Calenderid";			
			public const string Entityid = "Entityid";							
		}

		#endregion
		
		
	}
	
}
