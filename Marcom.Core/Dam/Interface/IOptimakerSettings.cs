﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Dam.Interface
{
    public interface IOptimakerSettings
    {
        int ID { get; set; }
        string Name { get; set; }

        int CategoryID { get; set; }
        string Description { get; set; }

        int DocID
        {
            get;
            set;

        }

        int DepartmentID
        {
            get;
            set;

        }

        int DocType
        {
            get;
            set;

        }

        int DocVersionID
        {
            get;
            set;

        }

        string PreviewImage
        {
            get;
            set;

        }
        bool  AutoApproval
        {
            get;
            set;
        }
        int TemplateEngineType
        {
            get;
            set;
        }
        int AssetType
        {
            get;
            set;
        }
        string ServerURL
        {
            get;
            set;
        }
        bool EnableHighResPDF
        {
            get;
            set;
        }
       
    }
}
