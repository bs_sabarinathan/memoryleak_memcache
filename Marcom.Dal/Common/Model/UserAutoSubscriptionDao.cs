using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
	/// UserAutoSubscription object for table 'CM_UserAutoSubscription'.
	/// </summary>
	
	public partial class UserAutoSubscriptionDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _userid;
		protected int _entityid;
		protected SubscriptionTypeDao _subscriptionid;
		protected int _entitytypeid;
		protected bool _isvalid;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion

        #region Extensibility Method Definitions

        public override bool Equals(object obj)
        {
            UserAutoSubscriptionDao toCompare = obj as UserAutoSubscriptionDao;
            if (toCompare == null)
            {
                return false;
            }

            if (!Object.Equals(this.Userid, toCompare.Userid))
                return false;
            if (!Object.Equals(this.Entityid, toCompare.Entityid))
                return false;
            if (!Object.Equals(this.Subscriptionid, toCompare.Subscriptionid))
                return false;
            if (!Object.Equals(this.EntityTypeid, toCompare.EntityTypeid))
                return false;           

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 13;
            hashCode = (hashCode * 7) + Userid.GetHashCode();
            hashCode = (hashCode * 7) + Entityid.GetHashCode();
            hashCode = (hashCode * 7) + Subscriptionid.GetHashCode();
            hashCode = (hashCode * 7) + EntityTypeid.GetHashCode();            
            return hashCode;
        }

        #endregion
		
		#region Constructors
		public UserAutoSubscriptionDao() {}
			
		public UserAutoSubscriptionDao(int pUserid, int pEntityid, SubscriptionTypeDao pSubscriptionid, int pEntityTypeid, bool pIsValid)
		{
			this._userid = pUserid; 
			this._entityid = pEntityid; 
			this._subscriptionid = pSubscriptionid; 
			this._entitytypeid = pEntityTypeid; 
			this._isvalid = pIsValid; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Userid
		{
			get { return _userid; }
			set { _bIsChanged |= (_userid != value); _userid = value; }
			
		}
		
		public virtual int Entityid
		{
			get { return _entityid; }
			set { _bIsChanged |= (_entityid != value); _entityid = value; }
			
		}
		
		public virtual SubscriptionTypeDao Subscriptionid
		{
			get { return _subscriptionid; }
			set { _bIsChanged |= (_subscriptionid != value); _subscriptionid = value; }
			
		}
		
		public virtual int EntityTypeid
		{
			get { return _entitytypeid; }
			set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }
			
		}
		
		public virtual bool IsValid
		{
			get { return _isvalid; }
			set { _bIsChanged |= (_isvalid != value); _isvalid = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string UserAutoSubscription = "UserAutoSubscription";			
			public const string Userid = "Userid";			
			public const string Entityid = "Entityid";			
			public const string Subscriptionid = "Subscriptionid";			
			public const string EntityTypeid = "EntityTypeid";			
			public const string IsValid = "IsValid";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string UserAutoSubscription = "UserAutoSubscription";			
			public const string Userid = "Userid";			
			public const string Entityid = "Entityid";			
			public const string Subscriptionid = "Subscriptionid";			
			public const string EntityTypeid = "EntityTypeid";			
			public const string IsValid = "IsValid";			
		}

		#endregion
		
		
	}
	
}
