﻿(function (ng, app) {
    "use strict";

    function muiplanningtoolcalenderdetailfiltersettingsCtrl($scope, $location, $resource, $timeout, $cookies, $compile, $cookieStore, $translate, UserService, PlanningService) {
        var IsSave = 0;
        var IsUpdate = 0;
        $scope.showSave = true;
        $scope.showUpdate = false;
        $scope.appliedfilter = "No filter applied";
        $scope.deletefiltershow = false;
        $scope.ClearFilterAttributes = function () {
            $scope.ClearFieldsAndReApply();
        }
        var rememberfilter = [];
        $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
        $scope.EntityTypeID = SystemDefinedEntityTypes.Objective;
        $scope.visible = false;
        $scope.filterValues = [];
        $scope.atributesRelationList = [];
        $scope.filterSettingValues = [];
        $scope.ObjectiveOwners = {};
        $scope.Savefilter = true;
        $scope.ObjectiveFilter = {
            ngObjectivefilterStatus: '',
            ngObjectivefilterOwner: [],
            ngObjectiveType: [],
            ngObjectivefilterSaveAs: '',
            ngObjectivefilterKeyword: ''
        }
        $scope.objectivefiltercreation = function (event) {
            $scope.ClearScopeModle();
            $scope.Deletefilter = false;
            $scope.Applyfilter = true;
            $scope.Updatefilter = false;
            $scope.Savefilter = true;
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            IsSave = 0;
            IsUpdate = 0;
        };
        $("#rootlevelfilter").on("ClearScope", function (event) {
            $scope.showSave = true;
            $scope.showUpdate = false;
            $scope.deletefiltershow = false;
            $scope.ClearScopeModle();
        });
        $("#rootlevelfilter").on('ApplyRootLevelFilter', function (event, filterid, filtername) {
            $scope.ApplyFilter(filterid, filtername);
        });
        $("#rootlevelfilter").on('EditFilterSettingsByFilterID', function (event, filterid) {
            $scope.showSave = false;
            $scope.showUpdate = true;
            $scope.deletefiltershow = true;
            $scope.LoadFilterSettingsByFilterID(event, filterid);
        });
        $scope.ClearScopeModle = function () {
            $scope.ngsaveasfilter = "";
            $("#saveFilter").removeAttr('disabled');
            $scope.ObjectiveFilter.ngObjectivefilterOwner = [], $scope.ObjectiveFilter.ngObjectiveType = [], $scope.ObjectiveFilter.ngObjectivefilterSaveAs = '', $scope.ObjectiveFilter.ngObjectivefilterKeyword = ''
            $scope.ObjectiveFilter.ngObjectivefilterStatus = [];
        }
        $scope.ClearFieldsAndReApply = function () {
            $scope.ngsaveasfilter = "";
            $("#saveFilter").removeAttr('disabled');
            $scope.ObjectiveFilter.ngObjectivefilterOwner = [], $scope.ObjectiveFilter.ngObjectiveType = [], $scope.ObjectiveFilter.ngObjectivefilterSaveAs = '', $scope.ObjectiveFilter.ngObjectivefilterKeyword = ''
            $scope.ObjectiveFilter.ngObjectivefilterStatus = [];
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            var filterattr = [];
            $scope.showSave = true;
            $scope.showUpdate = false;
            $scope.deletefiltershow = false;
            RememberObjFilterID = 0;
            RememberObjFilterAttributes = [];
            $cookieStore.remove('ObjAttrs' + parseInt($cookies['UserId'], 10));
            $cookieStore.remove('ObjFilterID' + parseInt($cookies['UserId'], 10));
            $cookieStore.remove('ObjFilterName' + parseInt($cookies['UserId'], 10));
            RemeberObjFilterName = "No filter applied";
            $("#Div_Objlist").trigger('ClearAndReApply');
            if ($scope.EntityTypeID == 10) {
                $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, 0, filterattr)
            }
        }
        UserService.GetAllObjectiveMembers(parseInt($scope.EntityTypeID, 10)).then(function (GettingObjectiveOwnersObj) {
            var GettingObjectiveOwnersResult = GettingObjectiveOwnersObj.Response;
            $scope.ObjectiveOwners = GettingObjectiveOwnersObj.Response;
        });
        PlanningService.GetFilterSettings(parseInt($scope.EntityTypeID, 10)).then(function (filterSettings) {
            $scope.filterValues = filterSettings.Response;
        });
        $timeout(function () {
            PlanningService.GetFilterSettings(parseInt($scope.EntityTypeID, 10)).then(function (filterSettings) {
                $scope.filterValues = filterSettings.Response;
            });
        }, 100);
        $scope.FilterSave = function () {
            if (IsSave == 1) {
                return false;
            }
            IsSave = 1;
            if ($scope.ObjectiveFilter.ngObjectivefilterSaveAs == '' || $scope.ObjectiveFilter.ngObjectivefilterSaveAs == undefined) {
                bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));
                return false;
            }
            var filterattributes = [];
            var FilterData = {};
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            $("#saveFilter").attr('disabled', 'disabled');
            var FilterData = {};
            FilterData.FilterId = 0;
            FilterData.FilterName = $scope.ObjectiveFilter.ngObjectivefilterSaveAs;
            FilterData.Keyword = $scope.ObjectiveFilter.ngObjectivefilterKeyword;
            FilterData.UserId = 1;
            FilterData.TypeID = parseInt($scope.EntityTypeID, 10);
            FilterData.entityTypeId = '';
            FilterData.StarDate = '';
            FilterData.EndDate = '';
            FilterData.IsDetailFilter = 0;
            FilterData.WhereConditon = whereConditionData;
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            if ($scope.ObjectiveFilter.ngObjectivefilterOwner.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterOwner.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.Owner,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterOwner[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.Owner
                    });
                }
            }
            if ($scope.ObjectiveFilter.ngObjectiveType.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectiveType.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.ObjectiveType,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectiveType[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.ObjectiveType
                    });
                }
            }
            if ($scope.ObjectiveFilter.ngObjectivefilterStatus.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterStatus.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.ObjectiveStatus,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterStatus[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.ObjectiveType
                    });
                }
            }
            PlanningService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                if (filterSettingsInsertresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4283.Caption'));
                    IsSave = 0;
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4401.Caption'));
                    IsSave = 0;
                    $("#Div_Objlist").trigger('ReloadFilterSettings', [parseInt($scope.EntityTypeID, 10), $scope.ObjectiveFilter.ngObjectivefilterSaveAs, filterSettingsInsertresult.Response]);
                    filterSettingsInsertresult.WhereConditon = [];
                    $scope.FilterID.selectedFilterID = filterSettingsInsertresult.Response;
                    RememberObjFilterID = $scope.FilterID.selectedFilterID;
                    RememberObjFilterAttributes = filterattributes;
                    RemeberObjFilterName = filterSettingsInsertresult.FilterName;
                    $cookieStore.put('ObjAttrs' + parseInt($cookies['UserId'], 10), filterattributes);
                    $cookieStore.put('ObjFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                    $cookieStore.put('ObjFilterName' + parseInt($cookies['UserId'], 10), filterSettingsInsertresult.FilterName);
                    if ($scope.EntityTypeID == 10) {
                        $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, filterattributes)
                    }
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $scope.LoadFilterSettingsByFilterID = function (event, filterId) {
            $scope.Updatefilter = false;
            $scope.Applyfilter = false;
            $scope.Deletefilter = false;
            $scope.Savefilter = false;
            $scope.filterSettingValues = [];
            $scope.FilterID.selectedFilterID = filterId;
            PlanningService.GetFilterSettingValuesByFilertId($scope.FilterID.selectedFilterID).then(function (filterSettingsValues) {
                $scope.filterSettingValues = filterSettingsValues.Response;
                for (var i = 0; i < $scope.filterSettingValues.FilterValues.length; i++) {
                    if ($scope.filterSettingValues.FilterValues[i].AttributeId == SystemDefiendAttributes.Owner && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3)
                        $scope.ObjectiveFilter.ngObjectivefilterOwner.push($scope.filterSettingValues.FilterValues[i].Value);
                    else if ($scope.filterSettingValues.FilterValues[i].AttributeId == SystemDefiendAttributes.ObjectiveType)
                        $scope.ObjectiveFilter.ngObjectiveType.push($scope.filterSettingValues.FilterValues[i].Value);
                    else if ($scope.filterSettingValues.FilterValues[i].AttributeId == SystemDefiendAttributes.ObjectiveStatus)
                        $scope.ObjectiveFilter.ngObjectivefilterStatus.push($scope.filterSettingValues.FilterValues[i].Value);
                }
                $scope.ObjectiveFilter.ngObjectivefilterKeyword = $scope.filterSettingValues.Keyword;
                $scope.ObjectiveFilter.ngObjectivefilterSaveAs = $scope.filterSettingValues.FilterName;
                $scope.Updatefilter = true;
                $scope.Applyfilter = false;
                $scope.Deletefilter = true;
                $scope.Savefilter = false;
            });
            event.stopImmediatePropagation();
            event.stopPropagation();
        }
        $scope.FilterUpdate = function () {
            if (IsUpdate == 1) {
                return false;
            }
            IsUpdate = 1;
            $scope.filterSettingValues.Keyword = '';
            $scope.filterSettingValues.FilterName = '';
            var FilterData = {};
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            var filterattributes = [];
            if ($scope.ObjectiveFilter.ngObjectivefilterOwner.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterOwner.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.Owner,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterOwner[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.Owner
                    });
                }
            }
            if ($scope.ObjectiveFilter.ngObjectiveType.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectiveType.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.ObjectiveType,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectiveType[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.ObjectiveType
                    });
                }
            }
            if ($scope.ObjectiveFilter.ngObjectivefilterStatus.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterStatus.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.ObjectiveStatus,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterStatus[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.ObjectiveType
                    });
                }
            }
            FilterData.FilterId = parseInt($scope.FilterID.selectedFilterID, 10);
            FilterData.FilterName = $scope.ObjectiveFilter.ngObjectivefilterSaveAs;
            FilterData.Keyword = $scope.ObjectiveFilter.ngObjectivefilterKeyword;
            FilterData.UserId = 1;
            FilterData.TypeID = parseInt($scope.EntityTypeID, 10);
            FilterData.entityTypeId = '';
            FilterData.StarDate = '';
            FilterData.EndDate = '';
            FilterData.WhereConditon = whereConditionData;
            FilterData.IsDetailFilter = 0;
            PlanningService.InsertFilterSettings(FilterData).then(function (res) {
                if (res.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4358.Caption'));
                    IsUpdate = 0;
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4399.Caption'));
                    IsUpdate = 0;
                    res.WhereConditon = [];
                    $("#Div_Objlist").trigger('ReloadFilterSettings', [parseInt($scope.EntityTypeID, 10), $scope.ObjectiveFilter.ngObjectivefilterSaveAs, res.Response]);
                    $scope.appliedfilter = $scope.ObjectiveFilter.ngObjectivefilterSaveAs;
                    var StartRowNo = 0;
                    var MaxNoofRow = 20;
                    var PageIndex = 0;
                    RememberObjFilterID = res.Response;
                    RememberObjFilterAttributes = filterattributes;
                    RemeberObjFilterName = res.FilterName;
                    $cookieStore.put('ObjAttrs' + parseInt($cookies['UserId'], 10), filterattributes);
                    $cookieStore.put('ObjFilterID' + parseInt($cookies['UserId'], 10), res.Response);
                    $cookieStore.put('ObjFilterName' + parseInt($cookies['UserId'], 10), res.FilterName);
                    if ($scope.EntityTypeID == 10) {
                        $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, res.Response, filterattributes)
                    }
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $scope.ApplyFilter = function (FilterID, FilterName) {
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            if (FilterID != undefined && FilterName != undefined) {
                $scope.FilterID.selectedFilterID = FilterID;
                $scope.appliedfilter = FilterName;
            }
            $scope.filterSettingValues.Keyword = '';
            $scope.filterSettingValues.FilterName = '';
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            if ($scope.ObjectiveFilter.ngObjectivefilterOwner.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterOwner.length; k++) {
                    if ($scope.ObjectiveFilter.ngObjectivefilterKeyword != '') {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.Owner,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterOwner[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.Owner,
                            'Keyword': $scope.ObjectiveFilter.ngObjectivefilterKeyword
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.Owner,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterOwner[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.Owner,
                            'Keyword': ''
                        });
                    }
                }
            }
            if ($scope.ObjectiveFilter.ngObjectiveType.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectiveType.length; k++) {
                    if ($scope.ObjectiveFilter.ngObjectivefilterKeyword != '') {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.ObjectiveType,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectiveType[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.ObjectiveType,
                            'Keyword': $scope.ObjectiveFilter.ngObjectivefilterKeyword
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.ObjectiveType,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectiveType[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.ObjectiveType,
                            'Keyword': ''
                        });
                    }
                }
            }
            if ($scope.ObjectiveFilter.ngObjectivefilterStatus.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterStatus.length; k++) {
                    if ($scope.ObjectiveFilter.ngObjectivefilterKeyword != '') {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.ObjectiveType,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterStatus[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.ObjectiveType,
                            'Keyword': $scope.ObjectiveFilter.ngObjectivefilterKeyword
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.ObjectiveStatus,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterStatus[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.ObjectiveType,
                            'Keyword': ''
                        });
                    }
                }
            }
            if (whereConditionData.length == 0 && $scope.ObjectiveFilter.ngObjectivefilterKeyword != '') {
                whereConditionData.push({
                    'AttributeID': 0,
                    'SelectedValue': 0,
                    'Level': 0,
                    'AttributeTypeId': 0,
                    'Keyword': $scope.ObjectiveFilter.ngObjectivefilterKeyword
                });
            }
            $scope.AppllyFilterObj.selectedattributes = [];
            if (FilterID != undefined) {
                $scope.AppllyFilterObj.selectedattributes = [];
            } else {
                $scope.AppllyFilterObj.selectedattributes = whereConditionData;
            }
            if (FilterID == undefined) {
                FilterID = 0;
            }
            RememberObjFilterID = FilterID;
            RememberObjFilterAttributes = $scope.AppllyFilterObj.selectedattributes;
            RemeberObjFilterName = $scope.appliedfilter;
            $cookieStore.put('ObjAttrs' + parseInt($cookies['UserId'], 10), $scope.AppllyFilterObj.selectedattributes);
            $cookieStore.put('ObjFilterID' + parseInt($cookies['UserId'], 10), FilterID);
            $cookieStore.put('ObjFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
            if ($scope.EntityTypeID == 10) {
                $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, FilterID, $scope.AppllyFilterObj.selectedattributes)
            }
        };
        $scope.filtersettingsreset = function () {
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            $scope.FilterID.selectedFilterID = 0;
        };
        $scope.DeleteFilter = function DeleteFilterSettingsValue() {
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            var filterattribtues = [];
            bootbox.confirm($translate.instant('LanguageContents.Res_2026.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var ID = $scope.FilterID.selectedFilterID;
                        PlanningService.DeleteFilterSettings(ID).then(function (deletefilterbyFilterId) {
                            if (deletefilterbyFilterId.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4299.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4398.Caption'));
                                $('.FilterHolder').slideUp("slow");
                                $scope.showSave = true;
                                $scope.showUpdate = false;
                                $scope.ClearScopeModle();
                                $scope.ApplyFilter(0, 'No filter applied');
                                $("#Div_Objlist").trigger('ReloadFilterSettings', [parseInt($scope.EntityTypeID, 10), 'No filter applied', 0]);
                                $scope.FilterID.selectedFilterID = 0;
                                $scope.AppllyFilterObj.selectedattributes = [];
                                if ($scope.EntityTypeID == 10) {
                                    $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, 0, filterattribtues)
                                }
                            }
                        });
                    }, 100);
                }
            });
        };
        $("#rootlevelfilter").on('RemeberApplyFilterReLoad', function (event, TypeID, RememberPlanFilterAttributes) {
            UserService.GetAllObjectiveMembers(parseInt($scope.EntityTypeID, 10)).then(function (GettingObjectiveOwnersObj) {
                $scope.ObjectiveFilter.ngObjectivefilterOwner = [];
                $scope.ObjectiveFilter.ngObjectiveType = [];
                $scope.ObjectiveFilter.ngObjectivefilterKeyword = '';
                $scope.ObjectiveFilter.ngObjectivefilterStatus = [];
                var GettingObjectiveOwnersResult = GettingObjectiveOwnersObj.Response;
                $scope.ObjectiveOwners = GettingObjectiveOwnersObj.Response;
                $timeout(function () {
                    RememberFilterOnReLoad(RememberPlanFilterAttributes);
                }, 10);
            });
        });

        function RememberFilterOnReLoad(RememberPlanFilterAttributes) {
            if ($scope.EntityTypeID == 10) {
                if (RememberPlanFilterAttributes.length != 0) {
                    $('.FilterHolder').show()
                    $scope.ddlParententitytypeId = [];
                    $scope.filterSettingValues.FilterValues = RememberPlanFilterAttributes;
                    for (var i = 0; i < $scope.filterSettingValues.FilterValues.length; i++) {
                        if ($scope.filterSettingValues.FilterValues[i].AttributeID == SystemDefiendAttributes.Owner && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3)
                            $scope.ObjectiveFilter.ngObjectivefilterOwner.push($scope.filterSettingValues.FilterValues[i].SelectedValue);
                        else if ($scope.filterSettingValues.FilterValues[i].AttributeID == SystemDefiendAttributes.ObjectiveType)
                            $scope.ObjectiveFilter.ngObjectiveType.push($scope.filterSettingValues.FilterValues[i].SelectedValue);
                        else if ($scope.filterSettingValues.FilterValues[i].AttributeID == SystemDefiendAttributes.ObjectiveStatus)
                            $scope.ObjectiveFilter.ngObjectivefilterStatus.push($scope.filterSettingValues.FilterValues[i].SelectedValue);
                    }
                    $scope.ObjectiveFilter.ngObjectivefilterKeyword = $scope.filterSettingValues.FilterValues[0].Keyword;
                }
            }
        }
        $scope.ObjFilterStatus = [{
            Id: 1,
            FilterStatus: 'Active'
        }, {
            Id: 0,
            FilterStatus: 'Deactivated'
        }];
        $scope.ObjTypeList = [{
            Id: 1,
            objType: 'Numeric(Quantitative)'
        }, {
            Id: 2,
            objType: 'Numeric(Non Quantitative)'
        }, {
            Id: 3,
            objType: 'Qualitative'
        }, {
            Id: 4,
            objType: 'Rating'
        }];
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.calender.detail.filtersettingsCtrl']"));
        });
    }

    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) {
            return ($q.reject("An unknown error occurred."));
        }
        return ($q.reject(response.data.message));
    }

    function handleSuccess(response) {
        return (response.data);
    }
    app.controller("mui.planningtool.calender.detail.filtersettingsCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$compile', '$cookieStore', '$translate', 'UserService', 'PlanningService', muiplanningtoolcalenderdetailfiltersettingsCtrl]);
})(angular, app);