﻿(function (ng, app) {
    "use strict"; function DamentityselectService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({ GetDAMViewSettings: GetDAMViewSettings, GetDAMToolTipSettings:GetDAMToolTipSettings,GetEntityDamFolders: GetEntityDamFolders, GetEntityAsset: GetEntityAsset, CheckPreviewGeneratorID: CheckPreviewGeneratorID, GetAttributeDetails: GetAttributeDetails }); function GetDAMViewSettings() { var request = $http({ method: "get", url: "api/dam/GetDAMViewSettings/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
function GetDAMToolTipSettings() { var request = $http({ method: "get", url: "api/dam/GetDAMToolTipSettings/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
function GetEntityDamFolders(EntityID) { var request = $http({ method: "get", url: "api/dam/GetEntityDamFolder/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
function GetEntityAsset(FolderID,EntityID,View,Orderby,PageNo,ViewAll){var request=$http({method:"get",url:"api/dam/GetAssets/"+FolderID+"/"+EntityID+"/"+View+"/"+Orderby+"/"+PageNo+"/"+ViewAll,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function CheckPreviewGeneratorID(Assetids) { var request = $http({ method: "post", ignoreLoadingBar: true, url: "api/dam/CheckPreviewGenerator/", params: { action: "add" }, data: { AssetIDs: Assetids } }); return (request.then(handleSuccess, handleError)); }
function GetAttributeDetails(damid){var request=$http({method:"get",ignoreLoadingBar:true,url:"api/dam/GetAssetAttributesDetails/"+damid,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("DamentityselectService",['$http','$q',DamentityselectService]);})(angular,app);