using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

	/// <summary>
	/// EntityTypeHierarchy object for table 'MM_EntityType_Hierarchy'.
	/// </summary>

    internal class EntityTypeHierarchy : IEntityTypeHierarchy
    {
        #region Member Variables

        protected int _id;
        protected int _parentactivitytypeid;
        protected int _childactivitytypeid;
        protected int _sortorder;
        
        
        #endregion

        #region Constructors
        public EntityTypeHierarchy() { }

        public EntityTypeHierarchy(int pId, int pParentActivityTypeid, int pChildActivityTypeid, int pSortOrder)
        {
            this._id = pId;
            this._parentactivitytypeid = pParentActivityTypeid;
            this._childactivitytypeid = pChildActivityTypeid;
            this._sortorder = pSortOrder;
        }

        #endregion

        #region Public Properties


        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int ParentActivityTypeID
        {
            get { return _parentactivitytypeid; }
            set { _parentactivitytypeid = value; }

        }

        public int ChildActivityTypeID
        {
            get { return _childactivitytypeid; }
            set { _childactivitytypeid = value; }

        }

        public int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }

        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityTypeHierarchy castObj = null;
            try
            {
                castObj = (EntityTypeHierarchy)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }	
}
