﻿(function (ng, app) {
    "use strict";

    function muiadminuserCtrl($scope, $location, $resource, $timeout, $cookies, $compile, $translate, AdminService) {
        var model;
        $scope.GetNonBusinessDaysforDatePicker();
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            model = model1;
        };
        $scope.ApprovalRoles = {};
        $scope.GlobalRoles = {};
        $scope.listdata = {};
        $scope.Global = [];
        $scope.Dropdown = [];
        $scope.treeTexts = {};
        $scope.multiselecttreeTexts = {};
        $scope.treeSources = {};
        $scope.treeSourcesObj = [];
        $scope.UploadAttributeData = [];
        $scope.fields = {
            usersID: ''
        };
        $scope.OptionObj = {};
        $scope.fieldoptions = [];
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });
        }
        var myPSPlugin = $("[id$='userPassword']").password_strength();
        $scope.fieldKeys = [];
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.settreeSources = function () {
            var keys = [];
            angular.forEach($scope.treeSources, function (key) {
                keys.push(key);
                $scope.treeSourcesObj = keys;
            });
        }
        $scope.treeTextsObj = [];
        $scope.settreeTexts = function () {
            var keys2 = [];
            angular.forEach($scope.treeTexts, function (key) {
                keys2.push(key);
                $scope.treeTextsObj = keys2;
            });
        }
        $scope.treelevelsObj = [];
        $scope.settreelevels = function () {
            var keys1 = [];
            angular.forEach($scope.Inherritingtreelevels, function (key) {
                keys1.push(key);
                $scope.treelevelsObj = keys1;
            });
        }
        $scope.listAttriToAttriResult = [];
        $scope.ShowHideAttributeOnRelation = {};
        $scope.optionsLists = [];
        $scope.EnableDisableControlsHolder = {};
        $scope.AttributeData = [];
        $scope.entityName = "";
        $scope.EntityMemberData = [];
        $scope.atributesRelationList = [];
        AdminService.GetUserDetailsAttributes(12, 0).then(function (entityAttributesRelation) {
            $scope.atributesRelationList = entityAttributesRelation.Response;
            $scope.dyn_Cont = '';
            if ($scope.atributesRelationList != null) {
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                        var totLevelCnt = $scope.atributesRelationList[i].Levels.length;
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                return item.Caption
                            };
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                return item.Caption
                            };
                            if (j == 0) {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.treeSources["dropdown_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree);
                                $scope.EnableDisableControlsHolder["Treedropdown_" + $scope.atributesRelationList[i].AttributeID] = false;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Treedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                $scope.settreeSources();
                            } else {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Treedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                            }
                            $scope.setFieldKeys();
                        }
                        try {
                            var CaptionObj = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0].split(",");
                            var LabelObject = $scope.atributesRelationList[i].Lable[0];
                            for (var j = 0; j < LabelObject.length; j++) {
                                if (j == 0) {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.InheritingLevelsitems.push({
                                            caption: LabelObject[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = CaptionObj[j].trim();
                                        $scope.settreeTexts();
                                    } else {
                                        $scope.InheritingLevelsitems.push({
                                            caption: LabelObject[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                } else {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.InheritingLevelsitems.push({
                                            caption: LabelObject[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = CaptionObj[j].trim();
                                        $scope.settreeTexts();
                                    } else {
                                        $scope.InheritingLevelsitems.push({
                                            caption: LabelObject[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                }
                            }
                            $scope.Inherritingtreelevels["dropdown_levels_" + $scope.atributesRelationList[i].ID] = $scope.InheritingLevelsitems;
                            $scope.settreelevels();
                            $scope.InheritingLevelsitems = [];
                            $scope.settreeTexts();
                            $scope.settreelevels();
                        } catch (ex) { }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                        var totLevelCnt1 = $scope.atributesRelationList[i].Levels.length;
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                return item.Caption
                            };
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                return item.Caption
                            };
                            if (j == 0) {
                                $scope.treeSources["multiselectdropdown_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree);
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                $scope.EnableDisableControlsHolder["MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID] = false;
                                $scope.dyn_Cont += "<div  ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                $scope.settreeSources();
                            } else {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                    $scope.dyn_Cont += "<div  ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                    $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                } else {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                    $scope.dyn_Cont += "<div  ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                    $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                }
                            }
                            $scope.setFieldKeys();
                        }
                        try {
                            var CaptionObj = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0].toString().split(",");
                            var LabelObject = $scope.atributesRelationList[i].Lable[0];
                            for (var j = 0; j < LabelObject.length; j++) {
                                if (j == 0) {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.InheritingLevelsitems.push({
                                            caption: LabelObject[j].Label,
                                            level: j + 1
                                        });
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = CaptionObj[j].trim();
                                        $scope.settreeTexts();
                                    } else {
                                        $scope.InheritingLevelsitems.push({
                                            caption: LabelObject[j].Label,
                                            level: j + 1
                                        });
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                } else {
                                    if (j == (LabelObject.length - 1)) {
                                        var k = j;
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = [];
                                        $scope.InheritingLevelsitems.push({
                                            caption: LabelObject[j].Label,
                                            level: j + 1
                                        });
                                        for (k; k < (CaptionObj.length) ; k++) {
                                            if (CaptionObj[k] != undefined) {
                                                $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)].push(CaptionObj[k].trim());
                                            } else {
                                                $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                            }
                                            $scope.settreeTexts();
                                        }
                                    } else {
                                        if (CaptionObj[j] != undefined) {
                                            $scope.InheritingLevelsitems.push({
                                                caption: LabelObject[j].Label,
                                                level: j + 1
                                            });
                                            $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = CaptionObj[j].trim();
                                            $scope.settreeTexts();
                                        } else {
                                            $scope.InheritingLevelsitems.push({
                                                caption: LabelObject[j].Label,
                                                level: j + 1
                                            });
                                            $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                            $scope.settreeTexts();
                                        }
                                    }
                                }
                            }
                            $scope.Inherritingtreelevels["multiselectdropdown_levels_" + $scope.atributesRelationList[i].ID] = $scope.InheritingLevelsitems;
                            $scope.settreelevels();
                            $scope.InheritingLevelsitems = [];
                            $scope.settreeTexts();
                            $scope.settreelevels();
                        } catch (ex) { }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        if ($scope.atributesRelationList[i].IsSpecial == true) {
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"> <input ng-disabled=\"EnableDisableControlsHolder.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.OwnerList[0].UserName;
                                $scope.setFieldKeys();
                            }
                        } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.EntityStatus) { } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            $scope.setoptions();
                            $scope.EnableDisableControlsHolder["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2  ng-disabled=\"EnableDisableControlsHolder.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,3)\"  ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" > <option value=\"\">Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \"value=\"{{ndata.Id}}\">{{ndata.Caption}}</option> </select></div></div>";
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                            $scope.setFieldKeys();
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        if ($scope.atributesRelationList[i].AttributeID != 70) {
                            $scope.EnableDisableControlsHolder["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                            } else {
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                            }
                            $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = "";
                            $scope.setFieldKeys();
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea ng-disabled=\"EnableDisableControlsHolder.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea></div></div>";
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        $scope.EnableDisableControlsHolder["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> ";
                        $scope.dyn_Cont += "<select ui-select2  ng-disabled=\"EnableDisableControlsHolder.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\" ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DateTime_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.DateTime_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-model=\"fields.DateTime_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DateTime_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID + "\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        var param1 = new Date.create();
                        var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                        $scope.fields["DateTime_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextMoney_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"text\" ng-model=\"fields.TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        $scope.fields["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls chkbx\"><input ng-disabled=\"EnableDisableControlsHolder.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["Period_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        try {
                            if ($scope.atributesRelationList[i].InheritFromParent) {
                                if ($scope.atributesRelationList[i].ParentValue == "-") {
                                    $scope.items.push({
                                        startDate: [],
                                        endDate: [],
                                        comment: '',
                                        sortorder: 0
                                    });
                                } else {
                                    if ($scope.atributesRelationList[i].ParentValue.length > 0) {
                                        for (var j = 0; j < $scope.atributesRelationList[i].ParentValue[0].length; j++) {
                                            var datStartUTCval = "";
                                            var datstartval = "";
                                            var datEndUTCval = "";
                                            var datendval = "";
                                            datStartUTCval = $scope.atributesRelationList[i].ParentValue[0][j].Startdate.substr(6, ($scope.atributesRelationList[i].ParentValue[0][j].Startdate.indexOf('+') - 6));
                                            datstartval = new Date.create(parseInt(datStartUTCval));
                                            datEndUTCval = $scope.atributesRelationList[i].ParentValue[0][j].EndDate.substr(6, ($scope.atributesRelationList[i].ParentValue[0][j].EndDate.indexOf('+') - 6));
                                            datendval = new Date.create(parseInt(datEndUTCval));
                                            $scope.items.push({
                                                startDate: ConvertStringToDate(ConvertDateToString(datstartval)),
                                                endDate: ConvertStringToDate(ConvertDateToString(datendval)),
                                                comment: $scope.atributesRelationList[i].ParentValue[0][j].Description,
                                                sortorder: 0
                                            });
                                        }
                                    } else {
                                        $scope.items.push({
                                            startDate: [],
                                            endDate: [],
                                            comment: '',
                                            sortorder: 0
                                        });
                                    }
                                }
                            } else {
                                $scope.items.push({
                                    startDate: [],
                                    endDate: [],
                                    comment: '',
                                    sortorder: 0
                                });
                            }
                        } catch (e) { }
                        $scope.dyn_Cont += "    <div class=\"control-group\"><label for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.atributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                        $scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span5\">";
                        $scope.dyn_Cont += "<input class=\"sdate Period_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"CheckPreviousEndDate(item.endDate , item.startDate,$index)\" ng-disabled=\"EnableDisableControlsHolder.Period_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-model=\"item.startDate\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + "item.startDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open" + "item.startDate" + "\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- Start date --\"/>";
                        $scope.fields["DatePart_Calander_Open" + "item.startDate"] = false;
                        $scope.dyn_Cont += "<input class=\"edate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Period_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" name=\"enddate\" id=\"item.endDate\" ng-model=\"item.endDate\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + "item.endDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open" + "item.endDate" + "\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- End date --\"/>";
                        $scope.fields["DatePart_Calander_Open" + "item.endDate"] = false;
                        $scope.dyn_Cont += "<input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Period_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\" ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
                        $scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
                        $scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
                        $scope.fields["Period_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        $scope.dyn_Cont += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class="control-group ng-scope"><label class="control-label"';
                        $scope.dyn_Cont += 'for="fields.Uploader_ ' + $scope.atributesRelationList[i].AttributeID + '">' + $scope.atributesRelationList[i].AttributeCaption + ': </label>';
                        $scope.dyn_Cont += '<div id="Uploader" class="controls">';
                        if ($scope.atributesRelationList[i].Value == "" || $scope.atributesRelationList[i].Value == null && $scope.atributesRelationList[i].Value == undefined) {
                            $scope.atributesRelationList[i].Value = "NoThumpnail.jpg";
                        }
                        $scope.dyn_Cont += '<img src="' + imagesrcpath + 'UploadedImages/' + $scope.atributesRelationList[i].Value + '" alt="' + $scope.atributesRelationList[i].Caption + '"';
                        $scope.dyn_Cont += 'ng-model="fields.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" class="ng-pristine ng-valid entityDetailImgPreview" id="UploaderImageControl">';
                        $scope.dyn_Cont += '<br><a ng-show="EnableDisableControlsHolder.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" ng-model="UploadImage" ng-click="UploadImagefile()" data-target="#EntityCreationUplaodImagediv" data-toggle="modal" class="ng-pristine ng-valid">Select Image</a>';
                        $scope.dyn_Cont += '</div></div>';
                        $scope.fields["Uploader_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    }
                    $scope.setFieldKeys();
                    $scope.setoptions();
                }
            }
            $scope.setFieldKeys();
            $("#dynamic_UserDetails").html('<div class="row-fluid"><div data-col="1" class="span4"></div><div data-col="2" class="span8"></div></div> ');
            $("#dynamic_UserDetails").html($compile($scope.dyn_Cont)($scope));
            $("#dynamic_UserDetails").scrollTop(0);
        });
        $scope.ClearScopeModle = function () {
            for (var variable in $scope.fields) {
                if (variable.indexOf("DateTime") != -1) {
                    $scope.fields[variable] = [];
                }
                if (typeof $scope.fields[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        $scope.fields[variable] = "";
                    }
                } else if (typeof $scope.fields[variable] === "number") {
                    $scope.fields[variable] = null;
                } else if (Array.isArray($scope.fields[variable])) {
                    $scope.fields[variable] = [];
                } else if (typeof $scope.fields[variable] === "object") {
                    $scope.fields[variable] = {};
                } else if (typeof $scope.fields[variable] === "boolean") {
                    $scope.fields[variable] = false;
                }
            }
        }
        $scope.editableInPopup = '<a class="iconLink" ng-click="GetUserByID(row)" ><i class="icon-edit"></i></a>'
        $scope.delete = '<a class="iconLink" data-toggle="modal" ng-click="DeleteUserByID(row)"  data-toggle="modal"><i class="icon-remove"></i></a>'
        $scope.DeleteUserByID = function DeleteUserByID(row) {
            AdminService.CheckUserInvolvement(row.entity.Id).then(function (checkUserInvolvement) {
                if (checkUserInvolvement.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4313.Caption'));
                } else {
                    if (checkUserInvolvement.Response == false) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1850.Caption'));
                    } else {
                        bootbox.confirm($translate.instant('LanguageContents.Res_1851.Caption') + " " + row.entity.FirstName + " " + row.entity.LastName + " ?", function (result) {
                            if (result) {
                                $timeout(function () {
                                    AdminService.DeleteUser(row.entity.Id).then(function (deleteuser) {
                                        if (deleteuser.StatusCode == 405) {
                                            NotifyError($translate.instant('LanguageContents.Res_4313.Caption'));
                                        } else {
                                            if (deleteuser.Response == true) {
                                                $scope.listdata.splice(row.rowIndex, 1);
                                                $scope.parentUserList = $scope.listdata;
                                                NotifySuccess($translate.instant('LanguageContents.Res_4798.Caption'));
                                            } else {
                                                NotifyError($translate.instant('LanguageContents.Res_4313.Caption'));
                                            }
                                        }
                                    });
                                }, 100);
                            } else { }
                        });
                    }
                }
            });
        }
        $scope.GetUserByID = function GetUserByID(row) {
            $("#userModal").modal('show');
            $scope.ClearScopeModle();
            $scope.Dropdown = [];
            $scope.treeTexts = {};
            $scope.multiselecttreeTexts = {};
            $scope.treeSources = {};
            $scope.treeSourcesObj = [];
            $scope.RowIndex = row.rowIndex;
            $scope.UserID = row.entity.Id;
            $scope.FirstName = row.entity.FirstName;
            $scope.LastName = row.entity.LastName;
            $scope.UserName = row.entity.UserName;
            $scope.Password = row.entity.Password;
            $scope.Email = row.entity.Email;
            $scope.Language = row.entity.Language;
            $scope.StartPage = row.entity.StartPage;
            $scope.Imageurl = row.entity.Image;
            $scope.inlineCheckboxSSOUserid = Boolean(row.entity.IsSSOUser.toString().toLowerCase() == "true" ? 1 : 0);
            $scope.inlineCheckboxIsApiUser = Boolean(row.entity.IsAPIUser.toString().toLowerCase() == "true" ? 1 : 0);
            if (row.entity.Image == 0) $scope.Imageurl = "1.png";
            AdminService.GetGlobalRoleUserByID(row.entity.Id).then(function (getroleuserbyid) {
                $scope.ddlGlobalRole = getroleuserbyid.Response;
                var a = $scope.ddlGlobalRole;
                AdminService.GetAssetAccessByID(row.entity.Id).then(function (getassetaccessbyid) {
                    $scope.AssetAccess = getassetaccessbyid.Response;
                    var acc = $scope.AssetAccess;
                });
                AdminService.GetApprovalRoleUserByID(row.entity.Id).then(function (getapprovalrolebyid) {
                    $scope.ddlApprovalRole = getapprovalrolebyid.Response;
                    var approval = $scope.ddlApprovalRole;
                });
                AdminService.GetUserDetailsAttributes(12, row.entity.Id).then(function (entityAttributesRelation) {
                    $scope.atributesRelationListEdit = entityAttributesRelation.Response;
                    if ($scope.atributesRelationListEdit != null) {
                        for (var i = 0; i < $scope.atributesRelationListEdit.length; i++) {
                            if ($scope.atributesRelationListEdit[i].AttributeTypeID == 6) {
                                var totLevelCnt = $scope.atributesRelationListEdit[i].Levels.length;
                                for (var j = 0; j < $scope.atributesRelationListEdit[i].Levels.length; j++) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = true;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = {};
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].multiple = false;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                        return item.Caption
                                    };
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                        return item.Caption
                                    };
                                    if (j == 0) {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationListEdit[i].tree).Children;
                                        $scope.treeSources["dropdown_" + $scope.atributesRelationListEdit[i].AttributeID] = JSON.parse($scope.atributesRelationListEdit[i].tree);
                                        $scope.EnableDisableControlsHolder["Treedropdown_" + $scope.atributesRelationListEdit[i].AttributeID] = false;
                                        if ($scope.atributesRelationListEdit[i].AttributeValue[j] != undefined) {
                                            $scope.fields["DropDown_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = $.grep($scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].data, function (e) {
                                                return e.id == $scope.atributesRelationListEdit[i].AttributeValue[j];
                                            })[0];
                                        }
                                        $scope.settreeSources();
                                    } else {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].data = [];
                                        if ($scope.atributesRelationListEdit[i].AttributeValue[j - 1] != undefined) $scope.ShowHideAttributeToAttributeRelations($scope.atributesRelationListEdit[i].AttributeID, j, totLevelCnt, 6);
                                        if ($scope.atributesRelationListEdit[i].AttributeValue[j] != undefined) {
                                            $scope.fields["DropDown_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = $.grep($scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].data, function (e) {
                                                return e.id == $scope.atributesRelationListEdit[i].AttributeValue[j];
                                            })[0];
                                        }
                                    }
                                }
                                try {
                                    var CaptionObj = $scope.atributesRelationListEdit[i].ParentTreeLevelValueCaption[0].split(",");
                                    var LabelObject = $scope.atributesRelationListEdit[i].Lable[0];
                                    for (var j = 0; j < LabelObject.length; j++) {
                                        if (j == 0) {
                                            if (CaptionObj[j] != undefined) {
                                                $scope.InheritingLevelsitems.push({
                                                    caption: LabelObject[j].Label,
                                                    level: j + 1
                                                });
                                                $scope.treeTexts["dropdown_text_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = CaptionObj[j].trim();
                                                $scope.settreeTexts();
                                            } else {
                                                $scope.InheritingLevelsitems.push({
                                                    caption: LabelObject[j].Label,
                                                    level: j + 1
                                                });
                                                $scope.treeTexts["dropdown_text_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = "-";
                                                $scope.settreeTexts();
                                            }
                                        } else {
                                            if (CaptionObj[j] != undefined) {
                                                $scope.InheritingLevelsitems.push({
                                                    caption: LabelObject[j].Label,
                                                    level: j + 1
                                                });
                                                $scope.treeTexts["dropdown_text_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = CaptionObj[j].trim();
                                                $scope.settreeTexts();
                                            } else {
                                                $scope.InheritingLevelsitems.push({
                                                    caption: LabelObject[j].Label,
                                                    level: j + 1
                                                });
                                                $scope.treeTexts["dropdown_text_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = "-";
                                                $scope.settreeTexts();
                                            }
                                        }
                                    }
                                    $scope.Inherritingtreelevels["dropdown_levels_" + $scope.atributesRelationListEdit[i].AttributeID] = $scope.InheritingLevelsitems;
                                    $scope.settreelevels();
                                    $scope.InheritingLevelsitems = [];
                                    $scope.settreeTexts();
                                    $scope.settreelevels();
                                } catch (ex) { }
                            } else if ($scope.atributesRelationListEdit[i].AttributeTypeID == 12) {
                                for (var j = 0; j < $scope.atributesRelationListEdit[i].Levels.length; j++) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = true;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = {};
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                        return item.Caption
                                    };
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                        return item.Caption
                                    };
                                    if (j == 0) {
                                        $scope.treeSources["multiselectdropdown_" + $scope.atributesRelationListEdit[i].AttributeID] = JSON.parse($scope.atributesRelationListEdit[i].tree);
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationListEdit[i].tree).Children;
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].multiple = false;
                                        if ($scope.atributesRelationListEdit[i].AttributeValue[j] != undefined) {
                                            $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = $.grep($scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].data, function (e) {
                                                return e.id == $scope.atributesRelationListEdit[i].AttributeValue[j];
                                            })[0];
                                        }
                                        $scope.EnableDisableControlsHolder["MultiSelectTreedropdown_" + $scope.atributesRelationListEdit[i].AttributeID] = false;
                                        $scope.settreeSources();
                                    } else {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].data = [];
                                        if ($scope.atributesRelationListEdit[i].AttributeValue[j - 1] != undefined) $scope.ShowHideAttributeToAttributeRelations($scope.atributesRelationListEdit[i].AttributeID, j, $scope.atributesRelationListEdit[i].Levels.length, 12);
                                        if (j == ($scope.atributesRelationListEdit[i].Levels.length - 1)) {
                                            if ($scope.atributesRelationListEdit[i].AttributeValue[j] != undefined) {
                                                var tempval = [];
                                                var k = j;
                                                for (k; k < $scope.atributesRelationListEdit[i].AttributeValue.length; k++) {
                                                    tempval.push($.grep($scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].data, function (e) {
                                                        return e.id == $scope.atributesRelationListEdit[i].AttributeValue[k];
                                                    })[0]);
                                                }
                                                $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = tempval;
                                            }
                                            $scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].multiple = true;
                                        } else {
                                            if ($scope.atributesRelationListEdit[i].AttributeValue[j] != undefined) {
                                                $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = $.grep($scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].data, function (e) {
                                                    return e.id == $scope.atributesRelationListEdit[i].AttributeValue[j];
                                                })[0];
                                            }
                                            $scope.Dropdown["OptionValues" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].multiple = false;
                                        }
                                    }
                                }
                                try {
                                    var CaptionObj = $scope.atributesRelationListEdit[i].ParentTreeLevelValueCaption[0].toString().split(",");
                                    var LabelObject = $scope.atributesRelationListEdit[i].Lable[0];
                                    for (var j = 0; j < LabelObject.length; j++) {
                                        if (j == 0) {
                                            if (CaptionObj[j] != undefined) {
                                                $scope.InheritingLevelsitems.push({
                                                    caption: LabelObject[j].Label,
                                                    level: j + 1
                                                });
                                                $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = CaptionObj[j].trim();
                                                $scope.settreeTexts();
                                            } else {
                                                $scope.InheritingLevelsitems.push({
                                                    caption: LabelObject[j].Label,
                                                    level: j + 1
                                                });
                                                $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = "-";
                                                $scope.settreeTexts();
                                            }
                                        } else {
                                            if (j == (LabelObject.length - 1)) {
                                                var k = j;
                                                $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = [];
                                                $scope.InheritingLevelsitems.push({
                                                    caption: LabelObject[j].Label,
                                                    level: j + 1
                                                });
                                                for (k; k < (CaptionObj.length) ; k++) {
                                                    if (CaptionObj[k] != undefined) {
                                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)].push(CaptionObj[k].trim());
                                                    } else {
                                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = "-";
                                                    }
                                                    $scope.settreeTexts();
                                                }
                                            } else {
                                                if (CaptionObj[j] != undefined) {
                                                    $scope.InheritingLevelsitems.push({
                                                        caption: LabelObject[j].Label,
                                                        level: j + 1
                                                    });
                                                    $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = CaptionObj[j].trim();
                                                    $scope.settreeTexts();
                                                } else {
                                                    $scope.InheritingLevelsitems.push({
                                                        caption: LabelObject[j].Label,
                                                        level: j + 1
                                                    });
                                                    $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationListEdit[i].AttributeID + "_" + (j + 1)] = "-";
                                                    $scope.settreeTexts();
                                                }
                                            }
                                        }
                                    }
                                    $scope.Inherritingtreelevels["multiselectdropdown_levels_" + $scope.atributesRelationListEdit[i].AttributeID] = $scope.InheritingLevelsitems;
                                    $scope.settreelevels();
                                    $scope.InheritingLevelsitems = [];
                                    $scope.settreeTexts();
                                    $scope.settreelevels();
                                } catch (ex) { }
                            } else if ($scope.atributesRelationListEdit[i].AttributeTypeID == 3) {
                                $scope.fields["ListSingleSelection_" + $scope.atributesRelationListEdit[i].AttributeID] = $scope.atributesRelationListEdit[i].AttributeValue;
                            } else if ($scope.atributesRelationListEdit[i].AttributeTypeID == 1) {
                                if ($scope.atributesRelationListEdit[i].AttributeID != 70) {
                                    $scope.fields["TextSingleLine_" + $scope.atributesRelationListEdit[i].AttributeID] = $scope.atributesRelationListEdit[i].AttributeValue;
                                }
                            } else if ($scope.atributesRelationListEdit[i].AttributeTypeID == 2) {
                                $scope.fields["TextMultiLine_" + $scope.atributesRelationListEdit[i].AttributeID] = $scope.atributesRelationListEdit[i].AttributeValue;
                            } else if ($scope.atributesRelationListEdit[i].AttributeTypeID == 4) {
                                for (var k = 0; k < $scope.atributesRelationListEdit[i].AttributeValue.length; k++) {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationListEdit[i].AttributeID].push($scope.atributesRelationListEdit[i].AttributeValue[k]);
                                }
                            } else if ($scope.atributesRelationListEdit[i].AttributeTypeID == 5 && $scope.atributesRelationListEdit[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationListEdit[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                                $scope.fields["DateTime_" + $scope.atributesRelationListEdit[i].AttributeID] = ConvertDateToString($scope.atributesRelationListEdit[i].AttributeValue.toString(), "yyyy/MM/dd");
                            } else if ($scope.atributesRelationListEdit[i].AttributeTypeID == 9) {
                                $scope.fields["CheckBoxSelection_" + $scope.atributesRelationListEdit[i].AttributeID] = $scope.atributesRelationListEdit[i].AttributeValue;
                            }
                        }
                    }
                });
            });
            $scope.IsSSO = true;
            $scope.UpdHide = false;
            $scope.EnableUpdate = true;
            $scope.EnableAdd = false;
            $scope.isDisabled = true;
            $('#TimeZone').val(row.entity.TimeZone);
            $('#TimeZone').select2();
            $timeout(function () {
                $('#FirstName').focus().select();
            }, 1000);
        };
        AdminService.GetUsers().then(function (getusers) {
            $scope.listdata = getusers.Response;
            $scope.parentUserList = $scope.listdata;
        });
        AdminService.GetGlobalRole().then(function (getroles) {
            $scope.GlobalRoles = getroles.Response;
        });
        AdminService.GetApprovalrole().then(function (getapprovalroles) {
            $scope.ApprovalRoles = getapprovalroles.Response;
        });
        AdminService.GetNavigationType().then(function (getstartpage) {
            $scope.StartPages = getstartpage.Response;
        });
        AdminService.GetAssetAccess().then(function (getassetaccess) {
            $scope.AssetAccessSrc = getassetaccess.Response;
        });
        $scope.addUser = function () {
            $("#userModal").modal('show');
            $scope.ClearScopeModle();
            $scope.inlineCheckboxIsApiUser = false;
            $scope.EnableAdd = true;
            $scope.UpdHide = true;
            $scope.EnableUpdate = false;
            $scope.IsSSO = false;
            $scope.FirstName = "";
            $scope.LastName = "";
            $scope.UserName = "";
            $scope.Password = "";
            $scope.Email = "";
            $scope.Language = "";
            $scope.TimeZone = "";
            $scope.StartPage = "";
            $scope.AssetAccess = "";
            $scope.ddlGlobalRole = '';
            $scope.ddlApprovalRole = '';
            $scope.Imageurl = "";
            $scope.confirmpassword = "";
            $scope.IsSSOUser = false;
            $timeout(function () {
                $('#FirstName').focus();
            }, 1000);
            $timeout(function () {
                GetValidationList();
                $("#UserDetailsForm123").addClass('notvalidate');
            }, 1000);
        };
        $scope.slno = '<span class="slno">{{row.rowIndex+1}}</span>';
        $scope.filterOptions = {
            filterText: ''
        };
        $scope.gridOptions = {
            data: 'parentUserList',
            enablePinning: false,
            filterOptions: $scope.filterOptions,
            columnDefs: [{
                field: "SlNo",
                displayName: $translate.instant('LanguageContents.Res_4753.Caption'),
                cellTemplate: $scope.slno,
                width: 60
            }, {
                field: "Id",
                displayName: $translate.instant('LanguageContents.Res_69.Caption'),
                width: 60,
                visible: false
            }, {
                field: "FirstName",
                displayName: $translate.instant('LanguageContents.Res_52.Caption')
            }, {
                field: "LastName",
                displayName: $translate.instant('LanguageContents.Res_53.Caption')
            }, {
                field: "UserName",
                displayName: $translate.instant('LanguageContents.Res_32.Caption'),
                visible: false
            }, {
                field: "Email",
                displayName: $translate.instant('LanguageContents.Res_55.Caption')
            }, {
                field: "Language",
                displayName: $translate.instant('LanguageContents.Res_56.Caption'),
                visible: false
            }, {
                field: "StartPage",
                displayName: $translate.instant('LanguageContents.Res_556.Caption'),
                visible: false
            }, {
                field: "DashboardTemplateID",
                displayName: $translate.instant('LanguageContents.Res_4988.Caption'),
                visible: false
            }, {
                field: "Image",
                displayName: $translate.instant('LanguageContents.Res_4989.Caption'),
                visible: false
            }, {
                field: "TimeZone",
                displayName: $translate.instant('LanguageContents.Res_4990.Caption')
            }, {
                field: "IsSSOUser",
                displayName: $translate.instant('LanguageContents.Res_4991.Caption'),
                visible: false
            }, {
                field: "IsAPIUser",
                displayName: $translate.instant('LanguageContents.Res_4077.Caption')
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.editableInPopup,
                width: 30
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.delete,
                width: 40
            }]
        };
        $scope.saveUser = function () {
            $("#btnTempUser123").click();
            $("#UserDetailsForm123").removeClass('notvalidate');
            if ($("#UserDetailsForm123 .error").length > 0) {
                return false;
            }
            if ($('#Email').val() == '') {
                bootbox.alert($translate.instant('LanguageContents.Res_1148.Caption'));
                return false;
            }
            var myPSPlugin = $("[id$='userPassword']").password_strength();
            if (myPSPlugin.metReq()) {
                if ($('#userPassword').val() != $('#confirmpassword').val()) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1895.Caption'));
                    return false;
                }
                $scope.AttributeData = [];
                var addUser = {};
                addUser.ID = 0;
                addUser.FirstName = $scope.FirstName;
                addUser.LastName = $scope.LastName;
                addUser.UserName = $scope.UserName;
                addUser.Password = $scope.Password;
                addUser.Email = $scope.Email;
                addUser.Language = $scope.Language;
                addUser.StartPage = parseInt($scope.StartPage);
                addUser.DashboardTemplateID = 0;
                addUser.TimeZone = $('#TimeZone>option:selected').val();
                $scope.Imageurl = "1.png";
                addUser.AssetAccessID = 1;
                addUser.Imageurl = $scope.Imageurl;
                addUser.IsApiUser = $scope.inlineCheckboxIsApiUser;
                if ($scope.atributesRelationList != null) {
                    for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                        if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                            for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                    if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id != undefined) {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                            "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                        });
                                    }
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                            for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                    if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                        for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                            $scope.AttributeData.push({
                                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                                "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level
                                            });
                                        }
                                    } else {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                            "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                        });
                                    }
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                            if ($scope.atributesRelationList[i].IsSpecial == true) {
                                if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                        "Level": 0
                                    });
                                }
                            } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                                if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                    if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                        var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined ? $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] : 0;
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": parseInt(value, 10),
                                            "Level": 0
                                        });
                                    }
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                            else {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID] != null ? $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                                    "Level": 0
                                });
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID] != null ? $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                                "Level": 0
                            });
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                            var MyDate = new Date.create();
                            var MyDateString = "";
                            if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                                MyDateString = $scope.fields["DateTime_" + $scope.atributesRelationList[i].AttributeID];
                            } else {
                                MyDateString = "";
                            }
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": MyDateString,
                                "Level": 0
                            });
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": $scope.fields["TextMoney_" + $scope.atributesRelationList[i].AttributeID],
                                "Level": 0
                            });
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID],
                                "Level": 0
                            });
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                            if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                    var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                                    for (var k = 0; k < multiselectiObject.length; k++) {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": parseInt(multiselectiObject[k], 10),
                                            "Level": 0
                                        });
                                    }
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": $scope.ImageFileName,
                                "Level": 0
                            });
                        }
                    }
                }
                addUser.AttributeData = $scope.AttributeData;
                AdminService.InsertUser(addUser).then(function (getaddUser) {
                    if (getaddUser.Response == 1) {
                        NotifyError($translate.instant('LanguageContents.Res_4890.Caption'));
                    } else if (getaddUser.Response == 2) {
                        NotifyError($translate.instant('LanguageContents.Res_4549.Caption'));
                    } else if (getaddUser.Response == -100) {
                        NotifyError($translate.instant('LanguageContents.Res_4553.Caption'));
                    } else if (getaddUser.Response > 0) {
                        AdminService.GetUsers().then(function (getusers) {
                            $scope.listdata = getusers.Response;
                            $scope.parentUserList = $scope.listdata;
                        });
                        if ($scope.ddlGlobalRole.length > 0) {
                            var addGlobalroles = {};
                            addGlobalroles.GlobalRoleID = $scope.ddlGlobalRole;
                            addGlobalroles.UserID = getaddUser.Response;
                            AdminService.InsertGlobalRoleUser(addGlobalroles).then(function () { });
                        }
                        if ($scope.ddlApprovalRole.length > 0) {
                            var addApprovalroles = {};
                            addApprovalroles.ApprovalroleId = $scope.ddlApprovalRole;
                            addApprovalroles.UserID = getaddUser.Response;
                            AdminService.InsertApprovalUserRoles(addApprovalroles).then(function () { });
                        }
                        if ($scope.AssetAccess.length > 0) {
                            var addAssetAccess = {};
                            addAssetAccess.AssetAccessID = $scope.AssetAccess;
                            addAssetAccess.UserID = getaddUser.Response;
                            AdminService.InsertAssetAccess(addAssetAccess).then(function () { });
                        }
                        NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_4268.Caption'));
                    }
                });
                $('#userModal').modal('hide');
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4553.Caption'));
            }
        };
        $scope.updateUser = function () {
            $("#btnTempUser123").click();
            var PasswordValidation = $("#UserDetailsForm123 .error").attr('ng-hide', 'updhide');
            if (PasswordValidation.hasClass("error") == true) {
                PasswordValidation.removeClass("error");
            }
            $("#UserDetailsForm123").removeClass('notvalidate');
            if ($("#UserDetailsForm123 .error").length > 0) {
                return false;
            }
            var updateUser = {};
            updateUser.ID = $scope.UserID;
            updateUser.FirstName = $scope.FirstName;
            updateUser.LastName = $scope.LastName;
            if ($scope.Email == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_1148.Caption'));
                return false;
            } else {
                updateUser.Email = $scope.Email;
            }
            updateUser.Language = "";
            updateUser.StartPage = parseInt($scope.StartPage);
            updateUser.AssetAccessID = $scope.AssetAccess;
            updateUser.AssetAccess = 1;
            updateUser.UserName = $scope.UserName;
            updateUser.DashboardTemplateID = 0;
            updateUser.IsSSOUser = $scope.inlineCheckboxSSOUserid;
            updateUser.IsApiUser = $scope.inlineCheckboxIsApiUser;
            $scope.Imageurl = "1.png";
            updateUser.Imageurl = $scope.Imageurl;
            updateUser.TimeZone = $('#TimeZone>option:selected').val();
            updateUser.Gender = 0;
            updateUser.IsApiUser = $scope.inlineCheckboxIsApiUser;
            $scope.AttributeData = [];
            if ($scope.atributesRelationList != null) {
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id != undefined) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                        "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                    for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                            "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level
                                        });
                                    }
                                } else {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                        "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                        if ($scope.atributesRelationList[i].IsSpecial == true) {
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                    "Level": 0
                                });
                            }
                        } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                            if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                    var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined ? $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] : 0;
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": parseInt(value, 10),
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                        if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                        else {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID] != null ? $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                                "Level": 0
                            });
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID] != null ? $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                            "Level": 0
                        });
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != 90) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.fields["DateTime_" + $scope.atributesRelationList[i].AttributeID],
                            "Level": 0
                        });
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.fields["TextMoney_" + $scope.atributesRelationList[i].AttributeID],
                            "Level": 0
                        });
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID],
                            "Level": 0
                        });
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                                for (var k = 0; k < multiselectiObject.length; k++) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": parseInt(multiselectiObject[k], 10),
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.ImageFileName,
                            "Level": 0
                        });
                    }
                }
            }
            updateUser.AttributeData = $scope.AttributeData;
            AdminService.UpdateUser(updateUser).then(function (updateaddUser) {
                if (updateaddUser.Response == true) {
                    if ($scope.UserID == $cookies["Userid"]) {
                        $(document).trigger("OnUserNameChange", [$scope.FirstName + " " + $scope.LastName]);
                    }
                    $scope.listdata[$scope.RowIndex].FirstName = $scope.FirstName;
                    $scope.listdata[$scope.RowIndex].LastName = $scope.LastName;
                    $scope.listdata[$scope.RowIndex].Email = $scope.Email;
                    $scope.listdata[$scope.RowIndex].UserName = $scope.UserName;
                    $scope.listdata[$scope.RowIndex].Language = $scope.Language;
                    $scope.listdata[$scope.RowIndex].StartPage = $scope.StartPage;
                    $scope.listdata[$scope.RowIndex].DashboardTemplateID = 0;
                    $scope.listdata[$scope.RowIndex].Image = $scope.Imageurl;
                    $scope.listdata[$scope.RowIndex].TimeZone = $('#TimeZone>option:selected').val();
                    $scope.listdata[$scope.RowIndex].IsSSOUser = $scope.inlineCheckboxSSOUserid;
                    $scope.listdata[$scope.RowIndex].IsAPIUser = $scope.inlineCheckboxIsApiUser;
                    $scope.listdata[$scope.RowIndex].AssetAccess = 1;
                    $scope.parentUserList = $scope.listdata;
                    if ($scope.ddlGlobalRole.length == 0) {
                        var updateGlobalroles = {};
                        updateGlobalroles.GlobalRoleID = [-2];
                        updateGlobalroles.UserID = updateUser.ID;
                        AdminService.InsertGlobalRoleUser(updateGlobalroles).then(function () { });
                    }
                    if ($scope.ddlGlobalRole.length > 0) {
                        var updateGlobalroles = {};
                        updateGlobalroles.GlobalRoleID = $scope.ddlGlobalRole;
                        updateGlobalroles.UserID = updateUser.ID;
                        AdminService.InsertGlobalRoleUser(updateGlobalroles).then(function () { });
                    }
                    if ($scope.ddlApprovalRole.length > 0) {
                        var addApprovalroles = {};
                        addApprovalroles.ApprovalroleId = $scope.ddlApprovalRole;
                        addApprovalroles.UserID = updateUser.ID;
                        AdminService.InsertApprovalUserRoles(addApprovalroles).then(function () { });
                    }
                    if ($scope.AssetAccess.length > 0) {
                        var updateAssetAccess = {};
                        updateAssetAccess.AssetAccessID = $scope.AssetAccess;
                        updateAssetAccess.UserID = updateUser.ID;
                        AdminService.InsertAssetAccess(updateAssetAccess).then(function () { });
                        AdminService.GetAssetAccess().then(function (getassetaccessupdate) {
                            for (i = $scope.AssetAccess; i < $scope.AssetAccess.length; i++) {
                                $scope.AssetAccessSrc[i].Role = getassetaccessupdate.Response[i].Role;
                            }
                        });
                    } else {
                        var updateAssetAccess = {};
                        updateAssetAccess.AssetAccessID = 1;
                        updateAssetAccess.UserID = updateUser.ID;
                        AdminService.InsertAssetAccess(updateAssetAccess).then(function () { });
                    }
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                }
            });
            AdminService.GetUsers().then(function (getusers) {
                $scope.listdata = getusers.Response;
                $scope.parentUserList = $scope.listdata;
            });
            $('#userModal').modal('hide');
        };
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToHide = [];
                var hideAttributeOtherThanSelected = [];
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        } else if (attrType == 12) {
                            if (j == levelcnt) $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }
                    if (attrType == 6) {
                        if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    } else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
            } catch (e) { }
        }

        function GetValidationList() {
            var str = [
				['#FirstName', 'presence', 'Cannot be empty'],
				['#userPassword', 'presence', 'Cannot be empty'],
				['#confirmpassword', 'presence', 'Cannot be empty'],
				['#Email', 'presence', 'Cannot be empty'],
				['#TimeZone', 'presence', 'Cannot be empty'],
				['#StartPage', 'presence', 'Cannot be empty'],
				['#confirmpassword', 'same-as:#userPassword', 'Passwords do not match']
            ];
            var EntitypeId = 12;
            AdminService.GetValidationDationByEntitytype(EntitypeId).then(function (GetValidationresult) {
                if (GetValidationresult.Response != null) {
                    $scope.listValidationResult = GetValidationresult.Response;
                    if ($scope.listAttriToAttriResult != null) {
                        for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                            var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                            if (attrRelIDs != undefined) {
                                for (var j = 0; j < attrRelIDs.length; j++) {
                                    if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                        });
                                    } else {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                        });
                                    }
                                }
                            }
                        }
                    }
                    var dynamicvalid = [];
                    for (var k = 0; k < $scope.listValidationResult.length; k++) {
                        str.push($scope.listValidationResult[k]);
                    }
                    $("#UserDetailsForm123").nod(str, {
                        'delay': 200,
                        'submitBtnSelector': '#btnTempUser123',
                        'disableSubmitBtn': 'false',
                        'silentSubmit': 'true'
                    });
                    if ($scope.listValidationResult.length != 0) {
                        for (var i = 0 ; i < $scope.listValidationResult.length; i++) {
                            if ($scope.listValidationResult[i][1] == "presence") {
                                $($scope.listValidationResult[i][0]).parent().addClass('relative');
                                $("<i class=\"icon-asterisk validationmark margin-right5x\"></i>").insertAfter($scope.listValidationResult[i][0]);
                            }
                        }
                    }
                }
            });
        }
        $('#passwordPolicy').qtip({
            content: {
                text: function (event, api) {
                    $.ajax({
                        url: "api/user/GetTenantXMLFile/",
                        type: 'GET',
                        success: function (xml) {
                            var xmldata = xml.Response;
                            var _minLength = xmldata[0].MinLength,
								_duration = xmldata[0].Duration,
								_maxLength = xmldata[0].MaxLength,
								_numsLength = xmldata[0].NumsLength,
								_upperLength = xmldata[0].UpperLength,
								_specialLength = xmldata[0].SpecialLength,
								_barWidth = xmldata[0].BarWidth,
								_barColor = xmldata[0].BarColor,
								_specialChars = xmldata[0].SpecialChars,
								_useMultipleColors = xmldata[0].MultipleColors;
                            var html = '';
                            html += '<h3 class="nomargin">Password policy</h3>';
                            html += '<table border="1" cellpadding="1" cellspacing="0">';
                            html += '<tbody>';
                            html += '<tr><td>Password minimum length:</td>';
                            html += '<td align="center">' + _minLength.toString() + '</td>';
                            html += '</tr>';
                            html += '<tr>';
                            html += '<td>Password Maximum length:</td>';
                            html += '<td align="center">' + _maxLength.toString() + '</td>';
                            html += '</tr>';
                            html += '<tr>';
                            html += '<td>Required digits:</td>';
                            html += '<td align="center">' + _numsLength.toString() + '</td>';
                            html += '</tr>';
                            html += '<tr>';
                            html += '<td>Required upper-case letters:</td>';
                            html += '<td align="center">' + _upperLength.toString() + '</td>';
                            html += '</tr>';
                            html += '<tr>';
                            html += '<td>Required special characters:</td>';
                            html += '<td align="center">' + _specialLength.toString() + '</td>';
                            html += '</tr>';
                            html += '<tr>';
                            html += '<td>Allowable special characters:</td>';
                            html += '<td align="center">' + _specialChars.toString() + '</td>';
                            html += '</tr>';
                            html += '</tbody></table>';
                            api.set('content.text', html)
                        }
                    })
                    return 'Loading...';
                }
            },
            style: {
                classes: "qtip-dark qtip-shadow qtip-rounded",
                title: {
                    'display': 'none'
                }
            },
        });
    }
    app.controller("mui.admin.userCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$compile', '$translate', 'AdminService', muiadminuserCtrl]);
})(angular, app);