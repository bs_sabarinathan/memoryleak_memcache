using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// Feed object for table 'cm_addtionalsttings'.
	/// </summary>

    internal class UpdateSettings : IUpdateSettings
	{
		#region Member Variables

		protected int _id;
        protected string _currentversion;
        protected string _client;
        protected string _licence;
        protected int _versionid;
        protected int _instanceid;
        protected IList<IAvailableUpdates> _availableupdates;
        protected IList<IPreviousUpdates> _previousupdates;

		
		#endregion
		
		#region Constructors
        public UpdateSettings() { }

        public UpdateSettings(int pid, string pCurrentVersion, string pClient, string pLicence, int pVersionId, int pInstanceId, IList<IAvailableUpdates> pAvailableUpdates, IList<IPreviousUpdates> pPreviousUpdates)
		{
            this._id = pid;
            this._currentversion = pCurrentVersion;
            this._client = pClient;
            this._licence = pLicence;
            this._availableupdates = pAvailableUpdates;
            this._previousupdates = pPreviousUpdates;
            this._versionid = pVersionId;
            this._instanceid = pInstanceId;
		}
				
	
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}

        public string CurrentVersion 
        {
            get { return _currentversion; }
            set { _currentversion = value; }
        }

        public string Client
        {
            get { return _client; }
            set { _client = value; }
        }

        public string Licence
        {
            get
            {
                return _licence;
            }
            set { _licence = value; }
        }
        public IList<IAvailableUpdates> AvailableUpdates
        {
            get { return _availableupdates; }
            set { _availableupdates = value; }
        }

        public IList<IPreviousUpdates> PreviousUpdates
        {
            get { return _previousupdates; }
            set { _previousupdates = value; }
        }
        public int VersionId
        {
            get { return _versionid; }
            set { _versionid = value; }
        }
        public int InstanceId
        {
            get { return _instanceid; }
            set { _instanceid = value; }
        }
		#endregion 
		
		#region Equals And HashCode Overrides
	
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
