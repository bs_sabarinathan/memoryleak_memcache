﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.User.Interface
{
    public interface IEntityUsers 
    {
        #region Public Properties
        bool IsOwner { set; get; }
        string OwnerName { set; get; }
        string Email { set; get; }
        #endregion
    }
}
