﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    /// <summary>
    /// Entity object for table 'PM_Entity'.
    /// </summary>
    internal class BaseEntity : IBaseEntity
    {
        #region Member Variables

        protected int _id;
        protected int _parentid;
        protected int _typeid;
        protected bool _active;
        protected string _uniquekey;
        protected int _entityid;
        protected bool _islock;
        protected string _name;
        protected int _version;
        protected int _level;
        protected int _activeEntityStateId;
        protected int _entityStateId;
        protected bool _enabledisableworkflow;
        #endregion

        #region Constructors
        public BaseEntity() { }

        public BaseEntity(int pId, int pParentid, int pTypeid, bool pActive, string pUniqueKey, int pEntityid, bool pIsLock, string pName, int pVersion, int pLevel, int pActiveEntityStateID, int pEntityStateID,bool pEnableDisableWorkflow)
        {
            this._id = pId;
            this._parentid = pParentid;
            this._typeid = pTypeid;
            this._active = pActive;
            this._uniquekey = pUniqueKey;
            this._entityid = pEntityid;
            this._islock = pIsLock;
            this._name = pName;
            this._version = pVersion;
            this._level = pLevel;
            this._activeEntityStateId = pActiveEntityStateID;
            this._entityStateId = pEntityStateID;
            this._enabledisableworkflow = pEnableDisableWorkflow;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int Parentid
        {
            get { return _parentid; }
            set { _parentid = value; }

        }

        public int Typeid
        {
            get { return _typeid; }
            set { _typeid = value; }

        }

        public bool Active
        {
            get { return _active; }
            set { _active = value; }

        }

        public string UniqueKey
        {
            get { return _uniquekey; }
            set
            {
                if (value != null && value.Length > 450)
                    throw new ArgumentOutOfRangeException("UniqueKey", "UniqueKey value, cannot contain more than 450 characters");
                _uniquekey = value;
            }

        }

        public int EntityID
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public int Level
        {
            get { return _level; }
            set { _level = value; }

        }

        public bool IsLock
        {
            get { return _islock; }
            set { _islock = value; }

        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
                _name = value;
            }

        }

        public int Version
        {
            get { return _version; }
            set { _version = value; }
        }

        public int ActiveEntityStateID 
        { 
            get { return _activeEntityStateId; } 
            set { _activeEntityStateId = value; } 
        }
        public int EntityStateID 
        { 
            get { return _entityStateId; }
            set { _entityStateId = value; } 
        }

        public bool EnableDisableWorkflow
        {
            get { return _enabledisableworkflow; }
            set { _enabledisableworkflow = value; }
        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            Entity castObj = null;
            try
            {
                castObj = (Entity)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}


