﻿(function (ng, app) {
    "use strict";

    function muiplanningtooldefaultdetailsectionobjectiveCtrl($scope, $timeout, $http, $compile, $stateParams, $resource, $cookies, $translate, PlanobjectiveService, $modal, MetadataService, CommonService, PlanningService) {
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.detail.section.objectiveCtrl']"));
        });
        $scope.showhideobjectivetable = true;
        $scope.noPredefinedData = false;
        $scope.AdditionalobjOwnerName = $cookies['Username'];
        $scope.AdditionalobjOwnerID = $cookies['UserId'];
        $scope.AdditionalobjOwnerEmail = $cookies['UserEmail'];
        $scope.ObjectiveItemsData = [];

        $scope.$on('LoadObjectives', function (event, ID) {
            $('#PredefineObjectivetbody').html($compile('')($scope));
            $('#AdditionalObjectivestbody').html($compile('')($scope));
            loadObjectives();
        });

        var AdminLogoSettings = 'ObjectiveTabView';
        $scope.attributeHeader = [];
        $scope.additionalObjHeader = []
        loadPredefinedObjectiveHeader();
        loadAdditionalObjectiveHeader();

        var AdditionalObjAttrValues = {
            Name: -1000,
            Type: -1001,
            Plannedtarget: -1002,
            Targetoutcome: -1003,
            Rating: -1004,
            Fulfillmentstate: -1005,
            Unit: -1006,
            Status: -1007
        };

        function loadAdditionalObjectiveHeader() {
            AdminLogoSettings = 'AditionalObjectiveTabView';
            $('#AdditionalObjectiveHeader').html($compile('')($scope));
            $scope.additionalObjHeader = [];

            CommonService.GetObjectiveTabAdminSettings(AdminLogoSettings, 10).then(function (getattribtuesResp) {
                var getattribtues = JSON.parse(getattribtuesResp.Response);
                if (getattribtues.AditionalObjectiveTabView.Attributes != null) {
                    var attrHeader = getattribtues.AditionalObjectiveTabView.Attributes.Attribute;
                    if (attrHeader.length == undefined) {
                        $scope.additionalObjHeader.push({
                            DisplayName: attrHeader.DisplayName,
                            Field: attrHeader.Field,
                            Id: attrHeader.Id,
                            IsFilter: attrHeader.IsFilter,
                            IsOrderBy: attrHeader.IsOrderBy,
                            IsSelect: attrHeader.IsSelect,
                            IsSpecial: attrHeader.IsSpecial,
                            Level: attrHeader.Level,
                            Type: attrHeader.Type,
                            WhereCondition: attrHeader.WhereCondition
                        });
                    }
                    else {
                        for (var key = 0; key < attrHeader.length; key++) {
                            $scope.additionalObjHeader.push({
                                DisplayName: attrHeader[key].DisplayName,
                                Field: attrHeader[key].Field,
                                Id: attrHeader[key].Id,
                                IsFilter: attrHeader[key].IsFilter,
                                IsOrderBy: attrHeader[key].IsOrderBy,
                                IsSelect: attrHeader[key].IsSelect,
                                IsSpecial: attrHeader[key].IsSpecial,
                                Level: attrHeader[key].Level,
                                Type: attrHeader[key].Type,
                                WhereCondition: attrHeader[key].WhereCondition
                            });
                        }
                    }
                }
                else
                    $scope.additionalObjHeader = [];

                var PredefinedHeader = '';
                PredefinedHeader += '<tr>';
                //PredefinedHeader += '<th><span></span></th>';
                if ($scope.additionalObjHeader.length > 0) {
                    for (var i = 0; i < $scope.additionalObjHeader.length; i++) {
                        PredefinedHeader += '<th><span>' + $scope.additionalObjHeader[i].DisplayName + '</span></th>';
                    }
                }
                else {
                    PredefinedHeader += '<th><span>Name</span></th>';
                }
                //PredefinedHeader += '<th><span>PlannedTarget</span></th><th><span>TargetOutcome</span></th>';


                PredefinedHeader += '</tr>';
                $('#AdditionalObjectiveHeader').html($compile(PredefinedHeader)($scope));
                //loadObjectives();
            });
        }

        function loadPredefinedObjectiveHeader() {
            AdminLogoSettings = 'ObjectiveTabView';
            $('#PredefinedObjectiveHeader').html($compile('')($scope));
            $scope.attributeHeader = [];
            //var GetPredefineObjectivetabList = $resource('planning/LoadPredefineObjectives/:EntityID', { EntityID: parseInt($stateParams.ID, 10) }, { get: { method: 'GET' } });
            //var GetPredefineObjectivetabListObj = GetPredefineObjectivetabList.get(function () {
            CommonService.GetObjectiveTabAdminSettings(AdminLogoSettings, 10).then(function (getattribtuesResp) {
                var getattribtues = JSON.parse(getattribtuesResp.Response);
                if (getattribtues.ObjectiveTabView.Attributes != null) {
                    var attrHeader = getattribtues.ObjectiveTabView.Attributes.Attribute;
                    if (attrHeader.length == undefined) {
                        $scope.attributeHeader.push({
                            DisplayName: attrHeader.DisplayName,
                            Field: attrHeader.Field,
                            Id: attrHeader.Id,
                            IsFilter: attrHeader.IsFilter,
                            IsOrderBy: attrHeader.IsOrderBy,
                            IsSelect: attrHeader.IsSelect,
                            IsSpecial: attrHeader.IsSpecial,
                            Level: attrHeader.Level,
                            Type: attrHeader.Type,
                            WhereCondition: attrHeader.WhereCondition
                        });
                    }
                    else {
                        for (var key = 0; key < attrHeader.length; key++) {
                            $scope.attributeHeader.push({
                                DisplayName: attrHeader[key].DisplayName,
                                Field: attrHeader[key].Field,
                                Id: attrHeader[key].Id,
                                IsFilter: attrHeader[key].IsFilter,
                                IsOrderBy: attrHeader[key].IsOrderBy,
                                IsSelect: attrHeader[key].IsSelect,
                                IsSpecial: attrHeader[key].IsSpecial,
                                Level: attrHeader[key].Level,
                                Type: attrHeader[key].Type,
                                WhereCondition: attrHeader[key].WhereCondition
                            });
                        }
                    }
                }
                else
                    $scope.attributeHeader = [];

                var PredefinedHeader = '';
                PredefinedHeader += '<tr>';
                //PredefinedHeader += '<th><span></span></th>';
                for (var i = 0; i < $scope.attributeHeader.length; i++) {
                    PredefinedHeader += '<th><span>' + $scope.attributeHeader[i].DisplayName + '</span></th>';
                }

                PredefinedHeader += '</tr>';
                $('#PredefinedObjectiveHeader').html($compile(PredefinedHeader)($scope));
                loadObjectives();
            });
        }

        function loadObjectives() {
            handleTextChangeEvents();
            PlanobjectiveService.LoadPredefineObjectives(parseInt($stateParams.ID, 10)).then(function (GetPredefineObjectivetabListObj) {
                $scope.ObjectiveItemsData = GetPredefineObjectivetabListObj.Response;
                var ObjectiveData = $scope.ObjectiveItemsData;
                var idarr = [];
                for (var i = 0; i < $scope.ObjectiveItemsData.length; i++) {
                    idarr.push($scope.ObjectiveItemsData[i].ObjectiveId);
                }

                var Node = {};
                Node.StartRowNo = 0;
                Node.MaxNoofRow = 20;
                Node.FilterID = 0;
                Node.SortOrderColumn = "null";
                Node.IsDesc = false;
                Node.IncludeChildren = false;
                Node.EntityID = parseInt($stateParams.ID, 10);
                Node.Level = '0';
                Node.ExpandingEntityID = $scope.ExpandingEntityID;
                Node.IDArr = idarr;
                Node.FilterAttributes = [];
                Node.IsPredefinedObj = true;
                MetadataService.ObjectiveDetail(Node).then(function (NodeCnt) {
                    var dd = NodeCnt.Response;
                });

                $scope.ObjectivesDataCollection(ObjectiveData);
            });
        }

        $scope.ObjectivesDataCollection = function (ObjectiveData) {
            $scope.predefinedHtml = '';
            if (ObjectiveData != null) {

                for (var i = 0; i < ObjectiveData.length; i++) {
                    $scope.predefinedHtml += '<tr ObjectiveId="' + ObjectiveData[i].ObjectiveId + '">';
                    //$scope.predefinedHtml += '<td>';
                    //$scope.predefinedHtml += '<span style="background-color: #' + ObjectiveData[i].ObjTypeColorCode + '" class="eicon-s margin-right5x objDefaultEIcon">' + ObjectiveData[i].ShortDescription + '</span>';
                    //$scope.predefinedHtml += '</td>';
                    //$scope.predefinedHtml += '<td><span></span></td>';
                    if ($scope.attributeHeader.length > 0) {
                        for (var j = 0; j < $scope.attributeHeader.length; j++) {
                            if (ObjectiveData[i].ObjectiveAttrDetails != null)
                                var AttributeDetails = $.grep(ObjectiveData[i].ObjectiveAttrDetails, function (e) { return e.ID == parseInt($scope.attributeHeader[j].Id) })[0];
                            else
                                var AttributeDetails = [];
                            if (parseInt($scope.attributeHeader[j].Id) == SystemDefiendAttributes.Name) {
                                $scope.predefinedHtml += '<td class="objName">';
                                if ($scope.IsLock == false) {
                                    $scope.predefinedHtml += '<span style="background-color: #' + ObjectiveData[i].ObjTypeColorCode + '" class="eicon-s margin-right5x objDefaultEIcon">' + ObjectiveData[i].ShortDescription + '</span>';
                                    $scope.predefinedHtml += '<a id="PredefineObjectiveEdit"';
                                    $scope.predefinedHtml += 'ng-click="PredefineObjectiveEdit(' + ObjectiveData[i].ObjectiveId + ')" data-target="#predefinedObjectiveEditDiv" data-toggle="modal">';
                                    $scope.predefinedHtml += ObjectiveData[i].ObjectiveName;
                                    $scope.predefinedHtml += '</a>';
                                }
                                else {
                                    $scope.predefinedHtml += '<a>' + ObjectiveData[i].ObjectiveName + '</a>';
                                }
                                $scope.predefinedHtml += '</td>';
                            }
                            else if (parseInt($scope.attributeHeader[j].Id) == AdditionalObjAttrValues.Plannedtarget) {
                                $scope.predefinedHtml += '<td><span>' + ObjectiveData[i].PlannedTarget + '</span></td>';
                            }
                            else if (parseInt($scope.attributeHeader[j].Id) == AdditionalObjAttrValues.Targetoutcome) {
                                $scope.predefinedHtml += '<td><span>' + ObjectiveData[i].TargetOutcome + '</span></td>';
                            }
                            else if (AttributeDetails == undefined) {
                                $scope.predefinedHtml += '<td><span>' + '-' + '</span></td>';
                            }
                            else if (AttributeDetails.TypeID == 3) {
                                if (AttributeDetails.TypeID == AttributeTypes.Owner && AttributeDetails.ID == SystemDefiendAttributes.Owner) {
                                    $scope.predefinedHtml += '<td><span>' + AttributeDetails.Caption[0] + '</span></td>';
                                }
                                else if (AttributeDetails.ID == SystemDefiendAttributes.FiscalYear) {
                                    $scope.predefinedHtml += '<td><span>' + (AttributeDetails.Caption[0] == undefined ? "-" : AttributeDetails.Caption[0].length == 0 ? "-" : AttributeDetails.Caption[0]) + '</span></td>';
                                }
                                else {
                                    if (AttributeDetails.Caption[0] != undefined && AttributeDetails.Caption[0] != null && AttributeDetails.Caption[0] != "")
                                        $scope.predefinedHtml += '<td><span>' + AttributeDetails.Caption[0] + '</span></td>';
                                    else
                                        $scope.predefinedHtml += '<td><span>' + '-' + '</span></td>';
                                }
                            }
                            else if (AttributeDetails.TypeID == 16) {
                                if (AttributeDetails.Value != undefined && AttributeDetails.Value != null && AttributeDetails.Value != "" && AttributeDetails.Value != "-") {
                                    var dateExpireUTCval = "";
                                    var dateExpireval = "";
                                    dateExpireUTCval = AttributeDetails.Value.substr(0, 10);
                                    dateExpireval = new Date.create(dateExpireUTCval);
                                    var dateExpire = dateFormat((dateExpireUTCval), $scope.GetDefaultSettings.DateFormat);
                                    $scope.predefinedHtml += '<td><span>' + dateExpire + '</span></td>';
                                }
                                else
                                    $scope.predefinedHtml += '<td><span>' + '-' + '</span></td>';
                            }
                            else if (AttributeDetails.TypeID == 10) {
                                if (AttributeDetails.Value != undefined && AttributeDetails.Value != null && AttributeDetails.Value != "" && AttributeDetails.Value != "-") {
                                    var dateExpireUTCval = "";
                                    var dateExpireval = "";
                                    dateExpireUTCval = AttributeDetails.Value[0].Startdate.substr(0, 10);
                                    dateExpireval = new Date.create(dateExpireUTCval);
                                    var PeriodstartDate = dateFormat((dateExpireUTCval), $scope.GetDefaultSettings.DateFormat);
                                    dateExpireUTCval = AttributeDetails.Value[0].EndDate.substr(0, 10);
                                    dateExpireval = new Date.create(dateExpireUTCval);
                                    var PeriodEndDate = dateFormat((dateExpireUTCval), $scope.GetDefaultSettings.DateFormat);
                                    $scope.predefinedHtml += '<td><span>' + PeriodstartDate + ' ' + PeriodEndDate + '</span></td>';
                                }
                                else
                                    $scope.predefinedHtml += '<td><span>' + '-' + '</span></td>';
                            }
                            else if (AttributeDetails.TypeID == 5) {
                                if (AttributeDetails.Value != undefined && AttributeDetails.Value != null && AttributeDetails.Value != "" && AttributeDetails.Value != "-") {
                                    var datStartUTCval = "";
                                    var datstartval = "";
                                    datStartUTCval = AttributeDetails.Value.substr(0, 10);
                                    datstartval = new Date.create(datStartUTCval);
                                    var resdate = dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat);
                                    $scope.predefinedHtml += '<td><span>' + resdate + '</span></td>';
                                }
                                else
                                    $scope.predefinedHtml += '<td><span>' + '-' + '</span></td>';
                            }
                            else if (AttributeDetails.TypeID == 13) {
                                var sel = $.grep(AttributeDetails.DropDownPricing, function (e) { return e.LevelName == $scope.attributeHeader[j].DisplayName })[0];
                                if (sel.selection.length > 0) {
                                    var selectiontext = "";
                                    var valueMatches = [];
                                    var cap = $.grep(sel.LevelOptions, function (e) { return e.level == sel.level });
                                    valueMatches = jQuery.grep(sel.LevelOptions, function (relation) {
                                        return sel.selection.indexOf(relation.NodeId.toString()) != -1;
                                    });
                                    if (valueMatches.length > 0) {
                                        selectiontext = "";
                                        for (var x = 0, val; val = valueMatches[x++];) {
                                            selectiontext += val.caption;
                                            if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                            else selectiontext += "</br>";
                                        }
                                    }
                                    else
                                        selectiontext = "-";
                                    $scope.predefinedHtml += '<td><span>' + selectiontext + '</span></td>';
                                }
                                else
                                    $scope.predefinedHtml += '<td><span>' + '-' + '</span></td>';
                            }
                            else if (AttributeDetails.TypeID == 6) {
                                var CaptionObj = AttributeDetails.Caption.split(",");
                                if (AttributeDetails.Lable.length > 0) {
                                    for (var k = 0; k < AttributeDetails.Lable.length; k++) {
                                        if (AttributeDetails.Lable[0].Level == parseInt($scope.attributeHeader[j].Level)) {
                                            if (CaptionObj[k] != undefined) {
                                                $scope.predefinedHtml += '<td><span>' + CaptionObj[k] + '</span></td>';
                                            }
                                            else
                                                $scope.predefinedHtml += '<td><span>' + "-" + '</span></td>';
                                        }
                                    }
                                }
                                else
                                    $scope.predefinedHtml += '<td><span>' + "-" + '</span></td>';
                            }
                            else if (AttributeDetails.TypeID == 17) {
                                if (AttributeDetails.Caption != undefined && AttributeDetails.Caption != null && AttributeDetails.Caption != "" && AttributeDetails.Caption != "-")
                                    $scope.predefinedHtml += '<td><span>' + AttributeDetails.Caption + '</span></td>';
                                else
                                    $scope.predefinedHtml += '<td><span>' + "-" + '</span></td>';
                            }
                            else if (AttributeDetails.TypeID == 12) {
                                var CaptionObj = AttributeDetails.Caption;
                                if (CaptionObj.length > 0) {
                                    for (var k = 0; k < AttributeDetails.Lable.length; k++) {
                                        if ($scope.attributeHeader[j].Level == AttributeDetails.Lable[k].Level) {
                                            if (AttributeDetails.Lable.length == 1) {
                                                var l = k;
                                                var treeTexts = [];
                                                if (k == CaptionObj.length)
                                                    $scope.predefinedHtml += '<td><span>' + '-' + '</span></td>';
                                                else {
                                                    if (CaptionObj[l] != undefined) {
                                                        for (l; l < CaptionObj.length; l++) {
                                                            treeTexts.push(CaptionObj[l]);
                                                        }
                                                        $scope.predefinedHtml += '<td><span>' + treeTexts.join(", ") + '</span></td>';
                                                    }
                                                    else
                                                        $scope.predefinedHtml += '<td><span>' + '-' + '</span></td>';
                                                }
                                            }
                                            else {
                                                if (k == 0) {
                                                    if (CaptionObj[k] != undefined) {
                                                        $scope.predefinedHtml += '<td><span>' + CaptionObj[k] + '</span></td>';
                                                    }
                                                    else
                                                        $scope.predefinedHtml += '<td><span>' + '-' + '</span></td>';
                                                }
                                                else {
                                                    var l = k;
                                                    if (k == (AttributeDetails.Lable.length - 1)) {
                                                        var treeTexts = [];
                                                        if (l == CaptionObj.length) {
                                                            $scope.predefinedHtml += '<td><span>' + '-' + '</span></td>';
                                                            break;
                                                        }
                                                        else {
                                                            if (CaptionObj[l] != undefined) {
                                                                for (l; l < CaptionObj.length; l++) {
                                                                    treeTexts.push(CaptionObj[l]);
                                                                }
                                                                $scope.predefinedHtml += '<td><span>' + treeTexts.join(", ") + '</span></td>';
                                                                break;
                                                            }
                                                            else {
                                                                $scope.predefinedHtml += '<td><span>' + '-' + '</span></td>';
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    if (CaptionObj[k] != undefined)
                                                        $scope.predefinedHtml += '<td><span>' + CaptionObj[k] + '</span></td>';
                                                    else
                                                        $scope.predefinedHtml += '<td><span>' + '-' + '</span></td>';
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (AttributeDetails.TypeID == 2) {
                                if (AttributeDetails.Value != undefined || AttributeDetails.Value != null)
                                    $scope.predefinedHtml += '<td><span>' + (AttributeDetails.Value == "" ? "-" : AttributeDetails.Value) + '</span></td>';
                                else
                                    $scope.predefinedHtml += '<td><span>' + '-' + '</span></td>';
                            }
                            else if (AttributeDetails.TypeID == 4) {
                                if (AttributeDetails.Caption != undefined && AttributeDetails.Caption != null && AttributeDetails.Caption != "")
                                    $scope.predefinedHtml += '<td><span>' + (AttributeDetails.Caption) + '</span></td>';
                                else
                                    $scope.predefinedHtml += '<td><span>' + '-' + '</span></td>';
                            }
                            else if (AttributeDetails.Value != undefined && AttributeDetails.Value != null && AttributeDetails.Value != "")
                                $scope.predefinedHtml += '<td><span>' + (AttributeDetails.Value[0]) + '</span></td>';
                            else
                                $scope.predefinedHtml += '<td><span>' + '-' + '</span></td>';
                        }
                        if ($scope.IsLock == false) {
                            if (ObjectiveData[i].IsMandatory == 1)
                                $scope.predefinedHtml += '<td><span><i ng-click="DeleterPredefineObjective(' + ObjectiveData[i].ObjectiveId + ',' + ObjectiveData[i].IsMandatory + ')" ></i></span></td>';
                            else
                                $scope.predefinedHtml += '<td><span><i ng-click="DeleterPredefineObjective(' + ObjectiveData[i].ObjectiveId + ',' + ObjectiveData[i].IsMandatory + ')" class="icon-remove"></i></span></td>';
                        }
                        else
                            $scope.predefinedHtml += '<td><span><i ng-click="DeleterPredefineObjective(' + ObjectiveData[i].ObjectiveId + ',' + ObjectiveData[i].IsMandatory + ')" ></i></span></td>';
                        $scope.predefinedHtml += '</tr>';
                    }
                    else {
                        $scope.predefinedHtml += '<td class="objName">';
                        if ($scope.IsLock == false) {
                            $scope.predefinedHtml += '<span style="background-color: #' + ObjectiveData[i].ObjTypeColorCode + '" class="eicon-s margin-right5x objDefaultEIcon">' + ObjectiveData[i].ShortDescription + '</span>';
                            $scope.predefinedHtml += '<a id="PredefineObjectiveEdit"';
                            $scope.predefinedHtml += 'ng-click="PredefineObjectiveEdit(' + ObjectiveData[i].ObjectiveId + ')" data-target="#predefinedObjectiveEditDiv" data-toggle="modal">';
                            $scope.predefinedHtml += ObjectiveData[i].ObjectiveName;
                            $scope.predefinedHtml += '</a>';
                        }
                        else {
                            $scope.predefinedHtml += '<a>' + ObjectiveData[i].ObjectiveName + '</a>';
                        }
                        $scope.predefinedHtml += '</td>';
                        if ($scope.IsLock == false) {
                            if (ObjectiveData[i].IsMandatory == 1)
                                $scope.predefinedHtml += '<td><span><i ng-click="DeleterPredefineObjective(' + ObjectiveData[i].ObjectiveId + ',' + ObjectiveData[i].IsMandatory + ')" ></i></span></td>';
                            else
                                $scope.predefinedHtml += '<td><span><i ng-click="DeleterPredefineObjective(' + ObjectiveData[i].ObjectiveId + ',' + ObjectiveData[i].IsMandatory + ')" class="icon-remove"></i></span></td>';
                        }
                        else
                            $scope.predefinedHtml += '<td><span><i ng-click="DeleterPredefineObjective(' + ObjectiveData[i].ObjectiveId + ',' + ObjectiveData[i].IsMandatory + ')" ></i></span></td>';
                        $scope.predefinedHtml += '</tr>';
                    }
                }
                $('#PredefineObjectivetbody').html($compile('')($scope));
                $('#PredefineObjectivetbody').html($compile($scope.predefinedHtml)($scope));

                $scope.GettingAddtionalObjectives();
            }
        }


        $scope.Objective = {
            ngObjectiveNumericStatus: 0,
            ngObjectiveNumericUnits: 0,
            ngObjectivePlannedtarget: 0,
            ngObjectiveTargetoutcome: 0,
            ngObjectiveComments: '',
            ngObjectiveNumericDescriptionInstructions: '',
            ngNonNumObjectiveStatus: 0,
            ngNonNumObjectivePlannedtarget: 0,
            ngNonNumObjectiveTargetoutcome: 0,
            ngNonNumObjectiveComments: '',
            ngQulaitativeObjectiveStatus: 0,
            ngQulaitativeObjectiveComments: '',
            ngQulaitativeObjectiveFulfilment: 0,
            ngRatingObjectiveStatus: 0,
            ngRatingObjective: 0,
            ngRatingObjectiveComments: ''
        }
        $scope.ObjectiveTypeId = 0;
        $scope.ObjectiveId = 0;
        $scope.ObjectiveDescription = '';
        $scope.ObjectiveComments = '';
        $scope.ObjectiveMandatoryStatus = 0;
        $scope.UnitId = 0;
        $scope.FulfillmentState = 0;
        $scope.Rating = '';
        $scope.PlannedTarget = 0;
        $scope.TargetOutcome = 0;
        $scope.RaingObjective = 0;
        $scope.ObjectiveEntityId = 0;
        $scope.ObjectiveEnableComments = false;
        $scope.PredefineObjectiveRatingsData = [];
        $scope.PredefineObjectiveEdit = function (objectiveId) {
            $('#updatePeredefineObjective').removeAttr('disabled');
            var Objective = this.ObjectiveItemsData
            var ObjectiveDatese = $.grep(Objective, function (e) {
                return e.ObjectiveId == parseInt(objectiveId);
            });
            $scope.ObjectiveTypeId = ObjectiveDatese[0].ObjectiveBaseTypeID;
            $scope.ObjectiveEntityId = ObjectiveDatese[0].ObjectiveEntityId;
            $scope.ObjectiveEnableComments = ObjectiveDatese[0].IsEnableFeedback;
            $scope.ObjectiveId = objectiveId;
            if ($scope.ObjectiveTypeId == 1) {
                $scope.ObjectiveId = ObjectiveDatese[0].ObjectiveId;
                $scope.ObjectiveEntityId = ObjectiveDatese[0].ObjectiveEntityId
                $scope.ObjectiveDescription = ObjectiveDatese[0].ObjectiveDescription;
                $scope.ObjectiveComments = ObjectiveDatese[0].ObjectiveComments != null ? ObjectiveDatese[0].ObjectiveComments : '';
                $scope.ObjectiveMandatoryStatus = ObjectiveDatese[0].Satus;
                $scope.UnitId = ObjectiveDatese[0].UnitId;
                $scope.PlannedTarget = (ObjectiveDatese[0].PlannedTarget).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
                $scope.TargetOutcome = (ObjectiveDatese[0].TargetOutcome).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
                handleTextChangeEvents();
                $scope.nonNumericQuantivedisplay = false;
                $scope.qualitativedisplay = false;
                $scope.ratingsdisplay = false;
                $scope.numericqunatitativedisplay = true;
                if ($scope.ObjectiveMandatoryStatus == 1) {
                    $scope.Objective.ngObjectiveNumericStatus = 'Mandatory';
                }
                if ($scope.ObjectiveMandatoryStatus == 0) {
                    $scope.Objective.ngObjectiveNumericStatus = 'Active';
                }
                if ($scope.ObjectiveEnableComments == false) $scope.numericQuantitativeComments = false;
                else $scope.numericQuantitativeComments = true;
                $scope.Objective.ngObjectivePlannedtarget = $scope.PlannedTarget;
                $scope.Objective.ngObjectiveTargetoutcome = $scope.TargetOutcome;
                $scope.Objective.ngObjectiveComments = $scope.ObjectiveComments;
                $scope.Objective.ngObjectiveNumericDescriptionInstructions = $scope.ObjectiveDescription;
            }
            if ($scope.ObjectiveTypeId == 2) {
                $scope.ObjectiveId = ObjectiveDatese[0].ObjectiveId;
                $scope.ObjectiveEntityId = ObjectiveDatese[0].ObjectiveEntityId
                $scope.ObjectiveDescription = ObjectiveDatese[0].ObjectiveDescription;
                $scope.ObjectiveComments = ObjectiveDatese[0].ObjectiveComments != null ? ObjectiveDatese[0].ObjectiveComments : '';
                $scope.ObjectiveMandatoryStatus = ObjectiveDatese[0].Satus;
                $scope.UnitId = ObjectiveDatese[0].UnitId;
                $scope.PlannedTarget = (ObjectiveDatese[0].PlannedTarget).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');;
                $scope.TargetOutcome = (ObjectiveDatese[0].TargetOutcome).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');;
                $scope.numericqunatitativedisplay = false;
                $scope.nonNumericQuantivedisplay = true;
                $scope.qualitativedisplay = false;
                $scope.ratingsdisplay = false;
                handleTextChangeEvents();
                if ($scope.ObjectiveMandatoryStatus == 1) {
                    $scope.Objective.ngNonNumObjectiveStatus = 'Mandatory';
                }
                if ($scope.ObjectiveMandatoryStatus == 0) {
                    $scope.Objective.ngNonNumObjectiveStatus = 'Active';
                }
                if ($scope.ObjectiveEnableComments == false) $scope.nonNumericComments = false;
                else $scope.nonNumericComments = true;
                $scope.Objective.ngNonNumObjectivePlannedtarget = $scope.PlannedTarget;
                $scope.Objective.ngNonNumObjectiveTargetoutcome = $scope.TargetOutcome;
                $scope.Objective.ngNonNumObjectiveComments = $scope.ObjectiveComments;
                $scope.Objective.ngNonNumObjectiveNumericDescriptionInstructions = $scope.ObjectiveDescription;
            }
            if ($scope.ObjectiveTypeId == 3) {
                $scope.ObjectiveDescription = ObjectiveDatese[0].ObjectiveDescription;
                $scope.ObjectiveComments = ObjectiveDatese[0].ObjectiveComments != null ? ObjectiveDatese[0].ObjectiveComments : '';
                $scope.ObjectiveMandatoryStatus = ObjectiveDatese[0].Satus;
                $scope.FulfillmentState = ObjectiveDatese[0].FulfillmentState;
                $scope.numericqunatitativedisplay = false;
                $scope.nonNumericQuantivedisplay = false;
                $scope.ratingsdisplay = false;
                $scope.qualitativedisplay = true;
                if ($scope.ObjectiveMandatoryStatus == 1) {
                    $scope.Objective.ngQulaitativeObjectiveStatus = 'Mandatory';
                }
                if ($scope.ObjectiveMandatoryStatus == 0) {
                    $scope.Objective.ngQulaitativeObjectiveStatus = 'Active';
                }
                if ($scope.ObjectiveEnableComments == false) $scope.ngqualitativeComments = false;
                else $scope.ngqualitativeComments = true;
                $scope.Objective.ngQulaitativeObjectiveComments = $scope.ObjectiveComments;
                $scope.Objective.ngQulaitativeObjectiveNumericDescriptionInstructions = $scope.ObjectiveDescription;
                $scope.Objective.ngQulaitativeObjectiveFulfilment = $scope.FulfillmentState;
            }
            if ($scope.ObjectiveTypeId == 4) {
                $scope.ObjectiveDescription = ObjectiveDatese[0].ObjectiveDescription;
                $scope.ObjectiveComments = ObjectiveDatese[0].ObjectiveComments != null ? ObjectiveDatese[0].ObjectiveComments : '';
                $scope.ObjectiveMandatoryStatus = ObjectiveDatese[0].Satus;
                $scope.Rating = ObjectiveDatese[0].Rating;
                PlanobjectiveService.GettingPredefineObjRatings(parseInt(objectiveId, 10)).then(function (GetPredefineObjRatingsObj) {
                    $scope.PredefineObjectiveRatingsData.splice(0, $scope.PredefineObjectiveRatingsData.length);
                    $scope.PredefineObjectiveRatingsData = GetPredefineObjRatingsObj.Response;
                });
                $scope.numericqunatitativedisplay = false;
                $scope.nonNumericQuantivedisplay = false;
                $scope.qualitativedisplay = false
                $scope.ratingsdisplay = true;
                if ($scope.ObjectiveMandatoryStatus == 1) {
                    $scope.Objective.ngRatingObjectiveStatus = 'Mandatory';
                }
                if ($scope.ObjectiveMandatoryStatus == 0) {
                    $scope.Objective.ngRatingObjectiveStatus = 'Active';
                }
                if ($scope.ObjectiveEnableComments == false) $scope.ngRatingcomments = false;
                else $scope.ngRatingcomments = true;
                $scope.Objective.ngRatingObjective = $scope.Rating;
                $scope.Objective.ngRatingObjectiveComments = $scope.ObjectiveComments;
                $scope.Objective.ngRatingObjectiveNumericDescriptionInstructions = $scope.ObjectiveDescription;
            }
        }
        $scope.updatePeredefineObjective = function () {
            $('#updatePeredefineObjective').attr('disabled', 'disabled');
            var UpdatePredefineObjectiveObj = {};
            UpdatePredefineObjectiveObj.ObjectiveEntityID = $scope.ObjectiveEntityId;
            UpdatePredefineObjectiveObj.ObjectiveID = $scope.ObjectiveId;
            UpdatePredefineObjectiveObj.EntityID = parseInt($stateParams.ID, 10);
            if ($scope.ObjectiveTypeId == 1) {
                $scope.CurrrentPredefineNumericPlannedTarget = 0;
                $scope.CurrentPredefineNumericTargetoutcome = 0;
                if ($scope.Objective.ngObjectivePlannedtarget != 0) $scope.CurrrentPredefineNumericPlannedTarget = $scope.Objective.ngObjectivePlannedtarget.replace(/\s/g, '');
                else $scope.CurrrentPredefineNumericPlannedTarget = 0;
                if ($scope.Objective.ngObjectiveTargetoutcome != 0) $scope.CurrentPredefineNumericTargetoutcome = $scope.Objective.ngObjectiveTargetoutcome.replace(/\s/g, '');
                else $scope.CurrentPredefineNumericTargetoutcome = 0;
                UpdatePredefineObjectiveObj.PlannedTarget = $scope.CurrrentPredefineNumericPlannedTarget;
                UpdatePredefineObjectiveObj.TargetOutcome = $scope.CurrentPredefineNumericTargetoutcome;
                UpdatePredefineObjectiveObj.RatingObjective = 0;
                UpdatePredefineObjectiveObj.Comments = $scope.Objective.ngObjectiveComments;
                UpdatePredefineObjectiveObj.Status = $scope.ObjectiveMandatoryStatus;
                UpdatePredefineObjectiveObj.Fulfillment = 0;
            }
            if ($scope.ObjectiveTypeId == 2) {
                $scope.CurrrentPredefineNonNumericPlannedTarget = 0;
                $scope.CurrentPredefineNonNumericTargetoutcome = 0;
                if ($scope.Objective.ngNonNumObjectivePlannedtarget != 0) $scope.CurrrentPredefineNonNumericPlannedTarget = $scope.Objective.ngNonNumObjectivePlannedtarget.replace(/\s/g, '');
                else $scope.CurrrentPredefineNonNumericPlannedTarget = 0;
                if ($scope.Objective.ngNonNumObjectiveTargetoutcome != 0) $scope.CurrentPredefineNonNumericTargetoutcome = $scope.Objective.ngNonNumObjectiveTargetoutcome.replace(/\s/g, '');
                else $scope.CurrentPredefineNonNumericTargetoutcome = 0;
                UpdatePredefineObjectiveObj.PlannedTarget = $scope.CurrrentPredefineNonNumericPlannedTarget;
                UpdatePredefineObjectiveObj.TargetOutcome = $scope.CurrentPredefineNonNumericTargetoutcome;
                UpdatePredefineObjectiveObj.RatingObjective = 0;
                UpdatePredefineObjectiveObj.Comments = $scope.Objective.ngNonNumObjectiveComments;
                UpdatePredefineObjectiveObj.Status = $scope.ObjectiveMandatoryStatus;
                UpdatePredefineObjectiveObj.Fulfillment = 0;
            }
            if ($scope.ObjectiveTypeId == 3) {
                UpdatePredefineObjectiveObj.PlannedTarget = 0;
                UpdatePredefineObjectiveObj.TargetOutcome = 0;
                UpdatePredefineObjectiveObj.RatingObjective = 0;
                UpdatePredefineObjectiveObj.Comments = $scope.Objective.ngQulaitativeObjectiveComments;
                UpdatePredefineObjectiveObj.Status = $scope.ObjectiveMandatoryStatus;
                UpdatePredefineObjectiveObj.Fulfillment = $scope.Objective.ngQulaitativeObjectiveFulfilment;
            }
            if ($scope.ObjectiveTypeId == 4) {
                UpdatePredefineObjectiveObj.PlannedTarget = 0;
                UpdatePredefineObjectiveObj.TargetOutcome = 0;
                UpdatePredefineObjectiveObj.RatingObjective = $scope.Objective.ngRatingObjective;
                UpdatePredefineObjectiveObj.Comments = $scope.Objective.ngRatingObjectiveComments;
                UpdatePredefineObjectiveObj.Status = $scope.ObjectiveMandatoryStatus;
                UpdatePredefineObjectiveObj.Fulfillment = 0;
            }
            PlanobjectiveService.UpdatePredefineObjectivesforEntity(UpdatePredefineObjectiveObj).then(function (UpdatePredefineObjectiveObjResult) {
                $('#predefinedObjectiveEditDiv').modal('hide');
                if (UpdatePredefineObjectiveObjResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4366.Caption'));
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4541.Caption'));
                    loadObjectives();

                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        var entityTargetOutCome = $.grep($scope.ListViewDetails[i].data.Response.Data, function (e) { return e.Id == parseInt($stateParams.ID, 10) && e.ObjectveID == $scope.ObjectiveId });
                        if (entityTargetOutCome.length > 0) {
                            entityTargetOutCome[0].TargetOutcome = parseInt(UpdatePredefineObjectiveObj.TargetOutcome);
                            if ($scope.PredefineObjectiveRatingsData.length != 0) {
                                if (parseInt($scope.Objective.ngRatingObjective) == 0)
                                    entityTargetOutCome[0].RatingObjective = "-";
                                else
                                    entityTargetOutCome[0].RatingObjective = $.grep($scope.PredefineObjectiveRatingsData, function (e) { return e.Id == parseInt($scope.Objective.ngRatingObjective) })[0].Caption;
                            }
                            if (UpdatePredefineObjectiveObj.Fulfillment != undefined || UpdatePredefineObjectiveObj.Fulfillment != null || UpdatePredefineObjectiveObj.Fulfillment != "")
                                entityTargetOutCome[0].Fulfilment = parseInt(UpdatePredefineObjectiveObj.Fulfillment) == 2 ? "Not Fulfilled" : parseInt(UpdatePredefineObjectiveObj.Fulfillment) == 1 ? "Fulfilled" : parseInt(UpdatePredefineObjectiveObj.Fulfillment);
                        }
                        var targetCount = $.grep($scope.ListViewDetails[i].data.Response.Data, function (e) { return e.Id == $scope.ObjectiveId });
                        if (targetCount.length > 0) {
                            if (!($.grep($scope.ListViewDetails[i].data.Response.Data, function (e) { return e.TargetOutcome > 0 && e.Id != $scope.ObjectiveId }).length == parseInt(targetCount[0].TargetOutcome))) {
                                if (parseInt(UpdatePredefineObjectiveObj.TargetOutcome) > 0)
                                    targetCount[0].TargetOutcome = targetCount[0].TargetOutcome + 1;
                                else if (parseInt(UpdatePredefineObjectiveObj.TargetOutcome) <= 0 && targetCount[0].TargetOutcome != 0)
                                    targetCount[0].TargetOutcome = targetCount[0].TargetOutcome - 1;
                            }
                            if (!($.grep($scope.ListViewDetails[i].data.Response.Data, function (e) { return (e.RatingObjective != "" && e.RatingObjective != "-") && e.Id != $scope.ObjectiveId }).length == parseInt(targetCount[0].RatingObjective))) {
                                if (parseInt(UpdatePredefineObjectiveObj.RatingObjective) > 0)
                                    targetCount[0].RatingObjective = parseInt(targetCount[0].RatingObjective) + 1;
                                else if (parseInt(UpdatePredefineObjectiveObj.RatingObjective) <= 0 && targetCount[0].RatingObjective != 0)
                                    targetCount[0].RatingObjective = parseInt(targetCount[0].RatingObjective) - 1;
                            }
                            if (!($.grep($scope.ListViewDetails[i].data.Response.Data, function (e) { return (e.Fulfilment != "Not Fulfilled" && e.Fulfilment != "-" && e.Fulfilment != "" && e.Fulfilment != null) && e.Id != $scope.ObjectiveId }).length == parseInt(targetCount[0].Fulfilment))) {
                                if (parseInt(UpdatePredefineObjectiveObj.Fulfillment) == 1)
                                    targetCount[0].Fulfilment = parseInt(targetCount[0].Fulfilment) + 1;
                                else if ((parseInt(UpdatePredefineObjectiveObj.Fulfillment) == 2 || parseInt(UpdatePredefineObjectiveObj.Fulfillment) == 0) && targetCount[0].Fulfilment != 0)
                                    targetCount[0].Fulfilment = parseInt(targetCount[0].Fulfilment) - 1;
                            }
                        }
                    }
                }
            });
        }
        $scope.ObjectiveEntityList = {};
        $scope.GetObjectiveEntitytoSelectObj = {};
        $scope.LoadObjectiveEntityToSelect = function () {
            $timeout(function () {
                $("#predefinedObjectivesList").modal('show');
            }, 100);
            PlanobjectiveService.GettingEntityPredefineObjectives(parseInt($stateParams.ID, 10)).then(function (GetObjectiveEntitytoSelectObj) {
                if (GetObjectiveEntitytoSelectObj.Response != null && GetObjectiveEntitytoSelectObj.Response.length > 0) {
                    $scope.ObjectiveEntitiesAddbtn = true;
                    $scope.ObjectiveEntityList = GetObjectiveEntitytoSelectObj.Response;
                    $scope.showhideobjectivetable = true;
                    $scope.noPredefinedData = false;
                } else {
                    $scope.ObjectiveEntitiesAddbtn = false;
                    $scope.showhideobjectivetable = false;
                    $scope.noPredefinedData = true;
                }
            });
        }
        $(document).on('click', '.checkbox-custom > input[ng-model=AllEntityObjChecked]', function (e) {
            var status = this.checked;
            $('#EntityPredefineObjectivestable > tbody input:checkbox').each(function () {
                this.checked = status;
                if (status) {
                    $(this).next('i').addClass('checked');
                } else {
                    $(this).next('i').removeClass('checked');
                }
            });
        });
        $scope.AddEntityObjectives = function () {
            var SaveEntityPredefinedObjectivesObj = {};
            var IDSList = GetSelectedEntityPredefinedObjecitves();
            if (IDSList.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4612.Caption'));
                return false;
            }
            SaveEntityPredefinedObjectivesObj.ObjectiveIDList = IDSList;
            SaveEntityPredefinedObjectivesObj.EntityID = parseInt($stateParams.ID, 10);
            PlanobjectiveService.InsertPredefineObjectivesforEntity(SaveEntityPredefinedObjectivesObj).then(function (EntityPreObjList) {
                var EntityPreObjListResult = EntityPreObjList.Response;
                $('#predefinedObjectivesList').modal('hide');
                if (EntityPreObjListResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4270.Caption'));
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4536.Caption'));
                    loadObjectives();
                }
            });
        }

        function GetSelectedEntityPredefinedObjecitves() {
            var IDList = new Array();
            $('#EntityPredefineObjectivestable > tbody input:checked').each(function () {
                IDList.push($(this).parents('tr > td').attr('data-id'));
            });
            return IDList
        }
        $scope.ClearingAdditionalObjectivecontrols = function () {
            $("#additionalObjectiveEntityControls").modal('show');
            $("#additionalObjectiveEntity").trigger("onAdditionalObjecitveCreation")
            $timeout(function () {
                $('#AdditionalObjectiveName').focus();
            }, 1000);
        };
        $scope.AdditionalObjective = {
            ngObjectiveName: '',
            ngObjectiveStatus: 1,
            ngObjectiveDescription: '',
            ngObjectiveType: 2,
            ngObjectivenonNumericUnits: 0,
            ngObjectivePlannedTargetCheck: false,
            ngObjectivePlannedTarget: 0,
            ngObjectiveTargetoutcomeCheck: false,
            ngObjectiveTargetoutcome: 0,
            ngObjectivenonNumericEnableCommentsCheck: false,
            ngObjectivenonNumericDescriptionInstructions: '',
            ngObjectiveQulaitativeDescriptionInstructions: '',
            ngObjectiveQulaitativeEnableCommentsCheck: false,
            ngObjectiveRatingDescriptionInstructions: '',
            ngObjectiveRatingEnableCommentsCheck: false,
            ngAdditionalNonNumObjectiveStatus: 0,
            ngAdditionalNonNumObjectivePlannedtarget: 0,
            ngAdditionalNonNumObjectiveTargetoutcome: 0,
            ngAdditionalNonNumObjectiveComments: '',
            ngAdditionalNonNumObjectiveInstructions: '',
            ngAdditionalQulaitativeObjectiveStatus: 0,
            ngAdditionalQulaitativeObjectiveFulfilment: 0,
            ngAdditionalQulaitativeObjectiveComments: '',
            ngAdditionalQulaitativeObjectiveInstructions: '',
            ngAdditionalRatingObjectiveStatus: 0,
            ngAdditionalRatingObjective: 0,
            ngAdditionalRatingObjectiveComments: '',
            ngAdditionalRatingObjectiveInstructions: ''
        }
        $scope.ObjectiveUnitsData = [];
        PlanobjectiveService.GettingObjectiveUnits().then(function (ObjectiveUnitsResult) {
            $scope.ObjectiveUnitsData = ObjectiveUnitsResult.Response;
        });
        $scope.MemberLists = [];
        $scope.RatingsHtml = '';
        $scope.ratingItems = [];
        $scope.ratingItems.push({
            ObjRatingText: ''
        });
        $scope.ratingRowsCount = 0;
        $scope.AddRatings = function () {
            $scope.ratingRowsCount = $scope.ratingRowsCount + 1;
            $scope.ratingItems.push({
                ObjRatingText: ''
            });
        };
        $scope.RemoveRatings = function (item) {
            if ($scope.ratingRowsCount < 1) {
                bootbox.alert($translate.instant('LanguageContents.Res_1953.Caption'));
                return false;
            } else {
                $scope.ratingRowsCount = $scope.ratingRowsCount - 1;
                $scope.ratingItems.splice($.inArray(item, $scope.items), 1);
            }
        };
        $scope.AdditionalObjectives = {};
        $scope.IsEnableFeedback = false;
        $scope.GettingAddtionalObjectives = function () {
            //var additionalObjectiveObj = $resource('planning/GettingAddtionalObjectives/:EntityID', { EntityID: parseInt($stateParams.ID, 10) }, { get: { method: 'GET' } });
            //var additionalObjectiveData = additionalObjectiveObj.get(function () {
            PlanningService.GettingAddtionalObjectives(parseInt($stateParams.ID, 10)).then(function (additionalObjectiveData) {
                $scope.AdditionalObjectives = additionalObjectiveData.Response;

                var AdditionalObjectiveData = $scope.AdditionalObjectives;
                $scope.additionalObjHtml = '';
                if (AdditionalObjectiveData.length > 0) {
                    for (var i = 0 ; i < AdditionalObjectiveData.length ; i++) {
                        $scope.additionalObjHtml += '<tr Description="' + AdditionalObjectiveData[i].ObjectiveDescription + '" Comments="' + AdditionalObjectiveData[i].ObjectiveComments + '"';
                        $scope.additionalObjHtml += 'mandatory="' + AdditionalObjectiveData[i].Satus + '" UnitId="' + AdditionalObjectiveData[i].UnitId + '" ObjectiveEntityID="' + AdditionalObjectiveData[i].ObjectiveEntityId + '"';
                        $scope.additionalObjHtml += 'id="' + AdditionalObjectiveData[i].ObjectiveTypeId + '" TargetOutcome="' + AdditionalObjectiveData[i].TargetOutcome + '"';
                        $scope.additionalObjHtml += 'objectiveId="' + AdditionalObjectiveData[i].ObjectiveId + '" PlannedTarget="' + AdditionalObjectiveData[i].PlannedTarget + '">';
                        //$scope.additionalObjHtml += '<td>';
                        //$scope.additionalObjHtml += '<span class="eicon objDefaultEIcon">obj</span>';
                        //$scope.additionalObjHtml += '</td>';
                        if ($scope.additionalObjHeader.length > 0) {

                            var check_headername = $.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Name });
                            if (check_headername.length > 0) {
                                $scope.additionalObjHtml += '<td class="objName"><span class="eicon-s margin-right5x objDefaultEIcon">obj</span>';
                                if ($scope.IsLock == false) {
                                    $scope.additionalObjHtml += '<a id="AdditionalObjectiveEdit_' + AdditionalObjectiveData[i].ObjectiveId + '" data-id="' + AdditionalObjectiveData[i].ObjectiveId + '"';
                                    $scope.additionalObjHtml += 'ng-click="AdditionalObjectiveEdit(' + AdditionalObjectiveData[i].ObjectiveId + ')"';
                                    $scope.additionalObjHtml += 'data-target="#AdditionalObjectiveEditDiv" data-toggle="modal">';
                                    $scope.additionalObjHtml += AdditionalObjectiveData[i].ObjectiveName;
                                    $scope.additionalObjHtml += '</a>';
                                }
                                else {
                                    $scope.additionalObjHtml += AdditionalObjectiveData[i].ObjectiveName;
                                }
                            }
                        }
                        else {
                            $scope.additionalObjHtml += '<td class="objName"><span class="eicon-s margin-right5x objDefaultEIcon">obj</span>';
                            if ($scope.IsLock == false) {
                                $scope.additionalObjHtml += '<a id="AdditionalObjectiveEdit_' + AdditionalObjectiveData[i].ObjectiveId + '" data-id="' + AdditionalObjectiveData[i].ObjectiveId + '"';
                                $scope.additionalObjHtml += 'ng-click="AdditionalObjectiveEdit(' + AdditionalObjectiveData[i].ObjectiveId + ')"';
                                $scope.additionalObjHtml += 'data-target="#AdditionalObjectiveEditDiv" data-toggle="modal">';
                                $scope.additionalObjHtml += AdditionalObjectiveData[i].ObjectiveName;
                                $scope.additionalObjHtml += '</a>';
                            }
                            else {
                                $scope.additionalObjHtml += AdditionalObjectiveData[i].ObjectiveName;
                            }
                            $scope.additionalObjHtml += '</td>';
                        }
                        if (AdditionalObjectiveData[i].ObjectiveTypeId == 2) {
                            for (var j = 0; j < $scope.additionalObjHeader.length; j++) {
                                switch (parseInt($scope.additionalObjHeader[j].Id)) {
                                    case AdditionalObjAttrValues.Type:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Type })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td><span>';
                                            if (AdditionalObjectiveData[i].ObjectiveTypeId == 2) {
                                                $scope.additionalObjHtml += 'Numeric(NQ)';
                                            }
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Plannedtarget:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Plannedtarget })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td PlannedTarget="' + AdditionalObjectiveData[i].PlannedTarget + '"><span>';
                                            if (AdditionalObjectiveData[i].PlannedTarget == 0) {
                                                $scope.additionalObjHtml += 'No Response yet';
                                            }
                                            if (AdditionalObjectiveData[i].PlannedTarget != 0) {
                                                $scope.additionalObjHtml += (AdditionalObjectiveData[i].PlannedTarget).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
                                            }
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Targetoutcome:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Targetoutcome })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td TargetOutcome="' + AdditionalObjectiveData[i].TargetOutcome + '"><span>';
                                            if (AdditionalObjectiveData[i].TargetOutcome == 0) {
                                                $scope.additionalObjHtml += 'No Response yet';
                                            }
                                            if (AdditionalObjectiveData[i].TargetOutcome != 0) {
                                                $scope.additionalObjHtml += (AdditionalObjectiveData[i].TargetOutcome).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
                                            }
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Rating:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Rating })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td><span>';
                                            $scope.additionalObjHtml += '-';
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Fulfillmentstate:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Fulfillmentstate })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td><span>';
                                            $scope.additionalObjHtml += '-';
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Unit:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Unit })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td UnitId="' + AdditionalObjectiveData[i].UnitId + '"><span>';
                                            if (AdditionalObjectiveData[i].UnitId != null && AdditionalObjectiveData[i].UnitId != "" && AdditionalObjectiveData[i].UnitId != undefined) {
                                                $scope.additionalObjHtml += AdditionalObjectiveData[i].UnitName;
                                            }
                                            if (AdditionalObjectiveData[i].UnitId == null || AdditionalObjectiveData[i].UnitId == "" || AdditionalObjectiveData[i].UnitId == undefined) {
                                                $scope.additionalObjHtml += '-';
                                            }
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Status:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Status })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td mandatory="' + AdditionalObjectiveData[i].Satus + '"><span>';
                                            if (AdditionalObjectiveData[i].Satus == 1)
                                                $scope.additionalObjHtml += 'Active';
                                            else {
                                                $scope.additionalObjHtml += 'Deactiveted';
                                            }
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }


                        }
                        if (AdditionalObjectiveData[i].ObjectiveTypeId == 3) {
                            for (var j = 0; j < $scope.additionalObjHeader.length; j++) {
                                switch (parseInt($scope.additionalObjHeader[j].Id)) {
                                    case AdditionalObjAttrValues.Type:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Type })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td><span>';
                                            $scope.additionalObjHtml += 'Qualitative';
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Plannedtarget:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Plannedtarget })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td><span>';
                                            $scope.additionalObjHtml += '-';
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Targetoutcome:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Targetoutcome })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td><span>';
                                            $scope.additionalObjHtml += '-';
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Rating:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Rating })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td><span>';
                                            $scope.additionalObjHtml += '-';
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Fulfillmentstate:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Fulfillmentstate })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td><span>';
                                            if (AdditionalObjectiveData[i].FulfillmentState == 0) {
                                                $scope.additionalObjHtml += 'No Response Yet';
                                            }
                                            if (AdditionalObjectiveData[i].FulfillmentState == 1) {
                                                $scope.additionalObjHtml += 'Fulfilled';
                                            }
                                            if (AdditionalObjectiveData[i].FulfillmentState == 2) {
                                                $scope.additionalObjHtml += 'Not Fulfilled';
                                            }
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Unit:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Unit })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td><span>';
                                            $scope.additionalObjHtml += '-';
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Status:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Status })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td mandatory="' + AdditionalObjectiveData[i].Satus + '"><span>';
                                            if (AdditionalObjectiveData[i].Satus == 1)
                                                $scope.additionalObjHtml += 'Active';
                                            else {
                                                $scope.additionalObjHtml += 'Deactiveted';
                                            }
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        if (AdditionalObjectiveData[i].ObjectiveTypeId == 4) {
                            for (var j = 0; j < $scope.additionalObjHeader.length; j++) {
                                switch (parseInt($scope.additionalObjHeader[j].Id)) {
                                    case AdditionalObjAttrValues.Type:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Type })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td><span>';
                                            $scope.additionalObjHtml += 'Rating';
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Plannedtarget:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Plannedtarget })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td><span>';
                                            $scope.additionalObjHtml += '-';
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Targetoutcome:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Targetoutcome })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td><span>';
                                            $scope.additionalObjHtml += '-';
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Rating:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Rating })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td><span>';
                                            if (AdditionalObjectiveData[i].Rating == null || AdditionalObjectiveData[i].Rating == 0) {
                                                $scope.additionalObjHtml += 'No Response yet';
                                            }
                                            if (AdditionalObjectiveData[i].Rating != null && AdditionalObjectiveData[i].Rating != 0) {
                                                $scope.additionalObjHtml += AdditionalObjectiveData[i].RatingCaption;
                                            }
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Fulfillmentstate:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Fulfillmentstate })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td><span>';
                                            $scope.additionalObjHtml += '-';
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Unit:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Unit })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td><span>';
                                            $scope.additionalObjHtml += '-';
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    case AdditionalObjAttrValues.Status:
                                        if (($.grep($scope.additionalObjHeader, function (e) { return e.Id == AdditionalObjAttrValues.Status })[0]) != undefined) {
                                            $scope.additionalObjHtml += '<td><span>';
                                            if (AdditionalObjectiveData[i].Satus == 1)
                                                $scope.additionalObjHtml += 'Active';
                                            else {
                                                $scope.additionalObjHtml += 'Deactiveted';
                                            }
                                            $scope.additionalObjHtml += '</span></td>';
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        $scope.additionalObjHtml += '<td><span>';
                        $scope.additionalObjHtml += '<td><span>';
                        if ($scope.IsLock == false)
                            $scope.additionalObjHtml += '<i ng-click="DeleteAdditionalObjective(' + AdditionalObjectiveData[i].ObjectiveId + ')" class="icon-remove"></i>';
                        $scope.additionalObjHtml += '</span></td>';
                        $scope.additionalObjHtml += '</tr>';
                        $('#AdditionalObjectivestbody').html($compile($scope.additionalObjHtml)($scope));
                    }
                }
                else {
                    $('#AdditionalObjectivestbody').html($compile($scope.additionalObjHtml)($scope));
                }
            });
        }
        $scope.additionalObjectiveCheck = false;
        $scope.additionalObjectivechecked = function (row, Index) {
            $('#AdditionalObjectivestbody > tr').click(function (event) {
                $scope.ObjectiveTypeId = $(this).attr('id');
                $scope.ObjectiveId = $(this).attr('objectiveId');
                $scope.ObjectiveEntityId = $(this).attr('ObjectiveEntityID');
                if ($scope.ObjectiveTypeId == 2) {
                    $scope.ObjectiveDescription = $(this).attr('Description');
                    $scope.ObjectiveComments = $(this).attr('Comments');
                    $scope.ObjectiveMandatoryStatus = $(this).attr('mandatory');
                    $scope.UnitId = $(this).attr('UnitId');
                    $scope.PlannedTarget = $(this).attr('PlannedTarget');
                    $scope.TargetOutcome = $(this).attr('TargetOutcome');
                    $scope.FulfillmentState = $(this).attr('Fulfillment');
                }
                if ($scope.ObjectiveTypeId == 3) {
                    $scope.ObjectiveDescription = $(this).attr('Description');
                    $scope.ObjectiveComments = $(this).attr('Comments');
                    $scope.ObjectiveMandatoryStatus = $(this).attr('mandatory');
                    $scope.FulfillmentState = $(this).attr('Fulfillment');
                }
                if ($scope.ObjectiveTypeId == 4) {
                    $scope.ObjectiveDescription = $(this).attr('Description');
                    $scope.ObjectiveComments = $(this).attr('Comments');
                    $scope.ObjectiveMandatoryStatus = $(this).attr('mandatory');
                    $scope.Rating = $(this).attr('Rating');
                }
            });
        }
        $scope.ObjectiveRatingsData = [];
        $scope.AdditionalObjectiveEdit = function (objectiveId) {
            $('#updateAdditionalObjective').removeAttr('disabled');
            var additionalObjectiveData = this.AdditionalObjectives;
            var additionalObjectiveDataResult = $.grep(additionalObjectiveData, function (e) {
                return e.ObjectiveId == parseInt(objectiveId);
            });
            $scope.ObjectiveTypeId = additionalObjectiveDataResult[0].ObjectiveTypeId;
            $scope.ObjectiveId = objectiveId;
            $scope.ObjectiveEntityId = additionalObjectiveDataResult[0].ObjectiveEntityId;
            if ($scope.ObjectiveTypeId == 2) {
                $timeout(function () {
                    $('#ngnonNumericAdditionalObjectiveName').focus().select()
                }, 1000);
                $scope.ObjectiveName = additionalObjectiveDataResult[0].ObjectiveName;
                $scope.ObjectiveId = additionalObjectiveDataResult[0].ObjectiveId;
                $scope.ObjectiveEntityId = additionalObjectiveDataResult[0].ObjectiveEntityId
                $scope.ObjectiveDescription = additionalObjectiveDataResult[0].ObjectiveDescription;
                if (additionalObjectiveDataResult[0].IsEnableFeedback == false) {
                    $scope.IsAdditionalNonNumericQualitiveEnableFeedback = true;
                } else {
                    $scope.IsAdditionalNonNumericQualitiveEnableFeedback = false;
                }
                $scope.ObjectiveComments = additionalObjectiveDataResult[0].ObjectiveComments;
                $scope.ObjectiveMandatoryStatus = additionalObjectiveDataResult[0].Satus;
                $scope.UnitId = additionalObjectiveDataResult[0].UnitId;
                $scope.PlannedTarget = additionalObjectiveDataResult[0].PlannedTarget;
                $scope.TargetOutcome = additionalObjectiveDataResult[0].TargetOutcome;
                $scope.AdditionalObjective.ngAdditionalNonNumObjectiveStatus = additionalObjectiveDataResult[0].Satus;
                $scope.Additionalqualitativedisplay = false;
                $scope.Additionalratingsdisplay = false;
                $scope.AdditionalnonNumericQuantivedisplay = true;
                if ($scope.ObjectiveMandatoryStatus == 1) $scope.AdditionalObjective.ngObjectivenonNumericUnits = $scope.UnitId;
                $scope.AdditionalObjective.ngAdditionalNonNumObjectivePlannedtarget = ($scope.PlannedTarget).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
                $scope.AdditionalObjective.ngAdditionalNonNumObjectiveTargetoutcome = ($scope.TargetOutcome).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
                $scope.AdditionalObjective.ngAdditionalNonNumObjectiveComments = $scope.ObjectiveComments;
                $scope.AdditionalObjective.ngAdditionalNonNumObjectiveInstructions = $scope.ObjectiveDescription;
                $scope.AdditionalObjective.ngnonNumericAdditionalObjectiveName = $scope.ObjectiveName;
            }
            if ($scope.ObjectiveTypeId == 3) {
                $timeout(function () {
                    $('#ngQualitiveAdditionalObjectiveName').focus().select()
                }, 1000);
                $scope.ObjectiveName = additionalObjectiveDataResult[0].ObjectiveName;
                $scope.ObjectiveDescription = additionalObjectiveDataResult[0].ObjectiveDescription;
                if (additionalObjectiveDataResult[0].IsEnableFeedback == false) {
                    $scope.IsAdditionalQualitiveEnableFeedback = true;
                } else {
                    $scope.IsAdditionalQualitiveEnableFeedback = false;
                }
                $scope.ObjectiveComments = additionalObjectiveDataResult[0].ObjectiveComments;
                $scope.ObjectiveMandatoryStatus = additionalObjectiveDataResult[0].Satus;
                $scope.FulfillmentState = additionalObjectiveDataResult[0].FulfillmentState;
                $scope.Fulfillment = additionalObjectiveDataResult[0].FulfillmentState;
                $scope.Additionalratingsdisplay = false;
                $scope.AdditionalnonNumericQuantivedisplay = false;
                $scope.Additionalqualitativedisplay = true;
                if ($scope.ObjectiveMandatoryStatus == 1) $scope.AdditionalObjective.ngAdditionalQulaitativeObjectiveStatus = additionalObjectiveDataResult[0].Satus;
                $scope.AdditionalObjective.ngAdditionalQulaitativeObjectiveFulfilment = $scope.FulfillmentState;
                $scope.AdditionalObjective.ngAdditionalQulaitativeObjectiveComments = $scope.ObjectiveComments;
                $scope.AdditionalObjective.ngAdditionalQulaitativeObjectiveInstructions = $scope.ObjectiveDescription;
                $scope.AdditionalObjective.ngQualitiveAdditionalObjectiveName = $scope.ObjectiveName;
            }
            if ($scope.ObjectiveTypeId == 4) {
                $timeout(function () {
                    $('#ngRatingAdditionalObjectiveName').focus().select()
                }, 1000);
                $scope.ObjectiveName = additionalObjectiveDataResult[0].ObjectiveName;
                $scope.ObjectiveDescription = additionalObjectiveDataResult[0].ObjectiveDescription;
                if (additionalObjectiveDataResult[0].IsEnableFeedback == false) {
                    $scope.IsAdditionalRatingEnableFeedback = true;
                } else {
                    $scope.IsAdditionalRatingEnableFeedback = false;
                }
                $scope.ObjectiveComments = additionalObjectiveDataResult[0].ObjectiveComments;
                $scope.ObjectiveMandatoryStatus = additionalObjectiveDataResult[0].Satus;
                $scope.Rating = additionalObjectiveDataResult[0].Rating;
                PlanobjectiveService.GettingAdditionalObjRatings(objectiveId).then(function (GetAdditionalObjRatingsObj) {
                    $scope.ObjectiveRatingsData.splice(0, $scope.ObjectiveRatingsData.length);
                    $scope.ObjectiveRatingsData = GetAdditionalObjRatingsObj.Response;
                });
                $scope.AdditionalnonNumericQuantivedisplay = false;
                $scope.Additionalqualitativedisplay = false;
                $scope.Additionalratingsdisplay = true;
                if ($scope.ObjectiveMandatoryStatus == 1) $scope.AdditionalObjective.ngAdditionalRatingObjectiveStatus = additionalObjectiveDataResult[0].Satus;
                $scope.AdditionalObjective.ngAdditionalRatingObjective = $scope.Rating;
                $scope.AdditionalObjective.ngAdditionalRatingObjectiveComments = $scope.ObjectiveComments;
                $scope.AdditionalObjective.ngAdditionalRatingObjectiveInstructions = $scope.ObjectiveDescription;
                $scope.AdditionalObjective.ngRatingAdditionalObjectiveName = $scope.ObjectiveName;
            }
        }
        $scope.AdditionalObjeviveeditcontrolclick = function () {
            $timeout(function () {
                $('#AddtionalObjectiHeadervename').focus().select()
            }, 10);
        }
        $scope.updateAdditionalObjective = function () {
            $('#updateAdditionalObjective').attr('disabled', 'disabled');
            var UpdateAdditionalObjectiveObj = {};
            UpdateAdditionalObjectiveObj.ObjectiveEntityID = $scope.ObjectiveEntityId;
            UpdateAdditionalObjectiveObj.EntityID = $scope.ObjectiveId;
            UpdateAdditionalObjectiveObj.ObjectiveTypeID = parseInt($scope.ObjectiveTypeId, 10);
            if ($scope.ObjectiveTypeId == 2) {
                if ($('#ngnonNumericAdditionalObjectiveName').val() == '') {
                    bootbox.alert($translate.instant('LanguageContents.Res_4586.Caption'));
                    $('#updateAdditionalObjective').removeAttr("disabled");
                    return false
                }
                $scope.CurrentngAdditionalNonNumObjectivePlannedtarget = 0;
                $scope.CurrentngAdditionalNonNumObjectiveTargetoutcome = 0;
                UpdateAdditionalObjectiveObj.Objectivename = $scope.AdditionalObjective.ngAdditionalObjectiveName;
                UpdateAdditionalObjectiveObj.Objectivename = $scope.AdditionalObjective.ngnonNumericAdditionalObjectiveName;
                UpdateAdditionalObjectiveObj.ObjectiveInstruction = $scope.ObjectiveDescription;
                UpdateAdditionalObjectiveObj.IsEnableFeedback = true;
                UpdateAdditionalObjectiveObj.UnitID = parseInt($scope.AdditionalObjective.ngObjectivenonNumericUnits, 10);
                UpdateAdditionalObjectiveObj.status = $scope.AdditionalObjective.ngAdditionalNonNumObjectiveStatus;
                if ($scope.AdditionalObjective.ngAdditionalNonNumObjectivePlannedtarget != 0) $scope.CurrentngAdditionalNonNumObjectivePlannedtarget = $scope.AdditionalObjective.ngAdditionalNonNumObjectivePlannedtarget.replace(/\s/g, '');
                else $scope.CurrentngAdditionalNonNumObjectivePlannedtarget = 0;
                if ($scope.AdditionalObjective.ngAdditionalNonNumObjectiveTargetoutcome != 0) $scope.CurrentngAdditionalNonNumObjectiveTargetoutcome = $scope.AdditionalObjective.ngAdditionalNonNumObjectiveTargetoutcome.replace(/\s/g, '');
                else $scope.CurrentngAdditionalNonNumObjectiveTargetoutcome = 0;
                UpdateAdditionalObjectiveObj.PlannedTarget = $scope.CurrentngAdditionalNonNumObjectivePlannedtarget;
                UpdateAdditionalObjectiveObj.TargetOutcome = $scope.CurrentngAdditionalNonNumObjectiveTargetoutcome;
                UpdateAdditionalObjectiveObj.RatingObjective = 0;
                UpdateAdditionalObjectiveObj.Comments = $scope.AdditionalObjective.ngAdditionalNonNumObjectiveComments;
                UpdateAdditionalObjectiveObj.Status = $scope.ObjectiveMandatoryStatus;
                UpdateAdditionalObjectiveObj.Fulfillment = 0;
            }
            if ($scope.ObjectiveTypeId == 3) {
                if ($('#ngQualitiveAdditionalObjectiveName').val() == '') {
                    bootbox.alert($translate.instant('LanguageContents.Res_4586.Caption'));
                    $('#updateAdditionalObjective').removeAttr("disabled");
                    return false
                }
                UpdateAdditionalObjectiveObj.Objectivename = $scope.AdditionalObjective.ngQualitiveAdditionalObjectiveName;
                UpdateAdditionalObjectiveObj.ObjectiveInstruction = $scope.ObjectiveDescription;
                UpdateAdditionalObjectiveObj.IsEnableFeedback = true;
                UpdateAdditionalObjectiveObj.UnitID = 0;
                UpdateAdditionalObjectiveObj.PlannedTarget = 0;
                UpdateAdditionalObjectiveObj.TargetOutcome = 0;
                UpdateAdditionalObjectiveObj.RatingObjective = 0;
                UpdateAdditionalObjectiveObj.status = $scope.AdditionalObjective.ngAdditionalQulaitativeObjectiveStatus;
                UpdateAdditionalObjectiveObj.Comments = $scope.AdditionalObjective.ngAdditionalQulaitativeObjectiveComments;
                UpdateAdditionalObjectiveObj.Status = $scope.ObjectiveMandatoryStatus;
                UpdateAdditionalObjectiveObj.Fulfillment = $scope.AdditionalObjective.ngAdditionalQulaitativeObjectiveFulfilment;
            }
            if ($scope.ObjectiveTypeId == 4) {
                if ($('#ngRatingAdditionalObjectiveName').val() == '') {
                    bootbox.alert($translate.instant('LanguageContents.Res_4586.Caption'));
                    $('#updateAdditionalObjective').removeAttr("disabled");
                    return false
                }
                UpdateAdditionalObjectiveObj.Objectivename = $scope.AdditionalObjective.ngRatingAdditionalObjectiveName;
                UpdateAdditionalObjectiveObj.ObjectiveInstruction = $scope.ObjectiveDescription;
                UpdateAdditionalObjectiveObj.IsEnableFeedback = true;
                UpdateAdditionalObjectiveObj.UnitID = 0;
                UpdateAdditionalObjectiveObj.status = $scope.AdditionalObjective.ngAdditionalRatingObjectiveStatus;
                UpdateAdditionalObjectiveObj.PlannedTarget = 0;
                UpdateAdditionalObjectiveObj.TargetOutcome = 0;
                UpdateAdditionalObjectiveObj.RatingObjective = $scope.AdditionalObjective.ngAdditionalRatingObjective;
                UpdateAdditionalObjectiveObj.Comments = $scope.AdditionalObjective.ngAdditionalRatingObjectiveComments;
                UpdateAdditionalObjectiveObj.Status = $scope.ObjectiveMandatoryStatus;
                UpdateAdditionalObjectiveObj.Fulfillment = 0;
            }
            PlanobjectiveService.UpdateAdditionalObjectivesforEntity(UpdateAdditionalObjectiveObj).then(function (UpdateAdditionalObjectiveObjResult) {
                $('#AdditionalObjectiveEditDiv').modal('hide');
                if (UpdateAdditionalObjectiveObjResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4353.Caption'));
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4071.Caption'));
                    $scope.GettingAddtionalObjectives();
                }
            });
        }
        $scope.DeleterPredefineObjective = function (objectiveId, objectiveMandatoryStatus) {
            if (objectiveMandatoryStatus == 0) {
                var deletePredefineObj = {};
                deletePredefineObj.ObjectiveId = objectiveId;
                deletePredefineObj.EntityId = parseInt($stateParams.ID, 10);
                var DeleteEntityPredefineObjectiveObj = {};
                PlanobjectiveService.DeleteActivityPredefineObjective(deletePredefineObj).then(function (DeleteEntityPredefineObjectiveObj) {
                    if (DeleteEntityPredefineObjectiveObj.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4302.Caption'));
                    } else {
                        $('#PredefineObjectivetbody > tr[objectiveId =' + objectiveId + ']').remove();
                        NotifySuccess($translate.instant('LanguageContents.Res_4539.Caption'));
                    }
                });
            } else bootbox.alert($translate.instant('LanguageContents.Res_1954.Caption'));
        };
        $scope.DeleteAdditionalObjective = function (objectiveId) {
            var deleteAdditionalObj = {};
            deleteAdditionalObj.ObjectiveId = objectiveId;
            deleteAdditionalObj.EntityID = parseInt($stateParams.ID, 10);
            var DeleteEntityAdditionalObjectiveObj = {};
            PlanobjectiveService.DeleteAdditionalObjective(deleteAdditionalObj).then(function (DeleteEntityAdditionalObjectiveObj) {
                if (DeleteEntityAdditionalObjectiveObj.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4302.Caption'));
                } else {
                    $('#AdditionalObjectivestbody > tr[objectiveId =' + objectiveId + ']').remove();
                    NotifySuccess($translate.instant('LanguageContents.Res_4539.Caption'));
                }
            });
        };
        $scope.savepredefineobjectiveData = function (plannedTarget, targetOutcome, objectiveId, targetType, newValue) {
            var PredefineinLineData = {};
            if (targetType == 11) {
                PredefineinLineData.PlannedTarget = newValue;
                PredefineinLineData.TargetOutcome = targetOutcome;
            }
            if (targetType == 12) {
                PredefineinLineData.PlannedTarget = plannedTarget;
                PredefineinLineData.TargetOutcome = newValue;
            }
            PredefineinLineData.ObjectiveId = objectiveId;
            PredefineinLineData.EntityId = parseInt($stateParams.ID, 10);
            PlanobjectiveService.UpdatePredefineObjectiveinLineData(PredefineinLineData).then(function (UpdatePredefineObjectiveinLineDataObj) {
                var result = UpdatePredefineObjectiveinLineDataObj.Response;
            });
        };
        $scope.saveadditionalobjecitveData = function (plannedTarget, targetOutcome, objectiveId, targetType, newValue) {
            var AdditionalObjinLineData = {};
            AdditionalObjinLineData.Objectivename = newValue;
            AdditionalObjinLineData.ObjectiveId = objectiveId;
            PlanobjectiveService.UpdateAdditionalObjectiveinLineData(AdditionalObjinLineData).then(function (UpdateAdditionalObjectiveinLineDataObj) {
                var result = UpdateAdditionalObjectiveinLineDataObj.Response;
                if (result == true) {
                    $("#AdditionalObjectiveEdit_" + objectiveId)[0].text = newValue;
                    $scope.GettingAddtionalObjectives();
                }
            });
        };

        function SetCursorPosition(ctrID, cursorPosition) {
            ctrID.caret(cursorPosition);
        }
        (function ($) {
            $.caretTo = function (el, index) {
                if (el.createTextRange) {
                    var range = el.createTextRange();
                    range.move("character", index);
                    range.select();
                } else if (el.selectionStart != null) {
                    el.focus();
                    el.setSelectionRange(index, index);
                }
            };
            $.caretPos = function (el) {
                if ("selection" in document) {
                    var range = el.createTextRange();
                    try {
                        range.setEndPoint("EndToStart", document.selection.createRange());
                    } catch (e) {
                        return 0;
                    }
                    return range.text.length;
                } else if (el.selectionStart != null) {
                    return el.selectionStart;
                }
            };
            $.fn.caret = function (index, offset) {
                if (typeof (index) === "undefined") {
                    return $.caretPos(this.get(0));
                }
                return this.queue(function (next) {
                    if (isNaN(index)) {
                        var i = $(this).val().indexOf(index);
                        if (offset === true) {
                            i += index.length;
                        } else if (typeof (offset) !== "undefined") {
                            i += offset;
                        }
                        $.caretTo(this, i);
                    } else {
                        $.caretTo(this, index);
                    }
                    next();
                });
            };
            $.fn.caretToStart = function () {
                return this.caret(0);
            };
            $.fn.caretToEnd = function () {
                return this.queue(function (next) {
                    $.caretTo(this, $(this).val().length);
                    next();
                });
            };
        }(jQuery));

        function FinancialValidation(event) {
            if (event.shiftKey == true) {
                event.preventDefault();
            }
            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 189 || event.keyCode == 190 || event.key === "-") {
                return true;
            } else {
                event.preventDefault();
            }
        };

        function CurrencyFormat(srcNumber) {
            var retCommaString = '';
            var getstr = srcNumber + '';
            if (getstr.match(" ")) return getstr;
            var count = 0;
            var commaString = '';
            for (var x = getstr.length - 1; x >= 0; x--) {
                if (count == 3) {
                    commaString = commaString + ' ';
                    count = 0;
                }
                commaString = commaString + getstr.charAt(x);
                count++;
            }
            for (var j = commaString.length - 1; j >= 0; j--) {
                retCommaString = retCommaString + commaString.charAt(j);
            }
            return (retCommaString);
        }

        function getMoney(A) {
            var a = new Number(A);
            var b = a.toFixed(2);
            a = parseInt(a);
            b = (b - a).toPrecision(2);
            b = parseFloat(b).toFixed(2);
            a = a.toLocaleString();
            a = a.replace(/,/g, '').replace(/ /g, '');
            a = Number(a);
            a = a.formatMoney(0, ' ', ' ');
            if (a.lastIndexOf('.00') > -1) {
                if (a.lastIndexOf('.00') == (a.length - 3)) {
                    a = a.substring(0, a.length - 3);
                }
            }
            if (b.substring(0, 1) == '-') {
                b = b.substring(2);
            } else b = b.substring(1);
            return a + b;
        }

        function handleTextChangeEvents() {
            $('#ObjectivePlannedtarget ,#ObjectiveTargetoutcome ,#NonNumObjectivePlannedtarget ,#NonNumObjectiveTargetoutcome ,#AdditionalNonNumObjectivePlannedtarget ,#AdditionalNonNumObjectiveTargetoutcome').autoNumeric('init', $scope.DecimalSettings['ObjectiveAutoNumeric']);
        }
    }
    app.controller("mui.planningtool.default.detail.section.objectiveCtrl", ['$scope', '$timeout', '$http', '$compile', '$stateParams', '$resource', '$cookies', '$translate', 'PlanobjectiveService', '$modal', 'MetadataService', 'CommonService', 'PlanningService', muiplanningtooldefaultdetailsectionobjectiveCtrl]);
})(angular, app);