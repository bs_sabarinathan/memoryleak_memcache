﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IPurchaseOrder interface for table 'PM_PurchaseOrder'.
    /// </summary>
    public interface IInvoice
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Entityid
        {
            get;
            set;

        }

        DateTime? CreateDate
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }

        string PONumber
        {
            get;
            set;
        }


        string InvoiceNumber
        {
            get;
            set;
        }

        int SupplierID
        {
            get;
            set;

        }

        DateTime? PaymentDate
        {
            get;
            set;

        }

        int? ApproverID
        {
            get;
            set;

        }

        byte[] InvoiceCopy
        {
            get;
            set;

        }


        string POID
        {
            get;
            set;

        }


        int UserID
        {
            get;
            set;

        }

        bool IsExternal
        {
            get;
            set;
        }

        int CostCenterID { get; set; }
        string SupplierName { get; set; }
        decimal amount { get; set; }
        string currencytypeName { get; set; }
        int currencytype { get; set; }
        string strAppprovalDate { get; set; }
        string strPaymentDate { get; set; }
        string strCreatedDate { get; set; }
        string strAmount { get; set; }
        string strStatusName { get; set; }
        bool isApproveSendAuthority { get; set; }
        IList<IFinancialMetadataAttributewithValues> DynamciData { get; set; }

        #endregion
    }
}
