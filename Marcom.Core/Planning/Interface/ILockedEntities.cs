﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    public interface ILockedEntities
    {
        #region Public Properties
        int ID
        {
            set;
            get;
        }
        int EntityID
        {
            set;
            get;
        }
        int LockedBy
        {
            set;
            get;
        }
        string LockedOn
        {
            set;
            get;
        }
        #endregion
    }
}
