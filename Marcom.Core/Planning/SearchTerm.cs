﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.IO;
using Lucene.Net.Index;
using Lucene.Net.Search;
namespace BrandSystems.Marcom.Core.Planning
{
    public class SearchTerm
    {
        private string strMetadataName;
        [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public string MetadataName
        {
            get { return strMetadataName; }
            set { strMetadataName = value; }
        }

        private List<string> objMetadataValue = new List<string>();
        [DataMember(IsRequired = false, Order = 1)]
        public List<string> MetadataValue
        {
            get { return objMetadataValue; }
            set { objMetadataValue = value; }
        }
    }

}
