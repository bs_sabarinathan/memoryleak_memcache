﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

    /// <summary>
    /// Attachments object for table 'PM_Attachments'.
    /// </summary>

    internal class TaskAttachment : ITaskAttachment
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected string _name;
        protected int _activeversionno;
        protected int _activefileid;
        protected int _typeid;
        protected DateTime _createdon;


        #endregion

        #region Constructors
        public TaskAttachment() { }

        public TaskAttachment(int pEntityid, string pName, int pActiveVersionNo, int pActiveFileid, int pTypeid, DateTime pCreatedon)
        {
            this._entityid = pEntityid;
            this._name = pName;
            this._activeversionno = pActiveVersionNo;
            this._activefileid = pActiveFileid;
            this._typeid = pTypeid;
            this._createdon = pCreatedon;
        }

        public TaskAttachment(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int Entityid
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
                _name = value;
            }

        }

        public int ActiveVersionNo
        {
            get { return _activeversionno; }
            set { _activeversionno = value; }

        }

        public int ActiveFileid
        {
            get { return _activefileid; }
            set { _activefileid = value; }

        }

        public virtual int Typeid
        {
            get { return _typeid; }
            set { _typeid = value; }

        }

        public virtual DateTime Createdon
        {
            get { return _createdon; }
            set { _createdon = value; }

        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            TaskAttachment castObj = null;
            try
            {
                castObj = (TaskAttachment)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
