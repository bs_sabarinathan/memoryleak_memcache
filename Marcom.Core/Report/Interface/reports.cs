﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Access;
namespace BrandSystems.Marcom.Core.Report.Interface
{
    /// <summary>
    /// IUser interface for table 'RM_Report'.
    /// </summary>
    public interface IReports
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int OID
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }
        string Description
        {
            get;
            set;

        }

        string Preview
        {
            get;
            set;

        }



        bool Show
        {
            get;
            set;

        }

        String Status
        {
            get;
            set;

        }

        int CategoryId
        {
            get;
            set;

        }

        bool EntityLevel
        {
            get;
            set;

        }

        bool SubLevel
        {
            get;
            set;

        }
        #endregion
    }
}
