using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Access
{

	/// <summary>
	/// GlobalAcl object for table 'AM_GlobalAcl'.
	/// </summary>
    public class EntityTypeRoleAclImport
	{
		#region Member Variables

        protected int _newid;
        protected int _id;
        protected int _entityroleid;
        protected int _moduleid;
        protected int _entitytypeid;
        protected int _sortorder;
        protected string _caption;
		
		
		#endregion
		
		#region Constructors
		public EntityTypeRoleAclImport() {}

        public EntityTypeRoleAclImport(int pId, string _pCaption, int pEntityroleid, int pModuleid, int pEntityTypeid, int pSortOrder, int pNewId)
		{
            this._id = pId;
            this._entityroleid = pEntityroleid; 
			this._moduleid = pModuleid; 
			this._entitytypeid = pEntityTypeid; 
			this._sortorder = pSortOrder;
            this._caption = _pCaption;
            this._newid = pNewId;
		}
		
		#endregion
		
		#region Public Properties

        public int NewId
        {
            get { return _newid; }
            set { _newid = value; }

        }

        public virtual int ID
        {
            get { return _id; }
            set {  _id = value; }

        }

        public virtual string Caption
        {
            get { return _caption; }
            set { _caption = value; }

        }

        public virtual int EntityRoleID
        {
            get { return _entityroleid; }
            set {  _entityroleid = value; }

        }

        public virtual int ModuleID
        {
            get { return _moduleid; }
            set {  _moduleid = value; }

        }

        public virtual int EntityTypeID
        {
            get { return _entitytypeid; }
            set {  _entitytypeid = value; }

        }

        public virtual int Sortorder
        {
            get { return _sortorder; }
            set { _sortorder = value; }

        }
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            EntityTypeRoleAclImport castObj = null;
			try
			{
                castObj = (EntityTypeRoleAclImport)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.ID);
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
				
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
		}
		#endregion
		
	}
	
}
