﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public partial class TagOptionDao : BaseDao, ICloneable 
    {
        #region Member Variables

		protected int _id;
		protected string _caption;
		protected int _attributeid;
        protected int _userid;
        protected string _createdon;
        protected string _usedon;
        protected int _totalhits;
        protected int _populartagstoshow;
        protected int _sortorder;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public TagOptionDao() {}

        public TagOptionDao(int pId, string pCaption, int pAttributeid, int pUserid, int pSortorder, string pCreateon, string pUsedon,int pTotalHits, int pPopularTagsToShow)
        {
            this._id = pId;
            this._caption = pCaption;
            this._attributeid = pAttributeid;
            this._userid = pUserid;
            this._sortorder = pSortorder;
            this._createdon = pCreateon;
            this._usedon = pUsedon;
            this._totalhits = pTotalHits;
            this._populartagstoshow = pPopularTagsToShow;
        }

        public TagOptionDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 500)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 500 characters");
			  _bIsChanged |= (_caption != value); 
			  _caption = value; 
			}
			
		}

        public virtual int AttributeID
		{
			get { return _attributeid; }
			set { _bIsChanged |= (_attributeid != value); _attributeid = value; }
			
		}

        public virtual int UserID
        {
            get { return _userid; }
            set { _bIsChanged |= (_userid != value); _userid = value; }

        }

        public virtual string CreateOn
        {
            get { return _createdon; }
            set
            {
                if (value != null && value.Length > 10)
                    throw new ArgumentOutOfRangeException("CreatedOn", "CreatedOn value, cannot contain more than 10 characters");
                _bIsChanged |= (_createdon != value);
                _createdon = value;
            }

        }

        public virtual string UsedOn
        {
            get { return _usedon; }
            set
            {
                if (value != null && value.Length > 10)
                    throw new ArgumentOutOfRangeException("UsedOn", "UsedOn value, cannot contain more than 10 characters");
                _bIsChanged |= (_usedon != value);
                _usedon = value;
            }

        }

        public virtual int TotalHits
        {
            get { return _totalhits; }
            set { _bIsChanged |= (_totalhits != value); _totalhits = value; }
        }

        public virtual int PopularTagsToShow
        {
            get { return _populartagstoshow; }
            set { _bIsChanged |= (_populartagstoshow != value); _populartagstoshow = value; }
        }

        public virtual int SortOrder
        {
            get { return _sortorder; }
            set { _bIsChanged |= (_sortorder != value); _sortorder = value; }
        }


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string TagOptionDao = "TagOptionDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";
            public const string AttributeID = "AttributeID";
            public const string UserID = "UserID";
            public const string CreateOn = "CreateOn";
            public const string Sortorder = "SortOrder";
            public const string UsedOn = "UsedOn";
            public const string TotalHits = "TotalHits";
            public const string PopularTagsToShow = "PopularTagsToShow";
					
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string TagOptionDao = "TagOptionDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";
            public const string AttributeID = "AttributeID";
            public const string UserID = "UserID";
            public const string CreateOn = "CreateOn";
            public const string Sortorder = "SortOrder";
            public const string UsedOn = "UsedOn";
            public const string TotalHits = "TotalHits";
            public const string PopularTagsToShow = "PopularTagsToShow";
		}

		#endregion
    }
}
