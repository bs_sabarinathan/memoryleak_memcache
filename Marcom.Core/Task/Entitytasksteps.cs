﻿using BrandSystems.Marcom.Core.Task.Interface;
using BrandSystems.Marcom.Dal;
using BrandSystems.Marcom.Dal.Task.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Task
{
    public partial class Entitytasksteps : IEntitytasksteps, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _TempID { get; set; }
        protected int _PhaseId { get; set; }
        protected string _Name { get; set; }
        protected string _Description { get; set; }
        protected int _Duration { get; set; }
        protected int _MinApproval { get; set; }
        protected string _Roles { get; set; }
        protected bool _IsMandatory { get; set; }
        protected int _SortOrder { get; set; }
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public Entitytasksteps() { }

        public Entitytasksteps(int PhaseId, string TempID, string Name, string Description, int Duration, int MinApproval, string Roles, bool IsMandatory, int SortOrder)
        {
            this._PhaseId = PhaseId;
            this._Name = Name;
            this._Description = Description;
            this._Duration = Duration;
            this._MinApproval = MinApproval;
            this._Roles = Roles;
            this._IsMandatory = IsMandatory;
            this._SortOrder = SortOrder;
            this._TempID = TempID;
        }

        public Entitytasksteps(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }
        public virtual int PhaseId
        {
            get { return _PhaseId; }
            set { _bIsChanged |= (_PhaseId != value); _PhaseId = value; }
        }
        public virtual string TempID
        {
            get { return _TempID; }
            set { _bIsChanged |= (_TempID != value); _TempID = value; }
        }
        public virtual string Name
        {
            get { return _Name; }
            set { _bIsChanged |= (_Name != value); _Name = value; }
        }
        public virtual string Description
        {
            get { return _Description; }
            set { _bIsChanged |= (_Description != value); _Description = value; }
        }

        public virtual int Duration
        {
            get { return _Duration; }
            set { _bIsChanged |= (_Duration != value); _Duration = value; }
        }

        public virtual int MinApproval
        {
            get { return _MinApproval; }
            set { _bIsChanged |= (_MinApproval != value); _MinApproval = value; }
        }
        public virtual string Roles
        {
            get { return _Roles; }
            set { _bIsChanged |= (_Roles != value); _Roles = value; }
        }
        public virtual bool IsMandatory
        {
            get { return _IsMandatory; }
            set { _bIsChanged |= (_IsMandatory != value); _IsMandatory = value; }
        }
        public virtual int SortOrder
        {
            get { return _SortOrder; }
            set { _bIsChanged |= (_SortOrder != value); _SortOrder = value; }
        }


        public int Status { get; set; }
      

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string TempID = "TempID";
            public const string ID = "ID";
            public const string PhaseId = "PhaseId";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string Duration = "Duration";
            public const string MinApproval = "MinApproval";
            public const string Roles = "Roles";
            public const string IsMandatory = "IsMandatory";
            public const string SortOrder = "SortOrder";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string TempID = "TempID";
            public const string ID = "ID";
            public const string PhaseId = "PhaseId";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string Duration = "Duration";
            public const string MinApproval = "MinApproval";
            public const string Roles = "Roles";
            public const string IsMandatory = "IsMandatory";
            public const string SortOrder = "SortOrder";
        }

        #endregion
    }
}
