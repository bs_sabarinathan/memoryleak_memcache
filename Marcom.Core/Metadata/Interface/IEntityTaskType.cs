﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    /// <summary>
    /// IOption interface for table 'MM_EntityTaskType'.
    /// </summary>
    public interface IEntityTaskType
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }


        int EntitytypeId
        {
            get;
            set;

        }
        int TaskTypeId
        {
            get;
            set;

        }

        #endregion
    }
}
