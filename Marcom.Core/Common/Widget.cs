﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{
    class Widget : IWidget
    {
            
        #region Member Variables
        protected string _id;
        protected int _templateid;
            protected string _caption;
            protected string _description;
            protected int _widgettypeid;
            protected bool _isdynamic;
            protected int _filterid;
            protected int _attributeid;
            protected int _dimensionid;
            protected string _matrixid;
            protected string _widgetquery;
            protected int _column;
            protected int _row;
            protected int _sizex;
            protected int _sizey;
            protected int _visualtype;
            protected bool _bIsDeleted;
            protected bool _bIsChanged;
            protected int _NoOfItem;
            protected string _listOfEntityIDs;
            protected string _listofSelectEntityIDs;
            protected int _NoOfYear;
            protected int _NoOfMonth;
        
        #endregion

            #region Constructors

            public Widget() { }

            public Widget(string caption, string description, int widgettypeid, int filterid, int attributeid) {
                this._caption = caption;
                this._description = description;
                this._widgettypeid = widgettypeid;
                this._filterid = filterid;
                this._attributeid = attributeid;
            }

        #endregion

        #region Public Properties

            public virtual string Id
            {
                get { return _id; }
                set { _bIsChanged |= (_id != value); _id = value; }

            }

            public virtual int TemplateID
            {
                get { return _templateid; }
                set { _bIsChanged |= (_templateid != value); _templateid = value; }

            }

            public virtual string Caption
            {
                get { return _caption; }
                set
                {
                    if (value != null && value.Length > 50)
                        throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
                    _bIsChanged |= (_caption != value);
                    _caption = value;
                }

            }

            public virtual string Description
            {
                get { return _description; }
                set
                {
                    if (value != null && value.Length > 1073741823)
                        throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                    _bIsChanged |= (_description != value);
                    _description = value;
                }

            }

            public virtual int WidgetTypeID
            {
                get { return _widgettypeid; }
                set { _bIsChanged |= (_widgettypeid != value); _widgettypeid = value; }

            }

            public virtual bool IsDynamic
            {
                get { return _isdynamic; }
                set { _bIsChanged |= (_isdynamic != value); _isdynamic = value; }
            }

            public virtual int FilterID
            {
                get { return _filterid; }
                set { _bIsChanged |= (_filterid != value); _filterid = value; }

            }

            public virtual int AttributeID
            {
                get { return _attributeid; }
                set { _bIsChanged |= (_attributeid != value); _attributeid = value; }

            }


            public virtual int DimensionID
            {
                get { return _dimensionid; }
                set { _bIsChanged |= (_dimensionid != value); _dimensionid = value; }

            }

            public virtual string MatrixID
            {
                get { return _matrixid; }
                set
                {
                    _bIsChanged |= (_matrixid != value);
                    _matrixid = value;
                }
            }

            public virtual string WidgetQuery
            {
                get { return _widgetquery; }
                set
                {
                    _bIsChanged |= (_widgetquery != value);
                    _widgetquery = value;
                }
            }

            public virtual int Column
            {
                get { return _column; }
                set { _bIsChanged |= (_column != value); _column = value; }
            }

            public virtual int Row
            {
                get { return _row; }
                set { _bIsChanged |= (_row != value); _row = value; }
            }

            public virtual int SizeX
            {
                get { return _sizex; }
                set { _bIsChanged |= (_sizex != value); _sizex = value; }
            }

            public virtual int SizeY
            {
                get { return _sizey; }
                set { _bIsChanged |= (_sizey != value); _sizey = value; }
            }

            public virtual int VisualType
            {
                get { return _visualtype; }
                set
                {
                    _bIsChanged |= (_visualtype != value);
                    _visualtype = value;
                }
            }

            public virtual int NoOfItem
            {
                get { return _NoOfItem; }
                set { _bIsChanged |= (_NoOfItem != value); _NoOfItem = value; }

            }

            public virtual string ListOfEntityIDs
            {
                get { return _listOfEntityIDs; }
                set
                {
                    _bIsChanged |= (_listOfEntityIDs != value);
                    _listOfEntityIDs = value;
                }

            }

            public virtual string ListofSelectEntityIDs
            {
                get { return _listofSelectEntityIDs; }
                set
                {
                    _bIsChanged |= (_listofSelectEntityIDs != value);
                    _listofSelectEntityIDs = value;
                }

            }
            public virtual int NoOfYear
            {
                get { return _NoOfYear; }
                set { _bIsChanged |= (_NoOfYear != value); _NoOfYear = value; }

            }
            public virtual int NoOfMonth
            {
                get { return _NoOfMonth; }
                set { _bIsChanged |= (_NoOfMonth != value); _NoOfMonth = value; }

            }
        #endregion

    }
}
