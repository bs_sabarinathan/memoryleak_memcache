﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    /// <summary>
    /// IAttribute interface for table 'MM_WorkFlowStep_PredefinedTask'.
    /// </summary>
    public interface IWorkFlowStepPredefinedTasks
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        } 

        int StepID
        {
            get;
            set;

        }

        int TaskID
        {
            get; 
            set;

        }


        #endregion
    }
}
