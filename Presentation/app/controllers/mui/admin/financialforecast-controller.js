﻿(function (ng, app) {
    "use strict"; function muiadminfinancialforecastctrl($scope, $location, $resource, $timeout, $cookies, $translate, AdminService) {
        $scope.aDivisonIdsList = []; $scope.Id = 0; var DivisionSelectedID = ''; var ForecastBasisselectedID = ''; AdminService.getfinancialForecastIds().then(function (GetAdditionalresult) { if (GetAdditionalresult.StatusCode == 200) { $scope.aDivisonIdsList = GetAdditionalresult.Response.m_Item1; $scope.DivisionSelected = $scope.aDivisonIdsList[0]; DivisionSelectedID = $scope.aDivisonIdsList[0].divisionforecastingIds; $scope.forcastbasis = GetAdditionalresult.Response.m_Item2; $scope.ForecastBasisselected = $scope.forcastbasis[0]; ForecastBasisselectedID = $scope.forcastbasis[0].basisforecastingIds; $scope.forecastLevelDataSelected = '0'; getFinancialForecastsettings(); } }); function getFinancialForecastsettings() {
            var DivisionSelectedID = ''; var ForecastBasisselectedID = ''; AdminService.GetFinancialForecastsettings().then(function (GetFinancialForecastsettingsresult) {
                if (GetFinancialForecastsettingsresult.StatusCode == 200) {
                    $scope.Id = GetFinancialForecastsettingsresult.Response.ID; $scope.DivisionSelected = $.grep($scope.aDivisonIdsList, function (e) { return e.divisionforecastingIds == GetFinancialForecastsettingsresult.Response.ForecastDivision })[0]; $scope.ForecastBasisselected = $.grep($scope.forcastbasis, function (e) { return e.basisforecastingIds == GetFinancialForecastsettingsresult.Response.ForecastBasis })[0]; $scope.forecastLevelDataSelected = GetFinancialForecastsettingsresult.Response.ForecastLevel; $scope.xdays = GetFinancialForecastsettingsresult.Response.ForecastDeadlines; if (GetFinancialForecastsettingsresult.Response.IsFinancialForecast) { $scope.CheckFFF = true; }
                    else { $scope.CheckFFF = false; }
                }
            });
        }
        $scope.SetIsFinancialStatus = function (event) {
            var checkbox = event.target; if (checkbox.checked)
                $scope.CheckFFF = true; else
                $scope.CheckFFF = false; $scope.UpdateForecastSettings();
        }
        $scope.SetDivison = function (t, type) {
            switch (type) { case 'division': $scope.DivisionSelected = t; DivisionSelectedID = t.divisionforecastingIds; break; case 'basis': $scope.ForecastBasisselected = t; ForecastBasisselectedID = t.basisforecastingIds; break; case 'level': $scope.forecastLevelDataSelected = t; break; }
            $timeout(function () { $scope.UpdateForecastSettings(); }, 100);
        }
        $scope.xdays = ""; $scope.UpdateForecastSettings = function () {
            var ObjFinancialForecastSettings = {}; ObjFinancialForecastSettings.Id = $scope.Id; ObjFinancialForecastSettings.ForecastBasis = $scope.ForecastBasisselected.basisforecastingIds; ObjFinancialForecastSettings.ForecastLevel = $scope.forecastLevelDataSelected; ObjFinancialForecastSettings.Forecastdeadlines = $scope.xdays != "" ? $scope.xdays : 0; ObjFinancialForecastSettings.ForecastDivision = $scope.DivisionSelected.divisionforecastingIds; ObjFinancialForecastSettings.IsFinancialforecast = $scope.CheckFFF; AdminService.updatefinancialforecastsettings(ObjFinancialForecastSettings).then(function (updateFinancialForecastSettings) {
                if (updateFinancialForecastSettings.StatusCode == 200) { $scope.Id = updateFinancialForecastSettings.Response; $scope.FinancialCurrentDivisionID.ID = $scope.DivisionSelected.divisionforecastingIds; NotifySuccess($translate.instant('LanguageContents.Res_4805.Caption')); }
                else { NotifyError($translate.instant('LanguageContents.Res_4351.Caption')); }
            });
        }
        $scope.getFFDataByStatusinfo = function () {
            var objGetFinancialforecastDataByStatus = {}; if (val == "Financialforecastsettings") { objGetFinancialforecastDataByStatus.Status = $scope.CheckFFF; }
            objGetFinancialforecastDataByStatus.FinancialData = val; AdminService.UpdateFinancialforecastData(objGetFinancialforecastDataByStatus).then(function (objGetFinancialforecastDataByStatus) { if (objGetFinancialforecastDataByStatus.Status == true || objGetFinancialforecastDataByStatus.Status == false) { NotifySuccess($translate.instant('LanguageContents.Res_4872.Caption')); } });
        };
    }
    app.controller("mui.admin.financialforecastctrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$translate', 'AdminService', muiadminfinancialforecastctrl])
})(angular, app);