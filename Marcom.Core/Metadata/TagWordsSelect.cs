﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class TagWordsSelect : ITagWordsSelect
    {
        #region Member Variables

        protected int _id;
		protected int _entityid;
        protected int _attributeid;
        protected int _optionid;
		
		
		#endregion
		
		#region Constructors
		public TagWordsSelect() {}

        public TagWordsSelect(int pId, int pEntityid, int pAttributeid, int pOptionid)
		{
            this._id = pId;
			 this._entityid = pEntityid;
            this._optionid = pOptionid;
            this._attributeid = pAttributeid;
		}
		
		#endregion
		
		#region Public Properties
		
        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }
		
        public virtual int Entityid
        {
            get { return _entityid; }
            set {  _entityid = value; }

        }
        public virtual int Attributeid
        {
            get { return _attributeid; }
            set { _attributeid = value; }

        }
        public virtual int Optionid
        {
            get { return _optionid; }
            set { _optionid = value; }

        }

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            TagWordsSelect castObj = null;
			try
			{
                castObj = (TagWordsSelect)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
				
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
		}
		#endregion
    }
}
