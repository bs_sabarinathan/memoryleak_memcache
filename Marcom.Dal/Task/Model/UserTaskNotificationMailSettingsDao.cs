using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Task.Model
{

    /// <summary>
    /// UserNotificationMailSettings object for table 'TM_UserNotificationMailSettings'.
    /// </summary>

    public partial class UserTaskNotificationMailSettingsDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _userid;
        protected DateTimeOffset _lastsenton;
        protected DateTimeOffset _lastupdatedon;
        protected bool _isnotificationenable;
        protected int _noofdays;
        protected TimeSpan _notificationtiming;
        protected bool _isemailenable;
        protected TimeSpan _mailtiming;
        protected bool _isemailassignedenable;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public UserTaskNotificationMailSettingsDao() { }

        public UserTaskNotificationMailSettingsDao(int pUserid, DateTimeOffset pLastSentOn, DateTimeOffset pLastUpdatedOn, bool pIsNotificationEnable, int pNoOfDays, TimeSpan pNotificationTiming, bool pIsEmailEnable, TimeSpan pMailTiming,bool pIsEmailAssignedEnable)
        {
            this._userid = pUserid;
            this._lastsenton = pLastSentOn;
            this._lastupdatedon = pLastUpdatedOn;
            this._isnotificationenable = pIsNotificationEnable;
            this._noofdays = pNoOfDays;
            this._notificationtiming = pNotificationTiming;
            this._isemailenable = pIsEmailEnable;
            this._mailtiming = pMailTiming;
            this._isemailassignedenable = pIsEmailAssignedEnable;
        }

        public UserTaskNotificationMailSettingsDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int Userid
        {
            get { return _userid; }
            set { _bIsChanged |= (_userid != value); _userid = value; }

        }

        public virtual DateTimeOffset LastSentOn
        {
            get { return _lastsenton; }
            set { _bIsChanged |= (_lastsenton != value); _lastsenton = value; }

        }

        public virtual DateTimeOffset LastUpdatedOn
        {
            get { return _lastupdatedon; }
            set { _bIsChanged |= (_lastupdatedon != value); _lastupdatedon = value; }

        }

        public virtual bool IsNotificationEnable
        {
            get { return _isnotificationenable; }
            set { _bIsChanged |= (_isnotificationenable != value); _isnotificationenable = value; }

        }

        public virtual int NoOfDays
        {
            get { return _noofdays; }
            set { _bIsChanged |= (_noofdays != value); _noofdays = value; }

        }

        public virtual TimeSpan NotificationTiming
        {
            get { return _notificationtiming; }
            set { _bIsChanged |= (_notificationtiming != value); _notificationtiming = value; }

        }

        public virtual bool IsEmailEnable
        {
            get { return _isemailenable; }
            set { _bIsChanged |= (_isemailenable != value); _isemailenable = value; }

        }

        public virtual TimeSpan MailTiming
        {
            get { return _mailtiming; }
            set { _bIsChanged |= (_mailtiming != value); _mailtiming = value; }

        }

        public virtual bool IsEmailAssignedEnable
        {
            get { return _isemailassignedenable; }
            set { _bIsChanged |= (_isemailassignedenable != value); _isemailassignedenable = value; }
        }
        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string UserNotificationMailSettings = "UserNotificationMailSettings";
            public const string Id = "Id";
            public const string Userid = "Userid";
            public const string LastSentOn = "LastSentOn";
            public const string LastUpdatedOn = "LastUpdatedOn";
            public const string IsNotificationEnable = "IsNotificationEnable";
            public const string NoOfDays = "NoOfDays";
            public const string NotificationTiming = "NotificationTiming";
            public const string IsEmailEnable = "IsEmailEnable";
            public const string MailTiming = "MailTiming";
            public const string IsEmailAssignedEnable = "IsEmailAssignedEnable";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string UserNotificationMailSettings = "UserNotificationMailSettings";
            public const string Id = "Id";
            public const string Userid = "Userid";
            public const string LastSentOn = "LastSentOn";
            public const string LastUpdatedOn = "LastUpdatedOn";
            public const string IsNotificationEnable = "IsNotificationEnable";
            public const string NoOfDays = "NoOfDays";
            public const string NotificationTiming = "NotificationTiming";
            public const string IsEmailEnable = "IsEmailEnable";
            public const string MailTiming = "MailTiming";
            public const string IsEmailAssignedEnable = "IsEmailAssignedEnable";
        }

        #endregion


    }

}
