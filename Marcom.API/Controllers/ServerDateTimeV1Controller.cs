﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Marcom.API.Authorization;
using Marcom.API.Models;
using Marcom.API.Services;

namespace Marcom.API.Controllers
{
    [AuthenticateTokenAttribute]
    public class ServerDateTimeV1Controller : ApiController
    {
         #region MemberVariables

        private ServerDateTimeOverviewRepositoryV1 serverdatetimeRepository;

        #endregion

        #region ConstructorLogic

        /// <summary>
        /// Constructer logic
        /// </summary>
        public ServerDateTimeV1Controller()
        {
            this.serverdatetimeRepository = new ServerDateTimeOverviewRepositoryV1();
        }

        #endregion

        #region  API-Actions

        /// <summary>
        /// This will fetch the current date time of the server
        /// </summary>
        [HttpGet]
        [Queryable]
        public ServerDateTimeModelV1 GetServerDateTime()
        {
            try
            {

                return serverdatetimeRepository.GetServerDateTime();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpGet]
        public string AuthenticateUser()
        {
            return "Validation successfull";
        }
        #endregion
    }
}
