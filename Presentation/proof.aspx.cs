﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using Presentation.com.proofhq.www;
using System.IO;
using System.Xml.Linq;
using Presentation.Utility;
using System.Configuration;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Utility;
using BrandSystems.Marcom.Core.AmazonStorageHelper;

namespace Presentation
{
    public partial class proof : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {


                var TaskID = Request.QueryString["tid"];
                var LoginUser = Request.QueryString["uid"];
                var TaskOwner = Request.QueryString["oid"];
                var Username = Request.QueryString["uname"];


                //string xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], "AdminSettings.xml");
                //XDocument adminXmlDoc = XDocument.Load(xmlpath);
                string TenantHost = HttpContext.Current.Request.Url.Host;
                TenantSelection tfp = new TenantSelection();
                string TenantFilePath = tfp.GetTenantFilePathByHostName(TenantHost);
                int TenantID = tfp.GetTenantIDByHostName(TenantHost);

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, MarcomManagerFactory.GetSystemSession(TenantID));
                string xmlpath = "";
                XElement xelementFilepath;
                if (marcomManager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
                {
                    xmlpath = Path.Combine(marcomManager.User.TenantPath, "AdminSettings.xml");
                    xelementFilepath = AWSHelper.ReadAdminXElement(marcomManager.User.AwsStorage.S3, marcomManager.User.AwsStorage.BucketName, xmlpath);
                }
                else
                {
                    xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + marcomManager.User.TenantPath, "AdminSettings.xml");
                    xelementFilepath = XElement.Load(xmlpath);
                }
                //The Key is root node current Settings
                string xelementName = "ProofHQSettings_Table";
                
                var pfEmail = xelementFilepath.Element(xelementName).Element("ProofHQSettings").Element("Email");
                var pfPwd = xelementFilepath.Element(xelementName).Element("ProofHQSettings").Element("Password");
                TenantResolver tenantres = new TenantResolver(System.Web.HttpContext.Current.Request.Url.Host.ToString());
                
                Guid userSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);
                IMarcomManager managers = MarcomManagerFactory.GetMarcomManager(null, userSession);

                var proofID = managers.DigitalAssetManager.GetProofIDByTaskID(Convert.ToInt32(TaskID));

                if (proofID != 0 || proofID != -1)
                {



                    var client = new soapService();
                    //Do login in to proofHQ. Need to use billing account
                    SOAPLoginObject res = client.doLogin(pfEmail.Value, pfPwd.Value);

                    //Get proof Id from task id

                    //Get proof thumnail
                    var image = client.getProofThumbnail(res.session, proofID, 1);
                    proofThum.Src = "data:" + image.data + ";" + image.encoding + "," + image.content;

                    var proofdetail = client.getProofDetails(res.session, proofID);

                    proofName.InnerText = proofdetail.filename;
                    proofVersion.InnerText = Convert.ToString(proofdetail.version);





                    proofS.InnerText = "Proof status if " + proofdetail.decision;

                    var sbStatus = new StringBuilder();



                    if (proofdetail.progress.sent == 2)
                    {
                        sbStatus.AppendLine("<span class='proofStatus-icon success'>S</span>");
                    }
                    else
                    {
                        sbStatus.AppendLine("<span class='proofStatus-icon'>S</span>");
                    }
                    if (proofdetail.progress.opened == 2)
                    {
                        sbStatus.AppendLine("<span class='proofStatus-icon success'>O</span>");
                    }
                    else
                    {
                        sbStatus.AppendLine("<span class='proofStatus-icon '>O</span>");
                    }
                    if (proofdetail.progress.comment_made == 2)
                    {
                        sbStatus.AppendLine("<span class='proofStatus-icon success'>C</span>");
                    }
                    else
                    {
                        sbStatus.AppendLine("<span class='proofStatus-icon '>C</span>");
                    }
                    if (proofdetail.progress.decision_made == 2)
                    {
                        sbStatus.AppendLine("<span class='proofStatus-icon success'>D</span>");
                    }
                    else
                    {
                        sbStatus.AppendLine("<span class='proofStatus-icon '>D</span>");
                    }


                    proofStatus.Text = sbStatus.ToString();


                    var reviewers = client.getProofReviewers(res.session, Convert.ToString(proofID));


                    var sb = new StringBuilder();
                    bool UserPresent = false;

                    foreach (var item in reviewers)
                    {
                        if (Convert.ToString(pfEmail.Value) != item.email)
                        {
                            var UserID = managers.DigitalAssetManager.GetUserIDByEmail(item.email);
                            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                            sb.AppendLine("<div><span class='assignee'><img src='Handlers/UserImage.ashx?id=" + UserID + "&amp;time=" + unixTimestamp + "'>" + item.name + "<span class='pull-right statusHolder'>" + item.decision + "</span></span></div>");

                            if (Username == item.email)
                            {
                                proofUrl.HRef = item.proof_url;
                                UserPresent = true;
                            }
                        }
                    }
                    if (!UserPresent)
                    {
                        proofUrl.HRef = proofdetail.team_url;
                    }



                    reviewer.Text = sb.ToString();

                }
            }
            catch (Exception)
            {

                TaskPopup.Style.Add("display", "none");
                //do nothing;
                errormsg.Text = "<h3 style='color: #444;text-align: center;margin-top: 20%;'>Unable to retrive proof detail.</h3>";
            }
        }
    }
}