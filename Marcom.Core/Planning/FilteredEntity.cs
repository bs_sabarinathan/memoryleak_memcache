﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.IO;

namespace BrandSystems.Marcom.Core.Planning
{
    /// <summary>
    /// Represent news feed
    /// </summary>
    [DataContract(Namespace = "MarcomPlatform")]
    public class FilteredEntity
    {

        private int intID;
        [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public int ID
        {
            get { return intID; }
            set { intID = value; }
        }

        private int intParentID;
        [DataMember(IsRequired = false, Order = 2), XmlAttribute()]
        public int ParentID
        {
            get { return intParentID; }
            set { intParentID = value; }
        }

        private int intTypeID;
        [DataMember(IsRequired = false, Order = 3), XmlAttribute()]
        public int TypeID
        {
            get { return intTypeID; }
            set { intTypeID = value; }
        }

        private int intLevel;
        [DataMember(IsRequired = false, Order = 4), XmlAttribute()]
        public int Level
        {
            get { return intLevel; }
            set { intLevel = value; }
        }

        private string strUniqueKey;
        [DataMember(IsRequired = false, Order = 5), XmlAttribute()]
        public string UniqueKey
        {
            get { return strUniqueKey; }
            set { strUniqueKey = value; }
        }

        private string strCostCenter;
        [DataMember(IsRequired = false, Order = 6), XmlAttribute()]
        public string CostCenter
        {
            get { return strCostCenter; }
            set { strCostCenter = value; }
        }

        private string strObjective;
        [DataMember(IsRequired = false, Order = 7), XmlAttribute()]
        public string Objective
        {
            get { return strObjective; }
            set { strObjective = value; }
        }

        private string strTotalChildrenCount;
        [DataMember(IsRequired = false, Order = 8), XmlAttribute()]
        public string TotalChildrenCount
        {
            get { return strTotalChildrenCount; }
            set { strTotalChildrenCount = value; }
        }

        private string strParentCount;
        [DataMember(IsRequired = false, Order = 9), XmlAttribute()]
        public string ParentCount
        {
            get { return strParentCount; }
            set { strParentCount = value; }
        }

        private string strCSS;
        [DataMember(IsRequired = false, Order = 10), XmlAttribute()]
        public string CSS
        {
            get { return strCSS; }
            set { strCSS = value; }
        }

        private int intPageNo;
        [DataMember(IsRequired = false, Order = 11), XmlAttribute()]
        public int PageNo
        {
            get { return intPageNo; }
            set { intPageNo = value; }
        }

        private bool boolIsExpanded = false;
        [DataMember(IsRequired = false, Order = 12), XmlAttribute()]
        public bool IsExpanded
        {
            get { return boolIsExpanded; }
            set { boolIsExpanded = value; }
        }

        private string strEntityIds;
        [DataMember(IsRequired = false, Order = 13), XmlAttribute()]
        public string EntityIds
        {
            get { return strEntityIds; }
            set { strEntityIds = value; }
        }

        private int intObjectiveTypeID;
        [DataMember(IsRequired = false, Order = 14), XmlAttribute()]
        public int ObjectiveTypeID
        {
            get { return intObjectiveTypeID; }
            set { intObjectiveTypeID = value; }
        }

    }

    /// <summary>
    /// Represent news feed
    /// </summary>
    [DataContract(Namespace = "MarcomPlatform")]
    public class FilteredEntityHolder
    {

        private int intHash;
        public int Hash
        {
            get { return intHash; }
            set { intHash = value; }
        }

        private List<FilteredEntity> objFilteredEntityHolder = new List<FilteredEntity>();
        [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public List<FilteredEntity> FilterEntityHolder
        {
            get { return objFilteredEntityHolder; }
            set { objFilteredEntityHolder = value; }
        }

        private List<FilteredEntity> objFilteredCostCenterHolder = new List<FilteredEntity>();
        [DataMember(IsRequired = false, Order = 2), XmlAttribute()]
        public List<FilteredEntity> FilteredCostCenterHolder
        {
            get { return objFilteredCostCenterHolder; }
            set { objFilteredCostCenterHolder = value; }
        }

        private List<FilteredEntity> objFilteredObjectiveHolder = new List<FilteredEntity>();
        [DataMember(IsRequired = false, Order = 3), XmlAttribute()]
        public List<FilteredEntity> FilteredObjectiveHolder
        {
            get { return objFilteredObjectiveHolder; }
            set { objFilteredObjectiveHolder = value; }
        }

        private int intTotalPage;
        [DataMember(IsRequired = false, Order = 4), XmlAttribute()]
        public int TotalPage
        {
            get { return intTotalPage; }
            set { intTotalPage = value; }
        }

        private int intNoItemOnLastPage;
        [DataMember(IsRequired = false, Order = 5), XmlAttribute()]
        public int NoItemOnLastPage
        {
            get { return intNoItemOnLastPage; }
            set { intNoItemOnLastPage = value; }
        }

        private bool boolIsSpecialCase = false;
        [DataMember(IsRequired = false, Order = 6), XmlAttribute()]
        public bool IsSpecialCase
        {
            get { return boolIsSpecialCase; }
            set { boolIsSpecialCase = value; }
        }
        private string StrLastfetchedDate;
        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>The CurrentDateandTime.</value>
        [DataMember(IsRequired = false, Order = 7), XmlAttribute()]
        public string LastfetchedDate
        {
            // Return StrLastfetchedDate
            get { return (System.DateTime.Now).ToString("yyyy-MM-dd HH:mm:ss"); }
            set { StrLastfetchedDate = value; }
        }
    }

    [DataContract(Namespace = "MarcomPlatform")]
    public class TreeInput
    {

        private int intPageID;
        [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public int PageID
        {
            get { return intPageID; }
            set { intPageID = value; }
        }

        private string strActivityListID;
        [DataMember(IsRequired = false, Order = 2), XmlAttribute()]
        public string ActivityListID
        {
            get { return strActivityListID; }
            set { strActivityListID = value; }
        }

        private string strCostCenterID;
        [DataMember(IsRequired = false, Order = 3), XmlAttribute()]
        public string CostCenterID
        {
            get { return strCostCenterID; }
            set { strCostCenterID = value; }
        }


        private string strParentID;
        [DataMember(IsRequired = false, Order = 4), XmlAttribute()]
        public string ParentID
        {
            get { return strParentID; }
            set { strParentID = value; }
        }

        private int intIncludeAllChildren;
        [DataMember(IsRequired = false, Order = 5), XmlAttribute()]
        public int IncludeAllChildren
        {
            get { return intIncludeAllChildren; }
            set { intIncludeAllChildren = value; }
        }

        private int intCostCenterGroupID;
        [DataMember(IsRequired = false, Order = 6), XmlAttribute()]
        public int CostCenterGroupID
        {
            get { return intCostCenterGroupID; }
            set { intCostCenterGroupID = value; }
        }

        private int intObjectiveGroupID;
        [DataMember(IsRequired = false, Order = 7), XmlAttribute()]
        public int ObjectiveGroupID
        {
            get { return intObjectiveGroupID; }
            set { intObjectiveGroupID = value; }
        }

        private bool intIsGlobalAdmin;
        [DataMember(IsRequired = false, Order = 8), XmlAttribute()]
        public bool IsGlobalAdmin
        {
            get { return intIsGlobalAdmin; }
            set { intIsGlobalAdmin = value; }
        }

        private int intFilterID;
        [DataMember(IsRequired = false, Order = 9), XmlAttribute()]
        public int FilterID
        {
            get { return intFilterID; }
            set { intFilterID = value; }
        }

        private string intPublishDate;
        [DataMember(IsRequired = false, Order = 10), XmlAttribute()]
        public string PublishDate
        {
            get { return intPublishDate; }
            set { intPublishDate = value; }
        }

        private string strSelectedID;
        [DataMember(IsRequired = false, Order = 11), XmlAttribute()]
        public string SelectedID
        {
            get { return strSelectedID; }
            set { strSelectedID = value; }
        }

        private string strObjectiveID;
        [DataMember(IsRequired = false, Order = 12), XmlAttribute()]
        public string ObjectiveID
        {
            get { return strObjectiveID; }
            set { strObjectiveID = value; }
        }

    }
}
