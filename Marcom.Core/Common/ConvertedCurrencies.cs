﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Common
    {
    class ConvertedCurrencies : IConvertedcurrencies
        {

        #region Member Variables
        protected int _id;
        protected string _currency;
        protected decimal _convertedAmount;
        protected string _error;
        //protected dynamic _convertedCurrency;
        #endregion

        #region Constructors
        public ConvertedCurrencies() { }

        public ConvertedCurrencies(int pid , string pCurrency , decimal pConvertedAmount , string pError)
            {
            this._id = pid;
            this._currency = pCurrency;
            this._convertedAmount = pConvertedAmount;
            this._error = pError;
            //this._convertedCurrency = pconvertedCurrency;

            }

        #endregion


        #region Public Properties

        public int Id
            {
            get { return _id; }
            set { _id = value; }

            }

        public string Currency
            {
            get { return _currency; }
            set { _currency = value; }
            }

        public decimal ConvertedAmount
            {
            get { return _convertedAmount; }
            set { _convertedAmount = value; }
            }
        public string Error
            {
            get { return _error; }
            set { _error = value; }
            }


        #endregion


        #region Equals And HashCode Overrides

        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
            {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
            }
        #endregion

        }
    }
