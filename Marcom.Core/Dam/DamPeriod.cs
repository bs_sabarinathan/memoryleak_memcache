using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

	/// <summary>
	/// EntityPeriod object for table 'PM_DAM_EntityPeriod'.
	/// </summary>
	
	internal class DamPeriod : IDamPeriod 
	{
		#region Member Variables

        protected int _id;
		protected int _entityid;
		protected DateTime _startdate;
		protected DateTime _enddate;
		protected string _description;
		protected int _sortorder;
		
		
		#endregion
		
		#region Constructors
		public DamPeriod() {}

        public DamPeriod(int pId, int pEntityid, DateTime pStartdate, DateTime pEndDate, string pDescription, int pSortOrder)
		{
            this._id = pId;
			this._entityid = pEntityid; 
			this._startdate = pStartdate; 
			this._enddate = pEndDate; 
			this._description = pDescription; 
			this._sortorder = pSortOrder; 
		}
		
		#endregion
		
		#region Public Properties
		
        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }
        
		public int Entityid
		{
			get { return _entityid; }
			set { _entityid = value; }
			
		}
		
		public DateTime Startdate
		{
			get { return _startdate; }
			set { _startdate = value; }
			
		}
		
		public DateTime EndDate
		{
			get { return _enddate; }
			set { _enddate = value; }
			
		}
		
		public string Description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");			 
			  _description = value; 
			}
			
		}
		
		public int SortOrder
		{
			get { return _sortorder; }
			set { _sortorder = value; }
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			EntityPeriod castObj = null;
			try
			{
				castObj = (EntityPeriod)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			return this.GetType().FullName.GetHashCode();
				
		}
		#endregion
		
	}
	
}
