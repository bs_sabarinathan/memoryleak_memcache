﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Dal.Base;
using NHibernate;
using NHibernate.Criterion;

namespace BrandSystems.Marcom.Dal.Planning
{
    public class PlanningRepository : GenericRepository
    {
        public PlanningRepository(ISessionFactory sessionFactory) : base(sessionFactory)
        {
        }
        //Define a function get sortorder
        /// <summary>
        /// Deletes an object of a specified type.
        /// </summary>
        /// <param name="itemsToDelete">The items to delete.</param>
        /// <typeparam name="T">The type of objects to delete.</typeparam>
        public int GetMaxSortOrder<T>(string propertyName)
        {
            ISession session = _sessionFactory.GetCurrentSession();
            int max = Convert.ToInt32(session.CreateCriteria(typeof(T)).SetProjection(Projections.Max(propertyName)).UniqueResult());

            return max;
        }
    }
}
