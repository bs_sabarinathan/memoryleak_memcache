﻿using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Metadata;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Configuration;
using BrandSystems.Marcom.Core.AmazonStorageHelper;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core;


namespace Presentation.Controllers
{
    public class XmlSetting
    {
        public static ListSettings ListSettings(string elementNode, int typeid, int TenantID)
        {

            //IListofRecord listofRecord = new ListofRecord();

            //string xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + GetTenantFilePath(TenantID), "AdminSettings.xml");
            //XDocument xDoc = XDocument.Load(xmlpath);
            string xmlpath = "";
            XDocument xDoc;
            string TenantsFilePath = GetTenantFilePath(TenantID);
            Guid userSession = MarcomManagerFactory.GetSystemSession(TenantID);
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, userSession);
            if (marcommanager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
            {
                xmlpath = Path.Combine(TenantsFilePath, "AdminSettings.xml");
                xDoc = AWSHelper.ReadAdminXDocument(marcommanager.User.AwsStorage.S3, marcommanager.User.AwsStorage.BucketName, xmlpath);
            }
            else
            {
                xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], TenantsFilePath + "AdminSettings.xml");
                xDoc = XDocument.Load(xmlpath);
            }


            ListSettings listSettings = new ListSettings();

            if (xDoc.Root.Descendants("ListSettings").Descendants("RootLevel").Where(a => Convert.ToInt32(a.Attribute("typeid").Value) == typeid).Select(a => a).Elements(elementNode).Count() > 0)
            {
                listSettings.Name = elementNode;

                //listSettings.Type = Convert.ToInt32(xDoc.Root.Elements(elementNode).Elements("EntityType").Select(a => a.Attribute("ID").Value).First());

                listSettings.Description = elementNode;


                if (elementNode == "Tree")
                {
                    var resp = xDoc.Root.Descendants("ListSettings").Descendants("RootLevel").Where(a => Convert.ToInt32(a.Attribute("typeid").Value) == typeid).Select(a => a).Descendants(elementNode);
                    var abd = resp.Elements("EntityTypes").Elements("EntityType").Select(a => a.Attribute("ID")).ToList();

                    List<int> entittypeobj = new List<int>();
                    for (int i = 0; i < abd.Count(); i++)
                    {

                        entittypeobj.Add(Convert.ToInt32(abd.ElementAt(i).Value));
                    }
                    if (typeid == 35)
                    {
                        var respfrcal = xDoc.Root.Descendants("ListSettings").Descendants("RootLevel").Where(a => Convert.ToInt32(a.Attribute("typeid").Value) == 6).Select(a => a).Descendants(elementNode);
                        var abdfrcal = respfrcal.Elements("EntityTypes").Elements("EntityType").Select(a => a.Attribute("ID")).ToList();

                        for (int i = 0; i < abdfrcal.Count(); i++)
                        {
                            if (!entittypeobj.Contains(Convert.ToInt32(abdfrcal.ElementAt(i).Value)))
                            {
                                entittypeobj.Add(Convert.ToInt32(abdfrcal.ElementAt(i).Value));
                            }
                        }
                    }

                    listSettings.EntityTypes = entittypeobj;
                }
                else
                {
                    List<int> entittypeobj = new List<int>();
                    entittypeobj.Add(typeid);
                    listSettings.EntityTypes = entittypeobj;
                }


                listSettings.Attributes = xDoc.Root.Descendants("ListSettings").Descendants("RootLevel").Where(a => Convert.ToInt32(a.Attribute("typeid").Value) == typeid).Select(a => a).Descendants(elementNode).Elements("Attributes").Elements("Attribute").Select(a => new AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList();
            }



            return listSettings;
        }

        public static List<AttributeSettings> ListGanttAttributes(string elementNode, int typeid, int TenantID)
        {
            List<AttributeSettings> GanttViewAttributes = new List<AttributeSettings>();
            if (elementNode == "GanttView" || elementNode == "ListView")
            {

                //string xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + GetTenantFilePath(TenantID), "AdminSettings.xml");
                //XDocument xDoc = XDocument.Load(xmlpath);
                string xmlpath = "";
                XDocument xDoc;
                string TenantsFilePath = GetTenantFilePath(TenantID);
                Guid userSession = MarcomManagerFactory.GetSystemSession(TenantID);
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, userSession);
                if (marcommanager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
                {
                    xmlpath = Path.Combine(TenantsFilePath, "AdminSettings.xml");
                    xDoc = AWSHelper.ReadAdminXDocument(marcommanager.User.AwsStorage.S3, marcommanager.User.AwsStorage.BucketName, xmlpath);
                }
                else
                {
                    xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], TenantsFilePath + "AdminSettings.xml");
                    xDoc = XDocument.Load(xmlpath);
                }


                List<AttributeSettings> ListViewAttributes = new List<AttributeSettings>();

                GanttViewAttributes = xDoc.Root.Descendants("ListSettings").Descendants("RootLevel").Where(a => Convert.ToInt32(a.Attribute("typeid").Value) == typeid).Select(a => a).Descendants(elementNode).Elements("Attributes").FirstOrDefault().Elements("Attribute").Select(a => new AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList();

                var elementlistNode = "ListView";

                XDocument xganttDoc = XDocument.Load(xmlpath);

                if (xDoc.Root.Descendants("ListSettings").Descendants("RootLevel").Where(a => Convert.ToInt32(a.Attribute("typeid").Value) == typeid).Select(a => a).Elements(elementNode).Count() > 0)
                {
                    GanttViewAttributes.AddRange(xDoc.Root.Descendants("ListSettings").Descendants("RootLevel").Where(a => Convert.ToInt32(a.Attribute("typeid").Value) == typeid).Select(a => a).Elements(elementNode).Elements("Attributes").FirstOrDefault().Elements("Attribute").Select(a => new AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList());
                }

                var qry = from m in GanttViewAttributes
                          group m by new { m.Id, m.IsOrderBy, m.IsSelect, m.IsFilter, m.Level, m.Type, m.WhereCondition, m.Field, m.DisplayName } into grp
                          where grp.Count() > 1
                          select grp.Key;

                foreach (var item in qry)
                {
                    var duplicateitem = GanttViewAttributes.Where(a => a.Id == Convert.ToInt32(item.Id) && a.Field == item.Field && a.Level == item.Level && a.DisplayName == item.DisplayName).Select(a => a).First();

                    GanttViewAttributes.Remove(duplicateitem);
                }

                //if (GanttViewAttributes.Count > ListViewAttributes.Count)
                //{
                //    listSettings.Attributes = GanttViewAttributes.Except(ListViewAttributes).ToList();
                //}
                //else
                //{
                //    listSettings.Attributes = ListViewAttributes.Except(GanttViewAttributes).ToList();
                //}                   


            }
            return GanttViewAttributes;

        }

        public static List<AttributeSettings> ListGanttAttributes(int typeid, int TenantID)
        {
            List<AttributeSettings> GanttViewAttributes = new List<AttributeSettings>();
            //if (elementNode == "GanttView" || elementNode == "ListView")
            //{

            //string xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + GetTenantFilePath(TenantID), "AdminSettings.xml");
            //XDocument xDoc = XDocument.Load(xmlpath);
            string xmlpath = "";
            XDocument xDoc;
            string TenantsFilePath = GetTenantFilePath(TenantID);
            Guid userSession = MarcomManagerFactory.GetSystemSession(TenantID);
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, userSession);
            if (marcommanager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
            {
                xmlpath = Path.Combine(TenantsFilePath, "AdminSettings.xml");
                xDoc = AWSHelper.ReadAdminXDocument(marcommanager.User.AwsStorage.S3, marcommanager.User.AwsStorage.BucketName, xmlpath);
            }
            else
            {
                xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], TenantsFilePath + "AdminSettings.xml");
                xDoc = XDocument.Load(xmlpath);
            }


            List<AttributeSettings> ListViewAttributes = new List<AttributeSettings>();

            GanttViewAttributes = xDoc.Root.Descendants("ListSettings").Descendants("RootLevel").Where(a => Convert.ToInt32(a.Attribute("typeid").Value) == typeid).Select(a => a).Descendants("GanttView").Elements("Attributes").FirstOrDefault().Elements("Attribute").Select(a => new AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList();
            ListViewAttributes = xDoc.Root.Descendants("ListSettings").Descendants("RootLevel").Where(a => Convert.ToInt32(a.Attribute("typeid").Value) == typeid).Select(a => a).Descendants("ListView").Elements("Attributes").FirstOrDefault().Elements("Attribute").Select(a => new AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList();



            //Add ListB records to ListC
            foreach (var person in GanttViewAttributes)
            {
                //Check if record already exists in ListC
                if (person.Type == (int)AttributesList.DropDownTree || person.Type == (int)AttributesList.TreeMultiSelection)
                {
                    if (ListViewAttributes.Where(a => a.Id == person.Id && a.Level == person.Level).Count() == 0)
                    {
                        ListViewAttributes.Add(person);
                    }
                }
                else
                {
                    if (ListViewAttributes.Where(a => a.Id == person.Id).Count() == 0)
                    {
                        ListViewAttributes.Add(person);
                    }
                }
            }


            return ListViewAttributes;

        }

        public static ListSettings MultiRootLevelSettings(string elementNode, int typeid, List<int> EntityTypeids, int TenantID)
        {

            //IListofRecord listofRecord = new ListofRecord();

            //string xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + GetTenantFilePath(TenantID), "AdminSettings.xml");
            //XDocument xDoc = XDocument.Load(xmlpath);
            string xmlpath = "";
            XDocument xDoc;
            string TenantsFilePath = GetTenantFilePath(TenantID);
            Guid userSession = MarcomManagerFactory.GetSystemSession(TenantID);
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, userSession);
            if (marcommanager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
            {
                xmlpath = Path.Combine(TenantsFilePath, "AdminSettings.xml");
                xDoc = AWSHelper.ReadAdminXDocument(marcommanager.User.AwsStorage.S3, marcommanager.User.AwsStorage.BucketName, xmlpath);
            }
            else
            {
                xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], TenantsFilePath + "AdminSettings.xml");
                xDoc = XDocument.Load(xmlpath);
            }

            ListSettings listSettings = new ListSettings();
            if (xDoc.Root.Descendants("ListSettings").Descendants("RootLevel").Where(a => Convert.ToInt32(a.Attribute("typeid").Value) == typeid).Select(a => a).Elements(elementNode).Count() > 0)
            {
                listSettings.Name = elementNode;
                listSettings.Description = elementNode;
                listSettings.EntityTypes = EntityTypeids;
                //for (int i = 0; i < EntityTypeids.Count(); i++)
                //{
                //    listSettings.EntityTypes.Add(EntityTypeids[i]);
                //}
                listSettings.Attributes = xDoc.Root.Descendants("ListSettings").Descendants("RootLevel").Where(a => Convert.ToInt32(a.Attribute("typeid").Value) == typeid).Select(a => a).Descendants(elementNode).Elements("Attributes").Elements("Attribute").Select(a => new AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList();
            }
            return listSettings;
        }

        public static ListSettings MyWorkSpaceSettingsread(string EntityTypeelementNode, string WorkspaceelementNode, int typeid, int TenantID)
        {

            //IListofRecord listofRecord = new ListofRecord();

            //string xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + GetTenantFilePath(TenantID), "AdminSettings.xml");
            //XDocument xDoc = XDocument.Load(xmlpath);string xmlpath = "";
            string xmlpath = "";
            XDocument xDoc;
            string TenantsFilePath = GetTenantFilePath(TenantID);
            Guid userSession = MarcomManagerFactory.GetSystemSession(TenantID);
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, userSession);
            if (marcommanager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
            {
                xmlpath = Path.Combine(TenantsFilePath, "AdminSettings.xml");
                xDoc = AWSHelper.ReadAdminXDocument(marcommanager.User.AwsStorage.S3, marcommanager.User.AwsStorage.BucketName, xmlpath);
            }
            else
            {
                xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], TenantsFilePath + "AdminSettings.xml");
                xDoc = XDocument.Load(xmlpath);
            }

            ListSettings listSettings = new ListSettings();


            if (EntityTypeelementNode == "Tree")
            {
                var resp = xDoc.Root.Descendants("ListSettings").Descendants("RootLevel").Where(a => Convert.ToInt32(a.Attribute("typeid").Value) == typeid).Select(a => a).Descendants(EntityTypeelementNode);
                var abd = resp.Elements("EntityTypes").Elements("EntityType").Select(a => a.Attribute("ID")).ToList();
                List<int> entittypeobj = new List<int>();
                for (int i = 0; i < abd.Count(); i++)
                {
                    entittypeobj.Add(Convert.ToInt32(abd.ElementAt(i).Value));
                }
                listSettings.EntityTypes = entittypeobj;
            }

            listSettings.Attributes = xDoc.Root.Descendants(WorkspaceelementNode).Elements("Attributes").Elements("Attribute").Select(a => new AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList();
            return listSettings;
        }

        public static ListSettings ReportListSettings(string reportname, string LogoSettings, string elemntnode, int typeid, List<int> EntityTypeids, List<string> Attributids, int TenantID)
        {

            //IListofRecord listofRecord = new ListofRecord();

            //string xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + GetTenantFilePath(TenantID), "AdminSettings.xml");
            //XDocument xDoc = XDocument.Load(xmlpath);
            string xmlpath = "";
            XDocument xDoc;
            string TenantsFilePath = GetTenantFilePath(TenantID);
            Guid userSession = MarcomManagerFactory.GetSystemSession(TenantID);
            IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, userSession);
            if (marcommanager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
            {
                xmlpath = Path.Combine(TenantsFilePath, "AdminSettings.xml");
                xDoc = AWSHelper.ReadAdminXDocument(marcommanager.User.AwsStorage.S3, marcommanager.User.AwsStorage.BucketName, xmlpath);
            }
            else
            {
                xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], TenantsFilePath + "AdminSettings.xml");
                xDoc = XDocument.Load(xmlpath);
            }


            ListSettings listSettings = new ListSettings();

            if (xDoc.Root.Descendants("ListSettings").Descendants("RootLevel").Where(a => Convert.ToInt32(a.Attribute("typeid").Value) == typeid).Select(a => a).Elements("Tree").Count() > 0)
            {
                listSettings.Name = reportname;

                //listSettings.Type = Convert.ToInt32(xDoc.Root.Elements(elementNode).Elements("EntityType").Select(a => a.Attribute("ID").Value).First());

                listSettings.Description = reportname;
                if (EntityTypeids.Count > 0)
                {

                    listSettings.EntityTypes = EntityTypeids;
                }

                else if (typeid > 0)
                {
                    var resp = xDoc.Root.Descendants("ListSettings").Descendants("RootLevel").Where(a => Convert.ToInt32(a.Attribute("typeid").Value) == typeid).Select(a => a).Descendants("Tree");
                    var abd = resp.Elements("EntityTypes").Elements("EntityType").Select(a => a.Attribute("ID")).ToList();
                    List<int> entittypeobj = new List<int>();
                    for (int i = 0; i < abd.Count(); i++)
                    {
                        entittypeobj.Add(Convert.ToInt32(abd.ElementAt(i).Value));
                    }
                    listSettings.EntityTypes = entittypeobj;
                }
                else
                {
                    List<int> entittypeobj = new List<int>();
                    entittypeobj.Add(typeid);
                    listSettings.EntityTypes = entittypeobj;
                }


                if (Attributids.Count > 0)
                {
                    List<AttributeSettings> GanttReportAttributes = new List<AttributeSettings>();
                    foreach (string element in Attributids)
                    {
                        string attrField = (string)element;
                        GanttReportAttributes.AddRange(xDoc.Root.Descendants(LogoSettings).Descendants(elemntnode).Elements("Attributes").Elements("Attribute").Where(a => Convert.ToString(a.Element("Field").Value) == attrField).Select(a => new AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList());
                        // xDoc.Root.Descendants(LogoSettings).Descendants(elemntnode).Elements("Attributes").Elements("Attribute").Where(a => Convert.ToInt32(a.Element("Id").Value) == attrid).Select(a => new AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList();
                    }
                    if (GanttReportAttributes.Count > 0)
                    {
                        listSettings.Attributes = GanttReportAttributes;
                    }
                }
                else
                {
                    listSettings.Attributes = xDoc.Root.Descendants(LogoSettings).Descendants(elemntnode).Elements("Attributes").Elements("Attribute").Select(a => new AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList();
                }

            }



            return listSettings;
        }

        public static List<AttributeSettings> GanttReportAttributes(string elementNode, List<string> Attributids, int TenantID)
        {
            List<AttributeSettings> GanttReportAttributes = new List<AttributeSettings>();
            if (elementNode == "GanttViewReport")
            {

                //string xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + GetTenantFilePath(TenantID), "AdminSettings.xml");
                //XDocument xDoc = XDocument.Load(xmlpath);

                string xmlpath = "";
                XDocument xDoc;
                string TenantsFilePath = GetTenantFilePath(TenantID);
                Guid userSession = MarcomManagerFactory.GetSystemSession(TenantID);
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, userSession);
                if (marcommanager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
                {
                    xmlpath = Path.Combine(TenantsFilePath, "AdminSettings.xml");
                    xDoc = AWSHelper.ReadAdminXDocument(marcommanager.User.AwsStorage.S3, marcommanager.User.AwsStorage.BucketName, xmlpath);
                }
                else
                {
                    xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], TenantsFilePath + "AdminSettings.xml");
                    xDoc = XDocument.Load(xmlpath);
                }

                if (Attributids.Count > 0)
                {
                    // List<AttributeSettings> GanttReportAttributesnew = new List<AttributeSettings>();
                    foreach (string element in Attributids)
                    {
                        string attrField = (string)element;
                        GanttReportAttributes.AddRange(xDoc.Root.Descendants("ReportSettings").Select(a => a).Descendants(elementNode).Elements("Attributes").FirstOrDefault().Elements("Attribute").Where(a => Convert.ToString(a.Element("Field").Value) == attrField).Select(a => new AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList());
                        // xDoc.Root.Descendants(LogoSettings).Descendants(elemntnode).Elements("Attributes").Elements("Attribute").Where(a => Convert.ToInt32(a.Element("Id").Value) == attrid).Select(a => new AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList();
                    }

                }
                else
                {
                    GanttReportAttributes = xDoc.Root.Descendants("ReportSettings").Select(a => a).Descendants(elementNode).Elements("Attributes").FirstOrDefault().Elements("Attribute").Select(a => new AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList();
                }



                //var elementlistNode = "ListView";

                //XDocument xganttDoc = XDocument.Load(xmlpath);
                if (Attributids.Count > 0)
                {
                    // List<AttributeSettings> GanttReportAttributesnew = new List<AttributeSettings>();
                    foreach (string element in Attributids)
                    {
                        string attrField = (string)element;
                        GanttReportAttributes.AddRange(xDoc.Root.Descendants("ReportSettings").Select(a => a).Elements(elementNode).Elements("Attributes").FirstOrDefault().Elements("Attribute").Where(a => Convert.ToString(a.Element("Field").Value) == attrField).Select(a => new AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList());
                        // xDoc.Root.Descendants(LogoSettings).Descendants(elemntnode).Elements("Attributes").Elements("Attribute").Where(a => Convert.ToInt32(a.Element("Id").Value) == attrid).Select(a => new AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList();
                    }

                }

                else if (xDoc.Root.Descendants("ReportSettings").Select(a => a).Elements(elementNode).Count() > 0)
                {
                    GanttReportAttributes.AddRange(xDoc.Root.Descendants("ReportSettings").Select(a => a).Elements(elementNode).Elements("Attributes").FirstOrDefault().Elements("Attribute").Select(a => new AttributeSettings { Id = Convert.ToInt32(a.Element("Id").Value), IsOrderBy = Convert.ToBoolean(a.Element("IsOrderBy").Value), IsSelect = Convert.ToBoolean(a.Element("IsSelect").Value), IsFilter = Convert.ToBoolean(a.Element("IsFilter").Value), Level = Convert.ToInt32(a.Element("Level").Value), Type = Convert.ToInt32(a.Element("Type").Value), WhereCondition = Convert.ToString(a.Element("WhereCondition").Value), Field = Convert.ToString(a.Element("Field").Value), IsSpecial = Convert.ToBoolean(a.Element("IsSpecial").Value), DisplayName = Convert.ToString(a.Element("DisplayName").Value) }).ToList());
                }

                var qry = from m in GanttReportAttributes
                          group m by new { m.Id, m.IsOrderBy, m.IsSelect, m.IsFilter, m.Level, m.Type, m.WhereCondition, m.Field, m.DisplayName } into grp
                          where grp.Count() > 1
                          select grp.Key;

                foreach (var item in qry)
                {
                    var duplicateitem = GanttReportAttributes.Where(a => a.Id == Convert.ToInt32(item.Id) && a.Field == item.Field && a.Level == item.Level && a.DisplayName == item.DisplayName).Select(a => a).First();

                    GanttReportAttributes.Remove(duplicateitem);
                }




            }
            return GanttReportAttributes;

        }

        public static bool TextFileWriter(StringBuilder strLogText)
        {

            try
            {

                //string path = getLatestFileVersion();


                string path = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "treelog.txt";

                if (!System.IO.File.Exists(path))
                {
                    System.IO.File.Create(path);

                }
                using (StreamWriter log = System.IO.File.AppendText(path))
                {
                    log.WriteLine(strLogText.ToString());
                    log.WriteLine();

                    log.Close();
                }
            }
            catch
            {

            }
            return false;
        }

        public static string GetTenantFilePath(int TenantID)
        {
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            XmlDocument xdcDocument = new XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            XmlElement xelRoot = xdcDocument.DocumentElement;
            XmlNodeList xnlNodes = xelRoot.SelectNodes("/Tenants/Tenant[@ID=" + TenantID + "]");
            return xnlNodes.Item(0).ChildNodes[3].InnerText;
        }

    }
}