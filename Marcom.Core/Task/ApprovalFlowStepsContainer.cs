﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BrandSystems.Marcom.Core.Task.Interface;
using BrandSystems.Marcom.Dal.Task.Model;

namespace BrandSystems.Marcom.Core.Task
{
    internal class ApprovalFlowStepsContainer
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int ID { get; set; }
        public int SortOrder { get; set; }
        public List<ApprovalFlowSteps> StepsData { get; set; }
    }
    internal class ApprovalFlowSteps
    {
        public int PhaseId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Roles { get; set; }
        public int Duration { get; set; }
        public int SortOrder { get; set; }
        public int MinApproval { get; set; }
        public bool IsMandatory { get; set; }
        public int ID { get; set; }
        public bool isopened { get; set; }
        public string RoleName { get; set; }
        public List<TaskMembers> MemberList { get; set; }
    }
}
