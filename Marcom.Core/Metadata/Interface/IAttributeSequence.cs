﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface IAttributeSequence
    {
        int Id { get; set; }
        int AttributeID { get; set; }
        int StartWithValue { get; set; }
        string Pattern { get; set; }
        int CurrentValue { get; set; }
    }
}
