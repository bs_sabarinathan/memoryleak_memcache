﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Access.Interface
{
    public interface IPublishAccess
    {
        #region Public Properties

        int ID
        {
            get;
            set;

        }

        int Role
        {
            get;
            set;

        }

        bool AccessPermission
        {
            get;
            set;

        }
        #endregion
    }
}
