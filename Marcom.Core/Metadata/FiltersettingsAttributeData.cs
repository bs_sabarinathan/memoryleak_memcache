﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Metadata;
using BrandSystems.Marcom.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
  internal class FiltersettingsAttributes : IFiltersettingsAttributes
    {
        public int EntityTypeId { get; set; }
        public int AttributeId { get; set; }
        public string DisplayName { get; set; }
        public int AttributeTypeId { get; set; }
        public int TreeLevel { get; set; }
        public bool IsSpecial { get; set; }
        public IList<IOption> OptionValues { get; set; }
        public IList<User.Interface.IUser> Users { get; set; }
        public IList<User.Interface.IUser> Member { get; set; }
        public int MemberId { get; set; }
        public IList<ITreeNode> LevelTreeNodes { get; set; }
        public IList<IEntityTypeStatusOptionsForFilter> EntityStatusOptionValues { get; set; }
        public IList<DropDownTreePricing> DropdowntreePricingAttr { get; set; } 
        public string tree { get; set; }
        //public IList<IEntityTypeStatusOptions> EntityStatusOptionValues { get; set; }
    }
}
