﻿(function (ng, app) {
    "use strict"; function muiadminassetcreationCtrl($scope, $resource, $location, AdminService, $translate) {
        var availableattrs = []; var allEntityTypes = []; var allAttributes = []; $scope.EntityTypeData = []; $scope.entitydatavalues = []; $scope.attributedatavalues = []; $scope.entitytype = 0; $scope.Damentitytype = {}; var AdminLogoSettings = 'AssetCreation'; var ListView = 'AssetType'; AdminService.GetEntityType(5).then(function (entitytypes) {
            $scope.Damentitytype = entitytypes.Response; $scope.entitytype = $scope.Damentitytype[0].Id; for (var i = 0; i < $scope.Damentitytype.length; i++) { var el = $scope.Damentitytype[i]; $scope.EntityTypeListdata.push({ "id": el.Id, "text": el.Caption, "ShortDescription": el.ShortDescription, "ColorCode": el.ColorCode, "Caption": el.Caption }); }
            $scope.loadattributes($scope.entitytype);
        }); $("#sortableAttribute2").droppable({ drop: function (event, ui) { var dropeedid = ui.helper[0].attributes["data-id"].value; var attrDelObj = $.grep(availableattrs, function (e) { return e.Id == dropeedid; }); availableattrs.splice($.inArray(attrDelObj[0], availableattrs), 1); } }); $("#sortableAttribute1").droppable({ drop: function (event, ui) { var dropeedid = ui.helper[0].attributes["data-id"].value; var attrAddObj = $.grep(allAttributes, function (e) { return e.Id == dropeedid; })[0]; availableattrs.push(attrAddObj); } }); $scope.loadattributes = function (entitytype) {
            availableattrs = []; $('#sortableAttribute2').html(''); $('#sortableAttribute1').html(''); if ($scope.entitytype == 0) { return true; }
            if (entitytype != "" && entitytype != null) {
                AdminService.GetAttributeDAMCreate(parseInt(entitytype)).then(function (getattributesfromdb) {
                    allAttributes = getattributesfromdb.Response; AdminService.DAMGetAdminSettings(AdminLogoSettings, ListView, parseInt(entitytype)).then(function (getattribtues) {
                        if (getattribtues.Response != null && getattribtues.Response != "") {
                            var getattribtues = JSON.parse(getattribtues.Response); if (getattribtues.AssetType.Attributes != null) {
                                $scope.attributedatavalues = getattribtues.AssetType.Attributes.Attribute; var attribtueslength = getattribtues.AssetType.Attributes.Attribute.length; var Attrhtml2 = ''; if (attribtueslength != undefined) {
                                    for (var b = 0; b < attribtueslength; b++) {
                                        var attravailablility = $.grep(allAttributes, function (e) { return e.Id == parseInt(getattribtues.AssetType.Attributes.Attribute[b].Id); }); if (attravailablility.length > 0)
                                            if (getattribtues.AssetType.Attributes.Attribute[b].Id != "3") { Attrhtml2 += '<li data-Id="' + getattribtues.AssetType.Attributes.Attribute[b].Id + '" data-DisplayName="' + getattribtues.AssetType.Attributes.Attribute[b].DisplayName + '" class="active DragableItem"><a href="JavaScript: void(0);">' + getattribtues.AssetType.Attributes.Attribute[b].DisplayName + '</a></li>'; }
                                    }
                                    $('#sortableAttribute2').html(Attrhtml2);
                                }
                                else {
                                    var attravailablility = $.grep(allAttributes, function (e) { return e.Id == parseInt(getattribtues.AssetType.Attributes.Attribute.Id); }); if (attravailablility.length > 0)
                                        if (getattribtues.AssetType.Attributes.Attribute.Id != "3") { Attrhtml2 += '<li data-Id="' + getattribtues.AssetType.Attributes.Attribute.Id + '" data-DisplayName="' + getattribtues.AssetType.Attributes.Attribute.DisplayName + '" class="active DragableItem"><a href="JavaScript: void(0);">' + getattribtues.AssetType.Attributes.Attribute.DisplayName + '</a></li>'; $('#sortableAttribute2').html(Attrhtml2); }
                                }
                                var Attrhtml = ''; for (var i = 0; i < allAttributes.length; i++) {
                                    var AttrValues = $.grep($scope.attributedatavalues, function (e) { return e.Id == allAttributes[i].Id; }); if (AttrValues.length == 0) {
                                        if ($scope.attributedatavalues.length == undefined) {
                                            if ($scope.attributedatavalues.Id == allAttributes[i].Id) { }
                                            else { Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>'; availableattrs.push(allAttributes[i]); }
                                        }
                                        else { Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>'; availableattrs.push(allAttributes[i]); }
                                    }
                                }
                                $('#sortableAttribute1').html(Attrhtml);
                            }
                            else { $('#sortableAttribute2').html(Attrhtml2); var Attrhtml = ''; for (var i = 0; i < allAttributes.length; i++) { Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>'; $('#sortableAttribute1').html(Attrhtml); } }
                        }
                        else { $('#sortableAttribute2').html(''); var Attrhtml = ''; for (var i = 0; i < allAttributes.length; i++) { Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>'; $('#sortableAttribute1').html(Attrhtml); } }
                    });
                });
            }
        }
        $(function () { $("#sortableAttribute1, #sortableAttribute2").sortable({ connectWith: ".connectedAttributeSortable", appendTo: 'body', containment: 'window', scroll: false, helper: 'clone' }).disableSelection(); }); $scope.formatResult = function (item) { var markup = '<table class="user-result">'; markup += '<tbody>'; markup += '<tr>'; markup += '<td class="user-image">'; markup += '<span class="eicon" style="vertical-align: middle; margin-top: -4px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>'; markup += '</td>'; markup += '<td class="user-info">'; markup += '<div class="user-title">' + item.Caption + '</div>'; markup += '</td>'; markup += '</tr>'; markup += '</tbody>'; markup += '</table>'; return markup; }; $scope.formatSelection = function (item) { var markup = '<table class="user-result">'; markup += '<tbody>'; markup += '<tr>'; markup += '<td class="user-image">'; markup += '<span class="eicon" style="vertical-align: middle; margin-top: -5px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>'; markup += '</td>'; markup += '<td class="user-info">'; markup += '<div class="user-title">' + item.Caption + '</div>'; markup += '</td>'; markup += '</tr>'; markup += '</tbody>'; markup += '</table>'; return markup; }; $scope.entitytype = ''; $scope.EntityTypeListdata = []; $scope.tagAllOptionsEntitytypelist = { multiple: false, allowClear: true, data: $scope.EntityTypeListdata, formatResult: $scope.formatResult, formatSelection: $scope.formatSelection, dropdownCssClass: "bigdrop", escapeMarkup: function (m) { return m; } }; $scope.save = function () {
            var entitytype = 0; if ($scope.entitytype != undefined) {
                if ($scope.entitytype.id != undefined)
                    entitytype = $scope.entitytype.id; else
                    entitytype = $scope.entitytype;
            }
            var DamAdminData = {}; var DamAdminID = {}; DamAdminID.LogoSettings = AdminLogoSettings; DamAdminID.Key = ListView; DamAdminID.TypeId = entitytype; DamAdminData.AssetType = { "Attributes": [], }; var AttributeArray = { "Attribute": [] }; for (var j = 0; j < $("#sortableAttribute2").find('li').length; j++) { var SortOrder = j + 1; var Attribute = { "Id": $("#sortableAttribute2").find('li').eq(j).attr("data-Id"), "SortOrder": SortOrder, "DisplayName": $("#sortableAttribute2").find('li').eq(j).attr("data-DisplayName"), }; AttributeArray.Attribute.push(Attribute) }
            DamAdminData.AssetType.Attributes.push(AttributeArray); AdminService.DAMadminSettingsforRootLevelInsertUpdate(DamAdminData, DamAdminID).then(function (result) { NotifySuccess($translate.instant('LanguageContents.Res_4802.Caption')); });
        }
    }
    app.controller("mui.admin.assetcreationCtrl", ['$scope', '$resource', '$location', 'AdminService', '$translate', muiadminassetcreationCtrl]);
})(angular, app);