using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
    /// FeedTemplateDao object for table 'CM_FeedFilter_Group'.
	/// </summary>
	
	public partial class FeedFilterGroupDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
	    protected string _feedgroup;
		protected string _template;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
        public FeedFilterGroupDao() { }
			
		public FeedFilterGroupDao(string pTemplate,string pFeedGroup)
		{
			this._template = pTemplate; 
            this._feedgroup = pFeedGroup; 
		}
				
		public FeedFilterGroupDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		
		public virtual string Template
		{
			get { return _template; }
			set 
			{
			  _bIsChanged |= (_template != value); 
			  _template = value; 
			}
			
		}

          	
		public virtual string FeedGroup
		{
			get { return _feedgroup; }
			set 
			{
			  _bIsChanged |= (_feedgroup != value); 
			  _feedgroup = value; 
			}
			
		}
        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 			
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string FeedFilterGroupDao = "FeedFilterGroupDao";			
			public const string Id = "Id";			
			public const string Template = "Template";			
            public const string FeedGroup = "FeedGroup";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string FeedFilterGroupDao = "FeedFilterGroupDao";			
			public const string Id = "Id";			
			public const string Template = "Template";	
            public const string FeedGroup = "FeedGroup";			
		
		}

		#endregion
		
		
	}
	
}
