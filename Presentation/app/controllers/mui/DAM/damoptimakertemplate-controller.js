﻿(function (ng, app) {
    "use strict";

    function muiDAMdamoptimakertemplateCtrl($window, $location, $timeout, $scope, $cookies, $resource, $stateParams, DamOptimakerTemplateService, $compile, $modalInstance, params) {
        var timerObj = {};
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath;
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            imagesrcpath = cloudpath;
        }
        $scope.PageNo = 1;
        $scope.currentTreeHolderId = "optiMakerTreeHoldersuggested";
        $scope.OptimakerSettings = [];
        $scope.Urls = {
            OptimakerBaseURL: "",
            OptimakerBaseDocURL: "",
            TemplatePath: ""
        };
        $scope.TaskStatusObj = [{
            "Name": "Thumbnail",
            ID: 1
        }, {
            "Name": "Summary",
            ID: 2
        }, {
            "Name": "List",
            ID: 3
        }];
        $scope.SettingsDamAttributes = {};
        $scope.OrderbyObj = [{
            "Name": "Name (Ascending)",
            ID: 1
        }, {
            "Name": "Name (Descending)",
            ID: 2
        }, {
            "Name": "Creation date (Ascending)",
            ID: 3
        }, {
            "Name": "Creation date (Descending)",
            ID: 4
        }];
        $scope.FilterStatus = 1;
        $scope.DamViewName = "Thumbnail";
        $scope.OrderbyName = "Creation date (Descending)";
        $scope.callbackid = 0;
        $scope.OrderBy = 4;
        DamOptimakerTemplateService.GetEntityType(5).then(function (assetTypes) {
            $scope.Damentitytype = assetTypes.Response;
        });
        var apple_selected, tree;
        $scope.my_tree_handler = function (branch) {
            timerObj.getassetdata = $timeout(function () {
                GetAssetsData(branch.id, $scope.PageNo);
            }, 100);
        };
        $scope.FilterByOptionChange = function (id) {
            if (id != $scope.FilterStatus) {
                $scope.PageNo = 1;
                $scope.FilterStatus = id;
                var res = $.grep($scope.TaskStatusObj, function (e) {
                    return e.ID == id;
                });
                $scope.DamViewName = res[0].Name;
                var b;
                b = tree.get_selected_branch();
                GetAssetsData(b.id, $scope.PageNo);
            }
        }
        $scope.OrderbyOption = function (orderid) {
            var branch;
            branch = tree.get_selected_branch();
            if (orderid != $scope.OrderBy) {
                $scope.PageNo = 1;
                $scope.OrderBy = orderid;
                var orderobj = $.grep($scope.OrderbyObj, function (e) {
                    return e.ID == orderid;
                });
                $scope.OrderbyName = orderobj[0].Name;
                var res = $.grep($scope.TaskStatusObj, function (e) {
                    return e.ID == $scope.FilterStatus;
                });
                GetAssetsData(branch.id, $scope.PageNo);
            }
        }
        $scope.my_data = [];
        $scope.my_tree = tree = {};

        function ShowOptimakerTemplates(id) {
            $('#SectionTabs a[href="#Suggested"]').tab('show');
            $scope.AssetSelectionFiles = [];
            PageLoad();
        }

        function PageLoad() {
            if (tree.length > 0) {
                tree.splice(0, tree.length);
            }
            $scope.my_tree = tree;
            if ($scope.my_data.length > 0) {
                $scope.my_data.splice(0, $scope.my_data.length);
            }
            DamOptimakerTemplateService.GetOptimakerSettingsBaseURL().then(function (data) {
                var urls = data.Response.split(",");
                $scope.Urls.OptimakerBaseURL = urls[0];
                $scope.Urls.OptimakerBaseDocURL = urls[1];
                $scope.Urls.TemplatePath = urls[2];
                $scope.try_async_load();
            });
        }
        $scope.SetCurrentTabID = function (treeholderid) {
            $scope.currentTreeHolderId = treeholderid;
            $scope.try_async_load();
        }
        $scope.try_async_load = function () {
            $scope.doing_async = true;
            return timerObj.getcategory = $timeout(function () {
                DamOptimakerTemplateService.GetCategoryTreeCollection().then(function (data) {
                    var res = [];
                    res = JSON.parse(data.Response);
                    $("#optimakerTemplatePopup").modal("show");
                    var Children = [];
                    $scope.my_data = res[0].Children;
                    $scope.initialTreeSelect = res[0].Children[0].Caption;
                    var treefolder = '<dam-optimaker-tree tree-data="my_data" tree-control="my_tree" on-select="my_tree_handler(branch)" expand-level="0" initial-selection="initialTreeSelect"></dam-optimaker-tree>';
                    $("#" + $scope.currentTreeHolderId.toString() + "").html($compile(treefolder)($scope));
                    $scope.doing_async = false;
                    return tree.expand_all();
                });
            }, 10);
        };

        function GetAssetsData(categoryID, pageno) {
            DamOptimakerTemplateService.GetAllOptimakerSettings(categoryID).then(function (data) {
                var res = [];
                $scope.OptimakerSettings = data.Response;
                loadSettings();
            });
        }

        function loadSettings() {
            var html = "";
            var ccode = "",
                shortdesc = "";
            if ($scope.currentTreeHolderId == "optiMakerTreeHoldersuggested") $("#DetailsSuggested").html($compile(html)($scope));
            else $("#DetailsLibrary").html($compile(html)($scope));
            if ($scope.OptimakerSettings.length > 0) {
                for (var i = 0, data; data = $scope.OptimakerSettings[i++];) {
                    shortdesc = $.grep($scope.Damentitytype, function (e) {
                        return e.Id == data.AssetType
                    })[0].ShortDescription;
                    ccode = $.grep($scope.Damentitytype, function (e) {
                        return e.Id == data.AssetType
                    })[0].ColorCode;
                    html += ' <div class="omt-Box"> ';
                    html += ' <div id="" class="omt-Selection"> ';
                    html += ' <div class="omt-chkBx displayNone"> ';
                    html += ' <i class="icon-stop"></i> ';
                    html += ' <label class="checkbox checkbox-custom"> ';
                    html += ' <input id="chkBox1" type="checkbox"/> ';
                    html += ' <i id="chkBoxIcon1" class="checkbox"></i> ';
                    html += ' </label> ';
                    html += ' </div> ';
                    html += ' <div class="omt-Block" my-qtip2 qtip-content="' + data.Description + '"> ';
                    html += ' <div class="omt-ImgBlock"> ';
                    html += ' <div class="omt-ImgContainer"> ';
                    html += ' <img src="' + imagesrcpath + 'DAMFiles/Templates/MediaGenerator/Images/ThumbNail/' + data.PreviewImage + '?' + generateUniqueTracker() + '" alt="Image" onerror="this.onerror=null;this.src=\'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'"/>';
                    html += ' </div> ';
                    html += ' </div> ';
                    html += ' <div class="omt-DetailBlock"> ';
                    html += ' <div class="omt-detail-eIconContainer"> ';
                    html += ' <span class="omt-eIcon" style="background-color: #' + ccode + ';">' + shortdesc + '</span> ';
                    html += ' </div> ';
                    html += ' <div class="omt-Details"> ';
                    html += ' <span class="omt-infoMain">' + data.Name + '</span> ';
                    html += ' <span class="omt-infoSub">' + data.Description + '</span> ';
                    html += ' <span class="omt-infoSub2"></span> ';
                    html += ' <span class="omt-infoDesc"></span> ';
                    html += ' </div> ';
                    html += ' </div> ';
                    html += ' </div> ';
                    html += ' <div class="omt-HoverPanel"> ';
                    html += ' <span> ';
                    html += ' <button ng-click="openIframe(' + data.ID + ',' + data.TemplateType + ')" class="btn btn-primary">USE</button> ';
                    html += ' </span> ';
                    html += ' </div> ';
                    html += ' </div> ';
                    html += ' </div> ';
                }
                if ($scope.currentTreeHolderId == "optiMakerTreeHoldersuggested") $("#DetailsSuggested").html($compile(html)($scope));
                else $("#DetailsLibrary").html($compile(html)($scope));
            } else {
                if ($scope.currentTreeHolderId == "optiMakerTreeHoldersuggested") {
                    var emptyHtml = '<div class="emptyFolder">There is no settings in this category.</div>';
                    $("#DetailsSuggested").html(emptyHtml);
                } else {
                    var emptyHtml = '<div class="emptyFolder">There is no settings in this category.</div>';
                    $("#DetailsLibrary").html(emptyHtml);
                }
            }
        }
        $scope.iframeurl = "";
        $scope.isTemplateWord = false;
        $scope.openIframe = function (id, templateType) {
            $('#optimakeriframe').attr('src', '');
            $scope.TemplateAsset.templateType = templateType;
            $('#loadIframeImg').css('display', 'block');
            $scope.isTemplateWord = false;
            var OptimakerRecord = {};
            OptimakerRecord = $.grep($scope.OptimakerSettings, function (e) {
                return e.ID == id && e.TemplateType == templateType;
            })[0];
            $scope.TemplateAsset.serverUrl = OptimakerRecord.ServerURL;
            $scope.selectedTemplateType.type = OptimakerRecord.Name;
            $scope.SetOptmakerObj($stateParams.ID, $scope.selectedTree.id, OptimakerRecord.DocType, $cookies['UserId'], OptimakerRecord.TemplateType != null ? false : true);
            var globalObject = new setGlobalOptimakerSettings($stateParams.ID, $scope.selectedTree.id == null ? 0 : $scope.selectedTree.id, OptimakerRecord.DocType, $cookies['UserId'], OptimakerRecord.TemplateType != null ? false : true)
            var urlstring = "";
            if (templateType != null) {
                if (templateType != 1) {
                    var get2ImgUrl = {};
                    get2ImgUrl.entityID = $stateParams.ID;
                    get2ImgUrl.folderID = $scope.selectedTree.id;
                    get2ImgUrl.userID = $cookies['UserId'];
                    get2ImgUrl.templateType = templateType;
                    get2ImgUrl.DocID = OptimakerRecord.DocID;
                    get2ImgUrl.serverUrl = $scope.TemplateAsset.serverUrl;
                    get2ImgUrl.assetType = OptimakerRecord.AssetType;
                    DamOptimakerTemplateService.Get2ImagineTemplateUrl(get2ImgUrl).then(function (data) {
                        urlstring = data.Response.m_Item1;
                        $scope.TemplateAsset.docversionID = data.Response.m_Item2;
                        $('#optimakeriframe').attr('src', urlstring.toString());
                        timerObj.loadIframeimage = $timeout(function () {
                            $('#loadIframeImg').css('display', 'none');
                        }, 1000);
                    });
                } else {
                    urlstring = OptimakerRecord.ServerURL.replace("[docid]", OptimakerRecord.DocID).replace("[departmentid]", OptimakerRecord.DepartmentID).replace("[doctype]", OptimakerRecord.DocType).replace("[templatename]", OptimakerRecord.Name).replace("[templatename]", OptimakerRecord.Name).replace("[mid]", $stateParams.ID + "|" + OptimakerRecord.DocID + "|" + TenantID).replace("[fid]", $scope.selectedTree.id == null ? 0 : $scope.selectedTree.id).replace("[cid]", $cookies['UserId']);
                    $('#optimakeriframe').attr('src', urlstring.toString());
                    timerObj.loadIframeimage = $timeout(function () {
                        $('#loadIframeImg').css('display', 'none');
                    }, 1000);
                }
                $("#optimakeriframe").addClass('heightcent');
                $('#SaveWordTemplatefooter').css('display', 'none');
            } else {
                $("#optimakeriframe").removeClass('heightcent');
                $('#SaveWordTemplatefooter').css('display', 'block');
                $scope.isTemplateWord = true;
                urlstring = $scope.Urls.OptimakerBaseDocURL.replace("[docpath]", $scope.Urls.TemplatePath + OptimakerRecord.WordPath).replace("[templatename]", OptimakerRecord.Name);;
                var left = (screen.width / 2) - (1200 / 2);
                var top = (screen.height / 2) - (800 / 2);
            }
            timerObj.optimakeriframe = $timeout(function () {
                $('#optimakeriframe').load(function () {
                    $('#optimakeriframe').css('visibility', 'visible');
                });
                $("#optiMaker-iFramePopup").modal('show');
            }, 100);
        }
        $scope.$on("CallbackfromOptiIfrm", function () {
            if ($scope.TemplateAsset.templateType == 2) {
                var dwnldImagAsset = {};
                dwnldImagAsset.entityID = $stateParams.ID;
                dwnldImagAsset.folderID = $scope.selectedTree.id;
                dwnldImagAsset.userID = $cookies['UserId'];
                dwnldImagAsset.docVersionID = $scope.TemplateAsset.docversionID;
                dwnldImagAsset.serverUrl = $scope.TemplateAsset.serverUrl;
                $('#TemplatePdfGeneratingPageModel').modal("show");
                DamOptimakerTemplateService.Download2ImagineAsset(dwnldImagAsset).then(function (UrlRes) {
                    if (UrlRes.Response != 0) {
                        var arr = [];
                        arr.push(UrlRes.Response);
                        $scope.$emit('loadAssetDetailSectionfrThumbnail', arr);
                        $('#TemplatePdfGeneratingPageModel').modal("hide");
                    }
                })
            }
            $scope.$emit('CallBackThumbnailAssetView', 0);
            $("#optimakerTemplatePopup").modal('hide');
        });
        timerObj.showOptimakertemplate = $timeout(function () { ShowOptimakerTemplates(params.id); }, 100);
        $scope.closethispopup = function () {
            $modalInstance.dismiss('cancel');
        }
        $scope.$on("$destroy", function () {
            $timeout.cancel(timerObj);
        });
        $scope.closeoptiTemplatePopup = function () {
            $scope.closethispopup();
        }
    }
    app.controller("mui.DAM.damoptimakertemplateCtrl", ['$window', '$location', '$timeout', '$scope', '$cookies', '$resource', '$stateParams', 'DamOptimakerTemplateService', '$compile', '$modalInstance', 'params', muiDAMdamoptimakertemplateCtrl]);
})(angular, app);