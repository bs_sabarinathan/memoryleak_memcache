/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

    /// <summary>
    /// Module object for table 'MM_TreeValue'.
    /// </summary>
    public class TreeValue : ITreeValue
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected int _attributeid;
        protected int _Nodeid;
        protected int _level;
        protected string _value;
        protected int _parentnode;

        #endregion

        #region Constructors
        public TreeValue() { }

        public TreeValue(int pId, int pEntityid, int pAttributeid, int pNodeid, int pLevel, string pValue,int pParentNode)
        {
            this._id = pId;
            this._entityid = pEntityid;
            this._attributeid = pAttributeid;
            this._Nodeid = pNodeid;
            this._level = pLevel;
            this._value = pValue;
            this._parentnode = pParentNode;
        }
        #endregion
        public TreeValue(int pId)
        {
            this._id = pId;
        }

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int Entityid
        {
            get { return _entityid; }
            set { _entityid = value; }
        }

        public virtual int Attributeid
        {
            get { return _attributeid; }
            set { _attributeid = value; }

        }
        public virtual int Nodeid
        {
            get { return _Nodeid; }
            set { _Nodeid = value; }

        }

        public virtual int Level
        {
            get { return _level; }
            set { _level = value; }

        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }

        }
        public int ParentNode
        {
            get { return _parentnode; }
            set { _parentnode = value; }

        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            Module castObj = null;
            try
            {
                castObj = (Module)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
