﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.ExpireHandler.Model
{
    public partial class ExpireHandlerActionSourceDataDao : BaseDao, ICloneable
    {
        #region Member Variables
        protected int _id;
        protected int _actionSourceID;
        protected int _attributeID;
        protected int _attributeTypeID;
        protected string _attributeCaption;       
        protected string _nodeID;
        protected int _level;
        protected int _splValue;
        protected bool _ispublish;
        protected string _taskinputvalue;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public ExpireHandlerActionSourceDataDao() { }

        public ExpireHandlerActionSourceDataDao(int pactionSourceID, int pattributeID, int pattributeTypeID, string pattributeCaption, string pnodeID, int plevel, int psplValue, bool pispublish,string ptaskinputvalue)
        {
            this._actionSourceID = pactionSourceID;
            this._attributeID = pattributeID;
            this._attributeTypeID = pattributeTypeID;
            this._attributeCaption = pattributeCaption;
            this._nodeID = pnodeID;
            this._level = plevel;
            this._splValue = psplValue;
            this._ispublish = pispublish;
            this._taskinputvalue = ptaskinputvalue;
        }

        public ExpireHandlerActionSourceDataDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int ActionSourceID
        {
            get { return _actionSourceID; }
            set { _actionSourceID = value; }

        }
        public virtual int AttributeID
        {
            get { return _attributeID; }
            set { _attributeID = value; }

        }

        public virtual int AttributeTypeID
        {
            get { return _attributeTypeID; }
            set { _attributeTypeID = value; }

        }
       

        public virtual string AttributeCaption
        {
            get { return _attributeCaption; }
            set { _attributeCaption = value; }
        }

        public virtual string NodeID
        {
            get { return _nodeID; }
            set { _nodeID = value; }
        }
            

        public virtual int Level
        {
            get { return _level; }
            set { _level = value; }

        }

        public virtual int SplValue
        {
            get { return _splValue; }
            set { _splValue = value; }

        }

        public virtual bool Ispublish
        {
            get { return _ispublish; }
            set { _ispublish = value; }
        }

        public virtual string Taskinputvalue
        {
            get { return _taskinputvalue; }
            set { _taskinputvalue = value; }
        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string ExpireHandlerActionSourceDataDao = "ExpireHandlerActionSourceDataDao";
            public const string Id = "Id";
            public const string ActionSourceID = "ActionSourceID";
            public const string AttributeID = "AttributeID";
            public const string AttributeCaption = "AttributeCaption";
            public const string AttributeTypeID = "AttributeTypeID";
            public const string NodeID = "NodeID";
            public const string Level = "Level";
            public const string SplValue = "SplValue";
            public const string Ispublish = "Ispublish";
            public const string Taskinputvalue = "Taskinputvalue";
         

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string ExpireHandlerActionSourceDataDao = "ExpireHandlerActionSourceDataDao";
            public const string Id = "Id";
            public const string ActionSourceID = "ActionSourceID";
            public const string AttributeID = "AttributeID";
            public const string AttributeCaption = "AttributeCaption";
            public const string AttributeTypeID = "AttributeTypeID";
            public const string NodeID = "NodeID";
            public const string Level = "Level";
            public const string SplValue = "SplValue";
            public const string Ispublish = "Ispublish";
            public const string Taskinputvalue = "Taskinputvalue";
         
        }

        #endregion

    }
}
