﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TNValidate;
using System.Reflection;


namespace BrandSystems.Marcom.Utility
{
    public class Validation : IDisposable
    {

        //private List<BrandSystems.Marcom.Core.Metadata.Attribute> objAttributes;

        //public List<BrandSystems.Marcom.Core.Metadata.Attribute> Attributes
        //{
        //    get { return objAttributes; }
        //    set { objAttributes = value; }
        //}

        private Validator Validate = new Validator(ValidationLanguageEnum.English);


        //public bool Validation()
        //{
        //    bool result = false;
            
        //    if (!Validate.HasErrors())
        //    {
        //        result = true;
        //    }            
        //    return result;
        //}

        public void Add(string obj, string Type)
        {
            Validate.That(obj, Type).IsEmail();
        }

        //public void AddAttribute(int EntityType, List<BrandSystems.Marcom.Core.Metadata.Attribute> obj)
        //{
        //   //for check this attribute associated with which validation then add
        //    foreach (var item in obj)
        //    {
                
        //    }
        //}

        public void AddAttribute(List<object> obj)
        {
            Type objType = obj.GetType();
            PropertyInfo[] objProperty = objType.GetProperties();
            foreach (var propertyInfo in objProperty)
            {
                if (propertyInfo.PropertyType.Name.StartsWith("Dictionary"))
                {
                    Dictionary<String, String> values = (Dictionary<String, String>)propertyInfo.GetValue(obj, null);
                    foreach (var value in values)
                    {
                    }
                }
                else if (propertyInfo.PropertyType.IsValueType || propertyInfo.PropertyType.Name.StartsWith("String"))
                {
                }
                else
                {
                }
            }
        }


        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
