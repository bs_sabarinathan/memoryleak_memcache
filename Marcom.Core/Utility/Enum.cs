﻿
public enum OperationId
{
    View = 1,
    Edit = 2,
    Delete = 4,
    Create = 8,
    Self = 16,
    Simulate = 32,
    Allow = 64
}

public enum CostcenterOntimeStatus
{
    Ontime = 0,
    Delayed = 1,
    Onhold = 2
}

public enum Modules
{
    Common = 1,
    Admin = 2,
    Planning = 3,
    UserAccess = 4,
    DAM = 5,
    CMS = 6

}

public enum FeatureID
{
    GanttView = 1,
    ListView = 2,
    Report = 3,
    Calender = 4,
    Financials = 5,
    Workflow = 6,
    Attachments = 7,
    Presentation = 8,
    Member = 9,
    Plan = 10,
    CostCenter = 11,
    Objective = 12,
    Dashboard = 13,
    Mypage = 14,
    GeneralSettings = 15,
    MetadataSettings = 16,
    ExternalLink = 17,
    Duplicate = 18,
    ApproveBudget = 19,
    Overview = 20,
    Task = 21,
    ViewEditAll = 22,
    Support = 23,
    Requestplannedamount = 24,
    Notification = 25,
    FundRequest = 26,
    SimulateUser = 27,
    DynamicEntity = 28,
    SSOExternalUser = 29,
    Workspaces = 30,
    ViewAll = 31,
    AttchmentEdit = 32,
    Full_Page_CMS = 33,
    CMS_With_Navigation = 34,
    MyTask = 35,
    MediaBank = 36,
    CMS_ContentEdit = 38,
    CMS_ContentView = 39,
    LiveChat = 46,
    MyBrief = 50,
    Dalim_approval_report = 51
}

public enum EntityTypeID
{
    Objective = 1,
    Milestone = 4
}
public enum AttributesList
{
    TextSingleLine = 1,
    TextMultiLine = 2,
    ListSingleSelection = 3,
    ListMultiSelection = 4,
    DateTime = 5,
    DropDownTree = 6,
    Tree = 7,
    TextMoney = 8,
    CheckBoxSelection = 9,
    Period = 10,
    Uploader = 11,
    TreeMultiSelection = 12,
    DropDownTreePricing = 13,
    Link = 14,
    ParentEntityName = -10,
    EntityOverviewStatus = 15,
    Dateaction = 16,
    Tag = 17,
    AutoID = 18,
    Amount = 19
}

public enum SystemDefinedAttributes
{
    Name = 68,
    Owner = 69,
    Status = 70,
    ApproveTime = 73,
    MilestoneEntityID = 66,
    MilestoneStatus = 67,
    DueDate = 56,
    AssignedAmount = 59,
    ObjectiveType = 107,
    EntityStatus = 71,
    MyRoleGlobalAccess = 74,
    MyRoleEntityAccess = 75,
    EntityOnTimeStatus = 77,
    Notes = 81,
    Taskname = 82,
    Taskdescription = 83,
    Taskduedate = 84,
    ObjectiveStatus = 108,
    FiscalYear = 1,
    IsExternal = 85,
    Dateaction = 86,
    Tag_Words = 87,
    Description = 3,
    SubAssignedAmount = 88,
    AvailableAssignedAmount = 89,
    CreationDate = 90,
    TrafficStatus = -100,
    CustomStatus = 91
    //Corporate = 111

}


public enum NavigationTypeID
{
    Plan = 1,
    CostCenter = 2,
    Objective = 3,
    MyPage = 4,
    //Support = 5,
    Dashboard = 6,
    //Task = 7,
    //Workspace = 8,
    AdminSetting = 9,
    //Reports = 10,
    ExternalLink = 11,
    MetadataSetting = 12
}

public enum NotificationTypeID
{

    EntityCreated = 1,
    RootLevelEntityCreated = 39,
    EntityUpdated = 2,
    EntityDeleted = 3,
    TaskCreated = 4,
    TaskMetadataUpdated = 5,
    TaskMemberAdded = 6,
    TaskStatusChanged = 7,
    MilestoneCreated = 8,
    MilestoneUpdated = 9,
    MilestoneDeleted = 10,
    EntityStateChanged = 11,
    EntityAttachmentCreated = 12,
    EntityAttachmentDeleted = 13,
    EntityMemberAdded = 14,
    EntityMemberRoleUpdated = 15,
    EntityMemberRemoved = 16,
    EntityCommentAdded = 17,
    EntityDuplicated = 18,
    CostcenterAdded = 19,
    FundRequestCreated = 20,
    ReleasedFunds = 21,
    CostcenterAssignedAmountChanged = 22,
    EntityPlanBudgetUpdated = 23,
    EntityApprovedAllocatedUpdated = 24,
    FundingRequestDeleted = 25,
    FundingRequestStatechanged = 26,
    CostCenterDeleted = 27,
    MoneyTransferred = 28,
    InsertCostCenter = 29,
    EnableDisableWorkflow = 30,
    EntityDateInsert = 31,
    EntityDateDelete = 32,
    TaskAttachementDeleted = 33,
    TaskAttachementInsert = 34,
    AdditionalObjectiveCreated = 35,
    EntityCommitBudgetUpdated = 36,
    EntitySpentBudgetUpdated = 37,
    CostCenterApprovedBudgetUpdated = 38,
}

public enum EntityTypeIDs
{
    Activity = 1,
    Costcenre = 2,
    Objective = 3,
    Milestone = 4,
    Calender = 7,
    AttributeGroupFilterInCostCentre = 6,
    AttributeGroupFilter = 5,
    CostCentreDetailTree = 8
}

public enum EntityTypeList
{

    Milestone = 1,
    CostCentre = 5,
    ActivityList = 6,
    Task = 30,
    Objective = 10,
    FundinngRequest = 7,
    UserDetails = 12,
    Calender = 35,
    Assetapprovaltask = 32,
    Proofapprovaltask = 36,
    dalimapprovaltask = 37


}
public enum MailStatus
{
    Pending = 1,
    Inprogress = 2,
    Sent = 3,
    Error = 4
}

public enum PurchaseOrderStates
{
    Created = 1,
    Approved = 2,
    Send = 3,
    Invoiced = 4,
    Rejected = 5
}

public enum PlanningStates
{
    InProgress = 0,
    Requested = 1,
    Approved = 2,
    Rejected = 3,
    Deleted = 4
}

public enum TaskTypes
{
    Work_Task = 2,
    Approval_Task = 3,
    Reviewal_Task = 31,
    Asset_Approval_Task = 32,
    Proof_approval_task = 36,
    dalim_approval_task = 37
}


public enum TaskStatus
{
    Unassigned = 0,
    In_progress = 1,
    Completed = 2,
    Approved = 3,
    Unable_to_complete = 4,
    Rejected = 5,
    Withdrawn = 6,
    Not_Applicable = 7,
    ForcefulComplete = 8,
    RevokeTask = 9,
    Inactive = 10
}

public enum TaskFlowStandard
{
    MarcomNormal = 0,
    MarcomDocumentApproval = 1,
    ProofHQ = 2,
    Dalim = 3
}

public enum UpcomingWeeks
{
    Overdue = 0,
    This_week = 1,
    Next_week = 2,
    Upcoming = 3

}
public enum TaskAssigneeStatus
{
    Appproved = 1,
    Completed = 2,
    Unable_to_complete = 3,
    Rejected = 4,
    Withdrawn = 5


}

public enum enumDivisonIds
{
    Yearly = 1,
    Monthly = 2,
    Quaterly = 3,
    Half_yearly = 4
}
public enum RecapDays
{
    Daily = -1,
    Mondays = 1,
    Tuesdays = 2,
    Wednesdays = 3,
    Thursdays = 4,
    Fridays = 5,
    Saturdays = 6
}

public enum FinancialAttributesIds
{
    //TotalNoOfItems=-1,
    Planned = -2,
    Requested = -3,
    ApprovedAllocation = -4,
    ApprovedBudget = -5,
    BudgetDeviation = -6,
    Commited = -7,
    Spent = -8,
    AvailableToSpend = -9,
    ID = -100,
    Name = -200
}

public enum StaticEntityRoles
{
    Owner = 1,
    Editor = 2


}
public enum FinancialMetadaTypes
{
    PO = 1,
    Spent = 2,
    FundingCostcenter = 3
}


enum POSysAttributes
{
    PONumber = 5,
    amount = 6,
    CreateDate = 7,
    SupplierName = 8,
    Description = 9,
    RequesterName = 10,
    ApprovedDate = 11,
    SentDate = 12,
    ApproverName = 13
};

enum SpentSysAttributes
{
    InvoiceNumber = 15,
    PaymentDate = 16,
    POID = 17,
    SupplierID = 18,
    Description = 19,
    amount = 20
};

public enum EntityRoles
{
    Owner = 1,
    Editer = 2,
    Viewer = 3,
    ExternalParticipate = 5,
    BudgerApprover = 8,
    ProofInitiator = 9,
    Corporate = 10
}


public enum AssociateEntitypes
{
    Milestone = 1,
    Work_Task = 2,
    Approval_Task = 3,
    Attachment = 4,
    Funding_Request = 7,
    PAttachment = 8,
    PresentationAttachmentLink = 9,
    Additional_Objectives_Assignments = 11,
    Task = 30,
    Review_Task = 31

}

public enum AssetView
{
    ThumbnailView = 1,
    SummaryView = 2,
    ListView = 3
}
//Created enum to maintain the status of the assest files
public enum AssetStatus
{
    Pending = 0,
    Inprogress = 1,
    Done = 2,
    Error = 3
}

public enum PlanTabs
{
    Financials = 1,
    Objectives = 2

}


public enum NewsfeedFilter
{
    Comment = 1,
    Milestone_information = 2,
    Task_information = 3,
    Financial = 4,
    Objectives_Assignments = 5,
    DeletingActivites_Members = 6,
    AddingActivites_Members = 7,
    Attachments = 8,
    UpdatingActivites_Members = 9
}

public enum AssetOperation
{
    Cut = 1,
    Copy = 2

}

public enum AttachmentLinks
{
    http = 1,
    https = 2,
    ftp = 3,
    file = 4
}

public enum SearchViews
{
    Production = 1,
    Asset = 3,
    Task = 2
}

public enum TabViews
{
    plan_overview = 1,
    plan_financial = 2,
    plan_objective = 3,
    costcentre_overview = 4,
    objectives_overview = 5
}
public enum TabFolderPlace
{
    Plans = 1,
    CostCentre = 2,
    Objectives = 3
}

public enum VisibilityTypes
{
    Days = 0,
    Weeks = 2,
    Months = 3,
    Years = 4
}
public enum enumbasisforecating
{
    Planned_Budget = 1,
    Approved_alloc = 2,
    Non_res_appr_alloc = 3

}

public enum expireactionType
{
    Unpublished = 1,
    createtask = 3,
    updatemetadata = 4
}

public enum expireactionfrom
{
    asset = 1,
    entity = 2,
    task = 3,
    AdminTask = 4
}

public enum expireactionType1
{
    Unpublished = 1,
    createtask = 3,
    updatemetadata = 4
}

public enum expireactionfrom1
{
    asset = 1,
    entity = 2,
    task = 3
}
public enum SearchType
{
    Production = 1,
    Assetlibrary = 2,
    Tasks = 3,
    Pages = 4,
    Attachments = 5,
    Tags = 6
}

public enum SystemAttributeTypes
{
    Text_SingleLine = 1,
    Text_MultiLine,
    List_Single_Selection,
    List_Multi_Selection,
    Date_Time = 5,
    DropDown_Tree,
    Tree,
    Text_Money,
    CheckBox_Selection,
    Period = 10,
    Uploader,
    DropDown_Tree_Multi_Selection,
    DropDown_Tree_Pricing,
    Link,
    Status
}
public enum EnumDimensionUnits
{
    Pixels = 1,
    Inches = 2,
    Millimeters = 3
}


public enum SearchCategory
{
    Productions = 1,
    Task = 2,
    Attachments = 3,
    Assetlibrary = 4,
    CmsNavigation = 5,
    Tags = 6
}

public enum ExpireEntityRoles
{
    Owner = -1001,
    Editor = -1002,
    Viewer = -1003,
    ExternalParticipant = -1005,
    //InternalParticipant = -1006,
    //ProdProjLeader = -1007,
    BudgetApprover = -1008,
    ProofInitiator = -1009

}

public enum FinancialTransactionType
{
    Planned = 1,
    ApprovedAllocation = 2,
    Commited = 3,
    Spent = 4,
    ForeCasting = 5,
    TotalAssignedAmount = 6,
    SubAssignedAmount = 7,
    AvailableAssignAmount = 8,
    RequestedAmount = 9
}

public enum TemplateType
{
    Optimaker = 1,
    TwoImagine = 2
}


//public enum IsTaskAlertSent
//{
//    yes = true,
//    no = false
//}

public enum TaskApprovalRount
{
    initialized = 1
}

public enum StorageArea
{
    Local = 0,
    Amazon = 1
}


public enum AWSFilePathIdentity
{
    BasePath = 0,
    DamOriginal = 1,
    DAMPreview = 2,
    DAMCropped = 3,
    DAMThumbnailChanges = 4,
    DAMStaticPreview = 5,
    DAMDownloads = 6,
    DAMFiles = 7,
    UserImages = 8,
    UserImagePreview = 9
}

public enum MetadataSettingsModuleID
{
    EntityType = 3
}


public enum NewsfeedNotification
{
    PersonalisedNewsfeed = 10,
    PersonalisedNotification = 13,
    PersonalisedTaskNewsfeedTempId = 17,
    PersonalisedEntityNewsfeedTempId = 46
}

public enum staticglobalRole
{
    admin = 1
}

public enum xmlType
{
    Admin = 101,
    Metadata = 102,
    CurrentWorking = 103,
    CurrentSyncDB = 104,
    FutureMetadata = 105,
    FutureSyncDB = 106,
    InitialWorking = 107
}


public enum cacheKey
{
    _MarcomSession = 1,
    _MetadataApplyChanges=2
}

public enum MetadataApplyChanges
{
    Lock = 0,
    Unlock = 1,
    ApplyChang
}
//public enum MetadataVersion
//{
//    NonMetadata = 0, // Default value for Non metadata XML
//    Active = 1, 
//    Old = 2
//}
