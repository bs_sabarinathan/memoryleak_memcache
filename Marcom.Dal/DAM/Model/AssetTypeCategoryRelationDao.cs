﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
    public partial class AssetTypeCategoryRelationDao
    {
        
        #region "Member variables"

        protected int _id;
        protected int _assettypeid;
        protected int _categoryid;

        #endregion

       #region "Constructor"

        public AssetTypeCategoryRelationDao() { }


        public AssetTypeCategoryRelationDao(int pid,  int pAssettypeid, int pCategoryid) 
        {
            this._id = pid;
            this._assettypeid = pAssettypeid;
            this._categoryid = pCategoryid;
        }

        #endregion


         #region "Public propertiers"

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int AssetTypeID
        {
            get { return _assettypeid; }
            set { _assettypeid = value; }

        }

        public virtual int CategoryID
        {
            get { return _categoryid; }
            set { _categoryid = value; }

        }

        #endregion


        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion


        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AssetTypeCategoryRelationDao = "AssetTypeCategoryRelationDao";
            public const string ID = "ID";
            public const string AssetTypeID = "AssetTypeID";
            public const string CategoryID = "CategoryID";

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AssetTypeCategoryRelationDao = "AssetTypeCategoryRelationDao";
            public const string ID = "ID";
            public const string AssetTypeID = "AssetTypeID";
            public const string CategoryID = "CategoryID";

        }

        #endregion
    }
}
