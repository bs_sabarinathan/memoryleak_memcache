﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Task.Interface;

namespace BrandSystems.Marcom.Core.Task
{

    /// <summary>
	/// TaskLinks object for table 'TM_Links'.
	/// </summary>
    public class TaskLinks : ITasklinks 
	{
		#region Member Variables

        protected int _id;
        protected int _entityid;
        protected string _name;
        protected string _url;
        protected int _activeversionno;
        protected int _typeid;
        protected string _createdon;
        protected int _ownerid;
        protected int _moduleid;
        protected Guid _linkguid;
        protected string _description;
        protected int _linkType;
		
		#endregion
		
		#region Constructors
		public TaskLinks() {}

        public TaskLinks(int pEntityID, string pName, string pURL, int pActiveVersionNo, int pTypeID, string pCreatedOn, int pOwnerID, int pmoduleID, Guid pLinkGuid, string pDescription,int pLinkType)
        {
            this._entityid = pEntityID;
            this._name = pName;
            this._url = pURL;
            this._activeversionno = pActiveVersionNo;
            this._typeid = pTypeID;
            this._createdon = pCreatedOn;
            this._ownerid = pOwnerID;
            this._moduleid = pmoduleID;
            this._linkguid = pLinkGuid;
            this._description = pDescription;
            this._linkType = pLinkType;
        }

        public TaskLinks(int pID)
		{
            this._id = pID; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int ID
		{
			get { return _id; }
            set {  _id = value; }
			
		}

        public virtual int EntityID
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }

        }
        public virtual string URL

        {
            get { return _url; }
            set { _url = value; }
        }

        public int ActiveVersionNo
        {
            get { return _activeversionno; }
            set { _activeversionno = value; }
        }

        public int TypeID
        {
            get { return _typeid; }
            set { _typeid = value; }
        }

        public string CreatedOn
        {
            get { return _createdon; }
            set { _createdon = value; }
        }

        public int OwnerID
        {
            get { return _ownerid; }
            set { _ownerid = value; }
        }
        public int ModuleID
        {
            get { return _moduleid; }
            set { _moduleid = value; }
        }

        public Guid LinkGuid
        {
            get { return _linkguid; }
            set { _linkguid = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string Extension { get; set; }

        public virtual int LinkType
        {
            get { return _linkType; }
            set { _linkType = value; }

        }

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            TaskLinks castObj = null;
			try
			{
                castObj = (TaskLinks)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.ID );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
}
