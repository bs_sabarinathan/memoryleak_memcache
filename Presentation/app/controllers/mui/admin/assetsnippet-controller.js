﻿(function (ng, app) {
    "use strict";

    function muiadminassetsnippetCtrl($scope, $location, $resource, $timeout, $cookies, $translate, AdminService, $sce) {
        $scope.ColorCodeGlobal = 'ffffff';
        $scope.assetSnippetdata = [];
        $scope.ThemeTypesResult = [];
        $scope.alignmentscope = 1;
        $scope.ColorCodeGlobalObj = {};
        $scope.ColorCodeGlobalObj.colorcode = 'ffffff';
        $scope.ColorOptions = {
            preferredFormat: "hex",
            showInput: true,
            showAlpha: false,
            allowEmpty: true,
            showPalette: true,
            showPaletteOnly: false,
            togglePaletteOnly: true,
            togglePaletteMoreText: 'more',
            togglePaletteLessText: 'less',
            showSelectionPalette: true,
            chooseText: "Choose",
            cancelText: "Cancel",
            showButtons: true,
            clickoutFiresChange: true,
            palette: [
				["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
				["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)", "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
				["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)", "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)", "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)", "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
            ]
        };
        $(".pick-a-color").pickAColor({
            showSpectrum: true,
            showSavedColors: false,
            saveColorsPerElement: true,
            fadeMenuToggle: true,
            showAdvanced: true,
            showHexInput: true,
            showBasicColors: true
        });
        getsnippetcode();

        function getsnippetcode() {
            AdminService.getsnippethoturl().then(function (data) {
                if (data != "") {
                    $scope.searchboxcode = '';
                    $scope.searchboxcode += "\n<script>";
                    $scope.searchboxcode += "\n(function ()";
                    $scope.searchboxcode += "\n";
                    $scope.searchboxcode += " {";
                    $scope.searchboxcode += "\n";
                    $scope.searchboxcode += " var token = '010578781295221296620ehge7t1eqto ';";
                    $scope.searchboxcode += "\n";
                    $scope.searchboxcode += " var alsb = document.createElement('script');";
                    $scope.searchboxcode += "\n";
                    $scope.searchboxcode += " alsb.type = 'text/javascript' ;";
                    $scope.searchboxcode += "\n";
                    $scope.searchboxcode += "alsb.src = '" + data + "/common/getsnippet/'+token";
                    $scope.searchboxcode += "\n";
                    $scope.searchboxcode += " var s = document.getElementsByTagName('script')[0];";
                    $scope.searchboxcode += "\n";
                    $scope.searchboxcode += " s.parentNode.insertBefore(alsb, s);";
                    $scope.searchboxcode += "\n";
                    $scope.searchboxcode += "})();";
                    $scope.searchboxcode += "\n";
                    $scope.searchboxcode += "\n</script>";
                    $scope.searchboxcode += '\n<alsb-searchbox data-token="GetSSOToken()"></alsb-searchbox>';
                    $scope.searchresultcode = "<alsr-searchresults></alsr-searchresults>";
                } else { }
            });
        }
        $scope.shadeColor = function (color, percent, Id) {
            var R = parseInt(color.substring(0, 2), 16);
            var G = parseInt(color.substring(2, 4), 16);
            var B = parseInt(color.substring(4, 6), 16);
            R = parseInt(R * (100 + percent) / 100);
            G = parseInt(G * (100 + percent) / 100);
            B = parseInt(B * (100 + percent) / 100);
            R = (R < 255) ? R : 255;
            G = (G < 255) ? G : 255;
            B = (B < 255) ? B : 255;
            var RR = ((R.toString(16).length == 1) ? "0" + R.toString(16) : R.toString(16));
            var GG = ((G.toString(16).length == 1) ? "0" + G.toString(16) : G.toString(16));
            var BB = ((B.toString(16).length == 1) ? "0" + B.toString(16) : B.toString(16));
            var BGcolor = "#" + RR + GG + BB;
            if (Id == "basedarkercolor") {
                $scope.assetSnippetdata["assetSnippetdata"]["basedarkercolor"] = BGcolor;
            }
        }
        getassetSnippetData();

        function getassetSnippetData() {
            AdminService.getassetSnippetData().then(function (data) {
                if (data.Response != null) {
                    $scope.assetSnippetdata = jQuery.parseJSON(data.Response);
                }
            });
        };
        $scope.Saveandregeneratecode = function () {
            $scope.shadeColor($scope.assetSnippetdata["assetSnippetdata"]["basecolor"], -10, "basedarkercolor");
            var root = {};
            root.assetSnippetData = $scope.assetSnippetdata;
            AdminService.GetAssetSnippetTemplates(root).then(function (data) {
                if (data.Response) {
                    NotifySuccess($translate.instant('LanguageContents.Res_1190.Caption'));
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_1191.Caption'));
                }
            });
        };
        $scope.renderHtml = function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };
        $('#snippetlivepreview').click(function () {
            var newwindow = window.open($(this).prop('href'), '');
            if (window.focus) {
                newwindow.focus();
            }
            return false;
        });
    }
    app.controller("mui.admin.assetsnippetCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$translate', 'AdminService', '$sce', muiadminassetsnippetCtrl])
})(angular, app);