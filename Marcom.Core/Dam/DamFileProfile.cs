﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.DAM.Interface;

namespace BrandSystems.Marcom.DAM
    {

    internal class DamFileProfile : IDamFileProfile
        {
        #region Member Variables
        
        protected int _dprofileid;
        protected string _dprofileame;
        protected double _dprofileheight;
        protected double _dprofilewidght;
        protected string _extension;
        protected int _dpi;
        protected string _isassetaccess;

        #endregion


         #region Constructors
        public DamFileProfile() { }

        public DamFileProfile(int did , string dname , double dheight , double dweight , string extension , int dpi , string isassetaccess)
        {
        this._dprofileid = did;
        this._dprofileame = dname;
        this._dprofileheight = dheight;
        this._dprofilewidght = dweight;
        this._extension = extension;
        this._dpi = dpi;
        this._isassetaccess = isassetaccess;
        }

        public DamFileProfile(int dId)
        {
        this._dprofileid = dId;
        }

        #endregion


        #region Public Properties


        public int ID
            {
            get { return _dprofileid; }
            set { _dprofileid = value; }
            }

        public string Name
            {
            get { return _dprofileame; }
            set { _dprofileame = value; }
            }

        public double Height
            {
            get { return _dprofileheight; }
            set { _dprofileheight = value;  }
            }


        public double Width
            {
            get { return _dprofilewidght; }
            set { _dprofilewidght = value; }
            }

        public string Extension
            {
            get { return _extension; }
            set { _extension = value; }
            }
        public int DPI
            {
            get { return _dpi; }
            set { _dpi = value; }
            }
        public string IsAssetAccess
            {
            get { return _isassetaccess; }
            set{_isassetaccess = value;}
            }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
            {
            if(this == obj) return true;
            DamFileProfile castObj = null;
            try
                {
                castObj = (DamFileProfile)obj;
                }
            catch(Exception) { return false; }
            return (castObj != null) &&
                (this._dprofileid == castObj.ID);
            }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
            {
            int hash = 57;
            hash = 27 * hash * _dprofileid.GetHashCode();
            return hash;
            }
        #endregion




        }
    }
