﻿using BrandSystems.Marcom.Core.Metadata.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class AttributeSequence : IAttributeSequence
    {
        public int Id { get; set; }
        public int AttributeID { get; set; }
        public int StartWithValue { get; set; }
        public string Pattern { get; set; }
        public int CurrentValue { get; set; }
    }
}
