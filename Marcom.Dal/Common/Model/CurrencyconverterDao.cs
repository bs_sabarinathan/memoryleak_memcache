﻿using System;
using System.Collections.Generic;


namespace BrandSystems.Marcom.Dal.Common.Model
    {

    /// <summary>
    /// Currencyconverter object for table 'CM_currencyconverter'.
    /// </summary>

    public partial class CurrencyconverterDao : BaseDao , ICloneable
        {
        #region Member Variables

        protected int _id;
        protected DateTime _startdate;
        protected DateTime _enddate;
        protected string _currencytype;
        protected double _currencyconvertrate;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;

        #endregion

        #region Constructors
        public CurrencyconverterDao() { }
        public CurrencyconverterDao(int ccid , DateTime ccstartdate , DateTime ccenddate , string cccurrencytype , double cccurrnecyconverterrate)
            {
            this._id = ccid;
            this._startdate = ccstartdate;
            this._enddate = ccenddate;
            this._currencytype = cccurrencytype;
            this._currencyconvertrate = cccurrnecyconverterrate;
            }

        public CurrencyconverterDao(int ccid)
            {
            this._id = ccid;
            }
        #endregion

        #region Public Properties

        public virtual int Id
            {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

            }
        public virtual DateTime Startdate
            {
            get { return _startdate; }
            set
                {
                _bIsChanged |= (_startdate != value);
                _startdate = value;
                }
            }

        public virtual DateTime Enddate
            {
            get { return _enddate; }
            set
                {
                _bIsChanged |= (_enddate != value);
                _enddate = value;
                }
            }

        public virtual string Currencytype
            {
            get { return _currencytype; }
            set
                {
                _bIsChanged |= (_currencytype != value);
                _currencytype = value;
                }
            }

        public virtual double Currencyrate
            {
            get { return _currencyconvertrate; }
            set
                {
                _bIsChanged |= (_currencyconvertrate != value);
                _currencyconvertrate = value;
                }
            }

        public virtual bool IsDeleted
            {
            get
                {
                return _bIsDeleted;
                }
            set
                {
                _bIsDeleted = value;
                }
            }

        public virtual bool IsChanged
            {
            get
                {
                return _bIsChanged;
                }
            set
                {
                _bIsChanged = value;
                }
            }

        #endregion 

        #region ICloneable methods

        public virtual object Clone()
            {
            return this.MemberwiseClone();
            }

        #endregion



        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
            {
            public const string CurrencyconverterDao = "CurrencyconverterDao";
            public const string Id = "Id";
            public const string Startdate = "Startdate";
            public const string Enddate = "Enddate";
            public const string Currencytype = "Currencytype";
            public const string Currencyrate = "Currencyrate";
            }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
            {
            public const string CurrencyconverterDao = "CurrencyconverterDao";
            public const string Id = "Id";
            public const string Startdate = "Startdate";
            public const string Enddate = "Enddate";
            public const string Currencytype = "Currencytype";
            public const string Currencyrate = "Currencyrate";

            }

        #endregion



        }
    }
