﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
   public interface IObjectiveEntitycondition
    {
       int ObjectiveId { get; set; }
       int ConditionType { get; set; }
       int EntityTypeId { get; set; }
       int AttributeId { get; set; }
       string AttributeOptions { get; set; }
    }
}
