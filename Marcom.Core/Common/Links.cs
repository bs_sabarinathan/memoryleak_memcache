using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// Links object for table 'CM_Links'.
	/// </summary>
	
	public class Links : Ilinks 
	{
		#region Member Variables

        protected int _id;
        protected int _entityid;
        protected string _name;
        protected string _url;
        protected int _activeversionno;
        protected int _typeid;
        protected string _createdon;
        protected int _ownerid;
        protected int _moduleid;
        protected int _linktype;
        protected Guid _linkguid;
        protected string _description;
		
		
		#endregion
		
		#region Constructors
		public Links() {}

        public Links(int pEntityID, string pName, string pURL, int pActiveVersionNo, int pTypeID, string pCreatedOn, int pOwnerID, int pmoduleID, Guid pLinkGuid, string pDescription,int pLinkType)
        {
            this._entityid = pEntityID;
            this._name = pName;
            this._url = pURL;
            this._activeversionno = pActiveVersionNo;
            this._typeid = pTypeID;
            this._createdon = pCreatedOn;
            this._ownerid = pOwnerID;
            this._moduleid = pmoduleID;
            this._linkguid = pLinkGuid;
            this._description = pDescription;
            this._linktype = pLinkType;
        }

        public Links(int pID)
		{
            this._id = pID; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int ID
		{
			get { return _id; }
            set {  _id = value; }
			
		}

        public virtual int EntityID
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }

        }
        public virtual string URL

        {
            get { return _url; }
            set { _url = value; }
        }

        public int ActiveVersionNo
        {
            get { return _activeversionno; }
            set { _activeversionno = value; }
        }

        public int TypeID
        {
            get { return _typeid; }
            set { _typeid = value; }
        }

        public string CreatedOn
        {
            get { return _createdon; }
            set { _createdon = value; }
        }

        public int OwnerID
        {
            get { return _ownerid; }
            set { _ownerid = value; }
        }
        public int ModuleID
        {
            get { return _moduleid; }
            set { _moduleid = value; }
        }

        public Guid LinkGuid
        {
            get { return _linkguid; }
            set { _linkguid = value; }
        }

        public virtual int LinkType
        {
            get { return _linktype; }
            set { _linktype = value; }

        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            Links castObj = null;
			try
			{
                castObj = (Links)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.ID );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
