﻿using System.Web;
using System.Web.Mvc; 
using Marcom.API.Services;

namespace Marcom.API
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            
            filters.Add(new HandleErrorAttribute());
         }
    }
}