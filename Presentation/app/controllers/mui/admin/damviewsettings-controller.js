﻿(function (ng, app) {
    "use strict"; function muiadmindamviewsettingsCtrl($scope, $resource, $timeout, $location, $translate, AdminService) {
        $scope.viewtype = 'ThumbnailView'; $scope.Damentitytype = {}; AdminService.GetEntityType(5).then(function (entitytypes) {
            $scope.Damentitytype = entitytypes.Response; var Entitytypelist = $scope.Damentitytype; $scope.entitytype = $scope.Damentitytype[0].Id; for (var i = 0; i < Entitytypelist.length; i++) { var el = Entitytypelist[i]; $scope.EntityTypeListdata.push({ "id": el.Id, "text": el.Caption, "ShortDescription": el.ShortDescription, "ColorCode": el.ColorCode, "Caption": el.Caption }); }
            if ($scope.EntityTypeListdata.length == Entitytypelist.length) { $timeout(function () { loadattributedetails() }, 10); }
        }); function loadattributedetails() { $scope.viewattribtues = []; if ($('#viewtype').val() != null && $('#entitytype').val() != null && $('#entitytype').val() != 0) { AdminService.GetDAMViewAdminSettings($('#viewtype').val(), $('#entitytype').val()).then(function (data) { var res = []; res = data.Response; $scope.viewattribtues = res[0]; }); } }
        $scope.EntityTypeListdata = []; $scope.loadattributedetails = function () { loadattributedetails(); }
        $scope.GetChckedViewSettings = function (row, chkboxtype) {
            var attributesview = $scope.viewattribtues; if (chkboxtype == "IsColumn" && row.IsColumn == true) { $.grep(attributesview, function (e) { return e.ID == row.ID })[0].IsColumn = false; }
            else if (chkboxtype == "IsColumn" && row.IsColumn == false) { $.grep(attributesview, function (e) { return e.ID == row.ID })[0].IsColumn = true; }
            else if (chkboxtype == "IsToolTip" && row.IsToolTip == true) { $.grep(attributesview, function (e) { return e.ID == row.ID })[0].IsToolTip = false; }
            else if (chkboxtype == "IsToolTip" && row.IsToolTip == false) { $.grep(attributesview, function (e) { return e.ID == row.ID })[0].IsToolTip = true; }
            var data1 = {}; data1.ViewType = $('#viewtype').val(); data1.DamType = $('#entitytype').val(); data1.attribtueslist = attributesview; AdminService.UpdateDamViewStatus(data1).then(function (data) {
                if (data.Response == true) { NotifySuccess($translate.instant('LanguageContents.Res_4872.Caption')); if ($('#viewtype').val() != null && $('#entitytype').val() != null && $('#entitytype').val() != 0) { AdminService.GetDAMViewAdminSettings($('#viewtype').val(), $('#entitytype').val()).then(function (data) { var res = []; res = data.Response; $scope.viewattribtues = res[0]; }); } }
                else
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'))
            });
        }
        $("#damviewsettingslist tbody").sortable({
            axis: "y", scroll: true, cursor: "move", update: function (event, ui) {
                var listItems = $("tr", this); var uiArray = new Array(); listItems.length; var sortorderval = 0; listItems.each(function (idx, li) { sortorderval = sortorderval + 1; var finid = parseInt($(li).find('td:first').attr('data-id')); uiArray.push($(li).find('td:first').attr('data-id')); $.grep($scope.viewattribtues, function (e) { return e.ID == finid; })[0].SortOrder = sortorderval; }); var dd = {}; dd.ViewType = $('#viewtype').val(); dd.DamType = $('#entitytype').val(); dd.attribtueslist = $scope.viewattribtues; AdminService.UpdateDamViewStatus(dd).then(function (data) {
                    if (data.Response == true)
                        NotifySuccess($translate.instant('LanguageContents.Res_4872.Caption')); else
                        NotifyError($translate.instant('LanguageContents.Res_4351.Caption'))
                });
            }
        }); $scope.$on("$destroy", function () { RecursiveUnbindAndRemove($("[ng-controller='mui.admin.damviewsettingsCtrl']")); }); $scope.formatResult = function (item) { var markup = '<table class="user-result">'; markup += '<tbody>'; markup += '<tr>'; markup += '<td class="user-image">'; markup += '<span class="eicon" style="vertical-align: middle; margin-top: -4px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>'; markup += '</td>'; markup += '<td class="user-info">'; markup += '<div class="user-title">' + item.text + '</div>'; markup += '</td>'; markup += '</tr>'; markup += '</tbody>'; markup += '</table>'; return markup; }; $scope.formatSelection = function (item) { var markup = '<table class="user-result">'; markup += '<tbody>'; markup += '<tr>'; markup += '<td class="user-image">'; markup += '<span class="eicon" style="vertical-align: middle; margin-top: -5px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>'; markup += '</td>'; markup += '<td class="user-info">'; markup += '<div class="user-title">' + item.text + '</div>'; markup += '</td>'; markup += '</tr>'; markup += '</tbody>'; markup += '</table>'; return markup; }; $scope.entitytype = ''; $scope.EntityTypeListdata = []; $scope.tagAllOptionsEntitytypelist = { multiple: false, allowClear: true, data: $scope.EntityTypeListdata, formatResult: $scope.formatResult, formatSelection: $scope.formatSelection, dropdownCssClass: "bigdrop", escapeMarkup: function (m) { return m; } };
    }
    app.controller("mui.admin.damviewsettingsCtrl", ['$scope', '$resource', '$timeout', '$location', '$translate', 'AdminService', muiadmindamviewsettingsCtrl]);
})(angular, app);