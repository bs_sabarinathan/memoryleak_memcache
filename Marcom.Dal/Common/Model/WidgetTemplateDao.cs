﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Common.Model
{

    /// <summary>
    /// WidgetTemplateDao object for table 'CM_WidgetTemplate'.
    /// </summary>
    public partial class WidgetTemplateDao : BaseDao, ICloneable 
    {
        #region Member Variables

		protected int _id;
		protected string _TemplateName;
        protected string _TemplateDescription;
        
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public WidgetTemplateDao() {}

        public WidgetTemplateDao(string  pTemplateName)
		{
            this._TemplateName = pTemplateName; 
			           
		}

        public WidgetTemplateDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual string TemplateName
        {
            get { return _TemplateName; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("TemplateName", "TemplateName value, cannot contain more than 500 characters");
                _bIsChanged |= (_TemplateName != value);
                _TemplateName = value;
            }

        }

        public virtual string TemplateDescription
        {
            get { return _TemplateDescription; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("TemplateDescription", "TemplateDescription value, cannot contain more than 500 characters");
                _bIsChanged |= (_TemplateDescription != value);
                _TemplateDescription = value;
            }

        }
		
	        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string WidgetTemplateDao = "WidgetTemplateDao";			
			public const string Id = "Id";
            public const string TemplateName = "TemplateName";
            public const string TemplateDescription = "TemplateDescription";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string WidgetTemplateDao = "WidgetTemplateDao";			
			public const string Id = "Id";
            public const string TemplateName = "TemplateName";
            public const string TemplateDescription = "TemplateDescription";
		}

		#endregion
		

    }
}
