﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Cms.Interface;

namespace BrandSystems.Marcom.Core.Cms
{
    public partial class RevisedEntityContent : IRevisedEntityContent
    {
        #region Member variables

        protected int _id;
        protected int _entityid;
        protected bool _active;
        protected string _content;
        protected string _createdon;

        #endregion

        #region Constructors

        public RevisedEntityContent()
        {

        }

        public RevisedEntityContent(int pid)
        {
            this._id = pid;
        }

        public RevisedEntityContent(int pid, int pentityid, bool pactive, string pcontent, string pcreateon)
        {
            this._id = pid;
            this._entityid = pentityid;
            this._active = pactive;
            this._content = pcontent;
            this._createdon = pcreateon;
        }

        #endregion

        #region Public properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public int EntityID
        {
            get { return _entityid; }
            set { _entityid = value; }
        }


        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }


        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }

        public string CreatedOn
        {
            get { return _createdon; }
            set { _createdon = value; }
        }

        #endregion


    }
}
