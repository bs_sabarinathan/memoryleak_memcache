﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Access.Interface
{
    public interface IGlobalEntityTypeAccess
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int GlobalRoleid
        {
            get;
            set;

        }

        int Moduleid
        {
            get;
            set;

        }

        int EntityTypeid
        {
            get;
            set;

        }

        bool AccessPermission
        {
            get;
            set;

        }


        #endregion
    }
}
