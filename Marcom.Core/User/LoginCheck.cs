﻿using BrandSystems.Marcom.Core.User.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.User
{
    public class LoginCheck : ILoginCheck, ICloneable
    {
        #region Member Variables

        protected int _id;
		protected string _email;
		protected DateTime _logintime;
		protected string _ipaddress;
        protected bool _notifyadmin;

        protected bool _bIsChanged;
        #endregion
		
		#region Constructors
		public LoginCheck() {}

        public LoginCheck(int Id, string Email, DateTime LoginTime, string IpAddress, bool NotifyAdmin)
		{
            this._id = Id;
            this._email = Email;
            this._logintime = LoginTime;
            this._ipaddress = IpAddress;
            this._notifyadmin = NotifyAdmin;
		}
                
		#endregion
		
		#region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }
        public virtual string Email
        {
            get { return _email; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Email", "Email value, cannot contain more than 50 characters");
                _bIsChanged |= (_email != value);
                _email = value;
            }

        }
        public virtual DateTime LoginTime
        {

            get { return _logintime; }
            set { _bIsChanged |= (_logintime != value); _logintime = value; }

        }
        public virtual string IpAddress
        {
            get { return _ipaddress; }
            set
            {
                if (value != null && value.Length > 1000)
                    throw new ArgumentOutOfRangeException("IpAddress", "IpAddress value, cannot contain more than 15 characters");
                _bIsChanged |= (_ipaddress != value);
                _ipaddress = value;
            }

        }
        public bool NotifyAdmin
        {
            get { return _notifyadmin; }
            set { _notifyadmin = value; }

        }
               
        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}

      

		#endregion 				
		
        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }
}
