﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

    /// <summary>
    /// Feed object for table 'CM_Feed'.
    /// </summary>

    class PurchaseOrderSettings : IPurchaseOrderSettings
    {

        public DateTime CreateDate { get; set; }
        public int ID { get; set; }
        public string Prefix { get; set; }
        public string DateFormat { get; set; }
        public int NoOffDigits { get; set; }
        public string DigitFormat { get; set; }
        public string NumberCount { get; set; }
    }

}
