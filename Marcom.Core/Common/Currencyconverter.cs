﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Common
    {

    /// <summary>
    /// Currencyconverter object for table 'CM_Currencyconverter'.
    /// </summary>

    internal class Currencyconverter : ICurrencyConverter
        {

        #region Member Variables
        protected int _id;
        protected string _startdate;
        protected string _enddate;
        protected string _currencytype;
        protected double _currencyrate;
        #endregion

        #region Constructors
        public Currencyconverter() { }
        public Currencyconverter(int ccid , string ccstartdate , string ccenddate , string cccurrencytype , double cccurrnecyrate)
            {
            this._id = ccid;
            this._startdate = ccstartdate;
            this._enddate = ccenddate;
            this._currencytype = cccurrencytype;
            this._currencyrate = cccurrnecyrate;
            }
        #endregion

        #region Public Properties

        public int Id
            {
            get { return _id; }
            set { _id = value; }

            }
        public string Startdate
            {
            get { return _startdate; }
            set { _startdate = value; }
            }
        public string Enddate
            {
            get { return _enddate; }
            set { _enddate = value; }
            }
        public string Currencytype
            {
            get { return _currencytype; }
            set { _currencytype = value; }
            }
        public double Currencyrate
            {
            get { return _currencyrate; }
            set { _currencyrate = value; }
            }
        #endregion 

        #region Equals And HashCode Overrides

        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
            {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
            }
        #endregion
		

        }
    }
