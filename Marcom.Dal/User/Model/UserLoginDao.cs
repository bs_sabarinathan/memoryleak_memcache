﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.User.Model
{
    public partial class UserLoginDao : BaseDao, ICloneable 
    {
        #region Member Variables

		protected int _id;
        protected int _IUserID;
        protected DateTime _ILoginTime;
        protected string _IPAddress;
        protected string _IPNumber;
        protected string _IVersion;
        protected string _IBrowser;
        protected int _IMajorVersion;
        protected string _IMinorVersion;
		protected string _IOS;
        protected bool _IIsSSO;
        protected string _ICountryName;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
       


		#endregion
		
		#region Constructors
		public UserLoginDao() {}

        public UserLoginDao(int pUserID, DateTime PLoginTime, string pIPAddress, string pIPNumber, string pBrowser, string PVersion, int PMajorVersion, string pMinorVersion, string pOS, bool pIIsSSO, string pCountryName)
		{
            this._IUserID = pUserID;
            this._ILoginTime = PLoginTime;
            this._IPAddress = pIPAddress;
            this._IPNumber = pIPNumber;
            this._IBrowser = pBrowser;
            this._IVersion = PVersion;
            this._IMajorVersion = PMajorVersion;
            this._IMinorVersion = pMinorVersion;
            this._IOS = pOS;
            this._IIsSSO =pIIsSSO;
            this._ICountryName = pCountryName;
			
            
		}

        public UserLoginDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int UserID
        {
            get { return _IUserID; }
            set { _IUserID = value; }

        }

        public virtual DateTime LoginTime
        {
            get { return _ILoginTime; }
            set
            {
                _ILoginTime = value;
            }

        }
       
        public virtual string IPAddress
		{
            get { return _IPAddress; }
			set 
			{
                _bIsChanged |= (_IPAddress != value);
                _IPAddress = value; 
			}
			
		}



        public virtual string IPNumber
        {
            get { return _IPNumber; }
            set
            {
                _bIsChanged |= (_IPNumber != value);
                _IPNumber = value;
            }

        }
        public virtual string Browser
        {
            get { return _IBrowser; }
            set
            {
                _bIsChanged |= (_IBrowser != value);
                _IBrowser = value;
            }

        }

        public virtual string Version
        {
            get { return _IVersion; }
            set
            {
                _bIsChanged |= (_IVersion != value);
                _IVersion = value;
            }

        }
       
        public virtual int MajorVersion
        {
            get { return _IMajorVersion; }
            set { _IMajorVersion = value; }

        }
        public virtual string MinorVersion
		{
            get { return _IMinorVersion; }
            set
            {
                _bIsChanged |= (_IMinorVersion != value);
                _IMinorVersion = value;
            }
			
		}

        public virtual string OS
        {
            get { return _IOS ; }
            set
            {
                _bIsChanged |= (_IOS != value);
                _IOS = value;
            }

        }

        public virtual bool IsSSO
        {
            get { return _IIsSSO; }
            set { _IIsSSO = value; }

        }

        public virtual string CountryName
        {
            get { return _ICountryName; }
            set
            {
                _bIsChanged |= (_ICountryName != value);
                _ICountryName = value;
            }

        }

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}

       

       

        

       

		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string UserLoginDao = "UserLoginDao";			
			public const string Id = "Id";
            public const string UserID = "UserID";
            public const string LoginTime = "LoginTime";
            public const string IPAddress = "IPAddress";
            public const string IPNumber = "IPNumber";
            public const string Browser = "Browser";
            public const string Version = "Version";
            public const string MajorVersion = "MajorVersion";
            public const string MinorVersion = "MinorVersion";
            public const string OS = "OS";
            public const string IsSSO = "IsSSO";
            public const string CountryName = "CountryName";
           
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string UserLoginDao = "UserLoginDao";
            public const string Id = "Id";
            public const string UserID = "UserID";
            public const string LoginTime = "LoginTime";
            public const string IPAddress = "IPAddress";
            public const string IPNumber = "IPNumber";
            public const string Browser = "Browser";
            public const string Version = "Version";
            public const string MajorVersion = "MajorVersion";
            public const string MinorVersion = "MinorVersion";
            public const string OS = "OS";
            public const string IsSSO = "IsSSO";
            public const string CountryName = "CountryName";

		}

		#endregion
    }
}
