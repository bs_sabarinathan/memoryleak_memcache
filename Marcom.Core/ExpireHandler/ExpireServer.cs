﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Microsoft.VisualBasic;
using System.Collections;
using System.Data;
using System.Diagnostics;
using BrandSystems.Library.Threading;
using BrandSystems.Library.Threading.Internal;
using System.Reflection;
using System.Configuration;
using BrandSystems.Marcom.Core.Managers;
using BrandSystems.Marcom.Utility;
using System.Drawing;
using System.Net.Mail;
using System.Web;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Managers.Proxy;
using BrandSystems.Marcom.Dal.Common.Model;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Configuration;
using System.Net.Configuration;
using System.IO;
using System.Xml.Linq;
using BrandSystems.Marcom.Core.Common;
using System.Data.SqlClient;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Dal.Task.Model;
using BrandSystems.Marcom.Dal.User.Model;
using BrandSystems.Marcom.Dal.Metadata.Model;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Dal.Planning.Model;
using BrandSystems.Marcom.Dal.DAM.Model;
using System.Globalization;
using BrandSystems.Marcom.Dal.ExpireHandler.Model;
using BrandSystems.Marcom.Core.Managers.Proxy;
using Newtonsoft.Json.Linq;
using BrandSystems.Marcom.Core.Task.Interface;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Access.Interface;
using BrandSystems.Marcom.Core.AmazonStorageHelper;
using BrandSystems.Marcom.Core.Utility;
namespace BrandSystems.Marcom.Core.ExpireHandler
{
    public class ExpireServer
    {
        // IMarcomManager marcomManager=null;
        //MarcomManager marcomManager = new MarcomManager();
        public ExpireServer()
        {



        }

        private static ExpireServer _instance = new ExpireServer();
        //private static string _header;
        //public static Image _logo;

        public static ExpireServer Instance
        {
            get { return _instance; }
            set { _instance = value; }
        }
        //public static string Header
        //{
        //    get { return _header; }
        //    set { _header = value; }
        //}
        //public static Image Logo
        //{
        //    get { return _logo; }
        //    set { _logo = value; }
        //}


        string nuller = "";
        public bool Initialize(String smtpServer)
        {
            try
            {

                //PreviewServer.Instance.previewStatusUpdate += PreviewStatusUpdateToDb;
                //var client = new SmtpClient
                //{
                //    Credentials = new System.Net.NetworkCredential("marcom.noreply@brandsystems.in", "bsi1234$"),
                //    Host = "mail.brandsystems.in"
                //};

                return true;
            }
            catch (Exception)
            {

                throw;
            }

            return false;
        }

        //Create properties for Logo, Header 

        public Dictionary<string, string> ApplicationSettings = new Dictionary<string, string>();
        private Dictionary<Guid, IWorkItemsGroup> _workItemGroupList = new Dictionary<Guid, IWorkItemsGroup>();
        private List<Guid> _workItemGroupListIDs = new List<Guid>();

        private static BSThreadPool _bsThreadPool;
        //private static IWorkItemsGroup _workItemGroup;
        //public delegate void PreviewStatusUpdate(MailHolder mailHolder, PreviewStatus status);
        //public delegate void PreviewStatusUpdate(PreviewHolder previewHolder, PreviewStatus status);
        //public event PreviewStatusUpdate previewStatusUpdate;

        #region "Start and Stop Engine"

        /// <summary>
        /// For Starting the Engine
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool StartEngine()
        {
            try
            {
                BSTPStartInfo bstpStartInfo = new BSTPStartInfo();
                bstpStartInfo.IdleTimeout = 30 * 1000; //30 Sec
                bstpStartInfo.MaxWorkerThreads = 30; //Max 30 thread we will create                
                bstpStartInfo.MinWorkerThreads = 0;
                bstpStartInfo.PerformanceCounterInstanceName = "Marcom Preview BSThreadPool";
                _bsThreadPool = new BSThreadPool(bstpStartInfo);

            }
            catch (Exception)
            {
                return false;

            }
            finally
            {
            }

            return true;
        }

        /// <summary>
        /// For Stoping the engine
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool StopEngine()
        {


            try
            {
                _bsThreadPool.Shutdown();
                _bsThreadPool.Dispose();
                _bsThreadPool = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();

            }
            catch (Exception)
            {
                return false;

            }
            finally
            {
            }

            return true;
        }

        #endregion

        public void HandleSendExpire(int TenantID) //send task Preview , Preview for actions that in MH .
        {

            try
            {
                LogHandler.LogInfo("function start HandleSendExpire:" + DateTime.Now, LogHandler.LogType.Notify);
                IMarcomManager marcomManagerExpire = MarcomManagerFactory.GetMarcomManager(null, MarcomManagerFactory.GetSystemSession(TenantID));
                XDocument adminXmlDoc = MarcomCache<XDocument>.ReadXDocument(xmlType.Admin, TenantID);
                var DefaultExpirysetting = adminXmlDoc.Descendants("Expirysetting").FirstOrDefault();
                string TaskListname = "";
                string TaskListDescription = "";
                if (DefaultExpirysetting != null)
                {
                    var DefaultExpirysettingActionTaskListName = adminXmlDoc.Descendants("Expirysetting").Descendants("ActionTaskListName").FirstOrDefault();
                    var DefaultExpirysettingActionTaskListDescription = adminXmlDoc.Descendants("Expirysetting").Descendants("ActionTaskListDescription").FirstOrDefault();
                    if (DefaultExpirysettingActionTaskListName != null)
                        TaskListname = adminXmlDoc.Descendants("Expirysetting").Descendants("ActionTaskListName").ElementAt(0).Value;
                    else
                        TaskListname = "Automated";
                    if (DefaultExpirysettingActionTaskListDescription != null)
                        TaskListDescription = adminXmlDoc.Descendants("Expirysetting").Descendants("ActionTaskListDescription").ElementAt(0).Value;
                    else
                        TaskListDescription = "Automated";


                }
                ClsDb clsDb = new ClsDb(marcomManagerExpire.User.TenantHost);
                DataSet dataSet = new DataSet();
                LogHandler.LogInfo("caling HandleSendExpire:" + DateTime.Now, LogHandler.LogType.Notify);
                dataSet = clsDb.MailData("SELECT ID,ActionType,ActionSrcID,EntityID,ActionPlace,AttributeID FROM  EH_ActionInitiator AS es  WHERE  Status= 0  AND   DATEDIFF(day, GETDATE(), ExecutionDate) = 0 ", CommandType.Text);



                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    //LogHandler.LogInfo("UpdateExpireActionDate methodcalling inside:"+ DateTime.Now, LogHandler.LogType.Notify);
                    using (ITransaction tx = marcomManagerExpire.GetTransaction(marcomManagerExpire.User.TenantID))
                    {
                        DataSet ds = new DataSet();
                        string dateformate;
                        // dateformate = tx.MarcomManager.GlobalAdditionalSettings[0].SettingValue.ToString().Replace('m', 'M');
                        int[] EntiymeberIDArr = { };

                        StringBuilder formatmail = new StringBuilder();
                        ds = clsDb.MailData("SELECT  SettingValue AS dateformate  FROM   CM_AdditionalSettings WHERE  SettingName='DateFormat' ", CommandType.Text);
                        dateformate = ds.Tables[0].Rows[0]["dateformate"].ToString();
                        var Expiretask = dataSet.Tables[0].AsEnumerable().ToList().GroupBy(s => s.Field<int>("ID"));

                        foreach (var ExpireAction in Expiretask)
                        {
                            try
                            {
                                LogHandler.LogInfo("HandleSendExpire methodcalling inside sourecID:" + ExpireAction.Key + ":" + DateTime.Now, LogHandler.LogType.Notify);
                                IList<EHActionInitiatorDao> ExpireHandlerAction = new List<EHActionInitiatorDao>();
                                var actionrows = (from tt in tx.PersistenceManager.ExpireHandlerRepository[marcomManagerExpire.User.TenantID].Query<EHActionInitiatorDao>() where tt.Id == ExpireAction.Key && tt.Status == 0 select tt).Select(a => a).ToList();
                                EHActionInitiatorDao Expiresourcedao = new EHActionInitiatorDao();

                                if (actionrows != null && actionrows.Count > 0)
                                {
                                    foreach (var val in actionrows)
                                    {
                                        int entityTypeId = 0;
                                        Expiresourcedao = new EHActionInitiatorDao();
                                        Expiresourcedao = (from tt in tx.PersistenceManager.ExpireHandlerRepository[marcomManagerExpire.User.TenantID].Query<EHActionInitiatorDao>() where tt.Id == val.Id select tt).Select(a => a).FirstOrDefault();
                                        int sourcfrom = val.ActionPlace;
                                        string Actionsrc = val.ActionSrcValue;
                                        int ProductionEntityID = 0;
                                        bool ProductionActive = false;
                                        string AttributeActiondates = "";
                                        DataSet ds2 = new DataSet();
                                        DataSet ds3 = new DataSet();
                                        StringBuilder mainTblQry = new StringBuilder();
                                        IList<IEntityRoleUser> listEntityRoleUser = new List<IEntityRoleUser>();
                                        //JObject jobj1 = JObject();
                                        if (val.ActionPlace > 1)
                                        {
                                            JObject jobj1 = JObject.Parse(val.ActionSrcValue);
                                            JObject jobj = (JObject)jobj1["ActionTaskSrcValue"];
                                            ProductionEntityID = Convert.ToInt16(jobj["ProductionEntityID"].ToString());
                                        }
                                        else if (val.ActionPlace == 1)
                                        {
                                            if (val.ActionType == 1)
                                            {
                                                JObject jobj = JObject.Parse(val.ActionSrcValue);
                                                ProductionEntityID = Convert.ToInt16(jobj["ProductionEntityID"].ToString());
                                            }
                                            else if (val.ActionType == 2 || val.ActionType == 3)
                                            {
                                                JObject jobj1 = JObject.Parse(val.ActionSrcValue);
                                                JObject jobj = (JObject)jobj1["ActionTaskSrcValue"];
                                                JArray arrobjMembers = (JArray)jobj["TaskMembers"];
                                                JArray arrobjMembersexpireroles = (JArray)jobj["TaskMembersexpireroles"];
                                                ProductionEntityID = Convert.ToInt16(jobj["ProductionEntityID"].ToString());
                                                //var entityroleuser = tx.PersistenceManager.MetadataRepository.GetAll<BrandSystems.Marcom.Dal.Access.Model.EntityRoleUserDao>();
                                                //var RoleUser = from entity in entityroleuser where entity.Entityid == ProductionEntityID select entity.Userid;

                                                var childEntiymeberResult = ((tx.PersistenceManager.CommonRepository[marcomManagerExpire.User.TenantID].ExecuteQuery("SELECT DISTINCT  UserID  FROM  AM_Entity_Role_User WHERE  EntityID = " + ProductionEntityID + "")).Cast<Hashtable>().ToList());
                                                EntiymeberIDArr = childEntiymeberResult.Cast<Hashtable>().Select(a => (int)a["UserID"]).Distinct().ToArray();

                                                //if (arrobjMembers.Count() > 0)
                                                //    listEntityRoleUser = marcomManagerExpire.PlanningManager.GetMember(ProductionEntityID);
                                            }
                                        }

                                        ProductionActive = (from item in tx.PersistenceManager.ExpireHandlerRepository[marcomManagerExpire.User.TenantID].Query<EntityDao>() where item.Id == ProductionEntityID select item.Active).FirstOrDefault();
                                        entityTypeId = (from item in tx.PersistenceManager.ExpireHandlerRepository[marcomManagerExpire.User.TenantID].Query<EntityDao>() where item.Id == ProductionEntityID select item.Typeid).FirstOrDefault();
                                        tx.Commit();
                                        if (ProductionActive)
                                        {

                                            clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=1,UpdatedOn=GETDATE() WHERE  id=" + val.Id + " ", CommandType.Text);
                                            if (val.ActionType == 1)
                                            {
                                                JObject jobj = JObject.Parse(val.ActionSrcValue);
                                                AttributeActiondates = "";
                                                int[] asetids = new int[1];
                                                asetids[0] = val.ActionSrcID;
                                                var ActionStatus = Convert.ToInt16(jobj["Ispublish"].ToString());

                                                if (ProductionEntityID > 0 && sourcfrom == 1)
                                                {
                                                    ds2 = clsDb.MailData("SELECT  AssetTypeid  as typeid FROM   DAM_Asset da  WHERE  id= " + val.ActionSrcID + "", CommandType.Text);
                                                    if (ds2.Tables[0].Rows.Count > 0)
                                                        ds3 = clsDb.MailData("SELECT convert(varchar(10), Attr_" + val.AttributeID + ", 110) AttributeActiondate  FROM   MM_AttributeRecord_" + Convert.ToInt16(ds2.Tables[0].Rows[0]["typeid"]) + " WHERE  id= " + val.ActionSrcID + "", CommandType.Text);
                                                    if (ds3.Tables[0].Rows.Count > 0)
                                                        AttributeActiondates = ds3.Tables[0].Rows[0]["AttributeActiondate"].ToString();
                                                    if (AttributeActiondates != null && AttributeActiondates.Length > 0)
                                                    {
                                                        bool updatestatus = false;
                                                        bool sucessststus = false;
                                                        string userhavePublishAsses = "false";
                                                        userhavePublishAsses = marcomManagerExpire.DigitalAssetManager.CheckAccessToPublish(val.ActionownerId, val.ActionSrcID);
                                                        if (userhavePublishAsses == "true")
                                                        {
                                                            if (ActionStatus == 1)
                                                            {
                                                                updatestatus = true;
                                                                sucessststus = marcomManagerExpire.DigitalAssetManager.PublishAssets(asetids, updatestatus, val.ActionownerId);
                                                            }
                                                            else if (ActionStatus == 0)
                                                            {
                                                                updatestatus = false;
                                                                sucessststus = marcomManagerExpire.DigitalAssetManager.UnPublishAssets(asetids, updatestatus, val.ActionownerId);
                                                            }

                                                            if (sucessststus)
                                                            {
                                                                clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=2,UpdatedOn=GETDATE() WHERE  id=" + val.Id + " ", CommandType.Text);
                                                                continue;
                                                            }
                                                            else
                                                            {
                                                                clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=3,UpdatedOn=GETDATE() WHERE  id=" + val.Id + " ", CommandType.Text);
                                                                continue;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=3,UpdatedOn=GETDATE() WHERE  id=" + val.Id + " ", CommandType.Text);
                                                            continue;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=4,UpdatedOn=GETDATE()  WHERE  id=" + val.Id + " ", CommandType.Text);
                                                        continue;
                                                    }
                                                }

                                            }
                                            else if (val.ActionType == 2)
                                            {
                                                JObject jobj1 = JObject.Parse(val.ActionSrcValue);
                                                JObject jobj = (JObject)jobj1["ActionTaskSrcValue"];

                                                AttributeActiondates = "";



                                                if (ProductionEntityID > 0)
                                                {
                                                    int Tasklistid = 0;
                                                    int TaskID = 0;
                                                    if (sourcfrom == 1)
                                                    {
                                                        ds2 = clsDb.MailData("SELECT  AssetTypeid  as typeid FROM   DAM_Asset da  WHERE  id= " + val.ActionSrcID + "", CommandType.Text);
                                                        if (ds2.Tables[0].Rows.Count > 0)
                                                        {
                                                            mainTblQry = new StringBuilder();
                                                            mainTblQry.Append(" DECLARE @COLcount INT ");
                                                            mainTblQry.Append(" SET @COLcount=(SELECT isnull(COL_LENGTH('MM_AttributeRecord_" + Convert.ToInt16(ds2.Tables[0].Rows[0]["typeid"]) + "','Attr_" + val.AttributeID + "'),0)) ");
                                                            mainTblQry.Append(" if @COLcount > 0 ");
                                                            mainTblQry.Append(" SELECT  isnull(convert(varchar(10), Attr_" + val.AttributeID + ", 110),'-') AS AttributeActiondate  FROM   MM_AttributeRecord_" + Convert.ToInt16(ds2.Tables[0].Rows[0]["typeid"]) + " WHERE  id= " + val.ActionSrcID + " ");
                                                            mainTblQry.Append(" ELSE ");
                                                            mainTblQry.Append(" SELECT '-' AS AttributeActiondate  ");


                                                            ds3 = clsDb.MailData(mainTblQry.ToString(), CommandType.Text);
                                                            if (ds3.Tables[0].Rows.Count > 0)
                                                                AttributeActiondates = ds3.Tables[0].Rows[0]["AttributeActiondate"].ToString();
                                                            else
                                                            {
                                                                clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=4,UpdatedOn=GETDATE()  WHERE  id=" + val.Id + " ", CommandType.Text);
                                                                continue;
                                                            }
                                                            if (AttributeActiondates == "-")
                                                            {
                                                                clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=4,UpdatedOn=GETDATE()  WHERE  id=" + val.Id + " ", CommandType.Text);
                                                                continue;
                                                            }
                                                        }
                                                        else
                                                        { clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=4,UpdatedOn=GETDATE()  WHERE  id=" + val.Id + " ", CommandType.Text); continue; }
                                                    }
                                                    else if (sourcfrom == 2)
                                                    {

                                                        ds2 = clsDb.MailData("SELECT  typeid as typeid FROM   PM_Entity pe  WHERE  id= " + val.ActionSrcID + "", CommandType.Text);
                                                        if (ds2.Tables[0].Rows.Count > 0)
                                                            ds3 = clsDb.MailData("SELECT  isnull(convert(varchar(10), Attr_" + val.AttributeID + ", 110),'-')  AttributeActiondate  FROM   MM_AttributeRecord_" + Convert.ToInt16(ds2.Tables[0].Rows[0]["typeid"]) + " WHERE  id= " + val.ActionSrcID + "", CommandType.Text);
                                                        if (ds3.Tables[0].Rows.Count > 0)
                                                            AttributeActiondates = ds3.Tables[0].Rows[0]["AttributeActiondate"].ToString();
                                                        else
                                                        {
                                                            clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=4,UpdatedOn=GETDATE()  WHERE  id=" + val.Id + " ", CommandType.Text);
                                                            continue;
                                                        }
                                                        if (AttributeActiondates == "-")
                                                        {
                                                            clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=4,UpdatedOn=GETDATE()  WHERE  id=" + val.Id + " ", CommandType.Text);
                                                            continue;
                                                        }

                                                    }
                                                    else if (sourcfrom == 3)
                                                    {
                                                        ds2 = clsDb.MailData("SELECT  TaskTypeId as typeid  FROM  MM_EntityTaskType WHERE EntitytypeId IN(SELECT  TypeID FROM  PM_Entity WHERE  id =  " + val.ActionSrcID + ")", CommandType.Text);
                                                        if (ds2.Tables[0].Rows.Count > 0)
                                                            ds3 = clsDb.MailData("SELECT convert(varchar(10), Attr_" + val.AttributeID + ", 110) AttributeActiondate  FROM   MM_AttributeRecord_" + Convert.ToInt16(ds2.Tables[0].Rows[0]["typeid"]) + " WHERE  id= " + val.ActionSrcID + "", CommandType.Text);
                                                        if (ds3.Tables[0].Rows.Count > 0)
                                                            AttributeActiondates = ds3.Tables[0].Rows[0]["AttributeActiondate"].ToString();
                                                        else
                                                        {
                                                            clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=4,UpdatedOn=GETDATE()  WHERE  id=" + val.Id + " ", CommandType.Text);
                                                            continue;
                                                        }
                                                        if (AttributeActiondates == "-")
                                                        {
                                                            clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=4,UpdatedOn=GETDATE()  WHERE  id=" + val.Id + " ", CommandType.Text);
                                                            continue;
                                                        }

                                                    }
                                                    if (AttributeActiondates != null && AttributeActiondates.Length > 0)
                                                    {
                                                        ds2 = clsDb.MailData("SELECT  id as tasklistid,LOWER(NAME) as name1  FROM   TM_EntityTaskList  WHERE  EntityID=" + val.EntityID + "  AND  NAME='" + TaskListname.ToString() + "'", CommandType.Text);
                                                        if (ds2.Tables[0].Rows.Count > 0)
                                                        {
                                                            Tasklistid = Convert.ToInt16(ds2.Tables[0].Rows[0]["tasklistid"]);
                                                        }
                                                        else
                                                        {

                                                            Tuple<int, IEntityTaskList> var2 = marcomManagerExpire.TaskManager.InsertUpdateEntityTaskList(0, TaskListname, TaskListDescription, 0, ProductionEntityID, val.ActionownerId);
                                                            if (var2 != null)
                                                            {
                                                                Tasklistid = var2.Item1;
                                                            }
                                                        }

                                                        if (Tasklistid > 0)
                                                        {

                                                            JArray arrAttchObj = (JArray)jobj["TaskAttachments"];
                                                            JArray arrFilesObj = (JArray)jobj["TaskFiles"];
                                                            JArray arrobjMembers = (JArray)jobj["TaskMembers"];
                                                            JArray EntityPhaseStepDetails = (JArray)jobj["EntityPhaseStepDetails"];// for task pahses and steps
                                                            JArray EntityStepMembers = (JArray)jobj["StepMembers"];
                                                            JArray arrChkList = (JArray)jobj["AdminTaskCheckList"];
                                                            JArray arrperiods = new JArray();
                                                            IList<IEntityTask> listTask = new List<IEntityTask>();

                                                            IEntityTask taskval = new EntityTask();
                                                            taskval.Name = (string)jobj["Name"];
                                                            taskval.TaskListID = Tasklistid;
                                                            taskval.Description = (string)jobj["Description"];
                                                            taskval.Note = (string)jobj["Note"];
                                                            taskval.TaskType = (int)jobj["TaskType"];
                                                            taskval.EntityTaskListID = 0;
                                                            taskval.AssetId = (int)jobj["AssetID"];

                                                            string Dueexpirydate = "";
                                                            if (AttributeActiondates.Length > 0)
                                                                Dueexpirydate = AttributeActiondates;
                                                            else
                                                                Dueexpirydate = (string)jobj["DateActionexpiredate"];

                                                            string DueDateoption = (string)jobj["DueDateoptions"];
                                                            if (DueDateoption != null && DueDateoption.Length > 0)
                                                            {

                                                                DateTime startDate = new DateTime();
                                                                startDate = getorginalexpirtime(Dueexpirydate, DueDateoption, dateformate);
                                                                taskval.DueDate = startDate;
                                                            }
                                                            else
                                                            {
                                                                taskval.DueDate = null;

                                                            }

                                                            taskval.EntityID = Convert.ToInt32((string)jobj["ParentEntityID"]);

                                                            listTask.Add(taskval);

                                                            IList<ITaskMembers> listTaskMembers = new List<ITaskMembers>();
                                                            if (arrobjMembers != null)
                                                            {
                                                                foreach (var arm in arrobjMembers)
                                                                {
                                                                    //ITaskMembers entrole = new TaskMembers();
                                                                    if ((int)arm > 0)
                                                                    {
                                                                        bool exists = EntiymeberIDArr.Contains((int)arm);
                                                                        if (exists)
                                                                        {
                                                                            var exitenityuserid = listTaskMembers.Where(a => a.UserID == (int)arm).Select(a => a.UserID).FirstOrDefault();
                                                                            if (exitenityuserid == 0)
                                                                            {
                                                                                ITaskMembers entrole = new TaskMembers();
                                                                                entrole.RoleID = 4;
                                                                                entrole.UserID = (int)arm;
                                                                                entrole.ApprovalRount = 1;
                                                                                entrole.ApprovalStatus = null;
                                                                                listTaskMembers.Add(entrole);
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        if ((int)arm == (int)ExpireEntityRoles.Editor)
                                                                        {
                                                                            ds2 = clsDb.MailData("SELECT DISTINCT  UserID  FROM  AM_Entity_Role_User WHERE  EntityID =" + ProductionEntityID + "  AND  RoleID=(SELECT  id FROM  AM_EntityTypeRoleAcl WHERE  EntityTypeID=" + entityTypeId + " AND  EntityRoleID=" + (int)EntityRoles.Editer + ")", CommandType.Text);
                                                                        }
                                                                        else if ((int)arm == (int)ExpireEntityRoles.Viewer)
                                                                        {
                                                                            ds2 = clsDb.MailData("SELECT DISTINCT  UserID  FROM  AM_Entity_Role_User WHERE  EntityID =" + ProductionEntityID + "  AND  RoleID=(SELECT  id FROM  AM_EntityTypeRoleAcl WHERE  EntityTypeID=" + entityTypeId + " AND  EntityRoleID=" + (int)EntityRoles.Viewer + ")", CommandType.Text);
                                                                        }
                                                                        else if ((int)arm == (int)ExpireEntityRoles.ExternalParticipant)
                                                                        {
                                                                            ds2 = clsDb.MailData("SELECT DISTINCT  UserID  FROM  AM_Entity_Role_User WHERE  EntityID =" + ProductionEntityID + "  AND  RoleID=(SELECT  id FROM  AM_EntityTypeRoleAcl WHERE  EntityTypeID=" + entityTypeId + " AND  EntityRoleID=" + (int)EntityRoles.ExternalParticipate + ")", CommandType.Text);
                                                                        }
                                                                        if (ds2.Tables[0].Rows.Count > 0)
                                                                        {
                                                                            for (int m = 0; m < ds2.Tables[0].Rows.Count; m++)
                                                                            {
                                                                                int enityuserid = Convert.ToInt32(ds2.Tables[0].Rows[m]["UserID"]);
                                                                                var exitenityuserid = listTaskMembers.Where(a => a.UserID == (int)enityuserid).Select(a => a.UserID).FirstOrDefault();
                                                                                if (exitenityuserid == 0)
                                                                                {
                                                                                    ITaskMembers entrole = new TaskMembers();
                                                                                    entrole.RoleID = 4;
                                                                                    entrole.UserID = (int)exitenityuserid;
                                                                                    entrole.ApprovalRount = 1;
                                                                                    entrole.ApprovalStatus = null;
                                                                                    listTaskMembers.Add(entrole);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                if (listTaskMembers.Count() == 0)
                                                                {

                                                                    ITaskMembers entrole = new TaskMembers();
                                                                    entrole.RoleID = 4;
                                                                    entrole.UserID = (int)val.ActionownerId;
                                                                    entrole.ApprovalRount = 1;
                                                                    entrole.ApprovalStatus = null;
                                                                    listTaskMembers.Add(entrole);
                                                                }
                                                            }
                                                            else if (arrobjMembers == null)
                                                            {
                                                                // listTaskMembers = null;
                                                            }


                                                            IList<INewTaskAttachments> listTaskattachment = new List<INewTaskAttachments>();



                                                            IList<IFile> listFiles = new List<IFile>();


                                                            listTaskattachment = null;

                                                            JArray taskAttribue = (JArray)jobj["AttributeData"];
                                                            IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                                                            if (taskAttribue != null)
                                                            {
                                                                foreach (var objAttr in taskAttribue)
                                                                {

                                                                    List<object> listObj = new List<object>();
                                                                    IAttributeData entityAttr = marcomManagerExpire.PlanningManager.AttributeDataservice();
                                                                    entityAttr.ID = (int)objAttr["AttributeID"];
                                                                    entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                                                                    entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                                                                    entityAttr.Level = (int)objAttr["Level"];
                                                                    if ((dynamic)objAttr["NodeID"].Count() > 0)
                                                                    {
                                                                        foreach (var obj in (dynamic)objAttr["NodeID"])
                                                                        {
                                                                            object valueObj = new object();
                                                                            valueObj = (dynamic)obj;
                                                                            listObj.Add(valueObj);
                                                                            if ((int)objAttr["AttributeTypeID"] == (int)AttributesList.Period)
                                                                            {
                                                                                arrperiods.Add(valueObj);
                                                                            }
                                                                        }
                                                                        entityAttr.Value = listObj;
                                                                        if ((string)objAttr["Value"] != "-1")
                                                                            entityAttr.SpecialValue = Convert.ToString(objAttr["Value"].ToString());
                                                                        else
                                                                            entityAttr.SpecialValue = "";
                                                                    }
                                                                    else
                                                                    {
                                                                        if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                                                        {
                                                                            entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                                                        }
                                                                        if (objAttr["NodeID"].Type == JTokenType.Date)
                                                                        {
                                                                            entityAttr.Value = (objAttr["NodeID"].ToString());//Newtonsoft.Json.JsonConvert.DeserializeObject<string>(obj["Value"].ToString());
                                                                        }
                                                                        if (objAttr["NodeID"].Type == JTokenType.Integer)
                                                                        {
                                                                            entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                                                        }
                                                                        else
                                                                            entityAttr.Value = objAttr["NodeID"].ToString();
                                                                    }
                                                                    listattributevalues.Add(entityAttr);
                                                                }
                                                            }
                                                            else if (taskAttribue == null)
                                                            {
                                                                listattributevalues = null;
                                                            }

                                                            IList<IEntityPeriod> listEntityperiods = new List<IEntityPeriod>();
                                                            if (arrperiods != null)
                                                            {
                                                                foreach (var arrp in arrperiods)
                                                                {
                                                                    if (arrp.Count() > 0)
                                                                    {
                                                                        string NewstartDate = (arrp["startDate"].ToString()) == "" ? null : ((DateTime.ParseExact(arrp["startDate"].ToString(), "dd-MM-yyyy", null)).ToString("yyyy-MM-dd") == "" ? null : (DateTime.ParseExact(arrp["startDate"].ToString(), "dd-MM-yyyy", null)).ToString("yyyy-MM-dd"));
                                                                        string NewendDate = (arrp["endDate"].ToString()) == "" ? null : ((DateTime.ParseExact(arrp["endDate"].ToString(), "dd-MM-yyyy", null)).ToString("yyyy-MM-dd") == "" ? null : (DateTime.ParseExact(arrp["endDate"].ToString(), "dd-MM-yyyy", null)).ToString("yyyy-MM-dd"));
                                                                        DateTime startDate1 = new DateTime();
                                                                        DateTime endDate = new DateTime();
                                                                        if (DateTime.TryParse(NewstartDate, out startDate1) && DateTime.TryParse(NewendDate, out endDate))
                                                                        {

                                                                            IEntityPeriod enti = marcomManagerExpire.PlanningManager.Entityperiodservice();
                                                                            enti.Startdate = startDate1;
                                                                            enti.EndDate = endDate;
                                                                            enti.Description = (string)arrp["comment"];
                                                                            enti.SortOrder = (int)arrp["sortorder"];
                                                                            listEntityperiods.Add(enti);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            else if (arrperiods == null)
                                                            {
                                                                listEntityperiods = null;
                                                            }

                                                            IList<IEntityTaskCheckList> AdminTaskChkLst = new List<IEntityTaskCheckList>();


                                                            if (arrChkList != null)
                                                            {
                                                                foreach (var arm in arrChkList)
                                                                {
                                                                    if ((string)arm["Name"] != null)
                                                                    {
                                                                        IEntityTaskCheckList adminChklist = marcomManagerExpire.TaskManager.EntityTaskCheckListService();
                                                                        adminChklist.Name = (string)arm["Name"];
                                                                        adminChklist.SortOrder = 1;
                                                                        AdminTaskChkLst.Add(adminChklist);
                                                                    }
                                                                }
                                                            }
                                                            else if (arrChkList == null)
                                                            {
                                                                AdminTaskChkLst = null;
                                                            }
                                                            IList<TaskMembersDao> taskmemberDao = new List<TaskMembersDao>();
                                                            IList<EntitytaskphasesDao> phaseDao = new List<EntitytaskphasesDao>();
                                                            IList<EntitytaskstepsDao> stepsDao = new List<EntitytaskstepsDao>();
                                                            Tuple<int, IEntityTask> var = marcomManagerExpire.TaskManager.InsertEntityTaskWithAttachments((int)jobj["ParentEntityID"], (int)jobj["TaskType"], (int)jobj["Typeid"], (string)jobj["Name"], listTask, listTaskattachment, listFiles, listTaskMembers, listattributevalues, arrAttchObj, AdminTaskChkLst, listEntityperiods, val.ActionownerId, null, jobj["PhaseStepDetails"] == null ? new JArray() : (JArray)jobj["PhaseStepDetails"], null);
                                                            if (var != null)
                                                            {
                                                                if (var.Item1 > 0)
                                                                    clsDb.MailData("UPDATE  EH_ActionInitiator SET STATUS=2,UpdatedOn=GETDATE() WHERE  id=" + val.Id + " ", CommandType.Text);
                                                                else
                                                                    clsDb.MailData("UPDATE  EH_ActionInitiator SET STATUS=3,UpdatedOn=GETDATE() WHERE  id=" + val.Id + " ", CommandType.Text);
                                                            }

                                                        }
                                                    }
                                                    else
                                                    {
                                                        clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=4,UpdatedOn=GETDATE()  WHERE  id=" + val.Id + " ", CommandType.Text);
                                                    }



                                                }


                                            }
                                            else if (val.ActionType == 3)
                                            {
                                                JObject jobj1 = JObject.Parse(val.ActionSrcValue);
                                                JObject jobj = (JObject)jobj1["ActionTaskSrcValue"];
                                                AttributeActiondates = "";
                                                if (sourcfrom == 1)
                                                {
                                                    ds2 = clsDb.MailData("SELECT  AssetTypeid  as typeid FROM   DAM_Asset da  WHERE  id= " + val.ActionSrcID + "", CommandType.Text);
                                                    if (ds2.Tables[0].Rows.Count > 0)
                                                        ds3 = clsDb.MailData("SELECT convert(varchar(10), Attr_" + val.AttributeID + ", 110) AttributeActiondate  FROM   MM_AttributeRecord_" + Convert.ToInt16(ds2.Tables[0].Rows[0]["typeid"]) + " WHERE  id= " + val.ActionSrcID + "", CommandType.Text);
                                                    if (ds3.Tables[0].Rows.Count > 0)
                                                        AttributeActiondates = ds3.Tables[0].Rows[0]["AttributeActiondate"].ToString();
                                                }
                                                else if (sourcfrom == 2)
                                                {
                                                    ds2 = clsDb.MailData("SELECT  typeid as typeid FROM   PM_Entity pe  WHERE  id= " + val.ActionSrcID + "", CommandType.Text);
                                                    if (ds2.Tables[0].Rows.Count > 0)
                                                        ds3 = clsDb.MailData("SELECT convert(varchar(10), Attr_" + val.AttributeID + ", 110) AttributeActiondate  FROM   MM_AttributeRecord_" + Convert.ToInt16(ds2.Tables[0].Rows[0]["typeid"]) + " WHERE  id= " + val.ActionSrcID + "", CommandType.Text);
                                                    if (ds3.Tables[0].Rows.Count > 0)
                                                        AttributeActiondates = ds3.Tables[0].Rows[0]["AttributeActiondate"].ToString();
                                                }
                                                else if (sourcfrom == 3)
                                                {
                                                    ds2 = clsDb.MailData("SELECT  TaskTypeId as typeid  FROM  MM_EntityTaskType WHERE EntitytypeId IN(SELECT  TypeID FROM  PM_Entity WHERE  id =  " + val.ActionSrcID + ")", CommandType.Text);
                                                    if (ds2.Tables[0].Rows.Count > 0)
                                                        ds3 = clsDb.MailData("SELECT convert(varchar(10), Attr_" + val.AttributeID + ", 110) AttributeActiondate  FROM   MM_AttributeRecord_" + Convert.ToInt16(ds2.Tables[0].Rows[0]["typeid"]) + " WHERE  id= " + val.ActionSrcID + "", CommandType.Text);
                                                    if (ds3.Tables[0].Rows.Count > 0)
                                                        AttributeActiondates = ds3.Tables[0].Rows[0]["AttributeActiondate"].ToString();
                                                }
                                                if (ProductionEntityID > 0 && AttributeActiondates.Length > 0)
                                                {

                                                    JArray metaAttribue = (JArray)jobj["AttributeData"];

                                                    IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                                                    if (metaAttribue != null)
                                                    {
                                                        foreach (var objAttr in metaAttribue)
                                                        {

                                                            List<object> listObj = new List<object>();
                                                            IAttributeData entityAttr = marcomManagerExpire.PlanningManager.AttributeDataservice();
                                                            entityAttr.ID = (int)objAttr["AttributeID"];
                                                            entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                                                            entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                                                            entityAttr.Level = (int)objAttr["Level"];
                                                            if ((dynamic)objAttr["NodeID"].Count() > 0)
                                                            {
                                                                foreach (var obj in (dynamic)objAttr["NodeID"])
                                                                {
                                                                    object valueObj = new object();
                                                                    valueObj = (dynamic)obj;
                                                                    listObj.Add(valueObj);
                                                                }
                                                                entityAttr.Value = listObj;
                                                                if ((string)objAttr["Value"] != "-1")
                                                                    entityAttr.SpecialValue = Convert.ToString(objAttr["Value"].ToString());
                                                                else
                                                                    entityAttr.SpecialValue = "";
                                                            }
                                                            else
                                                            {
                                                                if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                                                {
                                                                    entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                                                }
                                                                if (objAttr["NodeID"].Type == JTokenType.Date)
                                                                {
                                                                    entityAttr.Value = (objAttr["NodeID"].ToString());//Newtonsoft.Json.JsonConvert.DeserializeObject<string>(obj["Value"].ToString());
                                                                }
                                                                if (objAttr["NodeID"].Type == JTokenType.Integer)
                                                                {
                                                                    entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                                                }
                                                                else
                                                                    entityAttr.Value = objAttr["NodeID"].ToString();
                                                            }
                                                            listattributevalues.Add(entityAttr);
                                                        }
                                                    }
                                                    //List<object> obj1 = new List<object>();
                                                    //if (jrnewValue != null)
                                                    //{
                                                    //    foreach (var obj in jrnewValue)
                                                    //    {
                                                    //        if (obj.Type == JTokenType.Boolean)
                                                    //        {
                                                    //            obj1.Add(Convert.ToBoolean(obj.ToString()));
                                                    //        }
                                                    //        else if (obj.Type == JTokenType.Date)
                                                    //        {
                                                    //            obj1.Add(obj.ToString());

                                                    //        }
                                                    //        else if (obj.Type == JTokenType.Integer)
                                                    //        {

                                                    //            obj1.Add(int.Parse(obj.ToString()));

                                                    //        }
                                                    //        else
                                                    //            obj1.Add(obj.ToString());
                                                    //    }
                                                    //}
                                                    bool updatestatus = marcomManagerExpire.ExpireHandlerManager.ExpiryupdateEntityAttribute(listattributevalues, ProductionEntityID, val.ActionownerId);
                                                    if (updatestatus)
                                                        clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=2,UpdatedOn=GETDATE()  WHERE  id=" + val.Id + " ", CommandType.Text);
                                                    else
                                                        clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=3,UpdatedOn=GETDATE()  WHERE  id=" + val.Id + " ", CommandType.Text);
                                                }
                                                else
                                                { clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=4,UpdatedOn=GETDATE()  WHERE  id=" + val.Id + " ", CommandType.Text); }

                                            }
                                        }
                                        else
                                            clsDb.MailData("UPDATE  EH_ActionInitiator  SET STATUS=4,UpdatedOn=GETDATE()  WHERE  id=" + val.Id + " ", CommandType.Text);
                                    }

                                }
                                //if (SourcetypeID > 0)
                                //{
                                //    DateTime MyDateTime1 = getorginalexpirtime(DateActionexpiredate, "-1");

                                //    string updateCostcentreQuery = "UPDATE MM_AttributeRecord_" + SourcetypeID + " SET Attr_" + Convert.ToInt32(SystemDefinedAttributes.Dateaction) + " ='" + MyDateTime1.ToString() + "' WHERE ID=" + SourceID + "";
                                //    tx.PersistenceManager.ExpireHandlerRepository[marcomManagerExpire.User.TenantID].ExecuteQuery(updateCostcentreQuery);
                                //}
                                // tx.Commit();
                                //var Filedetails = (from tt in tx.PersistenceManager.CommonRepository.Query<DAMFileDao>() where tt.ID == previewfile.Key select tt).FirstOrDefault();
                                //string applicationPath = ConfigurationManager.AppSettings["OriginalXMLpath"];
                                string applicationPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();


                            }
                            catch (Exception ex)
                            {

                                LogHandler.LogInfo("exception generated Preview sending files" + ex.Message, LogHandler.LogType.Notify);
                                LogHandler.LogInfo("exception generated Preview sending files" + ex.StackTrace, LogHandler.LogType.Notify);
                                LogHandler.LogInfo("exception generated Preview sending files" + ex.InnerException.ToString(), LogHandler.LogType.Notify);
                            }

                        }

                        tx.Commit();
                    }

                }


                //return obj1;
            }
            catch (Exception ex)
            {
                LogHandler.LogInfo("exception generated Preview sending files" + ex.Message, LogHandler.LogType.Notify);
                // return null;
            }
        }

        public DateTime getorginalexpirtime(string Expiredate, string Expireoptions, string formate)
        {
            int selectedoption;
            string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt", 
                                           "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss", 
                                           "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt", 
                                            "M/d/yyyy h:mm", "M/d/yyyy h:mm", 
                                             "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm",
                                             "M/d/yyyy", "d/M/yyyy", "M-d-yyyy",
                                            "d-M-yyyy", "d-MMM-yy", "d-MMMM-yyyy",
                                            "dd/MM/yyyy","dd-MM-yyyy","yyyy-MM-dd",
                                             };
            DateTime result;
            DateTime MyDateTime;
            DateTime.TryParseExact(Expiredate, formats, System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out MyDateTime);
            selectedoption = Convert.ToInt32(Expireoptions);
            switch (selectedoption)
            {
                case 1:
                    result = MyDateTime.AddDays(+1);
                    break;
                case 2:
                    result = MyDateTime.AddDays(+2);
                    break;
                case 3:
                    result = MyDateTime.AddDays(+3);
                    break;
                case 4:
                    result = MyDateTime.AddDays(+4);
                    break;
                case 5:
                    result = MyDateTime.AddDays(+5);
                    break;
                case 6:
                    result = MyDateTime.AddDays(+6);
                    break;
                case 7:
                    result = MyDateTime.AddDays(+7);
                    break;
                case 8:
                    result = MyDateTime.AddDays(+14);
                    break;
                case 9:
                    result = MyDateTime.AddDays(+21);
                    break;
                case 10:
                    result = MyDateTime.AddMonths(+1);
                    break;
                case 11:
                    result = MyDateTime.AddMonths(+2);
                    break;
                case 12:
                    result = MyDateTime.AddMonths(+3);
                    break;
                case 13:
                    result = MyDateTime.AddMonths(+4);
                    break;
                case 14:
                    result = MyDateTime.AddMonths(+5);
                    break;
                case 15:
                    result = MyDateTime.AddMonths(+6);
                    break;
                case 16:
                    result = MyDateTime.AddMonths(+7);
                    break;
                case 17:
                    result = MyDateTime.AddMonths(+8);
                    break;
                case 18:
                    result = MyDateTime.AddMonths(+9);
                    break;
                case 19:
                    result = MyDateTime.AddMonths(+10);
                    break;
                case 20:
                    result = MyDateTime.AddMonths(+11);
                    break;
                case 21:
                    result = MyDateTime.AddMonths(+12);
                    break;
                case 22:
                    result = MyDateTime.AddDays(-1);
                    break;
                case 23:
                    result = MyDateTime.AddDays(-2);
                    break;
                case 24:
                    result = MyDateTime.AddDays(-3);
                    break;
                case 25:
                    result = MyDateTime.AddDays(-4);
                    break;
                case 26:
                    result = MyDateTime.AddDays(-5);
                    break;
                case 27:
                    result = MyDateTime.AddDays(-6);
                    break;
                case 28:
                    result = MyDateTime.AddDays(-7);
                    break;
                case 29:
                    result = MyDateTime.AddDays(-14);

                    break;
                case 30:
                    result = MyDateTime.AddDays(-21);

                    break;
                case 31:
                    result = MyDateTime.AddMonths(-1);

                    break;
                case 32:
                    result = MyDateTime.AddMonths(-2);

                    break;
                case 33:
                    result = MyDateTime.AddMonths(-3);
                    break;
                case 34:
                    result = MyDateTime.AddMonths(-4);

                    break;
                case 35:
                    result = MyDateTime.AddMonths(-5);
                    break;
                case 36:
                    result = MyDateTime.AddMonths(-6);
                    break;
                case 37:
                    result = MyDateTime.AddMonths(-7);
                    break;
                case 38:
                    result = MyDateTime.AddMonths(-8);
                    break;
                case 39:
                    result = MyDateTime.AddMonths(-9);
                    break;
                case 40:
                    result = MyDateTime.AddMonths(-10);
                    break;
                case 41:
                    result = MyDateTime.AddMonths(-11);
                    break;
                case 42:
                    result = MyDateTime.AddMonths(-12);
                    break;
                default:
                    result = MyDateTime;
                    break;
            }

            return result;
        }

        public void ExpireInitializeSchedulers(int TenantID)
        {

            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
            BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("ExpireInitializeSchedulers start" + DateTime.Now, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            System.Xml.XmlElement xelRoot = xdcDocument.DocumentElement;
            System.Xml.XmlNodeList xnlNodes = xelRoot.SelectNodes("/Tenants/Tenant");
            int tenantid;
            string TenantsFilePath = string.Empty;
            //loop through each tenant 
            foreach (System.Xml.XmlNode xndNode in xnlNodes)
            {
                ApplicationSettings.Clear();
                TenantsFilePath = xndNode["FilePath"].InnerText;
                //tenantid = Int16.Parse(xndNode["TenantID"].InnerText);
                tenantid = TenantID;

                Guid systemSession = MarcomManagerFactory.GetSystemSession(tenantid);
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, (Guid)systemSession);

                string xelementName = "MailConfig";
                XDocument adminXmlDoc = MarcomCache<XDocument>.ReadXDocument(xmlType.Admin, TenantID);
                XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);
                //The Key is root node current Settings


                //var xelementFilepath = XElement.Load(xmlpath);
                var xmlElement = xelementFilepath.Element(xelementName);
                foreach (var ele in xmlElement.Descendants())
                {
                    if (ele.Name.ToString() == "SenderConfig")
                    {
                        foreach (var cc in ele.Descendants())
                        {
                            ApplicationSettings.Add(cc.Name.ToString(), cc.Value.ToString());
                        }
                    }
                }

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, MarcomManagerFactory.GetSystemSession(tenantid));



                //objseacrhScheduler = new SearchEngineScheduler(ApplicationSettings);
                //objseacrhScheduler.Start(120, tenantid);


                //Expire handler scheduler logic
                var DefaultExpirysetting = adminXmlDoc.Descendants("Expirysetting").FirstOrDefault();
                if (DefaultExpirysetting != null)
                {
                    BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("ExpireInitializeSchedulers start inside Expirysetting" + DateTime.Now, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                    var DefaultExpiryActionON = adminXmlDoc.Descendants("Expirysetting").Descendants("ActionON").FirstOrDefault();
                    if (DefaultExpiryActionON != null)
                    {
                        var ExecutionActionONResponse = adminXmlDoc.Descendants("Expirysetting").Descendants("ActionON").ElementAt(0).Value;
                        if (ExecutionActionONResponse == "1")
                        {
                            BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("ExpireInitializeSchedulers start inside Expirysetting ActionON" + DateTime.Now, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                            var DefaultExpirysettingExecutionTime = adminXmlDoc.Descendants("Expirysetting").Descendants("ActionDateExecutionTime").FirstOrDefault();
                            if (DefaultExpirysettingExecutionTime != null)
                            {
                                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("ExpireInitializeSchedulers start inside Expirysetting ActionON ActionDateExecutionTime" + DateTime.Now, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                                var ExecutionTimeResponse = adminXmlDoc.Descendants("Expirysetting").Descendants("ActionDateExecutionTime").ElementAt(0).Value;
                                //string a = "05:30";
                                string a = ExecutionTimeResponse;

                                DateTime Schedulstarttime = new DateTime();
                                Schedulstarttime = DateTime.ParseExact(a, "H:m", null);

                                DateTime time = DateTime.Now;
                                TimeSpan span = time.Subtract(Schedulstarttime);
                                if (span.Hours <= 1 && span.Hours >= 0)
                                {
                                    BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("ExpireInitializeSchedulers start inside Expirysetting ActionON ActionDateExecutionTime time condion sucess" + DateTime.Now, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                                    //time.ToString("HH:mm:ss", CultureInfo.InvariantCulture);
                                    //objExpireScheduler = new ExpireScheduler(ApplicationSettings);
                                    //objExpireScheduler.Start(600, tenantid);
                                    HandleSendExpire(tenantid);
                                    BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("ExpireScheduler start" + DateTime.Now, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                                }

                            }
                        }
                    }
                }

            }

        }

    }

}
