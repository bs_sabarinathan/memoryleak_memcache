﻿(function (ng, app) {
    "use strict";

    function muiadminattributegroupCtrl($scope, $timeout, $http, $compile, $resource, $cookies, $location, $window, $translate, MetadataService) {
        $scope.DragDropvalues = '';
        $scope.EnableAttributeAdd = false;
        $scope.EnableAttributeUpdate = false;
        $scope.attributeRelationarr = [];
        $scope.attributegroupdata = [];
        $scope.attrgrp = {
            attributegroupID: 0,
            attributegroupCaption: '',
            attributegroupDescription: '',
            attributerelationID: 0,
            attributerelationCaption: '',
            ddlSelectedAttributeID: 0,
            IsPredefined: false,
            attributeList: []
        };
        var DragDrop = function () {
            $('#attributestable').tableDnD({
                onDrop: function (table, row) {
                    $scope.DragDropvalues = table.tBodies[0].rows;
                }
            });
        }
        if ($scope.CurrentMetadataVersionInfo == 0)
            $scope.IsOlderMetadataVersion.IsOlderVersion = false;
        else
            $scope.IsOlderMetadataVersion.IsOlderVersion = true;

        $scope.IsPredefinedData = false;

        $scope.setPredefinedchanges = function (event) {
            var checkbox = event.target;
            if ($scope.attrgrp.IsPredefined != undefined) {
                if ($scope.attrgrp.IsPredefined == false) {
                    $timeout(function () {
                        $scope.attrgrp.IsPredefined = true;
                    }, 10);
                }
                else {
                    $timeout(function () {
                        $scope.attrgrp.IsPredefined = false;
                    }, 10);
                }
            }
        }

        $scope.setPredefinedSearchable = function (event, IsSearchable, attributeId) {

            var res = $.grep($scope.attributeRelationarr, function (e) { return e.AttributeID == attributeId });
            if (res.length > 0) {
                if (res[0].IsSearchable == true) {
                    $timeout(function () {
                        res[0].IsSearchable = false;
                    }, 10);
                }
                else {
                    $timeout(function () {
                        res[0].IsSearchable = true;
                    }, 10);
                }
            }
        }

        $scope.setShowSearchResult = function (event, ShowAsColumn, attributeId) {

            var res = $.grep($scope.attributeRelationarr, function (e) { return e.AttributeID == attributeId });
            if (res.length > 0) {
                if (res[0].ShowAsColumn == true) {
                    $timeout(function () {
                        res[0].ShowAsColumn = false;
                    }, 10);
                }
                else {
                    $timeout(function () {
                        res[0].ShowAsColumn = true;
                    }, 10);
                }
            }
        }

        $scope.tempAttributeID = 0;
        $scope.LoadAttributeGroup = function () {
            $scope.attributegroupdata = [];
            MetadataService.GetAttributeGroup().then(function (listAttributeGroups) {
                $scope.attributegroupdata = listAttributeGroups.Response;
            });
        };
        $scope.LoadAttributeGroup();
        $scope.editattributegrouptableInPopup = '<a data-toggle="modal" ng-click="OnRowClickUpdateAttributeGroup(row)" data-toggle="modal" data-target="#attributegroupModal"><i class="icon-edit" style="padding-left: 10px; font-size: 14px; padding-top: 8px; display: inline-block; color: rgb(0, 136, 204); vertical-align:middle;"></i></a> ';
        $scope.attributegroupdelete = '<a data-toggle="modal" ng-click="OnRowClickDeleteAttributeGroup(row)" ng-show="(IsOlderMetadataVersion.IsOlderVersion==true)" data-toggle="modal"><i class="icon-remove" style="padding-left: 16px; padding-top: 8px; display: inline-block; color: rgb(200, 48, 37); vertical-align:middle;"></i></a> ';
        $scope.filterOptions = {
            filterText: ''
        };
        $scope.gridattributegroup = {
            data: 'attributegroupdata',
            enablePinning: false,
            filterOptions: $scope.filterOptions,
            columnDefs: [{
                field: "Id",
                displayName: $translate.instant('LanguageContents.Res_69.Caption'),
                width: 100
            }, {
                field: "Caption",
                displayName: $translate.instant('LanguageContents.Res_5046.Caption'),
                width: 180
            }, {
                field: "Description",
                displayName: $translate.instant('LanguageContents.Res_22.Caption'),
                width: 180
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.editattributegrouptableInPopup,
                width: 30
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.attributegroupdelete,
                width: 40
            }]
        };
        MetadataService.GetAttribute().then(function (res) {
            $scope.Attrs = [];
            $scope.attributes = res.Response;
            $scope.attrgrp.attributeList = $.grep(res.Response, function (e) {
                return (e.IsSpecial == false && e.AttributeTypeID != 10 && e.AttributeTypeID != 7);
            });
            $.each($scope.attrgrp.attributeList, function (i, el) {
                $scope.Attrs.push({
                    "id": el.Id,
                    "text": el.Caption,
                });
            });
            $scope.optionsSrc = $scope.Attrs;
            $scope.formatAttribute = function (item) {
                return "<span style=\"background-color: #ccc\" class=\"eicon-s margin-right5x\">" + item.id + "</span>;" + item.text;
            };
            $scope.formatAttributeSelection = function (item) {
                if ($scope.attrgrp.ddlSelectedAttributeID != "")
                    return "<span style=\"background-color: #ccc; margin-top: -3px;\" class=\"eicon-s margin-right5x\">" + item.id + "</span>" + item.text;
            };
            $scope.attributeOptionsConfig = {
                formatResult: $scope.formatAttribute,
                formatSelection: $scope.formatAttributeSelection
            };
        });
        $scope.formatAttribute = function (item) {
            return "<span style=\"background-color: #ccc\" class=\"eicon-s margin-right5x\">" + item.id + "</span>" + item.text;
        };
        $scope.formatAttributeSelection = function (item) {
            if ($scope.attrgrp.ddlSelectedAttributeID != "")
                return "<span style=\"background-color: #ccc; margin-top: -3px;\" class=\"eicon-s margin-right5x\">" + item.id + "</span>" + item.text;
        };
        $scope.attributeOptionsConfig = {
            formatResult: $scope.formatAttribute,
            formatSelection: $scope.formatAttributeSelection
        };
        $scope.addNewAttributeGroup = function () {
            $("#attributegroupModal").modal('show');
            $scope.attrgrp.attributegroupCaption = '';
            $scope.attrgrp.attributegroupDescription = '';
            $scope.attrgrp.attributerelationCaption = '';
            $scope.attrgrp.ddlSelectedAttributeID = null;
            $scope.attrgrp.IsPredefined = false;
            $scope.attributeRelationarr = [];
            $scope.attributeRelationarr.push({
                "ID": 0,
                "AttributeID": 76,
                "AttributeCaption": 'Header',
                "AttributeRelationCaption": 'Name',
                "SortOrder": 0,
                "IsSearchable": 0,
                "ShowAsColumn": 0
            });
            $scope.EnableAttributeAdd = true;
            $scope.EnableAttributeUpdate = false;
            $scope.attrgrp.attributegroupID = 0;
            var entitytypeValues = [
                ['#attrgrpcaption', 'presence', 'Please Enter the Attribute group caption']
            ];
            $("#attributegroupPage1").nod(entitytypeValues, {
                'delay': 200,
                'submitBtnSelector': '#btnTemp',
                'disableSubmitBtn': 'false',
                'silentSubmit': 'true'
            });
            $("#attributegroupPage1").addClass('notvalidate');
            $timeout(function () {
                $('#attrgrpcaption').focus().select();
            }, 1000);
        };
        $scope.addnewAttributeRelationToGrid = function () {
            if ($scope.attrgrp.ddlSelectedAttributeID == null) {
                bootbox.alert($translate.instant('LanguageContents.Res_1821.Caption'));
                return false
            }
            if ($scope.attrgrp.attributerelationCaption == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_2057.Caption'));
                return false
            }
            if ($scope.attributeRelationarr != undefined && $scope.attributeRelationarr != "") {
                if ($.grep($scope.attributeRelationarr, function (e) {
                        return e.AttributeID == $scope.attrgrp.ddlSelectedAttributeID;
                }).length > 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1822.Caption'));
                    return false;
                }
            }
            var attributerelationCaption = $.grep($scope.attrgrp.attributeList, function (e) {
                return e.Id == parseInt($scope.attrgrp.ddlSelectedAttributeID);
            })[0].Caption;
            $scope.attributeRelationarr.push({
                "ID": 0,
                "AttributeID": $scope.attrgrp.ddlSelectedAttributeID,
                "AttributeCaption": attributerelationCaption,
                "AttributeRelationCaption": $scope.attrgrp.attributerelationCaption,
                "SortOrder": 0,
                "IsSearchable": 0,
                "ShowAsColumn": 0
            });
            $scope.attrgrp.ddlSelectedAttributeID = 0;
            $scope.attrgrp.attributerelationCaption = '';
            $scope.attrgrp.IsPredefined = $scope.attrgrp.IsPredefined;
            $timeout(function () {
                DragDrop();
            }, 200);
        };
        $scope.UpdateAttributeRelationAndLoadToGrid = function () {
            if ($scope.attrgrp.ddlSelectedAttributeID == null) {
                bootbox.alert($translate.instant('LanguageContents.Res_1821.Caption'));
                return false
            }
            if ($scope.attrgrp.attributerelationCaption == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_2057.Caption'));
                return false
            }
            if ($scope.attributeRelationarr != undefined && $scope.attributeRelationarr != "") {
                if ($scope.tempAttributeID != $scope.attrgrp.ddlSelectedAttributeID) {
                    if ($.grep($scope.attributeRelationarr, function (e) {
                            return e.AttributeID == $scope.attrgrp.ddlSelectedAttributeID;
                    }).length > 0) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1822.Caption'));
                        $scope.attrgrp.attributerelationCaption = '';
                        $scope.attrgrp.ddlSelectedAttributeID = null;
                        $scope.attrgrp.IsPredefined = false;
                        $scope.EnableAttributeAdd = true;
                        $scope.EnableAttributeUpdate = false;
                        return false;
                    }
                }
            }
            var attributeCaption = $.grep($scope.attrgrp.attributeList, function (e) {
                return e.Id == parseInt($scope.attrgrp.ddlSelectedAttributeID);
            })[0].Caption;
            $.grep($scope.attributeRelationarr, function (e) {
                return e.AttributeID == parseInt($scope.tempAttributeID);
            })[0].AttributeCaption = attributeCaption;
            $.grep($scope.attributeRelationarr, function (e) {
                return e.AttributeID == parseInt($scope.tempAttributeID);
            })[0].AttributeRelationCaption = $scope.attrgrp.attributerelationCaption;
            $.grep($scope.attributeRelationarr, function (e) {
                return e.AttributeID == parseInt($scope.tempAttributeID);
            })[0].AttributeID = $scope.attrgrp.ddlSelectedAttributeID;
            $scope.attrgrp.ddlSelectedAttributeID = 0;
            $scope.attrgrp.attributerelationCaption = '';
            $scope.attrgrp.IsPredefined = false;
            $scope.tempAttributeID = 0;
            $scope.EnableAttributeAdd = true;
            $scope.EnableAttributeUpdate = false;
            $timeout(function () {
                DragDrop();
            }, 200);
        };
        $scope.ClearAttributeRelationCtrls = function () {
            $scope.attrgrp.attributerelationCaption = '';
            $scope.attrgrp.ddlSelectedAttributeID = 0;
            $scope.EnableAttributeAdd = true;
            $scope.attrgrp.IsPredefined = false;
            $scope.EnableAttributeUpdate = false;
        }
        $scope.LoadAttributeRelationOnGridClick = function (row) {
            var attributerelationCaption = $.grep($scope.attributeRelationarr, function (e) {
                return e.AttributeID == parseInt(row.AttributeID);
            })[0];
            $scope.attrgrp.ddlSelectedAttributeID = attributerelationCaption.AttributeID;
            $scope.attrgrp.attributerelationCaption = attributerelationCaption.AttributeRelationCaption;
            $scope.tempAttributeID = row.AttributeID;
            $scope.EnableAttributeAdd = false;
            $scope.EnableAttributeUpdate = true;
        };
        $scope.ChangeAttribute = function () {
            if ($scope.attrgrp.ddlSelectedAttributeID != undefined && $scope.attrgrp.ddlSelectedAttributeID != null) {
                $scope.attrgrp.attributerelationCaption = $.grep($scope.attrgrp.attributeList, function (e) {
                    return e.Id == parseInt($scope.attrgrp.ddlSelectedAttributeID);
                })[0].Caption;
            }
        };
        $scope.OnRowClickUpdateAttributeGroup = function (row) {
            $scope.attrgrp.attributerelationCaption = '';
            $scope.attrgrp.ddlSelectedAttributeID = null;
            $scope.attrgrp.IsPredefined = row.entity.IsPredefined
            $scope.attrgrp.attributegroupID = row.entity.Id;
            $scope.attrgrp.attributegroupCaption = row.entity.Caption;
            $scope.attrgrp.attributegroupDescription = row.entity.Description;
            MetadataService.GetAttributeGroupAttributeRelation(row.entity.Id).then(function (res) {
                $scope.attributeRelationarr = [];
                for (var i = 0; i < res.Response.length; i++) {
                    var result = $.grep($scope.attrgrp.attributeList, function (e) {
                        return e.Id == parseInt(res.Response[i].AttributeID);
                    });
                    if (result.length > 0) {
                        var attributeCaption = result[0].Caption;
                        $scope.attributeRelationarr.push({
                            "ID": res.Response[i].ID,
                            "AttributeID": res.Response[i].AttributeID,
                            "AttributeCaption": attributeCaption,
                            "AttributeRelationCaption": res.Response[i].Caption,
                            "SortOrder": 0,
                            "IsSearchable": res.Response[i].IsSearchable,
                            "ShowAsColumn": res.Response[i].ShowAsColumn
                        });
                    }
                }
                var HeaderAttrCount = $.grep($scope.attributeRelationarr, function (e) {
                    return e.AttributeID == 76;
                });
                if (HeaderAttrCount.length == 0) {
                    $scope.attributeRelationarr.push({
                        "ID": 0,
                        "AttributeID": 76,
                        "AttributeCaption": 'Header',
                        "AttributeRelationCaption": 'Name',
                        "SortOrder": 0,
                        "IsSearchable": 0,
                        "ShowAsColumn": 0
                    });
                }
            });
            $scope.EnableAttributeAdd = true;
            $scope.EnableAttributeUpdate = false;
            $timeout(function () {
                DragDrop();
            }, 1000);
            $timeout(function () {
                $('#attrgrpcaption').focus().select();
            }, 1000);
        };
        $scope.OnRowClickDeleteAttributeGroup = function (row) {
            bootbox.confirm($translate.instant('LanguageContents.Res_5548.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        if (row.ID != 0) {
                            MetadataService.DeleteAttributeGroup(row.entity.Id).then(function (res) {
                                if (res.Response == 1) {
                                    $scope.attributegroupdata = $.grep($scope.attributegroupdata, function (e) {
                                        return parseInt(e.Id) != parseInt(row.entity.Id);
                                    });
                                    NotifySuccess($translate.instant('LanguageContents.Res_4114.Caption'));
                                } else if (res.Response == 2) {
                                    NotifyError($translate.instant('LanguageContents.Res_4291.Caption'));
                                } else {
                                    NotifyError($translate.instant('LanguageContents.Res_4290.Caption'));
                                }
                            });
                        } else {
                            $scope.attributegroupdata = $.grep($scope.attributegroupdata, function (e) {
                                return parseInt(e.Id) != parseInt(row.entity.Id);
                            });
                        }
                    }, 50);
                }
            });
        };
        $scope.DeleteAttributeByID = function (row) {
            bootbox.confirm($translate.instant('LanguageContents.Res_1824.Caption'), function (result) {
                if (result) {
                    MetadataService.DeleteAttributeGroupAttributeRelation(row.ID).then(function (res) {
                        if (res.Response == 1) {
                            $scope.attributeRelationarr = $.grep($scope.attributeRelationarr, function (e) {
                                return parseInt(e.AttributeID) != parseInt(row.AttributeID);
                            });
                            $scope.ClearAttributeRelationCtrls();
                            NotifySuccess($translate.instant('LanguageContents.Res_4113.Caption'));
                        } else {
                            NotifyError($translate.instant('LanguageContents.Res_4289.Caption'));
                        }
                    });
                }
            });
        };
        $scope.saveAttributeGroupAndAttributeRelation = function () {
            $("#btnTemp").click();
            $("#attributegroupPage1").removeClass('notvalidate');
            if ($("#attributegroupPage1 .error").length > 0) {
                return false;
            }
            var SaveAttribute = {};
            SaveAttribute.AttributeGroupID = $scope.attrgrp.attributegroupID;
            SaveAttribute.Caption = $scope.attrgrp.attributegroupCaption;
            SaveAttribute.Description = $scope.attrgrp.attributegroupDescription;
            SaveAttribute.IsPredefined = $scope.attrgrp.IsPredefined;
            var value = 1;
            if ($scope.DragDropvalues != '') {
                for (var i = 0; i < $scope.DragDropvalues.length; i++) {
                    if (parseInt($scope.DragDropvalues[i].id) != 0) {
                        $.grep($scope.attributeRelationarr, function (e) {
                            return e.AttributeID == parseInt($scope.DragDropvalues[i].id);
                        })[0].SortOrder = value;
                        value++;
                    }
                }
            }
            SaveAttribute.AttributeData = $scope.attributeRelationarr;
            MetadataService.InsertUpdateAttributeGroupAndAttributeRelation(SaveAttribute).then(function (res) {
                if (res.Response != 0) {
                    $scope.LoadAttributeGroup();
                    NotifySuccess($translate.instant('LanguageContents.Res_4115.Caption'));
                } else
                    NotifySuccess($translate.instant('LanguageContents.Res_4334.Caption'));
                $scope.attrgrp.attributegroupID = 0;
                $scope.attrgrp.attributerelationCaption = '';
                $scope.attrgrp.attributegroupCaption = '';
                $scope.attrgrp.attributegroupDescription = '';
                $scope.attributeRelationarr = [];
                $("#attributegroupModal").modal('hide');
            });
        };
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.attributegroupCtrl']"));
        });
    }
    app.controller("mui.admin.attributegroupCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$location', '$window', '$translate', 'MetadataService', muiadminattributegroupCtrl]);
})(angular, app);