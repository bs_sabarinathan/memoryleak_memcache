﻿(function (ng, app) {

    "use strict";

    //app.controller(
    //	"mui.notificationtaskeditCntrl",
    function muinotificationtaskeditCntrl($scope, $cookies, $resource, $route, $routeParams, requestContext, $compile, $window, $timeout, $location, _, $sce, $translate, TaskService, PlanningService, CommonService, MetadataService, AccessService) {
        var model;
        $scope.GetNonBusinessDaysforDatePicker();
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            model = model1;
        };

        $scope.dynCalanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = false;

        };
        $scope.temptaskDiv = "";
        $scope.tempTaskID = 0;
        var fundrequestID = 0;
        $scope.TaskIDnotify = 0;
        $scope.notifyDivid = '';
        $scope.updatetaskEdit = true;
        $("#Notificationtaskedit").on('NotificationTaskAction', function (event, Id, EntitytypeId, ParentEntityID) {
            //checking the entity is locked or not locked
            $scope.IsLockNotif = false;
            //var GetLockStatus = $resource('planning/GetLockStatus/:EntityID', { EntityID: ParentEntityID }, { get: { method: 'GET' } });
            //var entitylockstatus = GetLockStatus.get({ EntityID: ParentEntityID }, function () {
            PlanningService.GetLockStatus(ParentEntityID).then(function (entitylockstatus) {
                $scope.IsLockNotif = entitylockstatus.Response.m_Item2;
                $scope.processingsrcobj.processinglock = $scope.IsLockNotif;
                $scope.GetTaskDetailFromService(Id, ParentEntityID);
            });
        });

        $("#Notificationtaskedit").on('TopMyTaskAction', function (event, Id, EntityId, ParentEntityID) {
            //checking the entity is locked or not locked
            $scope.IsLockNotif = false;
            //var GetLockStatus = $resource('planning/GetLockStatus/:EntityID', { EntityID: ParentEntityID }, { get: { method: 'GET' } });
            //var entitylockstatus = GetLockStatus.get({ EntityID: ParentEntityID }, function () {
            PlanningService.GetLockStatus(ParentEntityID).then(function (entitylockstatus) {
                $scope.IsLockNotif = entitylockstatus.Response.m_Item2;
                $scope.processingsrcobj.processinglock = $scope.IsLockNotif;
                $scope.GetTaskDetailFromService(Id, EntityId);
            });
        });

        $scope.TaskTitleChange = function (taskname) {
            $scope.name = $('<div />').html(taskname).text();
            $timeout(function () { $("#notifyworkName").focus().select() }, 10);
            $timeout(function () { $("#notifyUnassignedName").focus().select() }, 10);
            $timeout(function () { $("#notifyApprovalTaskName").focus().select() }, 10);
            $timeout(function () { $("#notifyReviewTaskName").focus().select() }, 10);
        };

        $scope.DatepickerShow = function (CurrentDueDate, CurrentId) {

            if (CurrentDueDate.length < 2) {
                $('#' + CurrentId).val('');
                $('#' + CurrentId).attr('AlredyDateSelected', 'true');
            }
            else {
                $('#' + CurrentId).attr('AlredyDateSelected', 'false');
            }
            $timeout(function () { $('[id=' + CurrentId + ']:enabled:visible:first').focus().select() }, 100);
            $timeout(function () {
                //  $('#' + CurrentId).datepicker('show');
                $scope.fields["DatePart_Calander_Open" + model] = true;
            }, 100);
        }

        // Validates that the input string is a valid date 
        function isValidDate(dateval, dateformat) {
            var formatlen;
            var defaultdateVal = [];
            defaultdateVal = dateval.length;
            formatlen = dateformat.length;
            if (formatlen == defaultdateVal || defaultdateVal > formatlen)
                return true;
            else
                return false;
        };



        $scope.saveEmptyDueDate = function (type) {

            $scope.EntityTaskChangesHolder.DueDate = '';

            //var TaskStatusserv = $resource('task/UpdatetaskEntityTaskDueDate');
            //var TaskStatusData = new TaskStatusserv();
            var TaskStatusData = {};
            TaskStatusData.TaskID = $scope.TaskBriefDetails.taskID;
            TaskStatusData.DueDate = $scope.EntityTaskChangesHolder.DueDate.toString();
            // var TaskStatusResult = TaskStatusserv.save(TaskStatusData, function () {
            TaskService.UpdatetaskEntityTaskDueDate(TaskStatusData).then(function (TaskStatusResult) {
                $scope.fired = false;
                if (TaskStatusResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    $scope.fired = false;
                }
                else {
                    if (TaskStatusResult.Response == false) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        $scope.fired = false;
                    }
                    else {
                        if (type == "unassignedue") {
                            if ($scope.EntityTaskChangesHolder.DueDate != '') {
                                $scope.TaskBriefDetails.dueIn = dateDiffBetweenDates($scope.dueDate);
                                $scope.dueDate = dateFormat($scope.EntityTaskChangesHolder.DueDate, $scope.DefaultSettings.DateFormat);
                                $scope.fired = false;
                            } else {
                                $scope.dueDate = '-';
                                $scope.TaskBriefDetails.dueDate = '-';
                                $scope.TaskBriefDetails.dueIn = 0;
                                $scope.fired = false;
                            }
                        }
                        else {
                            $scope.dueDate = '-';
                            $scope.TaskBriefDetails.dueDate = '-';
                            $scope.TaskBriefDetails.dueIn = 0;
                            $scope.fired = false;
                        }
                        RefreshCurrentTask();
                        $scope.EntityTaskChangesHolder = { "TaskName": "", "TaskDescription": "", "DueDate": "", "Note": "" };
                        NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                        $timeout(function () { feedforApprovalTask($scope.TaskIDnotify, $scope.notifyDivid, true); }, 2000);
                        $scope.fired = false;
                    }
                }
            });

        }

        $scope.Approvechangedate = function () {
            if ($scope.fired == false) {
                $scope.fired = true;
                $timeout(function () {
                    //$('#NotifyTaskApproveTaskDatepicker').datepicker("hide");
                    $scope.fields["DatePart_Calander_Open" + model] = false;
                    if ($scope.dueDate != null) {
                        var test = isValidDate($('#NotifyTaskApproveTaskDatepicker').val().toString(), $scope.DefaultSettings.DateFormat.toString());

                        if (test) {
                            if (dateDiffBetweenDates($scope.dueDate) >= 0) {
                                $scope.saveEntityTaskDetails('due');

                                $scope.editable = '';
                                $('#NotifyTaskApproveTaskDatepicker').attr('AlredyDateSelected', 'false');

                                $('#NotifyTaskApproveTaskDatepicker').val('');
                            }
                            else {
                                bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
                                $scope.fired = false;
                            }
                        }
                        else {
                            $scope.dueDate = new Date.create();
                            $scope.dueDate = null;
                            $('#NotifyTaskApproveTaskDatepicker').val('');
                            $scope.fired = false;

                            $scope.saveEmptyDueDate('due');
                            $scope.editable = '';
                            $('#NotifyTaskApproveTaskDatepicker').attr('AlredyDateSelected', 'false');
                            $('#NotifyTaskApproveTaskDatepicker').val('');
                            $scope.dueDate = "-";
                            $scope.fired = false;
                            //  $('#NotifyTaskApproveTaskDatepicker').datepicker("hide");
                            $scope.fields["DatePart_Calander_Open" + model] = false;
                        }
                    }
                    else {

                        $scope.saveEmptyDueDate('due');
                        $scope.editable = '';
                        $('#NotifyTaskApproveTaskDatepicker').attr('AlredyDateSelected', 'false');
                        $('#NotifyTaskApproveTaskDatepicker').val('');
                        $scope.dueDate = "-";
                        $scope.fired = false;
                        // $('#NotifyTaskApproveTaskDatepicker').datepicker("hide");
                        $scope.fields["DatePart_Calander_Open" + model] = false;
                    }
                }, 100);
            }
        }




        $('#NotifyTaskApproveTaskDatepicker').keydown(function (e) {
            if (e.keyCode == 13) {
                if ($('#NotifyTaskApproveTaskDatepicker').val() != "") {
                    try {
                        $scope.dueDate = new Date.create($('#NotifyTaskApproveTaskDatepicker').val());
                        if ($scope.dueDate.toString() == "Invalid Date") {
                            $scope.dueDate = null;
                        }
                    }
                    catch (e) { $scope.dueDate = null; }
                }

                //if ($scope.dueDate != null && dateDiffBetweenDates($scope.dueDate) <= 0) {
                //    bootbox.alert($translate.instant('LanguageContents.Res_1871.Caption'));
                //    return false;
                //}
                //else {
                if ($scope.dueDate != null) {
                    $scope.saveEntityTaskDetails('due');

                    $scope.editable = '';
                    $('#NotifyTaskApproveTaskDatepicker').attr('AlredyDateSelected', 'false');

                    $('#NotifyTaskApproveTaskDatepicker').val('');
                    // $('#NotifyTaskApproveTaskDatepicker').datepicker("hide");
                    $scope.fields["DatePart_Calander_Open" + model] = false;
                }
            }
        });

        //-------------Approval End

        //-------------Reviewal start

        $scope.Reviewchangedate = function () {
            if ($scope.fired == false) {
                $scope.fired = true;
                $timeout(function () {
                    // $('#NotifyTaskReviewTaskDatepicker').datepicker("hide");
                    $scope.fields["DatePart_Calander_Open" + model] = false;
                    if ($scope.dueDate != null) {
                        var test = isValidDate($('#NotifyTaskReviewTaskDatepicker').val().toString(), $scope.DefaultSettings.DateFormat.toString());

                        if (test) {
                            if (dateDiffBetweenDates($scope.dueDate) >= 0) {
                                $scope.saveEntityTaskDetails('due');

                                $scope.editable = '';
                                $('#NotifyTaskReviewTaskDatepicker').attr('AlredyDateSelected', 'false');

                                $('#NotifyTaskReviewTaskDatepicker').val('');
                            }
                            else {
                                bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
                                $scope.fired = false;
                            }
                        }
                        else {
                            $scope.dueDate = new Date.create();
                            $scope.dueDate = null;
                            $('#NotifyTaskReviewTaskDatepicker').val('');
                            $scope.fired = false;

                            $scope.saveEmptyDueDate('due');
                            $scope.editable = '';
                            $('#NotifyTaskReviewTaskDatepicker').attr('AlredyDateSelected', 'false');
                            $('#NotifyTaskReviewTaskDatepicker').val('');
                            $scope.dueDate = "-";
                            $scope.fired = false;
                            // $('#NotifyTaskReviewTaskDatepicker').datepicker("hide");
                            $scope.fields["DatePart_Calander_Open" + model] = false;

                        }
                    }
                    else {

                        $scope.saveEmptyDueDate('due');
                        $scope.editable = '';
                        $('#NotifyTaskReviewTaskDatepicker').attr('AlredyDateSelected', 'false');
                        $('#NotifyTaskReviewTaskDatepicker').val('');
                        $scope.dueDate = "-";
                        $scope.fired = false;
                        // $('#NotifyTaskReviewTaskDatepicker').datepicker("hide");
                        $scope.fields["DatePart_Calander_Open" + model] = false;
                    }
                }, 100);
            }
        }

        $('#NotifyTaskReviewTaskDatepicker').keydown(function (e) {
            if (e.keyCode == 13) {
                if ($('#NotifyTaskReviewTaskDatepicker').val() != "") {
                    try {
                        $scope.dueDate = new Date.create($('#NotifyTaskReviewTaskDatepicker').val());
                        if ($scope.dueDate.toString() == "Invalid Date") {
                            $scope.dueDate = null;
                        }
                    }
                    catch (e) { $scope.dueDate = null; }
                }

                //if ($scope.dueDate != null && dateDiffBetweenDates($scope.dueDate) <= 0) {
                //    bootbox.alert($translate.instant('LanguageContents.Res_1871.Caption'));
                //    return false;
                //}
                //else {
                if ($scope.dueDate != null) {
                    $scope.saveEntityTaskDetails('due');

                    $scope.editable = '';
                    $('#NotifyTaskReviewTaskDatepicker').attr('AlredyDateSelected', 'false');

                    $('#NotifyTaskReviewTaskDatepicker').val('');
                    //  $('#NotifyTaskReviewTaskDatepicker').datepicker("hide");
                    $scope.fields["DatePart_Calander_Open" + model] = false;
                }
            }
        });
        //-------------Approval End


        //-------------Worktask start



        $scope.fired = false;
        $scope.workchangedate = function () {
            if ($scope.fired == false) {
                $scope.fired = true;
                $timeout(function () {
                    //$('#NotofyTaskEditDatepicker').datepicker("hide");
                    $scope.fields["DatePart_Calander_Open" + model] = false;
                    if ($scope.dueDate != null) {
                        var test = isValidDate($('#NotofyTaskEditDatepicker').val().toString(), $scope.DefaultSettings.DateFormat.toString());

                        if (test) {
                            if (dateDiffBetweenDates($scope.dueDate) >= 0) {
                                $scope.saveEntityTaskDetails('due');

                                $scope.editable = '';
                                $('#NotofyTaskEditDatepicker').attr('AlredyDateSelected', 'false');

                                $('#NotofyTaskEditDatepicker').val('');
                            }
                            else {
                                bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
                                $scope.fired = false;
                            }
                        }
                        else {
                            $scope.dueDate = new Date.create();
                            $scope.dueDate = null;
                            $('#NotofyTaskEditDatepicker').val('');
                            $scope.fired = false;

                            $scope.saveEmptyDueDate('unassignedue');
                            $scope.editable = '';
                            $('#NotofyTaskEditDatepicker').attr('AlredyDateSelected', 'false');
                            $('#NotofyTaskEditDatepicker').val('');
                            $scope.dueDate = "-";
                            $scope.fired = false;
                            //$('#NotofyTaskEditDatepicker').datepicker("hide");
                            $scope.fields["DatePart_Calander_Open" + model] = false;

                        }
                    }
                    else {

                        $scope.saveEmptyDueDate('due');
                        $scope.editable = '';
                        $('#NotofyTaskEditDatepicker').attr('AlredyDateSelected', 'false');
                        $('#NotofyTaskEditDatepicker').val('');
                        $scope.dueDate = "-";
                        $scope.fired = false;
                        //  $('#NotofyTaskEditDatepicker').datepicker("hide");
                        $scope.fields["DatePart_Calander_Open" + model] = false;

                    }
                }, 100);
            }
        }



        $('#NotofyTaskEditDatepicker').keydown(function (e) {
            if (e.keyCode == 13) {
                if ($('#NotofyTaskEditDatepicker').val() != "") {
                    try {
                        $scope.dueDate = new Date.create($('#NotofyTaskEditDatepicker').val());
                        if ($scope.dueDate.toString() == "Invalid Date") {
                            $scope.dueDate = null;
                        }
                    }
                    catch (e) { $scope.dueDate = null; }
                }

                //if ($scope.dueDate != null && dateDiffBetweenDates($scope.dueDate) <= 0) {
                //    bootbox.alert($translate.instant('LanguageContents.Res_1871.Caption'));
                //    return false;
                //}
                //else {
                if ($scope.dueDate != null) {
                    $scope.saveEntityTaskDetails('due');

                    $scope.editable = '';
                    $('#NotofyTaskEditDatepicker').attr('AlredyDateSelected', 'false');

                    $('#NotofyTaskEditDatepicker').val('');
                    // $('#NotofyTaskEditDatepicker').datepicker("hide");
                    $scope.fields["DatePart_Calander_Open" + model] = false;
                }
            }
        });

        //-------------Worktask End

        //-------------Unassigned start

        $scope.Unassignedchangedate = function () {
            if ($scope.fired == false) {
                $scope.fired = true;
                $timeout(function () {
                    if ($scope.dueDate != null) {

                        var test = isValidDate($('#NotifyUnAssignedDatepicker').val().toString(), $scope.DefaultSettings.DateFormat.toString());

                        if (test) {

                            if (dateDiffBetweenDates($scope.dueDate) >= 0) {
                                $scope.saveEntityTaskDetails('unassignedue');

                                $scope.editable = '';
                                $('#NotifyUnAssignedDatepicker').attr('AlredyDateSelected', 'false');

                                $('#NotifyUnAssignedDatepicker').val('');
                            }
                            else {
                                bootbox.alert($translate.instant('LanguageContents.Res_1958.Caption'));
                                $scope.dueDate = "-";
                                $scope.fired = false;
                            }
                            // $('#NotifyUnAssignedDatepicker').datepicker("hide");
                            $scope.fields["DatePart_Calander_Open" + model] = false;
                        }
                        else {
                            $scope.dueDate = new Date.create();
                            $scope.dueDate = null;
                            $('#NotifyUnAssignedDatepicker').val('');
                            $scope.fired = false;

                            $scope.saveEmptyDueDate('unassignedue');
                            $scope.editable = '';
                            $('#NotifyUnAssignedDatepicker').attr('AlredyDateSelected', 'false');
                            $('#NotifyUnAssignedDatepicker').val('');
                            $scope.dueDate = "-";
                            $scope.fired = false;
                            //  $('#NotifyUnAssignedDatepicker').datepicker("hide");
                            $scope.fields["DatePart_Calander_Open" + model] = false;
                        }

                    }
                    else {

                        $scope.saveEmptyDueDate('unassignedue');
                        $scope.editable = '';
                        $('#NotifyUnAssignedDatepicker').attr('AlredyDateSelected', 'false');
                        $('#NotifyUnAssignedDatepicker').val('');
                        $scope.dueDate = "-";
                        $scope.fired = false;
                        //  $('#NotifyUnAssignedDatepicker').datepicker("hide");
                        $scope.fields["DatePart_Calander_Open" + model] = false;
                    }
                }, 100);
            }
        }

        $('#NotifyUnAssignedDatepicker').keydown(function (e) {
            if (e.keyCode == 13) {
                if ($('#NotifyUnAssignedDatepicker').val() != "") {
                    try {
                        $scope.dueDate = new Date.create($('#NotifyUnAssignedDatepicker').val());
                        if ($scope.dueDate.toString() == "Invalid Date") {
                            $scope.dueDate = null;
                        }
                    }
                    catch (e) { $scope.dueDate = null; }
                }

                //if ($scope.dueDate != null && dateDiffBetweenDates($scope.dueDate) <= 0) {
                //    bootbox.alert($translate.instant('LanguageContents.Res_1871.Caption'));
                //    return false;
                //}
                //else {
                if ($scope.dueDate != null) {
                    $scope.saveEntityTaskDetails('unassignedue');

                    $scope.editable = '';
                    $('#NotifyUnAssignedDatepicker').attr('AlredyDateSelected', 'false');

                    $('#NotifyUnAssignedDatepicker').val('');
                    // $('#NotifyUnAssignedDatepicker').datepicker("hide");
                    $scope.fields["DatePart_Calander_Open" + model] = false;
                }

            }
        });


        $scope.IsNotVersioning = true;


        //-------------Unassigned End

        $scope.OwnerName = $cookies['Username'];
        $scope.OwnerID = parseInt($cookies['UserId'], 10);
        $scope.ownerEmail = $cookies['UserEmail'];

        $scope.TaskActionFor = 1;  //{ if 1 means action for current entity and refresh current entity object or otherwise action for sub level}

        //EntityID assigning Globally
        $scope.TaskGlobalEntityID = $routeParams.ID;
        $scope.ContextTaskEntityID = $routeParams.ID;
        $scope.TaskOrderListObj = { TaskSortingStatus: "Reorder task lists", TaskSortingStatusID: 1, TaskOrderHandle: false, SortOrderObj: [], UniqueTaskSort: "Reorder task", UniqueTaskSortingStatusID: 1, UniqueTaskOrderHandle: false, UniqueSortOrderObj: [] };

        $scope.OwnerList = [];
        $scope.OwnerList.push({ "Roleid": 1, "RoleName": "Owner", "UserEmail": $scope.ownerEmail, "DepartmentName": "-", "Title": "-", "Userid": parseInt($scope.OwnerID, 10), "UserName": $scope.OwnerName, "IsInherited": '0', "InheritedFromEntityid": '0' });


        $scope.GetTaskDetailFromService = function (Id, EntityId) {
            $routeParams.ID = EntityId;
            $scope.editable = '';
            if (NewsFeedUniqueTimerForTask != undefined)
                $timeout.cancel(NewsFeedUniqueTimerForTask);
            $('#loadNotificationtask').on('hide.bs.modal', function () {
                if (NewsFeedUniqueTimerForTask != undefined || NewsFeedUniqueTimerForTask == null)
                    $timeout.cancel(NewsFeedUniqueTimerForTask);
            });
            $("#NotifyWorkMyTaskPopup").hide();
            $("#NotifyApprovalMyTaskPopup").hide();
            $("#NotifyReviewMyTaskPopup").hide();
            $("#NotifyUnassignedTask").hide();

            $timeout(function () {
                refreshModel();
            }, 20);



            //var GetEntityTaskDetails = $resource('task/GetEntityTaskDetails/:EntityTaskID', { EntityTaskID: Id }, { get: { method: 'GET' } });
            //var EntityTaskDetails = GetEntityTaskDetails.get({ EntityTaskID: Id }, function () {
            TaskService.GetEntityTaskDetails(Id).then(function (EntityTaskDetails) {
                $scope.TaskDetails = EntityTaskDetails.Response;
                $scope.IsnewcommentAvailable = 0;
                $scope.EntityMemberList = [];
                $scope.TaskIDnotify = Id;
                //var GetEntityMember = $resource('planning/Member/:EntityID', { EntityID: EntityId }, { get: { method: 'GET' } });
                //var EntityMemberList = GetEntityMember.get({ EntityID: EntityId }, function () {
                PlanningService.GetMember(EntityId).then(function (EntityMemberList) {
                    $scope.EntityMemberList = EntityMemberList.Response;

                    GetEntityLocationPath(Id);

                    $timeout(function () {
                        LoadMemberRoles();
                    }, 300);

                    if ($scope.TaskDetails.TaskStatus != 0) {

                        BindFundRequestTaskDetails($scope.TaskDetails.TaskType, $scope.TaskDetails);
                        if ($scope.TaskDetails.TaskType == 2) {
                            $("#NotifyApprovalMyTaskPopup").hide();
                            $("#NotifyUnassignedTask").hide();
                            $("#NotifyReviewMyTaskPopup").hide();
                            $scope.notifyDivid = "Notifyfeeddivworktaskforedit";
                            feedforApprovalTask(parseInt(Id), "Notifyfeeddivworktaskforedit", false);
                            $("#NotifyWorkMyTaskPopup").show();
                            var commentbuttinid = "feedcommentworkforedit";
                            $('#' + commentbuttinid).empty();
                            var temp = '';
                            if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
                                temp = 'feedtextholderworkforedit';
                                document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
                            }
                        }
                        else if ($scope.TaskDetails.TaskType == 3 || $scope.TaskDetails.TaskType == 32 || $scope.TaskDetails.TaskType == 36) {
                            $("#NotifyWorkMyTaskPopup").hide();
                            $("#NotifyReviewMyTaskPopup").hide();
                            $scope.notifyDivid = "Notifyfeeddivapprovaltaskforedit";

                            feedforApprovalTask(parseInt(Id), "Notifyfeeddivapprovaltaskforedit", false);
                            $("#NotifyApprovalMyTaskPopup").show();
                            var commentbuttinid = "feedcommentapprovaltaskforedit";
                            $('#' + commentbuttinid).empty();
                            var temp = '';
                            if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
                                temp = 'feedtextholderapprovaltaskforedit';
                                document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
                            }
                        }
                        else if ($scope.TaskDetails.TaskType == 31) {
                            $("#NotifyWorkMyTaskPopup").hide();
                            $("#NotifyApprovalMyTaskPopup").hide();
                            $scope.notifyDivid = "Notifyfeeddivforreviewtaskforedit";

                            feedforApprovalTask(parseInt(Id), "Notifyfeeddivforreviewtaskforedit", false);
                            $("#NotifyReviewMyTaskPopup").show();
                            var commentbuttinid = "divforreviewtaskcommentareaforedit";
                            $('#' + commentbuttinid).empty();
                            var temp = '';
                            if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
                                temp = 'textholderreviewtaskforedit';
                                document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
                            }
                        }
                    }
                    else {
                        LoadUnassignedTaskDetl($scope.TaskDetails.TaskType, $scope.TaskDetails);
                        $scope.SelectedTaskID = EntityId;
                        $("#NotifyWorkMyTaskPopup").hide();
                        $("#NotifyApprovalMyTaskPopup").hide();
                        $("#NotifyReviewMyTaskPopup").hide();
                        $("#NotifyUnassignedTask").show();
                        $scope.notifyDivid = "notifyfeeddivforunassignedassigned";

                        feedforApprovalTask(parseInt(Id), "notifyfeeddivforunassignedassigned", false);

                        var commentbuttinid = "notifyfeedcommentunassigned";
                        $('#' + commentbuttinid).empty();
                        var temp = '';
                        if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {
                            temp = 'notifyfeedtextholderunassigned';
                            document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
                        }
                    }
                });
            });
            $timeout(function () {
                $scope.Timerforcallback(parseInt($scope.TaskIDnotify), $scope.notifyDivid, true);
            }, 30000);


        }

        $scope.TaskBriefDetails = {
            taskID: 0,
            taskListUniqueID: 0,
            taskTypeId: 0,
            taskTypeName: "",
            EntityID: 0,
            ownerId: 0,
            taskName: "",
            dueIn: 0,
            dueDate: "",
            strDueDate: "",
            status: "",
            statusID: 0,
            taskOwner: "",
            Description: "",
            Note: "",
            taskmembersList: [],
            taskAttachmentsList: [],
            taskProgressCount: "",
            Totalassetcount: 0,
            Totalassetsize: ""
        };

        function refreshModel() {
            $scope.TaskBriefDetails = {
                taskID: 0,
                taskListUniqueID: 0,
                taskTypeId: 0,
                taskTypeName: "",
                EntityID: 0,
                ownerId: 0,
                taskName: "",
                dueIn: 0,
                dueDate: "",
                status: "",
                statusID: 0,
                taskOwner: "",
                Description: "",
                Note: "",
                taskmembersList: [],
                taskAttachmentsList: [],
                taskProgressCount: ""
            };
            $scope.FileList = [];
            $scope.AttachmentFilename = [];
            $scope.SelectedTaskID = 0;
        }

        $scope.NewTime = $scope.DefaultImageSettings.ImageSpan;



        var CurrentCheckListName = "";
        $scope.EditCheckList = function (Name, Index) {
            var Object = $.grep($scope.TaskBriefDetails.taskCheckList, function (n, i) {
                if ((i != Index && n.IsExisting == false)) {
                    return $scope.TaskBriefDetails.taskCheckList[i];

                }
            });

            if (Object.length > 0) {
                if (Object[0].Name.length == 0) {
                    if (Object[0].Id == 0) {
                        $scope.TaskBriefDetails.taskCheckList.splice($scope.TaskBriefDetails.taskCheckList.indexOf(Object[0]), 1);
                    } else {
                        Object[0].IsExisting = true;
                        Object[0].Name = CurrentCheckListName;
                    }
                }
                else {
                    Object[0].IsExisting = true;
                    Object[0].Name = CurrentCheckListName;
                }
            }

            CurrentCheckListName = Name;
        }

        $scope.TaskNoteChange1 = function (originalDesc) {
            $scope.Notechange = $('<div />').html(originalDesc).text();
            $timeout(function () { $("#notifynotechangework").focus().select() }, 10);
        };
        $scope.TaskNoteChange2 = function (originalDesc) {
            $scope.Notechange = $('<div />').html(originalDesc).text();
            $timeout(function () { $("notifynoteapprovaltask").focus().select() }, 10);
        };
        $scope.TaskNoteChange3 = function (originalDesc) {
            $scope.Notechange = $('<div />').html(originalDesc).text();
            $timeout(function () { $("#notifynoteReviewTask").focus().select() }, 10);
        };
        $scope.TaskNoteChange4 = function (originalDesc) {
            $scope.Notechange = $('<div />').html(originalDesc).text();
            $timeout(function () { $("#notifynoteUnassignedtask").focus().select() }, 10);
        };

        $scope.TaskDescChange1 = function (originalDesc) {
            $scope.DescriptionChange = $('<div />').html(originalDesc).text();
            $timeout(function () { $("#NotifytxtDesc1").focus().select() }, 10);
        };
        $scope.TaskDescChange2 = function (originalDesc) {
            $scope.DescriptionChange = $('<div />').html(originalDesc).text();
            $timeout(function () { $("#NotifytxtDesc2").focus().select() }, 10);
        };
        $scope.TaskDescChange3 = function (originalDesc) {
            $scope.DescriptionChange = $('<div />').html(originalDesc).text();
            $timeout(function () { $("#NotifytxtDesc3").focus().select() }, 10);
        };
        $scope.TaskDescChange4 = function (originalDesc) {
            $scope.DescriptionChange = $('<div />').html(originalDesc).text();
            $timeout(function () { $("#NotifytxtDesc4").focus().select() }, 10);
        };

        $scope.TaskcheckEditable = function (type, index) {
            if (type == "WorkTask")
                $timeout(function () { $("#notifyWorkTasktxtChk_" + index + "").focus().select() }, 10);
            else
                $timeout(function () { $("#notifyunassignedtasktxtChk_" + index + "").focus().select() }, 10);
        };


        var cssToRemove = "";
        //Populate task Breif Details
        function BindFundRequestTaskDetails(taskTypeId, TaskObject) {

            $scope.ShowCompleteBtn = false;
            $scope.ShowApproveBtn = false;
            $scope.ShowRejectedBtn = false;
            $scope.ShowWithdrawBtn = false;
            $scope.ShowUnabletoCompleteBtn = false;
            $scope.ShowRevokeButton = false;

            $("#NotifyApprovalMyTaskPopup,#NotifyReviewMyTaskPopup,#NotifyWorkMyTaskPopup,#NotifyUnassignedTask").removeClass(cssToRemove);
            $("#NotifyApprovalMyTaskPopup,#NotifyReviewMyTaskPopup,#NotifyWorkMyTaskPopup,#NotifyUnassignedTask").addClass("TaskPopup " + TaskObject.StatusName.replace(/\s+/g, ""));
            cssToRemove = TaskObject.StatusName.replace(/\s+/g, "");
            $scope.listPredfWorkflowFilesAttch = TaskObject.taskAttachment;
            var taskOwnerObj = $.grep(TaskObject.taskMembers, function (e) { return (e.RoleID == 1); })[0];

            if (TaskObject.taskAssigness != null) {
                var isThisMemberPresent = $.grep(TaskObject.taskAssigness, function (e) { return (e.UserID == parseInt($cookies['UserId']) && e.RoleID != 1); });
                if (taskTypeId == 2) {
                    if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                        if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                            $scope.ShowWithdrawBtn = false;
                        }
                    }
                    if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
                        if ($scope.IsLockNotif != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null) {
                                    $scope.ShowCompleteBtn = true;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }
                    }
                    if (TaskObject.TaskStatus == 4 && isThisMemberPresent.length >= 1) {
                        if ($scope.IsLockNotif != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null) {
                                    $scope.ShowCompleteBtn = true;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }
                    }
                    if (TaskObject.TaskStatus == 2 || TaskObject.TaskStatus == 8) {
                        if (isThisMemberPresent.length == 1) {
                            if (isThisMemberPresent[0].ApprovalStatus == 2 || isThisMemberPresent[0].ApprovalStatus == 3) {
                                if ($scope.IsLockNotif != true) {
                                    $scope.ShowCompleteBtn = false;
                                    $scope.ShowApproveBtn = false;
                                    $scope.ShowRejectedBtn = false;
                                    $scope.ShowWithdrawBtn = false;
                                    $scope.ShowUnabletoCompleteBtn = false;
                                    $scope.ShowRevokeButton = true;
                                }
                            }
                            else {
                                $scope.ShowCompleteBtn = false;
                                $scope.ShowApproveBtn = false;
                                $scope.ShowRejectedBtn = false;
                                $scope.ShowWithdrawBtn = false;
                                $scope.ShowUnabletoCompleteBtn = false;
                                $scope.ShowRevokeButton = false;
                            }
                        }
                        else {
                            $scope.ShowCompleteBtn = false;
                            $scope.ShowApproveBtn = false;
                            $scope.ShowRejectedBtn = false;
                            $scope.ShowWithdrawBtn = false;
                            $scope.ShowUnabletoCompleteBtn = false;
                            $scope.ShowRevokeButton = false;
                        }
                    }
                    if (TaskObject.TaskStatus == 8) {
                        if (isThisMemberPresent != null)
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == 2 || isThisMemberPresent[0].ApprovalStatus == 3) {
                                    if ($scope.IsLockNotif != true) {
                                        $scope.ShowCompleteBtn = false;
                                        $scope.ShowApproveBtn = false;
                                        $scope.ShowRejectedBtn = false;
                                        $scope.ShowWithdrawBtn = false;
                                        $scope.ShowUnabletoCompleteBtn = false;
                                        $scope.ShowRevokeButton = true;
                                    }
                                }
                                else {
                                    $scope.ShowCompleteBtn = false;
                                    $scope.ShowApproveBtn = false;
                                    $scope.ShowRejectedBtn = false;
                                    $scope.ShowWithdrawBtn = false;
                                    $scope.ShowUnabletoCompleteBtn = false;
                                    $scope.ShowRevokeButton = false;
                                }
                            }
                            else {
                                $scope.ShowCompleteBtn = false;
                                $scope.ShowApproveBtn = false;
                                $scope.ShowRejectedBtn = false;
                                $scope.ShowWithdrawBtn = false;
                                $scope.ShowUnabletoCompleteBtn = false;
                                $scope.ShowRevokeButton = false;
                            }
                    }


                    $scope.processingsrcobj.processingid = TaskObject.Id;
                    $scope.processingsrcobj.processingplace = "task";
                    $timeout(function () {
                        $scope.NotifyAssetfileTemplateLoading = "worktask";
                        $scope.$broadcast('ReloadAssetView');
                    }, 100);
                }
                else if (taskTypeId == 3 || taskTypeId == 32 || taskTypeId == 36) {

                    if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                        if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                            $scope.ShowWithdrawBtn = false;
                        }
                    }
                    if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
                        if ($scope.IsLockNotif != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null && taskTypeId != 36) {
                                    $scope.ShowApproveBtn = true;
                                    $scope.ShowRejectedBtn = true;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }
                    }
                    if (TaskObject.TaskStatus == 8) {
                        $scope.ShowCompleteBtn = false;
                        $scope.ShowApproveBtn = false;
                        $scope.ShowRejectedBtn = false;
                        $scope.ShowWithdrawBtn = false;
                        $scope.ShowUnabletoCompleteBtn = false;
                        $scope.ShowRevokeButton = false;
                    }
                    if (TaskObject.TaskStatus == 5 && isThisMemberPresent.length >= 1) {
                        if ($scope.IsLockNotif != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == 5 && taskTypeId != 36) {
                                    $scope.ShowApproveBtn = false;
                                    $scope.ShowRejectedBtn = false;
                                    $scope.ShowUnabletoCompleteBtn = false;
                                    $scope.ShowRevokeButton = true;
                                }
                            }
                        }
                    }

                    $scope.processingsrcobj.processingid = TaskObject.Id;
                    $scope.processingsrcobj.processingplace = "task";
                    $timeout(function () {
                        $scope.NotifyAssetfileTemplateLoading = "approvaltask";
                        $scope.$broadcast('ReloadAssetView');
                    }, 100);

                }
                else if (taskTypeId == 31) {
                    if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                        if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                            $scope.ShowWithdrawBtn = false;
                        }
                    }
                    if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
                        if ($scope.IsLockNotif != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null) {
                                    $scope.ShowApproveBtn = false;
                                    $scope.ShowCompleteBtn = true;
                                    $scope.ShowRejectedBtn = false;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }
                    }

                    if (TaskObject.TaskStatus == 8) {
                        $scope.ShowCompleteBtn = false;
                        $scope.ShowApproveBtn = false;
                        $scope.ShowRejectedBtn = false;
                        $scope.ShowWithdrawBtn = false;
                        $scope.ShowUnabletoCompleteBtn = false;
                    }

                    $scope.processingsrcobj.processingid = TaskObject.Id;
                    $scope.processingsrcobj.processingplace = "task";
                    $timeout(function () {
                        $scope.NotifyAssetfileTemplateLoading = "reveiewtask";
                        $scope.$broadcast('ReloadAssetView');
                    }, 100);
                }
            }
            $scope.TaskBriefDetails.taskListUniqueID = TaskObject.TaskListID;
            $scope.TaskBriefDetails.taskName = TaskObject.Name;
            $scope.TaskBriefDetails.taskTypeId = taskTypeId;
            $scope.TaskBriefDetails.taskTypeName = TaskObject.TaskTypeName;
            $scope.TaskBriefDetails.dueDate = '-';
            if (TaskObject.strDate != null && TaskObject.strDate.length > 0) {
                $scope.TaskBriefDetails.dueDate = dateFormat(TaskObject.strDate, $scope.DefaultSettings.DateFormat); //datstartval.toString("yyyy/MM/dd");
            }
            $scope.TaskBriefDetails.dueIn = TaskObject.totalDueDays;
            $scope.TaskBriefDetails.taskOwner = taskOwnerObj.UserName;
            $scope.TaskBriefDetails.status = TaskObject.StatusName;
            $scope.TaskBriefDetails.statusID = TaskObject.TaskStatus;
            $scope.TaskBriefDetails.Description = '-';
            if (TaskObject.Description.length > 0) {
                $scope.TaskBriefDetails.Description = TaskObject.Description;
            }
            $scope.TaskBriefDetails.Note = '-';
            if (TaskObject.Note != null)
                if (TaskObject.Note.length > 0) {
                    $scope.TaskBriefDetails.Note = TaskObject.Note;
                }
            $scope.TaskBriefDetails.taskID = TaskObject.Id;
            $scope.TaskBriefDetails.ownerId = taskOwnerObj.UserID;
            $scope.TaskBriefDetails.EntityID = TaskObject.EntityID;
            $scope.TaskBriefDetails.taskmembersList = TaskObject.taskAssigness;
            $scope.TaskBriefDetails.totalTaskMembers = TaskObject.TotaltaskAssigness;

            if (TaskObject.taskAssigness != null) {
                var unresponsedMembers = $.grep(TaskObject.taskAssigness, function (e) { return (e.ApprovalStatus != null); });
                if ((TaskObject.TaskStatus == 2 || TaskObject.TaskStatus == 3 || TaskObject.TaskStatus == 8) && TaskObject.TaskType != 2) {
                    $scope.TaskBriefDetails.taskProgressCount = "";
                }
                else {
                    if (unresponsedMembers != null)
                        $scope.TaskBriefDetails.taskProgressCount = "(" + unresponsedMembers.length.toString() + "/" + TaskObject.taskAssigness.length.toString() + ")";

                }

            }
            $scope.TaskMemberList = TaskObject.taskAssigness;
            $scope.TaskBriefDetails.taskAttachmentsList = TaskObject.taskAttachment;
            $scope.name = $scope.TaskBriefDetails.taskName;
            $scope.DescriptionChange = $scope.TaskBriefDetails.Description;
            if (TaskObject.strDate != "") {
                $scope.dueDate = ConvertStringToDate(TaskObject.strDate);
                $scope.TaskBriefDetails.strDueDate = ConvertStringToDate(TaskObject.strDate);
            }
            else {
                $scope.dueDate = "-";
                $scope.TaskBriefDetails.strDueDate = "-";
            }
            $scope.TaskBriefDetails.strDueDate = ConvertStringToDate(TaskObject.strDate);
            if (TaskObject.TaskStatus != 8)
                if (taskTypeId == 3 || taskTypeId == 31 || taskTypeId == 32 || taskTypeId == 36)
                    $scope.groupByTaskRoundTrip('ApprovalRount');
            if (TaskObject.taskAssigness != null)
                SeperateUsers();
            try { GetEntityAttributesDetails($scope.TaskBriefDetails.taskID); } catch (e) { }
            GetTaskCheckLists();
            ReloadTaskAttachments(TaskObject.Id);
            GetEntityTaskAttachmentinfo($scope.TaskBriefDetails.taskID);
        }

        $scope.FileID = 0;
        $scope.ChangeVersionNo = function () {
            $scope.FileID = $scope.contextFileID;
            $scope.IsNotVersioning = false;
            $scope.fnTimeOut();
        }

        function GetTaskCheckLists() {
            //var GetTaskDetails = $resource('task/getTaskchecklist/:TaskID', { TaskID: $scope.TaskBriefDetails.taskID }, { get: { method: 'GET' } });
            //var TaskDetailList = GetTaskDetails.get({ TaskID: $scope.TaskBriefDetails.taskID }, function () {
            TaskService.getTaskchecklist($scope.TaskBriefDetails.taskID).then(function (TaskDetailList) {
                $scope.TaskBriefDetails.taskCheckList = [];
                if (TaskDetailList.Response.length > 0) {
                    $scope.TaskBriefDetails.taskCheckList = TaskDetailList.Response;
                }
                if ($scope.TaskBriefDetails.taskTypeId == 2 && $scope.TaskBriefDetails.statusID == 0 && $scope.TaskBriefDetails.taskCheckList.length > 0) {
                    var taskChecklistResObj = $.grep($scope.TaskBriefDetails.taskCheckList, function (e) { return e.Status == true; });
                    $scope.TaskBriefDetails.WorkTaskInprogressStatus = "(" + taskChecklistResObj.length.toString() + "/" + +$scope.TaskBriefDetails.taskCheckList.length.toString() + ")";
                }
                if ($scope.TaskBriefDetails.statusID == 2 || $scope.TaskBriefDetails.statusID == 3 || $scope.TaskBriefDetails.statusID == 8) {
                    $scope.TaskBriefDetails.WorkTaskInprogressStatus = "";
                }
                else {
                    if ($scope.TaskBriefDetails.taskCheckList.length > 0) {
                        var taskChecklistResObj = $.grep($scope.TaskBriefDetails.taskCheckList, function (e) { return e.Status == true; });
                        $scope.TaskBriefDetails.WorkTaskInprogressStatus = "(" + taskChecklistResObj.length.toString() + "/" + +$scope.TaskBriefDetails.taskCheckList.length.toString() + ")";
                    }
                    else {
                        $scope.TaskBriefDetails.WorkTaskInprogressStatus = "";
                    }
                }

            });
        }

        function GetadminTaskCheckLists(taskID) {
            $scope.AdminTaskCheckList = [];
            //var GetTaskDetails = $resource('task/getTaskchecklist/:TaskID', { TaskID: taskID }, { get: { method: 'GET' } });
            //var TaskDetailList = GetTaskDetails.get({ TaskID: taskID }, function () {
            TaskService.getTaskchecklist(taskID).then(function (TaskDetailList) {
                $scope.AdminTaskCheckList = TaskDetailList.Response;
                if ($scope.AdminTaskCheckList.length == 0)
                    $scope.AdminTaskCheckList.push({ ID: 0, NAME: "" });

            });
        }


        function RefreshCurrentTask() {
            $scope.TaskDetails = [];
            //var GetEntityTaskDetails = $resource('task/GetEntityTaskDetails/:EntityTaskID', { EntityTaskID: $scope.TaskBriefDetails.taskID }, { get: { method: 'GET' } });
            //var EntityTaskDetails = GetEntityTaskDetails.get({ EntityTaskID: $scope.TaskBriefDetails.taskID }, function () {
            TaskService.GetEntityTaskDetails($scope.TaskBriefDetails.taskID).then(function (EntityTaskDetails) {
                $scope.TaskDetails = EntityTaskDetails.Response;
                $scope.SelectedTaskID = $scope.TaskDetails.TaskID;
                if ($scope.TaskDetails != null)
                    if ($scope.TaskDetails.TaskStatus != 0)
                        BindFundRequestTaskDetails($scope.TaskBriefDetails.taskTypeId, $scope.TaskDetails);
                    else {
                        LoadUnassignedTaskDetl($scope.TaskBriefDetails.taskTypeId, $scope.TaskDetails);
                    }

            });

        }

        //TaskMember roundtrip grouping
        //group the members list on the given property.
        function sortOnMemberRound(collection, name) {
            collection.sort(
                function (a, b) {
                    if (a[name] <= b[name]) {
                        return (-1);
                    }
                    return (1);
                }
            );
        }

        $scope.groupByTaskRoundTrip = function (attribute) {
            // First, reset the groups.
            $scope.TaskBriefDetails.TaskMembersRoundTripGroup = [];

            // Now, sort the collection of member on the
            // grouping-property. This just makes it easier
            // to split the collection.
            sortOnMemberRound($scope.TaskBriefDetails.totalTaskMembers, attribute);

            // determine which group members are currently in.
            var groupValue = "_INVALID_GROUP_VALUE_";

            // As we loop over each member, add it to the
            // current group - we'll create a NEW group every
            // time we come across a new attribute value.
            for (var i = 0 ; i < $scope.TaskBriefDetails.totalTaskMembers.length ; i++) {
                var friend = $scope.TaskBriefDetails.totalTaskMembers[i];

                //based on global access Conditions 

                // Should we create a new group?
                if (friend[attribute] !== groupValue) {

                    var group = {
                        label: friend[attribute],
                        friends: [],
                        rolename: "Round " + friend.ApprovalRount.toString()
                    };

                    groupValue = group.label;

                    $scope.TaskBriefDetails.TaskMembersRoundTripGroup.push(group);

                }

                // Add the friends to the currently active group
                // grouping.
                group.friends.push(friend);
            }
        };


        $scope.popMe = function (link) {

            var linktypeObj = $.grep($scope.DefaultSettings.LinkTypes, function (val) {
                return val.Id == parseInt(link.LinkType)
            })[0];

            if (linktypeObj != undefined) {
                var mypage = linktypeObj.Type + link.LinkURL;
                var myname = mypage;
                var w = 1200;
                var h = 800
                var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
                var win = window.open(mypage, myname, winprops)
            }
        };


        function SeperateUsers() {
            var UniqueTaskMembers = [];
            var dupes = {};
            if ($scope.TaskMemberList != null)
                $.each($scope.TaskMemberList, function (i, el) {
                    if (!dupes[el.UserID]) {
                        dupes[el.UserID] = true;
                        UniqueTaskMembers.push(el);
                    }
                });

            var UniqueEntityMembers = [];
            dupes = {};
            if ($scope.EntityMemberList != null)
                $.each($scope.EntityMemberList, function (i, el) {
                    if (el.IsInherited != true)
                        if (!dupes[el.Userid]) {
                            dupes[el.Userid] = true;
                            UniqueEntityMembers.push(el);
                        }
                });
            $scope.RemainMembers = [];
            $.each(UniqueEntityMembers, function (key, value) {
                var MemberList = $.grep(UniqueTaskMembers, function (e) { return e.UserID == value.Userid; });
                if (MemberList.length == 0) {
                    $scope.RemainMembers.push(value);
                }
            });
        }

        $scope.addAdditionalMember = function () {
            SeperateUsers();
            $scope.AddUnAssign = false;
            $scope.globalEntityMemberLists = [];
            $scope.globalTempcount = 1;
            $scope.ddltasklobalrole = '';
            $scope.taskGlobaluser = '';
            $scope.AutoCompleteSelectedObj = [];
            $("#NotifyAddTaskMemberModalPopup").modal("show");
        }

        $scope.AddTaskAdditionalMembers = function () {
            if ($('#ngNotifyAddTaskMember').hasClass('disabled')) { return; }
            $('#ngNotifyAddTaskMember').addClass('disabled');
            var memberList = [];
            memberList = GetUserSelectedAll();
            if (memberList.length > 0 || $scope.globalEntityMemberLists.length > 0) {
                //var InsertTaskMemberServ = $resource('task/InsertTaskMembers/');
                //var insertMemberTask = new InsertTaskMemberServ();
                var insertMemberTask = {};
                insertMemberTask.ParentId = $routeParams.ID;
                insertMemberTask.TaskID = $scope.TaskBriefDetails.taskID;
                insertMemberTask.TaskMembers = memberList;
                insertMemberTask.GlobalTaskMembers = $scope.globalEntityMemberLists;
                //var SaveTaskMemberResult = InsertTaskMemberServ.save(insertMemberTask, function () {
                TaskService.InsertTaskMembers(insertMemberTask).then(function (SaveTaskMemberResult) {
                    if (SaveTaskMemberResult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4267.Caption'));
                        $('#ngNotifyAddTaskMember').removeClass('disabled');
                    }
                    else {
                        $scope.globalEntityMemberLists = [];
                        var taskMemberObj = SaveTaskMemberResult.Response.m_Item2;
                        var taskAssignessObj = $.grep(taskMemberObj, function (e) { return e.RoleID > 1; });
                        $scope.TaskBriefDetails.taskmembersList = taskAssignessObj;
                        $scope.TaskMemberList = taskAssignessObj;
                        SeperateUsers();
                        $('#ngNotifyAddTaskMember').removeClass('disabled');
                        $('#NotifyAdditionalTaskMembers > tbody input:checked').each(function () {
                            $(this).next('i').removeClass('checked');
                        });
                        $timeout(function () { feedforApprovalTask($scope.TaskIDnotify, $scope.notifyDivid, true); }, 2000);
                        NotifySuccess($translate.instant('LanguageContents.Res_4108.Caption'));
                        $('#NotifyAddTaskMemberModalPopup').modal('hide');
                        RefreshCurrentTask();
                    }
                });
            }
            else {
                $('#ngNotifyAddTaskMember').removeClass('disabled');
            }
        }

        function GetUserSelectedAll() {
            var IDList = [];
            $('#NotifyAdditionalTaskMembers  > tbody input:checked').each(function () {
                IDList.push({
                    "Userid": ($(this).attr('data-userid')), "Id": ($(this).attr('data-id')), "TaskID": ($(this).attr('data-taskid')), "Roleid": 4
                , "UserName": ($(this).attr('data-username')), "UserEmail": ($(this).attr('data-UserEmail')), "DepartmentName": ($(this).attr('data-departmentname')),
                    "Role": ($(this).attr('data-role')), "Title": ($(this).attr('data-title'))
                });
            });
            return IDList
        }

        function sortOn(collection, name) {
            collection.sort(
                function (a, b) {
                    if (a[name] <= b[name]) {
                        return (-1);
                    }
                    return (1);
                }
            );
        }

        function sortOnMemberRound(collection, name) {
            collection.sort(
                function (a, b) {
                    if (a[name] <= b[name]) {
                        return (-1);
                    }
                    return (1);
                }
            );
        }

        //File attachment Related Blocks
        $scope.AttachmentFilename = [];
        $scope.globalAttachment = true;
        $scope.addAditionalattachments = function () {
            $scope.IsNotVersioning = true;
            $scope.globalAttachment = false;
            $scope.fnTimeOut();
        }
        $scope.AddAttachments = function () {

            if ($scope.SelectedTaskID > 0)
                $scope.globalAttachment = false;
            else
                $scope.globalAttachment = true;
            $scope.fnTimeOut();
        }
        $scope.fnTimeOut = function () {

            $('#NotifyfilelistWorkFlow').empty();
            $('#NotifydragfilesAttachment').show();
            $("#NotifytotalProgress").empty();
            $("#NotifytotalProgress").append('<span class="pull-left count">0 of 0 Uploaded</span><span class="size">0 B / 0 B</span>'
                                      + '<div class="progress progress-striped active">'
                                      + '<div style="width: 0%" class="bar"></div>'
                                      + '</div>');
            $scope.StrartUpload();
        }

        $scope.StrartUpload = function () {
            var uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                browse_button: 'NotifypickfilesAlternate',
                drop_element: 'NotifydragfilesAttachment',
                container: 'Notifyfilescontainer',
                max_file_size: '10000mb',
                url: 'Handlers/UploadHandler.ashx?Type=Attachment',
                flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                chunk_size: '64Kb',
                multipart_params: {}
            });

            uploader.bind('Init', function (up, params) {
                uploader.splice();
            });

            $('#Notifyuploadfiles').click(function (e) {
                uploader.start();
                e.preventDefault();
            });

            $('#NotifyclearUploader').click(function (e) {
                uploader.destroy();
                $('.moxie-shim').remove();
            });

            uploader.init();

            $('#Notifypickfiles').each(function () {

                var input = new mOxie.FileInput({
                    browse_button: this,
                    multiple: $scope.IsNotVersioning
                });

                input.onchange = function (event) {
                    uploader.addFile(input.files);
                };

                input.init();

            });

            uploader.bind('FilesAdded', function (up, files) {

                if (!$scope.IsNotVersioning) {
                    if (files.length > 1) {
                        uploader.splice(1, files.length - 1);
                        bootbox.alert($translate.instant('LanguageContents.Res_4146.Caption'));
                        return false;
                    }
                }

                $.each(files, function (i, file) {
                    var ste = file.name.split('.')[file.name.split('.').length - 1];
                    var stes = [];
                    stes = [{ ID: file.id, Extension: ste }];
                    if ($scope.listPredfWorkflowFilesAttch == undefined)
                        $scope.listPredfWorkflowFilesAttch = [];

                    $scope.listPredfWorkflowFilesAttch.push({ ID: file.id, Name: file.name, Createdon: new Date.create() });
                    $('#NotifyfilelistWorkFlow').append(
                        '<div id="' + file.id + '" class="attachmentBox" Data-role="Attachment" data-size="' + file.size + '" data-size-uploaded="0" data-status="false">' +
                            '<div class="row-fluid">' +
                            '<div class="span12">' +
                            '<div class="info">' +
                            '<span class="name" >' +
                            '<i class="icon-file-alt"></i>' + file.name +
                            '</span>' +
                            '<span class="pull-right size">0 B / ' + plupload.formatSize(file.size) + '' +
                            '<i class="icon-remove removefile" data-fileid="' + file.id + '"></i>' +
                            '</span>' +
                            '</div>' +
                            '<div class="progress progress-striped active">' +
                            '<div style="width: 0%" class="bar"></div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="row-fluid">' +
                            '<div class="span12">' +
                            '<form id="Form_' + file.id + '" class="form-inline">' +
                            '<select>' +
                            '<option>' + Mimer(ste) + '</option>' +
                            '<option>Video</option>' +
                            '<option>Document</option>' +
                            '<option>Zip</option>' +
                            '</select>' +
                            '<input type="text" name="DescVal_' + file.id + '" id="desc_' + file.id + '" placeholder="Description">' +
                            '</form>' +
                            '</div>' +
                            '</div>' +
                            '</div>'
                    );

                });
                $('#NotifydragfilesAttachment').hide();
                up.refresh(); // Reposition Flash/Silverlight
                TotalUploadProgress();
            });

            $('#NotifyfilelistWorkFlow').on('click', '.removefile', function () {

                var fileToRemove = $(this).attr('data-fileid');

                $.each(uploader.files, function (i, file) {

                    if (file.id == fileToRemove) {
                        uploader.removeFile(file);
                        $('#' + file.id).remove();
                    }


                });

                if ($('#NotifyfilelistWorkFlow .attachmentBox').length == 0) {
                    $('#NotifydragfilesAttachment').show();
                }

                TotalUploadProgress();
            });

            uploader.bind('UploadProgress', function (up, file) {
                $('#' + file.id + " .bar").css("width", file.percent + "%");
                $('#' + file.id + " .size").html(plupload.formatSize(Math.round(file.size * (file.percent / 100))) + ' / ' + plupload.formatSize(file.size));
                $('#' + file.id).attr('data-size-uploaded', Math.round(file.size * (file.percent / 100)));
                TotalUploadProgress();
            });

            uploader.bind('Error', function (up, err) {
                $('#NotifyfilelistWorkFlow').append("<div>Error: " + err.code +
                    ", Message: " + err.message +
                    (err.file ? ", File: " + err.file.name : "") +
                    "</div>"
                );
                up.refresh(); // Reposition Flash/Silverlight
            });

            uploader.bind('FileUploaded', function (up, file, response) {
                $('#' + file.id).attr('data-status', 'true');
                var fileDescription = $('#Form_' + file.id + '').find('input[name="DescVal_' + file.id + '"]').val();
                SaveFileDetails(file, response.response, fileDescription);
                $scope.globalAttachment = true;
                TotalUploadProgress();
            });

            uploader.bind('BeforeUpload', function (up, file) {
                $.extend(up.settings.multipart_params, { id: file.id, size: file.size });
            });

            uploader.bind('FileUploaded', function (up, file, response) {
                var obj = response;
                $('#' + file.id).attr('data-fileId', response.response);
            });

            function TotalUploadProgress() {
                var TotalSize = 0;
                var TotalCount = $('#NotifyfilelistWorkFlow .attachmentBox').length;
                var UploadedSize = 0;
                var UploadedCount = 0;
                $('#NotifyfilelistWorkFlow .attachmentBox').each(function () {
                    TotalSize += parseInt($(this).attr('data-size'));
                    UploadedSize += parseInt($(this).attr('data-size-uploaded'));
                    if ($(this).attr('data-status') == 'true') {
                        UploadedCount += 1;
                    }
                });
                $('#NotifytotalProgress .count').html(UploadedCount + ' of ' + TotalCount + ' Uploaded');
                $('#NotifytotalProgress .size').html(plupload.formatSize(UploadedSize) + ' / ' + plupload.formatSize(TotalSize));
                $('#NotifytotalProgress .bar').css("width", Math.round(((UploadedSize / TotalSize) * 100)) + "%");
            }
        }
        $scope.FileWizardHeader = "Change friendly name";

        $scope.AddNewLinktoTask = function () {
            $scope.EditLink = false;
            $scope.SaveLink = true;
            $scope.HidecfnTitle = "Add link";
            $scope.txtLinkName = "";
            $scope.txtLinkDesc = "";
            $scope.txtLinkURL = "";
            $scope.linkType = 1;
            $("#NotifyaddLinkPopup").modal("show");
        }

        $scope.EditAttachLink = function () {
            if ($scope.txtLinkName != "" && $scope.txtLinkURL != "") {

                //var TaskStatusserv = $resource('task/UpdatetaskLinkDescription/:FileID', { FileID: $scope.contextFileID }, { update: { method: 'PUT' } });
                //var TaskStatusData = new TaskStatusserv();
                var TaskStatusData = {};
                TaskStatusData.FileID = $scope.contextFileID;
                TaskStatusData.FileName = $scope.txtLinkName;
                TaskStatusData.Description = "";
                TaskStatusData.URL = $scope.txtLinkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '');
                TaskStatusData.LinkType = $scope.linkType;
                //var TaskStatusResult = TaskStatusserv.update(TaskStatusData, function () {
                TaskService.UpdatetaskLinkDescription(TaskStatusData).then(function (TaskStatusResult) {
                    if (TaskStatusResult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    }
                    else {
                        if (TaskStatusResult.Response == false) {
                            NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                            $("#NotifyaddLinkPopup").modal("hide");
                        }
                        else {
                            var taskListResObj = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (e) { return e.Id == $scope.contextFileID });
                            if (taskListResObj.length > 0) {
                                var taskAttachmentObj = taskListResObj;
                                if (taskAttachmentObj.length > 0) {
                                    taskAttachmentObj[0].Name = $scope.txtLinkName;
                                    taskAttachmentObj[0].Description = $scope.txtLinkDesc;
                                    taskAttachmentObj[0].LinkURL = $scope.txtLinkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '');
                                    taskAttachmentObj[0].LinkType = $scope.linkType;
                                    $scope.EditLink = false;
                                    $scope.SaveLink = true;
                                    $scope.HidecfnTitle = "Add link";
                                    $scope.txtLinkName = "";
                                    $scope.txtLinkDesc = "";
                                    $scope.linkType = 1;
                                    $scope.txtLinkURL = "";
                                    $scope.linkType = 1;
                                    $("#NotifyaddLinkPopup").modal("hide");
                                }
                            }

                            $timeout(function () { feedforApprovalTask($scope.TaskIDnotify, $scope.notifyDivid, true); }, 2000);
                            NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                        }
                    }
                });
            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_4602.Caption'));
            }
        }

        $scope.fileSaveObject = { "FileFriendlyName": "", "FileDescription": "" };
        $scope.linkSaveObject = { "LinkFriendlyName": "", "linkDescription": "" };
        $scope.EditLink = false;
        $scope.SaveLink = true;
        $scope.HidecfnTitle = "Add link";
        $scope.ChangeFileFriendlyName = function () {
            $('#NotifyFileControls tbody tr').remove();
            var taskListResObj = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (e) { return e.Id == $scope.contextFileID && e.Extension == $scope.ContextFileExtention; });
            if (taskListResObj.length > 0) {
                var taskAttachmentObj = taskListResObj;
                if (taskAttachmentObj.length > 0) {
                    if (taskAttachmentObj[0].Extension != "Link") {
                        $scope.FileWizardHeader = "Change friendly name";
                        var htmlStr = '';
                        htmlStr += '<tr> ';
                        htmlStr += ' <td>Name:</td> ';
                        htmlStr += ' <td> ';
                        htmlStr += '    <input type="text" id="friendlyName" ng-model="fileSaveObject.FileFriendlyName"  placeholder="Friendly Name">';
                        htmlStr += ' </td> ';
                        htmlStr += ' <td>Description:</td> ';
                        htmlStr += ' <td> ';
                        htmlStr += '    <textarea class="small-textarea" id="FileDescriptionVal" ng-model="fileSaveObject.FileDescription" placeholder="File Description" rows="3"></textarea>';
                        htmlStr += ' </td> ';
                        htmlStr += '</tr> ';
                        var trimFileName = taskAttachmentObj[0].Name.substr(0, taskAttachmentObj[0].Name.lastIndexOf('.')) || taskAttachmentObj[0].Name;
                        $scope.fileSaveObject.FileFriendlyName = trimFileName;
                        $scope.fileSaveObject.FileDescription = taskAttachmentObj[0].Description;
                        $scope.FileChange = true;
                        $scope.LinkChange = false;
                        $("#NotifyFileControls tbody").append($compile(htmlStr)($scope));
                        $("#NotifyFileUpdateModal").modal("show");
                    }

                    else {
                        $scope.HidecfnTitle = "Edit link";
                        $scope.txtLinkName = taskAttachmentObj[0].Name;
                        $scope.txtLinkDesc = taskAttachmentObj[0].Description;
                        $scope.txtLinkURL = taskAttachmentObj[0].LinkURL.replace('http://', '').replace('https://', '').replace("ftp://", "").replace("file://", "");
                        $scope.linkType = taskAttachmentObj[0].LinkType;
                        $("#NotifyaddLinkPopup").modal("show");
                        $timeout(function () { $('#NotifytxtLinkName').focus().select(); }, 1000);
                        $scope.EditLink = true;
                        $scope.SaveLink = false;

                    }
                }
            }
        }



        $scope.ChangeFileDescription = function () {
            $('#NotifyFileControls tbody tr').remove();
            var taskListResObj = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (e) { return e.Id == $scope.contextFileID && e.Extension == $scope.ContextFileExtention; });
            if (taskListResObj.length > 0) {
                var taskAttachmentObj = taskListResObj;
                var taskAttachmentObj = taskListResObj;
                if (taskAttachmentObj.length > 0) {
                    if (taskAttachmentObj[0].Extension != "Link") {
                        var taskAttachmentObj = taskListResObj;
                        $scope.FileWizardHeader = "Change file description";
                        var htmlStr = '';
                        htmlStr += '<tr> ';
                        htmlStr += ' <td>Description:</td> ';
                        htmlStr += ' <td> ';
                        htmlStr += '    <textarea class="large-textarea" id="FileDescriptionVal" ng-model="fileSaveObject.FileDescription" placeholder="File Description" rows="3"></textarea>';
                        htmlStr += ' </td> ';
                        htmlStr += '</tr> ';
                        var trimFileName = taskAttachmentObj[0].Name.substr(0, taskAttachmentObj[0].Name.lastIndexOf('.')) || taskAttachmentObj[0].Name;
                        $scope.fileSaveObject.FileFriendlyName = trimFileName;
                        $scope.fileSaveObject.FileDescription = taskAttachmentObj[0].Description;
                        $("#NotifyFileControls tbody").append($compile(htmlStr)($scope));
                        $scope.NotifyFileChange = true;
                        $scope.NotifyLinkChange = false;
                        $("#NotifyFileUpdateModal").modal("show");
                    }
                    else {
                        $scope.FileWizardHeader = "Change file description";
                        var htmlStr = '';
                        htmlStr += '<tr> ';
                        htmlStr += ' <td>Description:</td> ';
                        htmlStr += ' <td> ';
                        htmlStr += '    <textarea class="large-textarea" id="FileDescriptionVal" ng-model=" linkSaveObject.linkFriendlyName" placeholder="File Description" rows="3"></textarea>';
                        htmlStr += ' </td> ';
                        htmlStr += '</tr> ';
                        var trimLinkName = taskAttachmentObj[0].Name.substr(0, taskAttachmentObj[0].Name.lastIndexOf('.')) || taskAttachmentObj[0].Name;
                        $scope.fileSaveObject.FileFriendlyName = trimLinkName;
                        $scope.fileSaveObject.FileDescription = taskAttachmentObj[0].Description;
                        $("#NotifyFileControls tbody").append($compile(htmlStr)($scope));
                        $scope.NotifyFileChange = false;
                        $scope.NotifyLinkChange = true;
                        $("#NotifyFileUpdateModal").modal("show");
                    }
                }
            }
        }


        // Delete the attachment either it is link or file....
        $scope.DeleteFile = function (e) {

            if ($scope.ContextFileExtention == "Link") {
                bootbox.confirm($translate.instant('LanguageContents.Res_2022.Caption'), function (result) {
                    if (result) {
                        $timeout(function () {
                            //var DeleteAttachment = $resource('task/DeleteTaskLinkByid/:ID/:EntityId', { ID: $scope.contextFileID, EntityId: $scope.TaskBriefDetails.taskID });
                            //var deleteattach = DeleteAttachment.delete(function () {
                            TaskService.DeleteTaskLinkByid($scope.contextFileID, $scope.TaskBriefDetails.taskID).then(function (deleteattach) {
                                if (deleteattach.Response == true) {
                                    var fileObj = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (e) { return e.Id == $scope.contextFileID; });
                                    $scope.TaskBriefDetails.taskAttachmentsList.splice($.inArray(fileObj[0], $scope.TaskBriefDetails.taskAttachmentsList), 1);
                                    NotifySuccess($translate.instant('LanguageContents.Res_4793.Caption'));
                                } else {
                                    NotifyError($translate.instant('LanguageContents.Res_4297.Caption'));
                                }
                            })
                        }, 100);
                    }
                });

            }
            else {
                bootbox.confirm($translate.instant('LanguageContents.Res_2023.Caption'), function (result) {
                    if (result) {
                        $timeout(function () {
                            //var DeleteAttachment = $resource('task/DeleteTaskFileByid/:ID/:EntityId', { ID: $scope.contextFileID, EntityId: $scope.TaskBriefDetails.taskID });
                            //var deleteattach = DeleteAttachment.delete(function () {
                            TaskService.DeleteTaskFileByid($scope.contextFileID, $scope.TaskBriefDetails.taskID).then(function (deleteattach) {
                                if (deleteattach.Response == true) {
                                    var fileObj = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (e) { return e.Id == $scope.contextFileID; });
                                    $scope.TaskBriefDetails.taskAttachmentsList.splice($.inArray(fileObj[0], $scope.TaskBriefDetails.taskAttachmentsList), 1);
                                    NotifySuccess($translate.instant('LanguageContents.Res_4793.Caption'));
                                } else {
                                    NotifyError($translate.instant('LanguageContents.Res_4297.Caption'));
                                }
                            });
                        }, 100);
                    }
                });
            }
            $timeout(function () { feedforApprovalTask($scope.TaskIDnotify, $scope.notifyDivid, true); }, 2000);
        };

        //Delete Attachment before createtask
        $scope.deleteAttachedFiles = function (fileObject) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2024.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        if (fileObject.Extension != "link") {
                            var attachmentDelete = $.grep($scope.AttachmentFilename, function (e) { return e.FileGuid == fileObject.FileGuid });
                            $scope.AttachmentFilename.splice($.inArray(attachmentDelete[0], $scope.AttachmentFilename), 1);
                            var fileToDelete = $.grep($scope.FileList, function (e) { return e.FileGuid == fileObject.FileGuid });
                            $scope.FileList.splice($.inArray(fileToDelete[0], $scope.FileList), 1);
                            if (attachmentDelete[0].ID > 0) {
                                //var DeletetaskAttachmentServ = $resource("task/DeleteAttachments/:ActiveFileid", { ActiveFileid: attachmentDelete[0].ID });
                                //var result = DeletetaskAttachmentServ.delete(function () {
                                TaskService.DeleteAttachments(attachmentDelete[0].ID).then(function (result) {
                                    if (result != null && result.Response != false) {
                                        NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                                        ReloadTaskAttachments($scope.SelectedTaskID);
                                    }
                                });
                            }
                        } else {
                            if (fileObject.ID == 0) {
                                var attachmentDelete = $.grep($scope.AttachmentFilename, function (e) { return e.LinkID == fileObject.LinkID });
                                $scope.AttachmentFilename.splice($.inArray(attachmentDelete[0], $scope.AttachmentFilename), 1);
                            } else if (fileObject.ID > 0) {
                                var attachmentDelete = $.grep($scope.AttachmentFilename, function (e) { return e.ID == fileObject.ID });
                                $scope.AttachmentFilename.splice($.inArray(attachmentDelete[0], $scope.AttachmentFilename), 1);

                                //var DeletetaskAttachmentServ = $resource("task/DeleteLinkByID/:ID", { ID: attachmentDelete[0].ID });
                                //var result = DeletetaskAttachmentServ.delete(function () {
                                TaskService.DeleteLinkByID(attachmentDelete[0].ID).then(function (result) {
                                    if (result != null && result.Response != false) {
                                        NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                                        ReloadTaskAttachments($scope.SelectedTaskID);
                                    }
                                });
                            }
                        }
                    }, 100);
                }
            });
        }

        $scope.GetAttachmentResponse = [];
        $scope.openattachment = function () {
            //var GetAttachmentList = $resource('common/GetFilesandLinksByEntityID/:EntityID', { EntityID: $scope.TaskBriefDetails.EntityID });
            //var getattachments = GetAttachmentList.get(function () {
            CommonService.GetFilesandLinksByEntityID($scope.TaskBriefDetails.EntityID).then(function (getattachments) {
                $scope.getattachmentslist = getattachments.Response;
                if ($scope.getattachmentslist.length != 0) {
                    $scope.GetAttachmentResponse = $scope.getattachmentslist;
                    $scope.attachmentsNotificationTaskActionTableHeader = true;
                    $scope.attachmentsNotificationTaskActionTableData = true;
                    $scope.noattachments = '';
                }
                else {
                    $scope.attachmentsNotificationTaskActionTableHeader = false;
                    $scope.attachmentsNotificationTaskActionTableData = false;
                    $scope.noattachments = 'No attachment present';
                }
            });
        }


        $scope.LinkSelected = [];
        $scope.FileSelected = [];
        $scope.attachmentid = function (e) {
            var check = e.target.checked;
            var extn = e.target.attributes[1].nodeValue;
            var checkedid = e.target.attributes[2].nodeValue;
            if (check == true) {//check true
                if (extn == "Link") {
                    $scope.LinkSelected.push(checkedid);
                }
                else {
                    $scope.FileSelected.push(checkedid);
                }
            }
            else {//checked false
                if (extn == "Link") {
                    $scope.LinkSelected = jQuery.grep($scope.LinkSelected, function (value) {
                        return value != checkedid;
                    });
                }
                else {
                    $scope.FileSelected = jQuery.grep($scope.FileSelected, function (value) {
                        return value != checkedid;
                    });
                }
            }
        }

        $scope.CheckListSelection = function (status) {
            var baseClass = "checkbox";
            if (status)
                baseClass += " checked";
            return baseClass;
        }

        $scope.contextCheckListID = 0;
        $scope.contextCheckListIndex = 0;
        $scope.SetcontextCheckListID = function (id, indexval) {
            $scope.contextCheckListID = id;
            $scope.contextCheckListIndex = indexval;
        };


        $scope.DeleteEntityChecklists = function () {
            bootbox.confirm($translate.instant('LanguageContents.Res_2012.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        if ($scope.contextCheckListID != 0) {
                            //var DeleteChkList = $resource("task/DeleteEntityCheckListByID/:ID", { ID: $scope.contextCheckListID });
                            //var result = DeleteChkList.delete(function () {
                            TaskService.DeleteEntityCheckListByID($scope.contextCheckListID).then(function (result) {
                                if (result.StatusCode == 200) {

                                    var fileObj = $.grep($scope.TaskBriefDetails.taskCheckList, function (e) { return e.Id == $scope.contextCheckListID; });
                                    $scope.TaskBriefDetails.taskCheckList.splice($.inArray(fileObj[0], $scope.TaskBriefDetails.taskCheckList), 1);
                                    RefreshCurrentTask();
                                }

                            });
                        } else {
                            $scope.TaskBriefDetails.taskCheckList.splice($scope.contextCheckListIndex, 1);
                        }
                    }, 100);

                }
            });

            $timeout(function () { feedforApprovalTask($scope.TaskIDnotify, $scope.notifyDivid, true); }, 2000);
        }

        $scope.SaveTaskCheckList = function (Index, CheckListID, ChklistName, ChkListStatus, CheCklistExisting) {
            var checkbox;
            if (ChkListStatus.target != undefined) {
                checkbox = ChkListStatus.target.checked;
            }
            else {
                checkbox = ChkListStatus;
            }
            if (ChklistName != '-' && ChklistName.length > 0) {
                //var SaveTaskData = $resource('task/InsertUpdateEntityTaskCheckList/');
                //var SaveTask = new SaveTaskData();
                var SaveTask = {};
                SaveTask.Id = CheckListID;
                SaveTask.taskId = $scope.TaskBriefDetails.taskID;
                SaveTask.ChkListStatus = checkbox;
                SaveTask.ISowner = (CheckListID > 0 ? false : true);
                SaveTask.SortOrder = (Index + 1);
                SaveTask.CheckListName = ChklistName;
                SaveTask.isnew = CheCklistExisting == undefined ? false : true;
                //var SaveTaskResult = SaveTaskData.save(SaveTask, function () {
                TaskService.InsertUpdateEntityTaskCheckList(SaveTask).then(function (SaveTaskResult) {
                    if (SaveTaskResult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4263.Caption'));
                    }
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4157.Caption'));
                        var Object = $.grep($scope.TaskBriefDetails.taskCheckList, function (n, i) {
                            if (i == Index) {
                                return $scope.TaskBriefDetails.taskCheckList[i];
                            }
                        });
                        Object[0].Id = SaveTaskResult.Response;
                        Object[0].Status = checkbox;
                        Object[0].IsExisting = true;
                        RefreshCurrentTask();
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1872.Caption'));
                CheCklistExisting = true;
            }

            $timeout(function () { feedforApprovalTask($scope.TaskIDnotify, $scope.notifyDivid, true); }, 2000);
        }

        $scope.AddCheckListNext = function () {
            var Object = $.grep($scope.TaskBriefDetails.taskCheckList, function (n, i) {
                if (n.IsExisting == false) {
                    return $scope.TaskBriefDetails.taskCheckList[i];
                }
            });
            if (Object.length > 0) {
                if (Object[0].Name.length == 0) {
                    if (Object[0].Id == 0) {
                        $scope.TaskBriefDetails.taskCheckList.splice($scope.TaskBriefDetails.taskCheckList.indexOf(Object[0]), 1);
                    } else {
                        Object[0].IsExisting = true;
                        Object[0].Name = CurrentCheckListName;
                    }
                } else {
                    Object[0].Name = CurrentCheckListName;
                    Object[0].IsExisting = true;
                }
            }
            if ($scope.contextCheckListIndex == -1) {
                $scope.TaskBriefDetails.taskCheckList.push({ Id: 0, Name: '', OwnerId: 0, OwnerName: 0, SortOrder: 1, Status: false, TaskId: $scope.TaskBriefDetails.taskID, UserId: 0, UserName: "", CompletedOnValue: "", CompletedOn: null, IsExisting: false });
            } else {
                $scope.TaskBriefDetails.taskCheckList.splice(parseInt($scope.contextCheckListIndex) + 1, 0, { Id: 0, Name: '', OwnerId: 0, OwnerName: 0, SortOrder: 1, Status: false, TaskId: $scope.TaskBriefDetails.taskID, UserId: 0, UserName: "", CompletedOnValue: "", CompletedOn: null, IsExisting: false });
            }
            $timeout(function () { $('[datafocus-id =0]').focus(); }, 10);
        }


        $scope.SaveAttachfile = function () {
            if ($scope.LinkSelected.length == 0 && $scope.FileSelected.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_1873.Caption'));
                return false;
            }
            var items = GetRootLevelSelectedAll();
            var IDList = new Array();
            IDList = GetRootLevelSelectedAll();
            //var TaskStatusserv = $resource('task/CopyAttachmentsfromtask/:EntityID', { EntityID: $scope.TaskBriefDetails.taskID }, { update: { method: 'PUT' } });
            //var TaskStatusData = new TaskStatusserv();
            var TaskStatusData = {};
            TaskStatusData.EntityID = $scope.TaskBriefDetails.taskID;
            TaskStatusData.linkIDs = $scope.LinkSelected;
            TaskStatusData.ActivFileIDs = $scope.FileSelected;
            //var TaskStatusResult = TaskStatusserv.update(TaskStatusData, function () {
            TaskService.CopyAttachmentsfromtask(TaskStatusData).then(function (TaskStatusResult) {
                if (TaskStatusResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    $('#NotifyGeneralattachmentsPopup').modal('hide');
                }
                else {
                    if (TaskStatusResult.Response == false) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        $("#NotifyFileUpdateModal").modal("hide");
                    }
                    else {
                        if (TaskStatusResult.StatusCode == 200) {
                            if (TaskStatusResult.Extension == "Link") {
                                ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
                                NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                                $('#NotifyGeneralattachmentsPopup').modal('hide');
                            }
                            else {
                                ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
                                NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                                $('#NotifyGeneralattachmentsPopup').modal('hide');
                            }
                        }
                    }
                }
                $scope.LinkSelected = [];
                $scope.FileSelected = [];
            });
        };

        function GetRootLevelSelectedAll() {
            var IDList = new Array();
            $('#NotifyListContainer > tbody input:checked').each(function () {
                IDList.push($(this).attr('data-id'));
            });
            return IDList
        }

        function parseSize(size) {
            var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"],
                tier = 0;
            while (size >= 1024) {
                size = size / 1024;
                tier++;
            }
            return Math.round(size * 10) / 10 + " " + suffix[tier];
        }


        $scope.Clear = function () {
        }

        $scope.FileList = [];
        $scope.UniqueLinkID = 0;

        $scope.InsertableFileList = [];
        $scope.InsertableAttachmentFilename = [];

        function SaveFileDetails(file, response, fileDescription) {
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");

            $scope.FileList.push({
                "Name": file.name, "VersionNo": 1, "MimeType": resultArr[1], "Extension": extension, "OwnerID": $cookies['UserId'],
                "CreatedOn": Date.now(), "Checksum": "", "ModuleID": 1, "EntityID": $routeParams.ID, "Size": file.size, "FileGuid": resultArr[0], "FileDescription": fileDescription
            });

            $scope.InsertableFileList.push({
                "Name": file.name, "VersionNo": 1, "MimeType": resultArr[1], "Extension": extension, "OwnerID": $cookies['UserId'],
                "CreatedOn": Date.now(), "Checksum": "", "ModuleID": 1, "EntityID": $routeParams.ID, "Size": file.size, "FileGuid": resultArr[0], "FileDescription": fileDescription
            });


            $scope.AttachmentFilename.push({ "FileName": file.name, "Extension": extension, "Size": parseSize(file.size), "FileGuid": resultArr[0], "FileDescription": fileDescription, "URL": "", "LinkType": "", "ID": 0, "LinkID": 0 });
            $scope.InsertableAttachmentFilename.push({ "FileName": file.name, "Extension": extension, "Size": parseSize(file.size), "FileGuid": resultArr[0], "FileDescription": fileDescription, "ID": 0 });
            if ($scope.IsNotVersioning == true) {
                if ($scope.TaskBriefDetails.taskID > 0) {

                    //var SaveTaskData = $resource('task/InsertEntityTaskAttachments/');
                    //var SaveTask = new SaveTaskData();
                    var SaveTask = {};
                    SaveTask.TaskID = $scope.TaskBriefDetails.taskID;
                    SaveTask.TaskAttachments = $scope.InsertableAttachmentFilename;
                    SaveTask.TaskFiles = $scope.InsertableFileList;
                    // var SaveTaskResult = SaveTaskData.save(SaveTask, function () {
                    TaskService.InsertEntityTaskAttachments(SaveTask).then(function (SaveTaskResult) {
                        if (SaveTaskResult.StatusCode == 405) {
                            NotifyError($translate.instant('LanguageContents.Res_4275.Caption'));
                            $scope.globalAttachment = true;
                        }
                        else {
                            NotifySuccess($translate.instant('LanguageContents.Res_4388.Caption'));
                            $scope.InsertableFileList = [];
                            $scope.InsertableAttachmentFilename = [];
                            ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
                        }
                    });

                }
            }
            else {
                if ($scope.TaskBriefDetails.taskID > 0) {
                    var objfile = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (n, i) {
                        return $scope.TaskBriefDetails.taskAttachmentsList[i].Id == $scope.FileID;
                    });
                    if (objfile != null && objfile.length > 0) {
                        //var SaveTaskData = $resource('task/InsertEntityTaskAttachmentsVersion/');
                        //var SaveTask = new SaveTaskData();
                        var SaveTask = {};
                        SaveTask.TaskID = $scope.TaskBriefDetails.taskID;
                        SaveTask.TaskAttachments = $scope.InsertableAttachmentFilename;
                        SaveTask.TaskFiles = $scope.InsertableFileList;
                        SaveTask.VersioningFileId = objfile[0].VersioningFileId;
                        SaveTask.FileId = $scope.FileID;
                        //var SaveTaskResult = SaveTaskData.save(SaveTask, function () {
                        TaskService.InsertEntityTaskAttachmentsVersion(SaveTask).then(function (SaveTaskResult) {
                            if (SaveTaskResult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4275.Caption'));
                                $scope.globalAttachment = true;
                            }
                            else {
                                ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
                                ReloadTaskVersionDetails();
                                $scope.contextFileID = SaveTaskResult.Response;
                                $scope.FileID = SaveTaskResult.Response;
                                NotifySuccess($translate.instant('LanguageContents.Res_4388.Caption'));
                                $scope.InsertableFileList = [];
                                $scope.InsertableAttachmentFilename = [];
                                RefreshCurrentTask();
                            }
                        });
                    }
                }
            }
            $timeout(function () { feedforApprovalTask($scope.TaskIDnotify, $scope.notifyDivid, true); }, 2000);
        }

        function ReloadTaskAttachments(taskID) {
            //var GetTaskDetails = $resource('task/GetEntityTaskAttachmentFile/:TaskID', { TaskID: taskID }, { get: { method: 'GET' } });
            //var TaskDetailList = GetTaskDetails.get({ TaskID: taskID }, function () {
            TaskService.GetEntityTaskAttachmentFile(taskID).then(function (TaskDetailList) {
                $scope.attachedFiles = TaskDetailList.Response;
                $scope.TaskBriefDetails.taskAttachmentsList = TaskDetailList.Response;

            });

        }

        function ReloadTaskVersionDetails(TaskID) {
            if ($scope.TaskVersionFileList.length > 0) {
                $scope.TaskVersionFileList = [];
                $scope.ViewAllFiles();
            }

        }


        $scope.OpenAttachLink = function () {
            $scope.txtLinkName = "";
            $scope.txtLinkURL = "";
            $scope.linkType = 1;
            $("#NotifyaddLinkPopup").modal("show");

        }


        $scope.SaveActiveVersion = function () {
            if ($scope.versionFileId != 0) {
                //var SaveVerionNo = $resource('task/UpdateAttachmentVersionNo/:TaskID/:SelectedVersion/:VersioningFileId', { TaskID: $scope.TaskBriefDetails.taskID, SelectedVersion: $scope.SelectedVersionID, VersioningFileId: $scope.versionFileId });
                //var SaveResult = SaveVerionNo.save({ TaskID: $scope.TaskBriefDetails.taskID, SelectedVersion: $scope.SelectedVersionID }, function () {
                TaskService.UpdateAttachmentVersionNo($scope.TaskBriefDetails.taskID, $scope.SelectedVersionID, $scope.versionFileId).then(function (SaveResult) {
                    ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
                    $("#NotifyAttachmentVersion").modal('hide');
                });
            }
        }

        //Update the taskStatus
        $scope.UpdateTaskStatus = function (taskID, StatusID) {
            //var TaskStatusserv = $resource('task/UpdateTaskStatus/:TaskID/:Status', { TaskID: taskID, Status: StatusID }, { update: { method: 'PUT' } });
            //var TaskStatusData = new TaskStatusserv();
            //var TaskStatusResult = TaskStatusserv.update(TaskStatusData, function () {
            TaskService.UpdateTaskStatus(taskID, StatusID).then(function (TaskStatusResult) {
                if (TaskStatusResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
                }
                else {
                    if (TaskStatusResult.Response.m_Item1 == false) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1874.Caption'));
                        $('#NotifyWorkMyTaskPopup').hide();
                        $('#NotifyApprovalMyTaskPopup').hide();
                        $('#NotifyReviewMyTaskPopup').hide();
                        $('#loadtaskedit').hide();
                    }
                    else {
                        $('#NotifyWorkMyTaskPopup').hide();
                        $('#NotifyApprovalMyTaskPopup').hide();
                        $('#NotifyReviewMyTaskPopup').hide();
                        $('#loadtaskedit').hide();
                        RefreshCurrentTask();
                        NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                        $("#loadNotificationtask").trigger("ontaskActionnotification");
                    }
                }
            });
        }

        $scope.TaskVersionFileList = [];
        $scope.ViewAllFiles = function () {
            $scope.FileID = $scope.contextFileID;
            var objfile = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (n, i) {
                return $scope.TaskBriefDetails.taskAttachmentsList[i].Id == $scope.FileID;
            });
            if (objfile != null && objfile.length > 0) {
                $scope.versionFileId = objfile[0].VersioningFileId;
                var taskid = $scope.TaskBriefDetails.taskID;
                //var ViewFiles = $resource('task/ViewAllFilesByEntityID/:TaskID/:VersionFileID', { TaskID: $scope.TaskBriefDetails.taskID, VersionFileID: objfile[0].VersioningFileId }, { get: { method: 'GET' } });
                //var FileResult = ViewFiles.get({ TaskID: $scope.TaskBriefDetails.taskID }, function () {
                TaskService.ViewAllFilesByEntityID($scope.TaskBriefDetails.taskID, objfile[0].VersioningFileId).then(function (FileResult) {
                    if (FileResult.Response != null) {
                        $scope.TaskVersionFileList = FileResult.Response;
                        $scope.SelectedVersionID = $scope.TaskVersionFileList[0].VersionNo;
                    }
                });
            }
        }


        $scope.SaveFileObject = function () {
            //var TaskStatusserv = $resource('task/UpdatetaskAttachmentDescription/:FileID', { FileID: $scope.contextFileID }, { update: { method: 'PUT' } });
            //var TaskStatusData = new TaskStatusserv();
            var TaskStatusData = {};
            TaskStatusData.FileID = $scope.contextFileID;
            TaskStatusData.FileName = $scope.fileSaveObject.FileFriendlyName;
            TaskStatusData.Description = $scope.fileSaveObject.FileDescription;
            //  var TaskStatusResult = TaskStatusserv.update(TaskStatusData, function () {
            TaskService.UpdatetaskAttachmentDescription(TaskStatusData).then(function (TaskStatusResult) {
                if (TaskStatusResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                }
                else {
                    if (TaskStatusResult.Response == false) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        $("#NotifyFileUpdateModal").modal("hide");
                    }
                    else {
                        var taskListResObj = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (e) { return e.Id == $scope.contextFileID });
                        if (taskListResObj.length > 0) {
                            var taskAttachmentObj = taskListResObj;
                            if (taskAttachmentObj.length > 0) {

                                taskAttachmentObj[0].Name = $scope.fileSaveObject.FileFriendlyName;
                                taskAttachmentObj[0].Description = $scope.fileSaveObject.FileDescription;
                                $("#NotifyFileUpdateModal").modal("hide");

                            }
                        }
                        NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                    }
                }
            });
        }


        $scope.SaveLinkObject = function () {
            //var TaskStatusserv = $resource('task/UpdatetaskLinkDescription/:FileID', { FileID: $scope.contextFileID }, { update: { method: 'PUT' } });
            //var TaskStatusData = new TaskStatusserv();
            var TaskStatusData = {};
            TaskStatusData.FileID = $scope.contextFileID;
            TaskStatusData.FileName = $scope.linkSaveObject.linkFriendlyName;
            TaskStatusData.Description = $scope.linkSaveObject.linkDescription;

            //var TaskStatusResult = TaskStatusserv.update(TaskStatusData, function () {
            TaskService.UpdatetaskLinkDescription(TaskStatusData).then(function (TaskStatusResult) {
                if (TaskStatusResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                }
                else {
                    if (TaskStatusResult.Response == false) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        $("#NotifyFileUpdateModal").modal("hide");
                    }
                    else {
                        var taskListResObj = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (e) { return e.Id == $scope.contextFileID });
                        if (taskListResObj.length > 0) {
                            var taskAttachmentObj = taskListResObj;
                            if (taskAttachmentObj.length > 0) {

                                taskAttachmentObj[0].Name = $scope.linkSaveObject.linkFriendlyName;
                                taskAttachmentObj[0].Description = $scope.linkSaveObject.linkDescription;
                                $("#NotifyFileUpdateModal").modal("hide");

                            }
                        }
                        NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                    }
                }
            });

        }


        //Populate TaskList Data for Update

        $scope.SetContextMemberID = function (id) {
            $scope.ContextMemberID = id;
        };

        $scope.NotifyDeletableAssigneeName = "";

        $scope.NotifySetDeletebleAssigneName = function (Name) {
            $scope.NotifyDeletableAssigneeName = Name;
        };

        $scope.DeleteTaskMembers = function () {
            //var DeletetaskMembersServ = $resource("task/DeleteTaskMemberById/:ID/:TaskID", { ID: $scope.ContextMemberID, TaskID: $scope.TaskBriefDetails.taskID });
            bootbox.confirm($translate.instant('LanguageContents.Res_2043.Caption') + $scope.NotifyDeletableAssigneeName + " ?", function (result) {
                if (result) {
                    $timeout(function () {
                        // var delresult = DeletetaskMembersServ.delete(function () {
                        TaskService.DeleteTaskMemberById($scope.ContextMemberID, $scope.TaskBriefDetails.taskID).then(function (delresult) {
                            if (delresult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4308.Caption'));
                            } else {
                                if (delresult.Response == false) {

                                    bootbox.alert($translate.instant('LanguageContents.Res_1875.Caption'));
                                } else {
                                    NotifySuccess($translate.instant('LanguageContents.Res_4480.Caption'));
                                    $scope.NotifyDeletableAssigneeName = "";
                                    RefreshCurrentTask();
                                }
                            }
                        });
                    }, 100);
                }
            });

            $timeout(function () { feedforApprovalTask($scope.TaskIDnotify, $scope.notifyDivid, true); }, 2000);
        }

        $scope.CloseCheckListPopUp = function (Index, CheCklistExisting, ChklistName) {
            if ($scope.TaskBriefDetails.taskCheckList[Index].Id == 0) {
                $scope.TaskBriefDetails.taskCheckList.splice(Index, 1);
            } else {
                $scope.TaskBriefDetails.taskCheckList[Index].Name = CurrentCheckListName;
                $scope.TaskBriefDetails.taskCheckList[Index].IsExisting = true;
            }
            $("#NonEditableCL" + Index).show();
            $("#EditableCL" + Index).hide();
        }


        var fundrequestID = 0;
        $scope.NotifyFund_newsfeed_TEMP = [];
        var divtypeid = '';
        $scope.IsnewcommentAvailable = 0;

        //------------> CALL THE TASK FEEDS ON PAGE LOAD <--------------
        function feedforApprovalTask(entityid, divid, isLatestnewsfeed) {
            var tempPageno = 0;
            if (isLatestnewsfeed == false) {
                $('#' + divid).html('');
                PagenoforScroll = 0;
                $scope.temptaskDiv = divid;
                $scope.TaskFeedReloadOnScroll();
                tempPageno = 0;
            }
            else
                tempPageno = -1;

            fundrequestID = entityid;
            divtypeid = divid;
            // var fundingreqNewsFeed = $resource('common/GetEnityFeedsForFundingReq/:EntityID/:PageNo/:islatestfeed/');
            //---------------------------  Get the task feed ------------------------------------
            try {
                $scope.tempTaskID = entityid;
                //var getFundingreqNewsFeedResult = fundingreqNewsFeed.get({ EntityID: entityid, PageNo: tempPageno, islatestfeed: isLatestnewsfeed }, function () {
                CommonService.GetEnityFeedsForFundingReq(entityid, tempPageno, isLatestnewsfeed).then(function (getFundingreqNewsFeedResult) {
                    var fundingreqfundingreqfeeddivHtml = '';

                    if (getFundingreqNewsFeedResult.Response.length > 0) {
                        if (!isLatestnewsfeed) {
                            $scope.NotifyFund_newsfeed_TEMP = [];
                        }
                        $scope.NotifyFund_newsfeed_TEMP.push(getFundingreqNewsFeedResult.Response);
                    }
                    for (var i = 0 ; i < getFundingreqNewsFeedResult.Response.length; i++) {
                        var feedcomCount = getFundingreqNewsFeedResult.Response[i].FeedComment != null ? getFundingreqNewsFeedResult.Response[i].FeedComment.length : 0;
                        var fundingreqfeeddivHtml = '';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li data-parent="NewsParent" data-ID=' + getFundingreqNewsFeedResult.Response[i].FeedId + '>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"> <img src=Handlers/UserImage.ashx?id=' + getFundingreqNewsFeedResult.Response[i].Actor + '&time=' + $scope.NewTime + ' alt="Avatar"></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getFundingreqNewsFeedResult.Response[i].UserEmail + '>' + getFundingreqNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + getFundingreqNewsFeedResult.Response[i].FeedText + '</p></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getFundingreqNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML1_' + divid + '"  data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedId + '" id="NotifyTaskComment_' + getFundingreqNewsFeedResult.Response[i].FeedId + '" >Comment</a></span>';
                        //fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo" > ' + feedcomCount + ' Comment(s)<i class="icon-caret-down"    data-DynHTML="notifytaskCommentshowHTML" data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedComment + '"   id="Notifyfeed_' + getFundingreqNewsFeedResult.Response[i].FeedId + '" ></i></span></div></div></div>';
                        if (feedcomCount > 1) {
                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="notifytaskCommentshowHTML"  data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedComment + '"   id="Notifyfeed_' + getFundingreqNewsFeedResult.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                        }
                        else {
                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="notifytaskCommentshowHTML"  data-commnetid="' + getFundingreqNewsFeedResult.Response[i].FeedComment + '"   id="Notifyfeed_' + getFundingreqNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                        }
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul class="subComment">';
                        if (getFundingreqNewsFeedResult.Response[i].FeedComment != null && getFundingreqNewsFeedResult.Response[i].FeedComment != '') {
                            var j = 0;
                            if (getFundingreqNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                            }
                            else {
                                $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getFundingreqNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                            }

                            for (var k = 0; k < getFundingreqNewsFeedResult.Response[i].FeedComment.length; k++) {
                                $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getFundingreqNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                if (k == 0) {
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li';
                                }
                                else {
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li style = "display:none;"';
                                }

                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + ' id="feedcomment1_' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt">';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getFundingreqNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></div></li>';
                            }
                        }
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</ul></li>';
                        if ($scope.IsnewcommentAvailable == 0) {
                            $('#' + divid).append(fundingreqfeeddivHtml);
                            $('#' + divid).scrollTop(0);
                        }
                        else {
                            $('#' + divid).prepend(fundingreqfeeddivHtml);
                        }

                        var duplicateChk = {};
                        $('#' + divid + '[data-id]').each(function () {
                            if (duplicateChk.hasOwnProperty(this.id)) {

                                $(this).remove();
                            } else {
                                duplicateChk[this.id] = 'true';
                            }
                        });
                    }
                    commentclick(divid);
                    $('#' + divid).on('click', 'a[data-command="openlink"]', function (event) {
                        var TargetControl = $(this);
                        var mypage = TargetControl.attr('data-Name');
                        var myname = TargetControl.attr('data-Name');
                        var w = 1200;
                        var h = 800
                        var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
                        var win = window.open(mypage, myname, winprops)
                    });

                });
            }
            catch (e)
            { }
        }
        $scope.Timerforcallback = function (entityid, divid, stat) {
            feedforApprovalTask(parseInt(entityid), divid, true);
            NewsFeedUniqueTimerForTask = $timeout(function () {
                if ($('#' + divid).css('display') != 'none') {
                    $scope.Timerforcallback(parseInt(entityid), divid, true);
                }
            }, 30000);
        }

        //---------------------------  Get the task feed on page scroll------------------------------------
        var PagenoforScroll = 0;

        $scope.TaskFeedReloadOnScroll = function () {
            try {
                $('#' + $scope.temptaskDiv).scroll(function () {
                    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                        PagenoforScroll += 2;

                        //var latestNewsFeed = $resource('common/GetEnityFeedsForFundingReq/:EntityID/:PageNo/:islatestfeed/');
                        //var latestNewsFeedResult = latestNewsFeed.get({ EntityID: $scope.tempTaskID, PageNo: PagenoforScroll, islatestfeed: false }, function () {
                        CommonService.GetEnityFeedsForFundingReq($scope.tempTaskID, PagenoforScroll, false).then(function (latestNewsFeedResult) {
                            var fundingreqfundingreqfeeddivHtml = '';
                            if (latestNewsFeedResult.Response.length > 0) {
                                $scope.NotifyFund_newsfeed_TEMP.push(latestNewsFeedResult.Response);
                                for (var i = 0 ; i < latestNewsFeedResult.Response.length; i++) {
                                    var feedcomCount = latestNewsFeedResult.Response[i].FeedComment != null ? latestNewsFeedResult.Response[i].FeedComment.length : 0;
                                    var fundingreqfeeddivHtml = '';

                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li data-parent="NewsParent" data-ID=' + latestNewsFeedResult.Response[i].FeedId + '>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"> <img src=Handlers/UserImage.ashx?id=' + latestNewsFeedResult.Response[i].Actor + '&time=' + $scope.NewTime + ' alt="Avatar"></div>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + latestNewsFeedResult.Response[i].UserEmail + '>' + latestNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + latestNewsFeedResult.Response[i].FeedText + '</p></div>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + latestNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="CommentHTML1_' + $scope.temptaskDiv + '"  data-commnetid="' + latestNewsFeedResult.Response[i].FeedId + '" id="NotifyTaskComment_' + latestNewsFeedResult.Response[i].FeedId + '" >Comment</a></span>';
                                    //fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo" > ' + feedcomCount + ' Comment(s)<i class="icon-caret-down"    data-DynHTML="notifytaskCommentshowHTML" data-commnetid="' + latestNewsFeedResult.Response[i].FeedComment + '"   id="Notifyfeed_' + latestNewsFeedResult.Response[i].FeedId + '" ></i></span></div></div></div>';

                                    if (feedcomCount > 1) {
                                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="notifytaskCommentshowHTML"  data-commnetid="' + latestNewsFeedResult.Response[i].FeedComment + '"   id="Notifyfeed_' + latestNewsFeedResult.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                                    }
                                    else {
                                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="cmntInfo"><a data-DynHTML="notifytaskCommentshowHTML"  data-commnetid="' + latestNewsFeedResult.Response[i].FeedComment + '"   id="Notifyfeed_' + latestNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                                    }
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul class="subComment">';


                                    if (latestNewsFeedResult.Response[i].FeedComment != null && latestNewsFeedResult.Response[i].FeedComment != '') {
                                        var j = 0;

                                        if (latestNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                            $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                        }
                                        else {
                                            $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + latestNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                        }

                                        for (var k = 0; k < latestNewsFeedResult.Response[i].FeedComment.length; k++) {
                                            $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + latestNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                            if (k == 0) {
                                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li';
                                            }
                                            else {
                                                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li style = "display:none;"';
                                            }
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + ' id="feedcomment1_' + latestNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newsFeed">';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt">';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + latestNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + latestNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + latestNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + latestNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></div></li>';
                                        }



                                    }
                                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</ul></li>';
                                    $('#' + $scope.temptaskDiv).append(fundingreqfeeddivHtml);


                                    var duplicateChk = {};
                                    $('#' + $scope.temptaskDiv + '[data-id]').each(function () {
                                        if (duplicateChk.hasOwnProperty(this.id)) {
                                            $(this).remove();
                                        } else {
                                            duplicateChk[this.id] = 'true';
                                        }
                                    });
                                }
                            }
                            else
                                PagenoforScroll -= 2;
                            commentclick($scope.temptaskDiv);
                        });
                    }
                });
            }
            catch (e)
            { }
        }


        $('#Notifyfeeddivapprovaltaskforedit').on('click', 'a[data-DynHTML="notifytaskCommentshowHTML"]', function (event) {
            var feedid1 = event.target.attributes["id"].nodeValue;
            var feedid = feedid1.substring(11);
            var feedfilter = $.grep($scope.NotifyFund_newsfeed_TEMP[0], function (e) { return e.FeedId == parseInt(feedid); });
            $("#Notifyfeed_" + feedid).next('div').hide();
            if (feedfilter != '') {
                for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
                    //$('#feedcomment1_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    if ($('#feedcomment1_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
                        $(this)[0].innerHTML = "View " + (feedfilter[0].FeedComment.length - 1) + " more comment(s)";
                        if (i != 0)
                            $('#feedcomment1_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function () { });
                        else
                            $('#feedcomment1_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    }
                    else {
                        $('#feedcomment1_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                        $(this)[0].innerHTML = "Hide comment(s)";
                    }
                }

            }

        });

        $('#Notifyfeeddivforreviewtaskforedit').on('click', 'a[data-DynHTML="notifytaskCommentshowHTML"]', function (event) {
            var feedid1 = event.target.attributes["id"].nodeValue;
            var feedid = feedid1.substring(11);
            var feedfilter = $.grep($scope.NotifyFund_newsfeed_TEMP[0], function (e) { return e.FeedId == parseInt(feedid); });
            $("#Notifyfeed_" + feedid).next('div').hide();
            if (feedfilter != '') {
                for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
                    //$('#feedcomment1_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    if ($('#feedcomment1_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
                        $(this)[0].innerHTML = "View " + (feedfilter[0].FeedComment.length - 1) + " more comment(s)";
                        if (i != 0)
                            $('#feedcomment1_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function () { });
                        else
                            $('#feedcomment1_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    }
                    else {
                        $('#feedcomment1_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                        $(this)[0].innerHTML = "Hide comment(s)";
                    }
                }
            }
        });


        $('#Notifyfeeddivworktaskforedit').on('click', 'a[data-DynHTML="notifytaskCommentshowHTML"]', function (event) {
            var feedid1 = event.target.attributes["id"].nodeValue;
            var feedid = feedid1.substring(11);
            var feedfilter = $.grep($scope.NotifyFund_newsfeed_TEMP[0], function (e) { return e.FeedId == parseInt(feedid); });
            $("#Notifyfeed_" + feedid).next('div').hide();
            if (feedfilter != '') {
                for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
                    //$('#feedcomment1_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    if ($('#feedcomment1_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
                        $(this)[0].innerHTML = "View " + (feedfilter[0].FeedComment.length - 1) + " more comment(s)";
                        if (i != 0)
                            $('#feedcomment1_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function () { });
                        else
                            $('#feedcomment1_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    }
                    else {
                        $('#feedcomment1_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                        $(this)[0].innerHTML = "Hide comment(s)";
                    }
                }
            }
        });


        $scope.AddApprovalTaskNewsFeedforedit = function (divid, commentbuttinid) {
            var temp = '';
            if (fundrequestID != 0) {
                if (commentbuttinid == 'feedcommentworkforedit')
                    temp = 'feedtextholderworkforedit';
                else if (commentbuttinid == 'feedcommentapprovaltaskforedit')
                    temp = 'feedtextholderapprovaltaskforedit';
                else if (commentbuttinid == 'divforreviewtaskcommentareaforedit')
                    temp = 'textholderreviewtaskforedit';
                else
                    temp = 'notifyfeedtextholderunassigned';

                if ($('#' + temp).text() != "") {
                    return false;
                }
                $scope.IsnewcommentAvailable += 1;
                //var AddNewsFeed = $resource('common/Feed/');
                //var addnewsfeed = new AddNewsFeed();
                var addnewsfeed = {};
                addnewsfeed.Actor = parseInt($cookies['UserId'], 10);
                addnewsfeed.TemplateID = 40;
                addnewsfeed.EntityID = parseInt(fundrequestID);
                addnewsfeed.TypeName = "Tasks";
                addnewsfeed.AttributeName = "";
                addnewsfeed.FromValue = "";
                addnewsfeed.ToValue = $('#' + commentbuttinid).text();
                // var savenewsfeed = AddNewsFeed.save(addnewsfeed, function () {
                CommonService.Feed(addnewsfeed).then(function (savenewsfeed) {

                });
                $('#' + commentbuttinid).empty();
                if (document.getElementById(commentbuttinid).innerHTML.replace(/(<([^>]+)>)/ig, '').replace(/ /g, '').length == 0) {

                    document.getElementById(commentbuttinid).innerHTML = '<span id=\'' + temp + '\' class=\'placeholder\'>Write a comment...</span>';
                }
            }
            $timeout(function () { feedforApprovalTask($scope.TaskIDnotify, $scope.notifyDivid, true); }, 2000);
        };

        function commentclick(divtypeid) {
            $('#' + divtypeid).on('click', 'a[data-DynHTML="CommentHTML1_' + divtypeid + '"]', function (event) {
                //////var commentuniqueid = this.id;
                //////event.stopImmediatePropagation();
                //////$(this).hide();
                //////var fundingreqfeeddivHtml = '';
                //////fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul class="feedcontent"><li class="postComment">';
                //////fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="postbox"><div class="avatar"><span class="user"><img data-role="user-avatar" src=Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '></span></div>';
                //////fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="textarea-wrapper" data-role="textarea"><div contenteditable="true" tabindex="0" class="textarea" id="feedcomment1_' + commentuniqueid + '"<br></div></div>';
                //////fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<button type="submit" class="btn btn-primary" data-dyncommentbtn="ButtonHTML_' + divtypeid + '" >Comment</button></div></li></ul>';
                //////$(this).parents('div.block').after(fundingreqfeeddivHtml);
                //////$timeout(function () { $('#feedcomment1_' + commentuniqueid).html('').focus(); }, 10);
                //////buttonclick(divtypeid);




                var commentuniqueid = this.id;
                event.stopImmediatePropagation();
                $(this).hide();
                var fundingreqfeeddivHtml = '';

                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li class="writeNewComment">';
                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="newComment"><div class="userAvatar"><img data-role="user-avatar" src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\'></div>'; //should always be current userid
                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="textarea-wrapper" data-role="textarea"><div contenteditable="true" tabindex="0" class="textarea" id="mytaskfeedcomment_' + commentuniqueid + '"></div></div>';
                fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<button type="submit" class="btn btn-primary"  data-dyncommentbtn="ButtonHTML_' + divtypeid + '" >Comment</button></div></li>';

                var currentobj = $(this).parents('li[data-parent="NewsParent"]').find('ul');
                if ($(this).parents('li[data-parent="NewsParent"]').find('ul li').children().length > 0)
                    $(this).parents('li[data-parent="NewsParent"]').find('ul li:first').before(fundingreqfeeddivHtml);
                else
                    $(this).parents('li[data-parent="NewsParent"]').find('ul').html(fundingreqfeeddivHtml);
                $timeout(function () { $('#mytaskfeedcomment_' + commentuniqueid).html('').focus(); }, 10);
                buttonclick(divtypeid);


            });

        }

        function buttonclick(divtypeid) {
            $('#' + divtypeid).on('click', 'button[data-dyncommentbtn="ButtonHTML_' + divtypeid + '"]', function (event) {
                event.stopImmediatePropagation();
                //////if ($(this).prev().eq(0).text().toString().trim().length > 0) {
                //////    var AddFeedComment = $resource('common/InsertFeedComment/');
                //////    var addfeedcomment = new AddFeedComment();
                //////    var myDate = new Date.create();
                //////    addfeedcomment.FeedID = $(this).parents("li:eq(1)").attr('data-id');
                //////    addfeedcomment.Actor = parseInt($cookies['UserId']);
                //////    addfeedcomment.Comment = $(this).prev().eq(0).text();
                //////    var d = new Date.create();
                //////    var month = d.getMonth() + 1;
                //////    var day = d.getDate();
                //////    var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                //////    var output = d.getFullYear() + '-' +
                //////        (month < 10 ? '0' : '') + month + '-' +
                //////        (day < 10 ? '0' : '') + day + ' ' + hrs;
                //////    var _this = $(this);
                //////    var saveusercomment = AddFeedComment.save(addfeedcomment, function () {
                //////        if (addfeedcomment.Comment.length >= 1) {
                //////            var fundingreqfeeddivHtml = '';
                //////            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<ul class="feedcontent"><li>';
                //////            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="avatar"><img src= Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.NewTime + ' alt="Avatar"></span>';
                //////            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="block"><div class="arrow-left"></div>';
                //////            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="header"> <a href="javascript:void(0)">' + $cookies['Username'] + '</a>';
                //////            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</span>';
                //////            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="body">' + saveusercomment.Response + '</span>';
                //////            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="footer"><span class="time">Few seconds ago</span></span>';
                //////            fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></li></ul>';
                //////            $(_this).parents("li:eq(1)").append(fundingreqfeeddivHtml);
                //////            $('#NotifyTaskComment_' + addfeedcomment.FeedID).show();
                //////            $(_this).closest('ul').empty();
                //////        }
                //////        else { }
                //////    });
                //////}


                if ($(this).prev().eq(0).text().toString().trim().length > 0) {
                    //var AddFeedComment = $resource('common/InsertFeedComment/');
                    //var addfeedcomment = new AddFeedComment();
                    var addfeedcomment = {};
                    var FeedidforComment = $(this).parents("li:eq(1)").attr('data-id');
                    var myDate = new Date.create();
                    addfeedcomment.FeedID = $(this).parents("li:eq(1)").attr('data-id');
                    addfeedcomment.Actor = parseInt($cookies['UserId']);
                    addfeedcomment.Comment = $(this).prev().eq(0).text();
                    var d = new Date.create();
                    var month = d.getMonth() + 1;
                    var day = d.getDate();
                    var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                    var output = d.getFullYear() + '-' +
                        (month < 10 ? '0' : '') + month + '-' +
                        (day < 10 ? '0' : '') + day + ' ' + hrs;
                    var _this = $(this);

                    // var saveusercomment = AddFeedComment.save(addfeedcomment, function () {
                    CommonService.InsertFeedComment(addfeedcomment).then(function (saveusercomment) {
                        var fundingreqfeeddivHtml = '';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class=\"newsFeed\">';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="userAvatar"><img src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\' alt="Avatar"></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmnt">';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntHeader"><h5> <a href="undefined">' + $cookies['Username'] + '</a></h5></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntContent"><p>' + saveusercomment.Response + '</p></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="cmntFooter"><span class="cmntTime">Few seconds ago</span></div>';
                        fundingreqfeeddivHtml = fundingreqfeeddivHtml + '</div></div></div></li>';
                        $('#' + divtypeid).find('li[data-id=' + FeedidforComment + ']').first().find('li:first').before(fundingreqfeeddivHtml)
                        $(".writeNewComment").remove();;
                        $('#NotifyTaskComment_' + addfeedcomment.FeedID).show();
                    });
                }



            });
        }

        $scope.AddApprovalTaskNewsFeed = function () {
            if (fundrequestID != 0) {
                $scope.IsnewcommentAvailable += 1;
                //var AddNewsFeed = $resource('common/Feed/');
                //var addnewsfeed = new AddNewsFeed();
                var addnewsfeed = {};
                addnewsfeed.Actor = parseInt($cookies['UserId'], 10);
                addnewsfeed.TemplateID = 2;
                addnewsfeed.EntityID = parseInt(fundrequestID);
                addnewsfeed.TypeName = "Tasks";
                addnewsfeed.AttributeName = "";
                addnewsfeed.FromValue = "";
                addnewsfeed.ToValue = $('#NotifyFinancialfundingreqfeedcomment1').text();

                // var savenewsfeed = AddNewsFeed.save(addnewsfeed, function () {
                CommonService.Feed(addnewsfeed).then(function (savenewsfeed) {
                    var fundingreqfeeddivHtml = '';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<li data-ID=' + savenewsfeed.Response + '>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="avatar"> <img src= Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId'], 10) + '&time=' + $scope.NewTime + ' alt="Avatar"></span>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<div class="block" data-parent="parentforcomment"><div class="arrow-left"></div>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="header"><a href=mailto:' + $cookies['UserEmail'] + '>' + $cookies['Username'] + '</a></span>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="body">Added new comments <b>' + addnewsfeed.ToValue + '</b></span>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<span class="footer"><span class="time">Few seconds ago</span>';
                    fundingreqfeeddivHtml = fundingreqfeeddivHtml + '<button class="btn btn-small pull-right" data-DynHTML="CommentHTML1_' + $scope.temptaskDiv + '">Comment</button></span></div></li>';
                    $('#Notifyfinicialfundingrequestfeeddiv1').prepend(fundingreqfeeddivHtml);
                });
                $('#NotifyFinancialfundingreqfeedcomment1').empty();
            }
        };

        function validateURL(textval) {
            var urlregex = new RegExp(
                  "^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
            return urlregex.test(textval);
        }

        $scope.DecodedTaskNameNotify = function (TaskName) {
            if (TaskName != null & TaskName != "") {
                return $('<div />').html(TaskName).text();
            }
            else {
                return "";
            }
        };

        $scope.AddLink = function () {
            if ($scope.txtLinkName != "" && $scope.txtLinkURL != "") {
                if ($scope.TaskBriefDetails.taskID > 0) {
                    //var AttachmentLink = $resource('task/InsertLink');
                    //var AttachLink = new AttachmentLink();
                    var AttachLink = {};
                    AttachLink.EntityID = $scope.TaskBriefDetails.taskID;
                    AttachLink.Name = $scope.txtLinkName;
                    AttachLink.URL = $scope.txtLinkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '');
                    AttachLink.LinkType = $scope.linkType;
                    AttachLink.Description = "";
                    AttachLink.ActiveVersionNo = 1;
                    AttachLink.TypeID = 9;
                    AttachLink.CreatedOn = Date.now();
                    AttachLink.OwnerID = $cookies['UserId'];
                    AttachLink.OwnerName = $cookies['Username'];
                    AttachLink.ModuleID = 1;

                    //var AttachResponse = AttachmentLink.save(AttachLink, function () {
                    TaskService.InsertLink(AttachLink).then(function (AttachResponse) {
                        if (AttachResponse.Response != 0) {
                            NotifySuccess($translate.instant('LanguageContents.Res_4788.Caption'));
                            $scope.txtLinkName = "";
                            $scope.linkType = 1;
                            $scope.txtLinkURL = "";
                            $scope.txtLinkDesc = "";
                            $scope.EditLink = false;
                            $scope.SaveLink = true;
                            $scope.HidecfnTitle = "Add link";
                            $("#NotifyaddLinkPopup").modal("hide");

                            ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
                        }
                    });

                }
                else {
                    var linkName = ($scope.txtLinkName != undefined || $scope.txtLinkName != null) ? $scope.txtLinkName : "";
                    var url = ($scope.txtLinkURL != undefined || $scope.txtLinkURL != null) ? $scope.txtLinkURL.replace('http://', '').replace('https://', '').replace('file://', '').replace('ftp://', '') : "";
                    var linkdesc = ($scope.txtLinkDesc != undefined || $scope.txtLinkDesc != null) ? $scope.txtLinkDesc : "";
                    $scope.AttachmentFilename.push({ "FileName": $scope.txtLinkName, "Extension": "link", "Size": 0, "FileGuid": 0, "FileDescription": linkdesc, "URL": url, "LinkType": $scope.linkType, "ID": 0, "LinkID": ($scope.UniqueLinkID + 1) });
                    $scope.txtLinkName = "";
                    $scope.txtLinkURL = "";
                    $scope.linkType = 1;
                    $scope.txtLinkDesc = "";
                    $scope.EditLink = false;
                    $scope.SaveLink = true;
                    $scope.HidecfnTitle = "Add link";
                    $("#NotifyaddLinkPopup").modal("hide");
                }
            }
            else {

                bootbox.alert($translate.instant('LanguageContents.Res_4602.Caption'));
            }
        }


        $scope.TaskBriefDetailDateFormat = function (dateObj) {

            return dateFormat(dateObj, $scope.DefaultSettings.DateFormat);
        }

        function GetEntityLocationPath(EntityIDVal) {
            //var GetPathforbreadcrum = $resource('metadata/GetPath/:EntityID', { EntityID: EntityIDVal });
            //var getbc = GetPathforbreadcrum.get(function () {
            MetadataService.GetPath(EntityIDVal).then(function (getbc) {
                $scope.array = [];
                if (getbc != null && getbc != undefined) {
                    for (var i = 0; i < getbc.Response.length - 1; i++) {
                        $scope.array.push({ "ID": getbc.Response[i].ID, "Name": getbc.Response[i].Name, "UniqueKey": getbc.Response[i].UniqueKey, "ColorCode": getbc.Response[i].ColorCode, "ShortDescription": getbc.Response[i].ShortDescription, "TypeID": getbc.Response[i].TypeID, "mystyle": "0088CC" });
                    }
                }
            });
        }

        function GetentityMembers(entityID) {
            $scope.EntityMemberList = [];
            //Get entity members
            //var GetEntityMember = $resource('planning/Member/:EntityID', { EntityID: entityID }, { get: { method: 'GET' } });
            //var EntityMemberList = GetEntityMember.get({ EntityID: entityID }, function () {
            PlanningService.GetMember(entityID).then(function (EntityMemberList) {
                $scope.EntityMemberList = EntityMemberList.Response;
                $scope.EntityDistinctMembers = [];
                var dupes = {};
                $.each($scope.EntityMemberList, function (i, el) {
                    if (el.IsInherited != true) {
                        if (!dupes[el.Userid]) {
                            dupes[el.Userid] = true;
                            $scope.EntityDistinctMembers.push(el);
                        }
                    }
                });

            });
        }

        $scope.BreadCrumOverview = function (ID) {
            $("#NotificationtaskAction").hide();
            $('#TaskEditWorkTaskPopup').hide();
            $('#TaskEditApprovalTaskPopup').hide();
            $('#TaskEditReviewTaskPopup').hide();
            $('#NotifyUnassignedTask').hide();
            $("#loadNotificationtask").trigger("ontaskActionnotification");
        }

        //File details mouse over data
        $scope.callcontent = function (file) {

            var msg = "";
            if (file.Extension != "Link") {
                msg += "<b>Description:</b> " + (file.Description != null ? file.Description.toString() : "-") + "<br/><br/>";
                msg += "<b>Uploaded by:</b> " + (file.OwnerName != null ? file.OwnerName.toString() : "-") + "<br/><br/>";
                msg += "<b>File size: </b> " + (file.Size != null ? parseSize(file.Size) : "-") + "<br/><br/>";
                msg += "<b>Type:</b> " + (file.Extension != null ? file.Extension.toString() : "-") + "<br/><br/>";
                return msg;
            }
            else {
                msg += "<b>Url:</b> " + (file.LinkURL != null ? file.LinkURL.toString() : "-") + "<br/><br/>";
                return msg;
            }
        };

        $scope.ContextFileExtention = "";
        $scope.loadcontextmenu = function (e, fileid) {
            $scope.contextFileID = $(e.target).attr('data-fileid');
            var fileid = fileid;
            var linkextn = $(e.target).attr('data-extension');
            $scope.ContextFileExtention = linkextn;
            if (linkextn == "Link") {
                $scope.showmanageverson = false;
                $scope.shownewversion = false;
            }
            else {
                $scope.showmanageverson = true;
                $scope.shownewversion = true;
            }
        }

        $scope.OwnerName = $cookies['Username'];
        $scope.OwnerID = parseInt($cookies['UserId'], 10);
        $scope.ownerEmail = $cookies['UserEmail'];


        $scope.name = "";
        $scope.DescriptionChange = "";
        $scope.dueDate = new Date.create();
        $scope.dueDate = null;
        $scope.EntityTaskChangesHolder = { "TaskName": "", "TaskDescription": "", "DueDate": "", "Note": "" };

        $scope.saveEntityTaskDetails = function (type) {
            if (type == "nam") {
                if ($scope.name != "") {
                    if ($scope.TaskBriefDetails.taskName != $scope.name) {
                        $scope.EntityTaskChangesHolder.TaskName = $scope.name;
                        $scope.updatetaskEdit = true;
                    }
                    else
                        return false;

                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1876.Caption'));
                    return false;
                }
            }
            else if (type == "des") {
                if ($scope.TaskBriefDetails.Description != $scope.DescriptionChange) {
                    $scope.EntityTaskChangesHolder.TaskDescription = $scope.DescriptionChange;
                    $scope.updatetaskEdit = true;
                }
                else
                    return false;

            }
            else if (type == "note") {

                if ($scope.TaskBriefDetails.Note != $scope.Notechange) {
                    $scope.EntityTaskChangesHolder.Note = $scope.Notechange;
                    $scope.updatetaskEdit = true;
                }
                else
                    return false;

            }
            else if (type == "due") {

                if ($scope.dueDate != "" && $scope.dueDate != undefined && $scope.dueDate != null) {
                    $scope.EntityTaskChangesHolder.DueDate = dateFormat($scope.dueDate, $scope.DefaultSettings.DateFormat);
                    var tempduedate1 = formatteddateFormat($scope.TaskBriefDetails.dueDate, $scope.DefaultSettings.DateFormat);
                    if (ConvertDateToString(tempduedate1) == ConvertDateToString($scope.dueDate, $scope.DefaultSettings.DateFormat)) {
                        $scope.updatetaskEdit = false;
                    } else { $scope.updatetaskEdit = true; }
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1878.Caption'));
                    return false;
                }
            }
            else if (type == "unassignedue") {
                if ($scope.dueDate != "" && $scope.dueDate != undefined && $scope.dueDate != null) {
                    $scope.EntityTaskChangesHolder.DueDate = dateFormat($scope.dueDate, $scope.DefaultSettings.DateFormat);
                    var tempduedate1 = formatteddateFormat($scope.TaskBriefDetails.dueDate, $scope.DefaultSettings.DateFormat);
                    if (ConvertDateToString(tempduedate1) == ConvertDateToString($scope.dueDate, $scope.DefaultSettings.DateFormat)) {
                        $scope.updatetaskEdit = false;
                    } else { $scope.updatetaskEdit = true; }
                } else {
                    $scope.EntityTaskChangesHolder.DueDate = '';
                }
            }
            //var TaskStatusserv = $resource('task/UpdatetaskEntityTaskDetails');
            //var TaskStatusData = new TaskStatusserv();
            var TaskStatusData = {};
            TaskStatusData.TaskID = $scope.TaskBriefDetails.taskID;
            TaskStatusData.TaskName = $scope.EntityTaskChangesHolder.TaskName;
            TaskStatusData.Description = $scope.EntityTaskChangesHolder.TaskDescription;
            TaskStatusData.Note = $scope.EntityTaskChangesHolder.Note;
            TaskStatusData.DueDate = $scope.EntityTaskChangesHolder.DueDate.toString();
            TaskStatusData.actionfor = type;
            // var TaskStatusResult = TaskStatusserv.save(TaskStatusData, function () {
            TaskService.UpdatetaskEntityTaskDetails(TaskStatusData).then(function (TaskStatusResult) {
                if (TaskStatusResult.StatusCode == 405) {
                    $scope.fired = false;
                    if ($scope.updatetaskEdit != false) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    }
                }
                else {
                    if (TaskStatusResult.Response == false) {
                        $scope.fired = false;
                        if ($scope.updatetaskEdit != false) {
                            NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        }
                    }
                    else {
                        if (type == "unassignedue") {
                            if ($scope.EntityTaskChangesHolder.DueDate != '') {
                                $scope.TaskBriefDetails.dueIn = dateDiffBetweenDates($scope.dueDate);
                                $scope.dueDate = dateFormat($scope.EntityTaskChangesHolder.DueDate, $scope.DefaultSettings.DateFormat);
                                $scope.TaskBriefDetails.dueDate = dateFormat($scope.EntityTaskChangesHolder.DueDate, $scope.DefaultSettings.DateFormat);
                            } else {
                                $scope.dueDate = '-';
                                $scope.TaskBriefDetails.dueDate = '-';
                                $scope.TaskBriefDetails.dueIn = 0;
                            }
                        }

                        RefreshCurrentTask();
                        $scope.EntityTaskChangesHolder = { "TaskName": "", "TaskDescription": "", "DueDate": "", "Note": "" };
                        $scope.TaskBriefDetails.taskName = $scope.name;
                        $scope.TaskBriefDetails.Description = $scope.DescriptionChange;
                        $scope.TaskBriefDetails.Note = $scope.Notechange;
                        $scope.fired = false;
                        $timeout(function () { feedforApprovalTask($scope.TaskIDnotify, $scope.notifyDivid, true); }, 2000);
                        if ($scope.updatetaskEdit != false) {
                            NotifySuccess($translate.instant('LanguageContents.Res_4785.Caption'));
                        }
                    }
                }
            });

            $timeout(function () { feedforApprovalTask($scope.TaskIDnotify, $scope.notifyDivid, true); }, 2000);
        }

        function dateDiffBetweenDates(dateTo) {
            var _MS_PER_DAY = 1000 * 60 * 60 * 24;
            var dateToday = new Date.create();
            var utcDateSet = Date.UTC(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate());
            var utcToday = Date.UTC(dateToday.getFullYear(), dateToday.getMonth(), dateToday.getDate());
            return Math.floor((utcDateSet - utcToday) / _MS_PER_DAY);
        }


        $scope.OrganizeTaskMenuOption = function (taskStatus) {
            if (taskStatus == 0) {
                $scope.taskMenuOptionText = 'Set to "Not Applicable"';
                var phase = this.$root.$$phase;
                if (phase == '$apply' || phase == '$digest') {
                } else {
                    this.$apply();
                }
                $('#context2 li#TM3').show();
            }
            else if (taskStatus == 7) {
                $scope.taskMenuOptionText = 'Set to "Unassigned"';
                var phase = this.$root.$$phase;
                if (phase == '$apply' || phase == '$digest') {

                } else {
                    this.$apply();
                }
                $('#context2 li#TM3').show();
            }
            else {
                $('#context2 li#TM3').hide();
            }
        }


        $scope.ShowHideSettoComplete = function (taskTypeId, taskStatus) {
            if (taskTypeId == 3 || taskTypeId == 31 || taskTypeId == 32 || taskTypeId == 36) {
                if (taskStatus == 0)
                    $("#TM1").show();
                else
                    $("#TM1").hide();
            }
            else {
                $("#TM1").show();
            }
        }

        $scope.SetContextTaskID = function (id) {
            $scope.contextTaskID = id;
        };


        $scope.SetContextTaskListID = function (id) {
            $scope.ContextTaskListID = id;
        };

        $scope.contextFileID = 0;
        $scope.SetContextFileID = function (fileid) {
            $scope.contextFileID = fileid;
        };

        $scope.SetContextTaskListEntityID = function (id) {
            if (id != 0)
                $scope.ContextTaskEntityID = id;
            else
                $scope.ContextTaskEntityID = $routeParams.ID;
        };



        //Getting Task Types from Enum
        $scope.TaskTypeList = [];
        //var GetTaskTypeListServ = $resource('task/GetTaskTypes', { get: { method: 'GET' } });
        //var TaskTypeData = GetTaskTypeListServ.get(function () {
        TaskService.GetTaskTypes().then(function (TaskTypeData) {
            $scope.TaskTypeList = TaskTypeData.Response;

        });

        $scope.taskUnassignTypeID = 0;
        function LoadUnassignedTaskDetl(taskTypeId, TaskObject) {
            $scope.taskUnassignTypeID = taskTypeId;
            $scope.TaskTypeName = "";
            var TaskTypeObj = $.grep($scope.TaskTypeList, function (n, i) {
                return $scope.TaskTypeList[i].TaskType == taskTypeId;
            });
            if (TaskTypeObj != null && TaskTypeObj.length > 0) {
                $scope.TaskTypeName = TaskTypeObj[0].Name;
            }

            $scope.ShowCompleteBtn = false;
            $scope.ShowApproveBtn = false;
            $scope.ShowRejectedBtn = false;
            $scope.ShowWithdrawBtn = false;
            $scope.ShowUnabletoCompleteBtn = false;
            $scope.ShowRevokeButton = false;
            $("#NotifyApprovalMyTaskPopup,#NotifyReviewMyTaskPopup,#NotifyWorkMyTaskPopup,#NotifyUnassignedTask").removeClass(cssToRemove);
            if (TaskObject != null) {
                $("#NotifyApprovalMyTaskPopup,#NotifyReviewMyTaskPopup,#NotifyWorkMyTaskPopup,#NotifyUnassignedTask").addClass("TaskPopup " + TaskObject.StatusName.replace(/\s+/g, ""));
            }
            $scope.listPredfWorkflowFilesAttch = TaskObject.taskAttachment;
            var isThisMemberPresent = 0;
            if (TaskObject.taskAssigness != null && TaskObject.taskAssigness.length > 0) {
                isThisMemberPresent = $.grep(TaskObject.taskAssigness, function (e) { return (e.UserID == parseInt($cookies['UserId']) && e.RoleID != 1); });
            }
            var taskOwnerObj = null;
            if (TaskObject.taskMembers != null && TaskObject.taskMembers.length > 0) {
                taskOwnerObj = $.grep(TaskObject.taskMembers, function (e) { return (e.RoleID == 1); })[0];
            }

            if (taskOwnerObj != null && taskOwnerObj.length > 0) {
                if (taskTypeId == 2) {
                    if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                        if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                            $scope.ShowWithdrawBtn = false;
                        }
                    }
                    if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
                        if ($scope.IsLockNotif != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null) {
                                    $scope.ShowCompleteBtn = true;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }

                    }
                    if (TaskObject.TaskStatus == 4 && isThisMemberPresent.length >= 1) {
                        if ($scope.IsLockNotif != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null) {
                                    $scope.ShowCompleteBtn = true;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }

                    }
                    if (TaskObject.TaskStatus == 2 && isThisMemberPresent.length >= 1) {
                        if ($scope.IsLockNotif != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null) {
                                    $scope.ShowCompleteBtn = true;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }
                    }
                    $scope.processingsrcobj.processingid = TaskObject.Id;
                    $scope.processingsrcobj.processingplace = "task";
                    $timeout(function () {
                        $scope.NotifyAssetfileTemplateLoading = "unassigned";
                        $scope.$broadcast('ReloadAssetView');
                    }, 100);

                }
                else if (taskTypeId == 3 || taskTypeId == 32 || taskTypeId == 36) {

                    if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                        if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                            $scope.ShowWithdrawBtn = false;
                        }
                    }
                    if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
                        if ($scope.IsLockNotif != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null && taskTypeId != 36) {
                                    $scope.ShowApproveBtn = true;
                                    $scope.ShowRejectedBtn = true;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }
                    }

                    $scope.processingsrcobj.processingid = TaskObject.Id;
                    $scope.processingsrcobj.processingplace = "task";
                    $timeout(function () {
                        $scope.NotifyAssetfileTemplateLoading = "unassigned";
                        $scope.$broadcast('ReloadAssetView');
                    }, 100);

                }
                else if (taskTypeId == 31) {
                    if (TaskObject.TaskStatus == 1 && taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                        if (taskOwnerObj.UserID === parseInt($cookies['UserId'])) {
                            $scope.ShowWithdrawBtn = false;
                        }
                    }
                    if (TaskObject.TaskStatus == 1 && isThisMemberPresent.length >= 1) {
                        if ($scope.IsLockNotif != true) {
                            if (isThisMemberPresent.length == 1) {
                                if (isThisMemberPresent[0].ApprovalStatus == null) {
                                    $scope.ShowApproveBtn = false;
                                    $scope.ShowCompleteBtn = true;
                                    $scope.ShowRejectedBtn = false;
                                    $scope.ShowUnabletoCompleteBtn = true;
                                }
                            }
                        }
                    }

                    $scope.processingsrcobj.processingid = TaskObject.Id;
                    $scope.processingsrcobj.processingplace = "task";
                    $timeout(function () {
                        $scope.NotifyAssetfileTemplateLoading = "unassigned";
                        $scope.$broadcast('ReloadAssetView');
                    }, 100);
                }
            }
            $scope.TaskBriefDetails.taskListUniqueID = TaskObject.TaskListID;
            $scope.TaskBriefDetails.taskName = TaskObject.Name;
            $scope.TaskBriefDetails.taskTypeId = taskTypeId;
            $scope.TaskBriefDetails.taskTypeName = TaskObject.TaskTypeName;
            $scope.TaskBriefDetails.dueDate = '-';
            if (TaskObject.strDate != null && TaskObject.strDate.length > 0) {
                $scope.TaskBriefDetails.dueDate = dateFormat(TaskObject.strDate, $scope.DefaultSettings.DateFormat); //datstartval.toString("yyyy/MM/dd");
                $scope.TaskBriefDetails.strDueDate = ConvertStringToDate(TaskObject.strDate);
            }
            $scope.TaskBriefDetails.dueIn = TaskObject.totalDueDays;
            $scope.TaskBriefDetails.status = TaskObject.StatusName;
            $scope.TaskBriefDetails.statusID = TaskObject.TaskStatus;
            $scope.TaskBriefDetails.Description = '-';
            if (TaskObject.Description.length > 0) {
                $scope.TaskBriefDetails.Description = TaskObject.Description;
            }
            $scope.TaskBriefDetails.Note = '-';
            if (TaskObject.Note != null)
                if (TaskObject.Note.length > 0) {
                    $scope.TaskBriefDetails.Note = TaskObject.Note;
                }
            $scope.TaskBriefDetails.taskID = TaskObject.Id;
            $scope.TaskBriefDetails.EntityID = TaskObject.EntityID;
            $scope.TaskBriefDetails.taskmembersList = TaskObject.taskAssigness;
            $scope.TaskBriefDetails.totalTaskMembers = TaskObject.TotaltaskAssigness;
            var unresponsedMembers = null;
            if (TaskObject.taskAssigness != null && TaskObject.taskAssigness.length > 0) {
                unresponsedMembers = $.grep(TaskObject.taskAssigness, function (e) { return (e.ApprovalStatus != null); });
            }
            if ((TaskObject.TaskStatus == 2 || TaskObject.TaskStatus == 3 || TaskObject.TaskStatus == 8) && TaskObject.taskTypeId != 2) {
                $scope.TaskBriefDetails.taskProgressCount = "";
            }
            else {
                if (unresponsedMembers != null && unresponsedMembers.length > 0) {
                    $scope.TaskBriefDetails.taskProgressCount = "(" + unresponsedMembers.length.toString() + "/" + TaskObject.taskAssigness.length.toString() + ")";
                }
            }

            $scope.TaskMemberList = TaskObject.taskAssigness;
            $scope.TaskBriefDetails.taskAttachmentsList = TaskObject.taskAttachment;
            $scope.name = $scope.TaskBriefDetails.taskName;
            $scope.DescriptionChange = $scope.TaskBriefDetails.Description;
            $scope.dueDate = "-";
            if (TaskObject.strDate != null && TaskObject.strDate.length > 0) {
                $scope.dueDate = dateFormat(TaskObject.strDate, $scope.DefaultSettings.DateFormat);
                $scope.TaskBriefDetails.strDueDate = ConvertStringToDate(TaskObject.strDate);
            }
            if ($scope.taskUnassignTypeID == 2) {
                GetTaskCheckLists();
            }
            try { GetEntityAttributesDetails($scope.TaskBriefDetails.taskID); } catch (e) { }
            ReloadTaskAttachments(TaskObject.Id);
        }

        $scope.addUnAssignAdditionalMember = function () {
            //if ($scope.dueDate != "" && $scope.dueDate != undefined && $scope.dueDate != null && $scope.dueDate != "-") {
            SeperateUsers();
            $scope.globalEntityMemberLists = [];
            $scope.globalTempcount = 1;
            $scope.ddltasklobalrole = '';
            $scope.taskGlobaluser = '';
            $scope.AutoCompleteSelectedObj = [];
            $scope.AddUnAssign = true;
            $("#NotifyAddTaskMemberModalPopup").modal("show");
            //}
            //else {
            //    bootbox.alert($translate.instant('LanguageContents.Res_1879.Caption'));
            //}
        }

        $scope.AddUnAssignedTaskAdditionalMembers = function () {
            var ownerlist = null;
            if ($('#ngNotifyAddUnassignTaskMember').hasClass('disabled')) { return; }
            $('#ngNotifyAddUnassignTaskMember').addClass('disabled');
            if ($scope.TaskBriefDetails.ownerId == 0) {
                ownerlist = $scope.OwnerList;
            }

            var memberList = [];
            memberList = GetUserSelectedAll();
            if (memberList.length > 0 || $scope.globalEntityMemberLists.length > 0) {
                //var InsertTaskMemberServ = $resource('task/InsertUnAssignedTaskMembers/');
                //var insertMemberTask = new InsertTaskMemberServ();
                var insertMemberTask = {};
                insertMemberTask.ParentId = $routeParams.ID;
                insertMemberTask.TaskID = $scope.TaskBriefDetails.taskID;
                insertMemberTask.TaskMembers = memberList;
                insertMemberTask.ownerlist = ownerlist;
                insertMemberTask.GlobalTaskMembers = $scope.globalEntityMemberLists;
                // var SaveTaskMemberResult = InsertTaskMemberServ.save(insertMemberTask, function () {
                TaskService.InsertUnAssignedTaskMembers(insertMemberTask).then(function (SaveTaskMemberResult) {
                    if (SaveTaskMemberResult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4267.Caption'));
                        $('#ngNotifyAddUnassignTaskMember').removeClass('disabled');
                    }
                    else {
                        $('#ngNotifyAddUnassignTaskMember').removeClass('disabled');
                        $scope.globalEntityMemberLists = [];
                        var taskMemberObj = SaveTaskMemberResult.Response.m_Item2;
                        var currentTaskRound = taskMemberObj[0].ApprovalRount;
                        var taskAssignessObj = $.grep(taskMemberObj, function (e) { return e.RoleID > 1 && e.ApprovalRount == currentTaskRound; });
                        $scope.TaskBriefDetails.taskmembersList = taskAssignessObj;
                        if (taskMemberObj.length > 0) {
                            $scope.TaskBriefDetails.totalTaskMembers = taskMemberObj;
                        }
                        $scope.TaskMemberList = taskAssignessObj;
                        if ($scope.TaskBriefDetails.ownerId == 0) {
                            $scope.TaskBriefDetails.taskOwner = $scope.OwnerList[0].UserName;
                            $scope.TaskBriefDetails.ownerId = $scope.OwnerList[0].Userid;
                        }
                        SeperateUsers();
                        $('#NotifyAdditionalTaskMembers > tbody input:checked').each(function () {
                            $(this).next('i').removeClass('checked');
                        });
                        NotifySuccess($translate.instant('LanguageContents.Res_4108.Caption'));
                        $('#NotifyAddTaskMemberModalPopup').modal('hide');
                        RefreshCurrentTask();
                    }
                });
            }
            else {
                $('#ngNotifyAddUnassignTaskMember').removeClass('disabled');
            }
        }


        function LoadUnassigneeMembers() {
            $scope.TaskMemberList = [];
            var UniqueTaskMembers = [];
            var dupes = {};
            if ($scope.TaskMemberList != null && $scope.TaskMemberList.length > 0) {
                $.each($scope.TaskMemberList, function (i, el) {

                    if (!dupes[el.UserID]) {
                        dupes[el.UserID] = true;
                        UniqueTaskMembers.push(el);
                    }
                });
            }
            var UniqueEntityMembers = [];
            dupes = {};
            if ($scope.EntityMemberList != null && $scope.EntityMemberList.length > 0) {
                $.each($scope.EntityMemberList, function (i, el) {
                    if (el.IsInherited != true) {
                        if (!dupes[el.Userid]) {
                            dupes[el.Userid] = true;
                            UniqueEntityMembers.push(el);
                        }
                    }
                });
            }
            $scope.RemainMembers = [];
            if (UniqueEntityMembers != null && UniqueEntityMembers.length > 0) {
                $.each(UniqueEntityMembers, function (key, value) {
                    var MemberList = $.grep(UniqueTaskMembers, function (e) { return e.UserID == value.Userid; });
                    if (MemberList.length == 0) {

                        $scope.RemainMembers.push(value);

                    }
                });
            }
        }

        $scope.ClosePopUp = function () {
            if (NewsFeedUniqueTimerForTask != undefined)
                $timeout.cancel(NewsFeedUniqueTimerForTask);
            if ($scope.NotifyAssetfileTemplateLoading != null) {
                $scope.SetCurrentTabid("Notifytask-MetaData-" + $scope.NotifyAssetfileTemplateLoading, "Notifytask-Attachments-" + $scope.NotifyAssetfileTemplateLoading)
                $("#Notifytask-Attachments-" + $scope.NotifyAssetfileTemplateLoading + "dam").empty();
                $("#Notifytask-Attachments-" + $scope.NotifyAssetfileTemplateLoading + "dam").unbind();
                $scope.NotifyAssetfileTemplateLoading = "Null";
            }
        };

        /*
           Global memebers from the system to add in tasks logic goes here
           Author: Viniston
      */

        $scope.Roles = [];                          // Global member role scope
        $scope.globalEntityMemberLists = [];        // to hold the global entity task members from the system
        $scope.globalTempcount = 1;                 // temporary id count for global members
        $scope.AutoCompleteSelectedObj = [];        // auto complete member data holder
        // to load the entity roles for adding global members from the system
        function LoadMemberRoles() {
            //var GetRole = $resource('access/Role/', { get: { method: 'GET' } });
            //var role = GetRole.get(function () {
            AccessService.Role().then(function (role) {
                $scope.Roles = role.Response;
            });
        }

        $scope.addUsers = function () {                 // adding global members into the scope  logic goes here
            var result = $.grep($scope.globalEntityMemberLists, function (e) { return e.Userid == parseInt($scope.AutoCompleteSelectedObj[0].Id, 10); });
            var entitymemberresult = $.grep($scope.EntityMemberList, function (e) { return e.Userid == parseInt($scope.AutoCompleteSelectedObj[0].Id, 10); });
            if (result.length == 0 && entitymemberresult.length == 0) {
                var membervalues = $.grep($scope.Roles, function (e) { return e.Id == $scope.ddltasklobalrole; })[0];
                $scope.globalEntityMemberLists.push({ "TID": $scope.globalTempcount, "UserEmail": $scope.AutoCompleteSelectedObj[0].Email, "DepartmentName": $scope.AutoCompleteSelectedObj[0].Designation, "Title": $scope.AutoCompleteSelectedObj[0].Title, "Roleid": $scope.ddltasklobalrole, "RoleName": membervalues.Caption, "Userid": parseInt($scope.AutoCompleteSelectedObj[0].Id, 10), "UserName": $scope.AutoCompleteSelectedObj[0].FirstName + ' ' + $scope.AutoCompleteSelectedObj[0].LastName, "IsInherited": '0', "InheritedFromEntityid": '0', "FromGlobal": 0, "CostCentreID": 0 });
                $scope.globalTempcount = $scope.globalTempcount + 1;
                $scope.ddltasklobalrole = '';
                $scope.taskGlobaluser = '';
                $scope.AutoCompleteSelectedObj = [];
            }
            else {
                $scope.ddltasklobalrole = '';
                $scope.taskGlobaluser = '';
                $scope.AutoCompleteSelectedObj = [];
                bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
            }
        };

        $scope.deleteGlobalTaskMembers = function (item) {  // deleting global members from the scope  logic goes here
            bootbox.confirm($translate.instant('LanguageContents.Res_2025.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        $scope.globalEntityMemberLists.splice($.inArray(item, $scope.globalEntityMemberLists), 1);
                    }, 100);
                }
            });
        };




        //task dynamic metadata handling works goes here-->START
        $scope.fields = {
            usersID: ''
        };
        $scope.Dropdown = {};
        $scope.ShowHideAttributeOnRelation = {};
        $scope.EnableDisableControlsHolder = {};
        $scope.fromAssignedpopup = true;
        $scope.treePreviewObj = {};
        $scope.treeNodeSelectedHolder = [];
        $scope.treesrcdirec = {};
        $scope.staticTreesrcdirec = {};
        $scope.TreeEmptyAttributeObj = {};
        $scope.atributesRelationList = {};
        $scope.optionsLists = [];
        $scope.EnableDisableControlsHolder = {};
        $scope.Inherritingtreelevels = {};
        $scope.InheritingLevelsitems = [];
        $scope.treeTexts = {};
        $scope.treeSources = {};
        $scope.treeSourcesObj = [];
        $scope.UploadAttributeData = [];
        $scope.OptionObj = {

        };
        $scope.PercentageVisibleSettings = {};
        $scope.DropDownTreePricing = {};
        $scope.listAttributeValidationResult = [];
        $scope.listAttriToAttriResult = [];
        $scope.items = [];
        $scope.treelevels = {};
        $scope.DropDownTreeOptionValues = {};
        $scope.NormalDropdownCaption = {};
        $scope.uploader = {};
        $scope.UploaderCaption = {};
        $scope.NormalMultiDropdownCaption = {};
        $scope.treeSelection = [];
        $scope.normaltreeSources = {};

        //Tree multiselection part STARTED

        $scope.treeNodeSelectedHolder = new Array();

        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            var _ref;
            $scope.output = "You selected: " + branch.Caption;


            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }

            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
                for (var i = 0, parent; parent = parentArr[i++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == parent.AttributeId && e.id == parent.id; });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(parent);
                    }
                }
            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                    if (branch.Children.length > 0) {
                        RemoveRecursiveChildTreenode(branch.Children);
                    }
                }
            }

            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                }
                else
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }


        };

        $scope.renderHtml = function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };

        var treeTextVisbileflag = false;
        function IsNotEmptyTree(treeObj) {

            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                }
                else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }


        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == child.AttributeId && e.id == child.id; });
                if (remainRecord.length > 0) {

                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }

            }
        }

        $scope.treesrcdirec = {};

        $scope.my_tree = tree = {};

        //Tree  multiselection part ended

        $scope.ClearModelObject = function (ModelObject) {
            for (var variable in ModelObject) {
                if (typeof ModelObject[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        ModelObject[variable] = "";
                    }
                }
                else if (typeof ModelObject[variable] === "number") {
                    ModelObject[variable] = null;
                }
                else if (Array.isArray(ModelObject[variable])) {
                    ModelObject[variable] = [];
                }
                else if (typeof ModelObject[variable] === "object") {
                    ModelObject[variable] = {};
                }
            }

        }




        //------------> RECURSIVE FUNCTION TO HIDE ALL THE ATTRIBUTE RELATIONS FOR THE SELECTED ATTRIBUTE <--------------
        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToShow = [];

            //----------> CHECK THE ATTRIBUTE ON ATTRIBUTE ID AND LEVEL AND GET THE ATTRIBUTE INFO TO HIDE <--------------
            if (attrLevel > 0) {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            }
            else {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }

            if (attributesToShow[0] != undefined) {
                for (var i = 0; i < attributesToShow[0].length; i++) {
                    var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                //----------> HIDE THE ATTRIBUTE AND CLEAR THE SCOPE <----------
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                //----------> FIND FOR NEXT ATTRIBUTE IF THE ATTRIBUTE RELATIONS EXISTS <-------------
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                //---------> IF THE ATTRIBUTE HAS RELATION WITH OTHER ATTRIBUTE START RECURSIVE AGAIN
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            }
                            else {
                                //----------> HIDE THE ATTRIBUTE AND CLEAR THE SCOPE <----------
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";

                                //----------> FIND FOR NEXT ATTRIBUTE IF THE ATTRIBUTE RELATIONS EXISTS <-------------
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));

                                //---------> IF THE ATTRIBUTE HAS RELATION WITH OTHER ATTRIBUTE START RECURSIVE AGAIN
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //--------------------> SHOW OR HIDE ATTRIBUTES FOR DROPDOWN ON SINGLE SELECTION <-----------------------
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToShow = [];
                var hideAttributeOtherThanSelected = [];
                //---------> 
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    //-----------> CLEAR THE SUB LEVEL ON SELECTING THE PARENT LEVEL
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        }
                        else if (attrType == 12) {
                            if (j == levelcnt)
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }

                    //-----------------> LOAD THE SUB LEVELS ON SELECTING PARENT LEVEL <----------------
                    if (attrType == 6) {
                        if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                    else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
                //----------------------------
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    }
                    catch (e) { }
                }
                //--------------> IF THERE IS NO ATTRIBUTE TO ATTRIBUTE RELATATIONS THEN RETURN BACK <--------------------------
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }

                //------------> RECURSIVE FUNCTION TO HIDE ATTRIBUTE ON RELATIONS
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);

                //-----> IF SINGLE SELECTION DROPDOWN SELECTED
                if (attrType == 3) {
                    if ($scope.fields['ListSingleSelection_' + attrID] != null) {
                        optionValue = parseInt(($scope.fields['ListSingleSelection_' + attrID]), 10);
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                }
                    //-----> IF MULTI SELECTION DROPDOWN SELECTED
                else if (attrType == 4) {
                    if ($scope.fields['ListMultiSelection_' + attrID] != null) {
                        optionValue = $scope.fields['ListMultiSelection_' + attrID];
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                    //-----> IF DROPDOWN TREE SELECTED
                else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                    //-----> IF TREE SELECTED
                else if (attrType == 7) {
                    if ($scope.fields['Tree_' + attrID] != null) {
                        optionValue = $scope.fields['Tree_' + attrID];
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }

                //----------> IF OPTION AVAILABLE FOR THE ATTRIBUTE SHOW THE ATTRIBUTE
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                //----------------> CHECK IF THE SELECTED ATTRIBUTE IS SINGLE SELECTION DROPDOWN OR DROPDOWN TREE <--------------------
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                }
                                else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (e) { }
        }

        //--------> HIDE ALL THE ATTRIBUTE RELAIONS AFTER LOADING <--------------------
        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                }
                                else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (e) { }
        }


        //--------------------> SHOW OR HIDE ATTRIBUTES FOR DROPDOWN ON PAGE LOAD <-----------------------
        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad = function (attrID, attributeLevel, attrVal, attrType) {
            try {
                var optionValue = attrVal;
                var attributesToShow = [];

                //-----> IF SINGLE SELECTION DROPDOWN SELECTED
                if (attrType == 3) {
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeOptionID == optionValue;
                    })[0]);
                }
                    //-----> IF MULTI SELECTION DROPDOWN SELECTED
                else if (attrType == 4) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                    //-----> IF TREE SELECTED
                else if (attrType == 7) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                    //-----> IF DROPDOWN TREE SELECTED
                else if (attrType == 6 || attrType == 12) {
                    if (attrVal != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrVal != null) ? parseInt(attrVal, 10) : 0) && e.AttributeLevel == ((attributeLevel != null) ? parseInt(attributeLevel, 10) : 0));
                        })[0]);
                    }
                }

                //----------> IF OPTION AVAILABLE FOR THE ATTRIBUTE SHOW THE ATTRIBUTE
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                //----------------> CHECK IF THE SELECTED ATTRIBUTE IS SINGLE SELECTION DROPDOWN OR DROPDOWN TREE <--------------------
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                }
                                else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (e) { }
        }

        //Validation part
        function GetValidationList(rootID) {
            //var ValidationAttribute = $resource('metadata/GetValidationDationByEntitytype/:EntityTypeID', { EntityTypeID: rootID });
            //var GetValidationresult = ValidationAttribute.get(function () {

            MetadataService.GetValidationDationByEntitytype(rootID).then(function (GetValidationresult) {
                if (GetValidationresult.Response != null) {
                    $scope.listValidationResult = GetValidationresult.Response;
                    if ($scope.listAttriToAttriResult != null) {
                        for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                            var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                            if (attrRelIDs != undefined) {
                                for (var j = 0; j < attrRelIDs.length; j++) {
                                    if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                        });
                                    }
                                    else {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                        });
                                    }
                                }
                            }
                        }
                        $("#TaskDynamicMetadata").nod($scope.listValidationResult, {
                            'delay': 200,
                            'submitBtnSelector': '#tskbtnTemp',
                            'silentSubmit': 'true'
                        });
                    }


                }
            });
        }

        var treeTextVisbileflag = false;
        function IsNotEmptyTree(treeObj) {

            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                }
                else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        function GetTreeCheckedNodes(treeobj, attrID) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    $scope.fields["Tree_" + attrID].push(node.id);
                }
                if (node.Children.length > 0)
                    GetTreeCheckedNodes(node.Children, attrID);
            }
        }

        //Tree multiselection part STARTED

        $scope.treeNodeSelectedHolder = [];

        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);

            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }

            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                }
                else
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }

            if ($scope.treeNodeSelectedHolder.length > 0) {
                for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
                    $scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
                }
            }

            //----------------> Calling Attribute to Attribute relation on selecting
            $scope.ShowHideAttributeToAttributeRelations(branch.AttributeId, 0, 0, 7);

        };

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == child.AttributeId && e.id == child.id; });
                if (remainRecord.length > 0) {

                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }

            }
        }
        $scope.treesrcdirec = {};

        $scope.my_tree = tree = {};

        //Tree  multiselection part ended
        $scope.LoadDropDownChildLevels = function (attrID, attributeLevel, levelcnt, attrType) {
            if (levelcnt > 0) {
                var currntlevel = attributeLevel + 1;

                //-----------> CLEAR THE SUB LEVEL ON SELECTING THE PARENT LEVEL
                for (var j = currntlevel; j <= levelcnt; j++) {
                    $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);

                    if (attrType == 6) {
                        $scope.fields["DropDown_" + attrID + "_" + j] = "";
                    }
                    else if (attrType == 12) {
                        if (j == levelcnt)
                            $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                        else
                            $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                    }
                }

                //-----------------> LOAD THE SUB LEVELS ON SELECTING PARENT LEVEL <----------------

                if (attrType == 6) {
                    if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                            $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        });
                    }
                }
                else if (attrType == 12) {
                    if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                            $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        });
                    }
                }
            }
        }


        function LoadTaskMetadata(EntAttrDet, ID, tasktype, fromassignedplace) {

            //----------------> LOADING DETAILS BLOCK STARTS HERE <---------------------
            $scope.StopeUpdateStatusonPageLoad = false;
            $scope.detailsLoader = false;
            $scope.detailsData = true;
            $scope.dyn_Cont = '';
            $scope.dyn_Cont = "";
            $scope.ClearModelObject($scope.fields);
            $scope.attributedata = EntAttrDet;
            for (var i = 0; i < $scope.attributedata.length; i++) {
                if ($scope.attributedata[i].TypeID == 6) {
                    $scope.dyn_Cont2 = '';
                    //construct inline scope objects

                    var CaptionObj = $scope.attributedata[i].Caption.split(",");

                    for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                        if (j == 0) {
                            if (CaptionObj[j] != undefined) {
                                $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                            }
                            else {
                                $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                            }
                        }
                        else {
                            if (CaptionObj[j] != undefined) {
                                $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                            }
                            else {
                                $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                            }
                        }
                    }
                    $scope.treelevels["dropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                    $scope.items = [];


                    for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                        var inlineEditabletitile = $scope.treelevels['dropdown_levels_' + $scope.attributedata[i].ID][j].caption;

                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
                        }
                        else {
                            if ($scope.IsLockNotif == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditableoptimizedtreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.DropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                            }
                            else if ($scope.IsLockNotif == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
                            }
                        }
                    }
                }
                else if ($scope.attributedata[i].TypeID == 12) {
                    $scope.dyn_Cont2 = '';
                    var CaptionObj = $scope.attributedata[i].Caption;
                    for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                        if ($scope.attributedata[i].Lable.length == 1) {
                            var k = j;
                            var treeTexts = [];
                            var fields = [];
                            $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                            if (k == CaptionObj.length) {
                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                            }
                            else {
                                if (CaptionObj[k] != undefined) {
                                    for (k; k < CaptionObj.length; k++) {
                                        treeTexts.push(CaptionObj[k]);
                                        fields.push(CaptionObj[k]);
                                    }
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                }
                                else {
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                }
                            }

                        }
                        else {
                            if (j == 0) {
                                if (CaptionObj[j] != undefined) {
                                    $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                }
                                else {
                                    $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                }
                            }
                            else {
                                var k = j;
                                if (j == ($scope.attributedata[i].Lable.length - 1)) {
                                    var treeTexts = [];
                                    var fields = [];
                                    $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                    if (k == CaptionObj.length) {
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    }
                                    else {
                                        if (CaptionObj[k] != undefined) {
                                            for (k; k < CaptionObj.length; k++) {
                                                treeTexts.push(CaptionObj[k]);
                                                fields.push(CaptionObj[k]);
                                            }
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                        }
                                        else {
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        }
                                    }
                                }
                                else {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    }
                                    else {
                                        $scope.items.push({ caption: $scope.attributedata[i].Lable[j].Label, level: j + 1 });
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    }
                                }
                            }
                        }
                    }
                    $scope.treelevels["multiselectdropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;

                    $scope.items = [];


                    for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;

                        var inlineEditabletitile = $scope.treelevels['multiselectdropdown_levels_' + $scope.attributedata[i].ID][j].caption;

                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
                        }
                        else {
                            if ($scope.IsLockNotif == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditablemultiselecttreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                            }
                            else if ($scope.IsLockNotif == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
                            }
                        }
                    }
                }
                else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {  // singleLine and Multiline TextBoxes
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;

                    $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                    if ($scope.attributedata[i].Caption != undefined) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                    }
                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                    }
                    else {
                        if ($scope.IsLockNotif == false) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext   href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"   data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                        }
                        else if ($scope.IsLockNotif == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        }
                    }
                }

                else if ($scope.attributedata[i].TypeID == 2) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";

                    if ($scope.attributedata[i].Caption != undefined) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                    }
                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                    }
                    else {
                        if ($scope.IsLockNotif == false) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\"  attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"MultiLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\" data-type="' + $scope.attributedata[i].ID + '" data-original-title=\"' + $scope.attributedata[i].Lable + '\">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                        }
                        else if ($scope.IsLockNotif == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        }
                    }
                }

                else if ($scope.attributedata[i].TypeID == 11) {  // inline uploader

                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    $scope.fields["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                    $scope.UploaderCaption["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                    $scope.setUploaderCaption();
                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group ng-scope\"><label class=\"control-label\"\>"' + $scope.attributedata[i].Lable.toString() + '"</label>';
                    $scope.dyn_Cont += '<div class=\"controls\">';
                    $scope.dyn_Cont += '<img src="UploadedImages/' + $scope.attributedata[i].Value + '" alt="' + $scope.attributedata[i].Caption + '"';
                    $scope.dyn_Cont += 'class="entityDetailImgPreview">';

                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '</div></div>';
                    }
                    else {
                        if ($scope.IsLockNotif == false) {
                            $scope.dyn_Cont += "<a class='margin-left10x' ng-model='UploadImage' ng-click='UploadImagefile(" + $scope.attributedata[i].ID + ")' href='#UplaodImagediv' data-toggle='modal' attributeTypeID='" + $scope.attributedata[i].TypeID + "'";
                            $scope.dyn_Cont += 'entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="Uploader_' + $scope.attributedata[i].ID + '"';
                            $scope.dyn_Cont += 'my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                            $scope.dyn_Cont += ' class="ng-pristine ng-valid" attributename=\"' + $scope.UploaderCaption["UploaderCaption_" + $scope.attributedata[i].ID] + '\">Select Image';
                            $scope.dyn_Cont += '</a></div></div>';
                        }
                        else if ($scope.IsLockNotif == true) {
                            $scope.dyn_Cont += '</div></div>';
                        }
                    }
                }
                else if ($scope.attributedata[i].TypeID == 3) {

                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {

                        if ($scope.attributedata[i].Caption[0] != undefined) {

                            $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                            $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalDropdownCaption();

                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            }
                            else {
                                if ($scope.IsLockNotif == false) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                }
                                else if ($scope.IsLockNotif == true) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                }
                            }
                        }
                        else {

                            $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalDropdownCaption();

                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                $scope.dyn_Cont += '<div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span>';
                                $scope.dyn_Cont += '</div></div>';
                            }
                            else {
                                if ($scope.IsLockNotif == false) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                    $scope.dyn_Cont += '<div class="controls"><a  xeditabledropdown href=\"javascript:;\"';
                                    $scope.dyn_Cont += 'attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '"';
                                    $scope.dyn_Cont += 'attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"';
                                    $scope.dyn_Cont += 'data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                    $scope.dyn_Cont += 'attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\"';
                                    $scope.dyn_Cont += 'data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a>';
                                    $scope.dyn_Cont += '</div></div>';
                                }
                                else if ($scope.IsLockNotif == true) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                    $scope.dyn_Cont += '<div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span>';
                                    $scope.dyn_Cont += '</div></div>';
                                }
                            }
                        }

                    } else {

                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption[0].length > 1) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();

                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                }
                                else {
                                    if ($scope.IsLockNotif == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    }
                                    else if ($scope.IsLockNotif == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                    }
                                }
                            }
                        }
                        else {

                            $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalDropdownCaption();

                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            }
                            else {
                                if ($scope.IsLockNotif == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                }
                                else if ($scope.IsLockNotif == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                }
                            }
                        }
                    }
                }
                else if ($scope.attributedata[i].TypeID == 4) {

                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    if ($scope.attributedata[i].Caption[0] != undefined) {
                        if ($scope.attributedata[i].Caption.length > 1) {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;

                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            }
                            else {
                                if ($scope.IsLockNotif == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                }
                                else if ($scope.IsLockNotif == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                }
                            }
                        }
                    }
                    else {

                        $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                        $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;

                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        }
                        else {
                            if ($scope.IsLockNotif == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            }
                            else if ($scope.IsLockNotif == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            }
                        }
                    }
                }

                else if ($scope.attributedata[i].TypeID == 10) {

                    var inlineEditabletitile = $scope.attributedata[i].Caption;

                    var perioddates = [];

                    $scope.dyn_Cont += '<div class="period control-group nomargin" data-periodcontainerID="periodcontainerID">';
                    if ($scope.attributedata[i].Value == "-") {
                        $scope.IsStartDateEmpty = true;
                        $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';

                        $scope.dyn_Cont += '</div>';
                    }
                    else {

                        for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {

                            var datStartUTCval = "";
                            var datstartval = "";
                            var datEndUTCval = "";
                            var datendval = "";

                            //datStartUTCval = $scope.attributedata[i].Value[j].Startdate.substr(6, ($scope.attributedata[i].Value[j].Startdate.indexOf('+') - 6));


                            datStartUTCval = $scope.attributedata[i].Value[j].Startdate;
                            datstartval = new Date.create(datStartUTCval);


                            //datEndUTCval = $scope.attributedata[i].Value[j].EndDate.substr(6, ($scope.attributedata[i].Value[j].EndDate.indexOf('+') - 6));

                            datEndUTCval = $scope.attributedata[i].Value[j].EndDate;
                            datendval = new Date.create(datEndUTCval);

                            perioddates.push({ ID: $scope.attributedata[i].Value[j].Id, value: datendval });

                            $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                            $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));

                            $scope.fields["PeriodStartDate_Dir_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                            $scope.fields["PeriodEndDate_Dir_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));

                            if ($scope.attributedata[i].Value[j].Description == undefined) {
                                $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = "-";
                                $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "";
                            } else {
                                $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                            }

                            $('#fsedateid').css("visibility", "hidden");
                            $scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + $scope.attributedata[i].Value[j].Id + '">';
                            $scope.dyn_Cont += '<div class="inputHolder span11">';

                            $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label>';
                            $scope.dyn_Cont += '<div class="controls">';

                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<span>{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                $scope.dyn_Cont += '<span> to </span><span>{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                            }
                            else {
                                if ($scope.IsLockNotif == false) {
                                    $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                    $scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                }
                                else if ($scope.IsLockNotif == true) {
                                    $scope.dyn_Cont += '<span>{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                    $scope.dyn_Cont += '<span> to </span><span>{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                }
                            }

                            $scope.dyn_Cont += '</div></div>';

                            $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                            $scope.dyn_Cont += '<div class="controls">';

                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<span>{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                            }
                            else {
                                if ($scope.IsLockNotif == false) {
                                    $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                }
                                else if ($scope.IsLockNotif == true) {
                                    $scope.dyn_Cont += '<span>{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                }
                            }

                            $scope.dyn_Cont += '</div></div></div>';

                            if (j != 0) {
                                if ($scope.IsLockNotif == false) {
                                    $scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + $scope.attributedata[i].Value[j].Id + ')"><i class="icon-remove"></i></a></div>';
                                }
                            }

                            $scope.dyn_Cont += '</div>';

                            if (j == ($scope.attributedata[i].Value.length - 1)) {

                                $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';

                                $scope.dyn_Cont += '</div>';
                            }
                        }
                    }
                    $scope.dyn_Cont += ' </div>';

                    $scope.dyn_Cont += '<div class="control-group nomargin">';
                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '<label  data-tempid="startendID" class="control-label" for="label">Start date / End date</label>';
                        $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[Add Start / End Date ]</a>';
                        $scope.dyn_Cont += '<span>[Add Start / End Date ]</span>';
                    }
                    else {
                        if ($scope.IsLockNotif == false) {
                            if ($scope.attributedata[i].Value == "-") {
                                $scope.dyn_Cont += '<label id="fsedateid"  class="control-label" for="label">' + inlineEditabletitile + '</label>';
                            }
                            $scope.dyn_Cont += '<div class="controls">';
                            $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[Add ' + inlineEditabletitile + ' ]</a>';
                            $scope.dyn_Cont += '</div>';
                        }
                        else if ($scope.IsLockNotif == true) {
                            $scope.dyn_Cont += '<span class="controls">[Add ' + inlineEditabletitile + ' ]</span>';
                        }
                    }
                    $scope.dyn_Cont += '</div>';
                }
                else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime && $scope.attributedata[i].ID != SystemDefiendAttributes.CreationDate) {
                    var datStartUTCval = "";
                    var datstartval = "";
                    var inlineEditabletitile = $scope.attributedata[i].Caption;

                    //datStartUTCval = $scope.attributedata[i].Value.substr(6, ($scope.attributedata[i].Value.indexOf('+') - 6));
                    //datstartval = new Date.create(parseInt(datStartUTCval));
                    datStartUTCval = $scope.attributedata[i].Value;
                    datstartval = new Date.create(datStartUTCval);

                    $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateFormat(datstartval, GlobalUserDateFormat);
                    $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = formatteddateFormat(datstartval, $scope.DefaultSettings.DateFormat);

                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                    }
                    else {
                        if ($scope.IsLockNotif == false) {
                            $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a xeditabletreedatetime data-primaryid="' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-datetime_id=\"DateTime_' + $scope.attributedata[i].ID + '\" data-ng-model=\"DateTime_' + $scope.attributedata[i].ID + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"datetime' + $scope.attributedata[i].ID + '\" href=\"javascript:;\" attributename="datetime">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                        }
                        else if ($scope.IsLockNotif == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        }
                    }
                }
                else if ($scope.attributedata[i].TypeID == 7) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                    $scope.fields["Tree_" + $scope.attributedata[i].ID] = [];
                    $scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                    GetTreeCheckedNodes($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID], $scope.attributedata[i].ID);
                    $scope.staticTreesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class="control-group relative">';
                    $scope.dyn_Cont += '<label class="control-label">' + $scope.attributedata[i].Lable + ' </label>';
                    $scope.dyn_Cont += '<div class="controls">';
                    if ($scope.IsLockNotif == false) {
                        $scope.dyn_Cont += '<div xeditabletree  editabletypeid="treeType_' + $scope.attributedata[i].ID + '" attributename=\"' + $scope.attributedata[i].Lable + '\" isreadonly="' + $scope.attributedata[i].IsReadOnly + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '"  data-type="treeType_' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"' + $scope.attributedata[i].ID + '\" data-ng-model=\"tree_' + $scope.attributedata[i].ID + '"\    data-original-title=\"' + $scope.attributedata[i].Lable + '\">';
                        if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                            treeTextVisbileflag = false;
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                            }
                            else
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                        }
                        else {
                            $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                        }

                        $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\" treeplace="detail" node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                        $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';

                        $scope.dyn_Cont += ' </div>';
                    }
                    else {
                        if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                            treeTextVisbileflag = false;
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                            }
                            else
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                        }
                        else {
                            $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                        }

                        $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\"  node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                        $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';
                    }
                    $scope.dyn_Cont += '</div></div>';

                }
                else if ($scope.attributedata[i].TypeID == 8) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;

                    $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                    if ($scope.attributedata[i].Caption != undefined) {
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                    }
                    if ($scope.attributedata[i].IsReadOnly == true) {
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                    }
                    else {
                        if ($scope.IsLockNotif == false) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"   data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                        }
                        else if ($scope.IsLockNotif == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        }
                    }
                }
                else if ($scope.attributedata[i].TypeID == 13) {


                    $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.attributedata[i].ID + ""] = $scope.attributedata[i].DropDownPricing;
                    $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.attributedata[i].ID + ""] = true;
                    for (var j = 0, price; price = $scope.attributedata[i].DropDownPricing[j++];) {


                        if (price.selection.length > 0) {
                            var selectiontext = "";
                            var valueMatches = [];
                            if (price.selection.length > 0)
                                valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                    return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                });
                            if (valueMatches.length > 0) {
                                selectiontext = "";
                                for (var x = 0, val; val = valueMatches[x++];) {
                                    selectiontext += val.caption;
                                    if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                    else selectiontext += "</br>";
                                }
                            }
                            else
                                selectiontext = "-";
                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + j] = selectiontext;
                        }
                        else {
                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + j] = "-";
                        }

                        if ($scope.IsLockNotif == false) {

                            $scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = false;
                            $scope.dyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><a  href=\"javascript:;\" xeditablepercentage entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + j + '" editabletypeid="percentagetype' + $scope.attributedata[i].ID + '_' + j + '" data-type="percentagetype' + $scope.attributedata[i].ID + '_' + j + '"  my-qtip2 qtip-content=\"' + price.LevelName + '\"  attributename=\"' + price.LevelName + '\"  ><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + j + ')"></span></a></div></div>';
                        }
                        else {
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = true;
                            $scope.dyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><span class="editable"><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + j + ')"></span></span></div></div>';
                        }

                    }
                }
            }

            $("#notificationworktaskdynamicdetail").unbind(); $("#notificationworktaskdynamicdetail").empty(); $("#notificationworktaskdynamicdetail").html($compile("")($scope));
            $("#notificationapprovalworktaskdynamicHolder").unbind(); $("#notificationapprovalworktaskdynamicHolder").empty(); $("#notificationapprovalworktaskdynamicHolder").html($compile("")($scope));
            $("#notificationreviewtaskdynamicholder").unbind(); $("#notificationreviewtaskdynamicholder").empty(); $("#notificationreviewtaskdynamicholder").html($compile("")($scope));
            $("#notificationunassignedtaskdynamicdataholder").unbind(); $("#notificationunassignedtaskdynamicdataholder").empty(); $("#notificationunassignedtaskdynamicdataholder").html($compile("")($scope));

            if (fromassignedplace) {
                if ($scope.TaskDetails.TaskStatus != 0) {
                    if (tasktype == 2)
                        $("#notificationworktaskdynamicdetail").html(
                                            $compile($scope.dyn_Cont)($scope));
                    if (tasktype == 3)
                        $("#notificationapprovalworktaskdynamicHolder").html(
                                            $compile($scope.dyn_Cont)($scope));
                    if (tasktype == 31)
                        $("#notificationreviewtaskdynamicholder").html(
                                            $compile($scope.dyn_Cont)($scope));
                }
                else {
                    $("#notificationunassignedtaskdynamicdataholder").html(
                                           $compile($scope.dyn_Cont)($scope));
                }
            }
            else
                $("#notificationunassignedtaskdynamicdataholder").html(
                                        $compile($scope.dyn_Cont)($scope));


            //----------> HIDE THE ATTRIBUTE THAT ALL ARE HAVING ATTRIBUTE RELATIONS <------------
            HideAttributeToAttributeRelationsOnPageLoad();



            //---------> SHOW THE ATTRIBUTE THAT ALL ARE HAVING THE VALUE TO SHOW THE ATTRIBUTE RELATIONS <----------
            for (i = 0; i < $scope.attributedata.length; i++) {
                if (($scope.attributedata[i].TypeID == 3 || $scope.attributedata[i].TypeID == 4 || $scope.attributedata[i].TypeID == 6 || $scope.attributedata[i].TypeID == 7 || $scope.attributedata[i].TypeID == 12) && $scope.attributedata[i].IsSpecial == false) {
                    if ($scope.attributedata[i].TypeID == 12) {
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null) {
                                if (($scope.attributedata[i].Lable.length - 1) == j) {
                                    var k = $scope.attributedata[i].Value.length - $scope.attributedata[i].Lable.length;

                                    for (k; k < $scope.attributedata[i].Value.length; k++) {
                                        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[k].Nodeid, 12);
                                    }
                                }
                                else {
                                    $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid, 12);
                                }
                            }
                        }
                    }
                    else if ($scope.attributedata[i].TypeID == 6) {
                        for (var j = 0; j < $scope.attributedata[i].Lable.length - 1; j++) {
                            if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null)
                                $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid, 6);
                        }
                    }
                    else if ($scope.attributedata[i].TypeID == 4) {
                        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.attributedata[i].Value, 4);
                    }
                    else if ($scope.attributedata[i].TypeID == 7) {
                        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.fields["Tree_" + $scope.attributedata[i].ID], 7);
                    }
                    else {
                        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.attributedata[i].Value, $scope.attributedata[i].TypeID);
                    }
                }
            }

            //---------------->  DETAILS BLOCK ENDS HERE <---------------------

        }

        $scope.drpdirectiveSource = {};

        $scope.IsSourceformed = function (attrID, levelcnt, attributeLevel, attrType) {
            if (levelcnt > 0) {
                var dropdown_text = '', subid = 0;
                var currntlevel = attributeLevel + 1;
                if (attributeLevel == 1) {
                    var dropdown_text = 'dropdown_text_' + attrID + '_' + attributeLevel;
                    var idtomatch = $scope['dropdown_' + attrID + '_' + attributeLevel] != undefined ? ($scope['dropdown_' + attrID + '_' + attributeLevel].id != undefined ? $scope['dropdown_' + attrID + '_' + attributeLevel].id : $scope['dropdown_' + attrID + '_' + attributeLevel]) : 0;
                    if (idtomatch != 0)
                        $scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel] = $.grep($scope.treeSources["dropdown_" + attrID].Children,
                            function (e) {
                                return e.id == idtomatch;
                            }
                        )[0];
                }
                for (var j = currntlevel; j <= levelcnt; j++) {
                    dropdown_text = 'dropdown_text_' + attrID + '_' + j;
                    subid = $scope['dropdown_' + attrID + '_' + (j)] != undefined ? ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined ? $scope['dropdown_' + attrID + '_' + (j - 1)].id : $scope['dropdown_' + attrID + '_' + (j)]) : 0;
                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = {};
                    if (subid != 0 && subid > 0) {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + (j)] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children != undefined && $scope['dropdown_' + attrID + '_' + (j - 1)] != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = ($.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children, function (e) { return e.id == subid; }))[0];
                        }
                    }
                    else {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + j] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)] != undefined) {
                                var res = [];
                                if ($scope['dropdown_' + attrID + '_' + (j)] > 0)
                                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)]['Children'], function (e) { return e.id == $scope['dropdown_' + attrID + '_' + (j)] })[0];
                                else
                                    $scope['dropdown_' + attrID + '_' + (j)] = 0;
                            }
                        }
                    }
                }
            }
        }

        $scope.BindChildDropdownSource = function (attrID, levelcnt, attributeLevel, attrType) {

            if (levelcnt > 0) {
                var currntlevel = attributeLevel + 1;
                for (var j = currntlevel; j <= levelcnt; j++) {
                    $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].length);
                    if (attrType == 6) {
                        $scope["dropdown_" + attrID + "_" + j] = 0;
                        $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].length);
                    } else if (attrType == 12) {
                        if (j == levelcnt) $scope["multiselectdropdown_" + attrID + "_" + j] = [];
                        else $scope["multiselectdropdown_" + attrID + "_" + j] = "";
                    }
                }
                if (attrType == 6) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {
                        var children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["dropdown_" + attrID + "_" + attributeLevel]) })[0].Children;

                        if (children != undefined) {
                            var subleveloptions = [];
                            $.each(children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                } else if (attrType == 12) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {

                        var sublevel_res = [];
                        if ($scope["multiselectdropdown_" + attrID + "_" + attributeLevel] != undefined && $scope["multiselectdropdown_" + attrID + "_" + attributeLevel] > 0)
                            sublevel_res = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["multiselectdropdown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (sublevel_res != undefined) {
                            var subleveloptions = [];
                            $.each(sublevel_res, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                }
            }
        }


        //task metadata action methods comes here

        var treeTextVisbileflag = false;
        function IsNotEmptyTree(treeObj) {

            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                }
                else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        $scope.deletePeriodDate = function (periodid) {
            //var DeletePeriodByID = $resource('planning/DeleteEntityPeriod/:ID', { ID: periodid }, { delete: { method: 'DELETE' } });
            //var deletePerById = DeletePeriodByID.delete({ ID: periodid }, function () {
            PlanningService.DeleteEntityPeriod(periodid).then(function (deletePerById) {
                if (deletePerById.StatusCode == 200) {

                    NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                    UpdateEntityPeriodTree($routeParams.ID);
                    $("#dynamicdetail div[data-dynPeriodID = " + periodid + " ]").html('');

                    perioddates = $.grep(perioddates, function (val) {
                        return val.ID != periodid;
                    });

                }
                else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4316.Caption'));
                }
            });
        }

        //save detail block

        $scope.saveDropdownTree = function (attrID, attributetypeid, entityTypeid, optionarray) {
            if (attributetypeid == 6) {
                //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($scope.TaskBriefDetails.taskID, 10), AttributeID: attrID, Level: 0 });
                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                updateentityattrib.AttributetypeID = attributetypeid;
                // var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                MetadataService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    }
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        for (var j = 0; j < optionarray.length; j++) {
                            if (optionarray[j] != 0) {
                                $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                            }
                        }
                    }
                });
                $scope.treeSelection = [];
            }

            else if (attributetypeid == 1 || attributetypeid == 2) {
                //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($scope.TaskBriefDetails.taskID, 10), AttributeID: attrID, Level: 0 });
                //var updateentityattrib = new UpdateEntityAttributes();                
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                updateentityattrib.AttributetypeID = attributetypeid;
                if (attrID == 68) {
                    $(window).trigger("UpdateEntityName", optionarray);
                    $("ul li a[data-entityid='" + $scope.TaskBriefDetails.taskID + "'] span[data-name='text']").text([optionarray]);
                    $("ul li a[data-entityid='" + $scope.TaskBriefDetails.taskID + "']").attr('data-entityname', optionarray);
                    var Splitarr = [];
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            if ($scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"] != undefined) {
                                if ($scope.TaskBriefDetails.taskID != $scope.ListViewDetails[i].data.Response.Data[j]["Id"] && $scope.TaskBriefDetails.taskID == $scope.ListViewDetails[i].data.Response.Data[j]["ParentID"]) {
                                    if (Splitarr.length == 0) {
                                        Splitarr = $scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"].split('!@#');
                                    }
                                    $scope.ListViewDetails[i].data.Response.Data[j]["Parenentitytname"] = optionarray + "!@#" + Splitarr[1] + "!@#" + Splitarr[2];
                                }
                            } else {
                                break;
                            }

                        }
                    }
                }
                // var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                MetadataService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        $scope.fields['SingleLineTextValue_' + attrID] = optionarray;
                        if (attrID == 68) {
                            $('#breadcrumlink').text(optionarray);
                        }
                    }
                });
                $scope.treeSelection = [];
            }
            else if (attributetypeid == 3 || attributetypeid == 4) {
                //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($scope.TaskBriefDetails.taskID, 10), AttributeID: attrID, Level: 0 });
                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                updateentityattrib.AttributetypeID = attributetypeid;
                // var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                MetadataService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                });

                $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, optionarray);
                $scope.treeSelection = [];
            }
            else if (attributetypeid == 12) {
                //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($scope.TaskBriefDetails.taskID, 10), AttributeID: attrID, Level: 0 });
                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                //updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                // var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                MetadataService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        for (var j = 0; j < optionarray.length; j++) {
                            if (optionarray[j] != 0) {
                                $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                            }
                        }
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption')); UpdateTreeScope(attrID, $('a[entityid="' + entityTypeid + '"][attributeid="' + attrID + '"]').text());

                    }
                });
                $scope.treeSelection = [];
            }
            else if (attributetypeid == 8) {
                if (!isNaN(optionarray.toString())) {
                    //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($scope.TaskBriefDetails.taskID, 10), AttributeID: attrID, Level: 0 });
                    //var updateentityattrib = new UpdateEntityAttributes();
                    var updateentityattrib = {};
                    updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                    updateentityattrib.AttributeID = attrID;
                    updateentityattrib.Level = 0;
                    updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                    updateentityattrib.AttributetypeID = attributetypeid;
                    //  var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                    MetadataService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                        if (updateentityattribresult.StatusCode == 405)
                            NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                        else {
                            $scope.fields['SingleLineTextValue_' + attrID] = optionarray == "" ? 0 : optionarray;
                            $scope.fields['SingleLineText_' + attrID] = optionarray == "" ? 0 : optionarray;
                            NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        }
                    });
                    $scope.treeSelection = [];
                }
                else {
                    NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                }
            }
            else if (attributetypeid == 5) {
                // var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForLevels/:EntityID/:AttributeID/:Level', { EntityID: parseInt($scope.TaskBriefDetails.taskID, 10), AttributeID: attrID, Level: 0 });

                var sdate = new Date.create($scope.fields['DateTime_Dir_' + attrID].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                if (sdate == 'NaN-NaN-NaN') {
                    sdate = dateFormat($scope.fields['DateTime_Dir_' + periodid].toString(), GlobalUserDateFormat);
                }
                //  var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = [sdate];
                updateentityattrib.AttributetypeID = attributetypeid;
                // var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                MetadataService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        $scope.fields["DateTime_" + attrID] = dateFormat(sdate, GlobalUserDateFormat);
                    }
                });
                $scope.treeSelection = [];
            }

            $scope.treeSelection = [];

        };


        $scope.savePeriodVal = function (attrID, attrTypeID, entityid, periodid) {
            var one_day = 1000 * 60 * 60 * 24;
            if (periodid == 0) {
                //var InsertNewEntityPeriod = $resource('planning/InsertEntityPeriod/');
                //var newperiod = new InsertNewEntityPeriod();
                var newperiod = {};
                var sdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var edate = new Date.create($scope.fields['PeriodEndDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));

                //----------> Start date and end date diff checking
                var maxPeriodDate = new Date.create(Math.max.apply(null, tempperioddates));
                var diffval = (parseInt(edate.getTime() - sdate.getTime()));
                if (diffval < 0) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_1956.Caption'));
                    return false;
                }

                var tempperioddates = [];

                for (var i = 0 ; i < perioddates.length; i++) {
                    tempperioddates.push(perioddates[i].value)
                }

                var one_day = 1000 * 60 * 60 * 24;
                var maxPeriodDate = new Date.create(Math.max.apply(null, tempperioddates));
                var diffval = (parseInt(sdate.getTime() - maxPeriodDate.getTime()));
                if (diffval < 1) {

                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";

                    bootbox.alert($translate.instant('LanguageContents.Res_1957.Caption'));
                    return false;
                }

                newperiod.EntityID = entityid;
                newperiod.StartDate = ConvertDateToString($scope.fields['PeriodStartDate_Dir_' + periodid]);
                newperiod.EndDate = ConvertDateToString($scope.fields['PeriodEndDate_Dir_' + periodid]);
                newperiod.Description = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                newperiod.SortOrder = 0;

                // var resultperiod = InsertNewEntityPeriod.save(newperiod, function () {
                PlanningService.InsertEntityPeriod(newperiod).then(function (resultperiod) {
                    if (resultperiod.StatusCode == 405)
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    else
                        var newid = resultperiod.Response;

                    if ($scope.IsStartDateEmpty == true) {
                        $scope.IsStartDateEmpty = false;
                        $("[data-tempid=startendID]").remove();
                    }

                    perioddates.push({ ID: newid, value: edate });

                    $scope.dyn_Cont = '';
                    $scope.fields["PeriodStartDate_" + newid] = ConvertDateFromStringToString(ConvertDateToString(sdate));
                    $scope.fields["PeriodEndDate_" + newid] = ConvertDateFromStringToString(ConvertDateToString(edate))

                    $scope.fields["PeriodStartDate_Dir_" + newid] = dateFormat(new Date.create(sdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('dd-MM-yyyy'), $scope.DefaultSettings.DateFormat);
                    $scope.fields["PeriodEndDate_Dir_" + newid] = dateFormat(new Date.create(edate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('dd-MM-yyyy'), $scope.DefaultSettings.DateFormat);
                    $scope.fields["PeriodDateDesc_Dir_" + newid] = $scope.fields['PeriodDateDesc_Dir_' + periodid];


                    if ($scope.fields['PeriodDateDesc_Dir_' + periodid] == "" || $scope.fields['PeriodDateDesc_Dir_' + periodid] == undefined) {
                        $scope.fields["PeriodDateDesc_" + newid] = "-";
                    }
                    else {
                        $scope.fields["PeriodDateDesc_" + newid] = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                    }
                    $('#fsedateid').css("visibility", "hidden");
                    $scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + newid + '">';

                    $scope.dyn_Cont += '<div class="inputHolder span11">';

                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Start / End Date </label>';
                    $scope.dyn_Cont += '<div class="controls">';
                    $scope.dyn_Cont += '<a  xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodstartdate_id=\"PeriodStartDate_' + newid + '\" data-ng-model=\"PeriodStartDate_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\" data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + newid + '}}</a>';
                    $scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodenddate_id=\"PeriodEndDate_' + newid + '\" data-ng-model=\"PeriodEndDate_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\"  data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + newid + '}}</a>';
                    $scope.dyn_Cont += '</div></div>';

                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Comment Start / End Date </label>';
                    $scope.dyn_Cont += '<div class="controls">';
                    $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + ID + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodenddate_id=\"PeriodDateDesc_' + newid + '\" data-ng-model=\"PeriodDateDesc_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\"  data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + newid + '}}</a>';
                    $scope.dyn_Cont += '</div></div></div>';

                    if (perioddates.length != 1) {
                        $scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + newid + ')"><i class="icon-remove"></i></a></div></div>';
                    }

                    var divcompile = $compile($scope.dyn_Cont)($scope);

                    if ($scope.fromAssignedpopup) {
                        if ($scope.TaskBriefDetails.taskTypeId == 2)
                            $("#notificationworktaskdynamicdetail div[data-addperiodid]").append(divcompile);
                        if ($scope.TaskBriefDetails.taskTypeId == 3)
                            $("#notificationapprovalworktaskdynamicHolder div[data-addperiodid]").append(divcompile);
                        if ($scope.TaskBriefDetails.taskTypeId == 31)
                            $("#notificationreviewtaskdynamicholder div[data-addperiodid]").append(divcompile);
                    }
                    else
                        $("#notificationunassignedtaskdynamicdataholder div[data-addperiodid]").append(divcompile);


                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                });
            }
            else {
                //var UpdateperiodByID = $resource('planning/EntityPeriod/');
                //var updateperiod = new UpdateperiodByID();
                var updateperiod = {};
                var temparryStartDate = [];
                $('[data-periodstartdate_id^=PeriodStartDate_]').each(function () {

                    if (parseInt(periodid) < parseInt(this.attributes['data-primaryid'].textContent)) {
                        var sdate;
                        if (this.text != "[Add Start / End Date ]") {
                            sdate = new Date.create(this.text.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                            temparryStartDate.push(sdate);
                        }
                    }
                });

                var temparryEndate = [];
                $('[data-periodenddate_id^=PeriodEndDate_]').each(function () {
                    if (parseInt(periodid) > parseInt(this.attributes['data-primaryid'].textContent)) {
                        var edate = new Date.create(this.text.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                        temparryEndate.push(edate);
                    }
                });


                var sdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                if (sdate == 'NaN-NaN-NaN') {
                    sdate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['PeriodStartDate_Dir_' + periodid].toString(), GlobalUserDateFormat))
                }
                var edate = new Date.create($scope.fields['PeriodEndDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                if (edate == 'NaN-NaN-NaN') {
                    edate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['PeriodEndDate_Dir_' + periodid].toString(), GlobalUserDateFormat))
                }


                //----------> Start date and end date diff checking
                var diffval = ((parseInt(new Date.create(edate.toString('dd/MM/yyyy')).getTime()) - parseInt((new Date.create(sdate.toString('dd/MM/yyyy')).getTime()))));
                if (diffval < 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1956.Caption'));
                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    return false;
                }

                var maxPeriodEndDate = new Date.create(Math.max.apply(null, temparryEndate));
                var Convertsdate = new Date.create(sdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var diffvalend = (parseInt(Convertsdate.getTime() - maxPeriodEndDate.getTime()));
                if (parseInt(diffvalend) < 1) {


                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";

                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    bootbox.alert($translate.instant('LanguageContents.Res_1992.Caption'));
                    return false;
                }

                var minPeroidStartDate = new Date.create(Math.min.apply(null, temparryStartDate));
                var Convertedate = new Date.create(edate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var diffvalstart = (parseInt(Convertedate.getTime() - minPeroidStartDate.getTime()));
                if (parseInt(diffvalstart) > 1) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";

                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    bootbox.alert($translate.instant('LanguageContents.Res_1991.Caption'));

                    return false;
                }


                updateperiod.ID = periodid;
                updateperiod.EntityID = entityid;
                updateperiod.StartDate = sdate;
                updateperiod.EndDate = edate;
                updateperiod.SortOrder = 0;
                updateperiod.Description = $scope.fields['PeriodDateDesc_Dir_' + periodid];

                // var resultperiod = UpdateperiodByID.save(updateperiod, function () {
                PlanningService.PostEntityPeriod1(updateperiod).then(function (resultperiod) {
                    if (resultperiod.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    }
                    else {
                        $scope.fields["PeriodStartDate_" + periodid] = ConvertDateFromStringToString(sdate);
                        $scope.fields["PeriodEndDate_" + periodid] = ConvertDateFromStringToString(edate);
                        $scope.fields["PeriodDateDesc_" + periodid] = $scope.fields['PeriodDateDesc_Dir_' + periodid] == "" ? "-" : $scope.fields['PeriodDateDesc_Dir_' + periodid];
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                    }
                });
            }
        }

        $scope.savetreeDetail = function (attrID, attributetypeid, entityTypeid) {
            if (attributetypeid == 7) {
                $scope.treeNodeSelectedHolder = [];
                GetTreeObjecttoSave(attrID);
                //var UpdateEntityAttributes = $resource('metadata/SaveDetailBlockForTreeLevels/:EntityID/:AttributeID', { EntityID: parseInt($scope.TaskBriefDetails.taskID, 10), AttributeID: attrID });
                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.NewValue = $scope.treeNodeSelectedHolder;
                updateentityattrib.newTree = $scope.staticTreesrcdirec["Attr_" + attrID];
                updateentityattrib.oldTree = $scope.treesrcdirec["Attr_" + attrID];
                updateentityattrib.AttributetypeID = attributetypeid;
                updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                updateentityattrib.AttributeID = attrID;
                // var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                MetadataService.SaveDetailBlockForTreeLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    }
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        //var GetTreeRes = $resource('metadata/GetAttributeTreeNodeByEntityID/:AttributeID/:EntityID');
                        //var GetTree = GetTreeRes.get({ AttributeID: attrID, EntityID: parseInt($scope.TaskBriefDetails.taskID, 10) }, function () {
                        MetadataService.GetAttributeTreeNodeByEntityID(attrID, parseInt($scope.TaskBriefDetails.taskID, 10)).then(function (GetTree) {
                            $scope.treesrcdirec["Attr_" + attrID] = JSON.parse(GetTree.Response).Children;
                            if ($scope.treeNodeSelectedHolder.length > 0)
                                $scope.TreeEmptyAttributeObj["Attr_" + attrID] = true;
                            else
                                $scope.TreeEmptyAttributeObj["Attr_" + attrID] = false;

                        });
                        $scope.fields["Tree_" + attrID].splice(0, $scope.fields["Tree_" + attrID].length);
                        GetTreeCheckedNodes($scope.treeNodeSelectedHolder, attrID);
                        $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, $scope.fields["Tree_" + attrID]);
                    }
                });

            }
        }

        $scope.saveDropDownTreePricing = function (attrID, attributetypeid, entityTypeid, choosefromParent, inherritfromParent) {
            if (attributetypeid == 13) {
                var NewValue = [];
                NewValue = ReturnSelectedTreeNodes(attrID);
                //var UpdateEntityAttributes = $resource('metadata/UpdateDropDownTreePricing/:EntityID/:AttributeID', { EntityID: parseInt($scope.TaskBriefDetails.taskID, 10), AttributeID: attrID });
                //var updateentityattrib = new UpdateEntityAttributes();
                var updateentityattrib = {};
                updateentityattrib.EntityID = parseInt($scope.TaskBriefDetails.taskID, 10);
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = NewValue;
                updateentityattrib.AttributetypeID = attributetypeid;
                // var updateentityattribresult = UpdateEntityAttributes.save(updateentityattrib, function () {
                MetadataService.UpdateDropDownTreePricing(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    }
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        //var GetTreeRes;
                        //if (choosefromParent)
                        //    GetTreeRes = $resource('metadata/GetDropDownTreePricingObjectFromParentDetail/:AttributeID/:IsInheritFromParent/:Isfromparent/:Entityid/:ParentId');
                        //else
                        //    GetTreeRes = $resource('metadata/GetDropDownTreePricingObject/:AttributeID/:IsInheritFromParent/:Isfromparent/:Entityid/:ParentId');
                        //var GetTree = GetTreeRes.get({ AttributeID: attrID, IsInheritFromParent: choosefromParent, Isfromparent: choosefromParent, Entityid: parseInt($scope.TaskBriefDetails.taskID, 10), ParentId: 0 }, function () {
                        MetadataService.GetDropDownTree(choosefromParent, attrID, choosefromParent, choosefromParent, parseInt($scope.TaskBriefDetails.taskID, 10), 0).then(function (GetTree) {
                            if (GetTree.Response != null) {
                                var result = GetTree.Response;
                                for (var p = 0, price; price = result[p++];) {
                                    var attributeLevelOptions = [];
                                    attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""], function (e) { return e.level == p; }));
                                    if (attributeLevelOptions[0] != undefined) {
                                        attributeLevelOptions[0].selection = price.selection;
                                        attributeLevelOptions[0].LevelOptions = price.LevelOptions;

                                        //Updating data in detail block

                                        if (price.selection.length > 0) {
                                            var selectiontext = "";
                                            var valueMatches = [];
                                            if (price.selection.length > 0)
                                                valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                                    return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                                });
                                            if (valueMatches.length > 0) {
                                                selectiontext = "";
                                                for (var x = 0, val; val = valueMatches[x++];) {
                                                    selectiontext += val.caption;
                                                    if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                                    else selectiontext += "</br>";
                                                }
                                            }
                                            else
                                                selectiontext = "-";
                                            $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = selectiontext;
                                        }
                                        else {
                                            $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = "-";
                                        }
                                    }
                                }

                            }
                        });
                    }
                });

            }
        }

        function ReturnSelectedTreeNodes(attrID) {
            var selection = [];
            for (var y = 0, price; price = $scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""][y++];) {

                if (price.selection.length > 0) {
                    var matches = [];
                    matches = jQuery.grep(price.LevelOptions, function (relation) {
                        return price.selection.indexOf(relation.NodeId.toString()) != -1;
                    });
                    if (matches.length > 0)
                        for (var z = 0, node; node = matches[z++];) {
                            selection.push({ "NodeId": node.NodeId, "Level": price.level, "value": node.value != "" ? node.value : "-1" })
                        }

                }
            }
            return selection;
        }
        function UpdateTreeScope(ColumnName, Value) {
            for (var k = 0; k < $scope.ListViewDetails[0].data.Response.ColumnDefs.length; k++) {

                if (ColumnName == $scope.ListViewDetails[0].data.Response.ColumnDefs[k].Field) {
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                            if (EntityId == parseInt($scope.TaskBriefDetails.taskID, 10)) {

                                $scope.ListViewDetails[i].data.Response.Data[j][ColumnName] = Value;
                                return false;
                            }
                        }
                    }
                }
            }
        }


        function GetTreeObjecttoSave(attributeid) {
            GenerateTreeStructure($scope.staticTreesrcdirec["Attr_" + attributeid]);
        }

        var treeformflag = false;
        function GenerateTreeStructure(treeobj) {

            for (var i = 0, node; node = treeobj[i++];) {

                if (node.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == node.AttributeId && e.id == node.id; });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(node);
                    }
                    treeformflag = false;
                    if (ischildSelected(node.Children)) {
                        GenerateTreeStructure(node.Children);
                    }
                    else {
                        GenerateTreeStructure(node.Children);
                    }
                }
                else
                    GenerateTreeStructure(node.Children);
            }

        }


        function ischildSelected(children) {

            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    treeformflag = true;
                    return treeformflag
                }
            }
            return treeformflag;
        }

        //task metadata handling methods ends here


        //detail dynamic block works ends here

        function GetEntityAttributesDetails(taskID) {
            //var GetTaskDetails = $resource('task/GetEntityAttributesDetails/:TaskID', { TaskID: taskID }, { get: { method: 'GET' } });
            //var TaskDetailList = GetTaskDetails.get({ TaskID: taskID }, function () {
            TaskService.GetEntityAttributesDetails(taskID).then(function (TaskDetailList) {
                LoadTaskMetadata(TaskDetailList.Response, $scope.TaskBriefDetails.taskID, $scope.TaskBriefDetails.taskTypeId, $scope.fromAssignedpopup);

            });
        }


        //task dynamic metadata handling works ends here-->END
        ///dam file selection part
        function refreshAssetobjects() {
            $scope.PageNoobj = { pageno: 1 };
            $scope.TaskStatusObj = [{ "Name": "Thumbnail", ID: 1 }, { "Name": "Summary", ID: 2 }, { "Name": "List", ID: 3 }];
            $scope.SettingsDamAttributes = {};
            $scope.OrderbyObj = [{ "Name": "Name (Ascending)", ID: 1 }, { "Name": "Name (Descending)", ID: 2 }, { "Name": "Creation date (Ascending)", ID: 3 }, { "Name": "Creation date (Descending)", ID: 4 }];
            $scope.FilterStatus = { filterstatus: 1 };
            $scope.DamViewName = { viewName: "Thumbnail" };
            $scope.OrderbyName = { orderbyname: "Creation date (Descending)" };
            $scope.OrderBy = { order: 4 };
            $scope.AssetSelectionFiles = [];
        }
        refreshAssetobjects();
        $scope.NotifyAssetfileTemplateLoading = "Null";
        $scope.SetCurrentTabid = function (activecalss, deactiveclass) {
            $("#li" + activecalss + "").removeClass('active');
            $("#" + activecalss + "").removeClass('tab-pane active');
            $("#" + activecalss + "").removeClass('tab-pane');
            $("#li" + activecalss + "").addClass('active');
            $("#" + activecalss + "").addClass('tab-pane active');

            $("#li" + deactiveclass + "").removeClass('active');
            $("#" + deactiveclass + "").removeClass('tab-pane active');
            $("#" + deactiveclass + "").removeClass('tab-pane');
            $("#" + deactiveclass + "").addClass('tab-pane');

        }

        $scope.$on('CallBackAttachtak', function (event, ID) {
            GetEntityTaskAttachmentinfo($scope.TaskBriefDetails.taskID);
            //refreshAssetobjects();
            $timeout(function () {
                $scope.processingsrcobj.processingid = ID;
                $scope.processingsrcobj.processingplace = "task";
                $scope.$broadcast('ReloadAssetView');
            }, 100);

        });
        //$scope.CloseDampopup = function () {
        //    if ($scope.NotifyAssetfileTemplateLoading != null) {
        //        $scope.SetCurrentTabid("task-MetaData-" + $scope.NotifyAssetfileTemplateLoading, "task-Attachments-" + $scope.NotifyAssetfileTemplateLoading)
        //        $("#task-Attachments-" + $scope.NotifyAssetfileTemplateLoading + "dam").empty();
        //        $("#task-Attachments-" + $scope.NotifyAssetfileTemplateLoading + "dam").unbind();
        //        $scope.NotifyAssetfileTemplateLoading = "Null";
        //    }
        //};

        function GetEntityTaskAttachmentinfo(taskID) {
            //var GetTaskDetails = $resource('task/GetEntityTaskAttachmentinfo/:TaskID', { TaskID: taskID }, { get: { method: 'GET' } });
            //var TaskAttachmentinfo = GetTaskDetails.get({ TaskID: taskID }, function () {
            TaskService.GetEntityTaskAttachmentinfo(taskID).then(function (TaskAttachmentinfo) {
                if (TaskAttachmentinfo.Response != null) {
                    $scope.TaskBriefDetails.Totalassetcount = TaskAttachmentinfo.Response[0].totalcount;
                    $scope.TaskBriefDetails.Totalassetsize = parseSize(TaskAttachmentinfo.Response[0].totalfilesize);
                }

            });
        }

        $scope.$on('Taskassetinfoupdate', function (event) {

            $timeout(function () {
                GetEntityTaskAttachmentinfo($scope.TaskBriefDetails.taskID);
            }, 100);

        });

        //dam related logic goes here

        $scope.processingsrcobj = { processingid: $scope.TaskBriefDetails.taskID, processingplace: "task", processinglock: $scope.IsLockNotif };

        $scope.NormalDropdownCaptionObj = [];
        $scope.setNormalDropdownCaption = function () {
            var keys1 = [];

            angular.forEach($scope.NormalDropdownCaption, function (key) {
                keys1.push(key);
                $scope.NormalDropdownCaptionObj = keys1;
            });
        }

        // --- Define Controller Variables. ----------------- //

        // Get the render context local to this controller (and relevant params).
        var renderContext = requestContext.getRenderContext("mui.notificationtaskeditCntrl");


        // --- Define Scope Variables. ---------------------- //


        // The subview indicates which view is going to be rendered on the page.
        $scope.subview = renderContext.getNextSection();

        // Get the current year for copyright output.
        $scope.copyrightYear = (new Date()).getFullYear();


        // --- Bind To Scope Events. ------------------------ //


        // I handle changes to the request context.
        $scope.$on(
            "requestContextChanged",
            function () {
                // Make sure this change is relevant to this controller.
                if (!renderContext.isChangeRelevant()) {
                    return;
                }

                //Make sure the user is logged in
                $scope.LoginStatus();

                // Update the view that is being rendered.
                $scope.subview = renderContext.getNextSection();

            }
        );



        $scope.$on("$destroy", function () {
            $timeout.cancel(NewsFeedUniqueTimerForTask);
            RecursiveUnbindAndRemove($("[ng-controller='mui.notificationtaskeditCntrl']"));
        });
        // --- Initialize. ---------------------------------- //

        // ...

        //For Setting Color Code - By Madhur 22 Dec 2014
        $scope.set_color = function (clr) {
            if (clr != null)
                return { 'color': "#" + clr.toString().trim() };
            else
                return '';
        }
    }
    //);
    app.controller("mui.notificationtaskeditCntrl", ['$scope', '$cookies', '$resource', '$route', '$routeParams', 'requestContext', '$compile', '$window', '$timeout', '$location', '_', '$sce', '$translate', 'TaskService', 'PlanningService', 'CommonService', 'MetadataService', 'AccessService', muinotificationtaskeditCntrl]);
})(angular, app);