﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Task.Interface
{
    public interface IAdminTaskCheckList
    {
         int ID { get; set; }
         int TaskId { get; set; }
         string NAME { get; set; }
         int SortOrder { get; set; }
    }
}
