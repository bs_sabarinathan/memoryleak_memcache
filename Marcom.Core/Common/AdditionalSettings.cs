    using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// Feed object for table 'cm_addtionalsttings'.
	/// </summary>

    internal class AdditionalSettings :  IAdditionalSettings 
	{
		#region Member Variables

		protected int _id;
        protected string _settingname;
        protected string _settingvalue;
        protected IList<ICurrencyType> _currencyformatvalue;

		
		#endregion
		
		#region Constructors
        public AdditionalSettings() { }

        public AdditionalSettings(int pid, string pSettingName, string pSettingValue, IList<ICurrencyType> pCurrencyFormatvalue)
		{
            this._id = pid;
            this._settingname = pSettingName;
            this._settingvalue = pSettingValue;
            this._currencyformatvalue = pCurrencyFormatvalue;
		
		}
				
	
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}

        public string SettingName 
        {
            get { return _settingname; }
            set { _settingname = value; }
        }

        public string SettingValue
        {
            get { return _settingvalue; }
            set { _settingvalue = value; }
        }
        public IList<ICurrencyType> CurrencyFormatvalue
        {
            get { return _currencyformatvalue; }
            set { _currencyformatvalue = value; }
        }

		#endregion 
		
		#region Equals And HashCode Overrides
	
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
