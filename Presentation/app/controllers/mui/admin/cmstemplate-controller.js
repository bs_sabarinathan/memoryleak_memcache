﻿(function (ng, app) {
    "use strict";

    function muicmstemplateCtrl($scope, $location, $resource, $timeout, $translate, AdminService) {
        var isduplicate = false;
        var TenantPath = '';
        $scope.Defaultfirstcontent = '';
        $scope.defaultlastcontent = '';
        $scope.Orignalsnippettemplates = [];
        AdminService.getTenantClientPath().then(function (path) {
            TenantPath = path.substr(1, 7) + "/" + path.substr(10, 7) + "/";
        });
        $timeout(function () {
            StartSnippetImageUpload();
        }, 100);

        function StartSnippetImageUpload() {
            var uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                browse_button: 'pickfiles',
                container: 'filescontainer',
                max_file_size: '10mb',
                flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                url: 'Handlers/ImageUploader.ashx?Path=' + TenantPath + 'CMSFiles/contentbuilder/thumbnails',
                multi_selection: false,
                filters: [{
                    title: "Image files",
                    extensions: "jpg,gif,png"
                }]
            });
            uploader.bind('Init', function (up, params) { });
            uploader.init();
            uploader.bind('FilesAdded', function (up, files) {
                $.each(files, function (i, file) { });
                up.refresh();
                uploader.start();
                $('#pickfiles').hide();
                $('#uploadprogress').show();
                $('#uploadprogress .bar').css('width', '0');
                $('#uploadprogress #barText').html('0 %');
            });
            uploader.bind('UploadProgress', function (up, file) {
                $('#uploadprogress .bar').css("width", file.percent + "%");
                $('#uploadprogress #barText').html(file.percent + "%");
            });
            uploader.bind('Error', function (up, err) {
                bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                up.refresh();
            });
            uploader.bind('FileUploaded', function (up, file, response) {
                $('#newsnippetimage').attr('src', TenantPath + 'CMSFiles/contentbuilder/thumbnails/' + response.response.split(',')[0]);
                var w = parseInt(response.response.split(',')[1]);
                var h = parseInt(response.response.split(',')[2]);
                var path = response.response.split(',')[0];
                $scope.thumbnailguid = path;
            });
        }
        getSnippettemplates();

        function getSnippettemplates() {
            var basicHTML = getBasictemplate();
            AdminService.GetAllCmsSnippetTemplates().then(function (result) {
                if (result.Response != null) {
                    $scope.snippettemplates = result.Response;
                    $scope.Orignalsnippettemplates = [];
                    for (var i = 0; i < result.Response.length; i++) {
                        $scope.Orignalsnippettemplates.push({
                            ID: result.Response[i].ID,
                            Content: result.Response[i].SnippetContent,
                            DefaultFirstContent: result.Response[i].DefaultFirstContent,
                            DefaultLastContent: result.Response[i].DefaultLastContent
                        });
                    }
                    $timeout(function () {
                        angular.forEach($scope.snippettemplates, function (value, key) {
                            $('#img' + value.ID).attr('src', TenantFilePath + 'CMSFiles\\contentbuilder\\thumbnails\\' + value.ThumbnailGuid + "?time=" + new Date().getTime());
                        });
                    }, 100);
                }
            });
        };
        $scope.InsertUpdateSnippetTemplates = function () {
            if ($scope.selectedsnippetID == 0 && isduplicate == false) {
                if ($("#newsnippetimage").attr('src') == "") {
                    bootbox.alert($translate.instant('LanguageContents.Res_5727.Caption'));
                    return false;
                }
                var templateObj = {};
                templateObj.snippet = $scope.selectedsnippet;
                templateObj.active = 1;
                templateObj.thumnailguid = $scope.thumbnailguid;
                templateObj.Id = $scope.selectedsnippetID;
                var newHTML = getBasictemplate();
                if (isduplicate) {
                    templateObj.templateHTML = $scope.selectedsnippet;
                    templateObj.templateHTML = newHTML.replace(/CMS_HTML_Content/g, $scope.selectedsnippet);
                } else {
                    var temp = newHTML.replace(/CMS_HTML_Content/g, templateObj.snippet);
                    templateObj.templateHTML = temp;
                }
                AdminService.InsertCmsSnippetTemplate(templateObj).then(function (result) {
                    isduplicate = false;
                    if (result.Response != 0) {
                        NotifySuccess($translate.instant('LanguageContents.Res_5822.Caption'));
                        $("#HTMLSnippetEditorPopup").modal("hide");
                        $('#newsnippetimage').attr('src', '');
                        getSnippettemplates();
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_5838.Caption'));
                        $("#HTMLSnippetEditorPopup").modal("hide");
                        $('#newsnippetimage').attr('src', '');
                        getSnippettemplates();
                    }
                });
            } else if ($scope.selectedsnippetID != 0 && isduplicate == false) {
                if ($("#newsnippetimage").attr('src') == "") {
                    bootbox.alert($translate.instant('LanguageContents.Res_5727.Caption'));
                    return false;
                }
                $scope.UpdateSnippetTemplate();
            } else if ($scope.selectedsnippetID == 0 && isduplicate == true) {
                if ($("#newsnippetimage").attr('src') == "") {
                    bootbox.alert($translate.instant('LanguageContents.Res_5727.Caption'));
                    return false;
                }
                $scope.DuplicateSnippetTemplates();
            } else {
                var templateObj = {};
                templateObj.originalID = $scope.selectedsnippetID;
                templateObj.snippet = $scope.selectedsnippet
                templateObj.active = 1;
            }
        };
        $scope.UpdateSnippetTemplate = function () {
            var templateObj = {};
            templateObj.snippet = $scope.selectedsnippet;
            templateObj.active = 1;
            templateObj.thumnailguid = $scope.thumbnailguid;
            templateObj.Id = $scope.selectedsnippetID;
            $timeout(function () {
                AdminService.UpdateSnippetTemplate(templateObj).then(function (result) {
                    if (result.Response != 0) {
                        NotifySuccess($translate.instant('LanguageContents.Res_5799.Caption'));
                        $("#HTMLSnippetEditorPopup").modal("hide");
                        $('#newsnippetimage').attr('src', '');
                        getSnippettemplates();
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                        $("#HTMLSnippetEditorPopup").modal("hide");
                        $('#newsnippetimage').attr('src', '');
                        getSnippettemplates();
                    }
                });
            }, 1000);
        };

        function AppendtoSnippetHTML(templateSnippet) {
            jQuery.get(TenantFilePath + 'CMSFiles/contentbuilder/snippets.html', function (data) {
                if (data != '') {
                    var newhtml = data;
                    newhtml += templateSnippet;
                    return newhtml;
                }
            });
        }
        $scope.DeleteSnippetTemplate = function (templateObjfrDel) {
            $('#' + templateObjfrDel.ID).remove();
            var templateObj = {};
            templateObj.snippet = templateObjfrDel.SnippetContent;
            templateObj.active = 0;
            templateObj.thumnailguid = templateObjfrDel.ThumbnailGuid;
            templateObj.Id = templateObjfrDel.ID;
            templateObj.cmsTemplate = '';
            templateObj.updateHTML = $('#snippettemplatediv').html();
            AdminService.UpdateSnippetTemplate(templateObj).then(function (result) {
                if (result.Response != 0) {
                    var index = $scope.snippettemplates.indexOf(templateObjfrDel);
                    $scope.snippettemplates.splice(index, 1);
                    NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4284.Caption'));
                }
            });
        };
        $scope.EditSnippettemplate = function (snippetObj) {
            $('#HTMLSnippetEditorPopup').modal('show');
            isduplicate = false;
            $scope.selectedsnippet = snippetObj.SnippetContent;
            $scope.selectedsnippetID = snippetObj.ID;
            $scope.thumbnailguid = snippetObj.ThumbnailGuid;
            $('#newsnippetimage').attr('src', TenantFilePath + 'CMSFiles/contentbuilder/thumbnails/' + snippetObj.ThumbnailGuid);
        };
        $scope.AddnewSnippet = function () {
            $('#HTMLSnippetEditorPopup').modal('show');
            $scope.selectedsnippetID = 0;
            $scope.selectedsnippet = '';
            $scope.thumbnailguid = '';
            $('#newsnippetimage').attr('src', '');
        };
        $scope.ChangesnippetImage = function () { };

        function getBasictemplate() {
            var basictemplate = '';
            basictemplate += '<div id="CMSUniqueID" data-thumb="CMSFiles/contentbuilder/thumbnails/CMSUniqueID.png">';
            basictemplate += '    <div class="row clearfix">';
            basictemplate += '       <div class="column full">';
            basictemplate += '          CMS_HTML_Content';
            basictemplate += '        </div>';
            basictemplate += '    </div>';
            basictemplate += '</div>';
            return basictemplate;
        };

        function replaceAll(find, replace, str) {
            return str.replace(new RegExp(find, 'g'), replace);
        }
        var snippetHTMLtext = '';
        jQuery.get(TenantFilePath + 'CMSFiles/contentbuilder/snippets.html', function (data) {
            snippetHTMLtext = data;
            $('#snippettemplatediv').append(snippetHTMLtext);
        });

        function getnewsnippetHTML(id) {
            $('#' + id + ' > div > div.full').empty();
            $('#' + id + ' > div > div.full').html($scope.selectedsnippet);
            return $('#snippettemplatediv').html();
        }
        $scope.OpenPreview = function () {
            var tempHtml = "";
            if ($scope.selectedsnippetID > 0) {
                var defaultval = $.grep($scope.Orignalsnippettemplates, function (e) {
                    return e.ID == $scope.selectedsnippetID
                });
                tempHtml = defaultval[0].DefaultFirstContent + $scope.selectedsnippet.replace(/CMSFiles/g, TenantFilePath + 'CMSFiles') + defaultval[0].DefaultLastContent;
            } else {
                var defaultFirstContent = "<div><div class=\"row clearfix\"><div class=\"column\">";
                var defaultLastContent = "</div></div></div>";
                tempHtml = defaultFirstContent + $scope.selectedsnippet.replace(/CMSFiles/g, TenantFilePath + 'CMSFiles') + defaultLastContent;
            }
            $("#PreviewImg").html(tempHtml);
            $timeout(function () { $scope.GeneratePreview(); }, 200);
        };
        $scope.GeneratePreview = function () {
            var baseval = {
                imageinbase: ""
            };
            html2canvas($("#PreviewImg"), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL("image/jpeg");
                    baseval.imageinbase = img;
                    $("#PreviewImg").html(canvas);
                    AdminService.SaveImageFromBaseFormat(baseval).then(function (Res) {
                        if (Res.Response != "") {
                            $scope.thumbnailguid = Res.Response;
                            $('#newsnippetimage').attr('src', TenantFilePath + 'CMSFiles/contentbuilder/thumbnails/Temp/' + $scope.thumbnailguid);
                        }
                    });
                }
            });
        };
        $scope.DuplicateSnippettemplate = function (snippetObj) {
            $('#HTMLSnippetEditorPopup').modal('show');
            $('#newsnippetimage').attr('src', '');
            isduplicate = true;
            var contentHtml = $.grep($scope.Orignalsnippettemplates, function (e) {
                return e.ID == snippetObj.ID;
            });
            $scope.selectedsnippet = contentHtml[0].Content;
            $scope.selectedsnippetID = 0;
            $("#selectedsnippet").html(contentHtml[0].Content);
            $scope.Defaultfirstcontent = contentHtml[0].DefaultFirstContent;
            $scope.defaultlastcontent = contentHtml[0].DefaultLastContent;
        };
        $scope.DuplicateSnippetTemplates = function () {
            if ($scope.selectedsnippetID == 0 && isduplicate == true) {
                var templateObj = {};
                templateObj.content = $scope.selectedsnippet;
                templateObj.defaultFirstContent = $scope.Defaultfirstcontent;
                templateObj.defaultLastContent = $scope.defaultlastcontent;
                templateObj.thumnailguid = $scope.thumbnailguid;
                AdminService.DuplicateCmsSnippetTemplate(templateObj).then(function (result) {
                    isduplicate = false;
                    if (result.Response != 0) {
                        NotifySuccess($translate.instant('LanguageContents.Res_5822.Caption'));
                        $("#HTMLSnippetEditorPopup").modal("hide");
                        $('#selectedsnippet').attr('src', '');
                        getSnippettemplates();
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_5838.Caption'));
                        $("#HTMLSnippetEditorPopup").modal("hide");
                        $('#newsnippetimage').attr('src', '');
                        getSnippettemplates();
                    }
                });
            } else if ($scope.selectedsnippetID != 0 && isduplicate == false) {
                $scope.UpdateSnippetTemplate();
            } else {
                var templateObj = {};
                templateObj.originalID = $scope.selectedsnippetID;
                templateObj.snippet = $scope.selectedsnippet
                templateObj.active = 1;
            }
        };
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.muicmstemplateCtrl']"));
        });
    }
    app.controller("mui.admin.muicmstemplateCtrl", ['$scope', '$location', '$resource', '$timeout', '$translate', 'AdminService', muicmstemplateCtrl]);
})(angular, app);