using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.User.Model
{

	/// <summary>
	/// UserDao object for table 'UM_User'.
	/// </summary>
	public partial class UserDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected string _firstname;
		protected string _lastname;
		protected string _username;
		protected byte[] _password;
        protected string _saltPassword;
		protected string _email;
		protected string _image;
		protected string _language;
		protected string _timezone;
		protected int? _startpage;
        protected string _feedselection;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
        protected int _DashboardTemplateID;
        protected int _IsDashboardCustomized;
        protected string _OldPassword;
        protected bool _gender;
        protected string  _phone;
        protected string _address;
        protected string _city;
        protected int _zipcode;
        protected string _website;

        protected string _designation;
        protected string _title;
        protected string _companyName;
        protected string _Partners;
        protected int _languagesettings;
        protected bool _Isssouser;
        protected bool _Isapiuser;
        protected int _DimensionUnit;
        protected int _AssetAccess;
        protected bool _memberrelated;
		#endregion
		
		#region Constructors
		public UserDao() {}

        public UserDao(string pFirstName, string pLastName, string pUserName, byte[] pPassword, string pEmail, string pImage, string pLanguage, string pTimeZone, int? pStartPage, string psaltPassword, string pFeedSelection, int pDashboardTemplateID, int pIsDashboardCustomized, string pCompanyName, string pPartners, string pOldPassword, int pLanguageSettings, bool pIsapiuser, int pDimensionUnit, int pAssetAccess, bool pMemberRelated)
		{
			this._firstname = pFirstName; 
			this._lastname = pLastName; 
			this._username = pUserName; 
			this._password = pPassword; 
			this._email = pEmail; 
			this._image = pImage; 
			this._language = pLanguage; 
			this._timezone = pTimeZone; 
			this._startpage = pStartPage;
            this._saltPassword = psaltPassword;
            this._feedselection = pFeedSelection;
            this._DashboardTemplateID = pDashboardTemplateID;
            this._IsDashboardCustomized = pIsDashboardCustomized;
            this._companyName = pCompanyName;
            this._Partners = pPartners;
            this._OldPassword = pOldPassword;
            this._languagesettings = pLanguageSettings;
            this._Isapiuser = pIsapiuser;
            this._DimensionUnit = pDimensionUnit;
            this._AssetAccess = pAssetAccess;
            this._memberrelated = pMemberRelated;
		}

				
		public UserDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual string FirstName
		{
			get { return _firstname; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("FirstName", "FirstName value, cannot contain more than 50 characters");
			  _bIsChanged |= (_firstname != value); 
			  _firstname = value; 
			}
			
		}
		
		public virtual string LastName
		{
			get { return _lastname; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("LastName", "LastName value, cannot contain more than 50 characters");
			  _bIsChanged |= (_lastname != value); 
			  _lastname = value; 
			}
			
		}
		
		public virtual string UserName
		{
			get { return _username; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("UserName", "UserName value, cannot contain more than 250 characters");
			  _bIsChanged |= (_username != value); 
			  _username = value; 
			}
			
		}

        public virtual byte[] Password
		{
			get { return _password; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Password", "Password value, cannot contain more than 50 characters");
			  _bIsChanged |= (_password != value); 
			  _password = value; 
			}
			
		}


        public virtual string SaltPassword
        {
            get { return _saltPassword; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Password", "Password value, cannot contain more than 50 characters");
                _bIsChanged |= (_saltPassword != value);
                _saltPassword = value;
            }

        }

		public virtual string Email
		{
			get { return _email; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("Email", "Email value, cannot contain more than 250 characters");
			  _bIsChanged |= (_email != value); 
			  _email = value; 
			}
			
		}
		
		public virtual string Image
		{
			get { return _image; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("Image", "Image value, cannot contain more than 250 characters");
			  _bIsChanged |= (_image != value); 
			  _image = value; 
			}
			
		}
		
		public virtual string Language
		{
			get { return _language; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Language", "Language value, cannot contain more than 50 characters");
			  _bIsChanged |= (_language != value); 
			  _language = value; 
			}
			
		}
		
		public virtual string TimeZone
		{
			get { return _timezone; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("TimeZone", "TimeZone value, cannot contain more than 50 characters");
			  _bIsChanged |= (_timezone != value); 
			  _timezone = value; 
			}
			
		}
		
		public virtual int? StartPage
		{
			get { return _startpage; }
			set { _bIsChanged |= (_startpage != value); _startpage = value; }
			
		}

        public virtual string FeedSelection
        {
            get { return _feedselection; }
            set {_bIsChanged |= (_timezone != value);
                _feedselection = value;}

        }

        public virtual int DashboardTemplateID
        {
            get { return _DashboardTemplateID; }
            set { _DashboardTemplateID = value; }

        }

        public virtual int IsDashboardCustomized
        {
            get { return _IsDashboardCustomized; }
            set { _IsDashboardCustomized = value; }

        }
        public virtual string CompanyName
        {
            get { return _companyName; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("CompanyName", "CompanyName value, cannot contain more than 50 characters");
                _bIsChanged |= (_companyName != value);
                _companyName = value;
            }

        }
        public virtual string Partners
        {
            get { return _Partners; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("Partners", "Partners value, cannot contain more than 50 characters");
                _bIsChanged |= (_Partners != value);
                _Partners = value;
            }

        }
        public virtual string OldPassword
        {
            get { return _OldPassword; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("OldPassword", "OldPassword value, cannot contain more than 50 characters");
                _bIsChanged |= (_OldPassword != value);
                _OldPassword = value;
            }

        }

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}

        public virtual bool Gender
        {
            get { return _gender; }
            set { _gender = value; }

        }

        public virtual string Phone
        {
            get { return _phone; }
            set { _phone = value; }

        }

        public virtual string Address
        {
            get { return _address; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Address", "Address value, cannot contain more than 250 characters");
                _bIsChanged |= (_address != value);
                _address = value;
            }

        }

        public virtual string City
        {
            get { return _city; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("City", "City value, cannot contain more than 50 characters");
                _bIsChanged |= (_city != value);
                _city = value;
            }

        }

        public virtual int ZipCode
        {
            get { return _zipcode; }
            set { _zipcode = value; }
        }

        public virtual string Website
        {
            get { return _website; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Website", "Website value, cannot contain more than 50 characters");
                _bIsChanged |= (_website != value);
                _website = value;
            }

        }

        public virtual string Designation
        {
            get { return _designation; }
            set
            {
                if (value != null && value.Length > 75)
                    throw new ArgumentOutOfRangeException("Designation", "Designation value, cannot contain more than 75 characters");
                _bIsChanged |= (_designation != value);
                _designation = value;
            }

        }

        public virtual string Title
        {
            get { return _title; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Title", "Title value, cannot contain more than 250 characters");
                _bIsChanged |= (_title != value);
                _title = value;
            }

        }
        public virtual bool IsSSOUser
        {
            get { return _Isssouser; }
            set { _bIsChanged |= (_Isssouser != value); _Isssouser = value; }

        }

        public virtual int LanguageSettings
        {
            get { return _languagesettings; }
            set { _languagesettings = value; }
        }

        public virtual bool IsAPIUser
        {
            get { return _Isapiuser; }
            set { _Isapiuser = value; }
        }
        public virtual int DimensionUnit
        {
            get { return _DimensionUnit; }
            set { _bIsChanged |= (_DimensionUnit != value); _DimensionUnit=value; }
        }
        public virtual int AssetAccess
        {
            get { return _AssetAccess; }
            set { _bIsChanged |= (_AssetAccess != value); _AssetAccess = value; }
        }

        public virtual bool MemberRelated
        {
            get { return _memberrelated; }
            set { _memberrelated = value; }
        }
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string UserDao = "UserDao";			
			public const string Id = "Id";			
			public const string FirstName = "FirstName";			
			public const string LastName = "LastName";			
			public const string UserName = "UserName";			
			public const string Password = "Password";
            public const string SaltPassword = "SaltPassword";
			public const string Email = "Email";			
			public const string Image = "Image";			
			public const string Language = "Language";			
			public const string TimeZone = "TimeZone";			
			public const string StartPage = "StartPage";			
            public const string FeedSelection = "FeedSelection";
            public const string DashboardTemplateID = "DashboardTemplateID";
            public const string IsDashboardCustomized = "IsDashboardCustomized";

            public const string Gender = "Gender";
            public const string DOB = "DOB";
            public const string Phone = "Phone";
            public const string Address = "Address";
            public const string City = "City";
            public const string ZipCode = "ZipCode";
            public const string Website = "Website";

            public const string Designation = "Designation";
            public const string Title = "Title";
            public const string CompanyName = "CompanyName";
            public const string Partners = "Partners";
            public const string OldPassword = "OldPassword";
            public const string LanguageSettings = "LanguageSettings";
            public const string IsSSOUser = "IsSSOUser";
            public const string IsAPIUser = "IsAPIUser";
            public const string DimensionUnit = "DimensionUnit";
            public const string AssetAccess = "AssetAccess";
            public const string MemberRelated = "MemberRelated";

		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string UserDao = "UserDao";			
			public const string Id = "Id";			
			public const string FirstName = "FirstName";			
			public const string LastName = "LastName";			
			public const string UserName = "UserName";			
			public const string Password = "Password";
            public const string SaltPassword = "SaltPassword";
			public const string Email = "Email";			
			public const string Image = "Image";			
			public const string Language = "Language";			
			public const string TimeZone = "TimeZone";			
			public const string StartPage = "StartPage";			
            public const string FeedSelection = "FeedSelection";
            public const string DashboardTemplateID = "DashboardTemplateID";
            public const string IsDashboardCustomized = "IsDashboardCustomized";


            public const string Gender = "Gender";
            public const string DOB = "DOB";
            public const string Phone = "Phone";
            public const string Address = "Address";
            public const string City = "City";
            public const string ZipCode = "ZipCode";
            public const string Website = "Website";

            public const string Designation = "Designation";
            public const string Title = "Title";
            public const string CompanyName = "CompanyName";
            public const string Partners = "Partners";
            public const string OldPassword = "OldPassword";
            public const string LanguageSettings = "LanguageSettings";
            public const string IsSSOUser = "IsSSOUser";
            public const string IsAPIUser = "IsAPIUser";
            public const string DimensionUnit = "DimensionUnit";
            public const string AssetAccess = "AssetAccess";
            public const string MemberRelated = "MemberRelated";

		}

		#endregion
		
		
	}
	
}
