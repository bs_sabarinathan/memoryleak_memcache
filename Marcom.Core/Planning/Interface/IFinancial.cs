using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IFinancial interface for table 'PM_Financial'.
    /// </summary>
    public interface IFinancial
    {
        #region Public Properties


        int Id
        {
            get;
            set;

        }

        int Entityid
        {
            get;
            set;

        }

        int CostCenterid
        {
            get;
            set;

        }

        decimal PlannedAmount
        {
            get;
            set;

        }

        decimal RequestedAmount
        {
            get;
            set;

        }

        decimal ApprovedAllocatedAmount
        {
            get;
            set;

        }

        DateTimeOffset LastUpdateOn
        {
            get;
            set;

        }

        int Status
        {
            get;
            set;

        }

        decimal ApprovedBudget
        {
            get;
            set;

        }

        decimal Commited
        {
            get;
            set;

        }

        decimal Spent
        {
            get;
            set;

        }

        DateTimeOffset ApprovedBudgetDate
        {
            get;
            set;

        }

        decimal Q1
        {
            get;
            set;

        }

        decimal Q2
        {
            get;
            set;

        }

        decimal Q3
        {
            get;
            set;

        }

        decimal Q4
        {
            get;
            set;

        }

        decimal M1
        {
            get;
            set;

        }

        decimal M2
        {
            get;
            set;

        }

        decimal M3
        {
            get;
            set;

        }

        decimal M4
        {
            get;
            set;

        }

        decimal M5
        {
            get;
            set;

        }

        decimal M6
        {
            get;
            set;

        }

        decimal M7
        {
            get;
            set;

        }

        decimal M8
        {
            get;
            set;

        }

        decimal M9
        {
            get;
            set;

        }

        decimal M10
        {
            get;
            set;

        }

        decimal M11
        {
            get;
            set;

        }

        decimal M12
        {
            get;
            set;

        }

        decimal H1
        {
            get;
            set;

        }

        decimal H2
        {
            get;
            set;

        }

        decimal Y
        {
            get;
            set;

        }
        decimal ActualQ1
        {
            get;
            set;

        }

        decimal ActualQ2
        {
            get;
            set;

        }

        decimal ActualQ3
        {
            get;
            set;

        }

        decimal ActualQ4
        {
            get;
            set;

        }

        decimal ActualM1
        {
            get;
            set;

        }

        decimal ActualM2
        {
            get;
            set;

        }

        decimal ActualM3
        {
            get;
            set;

        }

        decimal ActualM4
        {
            get;
            set;

        }

        decimal ActualM5
        {
            get;
            set;

        }

        decimal ActualM6
        {
            get;
            set;

        }

        decimal ActualM7
        {
            get;
            set;

        }

        decimal ActualM8
        {
            get;
            set;

        }

        decimal ActualM9
        {
            get;
            set;

        }

        decimal ActualM10
        {
            get;
            set;

        }

        decimal ActualM11
        {
            get;
            set;

        }

        decimal ActualM12
        {
            get;
            set;

        }

        decimal ActualH1
        {
            get;
            set;

        }

        decimal ActualH2
        {
            get;
            set;

        }

        decimal ActualY
        {
            get;
            set;

        }

        decimal SubAssignedAmount
        {
            get;
            set;

        }

        decimal AvailableAssignedAmount
        {
            get;
            set;

        }

        DateTimeOffset LastUpdatedAmountOn
        {
            get;
            set;

        }
        #endregion
    }
}
