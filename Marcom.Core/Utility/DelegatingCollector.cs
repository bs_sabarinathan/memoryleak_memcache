﻿using Lucene.Net.Index;
using Lucene.Net.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Utility
{
    public delegate void DelegatingCollectorHit(IndexReader reader, Int32 documentBase, Int32 documentId, Scorer scorer);
    public class DelegatingCollector : Collector
    {
        private readonly DelegatingCollectorHit _method;
        private IndexReader _reader;
        private Int32 _docBase;
        private Scorer _scorer;

        public DelegatingCollector(DelegatingCollectorHit method)
        {
            _method = method;
        }

        public override void SetScorer(Scorer scorer)
        {
            _scorer = scorer;
        }

        public override void Collect(Int32 doc)
        {
            _method(_reader, _docBase, doc, _scorer);
        }

        public override void SetNextReader(IndexReader reader, Int32 docBase)
        {
            _reader = reader;
            _docBase = docBase;
        }

        public override Boolean AcceptsDocsOutOfOrder
        {
            get { return true; }
        }
    }
}
