using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// ObjectiveRatingDao object for table 'PM_Objective_Rating'.
	/// </summary>
	
	public partial class ObjectiveRatingDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected int _objectiveid;
		protected string _caption;
		protected int _sortorder;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public ObjectiveRatingDao() {}
			
		public ObjectiveRatingDao(int pObjectiveid, string pCaption, int pSortOrder)
		{
			this._objectiveid = pObjectiveid; 
			this._caption = pCaption; 
			this._sortorder = pSortOrder; 
		}
				
		public ObjectiveRatingDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual int Objectiveid
		{
			get { return _objectiveid; }
			set { _bIsChanged |= (_objectiveid != value); _objectiveid = value; }
			
		}
		
		public virtual string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
			  _bIsChanged |= (_caption != value); 
			  _caption = value; 
			}
			
		}
		
		public virtual int SortOrder
		{
			get { return _sortorder; }
			set { _bIsChanged |= (_sortorder != value); _sortorder = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string ObjectiveRatingDao = "ObjectiveRatingDao";			
			public const string Id = "Id";			
			public const string Objectiveid = "Objectiveid";			
			public const string Caption = "Caption";			
			public const string SortOrder = "SortOrder";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string ObjectiveRatingDao = "ObjectiveRatingDao";			
			public const string Id = "Id";			
			public const string Objectiveid = "Objectiveid";			
			public const string Caption = "Caption";			
			public const string SortOrder = "SortOrder";			
		}

		#endregion
		
		
	}
	
	
}
