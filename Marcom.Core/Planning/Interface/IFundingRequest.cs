using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IFundingRequest interface for table 'PM_FundingRequest'.
    /// </summary>
    public interface IFundingRequest
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int RequestedBy
        {
            get;
            set;

        }

        int CostCenterid
        {
            get;
            set;

        }

        int Entityid
        {
            get;
            set;

        }

        int FundRequestSTATUS
        {
            get;
            set;

        }

        string RequestAmount { get; set; }
        string RequestDate { get; set; }

        string Description { get; set; }

        DateTimeOffset LastUpdatedOn
        {
            get;
            set;

        }

        string DueDate
        {
            get;
            set;

        }

        string CostCenterName { get; set; }

        string CostCenterOwnerName { get; set; }

        int Status { get; set; }

        int CostCentreOwnerID { get; set; }
        string RequestUser { get; set; }
        int Duedates { get; set; }
        decimal CCPreviousAllApprovedAmount { get; set; }
        decimal CCTotalApprovedAmountThisLevel { get; set; }
        decimal CCAvailableAmount { get; set; }
        decimal CCAssignedAmount { get; set; }
        decimal AvailableAssigneAmount { get; set; }
        #endregion
    }
}
