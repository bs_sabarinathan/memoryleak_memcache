﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Common.Interface
    {
    /// <summary>
    /// ICurrencyConverter interface for table 'CM_Currencyconverter'.
    /// </summary>
    public interface ICurrencyConverter
        {
        #region Public Properties
      
        int Id {get;set;}
        string Startdate { get; set; }
        string Enddate { get; set; }
        string Currencytype { get; set; }
        double Currencyrate { get; set; }
        
        #endregion
        }
    }
