/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

	/// <summary>
    /// Option object for table 'MM_Option'.
	/// </summary>
	
	internal class Option : IOption 
	{
		#region Member Variables

		protected int _id;
		protected string _caption;
		protected int _attributeid;
        protected int _attributetypeid;
        protected int _sortorder;
		
		
		#endregion
		
		#region Constructors
		public Option() {}

        public Option(int pId, string pCaption, int pAttributeid, int pAttributeTypeid, int pSortorder)
		{
			this._id = pId; 
			this._caption = pCaption; 
			this._attributeid = pAttributeid;
            this._attributetypeid = pAttributeTypeid;
            this._sortorder = pSortorder;
		}

        public Option(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 100)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 100 characters");
			  _caption = value; 
			}
			
		}

        public virtual int AttributeID
        {
            get { return _attributeid; }
            set { _attributeid = value; }

        }

        public virtual int AttributeTypeID
        {
            get { return _attributetypeid; }
            set { _attributetypeid = value; }

        }

        public virtual int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }

        }
		
		
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			Option castObj = null;
			try
			{
                castObj = (Option)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
