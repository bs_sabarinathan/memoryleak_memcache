﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    /// <summary>
    /// IEntityTypeExtension interface for table 'MM_DamType_FileExtension'.
    /// </summary>
    public interface IDamTypeFileExtension
    {
        #region Public Properties

        int ID
        {
            get;
            set;

        }

        int EntityTypeID
        {
            get;
            set;

        }

        string ExtensionOptions
        {
            get;
            set;

        }

        int SortOrder
        {
            get;
            set;

        }

        bool IsDeleted
        {
            get;
            set;
        }
        #endregion
    }
}
