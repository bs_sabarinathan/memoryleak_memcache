using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// Navigation object for table 'CM_Navigation'.
	/// </summary>
	
	internal class Navigation : INavigation 
	{
		#region Member Variables

		protected int _id;
		protected int _parentid;
        protected int _Typeid;
		protected int _moduleid;
		protected int _featureid;
		protected string _caption;
		protected string _description;
		protected string _url;
        protected string _externalurl;
		protected string _javascript;
		protected bool _isactive;
		protected bool _ispopup;
		protected bool _isiframe;
		protected bool _isdynamicpage;
		protected bool _isexternal;
        protected bool _isdefault;
		protected bool _addusername;
		protected bool _adduseremail;
        protected bool _adduserid;
        protected bool _addlanguagecode;
		protected string _imageurl;
        protected int _sortorder;
        protected int _SearchType;
		
		
		#endregion
		
		#region Constructors
		public Navigation() {}

        public Navigation(int pId, int pParentid, int pTypeid, int pModuleid, int pFeatureid, string pCaption, string pDescription, string pUrl, string pJavaScript, bool pIsActive, bool pIsPopup, bool pIsIframe, bool pIsDynamicPage, bool pIsExternal, bool pAddUserName, bool pAddUserEmail, string pImageurl, bool pAddUserID, bool pAddLanguageCode, int pSortOrder, int pSearchType)
		{
			this._id = pId; 
			this._parentid = pParentid;
            this._Typeid = pTypeid;
			this._moduleid = pModuleid; 
			this._featureid = pFeatureid; 
			this._caption = pCaption; 
			this._description = pDescription; 
			this._url = pUrl; 
			this._javascript = pJavaScript; 
			this._isactive = pIsActive; 
			this._ispopup = pIsPopup; 
			this._isiframe = pIsIframe; 
			this._isdynamicpage = pIsDynamicPage; 
			this._isexternal = pIsExternal; 
			this._addusername = pAddUserName; 
			this._adduseremail = pAddUserEmail; 
			this._imageurl = pImageurl;
            this._adduserid = pAddUserID;
            this._addlanguagecode = pAddLanguageCode;
            this._sortorder = pSortOrder;
            this._SearchType = pSearchType;
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public int Parentid
		{
			get { return _parentid; }
			set { _parentid = value; }
			
		}
        public virtual int Typeid
        {
            get { return _Typeid; }
            set { _Typeid = value; }
        }
		
		public int Moduleid
		{
			get { return _moduleid; }
			set { _moduleid = value; }
			
		}
		
		public int Featureid
		{
			get { return _featureid; }
			set { _featureid = value; }
			
		}
		
		public string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
			  _caption = value; 
			}
			
		}
		
		public string Description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
			  _description = value; 
			}
			
		}
		
		public string Url
		{
			get { return _url; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Url", "Url value, cannot contain more than 1073741823 characters");
			  _url = value; 
			}
			
		}

        public virtual string ExternalUrl
        {

            get { return _externalurl; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("ExternalUrl", "ExternalUrl value, cannot contain more than 1073741823 characters");
                _externalurl = value;
            }
        }
		
		public string JavaScript
		{
			get { return _javascript; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("JavaScript", "JavaScript value, cannot contain more than 1073741823 characters");
			  _javascript = value; 
			}
			
		}
		
		public bool IsActive
		{
			get { return _isactive; }
			set { _isactive = value; }
			
		}
		
		public bool IsPopup
		{
			get { return _ispopup; }
			set { _ispopup = value; }
			
		}

        public virtual bool IsDefault
        {
            get { return _isdefault; }
            set { _isdefault = value; }
        }
		
		public bool IsIframe
		{
			get { return _isiframe; }
			set { _isiframe = value; }
			
		}
		

		public bool IsDynamicPage
		{
			get { return _isdynamicpage; }
			set { _isdynamicpage = value; }
			
		}
		
		public bool IsExternal
		{
			get { return _isexternal; }
			set { _isexternal = value; }
			
		}
		
		public bool AddUserName
		{
			get { return _addusername; }
			set { _addusername = value; }
			
		}
		
		public bool AddUserEmail
		{
			get { return _adduseremail; }
			set { _adduseremail = value; }
			
		}

        public bool AddUserID
        {
            get { return _adduserid; }
            set { _adduserid = value; }

        }

        public bool AddLanguageCode
        {
            get { return _addlanguagecode; }
            set { _addlanguagecode = value; }

        }
		
		public string Imageurl
		{
			get { return _imageurl; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("Imageurl", "Imageurl value, cannot contain more than 250 characters");
			  _imageurl = value; 
			}
			
		}

        public int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }
        }

        public int SearchType
        {
            get { return _SearchType; }
            set { _SearchType = value; }
        }

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			Navigation castObj = null;
			try
			{
				castObj = (Navigation)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				this._id.Equals( castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
