﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public partial class FinancialMultiSelectDao : BaseDao, ICloneable
    {
        #region member Variables

        protected int _id { get; set; }
        protected int _finid { get; set; }
        protected int _fintypeid { get; set; }
        protected int _finattributeid { get; set; }
        protected int _finoptionid { get; set; }
        #endregion


        #region Constructors
        public FinancialMultiSelectDao() { }
        public FinancialMultiSelectDao(int pid, int pfinid, int pfintypeid, int pfinattributeid, int pfinoptionid)
        {
            this._id = pid;
            this._fintypeid = pfintypeid;
            this._finattributeid = pfinattributeid;
            this._finoptionid = pfinoptionid;
            this._fintypeid = pfintypeid;
        }
        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int FinID
        {
            get { return _finid; }
            set { _finid = value; }

        }

        public virtual int FinTypeID
        {
            get { return _fintypeid; }
            set { _fintypeid = value; }

        }

        public virtual int FinAttributeID
        {
            get { return _finattributeid; }
            set { _finattributeid = value; }

        }

        public virtual int FinOptionID
        {
            get { return _finoptionid; }
            set { _finoptionid = value; }

        }

        #endregion


        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion


        #region Public Property and Mapping Constants

        public static class PropertyNames
        {
            //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)

            public const string FinancialMultiSelectDao = "FinancialMultiSelectDao";
            public const string ID = "ID";
            public const string FinID = "FinID";
            public const string FinTypeID = "FinTypeID";
            public const string FinAttributeID = "FinAttributeID";
            public const string FinOptionID = "FinOptionID";

        }


        public static class MappingNames
        {
            //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)

            public const string FinancialMultiSelectDao = "FinancialMultiSelectDao";
            public const string ID = "ID";
            public const string FinID = "FinID";
            public const string FinTypeID = "FinTypeID";
            public const string FinAttributeID = "FinAttributeID";
            public const string FinOptionID = "FinOptionID";

        }
        #endregion

    }

}
