﻿(function (ng, app) {
    "use strict";

    function muiplanningtoolcostcentredetaillistviewCtrl($scope, $timeout, $http, $resource, $compile, $window, CclistviewService) {
        $scope.copyrightYear = (new Date()).getFullYear();
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.detail.listviewCtrl']"));
        });
        var colStatus = false;
        ListViewLoad();

        function ListViewLoad() {
            $('#EntitiesTree').scrollTop(0);
            if ($("#EntitiesTree li").hasClass('active')) $("#EntitiesTree li.active").removeClass('active');
            if ($scope.ListTemplate.length > 0) {
                $("#ListHolder").html($scope.ListTemplate);
                for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                    LoadListView($scope.ListViewDetails[i].data, $scope.ListViewDetails[i].PageIndex, $scope.ListViewDetails[i].UniqueID)
                }
                $(window).AdjustHeightWidth();
            }
        }
        $scope.$on('onTreePageCreation', function (event, data) {
            ListViewLoad();
        });

        function LoadListView(ListContentData, PageIndex, UniqueID) {
            if ($("#EntitiesTree li").hasClass('active')) $("#EntitiesTree li.active").removeClass('active');
            var listColumnDefsdata = ListContentData.Response.GeneralColumnDefs;
            if (listColumnDefsdata != null) {
                var listContent = ListContentData.Response.Data;
                var contentnHtml = "<tr>";
                var columnHtml = "<tr >";
                var Constructunique = '';
                if (UniqueID.length > 0) {
                    Constructunique = UniqueID.substr(0, UniqueID.indexOf("-")) + ".";
                }
                var rootID = 0;
                for (var i = 0; i < listContent.length; i++) {
                    if (i != undefined) {
                        var UniqueId = '';
                        var finanicalUniqueKey = "";
                        var IscostCenter = false;
                        if (listContent[i]["TypeID"] == 5 && i == 0) IscostCenter = true;
                        else finanicalUniqueKey = listContent[i]["class"];
                        UniqueId = UniqueKEY(listContent[i]["class"]);
                        var GeneralUniquekey = UniqueKEY(listContent[i]["class"].toString());
                        var ClassName = GenateClass(GeneralUniquekey);
                        ClassName += " mo" + UniqueId;
                        contentnHtml += "<tr data-iscostcentre='" + IscostCenter + "' data-EntityLevel='" + listContent[i]["Level"] + "' data-over='true' data-uniquekey='" + UniqueId + "' class='" + ClassName + "'>"
                        for (var j = 0; j < listColumnDefsdata.m_Item1.length; j++) {
                            if (j != undefined && listColumnDefsdata.m_Item1[j] != "Name") {
                                if (colStatus == false) {
                                    if (listColumnDefsdata.m_Item1[j] != "Status") {
                                        columnHtml += "<th class='align-right'>";
                                    } else {
                                        columnHtml += "<th>";
                                    }
                                    columnHtml += "    <span data-Column=" + listColumnDefsdata.m_Item1[j] + ">" + listColumnDefsdata.m_Item2[j] + "</span>";
                                    columnHtml += "</th>";
                                }
                                if (listContent[i]["TypeName"] != "@Option") {
                                    if (AttributeTypes.Period == listColumnDefsdata.m_Item1[j].Type) {
                                        contentnHtml += "<td><span class='ng-binding'>" + (listContent[i]["TempPeriod"] != undefined ? listContent[i]["TempPeriod"] : "-") + "</span></td>";
                                    } else if (listColumnDefsdata.m_Item1[j] != "Status") {
                                        if (listColumnDefsdata.m_Item1[j] == "InRequest" && listContent[i]["Level"] == 1 && listContent[i][listColumnDefsdata.m_Item1[j]] != 0) {
                                            contentnHtml += "<td data-finkey='" + finanicalUniqueKey + "' data-CostCenterID='" + listContent[i]["CostCenterID"] + "' data-EntityID='" + listContent[i]["Id"] + "' data-UniqueID='" + UniqueId + "' data-EntityName='" + listContent[i]["Name"] + "' data-EntityType='" + listContent[i]["TypeName"] + "' class='CCStatusIcon'><span data-name='text' class='ng-binding'>" + (listContent[i][listColumnDefsdata.m_Item1[j]] == null ? "0" : (listContent[i][listColumnDefsdata.m_Item1[j]] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ')) + " " + $scope.SelectedCostCentreCurrency.ShortName.toUpperCase() + " </span><i class=\'icon-arrow-right icon-large pending\'></i></td>";
                                        } else {
                                            contentnHtml += "<td><span class='ng-binding align-right'>" + (listContent[i][listColumnDefsdata.m_Item1[j]] == null || parseFloat(listContent[i][listColumnDefsdata.m_Item1[j]]) < 0 ? "0" : (listContent[i][listColumnDefsdata.m_Item1[j]] / $scope.SelectedCostCentreCurrency.Rate).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ')) + " " + $scope.SelectedCostCentreCurrency.ShortName.toUpperCase() + "</span></td>";
                                        }
                                    } else {
                                        contentnHtml += "<td><span class='ng-binding'>" + (listContent[i][listColumnDefsdata.m_Item1[j]] == null ? "-" : (listContent[i][listColumnDefsdata.m_Item1[j]].length == 0 ? "-" : listContent[i][listColumnDefsdata.m_Item1[j]])) + "</span></td>";
                                    }
                                }
                            }
                            if (listContent[i]["TypeName"] == "@Option") {
                                contentnHtml += "<td><span></span></td> ";
                            } else contentnHtml += "<td style='display:none;'><span><label class='checkbox checkbox-custom pull-right'><input type='checkbox'><i class='checkbox'></i></label></span></td>";
                        }
                        contentnHtml += "</tr>";
                        colStatus = true;
                    }
                }
                if ($("#ListColumn > thead tr").length == 0) {
                    columnHtml += "<th style='display:none;'><span><label class='checkbox checkbox-custom pull-right'><input type='checkbox'><i class='checkbox'></i></label></span></th>";
                    columnHtml += "</tr>";
                    $('#ListColumn > thead').html(columnHtml);
                }
                if ($("#ListColumn > thead tr").length > 0) {
                    if (UniqueID != "") {
                        $('#ListContainer > table tbody tr[data-uniquekey="' + UniqueID + '"]').after(contentnHtml);
                    } else {
                        $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').html(contentnHtml);
                        $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').removeClass('pending');
                        $('#ListContainer > table tbody[data-page="' + PageIndex + '"]').removeAttr("style");
                    }
                }
            }
            $(window).AdjustHeightWidth();
        }
        $('#ListContainer').scroll(function () {
            $("#EntitiesTree").scrollTop($(this).scrollTop());
        });
        $("#ListContainer table").on("click", ".CCStatusIcon", function () {
            var row = $(this).closest("td");
            var UniqueIDval = row.attr("data-finkey");
            var EntityName = row.attr("data-EntityName");
            var EntityType = row.attr("data-EntityType");
            var substr = UniqueIDval.split('.');
            var costcentreid = substr[0];
            var EntityID = substr[1] + "." + substr[2];
            var EntityUniqueKey = UniqueIDval;
            $scope.EntityCostcenterparentId = substr[0];
            $("#feedFundingRequestModal").modal("show");
            $("#feedFundingRequestModal").trigger("onCostCentreFundingRequestsAction", [row.attr("data-CostCenterID"), row.attr("data-EntityID"), EntityUniqueKey, EntityName, EntityType]);
        });
        $(window).on("onCostcentreFinancialAccept", function (event, urEntityId, sttus) {
            $("#EntitiesTree").scrollTop(0);
            $("#ListContainer").scrollTop(0);
            var ChildappUnquieId = urEntityId.replace('.', '-');
            var ParentUnquieId = ChildappUnquieId.replace('-1', '');
            if ($('tr[data-uniquekey=' + ChildappUnquieId + ']').find('td').find('i').hasClass('icon-arrow-right icon-large pending')) $('tr[data-uniquekey=' + ChildappUnquieId + ']').find('td').find('i').removeClass('icon-arrow-right icon-large pending')
            var IscostCentreUniquekey = $('tr[data-iscostcentre=true]').attr('data-uniquekey');
            $scope.InrequestsVal = $('tr[data-uniquekey=' + ChildappUnquieId + '] td').eq(4).find('span').text();
            $scope.ApprORAlloc = $('tr[data-uniquekey=' + ChildappUnquieId + '] td').eq(6).find('span').text();
            var CurrName = $scope.InrequestsVal.replace(/\d+/g, '').replace(".", "");
            $('tr[data-uniquekey=' + ChildappUnquieId + '] td').eq(4).find('span').html('0.0' + CurrName);
            var res = (parseFloat($scope.InrequestsVal.replace(/[^0-9\.]/g, '')) + parseFloat($scope.ApprORAlloc.replace(/[^0-9\.]/g, ''))).toFixed(1);
            $('tr[data-uniquekey=' + ChildappUnquieId + '] td').eq(6).find('span').html(res + CurrName);
            $scope.CostCentreInrequestsVal = $('tr[data-uniquekey=' + IscostCentreUniquekey + '] td').eq(4).find('span').text();
            $scope.CostCentreApprORAlloc = $('tr[data-uniquekey=' + IscostCentreUniquekey + '] td').eq(6).find('span').text();
            var CostCentreres = (parseFloat($scope.CostCentreInrequestsVal.replace(/[^0-9\.]/g, '')) + parseFloat($scope.CostCentreApprORAlloc.replace(/[^0-9\.]/g, ''))).toFixed(1);
            $('tr[data-uniquekey=' + IscostCentreUniquekey + '] td').eq(4).find('span').html('0.0' + CurrName);
            $('tr[data-uniquekey=' + IscostCentreUniquekey + '] td').eq(6).find('span').html(CostCentreres + CurrName);
            $scope.ParentInrequestsVal = $('tr[data-uniquekey=' + $scope.EntityCostcenterparentId + '] td').eq(4).find('span').text();
            $scope.ParentApprORAlloc = $('tr[data-uniquekey=' + $scope.EntityCostcenterparentId + '] td').eq(6).find('span').text();
            var Parenrtres = (parseFloat($scope.ParentInrequestsVal.replace(/[^0-9\.]/g, '')) + parseFloat($scope.ParentApprORAlloc.replace(/[^0-9\.]/g, ''))).toFixed(1);
            $('tr[data-uniquekey=' + $scope.EntityCostcenterparentId + '] td').eq(4).find('span').html('0.0' + CurrName);
            $('tr[data-uniquekey=' + $scope.EntityCostcenterparentId + '] td').eq(6).find('span').html(Parenrtres + CurrName);
        });

        function RefreshListView() {
            if ($("#EntitiesTree li").hasClass('active')) $("#EntitiesTree li.active").removeClass('active');
            if ($scope.ListTemplate.length > 0) {
                $("#ListHolder").html($scope.ListTemplate);
                $timeout(function () { LazyloadListview(0, 0); }, 10);
            }
        }

        function LazyloadListview(indexno) {
            var Node = {};
            Node.StartRowNo = $scope.ListViewDetails[indexno].StartRowNo;
            Node.MaxNoofRow = $scope.ListViewDetails[indexno].MaxNoofRow;
            Node.FilterID = $scope.ListViewDetails[indexno].FilterID;
            Node.SortOrderColumn = $scope.ListViewDetails[indexno].SortOrderColumn;
            Node.IsDesc = $scope.ListViewDetails[indexno].IsDesc;
            Node.IncludeChildren = false;
            Node.EntityID = $scope.ListViewDetails[indexno].EntityID;
            Node.Level = $scope.ListViewDetails[indexno].Level;
            Node.ExpandingEntityID = '0';
            Node.FilterType = $scope.FilterType;
            Node.IDArr = $scope.ListViewDetails[indexno].IDArr;
            Node.FilterAttributes = $scope.ListViewDetails[indexno].FilterAttributes;
            var counter = indexno;
            CclistviewService.CostCentreDetail(Node).then(function (NodeCnt) {
                if (NodeCnt.Response.Data != null) {
                    $scope.ListViewDetails[counter].data = NodeCnt;
                    LoadListView($scope.ListViewDetails[counter].data, $scope.ListViewDetails[counter].PageIndex, $scope.ListViewDetails[counter].UniqueID);
                    if ($scope.ListViewDetails.length > (indexno + 1)) {
                    }
                }
            });
            $(window).AdjustHeightWidth();
        }

        function UniqueKEY(UniKey) {
            var substr = UniKey.split('.');
            var id = "";
            var row = substr.length;
            if (row > 1) {
                for (var i = 0; i < row; i++) {
                    id += substr[i];
                    if (i != row - 1) {
                        id += "-";
                    }
                }
                return id;
            }
            return UniKey;
        }

        function GenateClass(UniKey) {
            var ToLength = UniKey.lastIndexOf('-');
            if (ToLength == -1) {
                return "";
            }
            var id = UniKey.substring(0, ToLength);
            var afterSplit = id.split('-');
            var result = "";
            for (var i = 0; i < afterSplit.length; i++) {
                result += SplitClss(id, i + 1);
            }
            return result;
        }

        function SplitClss(NewId, lengthofdata) {
            var afterSplit = NewId.split('-');
            var finalresult = " p";
            for (var i = 0; i < lengthofdata; i++) {
                finalresult += afterSplit[i];
                if (i != lengthofdata - 1) {
                    finalresult += "-"
                }
            }
            return finalresult;
        }
    }
    app.controller("mui.planningtool.costcentre.detail.listviewCtrl", ['$scope', '$timeout', '$http', '$resource', '$compile', '$window', 'CclistviewService', muiplanningtoolcostcentredetaillistviewCtrl]);
})(angular, app);