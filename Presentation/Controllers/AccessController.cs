﻿using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Utility;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Presentation.Controllers
{
    public class AccessController : ApiController
    {
        // GET api/<controller>
        ServiceResponse result;
        [Route("api/Access/InsertEntityUserRole")]
        [HttpPost]
        public ServiceResponse InsertEntityUserRole([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.CreateEntityUserRole((int)jobj["EntitiyID"], (int)jobj["RoleID"], (int)jobj["UserID"], (bool)jobj["IsInherited"], (int)jobj["InheritedFromEntityID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Access/DeleteEntityUserRole/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteEntityUserRole(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.DeleteEntityUserRole(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Access/Role")]
        [HttpGet]
        public ServiceResponse Role()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.GetRole();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        //method renamed
        [Route("api/Access/PostRole")]
        [HttpPost]
        public ServiceResponse PostRole([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.SaveUpdateRole((string)jobj["Caption"], (string)jobj["Description"], (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        //method renamed
        [Route("api/Access/PutRole")]
        [HttpPut]
        public ServiceResponse PutRole([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = (marcomManager.AccessManager.SaveUpdateRole((string)jobj["Caption"], (string)jobj["Description"], (int)jobj["ID"]) == 0 ? false : true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Access/DeleteRole/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteRole(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.DeleteRole(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Access/InsertRole")]
        [HttpPost]
        public ServiceResponse InsertRole([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.SaveUpdateRole((string)jobj["Caption"], (string)jobj["Description"], (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Access/InsertEntityACL")]
        [HttpPost]
        public ServiceResponse InsertEntityACL([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.SaveUpdateEntityACL((int)jobj["RoleID"], (int)jobj["ModuleID"], (int)jobj["EntityTypeID"], (int)jobj["FeatureID"], (string)jobj["AccessRights"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Access/GetGlobalAcl/{RoleID}")]
        [HttpGet]
        public ServiceResponse GetGlobalAcl(int RoleID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.GetGlobalAcl(RoleID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Access/CheckGlobalAccessAvaiilability/{GlobalRole}/{Module}/{Features}")]
        [HttpGet]
        public ServiceResponse CheckGlobalAccessAvaiilability(int GlobalRole, int Module, int Features)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;

                result.Response = marcomManager.AccessManager.CheckGlobalAccessAvaiilability(GlobalRole, Module, Features);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Access/InsertGlobalACL")]
        [HttpPost]
        public ServiceResponse InsertGlobalACL([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                var access = jobj["AccessPermission"].ToString().Split(',');
                //var ids = (string)common.ID;
                bool responsevalue = marcomManager.AccessManager.DeleteOldGlobalAccess((int)jobj["GlobalRole"], (int)jobj["Module"], (int)jobj["Features"]);
                if (responsevalue == true)
                {
                    for (int i = 0; i < access.Count(); i++)
                    {
                        result.Response = marcomManager.AccessManager.SaveUpdateGlobalACL((int)jobj["GlobalRole"], (int)jobj["Module"], (int)jobj["EntityType"], (int)jobj["Features"], jobj["AccessPermission"][i].ToString(), 0);
                    }
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Access/DeleteGlobalACL/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteGlobalACL(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                var ids = ID.ToString();
                for (int i = 0; i < ids.Split(',').Length; i++)
                {
                    result.Response = marcomManager.AccessManager.DeleteGlobalACL(Convert.ToInt32(ids.Split(',')[i]));
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Access/GetGlobalRole")]
        [HttpGet]
        public ServiceResponse GetGlobalRole()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.GetGlobalRole();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Access/GetGlobalRoleByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetGlobalRoleByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.GetGlobalRoleByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Access/InsertUpdateGlobalRole")]
        [HttpPost]
        public ServiceResponse InsertUpdateGlobalRole([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.InsertUpdateGlobalRole((string)jobj["Caption"], (string)jobj["Description"], (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Access/DeleteGlobalRole/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteGlobalRole(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.DeleteGlobalRole(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Access/InsertGlobalRoleUser")]
        [HttpPost]
        public ServiceResponse InsertGlobalRoleUser([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                if ((jobj["UserID"] != null) && (jobj["GlobalRoleID"] != null) && (marcomManager.AccessManager.DeleteGlobalRoleUser((int)jobj["UserID"]) == true))
                {
                    JArray jobjGlobalRole = (JArray)jobj["GlobalRoleID"];
                    result.StatusCode = (int)HttpStatusCode.OK;
                    foreach (var val in jobjGlobalRole)
                    {
                        result.Response = marcomManager.AccessManager.InsertGlobalRoleUser((int)val, (int)jobj["UserID"]);
                    }
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Access/GetGlobalRoleUserByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetGlobalRoleUserByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.GetGlobalRoleUserByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Access/CheckUserIsAdmin")]
        [HttpGet]
        public ServiceResponse CheckUserIsAdmin()
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.CheckUserIsAdmin();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Access/GetUserAccessforsso")]
        [HttpGet]
        public ServiceResponse GetUserAccessforsso()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.CheckUserAccess(1, 2, 3);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;

        }
        [Route("api/Access/InsertUpdateGlobalEntitTypeACL")]
        [HttpPost]
        public ServiceResponse InsertUpdateGlobalEntitTypeACL([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                JArray entitytypeAccessObj = (JArray)jobj["entitytypeAccessObj"];
                result.Response = marcomManager.AccessManager.InsertUpdateGlobalEntitTypeACL(entitytypeAccessObj);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Access/GetGlobalEntityTypeAcl/{RoleID}/{moduleID}")]
        [HttpGet]
        public ServiceResponse GetGlobalEntityTypeAcl(int RoleID, int moduleID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.GetGlobalEntityTypeAcl(RoleID, moduleID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Access/DeleteGlobalEntityTypeACL/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteGlobalEntityTypeACL(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                var ids = ID.ToString();
                for (int i = 0; i < ids.Split(',').Length; i++)
                {
                    result.Response = marcomManager.AccessManager.DeleteGlobalEntityTypeACL(Convert.ToInt32(ids.Split(',')[i]));
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Access/GetAllEntityRole")]
        [HttpGet]
        public ServiceResponse GetAllEntityRole()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.GetAllEntityRole();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Access/GetEntityTypeRoleAccess/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetEntityTypeRoleAccess(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.GetEntityTypeRoleAccess(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }



        [Route("api/Access/InsertAssetAccess/")]
        [HttpPost]
        public ServiceResponse InsertAssetAccess([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));


                if ((jobj["UserID"] != null) && (jobj["AssetAccessID"] != null) && (marcomManager.AccessManager.DeleteAssetAccessByID((int)jobj["UserID"]) == true))
                {
                    JArray jobjAssetAccess = (JArray)jobj["AssetAccessID"];
                    result.StatusCode = (int)HttpStatusCode.OK;
                    foreach (var val in jobjAssetAccess)
                    {
                        result.Response = marcomManager.AccessManager.InsertAssetAccess((int)val, (int)jobj["UserID"]);
                    }
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Access/GetAssetAccessByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetAssetAccessByID(int ID)
        {

            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.GetAssetAccessByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Access/IsGlobaAccessLiveChat")]
        [HttpGet]
        public ServiceResponse IsGlobaAccessLiveChat()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.CheckUserAccess((int)Modules.Planning, (int)FeatureID.LiveChat);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Access/GetSuperAdminModule/{Id}")]
        [HttpGet]
        public ServiceResponse GetSuperAdminModule(int Id)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.GetSuperAdminModule(Id);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Access/SaveUpdateSuperAdminRoleFeatures")]
        [HttpPost]
        public ServiceResponse SaveUpdateSuperAdminRoleFeatures([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jr = (JArray)jobj["GlobalAccessFeatures"];
                for (int i = 0; i < jr.Count(); i++)
                {
                    result.Response = marcomManager.AccessManager.SaveUpdateSuperAdminRoleFeatures((int)jobj["GlobalRoleID"], (int)jr[i]["MenuID"], (int)jr[i]["Id"], (bool)jr[i]["IsChecked"] == true ? true : false, (int)jr[i]["GlobalAclID"]);
                }
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Access/DeleteGlobalByID/{ID}/{Menu}")]
        [HttpDelete]
        public ServiceResponse DeleteGlobalByID(int ID, int Menu)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.DeleteGlobalByID(ID, Menu);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Access/GetSuperglobalacl")]
        [HttpGet]
        public ServiceResponse GetSuperglobalacl()
        {
            result = new ServiceResponse();
            try
            {
               
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.GetSuperglobalacl();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Access/GetGlobalRoleUserByRoleID")]
        [HttpPost]
        public ServiceResponse GetGlobalRoleUserByRoleID([FromBody]JArray taskIDs)
        {
            result = new ServiceResponse();
            int count = 0;
            try
            {
                JObject test = new JObject();
                IList<object> tempdata = new List<object>(); IList<object> tempmemberdata = new List<object>();
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                foreach (var roleid in taskIDs)
                {
                    tempdata.Add(marcomManager.AccessManager.GetGlobalRoleUserByRoleID((int)roleid));
                }
                foreach (var i in tempdata)
                {
                    foreach (var k in ((ArrayList)i))
                    {
                        if (tempmemberdata.Count > 0)
                        {
                            for (var j = 0; j < tempmemberdata.Count; j++)
                            {
                                if ((int)(((System.Collections.Hashtable)((object)tempmemberdata[j]))["Userid"]) == (int)(((System.Collections.Hashtable)((object)k))["Userid"]))
                                {
                                    //already exists in array
                                    count++;
                                }
                            }
                            if (count == 0)
                            {
                                tempmemberdata.Add(k);
                            }
                        }
                        else
                        {
                            tempmemberdata.Add(k);
                        }
                    }
                }
                result.Response = tempmemberdata;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Access/SaveUpdateNewsfeedRoleFeatures/")]
        [HttpPost]
        public ServiceResponse SaveUpdateNewsfeedRoleFeatures([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                if (jobj["GlobalAccessFeatures"] != null)
                {
                    JArray jr = (JArray)jobj["GlobalAccessFeatures"];
                    if (jr.Count > 0)
                    {
                        bool isdeleted = marcomManager.AccessManager.DeleteByGlobalRoleID((int)jobj["GlobalRoleID"]);
                        foreach (var gaf in jr)
                        {
                            result.Response = marcomManager.AccessManager.SaveUpdateNewsfeedRoleFeatures((int)jobj["GlobalRoleID"], (JObject)gaf);
                        }
                    }
                    else
                    {
                        bool isdeleted = marcomManager.AccessManager.DeleteByGlobalRoleID((int)jobj["GlobalRoleID"]);
                        result.Response = isdeleted;
                    }
                }
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Access/SaveUpdateNotificationRoleFeatures/")]
        [HttpPost]
        public ServiceResponse SaveUpdateNotificationRoleFeatures([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                if (jobj["GlobalAccessFeatures"] != null)
                {
                    JArray jr = (JArray)jobj["GlobalAccessFeatures"];
                    if (jr.Count > 0)
                    {
                        bool isdeleted = marcomManager.AccessManager.DeleteByGlobalRoleID_Notification((int)jobj["GlobalRoleID"]);
                        foreach (var gaf in jr)
                        {
                            result.Response = marcomManager.AccessManager.SaveUpdateNotificationRoleFeatures((int)jobj["GlobalRoleID"], (JObject)gaf);
                        }
                    }
                    else
                    {
                        bool isdeleted = marcomManager.AccessManager.DeleteByGlobalRoleID_Notification((int)jobj["GlobalRoleID"]);
                        result.Response = isdeleted;
                    }
                }
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Access/GetSelectedFeedFilter/{GlobalRoleID}")]
        [HttpGet]
        public ServiceResponse GetSelectedFeedFilter(int GlobalRoleID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.GetSelectedFeedFilter(GlobalRoleID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Access/GetSelectedNotificationFilter/{GlobalRoleID}")]
        [HttpGet]
        public ServiceResponse GetSelectedNotificationFilter(int GlobalRoleID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.GetSelectedNotificationFilter(GlobalRoleID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Access/Updatepublishaccess")]
        [HttpPost]
        public ServiceResponse Updatepublishaccess([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray selectedfinancialattr = (JArray)jobj["AccessEnable"];

                if (selectedfinancialattr != null)
                {
                    foreach (var selectedfinAttr in selectedfinancialattr)
                    {
                        int Role = (int)selectedfinAttr["Role"];
                        bool AccessPermission = (bool)selectedfinAttr["AccessPermission"];
                        result.Response = marcomManager.AccessManager.Updatepublishaccess(Role, ((int)selectedfinAttr["AccessPermission"] == 0 ? false : true));
                    }
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Access/Getpublishaccess/")]
        [HttpGet]
        public ServiceResponse Getpublishaccess()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.Getpublishaccess();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Access/userpublishaccess/{entityid}")]
        [HttpGet]
        public ServiceResponse userpublishaccess(int entityid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.userpublishaccess(entityid);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }


    }
}