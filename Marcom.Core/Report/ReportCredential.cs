﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using BrandSystems.Marcom.Core.Report.Interface;
using System.Security.Cryptography;
using BrandSystems.Marcom.Core.Access;
namespace BrandSystems.Marcom.Core.Report
{
       /// <summary>
    /// User object for table 'RM_ReportCredential'.
    /// </summary>
    public class ReportCredential : IReportCredential, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _reporturl ;
        protected string _adminusername;
        protected string _adminpassword;
        protected string _viewerusername;
        protected string _viewerpassword;
        protected int _category;
        

        protected IList<GlobalAcl> _listofusergloablroles;

        #endregion

        #region Constructors
        public ReportCredential() { }

        public ReportCredential(string preporturl, string padminusername, string padminpassword,  string pviewerusername, string pviewerpassword, int pcategory)
        {
            this._reporturl = preporturl;
            this._adminusername = padminusername;
            this._adminpassword = padminpassword;
            this._viewerusername = pviewerusername;
            this._viewerpassword = pviewerpassword;
            this._category = pcategory;
           
        }

        public ReportCredential(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public string ReportUrl
        {
            get { return _reporturl; }
            set
            {
                if (value != null && value.Length > 200)
                    throw new ArgumentOutOfRangeException("ReportUrl", "ReportUrl value, cannot contain more than 200 characters");
                _reporturl = value;
            }

        }

        public string AdminUsername
        {
            get { return _adminusername; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("AdminUsername", "AdminUsername value, cannot contain more than 100 characters");
                _adminusername = value;
            }

        }

        public string AdminPassword
        {
            get { return _adminpassword; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("AdminPassword", "AdminPassword value, cannot contain more than 100 characters");
                _adminpassword = value;
            }

        }

        

        public string ViewerUsername
        {
            get { return _viewerusername; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("ViewerUsername", "ViewerUsername value, cannot contain more than 100 characters");

                _viewerusername = value;
            }

        }

        public string ViewerPassword
        {
            get { return _viewerpassword; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("ViewerPassword", "ViewerPassword value, cannot contain more than 100 characters");
                _viewerpassword = value;
            }

        }

        public int Category
        {
            get { return _category; }
            set { _category = value; }

        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            ReportCredential castObj = null;
            try
            {
                castObj = (ReportCredential)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }


        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

    }
	
}
