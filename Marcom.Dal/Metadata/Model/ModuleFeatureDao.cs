using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

	/// <summary>
	/// ModuleFeatureDao object for table 'MM_Module_Feature'.
	/// </summary>
	
	public partial class ModuleFeatureDao : BaseDao, ICloneable 
	{
		#region Member Variables

        protected int _id;
		protected int _moduleid;
		protected int? _featureid;
		protected bool _isenable;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion

        //#region Extensibility Method Definitions

        //public override bool Equals(object obj)
        //{
        //    ModuleFeatureDao toCompare = obj as ModuleFeatureDao;
        //    if (toCompare == null)
        //    {
        //        return false;
        //    }

        //    if (!Object.Equals(this.Moduleid, toCompare.Moduleid))
        //        return false;
        //    if (!Object.Equals(this.Featureid, toCompare.Featureid))
        //        return false;

        //    return true;
        //}

        //public override int GetHashCode()
        //{
        //    int hashCode = 13;
        //    hashCode = (hashCode * 7) + Moduleid.GetHashCode();
        //    hashCode = (hashCode * 7) + Featureid.GetHashCode();
        //    return hashCode;
        //}

        //#endregion
		
		#region Constructors
		public ModuleFeatureDao() {}
			
		public ModuleFeatureDao(int pModuleid, int? pFeatureid, bool pIsEnable)
		{

			this._moduleid = pModuleid; 
			this._featureid = pFeatureid; 
			this._isenable = pIsEnable; 
		}
		
        public ModuleFeatureDao(int pId)
        {
            this._id = pId; 
        }
		
		#endregion
		
		#region Public Properties
		
        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }
        
		public virtual int Moduleid
		{
			get { return _moduleid; }
			set { _bIsChanged |= (_moduleid != value); _moduleid = value; }
			
		}
		
		public virtual int? Featureid
		{
			get { return _featureid; }
			set { _bIsChanged |= (_featureid != value); _featureid = value; }
			
		}
		
		public virtual bool IsEnable
		{
			get { return _isenable; }
			set { _bIsChanged |= (_isenable != value); _isenable = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string Id = "Id";
			public const string ModuleFeatureDao = "ModuleFeatureDao";			
			public const string Moduleid = "Moduleid";			
			public const string Featureid = "Featureid";			
			public const string IsEnable = "IsEnable";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string Id = "Id";
			public const string ModuleFeatureDao = "ModuleFeatureDao";			
			public const string Moduleid = "Moduleid";			
			public const string Featureid = "Featureid";			
			public const string IsEnable = "IsEnable";			
		}

		#endregion
		
		
	}
	
}
