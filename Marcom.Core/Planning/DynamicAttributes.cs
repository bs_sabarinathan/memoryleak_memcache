﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    internal class DynamicAttributes : BaseEntity, IDynamicAttributes 
    {
        #region Member Variables

        protected int _id;
       // protected List<KeyValuePair<string, string>> _attributes;
        protected IDictionary _attributes;

		#endregion
		
		#region Constructors
		public DynamicAttributes() {}

        public DynamicAttributes(int pId, IDictionary pAttributes)
		{
		    this._id  = pId;
			this._attributes = pAttributes; 
		}

        public DynamicAttributes(int pId)
		{
			this._id = pId; 
		}
		
       
		
		#endregion
		
		#region Public Properties

      
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public IDictionary Attributes
        {
            get{ return _attributes;}
            set { _attributes = value; }
           
        }
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            DynamicAttributes castObj = null;
			try
			{
                castObj = (DynamicAttributes)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion


        public int _Id { get; set; }
    }
}
