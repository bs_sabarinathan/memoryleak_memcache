﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    public interface IWidget
    {

        string Id
        {
            get;
            set;
        }

        int TemplateID
        {
            get;
            set;
        }

        string Caption
        {
            get;
            set;
        }

        string Description
        {
            get;
            set;
        }

        int WidgetTypeID
        {
            get;
            set;
        }

        bool IsDynamic
        {
            get;
            set;
        }

        int FilterID
        {
            get;
            set;
        }

        int AttributeID
        {
            get;
            set;
        }

        int DimensionID
        {
            get;
            set;
        }

        string MatrixID
        {
            get;
            set;
        }

        string WidgetQuery
        {
            get;
            set;
        }

        int Column
        {
            get;
            set;
        }

        int Row
        {
            get;
            set;
        }

        int SizeX
        {
            get;
            set;
        }

        int SizeY
        {
            get;
            set;
        }

        int VisualType
        {
            get;
            set;
        }

        int NoOfItem
        {
            get;
            set;
        }
        string ListOfEntityIDs
        {
            get;
            set;
        }
        string ListofSelectEntityIDs
        {
            get;
            set;
        }
        int NoOfYear
        {
            get;
            set;
        }
        int NoOfMonth
        {
            get;
            set;
        }
    }
}
