﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Version = Lucene.Net.Util.Version;
using BrandSystems.Marcom.Core.Planning;
using System.Text;
using BrandSystems.Marcom.Core.Utility;
using System.Text.RegularExpressions;
using System.Xml;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace BrandSystems.Marcom.Core.Planning
{
    public static class GoLucene
    {
        // properties
        //public static string _luceneDir =
        //    Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"lucene_index");
        private static FSDirectory _directoryTemp;

        public static FSDirectory _directoryPath(string tenantPath)
        {


            return _directoryTemp;
        }



        // main search method
        public static IEnumerable<SampleData> _keywordsearch(string searchQuery, string searchField = "", string keyword = "", string tenantPath = "")
        {
            // validation
            if (string.IsNullOrEmpty(searchQuery.Replace("*", "").Replace("?", ""))) return new List<SampleData>();

            // set up lucene searcher
            using (var searcher = new IndexSearcher(_directoryPath(tenantPath), false))
            {
                var hits_limit = 10000;
                var analyzer = new StandardAnalyzer(Version.LUCENE_30);

                // search by single field
                if (!string.IsNullOrEmpty(searchField))
                {


                    // search by multiple fields (ordered by RELEVANCE)
                    var parser = new MultiFieldQueryParser
                        (Version.LUCENE_30, new[] { "SearchText" }, analyzer);
                    var sort = GoLucene.GetSort();
                    var query = parseQuery(searchQuery, parser);


                    var hits = searcher.Search(query, null, hits_limit, sort).ScoreDocs;
                    var results = _mapLuceneToDataList(hits, searcher, keyword);

                    analyzer.Close();
                    searcher.Dispose();
                    _directoryTemp.Dispose();
                    return results;
                }
                // search by multiple fields (ordered by RELEVANCE)
                else
                {
                    var parser = new MultiFieldQueryParser
                        (Version.LUCENE_30, new[] { "SearchText" }, analyzer);
                    var query = parseQuery(searchQuery, parser);
                    var hits = searcher.Search(query, null, hits_limit, Sort.RELEVANCE).ScoreDocs;
                    var results = _mapLuceneToDataList(hits, searcher, keyword);
                    analyzer.Close();
                    searcher.Dispose();
                    _directoryTemp.Dispose();
                    return results;
                }
            }
        }

        public static TopDocs PerformSearch(string searchQuery, int pageno, int pagesize, string sortfieldname = "Name", bool desc = false, string tenantPath = "")
        {
            // validation
            if (string.IsNullOrEmpty(searchQuery.Replace("*", "").Replace("?", ""))) return null;

            // set up lucene searcher
            using (var searcher = new IndexSearcher(_directoryPath(tenantPath), false))
            {
                var analyzer = new StandardAnalyzer(Version.LUCENE_30);
                // search by multiple fields (ordered by RELEVANCE)
                var parser = new MultiFieldQueryParser
                    (Version.LUCENE_30, new[] { "SearchText" }, analyzer);

                var sort = sortfieldname == "Name" ? GoLucene.GetSort(desc) : GoLucene.GetSort(sortfieldname, desc);
                var query = parseQuery(searchQuery, parser);
                var results = searcher.Search(query, null, pageno * pagesize, sort);
                analyzer.Close();
                searcher.Dispose();
                _directoryTemp.Dispose();
                return results;

            }
        }




        public static Tuple<int, int, int, int, int> GetTotalSearchCount(string InputText, bool istag, int UserID, string tenantPath = "")
        {
            // set up lucene searcher
            using (var searcher = new IndexSearcher(_directoryPath(tenantPath), false))
            {

                int p_c = 0, t_c = 0, a_c = 0, al_c = 0, pg_c = 0;
                var hits_limit = 10000;
                var analyzer = new StandardAnalyzer(Version.LUCENE_30);
                string strUserText = "";
                if (UserID != 0)
                {
                    strUserText = " AND UserID:" + UserID;
                }

                var queryParser = new MultiFieldQueryParser
                      (Version.LUCENE_30, new[] { "SearchText" }, analyzer);
                var sort = GoLucene.GetSort();
                string searchqry = istag == true ? "TagWord:" + InputText : InputText;
                string txt = InputText + " AND SearchType:Productions" + strUserText;
                var query1 = parseQuery(txt, queryParser);
                p_c = searcher.Search(query1, null, hits_limit, sort).TotalHits;
                txt = InputText + " AND SearchType:Task" + strUserText;
                query1 = parseQuery(txt, queryParser);
                t_c = searcher.Search(query1, null, hits_limit, sort).TotalHits;
                txt = InputText + " AND SearchType:Attachments" + strUserText;
                query1 = parseQuery(txt, queryParser);
                a_c = searcher.Search(query1, null, hits_limit, sort).TotalHits;
                txt = InputText + " AND SearchType:Assetlibrary" + strUserText;
                query1 = parseQuery(txt, queryParser);
                al_c = searcher.Search(query1, null, hits_limit, sort).TotalHits;
                txt = InputText + " AND SearchType:CmsNavigation" + strUserText;
                query1 = parseQuery(txt, queryParser);
                pg_c = searcher.Search(query1, null, hits_limit, sort).TotalHits;

                analyzer.Close();
                searcher.Dispose();
                _directoryTemp.Dispose();
                // Return results
                return Tuple.Create(p_c, t_c, a_c, al_c, pg_c);
            }
        }


        private static Query parseQuery(string searchQuery, QueryParser parser)
        {
            Query query;
            try
            {
                query = parser.Parse(searchQuery.Trim());
            }
            catch (ParseException)
            {
                query = parser.Parse(QueryParser.Escape(searchQuery.Trim()));
            }
            return query;
        }


        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
    (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
        private static IList<SampleData> _mapLuceneToDataList(IEnumerable<ScoreDoc> hits, IndexSearcher searcher, string keyword = "")
        {
            // v 2.9.4: use 'hit.doc'
            // v 3.0.3: use 'hit.Doc'
            //return hits.Select(hit => _mapLuceneDocumentToData(searcher.Doc(hit.Doc), keyword)).Where(a => a.Name != "").DistinctBy(a => a.Name.ToLower()).Take(5).ToList();
            return extrackResultfromIndex(hits, searcher, keyword).ToList().Where(a => a.Name != "").DistinctBy(a => a.Name.ToLower()).Take(5).ToList();
        }

        private static IList<SampleData> extrackResultfromIndex(IEnumerable<ScoreDoc> hits, IndexSearcher searcher, string keyword = "")
        {

            IList<SampleData> res = new List<SampleData>();
            try
            {

                List<SearchAttributeDetails> isrchAttr = new List<SearchAttributeDetails>();
                SearchAttributeDetails srchAttr = new SearchAttributeDetails(); string _matchVal = "", attrsVals = "";

                List<SearchAttributeDetails> attrs = new List<SearchAttributeDetails>();

                foreach (var hit in hits)
                {
                    Document doc = searcher.Doc(hit.Doc);
                    if (doc.Get("Name") != null) if (doc.Get("Name").ToLower().StartsWith(keyword)) res.Add(new SampleData { Name = EncryptString(doc.Get("Name")).ToLower() });
                    if (doc.Get("ID") != null) if (doc.Get("ID").ToLower().StartsWith(keyword)) res.Add(new SampleData { Name = EncryptString(doc.Get("ID")).ToLower() });
                    if (doc.Get("Description") != null) if (doc.Get("Description").ToLower().StartsWith(keyword)) res.Add(new SampleData { Name = EncryptString(doc.Get("Description")).ToLower() });
                    if (doc.Get("EntitytypeTypeCaption") != null) if (doc.Get("EntitytypeTypeCaption").ToLower().StartsWith(keyword)) res.Add(new SampleData { Name = EncryptString(doc.Get("EntitytypeTypeCaption")).ToLower() });
                    if (doc.Get("EntitytypeShortDescription") != null) if (doc.Get("EntitytypeShortDescription").ToLower().StartsWith(keyword)) res.Add(new SampleData { Name = EncryptString(doc.Get("EntitytypeShortDescription")).ToLower() });

                    attrsVals = doc.Get("AttributeDetails");
                    if (attrsVals != null && attrsVals.Length > 0)
                    {
                        srchAttr = new SearchAttributeDetails();
                        XmlDocument xdoc = new XmlDocument();
                        xdoc.LoadXml(attrsVals);
                        XmlNodeList attributes = xdoc.DocumentElement.SelectNodes("//p");
                        foreach (XmlNode node in attributes)
                        {
                            srchAttr = new SearchAttributeDetails();
                            srchAttr.AttributeCaption = node.Attributes["l"].Value; srchAttr.AttributeId = node.Attributes["id"].Value;
                            _matchVal = doc.Get("AttributeValue_" + srchAttr.AttributeCaption.Replace(" ", "__"));
                            if (_matchVal != null)
                                if (_matchVal.ToLower().StartsWith(keyword))
                                {
                                    res.Add(new SampleData { Name = _matchVal.ToLower() });
                                }
                        }

                    }
                }

                return res;
            }
            catch (Exception ex)
            {
                return res;
            }
        }

        private static SampleData _mapLuceneDocumentToData(Document doc, string keyword = "")
        {

            return new SampleData
            {
                //Id = Convert.ToInt32(doc.Get("ID")),
                Name = EncryptString(doc.Get("Name")).ToLower(),
                //Description = doc.Get("Description")
            };

        }

        // add/update/clear search index data 
        public static void AddUpdateLuceneIndex(SearchEntity sampleData, string tenantPath = "")
        {
            AddUpdateLuceneIndex(new List<SearchEntity> { sampleData }, tenantPath);
        }
        public static bool AddUpdateLuceneIndex(IEnumerable<SearchEntity> sampleDatas, string tenanthost)
        {
            bool serviceresponse = false;
            try
            {

                JObject jobj = new JObject();
                jobj["documents"] = JsonConvert.SerializeObject(sampleDatas);
                jobj["tenanthost"] = tenanthost;

                //calling lucene application
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["SearchEngineProvider"]);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Add("X-AuthorizationToken", Guid.NewGuid().ToString());
                        var response = client.PostAsJsonAsync("api/Values/AddUpdateLuceneIndex", jobj).Result;
                        string jsonObStr = response.Content.ReadAsStringAsync().Result;

                        JObject returnValue = (JObject)JsonConvert.DeserializeObject(jsonObStr);

                        serviceresponse = (bool)returnValue["Response"];
                    }
                }
                catch
                {
                    serviceresponse = false;
                }

                serviceresponse = true;
            }
            catch (LockObtainFailedException ex)
            {
                serviceresponse = false;
            }
            finally
            {

            }
            return serviceresponse;
        }
        public static void ClearAssetLuceneIndexRecord(int[] record_ids, string tenantPath)
        {
            try
            {

                JObject jobj = new JObject();
                jobj["record_ids"] = JsonConvert.SerializeObject(record_ids);
                jobj["tenanthost"] = tenantPath;

                //calling lucene application
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["SearchEngineProvider"]);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Add("X-AuthorizationToken", Guid.NewGuid().ToString());
                        var response = client.PostAsJsonAsync("api/Values/ClearAssetLuceneIndexRecord", jobj).Result;
                        string jsonObStr = response.Content.ReadAsStringAsync().Result;

                    }
                }
                catch
                {

                }


            }
            catch (LockObtainFailedException ex)
            {

            }
            finally
            {

            }
        }

        public static void ClearProductionLuceneIndexRecord(int record_id, string tenantPath = "")
        {

            try
            {

                JObject jobj = new JObject();
                jobj["docid"] = record_id;
                jobj["tenanthost"] = tenantPath;

                //calling lucene application
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["SearchEngineProvider"]);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Add("X-AuthorizationToken", Guid.NewGuid().ToString());
                        var response = client.PostAsJsonAsync("api/Values/ClearProductionLuceneIndexRecord", jobj).Result;
                        string jsonObStr = response.Content.ReadAsStringAsync().Result;

                    }
                }
                catch
                {

                }


            }
            catch (LockObtainFailedException ex)
            {

            }
            finally
            {

            }


        }

        public static bool ClearLuceneIndex(string tenantpath = "")
        {
            bool serviceresponse = false;
            try
            {

                JObject jobj = new JObject();
                jobj["tenanthost"] = tenantpath;

                //calling lucene application
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["SearchEngineProvider"]);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Add("X-AuthorizationToken", Guid.NewGuid().ToString());
                        var response = client.PostAsJsonAsync("api/Values/ClearLuceneIndex", jobj).Result;
                        string jsonObStr = response.Content.ReadAsStringAsync().Result;

                    }

                    serviceresponse = true;
                }
                catch
                {
                    serviceresponse = false;
                }

                serviceresponse = true;
            }
            catch (LockObtainFailedException ex)
            {
                serviceresponse = false;
            }
            finally
            {
            }
            return serviceresponse;
        }
        public static void Optimize(string tenantPath)
        {
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            using (var writer = new IndexWriter(_directoryPath(tenantPath), analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                analyzer.Close();
                writer.Optimize();
                writer.Dispose();
            }
            _directoryTemp.Dispose();
        }

        public static Sort GetSort(bool asc = false)
        {


            var fields = new[]
                {
                    new SortField("Name", SortField.STRING,asc), new SortField("Searchcategory", SortField.STRING,asc)

                };

            return new Sort(fields); // sort by name, then by caps 
        }

        public static Sort GetSort(string fieldname, bool desc = false)
        {


            var fields = new[]
                {
                    new  SortField(fieldname, SortField.LONG, desc)
                };

            return new Sort(fields); // sort by fieldname, then by caps 
        }

        public static string DecryptString(string fieldValue)
        {
            if (fieldValue != null)
            {

                fieldValue = HttpUtility.HtmlDecode(fieldValue);

                fieldValue = fieldValue.Replace("+", "LUCENEUIDELPlus");
                fieldValue = fieldValue.Replace("&&", "LUCENEUIDELandand");
                fieldValue = fieldValue.Replace("||", "LUCENEUIDELoror");
                fieldValue = fieldValue.Replace("!", "LUCENEUIDEL3not");
                fieldValue = fieldValue.Replace("(", "LUCENEUIDEL4OBR");
                fieldValue = fieldValue.Replace(")", "LUCENEUIDEL5CBR");
                fieldValue = fieldValue.Replace("{", "LUCENEUIDEL6OCB");
                fieldValue = fieldValue.Replace("}", "LUCENEUIDEL7CCB");
                fieldValue = fieldValue.Replace("[", "LUCENEUIDEL8OAB");
                fieldValue = fieldValue.Replace("]", "LUCENEUIDEL9CAB");
                fieldValue = fieldValue.Replace("^", "LUCENEUIDEL10OPS");
                fieldValue = fieldValue.Replace("~", "LUCENEUIDEL11DEL");
                fieldValue = fieldValue.Replace("*", "LUCENEUIDEL12STAR");
                fieldValue = fieldValue.Replace("?", "LUCENEUIDEL13QUES");
                fieldValue = fieldValue.Replace(":", "LUCENEUIDEL14COL");
                fieldValue = fieldValue.Replace("\\", "LUCENEUIDEL15DBLSLSH");
                fieldValue = fieldValue.Replace("\"", "LUCENEUIDEL16SGLSLSH");
                fieldValue = fieldValue.Replace("'", "LUCENEUIDEL17SQ");
                fieldValue = fieldValue.Replace("-", "LUCENEUIDEL18HIPHEN");
                fieldValue = fieldValue.Replace("#", "LUCENEUIDEL19HASH");
                fieldValue = fieldValue.Replace("@", "LUCENEUIDEL20AT");
                fieldValue = fieldValue.Replace("_", "LUCENEUIDEL21UNDER");
                fieldValue = fieldValue.Replace("&", "LUCENEUIDEL22SAND");
                fieldValue = fieldValue.Replace("|", "LUCENEUIDEL23SOR");
                fieldValue = fieldValue.Replace("/", "LUCENEUIDEL24SSLSH");
                fieldValue = fieldValue.Replace(",", "LUCENEUIDEL25COMMA");
                fieldValue = fieldValue.Replace(";", "LUCENEUIDEL26SEMICOLN");
                fieldValue = fieldValue.Replace("=", "LUCENEUIDEL27EQUAL");
                fieldValue = fieldValue.Replace("<", "LUCENEUIDEL28LESS");
                fieldValue = fieldValue.Replace(">", "LUCENEUIDEL29GREATER");
                fieldValue = fieldValue.Replace("%", "LUCENEUIDEL30PERC");
                fieldValue = fieldValue.Replace("$", "LUCENEUIDEL31DOLR");
                fieldValue = fieldValue.Replace("`", "LUCENEUIDEL32SPCLSLSH");
                fieldValue = fieldValue.Replace(" ", "LUCENEUIDEL33SPACE");

                return fieldValue;

            }
            else
            {
                return "";
            }
        }

        public static string DecryptStringPattern(string fieldValue)
        {
            if (fieldValue != null)
            {

                fieldValue = HttpUtility.HtmlDecode(fieldValue);

                fieldValue = fieldValue.Replace("+", "LUCENEUIDELPlus");
                fieldValue = fieldValue.Replace("&&", "LUCENEUIDELandand");
                fieldValue = fieldValue.Replace("||", "LUCENEUIDELoror");
                fieldValue = fieldValue.Replace("!", "LUCENEUIDEL3not");
                fieldValue = fieldValue.Replace("(", "LUCENEUIDEL4OBR");
                fieldValue = fieldValue.Replace(")", "LUCENEUIDEL5CBR");
                fieldValue = fieldValue.Replace("{", "LUCENEUIDEL6OCB");
                fieldValue = fieldValue.Replace("}", "LUCENEUIDEL7CCB");
                fieldValue = fieldValue.Replace("[", "LUCENEUIDEL8OAB");
                fieldValue = fieldValue.Replace("]", "LUCENEUIDEL9CAB");
                fieldValue = fieldValue.Replace("^", "LUCENEUIDEL10OPS");
                fieldValue = fieldValue.Replace("~", "LUCENEUIDEL11DEL");
                fieldValue = fieldValue.Replace("*", "LUCENEUIDEL12STAR");
                fieldValue = fieldValue.Replace("?", "LUCENEUIDEL13QUES");
                fieldValue = fieldValue.Replace(":", "LUCENEUIDEL14COL");
                fieldValue = fieldValue.Replace("\\", "LUCENEUIDEL15DBLSLSH");
                fieldValue = fieldValue.Replace("\"", "LUCENEUIDEL16SGLSLSH");
                fieldValue = fieldValue.Replace("'", "LUCENEUIDEL17SQ");
                fieldValue = fieldValue.Replace("-", "LUCENEUIDEL18HIPHEN");
                fieldValue = fieldValue.Replace("#", "LUCENEUIDEL19HASH");
                fieldValue = fieldValue.Replace("@", "LUCENEUIDEL20AT");
                fieldValue = fieldValue.Replace("_", "LUCENEUIDEL21UNDER");
                fieldValue = fieldValue.Replace("&", "LUCENEUIDEL22SAND");
                fieldValue = fieldValue.Replace("|", "LUCENEUIDEL23SOR");
                fieldValue = fieldValue.Replace("/", "LUCENEUIDEL24SSLSH");
                fieldValue = fieldValue.Replace(",", "LUCENEUIDEL25COMMA");
                fieldValue = fieldValue.Replace(";", "LUCENEUIDEL26SEMICOLN");
                fieldValue = fieldValue.Replace("=", "LUCENEUIDEL27EQUAL");
                fieldValue = fieldValue.Replace("<", "LUCENEUIDEL28LESS");
                fieldValue = fieldValue.Replace(">", "LUCENEUIDEL29GREATER");
                fieldValue = fieldValue.Replace("%", "LUCENEUIDEL30PERC");
                fieldValue = fieldValue.Replace("$", "LUCENEUIDEL31DOLR");
                fieldValue = fieldValue.Replace("`", "LUCENEUIDEL32SPCLSLSH");

                return fieldValue;

            }
            else
            {
                return "";
            }
        }

        public static string EncryptString(string fieldValue)
        {
            if (fieldValue != null)
            {
                fieldValue = fieldValue.Replace("LUCENEUIDELPlus", "+");
                fieldValue = fieldValue.Replace("LUCENEUIDELandand", "&&");
                fieldValue = fieldValue.Replace("LUCENEUIDELoror", "||");
                fieldValue = fieldValue.Replace("LUCENEUIDEL3not", "!");
                fieldValue = fieldValue.Replace("LUCENEUIDEL4OBR", "(");
                fieldValue = fieldValue.Replace("LUCENEUIDEL5CBR", ")");
                fieldValue = fieldValue.Replace("LUCENEUIDEL6OCB", "{");
                fieldValue = fieldValue.Replace("LUCENEUIDEL7CCB", "}");
                fieldValue = fieldValue.Replace("LUCENEUIDEL8OAB", "[");
                fieldValue = fieldValue.Replace("LUCENEUIDEL9CAB", "]");
                fieldValue = fieldValue.Replace("LUCENEUIDEL10OPS", "^");
                fieldValue = fieldValue.Replace("LUCENEUIDEL11DEL", "~");
                fieldValue = fieldValue.Replace("LUCENEUIDEL12STAR", "*");
                fieldValue = fieldValue.Replace("LUCENEUIDEL13QUES", "?");
                fieldValue = fieldValue.Replace("LUCENEUIDEL14COL", ":");
                fieldValue = fieldValue.Replace("LUCENEUIDEL15DBLSLSH", "\\");
                fieldValue = fieldValue.Replace("LUCENEUIDEL16SGLSLSH", "\"");
                fieldValue = fieldValue.Replace("LUCENEUIDEL17SQ", "'");
                fieldValue = fieldValue.Replace("LUCENEUIDEL18HIPHEN", "-");
                fieldValue = fieldValue.Replace("LUCENEUIDEL19HASH", "#");
                fieldValue = fieldValue.Replace("LUCENEUIDEL20AT", "@");
                fieldValue = fieldValue.Replace("LUCENEUIDEL21UNDER", "_");
                fieldValue = fieldValue.Replace("LUCENEUIDEL22SAND", "&");
                fieldValue = fieldValue.Replace("LUCENEUIDEL23SOR", "|");
                fieldValue = fieldValue.Replace("LUCENEUIDEL24SSLSH", "/");
                fieldValue = fieldValue.Replace("LUCENEUIDEL25COMMA", ",");
                fieldValue = fieldValue.Replace("LUCENEUIDEL26SEMICOLN", ";");
                fieldValue = fieldValue.Replace("LUCENEUIDEL27EQUAL", "=");
                fieldValue = fieldValue.Replace("LUCENEUIDEL28LESS", "<");
                fieldValue = fieldValue.Replace("LUCENEUIDEL29GREATER", ">");
                fieldValue = fieldValue.Replace("LUCENEUIDEL30PERC", "%");
                fieldValue = fieldValue.Replace("LUCENEUIDEL31DOLR", "$");
                fieldValue = fieldValue.Replace("LUCENEUIDEL32SPCLSLSH", "`");
                fieldValue = fieldValue.Replace("LUCENEUIDEL33SPACE", " ");

                return fieldValue;
            }
            else return "";
        }

        public static List<string> fillVariousDateformat(string labelval, DateTime dateval)
        {
            List<string> Metadata = new List<string>();

            try
            {
                Metadata.Add(dateval.ToString("yyyy-MM-dd"));
                Metadata.Add(dateval.ToString("MM-yyyy-dd"));
                Metadata.Add(dateval.ToString("dd-MM-yyyy"));

                Metadata.Add(dateval.ToString("yyyy/MM/dd"));
                Metadata.Add(dateval.ToString("MM/yyyy/dd"));
                Metadata.Add(dateval.ToString("dd/MM/yyyy"));

                Metadata.Add(dateval.ToString("d-m-yyyy"));
                Metadata.Add(dateval.ToString("m-d-yyyy"));
                Metadata.Add(dateval.ToString("yyyy-m-d"));

                Metadata.Add(dateval.ToString("d/m/yyyy"));
                Metadata.Add(dateval.ToString("m/d/yyyy"));
                Metadata.Add(dateval.ToString("yyyy/m/d"));

                Metadata.Add(dateval.ToString("yyyy"));
                Metadata.Add(dateval.ToString("MM"));
                Metadata.Add(dateval.ToString("dd"));

                return Metadata;
            }
            catch (Exception ex)
            {
                return Metadata;
            }

        }


        public static int? GetValueOf<T>(string EnumConst) where T : struct
        {
            int? result = null;

            T temp = default(T);
            if (Enum.TryParse<T>(EnumConst, out temp))
            {
                result = Convert.ToInt32(temp);
            }

            return result;
        }

        private static void _addToLuceneIndex(SearchEntity obj, IndexWriter writer, string tenantpath = "")
        {

            try
            {



                // remove older index entry
                if (obj.SearchType == "Attachments" || obj.SearchType == "Assetlibrary")
                {


                    //BooleanQuery booleanQuery = new BooleanQuery();
                    //Query query1 = new TermQuery(new Term("AssetId", obj.ID.ToString()));
                    //Query query2 = new TermQuery(new Term("client", "ABC"));
                    //booleanQuery.Add(query1, Occur.SHOULD);
                    //booleanQuery.Add(query2, Occur.SHOULD);

                    var searchQuery = new TermQuery(new Term("AssetId", obj.ID.ToString()));
                    writer.DeleteDocuments(searchQuery);
                }
                else
                {
                    var searchQuery = new TermQuery(new Term("ID", obj.ID.ToString()));
                    writer.DeleteDocuments(searchQuery);
                }


                // add new index entry
                var doc = new Document();

                StringBuilder SearchText = new StringBuilder();

                // add lucene fields mapped to db fields
                if (obj.SearchType == "Attachments" || obj.SearchType == "Assetlibrary")
                {
                    doc.Add(new Field("AssetId", obj.ID.ToString(), Field.Store.YES, Field.Index.ANALYZED));
                    SearchText.Append(DecryptStringPattern(ngGram.GenerateNGramsPattern(obj.ID.ToString(), obj.ID.Length)) + " ");
                }
                else
                {
                    doc.Add(new Field("ID", obj.ID.ToString(), Field.Store.YES, Field.Index.ANALYZED));
                    SearchText.Append(DecryptString(ngGram.GenerateNGramsPattern(obj.ID.ToString(), obj.ID.Length)) + " ");
                }
                doc.Add(new Field("TypeID", obj.TypeID.ToString(), Field.Store.YES, Field.Index.ANALYZED));
                doc.Add(new Field("TypeCategoryID", obj.TypeCategoryID.ToString(), Field.Store.YES, Field.Index.NO));
                doc.Add(new Field("Name", DecryptString(obj.Name), Field.Store.YES, Field.Index.ANALYZED));
                SearchText.Append(DecryptStringPattern(ngGram.GenerateNGramsPattern(obj.Name.ToString(), obj.Name.Length)) + " ");
                SearchText.Append(DecryptStringPattern(ngGram.GenerateNGramsPattern(obj.TypeID.ToString(), obj.Name.Length)) + " ");
                doc.Add(new Field("PathInfo", obj.PathInfo == null ? "" : obj.PathInfo, Field.Store.YES, Field.Index.NOT_ANALYZED));
                doc.Add(new Field("EntitytypeDetails", obj.EntitytypeDetails == null ? "" : obj.EntitytypeDetails, Field.Store.YES, Field.Index.NOT_ANALYZED));
                doc.Add(new Field("AttributesDetails", obj.AttributesDetails == null ? "" : obj.AttributesDetails, Field.Store.YES, Field.Index.NOT_ANALYZED));
                doc.Add(new Field("AssetDetails", obj.AssetDetails == null ? "" : obj.AssetDetails, Field.Store.YES, Field.Index.NOT_ANALYZED));
                doc.Add(new Field("AssetValueDetails", obj.AssetValueDetails == null ? "" : obj.AssetValueDetails, Field.Store.YES, Field.Index.NOT_ANALYZED));


                doc.Add(new Field("EntitytypeTypeCaption", DecryptStringPattern(obj.TypeCaption != null ? obj.TypeCaption : ""), Field.Store.YES, Field.Index.ANALYZED));
                doc.Add(new Field("EntitytypeShortDescription", DecryptStringPattern(obj.TypeShortDescription != null ? obj.TypeShortDescription : ""), Field.Store.YES, Field.Index.ANALYZED));


                SearchText.Append(DecryptString(obj.ID.ToString()) + " ");
                SearchText.Append(DecryptString(obj.Name) + " ");

                SearchText.Append(DecryptStringPattern(obj.TypeCaption != null ? obj.TypeCaption : "") + " ");
                SearchText.Append(DecryptStringPattern(obj.TypeShortDescription != null ? obj.TypeShortDescription : "") + " ");


                if (obj.FileName != null && obj.FileName != "")
                    SearchText.Append(DecryptString(obj.FileName) + " ");

                doc.Add(new Field("Description", obj.Description != null ? obj.Description : "", Field.Store.YES, Field.Index.ANALYZED));


                if (obj.Description != null && obj.Description != "")
                {
                    SearchText.Append(DecryptString(obj.Description) + " ");
                    SearchText.Append(DecryptStringPattern(ngGram.GenerateNGramsPattern(obj.Description != null ? obj.Description : "", obj.Description.Length)) + " ");
                }


                doc.Add(new Field("SearchType", obj.SearchType == null ? "" : obj.SearchType, Field.Store.YES, Field.Index.ANALYZED));
                doc.Add(new Field("UniqueKey", obj.UniqueKey == null ? "" : obj.UniqueKey, Field.Store.YES, Field.Index.ANALYZED));
                doc.Add(new Field("TagWord_List", obj.TagWord == null ? "" : string.Join("|", obj.TagWord), Field.Store.YES, Field.Index.NO));
                foreach (string tag in obj.TagWord)
                {
                    doc.Add(new Field("TagWord", tag.ToString(), Field.Store.YES, Field.Index.ANALYZED));
                    SearchText.Append(DecryptString(tag.ToString()) + " ");
                    SearchText.Append(DecryptStringPattern(ngGram.GenerateNGramsPattern(tag.ToString(), tag.Length)) + " ");
                }

                foreach (var date in obj.SearchDateMetadata)
                {
                    doc.Add(new Field("Date", date.ToString(), Field.Store.YES, Field.Index.ANALYZED));
                    SearchText.Append(DecryptString(date.ToString()) + " ");
                    SearchText.Append(DecryptStringPattern(ngGram.GenerateNGramsPattern(date.ToString(), date.Length)) + " ");
                }

                doc.Add(new Field("AttributeDetails", obj.AttributesDetails == null ? "" : obj.AttributesDetails, Field.Store.YES, Field.Index.NO));
                doc.Add(new Field("UserID_List", obj.UserIDs == null ? "" : String.Join("|", obj.UserIDs), Field.Store.YES, Field.Index.NO));

                doc.Add(new Field("Date", obj.DocumentDate == null ? DateTime.Now.Ticks.ToString() : obj.DocumentDate.Ticks.ToString(), Field.Store.YES, Field.Index.ANALYZED));

                foreach (int User in obj.UserIDs)
                {
                    doc.Add(new Field("UserID", User.ToString(), Field.Store.YES, Field.Index.ANALYZED));
                }
                Dictionary<string, string> tempMetadata = new Dictionary<string, string>();

                foreach (KeyValuePair<string, string> Item in obj.Metadata)
                {
                    var isexists = tempMetadata.Where(a => a.Key == Item.Key).Count();
                    if (isexists == 0)
                    {
                        var values = obj.Metadata.Where(a => a.Key == Item.Key).Select(a => a.Value == null ? "" : a.Value).ToArray();
                        tempMetadata.Add(Item.Key, String.Join(", ", values));
                        doc.Add(new Field("AttributeValue_" + Item.Key.Replace(" ", "__"), values == null ? "" : String.Join(", ", values), Field.Store.YES, Field.Index.NO));
                    }
                }
                foreach (KeyValuePair<string, string> Item in obj.Metadata)
                {
                    doc.Add(new Field("Metadata_" + Item.Key.Replace(" ", "__"), Item.Value, Field.Store.YES, Field.Index.ANALYZED));
                    SearchText.Append(DecryptString(Item.Value) + " ");
                    SearchText.Append(DecryptStringPattern(ngGram.GenerateNGramsPattern(Item.Value.ToString(), Item.Value.Length)) + " ");
                }

                doc.Add(new Field("SearchText", SearchText.ToString(), Field.Store.NO, Field.Index.ANALYZED));

                // add entry to index
                writer.AddDocument(doc);
            }



            catch (LockObtainFailedException ex)
            {
                var lockFilePath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + tenantpath + "SearchIndexFiles\\LuceneIndex" + "\\Index", "write.lock");
                DirectoryInfo indexDirInfo = new DirectoryInfo(ConfigurationManager.AppSettings["MarcomPresentation"] + tenantpath + "SearchIndexFiles\\LuceneIndex" + "\\Index");
                FSDirectory indexFSDir = FSDirectory.Open(indexDirInfo, new Lucene.Net.Store.SimpleFSLockFactory(indexDirInfo));
                IndexWriter.Unlock(indexFSDir);
                if (File.Exists(lockFilePath))
                    File.Delete(lockFilePath);
            }

            catch (Exception ex)
            {

            }


        }


        private static void _addtagToLuceneIndex(SearchEntity obj, IndexWriter writer, string tenantPath)
        {

            try
            {
                var searchQuery = new TermQuery(new Term("TagId", obj.ID.ToString()));
                writer.DeleteDocuments(searchQuery);
                // add new index entry
                var doc = new Document();
                StringBuilder SearchText = new StringBuilder();
                // add lucene fields mapped to db fields
                doc.Add(new Field("TagId", obj.ID.ToString(), Field.Store.YES, Field.Index.ANALYZED));
                doc.Add(new Field("Name", DecryptString(obj.Name.ToString()), Field.Store.YES, Field.Index.ANALYZED));
                SearchText.Append(DecryptStringPattern(ngGram.GenerateNGramsPattern(obj.Name.ToString(), obj.Name.Length)) + " ");
                doc.Add(new Field("Description", obj.Description.ToString(), Field.Store.YES, Field.Index.NO));
                doc.Add(new Field("SearchType", obj.SearchType == null ? "" : obj.SearchType, Field.Store.YES, Field.Index.ANALYZED));

                doc.Add(new Field("Date", DateTime.Now.Ticks.ToString(), Field.Store.YES, Field.Index.ANALYZED));

                doc.Add(new Field("SearchText", SearchText.ToString(), Field.Store.NO, Field.Index.ANALYZED));
                // add entry to index
                writer.AddDocument(doc);
            }

            catch (LockObtainFailedException ex)
            {
                var lockFilePath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + tenantPath + "SearchIndexFiles\\LuceneIndex" + "\\Index", "write.lock");
                DirectoryInfo indexDirInfo = new DirectoryInfo(ConfigurationManager.AppSettings["MarcomPresentation"] + tenantPath + "SearchIndexFiles\\LuceneIndex" + "\\Index");
                FSDirectory indexFSDir = FSDirectory.Open(indexDirInfo, new Lucene.Net.Store.SimpleFSLockFactory(indexDirInfo));
                IndexWriter.Unlock(indexFSDir);
                if (File.Exists(lockFilePath))
                    File.Delete(lockFilePath);
            }

        }


    }
}