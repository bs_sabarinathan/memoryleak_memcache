﻿tinymce.PluginManager.add("base64_image", function (a, b) {
    a.addButton("base64_image", {
        icon: "image",
        tooltip: "Upload image",
        onclick: function () {
            a.windowManager.open({
                title: "Insert image",
                url: b + "/dialog.html",
                width: 500,
                height: 150,
                buttons: [{
                    text: "Ok",
                    classes: 'widget btn primary first abs-layout-item',
                    onclick: function () {
                        var b = a.windowManager.getWindows()[0];

                        if (b.getContentWindow().document.getElementById('content').value == '') {
                            $.toast({ text: 'Please select a file', showHideTransition: 'slide', heading: 'Error', icon: 'Error', allowToastClose: false, hideAfter: 3000, stack: 5, textAlign: 'left', position: 'top-right' });
                            return false;
                        }

                        if (b.getContentWindow().document.getElementById('content').files[0].size > 1000 * (1024 * 2)) {
                            $.toast({ text: 'Max image file size is 1MB', showHideTransition: 'slide', heading: 'Error', icon: 'Error', allowToastClose: false, hideAfter: 3000, stack: 5, textAlign: 'left', position: 'top-right' });
                            return false;
                        }

                        if (b.getContentWindow().document.getElementById('content').files[0].type != "image/jpeg" && b.getContentWindow().document.getElementById('content').files[0].type != "image/jpg" &&
                            b.getContentWindow().document.getElementById('content').files[0].type != "image/png" && b.getContentWindow().document.getElementById('content').files[0].type != "image/gif") {

                            $.toast({ text: 'Only image file format can be uploaded', showHideTransition: 'slide', heading: 'Error', icon: 'Error', allowToastClose: false, hideAfter: 3000, stack: 5, textAlign: 'left', position: 'top-right' });
                            return false;
                        }

                        var data;

                        data = new FormData();
                        data.append('file', b.getContentWindow().document.getElementById('content').files[0]);

                        $.ajax({
                            url: 'api/image/convert-base64',
                            data: data,
                            processData: false,
                            contentType: false,
                            async: false,
                            type: 'POST',
                        }).done(function (msg) {
                            var imageAlt = b.getContentWindow().document.getElementById('desc').value;
                            //var imageSrc = "data:" + b.getContentWindow().document.getElementById('content').files[0].type + ";base64," + msg;

                            var imageSrc = msg;

                            var imageTag = '<img src="' + imageSrc + '" alt="' + imageAlt + '" style="max-width: 100%;" />';

                            a.insertContent(imageTag), b.close()

                        }).fail(function (jqXHR, textStatus) {

                            $.toast({ text: "Request failed: " + jqXH.responseText + " --- " + RtextStatus, showHideTransition: 'slide', heading: 'Error', icon: 'Error', allowToastClose: false, hideAfter: 3000, stack: 5, textAlign: 'left', position: 'top-right' });
                        });
                    }
                }, {
                    text: "Close",
                    onclick: "close"
                }]
            })
        }
    }),

    a.addMenuItem("base64_image", {
        icon: "image",
        text: "Insert image",
        context: "insert",
        prependToContext: !0,
        onclick: function () {
            a.windowManager.open({
                title: "Insert image",
                url: b + "api/image/convert-base64",
                width: 500,
                height: 150,
                buttons: [{
                    text: "Ok",
                    classes: 'widget btn primary first abs-layout-item',
                    onclick: function () {
                        var b = a.windowManager.getWindows()[0];

                        if (b.getContentWindow().document.getElementById('content').value == '') {

                            $.toast({ text: 'Please select a file', showHideTransition: 'slide', heading: 'Error', icon: 'Error', allowToastClose: false, hideAfter: 3000, stack: 5, textAlign: 'left', position: 'top-right' });
                            return false;
                        }

                        if (b.getContentWindow().document.getElementById('content').files[0].size > 1000 * (1024 * 2)) {
                            $.toast({ text: 'Max image file size is 2MB', showHideTransition: 'slide', heading: 'Error', icon: 'Error', allowToastClose: false, hideAfter: 3000, stack: 5, textAlign: 'left', position: 'top-right' });
                            return false;
                        }

                        if (b.getContentWindow().document.getElementById('content').files[0].type != "image/jpeg" && b.getContentWindow().document.getElementById('content').files[0].type != "image/jpg" &&
                            b.getContentWindow().document.getElementById('content').files[0].type != "image/png" && b.getContentWindow().document.getElementById('content').files[0].type != "image/gif") {
                            $.toast({ text: 'Only image file format can be uploaded', showHideTransition: 'slide', heading: 'Error', icon: 'Error', allowToastClose: false, hideAfter: 3000, stack: 5, textAlign: 'left', position: 'top-right' });
                            return false;
                        }

                        var data;

                        data = new FormData();
                        data.append('file', b.getContentWindow().document.getElementById('content').files[0]);

                        $.ajax({
                            url: 'api/image/convert-base64',
                            data: data,
                            processData: false,
                            contentType: false,
                            async: false,
                            type: 'POST',
                        }).done(function (msg) {
                            var imageAlt = b.getContentWindow().document.getElementById('desc').value;
                            //var imageSrc = "data:" + b.getContentWindow().document.getElementById('content').files[0].type + ";base64," + msg;

                            var imageSrc = msg;

                            var imageTag = '<img src="' + imageSrc + '" alt="' + imageAlt + '" style="max-width: 100%;" />';

                            a.insertContent(imageTag), b.close()

                        }).fail(function (jqXHR, textStatus) {
                            $.toast({ text: "Request failed: " + jqXH.responseText + " --- " + RtextStatus, showHideTransition: 'slide', heading: 'Error', icon: 'Error', allowToastClose: false, hideAfter: 3000, stack: 5, textAlign: 'left', position: 'top-right' });
                        });
                    }
                }, {
                    text: "Close",
                    onclick: "close"
                }]
            })
        }
    })

});