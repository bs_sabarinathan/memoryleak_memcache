﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Common.Model
{
    public partial class FinancialViewListDao : BaseDao
    {
        public virtual int Id { get; set; }
        public virtual string Caption { get; set; }
        public virtual bool IsDefault { get; set; }
        public virtual bool IsSelectable { get; set; }
        public virtual string Template { get; set; }

    }
}
