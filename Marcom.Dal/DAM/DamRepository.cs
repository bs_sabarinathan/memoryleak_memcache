﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Dal.Base;
using NHibernate;
using System.IO;
using System.Xml.Linq;
using System.Web;
using BrandSystems.Marcom.Dal.Metadata.Model;
using System.Collections;
using System.Xml.Serialization;
using System.Xml;
using NHibernate.Criterion;
using BrandSystems.Marcom.Dal.Common;
using BrandSystems.Marcom.Dal.Common.Model;
using BrandSystems.Marcom.Dal.Access.Model;


namespace BrandSystems.Marcom.Dal.DAM
{
    public class DamRepository : GenericRepository
    {
        public DamRepository(ISessionFactory sessionFactory)
            : base(sessionFactory)
        {
        }
    }
}
