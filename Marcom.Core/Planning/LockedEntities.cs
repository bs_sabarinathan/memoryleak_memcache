﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    class LockedEntities : ILockedEntities
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected int _lockedby;
        protected string _lockedon;

        #endregion

        #region Constructors
        public LockedEntities() { }

        public LockedEntities(int pID,int pEntityID, int pLockedBy, string pLockedOn)
        {
            this._id=pID;
            this._entityid = pEntityID;
            this._lockedby = pLockedBy;
            this._lockedon = pLockedOn;
        }

        #endregion

        #region Public Properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public int EntityID
        {
            get { return _entityid; }
            set { _entityid = value; }

        }
        public int LockedBy
        {
            get { return _lockedby; }
            set { _lockedby = value; }

        }

        public string LockedOn
        {
            get { return _lockedon; }
            set
            {
                _lockedon = value;
            }

        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            AdditionalObjectiveEntityValues castObj = null;
            try
            {
                castObj = (AdditionalObjectiveEntityValues)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}
