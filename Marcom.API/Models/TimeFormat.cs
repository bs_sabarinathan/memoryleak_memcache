﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Marcom.API.Models
{
    public class TimeFormat
    {

        #region "Member Variables"

        private int daysinmonth;
        private int millisecond;
        private int second;
        private int minute;
        private int hour;
        private int date;
        private int day;
        private int week;
        private int month;
        private int year;
        private string zone;


        #endregion

        #region "Members"

        public int DaysInMonth
        {
            get { return daysinmonth; }
            set { daysinmonth = value; }
        }

       

        public int MilliSecond
        {
            get { return millisecond; }
            set { millisecond = value; }
        }

       

        public int Second
        {
            get { return second; }
            set { second = value; }
        }

       

        public int Minute
        {
            get { return minute; }
            set { minute = value; }
        }

        

        public int Hour
        {
            get { return hour; }
            set { hour = value; }
        }

        

        public int Date
        {
            get { return date; }
            set { date = value; }
        }

       

        public int Day
        {
            get { return day; }
            set { day = value; }
        }


        

        public int Week
        {
            get { return week; }
            set { week = value; }
        }

        

        public int Month
        {
            get { return month; }
            set { month = value; }
        }

        

        public int Year
        {
            get { return year; }
            set { year = value; }
        }


        

        public string Zone
        {
            get { return zone; }
            set { zone = value; }
        }

        #endregion

    }
}