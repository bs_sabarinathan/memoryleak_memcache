﻿/// <reference path="mui/planningtool/component/attribute-group-controller.js" />
/// <reference path="../../views/mui/planningtool/component/attributegroup.html" />
(function (ng, app) {

    "use strict";

    //app.controller(
    //	"muiCtrl",
    function mbmuiCtrl($window, $location, $route, $timeout, $scope, $cookies, $resource, requestContext, DamService, CommonService, PlanningService, _, $compile, $translate) {


        // --- Define Controller Methods. ------------------- //
        //new variable to check the ganttlock status
        $scope.GanttLock = true;
        $("#footercopyright").text("Copyright \u00A9 - " + (new Date).getFullYear() + " BrandSystems | ");
        $scope.tempNotificationID = 0;
        $scope.IsOlderMetadataVersion = { IsOlderVersion: false };
        $scope.CurrentMetadataVersionInfo = { ID: 0, Name: '' };
        $scope.Username = $.cookie('Username');
        $cookies['Username'] = $.cookie('Username');
        $scope.Activity = { IsActivitySectionLoad: false };
        $scope.Tgl = {};
        $scope.AssetFilesObj_Temp = { AssetFiles: [] };//This scope is used only for assetedit page
        $scope.LanguageContents = {};
        $scope.financialAttrType = 0;
        $scope.Islock_temp = false;
        $scope.DefaultImageSettings = {
            ImageSpan: new Date().getTime().toString()
        }
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            $scope.logosrc = amazonURL + cloudsetup.BucketName + "/" + TenantFilePath + "logo.png";
        }
        else {
            $scope.logosrc = "assets/img/logo.png";
        }

        var langsettings = 1;
        //var GetLanguageContent = $resource('common/GetLanguageSettings/:LangID', { LangID: langsettings }, { get: { method: 'GET' } });
        //var languagecontent = GetLanguageContent.get({ LangID: langsettings }, function () {
        //CommonService.GetLanguageSettings(langsettings).then(function (languagecontent) {
        // $scope.LanguageContentsChange(JSON.parse(languagecontent.Response.replace(new RegExp("\n", 'g'), "\\n")));
        //});
        $scope.LanguageContentsChange = function (val) {
            $scope.LanguageContents = val;
            $scope.Tgl.ShowAll = $translate.instant('LanguageContents.Res_2.Caption');
        }
        if ($.cookie('StartPage') == 30)
            GlobalIsWorkspaces = true;
        $scope.NewUserImgSrc = "Handlers/UserImage.ashx?id=" + parseInt($cookies['UserId']) + "&time=" + $scope.DefaultImageSettings.ImageSpan; // initiall image (changes made to reflect image change in profile )

        $window.TemplateID = 0;
        $window.ISTemplate = false;
        $window.XeditableDirectivesSource = {};
        $('body').on('mouseover', '[data-over="true"]', function () {
            var attr = $(this).attr('data-id');
            if (typeof attr !== 'undefined' && attr !== false) {
                var id = $(this).attr('data-id');
                var cssStyle = '<style type="text/css" id="MouseOverCss">';
                $('#MouseOverCss').remove();

                cssStyle += "a.mo" + id + "{background-color: #F3F8FB;";
                cssStyle += "background-image: linear-gradient(to bottom, #F7FBFC, #ECF4F9);";
                cssStyle += "background-repeat: repeat-x;";
                cssStyle += "border-left: 4px solid #80D4FF;";
                cssStyle += "text-decoration: none;}";


                cssStyle += "tr.mo" + id + " td{background-color: #F3F8FB;";
                cssStyle += "background-image: linear-gradient(to bottom, #F7FBFC, #ECF4F9);";
                cssStyle += "background-repeat: repeat-x;}";

                $('head').append(cssStyle + "</style>");
            } else {
                var id = $(this).attr('data-uniqueKey');
                var cssStyle = '<style type="text/css" id="MouseOverCss">';
                $('#MouseOverCss').remove();

                cssStyle += "li.mo" + id + " a {background-color: #F3F8FB;";
                cssStyle += "background-image: linear-gradient(to bottom, #F7FBFC, #ECF4F9);";
                cssStyle += "background-repeat: repeat-x;";
                cssStyle += "border-left: 4px solid #80D4FF;";
                cssStyle += "text-decoration: none;}";


                cssStyle += "tr.mo" + id + " td{background-color: #F3F8FB;";
                cssStyle += "background-image: linear-gradient(to bottom, #F7FBFC, #ECF4F9);";
                cssStyle += "background-repeat: repeat-x;}";

                cssStyle += "div.mo" + id + "{background-color: #F3F8FB;";
                cssStyle += "background-image: linear-gradient(to bottom, #F7FBFC, #ECF4F9);";
                cssStyle += "background-repeat: repeat-x;}";

                $('head').append(cssStyle + "</style>");
            }
        });
        $('body').on('mouseout', '[data-over="true"]', function () {
            $('#MouseOverCss').remove();
        });

        $scope.notifyLoad = false;
        $scope.LiveTaskListIDCollection = [];
        $scope.notifications = [];
        $scope.UnreadNotification = '';
        $scope.viewed = '';
        var countofunread = 0;
        GetNotificationFunction();
        function GetNotificationFunction() {
            //var GetNotification = $resource('common/GetNotification/:Flag', { Flag: 0 });
            //var getnotification = GetNotification.get(function () {
            CommonService.GetNotification(0).then(function (getnotification) {
                if (getnotification.StatusCode == 405) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1807.Caption'), function () {
                        $.removeCookie("UserId");
                        $.removeCookie("Username");
                        $.removeCookie("UserImage");
                        $.removeCookie("UserEmail");
                        window.location.replace("login.html" + GetReturnURL());
                    });
                }

                $scope.LiveTaskListIDCollection = []; //Live tasklist update holder

                if (($('#notifyul').is(':visible') == false) && ($('#nonotification').is(':visible') == false)) {
                    if (getnotification.Response.Item1.length != 0) {
                        if ($scope.tempNotificationID != getnotification.Response.Item1[0].Notificationid) {
                            $scope.notifications = [];
                            $scope.UnreadNotification = '';
                            $('#nonotification').css('display', 'none');
                            for (var i = 0; i < getnotification.Response.Item1.length; i++) {
                                $scope.viewed = getnotification.Response.Item1[i].Isviewed;
                                $scope.notifications.push(getnotification.Response.Item1[i]);
                                if (getnotification.Response.Item1[i].Typeid == 4 || getnotification.Response.Item1[i].Typeid == 6)
                                    if ($scope.notifyLoad)
                                        if (getnotification.Response.Item1[i].Isviewed) {
                                            var tasklistList = [];
                                            tasklistList = $.grep($scope.LiveTaskListIDCollection, function (e) { return e.TaskLiStID == getnotification.Response.Item1[i].TaskListID; });
                                            if (tasklistList.length == 0)
                                                $scope.LiveTaskListIDCollection.push({ "TaskLiStID": getnotification.Response.Item1[i].TaskListID, "EntityID": getnotification.Response.Item1[i].TaskEntityID });
                                        }
                            }
                            countofunread = getnotification.Response.Item2;
                            if (countofunread == 0) {
                                $scope.UnreadNotification = '';
                            } else {
                                $scope.UnreadNotification = countofunread;
                            }
                            $scope.tempNotificationID = getnotification.Response.Item1[0].Notificationid;
                        }
                        else if ($scope.tempNotificationID == getnotification.Response.Item1[0].Notificationid) {
                            for (var i = 0; i < getnotification.Response.Item1.length; i++) {
                                $.grep($scope.notifications, function (item, i) {

                                    $timeout(function () {
                                        if (item.Notificationid == getnotification.Response.Item1[i].Notificationid) {
                                            item.NotificationHappendTime = getnotification.Response.Item1[i].NotificationHappendTime;
                                        }
                                    }, 100);


                                })
                            }

                        }
                        if ($scope.LiveTaskListIDCollection.length > 0) {
                            $scope.$broadcast('LiveTaskListUpdate', $scope.LiveTaskListIDCollection);
                        }
                    }
                    else {
                        $scope.LiveTaskListIDCollection = [];
                        $scope.tempNotificationID = 0;
                        $scope.notifications = [];
                        $('#nonotification').css('display', 'block');
                    }
                }



                if (getnotification.Response.Item3.length != 0) {
                    $scope.broadcastmsglist = [];
                    if (getnotification.Response.Item3[0].Messages != null) {
                        for (var i = 0; i < getnotification.Response.Item3.length; i++) {
                            $scope.broadcastmsglist.push({ Messages: getnotification.Response.Item3[i].Messages, Createdate: getnotification.Response.Item3[i].Createdate });
                        }
                        //$scope.broadcastMessage = $scope.broadcastmsg;

                        $("#systemBroadcastMessage").modal('show');
                    }
                    else { $("#systemBroadcastMessage").modal('hide'); }


                }


                $timeout(function () {
                    $scope.notifyLoad = true;
                    GetNotificationFunction();
                }, 30000);

            });

        }


        $('body').on('click', 'i[data-id="UpdateMsgStatus"]', function (event) {
            //alert('dgf');


            //var UpdateBroadcastMessag = $resource('common/updateBroadcastMessagesbyuser');
            //var UpdateBroadcastMessages = UpdateBroadcastMessag.save(function () {
            CommonService.updateBroadcastMessagesbyuser().then(function (UpdateBroadcastMessages) {
                var a = UpdateBroadcastMessages.Response;
                $("#systemBroadcastMessage").modal('hide');
                //$(document).trigger("OnViewedAllNotifications");
            });

        });
        $scope.SetUnreadClass = function (isviewed) {
            if (isviewed == false)
                return "Unread";
            else
                return "read";
        }

        $scope.AdjustNotificationBlockHeight = function () {
            $('#notifyulfooter').css('top', ($('#notifyul').height() + 85) + 'px');
        };
        $scope.attachmenteditpermission = false;
        //var GeGetAttachmentEditFeature = $resource("planning/GetAttachmentEditFeature")
        //var attachementpermission = GeGetAttachmentEditFeature.get(function () {
        PlanningService.GetAttachmentEditFeature().then(function (attachementpermission) {
            $scope.attachmenteditpermission = attachementpermission.Response;
        });

        $scope.NotificationRedirectEntityPath = function (entityid) {
            //var IsValidEntity = $resource("common/IsActiveEntity/:EntityID", { EntityID: entityid });

            //var result = IsValidEntity.get(function () {
            CommonService.IsActiveEntity(entityid).then(function (result) {
                if (result.Response == true) {
                    var entityId = entityid;
                    //var GetCheckUserPermissionForEntity = $resource('common/CheckUserPermissionForEntity/:EntitiyID', { 'EntitiyID': entityid }, { get: { method: 'GET' } });
                    //var CheckUserPermissionForEntity = GetCheckUserPermissionForEntity.get({ 'EntitiyID': entityid }, function () {
                    CommonService.CheckUserPermissionForEntity(entityid).then(function (CheckUserPermissionForEntity) {
                        if (CheckUserPermissionForEntity.Response == true)
                            $location.path('/mui/planningtool/detail/section/' + entityId + '/overview');
                        else
                            bootbox.alert($translate.instant('LanguageContents.Res_1882.Caption'));
                    });
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1866.Caption'));
                }
            });
        }


        $scope.OpenTopNotificationTaskPopUp = function (taskid, entityid, typeid) {

            //var IsValidEntity = $resource("common/IsActiveEntity/:EntityID", { EntityID: taskid });
            //var result = IsValidEntity.get(function () {
            CommonService.IsActiveEntity(taskid).then(function (result) {
                if (result.Response == true) {
                    $("#loadNotificationtask").modal("show");
                    $("#Notificationtaskedit").trigger('NotificationTaskAction', [taskid, typeid, entityid]); //how previously inside this service you got $(this) instance///check with viniston
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1866.Caption'));
                }
            });
        }

        $scope.OpenFundRequestActionfromNotify = function (fundReqID) {
            //var IsValidEntity = $resource("common/IsActiveEntity/:EntityID", { EntityID: fundReqID });

            //var result = IsValidEntity.get(function () {
            CommonService.IsActiveEntity(fundReqID).then(function (result) {
                if (result.Response == true) {
                    $("#feedFundingRequestModal").modal("show");
                    $('#feedFundingRequestModal').trigger("onNewsfeedCostCentreFundingRequestsAction", [fundReqID]);
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1866.Caption'));
                }
            });
        }

        $scope.NotificationRedirectAssetPath = function (Assetid) {

            $scope.$broadcast('openassetpopup', Assetid, true);

        }
        $(window).on("ontaskActionnotification", function (event) {
            $("#loadNotificationtask").modal("hide");
        });

        $scope.IsViewStateUpdate = function (flag) {
            //var UpdateIsViewState = $resource('common/UpdateIsviewedStatusNotification');
            //var updatestae = new UpdateIsViewState();
            var updatestae = {};
            updatestae.UserID = $scope.UserId;
            updatestae.FirstFiveNotifications = "";
            updatestae.Flag = 1;
            // var updateviewednotifications = UpdateIsViewState.save(updatestae, function () {
            CommonService.UpdateIsviewedStatusNotification(updatestae).then(function (updateviewednotifications) {
                $scope.UnreadNotification = '';
            });
        }
        $("#mailicon").focusout(function () {
            $('#mailicon').find('#notifyul').find('li').removeClass("Unread");
            GetNotificationFunction();
        });

        $(document).on("OnUserImageChange", function (event, changedimageURL) {
            $('#headeruserimg').attr('src', "Handlers/UserImage.ashx?id=" + parseInt($cookies['UserId']) + "&time=" + $scope.DefaultImageSettings.ImageSpan);
            $scope.NewUserImgSrc = "Handlers/UserImage.ashx?id=" + parseInt($cookies['UserId']) + "&time=" + $scope.DefaultImageSettings.ImageSpan;
        });

        $(document).on("OnUserNameChange", function (event, changedUserName) {
            $scope.Username = changedUserName;
            $cookies["UserName"] = $scope.Username;
        });

        $(document).on("OnViewedAllNotifications", function (event) {
            GetNotificationFunction();
        });



        $scope.focusTopMenu = function () {
            $('.topmenu').click(function (event) {
                $('#topmenucontainer .selected').removeClass('selected');
                $(this).parent().addClass('selected');
            });

            $('.topsubmenu').click(function (event) {
                $('#topmenucontainer .active').removeClass('active');
                $('#topmenucontainer .selected').removeClass('selected');
                $(this).parent().parent().parent().addClass('selected');
                $(this).parent().addClass('active');
            });
        }

        $scope.searchresponse = [];
        $scope.myData = [];

        $scope.LoadSearch = function () {
            //$('#searchlist').click(function (event) {
            //    event.stopImmediatePropagation();
            //    event.stopPropagation();
            //    return false;
            //});
            $(document).click(function () {
                $('#searchlist').parent().removeClass('open');
                $scope.searchtext = "";
            });
            var Text = $scope.searchtext;
            var ModuleIds = 1;
            if (Text.length > 2) {
                $scope.searchresponse = [];
                $scope.myData = [];
                //var SearchResult = $resource('planning/QuickSearch', { get: { method: 'POST' } });
                //var search = new SearchResult();
                var search = {};
                search.Text = Text;
                search.ModuleIds = ModuleIds;
                search.IsGlobalAdmin = IsGlobalAdmin;
                $scope.search = $scope.searchtext;
                //   var searchresult = SearchResult.save(search, function () {
                PlanningService.QuickSearch(search).then(function (searchresult) {
                    $scope.searchresponse = [];
                    $scope.myData = [];
                    $scope.searchresponse = searchresult.Response;
                    $scope.myData = $scope.searchresponse;
                    $('#searchlist').parent().addClass('open');
                });
            } else {
                $scope.myData = [];
                $('#searchlist').parent().removeClass('open');
            }
        };

        $scope.loadresultpage = function (event) {
            var localScope = $(event.target).scope();
            // Invoke the expression in the local scope
            // context to make sure we adhere to the
            // proper scope chain prototypal inheritance.
            if ($('.searchblock').length > 0) {
                $(window).trigger('loadsearchresult', $scope.searchtext);
            } else {
                $location.path('/mui/searchresult/');
            }
            $('#searchlist').parent().removeClass('open');
        }

        function funloadoverview(id, event) {
            $location.path('/mui/planningtool/detail/section/' + id + '/overview');
        }
        $scope.searchloadactivity = function (event, id, typeid, parentid, level, ThumbnailUrl) {
            SearchEntityID = id;
            $scope.EntityTypeID = typeid;
            EntityTypeID = typeid;
            SearchEntityLevel = level;
            if (ThumbnailUrl == "DAM") {
                //$location.path('/mui/planningtool/detail/section/' + parentid + '/attachment/' + id);
                $scope.$broadcast('openassetpopup', id, true);
            }
            else {
                if (typeid == 6) {
                    $location.path('/mui/planningtool/detail/section/' + id + '/overview');
                }
                else if (typeid == 5) {
                    $location.path('/mui/planningtool/costcentre/detail/section/' + id + '/overview');
                }
                else if (typeid == 10) {
                    $location.path('/mui/planningtool/objective/detail/section/' + id + '/overview');
                }
                else if (typeid == 2 || typeid == 3 || typeid == 31 || typeid == 30) {
                    //$location.path('/mui/planningtool/detail/section/'+ parentid +'/task/'+ id +'');
                    $scope.TaskID = id;
                    $scope.EntityTypeID = typeid;
                    $scope.EntityID = parentid;
                    $location.path('/mui/mytask');
                    //$timeout($location.path('mui/taskedit.html'), 1000);
                }
                    //else if (typeid == 30) {
                    //    $location.path('/mui/planningtool/detail/section/' + parentid + '/task/');
                    //}
                else if (typeid == 4) {
                    //'<a ng-href="download.aspx?FileID=d3d64370-d09b-4215-b40b-ae6bbee328d7&amp;FileFriendlyName=Koala.jpg&amp;Ext=.jpg" target="_blank" href="download.aspx?FileID=d3d64370-d09b-4215-b40b-ae6bbee328d7&amp;FileFriendlyName=Koala.jpg&amp;Ext=.jpg">Koala.jpg</a>'
                    //$location.path('/mui/planningtool/detail/section/' + parentid + '/attachment');
                }
                else {
                    $location.path('/mui/planningtool/detail/section/' + id + '/overview');
                }
            }
            $scope.searchtext = "";
            $('#searchlist').parent().removeClass('open');
        }

        function ViewRootLevelEntity(ID, event) {
            var IDList = new Array();
            $window.ListofEntityID = [];
            if (ID == null) {
                IDList = GetRootLevelSelectedAll();
            }
            else {
                IDList.push(ID);
            }
            if (IDList.length !== 0) {
                $window.ListofEntityID = IDList;
                var localScope = $(event.target).scope();
                $location.path("/mui/planningtool/detail/ganttview");
            }
        }

        function ViewRootLevelEntityCostCenter(ID, event) {
            var IDList = new Array();
            $window.ListofEntityID = [];
            if (ID == null) {
                IDList = GetRootLevelSelectedAll();
            }
            else {
                IDList.push(ID);
            }
            if (IDList.length !== 0) {
                $window.ListofEntityID = IDList;
                event.preventDefault();
                var localScope = $(event.target).scope();
                //Invoke the expression in the local scope
                //context to make sure we adhere to the
                //proper scope chain prototypal inheritance.
                $location.path("mui/planningtool/costcentre/detail/ganttview");
            }
        }

        function ViewRootLevelEntityObjective(ID, event) {
            var IDList = new Array();
            $window.ListofEntityID = [];
            IDList.push(ID);
            if (IDList.length !== 0) {
                $window.ListofEntityID = IDList;
                event.preventDefault();
                var localScope = $(event.target).scope();
                // Invoke the expression in the local scope
                // context to make sure we adhere to the
                // proper scope chain prototypal inheritance.
                $location.path("/mui/planningtool/objective/detail/listview");

            }


        }

        $('#result').click(function (event) {
            var localScope = $(event.target).scope();
            // Invoke the expression in the local scope
            // context to make sure we adhere to the
            // proper scope chain prototypal inheritance.
            localScope.$apply(
            function () {

                if ($('.searchblock').length > 0) {
                    $(window).trigger('loadsearchresult')
                } else {
                    $location.path('/mui/searchresult/');
                }
            });
        });

        //---------------------------------------------------------Privacy Statement popup--------------------------------------------------------------------------------------------
        $scope.popMe = function (mypage, myname, w, h) {
            var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
            var win = window.open(mypage, myname, winprops)
        };

        function ChooseUrl(Featureid, navID) {
            switch (Featureid) {
                case 10, 11, 12, 13, 14, 15, 16, 21:
                    return "";
                    break;
                case 17:
                    return "/" + navID;
                    break;
                default:
                    return "";
            }
        }
        // --- Define Scope Methods. ------------------------ //

        $scope.LogoutUser = function () {
            $scope.Logout();
        };
        // ...

        //--------------------------> DUPLICATE ENTITY <-----------------------------

        $scope.DuplicateEntityPageType = "";
        $scope.EntitytoDuplicate = [];
        $scope.EntityleveltoDuplicate = 0;
        $scope.ClearDuplicateVar = function () {
            $scope.varDuplicate.IsEntity = false;
            $scope.varDuplicate.IsEntityStatus = false;
            $scope.varDuplicate.IsAttachments = false;
            $scope.varDuplicate.IsMember = false;
            $scope.varDuplicate.IsMilestone = false;
            $scope.varDuplicate.IsTasks = false;
            $scope.varDuplicate.IsTaskCheckList = false;
            $scope.varDuplicate.IsTaskAttachment = false;
            $scope.varDuplicate.IsCostcentre = false;
            $scope.varDuplicate.IsPlannedBudget = false;
            $("#duplicateEnitynames").html("");
        }

        //-----------------> CALLED THIS FUNCTION FROM DETAIL PAGE OR LIST PAGE <---------------
        $scope.DuplicateEntity = function (duplicatedata, entitylevel, PageName) {
            $scope.DuplicateEntityPageType = PageName;
            $("#duplicateEnitynames").html("");

            if ($scope.DuplicateEntityPageType == "DETAIL")
                $("#nooftimes").show();
            else if ($scope.DuplicateEntityPageType == "LIST") {
                if (duplicatedata.length == 1)
                    $("#nooftimes").show();
                else
                    $("#nooftimes").hide();
            }

            $scope.EntitytoDuplicate = duplicatedata;
            $scope.EntityleveltoDuplicate = entitylevel;
            $scope.duplicateTimes = 1;
            $scope.DiscardChildDuplicate = false;

            $scope.varDuplicate = {
                IsEntity: false,
                IsEntityStatus: false,
                IsAttachments: false,
                IsMember: false,
                IsMilestone: false,
                IsTasks: false,
                IsTaskCheckList: false,
                IsTaskAttachment: false,
                IsCostcentre: false,
                IsPlannedBudget: false
            }

            var appendHtml = "<label class='control-label'>Entity Name 1</label><div class='controls margin-bottom10x'><input id='duplicatentityid0' type='text'/></div>";
            $("#duplicateEnitynames").append(appendHtml);

            $("#duplicateentity").show();
            $("#duplicatetask").hide();
            $("#duplicatecostcentre").hide();

        }

        //----------------> DUPLICATE AND SAVE THE ENTITY <----------------
        $scope.CreateDuplicateEntity = function () {
            if ($scope.duplicateTimes == "" || $scope.duplicateTimes == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4591.Caption'));
                return false;
            }
            if ($scope.varDuplicate.IsEntity == false && $scope.varDuplicate.IsTasks == false && $scope.varDuplicate.IsCostcentre == false) {
                bootbox.alert($translate.instant('LanguageContents.Res_4598.Caption'));
                return false;
            }
            var tempentitynames = [];

            var stas = false;
            if ($scope.DuplicateEntityPageType == "DETAIL" || ($scope.DuplicateEntityPageType == "LIST" && $scope.EntitytoDuplicate.length == 1)) {
                $('#duplicateEnitynames div').each(function (index) {
                    if ($('#duplicatentityid' + index).val() == "") {
                        bootbox.alert($translate.instant('LanguageContents.Res_4592.Caption'));
                        stas = true;
                        return false;
                    }
                    tempentitynames.push($('#duplicatentityid' + index).val());
                });
                if (stas == true)
                    return false;
            }
            else
                tempentitynames.push("");

            $("#showhideprogress").show();

            $('#DuplicateEntity').attr('disabled', true);
            //var DuplicateEntity = $resource('planning/DuplicateEntities/');
            //var duplicate = new DuplicateEntity();
            var duplicate = {};
            duplicate.EntityID = $scope.EntitytoDuplicate;

            duplicate.ParentLevel = $scope.EntityleveltoDuplicate;

            duplicate.duplicateEntityNames = tempentitynames;
            duplicate.DuplicateList = $scope.varDuplicate;

            if ($scope.duplicateTimes == undefined || $scope.duplicateTimes == "")
                duplicate.DuplicateTimes = 1;
            else
                duplicate.DuplicateTimes = $scope.duplicateTimes;
            duplicate.IsDuplicateChild = $scope.DiscardChildDuplicate;

            //   var SaveDuplicate = DuplicateEntity.save(duplicate, function () {
            PlanningService.DuplicateEntities(duplicate).then(function (SaveDuplicate) {
                if (SaveDuplicate.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4321.Caption'));
                }
                else {
                    $scope.duplicateTimes = "";
                    $scope.DiscardChildDuplicate = false;
                    $scope.ClearDuplicateVar();
                    var duplicateLists = SaveDuplicate.Response;
                    if ($scope.DuplicateEntityPageType == "DETAIL")
                        $scope.$broadcast("DETAILEVENT", duplicateLists);
                    else if ($scope.DuplicateEntityPageType == "LIST")
                        $scope.$broadcast("LISTEVENT", duplicateLists);

                    $timeout(function () {
                        $("#showhideprogress").hide();
                        $('#DuplicateEntity').attr('disabled', false);
                        $('#DuplicateModel').modal('hide');
                        NotifySuccess($translate.instant('LanguageContents.Res_4226.Caption'));
                    }, 1000);
                }
            });
        }

        $("#duplicateTimes").keypress(function (event) {
            var valid = (event.which >= 48 && event.which <= 57);
            if (!valid) {
                event.preventDefault();
                return false;
            }
        });
        $("#duplicateTimes").keyup(function (event) {
            var appendHtml = "";
            $("#duplicateEnitynames").html("");
            if ($scope.duplicateTimes == "" || $scope.duplicateTimes == 0) {
                event.preventDefault();
                return false;
            }

            var cnt = 1;
            for (var i = 0; i < parseInt($scope.duplicateTimes) ; i++) {
                appendHtml = appendHtml + "<label class='control-label'>Entity Name " + cnt + "</label><div class='controls margin-bottom10x'><input id='duplicatentityid" + i + "' type='text'/></div>"
                cnt++;
            }
            $("#duplicateEnitynames").append(appendHtml);
        });

        $("#entitytype").click(function (e) {
            $("#duplicateentity").show();
            $("#duplicatetask").hide();
            $("#duplicatecostcentre").hide();
        });
        $("#tasktype").click(function (e) {
            $("#duplicatetask").show();
            $("#duplicateentity").hide();
            $("#duplicatecostcentre").hide();
        });
        $("#costcentretype").click(function (e) {
            $("#duplicatecostcentre").show();
            $("#duplicatetask").hide();
            $("#duplicateentity").hide();
        });

        $("#chkentitytype").click(function (e) {
            if ($scope.varDuplicate.IsEntity == false)
                $("#entitytype").addClass("color-primary");
            else {
                $("#entitytype").removeClass("color-primary");
                $scope.varDuplicate.IsEntityStatus = false;
                $scope.varDuplicate.IsMilestone = false;
                $scope.varDuplicate.IsAttachments = false;
                $scope.DiscardChildDuplicate = false;
                $scope.varDuplicate.IsMember = false;
            }
        });
        $("#chktasktype").click(function (e) {
            if ($scope.varDuplicate.IsTasks == false)
                $("#tasktype").addClass("color-primary");
            else {
                $("#tasktype").removeClass("color-primary");
                $scope.varDuplicate.IsTaskAttachment = false;
                $scope.varDuplicate.IsTaskCheckList = false;
            }

        });
        $("#chkcostcentretype").click(function (e) {
            if ($scope.varDuplicate.IsCostcentre == false)
                $("#costcentretype").addClass("color-primary");
            else {
                $("#costcentretype").removeClass("color-primary");
                $scope.varDuplicate.IsPlannedBudget = false;
            }
        });

        // used for referencing assetedit page from mui
        $scope.$on('callBackAssetEditinMUI', function (event, ID, isLock, isNotify, viewtype) {
            $scope.Islock_temp = isLock;
            $scope.$broadcast('openassetpopup', ID, $scope.Islock_temp, isNotify, viewtype);
        });


        $scope.optmakerObj = { EntityID: 0, FolderID: 0, AssetTypeID: 0, CreatedByID: 0, IsWord: false };
        $scope.SetOptmakerObj = function (eid, fid, atype, uid, isWord) {
            $scope.optmakerObj.EntityID = eid;
            $scope.optmakerObj.FolderID = fid;
            $scope.optmakerObj.AssetTypeID = atype;
            $scope.optmakerObj.CreatedByID = uid;
            $scope.optmakerObj.IsWord = isWord;
        }
        $scope.getOptmakerObj = function () {
            return $scope.optmakerObj;
        }



        //--------------------------> DUPLICATE ENTITY ENDS HERE <-----------------------------

        //Dam Lightbox selection 
        $scope.damlightbox = { lightboxselection: [] };

        // --- Define Controller Variables. ----------------- //

        // Get the render context local to this controller (and relevant params).
        var renderContext = requestContext.getRenderContext("mbmui");


        // --- Define Scope Variables. ---------------------- //


        // The subview indicates which view is going to be rendered on the page.
        $scope.subview = renderContext.getNextSection();

        // Get the current year for copyright output.
        $scope.copyrightYear = (new Date()).getFullYear();


        //DAM optimaker related popup close events

        $scope.LoadassetsfrmOptimaker = function () {

            $scope.$broadcast('CallbackfromOptiIfrm', 0);
        };


        $timeout(function () {
            GetDAMViewSettings();
        }, 100);
        function GetDAMViewSettings() {
            // returns all the extension types and related DAM types in the system.
            DamService.GetDAMViewSettings()
                .then(
                    function (data) {
                        if (data.Response != null) {
                            $scope.SearchAssetLibrarySettings["ThumbnailSettings"] = data.Response[0].ThumbnailSettings;
                            $scope.SearchAssetLibrarySettings["SummaryViewSettings"] = data.Response[0].SummaryViewSettings;
                            $scope.SearchAssetLibrarySettings["ListViewSettings"] = data.Response[0].ListViewSettings;
                        }
                    }
                );
        }

        $scope.MediaBankSettings = {
            Redirectfromdam: false, FolderId: '', EntityId: '', ViewType: '',
            directfromMediaBank: false, ProductionTypeID: '', FolderName: '',
            EditableAssetGuid: '', EditableAssetExt: '', AssetID: 0
        };

        // --- Bind To Scope Events. ------------------------ //


        // I handle changes to the request context.
        $scope.$on(
            "requestContextChanged",
            function () {
                if (!renderContext.isChangeRelevant()) {
                    return;
                }

                //Make sure the user is logged in
                $scope.LoginStatus();

                // Update the view that is being rendered.
                $scope.subview = renderContext.getNextSection();
            }
        );

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mbmuiCtrl']"));
        });
        // --- Initialize. ---------------------------------- //

        // ...
        //For Setting Color Code - By Madhur 22 Dec 2014
        $scope.set_color = function (clr) {
            if (clr != null)
                return { 'background-color': "#" + clr.toString().trim() };
            else
                return '';
        }
    }
    //);
    app.controller('mbmuiCtrl', ['$window', '$location', '$route', '$timeout', '$scope', '$cookies', '$resource', 'requestContext', 'DamService', 'CommonService', 'PlanningService', '_', '$compile', '$translate', mbmuiCtrl]);
})(angular, app);