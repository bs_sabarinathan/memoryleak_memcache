﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BrandSystems.Marcom.Core.Task.Interface;
using BrandSystems.Marcom.Dal.Task.Model;

namespace BrandSystems.Marcom.Core.Task
{
    /// <summary>
    /// Entity object for table 'PM_Entity'.
    /// </summary>
    internal class AdminTask : IAdminTask
    {
        #region Member Variables

        protected int _id;
        protected int _TaskListID;
        protected string _Caption;
        protected int _TaskType;
        protected string _description;

        protected int _Sortorder;
        #endregion

        #region Constructors
        public AdminTask() { }

        public AdminTask(int pTaskListID, string pCaption, int pTaskType, string pDescription, int pSortorder)
        {
            this._TaskListID = pTaskListID;
            this._Caption = pCaption;
            this._description = pDescription;
            this._TaskType = pTaskType;
            this._Sortorder = pSortorder;

        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }



        public virtual string Description
        {
            get { return _description; }
            set
            {
                _description = value;
            }

        }

        public virtual string Caption
        {
            get { return _Caption; }
            set { _Caption = value; }

        }

        public virtual int TaskListID
        {
            get { return _TaskListID; }
            set { _TaskListID = value; }

        }

        public int TaskType
        {
            get { return _TaskType; }
            set { _TaskType = value; }

        }


        public virtual int SortOder
        {
            get { return _Sortorder; }
            set { _Sortorder = value; }

        }

        public string TaskTypeName { get; set; }
        public int TotalTaskAttachment { get; set; }
        public dynamic AttributeData { get; set; }

        public string AttachmentCollection { get; set; }
        public IList<AdminTaskCheckListDao> AdminTaskCheckList { get; set; }
        public string ColorCode { get; set; }
        public string ShortDesc { get; set; }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            AdminTask castObj = null;
            try
            {
                castObj = (AdminTask)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}


