﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Planning.Model
{
    public partial class EntityStatusDao : BaseDao, ICloneable
    {
        #region Member Variables
        protected int _id;
        protected int _entityid;
        protected int _statusid;
        protected int _timestatus;
        protected string _comment;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors

        public EntityStatusDao() { }


        public EntityStatusDao(int pID)
        {
            this._id = pID;
        }
        
        public EntityStatusDao(int pID, int pEntityID, int pStatusID, int pTimestatusID, string pComment)
        {
            this._id = pID;
            this._entityid = pEntityID;
            this._statusid = pStatusID;
            this._timestatus = pTimestatusID;
            this._comment = pComment;
        }


        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int EntityID
        {
            get { return _entityid; }
            set
            {
                _entityid = value;
            }

        }

        public virtual int StatusID
        {
            get { return _statusid; }
            set
            {
                _statusid = value;
            }

        }

        public virtual int IntimeStatus
        {
            get { return _timestatus; }
            set
            {
                _timestatus = value;
            }

        }

        public virtual string Comment
        {
            get { return _comment; }
            set
            {
                _comment = value;
            }

        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string EntityStatusDao = "EntityStatusDao";
            public const string ID = "ID";
            public const string EntityID = "EntityID";
            public const string StatusID = "StatusID";
            public const string IntimeStatus = "IntimeStatus";
            public const string Comment = "Comment";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string EntityStatusDao = "EntityStatusDao";
            public const string ID = "ID";
            public const string EntityID = "EntityID";
            public const string StatusID = "StatusID";
            public const string IntimeStatus = "IntimeStatus";
            public const string Comment = "Comment";
        }

        #endregion

    }
}
