﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Dam.Interface
{
    public interface IAssetAccess
    {
        int ID { get; set; }
        string Role { get; set; }
    }
}
