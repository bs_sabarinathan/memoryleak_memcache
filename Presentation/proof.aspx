﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="proof.aspx.cs" Inherits="Presentation.proof" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="assets/css/mui.css" rel="stylesheet" />
    <link href="assets/css/mui-responsive.css" rel="stylesheet" />
    <style>
        /*#region PROOF STATUS SECTION*/
        .proofHQ-IMG {
            padding-bottom: 5px;
            margin-bottom: 10px;
        }

            .proofHQ-IMG .proofHQ-sum-Box {
                height: 166px;
                margin: 0;
                padding: 0px;
                position: relative;
            }

                .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection {
                    margin: 0px;
                    position: relative;
                }

                    .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block {
                        width: 100%;
                        height: 166px;
                        margin: 0px;
                        overflow: hidden;
                        position: relative;
                    }

                        .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-leftSection {
                            display: inline-block;
                            width: 166px;
                            height: 166px;
                            margin: 0px;
                            overflow: hidden;
                            position: relative;
                        }

                            .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-leftSection .pHQ-sum-ImgBlock {
                                width: 164px;
                                height: 164px;
                                margin: 0px;
                                overflow: hidden;
                                border: 1px solid #EFEFEF;
                            }

                                .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-leftSection .pHQ-sum-ImgBlock .pHQ-sum-ImgContainer {
                                    display: table-cell;
                                    width: 164px;
                                    height: 164px;
                                    margin: 0px;
                                    overflow: hidden;
                                    text-align: center;
                                    vertical-align: middle;
                                    background-color: #FFF;
                                    cursor: pointer;
                                }

                                    .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-leftSection .pHQ-sum-ImgBlock .pHQ-sum-ImgContainer img {
                                        max-width: 164px;
                                        max-height: 164px;
                                        width: auto;
                                        height: auto;
                                    }

                                    .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-leftSection .pHQ-sum-ImgBlock .pHQ-sum-ImgContainer .pHQ-sum-Error-preview {
                                        z-index: 10 !important;
                                        width: inherit;
                                        height: inherit;
                                        vertical-align: middle;
                                        text-align: center;
                                        position: absolute;
                                        top: 1px;
                                        left: 1px;
                                        cursor: pointer;
                                    }

                                        .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-leftSection .pHQ-sum-ImgBlock .pHQ-sum-ImgContainer .pHQ-sum-Error-preview span.errorMsg {
                                            position: relative;
                                            display: inline-block;
                                            width: 100%;
                                            height: 60px;
                                            text-align: center;
                                            vertical-align: middle;
                                            position: absolute;
                                            left: 0px;
                                            bottom: 10px;
                                            background: rgba(255, 255, 255, 0.9);
                                        }

                                            .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-leftSection .pHQ-sum-ImgBlock .pHQ-sum-ImgContainer .pHQ-sum-Error-preview span.errorMsg i.icon-info-sign,
                                            .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-leftSection .pHQ-sum-ImgBlock .pHQ-sum-ImgContainer .pHQ-sum-Error-preview span.errorMsg i.icon-remove-sign {
                                                color: #ff0000;
                                                text-shadow: 0px 0px 10px #ffffff;
                                                display: inline-block;
                                                position: absolute;
                                                top: 12px;
                                                left: 5px;
                                                font-size: 25px;
                                            }

                                            .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-leftSection .pHQ-sum-ImgBlock .pHQ-sum-ImgContainer .pHQ-sum-Error-preview span.errorMsg span.errorTxt {
                                                display: inline-block;
                                                position: absolute;
                                                top: 10px;
                                                left: 32px;
                                                font-size: 14px;
                                                font-weight: bold;
                                                text-align: left;
                                                color: red;
                                            }

                        .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection {
                            display: inline-block;
                            width: 75%;
                            height: 166px;
                            margin: 0 0 0 20px;
                            overflow: hidden;
                            position: relative;
                            color: #777;
                        }

                            .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock {
                                min-width: 250px;
                                height: 166px;
                                margin: 0px;
                                overflow: hidden;
                                position: relative;
                            }

                                .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock .pHQ-sum-expandedHeader {
                                    height: 25px;
                                    border-bottom: 1px solid #DDD;
                                }

                                    .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock .pHQ-sum-expandedHeader h1,
                                    .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock .pHQ-sum-expandedHeader h2,
                                    .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock .pHQ-sum-expandedHeader h3,
                                    .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock .pHQ-sum-expandedHeader h4,
                                    .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock .pHQ-sum-expandedHeader h5 {
                                        margin: 0px !important;
                                        width: 100%;
                                        display: inline-block;
                                        overflow: hidden;
                                        text-overflow: ellipsis;
                                        white-space: nowrap;
                                    }

                                .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock .pHQ-sum-expandedDetails {
                                    height: 166px;
                                    overflow: hidden;
                                    overflow-y: auto;
                                }

                                    .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock .pHQ-sum-expandedDetails form,
                                    .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock .pHQ-sum-expandedDetails .control-group,
                                    .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock .pHQ-sum-expandedDetails .controls,
                                    .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock .pHQ-sum-expandedDetails .control-label {
                                        margin: 0px !important;
                                    }

                                    .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock .pHQ-sum-expandedDetails .control-label {
                                        line-height: 15px !important;
                                        cursor: auto;
                                    }

                                    .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock .pHQ-sum-expandedDetails .control-group > .control-label {
                                        width: auto !important;
                                        max-width: 200px;
                                        text-align: left;
                                        color: #555;
                                        font-weight: bold;
                                        font-size: 15px;
                                        padding: 4px 10px 4px 0px !important;
                                    }

                                        .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock .pHQ-sum-expandedDetails .control-group > .control-label:after {
                                            content: ":";
                                        }

                                    .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock .pHQ-sum-expandedDetails .control-group .controls .control-label {
                                        width: auto !important;
                                        font-size: 15px;
                                        max-width: 450px !important;
                                        text-align: left;
                                        color: #999;
                                        overflow: hidden;
                                    }

                                .proofHQ-IMG .proofHQ-sum-Box .pHQ-sum-Selection .pHQ-sum-Block .pHQ-sum-rightSection .pHQ-sum-expandedDetailBlock .pHQ-sum-Links {
                                    height: 25px;
                                    margin-top: 5px;
                                    padding: 5px 0;
                                    vertical-align: middle;
                                    font-size: 15px;
                                }

        .proofHQ-Details {
            min-height: 175px;
            position: relative;
            overflow: hidden;
            overflow-y: auto;
        }

            .proofHQ-Details .proofHQ-Details-header {
                width: auto !important;
                max-width: 200px;
                text-align: left;
                color: #555;
                font-weight: bold;
                font-size: 15px;
                padding: 4px 10px 4px 0px !important;
                margin-top: -7px;
            }

            .proofHQ-Details .proofStatus {
                display: inline-block;
                position: absolute;
                top: 0;
                right: 10px;
                width: 430px;
                margin-bottom: 10px;
                padding-bottom: 5px;
            }

                .proofHQ-Details .proofStatus .proofStatus-Symbols {
                    display: inline-block;
                    margin-right: 5px;
                }

                    .proofHQ-Details .proofStatus .proofStatus-Symbols .proofStatus-icon {
                        display: inline-block;
                        box-sizing: border-box;
                        overflow: hidden;
                        height: 22px;
                        width: 22px;
                        padding: 1px 0 0;
                        text-align: center;
                        vertical-align: middle;
                        font-size: 16px;
                        font-weight: bold;
                        background-color: #FFF;
                        border: 1px solid #CCC;
                        color: #CCC;
                    }

                        .proofHQ-Details .proofStatus .proofStatus-Symbols .proofStatus-icon.success {
                            background-color: #87B82A;
                            border: 1px solid #5A8E00;
                            color: #FFF;
                        }

                .proofHQ-Details .proofStatus .proofStatus-Text {
                    font-weight: bold;
                    font-size: 12px;
                    color: #999;
                }

            .proofHQ-Details .proofHQ-Details-content {
                min-height: inherit;
            }

                .proofHQ-Details .proofHQ-Details-content .lftSide {
                    padding-right: 10px;
                    min-height: inherit;
                    border-right: 1px solid #EEE;
                }
        /*#endregion*/
    </style>
</head>
<body>
    


    <div class="TaskPopup" runat="server" id="TaskPopup">
        <div class="row-fluid">
            <div class="proofHQ-IMG">
                <div class="proofHQ-sum-Box">
                    <div class="pHQ-sum-Selection">
                        <div class="pHQ-sum-Block">
                            <div class="pHQ-sum-leftSection">
                                <div class="pHQ-sum-ImgBlock">
                                    <div class="pHQ-sum-ImgContainer">
                                        <img id="proofThum" runat="server" alt="Image" src="">
                                    </div>
                                </div>
                            </div>
                            <div class="pHQ-sum-rightSection">
                                <div class="pHQ-sum-expandedDetailBlock">
                                    <div class="pHQ-sum-expandedDetails">
                                        <form class="form-horizontal ng-pristine ng-valid">
                                            <div class="control-group">
                                                <label class="control-label">Proof Name</label>
                                                <div class="controls">
                                                    <span class="control-label" runat="server" id="proofName"></span>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label">Version</label>
                                                <div class="controls">
                                                    <span class="control-label" runat="server" id="proofVersion"></span>
                                                </div>
                                            </div>
                                            <%--<div class="control-group">
                                                <label class="control-label">Folder name</label>
                                                <div class="controls">
                                                    <span class="control-label"><a>PDF</a></span>
                                                </div>
                                            </div>--%>
                                        </form>
                                        <div class="pHQ-sum-Links">
                                            
                                            <br />
                                            <a class="btn btn-primary" runat="server" id="proofUrl" target="_blank">Go to proof</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="proofHQ-Details">
                <label class="proofHQ-Details-header">Proof progress:</label>
                <div class="proofStatus">
                    <span class="proofStatus-Symbols">
                        <asp:Literal ID="proofStatus" runat="server"></asp:Literal>
                       

                    </span>
                    <span class="proofStatus-Text" runat="server" id="proofS">Proof status if Pending</span>
                </div>
                <div class="proofHQ-Details-content">
                    <div class="span6 lftSide">
                        <form class="form-horizontal nomargin">
                            <div class="control-group nomargin">
                                <label class="control-label" for="inputPassword">Assignee(s):</label>
                                <div class="controls">
                                    <div class="assigneeBlock">
                                        <!--If only one round-->
                                        <asp:Literal ID="reviewer" runat="server"></asp:Literal>

                                        
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="span6 rytSide">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Literal ID="errormsg" runat="server"></asp:Literal>
</body>
</html>
