﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Utility.MediaHandler
{
    public class ImageMagickHelper
    {

        private string _ImageMagickPath;

        private string _ProfilePath;

        #region "Public Properties"

        /// <summary>
        ///  Contains the path of image magic program.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string ImageMagickPath
        {
            get { return _ImageMagickPath; }
            set { _ImageMagickPath = value; }
        }

        /// <summary>
        ///  Contains the path of the profile folder.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string ProfilePath
        {
            get { return _ProfilePath; }
            set { _ProfilePath = value; }
        }

        #endregion

        #region "Member Functions"

        /// <summary>
        /// Creates the commands for the GetInfo task.
        /// </summary>
        /// <param name="FilePath">Path of the file whose information is needed</param>
        /// <param name="MData">Contains about what information is needed</param>
        /// <returns>The Xml of Image info as string</returns>
        /// <remarks></remarks>
        public string GetInfo(string filePath, string[] mData, string jobID, string surcExtension = null)
        {

            bool iccProfile = false;
            bool iptcProfile = false;
            bool imageType = false;
            bool general = false;
            string Item = "";


            StringBuilder sb = new StringBuilder();
            sb.Append(" -format ");
            sb.Append("\"");

            foreach (string Item_loopVariable in mData)
            {
                Item = Item_loopVariable;
                if (!(Item == "ICCProfile" | Item == "IPTCProfile" | Item == "ImageType"))
                {
                    general = true;
                    sb.Append("<" + Item.ToString() + ">");
                    sb.Append(ImageMagickFormat(Item));
                    sb.Append("</" + Item.ToString() + ">");
                }
                else if (Item == "ImageType")
                {
                    imageType = true;
                }
                else if (Item == "ICCProfile")
                {
                    iccProfile = true;
                }
                else if (Item == "IPTCProfile")
                {
                    iptcProfile = true;
                }
            }

            sb.Append("\"");
            sb.Append(" ");
            sb.Append("\"" + filePath + "\"");

            string strGeneralResult = null;
            if (general)
            {

                if (((surcExtension != null)))
                {
                    strGeneralResult = ExecuteCommand(sb.ToString(), "identify", surcExtension);

                }
                else
                {
                    strGeneralResult = ExecuteCommand(sb.ToString(), "identify");
                }
            }

            string strICCProfile = null;
            if (iccProfile)
            {
                strICCProfile = "<ICCProfile>" + GetInfoProfile(filePath, "ICCProfile", jobID) + "</ICCProfile>";
            }

            string strIPTCProfile = null;
            if (iptcProfile)
            {
                strIPTCProfile = "<IPTCProfile>" + GetInfoProfile(filePath, "IPTCProfile", jobID) + "</IPTCProfile>";
            }

            string strImageType = null;
            if (imageType)
            {
                strImageType = "<ImageType>" + GetTypeInfo(filePath) + "</ImageType>";
            }

            string fullResult = null;
            if (general)
            {
                fullResult = fullResult + strGeneralResult;
            }
            if (iccProfile)
            {
                fullResult = fullResult + strICCProfile;
            }
            if (iptcProfile)
            {
                fullResult = fullResult + strIPTCProfile;
            }
            if (imageType)
            {
                fullResult = fullResult + strImageType;
            }

            return "<ImageInfo>" + fullResult + "</ImageInfo>";

        }

        /// <summary>
        /// To get the Image type using verbose command
        /// </summary>
        /// <param name="filePath">Path of the file whose information is needed</param>
        /// <returns>Returns a string</returns>
        /// <remarks></remarks>
        public string GetTypeInfo(string filePath)
        {
            string nl = System.Environment.NewLine;
            StringBuilder sb = new StringBuilder();
            sb.Append(" -verbose ");
            sb.Append("\"" + filePath + "\"");
            string strResult = ExecuteCommand(sb.ToString(), "verbose");

            int i = (strResult).IndexOf("Type: ");
            int j = (strResult).IndexOf("Endianess:");
            int ilen = strResult.Length;

            string strType = strResult.Substring(i + 5, (ilen - (j)));
            string strCmd1 = strResult.Remove(j - 1);
            strType = strCmd1.Substring((i + 6) - 1);
            Trace.TraceInformation(DateTime.Now.ToString() + " - " + strType);
            //return strType.Split(":")(0).Replace(nl, "").Replace(" ", "");
            return "";

        }

        /// <summary>
        ///  Creates the commands for getting the profile, and calls the executeConvert function.
        /// </summary>
        /// <param name="FilePath">Path of the file whose information is needed</param>
        /// <param name="MData">Contains about what information is needed</param>
        /// <returns>ProfileFilePath</returns>
        /// <remarks></remarks>
        public string GetInfoProfile(string filePath, string mData, string jobID)
        {

            string profileFilePath = "";
            string strResult = null;
            StringBuilder sb = new StringBuilder();

            sb.Append(" ");
            sb.Append("\"" + filePath + "\"");
            sb.Append(" ");
            if (mData == "ICCProfile")
            {
                profileFilePath = ProfilePath + "\\" + jobID + ".icc";
            }
            else if (mData == "IPTCProfile")
            {
                profileFilePath = ProfilePath + "\\" + jobID + ".iptc";
            }
            if (!(profileFilePath == null))
            {
                sb.Append("\"" + profileFilePath + "\"");
            }

            ExecuteCommand(sb.ToString(), "convert");



            FileInfo profileFile = new FileInfo(profileFilePath);

            if (Path.GetExtension(profileFilePath) == ".icc")
            {
                if (profileFile.Length > 0)
                {
                    strResult = profileFilePath;
                }
                else
                {
                    strResult = "Not Present";
                }

            }
            else if (Path.GetExtension(profileFilePath) == ".iptc")
            {
                if (profileFile.Length > 0)
                {
                    strResult = profileFilePath;
                }
                else
                {
                    strResult = "Not Present";
                }
            }

            return strResult;

        }

        /// <summary>
        /// Creates the commands to get the mime type info.
        /// </summary>
        /// <param name="FilePath">Path of the file whose information is needed</param>
        /// <param name="MData">Contains about what information is needed</param>
        /// <returns>The Xml of Image info as string</returns>
        /// <remarks></remarks>
        public string GetMimeTypeInfo(string filePath, string[] mData, string jobID)
        {

            bool iccProfile = false;
            bool iptcProfile = false;
            bool imageType = false;
            bool general = false;
            string Item = "";

            StringBuilder sb = new StringBuilder();
            sb.Append(" -format ");
            sb.Append("\"");

            foreach (string Item_loopVariable in mData)
            {
                Item = Item_loopVariable;
                sb.Append("<" + Item.ToString() + ">");
                sb.Append(ImageMagickFormat(Item));
                sb.Append("</" + Item.ToString() + ">");
            }

            sb.Append("\"");
            sb.Append(" ");
            sb.Append("\"" + filePath + "\"");

            string strGeneralResult = null;
            strGeneralResult = ExecuteTOGetMimeType(sb.ToString(), "identify");
            return "<ImageInfo>" + strGeneralResult + "</ImageInfo>";

        }


        /// <summary>
        ///  Creates the commands for the Covert Task.
        /// </summary>
        /// <param name="SourcePath">Provides the source path</param>
        /// <param name="workprioritylevel">Provides the workprioritylevel</param>
        /// <param name="IsResize">Provides the IsResize</param>
        /// <param name="ISCrop">Provides the ISCrop</param>
        /// <param name="IsScale">Provides the IsScale</param>
        /// <param name="IsThumbnail">Provides the IsThumbnail</param>
        /// <param name="ResizeHeight">Provides the ResizeHeight</param>
        /// <param name="ResizeWidth">Provides the ResizeWidth</param>
        /// <param name="ResizeInPercentage">Provides the ResizeInPercentage</param>
        /// <param name="CropWidth">Provides the CropWidth</param>
        /// <param name="CropHeight">Provides the CropHeight</param>
        /// <param name="DesLeft">Provides the DesLeft</param>
        /// <param name="DesTop">Provides the DesTop</param>
        /// <param name="ScaleHeight">Provides the ScaleHeight</param>
        /// <param name="ScaleWidth">Provides the ScaleWidth</param>
        /// <param name="ScaleInPercentage">Provides the ScaleInPercentage</param>
        /// <param name="ColourSpace">Provides the ColourSpace</param>
        /// <param name="ImageType">Provides the ImageType</param>
        /// <param name="PageNo">Provides the PageNo</param>
        /// <param name="quality">Provides the quality</param>
        /// <param name="XResolution">Provides the XResolution</param>
        /// <param name="YResolution">Provides the YResolution</param>
        /// <param name="MemoryVal">Provides the MemoryVal</param>
        /// <param name="DestFilePath">Provides the DestFilePath</param>
        /// <returns>commandstring</returns>
        /// <remarks></remarks>
        public string ExecuteConvert(string sourcePath, bool workPriorityLevel, bool isResize, bool isCrop, bool isScale, bool isThumbnail, double resizeHeight, double resizeWidth, int resizeInPercentage, double cropWidth,
        double cropHeight, double desLeft, double desTop, double scaleHeight, double scaleWidth, int scaleInPercentage, string colourSpace, string imageType, int pageNo, int quality,
        double xResolution, double yResolution, string memoryVal, string destFilePath, string flipDirection, int rotationAngle, bool isRoundCorner = false)
        {


            StringBuilder sb = new StringBuilder();
            sb.Append("\"" + sourcePath + "\"");

            sb.Append(" -units PixelsPerInch ");

            sb.Append(" -background white -flatten ");

            if (!workPriorityLevel)
            {
                sb.Append(" -limit memory  ");
                sb.Append(memoryVal);
            }


            if (isCrop)
            {
                sb.Append(" -crop ");

                string opTop = null;
                if (desTop >= 0)
                {
                    opTop = "+";
                }
                else
                {
                    opTop = "-";
                }
                string opLeft = null;
                if (desLeft >= 0)
                {
                    opLeft = "+";
                }
                else
                {
                    opLeft = "-";
                }
                if ((cropHeight > 0 & cropWidth > 0))
                {
                    if (!(cropWidth == null))
                    {
                        sb.Append(Convert.ToInt32(Math.Floor(cropWidth)));
                    }
                    sb.Append("x");
                    if (!(cropHeight == null))
                    {
                        sb.Append(Convert.ToInt32(Math.Floor(cropHeight)));
                    }

                    sb.Append(opLeft);
                    sb.Append(Convert.ToInt32(Math.Floor(desLeft)));

                    sb.Append(opTop);
                    sb.Append(Convert.ToInt32(Math.Floor(desTop)));

                }

                if (isThumbnail)
                {
                    sb.Append(" -strip ");
                }
                else
                {
                    sb.Append(" +profile 8BIM");
                }

            }


            if (isResize)
            {
                sb.Append(" -resize ");
                if ((resizeHeight > 0 | resizeWidth > 0))
                {
                    sb.Append(Convert.ToInt32(Math.Floor(resizeWidth)));
                    sb.Append("x");
                    sb.Append(Convert.ToInt32(Math.Floor(resizeHeight)));
                }
                else if ((resizeInPercentage > 0))
                {
                    sb.Append(resizeInPercentage);
                    sb.Append("% ");
                }

                if (isThumbnail)
                {
                    sb.Append(" -strip ");
                }
                else
                {
                    sb.Append(" +profile 8BIM");
                }

            }


            if (isScale)
            {
                sb.Append(" -scale ");
                if ((scaleWidth > 0 | scaleHeight > 0))
                {
                    sb.Append(Convert.ToInt32(Math.Floor(scaleWidth)));
                    sb.Append("x");
                    sb.Append(Convert.ToInt32(Math.Floor(scaleHeight)));
                }
                else if ((scaleInPercentage > 0))
                {
                    sb.Append(scaleInPercentage);
                    sb.Append("% ");
                }

                if (isThumbnail)
                {
                    sb.Append(" -strip ");
                }
                else
                {
                    sb.Append(" +profile 8BIM");
                }

            }


            if ((xResolution > 0 | yResolution > 0))
            {
                sb.Append(" -units PixelsPerInch  -density ");
                if (xResolution > 0)
                {
                    sb.Append(xResolution);
                }
                else
                {
                    sb.Append(yResolution);
                }

                sb.Append("x");

                if (yResolution > 0)
                {
                    sb.Append(yResolution);
                }
                else
                {
                    sb.Append(xResolution);
                }

            }

            if ((quality > 0))
            {
                sb.Append(" -quality ");
                sb.Append(quality);
            }

            if (colourSpace.Length > 0)
            {
                sb.Append(" -colorspace ");
                sb.Append(colourSpace);

            }
            else
            {
                if (isRoundCorner)
                {
                    sb.Append(" -colorspace sRGB ");

                }
            }
            if (imageType.Length > 0)
            {
                sb.Append(" -type ");
                sb.Append(imageType);
            }

            sb.Append(" +repage ");

            if (flipDirection != "NULL")
            {
                if (flipDirection == "Horizontal")
                {
                    sb.Append(" -flop ");
                }
                else
                {
                    sb.Append(" -flip ");
                }
            }

            if (rotationAngle != 0)
            {
                sb.Append(" -rotate ");
                sb.Append(rotationAngle.ToString());
            }

            sb.Append(" ");
            if (!(destFilePath == null))
            {
                sb.Append("\"" + destFilePath + "\"");
            }

            string Result = null;

            try
            {
                Result = ExecuteCommand(sb.ToString(), "convert");
                if (Result != null)
                {

                    if (Result.Length > 0)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return Result;

        }

        /// <summary>
        /// Returns the Image format based in argument passed.
        /// </summary>
        /// <param name="arg"></param>
        /// <returns>The format using in IM command</returns>
        /// <remarks></remarks>
        public string ImageMagickFormat(string arg)
        {

            switch (arg)
            {
                case "ColourSpace":
                    return "%[colorspace]";
                case "Compression":
                    return "%[C]";
                case "Depth":
                    return "%[z]";
                case "Format":
                    return "%[m]";
                case "Height":
                    return "%[h]";
                case "ICCProfile":
                    return " ";
                case "ImageClass":
                    return "%[r]-%[colorspace]";
                case "ImageType":
                    return " ";
                case "IPTCProfile":
                    return " ";
                case "PageNo":
                    return "%[n]";
                case "Quality":
                    return "%[Q]";
                case "Width":
                    return "%[w]";
                case "XResolution":
                    return "%[x]";
                case "YResolution":
                    return "%[y]";
                case "EXIFProfile":
                    return "%[EXIF:*]";
                default:
                    return " ";
            }

            return null;

        }

        /// <summary>
        /// Code to execute IM through command line
        /// To identify format and characteristics of image file 
        /// To convert the image in to the specified one
        /// </summary>
        /// <param name="argC"></param>
        /// <param name="Op"></param>
        /// <returns>Img info after IM command execution Or nothing</returns>
        /// <remarks></remarks>
        public string ExecuteCommand(string argC, string op, string surcExtension = null)
        {
            // ImageResponseObject

            string strCmdResult = "";
            int intStatus = 0;
            string strCmdError = "";

            Process imageMagickProcess = new Process();

            if (op == "identify" | op == "verbose")
            {
                imageMagickProcess.StartInfo.FileName = ImageMagickPath + "\\identify";
                Trace.TraceInformation(DateTime.Now.ToString() + " - " + "identify {0}", argC);
            }
            else if (op == "convert")
            {
                imageMagickProcess.StartInfo.FileName = ImageMagickPath + "\\convert ";
                Trace.TraceInformation(DateTime.Now.ToString() + " - " + "convert {0}", argC);
            }

            imageMagickProcess.StartInfo.Arguments = string.Concat(new string[] { argC });
            imageMagickProcess.StartInfo.RedirectStandardOutput = true;
            imageMagickProcess.StartInfo.RedirectStandardError = true;
            imageMagickProcess.StartInfo.UseShellExecute = false;
            imageMagickProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            imageMagickProcess.StartInfo.CreateNoWindow = true;
            imageMagickProcess.Start();
            strCmdResult = imageMagickProcess.StandardOutput.ReadToEnd();
            strCmdError = imageMagickProcess.StandardError.ReadToEnd();
            intStatus = imageMagickProcess.ExitCode;
            imageMagickProcess.WaitForExit();
            Trace.TraceInformation(DateTime.Now.ToString() + " - " + "CMD Result : " + strCmdResult);
            Trace.TraceInformation(DateTime.Now.ToString() + " - " + "CMD Error : " + strCmdError);


            if (intStatus == 1)
            {
                if (op == "identify" | op == "verbose")
                {

                    if (((surcExtension != null)))
                    {
                        if (surcExtension == ".emf" | surcExtension == "emf" | surcExtension == "[0]emf")
                        {
                            if (((strCmdResult != null)))
                            {
                                return strCmdResult;
                            }
                            else
                            {
                                return strCmdError;

                            }
                        }
                        else
                        {
                            return strCmdError;

                        }


                    }
                    else
                    {
                        return strCmdError;
                    }
                }
                else
                {
                    return strCmdError;

                }
            }

            return strCmdResult;

        }

        /// <summary>
        /// Code to execute Ghostscript
        /// To identify format and characteristics of image file 
        /// To convert the image in to the specified one
        /// </summary>
        /// <param name="sourceFile">Source File Path</param>
        /// <param name="destinationFile">Destination File Path</param>
        /// <returns>Img info after GS command execution Or nothing</returns>
        /// <remarks></remarks>

        public string ExecuteRoundCorners(string sourcePath, bool workPriority, string destPath, int width, int height, string topLeft, string topRight, string bottomLeft, string bottomRight, string cornerColor)
        {

            StringBuilder sb = new StringBuilder();
            sb.Append("-size ");
            sb.Append(width.ToString() + "x");
            sb.Append(height.ToString());
            sb.Append(" xc:none -draw ");
            sb.Append("\"" + "roundRectangle " + topLeft + "\"");
            sb.Append(" -draw " + "\"" + "roundRectangle " + topRight + "\"");
            sb.Append(" -draw " + "\"" + "roundRectangle " + bottomLeft + "\"");
            sb.Append(" -draw " + "\"" + "roundRectangle " + bottomRight + "\"");
            sb.Append(" \"" + sourcePath + "\" -compose ATop -composite ");
            if ((!(cornerColor == null | object.ReferenceEquals(cornerColor, ""))))
            {
                sb.Append("-background ");
                sb.Append(cornerColor);
            }
            else
            {
                sb.Append("-background ");
                sb.Append("white");
            }
            sb.Append(" -flatten ");
            sb.Append("\"" + destPath + "\"");
            string Result = null;
            try
            {
                Result = ExecuteCommand(sb.ToString(), "convert");
                if (Result != null)
                {
                    if (Result.Length > 0)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return Result;

        }

        public string ExecuteTOGetMimeType(string argC, string op)
        {
            // ImageResponseObject

            string strCmdResult = "";
            int intStatus = 0;
            string strCmdError = "";

            Process imageMagickProcess = new Process();

            if (op == "identify" | op == "verbose")
            {
                imageMagickProcess.StartInfo.FileName = ImageMagickPath + "\\identify";
                Trace.TraceInformation(DateTime.Now.ToString().ToString() + " - " + "identify {0}", argC);
            }
            else if (op == "convert")
            {
                imageMagickProcess.StartInfo.FileName = ImageMagickPath + "\\convert ";
                Trace.TraceInformation(DateTime.Now.ToString().ToString() + " - " + "convert {0}", argC);
            }

            imageMagickProcess.StartInfo.Arguments = string.Concat(new string[] { argC });
            imageMagickProcess.StartInfo.RedirectStandardOutput = true;
            imageMagickProcess.StartInfo.RedirectStandardError = true;
            imageMagickProcess.StartInfo.UseShellExecute = false;
            imageMagickProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            imageMagickProcess.StartInfo.CreateNoWindow = true;
            imageMagickProcess.Start();
            strCmdResult = imageMagickProcess.StandardOutput.ReadToEnd();
            strCmdError = imageMagickProcess.StandardError.ReadToEnd();
            intStatus = imageMagickProcess.ExitCode;
            imageMagickProcess.WaitForExit();
            Trace.TraceInformation(DateTime.Now.ToString().ToString() + " - " + "CMD Result : " + strCmdResult);
            Trace.TraceInformation(DateTime.Now.ToString().ToString() + " - " + "CMD Error : " + strCmdError);
            return strCmdResult;

        }


        #endregion

    }
}
