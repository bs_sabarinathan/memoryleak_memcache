﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BrandSystems.Marcom.Core.Report.Interface;
using System.Collections;
using BrandSystems.Marcom.Utility;
using BrandSystems.Marcom.Metadata.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Core.Metadata.Interface;
using Newtonsoft.Json.Linq;


namespace BrandSystems.Marcom.Core.Interface.Managers
{

    public interface IReportManager
    {

        /// <summary>
        /// Update users.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="ReportUrl">The ReportUrl.</param>
        /// <param name="AdminUsername">The AdminUsername.</param>
        /// <param name="AdminPassword">The AdminPassword.</param>
        /// <param name="ViewerUsername">The ViewerUsername.</param>
        /// <param name="ViewerPassword">The ViewerPassword</param>
        /// <param name="Category">Category.</param>
        /// <returns>bool</returns>
        bool ReportCredential_InsertUpdate(string ReportUrl, string AdminUsername, string AdminPassword, string ViewerUsername, string ViewerPassword, int Category, int DataViewID, int id = 0);

        int ReportCredential_ValidateSave(string ReportUrl, string AdminUsername, string AdminPassword, string ViewerUsername, string ViewerPassword, int Category, int DataViewID, int id = 0);

        IList ReportCredential_Select(int id = 0);

        bool Report_InsertUpdate(int OID, string Name, string Caption, string Description, string Preview, bool Show, int CategoryId, bool EntityLevel, bool SubLevel, int id = 0);

        IList<IReports> MergeReports(int OID = 0);

        string CustomViews_Validate(string Name, string Query, int ID = 0);

        IList CustomViews_Select(int ID = 0);

        bool CustomViews_DeleteByID(int ID);
        int pushviewSchema();

        Tuple<int, string> CustomViews_Insert(string Name, string Description, string Query);

        Tuple<bool, string> CustomViews_Update(int ID, string Name, string Description, string Query);

        bool UpdateReportImage(string sourcepath, int imgwidth, int imgheight, int imgX, int imgY, int OID, string Preview, string ReportName);

        IList ReportLogin(string ReportUrl, string viewerusername, string viewerpassword);

        IList<IDataView> Dataview_select(int DataViewID, string TenantHost, string AdminUsername = null);

        IEnumerable<ReportModel> GetListOfReports(string viewerusername, string viewerpassword);

        IList<IReports> ShowReports(int OID, bool show = false);

        bool UpdateReportSchemaResponse(int status);

        int GetReportViewSchemaResponse();

        IList GetFinancialSummaryDetlRpt(string SelectedEntityTypeIDs);

        IList GetFinancialSummaryDetlRptByAttribute(string EntityTypeId, int attributeID);

        IList GetEntityFinancialSummaryDetl(string EntityTypeID, List<string> AttributeID, List<int> FinancialAttributes, string orderbyclause = "");

        string ListofRecordsSystemReport(int FilterID, IList<IFiltersettingsValues> filterSettingValues, int[] IdArr, string SortOrderColumn, bool IsDesc, ListSettings listSetting, bool IncludeChildren, int enumEntityTypeIds, int EntityID, bool IsSingleID, int UserID, int Level, bool IsobjectiveRootLevel, int ExpandingEntityID, string GanttstartDate, string Ganttenddate, bool IsMonthly);
        Guid? GetStrucuralRptDetail(ListSettings listSetting, bool IsshowFinancialDetl, bool IsDetailIncluded, bool IsshowTaskDetl, bool IsshowMemberDetl, int ExpandingEntityIDStr, bool IncludeChildrenStr);

        string GetReportJSONData(int reportId);

        IList<IEntityTypeAttributeRelationwithLevels> GetEntityTypeAttributeRelationWithLevelsByID(string ids);

        bool InsertUpdateReportSettingXML(JObject jsonXML, int reportID);

        IList GetFinancialReportSettings();
        bool insertupdatefinancialreportsettings(string reportsettingname, int reportID, string ReportImage, string description, JObject jsonXML);

        IList<IEntityTypeAttributeRelationwithLevels> GetAllEntityTypeAttributeRelationWithLevels();

        Tuple<Guid, string> GenerateFinancialExcel(int ReportID);

        bool UpdateFinancialSettingsReportImage(string sourcepath, int imgwidth, int imgheight, int imgX, int imgY, string Preview);

        /// <summary>
        /// Deletes the financial report settings.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="Id">The id.</param>
        /// <returns>bool</returns>
        bool DeletefinancialreportByID(int reportID);

        string ExportTaskListtoExcel(int entityId, int taskListId, bool isEntireTaskList, bool IsIncludeSublevel, int TenantID);

        IList<IAttribute> GetFulfillmentAttribute(int[] entityTypeId);

        int InsertUpdateCustomlist(int ID, string Name, string Description, string XmlData, string ValidatedQuery);

        bool DeleteCustomList(int ID);

        IList<ICustomList> GetAllCustomList();

        Tuple<string, string> CustomList_Validate(string Name, string XmlData);

        IList<IOption> GetFulfillmentAttributeOptions(int attributeId, int attributeLevel = 0);

        bool insertupdatetabsettings(int tabtype, int tablocation, JObject jsonXML);

        string GetLayoutData(int tabtype, int tablocation);

        IList GettableauReportSettings(int id=0);

        bool DeletetableaureportByID(int reportID);

        bool UpdatetableauSettingsReportImage(string sourcepath, int imgwidth, int imgheight, int imgX, int imgY, string Preview);

        bool insertupdatetableaureportsettings(string reportsettingname, int reportID, string ReportImage, string description, string url);


    }
}
