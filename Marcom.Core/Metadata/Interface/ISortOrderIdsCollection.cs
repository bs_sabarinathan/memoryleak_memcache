﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface ISortOrderIdsCollection
    {
        [XmlAttribute]
        int Id { get; set; }
        [XmlAttribute]
        int EntityIds { get; set; }
        [XmlAttribute]
        int Level { get; set; }
        [XmlAttribute]
        int ParentEntityID { get; set; }
        [XmlAttribute]
        int InterMediateID { get; set; }
        [XmlAttribute]
        string UniqueKey { get; set; }
        [XmlAttribute]
        int OptionID { get; set; }
        [XmlAttribute]
        string OptionCaption { get; set; }
    }
}
