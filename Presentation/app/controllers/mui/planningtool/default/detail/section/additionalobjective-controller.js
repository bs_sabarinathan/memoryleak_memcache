﻿(function (ng, app) {
    "use strict";

    function muiplanningtooldefaultdetailsectionadditionalobjectiveCtrl($scope, $location, $resource, $timeout, $cookies, $stateParams, $window, $compile, $translate, PlanobjectiveService) {
        var timeoutvariable = {};
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.detail.section.additionalobjectiveCtrl']"));
        });

        function ValidAdditionalObjectiveTab1() {
            var additionalDetailTabValues = [
				['#AdditionalObjectiveName', 'presence', 'Please Enter the Name']
            ];
            $("#AdditionalObjectiveTablMetadata").nod(additionalDetailTabValues, {
                'delay': 200,
                'submitBtnSelector': '#btnTempadditionalobj',
                'silentSubmit': 'true'
            });
        }
        $("#additionalObjectiveEntity").on("onAdditionalObjecitveCreation", function () {
            $scope.ClearingObjectivecontrols();
        });
        $scope.ClearingObjectivecontrols = function () {
            $("#AdditionalObjectiveWizardSave").removeAttr('disabled');
            $('#AdditionalObjectiveWizard').wizard('stepLoaded');
            window["tid_wizard_steps_all_complete_count1"] = 0;
            window["tid_wizard_steps_all_complete1"] = setInterval(function () {
                KeepAllStepsMarkedComplete1();
            }, 25);
            $('#AdditionalObjectiveWizardSave').hide();
            $('#btnWizardObjNext').show();
            $('#btnWizardObjPrev').hide();
            $scope.AdditionalObjective.ngObjectiveName = '';
            $scope.AdditionalObjective.ngObjectiveStatus = 1;
            $scope.AdditionalObjective.ngObjectiveDescription = '';
            $scope.AdditionalObjective.ngObjectiveType = 2;
            $scope.AdditionalObjective.ngObjectivenonNumericUnits = 0;
            $scope.AdditionalObjective.ngObjectivePlannedTargetCheck = false;
            $scope.AdditionalObjective.ngObjectivePlannedTarget = (0).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
            $scope.AdditionalObjective.ngObjectiveTargetoutcomeCheck = 0;
            $scope.AdditionalObjective.ngObjectiveTargetoutcome = (0).formatMoney($scope.DecimalSettings['FormatMoney'].Objective_FormatMoney, '.', ' ');
            $scope.AdditionalObjective.ngObjectivenonNumericEnableCommentsCheck = false;
            $scope.AdditionalObjective.ngObjectivenonNumericDescriptionInstructions = '';
            $scope.AdditionalObjective.ngObjectiveQulaitativeDescriptionInstructions = '';
            $scope.AdditionalObjective.ngObjectiveQulaitativeEnableCommentsCheck = false;
            $scope.AdditionalObjective.ngObjectiveRatingDescriptionInstructions = '';
            $scope.AdditionalObjective.ngObjectiveRatingEnableCommentsCheck = false;
            $scope.nonNumericQuantivedisplay = true;
            $scope.qualitativedisplay = false;
            $scope.ratingsdisplay = false;
            handleTextChangeEvents();
            $('#AdditionalObjectiveWizardSave').show();
        };
        var totalWizardSteps1 = $('#AdditionalObjectiveWizard').wizard('totnumsteps').totstep;
        $('#AdditionalObjectiveWizardSave').hide();
        $('#btnWizardObjNext').show();
        $('#btnWizardObjPrev').hide();
        window["tid_wizard_steps_all_complete_count1"] = 0;
        window["tid_wizard_steps_all_complete1"] = setInterval(function () {
            KeepAllStepsMarkedComplete1();
        }, 25);
        $scope.changeObjTab = function () {
            window["tid_wizard_steps_all_complete_count1"] = 0;
            window["tid_wizard_steps_all_complete1"] = setInterval(function () {
                KeepAllStepsMarkedComplete1();
            }, 25);
            var currentWizardStep = $('#AdditionalObjectiveWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#AdditionalObjectiveWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep === 1) {
                $('#AdditionalObjectiveWizardSave').hide();
                $('#btnWizardObjNext').show();
                $('#btnWizardObjPrev').hide();
            } else if (currentWizardStep === totalWizardSteps) {
                $('#AdditionalObjectiveWizardSave').show();
                $('#btnWizardObjNext').hide();
                $('#btnWizardObjPrev').show();
            } else {
                $('#AdditionalObjectiveWizardSave').hide();
                $('#btnWizardObjNext').show();
                $('#btnWizardObjPrev').show();
            }
        }
        $('#ngObjectivePlannedTargetCheck').click(function (e) {
            if ($(this).is(':checked')) {
                $scope.Objective.ngObjectiveNonNumericGlobalbaseline = '0';
            } else { }
        });
        $('#ngObjectiveTargetoutcomeCheck').click(function (e) {
            if ($(this).is(':checked')) {
                $scope.Objective.ngObjectiveNonNumericGlobaltarget = '0';
            } else { }
        });

        function KeepAllStepsMarkedComplete1() {
            $("#AdditionalObjectiveWizard ul.stepsObj").find("li").addClass("complete");
            $("#AdditionalObjectiveWizard ul.stepsObj").find("span.badge").addClass("badge-success");
            window["tid_wizard_steps_all_complete_count1"]++;
            if (window["tid_wizard_steps_all_complete_count1"] >= 2) {
                clearInterval(window["tid_wizard_steps_all_complete1"]);
            }
        }
        $scope.changePrevTab = function () {
            window["tid_wizard_steps_all_complete_count1"] = 0;
            window["tid_wizard_steps_all_complete1"] = setInterval(function () {
                KeepAllStepsMarkedComplete1();
            }, 25);
            $('#AdditionalObjectiveWizard').wizard('previous');
            var currentWizardStep = $('#AdditionalObjectiveWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#AdditionalObjectiveWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep === 1) {
                $('#btnWizardObjPrev').hide();
            }
            if (currentWizardStep < totalWizardSteps) {
                $('#AdditionalObjectiveWizardSave').hide();
                $('#btnWizardObjNext').show();
            } else {
                $('#AdditionalObjectiveWizardSave').show();
                $('#btnWizardObjNext').hide();
            }
        }
        ValidAdditionalObjectiveTab1();
        $scope.status = true;
        $scope.changenexttab = function () {
            $("#btnTempadditionalobj").click();
            if ($("#AdditionalObjectiveTablMetadata .error").length > 0) {
                return false;
            }
            window["tid_wizard_steps_all_complete_count1"] = 0;
            window["tid_wizard_steps_all_complete1"] = setInterval(function () {
                KeepAllStepsMarkedComplete1();
            }, 25);
            $('#AdditionalObjectiveWizard').wizard('next', '');
            var currentWizardStep = $('#AdditionalObjectiveWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#AdditionalObjectiveWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep > 1) {
                $('#btnWizardObjPrev').show();
            }
            if (currentWizardStep === totalWizardSteps) {
                $('#AdditionalObjectiveWizardSave').show();
                $('#btnWizardObjNext').hide();
            } else {
                $('#AdditionalObjectiveWizardSave').hide();
                $('#btnWizardObjNext').show();
            }
        }
        $scope.AdditionalObjective = {
            ngObjectiveName: '',
            ngObjectiveOwner: '',
            ngObjectiveStatus: 1,
            ngObjectiveDescription: '',
            ngObjectiveType: 2,
            ngObjectivenonNumericUnits: 0,
            ngObjectivePlannedTargetCheck: false,
            ngObjectivePlannedTarget: 0,
            ngObjectiveTargetoutcomeCheck: false,
            ngObjectiveTargetoutcome: 0,
            ngObjectivenonNumericEnableCommentsCheck: false,
            ngObjectivenonNumericDescriptionInstructions: '',
            ngObjectiveQulaitativeDescriptionInstructions: '',
            ngObjectiveQulaitativeEnableCommentsCheck: false,
            ngObjectiveRatingDescriptionInstructions: '',
            ngObjectiveRatingEnableCommentsCheck: false,
        }
        $scope.ObjectiveUnitsData = [];
        PlanobjectiveService.GettingObjectiveUnits().then(function (ObjectiveUnitsResult) {
            $scope.ObjectiveUnitsData = ObjectiveUnitsResult.Response;
        });
        $scope.MemberLists = [];
        $scope.AddRatingsHtml = function () {
            $scope.RatingsHtml = '';
            $scope.ratingItems = [];
            $scope.ratingItems.push({
                ObjRatingText: ''
            });
            $scope.ratingRowsCount = 0;
        };
        $scope.AddRatings = function () {
            if ($scope.ratingItems[$scope.ratingItems.length - 1].ObjRatingText == "" || $scope.ratingItems[0].ObjRatingText == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_1918.Caption'));
                return false;
            } else {
                $scope.ratingRowsCount = $scope.ratingRowsCount + 1;
                $scope.ratingItems.push({
                    ObjRatingText: ''
                });
            }
        }
        $scope.AddRatings1 = function () {
            $scope.ratingRowsCount = $scope.ratingRowsCount + 1;
            $scope.ratingItems.push({
                ObjRatingText: ''
            });
        };
        $scope.RemoveRatings = function (item) {
            if ($scope.ratingRowsCount < 2) {
                bootbox.alert($translate.instant('LanguageContents.Res_1917.Caption'));
                return false;
            } else {
                $scope.ratingRowsCount = $scope.ratingRowsCount - 1;
                $scope.ratingItems.splice($.inArray(item, $scope.items), 1);
            }
        };
        $scope.nonNumericQuantivedisplay = true;
        $scope.additionalAbjectiveTypeChange = function () {
            if ($scope.AdditionalObjective.ngObjectiveType == 2) {
                $scope.qualitativedisplay = false;
                $scope.ratingsdisplay = false;
                $scope.nonNumericQuantivedisplay = true;
            }
            if ($scope.AdditionalObjective.ngObjectiveType == 3) {
                $scope.ratingsdisplay = false;
                $scope.nonNumericQuantivedisplay = false;
                $scope.qualitativedisplay = true;
            }
            if ($scope.AdditionalObjective.ngObjectiveType == 4) {
                $scope.AddRatingsHtml();
                $scope.nonNumericQuantivedisplay = false;
                $scope.qualitativedisplay = false;
                $scope.ratingsdisplay = true;
                $scope.RatingsHtml += '<tr ng-repeat="item in ratingItems">';
                $scope.RatingsHtml += '<td><input type="text" class="nomargin" ng-model="item.ObjRatingText" maxlength="30" name="item.ObjRatingText" id="item.ObjRatingText"></td>';
                $scope.RatingsHtml += '<td><button class="btn" ng-click="AddRatings()" ng-model="AddRatings"><i class="icon-plus"></i></button></td>';
                $scope.RatingsHtml += '<td><button class="btn" ng-click="RemoveRatings(this)" ng-model="RemoveRatings"><i class="icon-remove"></i></button></td></tr>';
                $('#tblAdditionalRatingbody').html($compile($scope.RatingsHtml)($scope));
                $scope.AddRatings1();
            }
        }
        $scope.AdditionalObjective.ngObjectiveOwner = $scope.AdditionalobjOwnerName;
        $scope.MemberLists.push({
            "TID": $scope.count,
            "UserEmail": $scope.AdditionalobjOwnerEmail,
            "DepartmentName": "-",
            "Title": "-",
            "Roleid": parseInt(1),
            "RoleName": 'Owner',
            "Userid": parseInt($scope.AdditionalobjOwnerID, 10),
            "UserName": $scope.AdditionalobjOwnerName,
            "IsInherited": '0',
            "InheritedFromEntityid": '0'
        });
        $scope.saveAdditionalobjectiveEntity = function () {
            $("#btnTempadditionalobj").click();
            if ($("#AdditionalObjectiveTablMetadata .error").length > 0) {
                return false;
            }
            if ($scope.AdditionalObjective.ngObjectiveType == "4") {
                if ($scope.ratingItems[$scope.ratingItems.length - 1].ObjRatingText == "") {
                    bootbox.alert($translate.instant('LanguageContents.Res_1918.Caption'));
                    return false;
                }
            }
            $("#AdditionalObjectiveWizardSave").attr('disabled', 'disabled');
            var additionalObjectiveObj = {};
            additionalObjectiveObj.EntityID = parseInt($stateParams.ID, 10);
            additionalObjectiveObj.EntityTypeID = SystemDefinedEntityTypes.AdditionalObjective;
            additionalObjectiveObj.ObjectiveTypeID = $scope.AdditionalObjective.ngObjectiveType;
            additionalObjectiveObj.AdditionalObjectiveMembers = $scope.MemberLists;
            additionalObjectiveObj.Name = $scope.AdditionalObjective.ngObjectiveName;
            if ($scope.AdditionalObjective.ngObjectiveType == 2) {
                additionalObjectiveObj.Instruction = $scope.AdditionalObjective.ngObjectiveDescription;
                additionalObjectiveObj.IsEnableFeedback = $scope.AdditionalObjective.ngObjectivenonNumericEnableCommentsCheck;
                additionalObjectiveObj.UnitId = parseInt($scope.AdditionalObjective.ngObjectivenonNumericUnits, 10);
                $scope.AdditionalObjPlannedTarget = 0;
                $scope.AdditionalObjTargetOutcome = 0;
                if ($scope.AdditionalObjective.ngObjectivePlannedTarget != 0) $scope.AdditionalObjPlannedTarget = $scope.AdditionalObjective.ngObjectivePlannedTarget.replace(/\s/g, '');
                else $scope.AdditionalObjPlannedTarget = 0;
                if ($scope.AdditionalObjective.ngObjectiveTargetoutcome != 0) $scope.AdditionalObjTargetOutcome = $scope.AdditionalObjective.ngObjectiveTargetoutcome.replace(/\s/g, '');
                else $scope.AdditionalObjTargetOutcome = 0;
                additionalObjectiveObj.PlannedTarget = $scope.AdditionalObjPlannedTarget;
                additionalObjectiveObj.TargetOutCome = $scope.AdditionalObjTargetOutcome;
                additionalObjectiveObj.RatingObjective = 0;
                additionalObjectiveObj.Comments = '';
                additionalObjectiveObj.Fulfillment = 0;
                additionalObjectiveObj.ObjectiveStatus = $scope.AdditionalObjective.ngObjectiveStatus;
                additionalObjectiveObj.Ratings = [];
            }
            if ($scope.AdditionalObjective.ngObjectiveType == 3) {
                additionalObjectiveObj.Instruction = $scope.AdditionalObjective.ngObjectiveDescription;
                additionalObjectiveObj.IsEnableFeedback = $scope.AdditionalObjective.ngObjectiveQulaitativeEnableCommentsCheck;
                additionalObjectiveObj.UnitId = 0;
                additionalObjectiveObj.PlannedTarget = 0;
                additionalObjectiveObj.TargetOutCome = 0;
                additionalObjectiveObj.RatingObjective = 0;
                additionalObjectiveObj.Comments = '';
                additionalObjectiveObj.Fulfillment = 0;
                additionalObjectiveObj.ObjectiveStatus = $scope.AdditionalObjective.ngObjectiveStatus;
                additionalObjectiveObj.Ratings = [];
            }
            if ($scope.AdditionalObjective.ngObjectiveType == 4) {
                additionalObjectiveObj.Instruction = $scope.AdditionalObjective.ngObjectiveDescription;
                additionalObjectiveObj.IsEnableFeedback = $scope.AdditionalObjective.ngObjectiveRatingEnableCommentsCheck;
                additionalObjectiveObj.UnitId = 0;
                additionalObjectiveObj.PlannedTarget = 0;
                additionalObjectiveObj.TargetOutCome = 0;
                additionalObjectiveObj.RatingObjective = 0;
                additionalObjectiveObj.Comments = '';
                additionalObjectiveObj.Fulfillment = 0;
                additionalObjectiveObj.ObjectiveStatus = $scope.AdditionalObjective.ngObjectiveStatus;
                additionalObjectiveObj.Ratings = [];
                for (var i = 0; i < $scope.ratingItems.length; i++) {
                    additionalObjectiveObj.Ratings.push($scope.ratingItems[i].ObjRatingText);
                }
            }
            PlanobjectiveService.InsertAdditionalObjective(additionalObjectiveObj).then(function (additionalObjResult) {
                $('#additionalObjectiveEntityControls').modal('hide');
                if (additionalObjResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4277.Caption'));
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4070.Caption'));
                    $scope.GettingAddtionalObjectives();
                }
            });
        }

        function SetCursorPosition(ctrID, cursorPosition) {
            ctrID.caret(cursorPosition);
        }

        function FinancialValidation(event) {
            if (event.shiftKey == true) {
                event.preventDefault();
            }
            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 189 || event.keyCode == 190 || event.key === "-") {
                return true;
            } else {
                event.preventDefault();
            }
        };

        function CurrencyFormat(srcNumber) {
            var retCommaString = '';
            var getstr = srcNumber + '';
            if (getstr.match(" ")) return getstr;
            var count = 0;
            var commaString = '';
            for (var x = getstr.length - 1; x >= 0; x--) {
                if (count == 3) {
                    commaString = commaString + ' ';
                    count = 0;
                }
                commaString = commaString + getstr.charAt(x);
                count++;
            }
            for (var j = commaString.length - 1; j >= 0; j--) {
                retCommaString = retCommaString + commaString.charAt(j);
            }
            return (retCommaString);
        }

        function handleTextChangeEvents() {
            $('#ngObjectivePlannedTarget ,#ngObjectiveTargetoutcome ,#AdditionalNonNumObjectivePlannedtarget ,#AdditionalNonNumObjectiveTargetoutcome').autoNumeric('init', $scope.DecimalSettings['ObjectiveAutoNumeric']);
        }
        $scope.createadditionalobjectivepopupClose = function () { }
        $scope.$on("$destroy", function () {
            $timeout.cancel(timeoutvariable);
            var additionalDetailTabValues, totalWizardSteps1, currentWizardStep, totalWizardSteps, i, j = null;
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.detail.section.additionalobjectiveCtrl']"));
        });
    }
    app.controller("mui.planningtool.default.detail.section.additionalobjectiveCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$stateParams', '$window', '$compile', '$translate', 'PlanobjectiveService', muiplanningtooldefaultdetailsectionadditionalobjectiveCtrl]);
})(angular, app);