﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.User.Interface
{
    public interface IInsertUserDetails
    {
        #region Public Properties
      
        int Id
        {
            get;
            set;
        }
        int UserID
        {
            set;
            get;
        }
        string Email
        {
            get;
            set;
        }
         byte[] Password
        {
            get;
            set;
        }

         string PasswordSalt
        {
            get;
            set;
        }
        DateTime CreationTime
        {
            get;
            set;
        }
        string IPAddress
        {
            get;
            set;
        }
        string IPNumber
        {
            get;
            set;
        }
         string OS
        {
            get;
            set;
        }
   
    #endregion
    }
}
