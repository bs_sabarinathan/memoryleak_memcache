﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BrandSystems.Marcom.Core.Report.Interface;
using BrandSystems.Marcom.Metadata.Interface;
using Marcom.API.Authorization;
using Marcom.API.Models;
using Marcom.API.Services;
using Newtonsoft.Json.Linq;
using Marcom.API.Services.Financials;
using Marcom.API.Models.Financials;
using System.Data;
using Marcom.API.Services.Tasks;


namespace Marcom.API.Controllers
{
    [AuthenticateTokenAttribute]
    public class TaskV1Controller : ApiController
    {
        #region MemberVariables

        private TaskRepositoryV1 TaskRepository;

        #endregion

        #region ConstructorLogic
        /// <summary>
        /// Constructer logic
        /// </summary>
        public TaskV1Controller()
        {
            this.TaskRepository = new TaskRepositoryV1();
        }

        #endregion

        #region  API-Actions

        /// <summary>
        /// This will get the task of this user.
        /// </summary>
        [HttpGet]
        [Queryable]
        public dynamic GetMyTask(int FilterByentityID,int StartRowno,int MaxRowNo,int AssignRole,int UserId)
        {
            try
            {
                return TaskRepository.GetMytasks(FilterByentityID, StartRowno, MaxRowNo, AssignRole, UserId);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
      
        [HttpPost]
        public int ReinitiateRejectedTask(JObject jObj)
        {
            try
            {
                return TaskRepository.ReinitiateRejectedTask(jObj);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        //public JObject CurrencyConverter(JObject curr)
        //{

        //}


        #endregion

    }
}
