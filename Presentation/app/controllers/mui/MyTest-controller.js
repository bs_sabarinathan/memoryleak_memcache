﻿

(function (ng, app) {

    "use strict";

    app.controller(
		"mui.MyTestCtrl",
		function ($scope, $timeout, $http, requestContext, _, $compile, $resource, $cookies, $routeParams, $location, $window) {


		    // --- Initialize. ---------------------------------- //

		    //-------------- user defined functions ---------

		    // Get the render context local to this controller (and relevant params).
		    var renderContext = requestContext.getRenderContext("mui.MyTest");

		    $scope.subview = renderContext.getNextSection();


		    // --- Bind To Scope Events. ------------------------ //


		    // I handle changes to the request context.
		    $scope.$on(
				"requestContextChanged",
				function () {

				    // Make sure this change is relevant to this controller.
				    if (!renderContext.isChangeRelevant()) {

				        return;

				    }

				    // Update the view that is being rendered.
				    $scope.subview = renderContext.getNextSection();

				}
			);
		}
	);



    var myApp = angular.module('myApp', []);


})(angular, app);