using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

	/// <summary>
	/// TreeLevelDao object for table 'MM_TreeLevel'.
	/// </summary>
	
	public partial class TreeLevelDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
        protected int _level;
		protected string _levelname;
		protected int _attributeid;
        protected bool _ispercentage;
		
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public TreeLevelDao() {}

        public TreeLevelDao(int pId, int pLevel, string pLevelname, int pAttributeid,bool pIsPercentage)
		{
			this._id = pId; 
			this._level = pLevel; 
			this._levelname = pLevelname; 
			this._attributeid = pAttributeid; 
            this._ispercentage = pIsPercentage;
		}

        public TreeLevelDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int Level
		{
			get { return _level; }
			set {_bIsChanged |= (_level != value);  _level = value; }
			
		}

        public virtual string LevelName
		{
			get { return _levelname; }
			set 
			{
			  if (value != null && value.Length > 500)
			    throw new ArgumentOutOfRangeException("Levelname", "Levelname value, cannot contain more than 500 characters");
              _bIsChanged |= (_levelname != value);
              _levelname = value; 
			}
			
		}

        public virtual int AttributeID
		{
			get { return _attributeid; }
            set { _bIsChanged |= (_attributeid != value); _attributeid = value; }
			
		}

        public virtual bool IsPercentage
        {
            get { return _ispercentage; }
            set { _bIsChanged |= (_ispercentage != value); _ispercentage = value; }

        }


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string TreeLevelDao = "TreeLevelDao";			
			public const string Id = "Id";
            public const string Level = "Level";
            public const string LevelName = "LevelName";
            public const string AttributeID = "AttributeID";			
            public const string IsPercentage = "IsPercentage";	
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string TreeLevelDao = "TreeLevelDao";
            public const string Id = "Id";
            public const string Level = "Level";
            public const string LevelName = "LevelName";
            public const string AttributeID = "AttributeID";		
            public const string IsPercentage = "IsPercentage";	
		}

		#endregion
		
		
	}
	
}
