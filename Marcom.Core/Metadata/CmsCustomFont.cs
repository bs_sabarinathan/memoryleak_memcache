﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class CmsCustomFont : ICmsCustomFont
    {
        #region Member Variables

	protected int _id;
        protected int _providerid;
        protected string _kitid;
        protected string _name;

		
		#endregion
		
		#region Constructors
		public CmsCustomFont() {}

        public CmsCustomFont(int pId, int pProviderId, string pKitId, string pName)
		{
			this._id = pId;
            this._providerid = pProviderId;
            this._kitid = pKitId;
            this._name = pName; 
		}

        public CmsCustomFont(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}

        public string Name
		{
            get { return _name; }
			set 
			{
			  if (value != null && value.Length > 250)
                  throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
              _name = value; 
			}
			
		}

        public int ProviderID
        {
            get { return _providerid; }
            set { _providerid = value; }
        }

        public string KitID
		{
            get { return _kitid; }
            set { _kitid = value; }
		}

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            CmsCustomFont castObj = null;
			try
			{
                castObj = (CmsCustomFont)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
    }
}
