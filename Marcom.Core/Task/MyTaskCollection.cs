﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Task.Interface;

namespace BrandSystems.Marcom.Core.Task
{
    public class MyTaskCollection : IMyTaskCollection
    {

        public string FilterHeaderName
        {
            get;
            set;

        }

        public IList<MyTask> TaskCollection
        {
            get;
            set;
        }

        public string EntityColorCode
        {
            get;
            set;

        }

        public string EntityShortDescription
        {
            get;
            set;

        }
    }
}
