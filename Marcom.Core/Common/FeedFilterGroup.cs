using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// FeedTemplate object for table 'CM_Feed_Template'.
	/// </summary>
	
	internal class FeedFilterGroup : IFeedFilterGroup 
	{
		#region Member Variables

		protected int _id;
        protected string _feedgroup;
		protected string _template;
		
		
		#endregion
		
		#region Constructors
		public FeedFilterGroup() {}

        public FeedFilterGroup(string pTemplate, string pFeedGroup)
		{
			this._template = pTemplate; 
            this._feedgroup = pFeedGroup; 
        }

        public FeedFilterGroup(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
	
		public string Template
		{
			get { return _template; }
			set 
			{		 
			  _template = value; 
			}
			
		}
        	
		public string FeedGroup
		{
			get { return _feedgroup; }
			set 
			{		 
			  _feedgroup = value; 
			}
			
		}
		

		
		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			FeedTemplate castObj = null;
			try
			{
				castObj = (FeedTemplate)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
