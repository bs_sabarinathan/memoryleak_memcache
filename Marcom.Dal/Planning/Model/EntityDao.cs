using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Dal.Access.Model;


namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// EntityDao object for table 'PM_Entity'.
    /// </summary>

    public partial class EntityDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _parentid;
        protected int _typeid;
        protected bool _active;
        protected string _uniquekey;
        protected int _entityid;
        protected bool _islock;
        protected string _name;
        protected int _version;
        protected int _level;
        protected int _activeEntityStateId;
        protected int _entityStateId;
        protected bool _enabledisableworkflow;
        protected IList<EntityRoleUserDao> _entitymembers;
        protected IList<EntityCostReleationsDao> _entitycostcenters;
        protected EntityPresentationDao _presentation;
        protected IList<EntityPeriodDao> _periods;
        protected IList<FinancialDao> _financials;
        protected IList<FundingRequestDao> _fundingrequests;
        protected ObjectiveDao _objectives;
        protected CostCenterDao _costcenter;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public EntityDao() { }

        public EntityDao(int pId, int pParentid, int pTypeid, bool pActive, string pUniqueKey, int pEntityid, bool pIsLock, string pName, int pVersion, int pLevel, int pActiveEntityStateID, int pEntityStateID,bool pEnableDisableWorkflow)
        {
            this._id = pId;
            this._parentid = pParentid;
            this._typeid = pTypeid;
            this._active = pActive;
            this._uniquekey = pUniqueKey;
            this._entityid = pEntityid;
            this._islock = pIsLock;
            this._name = pName;
            this._version = pVersion;
            this._level = pLevel;
            this._activeEntityStateId = pActiveEntityStateID;
            this._entityStateId = pEntityStateID;
            this._enabledisableworkflow = pEnableDisableWorkflow;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int Parentid
        {
            get { return _parentid; }
            set { _bIsChanged |= (_parentid != value); _parentid = value; }

        }

        public virtual int Typeid
        {
            get { return _typeid; }
            set { _bIsChanged |= (_typeid != value); _typeid = value; }

        }

        public virtual bool Active
        {
            get { return _active; }
            set { _bIsChanged |= (_active != value); _active = value; }

        }

        public virtual string UniqueKey
        {
            get { return _uniquekey; }
            set
            {
                if (value != null && value.Length > 450)
                    throw new ArgumentOutOfRangeException("UniqueKey", "UniqueKey value, cannot contain more than 450 characters");
                _bIsChanged |= (_uniquekey != value);
                _uniquekey = value;
            }

        }

        public virtual int EntityID
        {
            get { return _entityid; }
            set { _bIsChanged |= (_entityid != value); _entityid = value; }

        }

        public virtual bool IsLock
        {
            get { return _islock; }
            set { _bIsChanged |= (_islock != value); _islock = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
                _bIsChanged |= (_name != value);
                _name = value;
            }

        }

        public virtual int Version
        {
            get { return _version; }
            set { _bIsChanged |= (_version != value); _version = value; }

        }

        public virtual int Level
        {
            get { return _level; }
            set { _bIsChanged |= (_level != value); _level = value; }

        }

        public virtual int ActiveEntityStateID
        {
            get { return _activeEntityStateId; }
            set { _bIsChanged |= (_activeEntityStateId != value); _activeEntityStateId = value; }

        }
        public virtual int EntityStateID
        {
            get { return _entityStateId; }
            set { _bIsChanged |= (_entityStateId != value); _entityStateId = value; }

        }
        public virtual bool EnableDisableWorkflow
        {
            get { return _enabledisableworkflow; }
            set { _bIsChanged |= (_enabledisableworkflow != value); _enabledisableworkflow = value; }

        }
        public virtual IList<EntityRoleUserDao> EntityMembers
        {
            get { return _entitymembers; }
            set { _bIsChanged |= (_entitymembers != value); _entitymembers = value; }

        }
        public virtual IList<EntityCostReleationsDao> EntityCostcenters
        {
            get { return _entitycostcenters; }
            set { _bIsChanged |= (_entitycostcenters != value); _entitycostcenters = value; }
        }
        public virtual EntityPresentationDao Presentation
        {
            get { return _presentation; }
            set { _bIsChanged |= (_presentation != value); _presentation = value; }

        }

        public virtual IList<EntityPeriodDao> Periods
        {
            get { return _periods; }
            set { _bIsChanged |= (_periods != value); _periods = value; }

        }

        public virtual IList<FinancialDao> Financials
        {
            get { return _financials; }
            set { _bIsChanged |= (_financials != value); _financials = value; }

        }

        public virtual IList<FundingRequestDao> FundingRequests
        {
            get { return _fundingrequests; }
            set { _bIsChanged |= (_fundingrequests != value); _fundingrequests = value; }

        }

        public virtual ObjectiveDao Objectives
        {
            get { return _objectives; }
            set { _bIsChanged |= (_objectives != value); _objectives = value; }

        }
        public virtual CostCenterDao Costcenter
        {
            get { return _costcenter; }
            set { _bIsChanged |= (_costcenter != value); _costcenter = value; }
        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string EntityDao = "EntityDao";
            public const string Id = "Id";
            public const string Parentid = "Parentid";
            public const string Typeid = "Typeid";
            public const string Active = "Active";
            public const string UniqueKey = "UniqueKey";
            public const string EntityID = "EntityID";
            public const string IsLock = "IsLock";
            public const string Name = "Name";
            public const string Version = "Version";
            public const string Level = "Level";
            public const string ActiveEntityStateID = "ActiveEntityStateID";
            public const string EntityStateID = "EntityStateID";
            public const string EnableDisableWorkflow = "EnableDisableWorkflow";
            public const string Presentation = "Presentation";
            public const string EntityMembers = "EntityMembers";
            public const string EntityCostcenters = "EntityCostcenters";
            public const string Periods_EntityPeriodCollection = "Periods_EntityPeriodCollection";
            public const string Periods = "Periods";
            public const string Financials_FinancialCollection = "Financials_FinancialCollection";
            public const string Financials = "Financials";
            public const string FundingRequests_FundingRequestCollection = "FundingRequests_FundingRequestCollection";
            public const string FundingRequests = "FundingRequests";
            public const string Objectives = "Objectives";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string EntityDao = "EntityDao";
            public const string Id = "Id";
            public const string Parentid = "Parentid";
            public const string Typeid = "Typeid";
            public const string Active = "Active";
            public const string UniqueKey = "UniqueKey";
            public const string EntityID = "EntityID";
            public const string IsLock = "IsLock";
            public const string Name = "Name";
            public const string Version = "Version";
            public const string Level = "Level";
            public const string ActiveEntityStateID = "ActiveEntityStateID";
            public const string EntityStateID = "EntityStateID";
            public const string EnableDisableWorkflow = "EnableDisableWorkflow";
            public const string EntityMembers = "EntityMembers";
            public const string EntityCostcenters = "EntityCostcenters";
            public const string Presentation = "Presentation";
            public const string Periods_EntityPeriodCollection = "Periods_EntityPeriodCollection";
            public const string Periods = "Periods";
            public const string Financials_FinancialCollection = "Financials_FinancialCollection";
            public const string Financials = "Financials";
            public const string FundingRequests_FundingRequestCollection = "FundingRequests_FundingRequestCollection";
            public const string FundingRequests = "FundingRequests";
            public const string Objectives = "Objectives";
        }

        #endregion


    }

}
