﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    public interface ISearchEntity
    {
        #region Public Properties

        string ID
        {
            set;
            get;
        }
        int ModuleID
        {
            set;
            get;
        }
        int TypeID
        {
            set;
            get;
        }
        int TypeCategoryID
        {
            set;
            get;
        }
        string Name
        {
            set;
            get;
        }
        string Path
        {
            set;
            get;
        }
        string Description
        {
            set;
            get;
        }
        string ThumbnailUrl
        {
            set;
            get;
        }
        List<int> UserIDs
        {
            set;
            get;
        }
        //List<KeyValuePair<string, string>> Metadata = new List<KeyValuePair<string, string>>()
        //{

        //};


        #endregion
    }
}
