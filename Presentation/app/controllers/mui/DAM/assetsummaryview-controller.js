﻿(function (ng, app) {
    "use strict";

    function muiDAMassetsummaryCtrl($window, $location, $timeout, $scope, $cookies, $resource, $stateParams, AssetListService, $compile) {
        var localfolderid = 0;
       // $scope.EntityID = parseInt($stateParams.ID, 10);

        $scope.userpublishaccess = false;
        $scope.IsPublish = false;
        userpublishaccess($stateParams.ID);
        /******** Current user publishacces ********/
        function userpublishaccess(entityID) {
            AssetListService.userpublishaccess(entityID).then(function (result) {
                $scope.userpublishaccess = result.Response;
                $scope.IsPublish = result.Response;
            });
        }

  

        if ($scope.selectedTree.id != undefined) {
            if ($scope.selectedTree.id == 0) {
                $('#assetsummaryview').removeClass('assetsummaryscroll');
                $('#assetsummaryview').addClass('assetsummaryscroll');
            } else {
                $('#assetsummaryview').removeClass('assetsummaryscroll');
                localfolderid = $scope.selectedTree.id;
            }
        }
        if ($("#icon_selectassets").hasClass("icon-check")) {
            $("#icon_selectassets").removeClass("icon-check");
            $("#icon_selectassets").addClass("icon-unchecked");
        }
        $scope.$on('LoadSummaryAssetView', function (event, folderID, EID, view) {
            var currentpath = $location.path().replace('/', '');
            var test = currentpath;
            test = test.split("/");
            if (test[test.length - 1] == "attachment") {
                if ($scope.SelectAllAssetsObj.checkselectall) {
                    $scope.$emit('clickcheckselectall', 1);
                }
            }
            localfolderid = folderID;
            if (test[test.length - 1] == "task" || folderID == 0) {
                $('#assetsummaryview').removeClass('assetsummaryscroll');
                $('#assetsummaryview').addClass('assetsummaryscroll');
            } else {
                $('#assetsummaryview').removeClass('assetsummaryscroll');

            }
            AssetListService.GetEntityAsset(folderID, EID, view, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo, false).then(function (data) {
                var res = [];
                res = data.Response;
                LoadView(res);
                loadScrollSettings();
            });
        });
        if ($scope.selectedTree.id != undefined) {
            AssetListService.GetEntityAsset($scope.selectedTree.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo, false).then(function (data) {
                var res = [];
                res = data.Response;
                LoadView(res);
                loadScrollSettings();
            });
        }
        var DynamicData = [];
        $scope.AssetListSelection.AssetSelection = {};
        $scope.AssetListSelection.AssetChkClass = {};
        $scope.AssetListSelectionClass.AssetSelectionClass = {};
        $scope.DownloadFileObj.DownloadFilesArr = [];
        $scope.AssetFilesObj.AssetTaskObj = [];

        function LoadView(res) {
            var html = '';
            if ($scope.AssetFilesObj.PageNo == 1) {
                $("#assetsummaryview").html(html);
                $scope.AssetListSelection.AssetSelection = {};
                $scope.AssetListSelection.AssetChkClass = {};
                $scope.AssetListSelectionClass.AssetSelectionClass = {};
                $scope.DownloadFileObj.DownloadFilesArr = [];
                $scope.AssetFilesObj.AssetFiles = [];
                $scope.AssetFilesObj_Temp.AssetFiles = [];
                $scope.AssetFilesObj.AssetDynamicData = [];
            }
            if (res != null) {
                var assets = [];
                loading = false;
                assets = res[0].AssetFiles;
                for (var j = 0, asset; asset = assets[j++];) {
                    $scope.AssetFilesObj.AssetFiles.push(asset);
                    $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                }
                DynamicData = res[0].AssetDynData;
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    $scope.AssetFilesObj.AssetDynamicData.push(dyn);
                }
                var isselectall = $scope.SelectAllAssetsObj.checkselectall;
                for (var i = 0, asset; asset = assets[i++];) {
                    if (!$scope.SelectAllAssetsObj.checkselectall) {
                        $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                        $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                    } else {
                        $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = true;
                        $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                    }
                    var assetselection = "";
                    assetselection = $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-sum-Selection selected" : "th-sum-Selection";
                    $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                    html += '<div class="th-sum-Box" id="divthsumboxid_' + asset.AssetUniqueID + '"> ';
                    html += ' <div id="select_' + i + '" ng-class=\"AssetListSelectionClass.AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-sum-Selection"> ';
                    html += '            <div class="th-sum-chkBx"> ';
                    html += '                <i class="icon-stop"></i> ';
                    html += '                <label class="checkbox checkbox-custom"> ';
                    html += '                    <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="sumchkBoxIcon_' + asset.AssetUniqueID + '" type="checkbox" /> ';
                    html += '                    <i id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetListSelection.AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i> ';
                    html += '                </label> ';
                    html += '            </div> ';
                    html += '            <div class="th-sum-options">';
                    html += '                <i class="icon-stop"></i>';
                    html += '                <i id="optionsIcon_' + i + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" contextmenuoptions="assetsummaryActionMenu"></i>';
                    html += '            </div>';
                    html += '            <div class="th-sum-Block"> ';
                    html += '                <div class="th-sum-leftSection"> ';
                    html += '                    <div class="th-sum-ImgBlock"> ';
                    html += '                        <div class="th-sum-ImgContainer" ng-click=\"Editasset(' + asset.AssetUniqueID + ')\"> ';
                    var imageBaseUrlcloud = (cloudsetup.storageType == clientFileStoragetype.local) ? TenantFilePath : (cloudsetup.Uploaderurl + "/" + cloudsetup.BucketName + "/" + TenantFilePath).replace(/\\/g, "\/");
                    if (asset.Category == 0) {
                        if (asset.Extension != null) {
                            if (asset.ProcessType > 0) {
                                if (asset.Status == 2) html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + asset.FileGuid + '.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_big/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 1 || asset.Status == 0) html += '<img class="loadingImg" id=' + asset.ActiveFileID + ' data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset(' + asset.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 3) {
                                    html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_big/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                }
                            } else {
                                html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_big/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else {
                            html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                        }
                    } else if (asset.Category == 1) {
                        html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_big/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 2) {
                        html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_big/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                    <div class="th-sum-baiscDetailBlock"> ';
                    html += '                        <div class="th-sum-ActionButtonContainer"> ';
                    html += '                            <span> ';
                    html += '                                <i class="icon-link displayNone" title="Linked"></i> ';
                    html += '                                <i class="icon-ok displayNone" title="Approved and Combined with ..."></i> ';
                    html += '<i class="icon-ok displayNone" title="Approved and Combined with ..."></i>';
                    asset.publishedON = asset.publishedON != "" ? dateFormat(asset.publishedON, $scope.format) : "-";
                    if (asset.Category == 0) {
                        if (asset.IsPublish == true) html += '<i data-publishedon=\"' + asset.publishedON + '\" data-location="publish" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '"  class="icon-arrow-up" title="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up displayNone" title="Published"></i>';
                        if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" class="icon-link" title="Linked"></i>';
                        else html += '<i id="Linkicon_' + asset.AssetID + '" class="icon-link displayNone" title="Linked"></i>';
                    } else {
                        if (asset.IsPublish == true) html += '<i id="Publishicon_' + asset.AssetUniqueID + '" data-location="publish" data-publishedon=\"' + asset.publishedON + '\" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up" title="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetUniqueID + '" class="icon-arrow-up displayNone" title="Published"></i>';
                        if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetUniqueID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" class="icon-link" title="Linked"></i>';
                        else html += '<i id="Linkicon_' + asset.AssetUniqueID + '" class="icon-link displayNone" title="Linked"></i>';
                    }
                    html += '                            </span> ';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-detail-eIconContainer"> ';
                    html += '                               <span class="th-sum-eIcon" my-qtip2 qtip-content="' + asset.Caption + '" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-basicDetails"> ';
                    html += GenereteDetailBlock(asset);
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                </div> ';
                    html += '                <div class="th-sum-rightSection"> ';
                    html += '                    <div class="th-sum-expandedDetailBlock"> ';
                    html += '                        <div class="th-sum-expandedHeader"> ';
                    html += '                            <h4>' + asset.AssetName + '</h4> ';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-expandedDetails"> ';
                    html += '                            <form class="form-horizontal"> ';
                    html += DrawSummaryBlock(asset);
                    html += '                            </form> ';
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                </div> ';
                    html += '            </div> ';
                    html += '        </div> ';
                    html += '    </div> ';
                }
                $("#assetsummaryview").append($compile(html)($scope));
                if ($('#assetview').css('display') == 'none') {
                    $('#assetview').css('display', 'block');
                }
            } else {
                if ($scope.AssetFilesObj.PageNo == 1 && $scope.Dam_Directory.length > 0) {
                    var emptyHtml = "";
                    if ($scope.processingsrcobj.processingplace == "attachment") emptyHtml = '<div class="emptyFolder">This folder is empty.</div>';
                    if ($scope.processingsrcobj.processingplace == "task") emptyHtml = '<div class="emptyFolder">There is no asset.</div>';
                    $("#assetsummaryview").html(emptyHtml);
                    if ($('#assetview').css('display') == 'none') {
                        $('#assetview').css('display', 'block');
                    }
                } else if ($scope.AssetFilesObj.PageNo == 1 && $scope.Dam_Directory.length == 0) {
                    var emptyHtml = "";
                    if ($scope.processingsrcobj.processingplace == "attachment") emptyHtml = '<div class="emptyFolder">There is no folder.</div>';
                    $("#assetsummaryview").html(emptyHtml);
                    if ($('#assetview').css('display') == 'none') {
                        $('#assetview').css('display', 'block');
                    }
                }
            }
        }
        $scope.checkSelection = function (assetid, activefileid, event) {
            var checkbox = event.target;
            var assetselection = "";
            assetselection = checkbox.checked == true ? "th-sum-Selection selected" : "th-sum-Selection";
            $scope.AssetListSelection.AssetChkClass["asset_" + assetid + ""] = true ? "checkbox checked" : "checkbox";
            $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + assetid + ""] = assetselection;
            var res = [];
            res = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                return e.AssetUniqueID == assetid;
            });
            if (res.length > 0) {
                var filename = (res[0].Name != undefined ? res[0].Name : res[0].FileName)
                DownloadAssets(checkbox, assetid, filename, res[0].Extension, res[0].FileGuid, res[0].Category);
            }
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                    return e == assetid;
                });
                if (remainRecord.length == 0) {
                    $scope.AssetFilesObj.AssetSelection.push(assetid);
                }
                if ($scope.AssetFilesObj.AssetSelection.length == $scope.AssetFilesObj.AssetFiles.length) {
                    if ($("#icon_selectassets").hasClass("icon-unchecked")) {
                        $("#icon_selectassets").removeClass("icon-unchecked");
                        $("#icon_selectassets").addClass("icon-check");
                        $scope.selectallassets(0);
                    }
                }
                if (res[0].Category == 0) {
                    AddAttachmentsToTask(assetid, activefileid, true);
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                    return e == assetid;
                });
                if (remainRecord.length > 0) {
                    $scope.AssetFilesObj.AssetSelection.splice($.inArray(assetid, $scope.AssetFilesObj.AssetSelection), 1);
                }
                if (res[0].Category == 0) {
                    AddAttachmentsToTask(assetid, activefileid, false);
                }
            }
            if (!checkbox.checked) {
                $scope.$emit('checkselectall', false);
            }
        }

        function AddAttachmentsToTask(assetid, activefileid, ischecked) {
            if (ischecked == true) {
                var response = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                    return e.ActiveFileID == activefileid
                })[0];
                if (response != null) {
                    data = $.grep($scope.AssetFilesObj.AssetDynamicData, function (e) {
                        return e.ID == assetid;
                    });
                    var flagattTask = $.grep($scope.DamAssetTaskselectionFiles.AssetFiles, function (e) {
                        return e.FileGuid == response.FileGuid
                    })[0];
                    if (flagattTask == null) {
                        $scope.DamAssetTaskselectionFiles.AssetFiles.push(response);
                        if (data[0] != undefined) {
                            $scope.DamAssetTaskselectionFiles.AssetDynamicData.push(data[0]);
                        }
                    }
                }
            } else {
                var response = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                    return e.ActiveFileID == activefileid
                })[0];
                var flagattTask = $.grep($scope.DamAssetTaskselectionFiles.AssetFiles, function (e) {
                    return e.FileGuid == response.FileGuid
                })[0];
                var flagdata = $.grep($scope.DamAssetTaskselectionFiles.AssetDynamicData, function (e) {
                    return e.ID == assetid
                })[0];
                $scope.DamAssetTaskselectionFiles.AssetFiles.splice($.inArray(flagattTask, $scope.DamAssetTaskselectionFiles.AssetFiles), 1);
                $scope.DamAssetTaskselectionFiles.AssetDynamicData.splice($.inArray(flagdata, $scope.DamAssetTaskselectionFiles.AssetDynamicData), 1);
            }
        }

        function parseSize(size) {
            var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"],
				tier = 0;
            while (size >= 1024) {
                size = size / 1024;
                tier++;
            }
            return Math.round(size * 10) / 10 + " " + suffix[tier];
        }

        function DownloadAssets(checkbox, assetid, name, ext, guid, category) {
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.DownloadFileObj.DownloadFilesArr, function (e) {
                    return e.AssetId == assetid;
                });
                if (remainRecord.length == 0) {
                    $scope.DownloadFileObj.DownloadFilesArr.push({
                        AssetId: assetid,
                        Name: name,
                        Fileguid: guid,
                        Extension: ext,
                        Category: category
                    });
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.DownloadFileObj.DownloadFilesArr, function (e) {
                    return e.AssetId == assetid;
                });
                if (remainRecord.length > 0) {
                    $scope.DownloadFileObj.DownloadFilesArr.splice($.inArray(remainRecord[0], $scope.DownloadFileObj.DownloadFilesArr), 1);
                }
            }
        }

        function GenereteDetailBlock(asset) {
            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsAttributes["ThumbnailSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            var html = '';
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    var cls = attr.ID == 68 ? "th-sum-infoMain" : "th-sum-infoSub";
                    if ($scope.AssetFilesObj.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetFilesObj.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = [];
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined) if (data[0]["" + attr.ID + ""] != '1900-01-01') html += '<span class="' + cls.toString() + ' ' + "" + ' control-label">' + data[0]["" + attr.ID + ""] + '</span>';
                        }
                    }
                }
            }
            return html;
        }

        function DrawSummaryBlock(asset) {
            var html = '';
            var attrRelation = [];
            attrRelation = $.grep($scope.SettingsAttributes["SummaryViewSettings"], function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    html += '<div class="control-group"> ';
                    html += '                                    <label class="control-label">' + attr.Caption + '</label> ';
                    html += '                                    <div class="controls"> ';
                    if ($scope.AssetFilesObj.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.AssetFilesObj.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data[0] != undefined) {
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined) html += '<span class="control-label">' + data[0]["" + attr.ID + ""] + '</span>';
                            else html += '<span class="control-label">-</span>';
                        }
                    } else {
                        html += '<label class="control-label">-</label> ';
                    }
                    html += '                                    </div> ';
                    html += '                                </div> ';
                }
            }
            return html;
        }
        $scope.$on('UpdatePublishIcon', function (event, fileids) {
            for (var i = 0; i < fileids.length; i++) {
                $('#Publishicon_' + fileids[i]).removeClass('displayNone')
            }
        });
        $scope.Editasset = function (AssetID) {
            $timeout(function () {
                $scope.$emit('callBackAssetEditinMUI', AssetID, $scope.AssetFilesObj.IsLock, false, 'Summary');
            }, 100);
        }

        function getMoreListItems() {
            $scope.AssetFilesObj.PageNo = $scope.AssetFilesObj.PageNo + 1;
            AssetListService.GetEntityAsset($scope.selectedTree.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo, false).then(function (data) {
                var res = [];
                if (res != null) {
                    if (res.length > 0)
                        loading = false;
                }
                res = data.Response;
                LoadView(res);
            });
            var loader = $("#loader");
            loader.text("Loading New Items");
        }

        function checkListItemContents() {
            getMoreListItems(function () { });
        }
        var loading = false;

        function loadScrollSettings() {
            if (localfolderid != 0) {
                $(window).scroll(function () {

                    if (loading) {
                        return;
                    }

                    if ($(window).scrollTop() > $(document).height() - $(window).height() - 200) {
                        loading = true;

                        checkListItemContents();


                    }

                });
            } else {

                $(".assetsummaryscroll").bind("scroll", function (e) {

                    if ($(this)[0].scrollHeight - $(this).scrollTop() < 2000) {
                        //if (($(this)[0].scrollHeight - 150) < 2000) {
                        checkListItemContents();

                    }
                });
            }


        }

        var previewTimer = $timeout(function () {
            CheckPreviewGenerator()
        }, 5000);

        function CheckPreviewGenerator() {
            getPreviewIDs();
        }

        function getPreviewIDs() {
            var assetids = [];
            for (var i = 0; i < $('.loadingImg').length; i++) {
                assetids.push($('.loadingImg')[i].id);
            }
            if (assetids.length > 0) {
                AssetListService.CheckPreviewGeneratorID(assetids).then(function (result) {
                    var generatedids = result.Response.split(',');
                    var ProcessType = 0;
                    var Srcextn = '.jpg';
                    for (var j = 0; j < generatedids.length; j++) {
                        if (generatedids[j] != "") {
                            var src = $('#' + generatedids[j]).attr('data-src');
                            try {
                                var upd = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                                    return e.ActiveFileID == generatedids[j]
                                });
                                if (upd.length > 0) {
                                    ProcessType = upd[0].ProcessType;
                                    Srcextn = upd[0].Extension;
                                    $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                                        return e.ActiveFileID == generatedids[j]
                                    })[0].Status = 2;
                                }
                                var lightboxid = $.grep($scope.damlightbox.lightboxselection, function (e) {
                                    return e.AssetId == upd[0].AssetID
                                });
                                if (lightboxid.length > 0) {
                                    $.grep($scope.damlightbox.lightboxselection, function (e) {
                                        return e.AssetId == upd[0].AssetID
                                    })[0].Status = 2;
                                    $.grep($scope.damlightbox.lightboxselection, function (e) {
                                        return e.AssetId == upd[0].AssetID
                                    })[0].AssetStatus = 2;
                                }
                            } catch (e) { }
                            var previewimageBaseUrlcloud = (cloudsetup.storageType == clientFileStoragetype.local) ? imageBaseUrlcloud : (cloudsetup.Uploaderurl + "/" + cloudsetup.BucketName + "/" + imageBaseUrlcloud);
                            var extn = '.jpg';
                            if ($('#AssetEditpopup').length > 0 && $('#AssetEditpopup').css('display') == 'block') {
                                if (GlobalAssetEditID == generatedids[j]) {
                                    $('#asseteditimage').attr('src', previewimageBaseUrlcloud + 'DAMFiles/Preview/Big_' + src + '' + extn + '?' + generateUniqueTracker());
                                }
                            }
                            $('#' + generatedids[j]).removeClass('loadingImg');
                            if (ProcessType == 1 || ProcessType == 3) $('#' + generatedids[j]).attr('src', previewimageBaseUrlcloud + 'DAMFiles/Preview/Small_' + src + '' + extn + '?' + generateUniqueTracker());
                            else $('#' + generatedids[j]).attr('src', previewimageBaseUrlcloud + 'DAMFiles/StaticPreview_big/' + Srcextn.substring(1, Srcextn.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker());
                        }
                    }
                });
            }
            PreviewGeneratorTimer = $timeout(function () {
                CheckPreviewGenerator()
            }, 3000);
        }
        $scope.$on("$destroy", function () {
            $(window).unbind('scroll');
            $timeout.cancel(previewTimer);
            $timeout.cancel(PreviewGeneratorTimer);
        });
        $scope.SetAssetActionId = function (assetid) {
            $scope.AssetFilesObj.selectedAssetId = assetid;
            var remainRecord = [];
            remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                return e == $scope.AssetFilesObj.selectedAssetId;
            });
            if (remainRecord.length == 0) {
                $scope.AssetFilesObj.AssetSelection.push($scope.AssetFilesObj.selectedAssetId);
            }
            var remdownloadrecord = [];
            remdownloadrecord = $.grep($scope.DownloadFileObj.DownloadFilesArr, function (e) {
                return e.AssetId == assetid;
            });
            if (remdownloadrecord.length == 0) {
                var res = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                    return e.AssetUniqueID == assetid
                });
                if (res.length > 0) $scope.DownloadFileObj.DownloadFilesArr.push({
                    AssetId: assetid,
                    FileName: res[0].FileName,
                    Fileguid: res[0].FileGuid,
                    Extension: res[0].Extension,
                    Category: res[0].Category
                });
            }
            $('#chkBoxIcon_' + assetid + '').addClass('checked');
            var checkboxchecked = true;
            var assetselection = "";
            assetselection = checkboxchecked == true ? "th-sum-Selection selected" : "th-sum-Selection";
            $scope.AssetListSelection.AssetChkClass["asset_" + assetid + ""] = checkboxchecked == true ? "checkbox checked" : "checkbox";
            $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + assetid + ""] = assetselection;
            $scope.AssetListSelection.AssetSelection["asset_'" + assetid + ""] = checkboxchecked;
            if ($scope.AssetFilesObj.AssetSelection.length == 0 || $scope.AssetFilesObj.AssetSelection.length == 1) {
                if ($scope.AssetFilesObj.AssetSelection[0] == assetid || $scope.AssetFilesObj.AssetSelection.length == 0) {
                    var temparr = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                        return e.Category == 1 && e.AssetUniqueID == assetid;
                    });
                    if (temparr.length > 0) {
                        $scope.IsPublish = false;
                        $scope.UnPublish = false;
                        $scope.showhidepackage = false;
                        $scope.Isblankasset = false;
                        return true;
                    } else {
                        $scope.showhidepackage = true;
                        $scope.Isblankasset = true;
                    }
                }
            }
            if ($scope.AssetFilesObj.AssetSelection.length == 0 || $scope.AssetFilesObj.AssetSelection.length == 1) {
                if ($scope.AssetFilesObj.AssetSelection[0] == assetid || $scope.AssetFilesObj.AssetSelection.length == 0) {
                    if ($('#Publishicon_' + assetid)[0].className == "icon-arrow-up") {
                        $scope.IsPublish = false;
                        if ($scope.userpublishaccess == true)
                        $scope.UnPublish = true;
                        else
                            $scope.UnPublish = false;
                    } else {                       
                        $scope.UnPublish = false;
                    }
                } else {                   
                    $scope.UnPublish = true;
                    $scope.showhidepackage = true;
                }
            } else {                
                $scope.UnPublish = true;
                $scope.showhidepackage = true;
                $scope.Isblankasset = true;
            }
        }
        $scope.CallBackAssetAction = function (assetid, istask, tasktype) {
            var actionName = assetAction.get(assetid);
            if (actionName != undefined) {
                $scope.$emit('DamAssetAction', actionName, istask, tasktype); userpublishaccess($stateParams.ID);
            }
        }
        $scope.$on('loadAssetDetailSection_th_Summary', function (event, assetList) {
            AssetListService.GetAssetDetsforSelectedAssets($scope.selectedTree.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo, false, assetList).then(function (data) {
                var res = [];
                res = data.Response;
                LoadViewDetails(res);
            });
        });

        function LoadViewDetails(res) {
            if (res != null) {
                var assets = [];
                loading = false;
                assets = res[0].AssetFiles;
                for (var j = 0, asset; asset = assets[j++];) {
                    $scope.AssetFilesObj.AssetFiles.push(asset);
                    $scope.AssetFilesObj_Temp.AssetFiles.push(asset);
                }
                DynamicData = res[0].AssetDynData;
                for (var k = 0, dyn; dyn = DynamicData[k++];) {
                    $scope.AssetFilesObj.AssetDynamicData.push(dyn);
                }
                var isselectall = $scope.SelectAllAssetsObj.checkselectall;
                for (var i = 0, asset; asset = assets[i++];) {
                    var html = '';
                    if (!$scope.SelectAllAssetsObj.checkselectall) {
                        $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
                        $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
                    } else {
                        $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = true;
                        $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
                    }
                    var assetselection = "";
                    assetselection = $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "th-sum-Selection selected" : "th-sum-Selection";
                    $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
                    html += '<div class="th-sum-Box" id="divthsumboxid_' + asset.AssetUniqueID + '"> ';
                    html += '        <div id="select_' + i + '" ng-class=\"AssetListSelectionClass.AssetSelectionClass.asset_' + asset.AssetUniqueID + '\" class="th-sum-Selection"> ';
                    html += '            <div class="th-sum-chkBx"> ';
                    html += '                <i class="icon-stop"></i> ';
                    html += '                <label class="checkbox checkbox-custom"> ';
                    html += '                    <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetSelection.asset_' + asset.AssetUniqueID + '" id="sumchkBoxIcon_' + asset.AssetUniqueID + '" type="checkbox" /> ';
                    html += '                    <i id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetListSelection.AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i> ';
                    html += '                </label> ';
                    html += '            </div> ';
                    html += '            <div class="th-sum-options">';
                    html += '                <i class="icon-stop"></i>';
                    html += '                <i id="optionsIcon_' + i + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" contextmenuoptions="assetsummaryActionMenu"></i>';
                    html += '            </div>';
                    html += '            <div class="th-sum-Block"> ';
                    html += '                <div class="th-sum-leftSection"> ';
                    html += '                    <div class="th-sum-ImgBlock"> ';
                    html += '                        <div class="th-sum-ImgContainer" ng-click=\"Editasset(' + asset.AssetUniqueID + ')\"> ';
                    if (asset.Category == 0) {
                        if (asset.Extension != null) {
                            if (asset.ProcessType > 0) {
                                if (asset.Status == 2) html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/Preview/Small_' + asset.FileGuid + '.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_big/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 1 || asset.Status == 0) html += '<img class="loadingImg" id=' + asset.ActiveFileID + ' data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset(' + asset.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 3) {
                                    html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_big/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                }
                            } else {
                                html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_big/' + asset.Extension.substring(1, asset.Extension.length).toString().toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else {
                            html += '<img src="' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                        }
                    } else if (asset.Category == 1) {
                        html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_big/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 2) {
                        html += '<img  src="' + imageBaseUrlcloud + 'DAMFiles/StaticPreview_big/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imageBaseUrlcloud + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                    <div class="th-sum-baiscDetailBlock"> ';
                    html += '                        <div class="th-sum-ActionButtonContainer"> ';
                    html += '                            <span> ';
                    html += '                                <i class="icon-link displayNone" title="Linked"></i> ';
                    html += '                                <i class="icon-ok displayNone" title="Approved and Combined with ..."></i> ';
                    html += '<i class="icon-ok displayNone" title="Approved and Combined with ..."></i>';
                    asset.publishedON = asset.publishedON != "" ? dateFormat(asset.publishedON, $scope.format) : "-";
                    if (asset.Category == 0) {
                        if (asset.IsPublish == true) html += '<i data-publishedon=\"' + asset.publishedON + '\" data-location="publish" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '"  class="icon-arrow-up" title="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up displayNone" title="Published"></i>';
                        if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" class="icon-link" title="Linked"></i>';
                        else html += '<i id="Linkicon_' + asset.AssetID + '" class="icon-link displayNone" title="Linked"></i>';
                    } else {
                        if (asset.IsPublish == true) html += '<i id="Publishicon_' + asset.AssetUniqueID + '" data-location="publish" data-publishedon=\"' + asset.publishedON + '\" data-publishedby=\"' + asset.publishedby + '\"  dampublisheddetailtooltip id="Publishicon_' + asset.AssetID + '" class="icon-arrow-up" title="Published"></i>';
                        else html += '<i id="Publishicon_' + asset.AssetUniqueID + '" class="icon-arrow-up displayNone" title="Published"></i>';
                        if (asset.LinkUserDetail != '') html += '<i id="Linkicon_' + asset.AssetUniqueID + '" data-location="link" data-linkdetails=\"' + asset.LinkUserDetail + '\"   dampublisheddetailtooltip id="Linkicon_' + asset.AssetID + '" class="icon-link" title="Linked"></i>';
                        else html += '<i id="Linkicon_' + asset.AssetUniqueID + '" class="icon-link displayNone" title="Linked"></i>';
                    }
                    html += '                            </span> ';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-detail-eIconContainer"> ';
                    html += '                               <span class="th-sum-eIcon" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-basicDetails"> ';
                    html += GenereteDetailBlock(asset);
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                </div> ';
                    html += '                <div class="th-sum-rightSection"> ';
                    html += '                    <div class="th-sum-expandedDetailBlock"> ';
                    html += '                        <div class="th-sum-expandedHeader"> ';
                    html += '                            <h4>' + asset.AssetName + '</h4> ';
                    html += '                        </div> ';
                    html += '                        <div class="th-sum-expandedDetails"> ';
                    html += '                            <form class="form-horizontal"> ';
                    html += DrawSummaryBlock(asset);
                    html += '                            </form> ';
                    html += '                        </div> ';
                    html += '                    </div> ';
                    html += '                </div> ';
                    html += '            </div> ';
                    html += '        </div> ';
                    html += '    </div> ';
                    $('#divthsumboxid_' + asset.AssetUniqueID).html($compile('')($scope));
                    $('#divthsumboxid_' + asset.AssetUniqueID).html($compile(html)($scope));
                }
            }
        };

        $scope.assetMenuActions = function (action) {

            var AssetObj = {};
            AssetObj.IsLock = $scope.AssetFilesObj.IsLock, 
            AssetObj.isNotify = false,
            AssetObj.viewtype = 'Summary'
            $scope.$emit('callBacktoDam', action);
        }
    }
    app.controller("mui.DAM.assetsummaryCtrl", ['$window', '$location', '$timeout', '$scope', '$cookies', '$resource', '$stateParams', 'AssetListService', '$compile', muiDAMassetsummaryCtrl]);
})(angular, app);