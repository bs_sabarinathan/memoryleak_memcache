﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Configuration;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Linq;
using Amazon.S3.Model;
using System.Net;

namespace Presentation
{
    public partial class DownloadAssetMetadata : System.Web.UI.Page
    {
        string TenantFilePath = "";
        int? tenantID;
        protected void Page_Load(object sender, EventArgs e)
        {
            string strPath = null;
            string tenanturl = HttpContext.Current.Request.Url.Host;
            BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
            TenantFilePath = tfp.GetTenantFilePathByHostName(tenanturl);
            tenantID = tfp.GetTenantIDByHostName(tenanturl);

            BrandSystems.Marcom.Core.User.StorageBlock S3Obj = new BrandSystems.Marcom.Core.User.StorageBlock();
            S3Obj = getcloudsettings(tenantID);
            try
            {
                string directoryPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() ;
                string DamDir = directoryPath + "AssetMetdaatDownloads";
                CleanSrcFolder(DamDir);
                DeleteZipFiles(DamDir);
                string nam = Request.QueryString["FileID"].ToLower();
                if (Request.QueryString["Ext"] == ".zip")
                {
                    if (nam.Contains("."))
                    {
                        nam = nam.Substring(0, nam.LastIndexOf('.'));
                        strPath = Server.MapPath("~/AssetMetdaatDownloads/" + Request.QueryString["FileID"].ToLower());
                    }
                    else
                        strPath = Server.MapPath("~/AssetMetdaatDownloads/" + Request.QueryString["FileID"].ToLower() + Request.QueryString["Ext"]);
                }

                if (Request.QueryString["Ext"] == ".xml")
                {
                    if (nam.Contains("."))
                    {
                        nam = nam.Substring(0, nam.LastIndexOf('.'));
                        strPath = Server.MapPath("~/AssetMetdaatDownloads/" + Request.QueryString["FileID"].ToLower());
                    }
                    else
                        strPath = Server.MapPath("~/AssetMetdaatDownloads/" + Request.QueryString["FileID"].ToLower() + "/" + Request.QueryString["FileFriendlyName"]);
                }

                else
                {
                    if (Convert.ToBoolean(Request.QueryString["IsLightboxcropped"]) == true)
                    {
                        if (nam.Contains("."))
                        {
                            nam = nam.Substring(0, nam.LastIndexOf('.'));
                            strPath = Server.MapPath("~/AssetMetdaatDownloads/" + Request.QueryString["FileID"].ToLower());
                        }
                        else
                            strPath = Server.MapPath("~/AssetMetdaatDownloads/" + Request.QueryString["FileID"].ToLower() + Request.QueryString["Ext"]);
                    }
                    else if (nam.Contains("."))
                    {
                        nam = nam.Substring(0, nam.LastIndexOf('.'));
                        strPath = Server.MapPath("~/AssetMetdaatDownloads/" + Request.QueryString["FileID"].ToLower());
                    }
                    else
                        strPath = Server.MapPath("~/AssetMetdaatDownloads/" + Request.QueryString["FileID"].ToLower() + Request.QueryString["Ext"]);
                }

                string fileName = Request.QueryString["FileID"].ToLower();

                if ((fileName != null))
                {
                    if (fileName.Length > 0)
                    {
                        if (S3Obj.storageType == (int)StorageArea.Local)
                        {
                            if (File.Exists(strPath))
                            {
                                DownloadFile(strPath, true);

                            }
                        }
                        else
                        {
                            string amazondownloadfilepath = strPath.Replace(ConfigurationManager.AppSettings["MarcomPresentation"], S3Obj.Uploaderurl + "/" + S3Obj.BucketName + "/").Replace("\\", "/");


                            //Create a stream for the file
                            Stream stream = null;

                            //This controls how many bytes to read at a time and send to the client
                            int bytesToRead = 10000;

                            // Buffer to read bytes in chunk size specified above
                            byte[] buffer = new Byte[bytesToRead];

                            // The number of bytes read
                            try
                            {
                                string downloadTokenValue = Request.QueryString["token"];

                                //Create a WebRequest to get the file
                                HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(amazondownloadfilepath);

                                //Create a response for this request
                                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                                if (fileReq.ContentLength > 0)
                                    fileResp.ContentLength = fileReq.ContentLength;

                                //Get the Stream returned from the response
                                stream = fileResp.GetResponseStream();

                                // prepare the response to the client. resp is the client Response
                                var resp = HttpContext.Current.Response;

                                //Indicate the type of data being sent
                                resp.ContentType = GetMIMEType(Request.QueryString["FileID"].ToLower() + Request.QueryString["Ext"]);

                                //Name the file 
                                resp.AppendCookie(new HttpCookie("fileDownloadToken", downloadTokenValue));
                                string friendlyname = Request.QueryString["FileFriendlyName"];
                                if (friendlyname.Contains("."))
                                {
                                    resp.AddHeader("Content-Disposition", "attachment; filename=\"" + Path.Combine(Request.QueryString["FileFriendlyName"]) + "\"");
                                }
                                else
                                {
                                    resp.AddHeader("Content-Disposition", "attachment; filename=\"" + Path.Combine(Request.QueryString["FileFriendlyName"], Request.QueryString["Ext"]) + "\"");
                                }
                                resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                                int length;
                                do
                                {
                                    // Verify that the client is connected.
                                    if (resp.IsClientConnected)
                                    {
                                        // Read data into the buffer.
                                        length = stream.Read(buffer, 0, bytesToRead);

                                        // and write it out to the response's output stream
                                        resp.OutputStream.Write(buffer, 0, length);

                                        // Flush the data
                                        resp.Flush();

                                        //Clear the buffer
                                        buffer = new Byte[bytesToRead];
                                    }
                                    else
                                    {
                                        // cancel the download if client has disconnected
                                        length = -1;
                                    }
                                } while (length > 0); //Repeat until no data is read
                            }
                            finally
                            {
                                if (stream != null)
                                {
                                    //Close the input stream
                                    stream.Close();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void DownloadFile(string fname, bool forceDownload)
        {

            string fullpath = System.IO.Path.GetFullPath(fname);
            string strFriendlyName = Request.QueryString["FileFriendlyName"];
            string downloadTokenValue = Request.QueryString["token"];

            string name = System.IO.Path.GetFileName(fullpath);
            name = name.Replace(System.IO.Path.GetFileName(fullpath), strFriendlyName);
            string ext = System.IO.Path.GetExtension(fullpath);
            string type = "";

            switch (ext)
            {
                case ".htm":
                case ".html":
                    type = "text/HTML";
                    break;
                case "-":
                    type = "text/HTML";
                    break;
                case ".txt":
                    type = "text/plain";
                    break;
                case ".doc":
                case ".rtf":
                case ".docx":
                    type = "Application/msword";
                    break;
                case ".csv":
                case ".xls":
                case ".xlsx":
                    type = "Application/x-msexcel";
                    break;
                case ".xml":
                    type = "Application/xml";
                    break;
                case ".tiff":
                    type = "image/tiff";
                    break;
                case ".bmp":
                    type = "image/bmp";
                    break;
                case ".jpeg":
                    type = "image/jpeg";
                    break;
                case ".jpg":
                    type = "image/jpeg";
                    break;
                case ".png":
                    type = "image/png";
                    break;
                case ".gif":
                    type = "image/gif";
                    break;
                case ".zip":
                    type = "Application/zip";
                    break;
                default:
                    type = "application/octet-stream";
                    break;
            }

            if ((forceDownload))
            {
                using (FileStream fs = File.Open(fullpath, FileMode.Open))
                {

                    string clength = fs.Length.ToString(CultureInfo.InvariantCulture);
                    HttpResponse response = HttpContext.Current.Response;
                    Response.ClearContent();
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + name);
                    if (fs.Length != -1) Response.AddHeader("Content-Length", clength.ToString());
                    Response.ContentType = type;
                    Response.AppendCookie(new HttpCookie("fileDownloadToken", downloadTokenValue)); //downloadTokenValue will have been provided in the form submit via the hidden input field                    
                    fs.CopyTo(Response.OutputStream);
                    Response.OutputStream.Flush();
                    Response.End();
                    //HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
            }
        }

        public DownloadAssetMetadata()
        {
            Load += Page_Load;
        }

        public void CleanSrcFolder(string path)
        {
            DirectoryInfo di1 = new DirectoryInfo(path);
            FileInfo[] files = di1.GetFiles();
            DirectoryInfo[] diArr = di1.GetDirectories();
            System.DateTime dteDateToDeleteBy = default(System.DateTime);
            dteDateToDeleteBy = DateTime.Today.Date.AddDays(-5);
            foreach (FileInfo file in files)
            {
                try
                {
                    int filedifferentdays = ((TimeSpan)(file.LastAccessTime - dteDateToDeleteBy)).Days;

                    if (filedifferentdays < 0)
                    {
                        file.Attributes = FileAttributes.Normal;
                        System.IO.File.Delete(file.FullName);
                    }


                }
                catch { }
            }
            foreach (DirectoryInfo dri in diArr)
            {
                try
                {
                    int dirdifferentdays = ((TimeSpan)(dri.LastAccessTime - dteDateToDeleteBy)).Days;

                    if (dirdifferentdays < 0)
                    {
                        dri.Delete(true);

                    }


                }
                catch { }

            }
        }
        public void DeleteZipFiles(string path)
        {
            DirectoryInfo di = new DirectoryInfo(path);
            FileInfo[] files = di.GetFiles("*.zip")
                                 .Where(p => p.Extension == ".zip").ToArray();
            foreach (FileInfo file in files)
                try
                {
                    if (file.LastAccessTime < DateTime.Now.AddDays(-5))
                    {
                        file.Attributes = FileAttributes.Normal;
                        System.IO.File.Delete(file.FullName);
                    }
                }
                catch { }

        }

        public BrandSystems.Marcom.Core.User.StorageBlock getcloudsettings(int? tenantID)
        {
            BrandSystems.Marcom.Core.User.StorageBlock amazonInfo = new BrandSystems.Marcom.Core.User.StorageBlock();
            try
            {

                if (tenantID != null)
                {
                    string xmlpath = ConfigurationManager.AppSettings["MarcomPresentation"] + "Tenants\\TenantsInfo.xml";
                    XDocument tenantInfoXdoc = XDocument.Load(xmlpath);

                    var AWSSetup = tenantInfoXdoc.Descendants("Tenants").Elements("Tenant").Where(e => Convert.ToInt32(e.Element("TenantID").Value) == tenantID).Select(s => s.Elements("AWSSetup")).FirstOrDefault();
                    var FileSystemsMode = tenantInfoXdoc.Descendants("Tenants").Elements("Tenant").Where(e => Convert.ToInt32(e.Element("TenantID").Value) == tenantID).Select(s => s.Element("FileSystem")).FirstOrDefault();

                    amazonInfo.AWSAccessKeyID = AWSSetup.Elements("AWSAccessKeyID").FirstOrDefault().Value.ToString();
                    amazonInfo.AWSSecretAccessKey = AWSSetup.Elements("AWSSecretAccessKey").FirstOrDefault().Value.ToString();
                    amazonInfo.BucketName = AWSSetup.Elements("BucketName").FirstOrDefault().Value.ToString();
                    amazonInfo.ServiceURL = AWSSetup.Elements("ServiceURL").FirstOrDefault().Value.ToString();
                    amazonInfo.RegionEndpoint = AWSSetup.Elements("RegionEndpoint").FirstOrDefault().Value.ToString();
                    amazonInfo.Uploaderurl = AWSSetup.Elements("UploaderUrl").FirstOrDefault().Value.ToString();
                    amazonInfo.storageType = Convert.ToInt32(FileSystemsMode.Value);
                }
                return amazonInfo;
            }
            catch
            {
                return amazonInfo;

            }
        }

        public static string GetMIMEType(string fileName)
        {
            //get file extension
            string extension = Path.GetExtension(fileName).ToLowerInvariant();

            if (extension.Length > 0 &&
                MIMETypesDictionary.ContainsKey(extension.Remove(0, 1)))
            {
                return MIMETypesDictionary[extension.Remove(0, 1)];
            }
            return "application/unknown";
        }
        private static readonly Dictionary<string, string> MIMETypesDictionary = new Dictionary<string, string>
  {
    {"ai", "application/postscript"},
    {"aif", "audio/x-aiff"},
    {"aifc", "audio/x-aiff"},
    {"aiff", "audio/x-aiff"},
    {"asc", "text/plain"},
    {"atom", "application/atom+xml"},
    {"au", "audio/basic"},
    {"avi", "video/x-msvideo"},
    {"bcpio", "application/x-bcpio"},
    {"bin", "application/octet-stream"},
    {"bmp", "image/bmp"},
    {"cdf", "application/x-netcdf"},
    {"cgm", "image/cgm"},
    {"class", "application/octet-stream"},
    {"cpio", "application/x-cpio"},
    {"cpt", "application/mac-compactpro"},
    {"csh", "application/x-csh"},
    {"css", "text/css"},
    {"dcr", "application/x-director"},
    {"dif", "video/x-dv"},
    {"dir", "application/x-director"},
    {"djv", "image/vnd.djvu"},
    {"djvu", "image/vnd.djvu"},
    {"dll", "application/octet-stream"},
    {"dmg", "application/octet-stream"},
    {"dms", "application/octet-stream"},
    {"doc", "application/msword"},
    {"docx","application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
    {"dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template"},
    {"docm","application/vnd.ms-word.document.macroEnabled.12"},
    {"dotm","application/vnd.ms-word.template.macroEnabled.12"},
    {"dtd", "application/xml-dtd"},
    {"dv", "video/x-dv"},
    {"dvi", "application/x-dvi"},
    {"dxr", "application/x-director"},
    {"eps", "application/postscript"},
    {"etx", "text/x-setext"},
    {"exe", "application/octet-stream"},
    {"ez", "application/andrew-inset"},
    {"gif", "image/gif"},
    {"gram", "application/srgs"},
    {"grxml", "application/srgs+xml"},
    {"gtar", "application/x-gtar"},
    {"hdf", "application/x-hdf"},
    {"hqx", "application/mac-binhex40"},
    {"htm", "text/html"},
    {"html", "text/html"},
    {"ice", "x-conference/x-cooltalk"},
    {"ico", "image/x-icon"},
    {"ics", "text/calendar"},
    {"ief", "image/ief"},
    {"ifb", "text/calendar"},
    {"iges", "model/iges"},
    {"igs", "model/iges"},
    {"jnlp", "application/x-java-jnlp-file"},
    {"jp2", "image/jp2"},
    {"jpe", "image/jpeg"},
    {"jpeg", "image/jpeg"},
    {"jpg", "image/jpeg"},
    {"js", "application/x-javascript"},
    {"kar", "audio/midi"},
    {"latex", "application/x-latex"},
    {"lha", "application/octet-stream"},
    {"lzh", "application/octet-stream"},
    {"m3u", "audio/x-mpegurl"},
    {"m4a", "audio/mp4a-latm"},
    {"m4b", "audio/mp4a-latm"},
    {"m4p", "audio/mp4a-latm"},
    {"m4u", "video/vnd.mpegurl"},
    {"m4v", "video/x-m4v"},
    {"mac", "image/x-macpaint"},
    {"man", "application/x-troff-man"},
    {"mathml", "application/mathml+xml"},
    {"me", "application/x-troff-me"},
    {"mesh", "model/mesh"},
    {"mid", "audio/midi"},
    {"midi", "audio/midi"},
    {"mif", "application/vnd.mif"},
    {"mov", "video/quicktime"},
    {"movie", "video/x-sgi-movie"},
    {"mp2", "audio/mpeg"},
    {"mp3", "audio/mpeg"},
    {"mp4", "video/mp4"},
    {"mpe", "video/mpeg"},
    {"mpeg", "video/mpeg"},
    {"mpg", "video/mpeg"},
    {"mpga", "audio/mpeg"},
    {"ms", "application/x-troff-ms"},
    {"msh", "model/mesh"},
    {"mxu", "video/vnd.mpegurl"},
    {"nc", "application/x-netcdf"},
    {"oda", "application/oda"},
    {"ogg", "application/ogg"},
    {"pbm", "image/x-portable-bitmap"},
    {"pct", "image/pict"},
    {"pdb", "chemical/x-pdb"},
    {"pdf", "application/pdf"},
    {"pgm", "image/x-portable-graymap"},
    {"pgn", "application/x-chess-pgn"},
    {"pic", "image/pict"},
    {"pict", "image/pict"},
    {"png", "image/png"}, 
    {"pnm", "image/x-portable-anymap"},
    {"pnt", "image/x-macpaint"},
    {"pntg", "image/x-macpaint"},
    {"ppm", "image/x-portable-pixmap"},
    {"ppt", "application/vnd.ms-powerpoint"},
    {"pptx","application/vnd.openxmlformats-officedocument.presentationml.presentation"},
    {"potx","application/vnd.openxmlformats-officedocument.presentationml.template"},
    {"ppsx","application/vnd.openxmlformats-officedocument.presentationml.slideshow"},
    {"ppam","application/vnd.ms-powerpoint.addin.macroEnabled.12"},
    {"pptm","application/vnd.ms-powerpoint.presentation.macroEnabled.12"},
    {"potm","application/vnd.ms-powerpoint.template.macroEnabled.12"},
    {"ppsm","application/vnd.ms-powerpoint.slideshow.macroEnabled.12"},
    {"ps", "application/postscript"},
    {"qt", "video/quicktime"},
    {"qti", "image/x-quicktime"},
    {"qtif", "image/x-quicktime"},
    {"ra", "audio/x-pn-realaudio"},
    {"ram", "audio/x-pn-realaudio"},
    {"ras", "image/x-cmu-raster"},
    {"rdf", "application/rdf+xml"},
    {"rgb", "image/x-rgb"},
    {"rm", "application/vnd.rn-realmedia"},
    {"roff", "application/x-troff"},
    {"rtf", "text/rtf"},
    {"rtx", "text/richtext"},
    {"sgm", "text/sgml"},
    {"sgml", "text/sgml"},
    {"sh", "application/x-sh"},
    {"shar", "application/x-shar"},
    {"silo", "model/mesh"},
    {"sit", "application/x-stuffit"},
    {"skd", "application/x-koan"},
    {"skm", "application/x-koan"},
    {"skp", "application/x-koan"},
    {"skt", "application/x-koan"},
    {"smi", "application/smil"},
    {"smil", "application/smil"},
    {"snd", "audio/basic"},
    {"so", "application/octet-stream"},
    {"spl", "application/x-futuresplash"},
    {"src", "application/x-wais-source"},
    {"sv4cpio", "application/x-sv4cpio"},
    {"sv4crc", "application/x-sv4crc"},
    {"svg", "image/svg+xml"},
    {"swf", "application/x-shockwave-flash"},
    {"t", "application/x-troff"},
    {"tar", "application/x-tar"},
    {"tcl", "application/x-tcl"},
    {"tex", "application/x-tex"},
    {"texi", "application/x-texinfo"},
    {"texinfo", "application/x-texinfo"},
    {"tif", "image/tiff"},
    {"tiff", "image/tiff"},
    {"tr", "application/x-troff"},
    {"tsv", "text/tab-separated-values"},
    {"txt", "text/plain"},
    {"ustar", "application/x-ustar"},
    {"vcd", "application/x-cdlink"},
    {"vrml", "model/vrml"},
    {"vxml", "application/voicexml+xml"},
    {"wav", "audio/x-wav"},
    {"wbmp", "image/vnd.wap.wbmp"},
    {"wbmxl", "application/vnd.wap.wbxml"},
    {"wml", "text/vnd.wap.wml"},
    {"wmlc", "application/vnd.wap.wmlc"},
    {"wmls", "text/vnd.wap.wmlscript"},
    {"wmlsc", "application/vnd.wap.wmlscriptc"},
    {"wrl", "model/vrml"},
    {"xbm", "image/x-xbitmap"},
    {"xht", "application/xhtml+xml"},
    {"xhtml", "application/xhtml+xml"},
    {"xls", "application/vnd.ms-excel"},                        
    {"xml", "application/xml"},
    {"xpm", "image/x-xpixmap"},
    {"xsl", "application/xml"},
    {"xlsx","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
    {"xltx","application/vnd.openxmlformats-officedocument.spreadsheetml.template"},
    {"xlsm","application/vnd.ms-excel.sheet.macroEnabled.12"},
    {"xltm","application/vnd.ms-excel.template.macroEnabled.12"},
    {"xlam","application/vnd.ms-excel.addin.macroEnabled.12"},
    {"xlsb","application/vnd.ms-excel.sheet.binary.macroEnabled.12"},
    {"xslt", "application/xslt+xml"},
    {"xul", "application/vnd.mozilla.xul+xml"},
    {"xwd", "image/x-xwindowdump"},
    {"xyz", "chemical/x-xyz"},
    {"zip", "application/zip"}
  };

    }
}