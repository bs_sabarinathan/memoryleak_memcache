using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
    /// MailFOOTERDao object for table 'CM_MailFooter'.
	/// </summary>
	
	public partial class MailFooterDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;        
        protected string _body;
        protected string _description;
		
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public MailFooterDao() {}

        public MailFooterDao(int pid, string pBody, string pDescription)
		{
            this._id = pid;			 
			this._body = pBody; 
			this._description = pDescription; 
		}

        public MailFooterDao
            (int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

       

        public virtual string Body
		{
			get { return _body; }
            set { _bIsChanged |= (_body != value); _body = value; }
			
		}

        public virtual string description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 100)
                  throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 100 characters");
              _bIsChanged |= (_description != value);
              _description = value; 
			}
			
		}
		
		


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string MailContentDao = "MailContentDao";			
			public const string Id = "Id";            
            public const string Body = "Body";
            public const string description = "description";			
					
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string MailContentDao = "MailContentDao";
            public const string Id = "Id";            
            public const string Body = "Body";
            public const string description = "description";			
		}

		#endregion
		
		
	}
	
}
