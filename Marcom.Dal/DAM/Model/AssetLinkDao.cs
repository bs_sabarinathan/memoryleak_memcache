﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
   public partial class AssetLinkDao:BaseDao,ICloneable
    {

        #region "Member variables"

        protected int _id;
        protected int _entityID;
        protected int _assetID;
        protected int _targetentityID;
        protected int _linkownerID;
        protected int _folderID;
        protected int _linkedby;
        

        #endregion

        #region "Constructor"

        public AssetLinkDao() { }


        public AssetLinkDao(int pid, int pEntityID, int pAssetID,int pTargetEntityID,int pLinkOwnerID,int pFolderID,int pLinkedBy) 
        {
            this._id = pid;
            this._entityID = pEntityID;
            this._assetID = pAssetID;
            this._targetentityID=pTargetEntityID;
            this._linkownerID = pLinkOwnerID;
            this._folderID = pFolderID;
            this._linkedby = pLinkedBy;
        }

        #endregion


         #region "Public propertiers"

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int EntityID
        {
            get { return _entityID; }
            set
            {
                _entityID = value;
            }

        }


        public virtual int AssetID
        {
            get { return _assetID; }
            set { _assetID = value; }

        }

       public virtual int TargetEntityID
       {
           get { return _targetentityID; }
           set { _targetentityID = value; }
       }

       public virtual int LinkOwnerID
       {
           get { return _linkownerID; }
           set { _linkownerID = value; }
       }

       public virtual int FolderID
       {
           get { return _folderID; }
           set { _folderID = value; }
       }

       public virtual int LinkedBy
       {
           get { return _linkedby; }
           set { _linkedby = value; }
       }
        #endregion


        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion


        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AssetLinkDao = "AssetLinkDao";
            public const string ID = "ID";
            public const string EntityID = "EntityID";
            public const string AssetID = "AssetID";
            public const string TargetEntityID = "TargetEntityID";
            public const string LinkUserID = "LinkUserID";
            public const string FolderID = "FolderID";
            public const string LinkedBy = "LinkedBy";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AssetLinkDao = "AssetLinkDao";
            public const string ID = "ID";
            public const string EntityID = "EntityID";
            public const string AssetID = "AssetID";
            public const string TargetEntityID = "TargetEntityID";
            public const string LinkUserID = "LinkUserID";
            public const string FolderID = "FolderID";
            public const string LinkedBy = "LinkedBy";
        }

        #endregion

    }
}
