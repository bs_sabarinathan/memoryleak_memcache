﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
    public partial class WordTemplateSettingsDao : BaseDao, IDisposable
    {
        #region "Member variables"

        protected int _id;
        protected string _name;
        protected int _categoryid;
        protected string _description;
        protected string _worddocpath;
        protected string _previewimage;

        #endregion


         #region "Constructor"

        public WordTemplateSettingsDao() { }


        public WordTemplateSettingsDao(int pid, string pname, int pcategoryid, string pdescription, string pworddocpath, string ppreviewimage) 
        {
            this._id = pid;
            this._name = pname;
            this._categoryid = pcategoryid;
            this._description = pdescription;
            this._worddocpath = pworddocpath;
            this._previewimage = ppreviewimage;
        }

        #endregion

        #region "Public propertiers"

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }



        public virtual string Name
        {
            get { return _name; }
            set
            {
                _name = value;
            }

        }



        public virtual int CategoryID
        {
            get { return _categoryid; }
            set { _categoryid = value; }

        }


        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }

        }

        public virtual string Worddocpath
        {
            get { return _worddocpath; }
            set { _worddocpath = value; }

        }

        public virtual string PreviewImage
        {
            get { return _previewimage; }
            set { _previewimage = value; }

        }
        #endregion

        #region ICloneable methods

        public void Dispose()
        {
            this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string OptimakerSettingsDao = "WordTemplateSettingsDao";
            public const string ID = "ID";
            public const string CategoryID = "CategoryID";
            public const string Description = "Description";
            public const string Name = "Name";
            public const string Worddocpath = "Worddocpath";
            public const string PreviewImage = "PreviewImage";
         

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string OptimakerSettingsDao = "WordTemplateSettingsDao";
            public const string ID = "ID";
            public const string CategoryID = "CategoryID";
            public const string Description = "Description";
            public const string Name = "Name";
            public const string Worddocpath = "Worddocpath";
            public const string PreviewImage = "PreviewImage";
         
        }

        #endregion
    }
}
