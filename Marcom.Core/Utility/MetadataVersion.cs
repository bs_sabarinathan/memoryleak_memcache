﻿using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.AmazonStorageHelper;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace BrandSystems.Marcom.Utility
{
   public static class MetadataVersion
    {
       
       public static int GetAdminMetadataXmlVersion(int TenantID)
       {
           Guid systemSession = MarcomManagerFactory.GetSystemSession(TenantID);
           IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null,(Guid)systemSession);
           string TenantsFilePath = GetTenantFilePath(TenantID);
           XDocument docx = MarcomCache<XDocument>.ReadXDocument(xmlType.Admin, TenantID);

           var WorkingMetadataVersion = Convert.ToInt32(docx.Root.Elements("Version").Descendants("WorkingVersion").Select(a => a.Attribute("value").Value).First());
           return WorkingMetadataVersion;
       }

       public static string GetTenantFilePath(int TenantID)
       {
           string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() ;
           TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
           XmlDocument xdcDocument = new XmlDocument();
           xdcDocument.Load(TenantInfoPath);
           XmlElement xelRoot = xdcDocument.DocumentElement;
           XmlNodeList xnlNodes = xelRoot.SelectNodes("/Tenants/Tenant[@ID=" + TenantID + "]");
           return xnlNodes.Item(0).ChildNodes[3].InnerText;
       }

    }
}
