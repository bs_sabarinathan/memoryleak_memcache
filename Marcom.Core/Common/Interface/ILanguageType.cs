﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Common;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    public class ILanguageType
    {
        #region Public Properties

        int ID
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        string Descripton
        {
            get;
            set;

        }

        int InheritedFrom
        {
            get;
            set;

        }

        string AddedOn
        {
            get;
            set;

        }

        int PendingTranslation
        {
            get;
            set;
        }

        bool IsDefaultLanguage
        {
            get;
            set;
        }

        string LangKey
        {
            get;
            set;
        }

        string LangContent
        {
            get;
            set;
        }

        int TranslationPending
        {
            get;
            set;
        }

        #endregion
    }
}
