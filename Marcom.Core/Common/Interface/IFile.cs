using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IFile interface for table 'CM_File'.
    /// </summary>
    public interface IFile
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        int VersionNo
        {
            get;
            set;

        }

        string MimeType
        {
            get;
            set;

        }

        string Extension
        {
            get;
            set;

        }


        long Size
        {
            get;
            set;

        }

        int Ownerid
        {
            get;
            set;

        }

        DateTimeOffset CreatedOn
        {
            get;
            set;

        }

        string Checksum
        {
            get;
            set;

        }

        int Moduleid
        {
            get;
            set;

        }

        int Entityid
        {
            get;
            set;

        }

        Guid Fileguid
        {
            get;
            set;

        }

        string strFileID { get; set; }

        string OwnerName
        {
            get;
            set;

        }

        int LinkType { get; set; }
        string Description { get; set; }
        string StrCreatedDate { get; set; }
        string LinkURL { get; set; }
        bool IsExist { get; set; }

        int VersioningFileId
        {
            get;
            set;
        }
        int ActiveFileVersionID
        {
            get;
            set;
        }
        #endregion
    }
}
