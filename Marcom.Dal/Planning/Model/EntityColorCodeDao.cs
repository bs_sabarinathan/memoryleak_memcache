using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// EntityColorCodeDao object for table 'PM_EntityColorCode'.
	/// </summary>
	
	public partial class EntityColorCodeDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected int _entitytypeid;
		protected int _attributeid;
		protected int _optionid;
		protected string _colorcode;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public EntityColorCodeDao() {}
			
		public EntityColorCodeDao(int pEntityTypeid, int pAttributeid, int pOptionid, string pColorCode)
		{
			this._entitytypeid = pEntityTypeid; 
			this._attributeid = pAttributeid; 
			this._optionid = pOptionid; 
			this._colorcode = pColorCode; 
		}
				
		public EntityColorCodeDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual int EntityTypeid
		{
			get { return _entitytypeid; }
			set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }
			
		}
		
		public virtual int Attributeid
		{
			get { return _attributeid; }
			set { _bIsChanged |= (_attributeid != value); _attributeid = value; }
			
		}
		
		public virtual int Optionid
		{
			get { return _optionid; }
			set { _bIsChanged |= (_optionid != value); _optionid = value; }
			
		}
		
		public virtual string ColorCode
		{
			get { return _colorcode; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("ColorCode", "ColorCode value, cannot contain more than 50 characters");
			  _bIsChanged |= (_colorcode != value); 
			  _colorcode = value; 
			}
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string EntityColorCodeDao = "EntityColorCodeDao";			
			public const string Id = "Id";			
			public const string EntityTypeid = "EntityTypeid";			
			public const string Attributeid = "Attributeid";			
			public const string Optionid = "Optionid";			
			public const string ColorCode = "ColorCode";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string EntityColorCodeDao = "EntityColorCodeDao";			
			public const string Id = "Id";			
			public const string EntityTypeid = "EntityTypeid";			
			public const string Attributeid = "Attributeid";			
			public const string Optionid = "Optionid";			
			public const string ColorCode = "ColorCode";			
		}

		#endregion
		
		
	}
	
}
