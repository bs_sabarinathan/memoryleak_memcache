﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
 public   interface IDynamicAttributes
    {
         Guid Id { get; set; }
         int EntityId { get; set; }
        // Iesi.Collections.Generic.ISet<DynamicnewAttributes> Attributes;
          List<KeyValuePair<String, String>> Attributes { get; set; }
    }
}
