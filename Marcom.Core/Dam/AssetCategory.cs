﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Dam.Interface;

namespace BrandSystems.Marcom.Core.Dam
{
    public class AssetCategory : IAssetCategory
    {
        #region "Member variables"

        protected int _id;
        protected string _name;
        protected string _parentkey;
        protected int _level;
        protected int _parentid;
        protected bool _ischecked;

        #endregion

        #region "Constructor"

        public AssetCategory() { }


        public AssetCategory(int pid, string pname, string pparentkey, int plevel, int pparentid, bool pIsChecked)
        {
            this._id = pid;
            this._name = pname;
            this._parentkey = pparentkey;
            this._level = plevel;
            this._parentid = pparentid;
            this._ischecked = pIsChecked;
        }

        #endregion


        #region "Public propertiers"

        public int id
        {
            get { return _id; }
            set { _id = value; }

        }

        public string Caption
        {
            get { return _name; }
            set
            {
                _name = value;
            }

        }

        public string ParentKey
        {
            get { return _parentkey; }
            set { _parentkey = value; }

        }

        public int Level
        {
            get { return _level; }
            set { _level = value; }

        }

        public bool IsChecked
        {
            get { return _ischecked; }
            set { _ischecked = value; }
        }

        public bool IsDeleted
        {
            get;
            set;
        }

        public List<AssetCategory> Children
        {
            get;
            set;
        }

        public int ParentID
        {
            get { return _parentid; }
            set { _parentid = value; }

        }

        #endregion

    }
}
