﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public partial class PreDefinedAttrGroupMultiSelectDao : BaseDao, ICloneable
    {
        #region Member Variables
        protected int _id;
        protected int _attributeid;
        protected int _optionid;
        protected int _groupid;
        protected int _grouprecordid;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion


        #region Constructors
        public PreDefinedAttrGroupMultiSelectDao() { }

        public PreDefinedAttrGroupMultiSelectDao(int pId, int pAttributeid, int pOptionid, int pGroupID, int pGroupRecordID)
        {
            this._id = pId;
            this._optionid = pOptionid;
            this._groupid = pGroupID;
            this._attributeid = pAttributeid;
            this._grouprecordid = pGroupRecordID;
        }


        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int Attributeid
        {
            get { return _attributeid; }
            set { _bIsChanged |= (_attributeid != value); _attributeid = value; }

        }
        public virtual int Optionid
        {
            get { return _optionid; }
            set { _bIsChanged |= (_optionid != value); _optionid = value; }

        }
        public virtual int GroupID
        {
            get { return _groupid; }
            set { _groupid = value; }
        }

        public virtual int GroupRecordID
        {
            get { return _grouprecordid; }
            set { _grouprecordid = value; }
        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string Id = "Id";
            public const string MultiSelectDao = "MultiSelectDao";
            public const string Attributeid = "Attributeid";
            public const string Optionid = "Optionid";
            public const string GroupID = "GroupID";
            public const string GroupRecordID = "GroupRecordID";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string Id = "Id";
            public const string MultiSelectDao = "MultiSelectDao";
            public const string Attributeid = "Attributeid";
            public const string Optionid = "Optionid";
            public const string GroupID = "GroupID";
            public const string GroupRecordID = "GroupRecordID";
        }

        #endregion

    }
}
