///#source 1 1 /app/controllers/mui/planningtool/costcentre/detail/section-controller.js
(function(ng, app) {
    "use strict";

    function muiplanningtoolcostcentredetailsectionCtrl($scope, $sce, $timeout, $http, $compile, $resource, $location, $window, $stateParams, CcsectionService) {
        $scope.CustomTab = {
            Selection: ""
        };
        $scope.TabCollectionsList = null;
        $scope.IsLock = true;
        $scope.EntityLockTask = false;
        CcsectionService.GetLockStatus($stateParams.ID).then(function(entitylockstatus) {
            $scope.IsLock = entitylockstatus.Response.Item1;
        });
        $scope.set_bgcolor = function(clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            } 
            else return ''
        }
        $scope.set_color = function(clr) {
            if (clr != null) return {
                'color': "#" + clr.toString().trim()
            }
            else return ''
        }
        $scope.loadBreadCrum = function(isOnload) {
            $("#plandetailfilterdiv").css('display', 'none')
            if ($("#EntitiesTree li a[data-entityid=" + $stateParams.ID + "]").length == 0 && $scope.Activity.IsActivitySectionLoad == false) {
                $scope.loadcostcentrefromsearchByID($stateParams.ID, "100");
            }
            $("#EntitiesTree li.active").removeClass('active')
            $("#EntitiesTree li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
            $scope.array = [];
            CcsectionService.GetPath($stateParams.ID).then(function(getbc) {
                $scope.array = [];
                for (var i = 0; i < getbc.Response.length; i++) {
                    if (i == (getbc.Response.length - 1)) {
                        var decodedName = $('<div/>').html(getbc.Response[i].Name).text();
                        $scope.array.push({
                            "ID": getbc.Response[i].ID,
                            "Name": decodedName,
                            "UniqueKey": getbc.Response[i].UniqueKey,
                            "ColorCode": getbc.Response[i].ColorCode,
                            "ShortDescription": getbc.Response[i].ShortDescription,
                            "TypeID": getbc.Response[i].TypeID,
                            "mystyle": "999999"
                        });
                        $scope.EntityShortText = getbc.Response[i].ShortDescription;
                        $scope.EntityColorcode = getbc.Response[i].ColorCode;
                        $scope.RootLevelEntityName = decodedName;
                        $scope.RootLevelEntityNameTemp = getbc.Response[i].Name;
                        $window.GlobalUniquekey = getbc.Response[i].UniqueKey;
                        $scope.EntityTypeID = getbc.Response[i].TypeID;
                        if (getbc.Response[i].IsLock == "True") $scope.IsLock = true;
                        else $scope.IsLock = false;
                    } else {
                        var decodedName = $('<div/>').html(getbc.Response[i].Name).text();
                        $scope.array.push({
                            "ID": getbc.Response[i].ID,
                            "Name": decodedName,
                            "UniqueKey": getbc.Response[i].UniqueKey,
                            "ColorCode": getbc.Response[i].ColorCode,
                            "ShortDescription": getbc.Response[i].ShortDescription,
                            "TypeID": getbc.Response[i].TypeID,
                            "mystyle": "0088CC"
                        });
                    }
                }
            });
            if (isOnload == false) {
                if ($scope.subview == "overview") {
                    $scope.$broadcast('LoadCostcentreSectionDetail', $stateParams.ID);
                } else if ($scope.subview == "financial") {
                    $scope.$broadcast('LoadFinancialDetailCC', $stateParams.ID);
                } else if ($scope.subview == "member") {
                    $scope.$broadcast('LoadMembersDetailCC', $stateParams.ID);
                } else if ($scope.subview == "objective") {} else if ($scope.subview == "attachment") {
                    $scope.$broadcast('LoadAttachment', $stateParams.ID);
                } else if ($scope.subview == "presentation") {
                    $scope.$broadcast('LoadPresentationsCC', $stateParams.ID);
                } else if ($scope.subview == "workflow") {
                    $scope.$broadcast('LoadWorkflowCC', $stateParams.ID);
                } else if ($scope.subview == "task") {
                    ResetTaskObjects();
                    $scope.$broadcast('LoadTask', $stateParams.ID);
                }
            }
        }
        $scope.loadBreadCrum(true);
        $scope.load = function(parameters) {
            $scope.LoadTab = function(tab) {
                $window.CurrentEntityTab = "";
                $window.CurrentEntityTab = tab;
                $(this).addClass('active');
                // Put the object into storage
                localStorage.setItem('lasttab', tab);
                $location.path('/mui/planningtool/costcentre/detail/section/' + $stateParams.ID + '/' + tab + '');
            };
        };
        $scope.CustomTabLoad = function(tab, tabUrl, IsSytemDefined, Name, AddEntityID) {
            if (IsSytemDefined == false) {
                $scope.CustomTab.Selection = $sce.trustAsResourceUrl("");
                CcsectionService.GetCustomTabUrlTabsByTypeID(tab, $stateParams.ID).then(function(gettabresult) {
                    if (gettabresult.Response != null) {
                        $scope.CustomTab.Selection = $sce.trustAsResourceUrl(gettabresult.Response);
                    }
                });
                $window.CurrentEntityTab = "";
                $window.CurrentEntityTab = tab;
                $(this).addClass('active');
                $location.path('/mui/planningtool/costcentre/customtab/' + tab + '/' + $stateParams.ID + '');
            } else {
                $scope.LoadTab(Name.toLowerCase());
            }
        }
        $scope.BreadCrumOverview = function(ID) {
            var actname = $('#breadcrumlink').text();
            actname = actname.substring(2, actname.length);
            $window.EntityTypeID = ID;
            $window.SubscriptionEntityTypeID = $('#breadcrumlink').attr('data-typeid');
            $window.GlobalUniquekey = $('#breadcrumlink').attr('data-uniquekey');
            $scope.RootLevelEntityName = actname;
            $scope.EntityShortText = $('#breadcrumlink').attr('data-shortdesc');
            $scope.EntityColorcode = $('#breadcrumlink').attr('data-colorcode');
            $location.path('/mui/planningtool/detail/section/' + ID);
        }
        $scope.TaskExpandedDetails = [];
        $scope.SubLevelTaskExpandedDetails = [];
        $scope.SubLevelTaskLibraryListContainer = {
            SubLevelTaskLibraryList: []
        };
        $scope.RootLevelTaskLibraryListContainer = {
            RootLevelTaskLibraryList: []
        };
        $scope.groupbySubLevelTaskObj = [];
        $scope.ngExpandAll = true;
        $scope.ngExpandCollapseStatus = {
            ngExpandAll: true
        };
        $scope.TaskOrderListObj = {
            TaskSortingStatus: "Reorder task lists",
            TaskSortingStatusID: 1,
            TaskOrderHandle: false,
            SortOrderObj: [],
            UniqueTaskSort: "Reorder task",
            UniqueTaskSortingStatusID: 1,
            UniqueTaskOrderHandle: false,
            UniqueSortOrderObj: []
        };
        $scope.SubLevelEnableObject = {
            Enabled: false,
            SublevelText: "Show task(s) from sub-levels",
            SublevelTextStatus: 1
        };
        GetAllTabCollections();

        function GetAllTabCollections() {
            CcsectionService.GetCustomEntityTabsByTypeID(5, 5, $stateParams.ID, 0).then(function(gettabresult) {
                if (gettabresult.Response != null) {
                    $scope.TabCollectionsList = gettabresult.Response;
                    if ($stateParams.tabID == undefined) {
                        if ($scope.subview == "customtab" || $scope.subview == undefined) {
                            if ($scope.TabCollectionsList[0].IsSytemDefined == false) {
                                $scope.CustomTabLoad($scope.TabCollectionsList[0].Id, $scope.TabCollectionsList[0].ExternalUrl, false, $scope.TabCollectionsList[0].ControleID, $scope.TabCollectionsList[0].AddEntityID);
                            }
                        } else {
                            $scope.LoadTab($scope.subview.toLowerCase());
                        }
                    } else {
                        var currentObj = $.grep($scope.TabCollectionsList, function(item, i) {
                            return item.Id == $stateParams.tabID;
                        });
                        if (currentObj != null) $scope.CustomTabLoad(currentObj[0].Id, currentObj[0].ExternalUrl, currentObj[0].IsSytemDefined, currentObj[0].ControleID, currentObj[0].AddEntityID);
                    }
                }
            });
        }
        $scope.BindClass = function(index, tab, ID) {
            if (index == 0 && $scope.subview == null) {
                if ($scope.subview == null) {
                    var tabid = $scope.TabCollectionsList[index].ControleID;
                    $scope.subview = tabid;
                }
                return "active";
            } else if ($scope.subview == tab) {
                return "active";
            } else if (ID == $stateParams.tabID) {
                return "active";
            }
        }
        ResetTaskObjects();

        function ResetTaskObjects() {
            $scope.GlobalTaskFilterStatusObj = {
                MulitipleFilterStatus: []
            };
            $scope.groups = [];
            $scope.TaskExpandedDetails = [];
            $scope.SubLevelTaskExpandedDetails = [];
            $scope.SubLevelTaskLibraryListContainer = {
                SubLevelTaskLibraryList: []
            };
            $scope.RootLevelTaskLibraryListContainer = {
                RootLevelTaskLibraryList: []
            };
            $scope.groupbySubLevelTaskObj = [];
            $scope.ngExpandAll = true;
            $scope.ngExpandCollapseStatus = {
                ngExpandAll: true
            };
            $scope.TaskOrderListObj = {
                TaskSortingStatus: "Reorder task lists",
                TaskSortingStatusID: 1,
                TaskOrderHandle: false,
                SortOrderObj: [],
                UniqueTaskSort: "Reorder task",
                UniqueTaskSortingStatusID: 1,
                UniqueTaskOrderHandle: false,
                UniqueSortOrderObj: []
            };
            $scope.SubLevelEnableObject = {
                Enabled: false,
                SublevelText: "Show task(s) from sub-levels",
                SublevelTextStatus: 1
            };
        }
        $scope.MainTaskLiveUpdateIndex = 0;
        $scope.subTaskLiveUpdateIndex = 0;
        $scope.$on('LiveTaskListUpdate', function(event, taskkist) {
            if (taskkist.length > 0) {
                $scope.MainTaskLiveUpdateIndex = 0;
                $scope.subTaskLiveUpdateIndex = 0;
                UpdateTaskData();
                UpdateSubLevelTaskData();
            }
        });

        function UpdateTaskData() {
            var tlObj, tasklistList = [];
            tlObj = $scope.LiveTaskListIDCollection.TasklistIDList[$scope.MainTaskLiveUpdateIndex];
            tasklistList = $.grep($scope.TaskExpandedDetails, function(e) {
                return e.ID == tlObj.TaskLiStID;
            });
            if (tasklistList.length > 0) var promise = $http.get('api/task/GetEntityTaskListDetails/' + tlObj.EntityID + '/' + tlObj.TaskLiStID + '').success(function(result) {
                var data = [];
                data = result.Response;
                if (data.length > 0) {
                    tasklistList[0].TaskList = [];
                    for (var j = 0, taskdata; taskdata = data[j++];) {
                        tasklistList[0].TaskList.push(taskdata);
                    }
                    try {
                        var taskListResObj = $.grep($scope.groups, function(e) {
                            return e.ID == tlObj.TaskLiStID && e.EntityParentID == tlObj.EntityID;
                        });
                        if (taskListResObj[0].IsGetTasks == true && taskListResObj[0].IsExpanded == true) {
                            if (tasklistList[0].TaskList.length > 0) {
                                taskListResObj[0].TaskList = tasklistList[0].TaskList;
                                taskListResObj[0].TaskCount = tasklistList[0].TaskList.length;
                            }
                        }
                    } catch (e) {}
                }
                $timeout(function() {
                    $scope.MainTaskLiveUpdateIndex = $scope.MainTaskLiveUpdateIndex + 1;
                    if ($scope.MainTaskLiveUpdateIndex <= $scope.LiveTaskListIDCollection.TasklistIDList.length - 1) UpdateTaskData();
                }, 20);
            });
        }

        function UpdateSubLevelTaskData() {
            var tlObj, tasklistList = [];
            tlObj = $scope.LiveTaskListIDCollection.TasklistIDList[$scope.subTaskLiveUpdateIndex];
            tasklistList = $.grep($scope.SubLevelTaskExpandedDetails, function(e) {
                return e.ID == tlObj.TaskLiStID;
            });
            if (tasklistList.length > 0) var promise = $http.get('api/task/GetEntityTaskListDetails/' + tlObj.EntityID + '/' + tlObj.TaskLiStID + '').success(function(result) {
                var data = [];
                data = result.Response;
                if (data.length > 0) {
                    tasklistList[0].TaskList = [];
                    for (var j = 0, taskdata; taskdata = data[j++];) {
                        tasklistList[0].TaskList.push(taskdata);
                    }
                    try {
                        var EntityTaskObj = [];
                        EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function(e) {
                            return e.EntityID == tlObj.EntityID;
                        });
                        if (EntityTaskObj.length > 0) {
                            var taskObjHolder = $.grep(EntityTaskObj[0].TaskListGroup, function(e) {
                                return e.ID == tlObj.TaskLiStID;
                            });
                            if (taskObjHolder.length > 0) {
                                taskObjHolder[0].TaskList = tasklistList[0].TaskList;
                                taskObjHolder[0].TaskCount = tasklistList[0].TaskList.length;;
                            }
                        }
                    } catch (e) {}
                }
                $timeout(function() {
                    $scope.subTaskLiveUpdateIndex = $scope.subTaskLiveUpdateIndex + 1;
                    if ($scope.subTaskLiveUpdateIndex <= $scope.LiveTaskListIDCollection.TasklistIDList.length - 1) UpdateSubLevelTaskData();
                }, 20);
            });
        }
        $scope.$on("$destroy", function() {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.detail.sectionCtrl']"));
        });
        $timeout(function() {
            $scope.load();
            $('.nav-tabs').tabdrop({
                text: 'More'
            });
        }, 0);
        $(window).on("UpdateEntityName", function(event, optionarray) {
            $scope.RootLevelEntityName = optionarray;
        });
    }
    app.controller("mui.planningtool.costcentre.detail.sectionCtrl", ['$scope', '$sce', '$timeout', '$http', '$compile', '$resource', '$location', '$window', '$stateParams', 'CcsectionService', muiplanningtoolcostcentredetailsectionCtrl]);
})(angular, app);
///#source 1 1 /app/controllers/mui/planningtool/SubEntityCreation-controller.js

(function (ng, app) {
    "use strict";
    //app.controller(
    //	"mui.planningtool.SubEntityCreationCtrl",
    function muiplanningtoolSubEntityCreationCtrl($scope, $location, $resource, $http, $cookies, $compile, $window, $translate, SubentitycreationService, $modalInstance, params) {
        // --- Define Controller Methods. ------------------- //
        var SubentityTypeCreationTimeout = {};
        var model;
        $scope.GetNonBusinessDaysforDatePicker();
        $scope.Calanderopen = function ($event, object, Call_from) {
            $event.preventDefault();
            $event.stopPropagation();
            if (Call_from == "DueDate") {
                object.StartDate = false; object.EndDate = false; object.DateAction = false; object.DueDate = true;
            }
            else if (Call_from == "StartDate") {
                object.EndDate = false; object.DueDate = false; object.DateAction = false; object.StartDate = true;
            }
            else if (Call_from == "EndDate") {
                object.StartDate = false; object.DueDate = false; object.DateAction = false; object.EndDate = true;
            }
            else {
                object.StartDate = false; object.EndDate = false; object.DueDate = false; object.DateAction = true;
            }
        };

        $scope.PeriodCalanderopen = function ($event, item, place) {
            $event.preventDefault();
            $event.stopPropagation();
            if (place == "start") {
                item.calstartopen = true;
                item.calendopen = false;
            }
            else if (place == "end") {
                item.calstartopen = false;
                item.calendopen = true;
            }
        };

        $scope.WizardSteps = [{ "StepId": 1, "StepName": $translate.instant('LanguageContents.Res_251.Caption'), "StepDOMID": "#step1" }];

        $scope.listofValidations = [];
        var remValtimer = "";
        $scope.DynamicAddValidation = function (attrID) {
            var IsValExist = $.grep($scope.listValidationResult, function (e) {
                return parseInt(e[0].split('_')[1]) == attrID;
            });

            if (IsValExist == null || IsValExist.length == 0) {
                var getVal = $.grep($scope.listofValidations, function (e) {
                    return parseInt(e[0].split('_')[1]) == attrID;
                });
                if (getVal != null && getVal.length > 0) {
                    $scope.listValidationResult.push(getVal[0]);

                    SubentityTypeCreationTimeout.btnclick = setTimeout(function () { $("#btnTempSub").click(); }, 100);
                    $("#EntityMetadata").removeClass('notvalidate');
                    $("#EntityMetadata").nod().destroy();
                    if (remValtimer)
                        SubentityTypeCreationTimeout.cancelto = setTimeout.cancel(remValtimer);
                    remValtimer = setTimeout(function () {
                        $("#EntityMetadata").nod($scope.listValidationResult, {
                            'delay': 200,
                            'submitBtnSelector': '#btnTempSub',
                            'silentSubmit': 'true'
                        });

                        SubentityTypeCreationTimeout.btnclick = setTimeout(function () { $("#btnTempSub").click(); }, 100);
                    }, 100);
                }
            }
        }

        $scope.DynamicRemoveValidation = function (attrID) {
            if ($scope.listValidationResult != undefined) {
                var IsValExist = $.grep($scope.listValidationResult, function (e) {
                    return parseInt(e[0].split('_')[1]) == attrID;
                });

                if (IsValExist != null && IsValExist.length > 0) {
                    $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                        return parseInt(e[0].split('_')[1]) != attrID;
                    });

                    SubentityTypeCreationTimeout.btnclick = setTimeout(function () { $("#btnTempSub").click(); }, 100);
                    $("#EntityMetadata").removeClass('notvalidate');
                    $("#EntityMetadata").nod().destroy();
                    if (remValtimer)
                        SubentityTypeCreationTimeout.cancelto = setTimeout.cancel(remValtimer);
                    remValtimer = setTimeout(function () {

                        $("#EntityMetadata").nod($scope.listValidationResult, {
                            'delay': 200,
                            'submitBtnSelector': '#btnTempSub',
                            'silentSubmit': 'true'
                        });

                        SubentityTypeCreationTimeout.btnclick = setTimeout(function () { $("#btnTempSub").click(); }, 100);
                    }, 10);
                }
            }
        }

        $scope.ShowOrHideAttributeToAttributeRelation = function (attrID, attrTypeID) {
            if ($scope.listAttriToAttriResult.length > 0) {
                var Attributetypename = '';
                var relationobj = $.grep($scope.listAttriToAttriResult, function (rel) {
                    return $.inArray(attrID, rel.AttributeRelationID.split(',')) != -1;
                });
                var ID = attrID.split("_");
                if (relationobj != undefined) {
                    if (relationobj.length > 0) {
                        for (var i = 0; i <= relationobj.length - 1; i++) {
                            Attributetypename = '';
                            if (relationobj[i].AttributeTypeID == 3) {
                                Attributetypename = 'ListSingleSelection_' + relationobj[i].AttributeID;
                            }
                            else if (relationobj[i].AttributeTypeID == 4) {
                                Attributetypename = 'ListMultiSelection_' + relationobj[i].AttributeID;
                            }
                            else if (relationobj[i].AttributeTypeID == 6) {
                                Attributetypename = 'DropDown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                            }
                            else if (relationobj[i].AttributeTypeID == 7) {
                                Attributetypename = 'Tree_' + relationobj[i].AttributeID;
                            }
                            else if (relationobj[i].AttributeTypeID == 12) {
                                Attributetypename = 'MultiSelectDropDown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                            }

                            if ($scope.fields[Attributetypename] == undefined) {
                                if (attrTypeID == 1) {
                                    $scope.fields["TextSingleLine_" + ID[0]] = "";
                                }
                                else if (attrTypeID == 2) {
                                    $scope.fields["TextMultiLine_" + ID[0]] = "";
                                }
                                else if (attrTypeID == 3) {
                                    $scope.fields["ListSingleSelection_" + ID[0]] = "";
                                }
                                else if (attrTypeID == 4) {
                                    $scope.fields["ListMultiSelection_" + ID[0]] = "";
                                }
                                else if (attrTypeID == 5) {
                                    $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                                }
                                else if (attrTypeID == 6) {
                                    $scope.fields["DropDown_" + ID[0] + "_" + ID[1]] = "";
                                }
                                else if (attrTypeID == 12) {
                                    $scope.fields["MultiSelectDropDown_" + ID[0] + "_" + ID[1]] = "";
                                }
                                else if (attrTypeID == 17) {
                                    $scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
                                }
                                continue;
                                //return false;
                            }
                            if (relationobj[i].AttributeTypeID == 4) {
                                if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
                                    try {
                                        $scope.DynamicAddValidation(parseInt(ID[0]));
                                    }
                                    catch (e) { };
                                    return true;
                                }
                            }
                            else if (relationobj[i].AttributeTypeID == 6 || relationobj[i].AttributeTypeID == 12) {
                                if ($scope.fields[Attributetypename].id == relationobj[i].AttributeOptionID) {
                                    try {
                                        $scope.DynamicAddValidation(parseInt(ID[0]));
                                    }
                                    catch (e) { };
                                    return true;
                                }
                            }
                            else if (relationobj[i].AttributeTypeID == 7) {
                                if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
                                    try {
                                        $scope.DynamicAddValidation(parseInt(ID[0]));
                                    }
                                    catch (e) { };
                                    return true;
                                }
                            }
                            else {
                                if ($scope.fields[Attributetypename] == relationobj[i].AttributeOptionID) {
                                    try {
                                        $scope.DynamicAddValidation(parseInt(ID[0]));
                                    }
                                    catch (e) { };
                                    return true;
                                }
                            }
                            //return false;
                        }
                        if (attrTypeID == 1) {
                            $scope.fields["TextSingleLine_" + ID[0]] = "";
                        }
                        else if (attrTypeID == 2) {
                            $scope.fields["TextMultiLine_" + ID[0]] = "";
                        }
                        else if (attrTypeID == 3) {
                            $scope.fields["ListSingleSelection_" + ID[0]] = "";
                        }
                        else if (attrTypeID == 4) {
                            $scope.fields["ListMultiSelection_" + ID[0]] = "";
                        }
                        else if (attrTypeID == 5) {
                            $scope.fields["DatePart_" + ID[0]] = null;
                        }
                        else if (attrTypeID == 6) {
                            $scope.fields["DropDown_" + ID[0] + "_" + ID[1]] = "";
                        }
                        else if (attrTypeID == 12) {
                            $scope.fields["MultiSelectDropDown_" + ID[0] + "_" + ID[1]] = "";
                        }
                        else if (attrTypeID == 17) {
                            $scope.fields["ListTagwords_" + ID[0]] = [];
                        }
                        try {
                            $scope.DynamicRemoveValidation(parseInt(ID[0]));
                        }
                        catch (e) { };
                        return false;
                    }
                    else
                        return true;
                }
                else
                    return true;
            }
            else
                return true;
        };

        //Gets all the tabs list in the systems..
        function GetEntityTypeTabCollections(Id, Caption, CurrentEntityId) {
            SubentitycreationService.GetPlantabsettings().then(function (gettabresult) {
                if (gettabresult.Response != null) {
                    $scope.ShowFinancial = gettabresult.Response.Financials;
                    $scope.ShowObjective = gettabresult.Response.Objectives;
                    $scope.ShowAttachments = gettabresult.Response.Attachments;


                    if ($scope.ShowFinancial == "false" && $scope.ShowObjective == "false" && $scope.ShowAttachments == "false") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "true" && $scope.ShowObjective == "true" && $scope.ShowAttachments == "true") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_35.Caption'), "StepDOMID": "#step5" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_1796.Caption'), "StepDOMID": "#step2" });
                        $scope.WizardSteps.push({ "StepId": 4, "StepName": $translate.instant('LanguageContents.Res_701.Caption'), "StepDOMID": "#step3" });
                        $scope.WizardSteps.push({ "StepId": 5, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "true" && $scope.ShowObjective == "true" && $scope.ShowAttachments == "false") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_1796.Caption'), "StepDOMID": "#step2" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_701.Caption'), "StepDOMID": "#step3" });
                        $scope.WizardSteps.push({ "StepId": 4, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "true" && $scope.ShowObjective == "false" && $scope.ShowAttachments == "false") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_1796.Caption'), "StepDOMID": "#step2" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "false" && $scope.ShowObjective == "true" && $scope.ShowAttachments == "false") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_701.Caption'), "StepDOMID": "#step3" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "false" && $scope.ShowObjective == "true" && $scope.ShowAttachments == "true") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_35.Caption'), "StepDOMID": "#step5" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_701.Caption'), "StepDOMID": "#step3" });
                        $scope.WizardSteps.push({ "StepId": 4, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "false" && $scope.ShowObjective == "false" && $scope.ShowAttachments == "true") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_35.Caption'), "StepDOMID": "#step5" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "true" && $scope.ShowObjective == "false" && $scope.ShowAttachments == "true") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_35.Caption'), "StepDOMID": "#step5" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_1796.Caption'), "StepDOMID": "#step2" });
                        $scope.WizardSteps.push({ "StepId": 4, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }



                    GenerateTabNavigations(Id, Caption, CurrentEntityId);
                }
                else {
                    $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    GenerateTabNavigations(Id, Caption, CurrentEntityId);
                }

            });
        }

        function GenerateTabNavigations(Id, Caption, CurrentEntityId) {
            var tabHtml = '';
            $("#SubentitywizardStepClass").html(
                                $compile(tabHtml)($scope));
            for (var t = 0, tab; tab = $scope.WizardSteps[t++];) {
                tabHtml += '<li data-target=\"' + tab.StepDOMID + '\"  ng-click=\"changeSubentityTab2($event)\"><span class="badge badge-info">' + tab.StepId + '</span>' + tab.StepName + '<span class="chevron"></span></li>';
            }
            $("#SubentitywizardStepClass").html(
                                $compile(tabHtml)($scope));

            SubentityTypeCreationTimeout.createdynwiz = setTimeout(function () {
                $('#SubentityMyWizard').wizard();
                $('#SubentityMyWizard').wizard('stepLoaded');
                window["tid_wizard_steps_all_complete_count"] = 0;
                window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
                $('#btnWizardNext').show();     //intially wizard Next button is enalbed
                $('#btnWizardPrev').hide();     //intially wizard Previous button is DISABLED
                $scope.CreateDynamicControls(Id, Caption, CurrentEntityId);
            }, 20);
        }

        //Wizard enable/disable setup
        $scope.Dropdown = [];

        // handling wizard tab click event in the wizard
        $scope.listAttriToAttriResult = [];
        $scope.ShowHideAttributeOnRelation = {};
        $scope.changeTab = function () {

            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
            var currentWizardStep = $('#SubentityMyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#SubentityMyWizard').wizard('totnumsteps').totstep;

            if (currentWizardStep === 1) {
                $('#btnWizardNext').show();
                $('#btnWizardPrev').hide();
            }
            else if (currentWizardStep === totalWizardSteps) {
                $('#btnWizardNext').hide();
                $('#btnWizardPrev').show();
            }

            else {
                $('#btnWizardNext').show();
                $('#btnWizardPrev').show();
            }
            if (currentWizardStep === 3) {
                SubentityTypeCreationTimeout.loadmetadata = setTimeout(function () { EntityMetadata(); }, 100);
            }

        }
        $scope.changeSubentityTab2 = function (e) {
            SubentityTypeCreationTimeout.btnclick = setTimeout(function () { $("#btnTempSub").click(); }, 100);
            $("#EntityMetadata").removeClass('notvalidate');
            if ($("#EntityMetadata .error").length > 0) {
                e.stopImmediatePropagation();
                e.stopPropagation();
                return false;
            }
        }

        function KeepAllStepsMarkedComplete() {
            $("#SubentityMyWizard ul.steps").find("li").addClass("complete");
            $("#SubentityMyWizard ul.steps").find("span.badge").addClass("badge-success");
            window["tid_wizard_steps_all_complete_count"]++;
            if (window["tid_wizard_steps_all_complete_count"] >= 4) {
                clearInterval(window["tid_wizard_steps_all_complete"]);
            }
        }

        // change previous tab in the wizard

        $scope.changePrevTab = function () {
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
            $('#SubentityMyWizard').wizard('previous');
            var currentWizardStep = $('#SubentityMyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#SubentityMyWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep === 1) {
                $('#btnWizardPrev').hide();
            }
            if (currentWizardStep < totalWizardSteps) {
                //$('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
            }
            else {
                //$('#btnWizardFinish').show();
                $('#btnWizardNext').hide();
            }
        }

        // change next tab in the wizard

        $scope.changenexttab = function () {
            EntityMetadata();
            $("#EntityMetadata").removeClass('notvalidate');
            SubentityTypeCreationTimeout.btnclick = setTimeout(function () { $("#btnTempSub").click(); }, 100);
            if ($("#EntityMetadata .error").length > 0) {
                return false;
            }

            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
            $('#SubentityMyWizard').wizard('next', '');
            var currentWizardStep = $('#SubentityMyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#SubentityMyWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep > 1) {
                $('#btnWizardPrev').show();
            }
            if (currentWizardStep === totalWizardSteps) {
                //$('#btnWizardFinish').show();
                $('#btnWizardNext').hide();
            }
            else {
                //$('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
            }
        }

        //InheritFrom Parent Functionality Start
        $scope.Inherritingtreelevels = {};
        $scope.InheritingLevelsitems = [];
        $scope.treeTexts = {};
        $scope.multiselecttreeTexts = {};
        $scope.treeSources = {};
        $scope.treeSourcesObj = [];
        $scope.UploadAttributeData = [];
        $scope.settreeSources = function () {
            var keys = [];
            angular.forEach($scope.treeSources, function (key) {
                keys.push(key);
                $scope.treeSourcesObj = keys;
            });
        }
        $scope.treeTextsObj = [];
        $scope.settreeTexts = function () {
            var keys2 = [];
            angular.forEach($scope.treeTexts, function (key) {
                keys2.push(key);
                $scope.treeTextsObj = keys2;
            });
        }
        $scope.treelevelsObj = [];
        $scope.settreelevels = function () {
            var keys1 = [];
            angular.forEach($scope.Inherritingtreelevels, function (key) {
                keys1.push(key);
                $scope.treelevelsObj = keys1;
            });
        }
        //InheritFrom Parent Functionality End


        //Tree multiselection part STARTED

        $scope.treeNodeSelectedHolder = new Array();
        $scope.PercentageVisibleSettings = {};
        $scope.DropDownTreePricing = {};
        //var apple_selected, treedata_avm, treedata_geography;
        var tree;
        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
            var _ref;
            $scope.output = "You selected: " + branch.Caption;


            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }

            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }

            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);

                }
            }

            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                }
                else
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }

            if ($scope.treeNodeSelectedHolder.length > 0) {
                for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
                    $scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
                }
            }

            //----------------> Calling Attribute to Attribute relation on selecting
            //$scope.ShowHideAttributeToAttributeRelations(branch.AttributeId, 0, 0, 7);

        };

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == child.AttributeId && e.id == child.id; });
                if (remainRecord.length > 0) {

                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }

            }
        }

        $scope.treesrcdirec = {};

        $scope.my_tree = tree = {};

        //Tree  multiselection part ended

        $scope.Parentid = 0;
        //getting cost center
        $scope.costCenterList = [];
        $scope.MemberLists = [];
        $scope.costcemtreObject = [];
        $scope.count = 1;
        $scope.createCostCentre = function (CurrentEntityId) {
            if ($scope.EntitytypeID != undefined) {
                SubentitycreationService.GetCostcentreforEntityCreation($scope.EntitytypeID, 2, parseInt(CurrentEntityId, 10)).then(function (costCenterList) {
                    $scope.costCenterList = costCenterList.Response;
                    $scope.costcemtreObject = [];
                    $scope.MemberLists = [];
                    for (var i = 0; i < $scope.costCenterList.length; i++) {
                        $scope.costcemtreObject.push({
                            "CostcenterId": $scope.costCenterList[i].Id, "costcentername": $scope.costCenterList[i].costcentername, "Sortorder": 1, "Isassociate": 1, "Isactive": 1, "OwnerName": $scope.costCenterList[i].username, "OwnerID": $scope.costCenterList[i].UserID
                        });
                        $scope.count = $scope.count + 1;
                    }
                });
            }

            $scope.treeCategory = [];
            if ($scope.EntitytypeID != undefined) {
                SubentitycreationService.GetCostcentreTreeforPlanCreation($scope.EntitytypeID, 0, parseInt(CurrentEntityId, 10)).then(function (costCenterList1) {
                    $("#dynamicCostCentreTreeOnSubEntity").html = "";
                    if (costCenterList1.Response != null && costCenterList1.Response != false) {
                        $scope.treeCategory = [];
                        $scope.treeCategory = JSON.parse(costCenterList1.Response);

                        $scope.dyn_Cont = '';
                        $scope.dyn_Cont += ' <input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_SubEntity" placeholder="Search" treecontext="treeNodeSearchDropdown_SubEntity"> ';
                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_SubEntity">';
                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                        $scope.dyn_Cont += '<costcentre-tree tree-filter="filterValue_SubEntity" tree-data=\"treeCategory\" accessable="false" tree-control="my_tree" on-select="OnCostCentreTreesSelection(branch,parent)" expand-level=\"100\"></costcentre-tree>';
                        $scope.dyn_Cont += '</div>';
                        $("#dynamicCostCentreTreeOnSubEntity").html($compile($scope.dyn_Cont)($scope));
                    }
                    else {
                        $scope.treeCategory = [];
                        $scope.dyn_Cont = '';
                        $scope.dyn_Cont += ' <input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_SubEntity" placeholder="Search" treecontext="treeNodeSearchDropdown_SubEntity"> ';
                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_SubEntity">';
                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                        $scope.dyn_Cont += '<costcentre-tree tree-filter="filterValue_SubEntity" tree-data=\"treeCategory\" accessable="false" tree-control="my_tree" on-select="OnCostCentreTreesSelection(branch,parent)" expand-level=\"100\"></costcentre-tree>';
                        $scope.dyn_Cont += '</div>';
                        $("#dynamicCostCentreTreeOnSubEntity").html($compile($scope.dyn_Cont)($scope));
                    }
                });
            }

        }
        $scope.GlobalMembersList = [];
        $scope.createGlobalMembers = function (CurrentEntityId) {
            SubentitycreationService.GetGlobalMembers(parseInt(CurrentEntityId, 10)).then(function (GlobalMembersList) {
                $scope.GlobalMembersList = GlobalMembersList.Response;
                for (var i = 0; i < $scope.GlobalMembersList.length; i++) {
                    if (parseInt($scope.GlobalMembersList[i].RoleID, 10) == 1)
                        $scope.MemberLists.push({ "TID": $scope.count, "UserEmail": $scope.GlobalMembersList[i].Email, "DepartmentName": "-", "Title": "-", "Roleid": 2, "RoleName": "Editor", "Userid": parseInt($scope.GlobalMembersList[i].UserID, 10), "UserName": $scope.GlobalMembersList[i].UserName, "IsInherited": $scope.GlobalMembersList[i].IsInherited, "InheritedFromEntityid": $scope.GlobalMembersList[i].InheritedFromEntityID, "FromGlobal": 1, "CostCentreID": 0, "InheritedFromEntityName": $scope.GlobalMembersList[i].InheritedFromEntityName });
                    else
                        $scope.MemberLists.push({ "TID": $scope.count, "UserEmail": $scope.GlobalMembersList[i].Email, "DepartmentName": "-", "Title": "-", "Roleid": parseInt($scope.GlobalMembersList[i].RoleID, 10), "RoleName": $scope.GlobalMembersList[i].Caption, "Userid": parseInt($scope.GlobalMembersList[i].UserID, 10), "UserName": $scope.GlobalMembersList[i].UserName, "IsInherited": $scope.GlobalMembersList[i].IsInherited, "InheritedFromEntityid": $scope.GlobalMembersList[i].InheritedFromEntityID, "FromGlobal": 1, "CostCentreID": 0, "InheritedFromEntityName": $scope.GlobalMembersList[i].InheritedFromEntityName });
                    $scope.count = $scope.count + 1;
                }
            });
        }

        $scope.addcostCentre = function () {
            if ($scope.costcentreid != undefined && $scope.costcentreid != null && $scope.costcentreid.length > 0) {
                $.each($scope.costcentreid, function (val, item) {
                    var result = $.grep($scope.costcemtreObject, function (e) { return e.CostcenterId == item; });
                    if (result.length == 0) {
                        var costCentrevalues = $.grep($scope.costCenterList, function (e) { return e.Id == parseInt(item) })[0];
                        $scope.costcemtreObject.push({
                            "CostcenterId": item, "costcentername": costCentrevalues.costcentername, "Sortorder": 1, "Isassociate": 1, "Isactive": 1, "OwnerName": costCentrevalues.username, "OwnerID": costCentrevalues.UserID
                        });
                        var memberObj = $.grep($scope.MemberLists, function (e) { return e.Roleid == 8 && e.Userid == item; });
                        if (memberObj.length == 0) {
                            $scope.MemberLists.push({ "TID": $scope.count, "UserEmail": costCentrevalues.usermail, "DepartmentName": costCentrevalues.Designation, "Title": costCentrevalues.Title, "Roleid": 8, "RoleName": 'BudgetApprover', "Userid": costCentrevalues.UserID, "UserName": costCentrevalues.username, "IsInherited": '0', "InheritedFromEntityid": '0', "FromGlobal": 1, "CostCentreID": item });
                            $scope.count = $scope.count + 1;
                        }
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1985.Caption'));
                    }
                });
            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_1920.Caption'));
            }
            SubentityTypeCreationTimeout.spliceto = setTimeout(function () {
                $scope.costcentreid.splice(0, $scope.costcentreid.length);
            }, 10);

            if ($scope.treeCategory.length > 0)
                $scope.RecursiveCostCentreTreeClearChecked($scope.treeCategory);
        }

        $scope.RecursiveCostCentreTreeClearChecked = function (Treeval) {
            $.each(Treeval, function (val, item) {
                item.ischecked = false;
                if (item.Children.length > 0) {
                    $scope.RecursiveCostCentreTreeClearChecked(item.Children);
                }
            });
        }

        $scope.deleteCostCentre = function (item) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2033.Caption'), function (result) {
                if (result) {
                    SubentityTypeCreationTimeout.memberlisto = setTimeout(function () {
                        $scope.costcemtreObject.splice($.inArray(item, $scope.costcemtreObject), 1);
                        var memberToRempve = $.grep($scope.MemberLists, function (e) { return e.CostCentreID == item.CostcenterId });
                        if (memberToRempve.length > 0)
                            $scope.MemberLists.splice($.inArray(memberToRempve[0], $scope.MemberLists), 1);
                    }, 100);
                }
            });
        };

        $scope.subEntityName = "";
        $scope.EntitytypeID = 0;
        var ownername = $cookies['Username'];
        var ownerid = $cookies['UserId'];
        $scope.ownerEmail = $cookies['UserEmail'];
        $scope.OwnerName = ownername;
        $scope.OwnerID = ownerid;
        $scope.AutoCompleteSelectedObj = [];
        $scope.OwnerList = [];
        $scope.treePreviewObj = {};
        $scope.owner = {};
        SubentitycreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
            $scope.owner = Getowner.Response;
            $scope.QuickInfo1AttributeCaption = $scope.owner.QuickInfo1AttributeCaption;
            $scope.QuickInfo2AttributeCaption = $scope.owner.QuickInfo2AttributeCaption;
            $scope.OwnerList.push({ "Roleid": 1, "RoleName": "Owner", "UserEmail": $scope.ownerEmail, "DepartmentName": $scope.owner.Designation, "Title": $scope.owner.Title, "Userid": parseInt($scope.OwnerID, 10), "UserName": $scope.OwnerName, "IsInherited": '0', "InheritedFromEntityid": '0', "QuickInfo1": $scope.owner.QuickInfo1, "QuickInfo2": $scope.owner.QuickInfo2 });
        });
        //User Autocomplete End

        // --- Define Controller Variables. ----------------- //
        $scope.contrls = '';
        function GetEntityTypeRoleAccess(rootID) {
            SubentitycreationService.GetEntityTypeRoleAccess(rootID).then(function (role) {
                $scope.Roles = role.Response;
            });
        }

        // Get the all the module to bind ng-grid
        $scope.wizard = {
            newval: ''
        };
        $scope.OptionObj = {

        };
        $scope.UserimageNewTime = new Date.create().getTime().toString();

        // option holder part for dynamic controls START
        $scope.fieldoptions = [];
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });

        }
        // option holder part for dynamic controls END

        $scope.dyn_Cont = '';
        //$scope.dyn_ContSpecial = '';

        /// dynamic value holding scope is start
        $scope.fields = {
            usersID: ''
        };
        $scope.fieldKeys = [];
        $scope.setFieldKeys = function () {
            var keys = [];

            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        /// dynamic value holding scopes end

        $scope.optionsLists = [];
        $scope.EnableDisableControlsHolder = {};
        $scope.AttributeData = [];
        $scope.entityName = "";
        $scope.EntityMemberData = [];
        $scope.addUsers = function () {
            var result = $.grep($scope.MemberLists, function (e) { return e.Userid == parseInt($scope.AutoCompleteSelectedObj[0].Id, 10) && e.Roleid == parseInt($scope.fields['userRoles'], 10) && e.FromGlobal == 0; });
            if (result.length == 0) {
                var membervalues = $.grep($scope.Roles, function (e) { return e.ID == parseInt($scope.fields['userRoles']) })[0];
                if (membervalues != undefined) {
                    if ($scope.AutoCompleteSelectedObj.length > 0) {
                        $scope.MemberLists.push({ "TID": $scope.count, "UserEmail": $scope.AutoCompleteSelectedObj[0].Email, "DepartmentName": $scope.AutoCompleteSelectedObj[0].Designation, "Title": $scope.AutoCompleteSelectedObj[0].Title, "Roleid": parseInt($scope.fields['userRoles'], 10), "RoleName": membervalues.Caption, "Userid": parseInt($scope.AutoCompleteSelectedObj[0].Id, 10), "UserName": $scope.AutoCompleteSelectedObj[0].FirstName + ' ' + $scope.AutoCompleteSelectedObj[0].LastName, "IsInherited": '0', "InheritedFromEntityid": '0', "FromGlobal": 0, "InheritedFromEntityName": $scope.AutoCompleteSelectedObj[0].InheritedFromEntityName, "QuickInfo1": $scope.AutoCompleteSelectedObj[0].QuickInfo1, "QuickInfo2": $scope.AutoCompleteSelectedObj[0].QuickInfo2 });
                        $scope.EntityMemberData.push({ "TID": $scope.count, "UserEmail": $scope.AutoCompleteSelectedObj[0].Email, "DepartmentName": $scope.AutoCompleteSelectedObj[0].Designation, "Title": $scope.AutoCompleteSelectedObj[0].Title, "Roleid": parseInt($scope.fields['userRoles'], 10), "RoleName": membervalues.Caption, "Userid": parseInt($scope.AutoCompleteSelectedObj[0].Id, 10), "UserName": $scope.AutoCompleteSelectedObj[0].FirstName + ' ' + $scope.AutoCompleteSelectedObj[0].LastName, "IsInherited": '0', "InheritedFromEntityid": '0', "FromGlobal": 0, "InheritedFromEntityName": $scope.AutoCompleteSelectedObj[0].InheritedFromEntityName, "QuickInfo1": $scope.AutoCompleteSelectedObj[0].QuickInfo1, "QuickInfo2": $scope.AutoCompleteSelectedObj[0].QuickInfo2 });
                        $scope.fields.usersID = '';
                        $scope.count = $scope.count + 1;
                        $scope.AutoCompleteSelectedObj = [];
                    }
                }
            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
            }
        };

        $scope.deleteOptions = function (users) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2025.Caption'), function (result) {
                if (result) {
                    SubentityTypeCreationTimeout.memberlistsplice = setTimeout(function () {
                        $scope.MemberLists.splice($.inArray(users, $scope.MemberLists), 1);
                        $scope.EntityMemberData.splice($.inArray(users, $scope.EntityMemberData), 1);
                    }, 100);
                }
            });
        };
        $scope.Entityamountcurrencytypeitem = [];
        //add period block
        $scope.items = [];
        $scope.subentityperiods = [];
        $scope.lastSubmit = [];
        $scope.addNew = function () {
            var ItemCnt = $scope.items.length;
            if (ItemCnt > 0) {
                if ($scope.items[ItemCnt - 1].startDate == null || $scope.items[ItemCnt - 1].startDate.length == 0 || $scope.items[ItemCnt - 1].endDate.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1986.Caption'));
                    return false;
                }
                $scope.items.push({ startDate: null, endDate: null, comment: '', sortorder: 0 });
            }
        };
        $scope.submitOne = function (item) {
            $scope.lastSubmit = angular.copy(item);
        };
        $scope.deleteOne = function (item) {
            $scope.lastSubmit.splice($.inArray(item, $scope.lastSubmit), 1);
            $scope.items.splice($.inArray(item, $scope.items), 1);
        };
        $scope.submitAll = function () {
            $scope.lastSubmit = angular.copy($scope.items);
        }
        $scope.FinancialRequestObj = [];
        $scope.ImageFileName = '';
        $scope.nonMandatoryIDList = [];

        $scope.saveEntity = function () {
            var percentageflag = false;
            $('div[data-role="formpercentagetotalcontainer"]').children().find('span[data-role="percentageerror"]').each(function (index, value) {

                if (($(this).attr('data-selection') != undefined) && ($(this).attr('data-ispercentage') != undefined) && ($(this).attr('data-isnotfilter') != undefined)) {
                    if (((parseInt($(this).attr('data-selection')) > 1) && ($(this).attr('data-ispercentage') == "true") && ($(this).attr('data-isnotfilter') == "true")) && ($(this).hasClass("result lapse"))) {
                        percentageflag = true;
                    }
                }
            });
            if (percentageflag) {
                return false;
            }
            $("#EntityMetadata").removeClass('notvalidate');
            SubentityTypeCreationTimeout.btnclick = setTimeout(function () { $("#btnTempSub").click(); }, 100);
            if ($("#EntityMetadata .error").length > 0) {
                return false;
            }
            $("#btnWizardFinish").attr('disabled', 'disabled');
            $scope.AttributeData = [];
            for (var i = 0 ; i < $scope.nonMandatoryIDList.length; i++) {
                $scope.IDList.push($scope.nonMandatoryIDList[i]);
            }
            if ($scope.IsObjectiveFired == false) {
                var entityMetadataObj = {};
                var metadata = [];
                metadata = FormObjectiveMetadata();
                entityMetadataObj.AttributeData = metadata;
                entityMetadataObj.Periods = [];
                GetObjectivePeriods();
                entityMetadataObj.Periods.push($scope.subentityperiods);
                entityMetadataObj.EntityTypeID = parseInt($scope.EntitytypeID, 10);
                SubentitycreationService.GettingPredefineObjectivesForEntityMetadata(entityMetadataObj).then(function (entityAttributeDataObjData) {
                    try {

                        UpdateObjectiveScope(entityAttributeDataObjData.Response);

                        SubentityTypeCreationTimeout.processto = setTimeout(function () {
                            ProcessEntityCreation();
                        }, 100);


                    }
                    catch (ex) {
                        SubentityTypeCreationTimeout.processto = setTimeout(function () {
                            ProcessEntityCreation();
                        }, 100);

                    }

                });
            }
            else {
                SubentityTypeCreationTimeout.processtimeoout = setTimeout(function () {
                    ProcessEntityCreation();
                }, 100);
            }

        };


        function ProcessEntityCreation() {
            var SaveEntity = {};
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id != undefined) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                    "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
                                    "Value": "-1"
                                });
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {

                        var attributeLevelOptions = [];
                        attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID + ""], function (e) { return e.level == (j + 1); }));
                        if (attributeLevelOptions[0] != undefined) {
                            if (attributeLevelOptions[0].selection != undefined) {
                                for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
                                    var valueMatches = [];
                                    if (attributeLevelOptions[0].selection.length > 1)
                                        valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                            return relation.NodeId.toString() === opt;
                                        });
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [opt],
                                        "Level": (j + 1),
                                        "Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
                                    });
                                }
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        var levelCount = $scope.atributesRelationList[i].Levels.length;
                        if (levelCount == 1) {
                            for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                    "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level,
                                    "Value": "-1"
                                });
                            }
                        }
                        else {
                            if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                    for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                            "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level,
                                            "Value": "-1"
                                        });
                                    }
                                }
                                else {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                        "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
                                        "Value": "-1"
                                    });
                                }
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) {
                        if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                    else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined ? $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] : 0;

                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt(value, 10),
                                    "Level": 0,
                                    "Value": "-1"
                                });

                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name)
                        $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                    else {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                    var MyDate = new Date.create();
                    var MyDateString;

                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        MyDateString = $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID];
                    }
                    else {
                        MyDateString = "";
                    }
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": MyDateString,
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == $scope.atributesRelationList[i].AttributeID; });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": [parseInt(nodeval.id, 10)],
                            "Level": parseInt(nodeval.Level, 10),
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields["TextMoney_" + $scope.atributesRelationList[i].AttributeID],
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": ($scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] == "" || $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] == false) ? 0 : 1,
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0 ; k < multiselectiObject.length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt(multiselectiObject[k], 10),
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.ImageFileName,
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                    if ($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        for (var j = 0; j < $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID].length; j++) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID][j], 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                    // $scope.Entityamountcurrencytypeitem.push({ amount: parseInt($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID].replace(/ /g, '')), currencytype: $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID], Attributeid: $scope.atributesRelationList[i].AttributeID });
                    if ($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] == "") {
                        $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = "0";
                    }
                    $scope.Entityamountcurrencytypeitem.push({ amount: parseFloat($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID].replace(/ /g, '')), currencytype: $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID], Attributeid: $scope.atributesRelationList[i].AttributeID });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID],
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 18) {

                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": "",
                        "Level": 0,
                        "Value": "-1"
                    });

                }
            }
            SaveEntity.ParentId = parseInt($scope.Parentid, 10);
            SaveEntity.Typeid = parseInt($scope.EntitytypeID, 10);
            SaveEntity.Active = true;
            SaveEntity.IsLock = false;
            SaveEntity.Name = $scope.entityName;
            SaveEntity.EntityMembers = [];
            SaveEntity.EntityCostRelations = $scope.costcemtreObject;
            SaveEntity.Periods = [];
            $scope.savesubentityperiods = [];
            SaveEntity.Entityamountcurrencytype = [];
            for (var m = 0; m < $scope.items.length; m++) {
                if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                    if ($scope.items[m].startDate.length != 0 && $scope.items[m].endDate.length != 0) {
                        $scope.savesubentityperiods.push({ startDate: ($scope.items[m].startDate.length != 0 ? ConvertDateToString($scope.items[m].startDate) : ''), endDate: ($scope.items[m].endDate.length != 0 ? ConvertDateToString($scope.items[m].endDate) : ''), comment: ($scope.items[m].comment.trim().length != 0 ? $scope.items[m].comment : ''), sortorder: 0 });
                    }
                }
            }
            SaveEntity.Periods.push($scope.savesubentityperiods);
            SaveEntity.AttributeData = $scope.AttributeData;
            SaveEntity.EntityMembers = $scope.EntityMemberData;
            SaveEntity.IObjectiveEntityValue = $scope.IDList;
            SaveEntity.FinancialRequestObj = $scope.FinancialRequestObj;

            if ($scope.AssetSelectionFiles.length == 0) {
                var res = [];
                for (var i = 0; i < $scope.SelectedAssetIdFolderID.SelectedAssetIDs.length; i++) {
                    res.push($scope.SelectedAssetIdFolderID.SelectedAssetIDs[i].AssetId);
                }
                SaveEntity.AssetArr = res;
            }
            else
                SaveEntity.AssetArr = $scope.AssetSelectionFiles;
            $scope.curram = [];
            for (var m = 0; m < $scope.Entityamountcurrencytypeitem.length; m++) {
                $scope.curram.push({ amount: $scope.Entityamountcurrencytypeitem[m].amount, currencytype: $scope.Entityamountcurrencytypeitem[m].currencytype, Attributeid: $scope.Entityamountcurrencytypeitem[m].Attributeid });
            }
            SaveEntity.Entityamountcurrencytype.push($scope.curram);
            SaveEntity.SelectedAssetIdFolderID = $scope.SelectedAssetIdFolderID.SelectedAssetIDs;
            SubentitycreationService.CreateEntity(SaveEntity).then(function (EntityList) {
                if (EntityList.Response > 0) {
                    $scope.EntityMemberData = [];
                    $scope.MemberLists = [];
                    $scope.fields = [];
                    $scope.entityObjectiveList = {};
                    $scope.entityAllObjectives = {};
                    $scope.MandatoryEntityObjectives = {};
                    $scope.MandatoryObjList = [];
                    $scope.NonMandatoryObjList = [];
                    $scope.IDList = [];
                    $scope.nonMandatoryIDList = [];

                    $('#dynamicControls').modal('hide');
                    $('#moduleContextmenu').modal('hide');
                    $scope.Activity.IsActivitySectionLoad = true;
                    SubentityTypeCreationTimeout.refreshentitylist = setTimeout(function () { $("#EntitiesTree").trigger("ActivityLoadEntity", [EntityList.Response]); }, 50);
                    $location.path('/mui/planningtool/default/detail/section/' + EntityList.Response + '/overview');
                    $scope.OwnerList = [];
                    $scope.treePreviewObj = {};
                    $scope.Entityamountcurrencytypeitem = [];
                    $scope.OwnerList.push({ "Roleid": 1, "RoleName": "Owner", "UserEmail": $scope.ownerEmail, "DepartmentName": "-", "Title": "-", "Userid": parseInt($scope.OwnerID, 10), "UserName": $scope.OwnerName, "IsInherited": '0', "InheritedFromEntityid": '0', "InheritedFromEntityName": "-" });
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4341.Caption'));
                    $("#btnWizardFinish").attr('disabled', false);
                }
                SubentityTypeCreationTimeout.closepopup = setTimeout(function () { $modalInstance.dismiss('cancel') }, 100);
                $("#EntityMetadata").html('');
            });
        }
        //$(window).on("GloblaUniqueKeyAccess", function (event, UniqueKey) {
        //    $scope.UniqueKey = UniqueKey;
        //});
        $scope.addRootEntity = function () {
            $scope.EnableAdd = true;
            $scope.EnableUpdate = false;
            $scope.step = 0;
            $scope.EnableOptionUpdate = false;
            $scope.EnableOptionAdd = true;
        };
        $scope.treelevels = [];
        $scope.tree = {};
        $scope.MultiSelecttree = {};
        $scope.Options = {};
        function SubentityTypeCreation(Id, Caption, CurrentEntityId) {
            if (Id != undefined) {
                GetEntityTypeRoleAccess(Id);
            }
            $scope.OwnerList = [];
            $scope.treeNodeSelectedHolder = [];
            $scope.treesrcdirec = {};
            $scope.owner = {};
            //SubentitycreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
            //    $scope.owner = Getowner.Response;
            //    $scope.OwnerList.push({ "Roleid": 1, "RoleName": "Owner", "UserEmail": $scope.ownerEmail, "DepartmentName": $scope.owner.Designation, "Title": $scope.owner.Title, "Userid": parseInt($scope.OwnerID, 10), "UserName": $scope.OwnerName, "IsInherited": '0', "InheritedFromEntityid": '0', "QuickInfo1": $scope.owner.QuickInfo1, "QuickInfo2": $scope.owner.QuickInfo2 });
            //});
            $scope.Parentid = CurrentEntityId;
            $scope.createCostCentre(CurrentEntityId);
            $scope.createGlobalMembers(CurrentEntityId);
            $scope.WizardSteps = [{ "StepId": 1, "StepName": $translate.instant('LanguageContents.Res_251.Caption'), "StepDOMID": "#step1" }];
            var tabHtml = '';
            $("#SubentitywizardStepClass").html(
                                $compile(tabHtml)($scope));
            GetEntityTypeTabCollections(Id, Caption, CurrentEntityId);
        }

        $scope.CreateDynamicControls = function (Id, Caption, CurrentEntityId) {
            $("#btnWizardFinish").removeAttr('disabled');
            $("#EntityMetadata").html('');
            $("#EntityMetadata").scrollTop(0);
            $("#EntityMetadata").html('<div class="loadingSubPopup"><img src="assets/img/loading.gif"> Loading...</div>');
            $scope.subEntityName = Caption;
            $scope.EntitytypeID = Id;
            $scope.items = [];
            $scope.DateObject = {};
            //--------------------------> GET ATTRIBUTE TO ATTRIBUTE RELATIONS <------------------------------
            SubentitycreationService.GetAttributeToAttributeRelationsByIDForEntity(Id).then(function (entityAttrToAttrRelation) {
                if (entityAttrToAttrRelation.Response != null) {
                    if (entityAttrToAttrRelation.Response.length > 0) {
                        $scope.listAttriToAttriResult = entityAttrToAttrRelation.Response;
                    }
                }
            });
            SubentitycreationService.GetEntityTypeAttributeRelationWithLevelsByID(Id, CurrentEntityId).then(function (entityAttributesRelation) {
                $scope.atributesRelationList = entityAttributesRelation.Response;
                $scope.dyn_Cont = '';
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                        var totLevelCnt = $scope.atributesRelationList[i].Levels.length;
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                            if (j == 0) {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.treeSources["dropdown_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree);
                                $scope.EnableDisableControlsHolder["Treedropdown_" + $scope.atributesRelationList[i].AttributeID] = false;

                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Treedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                $scope.settreeSources();
                            }
                            else {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Treedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                            }

                            $scope.setFieldKeys();
                        }
                        try {
                            var CaptionObj = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0].split(",");
                            var LabelObject = $scope.atributesRelationList[i].Lable[0];
                            for (var j = 0; j < LabelObject.length; j++) {
                                if (j == 0) {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = CaptionObj[j].trim();
                                        $scope.settreeTexts();
                                    }
                                    else {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                }
                                else {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = CaptionObj[j].trim();
                                        $scope.settreeTexts();
                                    }
                                    else {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                }
                            }
                            $scope.Inherritingtreelevels["dropdown_levels_" + $scope.atributesRelationList[i].ID] = $scope.InheritingLevelsitems;
                            $scope.settreelevels();
                            $scope.InheritingLevelsitems = [];
                            $scope.settreeTexts();
                            $scope.settreelevels();
                        }
                        catch (ex) {
                        }
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                        $scope.fields["Tree_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                        GetTreeObjecttoSave($scope.atributesRelationList[i].AttributeID);
                        if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            treeTextVisbileflag = false;
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID])) {
                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = true;
                            }
                            else
                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                        }
                        else {
                            $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                        }
                        $scope.dyn_Cont += '<div ng-show=\"ShowOrHideAttributeToAttributeRelation(\'' + $scope.atributesRelationList[i].AttributeID + '_0\')\" class="control-group treeNode-control-group">';
                        $scope.dyn_Cont += '<label class="control-label">' + $scope.atributesRelationList[i].AttributeCaption + '</label>';
                        $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                        $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '"></div>';
                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '">';
                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                        $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" accessable="' + $scope.atributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                        $scope.dyn_Cont += '</div></div></div>';
                        $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationList[i].AttributeID + '\">';
                        $scope.dyn_Cont += '<div class="controls">';
                        $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.atributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                        $scope.dyn_Cont += '</div></div>';
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                        var totLevelCnt1 = $scope.atributesRelationList[i].Levels.length;
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {

                            if (totLevelCnt1 == 1) {

                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;

                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                $scope.setFieldKeys();
                            }

                            else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                                if (j == 0) {
                                    $scope.treeSources["multiselectdropdown_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree);
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;

                                    $scope.EnableDisableControlsHolder["MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID] = false;
                                    $scope.dyn_Cont += "<div  ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                    $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                    $scope.settreeSources();
                                }
                                else {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];

                                    if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                        $scope.dyn_Cont += "<div  ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                        $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                    }
                                    else {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                        $scope.dyn_Cont += "<div  ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                        $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                    }
                                }
                                $scope.setFieldKeys();
                            }
                        }
                        try {
                            var CaptionObj = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0].toString().split(",");
                            var LabelObject = $scope.atributesRelationList[i].Lable[0];
                            for (var j = 0; j < LabelObject.length; j++) {
                                if (j == 0) {
                                    if ($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][j] != undefined) {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][j].trim();
                                        $scope.settreeTexts();
                                    }
                                    else {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                }
                                else {
                                    if (j == (LabelObject.length - 1)) {
                                        var k = j;
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = [];
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        for (k; k < ($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0].length) ; k++) {
                                            if ($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][k] != undefined) {
                                                $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)].push($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][k].trim());
                                            }
                                            else {
                                                $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                            }
                                            $scope.settreeTexts();
                                        }
                                    }
                                    else {
                                        if ($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][j] != undefined) {
                                            $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                            $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][j].trim();
                                            $scope.settreeTexts();
                                        }
                                        else {
                                            $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                            $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                            $scope.settreeTexts();
                                        }
                                    }
                                }
                            }
                            $scope.Inherritingtreelevels["multiselectdropdown_levels_" + $scope.atributesRelationList[i].ID] = $scope.InheritingLevelsitems;
                            $scope.settreelevels();
                            $scope.InheritingLevelsitems = [];
                            $scope.settreeTexts();
                            $scope.settreelevels();
                        }
                        catch (ex) { }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        if ($scope.atributesRelationList[i].IsSpecial == true) {
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"> <input ng-disabled=\"EnableDisableControlsHolder.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.OwnerList[0].UserName;
                                $scope.setFieldKeys();
                            }
                        }
                        else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.EntityStatus) {
                        }
                        else if ($scope.atributesRelationList[i].IsSpecial == false) {
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            $scope.setoptions();
                            $scope.EnableDisableControlsHolder["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                            $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2  ng-disabled=\"EnableDisableControlsHolder.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" > <option value=\"\">Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \"value=\"{{ndata.Id}}\">{{ndata.Caption}}</option> </select></div></div>";
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                            $scope.setFieldKeys();
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        if ($scope.atributesRelationList[i].AttributeID != 70) {
                            $scope.EnableDisableControlsHolder["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                            } else {
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                            }
                            $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            $scope.setFieldKeys();
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea></div></div>";
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        $scope.EnableDisableControlsHolder["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> ";
                        if ($scope.atributesRelationList[i].IsReadOnly)
                            $scope.dyn_Cont += "<select class=\"multiselect\"   data-placeholder=\"Select filter\" multiselect-dropdown disabled ng-disabled=\"EnableDisableControlsHolder.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\" ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"></select></div></div>";
                        else
                            $scope.dyn_Cont += "<select class=\"multiselect\"   data-placeholder=\"Select filter\" multiselect-dropdown  ng-disabled=\"EnableDisableControlsHolder.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\" ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\" ></select></div></div>";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        if ($scope.atributesRelationList[i].Caption == "Due Date") {
                            $scope.IsOpend = "DueDate";
                            $scope.DateObject.DueDate = false;
                            var isopenedhtmlstr = "DateObject.DueDate";
                        } else if ($scope.atributesRelationList[i].Caption == "StartDate") {
                            $scope.IsOpend = "StartDate";
                            var isopenedhtmlstr = "DateObject.StartDate";
                            $scope.DateObject.StartDate = false;
                        }
                        else {
                            $scope.IsOpend = "EndDate";
                            $scope.DateObject.EndDate = false;
                            var isopenedhtmlstr = "DateObject.EndDate";
                        }
                        $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + $scope.MinValue);
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {

                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + $scope.MaxValue);
                        }
                        else {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }

                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.DateTime_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"text\" ng-change=\"setTimeout(changeduedate_changed(fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "," + $scope.atributesRelationList[i].AttributeID + "),3000)\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event,DateObject,'" + $scope.IsOpend + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"" + isopenedhtmlstr + "\" min=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        var param1 = new Date.create();
                        var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        $scope.setFieldKeys();
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextMoney_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"text\" ng-model=\"fields.TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        $scope.fields["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                        $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = true;
                        $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = $scope.atributesRelationList[i].DropDownPricing;
                        if ($scope.atributesRelationList[i].IsReadOnly == true)
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        else
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div drowdowntreepercentagemultiselection data-purpose='entity' data-attributeid=" + $scope.atributesRelationList[i].AttributeID.toString() + "></div>";


                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        $scope.EnableDisableControlsHolder["Period_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        try {
                            if ($scope.atributesRelationList[i].InheritFromParent) {
                                if ($scope.atributesRelationList[i].ParentValue == "-") {
                                    $scope.items.push({ startDate: null, endDate: null, comment: '', sortorder: 0, calstartopen: false, calendopen: false });
                                }
                                else {
                                    if ($scope.atributesRelationList[i].ParentValue.length > 0) {
                                        for (var j = 0 ; j < $scope.atributesRelationList[i].ParentValue[0].length; j++) {
                                            var datStartUTCval = "";
                                            var datstartval = "";
                                            var datEndUTCval = "";
                                            var datendval = "";
                                            datStartUTCval = $scope.atributesRelationList[i].ParentValue[0][j].Startdate.substr(0, 10);
                                            datstartval = new Date.create((datStartUTCval));
                                            datEndUTCval = $scope.atributesRelationList[i].ParentValue[0][j].EndDate.substr(0, 10);
                                            datendval = new Date.create((datEndUTCval));
                                            $scope.items.push({ startDate: (ConvertDateToString(datstartval)), endDate: (ConvertDateToString(datendval)), comment: $scope.atributesRelationList[i].ParentValue[0][j].Description, sortorder: 0, calstartopen: false, calendopen: false });
                                        }
                                    }
                                    else {
                                        $scope.items.push({ startDate: null, endDate: null, comment: '', sortorder: 0, calstartopen: false, calendopen: false });
                                    }
                                }
                            }
                            else {
                                $scope.items.push({ startDate: null, endDate: null, comment: '', sortorder: 0, calstartopen: false, calendopen: false });
                            }
                        }
                        catch (e) { }
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                        }
                        else {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                        }
                        else {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }
                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                        $scope.dyn_Cont += "    <div class=\"control-group\"><label for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.atributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                        $scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span5\">";
                        $scope.dyn_Cont += "<input class=\"sdate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Period_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" name=\"startDate\" ng-click=\"PeriodCalanderopen($event,item,'start')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calstartopen\" min=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  ng-change=\"changeperioddate_changed(item.startDate,'StartDate')\" ng-model=\"item.startDate\" placeholder=\"-- Start date --\"/>";
                        $scope.dyn_Cont += "<input class=\"edate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Period_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" name=\"enddate\" ng-change=\"changeperioddate_changed(item.endDate,'EndDate')\" ng-model=\"item.endDate\"  ng-click=\"PeriodCalanderopen($event,item,'end')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calendopen\" min=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- End date --\"/>";
                        $scope.dyn_Cont += "<input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Period_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\" ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
                        $scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
                        $scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
                        $scope.fields["Period_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.fields["DatePart_Calander_Open" + "item.startDate"] = false;
                        $scope.fields["DatePart_Calander_Open" + "item.endDate"] = false;
                        $scope.setFieldKeys();
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                        StrartUpload_UploaderAttrSubEntity();
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        $scope.dyn_Cont += '<div ng-show=\"ShowOrHideAttributeToAttributeRelation(\'' + $scope.atributesRelationList[i].AttributeID + '_0\',' + $scope.atributesRelationList[i].AttributeTypeID + ')\" class="control-group"><label class="control-label"';
                        $scope.dyn_Cont += 'for="fields.Uploader_ ' + $scope.atributesRelationList[i].AttributeID + '">' + $scope.atributesRelationList[i].AttributeCaption + ': </label>';
                        $scope.dyn_Cont += '<div id="Uploader" class="controls">';
                        $scope.dyn_Cont += '<img id="UploaderPreview_' + $scope.atributesRelationList[i].AttributeID + '" class="ng-pristine ng-valid entityImgPreview"';
                        $scope.dyn_Cont += ' ng-model="fields.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" id="UploaderImageControl" src="" alt="No thumbnail present">';
                        $scope.dyn_Cont += '<br><a ng-show="EnableDisableControlsHolder.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" ng-model="UploadImage" ng-click="UploadImagefile(' + $scope.atributesRelationList[i].AttributeID + ')" class="ng-pristine ng-valid">Select Image</a>';
                        $scope.dyn_Cont += '</div></div>';
                        $scope.fields["Uploader_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                        $scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["tagoption_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.setoptions();
                        $scope.tempscope = [];
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\">";
                        $scope.dyn_Cont += "<label class=\"control-label\" for=\"fields.ListTagwords_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label>";
                        $scope.dyn_Cont += "<div class=\"controls\">";
                        $scope.dyn_Cont += "<directive-tagwords item-attrid = \"" + $scope.atributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + $scope.atributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                        $scope.dyn_Cont += "</div></div>";

                        if ($scope.atributesRelationList[i].InheritFromParent) {
                        }
                        else {
                        }
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.IsOpend = "DateAction";
                        var isopenedhtmlstr = "DateObject.DateAction";
                        $scope.DateObject = { "DateAction": false };
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                        }
                        else {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                        }
                        else {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.DateTime_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event,DateObject,'" + $scope.IsOpend + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"" + isopenedhtmlstr + "\" min=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        var param1 = new Date.create();
                        var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        $scope.fields["fields.DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.setFieldKeys();
                    }

                    else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["dListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.EnableDisableControlsHolder["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.CurrencyFormatsList;
                        $scope['origninalamountvalue_' + $scope.atributesRelationList[i].AttributeID] = '0';
                        $scope['currRate_' + $scope.atributesRelationList[i].AttributeID] = 1;
                        $scope.setoptions();
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\"  class=\"currencyTextbox   margin-right5x\" ng-change=\"Getamountentered(" + $scope.atributesRelationList[i].AttributeID + ")\" ng-disabled=\"EnableDisableControlsHolder.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.dListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"GetCostCentreCurrencyRateById(" + $scope.atributesRelationList[i].AttributeID + ")\" class=\"currencySelector\" ><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Name}}</option></select></div></div>";
                        $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = '0';
                        $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.DefaultSettings.CurrencyFormat.Id;
                        $scope.setFieldKeys();

                    }

                    $scope.setFieldKeys();
                    $scope.setoptions();
                }
                $scope.dyn_Cont += '<input style="visibility:hidden" type="submit" id="btnTempSub" class="ng-scope" invisible>';
                $scope.setFieldKeys();
                $("#EntityMetadata").html('<div class="row-fluid"><div data-col="1" class="span4"></div><div data-col="2" class="span8"></div></div> ');
                $("#EntityMetadata").html(
                    $compile($scope.dyn_Cont)($scope));
                //$("[id^='dTextSingleLine_']").autoNumeric('init',$scope.DecimalSettings['FinancialAutoNumeric']);
                var mar = ($scope.DecimalSettings.FinancialAutoNumeric.vMax).substr(($scope.DecimalSettings.FinancialAutoNumeric.vMax).indexOf(".") + 1);
                $scope.mindec = "";
                if (mar.length == 1) {
                    $("[id^='dTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: "0", mDec: "1" });
                }
                if (mar.length != 1) {
                    if (mar.length < 5) {
                        $scope.mindec = "0.";

                        for (i = 0; i < mar.length; i++) {
                            $scope.mindec = $scope.mindec + "0";
                        }
                        $("[id^='dTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: $scope.mindec });
                    }
                    else {
                        $("[id^='dTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: "0", mDec: "0" });
                    }
                }
                $("[id^='MTextMoney_']").autoNumeric('init', { aSep: ' ', vMin: "0", mDec: "0" });
                $("[id^='MTextMoney_']").keydown(function (event) {
                    if (event.which != 8 && isNaN(String.fromCharCode(event.which)) && (event.keyCode < 96 || event.keyCode > 105) && event.which != 46) {
                        bootbox.alert($translate.instant('Please enter only Number'));
                        event.preventDefault(); //stop character from entering input
                    }
                });
                $("#EntityMetadata").scrollTop(0);
                refreshAssetobjects();
                $scope.$broadcast('callbackforEntitycreation', 0, CurrentEntityId, 4);
                GetValidationList(Id);
                $("#EntityMetadata").addClass('notvalidate');

                SubentityTypeCreationTimeout.updateto = setTimeout(function () {
                    //--------> HIDE ALL THE ATTRIBUTE RELAIONS AFTER LOADING <--------------------
                    //HideAttributeToAttributeRelationsOnPageLoad();
                    UpdateInheritFromParentScopes();
                });
            });
        };

        var treeTextVisbileflag = false;
        function IsNotEmptyTree(treeObj) {

            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                }
                else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        function GetTreeObjecttoSave(attributeid) {
            GenerateTreeStructure($scope.treesrcdirec["Attr_" + attributeid]);
        }
        var treeformflag = false;
        function GenerateTreeStructure(treeobj) {
            if (treeobj.length > 0) {
                for (var i = 0, node; node = treeobj[i++];) {

                    if (node.ischecked == true) {
                        var remainRecord = [];
                        remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == node.AttributeId && e.id == node.id; });
                        if (remainRecord.length == 0) {
                            $scope.treeNodeSelectedHolder.push(node);
                        }
                        treeformflag = false;
                        if (ischildSelected(node.Children)) {
                            GenerateTreeStructure(node.Children);
                        }
                        else {
                            GenerateTreeStructure(node.Children);
                        }
                    }
                    else
                        GenerateTreeStructure(node.Children);
                }

            }
        }


        function ischildSelected(children) {

            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    treeformflag = true;
                    return treeformflag
                }
            }
            return treeformflag;
        }

        function UpdateInheritFromParentScopes() {
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) {
                    } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                            var attrInheritval = ($.grep($scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID], function (e) { return e.Id == $scope.atributesRelationList[i].ParentValue[0] }));
                            if (attrInheritval != undefined) {
                                if (attrInheritval.length > 0) {
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = attrInheritval[0].Id;
                                }
                            }
                        }
                        else {
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                        }

                        if ($scope.atributesRelationList[i].IsReadOnly) {
                            $scope.EnableDisableControlsHolder["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        }
                    }
                }
                if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) {
                    } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                            var attrInheritval = ($.grep($scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID], function (e) { return e.Id == $scope.atributesRelationList[i].ParentValue[0]["Currencytypeid"] }));
                            if (attrInheritval != undefined) {
                                if (attrInheritval.length > 0) {
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = attrInheritval[0]["Id"];
                                    $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = ($scope.atributesRelationList[i].ParentValue[0]["Amount"]).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').toString();
                                }
                            }
                        }
                        else {
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.DefaultSettings.CurrencyFormat.Id;
                            $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = '0';
                        }

                        if ($scope.atributesRelationList[i].IsReadOnly) {
                            $scope.EnableDisableControlsHolder["dListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.EnableDisableControlsHolder["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = true;
                        }
                    }
                }

                else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                            if ($scope.atributesRelationList[i].ParentValue[0] != undefined) {
                                $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                                for (var j = 0; j < $scope.atributesRelationList[i].ParentValue[0].length; j++) {
                                    var attrInheritval = ($.grep($scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID], function (e) { return e.Id == $scope.atributesRelationList[i].ParentValue[0][j] }));
                                    if (attrInheritval != undefined) {
                                        if (attrInheritval.length > 0) {
                                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID].push(attrInheritval[0].Id);
                                        }
                                    }

                                }
                            }
                        }
                        else {
                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                            var defaultmultiselectvalue = $scope.atributesRelationList[i].DefaultValue.split(',');
                            if ($scope.atributesRelationList[i].DefaultValue != "") {
                                for (var j = 0; j < defaultmultiselectvalue.length; j++) {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID].push($.grep($scope.atributesRelationList[i].Options, function (e) { return e.Id == defaultmultiselectvalue[j]; })[0].Id);
                                }
                            }
                        }
                        if ($scope.atributesRelationList[i].IsReadOnly) {
                            $scope.EnableDisableControlsHolder["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                        var levelCount = $scope.Inherritingtreelevels["dropdown_levels_" + $scope.atributesRelationList[i].ID].length
                        for (var j = 1; j <= levelCount; j++) {
                            var dropdown_text = "dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + j;
                            if (j == 1) {
                                if ($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j)].data.length > 0) {
                                    $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.treeSources["dropdown_" + $scope.atributesRelationList[i].AttributeID].Children, function (e) { return e.Caption == $scope.treeTexts[dropdown_text] }))[0];
                                }
                            }
                            else {
                                if ($scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)] != null)
                                    if ($scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children != undefined)
                                        $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) { return e.Caption == $scope.treeTexts[dropdown_text] }))[0];
                            }
                        }
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Treedropdown_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                    if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                        var levelCount = $scope.Inherritingtreelevels["multiselectdropdown_levels_" + $scope.atributesRelationList[i].ID].length
                        for (var j = 1; j <= levelCount; j++) {
                            var dropdown_text = "multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + j;
                            if (j == 1) {
                                if ($scope.atributesRelationList[i].InheritFromParent) {
                                    if ($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j)].data.length > 0) {
                                        $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.treeSources["multiselectdropdown_" + $scope.atributesRelationList[i].AttributeID].Children, function (e) { return e.Caption == $scope.multiselecttreeTexts[dropdown_text] }))[0];
                                    }
                                }
                            }
                            else {
                                if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)] != null) {
                                    if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children != undefined) {
                                        if (j == levelCount) {
                                            var k = j;
                                            if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children.length > 0) {
                                                $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = [];
                                                for (var l = 0; l < $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children.length; l++) {
                                                    $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j].push(($.grep($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) {
                                                        return e.Caption.toString().trim() == $scope.multiselecttreeTexts[dropdown_text][l].toString().trim();
                                                    }))[0]);
                                                }
                                            }
                                        }
                                        else {
                                            $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) { return e.Caption == $scope.multiselecttreeTexts[dropdown_text] }))[0];
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue;
                    }
                    else {
                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue;
                    }
                    else {
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 5) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = ConvertDateFromStringToString($scope.atributesRelationList[i].ParentValue.toString(), $scope.DefaultSettings.DateFormat);
                    }

                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {

                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Period_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = false;
                    }
                }
            }
        }

        //Validation functionality--call this for validation
        function GetValidationList(EntitypeId) {
            SubentitycreationService.GetValidationDationByEntitytype(EntitypeId).then(function (GetValidationresult) {
                if (GetValidationresult.Response != null) {
                    $scope.listValidationResult = GetValidationresult.Response;
                    $scope.listofValidations = GetValidationresult.Response;
                    if ($scope.listAttriToAttriResult != null) {
                        for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                            var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                            if (attrRelIDs != undefined) {
                                for (var j = 0; j < attrRelIDs.length; j++) {
                                    if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                        });
                                    }
                                    else {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                        });
                                    }
                                }
                            }
                        }
                    }
                    $("#EntityMetadata").nod($scope.listValidationResult, {
                        'delay': 200,
                        'submitBtnSelector': '#btnTempSub',
                        'silentSubmit': 'true'
                    });
                }
            });
        }

        $scope.entityObjectiveList = {};
        $scope.entityAllObjectives = {};
        $scope.MandatoryEntityObjectives = {};
        $scope.MandatoryObjList = [];
        $scope.NonMandatoryObjList = [];
        $scope.IDList = [];
        $scope.nonMandatoryIDList = [];
        $scope.IsObjectiveFired = false;
        function EntityMetadata() {
            $scope.AttributeData = [];
            var entityMetadataObj = {};
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "") {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id != undefined) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id,
                                    "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                });
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.Owner) {
                        if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                if (isNaN(parseFloat(($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10)) == false) {
                                    var value = parseInt(($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10);
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": value,
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0 ; k < multiselectiObject.length; k++) {
                                if (multiselectiObject[k] != undefined) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Caption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": parseInt(multiselectiObject[k], 10),
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                }
            }
            entityMetadataObj.AttributeData = $scope.AttributeData;
            entityMetadataObj.Periods = [];
            $scope.subentityperiods.splice(0, $scope.subentityperiods.length);
            for (var m = 0; m < $scope.items.length; m++) {
                if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                    if ($scope.items[m].startDate.length != 0 && $scope.items[m].endDate.length != 0) {
                        $scope.subentityperiods.push({ startDate: ConvertDateToString($scope.items[m].startDate), endDate: ConvertDateToString($scope.items[m].endDate), comment: '', sortorder: 0 });
                    }
                }
            }
            entityMetadataObj.Periods.push($scope.subentityperiods);
            entityMetadataObj.EntityTypeID = parseInt($scope.EntitytypeID, 10);
            SubentitycreationService.GettingPredefineObjectivesForEntityMetadata(entityMetadataObj).then(function (entityAttributeDataObjData) {
                $scope.IsObjectiveFired = true;
                if (entityAttributeDataObjData.Response != null && entityAttributeDataObjData.Response.length > 0) {
                    $scope.entityAllObjectives = entityAttributeDataObjData.Response;
                    $scope.MandatoryObjList = [];
                    $scope.NonMandatoryObjList = [];
                    $scope.IDList = [];
                    for (var i = 0 ; i < $scope.entityAllObjectives.length ; i++) {
                        if ($scope.entityAllObjectives[i].ObjectiveMandatoryStatus == true) {
                            $scope.MandatoryObjList.push($scope.entityAllObjectives[i]);
                            $scope.IDList.push(parseInt($scope.entityAllObjectives[i].ObjectiveId, 10));
                        }
                        else {
                            $scope.NonMandatoryObjList.push($scope.entityAllObjectives[i]);
                        }
                    }
                    $scope.entityObjectiveList = $scope.NonMandatoryObjList;
                    $scope.MandatoryEntityObjectives = $scope.MandatoryObjList;
                    $scope.ngNoObjectivediv = false;
                    $scope.ngObjectiveDivHolder = true;
                }
                else {
                    $scope.ngObjectiveDivHolder = false;
                    $scope.ngNoObjectivediv = true;
                }
            });
        }


        function FormObjectiveMetadata() {

            var AttributeData = [];


            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "") {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id != undefined) {
                                AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id,
                                    "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                });
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.Owner) {
                        if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                if (isNaN(parseFloat(($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10)) == false) {
                                    var value = parseInt(($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10);
                                    AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": value,
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0 ; k < multiselectiObject.length; k++) {
                                if (multiselectiObject[k] != undefined) {
                                    AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Caption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": parseInt(multiselectiObject[k], 10),
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                }
            }
            return AttributeData;
        }


        function GetObjectivePeriods() {

            $scope.subentityperiods.splice(0, $scope.subentityperiods.length);
            for (var m = 0; m < $scope.items.length; m++) {
                if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                    if ($scope.items[m].startDate.length != 0 && $scope.items[m].endDate.length != 0) {
                        $scope.subentityperiods.push({ startDate: ConvertDateToString($scope.items[m].startDate), endDate: ConvertDateToString($scope.items[m].endDate), comment: '', sortorder: 0 });
                    }
                }
            }
        }

        function UpdateObjectiveScope(objResponse) {

            if (objResponse != null) {


                if (objResponse != null && objResponse.length > 0) {
                    $scope.entityAllObjectives = objResponse;
                    $scope.MandatoryObjList = [];
                    $scope.NonMandatoryObjList = [];
                    $scope.IDList = [];
                    for (var i = 0 ; i < $scope.entityAllObjectives.length ; i++) {
                        if ($scope.entityAllObjectives[i].ObjectiveMandatoryStatus == true) {
                            $scope.MandatoryObjList.push($scope.entityAllObjectives[i]);
                            $scope.IDList.push(parseInt($scope.entityAllObjectives[i].ObjectiveId, 10));
                        }
                        else {
                            $scope.NonMandatoryObjList.push($scope.entityAllObjectives[i]);
                        }
                    }
                    $scope.entityObjectiveList = $scope.NonMandatoryObjList;
                    $scope.MandatoryEntityObjectives = $scope.MandatoryObjList;
                    $scope.ngNoObjectivediv = false;
                    $scope.ngObjectiveDivHolder = true;
                }
                else {
                    $scope.ngObjectiveDivHolder = false;
                    $scope.ngNoObjectivediv = true;
                }
            }
            else {
                $scope.IDList = [];
            }

        }
        $scope.UploadImagefile = function (attributeId) {
            $scope.UploadAttributeId = attributeId;
            $("#pickfilesUploaderAttrSub").click();
        }

        function StrartUpload_UploaderAttrSubEntity() {
            $('.moxie-shim').remove();
            var uploader_AttrSub = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                browse_button: 'pickfilesUploaderAttrSub',
                container: 'filescontainerooSub',
                max_file_size: '10000mb',
                flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                url: 'Handlers/UploadHandler.ashx?Type=Attachment',
                chunk_size: '64Kb',
                multi_selection: false,
                multipart_params: {}

            });
            uploader_AttrSub.bind('Init', function (up, params) {
            });

            uploader_AttrSub.init();
            uploader_AttrSub.bind('FilesAdded', function (up, files) {
                up.refresh();
                uploader_AttrSub.start();
            });
            uploader_AttrSub.bind('UploadProgress', function (up, file) {

            });
            uploader_AttrSub.bind('Error', function (up, err) {
                bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                up.refresh(); // Reposition Flash/Silverlight
            });
            uploader_AttrSub.bind('FileUploaded', function (up, file, response) {
                UpdateFileDetails(file, response.response);
            });
            uploader_AttrSub.bind('BeforeUpload', function (up, file) {
                $.extend(up.settings.multipart_params, { id: file.id, size: file.size });
            });
        }
        function UpdateFileDetails(file, response) {
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");
            $scope.ImageFileName = resultArr[0] + extension;
            var PreviewID = "UploaderPreview_" + $scope.UploadAttributeId;
            $('#' + PreviewID).attr('src', 'UploadedImages/' + $scope.ImageFileName);
        }
        $scope.Clear = function () {
            $modalInstance.dismiss('cancel');
        }
        $scope.UploadAttributeData = [];
        $scope.EntityObjSelect = function (objectiveId) {
            var IsExist = $.grep($scope.nonMandatoryIDList, function (e) {
                if (e == objectiveId) {
                    return e;
                }
            })
            if (IsExist.length == 0) {
                $scope.nonMandatoryIDList.push(objectiveId);
            }
        }

        $(document).on('click', '.checkbox-custom > input[id=SelectAllObjectives]', function (e) {
            var status = this.checked;
            $('#nonMandatoryObjectiveBody input:checkbox').each(function () {
                this.checked = status;
                if (status) {
                    $(this).next('i').addClass('checked');
                } else {
                    $(this).next('i').removeClass('checked');
                }
            });
            for (var i = 0 ; i < $scope.entityObjectiveList.length; i++) {
                $scope.nonMandatoryIDList.push($scope.entityObjectiveList[i].ObjectiveId);
            }
            for (var i = 0 ; i < $scope.MandatoryEntityObjectives.length; i++) {
                $scope.IDList.push($scope.MandatoryEntityObjectives[i].ObjectiveId);
            }
        });

        //------------> RECURSIVE FUNCTION TO HIDE ALL THE ATTRIBUTE RELATIONS FOR THE SELECTED ATTRIBUTE <--------------
        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToShow = [];

            //----------> CHECK THE ATTRIBUTE ON ATTRIBUTE ID AND LEVEL AND GET THE ATTRIBUTE INFO TO HIDE <--------------
            if (attrLevel > 0) {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            }
            else {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }

            if (attributesToShow[0] != undefined) {
                for (var i = 0; i < attributesToShow[0].length; i++) {
                    var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                //----------> HIDE THE ATTRIBUTE AND CLEAR THE SCOPE <----------
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                //----------> FIND FOR NEXT ATTRIBUTE IF THE ATTRIBUTE RELATIONS EXISTS <-------------
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                //---------> IF THE ATTRIBUTE HAS RELATION WITH OTHER ATTRIBUTE START RECURSIVE AGAIN
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            }
                            else {
                                //----------> HIDE THE ATTRIBUTE AND CLEAR THE SCOPE <----------
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";

                                //----------> FIND FOR NEXT ATTRIBUTE IF THE ATTRIBUTE RELATIONS EXISTS <-------------
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));

                                //---------> IF THE ATTRIBUTE HAS RELATION WITH OTHER ATTRIBUTE START RECURSIVE AGAIN
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $scope.LoadSubLevels = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                //---------> 
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    //-----------> CLEAR THE SUB LEVEL ON SELECTING THE PARENT LEVEL
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        }
                        else if (attrType == 12) {
                            if (j == levelcnt)
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }

                    //-----------------> LOAD THE SUB LEVELS ON SELECTING PARENT LEVEL <----------------
                    if (attrType == 6) {
                        if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                    else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
                //----------------------------
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    }
                    catch (e) { }
                }

            }
            catch (e) { }
        }

        //--------------------> SHOW OR HIDE ATTRIBUTES FOR DROPDOWN ON SINGLE SELECTION <-----------------------
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToShow = [];
                var hideAttributeOtherThanSelected = [];
                //---------> 
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    //-----------> CLEAR THE SUB LEVEL ON SELECTING THE PARENT LEVEL
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        }
                        else if (attrType == 12) {
                            if (j == levelcnt)
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }

                    //-----------------> LOAD THE SUB LEVELS ON SELECTING PARENT LEVEL <----------------
                    if (attrType == 6) {
                        if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                    else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
                //----------------------------
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    }
                    catch (e) { }
                }
                //--------------> IF THERE IS NO ATTRIBUTE TO ATTRIBUTE RELATATIONS THEN RETURN BACK <--------------------------
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }

                //------------> RECURSIVE FUNCTION TO HIDE ATTRIBUTE ON RELATIONS
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);

                //-----> IF SINGLE SELECTION DROPDOWN SELECTED
                if (attrType == 3) {
                    if ($scope.fields['ListSingleSelection_' + attrID] != null) {
                        optionValue = parseInt(($scope.fields['ListSingleSelection_' + attrID]), 10);
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                }
                    //-----> IF MULTI SELECTION DROPDOWN SELECTED
                else if (attrType == 4) {
                    if ($scope.fields['ListMultiSelection_' + attrID] != null) {
                        optionValue = $scope.fields['ListMultiSelection_' + attrID];
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                    //-----> IF DROPDOWN TREE SELECTED
                else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                    //-----> IF TREE SELECTED
                else if (attrType == 7) {
                    if ($scope.fields['Tree_' + attrID] != null) {
                        optionValue = $scope.fields['Tree_' + attrID];
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }

                //----------> IF OPTION AVAILABLE FOR THE ATTRIBUTE SHOW THE ATTRIBUTE
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                //----------------> CHECK IF THE SELECTED ATTRIBUTE IS SINGLE SELECTION DROPDOWN OR DROPDOWN TREE <--------------------
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                }
                                else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (e) { }
        }

        //--------> HIDE ALL THE ATTRIBUTE RELAIONS AFTER LOADING <--------------------
        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                }
                                else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (e) { }
        }

        $scope.AddDefaultEndDate = function (objdateval) {
            $("#EntityMetadata").addClass('notvalidate');
            if (objdateval.startDate == null) {
                objdateval.endDate = null
            }
            else {
                objdateval.endDate = new Date.create(objdateval.startDate);
                objdateval.endDate = objdateval.endDate.addDays(7);
            }
        };

        $scope.CheckPreviousEndDate = function (enddate, startdate, currentindex) {
            var enddate1 = null;
            if (currentindex != 0) {
                enddate1 = $scope.items[currentindex - 1].endDate;
            }
            if (enddate1 != null && enddate1 >= startdate) {
                bootbox.alert($translate.instant('LanguageContents.Res_1987.Caption'));
                $scope.items[currentindex].startDate = null;
                $scope.items[currentindex].endDate = null;
            }
            else {
                $("#EntityMetadata").addClass('notvalidate');
                if (startdate == null) {
                    $scope.items[currentindex].endDate = null
                }
                else {
                    $scope.items[currentindex].endDate = (new Date.create(startdate)).addDays(7);
                }
            }
        };

        $scope.CheckPreviousStartDate = function (enddate, startdate, currentindex) {
            var enddate1 = null;

            if (currentindex != 0 || currentindex == 0) {
                enddate1 = $scope.items[currentindex].endDate;
            }
            var edate = ConvertDateToString(enddate1);

            var sdate = ConvertDateToString(startdate);
            if (enddate1 != null) {
                if (edate < sdate) {
                    bootbox.alert($translate.instant('LanguageContents.Res_4240.Caption'));
                    $scope.items[currentindex].endDate = null;
                }
            }
        };


        //dam file selection part
        function refreshAssetobjects() {
            $scope.PageNoobj = { pageno: 1 };
            $scope.TaskStatusObj = [{ "Name": "Thumbnail", ID: 1 }, { "Name": "Summary", ID: 2 }, { "Name": "List", ID: 3 }];
            $scope.SettingsDamAttributes = {};
            $scope.OrderbyObj = [{ "Name": "Name (Ascending)", ID: 1 }, { "Name": "Name (Descending)", ID: 2 }, { "Name": "Creation date (Ascending)", ID: 3 }, { "Name": "Creation date (Descending)", ID: 4 }];
            $scope.FilterStatus = { filterstatus: 1 };
            $scope.DamViewName = { viewName: "Thumbnail" };
            $scope.OrderbyName = { orderbyname: "Creation date (Descending)" };
            $scope.OrderBy = { order: 4 };
            $scope.AssetSelectionFiles = [];
            $scope.SelectedAssetIdFolderID.SelectedAssetIDs = [];
        }
        refreshAssetobjects();

        $scope.costcentreid = [];

        $scope.OnCostCentreTreesSelection = function (branch, parentArr) {
            if (branch.ischecked == true) {
                $scope.costcentreid.push(branch.id);
            }
            else {
                $scope.costcentreid.splice($scope.costcentreid.indexOf(branch.id), 1);
            }
        };
        $scope.origninalamount = 0;
        $scope.Getamountentered = function (atrid) {

            if (1 == $scope.fields["ListSingleSelection_" + atrid])
                $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '');
            else
                $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '') / 1 / $scope['currRate_' + atrid];
        }
        $scope.GetCostCentreCurrencyRateById = function (atrid) {
            SubentitycreationService.GetCostCentreCurrencyRateById(0, $scope.fields["ListSingleSelection_" + atrid], true).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope['currRate_' + atrid] = parseFloat(resCurrencyRate.Response[1]);
                    if ($scope['origninalamountvalue_' + atrid] != 0) {

                        //$scope.fields["dTextSingleLine_" + atrid] = parseInt((parseFloat($scope['origninalamountvalue_' + atrid]) * $scope['currRate_' + atrid])).formatMoney(0, ' ', ' ');
                        $scope.fields["dTextSingleLine_" + atrid] = (parseFloat($scope['origninalamountvalue_' + atrid]) * $scope['currRate_' + atrid]).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    }
                    //$scope.AvailableAssignAmount = $scope.AvailableAssignAmount * $scope.SelectedCostCentreCurrency.Rate;
                }
            });
        }



        SubentitycreationService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
            if (CurrencyListResult.Response != null)
                $scope.CurrencyFormatsList = CurrencyListResult.Response;
        });

        $scope.OnPageClose = function () {
            $scope.listAttriToAttriResult = [];
            $("#EntityMetadata").html('');
            $modalInstance.dismiss('cancel');
        }



        $scope.$on('callbackstoptimer', function (event) {
            $scope.$broadcast('callbackaftercreation');
        });

        $scope.$on('callbackacivatetimer', function (event) {
            $scope.$broadcast('callbackacivatetimercall');
        });

        $('#LightBoxPreview').on('hidden.bs.modal', function (event) {
            $scope.$broadcast('callbackaftercreation');
        });

        //change event for duedate
        $scope.changeduedate_changed = function (duedate, ID) {
            if (duedate != null) {
                var test = isValidDate(duedate.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(duedate, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert("Selected date should not same as non businessday");
                            $scope.fields["DatePart_" + ID] = "";
                        }
                    }
                }
                else {
                    $scope.fields["DatePart_" + ID] = "";
                    bootbox.alert("Select valid duedate");
                }
            }
        }
        //change event for period date
        $scope.changeperioddate_changed = function (date, datetype) {
            if (date != null) {
                var test = isValidDate(date.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(date, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert("Selected date should not same as non businessday");
                            if (datetype == "StartDate")
                                $scope.items[0].startDate = "";
                            else
                                $scope.item[0].endDate = "";
                        }
                    }
                }
                else {
                    if (datetype == "StartDate")
                        $scope.items[0].startDate = "";
                    else
                        $scope.item[0].endDate = "";
                    bootbox.alert("Select valid duedate");
                }
            }
        }

        function isValidDate(dateval, dateformat) {
            var formatlen;
            var defaultdateVal = [];
            defaultdateVal = dateval.length;
            formatlen = dateformat.length;
            if (formatlen == defaultdateVal || defaultdateVal > formatlen)
                return true;
            else
                return false;
        };
        SubentityTypeCreation(params.ID, params.Caption, params.CurrentEntityId);

        $scope.$on("$destroy", function () {
            setTimeout.cancel(SubentityTypeCreationTimeout);
            setTimeout.cancel(remValtimer);
            model, tree, ownername, ownerid, treeTextVisbileflag, treeformflag = null;
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.SubEntityCreationCtrl']"));
            //var model, remValtimer, IsValExist, getVal, Attributetypename, relationobj, ID, i, j, o, x, k, m, t, tabHtml, currentWizardStep, totalWizardSteps, keys, keys1, keys2, apple_selected, tree, treedata_avm, treedata_geography, _ref, remainRecord, result, costCentrevalues, memberObj, memberToRempve, ownername, ownerid, membervalues, ItemCnt, percentageflag, entityMetadataObj, metadata, SaveEntity, attributeLevelOptions, levelCount, value, MyDate, MyDateString, treenodes, multiselectiObject, res, totLevelCnt, CaptionObj, LabelObject, totLevelCnt1, temp, param1, param2, datStartUTCval, datstartval, datEndUTCval, datendval, mar, treeTextVisbileflag, treeformflag, remainRecord, attrInheritval, defaultmultiselectvalue, attrRelIDs, multiselectiObject, AttributeData, extension, resultArr, PreviewID, IsExist, status, recursiveAttrID, attributesToShow, currntlevel, optionValue, hideAttributeOtherThanSelected, attrval, enddate1, edate, sdate, test, a, formatlen, defaultdateVal = null;
            $scope.$broadcast('callbackaftercreation');
        });
        // --- Initialize. ---------------------------------- //

    }
    //);
    app.controller("mui.planningtool.SubEntityCreationCtrl", ['$scope', '$location', '$resource', '$http', '$cookies', '$compile', '$window', '$translate', 'SubentitycreationService', '$modalInstance', 'params', muiplanningtoolSubEntityCreationCtrl]);
})(angular, app);
///#source 1 1 /app/controllers/mui/planningtool/costcentre/detail/detailfilter-controller.js
(function(ng,app){"use strict";function muiplanningtoolcostcentredetaildetailfilterCtrl($scope,$location,$resource,$timeout,$cookies,$compile,$window,$translate,CcdetailfilterService){$scope.attributegroupTypeid=0;$scope.treeNodeSelectedHolderValues=[];var IsSave=0;var IsUpdate=0;$scope.deletefiltershow=false;$scope.showSave=true;$scope.showUpdate=false;$scope.EntityTypeID=0;$scope.appliedfilter='No filter applied';$scope.FilterFields={};$scope.PeriodOptionValue={};var DateValidate=dateFormat('1990-01-01',$scope.DefaultSettings.DateFormat);var elementNode='DetailFilter';var selectedfilterid=0;$scope.items=[];$scope.PercentageVisibleSettings={};$scope.DropDownFilterTreePricing={};$scope.TreePricing=[];$scope.FilterDataXML=[];if($window.CostCentreFilterName!=''&&$window.CostCentreFilterName!='No filter applied'){}else{$window.CostCentreFilterName='';}
var TypeID=5;$scope.visible=false;$scope.dynamicEntityValuesHolder={};$scope.dyn_Cont='';$scope.fieldKeys=[];$scope.options={};$scope.setFieldKeys=function(){var keys=[];angular.forEach($scope.FilterFields,function(key){keys.push(key);$scope.fieldKeys=keys;});}
$scope.OptionObj={};$scope.fieldoptions=[];$scope.setoptions=function(){var keys=[];angular.forEach($scope.OptionObj,function(key){keys.push(key);$scope.fieldoptions=keys;});}
$scope.filterValues=[];$scope.atributesRelationList=[];$scope.atributesRelationListWithTree=[];$scope.filterSettingValues=[];$scope.atributeGroupList=[];$scope.DetailFilterCreation=function(event){$scope.ClearScopeModle($scope.FilterFields);$scope.FilterSettingsLoad();$scope.Deletefilter=false;$scope.Applyfilter=true;$scope.Updatefilter=false;$scope.Savefilter=true;$scope.ngsaveasfilter='';$scope.ngKeywordtext='';IsSave=0;IsUpdate=0;};$("#DetailFilterCreation").on("onDetailFilterCreation",function(event){$scope.ClearcostCentrefilterScopeModle();});$("#costcentredetailfiltersettings").on("ClearScope",function(event,filterid){$scope.showSave=true;$scope.showUpdate=false;$scope.deletefiltershow=false;$scope.ClearScopeModle();});$scope.ClearFilterAttributes=function(){$scope.ClearFieldsAndReApply();}
$scope.ClearScopeModle=function(){$scope.items=[];$scope.clearscope=!$scope.clearscope;$scope.ngKeywordtext='';$scope.ngsaveasfilter='';$scope.ddlEntitymember=[];$("#activitydetailfilter").removeAttr('disabled');for(var variable in $scope.FilterFields){if(typeof $scope.FilterFields[variable]==="string"){if(variable!=="ListSingleSelection_69"){$scope.FilterFields[variable]="";}}else if(typeof $scope.FilterFields[variable]==="number"){$scope.FilterFields[variable]=null;}else if(Array.isArray($scope.FilterFields[variable])){$scope.FilterFields[variable]=[];}else if(typeof $scope.FilterFields[variable]==="object"){$scope.FilterFields[variable]={};}}
$scope.treePreviewObj={};$scope.treeNodeSelectedHolderValues=[];var treecount=$.grep($scope.atributesRelationList,function(e){return e.AttributeTypeId==7});for(var i=0;i<treecount.length;i++){ClearSavedTreeNode(treecount[i].AttributeId);}
$scope.costcemtreObject=[];$scope.MemberLists=[];$scope.attributegroupTypeid=0;}
$scope.ClearFieldsAndReApply=function(){$scope.attributegroupTypeid=0;IsSave=0;$scope.items=[];$scope.clearscope=!$scope.clearscope;$scope.ngsaveasfilter='';$scope.ngKeywordtext='';$scope.ddlParententitytypeId=[];$("#activitydetailfilter").removeAttr('disabled');for(var variable in $scope.FilterFields){if(typeof $scope.FilterFields[variable]==="string"){if(variable!=="ListSingleSelection_69"){$scope.FilterFields[variable]="";}}else if(typeof $scope.FilterFields[variable]==="number"){$scope.FilterFields[variable]=null;}else if(Array.isArray($scope.FilterFields[variable])){$scope.FilterFields[variable]=[];}else if(typeof $scope.FilterFields[variable]==="object"){$scope.FilterFields[variable]={};}}
$scope.treePreviewObj={};$scope.treeNodeSelectedHolderValues=[];var treecount=$.grep($scope.atributesRelationList,function(e){return e.AttributeTypeId==7});for(var i=0;i<treecount.length;i++){ClearSavedTreeNode(treecount[i].AttributeId);}
var pricingObj=$.grep($scope.atributesRelationList,function(e){return e.AttributeTypeId==13});if(pricingObj!=undefined)if(pricingObj.length>0)for(var t=0,obj;obj=pricingObj[t++];){for(var z=0,price;price=$scope.DropDownFilterTreePricing["AttributeId_Levels_"+obj.AttributeId+""][z++];){if(price.selection!=null)if(price.selection.length>0)price.selection=[];}}
$scope.ShowPeriodOptions=false;$scope.costcemtreObject=[];$scope.MemberLists=[];var ApplyFilterobj=[];$scope.showSave=true;$scope.showUpdate=false;$scope.deletefiltershow=false;$("#EntitiesTree").trigger("loadactivityfromfilterforCostCenter",[0,ApplyFilterobj]);$("#costcentredetailfilter").trigger('ClearAndReApply');}
$scope.ClearcostCentrefilterScopeModle=function(){for(var variable in $scope.FilterFields){if(typeof $scope.FilterFields[variable]==="string"){if(variable!=="ListSingleSelection_69"){$scope.FilterFields[variable]="";}}else if(typeof $scope.FilterFields[variable]==="number"){$scope.FilterFields[variable]=null;}else if(Array.isArray($scope.FilterFields[variable])){$scope.FilterFields[variable]=[];}else if(typeof $scope.FilterFields[variable]==="object"){$scope.FilterFields[variable]={};}}
$scope.costcemtreObject=[];$scope.MemberLists=[];}
function KeepAllStepsMarkedComplete(){$("#MyWizard ul.steps").find("li").addClass("complete");$("#MyWizard ul.steps").find("span.badge").addClass("badge-success");window["tid_wizard_steps_all_complete_count"]++;if(window["tid_wizard_steps_all_complete_count"]>=20){clearInterval(window["tid_wizard_steps_all_complete"]);}}
$scope.hideFilterSettings=function(){$('.FilterHolder').slideUp("slow")
$timeout(function(){$(window).AdjustHeightWidth();},500);}
$("#costcentredetailfiltersettings").on('loaddetailfiltersettings',function(event,TypeID){$scope.FilterSettingsLoad();});$scope.OntimeStatusLists=[{Id:0,Name:'On time',BtnBgColor:'btn-success',AbgColor:'background-image: linear-gradient(to bottom, #62C462, #51A351); color: #fff;',SelectedClass:'btn-success'},{Id:1,Name:'Delayed',BtnBgColor:'btn-warning',AbgColor:'background-image: linear-gradient(to bottom, #FBB450, #F89406); color: #fff;',SelectedClass:'btn-warning'},{Id:2,Name:'On hold',BtnBgColor:'btn-danger',AbgColor:'background-image: linear-gradient(to bottom, #EE5F5B, #BD362F); color: #fff;',SelectedClass:'btn-danger'}]
$scope.FilterSettingsLoad=function(){$scope.atributeGroupList=[];$scope.attributegroupTypeid=0;$scope.EntityHierarchyTypesResult=[];if($window.CostCentreFilterName==''){$window.CostCentreFilterName='';}
$scope.treesrcdirec={};$scope.treePreviewObj={};$scope.dyn_Cont='';$scope.Updatefilter=false;$scope.Applyfilter=true;$scope.Deletefilter=false;$scope.Savefilter=false;$("#dynamic_Controls").html('');$scope.atributesRelationList=[];$scope.atributesRelationListWithTree=[];$scope.ngsaveasfilter='';$scope.ngKeywordtext='';var OptionFrom=0;var IsKeyword="false";var IsEntityType="false";var IsEntityMember="false";var CurrentActiveVersion=0;var KeywordOptionsresponse;$scope.PeriodOptions=[{Id:1,FilterPeriod:'Between'},{Id:2,FilterPeriod:'Within'}];CcdetailfilterService.GetOptionsFromXML(elementNode,5).then(function(KeywordOptionsResult){KeywordOptionsresponse=KeywordOptionsResult.Response
OptionFrom=KeywordOptionsresponse.split(',')[0];IsKeyword=KeywordOptionsresponse.split(',')[1];IsEntityType=KeywordOptionsresponse.split(',')[3];IsEntityMember=KeywordOptionsresponse.split(',')[4];CurrentActiveVersion=KeywordOptionsresponse.split(',')[2];CcdetailfilterService.GettingEntityTypeHierarchyForAdminTree(6,3).then(function(GerParentEntityData){$scope.entitytpesdata=GerParentEntityData.Response;$scope.tagAllOptions.data=[];if(GerParentEntityData.Response!=null){$.each(GerParentEntityData.Response,function(i,el){$scope.tagAllOptions.data.push({"id":el.Id,"text":el.Caption,"ShortDescription":el.ShortDescription,"ColorCode":el.ColorCode});});}
$scope.ddlParententitytypeId=$scope.EntityHierarchyTypesResult;var IDList1=new Array();var filterattidsmeber={};filterattidsmeber.IDList=$window.ListofEntityID;;filterattidsmeber.TypeID=TypeID;filterattidsmeber.FilterType='DetailFilter';filterattidsmeber.OptionFrom=OptionFrom;filterattidsmeber.IsEntityMember=IsEntityMember
$scope.tagmemberOptions=[];CcdetailfilterService.GettingFilterEntityMember(filterattidsmeber).then(function(entityyMemberRelation){$scope.entityMemberRelationdata=entityyMemberRelation.Response;if(entityyMemberRelation.Response!=null){$.each(entityyMemberRelation.Response,function(i,el){$scope.tagmemberOptions.push({"id":el.Id,"Name":el.FirstName+' '+el.LastName});});}
var filterattids={};filterattids.TypeID=TypeID;filterattids.FilterType='DetailFilter';filterattids.OptionFrom=OptionFrom;filterattids.IDList=$window.ListofEntityID;filterattids.IsEntityMember=IsEntityMember
CcdetailfilterService.GettingFilterAttribute(filterattids).then(function(entityAttributesRelation){$scope.atributesRelationList=entityAttributesRelation.Response;if($scope.atributesRelationList!=undefined){for(var i=0;i<$scope.atributesRelationList.length;i++){if($scope.atributesRelationList[i].AttributeTypeId==6){$scope.dyn_Cont+='<div class="control-group"><span>'+$scope.atributesRelationList[i].DisplayName+' : </span>';$scope.dyn_Cont+='<div class="controls"><select multiple="multiple"  class=\"multiselect\" multiselect-dropdown data-placeholder="Select '+$scope.atributesRelationList[i].DisplayName+' options" ng-model="FilterFields.DropDown_'+$scope.atributesRelationList[i].AttributeId+'_'+$scope.atributesRelationList[i].TreeLevel+'"\  ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList['+i+'].LevelTreeNodes \">';$scope.dyn_Cont+='</select></div></div>';$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel]=[];}else if($scope.atributesRelationList[i].AttributeTypeId==12){$scope.dyn_Cont+='<div class="control-group"><span>'+$scope.atributesRelationList[i].DisplayName+' : </span>';$scope.dyn_Cont+='<div class="controls"><select multiple="multiple"  class=\"multiselect\" multiselect-dropdown data-placeholder="Select '+$scope.atributesRelationList[i].DisplayName+' options" ng-model="FilterFields.DropDown_'+$scope.atributesRelationList[i].AttributeId+'_'+$scope.atributesRelationList[i].TreeLevel+'"\  ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList['+i+'].LevelTreeNodes \">';$scope.dyn_Cont+='</select></div></div>';$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel]=[];}else if($scope.atributesRelationList[i].AttributeTypeId==13){var k=$scope.TreePricing.length;var treecount12=$.grep($scope.FilterDataXML,function(e){return e.AttributeId==$scope.atributesRelationList[i].AttributeId});if(treecount12.length==0){var mm=$scope.atributesRelationList[i].AttributeId;$scope.PercentageVisibleSettings["AttributeId_Levels_"+$scope.atributesRelationList[i].AttributeId.toString()+""]=false;$scope.DropDownFilterTreePricing["AttributeId_Levels_"+$scope.atributesRelationList[i].AttributeId.toString()+""]=$scope.atributesRelationList[i].DropdowntreePricingAttr;$scope.dyn_Cont+="<div drowdowntreepercentagemultiselectionfilter  data-purpose='entity' data-attributeid="+$scope.atributesRelationList[i].AttributeId.toString()+"></div>";$scope.FilterDataXML.push({"AttributeId":parseInt($scope.atributesRelationList[i].AttributeId)});}}else if($scope.atributesRelationList[i].AttributeTypeId==3){if($scope.atributesRelationList[i].AttributeId==SystemDefiendAttributes.Owner){$scope.dyn_Cont+="<div class=\"control-group\"><span>"+$scope.atributesRelationList[i].DisplayName+"</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_"+$scope.atributesRelationList[i].AttributeId+"\"  id=\"DropDown_"+$scope.atributesRelationList[i].AttributeId+"\"     ng-options=\"ndata.Id as (ndata.FirstName+' '+ndata.LastName)  for ndata in  atributesRelationList["+i+"].Users \"></select></div>";$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]=[];}else if($scope.atributesRelationList[i].AttributeId==SystemDefiendAttributes.EntityStatus){var EntityStatusOptions=$scope.atributesRelationList[i].EntityStatusOptionValues
$scope.tagAllOptionsEntityStatus.data=[];if(EntityStatusOptions!=null){$.each(EntityStatusOptions,function(i,el){$scope.tagAllOptionsEntityStatus.data.push({"id":el.ID,"text":el.StatusOptions,"ShortDescription":el.ShortDesc,"ColorCode":el.ColorCode});});}
$scope.dyn_Cont+="<div class='control-group'>";$scope.dyn_Cont+="<span>"+$scope.atributesRelationList[i].DisplayName+" : </span><div class=\"controls\">";$scope.dyn_Cont+="<input id='ddlEntityStatus' class='width2x' placeholder='Select Entity Status Options' type='hidden' ui-select2='tagAllOptionsEntityStatus' ng-model=\"FilterFields.DropDown_"+$scope.atributesRelationList[i].AttributeId+"\" />";$scope.dyn_Cont+="</div></div>";}else if($scope.atributesRelationList[i].AttributeId==SystemDefiendAttributes.EntityOnTimeStatus){$scope.dyn_Cont+='<div class=\"control-group\"><span>'+$scope.atributesRelationList[i].DisplayName+' : </span>';$scope.dyn_Cont+="<div class=\"controls\"><select  class=\"multiselect\" multiselect-dropdown  multiple=\"multiple\" ";$scope.dyn_Cont+="data-placeholder=\"Select Entity OnTime Status\" ng-model=\"FilterFields.DropDown_"+$scope.atributesRelationList[i].AttributeId+"\"";$scope.dyn_Cont+="id=\"DropDown_"+$scope.atributesRelationList[i].AttributeId+"\"  ng-options=\"opt.Id as opt.Name for opt in  OntimeStatusLists \">";$scope.dyn_Cont+="</select>";$scope.dyn_Cont+="</div></div>";}else{$scope.dyn_Cont+='<div class=\"control-group\"><span>'+$scope.atributesRelationList[i].DisplayName+' : </span>';$scope.dyn_Cont+='<div class=\"controls\"> <select multiple="multiple"  class=\"multiselect\" multiselect-dropdown data-placeholder="Select '+$scope.atributesRelationList[i].DisplayName+' options" ng-model=\"FilterFields.DropDown_'+$scope.atributesRelationList[i].AttributeId+'"\" ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList['+i+'].OptionValues \">';$scope.dyn_Cont+='</select></div></div>';$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]=[];}}else if($scope.atributesRelationList[i].AttributeTypeId==4){$scope.dyn_Cont+="<div class=\"control-group\"><span>"+$scope.atributesRelationList[i].DisplayName+"</span>";$scope.dyn_Cont+="<div class=\"controls\"><select  class=\"multiselect\" multiselect-dropdown  multiple=\"multiple\"";$scope.dyn_Cont+="data-placeholder=\"Select "+$scope.atributesRelationList[i].DisplayName+"\"";$scope.dyn_Cont+="ng-model=\"FilterFields.DropDown_"+$scope.atributesRelationList[i].AttributeId+"\"";$scope.dyn_Cont+="id=\"DropDown_"+$scope.atributesRelationList[i].AttributeId+"\"  ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList["+i+"].OptionValues \">";$scope.dyn_Cont+="</select>";$scope.dyn_Cont+="</div></div>";$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]=[];}else if($scope.atributesRelationList[i].AttributeTypeId==10){$scope.setoptions();$scope.items=[];$scope.items.push({startDate:'',endDate:''});$scope.dyn_Cont+="<div class=\"control-group\"><span>"+$scope.atributesRelationList[i].DisplayName+"</span><div class=\"controls\"><div class=\"period nomargin\" id=\"periodcontrols\"  ng-form=\"subForm\">";$scope.dyn_Cont+="<div class=\"row-fluid\">";$scope.dyn_Cont+="<input class=\"sdate\" data-date-format='"+$scope.DefaultSettings.DateFormat+"' id=\"items.startDate\" type=\"text\" value=\"\" name=\"startDate\" ng-model=\"items.startDate\" ng-click=\"Calanderopen($event,"+$scope.fields["DatePart_Calander_Open"+"items.startDate"]+")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open"+"items.startDate"+"\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- Start date --\" ng-change=\"OpenOptionsForPeriod()\"/><input class=\"edate\" type=\"text\" data-date-format='"+$scope.DefaultSettings.DateFormat+"' ng-click=\"Calanderopen($event,"+$scope.fields["DatePart_Calander_Open"+"items.endDate"]+")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open"+"items.endDate"+"\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" value=\"\" name=\"enddate\" id=\"items.endDate\" ng-model=\"items.endDate\" placeholder=\"-- End date --\" ng-change=\"OpenOptionsForPeriod()\"/>";$scope.dyn_Cont+="</div></div></div></div>";$scope.FilterFields["Period_"+$scope.atributesRelationList[i].AttributeId]=[];$scope.setFieldKeys();$scope.dyn_Cont+="<div ng-show=\"ShowPeriodOptions\" class=\"control-group\" id=\"ShowPeriodOptions\" data-attrid=\""+$scope.atributesRelationList[i].AttributeId+"\"><span>Period range : </span><div class=\"controls\">";$scope.dyn_Cont+="<select class=\"nomargin\" ui-select2 ng-model=\"PeriodOptionValue\"><option ng-repeat=\"ndata in PeriodOptions\" value=\"{{ndata.Id}}\">{{ndata.FilterPeriod}}</option></select></div></div>";$scope.ShowPeriodOptions=false;$scope.fields["DatePart_Calander_Open"+"items.startDate"]=false;$scope.fields["DatePart_Calander_Open"+"items.endDate"]=false;}}}
if($scope.atributesRelationList.length>0)$scope.atributeGroupList=$.grep($scope.atributesRelationList,function(e){return e.AttributeTypeId==3||e.AttributeTypeId==4||e.AttributeTypeId==6||e.AttributeTypeId==7||e.AttributeTypeId==12});if(IsEntityType=="True"){$scope.dyn_Cont+="<div class='control-group'>";$scope.dyn_Cont+="<span>EntityType : </span><div class=\"controls\">";$scope.dyn_Cont+="<input id='ddlChildren' class='width2x' placeholder='Select EntityType Options' type='hidden' ui-select2='tagAllOptions' ng-model='ddlParententitytypeId' />";$scope.dyn_Cont+="</div></div>";}
$scope.atributesRelationListWithTree=$.grep(entityAttributesRelation.Response,function(e){return e.AttributeTypeId==7})
for(var i=0;i<$scope.atributesRelationListWithTree.length;i++){if($scope.atributesRelationListWithTree[i].AttributeTypeId==7){$scope.treePreviewObj={};$scope.treesrcdirec["Attr_"+$scope.atributesRelationListWithTree[i].AttributeId]=JSON.parse($scope.atributesRelationListWithTree[i].tree).Children;if($scope.treesrcdirec["Attr_"+$scope.atributesRelationListWithTree[i].AttributeId].length>0){if(IsNotEmptyTree($scope.treesrcdirec["Attr_"+$scope.atributesRelationListWithTree[i].AttributeId])){$scope.treePreviewObj["Attr_"+$scope.atributesRelationListWithTree[i].AttributeId]=true;}else $scope.treePreviewObj["Attr_"+$scope.atributesRelationListWithTree[i].AttributeId]=false;}else{$scope.treePreviewObj["Attr_"+$scope.atributesRelationListWithTree[i].AttributeId]=false;}
$scope.dyn_Cont+='<div class="control-group treeNode-control-group">';$scope.dyn_Cont+='<span>'+$scope.atributesRelationListWithTree[i].DisplayName+'</span>';$scope.dyn_Cont+='<div class="controls treeNode-controls">';$scope.dyn_Cont+='<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_'+$scope.atributesRelationListWithTree[i].AttributeId+'" placeholder="Search" treecontext="treeNodeSearchDropdowns_Attr_'+$scope.atributesRelationListWithTree[i].AttributeId+'"></div>';$scope.dyn_Cont+='<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdowns_Attr_'+$scope.atributesRelationListWithTree[i].AttributeId+'">';$scope.dyn_Cont+='<span ng-if="doing_async">...loading...</span>';$scope.dyn_Cont+='<abn-tree tree-filter="filterValue_'+$scope.atributesRelationListWithTree[i].AttributeId+'" tree-data=\"treesrcdirec.Attr_'+$scope.atributesRelationListWithTree[i].AttributeId+'\" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';$scope.dyn_Cont+='</div></div>';$scope.dyn_Cont+='<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_'+$scope.atributesRelationListWithTree[i].AttributeId+'\">';$scope.dyn_Cont+='<div class="controls">';$scope.dyn_Cont+='<eu-tree tree-data=\"treesrcdirec.Attr_'+$scope.atributesRelationListWithTree[i].AttributeId+'\" node-attributeid="'+$scope.atributesRelationListWithTree[i].AttributeId+'" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';$scope.dyn_Cont+='</div></div></div>';}}
if(IsKeyword=="True"){$scope.dyn_Cont+="<div class=\"control-group\"><span>Keyword : </span><input type='text' ng-model='ngKeywordtext' id='ngKeywordtext' placeholder='Filter Keyword'></div>";}
if(IsEntityMember=="True"){$scope.dyn_Cont+="<div class=\"control-group\"><span>Members </span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model='ddlEntitymember'  id= 'ddlChildrenmember'     ng-options=\"ndata.id as (ndata.Name)  for ndata in  tagmemberOptions \"></select></div>";}
$("#dynamic_Controls").html($compile($scope.dyn_Cont)($scope));$scope.Applyfilter=true;$scope.Updatefilter=false;$scope.Deletefilter=false;$scope.Savefilter=true;});});});});}
$timeout($scope.FilterSettingsLoad(),100);$scope.OpenOptionsForPeriod=function(){$scope.PeriodOptionValue={};$scope.PeriodOptionValue=1;var startdate=$scope.items.startDate;var enddate=$scope.items.endDate;if(startdate!=null&&enddate!=null){if(startdate!=undefined&&enddate!=undefined){$scope.ShowPeriodOptions=true;}else $scope.ShowPeriodOptions=false;}else $scope.ShowPeriodOptions=false;}
$scope.FilterSave=function(){if(IsSave==1){return false;}
IsSave=1;if($scope.ngsaveasfilter==''||$scope.ngsaveasfilter==undefined){bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));IsSave=0;return false;}
var ApplyFilterobj=[];var FilterData={};var whereConditionData=[];var multiSelectVal=[];var usersVal=[];var orgLevel=[];var fiscalyear=[];for(var i=0;i<$scope.atributesRelationList.length;i++){if($scope.atributesRelationList[i].AttributeTypeId==6){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel]!=undefined){orgLevel=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel];for(var k=0;k<orgLevel.length;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel][k],'Level':$scope.atributesRelationList[i].TreeLevel,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId});}}}else if($scope.atributesRelationList[i].AttributeTypeId==12){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel]!=undefined){orgLevel=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel];for(var k=0;k<orgLevel.length;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel][k],'Level':$scope.atributesRelationList[i].TreeLevel,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId});}}}else if($scope.atributesRelationList[i].AttributeTypeId==13){var Array=$scope.atributesRelationList[i].DropdowntreePricingAttr.length;for(var ii=0;ii<Array;ii++){if($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection!=null){var j=$scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;for(var k=0;k<j;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],'Level':ii+1,'AttributeTypeId':13,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':''});}}}}else if($scope.atributesRelationList[i].AttributeTypeId==3&&$scope.atributesRelationList[i].AttributeId==SystemDefiendAttributes.Owner){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]!=undefined){usersVal=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId];for(var k=0;k<usersVal.length;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':usersVal[k],'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId});}}}else if($scope.atributesRelationList[i].AttributeTypeId==7){var treenodes=[];treenodes=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==$scope.atributesRelationList[i].AttributeId;});for(var x=0,nodeval;nodeval=treenodes[x++];){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':parseInt(nodeval.id,10),'Level':parseInt(nodeval.Level,10),'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId});}}else if($scope.atributesRelationList[i].AttributeTypeId==3||$scope.atributesRelationList[i].AttributeTypeId==4){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]!=undefined){fiscalyear=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]
for(var k=0;k<fiscalyear.length;k++){if(fiscalyear[k].Id!=undefined){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k].Id,'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId});}else{if($scope.atributesRelationList[i].AttributeId==71){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k].id,'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId});}else{whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k],'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId});}}}}}}
var filtertype=2;if($scope.attributegroupTypeid!=0&&$scope.attributegroupTypeid!=undefined){whereConditionData.splice(0,whereConditionData.length);var res=$.grep($scope.atributesRelationList,function(e){return e.AttributeId==parseInt($scope.attributegroupTypeid,10)})[0];if(res.AttributeId==71){for(var k=0;k<res.EntityStatusOptionValues.length;k++){whereConditionData.push({'AttributeID':res.AttributeId,'SelectedValue':res.EntityStatusOptionValues[k].ID,'Level':0,'AttributeTypeId':res.AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':"",'EndDate':""});}}else if(res.AttributeTypeId==6||res.AttributeTypeId==12){for(var k=0;k<res.LevelTreeNodes.length;k++){whereConditionData.push({'AttributeID':res.AttributeId,'SelectedValue':res.LevelTreeNodes[k].Id,'Level':res.LevelTreeNodes[k].Level,'AttributeTypeId':res.AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':"",'EndDate':""});}}else if(res.AttributeTypeId==7){GetAllTreeObject(res.AttributeId);var treenodes=[];treenodes=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==res.AttributeId;});for(var x=0,nodeval;nodeval=treenodes[x++];){whereConditionData.push({'AttributeID':res.AttributeId,'SelectedValue':parseInt(nodeval.id,10),'Level':parseInt(nodeval.Level,10),'AttributeTypeId':res.AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':''});}}else{for(var k=0;k<res.OptionValues.length;k++){whereConditionData.push({'AttributeID':res.AttributeId,'SelectedValue':res.OptionValues[k].Id,'Level':0,'AttributeTypeId':res.AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':"",'EndDate':""});}}
FilterData.IsDetailFilter=6;filtertype=6;}else FilterData.IsDetailFilter=1;var entitytypeIdforFilter='';if($scope.ddlParententitytypeId!=undefined){if($scope.ddlParententitytypeId[0]!=undefined){for(var l=0;l<$scope.ddlParententitytypeId.length;l++){if(entitytypeIdforFilter==''){entitytypeIdforFilter=$scope.ddlParententitytypeId[l].id;}else{entitytypeIdforFilter=entitytypeIdforFilter+","+$scope.ddlParententitytypeId[l].id;}}}}
var entitymemberIdforFilter='';if($scope.ddlEntitymember!=undefined){if($scope.ddlEntitymember[0]!=undefined){for(var x=0;x<$scope.ddlEntitymember.length;x++){if(entitymemberIdforFilter==''){entitymemberIdforFilter=$scope.ddlEntitymember[x];}else{entitymemberIdforFilter=entitymemberIdforFilter+","+$scope.ddlEntitymember[x];}}}}
FilterData.FilterId=0;FilterData.FilterName=$scope.ngsaveasfilter;FilterData.Keyword=$scope.ngKeywordtext;FilterData.UserId=1;FilterData.TypeID=TypeID;if(entitytypeIdforFilter!=''){FilterData.entityTypeId=entitytypeIdforFilter+',5';}else{FilterData.entityTypeId='';}
FilterData.EntitymemberId=entitymemberIdforFilter
if($scope.items.startDate!=undefined&&$scope.items.endDate!=undefined){FilterData.StarDate=dateFormat($scope.items.startDate,'yyyy/mm/dd');FilterData.EndDate=dateFormat($scope.items.endDate,'yyyy/mm/dd');whereConditionData.push({'AttributeID':$("#ShowPeriodOptions").attr("data-attrid"),'SelectedValue':$scope.PeriodOptionValue,'Level':0,'AttributeTypeId':10});}else{FilterData.EndDate='';FilterData.StarDate='';}
FilterData.WhereConditon=whereConditionData;CcdetailfilterService.InsertFilterSettings(FilterData).then(function(filterSettingsInsertresult){$('#FilterSettingsModal').modal('hide');if(filterSettingsInsertresult.StatusCode==405){NotifyError($translate.instant('LanguageContents.Res_4279.Caption'));IsSave=0;}else{NotifySuccess($translate.instant('LanguageContents.Res_4397.Caption'));IsSave=0;$("#EntitiesTree").trigger("loadactivityfromfilterforCostCenter",[filterSettingsInsertresult.Response,ApplyFilterobj,filtertype]);$("#costcentredetailfilter").trigger('reloadccdetailfilter',[TypeID,$scope.ngsaveasfilter,filterSettingsInsertresult.Response,filtertype]);}});var multiSelectVal=[];var usersVal=[];var orgLevel=[];};$("#costcentredetailfiltersettings").on('EditFilterSettingsByFilterID',function(event,filterId,typeId,filtertypeval){$scope.showSave=false;$scope.showUpdate=true;$scope.deletefiltershow=true;$scope.LoadFilterSettingsByFilterID(event,filterId,typeId,filtertypeval);});$scope.LoadFilterSettingsByFilterID=function(event,filterId,typeId,filtertypeval){$scope.ClearScopeModle();$scope.Updatefilter=true;$scope.Applyfilter=false;$scope.Deletefilter=true;$scope.Savefilter=false;$scope.filterSettingValues=[];selectedfilterid=filterId;CcdetailfilterService.GetFilterSettingValuesByFilertId(filterId).then(function(filterSettingsValues){$scope.filterSettingValues=filterSettingsValues.Response;if($scope.filterSettingValues!=null){if(filtertypeval!=6){for(var i=0;i<$scope.filterSettingValues.FilterValues.length;i++){if($scope.filterSettingValues.FilterValues[i].AttributeTypeId==3&&$scope.filterSettingValues.FilterValues[i].AttributeId==SystemDefiendAttributes.Owner){if($scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId]!=undefined)$scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId].push($scope.filterSettingValues.FilterValues[i].Value);}else if($scope.filterSettingValues.FilterValues[i].AttributeTypeId==3||$scope.filterSettingValues.FilterValues[i].AttributeTypeId==4){if($scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId]!=undefined){if($scope.filterSettingValues.FilterValues[i].AttributeId==71){var statusselectedoptions=$.grep($scope.atributesRelationList,function(e){return e.AttributeId==parseInt(71);})[0].EntityStatusOptionValues;var seletedoptionvalue=[];for(var sts=0;sts<$.grep($scope.filterSettingValues.FilterValues,function(e){return e.AttributeId==parseInt(71);}).length;sts++){seletedoptionvalue.push($.grep($scope.tagAllOptionsEntityStatus.data,function(e){return e.id==parseInt($scope.filterSettingValues.FilterValues[sts].Value);})[0]);if(seletedoptionvalue!=null){$.each(seletedoptionvalue,function(i,el){$scope.tagAllOptionsEntityStatus.data.push({"id":el.ID,"text":el.StatusOptions,"ShortDescription":el.ShortDesc,"ColorCode":el.ColorCode});});}
$scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId]=seletedoptionvalue;}}else{$scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId].push($scope.filterSettingValues.FilterValues[i].Value);}}}else if($scope.filterSettingValues.FilterValues[i].Level!=0&&$scope.filterSettingValues.FilterValues[i].AttributeTypeId==6){if($scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId+"_"+$scope.filterSettingValues.FilterValues[i].Level]!=undefined){$scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId+"_"+$scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);}}else if($scope.filterSettingValues.FilterValues[i].AttributeTypeId==7){$scope.treesrcdirec["Attr_"+$scope.filterSettingValues.FilterValues[i].AttributeId]=JSON.parse($scope.filterSettingValues.FilterValues[i].TreeValues).Children;GetTreeObjecttoSave($scope.filterSettingValues.FilterValues[i].AttributeId);if(IsNotEmptyTree($scope.treesrcdirec["Attr_"+$scope.filterSettingValues.FilterValues[i].AttributeId])){$scope.treePreviewObj["Attr_"+$scope.filterSettingValues.FilterValues[i].AttributeId]=true;}else $scope.treePreviewObj["Attr_"+$scope.filterSettingValues.FilterValues[i].AttributeId]=false;}else if($scope.filterSettingValues.FilterValues[i].Level!=0&&$scope.filterSettingValues.FilterValues[i].AttributeTypeId==12){if($scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId+"_"+$scope.filterSettingValues.FilterValues[i].Level]!=undefined){$scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId+"_"+$scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);}}else if($scope.filterSettingValues.FilterValues[i].AttributeTypeId==13){$scope.DropDownFilterTreePricing["AttributeId_Levels_"+$scope.filterSettingValues.FilterValues[i].AttributeId]=$scope.filterSettingValues.FilterValues[i].DropdowntreePricingData;}else if($scope.filterSettingValues.FilterValues[i].AttributeTypeId==10){if($scope.filterSettingValues.StartDate!=null){$scope.ShowPeriodOptions=true;$scope.PeriodOptionValue=$scope.filterSettingValues.FilterValues[i].Value;}}}}else $scope.attributegroupTypeid=$scope.filterSettingValues.FilterValues[0].AttributeId;$scope.items.push({startDate:$scope.filterSettingValues.StartDate,endDate:$scope.filterSettingValues.EndDate});CcdetailfilterService.GettingEntityTypeHierarchyForAdminTree(6,3).then(function(GerParentEntityData){$scope.entitytpesdata=GerParentEntityData.Response;$scope.tagAllOptions.data=[];if(GerParentEntityData.Response!=null){$.each(GerParentEntityData.Response,function(i,el){$scope.tagAllOptions.data.push({"id":el.Id,"text":el.Caption,"ShortDescription":el.ShortDescription,"ColorCode":el.ColorCode});});}});$scope.EntityHierarchyTypesResult=[];for(var l=0;l<filterSettingsValues.Response.EntityTypeID.split(',').length;l++){if(filterSettingsValues.Response.EntityTypeID.split(',')[l]!='0'){if(filterSettingsValues.Response.EntityTypeID.split(',')[l]!="5")$scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data,function(e){return e.id==filterSettingsValues.Response.EntityTypeID.split(',')[l]})[0]);}}
$scope.ddlParententitytypeId=$scope.EntityHierarchyTypesResult==undefined?0:$scope.EntityHierarchyTypesResult[0]==undefined?0:$scope.EntityHierarchyTypesResult;if(filterSettingsValues.Response.EntityMemberID!=""&&filterSettingsValues.Response.EntityMemberID!=null){for(var l=0;l<filterSettingsValues.Response.EntityMemberID.split(',').length;l++){if(filterSettingsValues.Response.EntityMemberID.split(',')[l]!='0'){if($.grep($scope.tagmemberOptions,function(e){return e.id==filterSettingsValues.Response.EntityMemberID.split(',')[l]})[0]!=undefined){var mval=filterSettingsValues.Response.EntityMemberID.split(',')[l];if(mval!=""&&mval!=undefined)$scope.ddlEntitymember.push(parseInt(mval));}}}}
var startdateupdate=$scope.filterSettingValues.StartDate;var enddateupdate=$scope.filterSettingValues.EndDate;var editstartdate=startdateupdate.split('/');var editenddate=enddateupdate.split('/');startdateupdate=dateFormat(startdateupdate,$scope.DefaultSettings.DateFormat);enddateupdate=dateFormat(enddateupdate,$scope.DefaultSettings.DateFormat);if($scope.filterSettingValues.StartDate==""||$scope.filterSettingValues.EndDate==""){$scope.items.push({startDate:"",endDate:""});}else if($scope.filterSettingValues.StartDate!=DateValidate||$scope.filterSettingValues.EndDate!=DateValidate){$scope.items.startDate=startdateupdate;$scope.items.endDate=enddateupdate;}else{$scope.items.push({startDate:DateValidate,endDate:DateValidate});}
$scope.ngKeywordtext=$scope.filterSettingValues.Keyword;$scope.ngsaveasfilter=$scope.filterSettingValues.FilterName;$scope.Updatefilter=true;$scope.Applyfilter=false;$scope.Deletefilter=true;$scope.Savefilter=false;}});}
$scope.FilterUpdate=function(){if(IsUpdate==1){return false;}
IsUpdate=1;var ApplyFilterobj=[];var FilterData={};$scope.filterSettingValues.Keyword='';$scope.filterSettingValues.FilterName='';var whereConditionData=[];var multiSelectVal=[];var usersVal=[];var orgLevel=[];var fiscalyear=[];var entitytypeIdforFilter='';if($scope.ddlParententitytypeId!=undefined){if($scope.ddlParententitytypeId[0]!=undefined){for(var l=0;l<$scope.ddlParententitytypeId.length;l++){if(entitytypeIdforFilter==''){entitytypeIdforFilter=$scope.ddlParententitytypeId[l].id;}else{entitytypeIdforFilter=entitytypeIdforFilter+","+$scope.ddlParententitytypeId[l].id;}}}}
var entitymemberIdforFilter='';if($scope.ddlEntitymember!=undefined){if($scope.ddlEntitymember[0]!=undefined){for(var x=0;x<$scope.ddlEntitymember.length;x++){if(entitymemberIdforFilter==''){entitymemberIdforFilter=$scope.ddlEntitymember[x];}else{entitymemberIdforFilter=entitymemberIdforFilter+","+$scope.ddlEntitymember[x];}}}}
for(var i=0;i<$scope.atributesRelationList.length;i++){if($scope.atributesRelationList[i].AttributeTypeId==6){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel]!=undefined){orgLevel=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel];for(var k=0;k<orgLevel.length;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel][k],'Level':$scope.atributesRelationList[i].TreeLevel,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'EntityMemberIDs':entitymemberIdforFilter});}}}else if($scope.atributesRelationList[i].AttributeTypeId==12){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel]!=undefined){orgLevel=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel];for(var k=0;k<orgLevel.length;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel][k],'Level':$scope.atributesRelationList[i].TreeLevel,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'EntityMemberIDs':entitymemberIdforFilter});}}}else if($scope.atributesRelationList[i].AttributeTypeId==13){var Array=$scope.atributesRelationList[i].DropdowntreePricingAttr.length;for(var ii=0;ii<Array;ii++){if($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection!=null){var j=$scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;for(var k=0;k<j;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],'Level':ii+1,'AttributeTypeId':13,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':''});}}}}else if($scope.atributesRelationList[i].AttributeTypeId==3&&$scope.atributesRelationList[i].AttributeId==SystemDefiendAttributes.Owner){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]!=undefined){usersVal=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId];for(var k=0;k<usersVal.length;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':usersVal[k],'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'EntityMemberIDs':entitymemberIdforFilter});}}}else if($scope.atributesRelationList[i].AttributeTypeId==7){var treenodes=[];treenodes=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==$scope.atributesRelationList[i].AttributeId;});for(var x=0,nodeval;nodeval=treenodes[x++];){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':parseInt(nodeval.id,10),'Level':parseInt(nodeval.Level,10),'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'EntityMemberIDs':entitymemberIdforFilter});}}else if($scope.atributesRelationList[i].AttributeTypeId==3||$scope.atributesRelationList[i].AttributeTypeId==4){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]!=undefined){fiscalyear=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]
for(var k=0;k<fiscalyear.length;k++){if(fiscalyear[k].Id!=undefined){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k].Id,'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'EntityMemberIDs':entitymemberIdforFilter});}else{if($scope.atributesRelationList[i].AttributeId==71){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k].id,'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'EntityMemberIDs':entitymemberIdforFilter});}else{whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k],'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'EntityMemberIDs':entitymemberIdforFilter});}}}}}}
FilterData.FilterId=selectedfilterid;FilterData.TypeID=TypeID;FilterData.FilterName=$scope.ngsaveasfilter;FilterData.Keyword=$scope.ngKeywordtext;FilterData.UserId=1;FilterData.entityTypeId=entitytypeIdforFilter;FilterData.IsDetailFilter=1;FilterData.EntitymemberId=entitymemberIdforFilter
if($scope.items.startDate!=undefined&&$scope.items.endDate!=undefined){FilterData.StarDate=dateFormat($scope.items.startDate,'yyyy/mm/dd');FilterData.EndDate=dateFormat($scope.items.endDate,'yyyy/mm/dd');whereConditionData.push({'AttributeID':$("#ShowPeriodOptions").attr("data-attrid"),'SelectedValue':$scope.PeriodOptionValue,'Level':0,'AttributeTypeId':10,'EntityTypeIDs':entitytypeIdforFilter,'EntityMemberIDs':entitymemberIdforFilter});}else{FilterData.EndDate='';FilterData.StarDate='';}
var filtertype=2;if($scope.attributegroupTypeid!=0&&$scope.attributegroupTypeid!=undefined){whereConditionData.splice(0,whereConditionData.length);var res=$.grep($scope.atributesRelationList,function(e){return e.AttributeId==parseInt($scope.attributegroupTypeid,10)})[0];if(res.AttributeId==71){for(var k=0;k<res.EntityStatusOptionValues.length;k++){whereConditionData.push({'AttributeID':res.AttributeId,'SelectedValue':res.EntityStatusOptionValues[k].ID,'Level':0,'AttributeTypeId':res.AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':"",'EndDate':"",'EntityMemberIDs':entitymemberIdforFilter});}}else if(res.AttributeTypeId==6||res.AttributeTypeId==12){for(var k=0;k<res.LevelTreeNodes.length;k++){whereConditionData.push({'AttributeID':res.AttributeId,'SelectedValue':res.LevelTreeNodes[k].Id,'Level':res.LevelTreeNodes[k].Level,'AttributeTypeId':res.AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':"",'EndDate':"",'EntityMemberIDs':entitymemberIdforFilter});}}else if(res.AttributeTypeId==7){GetAllTreeObject(res.AttributeId);var treenodes=[];treenodes=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==res.AttributeId;});for(var x=0,nodeval;nodeval=treenodes[x++];){whereConditionData.push({'AttributeID':res.AttributeId,'SelectedValue':parseInt(nodeval.id,10),'Level':parseInt(nodeval.Level,10),'AttributeTypeId':res.AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','EntityMemberIDs':entitymemberIdforFilter});}}else{for(var k=0;k<res.OptionValues.length;k++){whereConditionData.push({'AttributeID':res.AttributeId,'SelectedValue':res.OptionValues[k].Id,'Level':0,'AttributeTypeId':res.AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':"",'EndDate':"",'EntityMemberIDs':entitymemberIdforFilter});}}
filtertype=6;FilterData.IsDetailFilter=6;}else FilterData.IsDetailFilter=1;if(whereConditionData.length==0&&entitymemberIdforFilter!=''){whereConditionData.push({'AttributeID':0,'SelectedValue':0,'Level':0,'AttributeTypeId':0,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}
FilterData.WhereConditon=whereConditionData;CcdetailfilterService.InsertFilterSettings(FilterData).then(function(filterSettingsUpdateResult){if(filterSettingsUpdateResult.StatusCode==405){NotifyError($translate.instant('LanguageContents.Res_4358.Caption'));IsUpdate=0;}else{NotifySuccess($translate.instant('LanguageContents.Res_4400.Caption'));IsUpdate=0;$scope.appliedfilter=$scope.ngsaveasfilter;$("#EntitiesTree").trigger("loadactivityfromfilterforCostCenter",[filterSettingsUpdateResult.Response,ApplyFilterobj,filtertype]);$("#costcentredetailfilter").trigger('reloadccdetailfilter',[TypeID,$scope.ngsaveasfilter,filterSettingsUpdateResult.Response,filtertype]);}});var multiSelectVal=[];var usersVal=[];var orgLevel=[];};$scope.FilterID=0;$("#costcentredetailfiltersettings").on('applyplandetailfilter',function(event,FilterID,FilterName,filterval){$scope.ApplyFilter(FilterID,FilterName,filterval);});$scope.ApplyFilter=function(FilterID,FilterName,filterval){var filtertype=2;$('#FilterSettingsModal').modal('hide');var StartRowNo=0;var MaxNoofRow=30;var PageIndex=0;if(FilterID!=undefined){selectedfilterid=FilterID;$scope.FilterID=FilterID;filtertype=filterval;}
$scope.appliedfilter=FilterName;$window.CostCentreFilterName=FilterName;if(FilterID==0){$scope.appliedfilter="No filter applied";}
var whereConditionData=[];var multiSelectVal=[];var usersVal=[];var orgLevel=[];var fiscalyear=[];var entitytypeIdforFilter='';if($scope.ddlParententitytypeId!=undefined){if($scope.ddlParententitytypeId[0]!=undefined){for(var l=0;l<$scope.ddlParententitytypeId.length;l++){if(entitytypeIdforFilter==''){entitytypeIdforFilter=$scope.ddlParententitytypeId[l].id;}else{entitytypeIdforFilter=entitytypeIdforFilter+","+$scope.ddlParententitytypeId[l].id;}}}}
var entitymemberIdforFilter='';if($scope.ddlEntitymember!=undefined){if($scope.ddlEntitymember[0]!=undefined){for(var x=0;x<$scope.ddlEntitymember.length;x++){if(entitymemberIdforFilter==''){entitymemberIdforFilter=$scope.ddlEntitymember[x];}else{entitymemberIdforFilter=entitymemberIdforFilter+","+$scope.ddlEntitymember[x];}}}}
for(var i=0;i<$scope.atributesRelationList.length;i++){if($scope.atributesRelationList[i].AttributeTypeId==6){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel]!=undefined){orgLevel=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel];for(var k=0;k<orgLevel.length;k++){if($scope.ngKeywordtext!=''){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel][k],'Level':$scope.atributesRelationList[i].TreeLevel,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':'','Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':''});}else{whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel][k],'Level':$scope.atributesRelationList[i].TreeLevel,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':'','Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':''});}}}}else if($scope.atributesRelationList[i].AttributeTypeId==12){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel]!=undefined){orgLevel=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel];for(var k=0;k<orgLevel.length;k++){if($scope.ngKeywordtext!=''){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel][k],'Level':$scope.atributesRelationList[i].TreeLevel,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':'','Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':''});}else{whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel][k],'Level':$scope.atributesRelationList[i].TreeLevel,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':'','Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':''});}}}}else if($scope.atributesRelationList[i].AttributeTypeId==13){var Array=$scope.atributesRelationList[i].DropdowntreePricingAttr.length;for(var ii=0;ii<Array;ii++){if($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection!=null){var j=$scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;for(var k=0;k<j;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],'Level':ii+1,'AttributeTypeId':13,'EntityTypeIDs':'','Keyword':'','EntityMemberIDs':''});}}}}else if($scope.atributesRelationList[i].AttributeTypeId==7){var treenodes=[];treenodes=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==$scope.atributesRelationList[i].AttributeId;});for(var x=0,nodeval;nodeval=treenodes[x++];){if($scope.ngKeywordtext!='')whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':parseInt(nodeval.id,10),'Level':parseInt(nodeval.Level,10),'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':$scope.ngKeywordtext,'EntityMemberIDs':''});else whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':parseInt(nodeval.id,10),'Level':parseInt(nodeval.Level,10),'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','EntityMemberIDs':''});}}else if($scope.atributesRelationList[i].AttributeTypeId==3&&$scope.atributesRelationList[i].AttributeId==SystemDefiendAttributes.Owner){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]!=undefined){usersVal=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId];for(var k=0;k<usersVal.length;k++){if($scope.ngKeywordtext!=''){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':usersVal[k],'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':'','Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':''});}else{whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':usersVal[k],'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':'','Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':''});}}}}else if($scope.atributesRelationList[i].AttributeTypeId==3||$scope.atributesRelationList[i].AttributeTypeId==4){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]!=undefined){fiscalyear=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]
for(var k=0;k<fiscalyear.length;k++){if(fiscalyear[k].Id!=undefined){if($scope.ngKeywordtext!=''){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k].Id,'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':'5','Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':''});}else{whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k].Id,'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':'','Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':''});}}else{if($scope.ngKeywordtext!=''){if($scope.atributesRelationList[i].AttributeId==71){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k].id,'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':'5','Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':''});}else{whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k],'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':'5','Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':''});}}else{if($scope.atributesRelationList[i].AttributeId==71){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k].id,'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':'','Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':''});}else{whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k],'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':'','Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':''});}}}}}}}
if(entitytypeIdforFilter!=''){if($scope.ngKeywordtext!=''){whereConditionData.push({'AttributeID':0,'SelectedValue':0,'Level':0,'AttributeTypeId':0,'EntityTypeIDs':entitytypeIdforFilter+',5','Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}else{whereConditionData.push({'AttributeID':0,'SelectedValue':0,'Level':0,'AttributeTypeId':0,'EntityTypeIDs':entitytypeIdforFilter+',5','Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}}
if(whereConditionData.length==0&&$scope.ngKeywordtext!=''){whereConditionData.push({'AttributeID':0,'SelectedValue':0,'Level':0,'AttributeTypeId':0,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}
if(whereConditionData.length==0&&($scope.items.startDate!=undefined||$scope.items.endDate!=undefined)){whereConditionData.push({'AttributeID':0,'SelectedValue':0,'Level':0,'AttributeTypeId':0,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}
if(whereConditionData.length==0&&$scope.entitymemberIdforFilter!=''){whereConditionData.push({'AttributeID':0,'SelectedValue':0,'Level':0,'AttributeTypeId':0,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}
var StartDate;var EndDate;var ApplyFilterobj=[];var optionperiod=$scope.PeriodOptionValue;if($scope.items.startDate==undefined)optionperiod=0;if($scope.items.endDate==undefined)optionperiod=0;if($scope.items.startDate!=undefined&&$scope.items.endDate!=undefined)optionperiod=$scope.PeriodOptionValue;if($scope.items.startDate!=undefined)StartDate=dateFormat($scope.items.startDate,'yyyy/mm/dd');if($scope.items.endDate!=undefined)EndDate=dateFormat($scope.items.endDate,'yyyy/mm/dd');if($scope.items.startDate!=undefined||$scope.items.endDate!=undefined)whereConditionData.push({'AttributeID':$("#ShowPeriodOptions").attr("data-attrid"),'SelectedValue':optionperiod,'Level':0,'AttributeTypeId':10,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});var ApplyFilterobj=[];if($scope.attributegroupTypeid!=0&&$scope.attributegroupTypeid!=undefined){whereConditionData.splice(0,whereConditionData.length);var res=$.grep($scope.atributesRelationList,function(e){return e.AttributeId==parseInt($scope.attributegroupTypeid,10)})[0];if(res.AttributeId==71){for(var k=0;k<res.EntityStatusOptionValues.length;k++){whereConditionData.push({'AttributeID':res.AttributeId,'SelectedValue':res.EntityStatusOptionValues[k].ID,'Level':0,'AttributeTypeId':res.AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':"",'EndDate':"",'EntityMemberIDs':entitymemberIdforFilter});}}else if(res.AttributeTypeId==6||res.AttributeTypeId==12){for(var k=0;k<res.LevelTreeNodes.length;k++){whereConditionData.push({'AttributeID':res.AttributeId,'SelectedValue':res.LevelTreeNodes[k].Id,'Level':res.LevelTreeNodes[k].Level,'AttributeTypeId':res.AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':"",'EndDate':"",'EntityMemberIDs':entitymemberIdforFilter});}}else if(res.AttributeTypeId==7){GetAllTreeObject(res.AttributeId);var treenodes=[];treenodes=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==res.AttributeId;});for(var x=0,nodeval;nodeval=treenodes[x++];){whereConditionData.push({'AttributeID':res.AttributeId,'SelectedValue':parseInt(nodeval.id,10),'Level':parseInt(nodeval.Level,10),'AttributeTypeId':res.AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','EntityMemberIDs':entitymemberIdforFilter});}}else if(res.AttributeTypeId==3&&res.AttributeId==SystemDefiendAttributes.Owner){for(var k=0;k<res.Users.length;k++){whereConditionData.push({'AttributeID':res.AttributeId,'SelectedValue':res.Users[k].Id,'Level':0,'AttributeTypeId':res.AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':"",'EndDate':"",'EntityMemberIDs':entitymemberIdforFilter});}}else{for(var k=0;k<res.OptionValues.length;k++){whereConditionData.push({'AttributeID':res.AttributeId,'SelectedValue':res.OptionValues[k].Id,'Level':0,'AttributeTypeId':res.AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':"",'EndDate':"",'EntityMemberIDs':entitymemberIdforFilter});}}
ApplyFilterobj=whereConditionData;filtertype=6;}else{if(FilterID!=0){ApplyFilterobj=whereConditionData;}else{ApplyFilterobj=[];}}
$("#EntitiesTree").trigger("loadactivityfromfilterforCostCenter",[FilterID,ApplyFilterobj,filtertype])};$scope.filtersettingsreset=function(){var StartRowNo=0;var MaxNoofRow=30;var PageIndex=0;selectedfilterid=0;};$scope.DeleteFilter=function DeleteFilterSettingsValue(){var ApplyFilterobj=[];bootbox.confirm($translate.instant('LanguageContents.Res_2026.Caption'),function(result){if(result){$timeout(function(){var ID=selectedfilterid;CcdetailfilterService.DeleteFilterSettings(ID).then(function(deletefilterbyFilterId){if(deletefilterbyFilterId.StatusCode==405){NotifyError($translate.instant('LanguageContents.Res_4299.Caption'));}else{NotifySuccess($translate.instant('LanguageContents.Res_4398.Caption'));$("#EntitiesTree").trigger("loadactivityfromfilterforCostCenter",[0,ApplyFilterobj]);$("#costcentredetailfilter").trigger('reloadccdetailfilter',[TypeID,'No filter applied',0]);}});},100);}});};$scope.$on("$destroy",function(){RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.detail.detailfilterCtrl']"));});$scope.tagAllOptionsEntityStatus={multiple:true,allowClear:true,data:$scope.ReassignMembersDataEntityStatus,formatResult:$scope.formatResultEntityStatus,formatSelection:$scope.formatSelection,dropdownCssClass:"bigdrop",escapeMarkup:function(m){return m;}};$scope.tagAllOptions={multiple:true,allowClear:true,data:$scope.ReassignMembersData,formatResult:$scope.formatResult,formatSelection:$scope.formatSelection,dropdownCssClass:"bigdrop",escapeMarkup:function(m){return m;}};$scope.ReassignMembersData=[];$scope.ReassignMembersDataEntityStatus=[];$scope.formatResult=function(item){var markup='<table class="user-result">';markup+='<tbody>';markup+='<tr>';markup+='<td class="user-image">';markup+='<span class="eicon" style="background-color: #'+item.ColorCode+'">'+item.ShortDescription+'</span>';markup+='</td>';markup+='<td class="user-info">';markup+='<div class="user-title">'+item.text+'</div>';markup+='</td>';markup+='</tr>';markup+='</tbody>';markup+='</table>';return markup;};$scope.formatResultEntityStatus=function(item){var markup='<table class="user-result">';markup+='<tbody>';markup+='<tr>';markup+='<td class="user-image">';markup+='<span class="eicon" style="background-color: #'+item.ColorCode+'">'+item.ShortDescription+'</span>';markup+='</td>';markup+='<td class="user-info">';markup+='<div class="user-title">'+item.text+'</div>';markup+='</td>';markup+='</tr>';markup+='</tbody>';markup+='</table>';return markup;};$scope.formatSelection=function(item){var markup='<table class="user-result">';markup+='<tbody>';markup+='<tr>';markup+='<td class="user-image">';markup+='<span class="eicon" style="background-color: #'+item.ColorCode+'">'+item.ShortDescription+'</span>';markup+='</td>';markup+='<td class="user-info">';markup+='<div class="user-title">'+item.text+'</div>';markup+='</td>';markup+='</tr>';markup+='</tbody>';markup+='</table>';return markup;};$scope.formatSelectionForEntityStauts=function(item){var markup='<table class="user-result">';markup+='<tbody>';markup+='<tr>';markup+='<td class="user-image">';markup+='<span class="eicon" style="background-color: #'+item.ColorCode+'">'+item.ShortDescription+'</span>';markup+='</td>';markup+='<td class="user-info">';markup+='<div class="user-title">'+item.text+'</div>';markup+='</td>';markup+='</tr>';markup+='</tbody>';markup+='</table>';return markup;};$scope.tagAllOptions={multiple:true,allowClear:true,data:$scope.ReassignMembersData,formatResult:$scope.formatResult,formatSelection:$scope.formatSelection,dropdownCssClass:"bigdrop",escapeMarkup:function(m){return m;}};$scope.tagAllOptionsEntityStatus={multiple:true,allowClear:true,data:$scope.ReassignMembersDataEntityStatus,formatResult:$scope.formatResultEntityStatus,formatSelection:$scope.formatSelection,dropdownCssClass:"bigdrop",escapeMarkup:function(m){return m;}};function IsNotEmptyTree(treeObj){var flag=false;for(var i=0,node;node=treeObj[i++];){if(node.ischecked==true){flag=true;return flag;}}
return flag;}
function ClearSavedTreeNode(attributeid){for(var i=0,branch;branch=$scope.treesrcdirec["Attr_"+attributeid][i++];){branch.ischecked=false;if(branch.Children.length>0){ClearRecursiveChildTreenode(branch.Children);}}}
function ClearRecursiveChildTreenode(children){for(var j=0,child;child=children[j++];){child.ischecked=false;if(child.Children.length>0){ClearRecursiveChildTreenode(child.Children);}}}
function GetTreeObjecttoSave(attributeid){$scope.treeNodeSelectedHolderValues=[];for(var i=0,branch;branch=$scope.treesrcdirec["Attr_"+attributeid][i++];){if(branch.ischecked==true){var remainRecord=[];remainRecord=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==branch.AttributeId&&e.id==branch.id;});if(remainRecord.length==0){$scope.treeNodeSelectedHolderValues.push(branch);}
if(branch.Children.length>0){FormRecursiveChildTreenode(branch.Children);}}}}
function GetAllTreeObject(attributeid){$scope.treeNodeSelectedHolderValues=[];for(var i=0,branch;branch=$scope.treesrcdirec["Attr_"+attributeid][i++];){var remainRecord=[];remainRecord=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==branch.AttributeId&&e.id==branch.id;});if(remainRecord.length==0){$scope.treeNodeSelectedHolderValues.push(branch);}
if(branch.Children.length>0){FormRecursiveChildTreenode(branch.Children);}}}
function FormRecursiveChildTreenode(children){for(var j=0,child;child=children[j++];){if(child.ischecked==true){var remainRecord=[];remainRecord=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==child.AttributeId&&e.id==child.id;});if(remainRecord.length==0){$scope.treeNodeSelectedHolderValues.push(child);if(child.Children.length>0){FormRecursiveChildTreenode(child.Children);}}}}}
var apple_selected,tree,treedata_avm,treedata_geography;$scope.my_tree_handler=function(branch,parentArr){var _ref;$scope.output="You selected: "+branch.Caption;if((_ref=branch.data)!=null?_ref.description:void 0){return $scope.output+='('+branch.data.description+')';}
if(branch.ischecked==true){var remainRecord=[];remainRecord=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==branch.AttributeId&&e.id==branch.id;});if(remainRecord.length==0){$scope.treeNodeSelectedHolderValues.push(branch);}
for(var i=0,parent;parent=parentArr[i++];){var remainRecord=[];remainRecord=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==parent.AttributeId&&e.id==parent.id;});if(remainRecord.length==0){$scope.treeNodeSelectedHolderValues.push(parent);}}}else{var remainRecord=[];remainRecord=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==branch.AttributeId&&e.id==branch.id;});if(remainRecord.length>0){$scope.treeNodeSelectedHolderValues.splice($scope.treeNodeSelectedHolderValues.indexOf(branch),1);if(branch.Children.length>0){RemoveRecursiveChildTreenode(branch.Children);}}}
if($scope.treesrcdirec["Attr_"+branch.AttributeId].length>0){if(IsNotEmptyTree($scope.treesrcdirec["Attr_"+branch.AttributeId])){$scope.treePreviewObj["Attr_"+branch.AttributeId]=true;}else $scope.treePreviewObj["Attr_"+branch.AttributeId]=false;}else{$scope.treePreviewObj["Attr_"+branch.AttributeId]=false;}
function RemoveRecursiveChildTreenode(children){for(var j=0,child;child=children[j++];){var remainRecord=[];remainRecord=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==child.AttributeId&&e.id==child.id;});if(remainRecord.length>0){$scope.treeNodeSelectedHolderValues.splice($.inArray(child,$scope.treeNodeSelectedHolderValues),1);if(child.Children.length>0){RemoveRecursiveChildTreenode(child.Children);}}}}}}
app.controller("mui.planningtool.costcentre.detail.detailfilterCtrl",['$scope','$location','$resource','$timeout','$cookies','$compile','$window','$translate','CcdetailfilterService',muiplanningtoolcostcentredetaildetailfilterCtrl]);})(angular,app);
///#source 1 1 /app/services/ccsection-service.js
app.service('CcsectionService',function($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetPath:GetPath,GetCustomTabUrlTabsByTypeID:GetCustomTabUrlTabsByTypeID,GetCustomEntityTabsByTypeID:GetCustomEntityTabsByTypeID,GetLockStatus:GetLockStatus});function GetPath(EntityID){var request=$http({method:"get",url:"api/Metadata/GetPath/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCustomTabUrlTabsByTypeID(tabID,entityID){var request=$http({method:"get",url:"api/common/GetCustomTabUrlTabsByTypeID/"+tabID+"/"+entityID,params:{action:"get",},});return(request.then(handleSuccess,handleError));}
function GetCustomEntityTabsByTypeID(TypeID,EntityTypeID,EntityID,CalID){if(arguments.length==1){var request=$http({method:"get",url:"api/common/GetCustomEntityTabsByTypeID/"+TypeID,params:{action:"get",}});}else{var request=$http({method:"get",url:"api/common/GetCustomEntityTabsByTypeID/"+TypeID+"/"+CalID+"/"+EntityTypeID+"/"+EntityID,params:{action:"get",},});}
return(request.then(handleSuccess,handleError));}
function GetLockStatus(EntityID){var request=$http({method:"get",url:"api/Planning/GetLockStatus/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}});
///#source 1 1 /app/services/SubEntityCreation-service.js
(function(ng,app){function SubentitycreationService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetCostcentreforEntityCreation:GetCostcentreforEntityCreation,GetCostcentreTreeforPlanCreation:GetCostcentreTreeforPlanCreation,GetGlobalMembers:GetGlobalMembers,GettingPredefineObjectivesForEntityMetadata:GettingPredefineObjectivesForEntityMetadata,CreateEntity:CreateEntity,GetCostCentreCurrencyRateById:GetCostCentreCurrencyRateById,GetCurrencyListFFsettings:GetCurrencyListFFsettings,GetEntityTypeRoleAccess:GetEntityTypeRoleAccess,GetUserById:GetUserById,GetAttributeToAttributeRelationsByIDForEntity:GetAttributeToAttributeRelationsByIDForEntity,GetEntityTypeAttributeRelationWithLevelsByID:GetEntityTypeAttributeRelationWithLevelsByID,GetValidationDationByEntitytype:GetValidationDationByEntitytype,GetPlantabsettings:GetPlantabsettings});function GetCostcentreforEntityCreation(EntityTypeID,FiscalYear,EntityID){var request=$http({method:"get",url:"api/Planning/GetCostcentreforEntityCreation/"+EntityTypeID+"/"+FiscalYear+"/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCostcentreTreeforPlanCreation(EntityTypeID,FiscalYear,EntityID){var request=$http({method:"get",url:"api/Planning/GetCostcentreTreeforPlanCreation/"+EntityTypeID+"/"+FiscalYear+"/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetGlobalMembers(EntityID){var request=$http({method:"get",url:"api/Planning/GetGlobalMembers/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettingPredefineObjectivesForEntityMetadata(formobj){var request=$http({method:"post",url:"api/Planning/GettingPredefineObjectivesForEntityMetadata/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function CreateEntity(formobj){var request=$http({method:"post",url:"api/Planning/CreateEntity/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetCostCentreCurrencyRateById(EntityId,CurrencyId,IsCostCentreCreation){var request=$http({method:"get",url:"api/Planning/GetCostCentreCurrencyRateById/"+EntityId+"/"+CurrencyId+"/"+IsCostCentreCreation,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCurrencyListFFsettings(){var request=$http({method:"get",url:"api/Planning/GetCurrencyListFFsettings/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeRoleAccess(EntityTypeID){var request=$http({method:"get",url:"api/access/GetEntityTypeRoleAccess/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetUserById(ID){var request=$http({method:"get",url:"api/user/GetUserById/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAttributeToAttributeRelationsByIDForEntity(ID){var request=$http({method:"get",url:"api/Metadata/GetAttributeToAttributeRelationsByIDForEntity/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeAttributeRelationWithLevelsByID(ID,ParentID){var request=$http({method:"get",url:"api/Metadata/GetEntityTypeAttributeRelationWithLevelsByID/"+ID+"/"+ParentID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetValidationDationByEntitytype(EntityTypeID){var request=$http({method:"get",url:"api/Metadata/GetValidationDationByEntitytype/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetPlantabsettings(){var request=$http({method:"get",url:"api/common/GetPlantabsettings/",params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("SubentitycreationService",['$http','$q',SubentitycreationService]);})(angular,app);
///#source 1 1 /app/services/ccdetailfilter-service.js
(function(ng,app){"use strict";function CcdetailfilterService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetOptionsFromXML:GetOptionsFromXML,GettingEntityTypeHierarchyForAdminTree:GettingEntityTypeHierarchyForAdminTree,GettingFilterEntityMember:GettingFilterEntityMember,GettingFilterAttribute:GettingFilterAttribute,InsertFilterSettings:InsertFilterSettings,GetFilterSettingValuesByFilertId:GetFilterSettingValuesByFilertId,DeleteFilterSettings:DeleteFilterSettings});function GetOptionsFromXML(elementNode,typeid){var request=$http({method:"get",url:"api/Metadata/GetOptionsFromXML/"+elementNode+"/"+typeid,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettingEntityTypeHierarchyForAdminTree(EntityTypeID,ModuleID){var request=$http({method:"get",url:"api/Metadata/GettingEntityTypeHierarchyForAdminTree/"+EntityTypeID+"/"+ModuleID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettingFilterEntityMember(formobj){var request=$http({method:"post",url:"api/Metadata/GettingFilterEntityMember/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GettingFilterAttribute(formobj){var request=$http({method:"post",url:"api/Metadata/GettingFilterAttribute/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function InsertFilterSettings(formobj){var request=$http({method:"post",url:"api/Planning/InsertFilterSettings/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetFilterSettingValuesByFilertId(FilterID){var request=$http({method:"get",url:"api/Planning/GetFilterSettingValuesByFilertId/"+FilterID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function DeleteFilterSettings(FilterId){var request=$http({method:"delete",url:"api/Planning/DeleteFilterSettings/"+FilterId,params:{action:"delete"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){response.data.StatusCode=response.status;return(response.data);}}
app.service("CcdetailfilterService",['$http','$q',CcdetailfilterService]);})(angular,app);
