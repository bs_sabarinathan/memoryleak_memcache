﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// PurchaseOrderDao object for table 'PM_PurchaseOrder'.
    /// </summary>

    public partial class InvoiceDetailDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _costcentreid;


        protected int _invoiceID;
        protected string _description;

        protected int _currencytype;

        protected decimal _amount;


        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public InvoiceDetailDao() { }

        public InvoiceDetailDao(int pCostCentreid, int pInvoiceID, int pCurrencyType, int PCurrencyType, decimal pAmount)
        {
            this._costcentreid = pCostCentreid;

            this._invoiceID = pInvoiceID;



            this._currencytype = pCurrencyType;
            this._amount = pAmount;


        }

        public InvoiceDetailDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int CostCenterID
        {
            get { return _costcentreid; }
            set { _bIsChanged |= (_costcentreid != value); _costcentreid = value; }

        }

        public virtual decimal Amount
        {
            get { return _amount; }
            set { _bIsChanged |= (_amount != value); _amount = value; }

        }







        public virtual int InvoiceID
        {
            get { return _invoiceID; }
            set { _bIsChanged |= (_invoiceID != value); _invoiceID = value; }

        }




        public virtual int CurrencyType
        {
            get { return _currencytype; }
            set { _bIsChanged |= (_currencytype != value); _currencytype = value; }

        }



        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string PurchaseOrderDetailDao = "PurchaseOrderDetailDao";
            public const string Id = "Id";
            public const string CostCenterID = "CostCentreID";
            public const string PoNumber = "PoNumber";
            public const string InvoiceID = "InvoiceID";

            public const string CurrencyType = "CurrencyType";

            public const string Amount = "Amount";

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string PurchaseOrderDetailDao = "PurchaseOrderDetailDao";
            public const string Id = "Id";
            public const string CostCenterID = "CostCentreID";
            public const string PoNumber = "PoNumber";
            public const string InvoiceID = "InvoiceID";

            public const string CurrencyType = "CurrencyType";

            public const string Amount = "Amount";

        }

        #endregion


    }

}
