﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Task.Model
{

    /// <summary>
    /// PurchaseOrderDao object for table 'PM_PurchaseOrder'.
    /// </summary>

    public partial class TaskListDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _Caption;
        protected string _description;
        protected int _Sortorder;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public TaskListDao() { }

        public TaskListDao(string pCaption,string pDescription, int pSortorder)
        {
            this._Caption = pCaption;
            this._description = pDescription;
            this._Sortorder = pSortorder;
        }

        public TaskListDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }



        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1000000)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1000000 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }

        public virtual string Caption
        {
            get { return _Caption; }
            set
            {
                if (value != null && value.Length > 1000000)
                    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 1000000 characters");
                _bIsChanged |= (_Caption != value);
                _Caption = value;
            }

        }


        public virtual int Sortorder
        {
            get { return _Sortorder; }
            set { _bIsChanged |= (_Sortorder != value); _Sortorder = value; }

        }


        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string TaskListDao = "TaskListDao";
            public const string ID = "ID";
            public const string Caption = "Caption";
            public const string Description = "Description";
            public const string Sortorder = "Sortorder";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string TaskListDao = "TaskListDao";
            public const string ID = "ID";
            public const string Caption = "Caption";
            public const string Description = "Description";
            public const string Sortorder = "Sortorder";
        }

        #endregion


    }

}
