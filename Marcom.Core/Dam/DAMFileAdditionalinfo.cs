﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.DAM.Interface;

namespace BrandSystems.Marcom.DAM
{
    public class DAMFileAdditionalinfo 
    {

        #region Public Properties

        public double Height
        {
            get;
            set;

        }
        public double Width
        {
            get;
            set;

        }
        public double Dpi
        {
            get;
            set;
        }
        public string TotalDuration
        {
            get;
            set;
        }
        //public double FrameWidth
        //{
        //    get;
        //    set;
        //}
        //public double FrameHeight
        //{
        //    get;
        //    set;
        //}

        public string FrameRate
        {
            get;
            set;
        }
        #endregion
    }
}
