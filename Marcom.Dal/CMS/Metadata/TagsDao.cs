﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.CMS.Model
{
    public partial class TagsDao : BaseDao, ICloneable
    {
        #region Member variables

        protected int _id;
        protected int _entityid;
        protected string _tag;


        #endregion

        #region Constructors

        public TagsDao()
        {

        }

        public TagsDao(int pid)
        {
            this._id = pid;
        }

        public TagsDao(int pid, int pentityid, bool pactive, string ptag)
        {
            this._id = pid;
            this._entityid = pentityid;
            this._tag = ptag;
        }

        #endregion

        #region Public properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public virtual int EntityID
        {
            get { return _entityid; }
            set { _entityid = value; }
        }

        public virtual string Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }



        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string RevisedEntityContentDao = "RevisedEntityContentDao";
            public const string ID = "ID";
            public const string EntityID = "EntityID";
            public const string Tag = "Tag";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string RevisedEntityContentDao = "RevisedEntityContentDao";
            public const string ID = "ID";
            public const string EntityID = "EntityID";
            public const string Tag = "Tag";
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

    }
}
