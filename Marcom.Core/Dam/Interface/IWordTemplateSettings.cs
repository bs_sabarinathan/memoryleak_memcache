﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Dam.Interface
{
    public interface IWordTemplateSettings
    {
        int ID { get; set; }
        string Name { get; set; }

        int CategoryID { get; set; }
        string Description { get; set; }
        string Worddocpath { get; set; }
        string PreviewImage { get; set; }
       
    }
}
