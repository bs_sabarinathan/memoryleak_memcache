﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.DAM.Model
    {
    /// <summary>
    /// FileProfileDao object for table 'DAM_File_Profile'.
    /// </summary>
    public partial class DamFileProfileDao : BaseDao , ICloneable
        {

        #region Member Variables

        protected int _id;
        protected string _name;
        protected double _height;
        protected double _width;
        protected string _extension;
        protected int _dpi;
        protected string _isassetaccess;

        #endregion

        #region Constructors
        public DamFileProfileDao() { }

        public DamFileProfileDao(int id , string name , double height , double width , string extension , int dpi , string isassetaccess)
            {
            this._name = name;
            this._id = id;
            this._height = height;
            this._width = width;
            this._extension = extension;
            this._dpi = dpi;
            this._isassetaccess = isassetaccess;

            }

        public DamFileProfileDao(int pId)
            {
            this._id = pId;
            }

        #endregion


        #region Public Properties


        public  virtual int ID
            {
            get { return _id; }
            set { _id = value; }
            }

        public  virtual string Name
            {
            get
                {
                return _name
                    ;
                }
            set { _name = value; }
            }

        public virtual double Height
            {
            get { return _height; }
            set { _height = value; }
            }


        public virtual double Width
            {
            get { return _width; }
            set { _width = value; }
            }

        public virtual string Extension
            {
            get { return _extension; }
            set { _extension = value; }
            }
        public virtual int DPI
            {
            get { return _dpi; }
            set { _dpi = value; }
            }
        public virtual string IsAssetAccess
            {
            get { return _isassetaccess; }
            set { _isassetaccess = value; }
            }


        #endregion

        #region ICloneable methods

        public virtual object Clone()
            {
            return this.MemberwiseClone();
            }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
            {
            public const string DamFileProfileDao = "DamFileProfileDao";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string Height = "Height";
            public const string Width  = "Width";
            public const string Extension = "Extension";
            public const string DPI = "DPI";
            public const string IsAssetAccess = "IsAssetAccess";
            }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
            {
            public const string DamFileProfileDao = "DamFileProfileDao";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string Height = "Height";
            public const string Width = "Width";
            public const string Extension = "Extension";
            public const string DPI = "DPI";
            public const string IsAssetAccess = "IsAssetAccess";
            }

        #endregion

        }
    }



       
    
