﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Cms.Interface
{
    public interface ITags
    {
        int ID
        {
            get;
            set;
        }

        int EntityID
        {
            get;
            set;
        }

        string Tag
        {
            get;
            set;
        }


    }
}
