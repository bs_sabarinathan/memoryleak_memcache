using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

	/// <summary>
	/// EntityTypeHierarchyDao object for table 'MM_EntityType_Hierarchy'.
	/// </summary>
	
	public partial class EntityTypeHierarchyDao : BaseDao, ICloneable
	{
		#region Member Variables

        protected int _id;
		protected int _parentactivitytypeid;
		protected int _childactivitytypeid;
		protected int _sortorder;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion

        //#region Extensibility Method Definitions

        //public override bool Equals(object obj)
        //{
        //    EntityTypeHierarchyDao toCompare = obj as EntityTypeHierarchyDao;
        //    if (toCompare == null)
        //    {
        //        return false;
        //    }

        //    if (!Object.Equals(this.ParentActivityTypeid, toCompare.ParentActivityTypeid))
        //        return false;
        //    if (!Object.Equals(this.ChildActivityTypeid, toCompare.ChildActivityTypeid))
        //        return false;

        //    return true;
        //}

        //public override int GetHashCode()
        //{
        //    int hashCode = 13;
        //    hashCode = (hashCode * 7) + ParentActivityTypeid.GetHashCode();
        //    hashCode = (hashCode * 7) + ChildActivityTypeid.GetHashCode();
        //    return hashCode;
        //}

        //#endregion
		
		#region Constructors
		public EntityTypeHierarchyDao() {}
			
        public EntityTypeHierarchyDao(int pId,int pParentActivityTypeid, int pChildActivityTypeid, int pSortOrder)
		{
            this._id = pId;
			this._parentactivitytypeid = pParentActivityTypeid; 
			this._childactivitytypeid = pChildActivityTypeid; 
			this._sortorder = pSortOrder; 
		}
		
		#endregion
		
		#region Public Properties
		
        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int ParentActivityTypeID
		{
			get { return _parentactivitytypeid; }
			set { _bIsChanged |= (_parentactivitytypeid != value); _parentactivitytypeid = value; }
			
		}

        public virtual int ChildActivityTypeID
		{
			get { return _childactivitytypeid; }
			set { _bIsChanged |= (_childactivitytypeid != value); _childactivitytypeid = value; }
			
		}
		
		public virtual int SortOrder
		{
			get { return _sortorder; }
			set { _bIsChanged |= (_sortorder != value); _sortorder = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string Id = "Id";	
			public const string EntityTypeHierarchyDao = "EntityTypeHierarchyDao";
            public const string ParentActivityTypeID = "ParentActivityTypeID";
            public const string ChildActivityTypeID = "ChildActivityTypeID";			
			public const string SortOrder = "SortOrder";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string Id = "Id";	
			public const string EntityTypeHierarchyDao = "EntityTypeHierarchyDao";
            public const string ParentActivityTypeID = "ParentActivityTypeID";
            public const string ChildActivityTypeID = "ChildActivityTypeID";			
			public const string SortOrder = "SortOrder";			
		}

		#endregion
		
		
	}
	
}
