﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

    /// <summary>
    /// AttributeDao object for table 'MM_WorkFlowStep_PredefinedTask'.
    /// </summary>

    public partial class WorkFlowStepPredefinedTasksDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _stepID;
        protected int _taskid;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public WorkFlowStepPredefinedTasksDao() { }

        public WorkFlowStepPredefinedTasksDao(int pId, int pStepID, int pTaskID, string pHelpText)
        {
            this._id = pId;
            this._stepID = pStepID;
            this._taskid = pTaskID;
        }

        public WorkFlowStepPredefinedTasksDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int StepID
        {
            get { return _stepID; }
            set { _bIsChanged |= (_stepID != value); _stepID = value; }

        }

         public virtual int TaskID
        {
            get { return _taskid; }
            set { _bIsChanged |= (_taskid != value); _taskid = value; }

        }


        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string WorkFlowStepPredefinedTasksDao = "WorkFlowStepPredefinedTasksDao";
            public const string Id = "Id";
            public const string StepID = "StepID";
            public const string TaskID = "TaskID";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string WorkFlowStepPredefinedTasksDao = "WorkFlowStepPredefinedTasksDao";
            public const string Id = "Id";
            public const string StepID = "StepID";
            public const string Name = "TaskID";
        }

        #endregion


    }

}
