﻿(function (ng, app) {
    "use strict"; function CreatecmsentityService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetPlantabsettings: GetPlantabsettings,
            GetCostcentreforEntityCreation: GetCostcentreforEntityCreation,
            CreateCmsPageEntity: CreateCmsPageEntity,
            GetCurrencyListFFsettings: GetCurrencyListFFsettings,
            GetCostCentreCurrencyRateById: GetCostCentreCurrencyRateById,
            GetUserById: GetUserById,
            GetEntityType: GetEntityType,
            GetAttributeToAttributeRelationsByIDForEntity: GetAttributeToAttributeRelationsByIDForEntity,
            GetEntityTypeAttributeRelationWithLevelsByID: GetEntityTypeAttributeRelationWithLevelsByID,
            GetValidationDationByEntitytype:GetValidationDationByEntitytype,
            GetEntityTypeRoleAccess:GetEntityTypeRoleAccess,
        });
        function GetPlantabsettings() {
            var request = $http({
                method: "get",
                url: "api/common/GetPlantabsettings/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function GetCostcentreforEntityCreation(EntityTypeID, FiscalYear, EntityID) { var request = $http({ method: "get", url: "api/Planning/GetCostcentreforEntityCreation/" + EntityTypeID + "/" + FiscalYear + "/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function CreateCmsPageEntity(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateCmsPageEntity/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetCurrencyListFFsettings() { var request = $http({ method: "get", url: "api/Planning/GetCurrencyListFFsettings/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetCostCentreCurrencyRateById(EntityId, CurrencyId, IsCostCentreCreation) { var request = $http({ method: "get", url: "api/Planning/GetCostCentreCurrencyRateById/" + EntityId + "/" + CurrencyId + "/" + IsCostCentreCreation, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetUserById(ID) { var request = $http({ method: "get", url: "api/user/GetUserById/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityType(ModuleID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityType/" + ModuleID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function GetAttributeToAttributeRelationsByIDForEntity(ID) { var request = $http({ method: "get", url: "api/Metadata/GetAttributeToAttributeRelationsByIDForEntity/" + ID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityTypeAttributeRelationWithLevelsByID(ID, ParentID) { var request = $http({ method: "get", url: "api/Metadata/GetEntityTypeAttributeRelationWithLevelsByID/" + ID + "/" + ParentID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetValidationDationByEntitytype(EntityTypeID) { var request = $http({ method: "get", url: "api/Metadata/GetValidationDationByEntitytype/" + EntityTypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityTypeRoleAccess(EntityTypeID) { var request = $http({ method: "get", url: "api/access/GetEntityTypeRoleAccess/" + EntityTypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("CreatecmsentityService", ['$http', '$q', CreatecmsentityService]);
})(angular, app);