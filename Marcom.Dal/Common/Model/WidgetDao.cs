﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Common.Model
{
    
    /// <summary>
    ///  Widget object for xml file.
    /// </summary>
    public partial class WidgetDao : BaseDao, ICloneable 
    {
        #region Member Variables
            protected string _id;
            protected int _templateid;
            protected string _caption;
            protected string _description;
            protected int _widgettypeid;
            protected bool _isdynamic;
            protected int _filterid;
            protected int _attributeid;
            protected int _dimensionid;
            protected string _matrixid;
            protected string _widgetquery;
            protected int _column; 
            protected int _row;
            protected int _sizex;
            protected int _sizey;
            protected int _visualtype;
            protected bool _bIsDeleted;
            protected bool _bIsChanged;
            protected int _NoOfItem;
            protected int _NoOfYear;
            protected int _NoOfMonth;
            protected string _ListOfEntityID;
            protected string _ListofSelectEntityID;

        #endregion

        #region Constructors

            public WidgetDao() { }

            public WidgetDao(string widgetid) 
            {
                this._id = widgetid;
            }

            public WidgetDao(string guid, string caption, string description, int widgettypeid, int filterid, int attributeid, bool isstatic, int dimensionid, string matrixid, string widgetQuery, int Column, int Row, int SizeX, int SizeY, int visualtypeid, int NoOfItem, string ListofEntityID, string ListofSelectEntityID, int NoOfYear, int NoOfMonth)
            {
                // TODO: Complete member initialization
                this._id = guid;
                this._caption = caption;
                this._description = description;
                this._widgettypeid = widgettypeid;
                this._filterid = filterid;
                this._attributeid = attributeid;
                this._isdynamic = isstatic;
                this._dimensionid = dimensionid;
                this._matrixid = matrixid;
                this._widgetquery = widgetQuery;
                this._column = Column;
                this._row = Row;
                this._sizex = SizeX;
                this._sizey = SizeY;
                this._visualtype = visualtypeid;
                this._NoOfItem = NoOfItem;
                this._ListOfEntityID = ListofEntityID;
                this._ListofSelectEntityID = ListofSelectEntityID;
                this._NoOfYear = NoOfYear;
                this._NoOfMonth = NoOfMonth;
            }

        #endregion

        #region Public Properties

            public virtual string ID
            {
                get { return _id; }
                set { _bIsChanged |= (_id != value); _id = value; }

            }

            public virtual int TemplateID
            {
                get { return _templateid; }
                set { _bIsChanged |= (_templateid != value); _templateid = value; }

            }

            public virtual string Caption
            {
                get { return _caption; }
                set
                {
                    if (value != null && value.Length > 500)
                        throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 500 characters");
                    _bIsChanged |= (_caption != value);
                    _caption = value;
                }

            }

            public virtual string Description
            {
                get { return _description; }
                set
                {
                    if (value != null && value.Length > 1073741823)
                        throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                    _bIsChanged |= (_description != value);
                    _description = value;
                }

            }

            public virtual int WidgetTypeID
            {
                get { return _widgettypeid; }
                set { _bIsChanged |= (_widgettypeid != value); _widgettypeid = value; }

            }

            public virtual bool IsDynamic
            {
                get { return _isdynamic; }
                set { _bIsChanged |= (_isdynamic != value); _isdynamic = value; }
            }

            public virtual int FilterID
            {
                get { return _filterid; }
                set { _bIsChanged |= (_filterid != value); _filterid = value; }

            }

            public virtual int AttributeID
            {
                get { return _attributeid; }
                set { _bIsChanged |= (_attributeid != value); _attributeid = value; }

            }

            public virtual int DimensionID
            {
                get { return _dimensionid; }
                set { _bIsChanged |= (_dimensionid != value); _dimensionid = value; }

            }

            public virtual string MatrixID
            {
                get { return _matrixid; }
                set
                {
                    _bIsChanged |= (_matrixid != value);
                    _matrixid = value;
                }
            }

            public virtual string WidgetQuery
            {
                get { return _widgetquery; }
                set
                {
                    _bIsChanged |= (_widgetquery != value);
                    _widgetquery = value;
                }
            }

            public virtual int Column
            {
                get { return _column; }
                set { _bIsChanged |= (_column != value); _column = value; }
            }

            public virtual int Row
            {
                get { return _row; }
                set { _bIsChanged |= (_row != value); _row = value; }
            }

            public virtual int SizeX
            {
                get { return _sizex; }
                set { _bIsChanged |= (_sizex != value); _sizex = value; }
            }

            public virtual int SizeY
            {
                get { return _sizey; }
                set { _bIsChanged |= (_sizey != value); _sizey = value; }
            }

            public virtual int VisualType
            {
                get { return _visualtype; }
                set
                {
                    _bIsChanged |= (_visualtype != value);
                    _visualtype = value;
                }
            }

            public virtual int NoOfItem
            {
                get { return _NoOfItem; }
                set { _bIsChanged |= (_NoOfItem != value); _NoOfItem = value; }

            }

            public virtual string ListOfEntityID
            {
                get { return _ListOfEntityID; }
                set
                {
                    _bIsChanged |= (_ListOfEntityID != value);
                    _ListOfEntityID = value;
                }
            }

            public virtual string ListofSelectEntityID
            {
                get { return _ListofSelectEntityID; }
                set
                {
                    _bIsChanged |= (_ListofSelectEntityID != value);
                    _ListofSelectEntityID = value;
                }
            }
            public virtual int NoOfYear
            {
                get { return _NoOfYear; }
                set { _bIsChanged |= (_NoOfYear != value); _NoOfYear = value; }

            }
            public virtual int NoOfMonth
            {
                get { return _NoOfMonth; }
                set { _bIsChanged |= (_NoOfMonth != value); _NoOfMonth = value; }

            }


        #endregion

        #region ICloneable methods

            public virtual object Clone()
            {
                return this.MemberwiseClone();
            }

            #endregion
				

    }
}
