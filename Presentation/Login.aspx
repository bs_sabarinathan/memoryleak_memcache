﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Presentation.Login" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="assets/css/mui.css" rel="stylesheet" />
    <link href="assets/css/mui-responsive.css" rel="stylesheet" />
    <link href="assets/css/login.css" rel="stylesheet" />
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <style>
        #form1 {
        }

        .loginForm {
            margin-top: 140px;
        }

        .loginLabel {
            display: block;
            font-size: 14px;
            margin-bottom: 5px;
            color: #777;
        }

        .btn {
            margin-top: 10px;
        }

        .leftside {
            height: 195px;
        }
    </style>
    <script type="text/javascript" src="assets/js/Jquery/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="assets/js/Jquery/Plugin/jquery.cookie.js"></script>
</head>
<body>
    <style>
        .bgImg {
            display: inline-block;
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0;
            left: 0;
            background-color: #ffffff;
            /*background: url(images/Scenary.jpg) no-repeat center center fixed;*/
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .errorTag {
            display: inline-block;
            width: 100%;
            line-height: 44px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            position: absolute;
            top: 50%;
            margin-top: -22px;
            padding: 0;
            text-align: center;
            font-size: 24px;
            color: red;
        }
    </style>
    <script type="text/javascript">

          $(document).ready(function () {

              var LogoSettings = {
                  "LogoSettings": "LogoSettings",
                  "typeid": "0"
              };
              var jsonString1 = JSON.stringify(LogoSettings);

              jQuery.support.cors = true;


              //---------------Loading the background Image for title and logo

              $.ajax({
                  url: 'api/User/GetTitleLogoBGImage/',
                  type: 'GET',
                  dataType: 'json',
                  success: function (result) {
                      if (result != false) {
                          var cssId = 'ui-login-theme';
                          if (!document.getElementById(cssId)) {
                              var head = document.getElementsByTagName('head')[0];
                              var link = document.createElement('link');
                              link.id = cssId;
                              link.rel = 'stylesheet';
                              link.href = result.Response[0];
                              head.appendChild(link);
                          }
                      }
                  },
                  error: function (data) {
                  }
              });

          });


    </script>

    <form id="form1" runat="server" class="loginThemed bgImg">
        <%-- <div runat="server" class="loginThemed bgImg">--%>
        <asp:Label ID="errorLabel" class="errorTag" runat="server">Invalid Credential</asp:Label>
        <%--</div>--%>
    </form>



</body>
</html>

