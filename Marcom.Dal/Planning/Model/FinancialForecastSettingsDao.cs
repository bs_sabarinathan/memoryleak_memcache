﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// FinancialDao object for table 'PM_Financial'.
    /// </summary>

    public partial class FinancialForecastSettingsDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected bool _isFinancialforecast;
        protected int _forecastDivision;
        protected int _forecastBasis;
        protected int _forecastLevel;
        protected int _forecastdeadlines;

        #endregion



        #region Constructors
        public FinancialForecastSettingsDao() { }

        public FinancialForecastSettingsDao(int pid, bool pIsFinancialforecast, int pForecastDivision, int pForecastBasis, int pForecastLevel, int pForecastdeadlines)
        {
            this._id = pid;
            this._isFinancialforecast = pIsFinancialforecast;
            this._forecastDivision = pForecastDivision;
            this._forecastBasis = pForecastBasis;
            this._forecastLevel = pForecastLevel;
            this._forecastdeadlines = pForecastdeadlines;

        }


        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual bool IsFinancialForecast
        {
            get { return _isFinancialforecast; }
            set { _isFinancialforecast = value; }

        }

        public virtual int ForecastDivision
        {
            get { return _forecastDivision; }
            set { _forecastDivision = value; }

        }

        public virtual int ForecastBasis
        {
            get { return _forecastBasis; }
            set { _forecastBasis = value; }

        }

        public virtual int ForecastLevel
        {
            get { return _forecastLevel; }
            set { _forecastLevel = value; }

        }

        public virtual int ForecastDeadlines
        {
            get { return _forecastdeadlines; }
            set { _forecastdeadlines = value; }

        }


        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {

            public const string ID = "ID";
            public const string IsFinancialForecast = "IsFinancialForecast";
            public const string ForecastDivision = "ForecastDivision";
            public const string ForecastBasis = "ForecastBasis";
            public const string ForecastLevel = "ForecastLevel";
            public const string ForecastDeadlines = "ForecastDeadlines";

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string ID = "ID";
            public const string IsFinancialForecast = "IsFinancialForecast";
            public const string ForecastDivision = "ForecastDivision";
            public const string ForecastBasis = "ForecastBasis";
            public const string ForecastLevel = "ForecastLevel";
            public const string ForecastDeadlines = "ForecastDeadlines";


        }

        #endregion


    }

}