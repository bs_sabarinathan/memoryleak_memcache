﻿using BrandSystems.Marcom.Core.Access.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Access
{

         internal class SuperAdminDetails : ISuperAdminDetails
    {  

        #region Member Variables        
        protected int _id;
        protected int _MenuID;
        protected int _IsTopnavigation;
        protected string _Name;
        protected bool _accesspermission;
        protected int _Featureid;
        #endregion


         #region Constructors
        public SuperAdminDetails() { }

        public SuperAdminDetails(int pId, int pMenuID, int pIsTopNavigation, string pName, bool pAccessPermission,int p_Featureid)
        {
            this._id = pId;
            this._MenuID = pMenuID;
            this._IsTopnavigation = pIsTopNavigation;
            this._Name = pName;
            this._accesspermission = pAccessPermission;
            this._Featureid = p_Featureid;
        }

        #endregion

        #region Public Properties


        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int MenuID
        {
            get { return _MenuID; }
            set { _MenuID = value; }

        }
        public int IsTopnavigation
        {
            get { return _IsTopnavigation; }
            set { _IsTopnavigation = value; }

        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }

        }
        public bool AccessPermission
        {
            get { return _accesspermission; }
            set { _accesspermission = value; }

        }
        public int FeatureID
        {
            get { return _Featureid; }
            set { _Featureid = value; }

        }


        #endregion
        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            TaskMember castObj = null;
            try
            {
                castObj = (TaskMember)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion


    }
}
