﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Task.Interface;

namespace BrandSystems.Marcom.Core.Task
{
    /// <summary>
    /// TaskFile object for table 'TM_File'.
    /// </summary>

    internal class TaskFile : ITaskFile
    {
        #region Member Variables

        protected int _id;
        protected string _name;
        protected int _versionno;
        protected string _mimetype;
        protected string _extension;
        protected long _size;
        protected int _ownerid;
        protected DateTimeOffset _createdon;
        protected string _checksum;
        protected int _moduleid;
        protected int _entityid;
        protected Guid _fileguid;
        protected string _OwnerName;
        protected string _description;

        #endregion

        #region Constructors
        public TaskFile() { }

        public TaskFile(string pName, int pVersionNo, string pMimeType, string pExtension, long pSize, int pOwnerid, DateTimeOffset pCreatedOn, string pChecksum, int pModuleid, int pEntityid, Guid pFileguid, string pOwnerName, string pDescription)
        {
            this._name = pName;
            this._versionno = pVersionNo;
            this._mimetype = pMimeType;
            this._extension = pExtension;
            this._size = pSize;
            this._ownerid = pOwnerid;
            this._createdon = pCreatedOn;
            this._checksum = pChecksum;
            this._moduleid = pModuleid;
            this._entityid = pEntityid;
            this._fileguid = pFileguid;
            this._OwnerName = pOwnerName;
            this._description = pDescription;

        }

        public TaskFile(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
                _name = value;
            }

        }

        public int VersionNo
        {
            get { return _versionno; }
            set { _versionno = value; }

        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }

        }


        public string MimeType
        {
            get { return _mimetype; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("MimeType", "MimeType value, cannot contain more than 250 characters");
                _mimetype = value;
            }

        }

        public string Extension
        {
            get { return _extension; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Extension", "Extension value, cannot contain more than 50 characters");
                _extension = value;
            }

        }


        public long Size
        {
            get { return _size; }
            set { _size = value; }

        }

        public int Ownerid
        {
            get { return _ownerid; }
            set { _ownerid = value; }

        }

        public DateTimeOffset CreatedOn
        {
            get { return _createdon; }
            set { _createdon = value; }

        }

        public string Checksum
        {
            get { return _checksum; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Checksum", "Checksum value, cannot contain more than 50 characters");
                _checksum = value;
            }

        }

        public int Moduleid
        {
            get { return _moduleid; }
            set { _moduleid = value; }

        }

        public int Entityid
        {
            get { return _entityid; }
            set { _entityid = value; }

        }


        public virtual Guid Fileguid
        {
            get { return _fileguid; }
            set { _fileguid = value; }

        }

        public virtual string OwnerName
        {
            get { return _OwnerName; }
            set { _OwnerName = value; }

        }
        public string StrCreatedDate { get; set; }
        public string strFileID { get; set; }
        public string linkURL { get; set; }
        public int LinkType { get; set; }
        public int AssetTypeid { get; set; }
        public int ProcessType { get; set; }
        public int Category { get; set; }
        public int Status { get; set; }
        public int Assetid { get; set; }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            TaskFile castObj = null;
            try
            {
                castObj = (TaskFile)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
