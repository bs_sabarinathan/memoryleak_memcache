﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.IO;
using BrandSystems.Marcom.Core.com.proofhq.www;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Dal.Metadata.Model;
using BrandSystems.Marcom.Dal.Base;
using System.Collections;
using System.Web;
using NHibernate.Mapping;
using System.Xml.Linq;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BrandSystems.Marcom.Dal.Planning.Model;
using BrandSystems.Marcom.Dal.User.Model;
using BrandSystems.Marcom.Core.Planning;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Dal.Access.Model;
using BrandSystems.Marcom.Core.Core.Managers.Proxy;
using BrandSystems.Marcom.Core.Dam.Interface;
using BrandSystems.Marcom.Core.Dam;
using BrandSystems.Marcom.Core.DAM.Interface;
using BrandSystems.Marcom.Dal.DAM.Model;
using BrandSystems.Marcom.Core.DAM;
using BrandSystems.Marcom.DAM;
using System.IO.Compression;
using ICSharpCode.SharpZipLib.Zip;
using BrandSystems.Marcom.Core.Common;
using System.Net.Mail;
using BrandSystems.Marcom.Dal.Task.Model;
using BrandSystems.Marcom.Dal.Common.Model;
using Microsoft.Win32;
using System.Drawing.Drawing2D;
using SD = System.Drawing;
using BrandSystems.Marcom.Core.Utility;
using System.Net;
using BrandSystems.Marcom.Dal.ExpireHandler.Model;
using BrandSystems.Marcom.Core.Managers.Proxy;
using BrandSystems.Marcom.Core.ExpireHandler.Interface;
using BrandSystems.Marcom.Core.ExpireHandler;
using System.Globalization;
using System.Xml;
using System.Web.Script.Serialization;


namespace BrandSystems.Marcom.Core.Managers
{
    internal partial class ExpireHandlerManager : IManager
    {
        void IManager.Initialize(IMarcomManager marcomManager)
        {
            //throw new System.NotImplementedException();
        }

        void IManager.CommitCaches()
        {
            // throw new System.NotImplementedException();
        }

        void IManager.RollbackCaches()
        {
            //throw new System.NotImplementedException();
        }

        private static ExpireHandlerManager instance = new ExpireHandlerManager();
        internal static ExpireHandlerManager Instance
        {
            get { return instance; }
        }
        /// <summary>
        /// The working metadata
        /// </summary>
        string currentworkingMetadata = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], ConfigurationManager.AppSettings["currentworkingMetadata"]);
        /// <summary>
        /// The history metadata
        /// </summary>
        string currentSyncDBXML = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], ConfigurationManager.AppSettings["currentSyncDBXML"]);

        /// <summary>
        /// The version of metadata
        /// </summary>
        string versionMetadata = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], ConfigurationManager.AppSettings["versionMetadata"]);

        public int CreateExpireAction(ExpireHandlerManagerProxy proxy, int ActionTypeID, int SourceID, int SourceEnityID, int SourceFrom, string Actionexutedays, string DateActionexpiredate, int ActionsourceId, int attributeid, JObject srcValue)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    string dateformate;
                    dateformate = proxy.MarcomManager.GlobalAdditionalSettings[0].SettingValue.ToString().Replace('m', 'M');
                    EHActionInitiatorDao Expiresourcedao = new EHActionInitiatorDao();
                    if (ActionsourceId > 0)
                        Expiresourcedao.Id = ActionsourceId;


                    Expiresourcedao.ActionownerId = proxy.MarcomManager.User.Id;
                    Expiresourcedao.ActionPlace = SourceFrom;
                    Expiresourcedao.ActionSrcID = SourceID;
                    string strJsonXML = JsonConvert.SerializeObject(srcValue); //json into string
                    Expiresourcedao.ActionSrcValue = strJsonXML;
                    Expiresourcedao.ActionType = ActionTypeID;
                    Expiresourcedao.EntityID = SourceEnityID;
                    Expiresourcedao.ExecutionDate = getorginalexpirtime(DateActionexpiredate, Actionexutedays, dateformate);
                    Expiresourcedao.TimeDuration = Convert.ToInt16(Actionexutedays);
                    Expiresourcedao.Status = 0;
                    Expiresourcedao.AttributeID = attributeid;
                    Expiresourcedao.UpdatedOn = DateTime.Now;
                    tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Save<EHActionInitiatorDao>(Expiresourcedao);
                        tx.Commit();
                    return Expiresourcedao.Id;

                    }
                            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public bool UpdateExpireActionDate(ExpireHandlerManagerProxy proxy, int SourceID, string DateActionexpiredate, int SourcetypeID, int Actiontype, string Actionexutedays, int SourceAttributeID, int SourceFrom, int ProductionEntityID)
        {



            int OwnerID = 0;

            try
            {

                OwnerID = Convert.ToInt32(proxy.MarcomManager.User.Id);



                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("-----------------------------------------------------------------------------------------------", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Started UpdateExpireActionDate", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    IList<EHActionInitiatorDao> ExpireHandlerAction = new List<EHActionInitiatorDao>();
                    EHActionInitiatorDao Expiresourcedao = new EHActionInitiatorDao();

                    var actionrows = (from tt in tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Query<EHActionInitiatorDao>() where tt.ActionSrcID == SourceID && tt.Status == 0 && tt.AttributeID == SourceAttributeID select tt).Select(a => a).ToList();
                    if (actionrows.Count > 0)
                    {
                        string dateformate;
                        dateformate = proxy.MarcomManager.GlobalAdditionalSettings[0].SettingValue.ToString().Replace('m', 'M');
                        foreach (var val in actionrows)
                        {
                            Expiresourcedao = new EHActionInitiatorDao();
                            int Actionexuteday = Convert.ToInt16(Actionexutedays.ToString());
                            string exActionexutedays = (Actionexuteday != -1) ? Actionexutedays.ToString() : val.TimeDuration.ToString();
                            Expiresourcedao.Id = val.Id;
                            Expiresourcedao.TimeDuration = Convert.ToInt16(exActionexutedays);
                            Expiresourcedao.ExecutionDate = getorginalexpirtime(DateActionexpiredate, exActionexutedays, dateformate);
                            Expiresourcedao.ActionownerId = proxy.MarcomManager.User.Id;
                            Expiresourcedao.ActionPlace = val.ActionPlace;
                            Expiresourcedao.ActionSrcID = val.ActionSrcID;
                            Expiresourcedao.ActionSrcValue = val.ActionSrcValue;
                            Expiresourcedao.ActionType = val.ActionType;
                            Expiresourcedao.EntityID = val.EntityID;
                            Expiresourcedao.Status = 0;
                            Expiresourcedao.UpdatedOn = DateTime.Now;
                            Expiresourcedao.AttributeID = val.AttributeID;
                            ExpireHandlerAction.Add(Expiresourcedao);


                        }
                        tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Save<EHActionInitiatorDao>(ExpireHandlerAction);
                        //tx.Commit();
                        BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("UpdateExpireActionDate is saved in EH_ActionSource with sourceid : " + SourceID, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                    }

                    if (SourceFrom == 1 && DateActionexpiredate != null && DateActionexpiredate != "-")
                    {
                        var assetTypeid = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.DAM.Model.AssetsDao>(SourceID);
                        var attr = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(SourceAttributeID);
                        var attrdetails = (from item in tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].Query<AttributeDao>() where item.Id == SourceAttributeID select item).FirstOrDefault();
                        BrandSystems.Marcom.Core.Utility.FeedNotificationServer fs = new Utility.FeedNotificationServer(proxy.MarcomManager.User.TenantID);
                        NotificationFeedObjects obj = new NotificationFeedObjects();
                        List<object> obj4 = new List<object>();
                        obj4.Add(DateActionexpiredate);
                        string str = "select * from MM_AttributeRecord_" + assetTypeid.AssetTypeid + " where ID= ? ";
                        IList item1 = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].ExecuteQuerywithMinParam(str, Convert.ToInt32(SourceID));
                        obj.obj3 = item1;
                        string dateformate;
                        dateformate = proxy.MarcomManager.GlobalAdditionalSettings[0].SettingValue.ToString().Replace('m', 'M');
                        //DateTime Actionexpiredate = new DateTime();
                        //Actionexpiredate = Convert.ToDateTime(DateActionexpiredate.ToString());
                        // string date = "2000-02-02";
                        //DateTime time = DateTime.Parse(DateActionexpiredate.ToString());
                        //string dateString = "@" +DateActionexpiredate.ToString();
                        //DateTime date3 = DateTime.ParseExact(dateString, @"yyyy/M/d",
                        //    System.Globalization.CultureInfo.InvariantCulture);
                        DateTime MyDateTime1 = getorginalexpirtime(DateActionexpiredate, "-1", dateformate);
                        obj.action = "Asset metadata update";
                        obj.Actorid = proxy.MarcomManager.User.Id;
                        obj.AttributeId = Convert.ToInt32(SourceAttributeID);
                        obj.EntityId = assetTypeid.EntityID;
                        obj.AttributeDetails = new List<AttributeDao>();
                        obj.AttributeDetails.Add(attrdetails);
                        obj.Attributetypeid = 16;
                        obj.AssociatedEntityId = assetTypeid.ID;
                        obj.obj2 = obj4;
                        //obj.ToValue = DateActionexpiredate;
                        obj.ToValue = MyDateTime1.ToString("yyyy-MM-dd");
                        if (assetTypeid.ActiveFileID > 0)
                        {
                            var FileObj = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].Query<DAMFileDao>().ToList().Where(item => item.ID == assetTypeid.ActiveFileID).Select(item => item.VersionNo).Max();
                            obj.Version = FileObj;
                        }
                        else
                            obj.Version = 1;
                        fs.AsynchronousNotify(obj);
                    }
                    else if (SourceFrom == 2 && DateActionexpiredate != null && DateActionexpiredate != "-")
                    {
                      
                        var attr = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(SourceAttributeID);
                        var attrdetails = (from item in tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].Query<AttributeDao>() where item.Id == SourceAttributeID select item).FirstOrDefault();
                        BrandSystems.Marcom.Core.Utility.FeedNotificationServer fs = new Utility.FeedNotificationServer(proxy.MarcomManager.User.TenantID);
                        NotificationFeedObjects obj = new NotificationFeedObjects();
                        List<object> obj4 = new List<object>();
                        obj4.Add(DateActionexpiredate);
                        string str = "select * from MM_AttributeRecord_" + SourcetypeID + " where ID= ? ";
                        IList item1 = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].ExecuteQuerywithMinParam(str, Convert.ToInt32(SourceID));
                        obj.obj3 = item1;
                        string dateformate;
                        dateformate = proxy.MarcomManager.GlobalAdditionalSettings[0].SettingValue.ToString().Replace('m', 'M');
                      
                        DateTime MyDateTime1 = getorginalexpirtime(DateActionexpiredate, "-1", dateformate);
                        obj.action = "metadata update";
                        obj.Actorid = proxy.MarcomManager.User.Id;
                        obj.AttributeId = Convert.ToInt32(SourceAttributeID);
                        obj.EntityId = ProductionEntityID;
                        obj.AttributeDetails = new List<AttributeDao>();
                        obj.AttributeDetails.Add(attrdetails);
                        obj.Attributetypeid = 16;
                        obj.AssociatedEntityId = 0;
                        obj.obj2 = obj4;
                        //obj.ToValue = DateActionexpiredate;
                        obj.ToValue = MyDateTime1.ToString("yyyy-MM-dd");
                        obj.Version =1;    
                        fs.AsynchronousNotify(obj);
                    }
                    else if (SourceFrom == 3 && DateActionexpiredate != null && DateActionexpiredate != "-")
                    {
                        var Entity = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Planning.Model.BaseEntityDao>(SourceID);
                        if (Entity != null)
                        { 
                            SourcetypeID = Entity.Typeid;
                            var attr = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(SourceAttributeID);
                            var attrdetails = (from item in tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].Query<AttributeDao>() where item.Id == SourceAttributeID select item).FirstOrDefault();
                            BrandSystems.Marcom.Core.Utility.FeedNotificationServer fs = new Utility.FeedNotificationServer(proxy.MarcomManager.User.TenantID);
                            NotificationFeedObjects obj = new NotificationFeedObjects();
                            List<object> obj4 = new List<object>();
                            obj4.Add(DateActionexpiredate);
                            string str = "select * from MM_AttributeRecord_" + SourcetypeID + " where ID= ? ";
                            IList item1 = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].ExecuteQuerywithMinParam(str, Convert.ToInt32(SourceID));
                            obj.obj3 = item1;
                            string dateformate;
                            dateformate = proxy.MarcomManager.GlobalAdditionalSettings[0].SettingValue.ToString().Replace('m', 'M');

                            DateTime MyDateTime1 = getorginalexpirtime(DateActionexpiredate, "-1", dateformate);
                            obj.action = "metadata update";
                            obj.Actorid = proxy.MarcomManager.User.Id;
                            obj.AttributeId = Convert.ToInt32(SourceAttributeID);
                            obj.EntityId = SourceID;
                            obj.AttributeDetails = new List<AttributeDao>();
                            obj.AttributeDetails.Add(attrdetails);
                            obj.Attributetypeid = 16;
                            obj.AssociatedEntityId = 0;
                            obj.obj2 = obj4;
                            //obj.ToValue = DateActionexpiredate;
                            obj.ToValue = MyDateTime1.ToString("yyyy-MM-dd");
                            obj.Version = 1;
                            fs.AsynchronousNotify(obj);

                        }
                    }
                    if (SourcetypeID > 0)
                    {
                        string dateformate;
                        dateformate = proxy.MarcomManager.GlobalAdditionalSettings[0].SettingValue.ToString().Replace('m', 'M');
                        DateTime MyDateTime1 = getorginalexpirtime(DateActionexpiredate, "-1", dateformate);

                        string updateCostcentreQuery = "UPDATE MM_AttributeRecord_" + SourcetypeID + " SET Attr_" + SourceAttributeID + " ='" + MyDateTime1.ToString() + "' WHERE ID=" + SourceID + "";
                        tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery(updateCostcentreQuery);
                    }
                    tx.Commit();

                    return true;



                    //BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("CreateExpireAction is saved in EH_ActionSource with ActionSourceID : " + ActionSourceID, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                }
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogError("Failed to EH_ActionSource", ex);
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("-----------------------------------------------------------------------------------------------", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                return false;
            }

            return true;
        }

        public DateTime getorginalexpirtime(string Expiredate, string Expireoptions,string  formate="")
        {
            int selectedoption;
            //string[] formats = {"d-M-yyyy","M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt", 
            //                               "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss", 
            //                               "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt", 
            //                                "M/d/yyyy h:mm", "M/d/yyyy h:mm", 
            //                                 "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm",
            //                                 "M/d/yyyy", "d/M/yyyy", "M-d-yyyy",
            //                                 "d-MMM-yy", "d-MMMM-yyyy",
            //                                "dd/MM/yyyy","dd-MM-yyyy","yyyy-MM-dd",
            //                                 };
             DateTime result;
                     DateTime MyDateTime;

            DateTime.TryParseExact(Expiredate, formate, System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out MyDateTime);
                    selectedoption = Convert.ToInt32(Expireoptions);
            switch (selectedoption)
            {
                case 1:
                    result = MyDateTime.AddDays(+1);                    
                    break;
                case 2:
                    result = MyDateTime.AddDays(+2);                    
                    break;
                case 3:
                    result = MyDateTime.AddDays(+3);                    
                    break;
                case 4:
                    result = MyDateTime.AddDays(+4);                    
                    break;
                case 5:
                    result = MyDateTime.AddDays(+5);                    
                    break;
                case 6:
                    result = MyDateTime.AddDays(+6);                    
                    break;
                case 7:
                    result = MyDateTime.AddDays(+7);                    
                    break;
                case 8:
                    result = MyDateTime.AddDays(+14);                    
                    break;
                case 9:
                    result = MyDateTime.AddDays(+21);                    
                    break;
                case 10:
                    result = MyDateTime.AddMonths(+1);                    
                    break;
                case 11:
                    result = MyDateTime.AddMonths(+2);                    
                    break;
                case 12:
                    result = MyDateTime.AddMonths(+3);                    
                    break;
                case 13:
                    result = MyDateTime.AddMonths(+4);                    
                    break;
                case 14:
                    result = MyDateTime.AddMonths(+5);                    
                    break;
                case 15:
                    result = MyDateTime.AddMonths(+6);                    
                    break;
                case 16:
                    result = MyDateTime.AddMonths(+7);                    
                    break;
                case 17:
                    result = MyDateTime.AddMonths(+8);                    
                    break;
                case 18:
                    result = MyDateTime.AddMonths(+9);                    
                    break;
                case 19:
                    result = MyDateTime.AddMonths(+10);                    
                    break;
                case 20:
                    result = MyDateTime.AddMonths(+11);                    
                    break;
                case 21:
                    result = MyDateTime.AddMonths(+12);                    
                    break;
                case 22:
                    result = MyDateTime.AddDays(-1);                   
                    break;
                case 23:
                    result = MyDateTime.AddDays(-2);                    
                    break;
                case 24:
                    result = MyDateTime.AddDays(-3);                    
                    break;
                case 25:
                    result = MyDateTime.AddDays(-4);                    
                    break;
                case 26:
                    result = MyDateTime.AddDays(-5);                    
                    break;
                case 27:
                    result = MyDateTime.AddDays(-6);                    
                    break;
                case 28:
                    result = MyDateTime.AddDays(-7);                    
                    break;
                case 29:
                    result = MyDateTime.AddDays(-14);
                    
                    break;
                case 30:
                    result = MyDateTime.AddDays(-21);
                    
                    break;
                case 31:
                    result = MyDateTime.AddMonths(-1);
                    
                    break;
                case 32:
                    result = MyDateTime.AddMonths(-2);
                    
                    break;
                case 33:
                    result = MyDateTime.AddMonths(-3);                    
                    break;
                case 34:
                    result = MyDateTime.AddMonths(-4);
                    
                    break;
                case 35:
                    result = MyDateTime.AddMonths(-5);                    
                    break;
                case 36:
                    result = MyDateTime.AddMonths(-6);                    
                    break;
                case 37:
                    result = MyDateTime.AddMonths(-7);                    
                    break;
                case 38:
                    result = MyDateTime.AddMonths(-8);                    
                    break;
                case 39:
                    result = MyDateTime.AddMonths(-9);                    
                    break;
                case 40:
                    result = MyDateTime.AddMonths(-10);                    
                    break;
                case 41:
                    result = MyDateTime.AddMonths(-11);                    
                    break;
                case 42:
                    result = MyDateTime.AddMonths(-12);                    
                    break;
                default:
                    result = MyDateTime;
                    break;
            }

            return result;
        }
        public IList<EHActionInitiatorDao> GetExpireActions(ExpireHandlerManagerProxy proxy, int SourceID, int Attributeid, int ActionID)
        {
            int version = MarcomManagerFactory.ActiveMetadataVersionNumber[proxy.MarcomManager.User.TenantID];
            IList<EHActionInitiatorDao> iinitoatorDao = new List<EHActionInitiatorDao>();
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    
                    //JObject o = JObject.Parse(@"{ 'CPU': 'Intel','Drives': ['DVD read/writer','500 gigabyte hard drive']}");

                    //string cpu = (string)o["CPU"];
                    if(ActionID>0)
                        iinitoatorDao = tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Query<EHActionInitiatorDao>().Where(a => a.ActionSrcID == SourceID && a.AttributeID == Attributeid && a.Status == 0 && a.Id == ActionID).ToList();
                    else
                        iinitoatorDao = tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Query<EHActionInitiatorDao>().Where(a => a.ActionSrcID == SourceID && a.AttributeID == Attributeid && a.Status == 0 ).ToList();

                    //JObject oo = JObject.Parse(iinitoatorDao.FirstOrDefault().ActionSrcValue);
                                    
                    return iinitoatorDao;
                                    }
                                }
            catch (Exception ex)
            {
                return null;
            }

        }


        public bool DeleteExpireAction(ExpireHandlerManagerProxy proxy, int ActionID)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {

                    IList<EHActionInitiatorDao> iinitoatorDao = new List<EHActionInitiatorDao>();
                    iinitoatorDao = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].Query<EHActionInitiatorDao>().Where(a => a.Id == ActionID).Select(a => a).ToList();

                    if (iinitoatorDao.Count > 0)
                        {
                       

                        tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Delete<EHActionInitiatorDao>(iinitoatorDao);

                    }
                    tx.Commit();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }





        public IList<IAttribute> GetEntityAttributes(ExpireHandlerManagerProxy proxy, int entityID)
        {
            try
            {
                IList<IAttribute> listAttributes = new List<IAttribute>();
                //string xmlPath = string.Empty;

                int versionNumber = MarcomManagerFactory.ActiveMetadataVersionNumber[proxy.MarcomManager.User.TenantID];
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    //int entityTypeId = (from item in tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].Query<EntityDao>()
                    //                    where item.Id == entityID
                    //                    select item.Typeid).FirstOrDefault();
                    int entityTypeId = (from item in tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Query<EntityDao>() where item.Id == entityID select item.Typeid).FirstOrDefault();
                    //xmlPath = tx.PersistenceManager.MetadataRepository[proxy.MarcomManager.User.TenantID].GetXmlPath(versionNumber, proxy.MarcomManager.User.TenantID);
                    XDocument xmlPath = MarcomCache<XDocument>.GetActiveVersionXmlPath(proxy.MarcomManager.User.TenantID);
                    XDocument xDoc = xmlPath;
                    IList<EntityTypeAttributeRelationDao> entityTypeRealtionDao = new List<EntityTypeAttributeRelationDao>();
                    IList<AttributeDao> attributesDao = new List<AttributeDao>();
                    if (versionNumber != 0)
                    {
                        var entityTypeXmlDao = tx.PersistenceManager.MetadataRepository[proxy.MarcomManager.User.TenantID].GetObject<EntityTypeAttributeRelationDao>(xmlPath).Join
                            (tx.PersistenceManager.MetadataRepository[proxy.MarcomManager.User.TenantID].GetObject<AttributeDao>(xmlPath),
                            entr => entr.AttributeID, at => at.Id, (entr, at) => new { entr, at }).Where(a => a.entr.EntityTypeID == entityTypeId).Select(a => a.at);
                        var treeXmlResult = entityTypeXmlDao.Where(a => a.AttributeTypeID == Convert.ToInt32(AttributesList.DropDownTree));

                        foreach (var obe in treeXmlResult)
                        {
                            var treeLevelXmlResult = tx.PersistenceManager.MetadataRepository[proxy.MarcomManager.User.TenantID].GetObject<TreeLevelDao>(xmlPath).Where(a => a.AttributeID == obe.Id);
                            foreach (var levelObj in treeLevelXmlResult)
                            {
                                IAttribute fullfillattributeObj = new BrandSystems.Marcom.Core.Metadata.Attribute();
                                fullfillattributeObj.Id = obe.Id;
                                fullfillattributeObj.Caption = levelObj.LevelName;
                                fullfillattributeObj.Level = levelObj.Level;
                                fullfillattributeObj.AttributeTypeID = obe.AttributeTypeID;
                                listAttributes.Add(fullfillattributeObj);
                    }
                        }
                        foreach (var obj in entityTypeXmlDao)
                    {
                            if (obj.AttributeTypeID != Convert.ToInt32(AttributesList.DropDownTree))
                            {
                                IAttribute fullfillattributeObj = new BrandSystems.Marcom.Core.Metadata.Attribute();
                                fullfillattributeObj.Id = obj.Id;
                                fullfillattributeObj.Caption = obj.Caption;
                                fullfillattributeObj.Level = 0;
                                fullfillattributeObj.AttributeTypeID = obj.AttributeTypeID;
                                listAttributes.Add(fullfillattributeObj);
                    }
                        }
                    }

                    else
                    {
                        entityTypeRealtionDao = tx.PersistenceManager.MetadataRepository[proxy.MarcomManager.User.TenantID].Query<EntityTypeAttributeRelationDao>().Where(a => a.EntityTypeID == entityTypeId).ToList();
                        attributesDao = tx.PersistenceManager.MetadataRepository[proxy.MarcomManager.User.TenantID].Query<AttributeDao>().Join(entityTypeRealtionDao, a => a.Id, b => b.AttributeID, (ab, bc) =>
                            new { ab, bc }).Where(a => a.bc.EntityTypeID == entityTypeId).Select
                                (a => a.ab).ToList();
                        var treeResult = attributesDao.Where(a => a.AttributeTypeID == Convert.ToInt32(AttributesList.DropDownTree));
                        foreach (var obe in treeResult)
                        {
                            var treeLeveResult = tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].Query<TreeLevelDao>().Where(a => a.AttributeID == obe.Id);
                            foreach (var levelObj in treeLeveResult)
                            {
                                IAttribute fullfillattributeObj = new BrandSystems.Marcom.Core.Metadata.Attribute();
                                fullfillattributeObj.Id = obe.Id;
                                fullfillattributeObj.Caption = levelObj.LevelName;
                                fullfillattributeObj.Level = levelObj.Level;
                                fullfillattributeObj.AttributeTypeID = obe.AttributeTypeID;
                                listAttributes.Add(fullfillattributeObj);
                        }
                    }
                        foreach (var obj in attributesDao)
                    {
                            if (obj.AttributeTypeID != Convert.ToInt32(AttributesList.DropDownTree))
                            {
                                IAttribute fullfillattributeObj = new BrandSystems.Marcom.Core.Metadata.Attribute();
                                fullfillattributeObj.Id = obj.Id;
                                fullfillattributeObj.Caption = obj.Caption;
                                fullfillattributeObj.Level = 0;
                                fullfillattributeObj.AttributeTypeID = obj.AttributeTypeID;
                                listAttributes.Add(fullfillattributeObj);
                            }
                        }
                    }
                    return listAttributes;
                }
            }
            catch (Exception)
            {

                throw;
                    }


        }

        //public int CreateExpireActionwithMetadata(ExpireHandlerManagerProxy proxy, int ActionID, int SourceID, int SourceEnityID, int SourceFrom, string Actionexutedays, string DateActionexpiredate, bool Actionexute, bool ispublish, int ActionsourceId, IList<IAttributeData> listattributevalues, string Taskinputvalue)
                    //{
        //    int version = MarcomManagerFactory.ActiveMetadataVersionNumber;

        //    int ActionSourceID = 0;
        //    int OwnerID = 0;

        //    try
        //    {
        //        Guid NewId = Guid.NewGuid();
        //        OwnerID = Convert.ToInt32(proxy.MarcomManager.User.Id);



        //        BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("-----------------------------------------------------------------------------------------------", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
        //        BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Started CreateExpireAction", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

        //        using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
        //        {

        //            if (ActionsourceId > 0)//update Exiting expireAction
        //            {
        //                ExpireHandlerActionSourceDao Expiresourcedao = new ExpireHandlerActionSourceDao();
        //                ExpireHandlerActionSourceDataDao ExpiresourceDatadao1 = new ExpireHandlerActionSourceDataDao();
        //                Expiresourcedao = new ExpireHandlerActionSourceDao();
        //                Expiresourcedao = (from tt in tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Query<ExpireHandlerActionSourceDao>() where tt.Id == ActionsourceId select tt).Select(a => a).FirstOrDefault();
        //                ExpiresourceDatadao1 = (from tt in tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Query<ExpireHandlerActionSourceDataDao>() where tt.ActionSourceID == ActionsourceId select tt).Select(a => a).FirstOrDefault();
        //                if (Expiresourcedao != null)
        //                {
        //                    string exActionexutedays = (Actionexutedays.ToString() != "-1") ? Actionexutedays.ToString() : Expiresourcedao.Actionexutedays;
        //                    Expiresourcedao.Actionexutedays = exActionexutedays;
        //                    Expiresourcedao.Actionexutedate = getorginalexpirtime(DateActionexpiredate, exActionexutedays);
        //                    tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Save<ExpireHandlerActionSourceDao>(Expiresourcedao);
        //                    BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("CreateExpireActionwithMetadata is saved in EH_ActionSource with sourceid : " + SourceID, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

        //                }
        //                if (ExpiresourceDatadao1 != null)
        //                {
        //                    IList<ExpireHandlerActionSourceDataDao> ExpireHandlerSourceDataAction = new List<ExpireHandlerActionSourceDataDao>();
        //                    ExpireHandlerActionSourceDataDao ExpiresourceDatadao = new ExpireHandlerActionSourceDataDao();
        //                    if (listattributevalues != null && listattributevalues.Count > 0)
        //                    {
        //                        StringBuilder deleteAttribute = new StringBuilder();
        //                        deleteAttribute.AppendLine("DELETE FROM EH_ActionSourceData");
        //                        deleteAttribute.AppendLine(" WHERE ActionSourceID = " + ActionsourceId);
        //                        tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery(deleteAttribute.ToString());


        //                        foreach (var val in listattributevalues)
        //                        {

        //                            ExpireHandlerActionSourceDataDao ExpiresourceDataupdatedao = new ExpireHandlerActionSourceDataDao();
        //                            ExpiresourceDataupdatedao.ActionSourceID = ActionsourceId;
        //                            ExpiresourceDataupdatedao.AttributeID = val.ID;
        //                            ExpiresourceDataupdatedao.AttributeTypeID = val.TypeID;
        //                            ExpiresourceDataupdatedao.AttributeCaption = val.Caption;
        //                            ExpiresourceDataupdatedao.NodeID = val.Value;
        //                            ExpiresourceDataupdatedao.Level = val.Level;
        //                            ExpiresourceDataupdatedao.Ispublish = ispublish;
        //                            ExpiresourceDataupdatedao.Taskinputvalue = Taskinputvalue;
        //                            tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Save<ExpireHandlerActionSourceDataDao>(ExpiresourceDataupdatedao);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        ExpiresourceDatadao = new ExpireHandlerActionSourceDataDao();
        //                        ExpiresourceDatadao = (from tt in tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Query<ExpireHandlerActionSourceDataDao>() where tt.Id == ExpiresourceDatadao1.Id select tt).Select(a => a).FirstOrDefault();
        //                        ExpiresourceDatadao.Ispublish = ispublish;
        //                        tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Save<ExpireHandlerActionSourceDataDao>(ExpiresourceDatadao);

        //                    }

        //                }
        //                tx.Commit();
        //                ActionSourceID = ActionsourceId;
        //                return ActionSourceID;

        //            }
        //            else
        //            {

        //                ExpireHandlerActionSourceDao Expiresourcedao = new ExpireHandlerActionSourceDao();
        //                Expiresourcedao.ActionID = ActionID;
        //                Expiresourcedao.SourceID = SourceID;
        //                Expiresourcedao.SourceEnityID = SourceEnityID;
        //                Expiresourcedao.SourceFrom = SourceFrom;
        //                Expiresourcedao.Actionexutedays = Actionexutedays.ToString();
        //                Expiresourcedao.Actionexutedate = getorginalexpirtime(DateActionexpiredate, Actionexutedays);
        //                //Expiresourcedao.Actionexute = Actionexute;
        //                Expiresourcedao.ActionownerId = OwnerID;
        //                tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Save<ExpireHandlerActionSourceDao>(Expiresourcedao);
        //                ActionSourceID = Expiresourcedao.Id;
        //                if (listattributevalues != null && listattributevalues.Count > 0)
        //                {
        //                    foreach (var val in listattributevalues)
        //                    {

        //                        ExpireHandlerActionSourceDataDao ExpiresourceDatadao = new ExpireHandlerActionSourceDataDao();
        //                        ExpiresourceDatadao.ActionSourceID = ActionSourceID;
        //                        ExpiresourceDatadao.AttributeID = val.ID;
        //                        ExpiresourceDatadao.AttributeTypeID = val.TypeID;
        //                        ExpiresourceDatadao.AttributeCaption = val.Caption;
        //                        ExpiresourceDatadao.NodeID = val.Value;
        //                        ExpiresourceDatadao.Level = val.Level;
        //                        ExpiresourceDatadao.Ispublish = ispublish;
        //                        ExpiresourceDatadao.Taskinputvalue = Taskinputvalue;
        //                        tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Save<ExpireHandlerActionSourceDataDao>(ExpiresourceDatadao);
        //                    }
        //                }
        //                else
        //                {
        //                    ExpireHandlerActionSourceDataDao ExpiresourceDatadao = new ExpireHandlerActionSourceDataDao();
        //                    ExpiresourceDatadao.ActionSourceID = ActionSourceID;
        //                    //ExpiresourceDatadao.AttributeID = 0;
        //                    //ExpiresourceDatadao.AttributeTypeID = 0;
        //                    //ExpiresourceDatadao.AttributeCaption = "-";
        //                    //ExpiresourceDatadao.NodeID = "-";
        //                    //ExpiresourceDatadao.Level = 0;
        //                    ExpiresourceDatadao.Ispublish = ispublish;
        //                    ExpiresourceDatadao.Taskinputvalue = Taskinputvalue;
        //                    tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Save<ExpireHandlerActionSourceDataDao>(ExpiresourceDatadao);

        //                }
        //                tx.Commit();

        //                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("CreateExpireAction is saved in EH_ActionSource with ActionSourceID : " + ActionSourceID, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
        //                return ActionSourceID;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        BrandSystems.Marcom.Core.Metadata.LogHandler.LogError("Failed to EH_ActionSource", ex);
        //        BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("-----------------------------------------------------------------------------------------------", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
        //        return 0;
        //    }

        //}



        public IList<IExpireActionSourcedatas> GetExpireHandlermetadata(ExpireHandlerManagerProxy proxy, int actionSourceID)
                    {
            try
                        {
                IList<IExpireActionSourcedatas> listAttributes = new List<IExpireActionSourcedatas>();


                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                        {

                    var objexpirehandler = tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].GetAll<ExpireHandlerActionSourceDataDao>().Where(a => a.ActionSourceID == actionSourceID).ToList();
                    foreach (var obj in objexpirehandler)
                    {
                        ExpireActionSourcedatas expireActionSourcedatasObj = new ExpireActionSourcedatas();
                        expireActionSourcedatasObj.ActionSourceID = Convert.ToInt32(obj.ActionSourceID);
                        expireActionSourcedatasObj.AttributeCaption = Convert.ToString(obj.AttributeCaption);
                        expireActionSourcedatasObj.AttributeID = Convert.ToInt32(obj.AttributeID);
                        expireActionSourcedatasObj.AttributeTypeID = Convert.ToInt32(obj.AttributeTypeID);
                        expireActionSourcedatasObj.Level = Convert.ToInt32(obj.Level);
                        expireActionSourcedatasObj.NodeID = Convert.ToString(obj.NodeID);
                        expireActionSourcedatasObj.Id = Convert.ToInt32(obj.Id);
                        expireActionSourcedatasObj.SplValue = Convert.ToInt32(obj.SplValue);
                        expireActionSourcedatasObj.Ispublish = Convert.ToBoolean(obj.Ispublish);
                        expireActionSourcedatasObj.Taskinputvalue = Convert.ToString(obj.Taskinputvalue);
                        listAttributes.Add(expireActionSourcedatasObj);
                        //expireActionSourcedatasObj.ActionSourceID = obj.ActionSourceID;
                        }
                }
                return listAttributes;
            }
            catch (Exception)
                        {

                throw;
                                }
        }

        public bool ExpiryupdateEntityAttribute(ExpireHandlerManagerProxy proxy, IList<IAttributeData> attributeData, int entityId, int updaterid)
                                {
            //updateEntityAttributes(ITransaction tx, IList<IAttributeData> attributeData, int entityId, int typeId)
            bool updatestatus = false;
            using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                                    {
                int entityTypeId = 0;
                try
                {

                    entityTypeId = (from item in tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Query<EntityDao>() where item.Id == entityId select item.Typeid).FirstOrDefault();
                    updatestatus = updateEntityAttributes(tx, attributeData, entityId, entityTypeId, updaterid, proxy.MarcomManager.User.TenantID);
                    //tx.Commit();
                    return updatestatus;
                                    }
                catch (Exception ex)
                                    {
                    BrandSystems.Marcom.Core.Metadata.LogHandler.LogError("Failed to ExpiryupdateEntityAttribute", ex);
                    BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("-----------------------------------------------------------------------------------------------", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                    return false;
                                    }
                                }


            return true;
        }
        public bool updateEntityAttributes(ITransaction tx, IList<IAttributeData> attributeData, int entityId, int typeId, int updaterid, int TenantID)
        {
            if (attributeData != null)
            {
                NotificationFeedObjects newsobj = new NotificationFeedObjects();
                BrandSystems.Marcom.Core.Utility.FeedNotificationServer fs = new Utility.FeedNotificationServer(TenantID);
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Started inseting values into Dynamic tables", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                string entityName = "AttributeRecord" + typeId + "_V" + MarcomManagerFactory.ActiveMetadataVersionNumber;
                IList<BrandSystems.Marcom.Core.Planning.Interface.IDynamicAttributes> listdynamicattributes = new List<BrandSystems.Marcom.Core.Planning.Interface.IDynamicAttributes>();
                Dictionary<string, object> dictAttr = new Dictionary<string, object>();
                IList<MultiSelectDao> listMultiselect = new List<MultiSelectDao>();
                IList<TagWordsSelectDao> listTagWordsSelectDao = new List<TagWordsSelectDao>();
                IList<TreeValueDao> listreeval = new List<TreeValueDao>();
                listreeval.Clear();
                DynamicAttributesDao dynamicdao = new DynamicAttributesDao();

                ArrayList entityids = new ArrayList();
                foreach (var obj in attributeData)
                {
                    entityids.Add(obj.ID);
                }
                var result = from item in tx.PersistenceManager.PlanningRepository[TenantID].Query<AttributeDao>() where entityids.Contains(item.Id) select item;
                //var removingOwner = result.Where(a => a.Id != Convert.ToInt32(SystemDefinedAttributes.Owner));
                var entityTypeCategory = tx.PersistenceManager.PlanningRepository[TenantID].Query<EntityTypeDao>().Where(a => a.Id == typeId).Select(a => a.Category).FirstOrDefault();
                var dynamicAttResult = result.Where(a => ((a.Id != 69) && (a.AttributeTypeID == 1 || a.AttributeTypeID == 2 || a.AttributeTypeID == 3 || a.AttributeTypeID == 5 || a.AttributeTypeID == 8 || a.AttributeTypeID == 9 || a.AttributeTypeID == 11 || a.AttributeTypeID == 16)));
                var treenodeResult = result.Where(a => a.AttributeTypeID == (Convert.ToInt32(AttributesList.Tree)));
                var treevalResult = result.Where(a => a.AttributeTypeID == (Convert.ToInt32(AttributesList.DropDownTree)));
                var multiAttrResult = result.Where(a => a.AttributeTypeID == (Convert.ToInt32(AttributesList.ListMultiSelection)));
                var tagwordsAttrResult = result.Where(a => a.AttributeTypeID == (Convert.ToInt32(AttributesList.Tag)));
                var systemDefinedResults = result.Where(a => a.IsSpecial == true);
                var multiselecttreevalResult = result.Where(a => a.AttributeTypeID == (Convert.ToInt32(AttributesList.TreeMultiSelection)));
                var multiselectPricingtreevalResult = result.Where(a => a.AttributeTypeID == (Convert.ToInt32(AttributesList.DropDownTreePricing)));

                if (systemDefinedResults.Count() > 0)
                {

                    //foreach (var val in systemDefinedResults)
                    //{
                    //    SystemDefinedAttributes systemType = (SystemDefinedAttributes)val.Id;
                    //    var dataResult = attributeData.Join(systemDefinedResults,
                    //            post => post.ID,
                    //            meta => meta.Id,
                    //            (post, meta) => new { databaseval = post }).Where(a => a.databaseval.ID == Convert.ToInt32(SystemDefinedAttributes.Owner));
                    //    switch (systemType)
                    //    {
                    //        case SystemDefinedAttributes.Owner:
                    //            IList<EntityRoleUserDao> Ientitroledao = new List<EntityRoleUserDao>();
                    //            if (dataResult.Count() > 0)
                    //            {
                    //                foreach (var ownerObj in dataResult)
                    //                {
                    //                    EntityRoleUserDao entityroledao = new EntityRoleUserDao();
                    //                    entityroledao.Entityid = entityId;
                    //                    var NewObj = tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].Query<EntityTypeRoleAclDao>().Where(t => t.EntityTypeID == typeId && (EntityRoles)t.EntityRoleID == EntityRoles.Owner).SingleOrDefault();
                    //                    int RoleID = NewObj.ID;
                    //                    entityroledao.Roleid = RoleID;
                    //                    entityroledao.Userid = ownerObj.databaseval.Value;
                    //                    entityroledao.IsInherited = false;
                    //                    entityroledao.InheritedFromEntityid = 0;
                    //                    Ientitroledao.Add(entityroledao);

                    //                }
                    //                tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].Save<EntityRoleUserDao>(Ientitroledao);
                    //            }
                    //            break;
                    //    }
                    //}
                }

                if (treevalResult.Count() > 0)
                {
                    var treeValQuery = attributeData.Join(treevalResult,
                                 post => post.ID,
                                 meta => meta.Id,
                                 (post, meta) => new { databaseval = post });
                    if (treeValQuery.Count() > 0)
                    {
                        foreach (var treeattr in treeValQuery)
                                {
                            foreach (var treevalobj in treeattr.databaseval.Value)
                                    {
                                TreeValueDao tre = new TreeValueDao();
                                tre.Attributeid = treeattr.databaseval.ID;
                                tre.Entityid = entityId;
                                tre.Nodeid = treevalobj;
                                tre.Level = treeattr.databaseval.Level;
                                tre.Value = "";
                                listreeval.Add(tre);
                            }
                        }
                        tx.PersistenceManager.PlanningRepository[TenantID].Save<Marcom.Dal.Metadata.Model.TreeValueDao>(listreeval);
                                    }
                                }
                if (multiselecttreevalResult.Count() > 0)
                {
                    var multiselecttreeValQuery = attributeData.Join(multiselecttreevalResult,
                                 post => post.ID,
                                 meta => meta.Id,
                                 (post, meta) => new { databaseval = post });
                    if (multiselecttreeValQuery.Count() > 0)
                    {
                        foreach (var treeattr in multiselecttreeValQuery)
                                {
                            foreach (var treevalobj in treeattr.databaseval.Value)
                                    {
                                TreeValueDao tre = new TreeValueDao();
                                tre.Attributeid = treeattr.databaseval.ID;
                                tre.Entityid = entityId;
                                tre.Nodeid = treevalobj;
                                tre.Level = treeattr.databaseval.Level;
                                tre.Value = "";
                                listreeval.Add(tre);
                            }
                        }
                        tx.PersistenceManager.PlanningRepository[TenantID].Save<Marcom.Dal.Metadata.Model.TreeValueDao>(listreeval);
                    }
                                    }

                if (multiselectPricingtreevalResult.Count() > 0)
                {
                    var multiselecttreeValQuery = attributeData.Join(multiselectPricingtreevalResult,
                                 post => post.ID,
                                 meta => meta.Id,
                                 (post, meta) => new { databaseval = post });
                    if (multiselecttreeValQuery.Count() > 0)
                    {
                        foreach (var treeattr in multiselecttreeValQuery)
                        {
                            foreach (var treevalobj in treeattr.databaseval.Value)
                                    {
                                TreeValueDao tre = new TreeValueDao();
                                tre.Attributeid = treeattr.databaseval.ID;
                                tre.Entityid = entityId;
                                tre.Nodeid = treevalobj;
                                tre.Level = treeattr.databaseval.Level;
                                tre.Value = treeattr.databaseval.SpecialValue;
                                listreeval.Add(tre);
                            }
                        }
                        tx.PersistenceManager.PlanningRepository[TenantID].Save<Marcom.Dal.Metadata.Model.TreeValueDao>(listreeval);
                                    }
                                }
                if (tagwordsAttrResult.Count() > 0)
                {
                    //tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].DeleteByID<Marcom.Dal.Metadata.Model.TagWordsSelectDao>(entityId);

                    IList<PopularTagWordsDao> poptagwrds = new List<PopularTagWordsDao>();
                    //string deletequery = "DELETE FROM MM_TagWordsSelect WHERE EntityID = ? ";
                    //tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].ExecuteQuerywithMinParam(deletequery.ToString(), Convert.ToInt32(entityId));

                    var query = attributeData.Join(tagwordsAttrResult,
                             post => post.ID,
                             meta => meta.Id,
                             (post, meta) => new { databaseval = post, attrappval = meta });
                    ArrayList optid = new ArrayList();
                    foreach (var at in query)
                    {
                        string query1 = "DELETE FROM MM_TagWordsSelect WHERE EntityID = ? AND AttributeID = ? ";
                        tx.PersistenceManager.PlanningRepository[TenantID].ExecuteQuerywithMinParam(query1.ToString(), Convert.ToInt32(entityId), Convert.ToInt32(at.databaseval.ID));
                        Marcom.Dal.Metadata.Model.TagWordsSelectDao mt = new Marcom.Dal.Metadata.Model.TagWordsSelectDao();
                        mt.Attributeid = at.databaseval.ID;
                        mt.Entityid = entityId;
                        mt.Optionid = Convert.ToInt32(at.databaseval.Value);
                        optid.Add(Convert.ToInt32(at.databaseval.Value));
                        listTagWordsSelectDao.Add(mt);

                        Marcom.Dal.Metadata.Model.PopularTagWordsDao poptag = new Marcom.Dal.Metadata.Model.PopularTagWordsDao();
                        poptag.Id = Convert.ToInt32(at.databaseval.Value);
                        poptag.UsedOn = DateTime.Today.ToString("yyyy-MM-dd");
                        poptag.TotalHits = 1;
                        poptag.AttributeID = 1;

                    }

                    tx.PersistenceManager.PlanningRepository[TenantID].Save<Marcom.Dal.Metadata.Model.TagWordsSelectDao>(listTagWordsSelectDao);

                    poptagwrds = (from item in tx.PersistenceManager.PlanningRepository[TenantID].Query<PopularTagWordsDao>() where optid.Contains(item.Id) select item).ToList();
                    foreach (var obj in poptagwrds)
                    {
                        obj.UsedOn = DateTime.Today.ToString("yyyy-MM-dd");
                        obj.TotalHits = obj.TotalHits + 1;
                    }
                    tx.PersistenceManager.PlanningRepository[TenantID].Save<Marcom.Dal.Metadata.Model.PopularTagWordsDao>(poptagwrds);

                }
                if (multiAttrResult.Count() > 0)
                {
                    //newsobj.obj3 = new ArrayList();
                    //newsobj.obj2 = new List<object>();
                    //IList<MultiSelectDao> listMultiselect4 = new List<MultiSelectDao>();
                    //var listOfOldValuesforFeed = (from item in tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].Query<MultiSelectDao>() where item.Attributeid == Convert.ToInt32(attributeid) && item.Entityid == Convert.ToInt32(entityId) select item).ToList();

                    //tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].DeleteByID<Marcom.Dal.Metadata.Model.MultiSelectDao>(entityId);

                    //string deletequery = "DELETE FROM MM_MultiSelect WHERE EntityID = ? ";
                    //tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].ExecuteQuerywithMinParam(deletequery.ToString(), Convert.ToInt32(entityId));
                    int perviousAttributeId = 0;
                    int currattputid = 0;
                    List<object> obj4 = new List<object>();
                    var query = attributeData.Join(multiAttrResult,
                             post => post.ID,
                             meta => meta.Id,
                             (post, meta) => new { databaseval = post, attrappval = meta });
                    int i = 0;
                    foreach (var at in query)
                    {
                        i = i + 1;
                        if (perviousAttributeId == 0)
                        {
                            perviousAttributeId = at.databaseval.ID;
                            newsobj = new NotificationFeedObjects();
                            fs = new Utility.FeedNotificationServer(TenantID);
                            newsobj.obj3 = new ArrayList();
                            obj4 = new List<object>();
                            var listOfOldValuesforFeed = (from item in tx.PersistenceManager.PlanningRepository[TenantID].Query<MultiSelectDao>() where item.Attributeid == Convert.ToInt32(at.databaseval.ID) && item.Entityid == Convert.ToInt32(entityId) select item).ToList();
                            foreach (var element in listOfOldValuesforFeed)
                                {
                                newsobj.obj3.Add(element.Optionid);
                                }
                            obj4.Add(at.databaseval.Value.ToString());
                            var attrdetails = (from item in tx.PersistenceManager.MetadataRepository[TenantID].Query<AttributeDao>() where item.Id == (int)at.databaseval.ID select item).FirstOrDefault();
                            newsobj.AttributeDetails = new List<AttributeDao>();
                            newsobj.AttributeDetails.Add(attrdetails);
                            if (query.Count() == 1)
                                {
                                newsobj.action = "metadata update";
                                newsobj.Actorid = updaterid;
                                newsobj.AttributeId = Convert.ToInt32(perviousAttributeId);
                                newsobj.EntityId = entityId;
                                newsobj.Attributetypeid = 4;
                                newsobj.obj2 = obj4;
                                fs.AsynchronousNotify(newsobj);
                            }

                        }
                        else if (perviousAttributeId > 0 && perviousAttributeId != at.databaseval.ID && query.Count() > 1)
                                    {
                            obj4.Add(at.databaseval.Value.ToString());
                            newsobj.action = "metadata update";
                            newsobj.Actorid = updaterid;
                            newsobj.AttributeId = Convert.ToInt32(perviousAttributeId);
                            newsobj.EntityId = entityId;
                            newsobj.Attributetypeid = 4;
                            newsobj.obj2 = obj4;
                            fs.AsynchronousNotify(newsobj);
                            perviousAttributeId = 0;
                                    }
                        else if (perviousAttributeId > 0 && perviousAttributeId == at.databaseval.ID && query.Count() > 1)
                        {
                            if (i != query.Count())
                                obj4.Add(at.databaseval.Value.ToString());
                                    else
                                    {
                                newsobj.action = "metadata update";
                                newsobj.Actorid = updaterid;
                                obj4.Add(at.databaseval.Value.ToString());
                                newsobj.AttributeId = Convert.ToInt32(perviousAttributeId);
                                newsobj.EntityId = entityId;
                                newsobj.Attributetypeid = 4;
                                newsobj.obj2 = obj4;
                                fs.AsynchronousNotify(newsobj);
                                perviousAttributeId = 0;
                                    }
                                }

                        string query1 = "DELETE FROM MM_MultiSelect WHERE EntityID = ? AND AttributeID = ? ";
                        tx.PersistenceManager.PlanningRepository[TenantID].ExecuteQuerywithMinParam(query1.ToString(), Convert.ToInt32(entityId), Convert.ToInt32(at.databaseval.ID));

                        Marcom.Dal.Metadata.Model.MultiSelectDao mt = new Marcom.Dal.Metadata.Model.MultiSelectDao();
                        mt.Attributeid = at.databaseval.ID;
                        mt.Entityid = entityId;
                        mt.Optionid = Convert.ToInt32(at.databaseval.Value);
                        listMultiselect.Add(mt);
                    }
                    tx.PersistenceManager.PlanningRepository[TenantID].Save<Marcom.Dal.Metadata.Model.MultiSelectDao>(listMultiselect);
                                    }
                if (treenodeResult.Count() > 0)
                                    {
                    var treenodequery = attributeData.Join(treenodeResult,
                                 post => post.ID,
                                 meta => meta.Id,
                                 (post, meta) => new { databaseval = post });
                    foreach (var et in treenodequery)
                                        {
                        foreach (var treenodeobj in et.databaseval.Value)
                                            {
                            Marcom.Dal.Metadata.Model.TreeValueDao tre = new Marcom.Dal.Metadata.Model.TreeValueDao();
                            tre.Attributeid = et.databaseval.ID;
                            tre.Entityid = entityId;
                            tre.Nodeid = treenodeobj;
                            tre.Level = et.databaseval.Level;
                            listreeval.Add(tre);
                                        }
                                    }
                    tx.PersistenceManager.PlanningRepository[TenantID].Save<Marcom.Dal.Metadata.Model.TreeValueDao>(listreeval);
                                }
                if (dynamicAttResult.Count() > 0 || entityTypeCategory != 1)
                                    {
                    Dictionary<string, dynamic> attr = new Dictionary<string, dynamic>();


                    var dynamicAttrQuery = attributeData.Join(dynamicAttResult,
                                post => post.ID,
                                meta => meta.Id,
                                (post, meta) => new { databaseval = post });
                    foreach (var ab in dynamicAttrQuery)
                    {

                        string key = Convert.ToString((int)ab.databaseval.ID);
                        int attributedataType = ab.databaseval.TypeID;
                        // dynamic value = ab.databaseval.Value;
                        StringBuilder DynamicQuery = new StringBuilder();
                        List<object> obj4 = new List<object>();
                        dynamic value = null;
                        switch (attributedataType)
                        {
                            case 1:
                            case 2:
                                {
                                    value = Convert.ToString(ab.databaseval.Value == null ? "" : HttpUtility.HtmlEncode((string)ab.databaseval.Value));
                                    var attrdetails = (from item in tx.PersistenceManager.MetadataRepository[TenantID].Query<AttributeDao>() where item.Id == (int)ab.databaseval.ID select item).FirstOrDefault();
                                    newsobj = new NotificationFeedObjects();
                                    fs = new Utility.FeedNotificationServer(TenantID);
                                    obj4 = new List<object>();
                                    obj4.Add(HttpUtility.HtmlEncode((string)value));
                                    string str = "select * from MM_AttributeRecord_" + typeId + " where ID= ? ";
                                    IList item3 = tx.PersistenceManager.MetadataRepository[TenantID].ExecuteQuerywithMinParam(str, Convert.ToInt32(entityId));
                                    newsobj.obj3 = item3;
                                    DynamicQuery = new StringBuilder();
                                    DynamicQuery.Append("update  MM_AttributeRecord_" + typeId + " set Attr_" + key + "= ? where ID= ? ");
                                    tx.PersistenceManager.PlanningRepository[TenantID].ExecuteQuerywithMinParam(DynamicQuery.ToString(), HttpUtility.HtmlEncode((string)value), entityId);

                                    newsobj.action = "metadata update";
                                    newsobj.Actorid = updaterid;
                                    newsobj.AttributeId = (int)ab.databaseval.ID;
                                    newsobj.EntityId = entityId;
                                    newsobj.AttributeDetails = new List<AttributeDao>();
                                    newsobj.AttributeDetails.Add(attrdetails);
                                    newsobj.Attributetypeid = attributedataType;
                                    newsobj.obj2 = obj4;
                                    fs.AsynchronousNotify(newsobj);
                                    DynamicQuery = new StringBuilder();

                                    break;
                                }
                            //case 11:

                            case 3:
                                {
                                    value = Convert.ToString((Convert.ToString(ab.databaseval.Value) == null || Convert.ToString(ab.databaseval.Value) == "") ? 0 : (int)ab.databaseval.Value);
                                    var attrdetails = (from item in tx.PersistenceManager.MetadataRepository[TenantID].Query<AttributeDao>() where item.Id == (int)ab.databaseval.ID select item).FirstOrDefault();
                                    newsobj = new NotificationFeedObjects();
                                    fs = new Utility.FeedNotificationServer(TenantID);
                                    obj4 = new List<object>();
                                    obj4.Add(value);
                                    string str = "select * from MM_AttributeRecord_" + typeId + " where ID= ? ";
                                    IList item3 = tx.PersistenceManager.MetadataRepository[TenantID].ExecuteQuerywithMinParam(str, Convert.ToInt32(entityId));
                                    newsobj.obj3 = item3;
                                    DynamicQuery = new StringBuilder();
                                    DynamicQuery.Append("update  MM_AttributeRecord_" + typeId + " set Attr_" + key + "= ? where ID= ? ");
                                    tx.PersistenceManager.PlanningRepository[TenantID].ExecuteQuerywithMinParam(DynamicQuery.ToString(), value, entityId);
                                    newsobj.action = "metadata update";
                                    newsobj.Actorid = updaterid;
                                    newsobj.AttributeId = (int)ab.databaseval.ID;
                                    newsobj.EntityId = entityId;
                                    newsobj.AttributeDetails = new List<AttributeDao>();
                                    newsobj.AttributeDetails.Add(attrdetails);
                                    newsobj.Attributetypeid = attributedataType;
                                    newsobj.obj2 = obj4;
                                    fs.AsynchronousNotify(newsobj);
                                    break;
                                }
                            case 5:
                                {
                                    if (ab.databaseval.Value != null && ab.databaseval.Value != "")
                                        value = DateTime.Parse(ab.databaseval.Value == null || ab.databaseval.Value == "" ? "" : (string)ab.databaseval.Value.ToString());
                                else
                                        value = null;

                                    var attrdetails = (from item in tx.PersistenceManager.MetadataRepository[TenantID].Query<AttributeDao>() where item.Id == (int)ab.databaseval.ID select item).FirstOrDefault();
                                    newsobj = new NotificationFeedObjects();
                                    fs = new Utility.FeedNotificationServer(TenantID);
                                    obj4 = new List<object>();
                                    obj4.Add(value);
                                    string str = "select * from MM_AttributeRecord_" + typeId + " where ID= ? ";
                                    IList item3 = tx.PersistenceManager.MetadataRepository[TenantID].ExecuteQuerywithMinParam(str, Convert.ToInt32(entityId));
                                    newsobj.obj3 = item3;
                                    DynamicQuery = new StringBuilder();
                                    DynamicQuery.Append("update  MM_AttributeRecord_" + typeId + " set Attr_" + key + "= ? where ID= ? ");
                                    tx.PersistenceManager.PlanningRepository[TenantID].ExecuteQuerywithMinParam(DynamicQuery.ToString(), value, entityId);
                                    newsobj.action = "metadata update";
                                    newsobj.Actorid = updaterid;
                                    newsobj.AttributeId = (int)ab.databaseval.ID;
                                    newsobj.EntityId = entityId;
                                    newsobj.AttributeDetails = new List<AttributeDao>();
                                    newsobj.AttributeDetails.Add(attrdetails);
                                    newsobj.Attributetypeid = attributedataType;
                                    newsobj.obj2 = obj4;
                                    fs.AsynchronousNotify(newsobj);
                                    break;
                                }
                            case 16:
                                {
                                    if (ab.databaseval.Value != null && ab.databaseval.Value != "")
                                        value = DateTime.Parse(ab.databaseval.Value == null || ab.databaseval.Value == "" ? "" : (string)ab.databaseval.Value.ToString());
                                    else
                                        value = null;

                                    DynamicQuery = new StringBuilder();
                                    DynamicQuery.Append("update  MM_AttributeRecord_" + typeId + " set Attr_" + key + "= ? where ID= ? ");
                                    tx.PersistenceManager.PlanningRepository[TenantID].ExecuteQuerywithMinParam(DynamicQuery.ToString(), value, entityId);

                                break;
                                    }
                            case 8:
                                    {
                                    value = 0;
                                    int n;
                                    if (ab.databaseval.Value != null)
                                        {
                                        if (int.TryParse(Convert.ToString(ab.databaseval.Value), out n))
                                            {
                                            value = Convert.ToInt32(ab.databaseval.Value);
                                        }
                                    }

                                    DynamicQuery = new StringBuilder();
                                    DynamicQuery.Append("update  MM_AttributeRecord_" + typeId + " set Attr_" + key + "= ? where ID= ? ");
                                    tx.PersistenceManager.PlanningRepository[TenantID].ExecuteQuerywithMinParam(DynamicQuery.ToString(), value, entityId);

                                    break;
                                }
                            case 9:
                                {
                                    if (ab.databaseval.Value != 0 && ab.databaseval.Value != 1)
                                    {
                                        value = Convert.ToBoolean(ab.databaseval.Value == null || ab.databaseval.Value == "" ? false : ab.databaseval.Value);
                                    }
                                    else if (ab.databaseval.Value == 0 || ab.databaseval.Value == 1)
                                    {
                                        value = (ab.databaseval.Value == 0) ? false : true;
                                }
                                    DynamicQuery = new StringBuilder();
                                    DynamicQuery.Append("update  MM_AttributeRecord_" + typeId + " set Attr_" + key + "= ? where ID= ? ");
                                    tx.PersistenceManager.PlanningRepository[TenantID].ExecuteQuerywithMinParam(DynamicQuery.ToString(), value, entityId);

                                break;
                        }
                        }
                        attr.Add(key, value);
                    }
                    dictAttr = attr != null ? attr : null;
                    tx.Commit();
                    //dynamicdao.Id = entityId;
                    //dynamicdao.Attributes = dictAttr;
                    //DynamicQuery.Append("update  MM_AttributeRecord_" + entityTypeid.Typeid + " set Attr_" + attributeid + "= ? where ID= ? ");

                    //tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].ExecuteQuerywithMinParam(DynamicQuery.ToString(), HttpUtility.HtmlEncode((string)NewValue[0]), EntityID);

                    //tx.Commit();
                    //tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].SaveByentity<DynamicAttributesDao>(entityName, dynamicdao);
                    BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Saved Succesfully into Dynamic tables", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

            }
            }
            return true;
        }

        /// <summary>
        /// Gets the entitytype relation.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="version">The version.</param>
        /// <returns>List of IEntityTypeAttributeRelationWithLevels</returns>
        public IList<IEntityTypeAttributeRelationwithLevels> GetEntityTypeAttributeRelationWithLevelsByID(ExpireHandlerManagerProxy proxy, int entityID)
        {
            try
            {
                BaseEntityDao bdao = new BaseEntityDao();
                IList<IEntityTypeAttributeRelationwithLevels> attrs = new List<IEntityTypeAttributeRelationwithLevels>();
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    bdao = tx.PersistenceManager.ExpireHandlerRepository[proxy.MarcomManager.User.TenantID].Query<BaseEntityDao>().Where(a => a.Id == entityID).FirstOrDefault();
                    tx.Commit();
                    attrs = proxy.MarcomManager.MetadataManager.GetEntityTypeAttributeRelationWithLevelsByID(bdao.Typeid, bdao.Parentid);
                    if (attrs != null)
                            {
                        int[] systemsdefined = { (int)SystemDefinedAttributes.Owner, (int)SystemDefinedAttributes.Name, (int)SystemDefinedAttributes.Status, (int)SystemDefinedAttributes.MyRoleEntityAccess,
                                               (int)SystemDefinedAttributes.MyRoleGlobalAccess,(int)SystemDefinedAttributes.EntityStatus,(int)SystemDefinedAttributes.EntityOnTimeStatus};
                        if (attrs.Count > 0)
                            return attrs.Where(a => !systemsdefined.Contains(a.AttributeID)).ToList<IEntityTypeAttributeRelationwithLevels>();
                    }
                    return attrs;

                            }
                        }
            catch (Exception ex)
                            {
                return null;
            }
            }

        public bool ValidExpireActionDate(ExpireHandlerManagerProxy proxy, int SourceID, string DateActionexpiredate, int SourcetypeID, int Actiontype, string Actionexutedays, int SourceAttributeID, int SourceFrom, int ProductionEntityID, string DueActionexutedays)
        {



            int OwnerID = 0;

            try
            {

                OwnerID = Convert.ToInt32(proxy.MarcomManager.User.Id);


                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("-----------------------------------------------------------------------------------------------", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Started UpdateExpireActionDate", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    string dateformate;
                    dateformate = proxy.MarcomManager.GlobalAdditionalSettings[0].SettingValue.ToString().Replace('m', 'M');
                    DateTime d1 = DateTime.Now;
                    double DueActionexpireNrOfDays = 1.00;
                    //DateTime d2 = DateTime.Now.AddDays(-5);
                    DateTime Actionexpiredate = getorginalexpirtime(DateActionexpiredate, Actionexutedays, dateformate);
                    TimeSpan t = Actionexpiredate - d1;
                    double ActionexpireNrOfDays = t.TotalDays;

                    if (Actionexutedays != DueActionexutedays)
                        if (DueActionexutedays.Length > 0 && DueActionexutedays != "-1")
                        {
                            DateTime DueActionexpiredate = getorginalexpirtime(DateActionexpiredate, DueActionexutedays, dateformate);
                            TimeSpan t2 = DueActionexpiredate - Actionexpiredate;
                            DueActionexpireNrOfDays = t2.TotalDays;
                        }

                    if (ActionexpireNrOfDays > 0 && DueActionexpireNrOfDays > 0)
                        return true;
                            else
                        return false;
                        tx.Commit();

                    //return true;


                    //BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("CreateExpireAction is saved in EH_ActionSource with ActionSourceID : " + ActionSourceID, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                }
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogError("Failed to EH_ActionSource", ex);
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("-----------------------------------------------------------------------------------------------", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                return false;
            }

            return true;
        }

        public IList<IEntityTypeExpireRoleAcl> GetEntityTypeExpireRoleAccess(ExpireHandlerManagerProxy proxy, int EntityTypeID)
        {
            try
            {

                IList<IEntityTypeExpireRoleAcl> _iientityTyperoleobj = new List<IEntityTypeExpireRoleAcl>();
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    IList<EntityTypeExpireRoleAcl> dao = new List<EntityTypeExpireRoleAcl>();
                    int[] FilterIds = { (int)EntityRoles.Owner, (int)EntityRoles.BudgerApprover };
                    var entityroleobj = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Query<EntityTypeRoleAclDao>().Where(a => a.EntityTypeID == EntityTypeID && !FilterIds.Contains(a.EntityRoleID)).OrderBy(a => a.Sortorder);
                    foreach (var item in entityroleobj)
                    {
                        EntityTypeExpireRoleAcl entitytypeRoleAclObj = new EntityTypeExpireRoleAcl();
                        entitytypeRoleAclObj.ID = item.ID;
                        entitytypeRoleAclObj.EntityTypeID = item.EntityTypeID;
                        entitytypeRoleAclObj.EntityRoleID = item.EntityRoleID;
                        entitytypeRoleAclObj.ModuleID = item.ModuleID;
                        entitytypeRoleAclObj.Sortorder = item.Sortorder;
                        entitytypeRoleAclObj.Caption = item.Caption;
                        int expireoleid = 0;                     //string roleString = roleval.Caption;
                        if (item.EntityRoleID == (int)EntityRoles.Owner)
                            expireoleid = (int)ExpireEntityRoles.Owner;
                        else if (item.EntityRoleID == (int)EntityRoles.Editer)
                            expireoleid = (int)ExpireEntityRoles.Editor;
                        else if (item.EntityRoleID == (int)EntityRoles.Viewer)
                            expireoleid = (int)ExpireEntityRoles.Viewer;
                        else if (item.EntityRoleID == (int)EntityRoles.ExternalParticipate)
                            expireoleid = (int)ExpireEntityRoles.ExternalParticipant;
                        else if (item.EntityRoleID == (int)EntityRoles.BudgerApprover)
                            expireoleid = (int)ExpireEntityRoles.BudgetApprover;
                        else if (item.EntityRoleID == (int)EntityRoles.ProofInitiator)
                            expireoleid = (int)ExpireEntityRoles.ProofInitiator;

                        entitytypeRoleAclObj.ExpireRoleID = expireoleid;
                        _iientityTyperoleobj.Add(entitytypeRoleAclObj);
                    }
                }
                return _iientityTyperoleobj;

            }
            catch
            {


            }
            return null;
        }



    }
}
