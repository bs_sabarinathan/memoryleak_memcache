
'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .run(
    ['$rootScope', '$state', '$stateParams',
      function ($rootScope, $state, $stateParams) {
          $rootScope.$state = $state;
          $rootScope.$stateParams = $stateParams;
      }
    ]
  )
.config(['$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', function ($stateProvider, $urlRouterProvider, JQ_CONFIG) {
    $urlRouterProvider.otherwise('/mui/task');
    $stateProvider
         .state('mui', {
             abstract: true,
             url: '/mui',
             templateUrl: 'newviews/app.html',
             cache: true,
             resolve: {
                 deps: ['$ocLazyLoad',
                 function ($ocLazyLoad) {
                     return $ocLazyLoad.load('/app/controllers/muiJS.js').then(function () {
                     })
                     .then(function () {
                         return $ocLazyLoad.load(['/appnew/Jquerybootstrapbundle.min.js']);
                     })
                 }]
             }
         }).state('mui.metadatasettings', {
             url: '/metadatasettings',
             templateUrl: 'views/mui/MetadataSettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/metadataSettings-controller.js', JQ_CONFIG.Plupload, JQ_CONFIG.AdjustWidth, JQ_CONFIG.select2]);
                 }]
             }
         }).state('mui.MetadataConfiguration', {
             url: '/MetadataConfiguration',
             templateUrl: 'views/mui/MetadataConfiguration.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/metadataconfiguration-controller.js', 'app/directives/adminalternate-directive.js', JQ_CONFIG.AdjustWidth, JQ_CONFIG.nod, JQ_CONFIG.tablednd, JQ_CONFIG.select2, JQ_CONFIG.tinycolor, JQ_CONFIG.Pickcolor, JQ_CONFIG.spectrum, JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.MetadataConfiguration.entitytypeattributerelation', {
             url: '/entitytypeattributerelation',
             templateUrl: 'views/mui/admin/entitytypeattributerelation.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/entitytypeattributerelation-controller.js']);
                 }]
             }
         }).state('mui.MetadataConfiguration.Damentitytype', {
             url: '/Damentitytype',
             templateUrl: 'views/mui/admin/Damentitytype.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/Damentitytype-controller.js']);
                 }]
             }
         }).state('mui.MetadataConfiguration.Taskentitytype', {
             url: '/Taskentitytype',
             templateUrl: 'views/mui/admin/Taskentitytype.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/taskentitytype-controller.js']);
                 }]
             }
         }).state('mui.MetadataConfiguration.cmspagetypeattributerelation', {
             url: '/cmspagetypeattributerelation',
             templateUrl: 'views/mui/admin/cmspagetypeattributerelation.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/cmspagetypeattributerelation-controller.js', 'app/services/cms-service.js']);
                 }]
             }
         }).state('mui.MetadataConfiguration.attributegroup', {
             url: '/attributegroup',
             templateUrl: 'views/mui/admin/attributegroup.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/attributegroup-controller.js']);
                 }]
             }
         }).state('mui.MetadataConfiguration.attribute', {
             url: '/MetadataConfiguration/attribute',
             templateUrl: 'views/mui/admin/attribute.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/attribute-controller.js']);
                 }]
             }
         }).state('mui.MetadataConfiguration.userdetails', {
             url: '/userdetails',
             templateUrl: 'views/mui/admin/userdetails.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/userdetails-controller.js', 'app/services/user-service.js']);
                 }]
             }
         }).state('mui.admin', {
             url: '/admin',
             templateUrl: 'views/mui/admin.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin-controller.js', 'app/services/ServiceJs.js']);
                 }]
             }
         }).state('mui.admin.superadmin', {
             url: '/superadmin',
             templateUrl: 'views/mui/admin/superadmin.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/superadmin-controller.js']);
                 }]
             }
         }).state('mui.admin.titlelogo', {
             url: '/titlelogo',
             templateUrl: 'views/mui/admin/titlelogo.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/titlelogo-controller.js', 'app/directives/adminalternate-directive.js', JQ_CONFIG.spectrum, JQ_CONFIG.tinycolor, JQ_CONFIG.Pickcolor, JQ_CONFIG.Plupload, JQ_CONFIG.AdjustWidth, JQ_CONFIG.select2]);
                 }]
             }
         }).state('mui.admin.navigation', {
             url: '/navigation',
             templateUrl: 'views/mui/admin/navigation.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/navigation-controller.js', JQ_CONFIG.select2, JQ_CONFIG.tablednd, JQ_CONFIG.nod, JQ_CONFIG.AdjustWidth]);
                 }]
             }
         }).state('mui.admin.additionalsettings', {
             url: '/additionalsettings',
             templateUrl: 'views/mui/admin/additionalsettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/additionalsettings-controller.js', JQ_CONFIG.select2]);
                 }]
             }
         }).state('mui.admin.languagesettings', {
             url: '/languagesettings',
             templateUrl: 'views/mui/admin/languagesettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/languagesettings-controller.js', JQ_CONFIG.select2]);
                 }]
             }
         }).state('mui.admin.notificationfiltersettings', {
             url: '/NotificationFilterSettings',
             templateUrl: 'views/mui/admin/NotificationFilterSettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/notificationfiltersettings-controller.js']);
                 }]
             }
         }).state('mui.admin.newsfeedfiltersettings', {
             url: '/NewsFeedFilterSettings',
             templateUrl: 'views/mui/admin/NewsFeedFilterSettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/newsfeedfiltersettings-controller.js']);
                 }]
             }
         }).state('mui.admin.searchengine', {
             url: '/searchengine',
             templateUrl: 'views/mui/admin/searchengine.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/searchengine-controller.js']);
                 }]
             }
         }).state('mui.admin.ganttviewheaderbar', {
             url: '/ganttviewheaderbar',
             templateUrl: 'views/mui/admin/ganttviewheaderbar.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/ganttviewheaderbar-controller.js']);
                 }]
             }
         }).state('mui.admin.statistics', {
             url: '/statistics',
             templateUrl: 'views/mui/admin/statistics.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/statistics-controller.js', JQ_CONFIG.select2, JQ_CONFIG.Canvas]);
                 }]
             }
         }).state('mui.admin.broadcastmsg', {
             url: '/broadcastmsg',
             templateUrl: 'views/mui/admin/broadcastmsg.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/broadcastmsg-controller.js']);
                 }]
             }
         }).state('mui.admin.tabs', {
             url: '/tabs',
             templateUrl: 'views/mui/admin/tabs.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/tabs-controller.js', JQ_CONFIG.select2, JQ_CONFIG.jquerymask, JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.admin.updates', {
             url: '/updates',
             templateUrl: 'views/mui/admin/updates.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/updates-controller.js']);
                 }]
             }
         }).state('mui.admin.passwordpolicy', {
             url: '/passwordpolicy',
             templateUrl: 'views/mui/admin/passwordpolicy.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/passwordpolicy-controller.js', JQ_CONFIG.select2, JQ_CONFIG.jquerymask]);
                 }]
             }
         }).state('mui.admin.searchcriteria', {
             url: '/searchcriteria',
             templateUrl: 'views/mui/admin/searchcriteria.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/searchcriteria-controller.js', 'app/services/dam-service.js', JQ_CONFIG.select2, JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.admin.businessdays', {
             url: '/businessdays',
             templateUrl: 'views/mui/admin/businessdays.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/businessdays-controller.js']);
                 }]
             }
         }).state('mui.admin.cloudsettings', {
             url: '/cloudsettings',
             templateUrl: 'views/mui/admin/cloudsettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/cloudsettings-controller.js']);
                 }]
             }
         }).state('mui.admin.user', {
             url: '/user',
             templateUrl: 'views/mui/admin/user.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/user-controller.js', 'app/services/user-service.js', JQ_CONFIG.passwordstrength, JQ_CONFIG.select2, JQ_CONFIG.jqueryQtip, JQ_CONFIG.nod]);
                 }]
             }
         }).state('mui.admin.apiusers', {
             url: '/apiusers',
             templateUrl: 'views/mui/admin/apiusers.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/apiusers-controller.js', 'app/services/user-service.js']);
                 }]
             }
         }).state('mui.admin.accountlock', {
             url: '/accountlock',
             templateUrl: 'views/mui/admin/accountlock.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/acclock-controller.js', JQ_CONFIG.jquerymask]);
                 }]
             }
         }).state('mui.admin.role', {
             url: '/role',
             templateUrl: 'views/mui/admin/role.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/role-controller.js']);
                 }]
             }
         }).state('mui.admin.pendinguser', {
             url: '/PendingUser',
             templateUrl: 'views/mui/admin/PendingUser.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/pendinguser-controller.js', 'app/services/user-service.js']);
                 }]
             }
         }).state('mui.admin.module', {
             url: '/module',
             templateUrl: 'views/mui/admin/module.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/module-controller.js']);
                 }]
             }
         }).state('mui.admin.SSO', {
             url: '/SSO',
             templateUrl: 'views/mui/admin/SSO.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/SSO-controller.js', JQ_CONFIG.select2]);
                 }]
             }
         }).state('mui.admin.assetaccess', {
             url: '/assetaccess',
             templateUrl: 'views/mui/admin/assetaccess.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/assetaccess-controller.js', 'app/services/dam-service.js', JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.admin.approvalrole', {
             url: '/approvalrole',
             templateUrl: 'views/mui/admin/approvalrole.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/approvalrole-controller.js']);
                 }]
             }
         }).state('mui.admin.dalimuser', {
             url: '/dalimuser',
             templateUrl: 'views/mui/admin/DalimUsers.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/DalimUser-controller.js', 'app/services/user-service.js']);
                 }]
             }
         }).state('mui.admin.listsettings', {
             url: '/listsettings',
             templateUrl: 'views/mui/admin/listsettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/listsettings-controller.js', JQ_CONFIG.JqueryCustom, JQ_CONFIG.select2]);
                 }]
             }
         }).state('mui.admin.treesettings', {
             url: '/treesettings',
             templateUrl: 'views/mui/admin/treesettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/treesettings-controller.js', JQ_CONFIG.JqueryCustom, JQ_CONFIG.select2]);
                 }]
             }
         }).state('mui.admin.ganttviewsettings', {
             url: '/ganttviewsettings',
             templateUrl: 'views/mui/admin/ganttviewsettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/ganttviewsettings-controller.js', JQ_CONFIG.JqueryCustom, JQ_CONFIG.select2]);
                 }]
             }
         }).state('mui.admin.listviewsettings', {
             url: '/listviewsettings',
             templateUrl: 'views/mui/admin/listviewsettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/listviewsettings-controller.js', JQ_CONFIG.select2, JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.admin.rootlevelfiltersettings', {
             url: '/rootlevelfiltersettings',
             templateUrl: 'views/mui/admin/rootlevelfiltersettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/rootlevelfiltersettings-controller.js', JQ_CONFIG.select2, JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.admin.detailfiltersettings', {
             url: '/detailfiltersettings',
             templateUrl: 'views/mui/admin/detailfiltersettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/detailfiltersettings-controller.js', JQ_CONFIG.select2, JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.admin.myworkspacesettings', {
             url: '/myworkspacesettings',
             templateUrl: 'views/mui/admin/myworkspacesettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/myworkspacesettings-controller.js', JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.admin.plantabsettings', {
             url: '/plantabsettings',
             templateUrl: 'views/mui/admin/plantabsettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/plantabsettings-controller.js']);
                 }]
             }
         }).state('mui.admin.designlayout', {
             url: '/designlayout',
             templateUrl: 'views/mui/admin/designlayout.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/designlayout-controller.js', 'app/services/report-service.js', JQ_CONFIG.select2, JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.admin.financialforecastsettings', {
             url: '/financialforecastsettings',
             templateUrl: 'views/mui/admin/Financialforecastsettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/Financialforecastsettings-controller.js', JQ_CONFIG.checkbox, JQ_CONFIG.select2]);
                 }]
             }
         }).state('mui.admin.financialforecast', {
             url: '/financialforecast',
             templateUrl: 'views/mui/admin/financialforecast.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/financialforecast-controller.js']);
                 }]
             }
         }).state('mui.admin.POSettings', {
             url: '/POSettings',
             templateUrl: 'views/mui/admin/POSettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/purchaseordersettings-controller.js']);
                 }]
             }
         }).state('mui.admin.Currencyconvertersettings', {
             url: '/Currencyconvertersettings',
             templateUrl: 'views/mui/admin/Currencyconvertersettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/Currencyconvertersettings-controller.js', JQ_CONFIG.select2]);
                 }]
             }
         }).state('mui.admin.financialmetadata', {
             url: '/financialmetadata',
             templateUrl: 'views/mui/admin/Financialmetadata.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/financialmetadata-controller.js', JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.admin.pometadata', {
             url: '/pometadata',
             templateUrl: 'views/mui/admin/pometadata.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/financialmetadata-controller.js', JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.admin.spenttransaction', {
             url: '/spenttransaction',
             templateUrl: 'views/mui/admin/spenttransaction.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/financialmetadata-controller.js', JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.admin.assetcreation', {
             url: '/assetcreation',
             templateUrl: 'views/mui/admin/assetcreationsettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/assetcreationsettings-controller.js', 'app/services/dam-service.js', JQ_CONFIG.JqueryCustom, JQ_CONFIG.select2]);
                 }]
             }
         }).state('mui.admin.damviewsettings', {
             url: '/damviewsettings',
             templateUrl: 'views/mui/admin/damviewsettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/damviewsettings-controller.js', JQ_CONFIG.JqueryCustom, JQ_CONFIG.select2]);
                 }]
             }
         }).state('mui.admin.ReformatProfilesettings', {
             url: '/ReformatProfilesettings',
             templateUrl: 'views/mui/admin/ReformatProfilesettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/ReformatProfilesettings-controller.js', JQ_CONFIG.select2]);
                 }]
             }
         }).state('mui.admin.optimaker', {
             url: '/optimaker',
             templateUrl: 'views/mui/admin/optimaker.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/optimaker-controller.js', 'app/services/dam-service.js', JQ_CONFIG.JqueryCustom, JQ_CONFIG.select2]);
                 }]
             }
         }).state('mui.admin.assetcategory', {
             url: '/assetcategory',
             templateUrl: 'views/mui/admin/assetcategory.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/assetcategory-controller.js', 'app/services/dam-service.js']);
                 }]
             }
         }).state('mui.admin.assetTypeFileExtension', {
             url: '/assetTypeFileExtension',
             templateUrl: 'views/mui/admin/assetTypeFileExtensionRelation.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/assetTypeFileExtensionRelation-controller.js', 'app/services/dam-service.js']);
                 }]
             }
         }).state('mui.admin.assetsnippet', {
             url: '/assetsnippet',
             templateUrl: 'views/mui/admin/assetsnippet.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/assetsnippet-controller.js', 'app/directives/adminalternate-directive.js', 'app/services/dam-service.js', JQ_CONFIG.spectrum, JQ_CONFIG.Pickcolor]);
                 }]
             }
         }).state('mui.admin.TemplateEngines', {
             url: '/TemplateEngines',
             templateUrl: 'views/mui/admin/TemplateEngines.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/TemplateEngines-controller.js', 'app/services/dam-service.js']);
                 }]
             }
         }).state('mui.admin.dashboardwidget', {
             url: '/dashboardwidget',
             templateUrl: 'views/mui/admin/dashboardwidget.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/dashboardwidget-controller.js', JQ_CONFIG.AdjustWidth, JQ_CONFIG.multiselect, JQ_CONFIG.multiselectpartial]);
                 }]
             }
         }).state('mui.admin.dashboardtemplate', {
             url: '/dashboardtemplate',
             templateUrl: 'views/mui/admin/dashboardtemplate.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/dashboardtemplate-controller.js', JQ_CONFIG.AdjustWidth, JQ_CONFIG.multiselect, JQ_CONFIG.multiselectpartial]);
                 }]
             }
         }).state('mui.admin.Units', {
             url: '/dalimcredential',
             templateUrl: 'views/mui/admin/Units.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/Units-controller.js']);
                 }]
             }
         }).state('mui.admin.cmstemplate', {
             url: '/cmstemplate',
             templateUrl: 'views/mui/admin/cmstemplate.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/cmstemplate-controller.js', 'app/services/cms-service.js', JQ_CONFIG.Html2Canvas, JQ_CONFIG.Plupload]);
                 }]
             }
         }).state('mui.admin.cmscustomfontsettings', {
             url: '/cmscustomfontsettings',
             templateUrl: 'views/mui/admin/CmsCustomFontSettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/CmsCustomFontSettings-controller.js', 'app/services/cms-service.js', JQ_CONFIG.select2]);
                 }]
             }
         }).state('mui.admin.cmscustomstylesettings', {
             url: '/cmscustomstylesettings',
             templateUrl: 'views/mui/admin/CmsCustomStyleSettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/CmsCustomStyleSettings-controller.js', 'app/services/cms-service.js']);
                 }]
             }
         }).state('mui.admin.cmstreestylesettings', {
             url: '/cmstreestylesettings',
             templateUrl: 'views/mui/admin/CmsTreeStyleSettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/CmsTreeStyleSettings-controller.js', 'app/directives/adminalternate-directive.js', 'app/services/cms-service.js', JQ_CONFIG.select2, JQ_CONFIG.Pickcolor, JQ_CONFIG.tinycolor, JQ_CONFIG.spectrum]);
                 }]
             }
         }).state('mui.admin.credentialmanagement', {
             url: '/credentialmanagement',
             templateUrl: 'views/mui/admin/credentialmanagement.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/credentialmanagement-controller.js', JQ_CONFIG.nod]);
                 }]
             }
         }).state('mui.admin.reports', {
             url: '/reports',
             templateUrl: 'views/mui/admin/reports.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/reports-controller.js', 'app/services/report-service.js']);
                 }]
             }
         }).state('mui.admin.customview', {
             url: '/customview',
             templateUrl: 'views/mui/admin/customview.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/customview-controller.js', JQ_CONFIG.nod, JQ_CONFIG.codemirror]);
                 }]
             }
         }).state('mui.admin.financialreportsettings', {
             url: '/financialreportsettings',
             templateUrl: 'views/mui/admin/financialreportsettings.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/financialreportsettings-controller.js', 'app/services/report-service.js', JQ_CONFIG.Jcrop, JQ_CONFIG.Plupload]);
                 }]
             }
         }).state('mui.admin.ganttviewreport', {
             url: '/ganttviewreport',
             templateUrl: 'views/mui/admin/ganttviewreport.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/ganttviewreport-controller.js', 'app/services/report-service.js', JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.admin.customlist', {
             url: '/customlist',
             templateUrl: 'views/mui/admin/customlist.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/customlist-controller.js']);
                 }]
             }
         }).state('mui.admin.taskflag', {
             url: '/taskflag',
             templateUrl: 'views/mui/admin/taskflag.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/taskflag-controller.js', 'app/directives/adminalternate-directive.js', 'app/services/task-service.js', JQ_CONFIG.tinycolor, JQ_CONFIG.Pickcolor, JQ_CONFIG.checkbox, JQ_CONFIG.spectrum]);
                 }]
             }
         }).state('mui.admin.approvaltemplate', {
             url: '/approvalflowtemplate',
             templateUrl: 'views/mui/admin/approvalflowtemplate.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/taskPredefinedApproveFlow.js', 'app/directives/adminalternate-directive.js', 'app/services/task-service.js', JQ_CONFIG.Pickcolor, JQ_CONFIG.tinycolor, JQ_CONFIG.spectrum]);
                 }]
             }
         }).state('mui.admin.tasklistlibrary', {
             url: '/tasklistlibrary',
             templateUrl: 'views/mui/admin/tasklistlibrary.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/tasklistlibrary-controller.js', 'app/services/task-service.js', 'app/services/dam-service.js', JQ_CONFIG.JqueryCustom, JQ_CONFIG.Plupload]);
                 }]
             }
         }).state('mui.admin.tasktemplate', {
             url: '/tasktemplate',
             templateUrl: 'views/mui/admin/tasktemplate.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/tasktemplate-controller.js', 'app/directives/adminalternate-directive.js', 'app/services/task-service.js', JQ_CONFIG.spectrum, JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.mypage', {
             url: '/mypage',
             templateUrl: 'views/mui/MyPage.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/mypage-controller.js', 'app/services/user-service.js', JQ_CONFIG.Plupload, JQ_CONFIG.AdjustWidth, JQ_CONFIG.bootstrapnotify]);
                 }]
             }
         }).state('mui.task', {
             url: '/task',
             templateUrl: 'views/mui/mytask.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/Mytask_Js.js', 'assets/css/Mytask_Css.css']);
                 }]
             }
         }).state('mui.mynotification', {
             url: '/mynotification',
             templateUrl: 'views/mui/mynotification.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/mynotification-controller.js', 'app/services/cms-service.js', 'app/services/common-services.js', JQ_CONFIG.AdjustWidth]);
                 }]
             }
         }).state('mui.myfundingrequest', {
             url: '/myfundingrequest',
             templateUrl: 'views/mui/myfundingrequest.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/myfundingrequest-controller.js', 'app/directives/multiselect-directives.js', 'app/directives/tooltip-directives.js', 'app/services/task-service.js', 'app/services/common-services.js', JQ_CONFIG.multiselectpartial, JQ_CONFIG.AdjustWidth, JQ_CONFIG.tooltip]);
                 }]
             }
         }).state('mui.myworkspace', {
             url: '/myworkspace',
             templateUrl: 'views/mui/myworkspace.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/myworkspace-controller.js', JQ_CONFIG.AdjustWidth]);
                 }]
             }
         }).state('mui.dashboard', {
             url: '/dashboard',
             templateUrl: 'views/mui/dashboard.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/dashboard-controller.js', 'app/services/common-services.js', 'app/services/metadata-service.js', 'app/services/planning-service.js', JQ_CONFIG.AdjustWidth]);
                 }]
             }
         }).state('mui.marcommediabank', {
             url: '/marcommediabank',
             templateUrl: 'views/mui/mediabank.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/mediabank-controller.js', 'app/services/dam-service.js', 'app/services/cms-service.js', 'app/services/planning-service.js', 'app/services/Metadata-service.js', 'app/services/task-service.js', 'app/services/user-service.js', 'app/services/expirehandler-service.js', 'app/directives/tooltip-directives.js', JQ_CONFIG.bootstrapeditable, 'app/directives/planningtool-directives.js', JQ_CONFIG.bootstrapnotify, JQ_CONFIG.tooltip, JQ_CONFIG.AdjustWidth, JQ_CONFIG.JqueryCustom, JQ_CONFIG.checkbox, JQ_CONFIG.multiselectpartial, JQ_CONFIG.multiselect, 'app/controllers/mui/DAM/damlightbox-controller.js', 'app/controllers/mui/DAM/assetedit-controller.js', 'app/controllers/mui/assetreformat-controller.js', JQ_CONFIG.Jcrop, JQ_CONFIG.Plupload, JQ_CONFIG, 'app/controllers/mui/fileupload-controller.js', 'app/controllers/mui/task/TaskCreation-controller.js']);
                 }]
             }
         }).state('mui.admin.proofhqcredential', {
             url: '/proofhqcredential',
             templateUrl: 'views/mui/admin/proofhqtaskcredential.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/proofhqcredential-controller.js', 'app/services/task-service.js']);
                 }]
             }
         }).state('mui.admin.dalimcredential', {
             url: '/dalimcredential',
             templateUrl: 'views/mui/admin/dalimtaskcredential.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/admin/dalimcredential-controller.js', 'app/services/task-service.js']);
                 }]
             }
         }).state('mui.planningtool', {
             url: '/planningtool',
             templateUrl: 'views/mui/planningtool.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool-controller.js']);
                 }]
             }
         }).state('mui.planningtool.default', {
             url: '/default',
             templateUrl: 'views/mui/planningtool/default.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default-controller.js', 'assets/js/plandefault-plugin.js', JQ_CONFIG.date]);
                 }]
             }
         }).state('mui.planningtool.default.list', {
             url: '/Plans',
             templateUrl: 'api/View/GetListView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/planningJS.js', 'assets/css/planningCSS.css']);
                 }]
             }
         }).state('mui.planningtool.default.detail', {
             url: '/detail',
             templateUrl: 'api/View/GetDetailView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/plandetailJS.js', 'assets/css/plandetailCSS.min.css', 'assets/js/plandetail-plugins.js']);
                 }]
             }
         }).state('mui.planningtool.default.detail.ganttview', {
             url: '/ganttview/:TrackID',
             templateUrl: 'views/mui/planningtool/default/detail/ganttview.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/planganttviewJS.js']);
                 }]
             }
         }).state('mui.planningtool.default.detail.listview', {
             url: '/listview/:TrackID',
             templateUrl: 'views/mui/planningtool/default/detail/listview.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/listview-controller.js', 'app/controllers/mui/planningtool/default/detail/detailfilter-controller.js', 'app/controllers/mui/planningtool/calender/calendercreation-controller.js', 'app/services/calender-service.js', 'app/services/report-service.js', 'app/services/user-service.js', JQ_CONFIG.wizard, JQ_CONFIG.nod, JQ_CONFIG.AdjustWidth, 'app/directives/planningtool-directives.js', JQ_CONFIG.multiselect, JQ_CONFIG.multiselectpartial, JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.planningtool.default.detail.section', {
             url: '/section/:ID',
             templateUrl: 'api/View/GetSectionView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/section-controller.js', JQ_CONFIG.JqueryCustom, 'app/directives/dam-directives.js']);
                 }]
             },
             parent: 'mui.planningtool.default.detail'
         }).state('mui.planningtool.default.detail.section.overview', {
             url: '/overview',
             templateUrl: 'api/View/GetOverView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/planoverviewJS.js', 'assets/css/planoverviewCSS.css', 'assets/js/planoverview-plugin.js']);
                 }]
             }
         }).state('mui.planningtool.default.detail.section.financial', {
             url: '/financial',
             templateUrl: 'api/View/GetFinancialView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/planfinancialJS.js', 'assets/js/planfinancial-plugin.js']);
                 }]
             }
         }).state('mui.planningtool.default.detail.section.workflow', {
             url: '/workflow',
             templateUrl: 'views/mui/planningtool/default/detail/section/workflow.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/section/workflow-controller.js']);
                 }]
             }
         }).state('mui.planningtool.default.detail.section.member', {
             url: '/member',
             templateUrl: 'views/mui/planningtool/default/detail/section/member.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/section/member-controller.js']);
                 }]
             }
         }).state('mui.planningtool.default.detail.section.objective', {
             url: '/objective',
             templateUrl: 'api/View/GetObjectiveView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/planobjectiveJS.js', 'assets/js/planobjective-plugin.js']);
                 }]
             }
         }).state('mui.planningtool.default.detail.section.attachment', {
             url: '/attachment',
             templateUrl: 'views/mui/planningtool/default/detail/section/attachment.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/DAM/planattachmentJS.js', 'assets/css/planattachmentCSS.css', 'assets/js/planattachment-plugin.js']);
                 }]
             }
         }).state('mui.planningtool.default.detail.section.presentation', {
             url: '/presentation',
             templateUrl: 'views/mui/planningtool/default/detail/section/presentation.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/planpresentationJS.js', 'assets/css/planpresentationCSS.css', 'assets/js/planpresentation-plugin.js']);
                 }]
             }
         }).state('mui.planningtool.default.detail.section.task', {
             url: '/task',
             templateUrl: 'views/mui/planningtool/default/detail/section/Task.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load('app/controllers/mui/task/taskJS.js', 'assets/js/task-plugin.js', 'assets/css/taskCSS.css', 'appnew/Jquerybootstrap.min');
                 }]
             }
         }).state('mui.planningtool.default.detail.section.customtab', {
             url: '/customtab',
             templateUrl: 'views/mui/planningtool/default/detail/section/customtab.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/section/customtab-controller.js']);
                 }]
             }
         }).state('mui.planningtool.costcentre', {
             url: '/costcentre',
             templateUrl: 'views/mui/planningtool/costcentre.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load('app/controllers/mui/planningtool/costcentre-controller.js');
                 }]
             }
         }).state('mui.planningtool.costcentre.costcentre', {
             url: '/costcentre',
             templateUrl: 'api/View/GetCostcentreListView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/list-controller.js', 'app/controllers/mui/planningtool/filtersettings-controller.js', 'app/controllers/mui/planningtool/costcentre/costcentrecreation-controller.js', 'app/services/Metadata-service.js', 'app/services/planning-service.js', 'app/services/user-service.js', 'app/services/access-service.js', JQ_CONFIG.AdjustWidth, JQ_CONFIG.bootbox, JQ_CONFIG.JqueryCustom, JQ_CONFIG.bootstrapnotify, JQ_CONFIG.wizard, JQ_CONFIG.select2, JQ_CONFIG.checkbox, JQ_CONFIG.autoNumeric, JQ_CONFIG.nod]);
                 }]
             }
         }).state('mui.planningtool.costcentre.subcostcentrecreation', {
             url: '/subcostcentrecreation',
             templateUrl: 'views/mui/planningtool/costcentre/SubCostCentreCreation.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/SubCostCentreCreation-controller.js', 'app/services/user-service.js', 'app/services/planning-service.js', 'app/services/metadata-service.js', 'app/services/access-service.js']);
                 }]
             }
         }).state('mui.planningtool.costcentre.list', {
             url: '/list',
             templateUrl: 'api/View/GetCostcentreListView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/list-controller.js', 'app/controllers/mui/planningtool/filtersettings-controller.js', 'app/controllers/mui/planningtool/costcentre/costcentrecreation-controller.js', 'app/services/Metadata-service.js', 'app/services/planning-service.js', 'app/services/user-service.js', 'app/services/access-service.js', JQ_CONFIG.AdjustWidth, JQ_CONFIG.bootbox, JQ_CONFIG.JqueryCustom, JQ_CONFIG.bootstrapnotify, JQ_CONFIG.wizard, JQ_CONFIG.select2, JQ_CONFIG.checkbox, JQ_CONFIG.autoNumeric, JQ_CONFIG.nod]);
                 }]
             }
         }).state('mui.planningtool.costcentre.detail', {
             url: '/detail',
             templateUrl: 'api/View/GetCostcentreDetailView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail-controller.js', 'app/controllers/mui/planningtool/default/detail/detailfilter-controller.js', 'app/controllers/mui/planningtool/calender/calendercreation-controller.js', 'app/services/Metadata-service.js', 'app/services/common-services.js', 'app/services/dam-service.js', 'app/services/report-service.js', 'app/services/calender-service.js', JQ_CONFIG.checkbox, JQ_CONFIG.date]);
                 }]
             }
         }).state('mui.planningtool.costcentre.detail.ganttview', {
             url: '/ganttview',
             templateUrl: 'views/mui/planningtool/costcentre/detail/ganttview.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/ganttview-controller.js', 'app/controllers/mui/planningtool/default/detail/detailfilter-controller.js', 'app/controllers/mui/planningtool/SubEntityCreation-controller.js', 'app/controllers/mui/DAM/damentityselection-controller.js', 'app/services/planning-service.js', 'app/services/common-services.js', 'app/services/report-service.js', 'app/services/metadata-service.js', 'app/services/user-service.js', 'app/services/dam-service.js', JQ_CONFIG.AdjustWidth, JQ_CONFIG.date, JQ_CONFIG.jqueryQtip, JQ_CONFIG.datepicker, JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.planningtool.costcentre.detail.ganttviewtrack', {
             url: '/ganttview/:TrackID',
             templateUrl: 'views/mui/planningtool/costcentre/detail/ganttview.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/ganttview-controller.js', 'app/controllers/mui/planningtool/default/detail/detailfilter-controller.js', 'app/controllers/mui/planningtool/SubEntityCreation-controller.js', 'app/controllers/mui/DAM/damentityselection-controller.js', , 'app/services/planning-service.js', 'app/services/common-services.js', 'app/services/report-service.js', 'app/services/metadata-service.js', 'app/services/user-service.js', 'app/services/dam-service.js', JQ_CONFIG.AdjustWidth, JQ_CONFIG.date, JQ_CONFIG.jqueryQtip, JQ_CONFIG.datepicker, JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.planningtool.costcentre.detail.ganttviewzoom', {
             url: '/ganttview/:zoomlevel',
             templateUrl: 'views/mui/planningtool/costcentre/detail/ganttview.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/ganttview-controller.js', 'app/controllers/mui/planningtool/default/detail/detailfilter-controller.js', 'app/controllers/mui/planningtool/SubEntityCreation-controller.js', 'app/controllers/mui/DAM/damentityselection-controller.js', , 'app/services/planning-service.js', 'app/services/common-services.js', 'app/services/report-service.js', 'app/services/metadata-service.js', 'app/services/user-service.js', 'app/services/dam-service.js', JQ_CONFIG.AdjustWidth, JQ_CONFIG.date, JQ_CONFIG.jqueryQtip, JQ_CONFIG.datepicker, JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.planningtool.costcentre.detail.ganttviewhybrid', {
             url: '/ganttview/:zoomlevel/:TrackID',
             templateUrl: 'views/mui/planningtool/costcentre/detail/ganttview.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/ganttview-controller.js', 'app/controllers/mui/planningtool/default/detail/detailfilter-controller.js', 'app/controllers/mui/planningtool/SubEntityCreation-controller.js', 'app/controllers/mui/DAM/damentityselection-controller.js', , 'app/services/planning-service.js', 'app/services/common-services.js', 'app/services/report-service.js', 'app/services/metadata-service.js', 'app/services/user-service.js', 'app/services/dam-service.js', JQ_CONFIG.AdjustWidth, JQ_CONFIG.date, JQ_CONFIG.jqueryQtip, JQ_CONFIG.datepicker, JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.planningtool.costcentre.detail.listview', {
             url: '/listview',
             templateUrl: 'views/mui/planningtool/costcentre/detail/listview.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/listview-controller.js', 'app/controllers/mui/planningtool/SubEntityCreation-controller.js', 'app/controllers/mui/planningtool/costcentre/detail/detailfilter-controller.js', 'app/controllers/mui/DAM/damentityselection-controller.js', , 'app/services/planning-service.js', 'app/services/common-services.js', 'app/services/report-service.js', 'app/services/metadata-service.js', 'app/services/user-service.js', 'app/services/dam-service.js', JQ_CONFIG.AdjustWidth, JQ_CONFIG.date]);
                 }]
             }
         }).state('mui.planningtool.costcentre.detail.listviewTrack', {
             url: '/listview/:TrackID',
             templateUrl: 'views/mui/planningtool/costcentre/detail/listview.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/listview-controller.js', 'app/controllers/mui/planningtool/SubEntityCreation-controller.js', 'app/controllers/mui/planningtool/costcentre/detail/detailfilter-controller.js', 'app/controllers/mui/DAM/damentityselection-controller.js', 'app/services/planning-service.js', 'app/services/common-services.js', 'app/services/report-service.js', 'app/services/metadata-service.js', 'app/services/user-service.js', 'app/services/dam-service.js', JQ_CONFIG.AdjustWidth, JQ_CONFIG.date]);
                 }]
             }
         }).state('mui.planningtool.costcentre.detail.section', {
             url: '/section/:ID',
             templateUrl: 'api/View/GetCostcentreSectionView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/section-controller.js', 'app/controllers/mui/planningtool/SubEntityCreation-controller.js', 'app/controllers/mui/planningtool/costcentre/detail/detailfilter-controller.js', 'app/services/metadata-service.js', 'app/services/common-services.js', 'app/services/planning-service.js', JQ_CONFIG.AdjustWidth]);
                 }]
             }
         }).state('mui.planningtool.costcentre.detail.section.overview', {
             url: '/overview',
             templateUrl: 'api/View/GetCostcentreOverView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/section/overview-controller.js', 'app/services/user-service.js', 'app/directives/multiselect-directives.js', 'app/directives/planningtool-directives.js', JQ_CONFIG.Plupload, JQ_CONFIG.bootstrapeditable, JQ_CONFIG.multiselect, JQ_CONFIG.multiselectpartial, JQ_CONFIG.Canvas, JQ_CONFIG.jqueryQtip, JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.planningtool.costcentre.detail.section.financial', {
             url: '/financial',
             templateUrl: 'api/View/GetCCFinancialView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/section/financial-controller.js']);
                 }]
             }
         }).state('mui.planningtool.costcentre.detail.section.member', {
             url: '/member',
             templateUrl: 'views/mui/planningtool/costcentre/detail/section/member.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/section/member-controller.js', 'app/controllers/mui/DAM/damentityselection-controller.js', 'app/services/user-service.js', 'app/services/access-service.js', 'app/directives/planningtool-directives.js', JQ_CONFIG.JqueryCustom, JQ_CONFIG.jqueryQtip, , 'app/directives/tooltip-directives.js']);
                 }]
             }
         }).state('mui.planningtool.costcentre.detail.section.task', {
             url: '/task',
             templateUrl: 'views/mui/planningtool/default/detail/section/Task.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/task/taskJS.js', 'assets/js/task-plugin.js', 'assets/css/taskCSS.css']);
                 }]
             }
         }).state('mui.planningtool.costcentre.detail.section.taskbyID', {
             url: '/task/:TaskID',
             templateUrl: 'views/mui/planningtool/default/detail/section/Task.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/section/task-controller.js']);
                 }]
             }
         }).state('mui.planningtool.costcentre.detail.section.workflow', {
             url: '/workflow',
             templateUrl: 'views/mui/planningtool/costcentre/detail/section/workflow.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/section/workflow-controller.js']);
                 }]
             }
         }).state('mui.planningtool.costcentre.detail.section.attachment', {
             url: '/attachment',
             templateUrl: 'views/mui/planningtool/default/detail/section/attachment.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/section/attachment-controller.js', 'app/controllers/mui/dam-controller.js', 'app/controllers/mui/planningtool/calender/calendercreation-controller.js', 'app/services/dam-service.js', 'app/services/report-service.js', 'app/services/user-service.js', 'app/services/calender-service.js', 'app/services/task-service.js', 'app/controllers/mui/DAM/assetthumpnailview-controller.js', 'app/controllers/mui/DAM/damentityselection-controller.js', 'app/controllers/mui/DAM/assetcreation-controller.js', 'app/controllers/mui/DAM/assetedit-controller.js', 'app/controllers/mui/task/TaskCreation-controller.js', 'app/controllers/mui/DAM/assetlist-controller.js', 'app/controllers/mui/DAM/damlightbox-controller.js', 'app/controllers/mui/DAM/showallassets-controller.js', JQ_CONFIG.Plupload, JQ_CONFIG.JqueryCustom, JQ_CONFIG.qtip, JQ_CONFIG.jqueryQtip, JQ_CONFIG.multiselectpartial, 'app/directives/tooltip-directives.js', 'app/directives/dam-directives.js', 'app/directives/planningtool-directives.js', JQ_CONFIG.wizard, JQ_CONFIG.date, JQ_CONFIG.nod, JQ_CONFIG.abn_tree, JQ_CONFIG.tinycolor, JQ_CONFIG.Pickcolor, 'app/directives/adminalternate-directive.js', JQ_CONFIG.spectrum, JQ_CONFIG.modalpopup, JQ_CONFIG.select2]);
                 }]
             }
         }).state('mui.planningtool.costcentre.detail.section.presentation', {
             url: '/presentation',
             templateUrl: 'views/mui/planningtool/costcentre/detail/section/presentation.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/costcentre/detail/section/presentation-controller.js', JQ_CONFIG.Redactor, JQ_CONFIG.date, JQ_CONFIG.datepicker]);
                 }]
             }
         }).state('mui.planningtool.calender', {
             url: '/calender',
             templateUrl: 'views/mui/planningtool/calender.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender-controller.js']);
                 }]
             }
         }).state('mui.planningtool.calender.list', {
             url: '/list',
             templateUrl: 'api/View/GetMuiListView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/list-controller.js', 'app/controllers/mui/planningtool/calender/detail/filtersettings-controller.js', 'app/services/planning-service.js', 'app/services/Metadata-service.js', 'app/services/calender-service.js', 'app/services/user-service.js', 'app/services/common-services.js', 'app/controllers/mui/planningtool/calender/calendercreation-controller.js', JQ_CONFIG.AdjustWidth, JQ_CONFIG.wizard, JQ_CONFIG.nod, JQ_CONFIG.checkbox, JQ_CONFIG.tooltip, 'app/directives/tooltip-directives.js', 'app/directives/planningtool-directives.js', JQ_CONFIG.JqueryCustom, JQ_CONFIG.multiselect, JQ_CONFIG.multiselectpartial]);
                 }]
             }
         }).state('mui.planningtool.calender.detail', {
             url: '/detail',
             templateUrl: 'api/View/GetMuiDetailView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail-controller.js', 'app/controllers/mui/planningtool/calender/detail/detailfilter-controller.js', 'app/services/planning-service.js', 'app/services/Metadata-service.js', 'app/services/common-services.js', 'app/services/user-service.js', 'app/controllers/mui/DAM/damentityselection-controller.js', 'app/services/report-service.js', 'app/services/dam-service.js', 'app/controllers/mui/planningtool/SubEntityCreation-controller.js', JQ_CONFIG.tooltip, 'app/directives/tooltip-directives.js', JQ_CONFIG.AdjustWidth]);
                 }]
             }
         }).state('mui.planningtool.calender.detail.ganttview', {
             url: '/ganttview/:TrackID',
             templateUrl: 'views/mui/planningtool/calender/detail/ganttview.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/ganttview-controller.js', JQ_CONFIG.date, 'app/controllers/mui/DAM/damentityselection-controller.js', 'app/controllers/mui/planningtool/SubEntityCreation-controller.js', 'app/services/dam-service.js', 'app/services/user-service.js', JQ_CONFIG.AdjustWidth]);
                 }]
             }
         }).state('mui.planningtool.calender.detail.listview', {
             url: '/listview/:TrackID',
             templateUrl: 'views/mui/planningtool/calender/detail/listview.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/listview-controller.js']);
                 }]
             }
         }).state('mui.planningtool.calender.detail.section', {
             url: '/section/:ID',
             templateUrl: 'api/View/GetMuiSectionView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/section-controller.js']);
                 }]
             }
         }).state('mui.planningtool.calender.detail.section.overview', {
             url: '/overview',
             templateUrl: 'views/mui/planningtool/calender/detail/section/overview.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/section/overview-controller.js']);
                 }]
             }
         }).state('mui.planningtool.calender.detail.section.member', {
             url: '/member',
             templateUrl: 'views/mui/planningtool/calender/detail/section/member.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/section/member-controller.js']);
                 }]
             }
         }).state('mui.planningtool.calender.detail.section.attachment', {
             url: '/attachment',
             templateUrl: 'views/mui/planningtool/default/detail/section/attachment.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/section/attachment-controller.js', 'assets/js/Bootstrap/bootstrap-dropdown.js', 'app/controllers/mui/dam-controller.js', 'app/controllers/mui/planningtool/default/detail/detailfilter-controller.js', 'app/controllers/mui/planningtool/calender/calendercreation-controller.js', 'app/services/common-services.js', 'app/services/dam-service.js', 'app/services/report-service.js', 'app/services/user-service.js', 'app/services/calender-service.js', 'app/services/task-service.js', 'app/controllers/mui/DAM/assetthumpnailview-controller.js', 'app/controllers/mui/DAM/assetcreation-controller.js', 'app/controllers/mui/DAM/assetedit-controller.js', 'app/controllers/mui/task/TaskCreation-controller.js', 'app/controllers/mui/DAM/assetlist-controller.js', 'app/controllers/mui/DAM/damlightbox-controller.js', 'app/controllers/mui/DAM/showallassets-controller.js', JQ_CONFIG.Plupload, JQ_CONFIG.JqueryCustom, JQ_CONFIG.qtip, 'app/directives/tooltip-directives.js', 'app/directives/dam-directives.js', JQ_CONFIG.wizard, JQ_CONFIG.date, JQ_CONFIG.nod, JQ_CONFIG.abn_tree, JQ_CONFIG.tinycolor, JQ_CONFIG.Pickcolor, 'app/directives/adminalternate-directive.js', JQ_CONFIG.spectrum, JQ_CONFIG.modalpopup]);
                 }]
             }
         }).state('mui.planningtool.calender.detail.section.presentation', {
             url: '/presentation',
             templateUrl: 'views/mui/planningtool/calender/detail/section/presentation.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/section/presentation-controller.js', 'app/services/planning-service.js']);
                 }]
             }
         }).state('mui.planningtool.calender.detail.section.task', {
             url: '/task',
             templateUrl: 'views/mui/planningtool/default/detail/section/task.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/task/taskJS.js', 'assets/js/task-plugin.js', 'assets/css/taskCSS.css']);
                 }]
             }
         }).state('mui.planningtool.calender.detail.section.customtab', {
             url: '/customtab',
             templateUrl: 'views/mui/planningtool/calender/detail/section/customtab.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/calender/detail/section/customtab-controller.js']);
                 }]
             }
         }).state('mui.planningtool.objective', {
             url: '/objective',
             templateUrl: 'views/mui/planningtool/objective.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective-controller.js', JQ_CONFIG.AdjustWidth, JQ_CONFIG.tooltip, 'app/directives/tooltip-directives.js']);
                 }]
             }
         }).state('mui.planningtool.objective.list', {
             url: '/list',
             templateUrl: 'api/View/GetObjectiveListView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/list-controller.js', 'app/controllers/mui/planningtool/objective/detail/filtersettings-controller.js', 'app/controllers/mui/planningtool/objective/objectivecreation-controller.js', JQ_CONFIG.jqueryQtip, 'app/services/planning-service.js', 'app/services/Metadata-service.js', 'app/services/calender-service.js', 'app/services/access-service.js', 'app/services/user-service.js', 'app/services/common-services.js', 'app/controllers/mui/planningtool/calender/calendercreation-controller.js', JQ_CONFIG.wizard, JQ_CONFIG.nod, JQ_CONFIG.checkbox, 'app/directives/planningtool-directives.js', JQ_CONFIG.JqueryCustom, JQ_CONFIG.multiselect, JQ_CONFIG.multiselectpartial, JQ_CONFIG.autoNumeric, JQ_CONFIG.modalpopup]);
                 }]
             }
         }).state('mui.planningtool.objective.detail', {
             url: '/detail',
             templateUrl: 'api/View/GetObjectiveDetailView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail-controller.js', 'app/controllers/mui/planningtool/objective/detail/detailfilter-controller.js', 'app/directives/tooltip-directives.js', 'app/services/planning-service.js', 'app/services/Metadata-service.js', 'app/services/common-services.js', 'app/services/user-service.js', 'app/controllers/mui/DAM/damentityselection-controller.js', 'app/services/report-service.js', 'app/services/dam-service.js', 'app/controllers/mui/planningtool/SubEntityCreation-controller.js', JQ_CONFIG.tooltip, 'app/directives/tooltip-directives.js']);
                 }]
             }
         }).state('mui.planningtool.objective.detail.ganttview', {
             url: '/ganttview/:TrackID',
             templateUrl: 'views/mui/planningtool/objective/detail/ganttview.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/ganttview-controller.js', 'app/controllers/mui/planningtool/SubEntityCreation-controller.js', 'app/controllers/mui/planningtool/default/detail/detailfilter-controller.js', JQ_CONFIG.date, JQ_CONFIG.JqueryCustom, JQ_CONFIG.select2, 'app/services/access-service.js', 'app/directives/planningtool-directives.js', JQ_CONFIG.multiselect, JQ_CONFIG.multiselectpartial]);
                 }]
             }
         }).state('mui.planningtool.objective.detail.listview', {
             url: '/listview/:TrackID',
             templateUrl: 'views/mui/planningtool/objective/detail/listview.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/listview-controller.js']);
                 }]
             }
         }).state('mui.planningtool.objective.detail.sectionentity', {
             url: '/sectionentity',
             templateUrl: 'api/View/GetSectionView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/section-controller.js']);
                 }]
             }
         }).state('mui.planningtool.objective.detail.detailfilter', {
             url: '/detailfilter',
             templateUrl: 'views/mui/planningtool/objective/detail/detailfilter.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/detailfilter-controller.js']);
                 }]
             }
         }).state('mui.planningtool.objective.detail.section', {
             url: '/section/:ID',
             templateUrl: 'api/View/GetObjectiveSectionView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/section-controller.js', 'app/directives/planningtool-directives.js', 'app/controllers/mui/planningtool/objective/detail/section/overview-controller.js', JQ_CONFIG.date, JQ_CONFIG.multiselect, JQ_CONFIG.multiselectpartial, 'app/directives/multiselect-directives.js']);
                 }]
             }
         }).state('mui.planningtool.objective.detail.section.overview', {
             url: '/overview',
             templateUrl: 'api/View/GetObjectiveOverView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/section/overview-controller.js', JQ_CONFIG.JqueryCustom, 'app/directives/planningtool-directives.js', JQ_CONFIG.multiselect, JQ_CONFIG.multiselectpartial, JQ_CONFIG.jqueryQtip, JQ_CONFIG.bootstrapeditable]);
                 }]
             }
         }).state('mui.planningtool.objective.detail.section.member', {
             url: '/member',
             templateUrl: 'views/mui/planningtool/objective/detail/section/member.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/section/member-controller.js', 'app/directives/planningtool-directives.js', JQ_CONFIG.multiselect, JQ_CONFIG.multiselectpartial, JQ_CONFIG.jqueryQtip, JQ_CONFIG.JqueryCustom]);
                 }]
             }
         }).state('mui.planningtool.objective.detail.section.attachment', {
             url: '/attachment',
             templateUrl: 'views/mui/planningtool/default/detail/section/attachment.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/default/detail/section/attachment-controller.js', 'assets/js/Bootstrap/bootstrap-dropdown.js', 'app/controllers/mui/dam-controller.js', 'app/controllers/mui/planningtool/default/detail/detailfilter-controller.js', 'app/controllers/mui/planningtool/calender/calendercreation-controller.js', 'app/services/common-services.js', 'app/services/dam-service.js', 'app/services/report-service.js', 'app/services/user-service.js', 'app/services/calender-service.js', 'app/services/task-service.js', 'app/controllers/mui/DAM/assetthumpnailview-controller.js', 'app/controllers/mui/DAM/assetcreation-controller.js', 'app/controllers/mui/DAM/assetedit-controller.js', 'app/controllers/mui/task/TaskCreation-controller.js', 'app/controllers/mui/DAM/assetlist-controller.js', 'app/controllers/mui/DAM/damlightbox-controller.js', 'app/controllers/mui/DAM/showallassets-controller.js', JQ_CONFIG.Plupload, JQ_CONFIG.JqueryCustom, JQ_CONFIG.qtip, 'app/directives/tooltip-directives.js', 'app/directives/dam-directives.js', JQ_CONFIG.wizard, JQ_CONFIG.date, JQ_CONFIG.nod, JQ_CONFIG.abn_tree, JQ_CONFIG.tinycolor, JQ_CONFIG.Pickcolor, 'app/directives/adminalternate-directive.js', JQ_CONFIG.spectrum, JQ_CONFIG.modalpopup]);
                 }]
             }
         }).state('mui.planningtool.objective.detail.section.presentation', {
             url: '/presentation',
             templateUrl: 'views/mui/planningtool/objective/detail/section/presentation.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail/section/presentation-controller.js', JQ_CONFIG.JqueryCustom, 'app/services/planning-service.js', JQ_CONFIG.Redactor]);
                 }]
             }
         }).state('mui.planningtool.objective.detail.section.task', {
             url: '/task',
             templateUrl: 'views/mui/planningtool/default/detail/section/Task.html',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/task/taskJS.js', 'assets/js/task-plugin.js', 'assets/css/taskCSS.css']);
                 }]
             }
         }).state('mui.planningtool.objective.detail.section.customtab', {
             url: '/customtab',
             templateUrl: 'api/View/GetObjectiveDetailView',
             resolve: {
                 deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                     return $ocLazyLoad.load(['app/controllers/mui/planningtool/objective/detail-controller.js', 'app/controllers/mui/planningtool/objective/detail/detailfilter-controller.js', 'app/directives/tooltip-directives.js', 'app/services/planning-service.js', 'app/services/Metadata-service.js', 'app/services/common-services.js', 'app/services/user-service.js', 'app/controllers/mui/DAM/damentityselection-controller.js', 'app/services/report-service.js', 'app/services/dam-service.js', 'app/controllers/mui/planningtool/SubEntityCreation-controller.js', JQ_CONFIG.tooltip, 'app/directives/tooltip-directives.js']);
                 }]
             }
         })
}]);