using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
    /// MailcontentDao object for table 'CM_MailContent'.
	/// </summary>
	
	public partial class MailContentDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
        protected string _subject;
        protected string _body;
        protected string _description;
        protected int _mailsubscriptiontypeid;
		
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public MailContentDao() {}

        public MailContentDao(int pid, string pSubject, string pBody, string pDescription, int pMailSubscriptionTypeID)
		{
            this._id = pid;
			this._subject = pSubject; 
			this._body = pBody; 
			this._description = pDescription;
            this._mailsubscriptiontypeid = pMailSubscriptionTypeID; 
		}

        public MailContentDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual string Subject
		{
			get { return _subject; }            
			set 
			{
			  if (value != null && value.Length > 50)
                  throw new ArgumentOutOfRangeException("Subject", "Subject value, cannot contain more than 50 characters");
              _bIsChanged |= (_subject != value);
              _subject = value; 
			}
		}

        public virtual string Body
		{
			get { return _body; }
            set { _bIsChanged |= (_body != value); _body = value; }
			
		}

        public virtual string description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 100)
                  throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 100 characters");
              _bIsChanged |= (_description != value);
              _description = value; 
			}
			
		}
        public virtual int MailSubscriptionTypeID
        {
            get { return _mailsubscriptiontypeid; }
            set { _bIsChanged |= (_mailsubscriptiontypeid != value); _mailsubscriptiontypeid = value; }

        }
		


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string MailContentDao = "MailContentDao";			
			public const string Id = "Id";
            public const string Subject = "Subject";
            public const string Body = "Body";
            public const string description = "description";
            public const string MailSubscriptionTypeID = "MailSubscriptionTypeID";
					
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string MailContentDao = "MailContentDao";
            public const string Id = "Id";
            public const string Subject = "Subject";
            public const string Body = "Body";
            public const string description = "description";
            public const string MailSubscriptionTypeID = "MailSubscriptionTypeID";
		
		}

		#endregion
		
		
	}
	
}
