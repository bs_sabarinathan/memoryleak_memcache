﻿(function (ng, app) {
    function muiplanningtoolcalenderdetailCtrl($scope, $timeout, $http, $compile, $resource, $stateParams, $window, $location, sharedProperties, $templateCache, $cookies, $translate, CommonService, ReportService, PlanningService, MetadataService) {
        var model;
        $scope.GetNonBusinessDaysforDatePicker();
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            $scope.Ganttenddate1 = false;
            model = model1;
        };
        $scope.dynCalanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = false;
            $scope.Ganttenddate1 = true;
        };
        $scope.Newsfilter = {
            FeedgroupMulitipleFilterStatus: [],
            Feedgrouplistvalues: '-1'
        };
        var CollectionIds = "";
        if ($.cookie('ListofEntityID') == undefined || $.cookie('ListofEntityID') == null || $.cookie('ListofEntityID') == "") {
            $.cookie('ListofEntityID', GetGuidValue($stateParams.TrackID));
        }
        CollectionIds = $.cookie('ListofEntityID').split(",");
        $scope.CurrentTrackingUrl = $stateParams.TrackID;
        $window.ListofEntityID = CollectionIds;
        $scope.ExpandingEntityID = '0';
        $scope.calenderidforreport = '0';
        $scope.Level = "0";
        $scope.CurrentCalenderID = 0;
        $scope.stringValue = sharedProperties.getString();
        $scope.objectValue = sharedProperties.getObject().data;
        $("#plandetailfilterdiv").css('display', 'block')
        $scope.SelectedCostCentreCurrency = {
            TypeId: $scope.DefaultSettings.CurrencyFormat.Id,
            Name: $scope.DefaultSettings.CurrencyFormat.ShortName,
            ShortName: $scope.DefaultSettings.CurrencyFormat.Name,
            Rate: 1
        };
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.calender.detailCtrl']"));
        });
        $("#objectivedetailfilter").on('loadfiltersettings', function (event, TypeID) {
            $("#objectivedetailfiltersettings").trigger('loaddetailfiltersettings', 10);
        });
        $("#objectivedetailfilter").on('applyplandetailfilter', function (event, filterid, filtername) {
            $("#objectivedetailfiltersettings").trigger('applyplandetailfilter', [filterid, filtername]);
        });
        $("#objectivedetailfilter").on('EditFilterSettingsByFilterID', function (event, filterid, typeid) {
            $("#objectivedetailfiltersettings").trigger('EditFilterSettingsByFilterID', [filterid, typeid]);
        });
        $("#objectivedetailfilter").on('ClearScope', function (event, filterid) {
            $("#objectivedetailfiltersettings").trigger('ClearScope', [filterid]);
        });
        $("#objectivedetailfilter").on('ClearAndReApply', function (event, filterid) {
            $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
            $scope.SelectedFilterID = 0;
            $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
        });
        var FilterObj = []; 
        var AplyFilterObj = [];
        var FilterID = 0;
        $scope.ObjectiveFilterID = 0;
        $scope.ListViewDetails = [{
            data: "",
            PageIndex: 0,
            UniqueID: ""
        }];
        $scope.ListTemplate = "";
        $scope.GanttViewDataPageTemplate = '';
        $scope.GanttViewBlockPageTemplate = '';
        $scope.CurrentData = null;
        $('#TreeCtrlCss').remove();
        var GlobalChildNode = new Array();
        $scope.EntityHighlight = false;
        if ($stateParams.ID != undefined && $stateParams.ID != 0) {
            $scope.ExpandingEntityID = $stateParams.ID;
            GlobalChildNode.push($stateParams.ID);
            $scope.Level = "100";
            $scope.EntityHighlight = true;
        } else {
            GlobalChildNode = $window.ListofEntityID;
            GetTreeCount();
        }
        $scope.loadobjectivefromsearchByID = function (id, level) {
            $scope.Level = level;
            $scope.ObjectiveFilterID = 0;
            FilterObj = [];
            $scope.ListViewDetails = [{
                data: "",
                PageIndex: 0,
                UniqueID: ""
            }];
            $scope.ListTemplate = "";
            $scope.GanttViewDataPageTemplate = '';
            $scope.GanttViewBlockPageTemplate = '';
            GlobalChildNode = new Array();
            GlobalChildNode.push(id)
            $scope.ExpandingEntityID = '0';
            $scope.EntityHighlight = true;
            $('#TreeCtrlCss').remove();
            GetTreeCount();
        }
        $scope.$on('OnFullfillmentChangecalender', function (event, calenderID) {
            if ($.cookie('ListofEntityID') == undefined || $.cookie('ListofEntityID') == null || $.cookie('ListofEntityID') == "") {
                $.cookie('ListofEntityID', GetGuidValue($stateParams.TrackID));
            }
            CollectionIds = $.cookie('ListofEntityID').split(",");
            $scope.CurrentTrackingUrl = $stateParams.TrackID;
            $window.ListofEntityID = CollectionIds;
            $scope.ListViewDetails = [{
                data: "",
                PageIndex: 0,
                UniqueID: ""
            }];
            GlobalChildNode = $window.ListofEntityID;
            GlobalChildNode.push(0);
            GetTreeCount();
        });

        function GetTreeCount() {
            $('#EntitiesTree').scrollTop(0);
            $scope.TreeClassList = [];
            var Node = {};
            Node.StartRowNo = 0;
            Node.MaxNoofRow = 20;
            Node.FilterID = $scope.ObjectiveFilterID;
            Node.SortOrderColumn = "null";
            Node.IsDesc = false;
            Node.IncludeChildren = true;
            Node.EntityID = '0';
            Node.Level = $scope.Level;
            Node.ExpandingEntityID = $scope.ExpandingEntityID;
            Node.IDArr = GlobalChildNode;
            Node.FilterAttributes = FilterObj;
            MetadataService.CalenderDetail(Node).then(function (NodeCnt) {
                if (NodeCnt.Response != null && NodeCnt.Response.Data != null) {
                    fnPageTemplate(parseInt(NodeCnt.Response.DataCount));
                    $scope.ParenttreeList = NodeCnt.Response.Data;
                    if ($scope.ParenttreeList != null) {
                        if ($scope.ListViewDetails[0].data == "") {
                            $scope.ListViewDetails[0].data = NodeCnt;
                            $scope.ListViewDetails[0].PageIndex = 0
                            $scope.ListViewDetails[0].UniqueID = ""
                        } else {
                            $scope.ListViewDetails.push({
                                data: NodeCnt,
                                PageIndex: 0,
                                UniqueID: ""
                            });
                        }
                        var TreeItem = "";
                        var CalenderID = 0;
                        var Constructunique = '';
                        for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                            var UniqueId = '';
                            var Iscalender = false;
                            var ExtraSpace = "<i class='icon-'></i>";
                            CalenderID = $scope.ParenttreeList[i]["CalenderID"];
                            if ($scope.ParenttreeList[i]["TypeID"] == 35) {
                                ExtraSpace = "";
                                Iscalender = true;
                                Constructunique = $scope.ParenttreeList[i]["Id"] + '.';
                                $scope.ParenttreeList[i]["UniqueKey"] = $scope.ParenttreeList[i]["Id"].toString();
                                UniqueId = UniqueKEY($scope.ParenttreeList[i]["Id"].toString());
                            } else {
                                UniqueId = UniqueKEY(Constructunique + $scope.ParenttreeList[i]["UniqueKey"]);
                            }
                            var GeneralUniquekey = UniqueKEY($scope.ParenttreeList[i]["class"].toString());
                            var ClassName = GenateClass(GeneralUniquekey);
                            ClassName += " mo" + GeneralUniquekey;
                            TreeItem += "<li  data-CalenderID=" + CalenderID + " data-over='true' class='" + ClassName + "'  data-uniqueKey='" + GeneralUniquekey + "'><a data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-role='Activity' data-iscalender='" + Iscalender + "' data-entityID=" + $scope.ParenttreeList[i]["Id"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> ";
                            TreeItem += ExtraSpace + Space($scope.ParenttreeList[i]["UniqueKey"], $scope.ParenttreeList[i]["TypeID"]);
                            if ($scope.ParenttreeList[i]["TotalChildrenCount"] > 0) {
                                var level = ($scope.Level == 0 ? 2 : $scope.Level);
                                if (GeneralUniquekey.split("-").length <= level) {
                                    TreeItem += "<i data-role='Arrow' data-IsExpanded='true' data-CostCentreID=" + CalenderID + " data-IsConnected='true' data-icon='" + GeneralUniquekey + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-UniqueId=\"" + GeneralUniquekey + "\"></i>";
                                } else {
                                    TreeItem += "<i data-role='Arrow' data-IsExpanded='false' data-CostCentreID=" + CalenderID + " data-IsConnected='false' data-icon='" + GeneralUniquekey + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-UniqueId=\"" + GeneralUniquekey + "\"></i>";
                                }
                            } else {
                                TreeItem += "<i class='icon-'></i>";
                            }
                            TreeItem += " <span style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>";
                            TreeItem += "<span class='treeItemName' data-name='text' data-Calenderidforreport=" + CalenderID + " >" + $scope.ParenttreeList[i]["Name"] + "</span>";
                            TreeItem += "</a> </li>";
                        }
                        var LoadedPage = 0;
                        $scope.$broadcast('onTreePageCreation', [NodeCnt, 0, ""]);
                        $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').attr("data-items", i);
                        $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').append($compile(TreeItem)($scope));
                        $('#EntitiesTree li[data-page=' + LoadedPage + ']').removeClass('pending').addClass('done');
                        if ($scope.EntityHighlight == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $("#EntitiesTree  li[data-CalenderID=" + $scope.CurrentCalenderID + "] a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                            $scope.EntityHighlight = false;
                        }
                        if (SearchEntityID != 0) {
                            SearchEntityID = 0;
                        }
                    }
                } else {
                    $("#GanttDataContainer").html("");
                    $("#GanttBlockContainer").html("");
                    $("#EntitiesTree").html("");
                    $("#ListHolder").html("");
                }
                $scope.disableShowAll = false;
            });
            $(window).AdjustHeightWidth();
        }
        var TreePageLoadTimer = undefined;

        function fnPageTemplate(ItemCnt) {
            $("#EntitiesTree").html("");
            $scope.GanttViewDataPageTemplate = "";
            $scope.GanttViewBlockPageTemplate = "";
            var PageSize = 1;
            var itemsPerPage = 20;
            if (ItemCnt > itemsPerPage) {
                PageSize = ItemCnt / itemsPerPage;
            }
            var TreeTemplate = '';
            var ListTemplate = '';
            var height = 34 * 20;
            for (var i = 0; i < PageSize; i++) {
                TreeTemplate += "<li class='pending' style='min-height: " + height + "px;' data-page='" + i + "'><ul class='nav nav-list nav-list-menu'></ul></li>"
                ListTemplate += "<tbody class='widthcent pending' style='min-height: " + height + "px;' data-page='" + i + "'></tbody>";
                $scope.GanttViewDataPageTemplate += '<tbody class="widthcent"  data-page="' + i + '"><tr><td style="height: ' + (height - 14) + 'px;border-color: #fff;"></td></tr></tbody>';
                $scope.GanttViewBlockPageTemplate += '<div data-page="' + i + '" style="min-height: ' + height + 'px;" class="ganttview-data-blocks-page widthInherit"></div>';
            }
            if (TreeTemplate.length > 0) {
                $scope.GanttViewDataPageTemplate += '<tbody class="blankTreeNode"></tbody>';
                $scope.ListTemplate = ListTemplate;
                $("#EntitiesTree").trigger('onListViewTemplateCreation', [ListTemplate]);
                $("#EntitiesTree").html($compile(TreeTemplate + '<li class="blankTreeNode"></li>')($scope));
            }
        }
        var timer;
        var pageQueue = [];
        $('#EntitiesTree').scroll(function () {
            $("#ListHolder").scrollTop($(this).scrollTop());
            $("#GanttDataContainer").scrollTop($(this).scrollTop());
            clearTimeout(timer);
            timer = setTimeout(function () {
                StartPageLoad(false)
            }, 20);
        });
        $(window).scroll(function () {
            clearTimeout(timer);
            timer = setTimeout(function () {
                StartPageLoad(true)
            }, 20);
        });

        function StartPageLoad(windowScroll) {
            var obj = $('#EntitiesTree');
            var areaHeight = 0;
            var areaTop = 0;
            if (windowScroll) {
                areaHeight = $(window).height();
                areaTop = $(window).scrollTop();
            } else {
                areaHeight = $(obj).height();
                areaTop = $(obj).position().top;
            }
            $('li.pending', obj).each(function () {
                var top = $(this).position().top - areaTop;
                var height = $(this).height();
                if (top + height + height < 0) { } else if (top > areaHeight) { } else {
                    $(this).removeClass('pending').addClass('inprogress');
                    pageQueue.push($(this).attr('data-page'));
                }
            });
        }
        setTimeout(LoadPageContent, 20);

        function LoadPageContent() {
            var PageNo = pageQueue.pop();
            if (PageNo == undefined) {
                setTimeout(LoadPageContent, 20);
            } else {
                var StartRowNo = PageNo * 20;
                var MaxNoofRow = 20;
                TreeLoadPage(PageNo, StartRowNo, MaxNoofRow);
            }
        }

        function UniqueKEY(UniKey) {
            var substr = UniKey.split('.');
            var id = "";
            var row = substr.length;
            if (row > 1) {
                for (var i = 0; i < row; i++) {
                    id += substr[i];
                    if (i != row - 1) {
                        id += "-";
                    }
                }
                return id;
            }
            return UniKey;
        }
        $scope.TreeClassList = [];

        function GenateClass(UniKey) {
            var ToLength = UniKey.lastIndexOf('-');
            if (ToLength == -1) {
                return "";
            }
            var id = UniKey.substring(0, ToLength);
            var afterSplit = id.split('-');
            var result = "";
            for (var i = 0; i < afterSplit.length; i++) {
                result += SplitClss(id, i + 1);
            }
            return result;
        }

        function SplitClss(NewId, lengthofdata) {
            var afterSplit = NewId.split('-');
            var finalresult = " p";
            for (var i = 0; i < lengthofdata; i++) {
                finalresult += afterSplit[i];
                if (i != lengthofdata - 1) {
                    finalresult += "-"
                }
            }
            return finalresult;
        }

        function Space(UniKey, typeID) {
            var substr = UniKey.split('.');
            var itag = "";
            var row = substr.length;
            if (row > 0) {
                for (var i = 1; i < row; i++) {
                    itag += "<i class='icon-'></i>";
                }
            }
            if (typeID != 10) return itag;
            else return "";
        }

        function LoadChildTreeNodes(data, ChildCount, UniqueId, CostCenterId) {
            if (ChildCount > 0) {
                if ($('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isExpanded") == "true") {
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("class", "icon-caret-right");
                    Collapse(UniqueId);
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isExpanded", "false");
                    $('#EntitiesTree').scroll();
                } else {
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("class", "icon-caret-down");
                    Expand(UniqueId);
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isExpanded", "true");
                }
                if ($('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isChildrenPresent") == "true") {
                    $scope.ExpandingEntityID = data;
                    TreeLoad(false, UniqueId);
                    $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isChildrenPresent", "false");
                }
            }
        }

        function TreeLoad(Actvitycreation, IdUnique) {
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var TreeItem = "";
            if (Actvitycreation == false) {
                var parentNode = {};
                parentNode.StartRowNo = StartRowNo;
                parentNode.MaxNoofRow = MaxNoofRow;
                parentNode.FilterID = 0;
                parentNode.SortOrderColumn = "null";
                parentNode.IsDesc = false;
                parentNode.IncludeChildren = false;
                parentNode.EntityID = '0';
                parentNode.Level = $scope.Level;
                parentNode.ExpandingEntityID = $scope.ExpandingEntityID;
                parentNode.IsSingleID = false;
                parentNode.IDArr = GlobalChildNode;
                parentNode.FilterAttributes = AplyFilterObj;
                MetadataService.ActivityDetail(parentNode).then(function (ParentNode) {
                    $scope.ParenttreeList = ParentNode.Response.Data;
                    if ($scope.ParenttreeList != null) {
                        if ($scope.ListViewDetails[0].data == "") {
                            $scope.ListViewDetails[0].data = parentNode;
                            $scope.ListViewDetails[0].PageIndex = 0
                            $scope.ListViewDetails[0].UniqueID = IdUnique
                        } else {
                            $scope.ListViewDetails.push({
                                data: parentNode,
                                PageIndex: 0,
                                UniqueID: IdUnique
                            });
                        }
                        var SubEntityUniquekey = '';
                        var UniqueId = '';
                        for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                            UniqueId = UniqueKEY($scope.ParenttreeList[i]["UniqueKey"]);
                            var ClassName = GenateClass(UniqueId);
                            ClassName += " mo" + UniqueId;
                            SubEntityUniquekey = UniqueId;
                            TreeItem += "<li data-over='true' class='" + ClassName + "'  data-uniqueKey='" + UniqueId + "'><a data-role='Activity' data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-EntityTypeID='" + $scope.ParenttreeList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> ";
                            TreeItem += Space($scope.ParenttreeList[i]["UniqueKey"]);
                            if ($scope.ParenttreeList[i]["TotalChildrenCount"] > 0) {
                                if ($scope.ParenttreeList[i]["ParentID"] == 0) {
                                    TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                                } else {
                                    TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                                }
                            } else {
                                TreeItem += "<i class='icon-'></i>";
                            }
                            TreeItem += " <span data-icon='ActivityIcon' data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-uniqueKey='" + UniqueId + "' style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>";
                            TreeItem += "<span data-name='text'>" + $scope.ParenttreeList[i]["Name"] + "</span>";
                            if ($scope.ParenttreeList[i].Permission == 1 || $scope.ParenttreeList[i].Permission == 2) {
                                TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                            } else if ($scope.ParenttreeList[i].Permission == 3) {
                                var EntityTypesWithPermission = $.grep($scope.ContextMenuRecords.ChildContextData, function (e) {
                                    return e.ParentTypeId == parseInt($scope.ParenttreeList[i]["TypeID"]) && e.EntityTypeAccessIsEnabled == true;
                                })
                                if (EntityTypesWithPermission.length > 0) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                            } else if ($scope.ParenttreeList[i].Permission == 0) { }
                            TreeItem += "</a> </li>";
                        }
                        $('#treeSettings li[data-uniqueKey=' + IdUnique + ']').after($compile(TreeItem)($scope));
                        if (Actvitycreation == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $('#EntitiesTree  li[data-uniqueKey=' + UniqueId + ']').addClass('active');
                            if ($('#EntitiesTree  li[data-uniqueKey=' + IdUnique + '] i').attr('data-isChildrenPresent') == undefined) {
                                $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').prev('i').remove();
                                $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').before($compile('<i class="icon-caret-down" data-uniqueid="' + IdUnique + '" data-Permission="false" data-totalchildrencount="1" data-parentid=' + $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').attr('data-parentid') + ' data-ischildloaded="true" data-ischildrenpresent="false" data-isexpanded="true" data-icon="' + IdUnique + '" data-role="Arrow"></i>')($scope));
                            }
                        }
                        $scope.$broadcast('onTreePageCreation', [parentNode, 0, IdUnique]);
                        if ($scope.EntityHighlight == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                            $scope.EntityHighlight = false;
                        }
                    }
                });
            } else var parentNode = {};
            parentNode.StartRowNo = StartRowNo;
            parentNode.MaxNoofRow = MaxNoofRow;
            parentNode.FilterID = 0;
            parentNode.SortOrderColumn = "null";
            parentNode.IsDesc = false;
            parentNode.IncludeChildren = true;
            parentNode.EntityID = '0';
            parentNode.Level = $scope.Level;
            parentNode.ExpandingEntityID = $scope.ExpandingEntityID;
            parentNode.IDArr = GlobalChildNode;
            parentNode.FilterAttributes = AplyFilterObj;
            MetadataService.GetActivityByID(parentNode).then(function (ParentNode) {
                $scope.ParenttreeList = ParentNode.Response.Data;
                if ($scope.ParenttreeList != null) {
                    if ($scope.ListViewDetails[0].data == "") {
                        $scope.ListViewDetails[0].data = ParentNode;
                        $scope.ListViewDetails[0].PageIndex = 0
                        $scope.ListViewDetails[0].UniqueID = IdUnique
                    } else {
                        $scope.ListViewDetails.push({
                            data: ParentNode,
                            PageIndex: 0,
                            UniqueID: IdUnique
                        });
                    }
                    var SubEntityUniquekey = '';
                    var UniqueId = '';
                    for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                        UniqueId = UniqueKEY($scope.ParenttreeList[i]["UniqueKey"]);
                        var ClassName = GenateClass(UniqueId);
                        ClassName += " mo" + UniqueId;
                        SubEntityUniquekey = UniqueId;
                        TreeItem += "<li data-over='true' class='" + ClassName + "'  data-uniqueKey='" + UniqueId + "'><a data-role='Activity' data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-EntityTypeID='" + $scope.ParenttreeList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> ";
                        TreeItem += Space($scope.ParenttreeList[i]["UniqueKey"]);
                        if ($scope.ParenttreeList[i]["TotalChildrenCount"] > 0) {
                            if ($scope.ParenttreeList[i]["ParentID"] == 0) {
                                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                            } else {
                                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                            }
                        } else {
                            TreeItem += "<i class='icon-'></i>";
                        }
                        TreeItem += " <span data-icon='ActivityIcon' data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-uniqueKey='" + UniqueId + "' style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>";
                        TreeItem += "<span class='treeItemName' data-name='text'>" + $scope.ParenttreeList[i]["Name"] + "</span>";
                        if ($scope.ParenttreeList[i].Permission == 1 || $scope.ParenttreeList[i].Permission == 2) {
                            TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                        } else if ($scope.ParenttreeList[i].Permission == 3) {
                            var EntityTypesWithPermission = $.grep($scope.ContextMenuRecords.ChildContextData, function (e) {
                                return e.ParentTypeId == parseInt($scope.ParenttreeList[i]["TypeID"]) && e.EntityTypeAccessIsEnabled == true;
                            })
                            if (EntityTypesWithPermission.length > 0) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeList[i].Permission + " data-typeID=" + $scope.ParenttreeList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeList[i]["Id"] + "></i>";
                        } else if ($scope.ParenttreeList[i].Permission == 0) { }
                        TreeItem += "</a> </li>";
                    }
                    $('#treeSettings li[data-uniqueKey=' + IdUnique + ']').after($compile(TreeItem)($scope));
                    if (Actvitycreation == true) {
                        $("#EntitiesTree li.active").removeClass('active')
                        $('#EntitiesTree  li[data-uniqueKey=' + UniqueId + ']').addClass('active');
                        if ($('#EntitiesTree  li[data-uniqueKey=' + IdUnique + '] i').attr('data-isChildrenPresent') == undefined) {
                            $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').prev('i').remove();
                            $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').before($compile('<i class="icon-caret-down" data-uniqueid="' + IdUnique + '" data-Permission="false" data-totalchildrencount="1" data-parentid=' + $('#EntitiesTree  span[data-uniqueKey=' + IdUnique + ']').attr('data-parentid') + ' data-ischildloaded="true" data-ischildrenpresent="false" data-isexpanded="true" data-icon="' + IdUnique + '" data-role="Arrow"></i>')($scope));
                        }
                    }
                    $scope.$broadcast('onTreePageCreation', [ParentNode, 0, IdUnique]);
                    if ($scope.EntityHighlight == true) {
                        $("#EntitiesTree li.active").removeClass('active')
                        $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                        $scope.EntityHighlight = false;
                    }
                }
            });
            $(window).AdjustHeightWidth();
        }
        $("#treeSettings").click(function (e) {
            if ($scope.disableShowAll == false) {
                $(window).AdjustHeightWidth();
                var TargetControl = $(e.target);
                $scope.muiiscalender.Iscalender = true;
                $scope.muiiscalender.CalenderID = $(TargetControl).parents('li').attr('data-CalenderID');
                var col = "";
                if (TargetControl[0].tagName == "I" && TargetControl.attr('data-role') == 'Arrow') {
                    LoadChildTreeNodes(TargetControl.attr('data-ParentID'), TargetControl.attr('data-TotalChildrenCount'), TargetControl.attr('data-UniqueId'), TargetControl.attr('data-costcentreid'))
                } else if (TargetControl[0].tagName == "I" && TargetControl.attr('data-role') == 'Overview') {
                    $scope.LoadEntityTitle(TargetControl.attr('data-typeID'));
                    $window.SubscriptionEntityTypeID = TargetControl.find('i[data-role="Overview"]').attr('data-typeid');
                    $window.GlobalUniquekey = TargetControl.parent().attr('data-uniquekey');
                } else if (TargetControl[0].tagName == "A" && TargetControl.attr('data-role') == 'Activity') {
                    if ($("#EntitiesTree li").hasClass('active')) $("#EntitiesTree li.active").removeClass('active');
                    $(TargetControl).parents('li').addClass("active");
                    $scope.CurrentCalenderID = $(TargetControl).parents('li').attr('data-CalenderID');
                    $scope.calenderidforreport = $(TargetControl).children("span").eq(1).attr('data-Calenderidforreport');
                    $scope.EntityShortText = TargetControl.attr('data-shorttext');
                    $scope.EntityColorcode = TargetControl.attr('data-colorcode');
                    $scope.RootLevelEntityName = TargetControl.attr('data-entityname').toString();
                    e.preventDefault();
                    var localScope = $(e.target).scope();
                    localScope.$apply(function () {
                        var id = TargetControl.attr('data-entityID');
                        $window.EntityTypeID = id;
                        $window.SubscriptionEntityTypeID = TargetControl.find('i[data-role="Overview"]').attr('data-typeid');
                        $window.GlobalUniquekey = TargetControl.parent().attr('data-uniquekey');
                        if (TargetControl.attr('data-iscalender') == "true") $location.path('/mui/planningtool/calender/detail/section/' + TargetControl.attr('data-entityID') + '/overview');
                        else if (!localStorage.lasttab) $location.path('/mui/calender/detail/sectionentity/' + TargetControl.attr('data-entityID') + '/overview');
                        else $location.path('/mui/calender/detail/sectionentity/' + TargetControl.attr('data-entityID') + '/' + localStorage.getItem('lasttab'));
                        //$location.path('/mui/planningtool/calender/detail/sectionentity/' + TargetControl.attr('data-entityID'));
                    });
                } else if (TargetControl[0].tagName == "SPAN" && TargetControl.attr('data-name') == 'text') {
                    if ($("#EntitiesTree li").hasClass('active')) $("#EntitiesTree li.active").removeClass('active');
                    $(TargetControl).parents('li').addClass("active");
                    $scope.CurrentCalenderID = $(TargetControl).parents('li').attr('data-CalenderID');
                    $scope.EntityShortText = TargetControl.parent().attr('data-shorttext');
                    $scope.EntityColorcode = TargetControl.parent().attr('data-colorcode');
                    $scope.RootLevelEntityName = TargetControl.parent().attr('data-entityname').toString();
                    $scope.calenderidforreport = $(TargetControl).attr('data-Calenderidforreport');
                    e.preventDefault();
                    var localScope = $(e.target).scope();
                    localScope.$apply(function () {
                        var id = TargetControl.parent().attr('data-entityID');
                        $window.EntityTypeID = id;
                        $window.SubscriptionEntityTypeID = TargetControl.next().attr('data-typeid');
                        $window.GlobalUniquekey = TargetControl.parent().parent().attr('data-uniquekey');
                        if (TargetControl.parent().attr('data-iscalender') == "true") $location.path('/mui/calender/detail/section/' + TargetControl.parent().attr('data-entityID') + '/overview');
                        else if (!localStorage.lasttab) $location.path('/mui/calender/detail/sectionentity/' + TargetControl.parent().attr('data-entityID') + '/overview');
                        else $location.path('/mui/calender/detail/sectionentity/' + TargetControl.parent().attr('data-entityID') + '/' + localStorage.getItem('lasttab'));
                        //$location.path('/mui/planningtool/calender/detail/sectionentity/' + TargetControl.parent().attr('data-entityID'));
                    });
                }
                $(window).AdjustHeightWidth();
            }
        });

        function GenerateCss() {
            var row = $scope.TreeClassList.length;
            var cssStyle = '<style type="text/css" id="TreeCtrlCss">';
            $('#TreeCtrlCss').remove()
            for (var i = 0; i < row; i++) {
                cssStyle += '.' + $scope.TreeClassList[i].trim();
                cssStyle += "{display: none;}";
            }
            $('head').append(cssStyle + "</style>");
        }

        function Collapse(value) {
            if ($scope.TreeClassList.indexOf("p" + value) == -1) {
                $scope.TreeClassList.push("p" + value);
            }
            GenerateCss();
        }

        function Expand(value) {
            var index = $scope.TreeClassList.indexOf("p" + value)
            if (index != -1) {
                $scope.TreeClassList.splice(index, 1);
            }
            GenerateCss();
        }
        $("#EntitiesTree").on("loadobjectivefromfilter", function (event, filterid, ApplyFilterobj) {
            $scope.ListViewDetails = [{
                data: "",
                PageIndex: 0,
                UniqueID: ""
            }];
            $scope.ListTemplate = "";
            $scope.GanttViewDataPageTemplate = '';
            $scope.GanttViewBlockPageTemplate = '';
            if (filterid != undefined) {
                $scope.ObjectiveFilterID = filterid;
            } else {
                $scope.ObjectiveFilterID = 0;
            }
            FilterObj = ApplyFilterobj;
            if ($stateParams.ID != undefined && $stateParams.ID != 0) GlobalChildNode.push($stateParams.ID)
            else GlobalChildNode = $window.ListofEntityID;
            $('#TreeCtrlCss').remove();
            GetTreeCount();
        });

        function TreeLoadPage(PageNo, StartRowNo, MaxNoofRow) {
            if (AplyFilterObj.length > 0) {
                $scope.Level = "100";
            }
            var TreeItem = "";
            var ChildInclude = true;
            var parentNode = {};
            parentNode.StartRowNo = StartRowNo;
            parentNode.MaxNoofRow = MaxNoofRow;
            parentNode.FilterID = FilterID;
            parentNode.SortOrderColumn = "null";
            parentNode.IsDesc = false;
            parentNode.IncludeChildren = ChildInclude;
            parentNode.EntityID = '0';
            parentNode.Level = $scope.Level;
            parentNode.ExpandingEntityID = $scope.ExpandingEntityID;
            parentNode.IsSingleID = false;
            parentNode.IDArr = GlobalChildNode;
            parentNode.FilterAttributes = AplyFilterObj;
            MetadataService.ActivityDetail(parentNode).then(function (ParentNode) {
                $scope.ParenttreeNewList = ParentNode.Response.Data;
                if ($scope.ParenttreeNewList != null) {
                    $scope.ListViewDetails.push({
                        data: ParentNode,
                        PageIndex: PageNo,
                        UniqueID: ""
                    });
                    for (var i = 0; i < $scope.ParenttreeNewList.length; i++) {
                        var UniqueId = UniqueKEY($scope.ParenttreeNewList[i]["UniqueKey"]);
                        var ClassName = GenateClass(UniqueId);
                        ClassName += " mo" + UniqueId;
                        TreeItem += "<li data-over='true' class='" + ClassName + "'  data-uniqueKey='" + UniqueId + "'><a data-role='Activity' data-EntityLevel='" + $scope.ParenttreeNewList[i]["Level"] + "' data-EntityTypeID='" + $scope.ParenttreeNewList[i]["TypeID"] + "'  data-entityID=" + $scope.ParenttreeNewList[i]["Id"] + " data-colorcode=" + $scope.ParenttreeNewList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeNewList[i]["ShortDescription"] + " data-Permission=" + $scope.ParenttreeNewList[i].Permission + " data-entityname=\"" + $scope.ParenttreeNewList[i]["Name"] + "\"> ";
                        TreeItem += Space($scope.ParenttreeNewList[i]["UniqueKey"]);
                        if ($scope.ParenttreeNewList[i]["TotalChildrenCount"] > 0) {
                            if (UniqueId.split("-").length <= parseInt($scope.Level) || $scope.ParenttreeNewList[i]["ParentID"] == 0) {
                                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeNewList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeNewList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeNewList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                            } else {
                                TreeItem += "<i data-role='Arrow' data-icon='" + UniqueId + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeNewList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeNewList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeNewList[i].Permission + " data-UniqueId=\"" + UniqueId + "\"></i>";
                            }
                        } else {
                            TreeItem += "<i class='icon-'></i>";
                        }
                        TreeItem += " <span data-icon='ActivityIcon' data-uniqueKey='" + UniqueId + "' data-ParentID=" + $scope.ParenttreeNewList[i]["Id"] + " style='background-color: #" + $scope.ParenttreeNewList[i]["ColorCode"] + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeNewList[i]["ShortDescription"] + "</span>";
                        TreeItem += " <span class='treeItemName' data-name='text'>" + $scope.ParenttreeNewList[i]["Name"] + "</span>";
                        if ($scope.ParenttreeNewList[i].Permission == 1 || $scope.ParenttreeNewList[i].Permission == 2) {
                            TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeNewList[i].Permission + " data-typeID=" + $scope.ParenttreeNewList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeNewList[i]["Id"] + "></i>";
                        } else if ($scope.ParenttreeNewList[i].Permission == 3) {
                            var EntityTypesWithPermission = $.grep($scope.ContextMenuRecords.ChildContextData, function (e) {
                                return e.ParentTypeId == parseInt($scope.ParenttreeNewList[i]["TypeID"]) && e.EntityTypeAccessIsEnabled == true;
                            })
                            if (EntityTypesWithPermission.length > 0) TreeItem += "<i data-role='Overview' class='icon-reorder menu' data-toggle='modal' data-AccessValue=" + $scope.ParenttreeNewList[i].Permission + " data-typeID=" + $scope.ParenttreeNewList[i]["TypeID"] + " data-entityID=" + $scope.ParenttreeNewList[i]["Id"] + "></i>";
                        } else if ($scope.ParenttreeNewList[i].Permission == 0) { }
                        TreeItem += "</a> </li>";
                    }
                    if (TreeItem.length > 0) {
                        $('#EntitiesTree li[data-page=' + PageNo + '] > ul').html(TreeItem);
                        $('#EntitiesTree li[data-page=' + PageNo + ']').removeClass('inprogress').addClass('done').removeAttr('style');
                        if ($scope.EntityHighlight == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active');
                            $scope.EntityHighlight = false;
                        }
                        $scope.$broadcast('onTreePageCreation', [ParentNode, PageNo, ""]);
                        setTimeout(LoadPageContent, 1);
                    }
                }
                $(window).AdjustHeightWidth();
            });
        };
        $scope.listofreportEntityID = '';
        $scope.ObjectiveListofReportRecords = function () {
            $scope.ReportGenerate = false;
            $scope.ReportGanttGenerate = false;
            $scope.ReportEdit = false;
            $scope.SelectedReport = "";
            $scope.GanttReportDiv = false;
            $scope.CustomReportDiv = false;
            $scope.ReportImageURL = "NoImage.png";
            $scope.reportDescription = "-";
            $scope.showCustomEntityCheck = false;
            $scope.showCustomSubLevels = false;
            $scope.CustomEntityCheck = "";
            $scope.CustomSubLevelsCheck = "";
            ReportService.ShowReports(0, true).then(function (saveviewscomment) {
                if (saveviewscomment.Response != null) {
                    $scope.ReportTypes = saveviewscomment.Response;
                } else { }
            });
        }
        $scope.changeReportType = function () {
            var eidlist = $scope.listofreportEntityID;
            $scope.Reportreset = [];
            $scope.selectvalue = 0;
            $scope.selectvalue = $scope.SelectedReport;
            $scope.ReportImageURL = "NoImage.png";
            $scope.reportDescription = "-";
            $scope.GanttReportDiv = false;
            $scope.CustomReportDiv = false;
            $scope.showCustomEntityCheck = false;
            $scope.showCustomSubLevels = false;
            $scope.CustomEntityCheck = "";
            $scope.CustomSubLevelsCheck = "";
            if ($scope.selectvalue > 0) {
                $scope.ReportEdit = true;
                $scope.ReportGenerate = true;
                $scope.ReportGanttGenerate = false;
                if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                    $scope.CustomReportDiv = true;
                } else {
                    $scope.CustomReportDiv = false;
                }
                ReportService.ShowReports($scope.selectvalue, true).then(function (BindReports) {
                    if (BindReports.Response != null) {
                        $scope.Reportreset = BindReports.Response;
                        var count = BindReports.Response.length;
                        $scope.showCustomEntityCheck = $scope.Reportreset[count - 1].EntityLevel;
                        $scope.showCustomSubLevels = $scope.Reportreset[count - 1].SubLevel;
                        $scope.reportDescription = $scope.Reportreset[count - 1].Description;
                        $scope.ReportValue = $scope.Reportreset[count - 1].OID;
                        $scope.ReportImageURL = $scope.Reportreset[count - 1].Preview;
                        $scope.ReportEdit = "devexpress.reportserver:open?reportId=" + $scope.ReportValue + "&revisionId=0&preview=0";
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_4523.Caption'));
                    }
                });
            } else if ($scope.selectvalue == -100 || $scope.selectvalue == -101) {
                $scope.ReportEdit = false;
                $scope.ReportGenerate = false;
                $scope.ReportGanttGenerate = true;
                $scope.reportDescription = "Gantt View report is generated for the selected entities";
                $scope.ReportImageURL = "GanttReport.png";
                $scope.GanttReportDiv = true;
                $scope.Ganttoption = '';
                $scope.GanttEntityType = '';
                $scope.GanttstartDate = "";
                $scope.Ganttenddate = "";
                $scope.GanttSelectedEntityCheck = "";
                $scope.GanttAllSubLevelsCheck = "";
                $scope.CustomReportDiv = false;
                if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                    $scope.SelectedEntityCheckDiv = true;
                    $scope.SubLevelsCheckDiv = true;
                } else {
                    $scope.SelectedEntityCheckDiv = false;
                    $scope.SubLevelsCheckDiv = false;
                }
                MetadataService.GetEntityTypefromDB().then(function (getEntityTypes) {
                    $scope.GanttEntityTypes = getEntityTypes.Response;
                });
                CommonService.GetAdminSettingselemntnode('ReportSettings', 'GanttViewReport', 0).then(function (getattribtuesResult) {
                    var getattribtues = getattribtuesResult.Response;
                    if ((getattribtues != undefined || getattribtues != null) && (getattribtues.GanttViewReport != undefined || getattribtues.GanttViewReport != null)) {
                        if (getattribtues.GanttViewReport.Attributes.Attribute.length > 0) {
                            $scope.Ganttoptions = getattribtues.GanttViewReport.Attributes.Attribute;
                        } else if (getattribtues.GanttViewReport.Attributes.Attribute.Id != "") {
                            $scope.Ganttoptionsdata = [];
                            $scope.Ganttoptionsdata.push({
                                "Id": getattribtues.GanttViewReport.Attributes.Attribute.Id,
                                "Field": getattribtues.GanttViewReport.Attributes.Attribute.Field,
                                "DisplayName": getattribtues.GanttViewReport.Attributes.Attribute.DisplayName
                            });
                            $scope.Ganttoptions = $scope.Ganttoptionsdata;
                        }
                    }
                });
            } else {
                $scope.ReportEdit = false;
                $scope.ReportGenerate = false;
                $scope.GanttReportDiv = false;
            }
        }
        $scope.changeEntityType = function () {
            if ($scope.GanttEntityType.length > 0) {
                $scope.SelectedEntityCheckDiv = false;
                $scope.SubLevelsCheckDiv = false;
                $scope.GanttSelectedEntityCheck = "";
                $scope.GanttAllSubLevelsCheck = "";
            } else if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                $scope.SelectedEntityCheckDiv = true;
                $scope.SubLevelsCheckDiv = true;
            }
            };
        $scope.ChangeEntitylevelCheck = function () {
            if ($scope.GanttSelectedEntityCheck == true)
            { $scope.GanttAllSubLevelsCheck = false; }
        };
        $scope.ChangeSubLevelsCheck = function () {
            if ($scope.GanttAllSubLevelsCheck == true)
            { $scope.GanttSelectedEntityCheck = false; }
        };
        //$scope.AddDefaultEndDate = function (startdate) {
        //    $("#EntityMetadata").addClass('notvalidate');
       // var startDate = new Date.create(startdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")); if (startDate == null) { $scope.Ganttenddate = null }
      //    else { $scope.Ganttenddate = startDate.addDays(7); }};
        $scope.CustomChangeEntitylevelCheck = function () {
            if ($scope.CustomEntityCheck == true) {
                $scope.CustomSubLevelsCheck = false;
            }
        };
        $scope.CustomChangeSubLevelsCheck = function () {
            if ($scope.CustomSubLevelsCheck == true) {
                $scope.CustomEntityCheck = false;
            }
        };
        $scope.hideReportPopup = function () {
            if ($scope.selectvalue == -100 || $scope.selectvalue == -101) {
                if ($scope.GanttstartDate.toString().length > 0 && $scope.Ganttenddate.toString().length > 0) {
                    var sdate = new Date.create($scope.GanttstartDate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                    var edate = new Date.create($scope.Ganttenddate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                    var diffval = ((parseInt(new Date.create(edate.toString('dd/MM/yyyy')).getTime()) - parseInt((new Date.create(sdate.toString('dd/MM/yyyy')).getTime()))));
                    if (diffval < 0) {
                        $scope.GanttstartDate = "";
                        $scope.Ganttenddate = "";
                        bootbox.alert($translate.instant('LanguageContents.Res_4767.Caption'));
                        return false;
                    }
                } else if ($scope.GanttstartDate.toString().length > 0 || $scope.Ganttenddate.toString().length > 0) {
                    $scope.GanttstartDate = "";
                    $scope.Ganttenddate = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_4578.Caption'));
                    return false;
                }
                var ExpandingEntityID = 0;
                var IncludeChildren = true;
                if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                    ExpandingEntityID = $stateParams.ID;
                }
                if (($scope.ObjectiveFilterID == 0) && (ExpandingEntityID > 0) && ($scope.GanttSelectedEntityCheck == "") && ($scope.GanttAllSubLevelsCheck == "")) {
                    ExpandingEntityID = 0;
                    IncludeChildren = true;
                }
                if (($scope.ObjectiveFilterID > 0) && (ExpandingEntityID > 0) && ($scope.GanttSelectedEntityCheck == "") && ($scope.GanttAllSubLevelsCheck == "")) {
                    ExpandingEntityID = 0;
                    IncludeChildren = false;
                } else if ($scope.ObjectiveFilterID > 0 || ($scope.GanttSelectedEntityCheck == true)) {
                    IncludeChildren = false;
                }
                $("#ReportPageModel").modal("hide");
                $('#loadingReportPageModel').modal("show");
                var GetObjectiveListreposes = [];
                var IsMonthlyGantt = $scope.selectvalue == -100 ? false : true;
                var Node4 = {};
                Node4.FilterID = $scope.ObjectiveFilterID;
                Node4.SortOrderColumn = "null";
                Node4.IsDesc = false;
                Node4.IncludeChildren = IncludeChildren;
                Node4.EntityID = $scope.calenderidforreport;
                Node4.Level = 100;
                Node4.ExpandingEntityID = ExpandingEntityID;
                Node4.IsMonthly = IsMonthlyGantt;
                Node4.IDArr = GlobalChildNode;
                Node4.FilterAttributes = FilterObj;
                Node4.EntityTypeArr = $scope.GanttEntityType;
                Node4.OptionAttributeArr = $scope.Ganttoption;
                Node4.GanttstartDate = $scope.GanttstartDate;
                Node4.Ganttenddate = $scope.Ganttenddate;
                ReportService.CalendarDetailReportSystem(Node4).then(function (GetObjectiveList2) {
                    if (GetObjectiveList2.Response != null) {
                        GetObjectiveListreposes = GetObjectiveList2.Response;
                        var a = document.createElement('a'),
							fileid = GetObjectiveListreposes,
							extn = '.xlsx';
                        var datetimetdy = ConvertDateToString(new Date.create(), $scope.DefaultSettings.DateFormat);
                        var filename = (IsMonthlyGantt == false ? 'Gantt-view-(Quarterly)-' : 'Gantt-view-(Monthly)-') + datetimetdy + extn;
                        a.href = 'DownloadReport.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '';
                        a.download = (IsMonthlyGantt == false ? 'Gantt-view-(Quarterly)-' : 'Gantt-view-(Monthly)-') + datetimetdy;
                        document.body.appendChild(a);
                        $('#loadingReportPageModel').modal("hide");
                        a.click();
                    }
                });
            } else if ($scope.selectvalue > 0) {
                var IDLIST = new Array();
                var ExpandingEntityID = 0;
                var IncludeChildren = true;
                if ($stateParams.ID != undefined && $stateParams.ID != 0) {
                    ExpandingEntityID = $stateParams.ID;
                }
                if (($scope.ObjectiveFilterID == 0) && (ExpandingEntityID > 0) && ($scope.CustomEntityCheck == "") && ($scope.CustomSubLevelsCheck == "")) {
                    ExpandingEntityID = 0;
                    IncludeChildren = true;
                }
                if (($scope.ObjectiveFilterID > 0) && (ExpandingEntityID > 0) && ($scope.CustomEntityCheck == "") && ($scope.CustomSubLevelsCheck == "")) {
                    ExpandingEntityID = 0;
                    IncludeChildren = false;
                } else if ($scope.ObjectiveFilterID > 0 || ($scope.CustomEntityCheck == true)) {
                    IncludeChildren = false;
                }
                $scope.listofreportEntityID = '';
                var Node1 = {};
                Node1.FilterID = $scope.ObjectiveFilterID;
                Node1.SortOrderColumn = "null";
                Node1.IsDesc = false;
                Node1.IncludeChildren = IncludeChildren;
                Node1.EntityID = $scope.calenderidforreport;
                Node1.Level = 100;
                Node1.ExpandingEntityID = ExpandingEntityID;
                Node1.IDArr = GlobalChildNode;
                Node1.FilterAttributes = FilterObj;
                MetadataService.ObjectiveDetailReport(Node1).then(function (GetCostCenterActivityReportList) {
                    if (GetCostCenterActivityReportList.Response != null) {
                        if (GetCostCenterActivityReportList.Response.length > 0) {
                            for (var i = 0; i < GetCostCenterActivityReportList.Response.length; i++) {
                                IDLIST.push(GetCostCenterActivityReportList.Response[i]);
                            }
                        }
                        $scope.listofreportEntityID = IDLIST.join(",");
                        var param = {
                            'ID': $scope.ReportValue,
                            'SID': $scope.listofreportEntityID
                        };
                        var w = 1200;
                        var h = 800
                        var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
                        OpenWindowWithPost("reportviewer.aspx", winprops, "NewFile", param);
                    }
                });
            }
        }

        function OpenWindowWithPost(url, windowoption, name, params) {
            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", url);
            form.setAttribute("target", name);
            for (var i in params) {
                if (params.hasOwnProperty(i)) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = i;
                    input.value = params[i];
                    form.appendChild(input);
                }
            }
            document.body.appendChild(form);
            window.open("report.aspx", name, windowoption);
            form.submit();
            document.body.removeChild(form);
        }
        $scope.ObjectiveExpandtext = $translate.instant('LanguageContents.Res_4939.Caption');
        $scope.ObjectiveExpndClass = 'icon-caret-right';
        $scope.disableShowAll = false;
        $scope.ExpandAll = function () {
            if ($scope.disableShowAll == false) {
                $scope.disableShowAll = true;
                $scope.ListViewDetails = [{
                    data: "",
                    PageIndex: 0,
                    UniqueID: ""
                }];
                $scope.ListTemplate = "";
                $scope.GanttViewDataPageTemplate = '';
                $scope.GanttViewBlockPageTemplate = '';
                $('#TreeCtrlCss').remove();
                if ($scope.ObjectiveExpandtext == $translate.instant('LanguageContents.Res_4939.Caption')) {
                    $scope.ObjectiveExpandtext = $translate.instant('LanguageContents.Res_4940.Caption');
                    $scope.ObjectiveExpndClass = 'icon-caret-down';
                    $scope.Level = "100";
                    $scope.ExpandingEntityID = '0';
                    GetTreeCount();
                } else {
                    $scope.ObjectiveExpandtext = $translate.instant('LanguageContents.Res_4939.Caption');
                    $scope.ObjectiveExpndClass = 'icon-caret-right';
                    $scope.Level = "0";
                    $scope.ExpandingEntityID = '0';
                    GetTreeCount();
                }
                $('.ganttview-data').scrollTop(0);
                $('.list_rightblock').scrollTop(0);
            }
        }
        $scope.SelectedFilterID = 0;
        $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
        $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
        PlanningService.GetFilterSettingsForDetail(10).then(function (filterSettings) {
            $scope.filterValues = filterSettings.Response;
        });
        $scope.DetailFilterCreation = function (event) {
            if ($scope.SelectedFilterID == 0) {
                $('.FilterHolder').slideDown("slow")
                $("#objectivedetailfilter").trigger('ClearScope', $scope.SelectedFilterID);
            } else {
                $("#objectivedetailfilter").trigger('EditFilterSettingsByFilterID', [$scope.SelectedFilterID, 10]);
                $('.FilterHolder').slideDown("slow");
                $scope.showSave = false;
                $scope.showUpdate = true;
            }
        };
        $scope.ApplyFilter = function (filterid, filtername) {
            $scope.SelectedFilterID = filterid;
            if (filterid != 0) {
                $("#Filtersettingdiv").removeClass("btn-group open");
                $("#Filtersettingdiv").addClass("btn-group");
                $('.FilterHolder').slideUp("slow")
                $scope.AddEditFilter = 'Edit filter';
                $scope.appliedfilter = filtername;
            } else {
                $scope.appliedfilter = filtername;
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
                $('.FilterHolder').slideUp("slow");
                $("#objectivedetailfilter").trigger('ClearScope', $scope.SelectedFilterID);
            }
            $("#objectivedetailfilter").trigger('applyplandetailfilter', [filterid, filtername]);
        }
        $("#objectivedetailfilter").on('reloaddetailfilter', function (event, typeId, filtername, filterid) {
            $scope.SelectedFilterID = filterid;
            if (filtername != '') {
                $("#objectivedetailfilter").trigger('EditFilterSettingsByFilterID', [$scope.SelectedFilterID, 10]);
                $scope.appliedfilter = filtername;
                $scope.AddEditFilter = 'Edit filter';
            }
            if (filtername == $translate.instant('LanguageContents.Res_401.Caption')) {
                $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
                $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption');
                $scope.SelectedFilterID = 0;
                $('.FilterHolder').slideUp("slow")
            }
            PlanningService.GetFilterSettingsForDetail(typeId).then(function (filterSettings) {
                $scope.filterValues = filterSettings.Response;
                $scope.atributesRelationList = [];
            });
        });
        PlanningService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
            if (CurrencyListResult.Response != null) {
                $scope.CurrencyFormatsList = CurrencyListResult.Response;
            }
        });
    }
    app.controller("mui.planningtool.calender.detailCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$stateParams', '$window', '$location', 'sharedProperties', '$templateCache', '$cookies', '$translate', 'CommonService', 'ReportService', 'PlanningService', 'MetadataService', muiplanningtoolcalenderdetailCtrl]);
})(angular, app);