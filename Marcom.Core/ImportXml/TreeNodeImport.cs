/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace BrandSystems.Marcom.Core.ImportXml
{

    /// <summary>
    /// Module object for table 'MM_TreeNode'.
    /// </summary>

    public class TreeNodeImport
    {
        #region Member Variables

        protected int _newid;
        protected int _id;
        protected int _nodeid;
        protected int _parentpodeid;
        protected int _level;
        protected string _key;
        protected int _attributeid;
        protected string _caption;
        protected int _sortOrder;
        protected string _colorcode;

        #endregion

        #region Constructors
        public TreeNodeImport() { }

        public TreeNodeImport(int pId, int pNodeid, int pParentnodeid, int pLevel, string pKEY, int pAttributeid, string pCaption, int pSortOrder, string pColorCode, int pNewId)
        {
            this._id = pId;
            this._nodeid = pNodeid;
            this._level = pLevel;
            this._key = pKEY;
            this._parentpodeid = pParentnodeid;
            this._attributeid = pAttributeid;
            this._caption = pCaption;
            this._sortOrder = pSortOrder;
            this._colorcode = pColorCode;
            this._newid = pNewId;
        }

        public TreeNodeImport(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int NewId
        {
            get { return _newid; }
            set { _newid = value; }

        }

        public virtual int NodeID
        {
            get { return _nodeid; }
            set { _nodeid = value; }

        }
        public virtual int ParentNodeID
        {
            get { return _parentpodeid; }
            set { _parentpodeid = value; }

        }
        public virtual int Level
        {
            get { return _level; }
            set { _level = value; }

        }

        public virtual string KEY
        {
            get { return _key; }
            set { _key = value; }

        }

        public virtual string ColorCode
        {
            get { return _colorcode; }
            set { _colorcode = value; }

        }

        public virtual int AttributeID
        {
            get { return _attributeid; }
            set { _attributeid = value; }

        }

        public virtual string Caption
        {
            get { return _caption; }
            set { _caption = value; }

        }

        public virtual int SortOrder
        {
            get { return _sortOrder; }
            set { _sortOrder = value; }

        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            TreeNodeImport castObj = null;
            try
            {
                castObj = (TreeNodeImport)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }


}
