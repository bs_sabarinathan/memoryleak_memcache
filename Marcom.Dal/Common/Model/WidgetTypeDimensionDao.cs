﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Common.Model
{

    /// <summary>
    /// WidgetTypeDimensionDao object for table 'CM_WidgetTypeDimension'.
    /// </summary>
    public partial class WidgetTypeDimensionDao : BaseDao, ICloneable
    {
        #region Member Variables

		protected int _id;
        protected int _WidgetTypeID;
        protected string _DimensionName;

        
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public WidgetTypeDimensionDao() {}

        public WidgetTypeDimensionDao(int pWidgetTypeID,string  pDimensionName)
		{
            this._WidgetTypeID = pWidgetTypeID;
            this._DimensionName = pDimensionName; 
		           
		}

        public WidgetTypeDimensionDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int WidgetTypeID
		{
            get { return _WidgetTypeID; }
            set { _bIsChanged |= (_WidgetTypeID != value); _WidgetTypeID = value; }
			
		}



        public virtual string DimensionName
		{
            get { return _DimensionName; }
			set 
			{
			  if (value != null && value.Length > 100)
                  throw new ArgumentOutOfRangeException("DimensionName", "DimensionName value, cannot contain more than 100 characters");
              _bIsChanged |= (_DimensionName != value);
              _DimensionName = value; 
			}
			
		}
		
	    public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string WidgetTypeDimensionDao = "WidgetTypeDimensionDao";			
			public const string Id = "Id";
            public const string WidgetTypeID = "WidgetTypeID";			
			public const string DimensionName = "DimensionName";			
			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string WidgetTypeDimensionDao = "WidgetTypeDimensionDao";			
			public const string Id = "Id";
            public const string WidgetTypeID = "WidgetTypeID";
            public const string DimensionName = "DimensionName";			
			
		}

		#endregion
		
    }
}
