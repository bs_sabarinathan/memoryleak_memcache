﻿using BrandSystems.Marcom.Core.Access.Interface;
using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IAttachments interface for table 'PM_Tasks'.
    /// </summary>
    public interface ITask
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int EntityId
        {
            get;
            set;

        }

        int MemberID
        {
            get;
            set;

        }

        int StepID
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }

        int TaskStatus
        {
            get;
            set;

        }


        DateTime DueDate
        {
            get;
            set;

        }

        int PredefinedTaskID
        {
            get;
            set;

        }

        int Status { get; set; }
        string TaskOwnerName { get; set; }
        string TaskStatusName { get; set; }
        int Duedates { get; set; }
        dynamic TaskAttachment { get; set; }
        IList<ITaskMember> TaskMembers { get; set; }
        Boolean IsPredefined { get; set; }
        int TaskTypeId { get; set; }
        string TaskTypeName { get; set; }
        string ParentEntityName { get; set; }
        string ParentEntityTypeName { get; set; }
        IList<IFundingRequest> fundrequestTask { get; set; }

        #endregion
    }
}
