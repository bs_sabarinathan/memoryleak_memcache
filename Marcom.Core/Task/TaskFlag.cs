﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task.Interface;

namespace BrandSystems.Marcom.Core.Task
{
    /// <summary>
    /// Entity object for table 'PM_Entity'.
    /// </summary>
    internal class TaskFlag : ITaskFlag
    {
        #region Member Variables

        protected int _id;
        protected string _Caption;
        protected string _ColorCode;
        protected string _description;
        protected int _Sortorder;
        #endregion

        #region Constructors
        public TaskFlag() { }

        public TaskFlag(string pCaption, string pColorCode, string pDescription, int pSortorder)
        {
            this._Caption = pCaption;
            this._ColorCode = pColorCode;
            this._description = pDescription;
            this._Sortorder = pSortorder;
        }

        #endregion

        #region Public Properties

        public  int Id
        {
            get { return _id; }
            set { _id = value; }

        }

     

        public  string Description
        {
            get { return _description; }
            set
            {
                
                _description = value;
            }

        }

        public  string Caption
        {
            get { return _Caption; }
            set
            {
                _Caption = value;
            }

        }

        public  string ColorCode
        {
            get { return _ColorCode; }
            set {_ColorCode = value; }

        }


        public  int SortOder
        {
            get { return _Sortorder; }
            set {  _Sortorder = value; }

        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            TaskFlag castObj = null;
            try
            {
                castObj = (TaskFlag)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}


