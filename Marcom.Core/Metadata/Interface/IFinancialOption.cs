﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface IFinancialOption
    {
        /// <summary>
        /// IFinancialOption interface for table ' [MM_Fin_Option]'.
        /// </summary>

        int ID { get; set; }
        string Caption { get; set; }
        int FinAttributeID { get; set; }
        int SortOrder { get; set; }

    }
}
