﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Dal.Base;
using NHibernate;

namespace BrandSystems.Marcom.Dal.Task
{
   public class TaskRepository:GenericRepository
    {
       public TaskRepository(ISessionFactory sessionFactory): base(sessionFactory)
        {
        }
    }
}
