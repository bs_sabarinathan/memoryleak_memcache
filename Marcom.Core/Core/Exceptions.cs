﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core
{
    public class MarcomAccessDeniedException : Exception
    {
        public MarcomAccessDeniedException()
        {
        }

        public MarcomAccessDeniedException(string message)
            : base(message)
        {
        }

        public MarcomAccessDeniedException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
