﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{
    /// <summary>
    /// FilterSettingsDao object for table 'PM_FilterValues'.
    /// </summary>
  public partial class FiltersettingsValuesDao : ICloneable
    {
       #region Member Variables

        protected int _id;
        protected int _filterid;
        protected int _attributeid;
        protected int _level;
        protected int _value;
        protected int _attributetypeId;
        
        #endregion

        #region Constructors
        public FiltersettingsValuesDao() { }

        public FiltersettingsValuesDao(int pId, int pFilterId, int pAttributeId,int pLevel, int pValue,int pAttributeTypeId)
        {
            this._id = pId;
            this._filterid = pFilterId;
            this._attributeid = pAttributeId;
            this._level = pLevel;
            this._value = pValue;
            this._attributeid = pAttributeTypeId;
        }

        #endregion

        #region Public Properties


        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int FilterId
        {
            get { return _filterid; }
            set { _filterid = value; }

        }

        public virtual int AttributeId
        {
            get { return _attributeid; }
            set { _attributeid = value; }

        }
        public virtual int Level
        {
            get { return _level; }
            set { _level = value; }
        }
        public virtual int Value
        {
            get { return _value; }
            set { _value = value; }

        }
        public virtual int AttributeTypeId
        {
            get { return _attributetypeId; }
            set { _attributetypeId = value; }

        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string Id = "Id";
            public const string FilterId = "FilterId";
            public const string AttributeId = "AttributeId";
            public const string Level = "Level";
            public const string Value = "Value";
            public const string AttributeTypeId = "AttributeTypeId";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string Id = "Id";
            public const string FilterId = "FilterId";
            public const string AttributeId = "AttributeId";
            public const string Level = "Level";
            public const string Value = "Value";
            public const string AttributeTypeId = "AttributeTypeId";
        }

        #endregion
    }
}
