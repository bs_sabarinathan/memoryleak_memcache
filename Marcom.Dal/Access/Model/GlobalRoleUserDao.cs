using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Access.Model
{

	/// <summary>
	/// EntityRoleUserDao object for table 'AM_Entity_Role_User'.
	/// </summary>
	public partial class GlobalRoleUserDao : BaseDao, ICloneable 
	{
		#region Member Variables

        protected int _id;
		protected int _GlobalRoleId;
		protected int _userid;
		protected bool _isinherited;
		protected int _inheritedfromentityid;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion

        //#region Extensibility Method Definitions
        //public override bool Equals(object obj)
        //{
        //    GlobalRoleUserDao toCompare = obj as GlobalRoleUserDao;
        //    if (toCompare == null)
        //    {
        //        return false;
        //    }

           
        //    if (!Object.Equals(this.GlobalRoleId, toCompare.GlobalRoleId))
        //        return false;
        //    if (!Object.Equals(this.Userid, toCompare.Userid))
        //        return false;           

        //    return true;
        //}

        //public override int GetHashCode()
        //{
        //    int hashCode = 13;
            
        //    hashCode = (hashCode * 7) + GlobalRoleId.GetHashCode();
        //    hashCode = (hashCode * 7) + Userid.GetHashCode();
        //    return hashCode;
        //}

        //#endregion
		
		#region Constructors
		public GlobalRoleUserDao() {}

        public GlobalRoleUserDao(int pId, int pGlobalRoleId, int pUserid)
		{

            this._id = pId;
            this._GlobalRoleId = pGlobalRoleId; 
			this._userid = pUserid; 
			
		}
		
		#endregion
		
		#region Public Properties


        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int GlobalRoleId
		{
            get { return _GlobalRoleId; }
            set { _bIsChanged |= (_GlobalRoleId != value); _GlobalRoleId = value; }
			
		}
		
		public virtual int Userid
		{
			get { return _userid; }
			set { _bIsChanged |= (_userid != value); _userid = value; }
			
		}
		
		


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{


            public const string Id = "Id";
            public const string GlobalRoleUserDao = "GlobalRoleUserDao";

            public const string GlobalRoleId = "GlobalRoleId";
            public const string Userid = "Userid";	
				
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{

            public const string Id = "Id";	
            public const string GlobalRoleUserDao = "GlobalRoleUserDao";

            public const string GlobalRoleId = "GlobalRoleId";			
			public const string Userid = "Userid";			
					
		}

		#endregion
		
		
	}
	
}
