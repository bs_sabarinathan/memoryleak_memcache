using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IGanttviewHeaderBar interface for table 'CM_GanttviewHeaderBar'.
    /// </summary>
    public interface IGanttviewHeaderBar
    {
        #region Public Properties


        int Id
        {
            get;
            set;

        }
        
        string Name
        {
            get;
            set;

        }

        string Startdate
        {
            get;
            set;

        }

        string EndDate
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }

        string ColorCode
        {
            get;
            set;

        }

        #endregion
    }
}
