﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Dam.Interface
{
    public interface IAssetlink
    {
        int ID { get; set; }
        int EntityID { get; set; }
        int AssetID { get; set; }
        int TargetEntityID { get; set; }
        int LinkOwnerID { get; set; }
        int FolderID { get; set; }
        int LinkedBy { get; set; }
       
    }
}
