﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BrandSystems.Marcom.Core.Report.Interface;
using BrandSystems.Marcom.Metadata.Interface;
using Marcom.API.Authorization;
using Marcom.API.Models;
using Marcom.API.Services;
using Newtonsoft.Json.Linq;
using Marcom.API.Services.Financials;
using Marcom.API.Models.Financials;
using System.Data;
using Marcom.API.Services.DigitalAssetLibrary;
using BrandSystems.Marcom.Core.DAM.Interface;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Dam.Interface;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace Marcom.API.Controllers
{
    [AuthenticateTokenAttribute]
    public class DigitalassetlibraryV1Controller : ApiController
    {
        #region MemberVariables

        private DigitalAssetRepositoryV1 DigitalRepository;

        #endregion

        #region ConstructorLogic
        /// <summary>
        /// Constructer logic
        /// </summary>
        public DigitalassetlibraryV1Controller()
        {
            this.DigitalRepository = new DigitalAssetRepositoryV1();
        }

        #endregion

        #region  API-Actions

        /// <summary>
        /// This will get the task of this user.
        /// </summary>
        [HttpGet]
        public List<object> GetDAMViewSettings()
        {
            try
            {
                return DigitalRepository.GetDAMViewSettings();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// This will get the task of this user.
        /// </summary>
        [HttpPost]
        public string GetEntityDamFolder(JObject jobj)
        {
            try
            {
                int EntityID = (int)jobj["EntityID"];
                return DigitalRepository.GetEntityDamFolder(EntityID);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public List<object> GetAssets(JObject jobj)
        {
            try
            {

                return DigitalRepository.GetAssets(Convert.ToInt32(jobj["folderid"]), (int)jobj["entityID"], (int)jobj["viewType"], (int)jobj["orderbyid"], (int)jobj["pageNo"], (bool)jobj["ViewAll"]);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public string CheckPreviewGenerator(JObject jobj)
        {
            try
            {

                return DigitalRepository.CheckPreviewGenerator((string)jobj["assetids"]);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public IAssets GetAssetAttributesDetails(JObject jobj)
        {
            try
            {

                return DigitalRepository.GetAssetAttributesDetails((int)jobj["assetId"]);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        [HttpPost]
        public int PostEntityPeriod(JObject jobj)
        {
            try
            {

                int Result = DigitalRepository.PostEntityPeriod(jobj);
                return Result;
            }
            catch
            {
                return -99;
            }
        }
        [HttpPost]
        public List<object> GetDAMToolTipSettings(JObject jobj)
        {
            try
            {
                List<object> Result = DigitalRepository.GetDAMToolTipSettings();
                return Result;
            }
            catch
            {
                return null;
            }
        }
        [HttpPost]
        public int CreateAsset(JObject jobj)
        {
            int assetid = 0;
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.ImpersonateSession[(int)jobj["ImpersonateUID"]]);
                JArray saveObj = (JArray)jobj["saveAsset"];
                JArray arrperiods = (JArray)saveObj[0]["Periods"];
                JArray arrobjAttibutevalues = (JArray)saveObj[0]["AttributeData"];
                JArray arrentityamountcurrencytype = (JArray)saveObj[0]["Entityamountcurrencytype"];
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {
                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                        }
                        else
                        {
                            if (entityAttr.ID != (int)SystemDefinedAttributes.CreationDate)
                            {
                                if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                {
                                    entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Date)
                                {
                                    entityAttr.Value = (objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Integer)
                                {
                                    entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                }
                                else
                                    entityAttr.Value = objAttr["NodeID"].ToString();
                            }
                            else
                            {
                                entityAttr.Value = DateTime.UtcNow.ToString("yyyy-MM-dd");
                            }
                        }
                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }
                IList<IDamPeriod> listEntityperiods = new List<IDamPeriod>();
                if (arrperiods != null)
                {
                    foreach (var arrp in arrperiods)
                    {
                        //if (entityPeriodObj.Count() > 0)
                        //{
                        //    foreach (var arrp in entityPeriodObj)
                        //    {
                        DateTime startDate = new DateTime();
                        DateTime endDate = new DateTime();
                        if (DateTime.TryParse(arrp["startDate"].ToString(), out startDate) && DateTime.TryParse(arrp["endDate"].ToString(), out endDate))
                        {

                            IDamPeriod enti = marcomManager.DigitalAssetManager.Damperiodservice();
                            enti.Startdate = startDate;
                            enti.EndDate = endDate;
                            enti.Description = (string)arrp["comment"];
                            enti.SortOrder = (int)arrp["sortorder"];
                            listEntityperiods.Add(enti);
                        }
                        //    }
                        //}
                    }
                }
                else if (arrperiods == null)
                {
                    listEntityperiods = null;
                }
                IList<IAssetAmountCurrencyType> listentityamountcurrencytype = new List<IAssetAmountCurrencyType>();
                if (arrentityamountcurrencytype != null)
                {
                    if (arrentityamountcurrencytype.Count() > 0)
                    {
                        foreach (var item in arrentityamountcurrencytype)
                        {
                            IAssetAmountCurrencyType entamcur = marcomManager.DigitalAssetManager.AssetAmountCurrencyTypeservice();
                            entamcur.Amount = (decimal)item["amount"];
                            entamcur.Currencytypeid = (int)item["currencytype"];
                            entamcur.Attributeid = (int)item["Attributeid"];
                            listentityamountcurrencytype.Add(entamcur);
                        }
                    }

                }
                else if (arrentityamountcurrencytype == null)
                {
                    arrentityamountcurrencytype = null;
                }
                //saveObj[0]["Typeid"].ToString()
                var typeid = int.Parse(saveObj[0]["Typeid"].ToString());
                var Name = saveObj[0]["Name"].ToString();
                var FileName = saveObj[0]["FileName"].ToString();
                var VersionNo = int.Parse(saveObj[0]["VersionNo"].ToString());
                var MimeType = saveObj[0]["MimeType"].ToString();
                var Extension = saveObj[0]["Extension"].ToString();
                var Size = int.Parse(saveObj[0]["Size"].ToString());
                var Status = int.Parse(saveObj[0]["Status"].ToString());
                var EntityID = int.Parse(saveObj[0]["EntityID"].ToString());
                var FileGuid = saveObj[0]["FileGuid"].ToString();
                var Description = saveObj[0]["Description"].ToString();
                var Active = bool.Parse(saveObj[0]["Active"].ToString());
                var FolderID = 0;
                if (saveObj[0]["FolderID"] != null)
                    FolderID = Convert.ToInt32(saveObj[0]["FolderID"].ToString());
                assetid = DigitalRepository.CreateAsset(FolderID, typeid, Name, listattributevalues, listEntityperiods, FileName, VersionNo, MimeType, Extension, Size, EntityID, FileGuid, Description, false, 0, 0, null, null, 0, false, 0, false, null, false, listentityamountcurrencytype, false);
                //result.Response = marcomManager.DigitalAssetManager.CreateAsset(FolderID, typeid, Name, listattributevalues, FileName, VersionNo, MimeType, Extension, Size, EntityID, FileGuid, Description);
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("CreateAsset method hits exception  as '" + ex + "' with key " + ((string[])(Request.Headers.GetValues("sessioncookie")))[0], BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                assetid = 0;
            }
            return assetid;
        }
        [HttpPost]
        public bool CreateNewAssetWithFolder(JObject jObj)
        {
            bool isupdated = false;
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.ImpersonateSession[(int)jObj["ImpersonateUID"]]);
                JObject saveObj = (JObject)jObj["SaveAsset"];
                JArray AssetObj = (JArray)saveObj["Assets"];
                int EntityID = (int)saveObj["EntityID"];
                isupdated = DigitalRepository.CreateNewAssetWithFolder(EntityID, AssetObj, (int)jObj["ImpersonateUID"]);
            }
            catch (Exception ex)
            {
                isupdated = false;
            }
            return isupdated;
        }
        [HttpPost]
        public Tuple<IList, IList> GetDAMExtensions()
        {
            try
            {
                return DigitalRepository.GetDAMExtensions();
            }
            catch
            {
                return null;
            }
        }
        [HttpPost]
        public string S3Settings()
        {
            string res = "";
            try
            {
                res = DigitalRepository.S3Settings();
            }
            catch (Exception ex)
            {
                res = "";
            }
            return res;
        }


        [HttpPost]
        public string GetAssetCategoryTree(JObject jObj)
        {
            string res = "";
            try
            {
                res = DigitalRepository.GetAssetCategoryTree(Convert.ToInt32(jObj["AssetTypeID"]));
            }
            catch
            {
                res = "";
            }
            return res;
        }

        [HttpPost]
        public IList<IEntityTypeAttributeRelationwithLevels> GetDamAttributeRelation(JObject jObj)
        {
            try
            {
                return DigitalRepository.GetDamAttributeRelation(Convert.ToInt32(jObj["damID"]));
            }
            catch
            {
                return null;
            }
        }
        [HttpPost]
        public int CreateBlankAsset(JObject jobj)
        {
            int res = 0;
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.ImpersonateSession[(int)jobj["ImpersonateUID"]]);

                JArray saveObj = (JArray)jobj["saveAsset"];
                JArray arrperiods = (JArray)saveObj[0]["Periods"];
                JArray arrobjAttibutevalues = (JArray)saveObj[0]["AttributeData"];
                JArray arrentityamountcurrencytype = (JArray)saveObj[0]["Entityamountcurrencytype"];
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {
                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                        }
                        else
                        {
                            if (entityAttr.ID != (int)SystemDefinedAttributes.CreationDate)
                            {
                                if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                {
                                    entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Date)
                                {
                                    entityAttr.Value = (objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Integer)
                                {
                                    entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                }
                                else
                                    entityAttr.Value = objAttr["NodeID"].ToString();
                            }
                            else
                            {
                                entityAttr.Value = DateTime.UtcNow.ToString("yyyy-MM-dd");
                            }
                        }
                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }
                IList<IDamPeriod> listEntityperiods = new List<IDamPeriod>();
                if (arrperiods != null)
                {
                    foreach (var arrp in arrperiods)
                    {
                        DateTime startDate = new DateTime();
                        DateTime endDate = new DateTime();
                        if (DateTime.TryParse(arrp["startDate"].ToString(), out startDate) && DateTime.TryParse(arrp["endDate"].ToString(), out endDate))
                        {

                            IDamPeriod enti = marcomManager.DigitalAssetManager.Damperiodservice();
                            enti.Startdate = startDate;
                            enti.EndDate = endDate;
                            enti.Description = (string)arrp["comment"];
                            enti.SortOrder = (int)arrp["sortorder"];
                            listEntityperiods.Add(enti);
                        }
                    }
                }
                else if (arrperiods == null)
                {
                    listEntityperiods = null;
                }
                IList<IAssetAmountCurrencyType> listentityamountcurrencytype = new List<IAssetAmountCurrencyType>();
                if (arrentityamountcurrencytype != null)
                {
                    if (arrentityamountcurrencytype.Count() > 0)
                    {
                        foreach (var item in arrentityamountcurrencytype)
                        {
                            IAssetAmountCurrencyType entamcur = marcomManager.DigitalAssetManager.AssetAmountCurrencyTypeservice();
                            entamcur.Amount = (decimal)item["amount"];
                            entamcur.Currencytypeid = (int)item["currencytype"];
                            entamcur.Attributeid = (int)item["Attributeid"];
                            listentityamountcurrencytype.Add(entamcur);
                        }
                    }

                }
                else if (arrentityamountcurrencytype == null)
                {
                    arrentityamountcurrencytype = null;
                }
                //saveObj[0]["Typeid"].ToString()
                var typeid = int.Parse(saveObj[0]["Typeid"].ToString());
                var Name = saveObj[0]["Name"].ToString();
                var Status = int.Parse(saveObj[0]["Status"].ToString());
                var EntityID = int.Parse(saveObj[0]["EntityID"].ToString());
                var Active = bool.Parse(saveObj[0]["Active"].ToString());
                var FolderID = 0;
                if (saveObj[0]["FolderID"] != null)
                    FolderID = int.Parse(saveObj[0]["FolderID"].ToString());
                var Category = int.Parse(saveObj[0]["Category"].ToString());
                var url = saveObj[0]["Url"].ToString();
                res = DigitalRepository.CreateBlankAsset(FolderID, typeid, Name, listattributevalues, EntityID, Category, listEntityperiods, url, false, 0, null, 0, listentityamountcurrencytype);

            }
            catch
            {
                res = 0;
            }
            return res;
        }
        [HttpPost]
        public List<Tuple<string, string>> savemultipleUploadedImg(JObject jobj)
        {
            try
            {
                return DigitalRepository.savemultipleUploadedImg(jobj);
            }
            catch
            {
                return null;
            }
        }
        [HttpPost]
        public bool writeUploadErrorlog(JObject jObj)
        {
            bool res = false;
            try
            {
                res = DigitalRepository.writeUploadErrorlog(jObj);
            }
            catch (Exception ex)
            {
                res = false;
            }
            return res;
        }

        [HttpPost]
        public List<object> GetAssetCategoryPathInAssetEdit(JObject jobj)
        {
            try
            {
                List<object> Result = DigitalRepository.GetAssetCategoryPathInAssetEdit(Convert.ToInt32(jobj["AssetTypeID"]));
                return Result;
            }
            catch
            {
                return null;
            }
        }

        #endregion
    }
}