﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.User.Interface
{
    public interface IApprovalRole
    {
        #region Public Properties

        int ID
        {
            get;
            set;
        }
        string Caption
        {
            get;
            set;

        }
        string Description
        {
            get;
            set;

        }

        #endregion
    }
}
