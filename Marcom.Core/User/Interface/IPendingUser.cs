using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.User.Interface
{
    /// <summary>
    /// IUser interface for table 'UM_User'.
    /// </summary>
    public interface IPendingUser
    {
        #region Public Properties
                 
        int Id
        {
            get;
            set;                                                      

        }

        string FirstName
        {
            get;
            set;

        }

        string LastName
        {
            get;
            set;

        }

        string UserName
        {
            get;
            set;

        }

        byte[] Password
        {
            get;
            set;

        }

        string SaltPassword
        {
            get;
            set;

        }

        string Email
        {
            get;
            set;

        }

      

        string Language
        {
            get;
            set;

        }

        string TimeZone
        {
            get;
            set;

        }

    


        bool Gender
        {
            get;
            set;

        }

        DateTime DOB
        {
            get;
            set;

        }

        string Phone
        {
            get;
            set;

        }

        string Address
        {
            get;
            set;

        }

        string City
        {
            get;
            set;

        }

        int ZipCode
        {
            get;
            set;

        }

        string Website
        {
            get;
            set;

        }

    
        string ActivationStatus
        {
            get;
            set;
        }

        string CompanyName
        {
            get;
            set;

        }


        string Partners
        {
            get;
            set;
        }
        string Title
        {
            get;
            set;

        }


        string Department
        {
            get;
            set;
        }
        #endregion
    }
}
