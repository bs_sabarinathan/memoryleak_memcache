﻿using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.AmazonStorageHelper;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Utility;
using BrandSystems.Marcom.Dal.Planning.Model;
using FastMember;
using Marcom.API.Authorization;
using Marcom.API.Models.Financials;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Xml.Linq;

namespace Marcom.API.Services.Financials
{
    public class FinancialsRepositoryV1
    {
        enum POSysAttributes
        {
            PONumber = 5,
            amount = 6,
            CreateDate = 7,
            SupplierName = 8,
            Description = 9,
            RequesterName = 10,
            ApprovedDate = 11,
            SentDate = 12,
            ApproverName = 13
        };

        enum SpentSysAttributes
        {
            InvoiceNumber = 15,
            PaymentDate = 16,
            POID = 17,
            SupplierID = 18,
            Description = 19,
            amount = 20
        };

        enum FinTypeID
        {
            PurchaseOrder = 1,
            Spent = 2,
            FinancialMeta = 3
        }

        public DataTable GetAttributes(int TypeID)
        {
            try
            {
                DataTable dt = new DataTable();

                var sqlstr = new StringBuilder();
                switch (TypeID)
                {
                    case 1: //PurchaseOrder
                        sqlstr.Append("SELECT mmf.ID  AS attributeID, mmf.Caption AS attributename, mmf.IsSystemDefined,");
                        sqlstr.Append("CASE mmf.Caption ");
                        sqlstr.Append("WHEN 'Order no.' THEN 'PONumber' ");
                        sqlstr.Append("WHEN 'Amount' THEN 'amount' ");
                        sqlstr.Append("WHEN 'Created on' THEN 'CreateDate' ");
                        sqlstr.Append("WHEN 'Supplier' THEN 'SupplierName' ");
                        sqlstr.Append("WHEN 'Requested by' THEN 'RequesterName' ");
                        sqlstr.Append("WHEN 'Approved date' THEN 'ApprovedDate' ");
                        sqlstr.Append("WHEN 'Send date' THEN 'SentDate' ");
                        sqlstr.Append("WHEN 'Approved by' THEN 'ApproverName' ");
                        sqlstr.Append("ELSE mmf.Caption END AS enumnames, ");
                        sqlstr.Append("mmf.AttributeTypeID AS AttributeTypeID ");
                        sqlstr.Append("FROM MM_Fin_Attribute mmf WHERE mmf.FinTypeID = 1");
                        break;
                    case 2: //Spent
                        sqlstr.Append("SELECT mmf.ID  AS attributeID, mmf.Caption AS attributename, mmf.IsSystemDefined, ");
                        sqlstr.Append("CASE mmf.Caption ");
                        sqlstr.Append("WHEN 'Invoice No' THEN 'InvoiceNumber' ");
                        sqlstr.Append("WHEN 'Transaction Date' THEN 'PaymentDate' ");
                        sqlstr.Append("WHEN 'PO' THEN 'POID' ");
                        sqlstr.Append("WHEN 'Supllier ID' THEN 'SupplierID' ");
                        sqlstr.Append("WHEN 'Amount' THEN 'amount' ");
                        sqlstr.Append("ELSE mmf.Caption END AS enumnames, ");
                        sqlstr.Append("mmf.AttributeTypeID AS AttributeTypeID ");
                        sqlstr.Append("FROM MM_Fin_Attribute mmf WHERE mmf.FinTypeID = 2");
                        break;
                    case 3: //FinancialMeta
                        sqlstr.Append("SELECT mmf.ID  AS attributeID, mmf.Caption AS attributename, mmf.IsSystemDefined,");
                        sqlstr.Append("CASE mmf.Caption ");
                        sqlstr.Append("WHEN 'Planned' THEN 'PlannedAmount' ");
                        sqlstr.Append("WHEN 'In requests' THEN 'InRequest' ");
                        sqlstr.Append("WHEN 'Approved allocations' THEN 'ApprovedAllocatedAmount' ");
                        sqlstr.Append("WHEN 'Name' THEN 'CostCenterName' ");
                        sqlstr.Append("ELSE mmf.Caption END AS enumnames, ");
                        sqlstr.Append("mmf.AttributeTypeID AS AttributeTypeID ");
                        sqlstr.Append("FROM MM_Fin_Attribute mmf WHERE mmf.FinTypeID = 3");
                        break;
                    default:
                        break;
                }

                string connectionstr = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connectionstr))
                {
                    SqlCommand sqlCommand = new SqlCommand();
                    SqlDataAdapter dadapter = new SqlDataAdapter();

                    sqlCommand.CommandText = sqlstr.ToString();
                    sqlCommand.Connection = conn;
                    conn.Open();
                    dadapter.SelectCommand = sqlCommand;
                    dadapter.Fill(dt);

                }

                return dt;
            }
            catch (Exception e)
            {
                APILog.Logging.LogException(e, "***API Error******Error in GetAttributes: ");
                throw e;
            }

        }


        //public JObject ConvertCurrency(JObject curr)
        //{
        //    IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
        //    var convertedcurrencies = marcommanager.CommonManager.getCurrencyconverterData(;
        //    return convertedcurrencies;
        //}

        public dynamic GetPODetailsByEntity(int ID)
        {
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
                IList<BrandSystems.Marcom.Core.Planning.Interface.IPurchaseOrder> iPurOrder = new List<BrandSystems.Marcom.Core.Planning.Interface.IPurchaseOrder>();
                var obj = marcommanager.PlanningManager.GetAllPurchaseOrdersByEntityID(ID);
                iPurOrder = obj.Item1;

                DataTable dtPOAttributes = new DataTable();
                dtPOAttributes = GetAttributes(1);
                dtPOAttributes.TableName = "attributes";
                DataTable dtPODetails = new DataTable("PODetails");

                using (var reader = ObjectReader.Create(iPurOrder))
                {
                    dtPODetails.Load(reader);
                }

                List<string> deletecollst = new List<string>();
                //renaming to attribute ids and delete unwanted columns
                foreach (DataColumn colPODetails in dtPODetails.Columns)
                {
                    DataColumnCollection colAttributes = dtPOAttributes.Columns;
                    var LinqResult = from p in dtPOAttributes.AsEnumerable()
                                     where p.Field<string>("enumnames") == colPODetails.ColumnName
                                     select new
                                     {
                                         attributename = p.Field<string>("attributename"),
                                         attrID = p.Field<int>("attributeID")
                                     };

                    if (!LinqResult.Any())
                    {
                        deletecollst.Add(colPODetails.ColumnName.ToString()); //delete unwanted columns
                    }
                    {
                        foreach (var res in LinqResult)
                        {
                            colPODetails.ColumnName = res.attrID.ToString();  //rename the columns to attribute ids
                        }
                    }
                }
                deletecollst.Remove("Entityid");
                deletecollst.Remove("CostCentreID");

                //delete unwanted columns
                foreach (var col in deletecollst)
                {
                    dtPODetails.Columns.Remove(col);
                }

                //add dynamic attributes
                var dynAttributes = from p in dtPOAttributes.AsEnumerable()
                                    where p.Field<bool>("IsSystemDefined") == false
                                    select new
                                    {
                                        attributename = p.Field<string>("attributename"),
                                        attrID = p.Field<int>("attributeID")
                                    };
                foreach (var dynattr in dynAttributes)
                {
                    dtPODetails.Columns.Add(dynattr.attrID.ToString());
                }
                int rowindex = 0;
                foreach (var ipo in iPurOrder)
                {
                    foreach (var idynattr in ipo.DynamciData)
                    {
                        dtPODetails.Rows[rowindex][idynattr.ID.ToString()] = idynattr.values.ToString();
                    }
                    rowindex++;
                }

                dtPOAttributes.Columns.Remove("enumnames");
                dtPOAttributes.Columns.Remove("AttributeTypeID");
                DataSet ds = new DataSet("PurchaseOrder");
                ds.Tables.Add(dtPOAttributes);
                ds.Tables.Add(dtPODetails);

                string json = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var dynamicObject = Json.Decode(json);
                return dynamicObject;
            }
            catch (Exception e)
            {
                APILog.Logging.LogException(e, "***API Error******Error in GetPODetailsByEntity: ");
                throw e;
            }

        }

        public bool CreatePO(JObject POs)
        {
            try
            {

                JArray item_array = (JArray)POs["PurchaseOrders"];
                var objects = JArray.Parse(item_array.ToString());
                IPurchaseOrder iPurOrder = new BrandSystems.Marcom.Core.Planning.PurchaseOrder();

                DataTable dtPOAttributes = new DataTable();
                dtPOAttributes = GetAttributes(1);
                dtPOAttributes.TableName = "attributes";

                foreach (JObject root in objects)
                {
                    Dictionary<string, string> jattributes = new Dictionary<string, string>();

                    foreach (KeyValuePair<String, JToken> tag in root)
                    {
                        jattributes.Add(tag.Key.ToString(), tag.Value.ToString());
                    }

                    iPurOrder.PONumber = jattributes["5"]; jattributes.Remove("5");

                    if (jattributes.ContainsKey("6"))
                    {
                        iPurOrder.amount = Convert.ToDecimal(jattributes["6"]); jattributes.Remove("6");
                    }

                    if (jattributes.ContainsKey("7"))
                    {
                        iPurOrder.CreateDate = DateTime.Parse(jattributes["7"]); jattributes.Remove("7");
                    }
                    if (jattributes.ContainsKey("8"))
                    {
                        iPurOrder.SupplierName = jattributes["8"]; jattributes.Remove("8");
                    }
                    if (jattributes.ContainsKey("9"))
                    {
                        iPurOrder.Description = jattributes["9"]; jattributes.Remove("9");
                    }
                    if (jattributes.ContainsKey("10"))
                    {
                        iPurOrder.RequesterName = jattributes["10"]; jattributes.Remove("10");
                    }
                    if (jattributes.ContainsKey("11"))
                    {
                        iPurOrder.ApprovedDate = DateTime.Parse(jattributes["11"]); jattributes.Remove("11");
                    }
                    if (jattributes.ContainsKey("12"))
                    {
                        iPurOrder.SentDate = DateTime.Parse(jattributes["12"]); jattributes.Remove("12");
                    }
                    if (jattributes.ContainsKey("13"))
                    {
                        iPurOrder.ApproverName = jattributes["13"]; jattributes.Remove("13");
                    }

                    int EntityID = int.Parse(jattributes["EntityID"]); jattributes.Remove("EntityID");
                    int CostCentreID = int.Parse(jattributes["CostCentreID"]); jattributes.Remove("CostCentreID");

                    IList<IAttributeData> MetadataValues = new List<IAttributeData>();
                    foreach (var jattr in jattributes)
                    {
                        IAttributeData dynamicattr = new BrandSystems.Marcom.Core.Planning.AttributeData();
                        int attributetypeid = (from DataRow dr in dtPOAttributes.Rows
                                               where (int)dr["attributeID"] == int.Parse(jattr.Key)
                                               select (int)dr["AttributeTypeID"]).FirstOrDefault();
                        dynamicattr.TypeID = attributetypeid;
                        dynamicattr.ID = int.Parse(jattr.Key);
                        dynamicattr.Value = jattr.Value.ToString();
                        MetadataValues.Add(dynamicattr);
                    }

                    IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
                    iPurOrder.UserID = marcommanager.User.Id;
                    int obj = marcommanager.PlanningManager.InsertUpdatePO(EntityID, CostCentreID, iPurOrder, MetadataValues);
                    return true;
                }
                return true;

            }
            catch (MarcomAccessDeniedException ex)
            {
                throw new UnauthorizedAccessException("UnauthorizedAccessException");
            }
            catch (Exception e)
            {
                APILog.Logging.LogException(e, "***API Error******Error in CreatePO: ");
                return false;
            }

        }

        public bool UpdatePO(JObject POs)
        {
            try
            {

                JArray item_array = (JArray)POs["PurchaseOrders"];
                var objects = JArray.Parse(item_array.ToString());
                IPurchaseOrder iPurOrder = new BrandSystems.Marcom.Core.Planning.PurchaseOrder();

                DataTable dtPOAttributes = new DataTable();
                dtPOAttributes = GetAttributes(1);
                dtPOAttributes.TableName = "attributes";
                ArrayList updateattributeids = new ArrayList();
                foreach (JObject root in objects)
                {
                    Dictionary<string, string> jattributes = new Dictionary<string, string>();

                    foreach (KeyValuePair<String, JToken> tag in root)
                    {
                        jattributes.Add(tag.Key.ToString(), tag.Value.ToString());
                    }

                    iPurOrder.PONumber = jattributes["5"]; jattributes.Remove("5");
                    if (jattributes.ContainsKey("6"))
                    {
                        updateattributeids.Add("6");
                        iPurOrder.amount = Convert.ToDecimal(jattributes["6"]); jattributes.Remove("6");
                    }
                    if (jattributes.ContainsKey("7"))
                    {
                        updateattributeids.Add("7");
                        iPurOrder.CreateDate = DateTime.Parse(jattributes["7"]); jattributes.Remove("7");
                    }
                    if (jattributes.ContainsKey("8"))
                    {
                        updateattributeids.Add("8");
                        iPurOrder.SupplierName = jattributes["8"]; jattributes.Remove("8");
                    }
                    if (jattributes.ContainsKey("9"))
                    {
                        updateattributeids.Add("9");
                        iPurOrder.Description = jattributes["9"]; jattributes.Remove("9");
                    }
                    if (jattributes.ContainsKey("10"))
                    {
                        updateattributeids.Add("10");
                        iPurOrder.RequesterName = jattributes["10"]; jattributes.Remove("10");
                    }
                    if (jattributes.ContainsKey("11"))
                    {
                        updateattributeids.Add("11");
                        iPurOrder.ApprovedDate = DateTime.Parse(jattributes["11"]); jattributes.Remove("11");
                    }
                    if (jattributes.ContainsKey("12"))
                    {
                        updateattributeids.Add("12");
                        iPurOrder.SentDate = DateTime.Parse(jattributes["12"]); jattributes.Remove("12");
                    }
                    if (jattributes.ContainsKey("13"))
                    {
                        updateattributeids.Add("13");
                        iPurOrder.ApproverName = jattributes["13"]; jattributes.Remove("13");
                    }

                    int EntityID = int.Parse(jattributes["EntityID"]); jattributes.Remove("EntityID");
                    int CostCentreID = int.Parse(jattributes["CostCentreID"]); jattributes.Remove("CostCentreID");

                    IList<IAttributeData> MetadataValues = new List<IAttributeData>();
                    foreach (var jattr in jattributes)
                    {
                        IAttributeData dynamicattr = new BrandSystems.Marcom.Core.Planning.AttributeData();
                        int attributetypeid = (from DataRow dr in dtPOAttributes.Rows
                                               where (int)dr["attributeID"] == int.Parse(jattr.Key)
                                               select (int)dr["AttributeTypeID"]).FirstOrDefault();
                        dynamicattr.TypeID = attributetypeid;
                        dynamicattr.ID = int.Parse(jattr.Key);
                        dynamicattr.Value = jattr.Value.ToString();
                        MetadataValues.Add(dynamicattr);
                        updateattributeids.Add(jattr.Key);
                    }

                    IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
                    iPurOrder.UserID = marcommanager.User.Id;
                    int obj = marcommanager.PlanningManager.UpdateApiPO(EntityID, CostCentreID, updateattributeids, iPurOrder, MetadataValues);
                    if ((obj == -99) || (obj == 0))
                    {
                        Marcom.API.APILogModel.ApiLogHandler.LogInfo("could not update PO: " + iPurOrder.PONumber, Marcom.API.APILogModel.ApiLogHandler.LogType.Notify);
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                return true;
            }
            catch (MarcomAccessDeniedException ex)
            {
                throw new UnauthorizedAccessException("UnauthorizedAccessException");
            }
            catch (Exception e)
            {
                APILog.Logging.LogException(e, "***API Error******Error in Update PO: ");
                return false;
            }

        }

        public dynamic GetSpentDetailsByEntity(int ID)
        {
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
                IList<IInvoice> iSpentRecs = new List<IInvoice>();
                var obj = marcommanager.PlanningManager.GetAllInvoiceByEntityID(ID);
                iSpentRecs = obj.Item1;

                DataTable dtSpentAttributes = new DataTable();
                dtSpentAttributes = GetAttributes(2);
                dtSpentAttributes.TableName = "attributes";

                DataTable dtSpentDetails = new DataTable("SpentDetails");
                using (var reader = ObjectReader.Create(iSpentRecs))
                {
                    dtSpentDetails.Load(reader);
                }

                List<string> deletecollst = new List<string>();
                foreach (DataColumn colSpent in dtSpentDetails.Columns)
                {
                    DataColumnCollection colAttributes = dtSpentAttributes.Columns;
                    var LinqResult = from p in dtSpentAttributes.AsEnumerable()
                                     where p.Field<string>("enumnames") == colSpent.ColumnName
                                     select new
                                     {
                                         attributename = p.Field<string>("attributename"),
                                         attrID = p.Field<int>("attributeID")
                                     };

                    if (!LinqResult.Any())
                    {
                        deletecollst.Add(colSpent.ColumnName.ToString()); //delete unwanted columns
                    }
                    {
                        foreach (var res in LinqResult)
                        {
                            colSpent.ColumnName = res.attrID.ToString();  //rename the columns to attribute ids
                        }
                    }
                }
                deletecollst.Remove("Entityid");
                deletecollst.Remove("CostCenterID");

                foreach (var col in deletecollst)
                {
                    dtSpentDetails.Columns.Remove(col);
                }

                //add dynamic attributes
                var dynAttributes = from p in dtSpentAttributes.AsEnumerable()
                                    where p.Field<bool>("IsSystemDefined") == false
                                    select new
                                    {
                                        attributename = p.Field<string>("attributename"),
                                        attrID = p.Field<int>("attributeID")
                                    };
                foreach (var dynattr in dynAttributes)
                {
                    dtSpentDetails.Columns.Add(dynattr.attrID.ToString());
                }

                int rowindex = 0;
                foreach (var ispent in iSpentRecs)
                {
                    foreach (var idynattr in ispent.DynamciData)
                    {
                        dtSpentDetails.Rows[rowindex][idynattr.ID.ToString()] = idynattr.values.ToString();
                    }
                    rowindex++;
                }

                dtSpentAttributes.Columns.Remove("enumnames");
                dtSpentAttributes.Columns.Remove("AttributeTypeID");
                DataSet ds = new DataSet("SpentTransactions");
                ds.Tables.Add(dtSpentAttributes);
                ds.Tables.Add(dtSpentDetails);

                string json = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var dynamicObject = Json.Decode(json);
                return dynamicObject;

            }
            catch (Exception e)
            {
                APILog.Logging.LogException(e, "***API Error******Error in GetSpentDetailsByEntity: ");
                throw e;
            }

        }

        public bool CreateSpentTransactions(JObject POs)
        {
            try
            {

                JArray item_array = (JArray)POs["SpentTransactions"];
                var objects = JArray.Parse(item_array.ToString());
                IInvoice iSpentRecs = new BrandSystems.Marcom.Dal.Planning.Model.Invoice();

                DataTable dtSpentAttributes = new DataTable();
                dtSpentAttributes = GetAttributes(2);
                dtSpentAttributes.TableName = "attributes";

                foreach (JObject root in objects)
                {
                    Dictionary<string, string> jattributes = new Dictionary<string, string>();

                    foreach (KeyValuePair<String, JToken> tag in root)
                    {
                        jattributes.Add(tag.Key.ToString(), tag.Value.ToString());
                    }
                    iSpentRecs.InvoiceNumber = jattributes["15"]; jattributes.Remove("15");
                    if (jattributes.ContainsKey("16"))
                    {
                        iSpentRecs.CreateDate = DateTime.Parse(jattributes["16"]); jattributes.Remove("16");
                    }

                    if (jattributes.ContainsKey("17"))
                    {
                        iSpentRecs.POID = jattributes["17"]; jattributes.Remove("17");
                    }
                    if (jattributes.ContainsKey("18"))
                    {
                        iSpentRecs.SupplierName = jattributes["18"]; jattributes.Remove("18");
                    }
                    if (jattributes.ContainsKey("19"))
                    {
                        iSpentRecs.Description = jattributes["19"]; jattributes.Remove("19");
                    }
                    if (jattributes.ContainsKey("20"))
                    {
                        iSpentRecs.amount = Convert.ToDecimal(jattributes["20"]); jattributes.Remove("20");
                    }

                    int EntityID = int.Parse(jattributes["EntityID"]); jattributes.Remove("EntityID");
                    int CostCentreID = int.Parse(jattributes["CostCentreID"]); jattributes.Remove("CostCentreID");

                    IList<IAttributeData> MetadataValues = new List<IAttributeData>();
                    foreach (var jattr in jattributes)
                    {
                        IAttributeData dynamicattr = new BrandSystems.Marcom.Core.Planning.AttributeData();
                        int attributetypeid = (from DataRow dr in dtSpentAttributes.Rows
                                               where (int)dr["attributeID"] == int.Parse(jattr.Key)
                                               select (int)dr["AttributeTypeID"]).FirstOrDefault();
                        dynamicattr.TypeID = attributetypeid;
                        dynamicattr.ID = int.Parse(jattr.Key);
                        dynamicattr.Value = jattr.Value.ToString();
                        MetadataValues.Add(dynamicattr);
                    }

                    IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
                    iSpentRecs.UserID = marcommanager.User.Id;
                    int obj = marcommanager.PlanningManager.InsertApiSpentTransaction(EntityID, CostCentreID, iSpentRecs, MetadataValues);
                    if ((obj == -99) || (obj == 0))
                    {
                        Marcom.API.APILogModel.ApiLogHandler.LogInfo("could not create spent: " + iSpentRecs.InvoiceNumber, Marcom.API.APILogModel.ApiLogHandler.LogType.Notify);
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                return true;
            }
            catch (MarcomAccessDeniedException ex)
            {
                throw new UnauthorizedAccessException("UnauthorizedAccessException");
            }
            catch (Exception e)
            {
                APILog.Logging.LogException(e, "***API Error******Error in CreateSpentTransactions: ");
                return false;
            }

        }

        public bool UpdateSpentTransactions(JObject POs)
        {
            try
            {

                JArray item_array = (JArray)POs["SpentTransactions"];
                var objects = JArray.Parse(item_array.ToString());
                IInvoice iSpentRecs = new BrandSystems.Marcom.Dal.Planning.Model.Invoice();
                ArrayList updateattributeids = new ArrayList();
                DataTable dtSpentAttributes = new DataTable();
                dtSpentAttributes = GetAttributes(2);
                dtSpentAttributes.TableName = "attributes";

                foreach (JObject root in objects)
                {
                    Dictionary<string, string> jattributes = new Dictionary<string, string>();

                    foreach (KeyValuePair<String, JToken> tag in root)
                    {
                        jattributes.Add(tag.Key.ToString(), tag.Value.ToString());
                    }

                    iSpentRecs.InvoiceNumber = jattributes["15"]; jattributes.Remove("15");
                    int EntityID = int.Parse(jattributes["EntityID"]); jattributes.Remove("EntityID");
                    int CostCentreID = int.Parse(jattributes["CostCentreID"]); jattributes.Remove("CostCentreID");

                    if (jattributes.ContainsKey("16"))
                    {
                        iSpentRecs.CreateDate = DateTime.Parse(jattributes["16"]); jattributes.Remove("16");
                        updateattributeids.Add("16");
                    }
                    if (jattributes.ContainsKey("17"))
                    {
                        iSpentRecs.POID = jattributes["17"]; jattributes.Remove("17");
                        updateattributeids.Add("17");
                    }
                    if (jattributes.ContainsKey("18"))
                    {
                        iSpentRecs.SupplierName = jattributes["18"]; jattributes.Remove("18");
                        updateattributeids.Add("18");
                    }
                    if (jattributes.ContainsKey("19"))
                    {
                        iSpentRecs.Description = jattributes["19"]; jattributes.Remove("19");
                        updateattributeids.Add("19");
                    }
                    if (jattributes.ContainsKey("20"))
                    {
                        iSpentRecs.amount = Convert.ToDecimal(jattributes["20"]); jattributes.Remove("20");
                        updateattributeids.Add("20");
                    }


                    IList<IAttributeData> MetadataValues = new List<IAttributeData>();
                    foreach (var jattr in jattributes)
                    {
                        IAttributeData dynamicattr = new BrandSystems.Marcom.Core.Planning.AttributeData();
                        int attributetypeid = (from DataRow dr in dtSpentAttributes.Rows
                                               where (int)dr["attributeID"] == int.Parse(jattr.Key)
                                               select (int)dr["AttributeTypeID"]).FirstOrDefault();
                        dynamicattr.TypeID = attributetypeid;
                        dynamicattr.ID = int.Parse(jattr.Key);
                        dynamicattr.Value = jattr.Value.ToString();
                        MetadataValues.Add(dynamicattr);
                        updateattributeids.Add(jattr.Key);
                    }

                    IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
                    iSpentRecs.UserID = marcommanager.User.Id;
                    int obj = marcommanager.PlanningManager.UpdateApiSpentTransaction(EntityID, CostCentreID, updateattributeids, iSpentRecs, MetadataValues);
                    return true;

                }
                return true;
            }
            catch (MarcomAccessDeniedException ex)
            {
                throw new UnauthorizedAccessException("UnauthorizedAccessException");
            }
            catch (Exception e)
            {
                APILog.Logging.LogException(e, "***API Error******Error in UpdateSpentTransactions: ");
                return false;
            }

        }

        public bool UpdateFinancialMetadata(JObject Financials)
        {
            try
            {

                JArray item_array = (JArray)Financials["FinancialDetails"];
                var objects = JArray.Parse(item_array.ToString());
                DataTable dtSpentAttributes = new DataTable();
                dtSpentAttributes = GetAttributes(3);
                dtSpentAttributes.TableName = "attributes";


                foreach (JObject root in objects)
                {
                    Dictionary<string, string> jattributes = new Dictionary<string, string>();

                    foreach (KeyValuePair<String, JToken> tag in root)
                    {
                        jattributes.Add(tag.Key.ToString(), tag.Value.ToString());
                    }

                    int EntityID = int.Parse(jattributes["EntityID"]); jattributes.Remove("EntityID");
                    int CostCentreID = int.Parse(jattributes["CostCentreID"]); jattributes.Remove("CostCentreID");

                    IList<IAttributeData> MetadataValues = new List<IAttributeData>();
                    foreach (var jattr in jattributes)
                    {
                        IAttributeData dynamicattr = new BrandSystems.Marcom.Core.Planning.AttributeData();
                        int attributetypeid = (from DataRow dr in dtSpentAttributes.Rows
                                               where (int)dr["attributeID"] == int.Parse(jattr.Key)
                                               select (int)dr["AttributeTypeID"]).FirstOrDefault();
                        dynamicattr.TypeID = attributetypeid;
                        dynamicattr.ID = int.Parse(jattr.Key);
                        dynamicattr.Value = jattr.Value.ToString();
                        MetadataValues.Add(dynamicattr);

                    }

                    IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
                    bool obj = marcommanager.PlanningManager.UpdateFinancialMetadata(EntityID, CostCentreID, MetadataValues);
                    if ((obj == false))
                    {
                        Marcom.API.APILogModel.ApiLogHandler.LogInfo("could not update Financial metadata: ", Marcom.API.APILogModel.ApiLogHandler.LogType.Notify);
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                return true;
            }
            catch (MarcomAccessDeniedException ex)
            {
                throw new UnauthorizedAccessException("UnauthorizedAccessException");
            }
            catch (Exception e)
            {
                APILog.Logging.LogException(e, "***API Error******Error in UpdateFinancialMetadata: ");
                return false;
            }

        }

        public dynamic GetFinanceDetailsByEntity(int ID)
        {
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
                IList<IFinancialDetail> df = marcommanager.PlanningManager.GetApiEntityFinancialdDetails(ID, marcommanager.User.Id, 0);

                DataTable dtFinAttributes = new DataTable();
                dtFinAttributes = GetAttributes(3);
                dtFinAttributes.TableName = "attributes";

                DataTable dtFinancials = new DataTable("FinancialDetails");

                using (var reader = ObjectReader.Create(df))
                {
                    dtFinancials.Load(reader);
                }

                List<string> deletecollst = new List<string>();
                //renaming to attribute ids and delete unwanted columns
                foreach (DataColumn colFinDetails in dtFinancials.Columns)
                {
                    DataColumnCollection colAttributes = dtFinAttributes.Columns;
                    var LinqResult = from p in dtFinAttributes.AsEnumerable()
                                     where p.Field<string>("enumnames").ToLower() == colFinDetails.ColumnName.ToLower()
                                     select new
                                     {
                                         attributename = p.Field<string>("attributename"),
                                         attrID = p.Field<int>("attributeID")
                                     };

                    if (!LinqResult.Any())
                    {
                        deletecollst.Add(colFinDetails.ColumnName.ToString()); //delete unwanted columns
                    }
                    {
                        foreach (var res in LinqResult)
                        {
                            colFinDetails.ColumnName = res.attrID.ToString();  //rename the columns to attribute ids
                        }
                    }
                }
                deletecollst.Remove("EntityID");
                deletecollst.Remove("CostCenterID");

                //delete unwanted columns
                foreach (var col in deletecollst)
                {
                    dtFinancials.Columns.Remove(col);
                }

                //dtFinancials.Columns.Remove("21");
                //dtFinancials.Columns.Remove("22");

                //add dynamic attributes
                var dynAttributes = from p in dtFinAttributes.AsEnumerable()
                                    where p.Field<bool>("IsSystemDefined") == false
                                    select new
                                    {
                                        attributename = p.Field<string>("attributename"),
                                        attrID = p.Field<int>("attributeID")
                                    };
                foreach (var dynattr in dynAttributes)
                {
                    dtFinancials.Columns.Add(dynattr.attrID.ToString());
                }

                int rowindex = 0;
                foreach (var ispent in df)
                {
                    foreach (var idynattr in ispent.DynamicData)
                    {
                        dtFinancials.Rows[rowindex][idynattr.ID.ToString()] = idynattr.values.ToString();
                    }
                    rowindex++;
                }

                dtFinAttributes.Columns.Remove("enumnames");
                dtFinAttributes.Columns.Remove("AttributeTypeID");
                DataSet ds = new DataSet("PurchaseOrder");
                ds.Tables.Add(dtFinAttributes);
                ds.Tables.Add(dtFinancials);

                string json = JsonConvert.SerializeObject(ds, Formatting.Indented);
                var dynamicObject = Json.Decode(json);
                return dynamicObject;

            }
            catch (Exception e)
            {
                APILog.Logging.LogException(e, "***API Error******Error in GetFinanceDetailsByEntity: ");
                throw e;
            }

        }

        public Boolean IsExternalSourceAllowed()
        {
            try
            {
                //Get the tenant related file path
                //string xmlpath = Path.Combine(HttpRuntime.AppDomainAppPath, "AdminSettings.xml"); 
                string TenantHost = HttpContext.Current.Request.Url.Host;
                TenantSelection tfp = new TenantSelection();
                string TenantFilePath = tfp.GetTenantFilePathByHostName(TenantHost);
                int TenantID = tfp.GetTenantIDByHostName(TenantHost);

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, MarcomManagerFactory.GetSystemSession(TenantID));
                string xmlpath = "";
                XElement xelementFilepath;
                if (marcomManager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
                {
                    xmlpath = Path.Combine(marcomManager.User.TenantPath, "AdminSettings.xml");
                    xelementFilepath = AWSHelper.ReadAdminXElement(marcomManager.User.AwsStorage.S3, marcomManager.User.AwsStorage.BucketName, xmlpath);
                }
                else
                {
                    xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + marcomManager.User.TenantPath, "AdminSettings.xml");
                    xelementFilepath = XElement.Load(xmlpath);
                }
                string xelementName = "FinancialSettings";

                var xmlElement = xelementFilepath.Element(xelementName);
                foreach (var des in xmlElement.Descendants())
                {
                    if (des.Name.ToString() == "IsExternalsource")
                    {
                        return (Convert.ToBoolean(des.Value));
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                APILog.Logging.LogException(e, "***API Error******Error in IsExternalSourceAllowed: ");
                throw e;
            }
        }

        public dynamic GetConvertedCurrencies(JObject curr)
        {
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
                var convertedcurobj = marcomManager.CommonManager.CurrencyConvertJSON(curr);
                //return convertedcurobj;

                string json = "{\"Convertcurrencies\":" + JsonConvert.SerializeObject(convertedcurobj, Formatting.Indented) + "}";

                var dynamicObject = Json.Decode(json);
                return dynamicObject;

            }
            catch (MarcomAccessDeniedException ex)
            {
                throw new UnauthorizedAccessException("UnauthorizedAccessException");
            }
            catch (Exception e)
            {
                APILog.Logging.LogException(e, "***API Error******Error in GetConvertedCurrencies ");
                throw e;
            }

        }

    }


}