﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Base
{
    static class Queryinterceptor
    {
        public static NHibernate.IQuery QueryHintRecompile(this NHibernate.IQuery query)
        {
            query.SetComment(QueryHintInterceptor.QueryHintRecompileCommentString);
            return query;
        }
    }
}
