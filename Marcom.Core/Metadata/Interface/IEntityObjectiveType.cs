﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface IEntityObjectiveType
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }


        int EntitytypeId
        {
            get;
            set;

        }
        int ObjectiveTypeId
        {
            get;
            set;

        }

        int ObjectiveBaseTypeId
        {
            get;
            set;

        }

        int UnitId
        {
            get;
            set;

        }

        #endregion
    }
}
