﻿(function (ng, app) {
    function ObjectivedetailfilterService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetFilterSettingsForDetail: GetFilterSettingsForDetail,
            InsertFilterSettings: InsertFilterSettings,
            GetFilterSettingValuesByFilertId: GetFilterSettingValuesByFilertId,
            DeleteFilterSettings: DeleteFilterSettings,
            GetOptionsFromXML: GetOptionsFromXML,
            GettingEntityTypeHierarchyForAdminTree: GettingEntityTypeHierarchyForAdminTree,
            GettingFilterEntityMember: GettingFilterEntityMember,
            GettingFilterAttribute: GettingFilterAttribute
        });
        function GetFilterSettingsForDetail(TypeID) { var request = $http({ method: "get", url: "api/Planning/GetFilterSettingsForDetail/" + TypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function InsertFilterSettings(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertFilterSettings/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetFilterSettingValuesByFilertId(FilterID) { var request = $http({ method: "get", url: "api/Planning/GetFilterSettingValuesByFilertId/" + FilterID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DeleteFilterSettings(FilterId) { var request = $http({ method: "delete", url: "api/Planning/DeleteFilterSettings/" + FilterId, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function GetOptionsFromXML(elementNode, typeid) { var request = $http({ method: "get", url: "api/Metadata/GetOptionsFromXML/" + elementNode + "/" + typeid, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GettingEntityTypeHierarchyForAdminTree(EntityTypeID, ModuleID) { var request = $http({ method: "get", url: "api/Metadata/GettingEntityTypeHierarchyForAdminTree/" + EntityTypeID + "/" + ModuleID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GettingFilterEntityMember(formobj) { var request = $http({ method: "post", url: "api/Metadata/GettingFilterEntityMember/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GettingFilterAttribute(formobj) { var request = $http({ method: "post", url: "api/Metadata/GettingFilterAttribute/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("ObjectivedetailfilterService", ['$http', '$q', ObjectivedetailfilterService]);
})(angular, app);