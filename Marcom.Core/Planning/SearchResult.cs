﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.IO;

namespace BrandSystems.Marcom.Core.Planning
{
    public class SearchResult
    {
        private int intCount;
        [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public int Count
        {
            get { return intCount; }
            set { intCount = value; }
        }

        private int intPageNo = 1;
        [DataMember(IsRequired = false, Order = 2), XmlAttribute()]
        public int PageNo
        {
            get { return intPageNo; }
            set { intPageNo = value; }
        }

        private int intTotalPages;
        [DataMember(IsRequired = false, Order = 3), XmlAttribute()]
        public int TotalPages
        {
            get { return intTotalPages; }
            set { intTotalPages = value; }
        }

        private List<ResultEntity> objSearchResultEntity;
        [DataMember(IsRequired = false, Order = 4), XmlAttribute()]
        public List<ResultEntity> SearchResultEntity
        {
            get { return objSearchResultEntity; }
            set { objSearchResultEntity = value; }
        }

        private List<ResultEntity> objdistincttypes;
        [DataMember(IsRequired = false, Order = 4), XmlAttribute()]
        public List<ResultEntity> DistinctTypes
        {
            get { return objdistincttypes; }
            set { objdistincttypes = value; }
        }

        object lstDistictResultcount;
        [DataMember(IsRequired = false, Order = 5), XmlAttribute()]
        public object DistictResultcount
        {
            get { return lstDistictResultcount; }
            set { lstDistictResultcount = value; }
        }
        List<object> lstAssetResult;
        [DataMember(IsRequired = false, Order = 6), XmlAttribute()]
        public List<object> AssetData
        {
            get { return lstAssetResult; }
            set { lstAssetResult = value; }
        }

        public int ProductionCount { get; set; }
        public int CMSCount { get; set; }
        public int TaskCount { get; set; }
        public int AttachmentCount { get; set; }
        public int AssetLibraryCount { get; set; }
        public string CurrentScroll { get; set; }
    }
}
