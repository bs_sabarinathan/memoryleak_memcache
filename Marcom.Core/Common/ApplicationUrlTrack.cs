using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// Navigation object for table 'CM_Navigation'.
	/// </summary>

    internal class ApplicationUrlTrack : IApplicationUrlTrack 
	{
		#region Member Variables

         protected Guid _trackid;
		protected string _trackvalue;
		protected DateTime _doi;
		
		
		#endregion
		
		#region Constructors
        public ApplicationUrlTrack() { }

        public ApplicationUrlTrack(Guid pTrackId, string pTrackValue, DateTime pDoi)
        {
            this._trackid = pTrackId;
            this._trackvalue = pTrackValue;
            this._doi = pDoi;
        }
		
		#endregion
		
		#region Public Properties

        public virtual Guid TrackID
        {
            get { return _trackid; }
            set {  _trackid = value; }

        }

        public virtual string TrackValue
        {
            get { return _trackvalue; }
            set
            {
               
                _trackvalue = value;
            }

        }



        public virtual DateTime DOI
        {
            get { return _doi; }
            set {  _doi = value; }
        }

		#endregion 
		
		
	}
	
}
