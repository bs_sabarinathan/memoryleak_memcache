﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
    {
    interface IFinancialMultiSelect
        {
        /// <summary>
        /// IFinancialMultiSelect interface for table '[MM_Fin_MultiSelect]'.
        /// </summary>
        int FinID { get; set; }
        int FinTypeID { get; set; }
        int FinAttributeID { get; set; }
        int FinOptionID { get; set; }
        int ID { get; set; }
        }
    }
