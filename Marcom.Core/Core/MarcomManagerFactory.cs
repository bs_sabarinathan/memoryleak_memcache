﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Managers;
using BrandSystems.Marcom.Dal.Base;
using System.Threading;
using Mail;
using System.Configuration;
using BrandSystems.Marcom.Core.Interface.Managers;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Dal.Metadata.Model;
using System.Collections;
using System.Xml;
using BrandSystems.Marcom.Core.Utility;
using Newtonsoft.Json;
using BrandSystems.Marcom.Core.User.Interface;
using BrandSystems.Marcom.Core.User;
using BrandSystems.Marcom.Core.Metadata.Interface;
using System.Net.Http;

namespace BrandSystems.Marcom.Core
{
    public class MarcomManagerFactory
    {

        private static string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();

        // search the file below the current directory
        private static string retFilePath = baseDir + "//" + "SessionMonitor.txt";
        private static bool bReturnLog = false;


        // Dictionary to keep control over all current sessions
        private static Dictionary<Guid, IMarcomManager> _sessions = new Dictionary<Guid, IMarcomManager>();

        // Dictionary to keep control over all current API sessions with tokens
        private static Dictionary<Guid, Guid> _APITokenSessions = new Dictionary<Guid, Guid>();

        // Dictionary to keep control over all ongoing transactions
        private static Dictionary<string, Transaction> _transactions = new Dictionary<string, Transaction>();

        // List of the Managers
        private static List<IManager> _managers = new List<IManager>();

        // Reference to the only EventManager
        private static IEventManager _eventManager = null;

        //Reference to the only PluginManager
        private static IPluginManager _pluginManager = null;

        private static bool _isInitialized = false;

        private static readonly object _locker = new object();

        internal static Guid[] _systemSessionId = new Guid[10];


        public static string PrintMarcomManager()
        {
            string names = String.Join(", ", _sessions.Select(x => x.Key));
            return names;
        }

        public static string PrintMarcomUserManager()
        {
            string names = String.Join(", ", _sessions.Select(x => x.Value.User.FirstName));
            return names;
        }


        /// <summary>
        /// Gets a MarcomManager with a certain sessionId
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public static IMarcomManager GetMarcomManager(HttpRequestMessage RequestObj, Guid sessionId)
        {
            try
            {

                // Return a user specific MarcomManager
                IUser UserObj = null;
                UserObj = MarcomCache<User.User>.GetCache("_marcomUsersCache", sessionId);

                if (UserObj != null)
                {
                    if (RequestObj != null)
                    {
                        MarcomCache<string>.writeLog("", sessionId.ToString(), RequestObj.RequestUri.ToString(), "User is present in ElasticCache");
                    }
                    if (_sessions.ContainsKey(sessionId))
                    {
                        return _sessions[sessionId];
                    }
                    else
                    {
                        return CreateSessionFromCache(sessionId);
                    }
                }
                else
                {
                    if (RequestObj != null)
                    {
                        MarcomCache<string>.writeLog("", sessionId.ToString(), RequestObj.RequestUri.ToString(), "User is not present in ElasticCache");
                    }
                }
                return null;

            }
            catch (Exception ex)
            {
                String names = String.Join(", ", _sessions.Select(x => x.Key));
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MarcomManagerFactory -> GetMarcomManager method hits exception. Session id is : " + sessionId.ToString() + ", session dictonary length is: " + _sessions.Count().ToString() + ", session values :" + names + ", exception is" + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.Notify);
                MarcomCache<string>.writeLog("", sessionId.ToString(), RequestObj.RequestUri.ToString(), "GetMarcomManager method hits exception. Session id is : " + sessionId.ToString() + ", session dictonary length is: " + _sessions.Count().ToString() + ", session values :" + names + ", exception is" + ex.ToString());
                throw ex;
            }
        }

        #region "Initialization"

        /// <summary>
        /// Check if Marcom platform is initialized, if not it is initialized
        /// </summary>
        internal static void EnsureInitialized()
        {
            lock (_managers)
            {
                if (!_isInitialized)
                    Initialize();
            }
        }

        private static void Reinitialize()
        {
            _isInitialized = false;
            Initialize();
        }

        /// <summary>
        /// Get active version number from Admin settings XML
        /// </summary>
        public static int[] ActiveMetadataVersionNumber = new int[10];

        /// <summary>
        /// Get version number of working metadata from Admin settings XML
        /// </summary>
        //public static int AdminMetadataVersionNumber = BrandSystems.Marcom.Utility.MetadataVersion.WorkingAdminMetadataVersion;
        public static int[] AdminMetadataVersionNumber = new int[10];

        /// <summary>
        /// Set whether to work with Current Working version (true) or Future working version (false)
        /// </summary>
        //public static bool IsWorkingWithCurrentWorkingVersion = true;
        public static bool[] IsWorkingWithCurrentWorkingVersion = new bool[10];

        /// <summary>
        /// Set whether to to see old verion
        /// </summary>
        //public static bool viewOldMetadataVersion = false;
        public static bool[] viewOldMetadataVersion = new bool[10];
        /// <summary>
        /// set the old version number to be seen
        /// </summary>
        //public static string oldMetadataVersionNumber = string.Empty;
        public static string[] oldMetadataVersionNumber = new string[10];

        /// <summary>
        /// set wheather is path in Metadata settings or Outside
        /// </summary>
        public static bool[] IsCurrentWorkingXml = new bool[10];

        public static IList<ModuleFeature> ListOfModuleFeature;

        /// <summary>
        /// Initialize Marcom Platform
        /// </summary>
        private static void Initialize()
        {
            BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("initialization block hit happen at" + DateTime.Now, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);


            if (_isInitialized)
                ShutDown();

            // Initialize the event manager
            _eventManager = EventManager.Instance;
            _eventManager.Initialize();

            IMarcomManager initializingMarcomManager = new MarcomManager(_eventManager, null);

            // Initialize log4net
            log4net.Config.XmlConfigurator.Configure();

            // Initialize DataAccess Layer
            PersistenceManager.Instance.Initialize();

            _managers = new List<IManager>();
            // Create the managers (this could be done by configuration)
            _managers.Add(PlanningManager.Instance);
            _managers.Add(UserManager.Instance);
            _managers.Add(MetadataManager.Instance);
            _managers.Add(ReportManager.Instance);
            _managers.Add(DigitalAssetManager.Instance);
            //_managers.Add(TaskManager.Instance);
            // Todo: add the rest of the managers


            // Initialize all managers
            foreach (IManager manager in _managers)
            {
                manager.Initialize(initializingMarcomManager);
            }


            //BrandSystems.Marcom.Utility.DynamicTableLibrary.Instance.Initialize();

            MailServer.Instance.Initialize("");
            MailServer.Instance.StartEngine();
            //GetSystemSession();
            SetVersionNo();
            SetMetadataVersions();
            _isInitialized = true;

            //string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
            //string retFilePath = baseDir + "//" + "ServiceErrorLog.txt";
            //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "Initialize " + MarcomManagerFactory.PrintMarcomManager(), DateTime.Now);


            //BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("initialization block hit done at" + DateTime.Now, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);


        }

        /// <summary>
        /// Shutdown and cleanup Marcom Platform
        /// </summary>
        private static void ShutDown()
        {
            _managers.Clear();
            _sessions.Clear();
            _transactions.Clear();
            _isInitialized = false;

            //string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
            //string retFilePath = baseDir + "//" + "ServiceErrorLog.txt";
            //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "ShutDown " + MarcomManagerFactory.PrintMarcomManager(), DateTime.Now);

        }

        #endregion

        #region "Sessions"

        /// <summary>
        /// Authenticates the user and returns a sessionId that can be used 
        /// in GetMarcomManger(). If the user cannot be authenticated -1 is returned...
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static Guid CreateSession(int ID, int tenantID, string tenanthostname)
        {
            MarcomCache<string>.writeLog("", "", "Calling CreateSession", "Started");
            BrandSystems.Marcom.Core.User.Interface.IUser user = UserManager.Instance.ValidateUser(ID, false, tenanthostname);

            if (user != null)
            {
                MarcomCache<string>.writeLog("", "", "Creating CreateSession", "Started");

                //fill tenant info for the user
                BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
                string TenantFilePath = tfp.GetTenantFilePath(tenantID);
                string TenantName = tfp.GetTenantNamebyTenantID(tenantID);
                user.TenantID = tenantID;
                user.TenantHost = tenanthostname;
                user.TenantPath = TenantFilePath;
                user.TenantName = TenantName;
                user.AwsStorage = tfp.GetTenantAmazonStoragebytenantId(tenantID);

                //EnsureInitialized();
                Guid sessionId = Guid.NewGuid();// /* A generated session key */ 1;
                _sessions[sessionId] = new MarcomManager(_eventManager, _pluginManager);
                _sessions[sessionId].SessionID = sessionId;
                _sessions[sessionId].User = user;

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, sessionId);
                user.ListOfAccess = new Dictionary<string, bool>();
                user.ListOfAccess = marcomManager.AccessManager.getUserGlobalRoleList(marcomManager);

                _sessions[sessionId].User = user;


                //ErrorLog.LogFilePath = retFilePath;
                //ErrorLog.CustomErrorRoutine(false, "Usersession created " + sessionId, DateTime.Now);

                //string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
                //string retFilePath = baseDir + "//" + "ServiceErrorLog.txt";
                //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "CreateSession creation" + MarcomManagerFactory.PrintMarcomManager(), DateTime.Now);

                MarcomCache<string>.writeLog(user.FirstName.ToString(), sessionId.ToString(), "Created Session", "Success");
                return sessionId;
            }
            else
            {
                //string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
                //string retFilePath = baseDir + "//" + "ServiceErrorLog.txt";
                //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "CreateSession else Guid.Empty" + MarcomManagerFactory.PrintMarcomManager(), DateTime.Now);
                if (user.FirstName == null)
                {
                    user.FirstName = " ";
                }
                MarcomCache<string>.writeLog(user.FirstName.ToString(), "", "Failed to CreateSession", "Failed");


                return Guid.Empty;
            }
            throw new Exception("Invalid Opeartion");

        }


        public static IMarcomManager CreateSessionFromCache(Guid sessionId)
        {
            _sessions[sessionId] = new MarcomManager(_eventManager, _pluginManager);
            _sessions[sessionId].SessionID = sessionId;

            //ErrorLog.LogFilePath = retFilePath;
            //ErrorLog.CustomErrorRoutine(false, "Usersession created " + sessionId, DateTime.Now);

            //string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
            //string retFilePath = baseDir + "//" + "ServiceErrorLog.txt";
            //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "CreateSession creation" + MarcomManagerFactory.PrintMarcomManager(), DateTime.Now);

            MarcomCache<string>.writeLog("", sessionId.ToString(), "Created Session from cache", "Success");
            return _sessions[sessionId];

        }
        /// <summary>
        /// Authenticates the user and returns a sessionId that can be used 
        /// in GetMarcomManger(). If the user cannot be authenticated -1 is returned...
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static void InitializeSystem()
        {
            BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("initialization from global asax hit happen at" + DateTime.Now, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

            EnsureInitialized();

            //string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
            //string retFilePath = baseDir + "//" + "ServiceErrorLog.txt";
            //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "InitializeSystem" + MarcomManagerFactory.PrintMarcomManager(), DateTime.Now);


        }


        public static int AuthenticateAPIToken(Guid APIToken, int TenantID)
        {
            //logic to fetch the user id based on the token.
            //UserManager.Instance.GetApiUserIDByAuthToken(APIToken.ToString());

            MarcomManagerFactory.EnsureInitialized();

            Guid SessionID = GetSystemSession(TenantID);
            int UserID = 0;
            IMarcomManager metadataManagerProxy = MarcomManagerFactory.GetMarcomManager(null, SessionID);
            using (ITransaction tx = metadataManagerProxy.GetTransaction(metadataManagerProxy.User.TenantID))
            {
                string qry = "SELECT uua.UserID  FROM UM_UserAPIInterface uua WHERE uua.APIGuid='" + APIToken.ToString() + "'";
                var result = tx.PersistenceManager.MetadataRepository[metadataManagerProxy.User.TenantID].ExecuteQuery(qry).Cast<Hashtable>().ToList();
                if (result.Count == 0)
                {
                    return UserID;
                }
                else
                {
                    return Convert.ToInt32(result[0]["UserID"]);
                }
            }

        }

        public static Guid CreateAPISession(Guid AuthorizationToken, int ID, string TenantHost, bool impersonateuser)
        {
            ErrorLog.LogFilePath = retFilePath;


            try
            {

                bReturnLog = ErrorLog.CustomErrorRoutine(false, "Marcommanagerfactory CreateAPISession started at " + DateTime.Now.ToString(), DateTime.Now);

                bReturnLog = ErrorLog.CustomErrorRoutine(false, "Marcommanagerfactory CreateAPISession started for token " + AuthorizationToken.ToString() + " at " + DateTime.Now.ToString(), DateTime.Now);

                //check if the token key has a session in place already
                if (_APITokenSessions.ContainsKey(AuthorizationToken))
                {
                    if (_sessions.ContainsKey(_APITokenSessions[AuthorizationToken])) //if the session persists
                    {
                        return _APITokenSessions[AuthorizationToken]; // return the existing session
                    }
                }

                //CHECK FOR IMPERSONATE USER
                bool apiuserproxy = true;
                if (impersonateuser == true) { apiuserproxy = false; }


                bReturnLog = ErrorLog.CustomErrorRoutine(false, "Marcommanagerfactory UserManager.Instance.ValidateUser started for user" + ID + " at " + DateTime.Now.ToString(), DateTime.Now);

                //generate new session for the api user      
                BrandSystems.Marcom.Core.User.Interface.IUser user = UserManager.Instance.ValidateUser(ID, apiuserproxy, TenantHost);


                bReturnLog = ErrorLog.CustomErrorRoutine(false, "Marcommanagerfactory UserManager.Instance.ValidateUser end for user " + ID + " name is " + user.Email + " at " + DateTime.Now.ToString(), DateTime.Now);



                if (user != null)
                {
                    //Get the tenant related file path
                    BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
                    user.TenantHost = TenantHost;
                    user.TenantPath = tfp.GetTenantFilePathByHostName(TenantHost);
                    user.TenantName = tfp.GetTenantNamebyTenantHost(TenantHost);
                    user.TenantID = tfp.GetTenantIDByHostName(TenantHost);
                    user.AwsStorage = tfp.GetTenantAmazonStorageByHostName(TenantHost);

                    //check if impersonate user already has existing session then return that user session
                    var AlreadyPresentSessionID = _sessions.Where(p => p.Value.User.Id == ID).Select(p => p.Key);
                    if (AlreadyPresentSessionID.Count() > 0)
                    {
                        Guid existinguser = AlreadyPresentSessionID.First();
                        return existinguser;
                    }

                    EnsureInitialized();
                    Guid sessionId = Guid.NewGuid();// 
                    _sessions[sessionId] = new MarcomManager(_eventManager, _pluginManager);
                    _sessions[sessionId].SessionID = sessionId;
                    _sessions[sessionId].User = user;
                    _APITokenSessions.Add(AuthorizationToken, sessionId); //adding the token and session to dictionary



                    bReturnLog = ErrorLog.CustomErrorRoutine(false, "Marcommanagerfactory CreateAPISession done at " + DateTime.Now.ToString(), DateTime.Now);


                    return sessionId;
                }
                else
                {
                    return Guid.Empty;
                }

            }

            catch (Exception ex)
            {
                ErrorLog.LogFilePath = retFilePath;
                bReturnLog = ErrorLog.ErrorRoutine(false, ex);
            }

            throw new Exception("Invalid Opeartion");
        }

        public static Guid CreateSessionSetUpToolAdmin(int ID, string HostName)
        {
            BrandSystems.Marcom.Core.User.Interface.IUser user = UserManager.Instance.ValidateUser(ID, false, HostName);
            if (user != null)
            {
                EnsureInitialized();
                Guid sessionId = Guid.NewGuid();// 
                _sessions[sessionId] = new MarcomManager(_eventManager, _pluginManager);
                _sessions[sessionId].SessionID = sessionId;
                _sessions[sessionId].User = user;
                return sessionId;
            }
            else
            {
                return Guid.Empty;
            }

            throw new Exception("Invalid Opeartion");
        }

        public static Guid GetSystemSession(int TenantID)
        {
            MarcomCache<string>.writeLog("System", "", "Calling GetSystemSession", "Started");
            if (_systemSessionId[TenantID] == Guid.Empty)
            {
                MarcomCache<string>.writeLog("System", "", "Creating SystemSession", "Started");

                BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
                string TenantFilePath = tfp.GetTenantFilePath(TenantID);
                string TenantHost = tfp.GetTenantHostURLbyTenantID(TenantID);
                string TenantName = tfp.GetTenantNamebyTenantID(TenantID);
                BrandSystems.Marcom.Core.User.Interface.IUser user = new User.User();
                user.UserName = "system";
                user.TenantID = TenantID;
                user.TenantPath = TenantFilePath;
                user.TenantName = TenantName;
                user.TenantHost = TenantHost;
                user.AwsStorage = tfp.GetTenantAmazonStoragebytenantId(TenantID);
                _systemSessionId[TenantID] = Guid.NewGuid();// /* A generated session key */ 1;
                _sessions[_systemSessionId[TenantID]] = new MarcomManager(_eventManager, _pluginManager);
                _sessions[_systemSessionId[TenantID]].SessionID = _systemSessionId[TenantID];
                _sessions[_systemSessionId[TenantID]].User = user;


                MarcomCache<IMarcomManager>.SetCache(_sessions[_systemSessionId[TenantID]], cacheKey._MarcomSession.ToString(), _systemSessionId[TenantID]);

                //string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
                //string retFilePath = baseDir + "//" + "ServiceErrorLog.txt";
                //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "GetSystemSession" + MarcomManagerFactory.PrintMarcomManager(), DateTime.Now);

                //ErrorLog.LogFilePath = retFilePath;
                //ErrorLog.CustomErrorRoutine(false, "system created " + _systemSessionId[TenantID], DateTime.Now);


                if (user.FirstName == null)
                {
                    user.FirstName = "";
                }
                MarcomCache<string>.writeLog(user.FirstName.ToString(), _systemSessionId[TenantID].ToString(), "Created SystemSession", "Success");

                return _systemSessionId[TenantID];
            }
            else
            {
                MarcomCache<string>.writeLog("System", _systemSessionId[TenantID].ToString(), "Returning Existing SystemSession", "Started");
                return _systemSessionId[TenantID];
            }
            throw new Exception("Invalid Opeartion");
        }

        public static void SetVersionNo()
        {

            Configuration[] configAry = new Configuration[] { };
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            XmlDocument xdcDocument = new XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            XmlElement xelRoot = xdcDocument.DocumentElement;
            XmlNodeList xnlNodes = xelRoot.SelectNodes("/Tenants/Tenant");
            int tenantid;
            foreach (XmlNode xndNode in xnlNodes)
            {
                tenantid = Int16.Parse(xndNode["TenantID"].InnerText);
                IMarcomManager metadataManagerProxy = MarcomManagerFactory.GetMarcomManager(null, GetSystemSession(tenantid));
                using (ITransaction tx = metadataManagerProxy.GetTransaction(tenantid))
                {
                    string qry = "SELECT ID FROM MM_MetadataVersion WHERE STATE = 1";
                    var result = tx.PersistenceManager.MetadataRepository[tenantid].ExecuteQuery(qry).Cast<Hashtable>().ToList();
                    if (result.Count == 0)
                    {
                        ActiveMetadataVersionNumber[tenantid] = 0;
                    }
                    else
                    {
                        ActiveMetadataVersionNumber[tenantid] = Convert.ToInt32(result[0]["ID"]);
                    }
                }
            }
        }



        public static void SetMetadataVersions()
        {
            Configuration[] configAry = new Configuration[] { };
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            XmlDocument xdcDocument = new XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            XmlElement xelRoot = xdcDocument.DocumentElement;
            XmlNodeList xnlNodes = xelRoot.SelectNodes("/Tenants/Tenant");
            int tenantid = 0;
            foreach (XmlNode xndNode in xnlNodes)
            {
                tenantid = Int16.Parse(xndNode["TenantID"].InnerText);
                AdminMetadataVersionNumber[tenantid] = BrandSystems.Marcom.Utility.MetadataVersion.GetAdminMetadataXmlVersion(tenantid);
                IsWorkingWithCurrentWorkingVersion[tenantid] = true;
                viewOldMetadataVersion[tenantid] = false;
                oldMetadataVersionNumber[tenantid] = string.Empty;
                IsCurrentWorkingXml[tenantid] = false;
            }
        }

        /// <summary>
        /// Logout the current user
        /// </summary>
        /// <param name="sessionId"></param>
        public static void EndSession(Guid sessionId)
        {
            _sessions.Remove(sessionId);

            /** Destroy the cached session Object **/

            MarcomCache<bool>.RemoveCache(sessionId);

            MarcomCache<string>.writeLog("", sessionId.ToString(), "End Session", "Success");

            //string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
            //string retFilePath = baseDir + "//" + "ServiceErrorLog.txt";
            //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "EndSession " + sessionId + " " + MarcomManagerFactory.PrintMarcomManager(), DateTime.Now);


            //remove session details for API users
            var APITokenToRemove = _APITokenSessions.Where(kvp => kvp.Value == sessionId).Select(kvp => kvp.Key);
            foreach (var APIsession in APITokenToRemove)
            {
                _APITokenSessions.Remove(APIsession);
            }
            //

            // Some other cleanup
        }

        #endregion

        #region "Transactions"

        internal static ITransaction GetTransaction(bool create, int tenantid)
        {
            string key = GetTransactionKey();
            lock (_transactions)
            {
                if (!_transactions.ContainsKey(key))
                {

                    if (create)
                    {
                        _transactions.Add(key, new Transaction(key, false, tenantid));
                    }
                    else
                    {
                        return null;
                    }
                }

                Transaction t = _transactions[key];
                t.IncrementCommitCount();
                return t;
            }
        }

        private static string GetTransactionKey()
        {
            return Thread.CurrentThread.GetHashCode().ToString();
        }

        internal static void TransactionCommited(ITransaction tx)
        {
            // Todo: Add synchronization here
            // Call all managers
            foreach (IManager manager in _managers)
            {
                manager.CommitCaches();
            }
        }

        internal static void TransactionRollbacked(ITransaction tx)
        {
            // Todo: Add synchronization here
            // Call all managers
            foreach (IManager manager in _managers)
            {
                manager.RollbackCaches();
            }
        }

        internal static void RemoveTransaction(string key)
        {
            lock (_transactions)
            {
                _transactions.Remove(key);
            }
        }

        #endregion
    }
}
