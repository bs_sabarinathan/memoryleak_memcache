﻿(function (ng, app) {
    "use strict"; function muiadminassetaccessCtrl($scope, $location, $resource, $timeout, $http, $cookies, $translate, AdminService) {
        $scope.EnableOptionAdd = true; $scope.count = 1; $scope.optionsLists = []; $scope.wizard = { attributeType: 0, AttributeId: 0, Description: '', Caption: '', newOption: '', EditOption: '', OptionID: 0, TreeLevelNumberByNode: [], TreeLevels: [], IsDisableAttributeType: false, TotalPopulartags: 5, TagwordsSearch: '' }; $scope.addOptions = function () {
            var Rolename = []; var val = $scope.wizard.newOption.trim(); if (val.length > 0) {
                var isExist = $.grep($scope.optionsLists, function (rel) { return rel.Caption == $scope.wizard.newOption; }); if (isExist != null) {
                    if (isExist.length > 0) { bootbox.alert($translate.instant('LanguageContents.Res_1122.Caption')); }
                    else { $scope.optionsLists.push({ "PId": $scope.count, "ID": 0, "Caption": $scope.wizard.newOption, "SortOrder": ($scope.optionsLists.length + 1) }); InsertRole($scope.wizard.newOption); }
                }
            }
            else { bootbox.alert($translate.instant('LanguageContents.Res_1123.Caption')); $scope.wizard.newOption = ""; return false; }
        }; function RefreshControls() { $scope.wizard.EditOption = ''; $scope.wizard.newOption = ''; $scope.wizard.OptionID = 0; $scope.EnableOptionUpdate = false; $scope.EnableOptionAdd = true; }
        function InserUpdateRole(caption, id) {
            var addoptions = {}; addoptions.Caption = caption; addoptions.Id = id; AdminService.AssetAccess(addoptions).then(function (addoptionsResponse) {
                if (addoptionsResponse.Response == true) { NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption')); RefreshControls(); LoadRoles(); }
                else NotifyError($translate.instant('LanguageContents.Res_1125.Caption'));
            });
        }
        function InsertRole(caption) {
            var addoptions = {}; addoptions.Caption = caption; addoptions.Id = 0; AdminService.AssetAccess(addoptions).then(function (addoptionsResponse) {
                if (addoptionsResponse.Response == true) { NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption')); RefreshControls(); LoadRoles(); }
                else NotifyError($translate.instant('LanguageContents.Res_1124.Caption'));
            });
        }
        $scope.EditOptions = function () {
            if ($scope.wizard.OptionID != 0) { var UpdateOption = $.grep($scope.optionsLists, function (e) { return e.PId == $scope.wizard.PrimaryID; }); UpdateOption[0].Caption = $scope.wizard.newOption; InserUpdateRole($scope.wizard.newOption, $scope.wizard.PrimaryID); }
            else if ($scope.wizard.OptionID == 0) { var UpdateOption = $.grep($scope.optionsLists, function (e) { return e.PId == $scope.wizard.PrimaryID; }); UpdateOption[0].Caption = $scope.wizard.newOption; InserUpdateRole($scope.wizard.newOption, $scope.wizard.PrimaryID); }
            $scope.wizard.EditOption = ''; $scope.wizard.newOption = ''; $scope.wizard.OptionID = 0; $scope.EnableOptionUpdate = false; $scope.EnableOptionAdd = true;
        }; $scope.GetOptionbyID = function GetOptionbyID(row) { $scope.wizard.PrimaryID = row.PId; $scope.wizard.OptionID = row.ID; $scope.wizard.EditOption = row.Caption; $scope.wizard.newOption = row.Caption; $scope.EnableOptionUpdate = true; $scope.EnableOptionAdd = false; }; $scope.deleteOptions = function deleteOptions(row) {
            $scope.GetOptionbyID(row); bootbox.confirm($translate.instant('LanguageContents.Res_2050.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        try {
                            if (row.PId != 0) {
                                var ID = $scope.wizard.PrimaryID; AdminService.DeleteAssetAccessOption(ID).then(function (deleteById) {
                                    if (deleteById.StatusCode == 405) { NotifyError($scpe.LanguageContents.Res_4303); }
                                    else {
                                        var index = $scope.optionsLists.indexOf(row)
                                        $scope.optionsLists.splice(index, 1); NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption')); $scope.wizard.newOption = ''; $scope.EnableOptionUpdate = false; $scope.EnableOptionAdd = true;
                                    }
                                });
                            }
                            else {
                                var index = $scope.optionsLists.indexOf(row)
                                $scope.optionsLists.splice(index, 1); NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption')); $scope.wizard.newOption = ''; $scope.EnableOptionUpdate = false; $scope.EnableOptionAdd = true;
                            }
                        }
                        catch (e) { NotifyError($translate.instant('LanguageContents.Res_4524.Caption')); }
                    }, 100);
                }
            });
        }; function LoadRoles() { $scope.optionsLists = []; AdminService.GetAssestAccessSaved().then(function (GetAssestAccessResults) { if (GetAssestAccessResults.Response.length != null) { for (var i = 0, role; role = GetAssestAccessResults.Response[i++];) { $scope.optionsLists.push({ "PId": role.ID, "ID": 0, "Caption": role.Role, "SortOrder": ($scope.optionsLists.length + 1) }); } } }); }
        $scope.alternateColor = function (index) {
            if (index % 2)
                return "FDFDFD"; else
                return "F3F3F3";
        }
        $scope.set_BGColor = function (clr) {
            if (clr != null)
                return { 'background-color': "#" + clr.toString().trim() }
            else
                return '';
        }
        $timeout(function () { LoadRoles(); }, 100);
    }
    app.controller("mui.admin.assetaccessCtrl", ['$scope', '$location', '$resource', '$timeout', '$http', '$cookies', '$translate', 'AdminService', muiadminassetaccessCtrl]);
})(angular, app);