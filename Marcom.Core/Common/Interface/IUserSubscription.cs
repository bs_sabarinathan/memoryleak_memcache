using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IUserSubscription interface for table 'CM_User_Subscription'.
    /// </summary>
    public interface IUserSubscription
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Userid
        {
            get;
            set;

        }

        int Entityid
        {
            get;
            set;

        }

        DateTimeOffset SubscribedOn
        {
            get;
            set;

        }

        DateTimeOffset LastUpdatedOn
        {
            get;
            set;

        }

        bool IsComplex
        {
            get;
            set;

        }

        bool IsMultiLevel
        {
            get;
            set;

        }

        int EntityTypeid
        {
            get;
            set;

        }
        int FilterRoleOption
        {
            get;
            set;

        }
        
        

        #endregion
    }
}
