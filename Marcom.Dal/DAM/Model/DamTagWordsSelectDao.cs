﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
    public partial class DamTagWordsSelectDao : BaseDao, ICloneable
    {
                #region Member Variables

        protected int _id;
        protected int _assetid;
        protected int _attributeid;
        protected int _optionid;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
		public DamTagWordsSelectDao() {}

        public DamTagWordsSelectDao(int pId, int pAssetid, int pAttributeid, int pOptionid)
		{
            this._id = pId;
            this._assetid = pAssetid;
            this._optionid = pOptionid;
            this._attributeid = pAttributeid;
		}
		
      
		#endregion
		
		#region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int Assetid
		{
            get { return _assetid; }
            set { _bIsChanged |= (_assetid != value); _assetid = value; }
			
		}
        public virtual int Attributeid
        {
            get { return _attributeid; }
            set { _bIsChanged |= (_attributeid != value); _attributeid = value; }

        }
        public virtual int Optionid
		{
            get { return _optionid; }
            set { _bIsChanged |= (_optionid != value); _optionid = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string Id = "Id";
            public const string DamTagWordsSelectDao = "DamTagWordsSelectDao";
            public const string Attributeid = "Attributeid";
            public const string Assetid = "Assetid";
            public const string Optionid = "Optionid";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string Id = "Id";
            public const string DamTagWordsSelectDao = "DamTagWordsSelectDao";
            public const string Attributeid = "Attributeid";
            public const string Assetid = "Assetid";
            public const string Optionid = "Optionid";			
		}

		#endregion
    }
}