﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Task.Model
{
    public partial class EntityTaskDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _EntityID;
        protected int _MemberID;
        protected int _TaskType;
        protected string _Name;
        protected string _Description;
        protected int _TaskStatus;
        protected DateTime? _DueDate;
        protected int _EntityTaskListID;
        protected int _sortOrder;
        protected int _TaskListID;
        protected int _assetId;
        protected string _note;
        protected int _taskversionno;
        protected int _DalimID;
        protected int _CurrentPhaseId;
        protected DateTime? _UpdatedOn;
        protected int? _taskFlowId;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;

        #endregion

        #region Constructors
        public EntityTaskDao() { }

        public EntityTaskDao(int pTaskListID, int pEntityTaskListID, int pTaskType, DateTime? pDueDate, string pDescription, string pName, int pMemberID, int pEntityID, int pTaskStatus, int pSortOrder, int pAssetId, string pNote, int TaskVersionNo, int pDalimID, int pCurrentPhaseId, DateTime? pUpdatedOn, int? ptaskFlowId)
        {
            this._TaskListID = pTaskListID;
            this._EntityTaskListID = pEntityTaskListID;
            this._DueDate = pDueDate;
            this._Description = pDescription;
            this._Name = pName;
            this._TaskType = pTaskType;
            this._MemberID = pMemberID;
            this._EntityID = pEntityID;
            this._TaskStatus = pTaskStatus;
            this._sortOrder = pSortOrder;
            this._assetId = pAssetId;
            this._note = pNote;
            this._taskversionno = TaskVersionNo;
            this._DalimID = pDalimID;
            this._UpdatedOn = pUpdatedOn;
            this._CurrentPhaseId = pCurrentPhaseId;
            this._taskFlowId = ptaskFlowId;

        }

        public EntityTaskDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }
        public virtual int EntityID
        {
            get { return _EntityID; }
            set { _bIsChanged |= (_EntityID != value); _EntityID = value; }

        }
        public virtual int MemberID
        {
            get { return _MemberID; }
            set { _bIsChanged |= (_MemberID != value); _MemberID = value; }

        }

        public virtual int TaskStatus
        {
            get { return _MemberID; }
            set { _bIsChanged |= (_MemberID != value); _MemberID = value; }

        }

        public virtual int TaskType
        {
            get { return _TaskType; }
            set { _bIsChanged |= (_TaskType != value); _TaskType = value; }

        }

        public virtual string Name
        {
            get { return _Name; }
            set { _bIsChanged |= (_Name != value); _Name = value; }

        }

        public virtual int TaskVersionNo
        {
            get { return _taskversionno; }
            set { _bIsChanged |= (_taskversionno != value); _taskversionno = value; }

        }


        public virtual DateTime? DueDate
        {
            get { return _DueDate; }
            set { _bIsChanged |= (_DueDate != value); _DueDate = value; }
        }

        public virtual int EntityTaskListID
        {
            get { return _EntityTaskListID; }
            set { _bIsChanged |= (_EntityTaskListID != value); _EntityTaskListID = value; }
        }


        public virtual int TaskListID
        {
            get { return _TaskListID; }
            set { _bIsChanged |= (_TaskListID != value); _TaskListID = value; }
        }


        public virtual string Description
        {
            get { return _Description; }
            set { _bIsChanged |= (_Description != value); _Description = value; }

        }
        public virtual int Sortorder
        {
            get { return _sortOrder; }
            set { _bIsChanged |= (_sortOrder != value); _sortOrder = value; }

        }

        public virtual string Note
        {
            get { return _note; }
            set { _bIsChanged |= (_note != value); _note = value; }

        }

        public virtual int AssetId
        {
            get { return _assetId; }
            set { _bIsChanged |= (_assetId != value); _assetId = value; }

        }

        public virtual int DalimID
        {
            get { return _DalimID; }
            set { _bIsChanged |= (_DalimID != value); _DalimID = value; }

        }

        public virtual int? TaskFlowID
        {
            get { return _taskFlowId; }
            set { _bIsChanged |= (_taskFlowId != value); _taskFlowId = value; }

        }

        public virtual int CurrentPhaseId
        {
            get { return _CurrentPhaseId; }
            set { _bIsChanged |= (_CurrentPhaseId != value); _CurrentPhaseId = value; }

        }

        public virtual DateTime? UpdatedOn
        {
            get { return _UpdatedOn; }
            set { _bIsChanged |= (_UpdatedOn != value); _UpdatedOn = value; }
        }


        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string EntityTaskDao = "EntityTaskDao";
            public const string ID = "ID";
            public const string EntityID = "EntityID";
            public const string MemberID = "MemberID";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string TaskStatus = "TaskStatus";
            public const string DueDate = "DueDate";
            public const string EntityTaskListID = "EntityTaskListID";
            public const string TaskListID = "TaskListID";
            public const string TaskType = "TaskType";
            public const string Sortorder = "Sortorder";
            public const string AssetId = "AssetId";
            public const string Note = "Note";
            public const string TaskVersionNo = "TaskVersionNo";
            public const string DalimID = "DalimID";
            public const string UpdatedOn = "UpdatedOn";
            public const string CurrentPhaseId = "CurrentPhaseId";
            public const string TaskFlowID = "TaskFlowID";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string EntityTaskDao = "EntityTaskDao";
            public const string ID = "ID";
            public const string EntityID = "EntityID";
            public const string MemberID = "MemberID";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string TaskStatus = "TaskStatus";
            public const string DueDate = "DueDate";
            public const string EntityTaskListID = "EntityTaskListID";
            public const string TaskListID = "TaskListID";
            public const string TaskType = "TaskType";
            public const string Sortorder = "Sortorder";
            public const string AssetId = "AssetId";
            public const string Note = "Note";
            public const string TaskVersionNo = "TaskVersionNo";
            public const string DalimID = "DalimID";
            public const string UpdatedOn = "UpdatedOn";
            public const string CurrentPhaseId = "CurrentPhaseId";
            public const string TaskFlowID = "TaskFlowID";

        }

        #endregion


    }
}
