﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
   public interface IMilestoneMetadata
    {
        #region Public Properties
        int EntityId
        {
            get;
            set;

        }

       int ParentEntityId
        {
            get;
            set;

        }
       string  ParentEntityName
       {
           get;
           set;
       }

       string ParentEntityTypeShortDescription
       {
           get;
           set;
       }

       string ParentEntityTypeColorCode
       {
           get;
           set;
       }
       
        IList<IAttributeData> AttributeData
        {
            get;
            set;
        }

        #endregion
    }
}
