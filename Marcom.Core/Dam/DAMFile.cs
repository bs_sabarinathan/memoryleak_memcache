﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.DAM.Interface;

namespace BrandSystems.Marcom.DAM
{

    /// <summary>
    /// File object for table 'DAM_File'.
    /// </summary>

    internal class DAMFile : IDAMFile
    {
        #region Member Variables

        protected int _id;
        protected string _name;
        protected string _mimetype;
        protected string _extension;
        protected long _size;
        protected int _ownerid;
        protected DateTimeOffset _createdon;
        protected string _checksum;
        protected int _assetid;
        protected Guid _fileguid;
        protected string _OwnerName;
        protected string _description;
        protected int _versionNo;
        protected int _status;
        protected string _additionalinfo;
        protected int _ProcessType;
        #endregion

        #region Constructors
        public DAMFile() { }

        public DAMFile(string pName, string pMimeType, string pExtension, long pSize, int pOwnerid, DateTimeOffset pCreatedOn, string pChecksum, int pModuleid, int pAssetid, Guid pFileguid, string pOwnerName, string pDescription, int pStatus, int pVersionNo, string padditionalinfo, int pProcessType)
        {
            this._name = pName;
            this._mimetype = pMimeType;
            this._extension = pExtension;
            this._size = pSize;
            this._ownerid = pOwnerid;
            this._createdon = pCreatedOn;
            this._checksum = pChecksum;
            this._assetid = pAssetid;
            this._fileguid = pFileguid;
            this._OwnerName = pOwnerName;
            this._description = pDescription;
            this._versionNo = pVersionNo;
            this._additionalinfo = padditionalinfo;
            this._status = pStatus;
            this._ProcessType = pProcessType;
        }

        public DAMFile(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
                _name = value;
            }

        }



        public string MimeType
        {
            get { return _mimetype; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("MimeType", "MimeType value, cannot contain more than 250 characters");
                _mimetype = value;
            }

        }

        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
            }

        }

        public string Extension
        {
            get { return _extension; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Extension", "Extension value, cannot contain more than 50 characters");
                _extension = value;
            }

        }


        public long Size
        {
            get { return _size; }
            set { _size = value; }

        }

        public int Ownerid
        {
            get { return _ownerid; }
            set { _ownerid = value; }

        }

        public DateTimeOffset CreatedOn
        {
            get { return _createdon; }
            set { _createdon = value; }

        }

        public string Checksum
        {
            get { return _checksum; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Checksum", "Checksum value, cannot contain more than 50 characters");
                _checksum = value;
            }

        }


        public  Guid Fileguid
        {
            get { return _fileguid; }
            set { _fileguid = value; }

        }

        public  string OwnerName
        {
            get { return _OwnerName; }
            set { _OwnerName = value; }

        }

        public int AssetID
        {
            get { return _assetid; }
            set { _assetid = value; }
        }
        public string StrCreatedDate { get; set; }
        public string strFileID { get; set; }



        public string LinkURL { get; set; }

        public int VersionNo
        {
            get { return _versionNo; }
            set { _versionNo = value; }

        }
        public int Status { get; set; }

        public string Additionalinfo
        {
            get { return _additionalinfo; }
            set
            {
                _additionalinfo = value;
            }

        }
        public string StrCreatedDateTime { get; set; }
        public Boolean Activestatus { get; set; }
        public int ProcessType
        {
            get { return _ProcessType; }
            set { _ProcessType = value; }
        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            DAMFile castObj = null;
            try
            {
                castObj = (DAMFile)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.ID);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
