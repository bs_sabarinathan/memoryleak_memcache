﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;


namespace BrandSystems.Marcom.Core.Common
{
    internal class WidgetTypeRoles:IWidgetTypeRoles
    {
        #region Member Variables

		protected int _id;
        protected int _WidgetTypeID;
        protected int _RoleID;
		
		
		#endregion
		
		#region Constructors
		public WidgetTypeRoles() {}

        public WidgetTypeRoles(int pWidgetTypeID, int pRoleID)
		{
            this._WidgetTypeID = pWidgetTypeID;
            this._RoleID = pRoleID; 
			
		}

        public WidgetTypeRoles(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}

        public int WidgetTypeID
		{
            get { return _WidgetTypeID; }
            set { _WidgetTypeID = value; }
			
		}

        public int RoleID
		{
            get { return _RoleID; }
            set { _RoleID = value; }
			
		}
		
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            WidgetTypeRoles castObj = null;
			try
			{
                castObj = (WidgetTypeRoles)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
    }
}
