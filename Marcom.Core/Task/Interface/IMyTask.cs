﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Task.Interface
{
    public interface IMyTask
    {
        #region Public Properties

        int TaskId
        {
            get;
            set;

        }

        int TaskTypeId
        {
            get;
            set;
        }

        string TaskName
        {
            get;
            set;

        }

        string TaskDescription
        {
            get;
            set;

        }

        int EntityId
        {
            get;
            set;

        }

        string EntityName
        {
            get;
            set;

        }

        IList Path
        {
            get;
            set;

        }


        int RoleId
        {
            get;
            set;

        }

        string DueDate
        {
            get;
            set;

        }

        int Noofdays
        {
            get;
            set;

        }

        string UserName
        {
            get;
            set;

        }

        int UserID
        {
            get;
            set;

        }

        int IsAdminTask
        {
            get;
            set;

        }

        int TaskStatus
        {
            get;
            set;

        }
        string ProgressCount
        {
            get;
            set;

        }


        #endregion
    }
}
