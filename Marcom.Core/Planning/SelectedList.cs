﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Planning
{
    public class SelectedList
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }
}
