﻿(function (ng, app) {

    "use strict";

    //app.controller(
    //	"mui.planningtool.component.attributegroupCtrl",
    function attributegroupCtrl($scope, $timeout, $resource, $compile, $cookies, DamService, MetadataService) {   // I handle changes to the request context.




        //$scope.itemShowHideProgress = false;
        $scope.toggleObject = { item: 0 };
        $scope.PopularTagsToShow = 5;
        var addseelctpopularclass = "";
        $scope.newtagoption = [];
        var lastResults = [];
        $scope.TagOptionsLists = [];
        $scope.TagOptionsListsOnAplhabet = [];
        $scope.PopularTagWordsLists = [];
        $scope.tagalphabet = "A";
        $scope.monthback = new Date();
        $scope.monthback.setMonth($scope.monthback.getMonth() - 1);
        $scope.tempfilterval = [];
        $scope.textsearch = "";
        $scope.itemAttributehelptext = "";
        $scope.itemIsenable = false;
        $scope.alphabet = ["All", "#", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];

        //------------------------------> BIND TAG WORDS TO TAG CONTROL <----------------------------
        $scope.CallSelect2Tag = function () {
            $("#" + $scope.itemAttrid).select2({
                tags: $scope.TagOptionsLists,
                placeholder: "Enter tags",
                multiple: true,
                minimumInputLength: 1,
                formatResult: function (item) {
                    return item.text;
                },
                formatSelection: function (item) {
                    return item.text;
                },
                sortResults: function (results) {
                    return results.sort(function (a, b) {
                        var aName = a.text.toLowerCase();
                        var bName = b.text.toLowerCase();
                        return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                    })
                },
                createSearchChoice: function (term, data) {
                    if ($(data).filter(function () {
                      return this.text.toLowerCase().localeCompare(term.toLowerCase()) === 0;
                    }).length === 0) {
                        return { id: $.trim("0" + term.toLowerCase()), text: $.trim(term.toLowerCase()) };
                    }
                },
            }).on("change", function (e, data) {
                if (e.added) {
                    if (e.added.id[0] == "0") {
                        //bootbox.confirm("Are you sure you want to add new tag word? ", function (result) {
                        //if (result) {
                        $timeout(function () {
                            $scope.newtagoption = [];
                            $scope.newtagoption.push({ "ID": 0, "Caption": e.added.text.toLowerCase(), "CreatedBy": $cookies['Username'], "CreatedOn": ConvertDateFromStringToString(ConvertDateToString(new Date())), "SortOrder": ($scope.TagOptionsLists.length + 1) });

                            var addoptions = {};
                            //var AddOptions = $resource('metadata/InsertUpdateTagOption/');
                            //var addoptions = new AddOptions();
                            addoptions.TagOptions = $scope.newtagoption;
                            addoptions.AttributeID = 87;
                            addoptions.IsforAdmin = false;
                            addoptions.TotalPopulartags = $scope.PopularTagsToShow;
                            MetadataService.InsertUpdateTagOption(addoptions).then(function (addoptionsResponse) {
                                //var addoptionsResponse = AddOptions.save(addoptions, function () {
                                if (addoptionsResponse.StatusCode == 200 && addoptionsResponse.Response > 0) {
                                    $scope.itemTagwordId.push(addoptionsResponse.Response);
                                    $scope.TagOptionsLists.push({ "id": addoptionsResponse.Response, "text": e.added.text.toLowerCase(), "IsChecked": true });
                                    $scope.newtagoption[0].ID = addoptionsResponse.Response;
                                    $scope.itemTagwordList.push({ "Id": addoptionsResponse.Response, "Caption": e.added.text.toLowerCase() });

                                    $scope.searchfrtag({ attrid: 87, optionid: e.added.id, isselected: true });
                                }
                            });
                        }, 100);
                        //}
                        //});
                    }
                    else {
                        $scope.itemTagwordId.push(e.added.id);
                        $.grep($scope.TagOptionsLists, function (obj) { return obj.id == e.added.id; })[0].IsChecked = true;

                        $scope.searchfrtag({ attrid: 87, optionid: e.added.id, isselected: true });

                        addseelctpopularclass = $.grep($scope.PopularTagWordsLists, function (objpop) { return objpop.id == e.added.id })[0];
                        if (addseelctpopularclass != undefined && addseelctpopularclass != null) {
                            if (!$("#" + e.added.id + $scope.itemAttrid).hasClass("selected")) {
                                $("#" + e.added.id + $scope.itemAttrid).addClass("selected");
                            }
                        }


                    }
                }
                else if (e.removed) {
                    $.grep($scope.TagOptionsLists, function (obj) { return obj.id == e.removed.id; })[0].IsChecked = false;
                    $scope.itemTagwordId.splice($scope.itemTagwordId.indexOf(parseInt(e.removed.id)), 1);

                    $scope.searchfrtag({ attrid: SystemDefiendAttributes.TagOption, optionid: e.removed.id, isselected: false });

                    addseelctpopularclass = $.grep($scope.PopularTagWordsLists, function (obj) { return obj.id == e.removed.id })[0];
                    if (addseelctpopularclass != undefined && addseelctpopularclass != null) {
                        if ($("#" + e.removed.id + $scope.itemAttrid).hasClass("selected")) {
                            $("#" + e.removed.id + $scope.itemAttrid).removeClass("selected");


                        }
                    }
                    if ($("#EntityMetadata .error").find('directive-tagwords').length > 0) {
                        if ($scope.itemTagwordId.length == 0) {
                            $("#EntityMetadata .error").find('directive-tagwords').find('.validationmark').removeAttr("style");
                            $("#EntityMetadata .error").find('directive-tagwords').find('.validationmark').css({ 'color': '#B94A48' });
                        }
                    }
                    else {
                        if ($scope.itemTagwordId.length == 0) {
                            $("#EntityMetadata").find('directive-tagwords').find('.validationmark').css({ 'color': '#B94A48' });
                            $("#EntityMetadata").find('directive-tagwords').parent().parent().addClass("error");
                        }
                    }
                }
            });

            //$scope.itemShowHideProgress = true;
        }

        $scope.closetagpopup = function () {
            $(".tagWordsListPopup").modal('hide');
        }
        //-----------------------------> FILTER TAG WORDS BASED ON ALPHABET CLICK OR ON TEXT ENTER <----------------
        $scope.LoadTagswordsOnAplhabetClick = function (valaplha, tagID) {

            if ($scope.itemIsreadonly == "true") {
                return false;
            }
            $(".tagWordsListPopup").modal('show');
            if (valaplha == '#') {
                $scope.TagOptionsListsOnAplhabet = $scope.TagOptionsLists.filter(function (value) {
                    return value.text.charAt(0).match(/[a-z]/i) == null;
                });
            }
            else if (valaplha == 'All') {
                $scope.TagOptionsListsOnAplhabet = $scope.TagOptionsLists;
            }
            else {

                $scope.TagOptionsListsOnAplhabet = $scope.TagOptionsLists.filter(function (value) {
                    return value.text.charAt(0).toLowerCase() == valaplha.toLowerCase();
                });
            }

            $scope.tempfilterval = $scope.TagOptionsListsOnAplhabet;
        }

        //-----------------------------> GET ALL THE TAG WORD OPTIONS <-----------------------------
        MetadataService.GetTagOptionList(false).then(function (tagoptionlist) {
            //var GetTagOptionList = $resource('metadata/GetTagOptionList/:IsAdmin', { IsAdmin: false }, { get: { method: 'GET' } });
            //var tagoptionlist = GetTagOptionList.get({ IsAdmin: false }, function () {
            if (tagoptionlist.Response != null) {
                $scope.optionListSer = tagoptionlist.Response;
                $scope.itemTagwordList = tagoptionlist.Response;
                for (var i = 0; i < $scope.optionListSer.length; i++) {
                    $scope.TagOptionsLists.push({ "id": $scope.optionListSer[i].Id, "text": $scope.optionListSer[i].Caption, "IsChecked": false });
                }
                //----------------------DISPLAYING THE TAG WORDS IN ASCENDING ORDER-------------------//
                var arr = $scope.TagOptionsLists;
                arr.sort(function (a, b) {
                    var aName = a.text;
                    var bName = b.text;
                    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                });

                $scope.TagOptionsListsOnAplhabet = arr;

                var poptag = $.grep(tagoptionlist.Response, function (e) { return new Date(e.UsedOn) >= $scope.monthback });
                poptag = poptag.sort(function (a, b) { return parseInt(b.TotalHits) - parseInt(a.TotalHits) });
                $scope.PopularTagsToShow = tagoptionlist.Response[0].PopularTagsToShow;

                var poptaglength = poptag.length < tagoptionlist.Response[0].PopularTagsToShow ? poptag.length : tagoptionlist.Response[0].PopularTagsToShow;
                for (var i = 0; i < poptaglength; i++) {
                    $scope.PopularTagWordsLists.push({ "id": poptag[i].Id, "text": poptag[i].Caption });
                }
                $scope.CallSelect2Tag();

                if ($scope.itemTagwordId.length > 0) {
                    $timeout(function () {
                        for (var k = 0; k < $scope.itemTagwordId.length; k++) {
                            $("#" + $scope.itemTagwordId[k] + $scope.itemAttrid).addClass("selected");
                            var tagopt = $.grep($scope.TagOptionsLists, function (e) { return e.id == $scope.itemTagwordId[k]; });
                            if (tagopt != undefined && tagopt.length > 0) {
                                tagopt[0].IsChecked = true
                            }
                            else {
                                $scope.itemTagwordId.splice(k, 1);
                            }
                        }
                        $("#" + $scope.itemAttrid).select2('val', $scope.itemTagwordId);
                    }, 20);
                }
            }
            else
                $scope.CallSelect2Tag();
        });


        //---------------------> BIND POPULAR TAG WORDS TO TAG CONTROL <------------------
        $scope.BingPopularTagToTagwords = function (val) {
            if ($scope.itemIsreadonly == "true") {
                return false;
            }
            if (!$("#" + val + $scope.itemAttrid).hasClass("selected")) {
                $("#" + val + $scope.itemAttrid).addClass("selected");
                $scope.itemTagwordId.push(val);
                $("#" + $scope.itemAttrid).select2('val', $scope.itemTagwordId);
                $.grep($scope.TagOptionsLists, function (e) { return e.id == val; })[0].IsChecked = true;

                $scope.searchfrtag({ attrid: $scope.itemAttrid, optionid: val, isselected: true });
            }
            if($("#EntityMetadata .error").find('directive-tagwords').length > 0)
            {
                if ($scope.itemTagwordId.length > 0)
                {
                   $("#EntityMetadata .error").find('directive-tagwords').find('.validationmark').css({ 'color': '' });
                    $("#EntityMetadata .error").find('directive-tagwords').parent().parent().removeClass("error");
                }
            }
        }

        //-------------------> ADD TAG WORDS FROM POPUP TO TAG WORD CONTROL <----------------------
        $scope.AddSelectedTagsToTagWords = function () {
            var selectedtag = $.map($.grep($scope.TagOptionsLists, function (e) { return e.IsChecked == true; }), function (tagval) { return tagval.id; });
            if (selectedtag != null && selectedtag.length > 0) {
                $scope.itemTagwordId = selectedtag;
                $("#" + $scope.itemAttrid).select2('val', $scope.itemTagwordId);

                var unique = $.grep($scope.itemTagwordId, function (element) {
                    return $.inArray(element, $scope.PopularTagWordsLists) == -1;
                })

                if (unique.length > 0) {
                    for (var i = 0; i < $scope.PopularTagWordsLists.length; i++) {
                        $("#" + $scope.PopularTagWordsLists[i].id + $scope.itemAttrid).removeClass("selected");
                    }
                    for (var i = 0; i < unique.length; i++) {
                        $("#" + unique[i] + $scope.itemAttrid).addClass("selected");
                    }

                    $scope.searchfrtag({ attrid: $scope.itemAttrid, optionid: selectedtag[selectedtag.length - 1], isselected: true });
                }


            }
            $timeout(function () {
                $('#tagWordsListPopup_' + $scope.itemAttrid).modal('hide');
            }, 50);
        }

        //-------------------> TEXT BOX SEARCH <--------------------------

        var timer = 0;
        $timeout(function () {
            $("#Ontextsearch_" + $scope.itemAttrid).keyup(function (event) {
                clearTimeout(timer);
                timer = setTimeout(function () {
                    $timeout(function () {
                        if ($("#Ontextsearch_" + $scope.itemAttrid)[0].value == "") $scope.toggleObject.item = 0;
                        else $scope.toggleObject.item = $scope.alphabet.indexOf(String($("#Ontextsearch_" + $scope.itemAttrid)[0].value[0]).toUpperCase()) == -1 ? 1 : $scope.alphabet.indexOf(String($("#Ontextsearch_" + $scope.itemAttrid)[0].value[0]).toUpperCase());
                        $scope.TagOptionsListsOnAplhabet = [];
                        $scope.TagOptionsListsOnAplhabet = $.grep($scope.tempfilterval, function (element) {
                            return element.text.toLowerCase().indexOf($scope.textsearch.toLowerCase()) != -1;
                        })
                    }, 10);
                }, 500);

            });
        }, 1000);

        //$scope.select2Options = {
        //    tags: $scope.TagOptionsLists,
        //    placeholder: "Enter tags",
        //    multiple: true,
        //    formatResult: function (item) {
        //        return item.text;
        //    },
        //    formatSelection: function (item) {
        //        return item.text;
        //    },
        //    createSearchChoice: function (term) {
        //        console.log('createSearchChoice');
        //        var newid = (lastResults.some(function (r) { return r.Caption == term }) ? "" : "0" + term);
        //        return { id: newid, text: term };
        //    },

        //}

        $scope.$watch('itemCleartag', function () {
            if ($scope.itemCleartag == true || $scope.itemCleartag == "true") {
                $timeout(function () {
                    for (var i = 0; i < $scope.itemTagwordId.length; i++) {
                        $("#" + $scope.itemTagwordId[i] + $scope.itemAttrid).removeClass("selected");
                    }
                    for (var k = 0; k < $scope.TagOptionsLists.length; k++) {
                        $scope.TagOptionsLists[k].IsChecked = false;
                    }
                    $scope.itemTagwordId.length = 0;
                    $("#" + $scope.itemAttrid).select2("val", "");
                    $scope.itemCleartag = false;
                }, 100);
            }
        });

        $scope.$on("$destroy", function () {
            //need to change this line
            //RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentreCtrl']"));
        });
        // --- Initialize. ---------------------------------- //


        // ...


    }
    //);
    app.controller('mui.planningtool.component.tagwordsCtrl', ['$scope', '$timeout', '$resource', '$compile', '$cookies', 'DamService', 'MetadataService', attributegroupCtrl]);

    app.directive('directiveTagwords', [function () {
        return {
            restrict: 'EA',
            templateUrl: 'views/mui/planningtool/component/tagwords.html',
            controller: 'mui.planningtool.component.tagwordsCtrl',
            replace: false,
            transclude: true,
            scope: {
                itemAttrid: '@',
                itemTagwordId: '=',
                itemTagwordList: '=',
                itemShowHideProgress: '=',
                searchfrtag: '&',
                itemCleartag: '=',
                itemAttributehelptext: '@',
                itemIsenable: '@',
                itemIsreadonly: '@'
            }
        }
    }]);
})(angular, app);