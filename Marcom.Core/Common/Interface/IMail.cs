using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IMail interface for table 'CM_Mail'.
    /// </summary>
    public interface IMail
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }
        string ToMail
        {
            get;
            set;

        }

        string Subject
        {
            get;
            set;

        }

        string Body
        {
            get;
            set;

        }

        DateTimeOffset ActualTime
        {
            get;
            set;

        }


        int NoOfTrial
        {
            get;
            set;

        }

        DateTimeOffset SentTime
        {
            get;
            set;

        }
        DateTimeOffset LastProccessedTime
        {
            get;
            set;

        }
        
        string Status
        {
            get;
            set;

        }
          int mailtype
        {
            get;
            set;

        }
          bool isrecapmail
          {
              get;
              set;

          }

          DateTimeOffset RecapTimeStamp
          {
              get;
              set;

          }

          DateTimeOffset RecapTime
          {
              get;
              set;

          }
         
          bool IsRecapSent
          {
              get;
              set;

          }
        #endregion
    }
}
