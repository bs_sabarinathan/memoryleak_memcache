﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// Supplier interface for table 'PM_Supplier'.
    /// </summary>
    public interface ISupplier
    {
        int ID { get; set; }
        string CompanyName { get; set; }
        string Department { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string City { get; set; }
        string ZipCode { get; set; }
        string State { get; set; }
        string Country { get; set; }
        string Vat { get; set; }
        string Phone { get; set; }
        string Email { get; set; }
        string Fax { get; set; }
        string CustomerNumber { get; set; }
    
    }
}
