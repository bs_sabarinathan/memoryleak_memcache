﻿(function (ng, app) {
    app.directive('compile', ['$compile', function ($compile) {
        return function (scope, element, attrs) {
            scope.$watch(function (scope) {
                return scope.$eval(attrs.compile);
            }, function (value) {
                element.html(value);
                $compile(element.contents())(scope);
            });
        };
    }]);
    //app.directive('attributeGroup', [function () {
    //    return {
    //        restrict: 'EA',
    //        templateUrl: 'views/mui/planningtool/component/attributegroup.html',
    //        controller: 'mui.planningtool.component.attributegroupCtrl',
    //        replace: true,
    //        scope: {
    //            itemIndex: '@',
    //            itemGroupId: '@',
    //            itemTitle: '@',
    //            itemProcessedNumber: '=itemProcessed',
    //            itemIsLock: '@',
    //            itemLanguageContent: '=',
    //            itemImagecrop: '=',
    //            itemImagefilename: '@',
    //            itemPredefined: '@',
    //            itemPagesize: '@'
    //        }
    //    }
    //}]);
    //app.directive('attributegroupIndetailblock', [function () {
    //    return {
    //        restrict: 'EA',
    //        templateUrl: 'views/mui/planningtool/component/attributegroupIndetailblock.html',
    //        controller: 'mui.planningtool.component.attributegroupCtrl',
    //        replace: true,
    //        scope: {
    //            itemIndex: '@',
    //            itemGroupId: '@',
    //            itemTitle: '@',
    //            itemProcessedNumber: '=itemProcessed',
    //            itemIsLock: '@',
    //            itemLanguageContent: '=',
    //            itemImagecrop: '=',
    //            itemImagefilename: '@',
    //            itemPredefined: '@',
    //            itemPagesize: '@'
    //        }
    //    }
    //}]);
    //app.directive('attributeGrouplistview', [function () {
    //    return {
    //        restrict: 'EA',
    //        templateUrl: 'views/mui/planningtool/component/attributegroupListView.html',
    //        controller: 'mui.planningtool.component.attributegroupCtrl',
    //        replace: true,
    //        scope: {
    //            itemIndex: '@',
    //            itemGroupId: '@',
    //            itemTitle: '@',
    //            itemProcessedNumber: '=itemProcessed',
    //            itemIsLock: '@',
    //            itemLanguageContent: '=',
    //            itemImagecrop: '=',
    //            itemImagefilename: '@',
    //            itemPredefined: '@',
    //            itemPagesize: '@'
    //        }
    //    }
    //}]);
    //app.directive('attributeGrouplistviewindetail', [function () {
    //    return {
    //        restrict: 'EA',
    //        templateUrl: 'views/mui/planningtool/component/attributegroupListViewInDetailBlock.html',
    //        controller: 'mui.planningtool.component.attributegroupCtrl',
    //        replace: true,
    //        scope: {
    //            itemIndex: '@',
    //            itemGroupId: '@',
    //            itemTitle: '@',
    //            itemProcessedNumber: '=itemProcessed',
    //            itemIsLock: '@',
    //            itemLanguageContent: '=',
    //            itemImagecrop: '=',
    //            itemImagefilename: '@',
    //            itemPredefined: '@',
    //            itemPagesize: '@'
    //        }
    //    }
    //}]);
    //app.directive('attributeGrouplistganttviewincustomtab', [function () {
    //    return {
    //        restrict: 'EA',
    //        replace: false,
    //        transclude: true,
    //        templateUrl: 'views/mui/planningtool/component/attributegroupListGanttViewInCustomTab.html',
    //        controller: 'mui.planningtool.component.attributegrouplistganttviewincustomtabCtrl',
    //        scope: {
    //            itemIndex: '@',
    //            itemGroupId: '@',
    //            itemTitle: '@',
    //            itemProcessedNumber: '=itemProcessed',
    //            itemIsLock: '@',
    //            itemLanguageContent: '=',
    //            itemImagecrop: '=',
    //            itemImagefilename: '@',
    //            itemPredefined: '@',
    //            itemPagesize: '@'
    //        }
    //    }
    //}]);

    //app.directive('attributegroupIntabcreation', [function () {
    //    return {
    //        restrict: 'EA',
    //        templateUrl: 'views/mui/planningtool/component/attributegroupInCreationTab.html',
    //        controller: 'mui.planningtool.component.attributegroupInCreationTabCtrl',
    //        replace: true,
    //        scope: {
    //            itemGroupId: '@',
    //            itemTitle: '@',
    //            itemPredefined: '@',
    //            itemLanguageContent: '=',
    //            itemImagecrop: '=',
    //            itemImagefilename: '@',
    //            itemAddedattrgrps: '='
    //        }

    //    }
    //}]);

    /* This directive added to tagwords-controller */

    //app.directive('directiveTagwords', [function () {
    //    return {
    //        restrict: 'EA',
    //        templateUrl: 'views/mui/planningtool/component/tagwords.html',
    //        controller: 'mui.planningtool.component.tagwordsCtrl',
    //        replace: false,
    //        transclude: true,
    //        scope: {
    //            itemAttrid: '@',
    //            itemTagwordId: '=',
    //            itemTagwordList: '=',
    //            itemShowHideProgress: '=',
    //            searchfrtag: '&',
    //            itemCleartag: '='
    //        }
    //    }
    //}]);

    app.directive('ngblur', [function () {
        return function (scope, element, attrs) {
            if (element.length > 0) {
                element[0].onblur = function () {
                    eval(attrs.ngblur);
                };
            }
        };
    }]);
    app.directive('ngfocus', [function () {
        return function (scope, element, attrs) {
            if (element.length > 0) {
                element[0].onfocus = function () {
                    eval(attrs.ngfocus);
                };
            };
        };
    }]);
    app.directive('ngload', [function () {
        return function (scope, element, attrs) {
            if (element.length > 0) {
                element[0].justload = function () {
                    eval(attrs.ngload);
                };
                element[0].justload();
            };
        };
    }]);
    app.directive('xuserinfoeditable', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var loadXuserinfoeditable = function () {
                    angular.element(element).editable({
                        display: function (value, srcData) {
                            if ((value === "") || (value === undefined)) value = "-";
                            ngModel.$setViewValue(value);
                            element.html(value);
                            scope.$apply();
                        }
                    });
                }
                $timeout(function () {
                    loadXuserinfoeditable();
                }, 10);
                angular.element(element).on('save', function (e, params) {
                    scope.saveuserdetailsinfo(attrs.attributeid, params.newValue);
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditablusergender', ['$timeout', '$compile', function ($timeout, $compile) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var loadXeditable = function () {
                    angular.element(element).editable({
                        display: function (value, srcData) {
                            var status = '';
                            ngModel.$setViewValue(value);
                            if (value == "1") status = 'Reached';
                            else status = 'Not Reached';
                            if (!(value instanceof Date)) element.html(status);
                            scope.$apply();
                        }
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('save', function (e, params) { });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditable44', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var loadXeditable2 = function () {
                    angular.element(element).editable({
                        display: function (value, srcData) {
                            ngModel.$setViewValue(value);
                            scope.$apply();
                        }
                    });
                }
                $timeout(function () {
                    loadXeditable2();
                }, 10);
                angular.element(element).on('save', function (e, params) {
                    scope.saveuserdetailsinfo(attrs.attributeid, params.newValue.toString('dd/MM/yyyy'));
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditabledatepicker', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var loadXeditable2 = function () {
                    angular.element(element).editable({
                        display: function (value, srcData) {
                            ngModel.$setViewValue(value);
                            scope.$apply();
                        }
                    });
                }
                $timeout(function () {
                    loadXeditable2();
                }, 10);
                angular.element(element).on('save', function (e, params) {
                    scope.saveuserdetailsinfo(attrs.attributeid, params.newValue.toString('dd/MM/yyyy'));
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditabledropdownlanguage', ['$timeout', '$compile', '$resource', '$window', 'MuidirectiveService', function ($timeout, $compile, $resource, $window, MuidirectiveService) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel, cookies) {
                var Version = 1;
                var langid = attrs.langid;
                var UserID = attrs.userid;
                var isDataTypeExists = false;
                MuidirectiveService.GetLanguageTypes().then(function (languagetypes) {
                    $window.XeditableDirectivesSource["languagetext"] = languagetypes.Response;
                    scope.normaltreeSources["languagetext"] = languagetypes.Response;
                });
                for (var variable in $.fn.editabletypes) {
                    if (langid == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(langid, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopup = '';
                    inlinePopup += '<div><label><select ui-select2 data-ng-model="languagetext" name="language settings" class="ngcompileDropdown">';
                    inlinePopup += '<option value=\"{{ndata.ID}}\" ng-repeat=\"ndata in normaltreeSources.languagetext\">{{ndata.Name}}</option></select></label></div>';
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopup,
                        inputclass: ''
                    });
                    $.fn.editabletypes['marcomlangage'] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompileDropdown'))(scope);
                            scope['languagetext'] = (($.grep(scope.normaltreeSources["languagetext"], function (e) {
                                return e.Name == scope.fields['languagetext']
                            }))[0]).ID;
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    scope.treeSelection = [];
                    var dropdowntext = 'languagetext';
                    var langnewid = scope.languagetext;
                    scope.saveLanguageUserSettings(attrs.userid, langnewid, scope['languagetext']);
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditabletreedropdownmypage', ['$timeout', '$compile', '$resource', '$window', 'MuidirectiveService', function ($timeout, $compile, $resource, $window, MuidirectiveService) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var levelCount = scope.treelevels['dropdown_levels_' + attrs.attributeid].length
                var isDataTypeExists = false;
                if (attrs.choosefromparent == "false") {
                    MuidirectiveService.GetTreeNode(attrs.attributeid).then(function (GetTree) {
                        scope.treeSources["dropdown_" + attrs.attributeid] = JSON.parse(GetTree.Response);
                        DesignXeditable();
                    });
                } else {
                    MuidirectiveService.GetTreeNodeByEntityID(attrs.attributeid, attrs.entityid).then(function (GetTree) {
                        scope.treeSources["dropdown_" + attrs.attributeid] = JSON.parse(GetTree.Response);
                        DesignXeditable();
                    });
                }

                function DesignXeditable() {
                    MuidirectiveService.GetTreeNode(attrs.attributeid).then(function (GetTree) {
                        scope.treeSources["dropdown_" + attrs.attributeid] = JSON.parse(GetTree.Response);
                        for (var variable in $.fn.editabletypes) {
                            if (attributeName == variable) isDataTypeExists = true;
                        }
                        for (var i = 1; i <= levelCount; i++) {
                            if (i == 1) {
                                scope['ddtoption_' + attrs.attributeid + '_' + i] = {};
                                scope['ddtoption_' + attrs.attributeid + '_' + i].multiple = false;
                                scope['ddtoption_' + attrs.attributeid + '_' + i].data = scope.treeSources['dropdown_' + attrs.attributeid].Children;
                                scope['ddtoption_' + attrs.attributeid + '_' + i].formatResult = function (item) {
                                    return item.Caption
                                };
                                scope['ddtoption_' + attrs.attributeid + '_' + i].formatSelection = function (item) {
                                    return item.Caption
                                };
                            } else {
                                scope['ddtoption_' + attrs.attributeid + '_' + i] = {};
                                scope['ddtoption_' + attrs.attributeid + '_' + i].multiple = false;
                                scope['ddtoption_' + attrs.attributeid + '_' + i].data = [];
                                scope['ddtoption_' + attrs.attributeid + '_' + i].formatResult = function (item) {
                                    return item.Caption
                                };
                                scope['ddtoption_' + attrs.attributeid + '_' + i].formatSelection = function (item) {
                                    return item.Caption
                                };
                            }
                        }
                        if (isDataTypeExists === false) {
                            var Address = function (options) {
                                this.init(attributeName, options, Address.defaults);
                            };
                            $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                            var inlinePopupDesign1 = '<table>';
                            for (var i = 1; i <= levelCount; i++) {
                                if (i == 1) {
                                    var levelcaption = scope.treelevels['dropdown_levels_' + attrs.attributeid][i - 1].caption;
                                    inlinePopupDesign1 += '<tr><td><label><span>' + levelcaption + ': </span></label></td><td><label>';
                                    inlinePopupDesign1 += '<input ui-select2=\"ddtoption_' + attrs.attributeid + '_' + i + '\" type="hidden"  data-ng-model=\"dropdown_' + attrs.attributeid + '_' + i + '\" class=\"ngcompile\" ng-change=\"BindChildDropdownSource(' + attrs.attributeid + ',' + levelCount + ',' + i + ', 6)\" />';
                                    inlinePopupDesign1 += '</label></td></tr>';
                                } else {
                                    var levelcaption = scope.treelevels['dropdown_levels_' + attrs.attributeid][i - 1].caption;
                                    inlinePopupDesign1 += '<tr><td>  <label><span>' + levelcaption + ': </span></label></td><td><label>';
                                    inlinePopupDesign1 += '<input ui-select2=\"ddtoption_' + attrs.attributeid + '_' + i + '\"  data-ng-model=\"dropdown_' + attrs.attributeid + '_' + i + '\" class=\"ngcompile\" ng-change=\"BindChildDropdownSource(' + attrs.attributeid + ',' + levelCount + ',' + i + ', 6)\" />';
                                    inlinePopupDesign1 += '</label></td></tr>';
                                }
                            }
                            inlinePopupDesign1 += '</table>';
                            Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                                tpl: inlinePopupDesign1,
                                inputclass: ''
                            });
                            $.fn.editabletypes[attributeName] = Address;
                        }
                        var loadXeditable = function () {
                            angular.element(element).editable({
                                title: attrs.title,
                                display: function (value, srcData) { },
                                savenochange: true
                            });
                        }
                        $timeout(function () {
                            loadXeditable();
                        }, 10);
                        angular.element(element).on('shown', function (e, editable) {
                            $timeout(function () {
                                if (editable != undefined) {
                                    $compile($('.ngcompile'))(scope);
                                    for (var i = 1; i <= levelCount; i++) {
                                        var dropdown_text = 'dropdown_text_' + attrs.attributeid + '_' + i;
                                        if (i == 1) {
                                            scope['dropdown_' + attrs.attributeid + '_' + i] = ($.grep(scope.treeSources["dropdown_" + attrs.attributeid].Children, function (e) {
                                                return e.Caption.toString() == scope.treeTexts[dropdown_text].toString().trim()
                                            }))[0];
                                        } else {
                                            if (scope['dropdown_' + attrs.attributeid + '_' + (i - 1)] != undefined) {
                                                if (scope['dropdown_' + attrs.attributeid + '_' + (i - 1)].Children != undefined) {
                                                    scope.BindChildDropdownSource((attrs.attributeid), levelCount, (i - 1), 6);
                                                    scope['dropdown_' + attrs.attributeid + '_' + i] = ($.grep(scope['dropdown_' + attrs.attributeid + '_' + (i - 1)].Children, function (e) {
                                                        return e.Caption.toString() == scope.treeTexts[dropdown_text].toString().trim()
                                                    }))[0];
                                                }
                                            }
                                        }
                                    }
                                }
                            }, 10);
                        });
                        angular.element(element).on('save', function (e, params) {
                            scope.treeSelection = [];
                            for (var i = 1; i <= levelCount; i++) {
                                var dropdown_text = 'DropDown_' + attrs.attributeid + '_' + i;
                                if ((scope['dropdown_' + attrs.attributeid + '_' + i] != undefined) && (scope['dropdown_' + attrs.attributeid + '_' + i] != "")) {
                                    scope.fields[dropdown_text] = scope['dropdown_' + attrs.attributeid + '_' + i];
                                    if (scope['dropdown_' + attrs.attributeid + '_' + i].id == undefined || scope['dropdown_' + attrs.attributeid + '_' + i].id == "") {
                                        scope.treeSelection.push(0);
                                    } else {
                                        scope.treeSelection.push(scope['dropdown_' + attrs.attributeid + '_' + i].id);
                                    }
                                    scope.treeTexts['dropdown_text_' + attrs.attributeid + '_' + i] = scope['dropdown_' + attrs.attributeid + '_' + i].Caption;
                                } else {
                                    scope.fields[dropdown_text] = '-';
                                    scope.treeSelection.push(0);
                                    scope.treeTexts['dropdown_text_' + attrs.attributeid + '_' + i] = '-';
                                }
                                scope.$apply();
                            }
                            scope.saveDropdownTreeFromMyPage(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope.treeSelection);
                        });
                    });
                }
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditabletextmypage', ['$timeout', '$compile', '$resource', '$window', function ($timeout, $compile, $resource, $window) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var CurrentEntityID = parseInt(attrs.entityid, 10);
                var attributeID = attrs.attributeid;
                var Address = function (options) {
                    this.init(attributeID, options, Address.defaults);
                };
                $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                var inlinePopup = '';
                if (parseInt(attrs.attributetypeid) == 1) inlinePopup += '<div><label><input class="ngcompile" type="text" id="SingleTextXeditalble' + attrs.attributeid + '" data-ng-model="SingleTextValue_' + attrs.attributeid + '"></label></div>';
                else if (parseInt(attrs.attributetypeid) == 2) inlinePopup += '<div><label><textarea class="ngcompile" id="SingleTextXeditalble' + attrs.attributeid + '" data-ng-model="SingleTextValue_' + attrs.attributeid + '" ></textarea></label></div>';
                Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                    tpl: inlinePopup,
                    inputclass: ''
                });
                $.fn.editabletypes[attributeID] = Address;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.originalTitle,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompile'))(scope);
                            scope['SingleTextValue_' + attrs.attributeid] = scope.fields['SingleLineTextValue_' + attrs.attributeid];
                            var isValidationReq = "";
                            if (scope.listAttributeValidationResult != undefined) {
                                isValidationReq = $.grep(scope.listAttributeValidationResult, function (e) {
                                    return (e.AttributeID == parseInt(attrs.attributeid));
                                });
                            }
                            $("#overviewDetialblock").removeClass('warning');
                            if (isValidationReq != "" && isValidationReq != undefined) {
                                if (isValidationReq.length > 0 && isValidationReq[0].RelationShipID != 0) {
                                    var metrics = [
										['#SingleTextXeditalble' + attrs.attributeid + '', isValidationReq[0].ValueType, isValidationReq[0].ErrorMessage]
                                    ];
                                    var options = {
                                        'groupClass': 'warning',
                                    };
                                    $("#overviewDetialblock").nod(metrics, options);
                                }
                            }
                            $timeout(function () {
                                $('#SingleTextXeditalble' + attrs.attributeid).focus().select()
                            }, 10);
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    scope.fields['SingleLineTextValue_' + attrs.attributeid] = scope['SingleTextValue_' + attrs.attributeid].toString().trim() == "" ? "-" : scope['SingleTextValue_' + attrs.attributeid];
                    scope.saveDropdownTreeFromMyPage(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope['SingleTextValue_' + attrs.attributeid]);
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditabledropdownmypage', ['$timeout', '$compile', '$resource', '$window', 'MuidirectiveService', function ($timeout, $compile, $resource, $window, MuidirectiveService) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var Version = 1;
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var isDataTypeExists = false;
                MuidirectiveService.GetOptionDetailListByIDForMyPage(attrs.attributeid, scope.UserId).then(function (optionlist) {
                    $window.XeditableDirectivesSource["NormalDropDown_" + attrs.attributeid] = optionlist.Response;
                    scope.normaltreeSources["NormalDropDown_" + attrs.attributeid] = optionlist.Response;
                });
                for (var variable in $.fn.editabletypes) {
                    if (attributeName == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(attributeName, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopup = '';
                    inlinePopup += '<div><label><select ui-select2 data-ng-model="NormalDropDown_' + attrs.attributeid + '" name="city" class="ngcompileDropdown"><option ng-repeat=\"ndata in normaltreeSources.NormalDropDown_' + attrs.attributeid + '\" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></label></div>';
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopup,
                        inputclass: ''
                    });
                    $.fn.editabletypes[attributeName] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompileDropdown'))(scope);
                            var tempvar = ($.grep(scope.normaltreeSources["NormalDropDown_" + attrs.attributeid], function (e) {
                                return e.Caption == scope.fields['NormalDropDown_' + attrs.attributeid]
                            }))[0];
                            if (tempvar != undefined) scope['NormalDropDown_' + attrs.attributeid] = tempvar.Id;
                            else scope['NormalDropDown_' + attrs.attributeid] = "";
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    scope.treeSelection = [];
                    var dropdowntext = 'NormalDropDown_' + attrs.attributeid;
                    if (scope['NormalDropDown_' + attrs.attributeid] != undefined) {
                        var tempvar = ($.grep(scope.normaltreeSources["NormalDropDown_" + attrs.attributeid], function (e) {
                            return parseInt(e.Id) == parseInt(scope['NormalDropDown_' + attrs.attributeid])
                        }))[0];
                        scope.fields[dropdowntext] = tempvar.Caption;
                        scope.treeSelection.push(tempvar.Id);
                        scope.$apply();
                    } else {
                        scope.fields[dropdowntext] = '-';
                        scope.treeSelection.push(0);
                        scope.$apply();
                    }
                    scope.saveDropdownTreeFromMyPage(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope.treeSelection);
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditablemultiselectdropdownmypage', ['$timeout', '$compile', '$resource', '$window', 'MuidirectiveService', function ($timeout, $compile, $resource, $window, MuidirectiveService) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var Version = 1;
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var isDataTypeExists = false;
                MuidirectiveService.GetOptionDetailListByIDForMyPage(attrs.attributeid, scope.UserId).then(function (optionlist) {
                    $window.XeditableDirectivesSource["NormalMultiDropDown_" + attrs.attributeid] = optionlist.Response;
                    scope.normaltreeSources["NormalMultiDropDown_" + attrs.attributeid] = optionlist.Response;
                });
                for (var variable in $.fn.editabletypes) {
                    if (attributeName == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(attributeName, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopup = '';
                    //inlinePopup += '<div><label><select ui-select2 id="NormalMultiDropDown1_' + attrs.attributeid + '" multiple=\"multiple\" data-ng-model="NormalMultiDropDown_' + attrs.attributeid + '" name="city" class="ngcompileDropdown">';
                    //inlinePopup += '<option ng-option=\"ndata.Id as ndata.Caption for ndata in normaltreeSources.NormalMultiDropDown_' + attrs.attributeid + '\"</select></label></div>';

                    inlinePopup += '<div><label> \
                                                <select multiselect-dropdown ng-options="ndata.Id as ndata.Caption for ndata in normaltreeSources.NormalMultiDropDown_' + attrs.attributeid + '\" id="NormalMultiDropDown1_' + attrs.attributeid + '" multiple=\"multiple\" data-ng-model="NormalMultiDropDown_' + attrs.attributeid + '" name="city" class="ngcompileDropdown multiselect"> \
                                                </select> \
                                            </label></div>';
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopup,
                        inputclass: ''
                    });
                    $.fn.editabletypes[attributeName] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);

                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompileDropdown'))(scope);
                            var captions = scope.fields['NormalMultiDropDown_' + attrs.attributeid].split(',')
                            scope['NormalMultiDropDown_' + attrs.attributeid] = [];
                            var tempvar = [];
                            for (var x = 0; x <= captions.length; x++) {
                                if (captions[x] != undefined) {
                                    tempvar.push(($.grep(scope.normaltreeSources["NormalMultiDropDown_" + attrs.attributeid], function (e) {
                                        return e.Caption.trim() == captions[x].trim()
                                    }))[0]);
                                    if (tempvar[x] != null) {
                                        scope['NormalMultiDropDown_' + attrs.attributeid].push(tempvar[x].Id);
                                    }
                                }
                            }
                            if (scope.listAttributeValidationResult != undefined) {
                                var isValidationReq = $.grep(scope.listAttributeValidationResult, function (e) {
                                    return (e.AttributeID == parseInt(attrs.attributeid));
                                });
                                $("#overviewDetialblock").removeClass('warning');
                                if (isValidationReq.length > 0 && isValidationReq[0].RelationShipID != 0) {
                                    var metrics = [
										['#NormalMultiDropDown1_' + attrs.attributeid + '', isValidationReq[0].ValueType, isValidationReq[0].ErrorMessage]
                                    ];
                                    var options = {
                                        'groupClass': 'warning',
                                    };
                                    $("#overviewDetialblock").nod(metrics, options);
                                }
                            }
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    scope.treeSelection = [];
                    var dropdowntext = 'NormalMultiDropDown_' + attrs.attributeid;
                    var tempcap = [];
                    if (scope['NormalMultiDropDown_' + attrs.attributeid] != undefined && scope['NormalMultiDropDown_' + attrs.attributeid].length > 0) {
                        var tempvar = [];
                        var tempval = scope['NormalMultiDropDown_' + attrs.attributeid];
                        for (var x = 0; x < tempval.length; x++) {
                            tempvar.push(($.grep(scope.normaltreeSources["NormalMultiDropDown_" + attrs.attributeid], function (e) {
                                return parseInt(e.Id) == parseInt(tempval[x]);
                            }))[0]);
                        }
                        for (var j = 0; j < tempvar.length; j++) {
                            tempcap.push(tempvar[j].Caption);
                            scope.treeSelection.push(tempvar[j].Id);
                            scope.$apply();
                        }
                        scope.fields['NormalMultiDropDown_' + attrs.attributeid] = tempcap.join(',');
                    } else {
                        scope.fields[dropdowntext] = '-';
                        scope.treeSelection.push(0);
                        scope.$apply();
                    }
                    scope.saveDropdownTreeFromMyPage(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope.treeSelection);
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditablemultiselecttreedropdownmypage', ['$timeout', '$compile', '$resource', '$window', 'MuidirectiveService', function ($timeout, $compile, $resource, $window, MuidirectiveService) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var levelCount = scope.treelevels['multiselectdropdown_levels_' + attrs.attributeid].length
                var isDataTypeExists = false;
                if (attrs.choosefromparent == "false") {
                    MuidirectiveService.GetTreeNode().then(function (GetTree) {
                        scope.treeSources["multiselectdropdown_" + attrs.attributeid] = JSON.parse(GetTree.Response);
                        DesignXeditable();
                    });
                } else {
                    MuidirectiveService.GetTreeNodeByEntityID(attrs.attributeid, attrs.entityid).then(function () {
                        scope.treeSources["multiselectdropdown_" + attrs.attributeid] = JSON.parse(GetTree.Response);
                        DesignXeditable();
                    });
                }

                function DesignXeditable() {
                    for (var variable in $.fn.editabletypes) {
                        if (attributeName == variable) isDataTypeExists = true;
                    }
                    for (var i = 1; i <= levelCount; i++) {
                        scope['ddtoption_' + attrs.attributeid + '_' + i] = {};
                        if (i == 1) {
                            scope['ddtoption_' + attrs.attributeid + '_' + i].multiple = false;
                            scope['ddtoption_' + attrs.attributeid + '_' + i].data = scope.treeSources['multiselectdropdown_' + attrs.attributeid].Children;
                        } else {
                            if (i == levelCount) scope['ddtoption_' + attrs.attributeid + '_' + i].multiple = true;
                            else scope['ddtoption_' + attrs.attributeid + '_' + i].multiple = false;
                            scope['ddtoption_' + attrs.attributeid + '_' + i].data = [];
                        }
                        scope['ddtoption_' + attrs.attributeid + '_' + i].formatResult = function (item) {
                            return item.Caption
                        };
                        scope['ddtoption_' + attrs.attributeid + '_' + i].formatSelection = function (item) {
                            return item.Caption
                        };
                    }
                    if (isDataTypeExists === false) {
                        var Address = function (options) {
                            this.init(attributeName, options, Address.defaults);
                        };
                        $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                        var inlinePopupDesign1 = '';
                        inlinePopupDesign1 += '<table>';
                        for (var i = 1; i <= levelCount; i++) {
                            var levelcaption = scope.treelevels['multiselectdropdown_levels_' + attrs.attributeid][i - 1].caption;
                            if (i == 1) {
                                var levelcaption = scope.treelevels['multiselectdropdown_levels_' + attrs.attributeid][i - 1].caption;
                                inlinePopupDesign1 += '<tr><td><label><span>' + levelcaption + ': </span></label></td><td><label>';
                                inlinePopupDesign1 += '<input ui-select2=\"ddtoption_' + attrs.attributeid + '_' + i + '\" type="hidden"  data-ng-model=\"multiselectdropdown_' + attrs.attributeid + '_' + i + '\" class=\"ngcompile\" ng-change=\"BindChildDropdownSource(' + attrs.attributeid + ',' + levelCount + ',' + i + ', 12)\" />';
                                inlinePopupDesign1 += '</label></td></tr>';
                            } else {
                                var levelcaption = scope.treelevels['multiselectdropdown_levels_' + attrs.attributeid][i - 1].caption;
                                inlinePopupDesign1 += '<tr><td>  <label><span>' + levelcaption + ': </span></label></td><td><label>';
                                inlinePopupDesign1 += '<input ui-select2=\"ddtoption_' + attrs.attributeid + '_' + i + '\" type="hidden"  data-ng-model=\"multiselectdropdown_' + attrs.attributeid + '_' + i + '\" class=\"ngcompile\" ng-change=\"BindChildDropdownSource(' + attrs.attributeid + ',' + levelCount + ',' + i + ', 12)\" />';
                                inlinePopupDesign1 += '</label></td></tr>';
                            }
                        }
                        inlinePopupDesign1 += '</table>';
                        Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                            tpl: inlinePopupDesign1,
                            inputclass: ''
                        });
                        $.fn.editabletypes[attributeName] = Address;
                    }
                    var loadXeditable = function () {
                        angular.element(element).editable({
                            title: attrs.title,
                            display: function (value, srcData) { },
                            savenochange: true
                        });
                    }
                    $timeout(function () {
                        loadXeditable();
                    }, 10);
                    angular.element(element).on('shown', function (e, editable) {
                        $timeout(function () {
                            if (editable != undefined) {
                                $compile($('.ngcompile'))(scope);
                                for (var i = 1; i <= levelCount; i++) {
                                    var dropdown_text = 'multiselectdropdown_text_' + attrs.attributeid + '_' + i;
                                    if (i == 1) {
                                        scope['multiselectdropdown_' + attrs.attributeid + '_' + i] = ($.grep(scope.treeSources["multiselectdropdown_" + attrs.attributeid].Children, function (e) {
                                            return e.Caption.toString() == scope.treeTexts[dropdown_text].toString().trim()
                                        }))[0];
                                    } else {
                                        if (scope['multiselectdropdown_' + attrs.attributeid + '_' + (i - 1)] != undefined) {
                                            if (scope['multiselectdropdown_' + attrs.attributeid + '_' + (i - 1)].Children != undefined) {
                                                scope.BindChildDropdownSource((attrs.attributeid), levelCount, (i - 1), 12);
                                                if (i == levelCount) {
                                                    var tempcaption = scope.treeTexts[dropdown_text].toString().trim().split(',');
                                                    scope['multiselectdropdown_' + attrs.attributeid + '_' + i] = [];
                                                    var tempval = [];
                                                    for (var k = 0; k < tempcaption.length; k++) {
                                                        if (scope['multiselectdropdown_' + attrs.attributeid + '_' + (i - 1)].Children != undefined) scope['multiselectdropdown_' + attrs.attributeid + '_' + i].push(($.grep(scope['multiselectdropdown_' + attrs.attributeid + '_' + (i - 1)].Children, function (e) {
                                                            return e.Caption.toString() == tempcaption[k].trim();
                                                        }))[0]);
                                                    }
                                                } else {
                                                    scope['multiselectdropdown_' + attrs.attributeid + '_' + i] = ($.grep(scope['multiselectdropdown_' + attrs.attributeid + '_' + (i - 1)].Children, function (e) {
                                                        return e.Caption.toString() == scope.treeTexts[dropdown_text].toString().trim()
                                                    }))[0];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }, 10);
                    });
                    angular.element(element).on('save', function (e, params) {
                        scope.treeSelection = [];
                        for (var i = 1; i <= levelCount; i++) {
                            var dropdown_text = 'MultiSelectDropDown_' + attrs.attributeid + '_' + i;
                            if (i == levelCount) {
                                if ((scope['multiselectdropdown_' + attrs.attributeid + '_' + i] != undefined) && (scope['multiselectdropdown_' + attrs.attributeid + '_' + i] != "")) {
                                    var tempval = [];
                                    for (var k = 0; k < (scope['multiselectdropdown_' + attrs.attributeid + '_' + i].length) ; k++) {
                                        scope.fields[dropdown_text] = scope['multiselectdropdown_' + attrs.attributeid + '_' + i][k];
                                        if (scope['multiselectdropdown_' + attrs.attributeid + '_' + i][k].id == undefined || scope['multiselectdropdown_' + attrs.attributeid + '_' + i][k].id == "") {
                                            scope.treeSelection.push(0);
                                        } else {
                                            scope.treeSelection.push(scope['multiselectdropdown_' + attrs.attributeid + '_' + i][k].id);
                                        }
                                        tempval.push(scope['multiselectdropdown_' + attrs.attributeid + '_' + i][k].Caption);
                                    }
                                    if (tempval.length > 0) scope.treeTexts['multiselectdropdown_text_' + attrs.attributeid + '_' + i] = tempval.join(", ");
                                    else scope.treeTexts['multiselectdropdown_text_' + attrs.attributeid + '_' + i] = '-';
                                } else {
                                    scope.fields[dropdown_text] = '-';
                                    scope.treeSelection.push(0);
                                    scope.treeTexts['multiselectdropdown_text_' + attrs.attributeid + '_' + i] = '-';
                                }
                            } else {
                                if ((scope['multiselectdropdown_' + attrs.attributeid + '_' + i] != undefined) && (scope['multiselectdropdown_' + attrs.attributeid + '_' + i] != "")) {
                                    scope.fields[dropdown_text] = scope['multiselectdropdown_' + attrs.attributeid + '_' + i];
                                    if (scope['multiselectdropdown_' + attrs.attributeid + '_' + i].id == undefined || scope['multiselectdropdown_' + attrs.attributeid + '_' + i].id == "") {
                                        scope.treeSelection.push(0);
                                    } else {
                                        scope.treeSelection.push(scope['multiselectdropdown_' + attrs.attributeid + '_' + i].id);
                                    }
                                    scope.treeTexts['multiselectdropdown_text_' + attrs.attributeid + '_' + i] = scope['multiselectdropdown_' + attrs.attributeid + '_' + i].Caption;
                                } else {
                                    scope.fields[dropdown_text] = '-';
                                    scope.treeSelection.push(0);
                                    scope.treeTexts['multiselectdropdown_text_' + attrs.attributeid + '_' + i] = '-';
                                }
                            }
                            scope.$apply();
                        }
                        scope.saveDropdownTreeFromMyPage(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope.treeSelection);
                    });
                }
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditabletreedropdown1mypage', ['$timeout', '$compile', '$resource', '$window', function ($timeout, $compile, $resource, $window) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var attributeName = attrs.attributename;
                var periodstartdateid = attrs.periodstartdateId;
                var attrID = attrs.attributeid;
                var isDataTypeExists = false;
                for (var variable in $.fn.editabletypes) {
                    if (attributeName == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(attributeName, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopupDesign1 = '';
                    inlinePopupDesign1 += '<div class="row-fluid"><div class="inputHolder">';
                    inlinePopupDesign1 += '<div class="ngcompile"><input placeholder="-- Start date --" id="' + periodstartdateid + '" data-ng-model="fields.PeriodStartDate_Dir_' + attrID + '" type="text"  data-date-format="' + Defaultdate + '" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open"' + attrID + ']  ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open"' + attrID + '"\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" required="" name="startDate" class="ng-pristine ng-invalid ng-invalid-required"></div>';
                    inlinePopupDesign1 += '</div></div>';
                    $scope.fields["DatePart_Calander_Open" + attrID] = false;
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopupDesign1,
                        inputclass: ''
                    });
                    $.fn.editabletypes[attributeName] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) { });
                angular.element(element).on('save', function (e, params) {
                    scope.savePeriodVal(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, attrs.primaryid);
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditabletreedropdown1234', ['$timeout', '$compile', '$resource', '$window', function ($timeout, $compile, $resource, $window) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var attributeName = attrs.attributename + attrs.primaryid;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var periodstartdateid = attrs.periodstartdateId;
                var periodenddateid = attrs.periodenddateId;
                var attrID = attrs.primaryid;
                var isDataTypeExists = false;
                for (var variable in $.fn.editabletypes) {
                    if (attributeName == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(attributeName, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopupDesign1 = '';
                    inlinePopupDesign1 += '<div class="row-fluid"><div class="inputHolder">';
                    inlinePopupDesign1 += '<div class="ngcompile"><input placeholder="" id="' + periodstartdateid + '" data-ng-model="fields.DateTime_Dir_' + attrID + '" type="text"  data-date-format="' + Defaultdate + '" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open"' + attrID + ']  ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open"' + attrID + '"\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  required="" name="startDate" class="ng-pristine ng-invalid ng-invalid-required"></div>';
                    inlinePopupDesign1 += '</div></div>';
                    $scope.fields["DatePart_Calander_Open" + attrID] = false;
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopupDesign1,
                        inputclass: ''
                    });
                    $.fn.editabletypes[attributeName] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompile'))(scope);
                            if (scope.fields["DateTime_" + attrID] != "" && scope.fields["DateTime_" + attrID] != "-") scope.fields["DateTime_Dir_" + attrID] = scope.fields["DateTime_" + attrID];
                            else scope.fields["DateTime_Dir_" + attrID] = null;
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    if (scope.fields["DateTime_Dir_" + attrID] == null && scope.fields["DateTime_Dir_" + attrID] == undefined) scope.fields["DateTime_Dir_" + attrID] = "";
                    scope.saveDropdownTreeFromMyPage(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, attrs.attributeid);
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.service('uuid', [function () {
        this.generate = function () {
            var uuid = "",
				i, random;
            for (i = 0; i < 32; i++) {
                random = Math.random() * 16 | 0;
                if (i == 8 || i == 12 || i == 16 || i == 20) {
                    uuid += "-"
                }
                uuid += (i == 12 ? 4 : (i == 16 ? (random & 3 | 8) : random)).toString(16);
            }
            return uuid;
        };
    }]);
    app.directive('onRepeatLast', [function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var isLast = scope.$last || scope.$parent.$last;
                if (isLast) {
                    scope.$evalAsync(attrs.onRepeatLast);
                }
            }
        }
    }]);
    app.directive('gridster', ['$timeout', function ($timeout) {
        return {
            restrict: 'AC',
            scope: {
                widgets: '=',
                onremove: '&',
                onedit: '&'
            },
            templateUrl: 'views/mui/dashboard/gridster.html',
            controller: 'gridsterCtrl',
            link: function (scope, elm, attrs, ctrl) {
                elm.css('opacity', 0);
                scope.initGrid = function () {
                    $timeout(function () {
                        var $ul = ctrl.$el.find('#dashboardBlock');
                        var newDimensions = ctrl.calculateNewDimensions();
                        var MaxWidth = (newDimensions[0][0] + (newDimensions[1][0] * 2)) * ctrl.options.cols;
                        $ul.width(MaxWidth);
                        ctrl.options.widget_margins = newDimensions[1];
                        ctrl.options.widget_base_dimensions = newDimensions[0];
                        ctrl.gridster = $ul.gridster(ctrl.options).data('gridster');
                        ctrl.hookWidgetResizer();
                        scope.$broadcast('gridReady');
                        ctrl.resizeWidgetDimensions(true);
                        elm.css('opacity', 1);
                    });
                };
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('widget', ['$timeout', '$window', '$resource', 'uuid', 'MuidirectiveService', function ($timeout, $window, $resource, uuid, MuidirectiveService) {
        return {
            restrict: 'A',
            require: '^gridster',
            scope: {
                remove: '&',
                editwid: '&',
                widget: '='
            },
            link: function (scope, element, attrs, ctrl) {
                var $el = $(element);
                if (scope.widget.id === undefined) {
                    scope.widget.id = uuid.generate();
                }
                scope.$on('gridReady', function (event) {
                    var base_size = ctrl.gridster.options.widget_base_dimensions;
                    var margins = ctrl.gridster.options.widget_margins;
                    var headerHeight = $el.find('header').outerHeight();
                    var $content = $el.find('.content');
                    $el.resizable({
                        grid: [base_size[0] + (margins[0] * 2), base_size[1] + (margins[1] * 2)],
                        animate: false,
                        containment: ctrl.$el,
                        autoHide: false,
                        start: function (event, ui) {
                            var base_size = ctrl.gridster.options.widget_base_dimensions;
                            var margins = ctrl.gridster.options.widget_margins;
                            element.resizable('option', 'grid', [base_size[0] + (margins[0] * 2), base_size[1] + (margins[1] * 2)]);
                        },
                        resize: function (event, ui) {
                            $content.outerHeight($el.innerHeight() - headerHeight);
                        },
                        stop: function (event, ui) {
                            $content.outerHeight($el.innerHeight() - headerHeight);
                            setTimeout(function () {
                                sizeToGrid($el);
                                ctrl.updateModel();
                            }, 300);
                        }
                    });
                    $('.ui-resizable-handle, .no-drag, .disabled, [disabled]', $el).hover(function () {
                        ctrl.gridster.disable();
                    }, function () {
                        ctrl.gridster.enable();
                    });
                });

                function sizeToGrid($el) {
                    var base_size = ctrl.gridster.options.widget_base_dimensions;
                    var margins = ctrl.gridster.options.widget_margins;
                    var w = (parseInt($el.width()) - base_size[0]);
                    var h = (parseInt($el.height()) - base_size[1]);
                    for (var grid_w = 1; w > 0; w -= ((base_size[0] + (margins[0] * 2)))) {
                        grid_w++;
                    }
                    for (var grid_h = 1; h > 0; h -= ((base_size[1] + (margins[1] * 2)))) {
                        grid_h++;
                    }
                    $el.css({
                        width: '',
                        height: '',
                        top: '',
                        left: '',
                        position: ''
                    });
                    ctrl.gridster.resize_widget($el, grid_w, grid_h);
                    var newDimensions = ctrl.calculateNewDimensions();
                    $('#dynamicWidgets li[data-widget-id="' + $el.attr("data-widget-id").replace(/(\r\n|\n|\r)/gm, "") + '"]').each(function () {
                        $('.box-content', this).height((newDimensions[0][0] * grid_h) - 32);
                        $('.graph', this).height($('.box-content', this).height() - 20);
                        var chartid = $(this).find('.graph').attr("id");
                        if (chartid != undefined) {
                            $(document).trigger("OnChartLoad", chartid);
                        }
                    });
                    var TempId = $window.TemplateID;
                    var Wizaid = $el.attr("data-widget-Realid").replace(/(\r\n|\n|\r)/gm, "");
                    var colval = $el.attr("data-col").replace(/(\r\n|\n|\r)/gm, "");
                    var rowval = $el.attr("data-row").replace(/(\r\n|\n|\r)/gm, "");
                    var sizex = grid_w;
                    var sizey = grid_h;
                    var widgetData1 = [];
                    var gridData1 = {
                        col: parseInt(colval),
                        row: parseInt(rowval),
                        sizex: grid_w,
                        sizey: grid_h
                    };
                    widgetData1.push({
                        grid: gridData1,
                        widgtid: Wizaid,
                        templateid: $window.TemplateID
                    });
                    var editwidgetresize = {};
                    editwidgetresize.widgetdatalist = widgetData1;
                    if ($window.ISTemplate == true) {
                        editwidgetresize.IsAdminPage = true;
                    } else {
                        editwidgetresize.IsAdminPage = false;
                    }
                    MuidirectiveService.WidgetDragEditing(editwidgetresize).then(function (saveusercomment1) { });
                }
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('activityItem', ['$compile', '$http', '$templateCache', function ($compile, $http, $templateCache) {
        var templateText, templateLoader, baseURL = 'views/mui/',
			typeTemplateMapping = {
			    NewsFeed: 'dashboard/NewsFeedWidget.html',
			    Milestone: 'dashboard/MilestoneWidhet.html',
			    Workspace: 'dashboard/WorkspaceWidget.html',
			    topxactivity: 'dashboard/TopxActivityWidget.html',
			    CostCenterFinancial: 'dashboard/CostCenterWidget.html',
			    ActivityFinancial: 'dashboard/FinancialSummaryWidget.html',
			    Notification: 'dashboard/NotificationWidget.html',
			    Browser: 'dashboard/BrowserWidget.html',
			    BrowserVersion: 'dashboard/BrowserVersionWidget.html',
			    OS: 'dashboard/OSWidget.html',
			    CountryName: 'dashboard/CountryNameWidget.html',
			    MyTask: 'mytask.html',
			    FundingRequest: 'myFundingRequest.html',
			    PublishedAsset: 'dashboard/PublishedAssets.html',
			    DownloadedAsset: 'dashboard/DownloadedAssets.html',
			    UploadAsset: 'dashboard/UploadAssets.html'
			};
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            compile: function (tElement, tAttrs, transclusionFunc) {
                var tplURL = baseURL + typeTemplateMapping[tAttrs.type];
                templateLoader = $http.get(tplURL, {
                    cache: $templateCache
                }).success(function (html) {
                    tElement.html(html);
                });
                return function (scope, element, attrs) {
                    templateLoader.then(function (templateText) {
                        element.html($compile(tElement.html())(scope));
                    });
                    scope.type = attrs.type;
                    scope.metrics = attrs.matrixid;
                    scope.DimensionID = attrs.dimensionid;
                    scope.visualType = attrs.visualtype;
                    scope.WizardID = attrs.wizardid;
                    scope.WizardTypeID = attrs.wizardtypeid;
                    scope.WizardRealID = attrs.wizardrealid;
                    scope.WizardNoOfItem = attrs.noofitem;
                    scope.WizardNoOfYear = attrs.noofyear;
                    scope.WizardNoOfMonth = attrs.noofmonth;
                };
            },
        };
    }])
    app.directive('refreshui', function ($location, $route, $window, $templateCache) {
        return function (scope, element, attrs) {
            element.bind('click', function () {
                if (element[0] && element[0].href && element[0].href !== $location.absUrl()) {
                    $window.location.reload();
                }
                if (element[0] && element[0].href && element[0].href === $location.absUrl()) {
                    $route.reload();
                }
            });
        }
    });

    app.filter('orderObjectBy', function () {
        return function (items, field, reverse) {
            var filtered = [];
            angular.forEach(items, function (item) {
                filtered.push(item);
            });
            function index(obj, i) {
                for (var i = 0; i < obj.length; i++) {
                    if (obj[i]["Lable"] == field) {
                        return obj[i]["Caption"];
                    }
                }
            }
            filtered.sort(function (a, b) {
                var comparator;
                var reducedA = field.split('.').reduce(index, a);
                var reducedB = field.split('.').reduce(index, b);
                if (reducedA === reducedB) {
                    comparator = 0;
                } else {
                    comparator = (reducedA > reducedB ? 1 : -1);
                }
                return comparator;
            });
            if (reverse) {
                filtered.reverse();
            }
            return filtered;
        };
    });
    app.directive("myqtooltip", ['$compile', '$parse', function ($compile, $parse) {

        return function (scope, element, attrs) {

            var attrslst = attrs;
            $(element).qtip({

                content: {
                    text: '',
                },
                events: {
                    show: function (event, api) {
                        var qtipContent_id = api.elements.content.attr('id');
                        var res = $.grep(scope.Financialhelptext, function (e) {
                            return e.ID == attrslst.tooltipid;
                        });
                        if (res.length > 0) {
                            if (res[0].IsHelptextEnabled == false)
                                return false;
                            else
                                scope.DecodeHelptextDescription(qtipContent_id, attrslst.tooltipid);
                        }
                        else
                            return false;
                    }
                },
                style: {
                    classes: scope.qtipSkin + " qtip-dark qtip-shadow qtip-rounded",
                    title: { 'display': 'none' }
                },
                position: {
                    my: 'bottom left',
                    at: 'top left',
                    viewport: $(window),
                    adjust: {
                        method: 'shift',
                        mouse: true
                    },
                    corner: {
                        target: 'bottom left',
                        tooltip: 'bottom left',
                        mimic: 'top'
                    },
                    target: 'mouse'

                }


            });

            scope.$on("$destroy", function () {
                $(element).qtip('destroy', true); // Immediately destroy all tooltips belonging to the selected elements
                $(window).off("resize.Viewport"); //$destory Method which can be used to clean up DOM bindings before an element is removed from the DOM.
            });
        }
    }]);
})(angular, app);

//mui-directive services

(function (ng, app) {
    "use strict";

    function MuidirectiveService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            GetLanguageTypes: GetLanguageTypes,
            GetTreeNode: GetTreeNode,
            GetTreeNodeByEntityID: GetTreeNodeByEntityID,
            GetOptionDetailListByIDForMyPage: GetOptionDetailListByIDForMyPage,
            WidgetDragEditing: WidgetDragEditing
        });

        function GetLanguageTypes() {
            var request = $http({
                method: "get",
                url: "api/common/GetLanguageTypes/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreeNode(AttributeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreeNode/" + AttributeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreeNodeByEntityID(AttributeID, ParentEntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreeNodeByEntityID/" + AttributeID + "/" + ParentEntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionDetailListByIDForMyPage(ID, UserID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOptionDetailListByIDForMyPage/" + ID + "/" + UserID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function WidgetDragEditing(widgetdatalist) {
            var request = $http({
                method: "post",
                url: "api/common/WidgetDragEditing/",
                params: {
                    action: "add",
                },
                data: widgetdatalist
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.service("MuidirectiveService", ['$http', '$q', MuidirectiveService]);
})(angular, app);