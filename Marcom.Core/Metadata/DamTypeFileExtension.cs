﻿/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

    /// <summary>
    /// DamTypeFileExtension object for table 'MM_DamType_FileExtension'.
    /// </summary>

    internal class DamTypeFileExtension : IDamTypeFileExtension
    {
        #region Member Variables

        protected int _id;
        protected int _entitytypeid;
        protected string _ExtensionOptions;
        protected int _sortorder;
        protected bool _isdeleted;
        #endregion

        #region Constructors
        public DamTypeFileExtension() { }

        public DamTypeFileExtension(int pID, int pEntityTypeID, string pExtensionOptions, int pSortOrder, bool pIsDeleted)
        {
            this._id = pID;
            this._entitytypeid = pEntityTypeID;
            this._ExtensionOptions = pExtensionOptions;
            this._sortorder = pSortOrder;
            this._isdeleted = pIsDeleted;
        }

        public DamTypeFileExtension(int pID)
        {
            this._id = pID;
        }

        #endregion

        #region Public Properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public int EntityTypeID
        {
            get { return _entitytypeid; }
            set
            {
                _entitytypeid = value;
            }

        }

        public string ExtensionOptions
        {
            get { return _ExtensionOptions; }
            set
            {
                _ExtensionOptions = value;
            }

        }
        public int SortOrder
        {
            get { return _sortorder; }
            set
            {
                _sortorder = value;
            }

        }
        public bool IsDeleted
        {
            get { return _isdeleted; }
            set
            {
                _isdeleted = value;
            }

        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            DamTypeFileExtension castObj = null;
            try
            {
                castObj = (DamTypeFileExtension)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.ID);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
