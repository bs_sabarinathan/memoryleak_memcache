﻿(function (ng, app) {
    "use strict"; function muiadminreformatprofilectrl($scope, $location, $resource, AdminService, $timeout, $cookies, $window, $compile, $translate) {
        $scope.mimes = {}; $scope.profileCollection = [{ "ID": "-1", "Name": "Create New" }]; $scope.Unitsshortname = "px"; $scope.ProfileResData = []; $scope.obj = { Crop: { "Cropenable": false, "Aspetratiow": 0, "Aspetratioh": 0 }, CropResize: { "CropResizeenable": false, "Height": 0, "Width": 0 }, Formate: { "Formateenable": false, "mime": "jpg", "Dpi": 0 }, Roundcorner: { "Roundcornerenable": false, "Topleft": 0, "Topright": 0, "Bottomleft": 0, "Bottomright": 0, "cornercolour": "" } }; GetMimeTypevalue(); $scope.UnitsCollection = [{ "ID": "1", "Name": "Pixels", "shortname": "px" }, { "ID": "2", "Name": "Inches", "shortname": "inch" }, { "ID": "3", "Name": "Millimeter", "shortname": "mm" }]; $scope.profprofile = -1; $scope.profUnits = 1; $scope.perviousUnitsshortname = "px"; $scope.obj.Roundcorner.cornercolour = ''; $scope.ColorOptions = { preferredFormat: "hex", showInput: true, showAlpha: false, allowEmpty: true, showPalette: true, showPaletteOnly: false, togglePaletteOnly: true, togglePaletteMoreText: 'more', togglePaletteLessText: 'less', showSelectionPalette: true, chooseText: "Choose", cancelText: "Cancel", showButtons: true, clickoutFiresChange: true, palette: [["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"], ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)", "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"], ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)", "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)", "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)", "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]] }; $scope.colorchange = function (color) { }
        $timeout(function () {
            AdminService.GetReformatProfile(-1).then(function (result) {
                if (result.Response == 0) { }
                else {
                    $scope.ProfileResData = $.grep(result.Response, function (e) { return e.ID != -1; }); if ($scope.ProfileResData != undefined) {
                        for (var k = 0, data; data = $scope.ProfileResData[k++];) { $scope.profileCollection.push({ "ID": data["ID"], "Name": data["Name"] }); }
                        $timeout(function () { $scope.profprofile = $scope.ProfileResData[0].ID; $scope.profprofilename = $scope.ProfileResData[0].Name; $scope.profUnits = $scope.ProfileResData[0].Units; }, 50); $scope.ProfileSrcData = JSON.parse($scope.ProfileResData[0].ProfileSrcValue).ActionSrcValue.ProfileSrcValue; if ($scope.ProfileSrcData != undefined) { $scope.obj.Crop = $scope.ProfileSrcData.Crop; $scope.obj.CropResize = $scope.ProfileSrcData.CropResize; $scope.obj.Formate = $scope.ProfileSrcData.Formate; $scope.obj.Roundcorner = $scope.ProfileSrcData.Roundcorner; }
                    }
                    else { $scope.Unitsshortname = "px"; $scope.ProfileResData = []; $scope.profprofile = -1; $scope.profUnits = 1; }
                }
            });
        }, 100); $scope.profilechange = function (profile) {
            if ($scope.ProfileResData != undefined && profile > 0) { var currProfileResData = $.grep($scope.ProfileResData, function (e) { return e.ID == profile; }); $timeout(function () { $scope.profprofile = currProfileResData[0].ID; $scope.profprofilename = currProfileResData[0].Name; $scope.profUnits = currProfileResData[0].Units; var taskActionSrc = $.grep($scope.UnitsCollection, function (e) { return e.ID == $scope.profUnits; }); $scope.Unitsshortname = taskActionSrc[0].shortname; }, 50); $scope.ProfileSrcData = JSON.parse(currProfileResData[0].ProfileSrcValue).ActionSrcValue.ProfileSrcValue; if ($scope.ProfileSrcData != undefined) { $scope.obj.Crop = $scope.ProfileSrcData.Crop; $scope.obj.CropResize = $scope.ProfileSrcData.CropResize; $scope.obj.Formate = $scope.ProfileSrcData.Formate; $scope.obj.Roundcorner = $scope.ProfileSrcData.Roundcorner; } }
            else if (profile == -1) { $scope.profprofilename = ""; $scope.profUnits = 1; $scope.Unitsshortname = 'px'; $scope.obj = { Crop: { "Cropenable": false, "Aspetratiow": 0, "Aspetratioh": 0 }, CropResize: { "CropResizeenable": false, "Height": 0, "Width": 0 }, Formate: { "Formateenable": false, "mime": "jpg", "Dpi": 0 }, Roundcorner: { "Roundcornerenable": false, "Topleft": 0, "Topright": 0, "Bottomleft": 0, "Bottomright": 0, "cornercolour": "" } }; }
        }
        function GetMimeTypevalue() { AdminService.GetMimeType().then(function (data) { if (data.Response != null) { $scope.mimes = data.Response; } }); }
        $scope.RestCropvalues = function () { $scope.obj.Crop = { "Cropenable": false, "Aspetratiow": 0, "Aspetratioh": 0 }; }
        $scope.RestCropResize = function () { $scope.obj.CropResize = { "CropResizeenable": false, "Height": 0, "Width": 0 }; }
        $scope.RestFormate = function () { $scope.obj.Formate = { "Formateenable": false, "mime": "jpg", "Dpi": 0 }; }
        $scope.RestRoundcorner = function () { $scope.obj.Roundcorner = { "Roundcornerenable": false, "Topleft": 0, "Topright": 0, "Bottomleft": 0, "Bottomright": 0, "cornercolour": "" }; }
        $scope.Unitschange = function (Units, result) {
            if (result) { if (Units == 2 || Units == 3 || Units == 1) { if ($scope.obj.Formate.Dpi == 0 && ($scope.profprofile > 0 || $scope.obj.CropResize.Width > 0 || $scope.obj.CropResize.Height > 0 || $scope.obj.Roundcorner.Topleft > 0 || $scope.obj.Roundcorner.Topright || $scope.obj.Roundcorner.Bottomleft || $scope.obj.Roundcorner.Bottomright)) { bootbox.alert($translate.instant('LanguageContents.Res_1118.Caption')); return false; } } }
            var taskActionSrc = $.grep($scope.UnitsCollection, function (e) { return e.ID == Units; }); $scope.Unitsshortname = taskActionSrc[0].shortname; if ($scope.obj.Formate.Dpi > 0) {
                if ($scope.perviousUnitsshortname != $scope.Unitsshortname) {
                    if ($scope.Unitsshortname == 'px') { $scope.obj.CropResize.Width = ConvertToPixels($scope.obj.Formate.Dpi, $scope.obj.CropResize.Width, $scope.perviousUnitsshortname); $scope.obj.CropResize.Height = ConvertToPixels($scope.obj.Formate.Dpi, $scope.obj.CropResize.Height, $scope.perviousUnitsshortname); $scope.obj.Roundcorner.Topleft = ConvertToPixels($scope.obj.Formate.Dpi, $scope.obj.Roundcorner.Topleft, $scope.perviousUnitsshortname); $scope.obj.Roundcorner.Topright = ConvertToPixels($scope.obj.Formate.Dpi, $scope.obj.Roundcorner.Topright, $scope.perviousUnitsshortname); $scope.obj.Roundcorner.Bottomleft = ConvertToPixels($scope.obj.Formate.Dpi, $scope.obj.Roundcorner.Bottomleft, $scope.perviousUnitsshortname); $scope.obj.Roundcorner.Bottomright = ConvertToPixels($scope.obj.Formate.Dpi, $scope.obj.Roundcorner.Bottomright, $scope.perviousUnitsshortname); }
                    else { $scope.obj.CropResize.Width = ChangeUnit($scope.obj.Formate.Dpi, $scope.obj.CropResize.Width, $scope.Unitsshortname).toFixed(1); $scope.obj.CropResize.Height = ChangeUnit($scope.obj.Formate.Dpi, $scope.obj.CropResize.Height, $scope.Unitsshortname).toFixed(1); $scope.obj.Roundcorner.Topleft = ChangeUnit($scope.obj.Formate.Dpi, $scope.obj.Roundcorner.Topleft, $scope.Unitsshortname).toFixed(1); $scope.obj.Roundcorner.Topright = ChangeUnit($scope.obj.Formate.Dpi, $scope.obj.Roundcorner.Topright, $scope.Unitsshortname).toFixed(1); $scope.obj.Roundcorner.Bottomleft = ChangeUnit($scope.obj.Formate.Dpi, $scope.obj.Roundcorner.Bottomleft, $scope.Unitsshortname).toFixed(1); $scope.obj.Roundcorner.Bottomright = ChangeUnit($scope.obj.Formate.Dpi, $scope.obj.Roundcorner.Bottomright, $scope.Unitsshortname).toFixed(1); }
                    $scope.perviousUnitsshortname = $scope.Unitsshortname;
                }
            }
        }
        function ChangeUnit(intdpi, Exitingvalue, Convertype) {
            var output; if (Convertype == "inch") { output = Exitingvalue / intdpi; }
            else if (Convertype == "mm") { output = Exitingvalue / intdpi; output = output * 25.4; }
            return output;
        }
        function ConvertToPixels(intdpi, Exitingvalue, ExistingType) {
            var output; if (ExistingType == "inch") { output = Math.round(intdpi * Exitingvalue).toFixed(0); }
            else if (ExistingType == "mm") { output = Math.round((Exitingvalue * intdpi) / 25.4).toFixed(0); }
            return output;
        }
        $scope.Saveprofiles = function () {
            if ($('#btnSaveprofile').hasClass('disabled')) { return; }
            $('#btnSaveprofile').addClass('disabled'); if ($scope.profUnits == 2 || $scope.profUnits == 3 || $scope.profUnits == 1) { if ($scope.obj.Formate.Dpi == 0) { bootbox.alert($translate.instant('LanguageContents.Res_1118.Caption')); $('#btnSaveprofile').removeClass('disabled'); return false; } }
            $scope.SaveReformatProfile = []; var Saveprofile = {}; Saveprofile.ProfileID = $scope.profprofile; Saveprofile.Name = $scope.profprofilename; Saveprofile.Units = $scope.profUnits; Saveprofile.ProfileSrcValue = $scope.obj; $scope.SaveReformatProfile = []; $scope.SaveReformatProfile.push({ "ProfileID": Saveprofile.ProfileID, "Name": Saveprofile.Name, "Units": Saveprofile.Units, "ProfileSrcValue": { "ActionSrcValue": Saveprofile } }); var SaveReformatProfile = { SaveReformatProfile: $scope.SaveReformatProfile[0] }; AdminService.CreateReformatProfile(SaveReformatProfile).then(function (SaveprofileResult) {
                if (SaveprofileResult.StatusCode == 405) { $('#btnSaveprofile').removeClass('disabled'); NotifyError($translate.instant('LanguageContents.Res_1120.Caption')); }
                else {
                    if (SaveprofileResult.Response == false)
                    { $('#btnSaveprofile').removeClass('disabled'); NotifyError($translate.instant('LanguageContents.Res_1120.Caption')); }
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_1119.Caption')); $scope.profileCollection = [{ "ID": "-1", "Name": "Create New" }]; $timeout(function () {
                            AdminService.GetReformatProfile(-1).then(function (result) {
                                if (result.Response == 0) { }
                                else {
                                    $scope.ProfileResData = $.grep(result.Response, function (e) { return e.ID != -1; }); if ($scope.ProfileResData != undefined) {
                                        for (var k = 0, data; data = $scope.ProfileResData[k++];) { $scope.profileCollection.push({ "ID": data["ID"], "Name": data["Name"] }); }
                                        $timeout(function () { $scope.profprofile = $scope.ProfileResData[0].ID; $scope.profprofilename = $scope.ProfileResData[0].Name; $scope.profUnits = $scope.ProfileResData[0].Units; }, 50); $scope.ProfileSrcData = JSON.parse($scope.ProfileResData[0].ProfileSrcValue).ActionSrcValue.ProfileSrcValue; if ($scope.ProfileSrcData != undefined) { $scope.obj.Crop = $scope.ProfileSrcData.Crop; $scope.obj.CropResize = $scope.ProfileSrcData.CropResize; $scope.obj.Formate = $scope.ProfileSrcData.Formate; $scope.obj.Roundcorner = $scope.ProfileSrcData.Roundcorner; }
                                    }
                                    else { $scope.Unitsshortname = "px"; $scope.ProfileResData = []; $scope.profprofile = -1; $scope.profUnits = 1; }
                                }
                            });
                        }, 100); $('#btnSaveprofile').removeClass('disabled'); if (SaveprofileResult.Response != null) { $('#btnSaveprofile').removeClass('disabled'); }
                    }
                }
            });
        }
        $("input[id*='Round']").keypress(function (e) { if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { return false; } }); $("input[id*='FormateDpi']").keypress(function (e) { if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { return false; } }); $("input[id*='ratio']").keypress(function (e) { if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { return false; } }); $("input[id*='Crop']").keypress(function (event) { return isNumber(event) }); function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 45 && (charCode != 46 || $(this).val().indexOf('.') != -1) && (charCode < 48 || charCode > 57))
                return false; return true;
        }
        $scope.obj.Formate.Dpi = ""; $("#FormateDpi").focusout(function () { if ($scope.obj.Formate.Dpi == 0) { bootbox.alert($translate.instant('LanguageContents.Res_1118.Caption')); } });
    }
    app.controller("mui.admin.reformatprofilectrl", ['$scope', '$location', '$resource', 'AdminService', '$timeout', '$cookies', '$window', '$compile', '$translate', muiadminreformatprofilectrl]);
})(angular, app);