/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

	/// <summary>
	/// AttributeSetAttribute object for table 'MM_AttributeSet_Attribute'.
	/// </summary>
	
	internal class AttributeSetAttribute : IAttributeSetAttribute 
	{
		#region Member Variables

		protected int _attributesetid;
		protected int _attributeid;
		protected int? _sortorder;
		protected int? _validationid;
		
		
		#endregion
		
		#region Constructors
		public AttributeSetAttribute() {}
			
		public AttributeSetAttribute(int pAttributeSetid, int pAttributeid, int? pSortOrder, int? pValidationid)
		{
			this._attributesetid = pAttributeSetid; 
			this._attributeid = pAttributeid; 
			this._sortorder = pSortOrder; 
			this._validationid = pValidationid; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int AttributeSetid
		{
			get { return _attributesetid; }
			set { _attributesetid = value; }
			
		}
		
		public int Attributeid
		{
			get { return _attributeid; }
			set { _attributeid = value; }
			
		}
		
		public int? SortOrder
		{
			get { return _sortorder; }
			set { _sortorder = value; }
			
		}
		
		public int? Validationid
		{
			get { return _validationid; }
			set { _validationid = value; }
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			AttributeSetAttribute castObj = null;
			try
			{
				castObj = (AttributeSetAttribute)obj;
			} catch(Exception) { return false; } 
			return castObj.GetHashCode() == this.GetHashCode();
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			return this.GetType().FullName.GetHashCode();
				
		}
		#endregion
		
	}
	
}
