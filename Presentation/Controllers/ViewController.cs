﻿using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RazorEngine;
using System.Configuration;
using System.Net.Http.Headers;

namespace Presentation.Controllers
{
    [RoutePrefix("api/View")]
    public class ViewController : ApiController
    {
        ServiceResponse result;

        [HttpPost]
        public ServiceResponse InsertEntityUserRole([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.AccessManager.CreateEntityUserRole((int)jobj["EntitiyID"], (int)jobj["RoleID"], (int)jobj["UserID"], (bool)jobj["IsInherited"], (int)jobj["InheritedFromEntityID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        #region Default
        [HttpGet]
        [Route("GetSectionView")]
        public HttpResponseMessage GetSectionView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + @"views\mui\planningtool\default\detail\section.cshtml", serSessionModule);
            return result;
        }

        [HttpGet]
        [Route("GetListView")]
        public HttpResponseMessage GetListView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + @"views\mui\planningtool\default\list.cshtml", serSessionModule);
            return result;
        }

        /// <summary>
        /// Retrieves an individual cookie from the cookies collection
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cookieName"></param>
        /// <returns></returns>
        public string GetCookie(HttpRequestMessage request, string cookieName)
        {
            CookieHeaderValue cookie = request.Headers.GetCookies(cookieName).FirstOrDefault();
            if (cookie != null)
                return cookie[cookieName].Value;

            return null;
        }

        [HttpGet]
        [Route("GetDetailView")]
        public HttpResponseMessage GetDetailView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + @"views\mui\planningtool\default\detail.cshtml", serSessionModule);
            return result;
        }
        [HttpGet]
        [Route("GetOverView")]
        public HttpResponseMessage GetOverView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + marcomManager.User.TenantPath + @"views\mui\planningtool\default\detail\section\overview.cshtml", serSessionModule);
            return result;
        }
        [HttpGet]
        [Route("GetFinancialView")]
        public HttpResponseMessage GetFinancialView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + marcomManager.User.TenantPath + @"views\mui\planningtool\default\detail\section\financial.cshtml", serSessionModule);
            return result;
        }
        [HttpGet]
        [Route("GetObjectiveView")]
        public HttpResponseMessage GetObjectiveView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + marcomManager.User.TenantPath + @"views\mui\planningtool\default\detail\section\objective.html", serSessionModule);
            return result;
        }
        #endregion Default

        #region Costcentre
        [HttpGet]
        [Route("GetCostcentreSectionView")]
        public HttpResponseMessage GetCostcentreSectionView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + @"views\mui\planningtool\costcentre\detail\section.cshtml", serSessionModule);
            return result;
        }
        [HttpGet]
        [Route("GetCCFinancialView")]
        public HttpResponseMessage GetCCFinancialView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + marcomManager.User.TenantPath + @"views\mui\planningtool\costcentre\detail\section\financial.html", serSessionModule);
            return result;
        }
        [HttpGet]
        [Route("GetCostcentreListView")]
        public HttpResponseMessage GetCostcentreListView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + @"views\mui\planningtool\costcentre\list.cshtml", serSessionModule);
            return result;
        }
        [HttpGet]
        [Route("GetCostcentreDetailView")]
        public HttpResponseMessage GetCostcentreDetailView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + @"views\mui\planningtool\costcentre\detail.cshtml", serSessionModule);
            return result;
        }
        [HttpGet]
        [Route("GetCostcentreOverView")]
        public HttpResponseMessage GetCostcentreOverView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + marcomManager.User.TenantPath + @"views\mui\planningtool\costcentre\detail\section\overview.cshtml", serSessionModule);
            return result;
        }
        #endregion Costcentre

        #region Objective

        [HttpGet]
        [Route("GetObjectiveSectionView")]
        public HttpResponseMessage GetObjectiveSectionView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + @"views\mui\planningtool\objective\detail\section.cshtml", serSessionModule);
            return result;
        }
        [HttpGet]
        [Route("GetObjectiveOverView")]
        public HttpResponseMessage GetObjectiveOverView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + marcomManager.User.TenantPath + @"views\mui\planningtool\objective\detail\section\overview.html", serSessionModule);
            return result;
        }
        [HttpGet]
        [Route("GetObjectiveListView")]
        public HttpResponseMessage GetObjectiveListView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + @"views\mui\planningtool\objective\list.cshtml", serSessionModule);
            return result;
        }
        [HttpGet]
        [Route("GetObjectiveDetailView")]
        public HttpResponseMessage GetObjectiveDetailView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + @"views\mui\planningtool\objective\detail.cshtml", serSessionModule);
            return result;
        }
        #endregion Objective

        #region Calender

        [HttpGet]
        [Route("GetMuiSectionView")]
        public HttpResponseMessage GetMuiSectionView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + @"views\mui\planningtool\calender\detail\section.cshtml", serSessionModule);
            return result;
        }
        [HttpGet]
        [Route("GetMuiListView")]
        public HttpResponseMessage GetMuiListView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + @"views\mui\planningtool\calender\list.cshtml", serSessionModule);
            return result;
        }
        [HttpGet]
        [Route("GetMuiDetailView")]
        public HttpResponseMessage GetMuiDetailView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + @"views\mui\planningtool\calender\detail.cshtml", serSessionModule);
            return result;
        }
        #endregion Calender


        #region Support
        [HttpGet]
        [Route("GetSupportView")]
        public HttpResponseMessage GetSupportView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + @"views\mui\support.cshtml", serSessionModule);
            return result;
        }
        #endregion Support

        #region CMS
        [HttpGet]
        [Route("GetCmsSectionView")]
        public HttpResponseMessage GetCmsSectionView()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
            var serSessionModule = new UserAccessModule(Guid.Parse(GetCookie(Request, "session")));
            var result = LoadingSession(ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + @"views\mui\cms\default\detail\section.cshtml", serSessionModule);
            return result;
        }

        #endregion


        public HttpResponseMessage LoadingSession(String path, UserAccessModule serSessionModule)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {

                string viewpath = (path);
                string template = File.ReadAllText(viewpath);
                template = template.Replace("//_model.UserSessionID = g;", @"_model.UserSessionID = new Guid(""" + serSessionModule.UserSessionID + @""");");
                string parsedview = Razor.Parse(template, serSessionModule);
                response.Content = new StringContent(parsedview);
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("text/html");
                response.Headers.CacheControl = new CacheControlHeaderValue
                { Public = true, MaxAge = TimeSpan.FromSeconds(24 * 7 * 60) };
            }
            catch (Exception e)
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return response;

        }



        public class UserAccessModule : ViewController
        {
            public enum Operations
            {
                // Read = 1,
                View = 1,
                Edit = 2,
                Delete = 4,
                Create = 8,
                Self = 16,
                Simulate = 32,
                Allow = 64
            }
            public enum Feature
            {

                GanttView = 1,
                ListView = 2,
                Report = 3,
                CalenderView = 4,
                Financials = 5,
                Workflow = 6,
                Attachments = 7,
                Presentation = 8,
                Member = 9,
                Plan = 10,
                CostCenter = 11,
                Objective = 12,
                Dashboard = 13,
                Mypage = 14,
                GeneralSettings = 15,
                MetadataSettings = 16,
                ExternalLink = 17,
                Duplicate = 18,
                ApproveBudget = 19,
                Overview = 20,
                Task = 21,
                ViewEditAll = 22,
                Support = 23,
                Requestplannedamount = 24,
                Notification = 25,
                FundRequest = 26,
                SimulateUser = 27,
                DynamicEntity = 28,
                SSOExternalUser = 29,
                ViewAll = 31,
                CalenderEdit = 37

            }
            public enum Module
            {
                Common = 1,
                Admin = 2,
                Planning = 3,
                UserAccess = 4
            }

            public Guid UserSessionID { get; set; }// = MarcomManagerFactory.CreateSession(13741);

            public UserAccessModule()
            {

            }

            public UserAccessModule(Guid userSession)
            {
                this.UserSessionID = userSession;
            }
            public bool GetAccess(Module module, Feature feature, Operations operation)
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, UserSessionID);
                return marcomManager.AccessManager.CheckUserAccess((int)module, (int)feature, (int)operation);
            }

            public bool GetCustomAccess(string key)
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, UserSessionID);
                return marcomManager.User.ListOfAccess[key];
            }


            public bool GetAccess(Module module, Feature feature)
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, UserSessionID);
                return marcomManager.AccessManager.CheckUserAccess((int)module, (int)feature);
            }

        }
    }
}