using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IObjectiveCondition interface for table 'PM_ObjectiveCondition'.
    /// </summary>
    public interface IObjectiveCondition
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Objectiveid
        {
            get;
            set;

        }

        int? EntityTypeid
        {
            get;
            set;

        }

        int? Attributeid
        {
            get;
            set;

        }

        int ConditionType
        {
            get;
            set;

        }

        int SortOrder
        {
            get;
            set;

        }

        int AttributeLevel
        {
            get;
            set;
        }
        

        #endregion
    }
}
