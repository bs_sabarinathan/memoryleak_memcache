using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Access.Model
{

	/// <summary>
	/// EntityAclDao object for table 'AM_Entity_Acl'.
	/// </summary>
	public partial class EntityAclDao : BaseDao, ICloneable 
	{
		#region Member Variables

        protected int _id;
		protected int _roleid;
		protected int _moduleid;
		protected int _entitytypeid;
		protected int _featureid;
		protected int _accesspermission;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion

        //#region Extensibility Method Definitions

        //public override bool Equals(object obj)
        //{
        //    EntityAclDao toCompare = obj as EntityAclDao;
        //    if (toCompare == null)
        //    {
        //        return false;
        //    }

        //    if (!Object.Equals(this.Roleid, toCompare.Roleid))
        //        return false;
        //    if (!Object.Equals(this.Moduleid, toCompare.Moduleid))
        //        return false;
        //    if (!Object.Equals(this.EntityTypeid, toCompare.EntityTypeid))
        //        return false;
        //    if (!Object.Equals(this.Featureid, toCompare.Featureid))
        //        return false;

        //    return true;
        //}

        //public override int GetHashCode()
        //{
        //    int hashCode = 13;
        //    hashCode = (hashCode * 7) + Roleid.GetHashCode();
        //    hashCode = (hashCode * 7) + Moduleid.GetHashCode();
        //    hashCode = (hashCode * 7) + EntityTypeid.GetHashCode();
        //    hashCode = (hashCode * 7) + Featureid.GetHashCode();
        //    return hashCode;
        //}

        //#endregion
		
		#region Constructors
		public EntityAclDao() {}
			
		public EntityAclDao(int pId,int pRoleid, int pModuleid, int pEntityTypeid, int pFeatureid, int pAccessPermission)
		{
            this._id = pId;
			this._roleid = pRoleid; 
			this._moduleid = pModuleid; 
			this._entitytypeid = pEntityTypeid; 
			this._featureid = pFeatureid; 
			this._accesspermission = pAccessPermission; 
		}
		
		#endregion
		
		#region Public Properties
		

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }
        

		public virtual int Roleid
		{
			get { return _roleid; }
			set { _bIsChanged |= (_roleid != value); _roleid = value; }
			
		}
		
		public virtual int Moduleid
		{
			get { return _moduleid; }
			set { _bIsChanged |= (_moduleid != value); _moduleid = value; }
			
		}
		
		public virtual int EntityTypeid
		{
			get { return _entitytypeid; }
			set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }
			
		}
		
		public virtual int Featureid
		{
			get { return _featureid; }
			set { _bIsChanged |= (_featureid != value); _featureid = value; }
			
		}
		
		public virtual int AccessPermission
		{
			get { return _accesspermission; }
			set { _bIsChanged |= (_accesspermission != value); _accesspermission = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string Id = "Id";
			public const string EntityAclDao = "EntityAclDao";			
			public const string Roleid = "Roleid";			
			public const string Moduleid = "Moduleid";			
			public const string EntityTypeid = "EntityTypeid";			
			public const string Featureid = "Featureid";			
			public const string AccessPermission = "AccessPermission";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string Id = "Id";	
			public const string EntityAclDao = "EntityAclDao";			
			public const string Roleid = "Roleid";			
			public const string Moduleid = "Moduleid";			
			public const string EntityTypeid = "EntityTypeid";			
			public const string Featureid = "Featureid";			
			public const string AccessPermission = "AccessPermission";			
		}

		#endregion
		
		
	}
		
}
