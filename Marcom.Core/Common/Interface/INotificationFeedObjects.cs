﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Dal.Metadata.Model;
using Mail;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    public interface INotificationFeedObjects
    {
        string action { get; set; }
        int EntityId { get; set; }
        String TypeName { get; set; }
        String AttributeName { get; set; }
        String FromValue { get; set; }
        String ToValue { get; set; }
        int Userid { get; set; }
        //Object obj1 { get; set; }
        IList<object> obj2 { get; set; }  //used to put multiple values while metadataupdate
        IList obj3 { get; set; }  //used to put multiple values while metadataupdate
        int FinancialPlannedAmount { get; set; }
        int AttributeId { get; set; }
        IList<AttributeDao> AttributeDetails { get; set; }
        int Actorid { get; set; }
        int Attributetypeid { get; set; }
        DateTimeOffset CreatedOn { get; set; }
        int EntityTypeId { get; set; }
        int AssociatedEntityId { get; set; }
        int ParentId { get; set; }
        bool memberisinherited { get; set; }
        string attachmenttype { get; set; }
        IList<TreeValueDao> treeValues { get; set; }
        IList<TreeNodeDao> treeNode { get; set; }
        int objectiveId { get; set; }
        int objectiveName { get; set; }
        string AttributeRecordName { get; set; }
        IList<BrandSystems.Marcom.Core.Metadata.Interface.IEntityTypeAttributeGroupRelationwithLevels> attrgrprel { get; set; }
        IList<BrandSystems.Marcom.Core.Planning.Interface.IAttributeData> attrdata { get; set; }
    }

}
