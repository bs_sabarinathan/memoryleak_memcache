﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    public interface IPreviousUpdates
    {
        string Version { get; set; }
        string Feature { get; set; }
        string Status { get; set; }
        string date { get; set; }
        int Id { get; set; }
        bool showbutton { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string DocPath { get; set; }
        string username { get; set; }
    }
}
