﻿(function (ng, app) {
    "use strict";

    function muiplanningtoolobjectivedetailfiltersettingsCtrl($scope, $location, $resource, $timeout, $cookies, $compile, $cookieStore, $translate, ObjectivefiltersettingsService) {
        var IsSave = 0;
        var IsUpdate = 0;
        $scope.showSave = true;
        $scope.showUpdate = false;
        $scope.appliedfilter = "No filter applied";
        $scope.deletefiltershow = false;
        $scope.ClearFilterAttributes = function () {
            $scope.ClearFieldsAndReApply();
        }
        var rememberfilter = [];
        $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption');
        $scope.EntityTypeID = SystemDefinedEntityTypes.Objective;
        $scope.visible = false;
        $scope.filterValues = [];
        $scope.atributesRelationList = [];
        $scope.filterSettingValues = [];
        $scope.ObjectiveOwners = {};
        $scope.Savefilter = true;
        $scope.ObjectiveFilter = {
            ngObjectivefilterStatus: [],
            ngObjectivefilterOwner: [],
            ngObjectiveType: [],
            ngObjectivefilterSaveAs: '',
            ngObjectivefilterKeyword: ''
        }
        $scope.objectivefiltercreation = function (event) {
            $scope.ClearScopeModle();
            $scope.Deletefilter = false;
            $scope.Applyfilter = true;
            $scope.Updatefilter = false;
            $scope.Savefilter = true;
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            IsSave = 0;
            IsUpdate = 0;
        };
        $("#rootlevelfilter").on("ClearScope", function (event) {
            $scope.showSave = true;
            $scope.showUpdate = false;
            $scope.deletefiltershow = false;
            $scope.ClearScopeModle();
        });
        $("#rootlevelfilter").on('ApplyRootLevelFilter', function (event, filterid, filtername) {
            $scope.ApplyFilter(filterid, filtername);
        });
        $("#rootlevelfilter").on('EditFilterSettingsByFilterID', function (event, filterid) {
            $scope.showSave = false;
            $scope.showUpdate = true;
            $scope.deletefiltershow = true;
            $scope.LoadFilterSettingsByFilterID(event, filterid);
        });
        $scope.ClearScopeModle = function () {
            $scope.ngsaveasfilter = "";
            $("#saveFilter").removeAttr('disabled');
            $scope.ObjectiveFilter.ngObjectivefilterOwner = [], $scope.ObjectiveFilter.ngObjectiveType = [], $scope.ObjectiveFilter.ngObjectivefilterSaveAs = '', $scope.ObjectiveFilter.ngObjectivefilterKeyword = ''
            $scope.ObjectiveFilter.ngObjectivefilterStatus = [];
        }
        $scope.ClearFieldsAndReApply = function () {
            $scope.ngsaveasfilter = "";
            $("#saveFilter").removeAttr('disabled');
            $scope.ObjectiveFilter.ngObjectivefilterOwner = [], $scope.ObjectiveFilter.ngObjectiveType = [], $scope.ObjectiveFilter.ngObjectivefilterSaveAs = '', $scope.ObjectiveFilter.ngObjectivefilterKeyword = ''
            $scope.ObjectiveFilter.ngObjectivefilterStatus = [];
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            var filterattr = [];
            $scope.showSave = true;
            $scope.showUpdate = false;
            $scope.deletefiltershow = false;
            RememberObjFilterID = 0;
            RememberObjFilterAttributes = [];
            $cookieStore.remove('ObjAttrs' + parseInt($cookies['UserId'], 10));
            $cookieStore.remove('ObjFilterID' + parseInt($cookies['UserId'], 10));
            $cookieStore.remove('ObjFilterName' + parseInt($cookies['UserId'], 10));
            RemeberObjFilterName = "No filter applied";
            $("#Div_Objlist").trigger('ClearAndReApply');
            if ($scope.EntityTypeID == 10) {
                $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, 0, filterattr)
            }
        }
        ObjectivefiltersettingsService.GetAllObjectiveMembers(parseInt($scope.EntityTypeID, 10)).then(function (GettingObjectiveOwnersObj) {
            var GettingObjectiveOwnersResult = GettingObjectiveOwnersObj.Response;
            $scope.ObjectiveOwners = GettingObjectiveOwnersObj.Response;
        });
        var filterSettings;
        ObjectivefiltersettingsService.GetFilterSettings(parseInt($scope.EntityTypeID, 10)).then(function (filterSettings) {
            $scope.filterValues = filterSettings.Response;
        });
        $timeout(function () {
            filterSettings;
        }, 100);
        $scope.FilterSave = function () {
            if (IsSave == 1) {
                return false;
            }
            IsSave = 1;
            if ($scope.ObjectiveFilter.ngObjectivefilterSaveAs == '' || $scope.ObjectiveFilter.ngObjectivefilterSaveAs == undefined) {
                bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));
                return false;
            }
            var filterattributes = [];
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            $("#saveFilter").attr('disabled', 'disabled');
            var FilterData = {};
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            if ($scope.ObjectiveFilter.ngObjectivefilterOwner.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterOwner.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.Owner,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterOwner[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.Owner
                    });
                }
            }
            if ($scope.ObjectiveFilter.ngObjectiveType.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectiveType.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.ObjectiveType,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectiveType[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.ObjectiveType
                    });
                }
            }
            if ($scope.ObjectiveFilter.ngObjectivefilterStatus.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterStatus.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.ObjectiveStatus,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterStatus[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.ObjectiveType
                    });
                }
            }
            FilterData.FilterId = 0;
            FilterData.FilterName = $scope.ObjectiveFilter.ngObjectivefilterSaveAs;
            FilterData.Keyword = $scope.ObjectiveFilter.ngObjectivefilterKeyword;
            FilterData.UserId = 1;
            FilterData.TypeID = parseInt($scope.EntityTypeID, 10);
            FilterData.entityTypeId = '';
            FilterData.StarDate = '';
            FilterData.EndDate = '';
            FilterData.IsDetailFilter = 0;
            FilterData.WhereConditon = whereConditionData;
            ObjectivefiltersettingsService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                if (filterSettingsInsertresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4283.Caption'));
                    IsSave = 0;
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4401.Caption'));
                    IsSave = 0;
                    $("#Div_Objlist").trigger('ReloadFilterSettings', [parseInt($scope.EntityTypeID, 10), $scope.ObjectiveFilter.ngObjectivefilterSaveAs, filterSettingsInsertresult.Response]);
                    filterSettingsInsertresult.WhereConditon = [];
                    $scope.FilterID.selectedFilterID = filterSettingsInsertresult.Response;
                    RememberObjFilterID = $scope.FilterID.selectedFilterID;
                    RememberObjFilterAttributes = filterattributes;
                    RemeberObjFilterName = filterSettingsInsertresult.FilterName;
                    $cookieStore.put('ObjAttrs' + parseInt($cookies['UserId'], 10), filterattributes);
                    $cookieStore.put('ObjFilterID' + parseInt($cookies['UserId'], 10), $scope.FilterID.selectedFilterID);
                    $cookieStore.put('ObjFilterName' + parseInt($cookies['UserId'], 10), filterSettingsInsertresult.FilterName);
                    if ($scope.EntityTypeID == 10) {
                        $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, $scope.FilterID.selectedFilterID, filterattributes)
                    }
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $scope.LoadFilterSettingsByFilterID = function (event, filterId) {
            $scope.Updatefilter = false;
            $scope.Applyfilter = false;
            $scope.Deletefilter = false;
            $scope.Savefilter = false;
            $scope.filterSettingValues = [];
            $scope.FilterID.selectedFilterID = filterId;
            ObjectivefiltersettingsService.GetFilterSettingValuesByFilertId($scope.FilterID.selectedFilterID).then(function (filterSettingsValues) {
                $scope.filterSettingValues = filterSettingsValues.Response;
                for (var i = 0; i < $scope.filterSettingValues.FilterValues.length; i++) {
                    if ($scope.filterSettingValues.FilterValues[i].AttributeId == SystemDefiendAttributes.Owner && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3) $scope.ObjectiveFilter.ngObjectivefilterOwner.push($scope.filterSettingValues.FilterValues[i].Value);
                    else if ($scope.filterSettingValues.FilterValues[i].AttributeId == SystemDefiendAttributes.ObjectiveType) $scope.ObjectiveFilter.ngObjectiveType.push($scope.filterSettingValues.FilterValues[i].Value);
                    else if ($scope.filterSettingValues.FilterValues[i].AttributeId == SystemDefiendAttributes.ObjectiveStatus) $scope.ObjectiveFilter.ngObjectivefilterStatus.push($scope.filterSettingValues.FilterValues[i].Value);
                }
                $scope.ObjectiveFilter.ngObjectivefilterKeyword = $scope.filterSettingValues.Keyword;
                $scope.ObjectiveFilter.ngObjectivefilterSaveAs = $scope.filterSettingValues.FilterName;
                $scope.Updatefilter = true;
                $scope.Applyfilter = false;
                $scope.Deletefilter = true;
                $scope.Savefilter = false;
            });
            event.stopImmediatePropagation();
            event.stopPropagation();
        }
        $scope.FilterUpdate = function () {
            if (IsUpdate == 1) {
                return false;
            }
            IsUpdate = 1;
            $scope.filterSettingValues.Keyword = '';
            $scope.filterSettingValues.FilterName = '';
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            var filterattributes = [];
            var FilterData = {};
            if ($scope.ObjectiveFilter.ngObjectivefilterOwner.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterOwner.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.Owner,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterOwner[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.Owner
                    });
                }
            }
            if ($scope.ObjectiveFilter.ngObjectiveType.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectiveType.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.ObjectiveType,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectiveType[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.ObjectiveType
                    });
                }
            }
            if ($scope.ObjectiveFilter.ngObjectivefilterStatus.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterStatus.length; k++) {
                    whereConditionData.push({
                        'AttributeID': SystemDefiendAttributes.ObjectiveStatus,
                        'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterStatus[k],
                        'Level': 0,
                        'AttributeTypeId': AttributeTypes.ObjectiveType
                    });
                }
            }
            FilterData.FilterId = parseInt($scope.FilterID.selectedFilterID, 10);
            FilterData.FilterName = $scope.ObjectiveFilter.ngObjectivefilterSaveAs;
            FilterData.Keyword = $scope.ObjectiveFilter.ngObjectivefilterKeyword;
            FilterData.UserId = 1;
            FilterData.TypeID = parseInt($scope.EntityTypeID, 10);
            FilterData.entityTypeId = '';
            FilterData.StarDate = '';
            FilterData.EndDate = '';
            FilterData.WhereConditon = whereConditionData;
            FilterData.IsDetailFilter = 0;
            ObjectivefiltersettingsService.InsertFilterSettings(FilterData).then(function (filterSettingsUpdateresult) {
                if (filterSettingsUpdateresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4358.Caption'));
                    IsUpdate = 0;
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4399.Caption'));
                    IsUpdate = 0;
                    filterSettingsUpdateresult.WhereConditon = [];
                    $("#Div_Objlist").trigger('ReloadFilterSettings', [parseInt($scope.EntityTypeID, 10), $scope.ObjectiveFilter.ngObjectivefilterSaveAs, filterSettingsUpdateresult.Response]);
                    $scope.appliedfilter = $scope.ObjectiveFilter.ngObjectivefilterSaveAs;
                    var StartRowNo = 0;
                    var MaxNoofRow = 20;
                    var PageIndex = 0;
                    RememberObjFilterID = filterSettingsUpdateresult.Response;
                    RememberObjFilterAttributes = filterattributes;
                    RemeberObjFilterName = filterSettingsUpdateresult.FilterName;
                    $cookieStore.put('ObjAttrs' + parseInt($cookies['UserId'], 10), filterattributes);
                    $cookieStore.put('ObjFilterID' + parseInt($cookies['UserId'], 10), filterSettingsUpdateresult.Response);
                    $cookieStore.put('ObjFilterName' + parseInt($cookies['UserId'], 10), $scope.ObjectiveFilter.ngObjectivefilterSaveAs);
                    if ($scope.EntityTypeID == 10) {
                        $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, filterSettingsUpdateresult.Response, filterattributes)
                    }
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $scope.ApplyFilter = function (FilterID, FilterName) {
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            if (FilterID != undefined && FilterName != undefined) {
                $scope.FilterID.selectedFilterID = FilterID;
                $scope.appliedfilter = FilterName;
            }
            $scope.filterSettingValues.Keyword = '';
            $scope.filterSettingValues.FilterName = '';
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            if ($scope.ObjectiveFilter.ngObjectivefilterOwner.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterOwner.length; k++) {
                    if ($scope.ObjectiveFilter.ngObjectivefilterKeyword != '') {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.Owner,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterOwner[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.Owner,
                            'Keyword': $scope.ObjectiveFilter.ngObjectivefilterKeyword
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.Owner,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterOwner[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.Owner,
                            'Keyword': ''
                        });
                    }
                }
            }
            if ($scope.ObjectiveFilter.ngObjectiveType.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectiveType.length; k++) {
                    if ($scope.ObjectiveFilter.ngObjectivefilterKeyword != '') {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.ObjectiveType,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectiveType[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.ObjectiveType,
                            'Keyword': $scope.ObjectiveFilter.ngObjectivefilterKeyword
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.ObjectiveType,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectiveType[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.ObjectiveType,
                            'Keyword': ''
                        });
                    }
                }
            }
            if ($scope.ObjectiveFilter.ngObjectivefilterStatus.length > 0) {
                for (var k = 0; k < $scope.ObjectiveFilter.ngObjectivefilterStatus.length; k++) {
                    if ($scope.ObjectiveFilter.ngObjectivefilterKeyword != '') {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.ObjectiveType,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterStatus[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.ObjectiveType,
                            'Keyword': $scope.ObjectiveFilter.ngObjectivefilterKeyword
                        });
                    } else {
                        whereConditionData.push({
                            'AttributeID': SystemDefiendAttributes.ObjectiveStatus,
                            'SelectedValue': $scope.ObjectiveFilter.ngObjectivefilterStatus[k],
                            'Level': 0,
                            'AttributeTypeId': AttributeTypes.ObjectiveType,
                            'Keyword': ''
                        });
                    }
                }
            }
            if (whereConditionData.length == 0 && $scope.ObjectiveFilter.ngObjectivefilterKeyword != '') {
                whereConditionData.push({
                    'AttributeID': 0,
                    'SelectedValue': 0,
                    'Level': 0,
                    'AttributeTypeId': 0,
                    'Keyword': $scope.ObjectiveFilter.ngObjectivefilterKeyword
                });
            }
            $scope.AppllyFilterObj.selectedattributes = [];
            if (FilterID != undefined) {
                $scope.AppllyFilterObj.selectedattributes = [];
            } else {
                $scope.AppllyFilterObj.selectedattributes = whereConditionData;
            }
            if (FilterID == undefined) {
                FilterID = 0;
            }
            RememberObjFilterID = FilterID;
            RememberObjFilterAttributes = $scope.AppllyFilterObj.selectedattributes;
            RemeberObjFilterName = $scope.appliedfilter;
            $cookieStore.put('ObjAttrs' + parseInt($cookies['UserId'], 10), $scope.AppllyFilterObj.selectedattributes);
            $cookieStore.put('ObjFilterID' + parseInt($cookies['UserId'], 10), FilterID);
            $cookieStore.put('ObjFilterName' + parseInt($cookies['UserId'], 10), $scope.appliedfilter);
            if ($scope.EntityTypeID == 10) {
                $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, FilterID, $scope.AppllyFilterObj.selectedattributes)
            }

        };
        $scope.filtersettingsreset = function () {
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            $scope.FilterID.selectedFilterID = 0;
        };
        $scope.DeleteFilter = function DeleteFilterSettingsValue() {
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            var filterattribtues = [];
            bootbox.confirm($translate.instant('LanguageContents.Res_2026.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var ID = $scope.FilterID.selectedFilterID;
                        ObjectivefiltersettingsService.DeleteFilterSettings(ID).then(function (deletefilterbyFilterId) {
                            if (deletefilterbyFilterId.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4299.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4398.Caption'));
                                $('.FilterHolder').slideUp("slow");
                                $scope.showSave = true;
                                $scope.showUpdate = false;
                                $scope.ClearScopeModle();
                                $scope.ApplyFilter(0, 'No filter applied');
                                $("#Div_Objlist").trigger('ReloadFilterSettings', [parseInt($scope.EntityTypeID, 10), 'No filter applied', 0]);
                                $scope.FilterID.selectedFilterID = 0;
                                $scope.AppllyFilterObj.selectedattributes = [];
                                if ($scope.EntityTypeID == 10) {
                                    $scope.LoadRootLevelObjective(PageIndex, StartRowNo, MaxNoofRow, 0, filterattribtues)
                                }
                            }
                        });
                    }, 100);
                }
            });
        };
        $("#rootlevelfilter").on('RemeberApplyFilterReLoad', function (event, TypeID, RememberPlanFilterAttributes) {
            ObjectivefiltersettingsService.GetAllObjectiveMembers(parseInt($scope.EntityTypeID, 10)).then(function (GettingObjectiveOwnersObj) {
                //$scope.ObjectiveFilter.ngObjectivefilterOwner = [];
                //$scope.ObjectiveFilter.ngObjectiveType = [];
                //$scope.ObjectiveFilter.ngObjectivefilterKeyword = '';
                //$scope.ObjectiveFilter.ngObjectivefilterStatus = [];
                var GettingObjectiveOwnersResult = GettingObjectiveOwnersObj.Response;
                $scope.ObjectiveOwners = GettingObjectiveOwnersObj.Response;
                $timeout(function () {
                    RememberFilterOnReLoad(RememberPlanFilterAttributes)
                }, 10);
            });
        });

        function RememberFilterOnReLoad(RememberPlanFilterAttributes) {
            if ($scope.EntityTypeID == 10) {
                if (RememberPlanFilterAttributes.length != 0) {
                    $('.FilterHolder').show()
                    $scope.ddlParententitytypeId = [];
                    $scope.filterSettingValues.FilterValues = RememberPlanFilterAttributes;
                    for (var i = 0; i < $scope.filterSettingValues.FilterValues.length; i++) {
                        if ($scope.filterSettingValues.FilterValues[i].AttributeID == SystemDefiendAttributes.Owner && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3)
                        {
                            var res = $.grep($scope.ObjectiveFilter.ngObjectivefilterOwner, function (e) { return e == $scope.filterSettingValues.FilterValues[i].SelectedValue });
                            if (res.length == 0) $scope.ObjectiveFilter.ngObjectivefilterOwner.push($scope.filterSettingValues.FilterValues[i].SelectedValue);
                        }
                        else if ($scope.filterSettingValues.FilterValues[i].AttributeID == SystemDefiendAttributes.ObjectiveType)
                        {
                            var res1 = $.grep($scope.ObjectiveFilter.ngObjectiveType, function (e) { return e == $scope.filterSettingValues.FilterValues[i].SelectedValue });
                            if (res1.length == 0) $scope.ObjectiveFilter.ngObjectiveType.push($scope.filterSettingValues.FilterValues[i].SelectedValue);
                        }
                        else if ($scope.filterSettingValues.FilterValues[i].AttributeID == SystemDefiendAttributes.ObjectiveStatus) {
                            var res2 = $.grep($scope.ObjectiveFilter.ngObjectivefilterStatus, function (e) { return e == $scope.filterSettingValues.FilterValues[i].SelectedValue });
                            if (res2.length == 0) $scope.ObjectiveFilter.ngObjectivefilterStatus.push($scope.filterSettingValues.FilterValues[i].SelectedValue);
                        }
                    }
                    $scope.ObjectiveFilter.ngObjectivefilterKeyword = $scope.filterSettingValues.FilterValues[0].Keyword;
                }
            }
        }
        $scope.ObjFilterStatus = [{
            Id: 1,
            FilterStatus: $translate.instant('LanguageContents.Res_4038.Caption')
        }, {
            Id: 0,
            FilterStatus: $translate.instant('LanguageContents.Res_4202.Caption')
        }];
        $scope.ObjTypeList = [{
            Id: 1,
            objType: $translate.instant('LanguageContents.Res_4534.Caption')
        }, {
            Id: 2,
            objType: $translate.instant('LanguageContents.Res_4533.Caption')
        }, {
            Id: 3,
            objType: $translate.instant('LanguageContents.Res_706.Caption')
        }, {
            Id: 4,
            objType: $translate.instant('LanguageContents.Res_707.Caption')
        }];
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.objective.detail.filtersettingsCtrl']"));
        });
    }
    app.controller("mui.planningtool.objective.detail.filtersettingsCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$compile', '$cookieStore', '$translate', 'ObjectivefiltersettingsService', muiplanningtoolobjectivedetailfiltersettingsCtrl]);
})(angular, app);