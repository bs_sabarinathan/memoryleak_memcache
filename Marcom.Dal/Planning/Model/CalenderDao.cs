﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// ObjectiveDao object for table 'PM_Calender'.
    /// </summary>

    public partial class CalenderDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _typeid;
        protected string _name;
        protected string _description;
        protected bool? _calenderstatus;
        protected DateTime? _publishedon;
        protected bool _isexternal;
        protected int _visibilityperiod;
        protected int _visibilitytype;
        protected int _ownerid;
        protected DateTime _createdon;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        protected Guid _externalurlid;
        #endregion

        #region Constructors
        public CalenderDao() { }

        public CalenderDao(int pId, int pTypeid, string pName, string pDescription, bool? pCalenderStatus, bool pIsExternal, int pVisibilityPeriod, int pVisibilityType, int pOwnerId, DateTime pCreatedOn, DateTime? pPublishedon, Guid pExternalUrlID)
        {
            this._id = pId;
            this._typeid = pTypeid;
            this._name = pName;
            this._description = pDescription;
            this._calenderstatus = pCalenderStatus;
            this._isexternal = pIsExternal;
            this._visibilityperiod = pVisibilityPeriod;
            this._visibilitytype = pVisibilityType;
            this._ownerid = pOwnerId;
            this._createdon = pCreatedOn;
            this._publishedon = pPublishedon;
            this._externalurlid = pExternalUrlID;

        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }
        }

        public virtual int Typeid
        {
            get { return _typeid; }
            set { _bIsChanged |= (_typeid != value); _typeid = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                //if (value != null && value.Length > 50)
                //  throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 50 characters");
                _bIsChanged |= (_name != value);
                _name = value;
            }
        }
        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }
        }
       
        public virtual bool? CalenderStatus
        {
            get { return _calenderstatus; }
            set { _calenderstatus = value; }
        }

        public virtual DateTime? PublishedOn
        {
            get { return _publishedon; }
            set { _publishedon = value; }
        }

        public virtual bool IsExternal
        {
            get { return _isexternal; }
            set { _isexternal = value; }
        }
        public virtual int Visibilityperiod
        {
            get { return _visibilityperiod; }
            set { _visibilityperiod = value; }
        }
        public virtual int Visibilitytype
        {
            get { return _visibilitytype; }
            set { _visibilitytype = value; }
        }
        public virtual int OwnerID
        {
            get { return _ownerid; }
            set { _ownerid = value; }
        }

        public virtual DateTime CreatedOn
        {
            get { return _createdon; }
            set { _createdon = value; }
        }
        public virtual Guid ExternalUrlID
        {
            get { return _externalurlid; }
            set
            {
        
                _externalurlid = value;
            }
        
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string CalenderDao = "CalenderDao";
            public const string Id = "Id";
            public const string Typeid = "Typeid";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string CalenderStatus = "CalenderStatus";
            public const string PublishedOn = "PublishedOn";
            public const string IsExternal = "IsExternal";
            public const string Visibilityperiod = "Visibilityperiod";
            public const string Visibilitytype = "Visibilitytype";
            public const string OwnerID = "OwnerID";
            public const string CreatedOn = "CreatedOn";
            public const string ExternalUrlID = "ExternalUrlID";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string CalenderDao = "CalenderDao";
            public const string Id = "Id";
            public const string Typeid = "Typeid";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string CalenderStatus = "CalenderStatus";
            public const string PublishedOn = "PublishedOn";
            public const string IsExternal = "IsExternal";
            public const string Visibilityperiod = "Visibilityperiod";
            public const string Visibilitytype = "Visibilitytype";
            public const string OwnerID = "OwnerID";
            public const string CreatedOn = "CreatedOn";
            public const string ExternalUrlID = "ExternalUrlID";
        }

        #endregion


    }

}
