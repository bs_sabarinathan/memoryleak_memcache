using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IEntity interface for table 'PM_Entity'.
    /// </summary>
    public interface IEntity
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Parentid
        {
            get;
            set;

        }

        int Typeid
        {
            get;
            set;

        }

        bool Active
        {
            get;
            set;

        }

        string UniqueKey
        {
            get;
            set;

        }

        int EntityID
        {
            get;
            set;

        }

        bool IsLock
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        int Version
        {
            get;
            set;

        }
        int ActiveEntityStateID
        {
            get;
            set;
        }
        int EntityStateID
        {
            get;
            set;
        }

        int Level
        {
            get;
            set;
        }
        bool EnableDisableWorkflow
        {
            get;
            set;
        }
        IList<IAttributeData> AttributeData
        {
            get;
            set;
        }

        IList<IEntityRoleUser> EntityMembers
        {
            get;
            set;
        }
        IList<IEntityCostReleations> EntityCostcenters
        {
            get;
            set;
        }
        IEntityPresentation Presentation
        {
            get;
            set;
        }

        IList<IEntityPeriod> Periods
        {
            get;
            set;

        }

        IList<IFinancial> Financials
        {
            get;
            set;

        }

        IList<IFundingRequest> FundingRequests
        {
            get;
            set;

        }

        IObjective Objectives
        {
            get;
            set;

        }

        ICostCenter Costcenter
        {
            get;
            set;
        }

        string OwnerName
        {
            get;
            set;
        }

        #endregion
    }
}
