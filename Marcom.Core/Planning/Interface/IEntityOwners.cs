﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
   public interface IEntityOwners
    {
       int EntityId { get; set; }
       int RoleId { get; set; }
       int UserId { get; set; }
       string UserName { get; set; }
       string UserEmail { get; set; }
    }
}
