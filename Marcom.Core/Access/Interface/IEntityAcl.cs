using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Access.Interface
{
    /// <summary>
    /// IEntityAcl interface for table 'AM_Entity_Acl'.
    /// </summary>
    public interface IEntityAcl
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }
        
        int Roleid
        {
            get;
            set;

        }

        int Moduleid
        {
            get;
            set;

        }

        int EntityTypeid
        {
            get;
            set;

        }

        int Featureid
        {
            get;
            set;

        }

        int AccessPermission
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
