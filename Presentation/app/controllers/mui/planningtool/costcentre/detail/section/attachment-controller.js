﻿(function (ng, app) {
    "use strict"; function muiplanningtoolcostcentredetailsectionattachmentCtrl($scope, $timeout, $http, $compile, $resource, $window, $cookies, $stateParams, $translate, TaskService, CommonService) {
        $scope.$on("$destroy", function () { RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.detail.section.attachmentCtrl']")); }); LoadAttachments(); $scope.$on('LoadAttachmentCC', function (event, ID) { $("#AttchmentContent tbody").html($compile('')($scope)); LoadAttachments(); }); function LoadAttachments() {
            if ($scope.IsLock == true) { $scope.btnDownload = false; $scope.btnAddLink = false; $scope.btnAddFiles = false; }
            else { $scope.btnDownload = true; $scope.btnAddLink = true; $scope.btnAddFiles = true; }
            CommonService.GetFilesandLinksByEntityID(parseInt($stateParams.ID)).then(function (getAttachlist) {
                $scope.result = getAttachlist.Response; if ($scope.IsNotVersioning == false) { ReloadTaskVersionDetails(); }
                $scope.IsNotVersioning = true; $scope.AttachmentVersionFileList = getAttachlist.Response; LoadAttachmentsToBind($scope.result);
            });
        }
        function LoadAttachmentsToBind(attachmentDetails) {
            if (attachmentDetails != null) {
                $scope.AttachUi = ''; jQuery.each(attachmentDetails, function (i) {
                    $scope.AttachUi += '<tr data-id="' + attachmentDetails[i].Id + '">'
                    $scope.AttachUi += '<td><span>'
                    $scope.AttachUi += '<label class=\"checkbox checkbox-custom pull-right\">'
                    $scope.AttachUi += '<input type="checkbox"><i class="checkbox"></i></label></span></td>'
                    $scope.AttachUi += '<td><span>'
                    if (attachmentDetails[i].Extension != 'Link') {
                        if ($scope.IsLock == false)
                            $scope.AttachUi += '<i class="icon-file attachmentIcon-active"></i><a my-qtip2 qtip-content="' + TooltipattachmentName(attachmentDetails[i].Name) + '" target=\"_blank\" ng-href=\"download.aspx?FileID=' + attachmentDetails[i].Fileguid + '&amp;FileFriendlyName=' + attachmentDetails[i].Name + '&amp;Ext=' + attachmentDetails[i].Extension + '\">' + attachmentDetails[i].Name + '</a>'; else
                            $scope.AttachUi += attachmentDetails[i].Name; $scope.AttachUi += '<td><span>' + parseSize(attachmentDetails[i].Size) + '</span></td>'
                        $scope.AttachUi += '<td><span>' + attachmentDetails[i].Extension.substring(1, attachmentDetails[i].Extension.length) + '</span></td>'
                        $scope.AttachUi += '<td><span>' + attachmentDetails[i].VersionNo + '</span></td>'
                    }
                    else {
                        if ($scope.IsLock == false)
                            $scope.AttachUi += '<i class="icon-globe attachmentIcon-active"></i><a my-qtip2 qtip-content="' + TooltipattachmentName(attachmentDetails[i].Name) + '" ng-click=\"popMe($event);\" data-id=\"1\"; data-name=' + attachmentDetails[i].LinkURL + ' href=\"javascript:void(0);\">' + attachmentDetails[i].Name + '</a>'; else
                            $scope.AttachUi += attachmentDetails[i].Name; if (attachmentDetails[i].Size == 0) { $scope.AttachUi += '<td><span>' + '0' + '</span></td>' }
                            else { $scope.AttachUi += '<td><span>' + attachmentDetails[i].Size + '</span></td>' }
                        $scope.AttachUi += '<td><span>' + attachmentDetails[i].Extension + '</span></td>'
                        $scope.AttachUi += '<td><span>' + '-' + '</span></td>'
                    }
                    $scope.AttachUi += '<td><span>' + attachmentDetails[i].CreatedOn.Year + '-' + attachmentDetails[i].CreatedOn.Month + '-' + attachmentDetails[i].CreatedOn.Day + '</span></td>'
                    $scope.AttachUi += '<td><span>' + attachmentDetails[i].OwnerName + '</span></td>'
                    $scope.AttachUi += '<td class=\"td5\"><span>'; if ($scope.IsLock == false)
                        $scope.AttachUi += '<i id=context5copy_' + attachmentDetails[i].Id + ' class=\"icon-reorder\" data-toggle=\"modal\" data-role=\"TaskFileDetail\"  data-fileid=' + attachmentDetails[i].Id + ' data-extension=' + attachmentDetails[i].Extension + ' ng-click=\"loadcontextmenu($event);\" context=\"context5\"></i>'; $scope.AttachUi += '</span></td>'; $scope.AttachUi += '</tr>';
                }); $("#AttchmentContent tbody").html($compile($scope.AttachUi)($scope));
            }
        }
        var timeoutReference; $("#txtAttachFilter").keyup(function (event) { if (timeoutReference) clearTimeout(timeoutReference); timeoutReference = setTimeout(function () { var tempvar = $.grep($scope.result, function (e) { return e.Name.toString().toLowerCase().indexOf($("#txtAttachFilter").val().toString().toLowerCase()) != -1; }); LoadAttachmentsToBind(tempvar); }, 800); }); $scope.ContextFileExtention = ""; $scope.loadcontextmenu = function (e) {
            var linkextn = $(e.target).attr('data-extension'); $scope.ContextFileExtention = linkextn; if (linkextn == "Link") { $scope.showmanageverson = false; $scope.shownewversion = false; }
            else { $scope.showmanageverson = true; $scope.shownewversion = true; }
        }
        $scope.FileID = 0; $scope.IsNotVersioning = true; $scope.ChangeVersionNo = function () { $scope.FileID = $scope.contextFileID; $scope.IsNotVersioning = false; $scope.fnTimeOut(); }
        $scope.DeleteAttachmentVersionByID = function (ID, Index, ActiveFileVersionID) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2050.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        if (ActiveFileVersionID != 0) { bootbox.alert($translate.instant('LanguageContents.Res_1973.Caption')); return false; }
                        TaskService.DeleteFileByID(ID).then(function (deleteattach) { if (deleteattach.Response == true) { $scope.AttachmentVersionFileList.splice(Index, 1); NotifySuccess($translate.instant('LanguageContents.Res_4793.Caption')); } else { NotifyError($translate.instant('LanguageContents.Res_4297.Caption')); } });
                    }, 100);
                }
            });
        }
        function TooltipattachmentName(description) {
            if (description != null & description != "") { return description; }
            else { return "<label class=\"align-center\">No description</label>"; }
        }; $scope.popMe = function (e) {
            var TargetControl = $(e.target); var mypage = TargetControl.attr('data-Name'); if (!(mypage.indexOf("http") == 0)) { mypage = "http://" + mypage; }
            var myname = TargetControl.attr('data-Name'); var w = 1200; var h = 800
            var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
            var win = window.open(mypage, myname, winprops)
        }; function parseSize(size) {
            var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"], tier = 0; while (size >= 1024) { size = size / 1024; tier++; }
            return Math.round(size * 10) / 10 + " " + suffix[tier];
        }
        $("#AttchmentContent").click(function (e) {
            var TargetControl = $(e.target); if (TargetControl.attr('data-role') == 'delete') {
                bootbox.confirm($translate.instant('LanguageContents.Res_2015.Caption'), function (result) {
                    if (result) {
                        $timeout(function () {
                            var val = TargetControl.parents('tr').attr('data-id'); var ext = TargetControl.attr('data-link'); if (ext != 'Link') {
                                CommonService.DeleteFileByID(val).then(function (deleteattach) {
                                    if (deleteattach.Response == true) {
                                        $scope.result.splice(val, 1); if (ext != 'Link') { NotifySuccess($translate.instant('LanguageContents.Res_4793.Caption')); } else { NotifyError($translate.instant('LanguageContents.Res_4796.Caption')); }
                                        LoadAttachments();
                                    }
                                });
                            } else {
                                CommonService.DeleteLinkByID(val).then(function (deleteattach) {
                                    if (deleteattach.Response == true) {
                                        $scope.result.splice(val, 1); if (ext != 'Link') { NotifySuccess($translate.instant('LanguageContents.Res_4793.Caption')); } else { NotifyError($translate.instant('LanguageContents.Res_4796.Caption')); }
                                        LoadAttachments();
                                    }
                                });
                            }
                        }, 100);
                    }
                });
            }
        }); $scope.fnTimeOut = function () {
            $('#filelistCC').empty(); $('#dragCCfiles').show(); $("#totalCCAttachProgress").empty(); $("#totalCCAttachProgress").append('<span class="pull-left count">0 of 0 Uploaded</span><span class="size">0 B / 0 B</span>'
            + '<div class="progress progress-striped active">'
            + '<div style="width: 0%" class="bar"></div>'
            + '</div>'); $scope.StrartUpload(); $timeout(function () { $('#txtLinkName').focus(); }, 1000);
        }
        $scope.StrartUpload = function () {
            var uploader = new plupload.Uploader({ runtimes: 'gears,html5,flash,silverlight,browserplus,html4', browse_button: 'pickfilesAlternate', drop_element: 'dragCCfiles', container: 'filescontainer', max_file_size: '10000mb', url: 'Handlers/UploadHandler.ashx?Type=Attachment', flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf', silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap', chunk_size: '64Kb', multipart_params: {} }); uploader.bind('Init', function (up, params) { uploader.splice(); }); $('#uploadfiles').click(function (e) { uploader.start(); e.preventDefault(); }); $('#clearUploader').click(function (e) { uploader.destroy(); $('.moxie-shim').remove(); }); uploader.init(); $('#pickfiles').each(function () { var input = new mOxie.FileInput({ browse_button: this, multiple: $scope.IsNotVersioning }); input.onchange = function (event) { uploader.addFile(input.files); }; input.init(); }); uploader.bind('FilesAdded', function (up, files) { $.each(files, function (i, file) { var ste = file.name.split('.')[file.name.split('.').length - 1]; $('#filelistCC').append('<div id="' + file.id + '" class="attachmentBox" Data-role="Attachment" data-size="' + file.size + '" data-size-uploaded="0" data-status="false">' + '<div class="row-fluid">' + '<div class="span12">' + '<div class="info">' + '<span class="name" >' + '<i class="icon-file-alt"></i>' + file.name + '</span>' + '<span class="pull-right size">0 B / ' + plupload.formatSize(file.size) + '' + '<i class="icon-remove removefile" data-fileid="' + file.id + '"></i>' + '</span>' + '</div>' + '<div class="progress progress-striped active">' + '<div style="width: 0%" class="bar"></div>' + '</div>' + '</div>' + '</div>' + '<div class="row-fluid">' + '<div class="span12">' + '<form id="Form_' + file.id + '" class="form-inline">' + '<select>' + '<option>' + Mimer(ste) + '</option>' + '<option>Image</option>' + '<option>Video</option>' + '<option>Document</option>' + '<option>Zip</option>' + '</select>' + '<input type="text" name="DescVal_' + file.id + '" id="desc_' + file.id + '" placeholder="Description">' + '</form>' + '</div>' + '</div>' + '</div>'); }); $('#dragCCfiles').hide(); up.refresh(); TotalUploadProgress(); }); $('#filelistCC').on('click', '.removefile', function () {
                var fileToRemove = $(this).attr('data-fileid'); $.each(uploader.files, function (i, file) { if (file.id == fileToRemove) { uploader.removeFile(file); $('#' + file.id).remove(); } }); if ($('#filelistCC .attachmentBox').length == 0) { $('#dragCCfiles').show(); }
                TotalUploadProgress();
            }); uploader.bind('UploadProgress', function (up, file) { $('#' + file.id + " .bar").css("width", file.percent + "%"); $('#' + file.id + " .size").html(plupload.formatSize(Math.round(file.size * (file.percent / 100))) + ' / ' + plupload.formatSize(file.size)); $('#' + file.id).attr('data-size-uploaded', Math.round(file.size * (file.percent / 100))); TotalUploadProgress(); }); uploader.bind('Error', function (up, err) {
                $('#filelistCC').append("<div>Error: " + err.code + ", Message: " + err.message +
                (err.file ? ", File: " + err.file.name : "") + "</div>"); up.refresh();
            }); uploader.bind('FileUploaded', function (up, file, response) { $('#' + file.id).attr('data-status', 'true'); var fileDescription = $('#Form_' + file.id + '').find('input[name="DescVal_' + file.id + '"]').val(); SaveFileDetails(file, response.response, fileDescription); TotalUploadProgress(); }); uploader.bind('BeforeUpload', function (up, file) { $.extend(up.settings.multipart_params, { id: file.id, size: file.size }); }); uploader.bind('FileUploaded', function (up, file, response) { var obj = response; $('#' + file.id).attr('data-fileId', response.response); }); function TotalUploadProgress() { var TotalSize = 0; var TotalCount = $('#filelistCC .attachmentBox').length; var UploadedSize = 0; var UploadedCount = 0; $('#filelistCC .attachmentBox').each(function () { TotalSize += parseInt($(this).attr('data-size')); UploadedSize += parseInt($(this).attr('data-size-uploaded')); if ($(this).attr('data-status') == 'true') { UploadedCount += 1; } }); $('#totalCCAttachProgress .count').html(UploadedCount + ' of ' + TotalCount + ' Uploaded'); $('#totalCCAttachProgress .size').html(plupload.formatSize(UploadedSize) + ' / ' + plupload.formatSize(TotalSize)); $('#totalCCAttachProgress .bar').css("width", Math.round(((UploadedSize / TotalSize) * 100)) + "%"); }
        }
        $scope.InsertableFileList = []; $scope.InsertableFileListforVersion = []; $scope.InsertableAttachmentFilename = []; $scope.InsertableAttachmentFilenameforVersion = []; function SaveFileDetails(file, response, fileDescription) {
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(","); if ($scope.IsNotVersioning == true) { var AttachList = {}; AttachList.Name = file.name; AttachList.VersionNo = 1; AttachList.MimeType = resultArr[1]; AttachList.Extension = extension; AttachList.OwnerID = $cookies['UserId']; AttachList.CreatedOn = Date.now(); AttachList.Checksum = ""; AttachList.ModuleID = 1; AttachList.EntityID = parseInt($stateParams.ID); AttachList.Size = file.size; AttachList.Description = fileDescription; AttachList.OwnerName = $cookies["Username"]; AttachList.FileGuid = resultArr[0]; CommonService.InsertFile(AttachList).then(function (AttachResponse) { if (AttachResponse.Response != 0) { AttachResponse.Id = AttachResponse.Response.Id; $scope.result.push(AttachResponse); NotifySuccess($translate.instant('LanguageContents.Res_4786.Caption')); LoadAttachments(); } }); }
            else {
                $scope.InsertableFileListforVersion.push({ "Name": file.name, "VersionNo": 1, "MimeType": resultArr[1], "Extension": extension, "OwnerID": $cookies['UserId'], "CreatedOn": Date.now(), "Checksum": "", "ModuleID": 1, "EntityID": parseInt($stateParams.ID), "Size": file.size, "FileGuid": resultArr[0], "FileDescription": fileDescription }); $scope.InsertableAttachmentFilenameforVersion.push({ "FileName": file.name, "Extension": extension, "Size": parseSize(file.size), "FileGuid": resultArr[0], "FileDescription": fileDescription, "ID": 0 }); var objfile = $.grep($scope.result, function (n, i) { return $scope.result[i].Id == $scope.FileID; }); if (objfile != null && objfile.length > 0) {
                    var SaveAttachment = {}; SaveAttachment.EntityID = parseInt($stateParams.ID); SaveAttachment.Attachments = $scope.InsertableAttachmentFilenameforVersion; SaveAttachment.Files = $scope.InsertableFileListforVersion; SaveAttachment.VersioningFileId = objfile[0].VersioningFileId; SaveAttachment.FileId = $scope.FileID; CommonService.InsertEntityAttachmentsVersion(SaveAttachment).then(function (SaveResult) {
                        if (SaveResult.StatusCode == 405) { NotifyError($translate.instant('LanguageContents.Res_4275.Caption')); $scope.globalAttachment = true; }
                        else { $scope.contextFileID = SaveResult.Response; $scope.FileID = SaveResult.Response; NotifySuccess($translate.instant('LanguageContents.Res_4388.Caption')); LoadAttachments(); $scope.InsertableAttachmentFilenameforVersion = []; $scope.InsertableFileList = []; $scope.InsertableFileListforVersion = []; $scope.InsertableAttachmentFilename = []; }
                    });
                }
            }
        }
        $scope.versionFileId = 0; $scope.AttachmentVersionFileList = []; $scope.ViewAllFiles = function () { $scope.FileID = $scope.contextFileID; var objfile = $.grep($scope.result, function (n, i) { return $scope.result[i].Id == $scope.FileID; }); if (objfile != null && objfile.length > 0) { $scope.versionFileId = objfile[0].VersioningFileId; var EntityID = parseInt($stateParams.ID); TaskService.ViewAllFilesByEntityID(parseInt($stateParams.ID), objfile[0].VersioningFileId).then(function (FileResult) { if (FileResult.Response != null) { $scope.AttachmentVersionFileList = FileResult.Response; $scope.SelectedVersionID = $scope.AttachmentVersionFileList[0].VersionNo; } }); } }
        $scope.SaveActiveVersion = function () { if ($scope.versionFileId != 0) { var data = {}; data.TaskID = parseInt($stateParams.ID); data.SelectedVersion = $scope.SelectedVersionID; data.VersioningFileId = $scope.versionFileId; TaskService.UpdateAttachmentVersionNo(data).then(function (res) { LoadAttachments(); $("#AttachmentVersion").modal('hide'); }); } }
        function ReloadTaskVersionDetails() { if ($scope.AttachmentVersionFileList.length > 0) { $scope.AttachmentVersionFileList = []; $scope.ViewAllFiles(); } }
        $scope.DeleteFile = function (e) {
            var linkextn = $("#context5copy_" + $scope.contextFileID + "").attr("data-extension"); if (linkextn == "Link") { bootbox.confirm($translate.instant('LanguageContents.Res_2022.Caption'), function (result) { if (result) { $timeout(function () { TaskService.DeleteTaskLinkByid($scope.contextFileID, parseInt($stateParams.ID)).then(function (deleteattach) { if (deleteattach.Response == true) { var fileObj = $.grep($scope.result, function (e) { return e.Id == $scope.contextFileID; }); $scope.result.splice($.inArray(fileObj[0], $scope.result), 1); LoadAttachments(); NotifySuccess($translate.instant('LanguageContents.Res_4793.Caption')); } else { NotifyError($translate.instant('LanguageContents.Res_4297.Caption')); } }); }, 100); } }); }
            else { bootbox.confirm($translate.instant('LanguageContents.Res_2023.Caption'), function (result) { if (result) { $timeout(function () { TaskService.DeleteTaskFileByid($scope.contextFileID, parseInt($stateParams.ID)).then(function (deleteattach) { if (deleteattach.Response == true) { var fileObj = $.grep($scope.result, function (e) { return e.Id == $scope.contextFileID; }); $scope.result.splice($.inArray(fileObj[0], $scope.result), 1); LoadAttachments(); NotifySuccess($translate.instant('LanguageContents.Res_4793.Caption')); } else { NotifyError($translate.instant('LanguageContents.Res_4297.Caption')); } }); }, 100); } }); }
        }; function validateURL(textval) { var urlregex = new RegExp("^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)"); return urlregex.test(textval); }
        $('#addLink').click(function (e) {
            if ($('#txtLinkName').val() == "" || $('#txtLink').val() == "") { bootbox.alert($translate.instant('LanguageContents.Res_4585.Caption')); return false; }
            var ComURL = $('#txtprelink').val() + '://' + $('#txtLink').val(); var AttachLink = {}; AttachLink.EntityID = parseInt($stateParams.ID); AttachLink.Name = $('#txtLinkName').val(); AttachLink.URL = ComURL; AttachLink.Description = $('#txtLinkDesc').val(); AttachLink.ActiveVersionNo = 1; AttachLink.TypeID = 9; AttachLink.CreatedOn = Date.now(); AttachLink.OwnerID = $cookies['UserId']; AttachLink.OwnerName = $cookies['Username']; AttachLink.ModuleID = 1; CommonService.InsertLink(AttachLink).then(function (AttachResponse) { if (AttachResponse.Response != 0) { AttachResponse.Id = AttachResponse.Response.Id; $scope.result.push(AttachResponse); NotifySuccess($translate.instant('LanguageContents.Res_4788.Caption')); $scope.LinkName = ""; $scope.Link = ""; $("#addLinkPopup").modal("hide"); LoadAttachments(); } });
        }); $(document).on('click', '.checkbox-custom > input[id=RootLevelSelectAll]', function (e) { var status = this.checked; $('#AttchmentContent >  tbody input:checkbox').each(function () { this.checked = status; if (status) { $(this).next('i').addClass('checked'); } else { $(this).next('i').removeClass('checked'); } }); }); function GetRootLevelSelectedAll() { var IDList = new Array(); $('#AttchmentContent  > tbody input:checked').each(function () { IDList.push($(this).parents('tr').attr('data-id')); }); return IDList }
    }
    app.controller("mui.planningtool.costcentre.detail.section.attachmentCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$window', '$cookies', '$stateParams', '$translate', 'TaskService', 'CommonService', muiplanningtoolcostcentredetailsectionattachmentCtrl]);
})(angular, app);