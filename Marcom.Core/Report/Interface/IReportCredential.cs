﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Access;
namespace BrandSystems.Marcom.Core.Report.Interface
{
    /// <summary>
    /// IUser interface for table 'RM_ReportCredential'.
    /// </summary>
    public interface IReportCredential
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string ReportUrl
        {
            get;
            set;

        }

        string AdminUsername
        {
            get;
            set;

        }

        string AdminPassword
        {
            get;
            set;

        }
        string ViewerUsername
        {
            get;
            set;

        }

        string ViewerPassword
        {
            get;
            set;

        }

       

        int Category
        {
            get;
            set;

        }

        

        #endregion
    }
}
