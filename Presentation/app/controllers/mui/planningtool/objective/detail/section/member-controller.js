﻿(function (ng, app) {
    "use strict";

    function muiplanningtoolobjectivedetailmemberCtrl($scope, $timeout, $http, $compile, $resource, $stateParams, $window, $translate, MemberService) {
        function sortOn(collection, name) {
            collection.sort(function (a, b) {
                if (a[name] <= b[name]) {
                    return (-1);
                }
                return (1);
            });
        }      
        $scope.UserimageNewTime = new Date().getTime().toString();      
        $scope.groupBy = function (attribute, ISglobal) {
            $scope.groups = [];
            sortOn($scope.memberList, attribute);
            var groupValue = "_INVALID_GROUP_VALUE_";
            for (var i = 0, friend; friend = $scope.memberList[i++];) {
                if (ISglobal == false) {
                    if (friend["IsInherited"] === false) {
                        if (friend[attribute] !== groupValue) {
                            var group = {
                                label: friend[attribute],
                                friends: [],
                                rolename: friend.Role
                            };
                            groupValue = group.label;
                            $scope.groups.push(group);
                        }
                        group.friends.push(friend);
                    }
                } else if (ISglobal == true) {
                    if (friend[attribute] !== groupValue) {
                        var group = {
                            label: friend[attribute],
                            friends: [],
                            rolename: friend.Role
                        };
                        groupValue = group.label;
                        $scope.groups.push(group);
                    }
                    group.friends.push(friend);
                }
            }
        };
        $scope.memberList = [];
        $scope.AutoCompleteSelectedUserObj = {
            "UserSelection": []
        };
        $scope.groups = [];

        function GetLevelfromUniqueKey(UniqueKey) {
            if (UniqueKey != null || UniqueKey.length > 1) {
                var substr = UniqueKey.split('.');
            }
            return substr.length;
        }
        $('#membersDivObj').on('LoadMembersDetailObj', function (event, ID) {
            $scope.EntityID = ID;
            $scope.load();
        });
        $scope.addMember = function () {
            $('#memberModal').modal('show');
            $timeout(function () {
                $('#ddluser').focus();
            }, 1000)
        };
        $scope.AutoCompleteSelectedObj = [];
        $scope.EntityID = $stateParams.ID;
        $scope.IsInherited = true
        $scope.InheritedFromEntityid = $stateParams.ID;
        var levelid = GetLevelfromUniqueKey($window.GlobalUniquekey);
        $scope.ShowGlobalAccess = levelid > 1 ? true : false;
        $scope.UserLists = {};
        $scope.Roles = {};
        $scope.load = function (parameters) {
            MemberService.GetMember($scope.EntityID).then(function (member) {
                var resp = member.Response;
                $scope.memberList = member.Response;
                $scope.groupBy('Roleid', false);
                $scope.QuickInfo1AttributeCaption = $scope.memberList[0].QuickInfo1AttributeCaption;
                $scope.QuickInfo2AttributeCaption = $scope.memberList[0].QuickInfo2AttributeCaption;
                MemberService.GetUsers().then(function (UserbyNamelist) {
                    $scope.UserLists = UserbyNamelist.Response;
                    MemberService.GetEntityDetailsByID($scope.EntityID).then(function (GetEntityresult) {
                        if (GetEntityresult.Response != null) {
                            MemberService.GetEntityTypeRoleAccess(GetEntityresult.Response.Typeid).then(function (role) {
                                $scope.Roles = role.Response;
                            });
                        }
                    });
                });
            });
        };
        $scope.ddlrole = '';
        $scope.addEntityMember = function () {
            try {
                if ($scope.ddlrole == '' || $scope.ddlrole.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1906.Caption'));
                    $('#addObjmemberID').removeClass('disabled');
                    return false;
                } else {
                    $('#addObjmemberID').addClass('disabled');
                }
                var userval = $('#ddluser').val();
                if (userval.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_5749.Caption'));
                    $('#addObjmemberID').removeClass('disabled');
                    return false;
                }
                var InsertMember = {};
                InsertMember.EntityID = $scope.EntityID;
                InsertMember.RoleID = $scope.ddlrole;
                InsertMember.Assignee = parseInt($scope.AutoCompleteSelectedObj[0].Id, 10);
                InsertMember.IsInherited = false;
                InsertMember.InheritedFromEntityid = 0;
                var result = $.grep($scope.memberList, function (e) {
                    return (e.Userid == parseInt($scope.AutoCompleteSelectedObj[0].Id, 10) && e.Roleid == $scope.ddlrole) && (e.IsInherited == false);
                });
                if (result.length == 0) {
                    MemberService.PostMember(InsertMember).then(function (insertmember) {
                        MemberService.GetMember($scope.EntityID).then(function (member) {
                            var resp = member.Response;
                            $scope.memberList = member.Response;
                            var GlobalAccessToshow = ($scope.GlobalAccessStatus.GlobalStatus == 1 ? true : false)
                            $scope.groupBy('Roleid', GlobalAccessToshow);
                        });
                        $('#Objddlrole').select2('val', '');
                        $scope.ddlrole = '';
                        $('#addObjmemberID').removeClass('disabled');
                        $scope.ddlUser = '';
                        $scope.AutoCompleteSelectedObj = [];
                        NotifySuccess($translate.instant('LanguageContents.Res_4479.Caption'));
                    });
                } else {
                    $('#Objddlrole').select2('val', '');
                    $scope.ddlrole = '';
                    $scope.ddlUser = '';
                    $scope.AutoCompleteSelectedObj = [];
                    bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
                    $('#addObjmemberID').removeClass('disabled');
                }
            } catch (e) {
                $('#addObjmemberID').removeClass('disabled');
            }
        };
        $scope.updateEntityMember = function (ID) {
            var updatemember = {};
            updatemember.ID = ID;
            updatemember.EntityID = $scope.EntityID;
            updatemember.RoleID = $scope.ddlrole;
            updatemember.Assignee = parseInt($scope.AutoCompleteSelectedObj[0].Id, 10);
            updatemember.IsInherited = $scope.IsInherited;
            updatemember.InheritedFromEntityid = $scope.InheritedFromEntityid;
            MemberService.PutMember(updatemember).then(function () { });
        };
        $scope.deleteEntityMember = function (ID, Name) {
            if ($scope.IsLock == true) {
                return false;
            }
            bootbox.confirm($translate.instant('LanguageContents.Res_1851.Caption') + " " + Name + " ?", function (result) {
                if (result) {
                    $timeout(function () {
                        MemberService.Member(ID).then(function () {
                            MemberService.GetMember($scope.EntityID).then(function (member) {
                                var resp = member.Response;
                                $scope.memberList = member.Response;
                                var GlobalAccessToshow = ($scope.GlobalAccessStatus.GlobalStatus == 1 ? true : false);
                                $scope.groupBy('Roleid', GlobalAccessToshow);
                            });
                        });
                    }, 100);
                }
            });
        };
        $scope.GlobalAccessStatus = {
            GlobalText: "Show global access",
            GlobalStatus: 0
        };
        $scope.toggleAutoUserAccess = function () {
            if ($scope.GlobalAccessStatus.GlobalStatus == 0) {
                $scope.GlobalAccessStatus.GlobalStatus = 1;
                $scope.GlobalAccessStatus.GlobalText = "Hide global access";
                $scope.groupBy('Roleid', true);
            } else {
                $scope.GlobalAccessStatus.GlobalStatus = 0;
                $scope.GlobalAccessStatus.GlobalText = "Show global access";
                $scope.groupBy('Roleid', false);
            }
        }
        $scope.CloseAddmember = function () {
            $scope.ddlrole = '';
            $('#Objddlrole').select2('val', '');
            $('#addObjmemberID').removeClass('disabled');
            $scope.ddlUser = '';
            $scope.AutoCompleteSelectedObj = [];
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.objective.detail.memberCtrl']"));
        });
        $timeout(function () {
            $scope.load();
        }, 0);
    }
    app.controller("mui.planningtool.objective.detail.memberCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$stateParams', '$window', '$translate', 'MemberService', muiplanningtoolobjectivedetailmemberCtrl]);
})(angular, app);