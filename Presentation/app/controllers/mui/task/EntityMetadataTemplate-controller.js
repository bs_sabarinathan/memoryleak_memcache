﻿(function (ng, app) {
    "use strict";

    function muitaskEntityMetadataTemplateCtrl($window, $location, $timeout, $scope, $cookies, $resource, $compile, $stateParams, $translate, TaskEntityMetadataTemplateService, $sce, $modal) {
        var cloudpath = (cloudsetup.storageType == clientFileStoragetype.Amazon) ? (cloudsetup.Uploaderurl + "/" + cloudsetup.BucketName + "/" + TenantFilePath).replace(/\\/g, "\/") : TenantFilePath;

        $scope.ShowHideAttributeOnRelation = {};
        $scope.NormalDropdownCaption = {};
        $scope.NormalMultiDropdownCaption = {};
        $scope.treeTexts = {};
        $scope.treelevels = {};
        $scope.DropDownTreeOptionValues = {};
        $scope.UploaderCaption = {};
        $scope.listAttriToAttriResult = [];
        $scope.normaltreeSources = {};
        $scope.treeSources = {};
        $scope.isFirstTab = true;
        $scope.loadingControls = false;
        $scope.DamViewName = {};
        $scope.OrderbyName = {};
        $scope.atributesRelationList = [];
        $scope.subentityperiods = [];
        $scope.MulitipleFilterStatus = {};
        $scope.AssetSelectionFiles = [];
        $scope.treesrcdirec = {};
        $scope.staticTreesrcdirec = {};
        $scope.TreeEmptyAttributeObj = {};
        var treeTextVisbileflag = false;
        var treeformflag = false;
        var perioddates = [];
        $scope.GetNonBusinessDaysforDatePicker();
        $scope.treeTextsObj = [];
        $scope.PerioddirectiveCalanderopen = function ($event, attrid, place) {
            $event.preventDefault();
            $event.stopPropagation();
            if (place == "start") $scope.fields["PeriodStartDateopen_" + attrid] = true;
            else $scope.fields["PeriodEndDateopen_" + attrid] = true;
        };
        $scope.settreeTexts = function () {
            var keys2 = [];
            angular.forEach($scope.treeTexts, function (key) {
                keys2.push(key);
                $scope.treeTextsObj = keys2;
            });
        }
        $scope.treeSourcesObj = [];
        $scope.settreeSources = function () {
            var keys = [];
            angular.forEach($scope.treeSources, function (key) {
                keys.push(key);
                $scope.treeSourcesObj = keys;
            });
        }
        $scope.treelevelsObj = [];
        $scope.settreelevels = function () {
            var keys1 = [];
            angular.forEach($scope.treelevels, function (key) {
                keys1.push(key);
                $scope.treelevelsObj = keys1;
            });
        }
        $scope.fields = {
            usersID: ''
        };
        $scope.fieldKeys = [];
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.NormalDropdownCaptionObj = [];
        $scope.setNormalDropdownCaption = function () {
            var keys1 = [];
            angular.forEach($scope.NormalDropdownCaption, function (key) {
                keys1.push(key);
                $scope.NormalDropdownCaptionObj = keys1;
            });
        }
        $scope.NormalMultiDropdownCaptionObj = [];
        $scope.setNormalMultiDropdownCaption = function () {
            var keys1 = [];
            angular.forEach($scope.NormalMultiDropdownCaption, function (key) {
                keys1.push(key);
                $scope.NormalMultiDropdownCaptionObj = keys1;
            });
        }
        $scope.UploaderCaptionObj = [];
        $scope.setUploaderCaption = function () {
            var keys1 = [];
            angular.forEach($scope.UploderCaption, function (key) {
                keys1.push(key);
                $scope.UploaderCaptionObj = keys1;
            });
        }
        $scope.Calanderopen = function ($event, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            $scope.fields["DatePart_Calander_Open" + model] = false;
        };
        $scope.dynCalanderopen = function ($event, isopen, model) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = false;
            $scope.fields["DatePart_Calander_Open" + model] = true;
        };
        $scope.ClearModelObject = function (ModelObject) {
            for (var variable in ModelObject) {
                if (typeof ModelObject[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        ModelObject[variable] = "";
                    }
                } else if (typeof ModelObject[variable] === "number") {
                    ModelObject[variable] = null;
                } else if (Array.isArray(ModelObject[variable])) {
                    ModelObject[variable] = [];
                } else if (typeof ModelObject[variable] === "object") {
                    ModelObject[variable] = {};
                }
            }
        }
        $scope.$on('LoadTaskMetadata', function TaskMetadataDetails(event, taskinfo) {
            $scope.dyn_Cont = "";
            $scope.EntityID = taskinfo.TaskId;
            $scope.IsLock = taskinfo.IsLock;
            $scope.visibleID = taskinfo.visibleID;
            $scope.visibleName = taskinfo.visibleName;
            $scope.Entitytypeid = taskinfo.TaskTypeId;
            $scope.parentid = taskinfo.ParentId;
            if (taskinfo.Response.length == 0) {
                TaskEntityMetadataTemplateService.GetEntityRelatedDataOnLoad_Set2($scope.EntityID, parseInt($cookies['UserId']), 0, false, SystemDefinedEntityTypes.Milestone).then(function (CurrentTaskRelatedMetadata) {
                    if (CurrentTaskRelatedMetadata.Response != null) {
                        if (CurrentTaskRelatedMetadata != null) {
                            $scope.tempresultHolder_Set2 = CurrentTaskRelatedMetadata.Response;
                            LoadTaskMetadata($scope.tempresultHolder_Set2[0], $scope.tempresultHolder_Set2[1], $scope.tempresultHolder_Set2[2], $scope.tempresultHolder_Set2[3]);
                        }
                    }
                });
            } else {
                LoadTaskMetadata(null, [], {}, taskinfo.Response);
            }
        })

        function LoadTaskMetadata(AttrtoAttrRel, EntAttrValDet, CostCentreFin, EntAttrDet) {
            var entityAttrToAttrRelation = AttrtoAttrRel;
            if (entityAttrToAttrRelation != null) $scope.listAttriToAttriResult = entityAttrToAttrRelation;
            var ID = $scope.EntityID;
            $scope.items = [];
            $scope.fields = {
                usersID: 0,
            };
            $scope.tree = {};
            $scope.fieldKeys = [];
            $scope.options = {};
            $scope.setFieldKeys = function () {
                var keys = [];
                angular.forEach($scope.fields, function (key) {
                    keys.push(key);
                    $scope.fieldKeys = keys;
                });
            }
            $scope.EnableDisableControlsHolder = {};
            $scope.DropDownTreePricing = {};
            $scope.PercentageVisibleSettings = {};

            function IsNotEmptyTree(treeObj) {
                for (var i = 0, node; node = treeObj[i++];) {
                    if (node.ischecked == true) {
                        treeTextVisbileflag = true;
                        return treeTextVisbileflag;
                    } else {
                        IsNotEmptyTree(node.Children);
                    }
                }
                return treeTextVisbileflag;
            }
            $scope.StopeUpdateStatusonPageLoad = false;
            $scope.detailsLoader = false;
            $scope.detailsData = true;
            $scope.dyn_Cont = '';
            var getentityattributesdetails = EntAttrDet;
            $scope.dyn_Cont = "";
            $scope.attributedata = getentityattributesdetails;
            if ($scope.visibleID == true) {
                $scope.dyn_Cont += '<div class="control-group"><label class="control-label" for="label"> ID </label><div class="controls"><label class="control-label" for="label">' + ID + '</label></div></div>'
            }
            if (!$scope.IsLock && $scope.visibleName) {

                $scope.ActivityName = $scope.RootLevelEntityName;
                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\> Name </label><div class=\"controls\"><a xeditable href=\"javascript:;\" ng-click="entityeditcontrolclick()" attributeTypeID="1" entityid="' + ID + '" attributeTypeID="1" attributeid="68" id=\"ActivityName\" data-ng-model=\"ActivityName"\   my-qtip2 qtip-content=\" Name \"  data-type=\"text\" data-original-title=\"Activity Name\">' + $scope.ActivityName + '</a></div></div>';

            } else if ($scope.IsLock && $scope.visibleName) {
                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\> Name </label><div class=\"controls\"><label class="control-label">' + $scope.RootLevelEntityNameTemp + '</label></div></div>';
            }
            if ($scope.attributedata != null || $scope.attributedata != undefined) {
                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 6) {
                        $scope.dyn_Cont2 = '';
                        var CaptionObj = $scope.attributedata[i].Caption.split(",");
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            if (j == 0) {
                                if (CaptionObj[j] != undefined) {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.settreeTexts();
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.setFieldKeys();
                                } else {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.settreeTexts();
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.setFieldKeys();
                                }
                            } else {
                                if (CaptionObj[j] != undefined) {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.settreeTexts();
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.setFieldKeys();
                                } else {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.settreeTexts();
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.setFieldKeys();
                                }
                            }
                        }
                        $scope.treelevels["dropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                        $scope.settreelevels();
                        $scope.items = [];
                        $scope.settreeSources();
                        $scope.settreeTexts();
                        $scope.settreelevels();
                        $scope.setFieldKeys();
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                            var inlineEditabletitile = $scope.treelevels['dropdown_levels_' + $scope.attributedata[i].ID][j].caption;
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_' + (j + 1) + '\',' + $scope.attributedata[i].TypeID + ') class=\"control-group\"><label class=\"control-label AttrID_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_' + (j + 1) + '\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditabletreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.DropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" attributename=\"' + inlineEditabletitile + '\" data-type=\"' + inlineEditabletitile + $scope.attributedata[i].ID + '\" >{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_' + (j + 1) + '\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 12) {
                        $scope.dyn_Cont2 = '';
                        var CaptionObj = $scope.attributedata[i].Caption;
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            if ($scope.attributedata[i].Lable.length == 1) {
                                var k = j;
                                var treeTexts = [];
                                var fields = [];
                                $scope.items.push({
                                    caption: $scope.attributedata[i].Lable[j].Label,
                                    level: j + 1
                                });
                                if (k == CaptionObj.length) {
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                } else {
                                    if (CaptionObj[k] != undefined) {
                                        for (k; k < CaptionObj.length; k++) {
                                            treeTexts.push(CaptionObj[k]);
                                            $scope.settreeTexts();
                                            fields.push(CaptionObj[k]);
                                            $scope.setFieldKeys();
                                        }
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                    } else {
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    }
                                }
                            } else {
                                if (j == 0) {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.settreeTexts();
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.setFieldKeys();
                                    } else {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.setFieldKeys();
                                    }
                                } else {
                                    var k = j;
                                    if (j == ($scope.attributedata[i].Lable.length - 1)) {
                                        var treeTexts = [];
                                        var fields = [];
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        if (k == CaptionObj.length) {
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        } else {
                                            if (CaptionObj[k] != undefined) {
                                                for (k; k < CaptionObj.length; k++) {
                                                    treeTexts.push(CaptionObj[k]);
                                                    $scope.settreeTexts();
                                                    fields.push(CaptionObj[k]);
                                                    $scope.setFieldKeys();
                                                }
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                            } else {
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            }
                                        }
                                    } else {
                                        if (CaptionObj[j] != undefined) {
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.settreeTexts();
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.setFieldKeys();
                                        } else {
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.settreeTexts();
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.setFieldKeys();
                                        }
                                    }
                                }
                            }
                        }
                        $scope.treelevels["multiselectdropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                        $scope.settreelevels();
                        $scope.items = [];
                        $scope.settreeSources();
                        $scope.settreeTexts();
                        $scope.settreelevels();
                        $scope.setFieldKeys();
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                            var inlineEditabletitile = $scope.treelevels['multiselectdropdown_levels_' + $scope.attributedata[i].ID][j].caption;
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_' + (j + 1) + '\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_' + (j + 1) + '\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditablemultiselecttreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  attributename=\"' + inlineEditabletitile + '\" data-type=\"' + inlineEditabletitile + '\" >{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_' + (j + 1) + '\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><label class="control-label">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</label></div></div>';
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext   href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"  my-qtip2 qtip-content=\"' + $scope.attributedata[i].Lable + '\"  data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 2) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\"  attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"MultiLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\" data-type="' + $scope.attributedata[i].ID + '" my-qtip2 qtip-content=\"' + $scope.attributedata[i].Lable + '\" data-original-title=\"' + $scope.attributedata[i].Lable + '\">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 11) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                        $scope.setFieldKeys();
                        $scope.UploaderCaption["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                        $scope.setUploaderCaption();
                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group ng-scope AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable.toString() + '</label>';
                        $scope.dyn_Cont += '<div class=\"controls\">';

                        if ($scope.attributedata[i].Caption == "" || $scope.attributedata[i].Caption == null && $scope.attributedata[i].Caption == undefined) {
                            $scope.attributedata[i].Caption = $scope.attributedata[i].Lable;
                        }
                        if ($scope.attributedata[i].Value == "" || $scope.attributedata[i].Value == null && $scope.attributedata[i].Value == undefined) {
                            $scope.attributedata[i].Value = "NoThumpnail.jpg";
                        }
                        if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                            $scope.dyn_Cont += '<img src="' + cloudpath + 'UploadedImages/' + $scope.attributedata[i].Value + '" alt="' + $scope.attributedata[i].Caption + '"';
                        }
                        else {
                            $scope.dyn_Cont += '<img src="' + TenantFilePath + 'UploadedImages/' + $scope.attributedata[i].Value + '" alt="' + $scope.attributedata[i].Caption + '"';
                        }
                        $scope.dyn_Cont += ' class="entityDetailImgPreview" id="UploaderPreview_' + $scope.attributedata[i].ID + '">';
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '</div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += "<a ng-click='UploadImagefile(" + $scope.attributedata[i].ID + ")'  attributeTypeID='" + $scope.attributedata[i].TypeID + "'";
                                $scope.dyn_Cont += 'entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="Uploader_' + $scope.attributedata[i].ID + '"';
                                $scope.dyn_Cont += 'my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                // $scope.dyn_Cont += ' class="ng-pristine ng-valid" attributename=\"' + $scope.UploaderCaption["UploaderCaption_" + $scope.attributedata[i].ID] + '\"> Select Image';
                                $scope.dyn_Cont += ' class="ng-pristine ng-valid" attributename=\"' + $scope.UploaderCaption["uploader_" + $scope.attributedata[i].ID] + '\"> Select Image';
                                $scope.dyn_Cont += '</a></img></div></div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '</div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 3) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                }
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                    $scope.dyn_Cont += '<div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span>';
                                    $scope.dyn_Cont += '</div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                        $scope.dyn_Cont += '<div class="controls"><a  xeditabledropdown href=\"javascript:;\"';
                                        $scope.dyn_Cont += 'attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '"';
                                        $scope.dyn_Cont += 'attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"';
                                        $scope.dyn_Cont += 'data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                        $scope.dyn_Cont += 'attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\"';
                                        $scope.dyn_Cont += 'data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a>';
                                        $scope.dyn_Cont += '</div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                        $scope.dyn_Cont += '<div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label>';
                                        $scope.dyn_Cont += '</div></div>';
                                    }
                                }
                            }
                        } else {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption[0].length > 0) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.setFieldKeys();
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    $scope.setNormalDropdownCaption();
                                    if ($scope.attributedata[i].IsReadOnly == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    } else {
                                        if ($scope.IsLock == false) {
                                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                        } else if ($scope.IsLock == true) {
                                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                        }
                                    }
                                }
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.setFieldKeys();
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalDropdownCaption();
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 4) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.setFieldKeys();
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                }
                            }
                        } else {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.setFieldKeys();
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 10) {
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        perioddates = [];
                        $scope.dyn_Cont += '<div class="period nomargin" data-periodcontainerID="periodcontainerID">';
                        if ($scope.attributedata[i].Value == "-") {
                            $scope.IsStartDateEmpty = true;
                            $scope.dyn_Cont += '<div class="nomargin" data-addperiodID="addperiodID">';
                            $scope.dyn_Cont += '</div>';
                        } else {
                            for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
                                var datStartUTCval = "";
                                var datstartval = "";
                                var datEndUTCval = "";
                                var datendval = "";
                                datStartUTCval = $scope.attributedata[i].Value[j].Startdate.substr(0, 10);
                                datstartval = new Date.create(datStartUTCval);
                                datEndUTCval = $scope.attributedata[i].Value[j].EndDate.substr(0, 10);
                                datendval = new Date.create(datEndUTCval);
                                perioddates.push({
                                    ID: $scope.attributedata[i].Value[j].Id,
                                    value: datendval
                                });
                                $scope.fields["PeriodStartDateopen_" + $scope.attributedata[i].Value[j].Id] = false;
                                $scope.fields["PeriodEndDateopen_" + $scope.attributedata[i].Value[j].Id] = false;
                                $scope.MinValue = $scope.attributedata[i].MinValue;
                                $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id] = new Date.create();
                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MinValue + 1));
                                } else {
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MaxValue - 1));
                                } else {
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].getDate() + 100000);
                                }
                                var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id]);
                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id] = (temp.MinDate);
                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id] = (temp.MaxDate);
                                $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datstartval, $scope.format);
                                $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datendval, $scope.format);
                                $scope.fields["PeriodStartDate_Dir_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datstartval, $scope.format);
                                $scope.fields["PeriodEndDate_Dir_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datendval, $scope.format);
                                if ($scope.attributedata[i].Value[j].Description == undefined) {
                                    $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = "-";
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "";
                                } else {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                    $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                }
                                $('#fsedateid').css("visibility", "hidden");
                                $scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + $scope.attributedata[i].Value[j].Id + '">';
                                $scope.dyn_Cont += '<div class="inputHolder nomargin span11">';
                                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label>';
                                $scope.dyn_Cont += '<div class="controls">';
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}';
                                    $scope.dyn_Cont += ' to {{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</label>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                        $scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}';
                                        $scope.dyn_Cont += ' to {{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</label>';
                                    }
                                }
                                $scope.dyn_Cont += '</div></div>';
                                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + ' Comment </label>';
                                $scope.dyn_Cont += '<div class="controls">';
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</label>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<label class="control-label">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</label>';
                                    }
                                }
                                $scope.dyn_Cont += '</div></div></div>';
                                if (j != 0) {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + $scope.attributedata[i].Value[j].Id + ')"><i class="icon-remove"></i></a></div>';
                                    }
                                }
                                $scope.dyn_Cont += '</div>';
                                if (j == ($scope.attributedata[i].Value.length - 1)) {
                                    $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';
                                    $scope.dyn_Cont += '</div>';
                                }
                            }
                        }
                        $scope.dyn_Cont += ' </div>';
                        $scope.MinValue = $scope.attributedata[i].MinValue;
                        $scope.MaxValue = $scope.attributedata[i].MaxValue;
                        $scope.fields["DatePartMinDate_0"] = new Date.create();
                        $scope.fields["DatePartMaxDate_0"] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.fields["DatePartMinDate_0"].setDate($scope.fields["DatePartMinDate_0"].getDate() + ($scope.MinValue + 1));
                        } else {
                            $scope.fields["DatePartMinDate_0"].setDate($scope.fields["DatePartMinDate_0"].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_0"].setDate($scope.fields["DatePartMaxDate_0"].getDate() + ($scope.MaxValue - 1));
                        } else {
                            $scope.fields["DatePartMaxDate_0"].setDate($scope.fields["DatePartMaxDate_0"].getDate() + 100000);
                        }
                        $scope.dyn_Cont += '<div class="control-group nomargin">';
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<label  data-tempid="startendID" class="control-label" for="label">' + "Start /End date" + '</label>';
                            $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[' + "Add Start / End date" + ' ]</a>';
                        } else {
                            if ($scope.IsLock == false) {
                                if ($scope.attributedata[i].Value == "-") {
                                    $scope.dyn_Cont += '<label id="fsedateid"  class="control-label" for="label">' + inlineEditabletitile + '</label>';
                                }
                                $scope.dyn_Cont += '<div class="controls">';
                                $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[Add ' + inlineEditabletitile + ' ]</a>';
                                $scope.dyn_Cont += '</div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<span></span>';
                            }
                        }
                        $scope.dyn_Cont += '</div>';
                    } else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        var datStartUTCval = "";
                        var datstartval = "";
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
                            datStartUTCval = $scope.attributedata[i].Value.substr(0, 10);
                            datstartval = new Date.create(datStartUTCval);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateFormat(datstartval, $scope.format);
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = formatteddateFormat(datstartval, "dd/MM/yyyy");
                        } else {
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                        }
                        if ($scope.attributedata[i].ID != SystemDefiendAttributes.CreationDate) {
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.MinValue = $scope.attributedata[i].MinValue;
                                    $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = new Date.create();
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = new Date.create();
                                    if ($scope.MinValue < 0) {
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue + 1));
                                    } else {
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue));
                                    }
                                    if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MaxValue - 1));
                                    } else {
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + 100000);
                                    }
                                    var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID]);
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = (temp.MinDate);
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = (temp.MaxDate);
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a xeditabletreedatetime data-primaryid="' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-datetime_id=\"DateTime_' + $scope.attributedata[i].ID + '\" data-ng-model=\"DateTime_' + $scope.attributedata[i].ID + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"datetime' + $scope.attributedata[i].ID + '\" href=\"javascript:;\" attributename="datetime">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    $scope.fields["DatePart_" + $scope.attributedata[i].ID] = null;
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        } else {
                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 7) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["Tree_" + $scope.attributedata[i].ID] = [];
                        $scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                        GetTreeCheckedNodes($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID], $scope.attributedata[i].ID);
                        $scope.staticTreesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\">';
                        $scope.dyn_Cont += '<label class="control-label">' + $scope.attributedata[i].Lable + ' </label>';
                        $scope.dyn_Cont += '<div class="controls">';
                        if ($scope.IsLock == false) {
                            $scope.dyn_Cont += '<div xeditabletree  editabletypeid="treeType_' + $scope.attributedata[i].ID + '" attributename=\"' + $scope.attributedata[i].Lable + '\" isreadonly="' + $scope.attributedata[i].IsReadOnly + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '"  data-type="treeType_' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"' + $scope.attributedata[i].ID + '\" data-ng-model=\"tree_' + $scope.attributedata[i].ID + '"\    data-original-title=\"' + $scope.attributedata[i].Lable + '\">';
                            if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                                treeTextVisbileflag = false;
                                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                                    $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                                } else $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            } else {
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            }
                            $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\" treeplace="detail" node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                            $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';
                            $scope.dyn_Cont += ' </div>';
                        } else {
                            if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                                treeTextVisbileflag = false;
                                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                                    $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                                } else $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            } else {
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            }
                            $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\"  node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                            $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';
                        }
                        $scope.dyn_Cont += '</div></div>';
                    } else if ($scope.attributedata[i].TypeID == 8) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = ($scope.attributedata[i].Value).formatMoney(0, ' ', ' ');
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"   data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 19) {
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                            if ($scope.attributedata[i].Value != null) {
                                $scope['origninalamountvalue_' + $scope.attributedata[i].ID] = $scope.attributedata[i].Value.Amount;
                                $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html((($scope.attributedata[i].Value.Amount).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' '))).text();
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value.Currencytypeid;
                                var currtypeid = $scope.attributedata[i].Value.Currencytypeid;
                                $scope.currtypenameobj = ($.grep($scope.CurrencyFormatsList, function (e) {
                                    return e.Id == currtypeid;
                                }));
                                $scope.fields["currtypename_" + $scope.attributedata[i].ID] = $scope.currtypenameobj[0]["ShortName"];
                            } else {
                                $scope['origninalamountvalue_' + $scope.attributedata[i].ID] = 0;
                                $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.DefaultSettings.CurrencyFormat.Id;
                                $scope.fields["currtypename_" + $scope.attributedata[i].ID] = "-";
                            }
                            $scope.currencytypeslist = $scope.CurrencyFormatsList;
                            $scope.setFieldKeys();
                            $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalDropdownCaption();
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label widthauto ng-binding">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label><span class="va-middle inlineBlock padding-top5x margin-left5x color-info ng-binding">{{fields.currtypename_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletextforcurrencyamount   href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"  my-qtip2 qtip-content=\"' + $scope.attributedata[i].Lable + '\"  data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}<span class="margin-left5x">{{fields.currtypename_' + $scope.attributedata[i].ID + '}}</span></a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label widthauto ng-binding">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label><span class="va-middle inlineBlock padding-top5x margin-left5x color-info ng-binding">{{fields.currtypename_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 13) {
                        $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.attributedata[i].ID + ""] = $scope.attributedata[i].DropDownPricing;
                        $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.attributedata[i].ID + ""] = true;
                        for (var j = 0, price; price = $scope.attributedata[i].DropDownPricing[j++];) {
                            if (price.selection.length > 0) {
                                var selectiontext = "";
                                var valueMatches = [];
                                if (price.selection.length > 0) valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                    return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                });
                                if (valueMatches.length > 0) {
                                    selectiontext = "";
                                    for (var x = 0, val; val = valueMatches[x++];) {
                                        selectiontext += val.caption;
                                        if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                        else selectiontext += "</br>";
                                    }
                                } else selectiontext = "-";
                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + j] = selectiontext;
                            } else {
                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + j] = "-";
                            }
                            if ($scope.attributedata[i].IsReadOnly == false && $scope.IsLock == false) {
                                $scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = false;
                                $scope.dyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><a  href=\"javascript:;\" xeditablepercentage entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + j + '" editabletypeid="percentagetype' + $scope.attributedata[i].ID + '_' + j + '" data-type="percentagetype' + $scope.attributedata[i].ID + '_' + j + '"  my-qtip2 qtip-content=\"' + price.LevelName + '\"  attributename=\"' + price.LevelName + '\"  ><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + j + ')"></span></a></div></div>';
                            } else {
                                $scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = true;
                                $scope.dyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><span class="editable"><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + j + ')"></span></span></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 17) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 0) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value;
                                $scope.setFieldKeys();
                                $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabletagwords href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="TagWordsCaption_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.TagWordsCaption_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                    }
                                }
                            }
                        } else {
                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                            $scope.setFieldKeys();
                            $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = [];
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabletagwords href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="TagWordsCaption_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.TagWordsCaption_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 16) {
                        var dateExpireUTCval = "";
                        var dateExpireval = "";
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        var dateExpire;
                        if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
                            var dateExpireUTCval = "";
                            var dateExpireval = "";
                            dateExpireUTCval = $scope.attributedata[i].Value.substr(0, 10);
                            dateExpireval = new Date.create(dateExpireUTCval);
                            dateExpire = dateFormat((dateExpireUTCval), $scope.format);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateExpire;
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = dateExpire;
                            $scope.fields["ExpireDateTime_" + $scope.attributedata[i].ID] = dateExpire;
                        } else {
                            dateExpire = "-"
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["ExpireDateTime_" + $scope.attributedata[i].ID] = "-";
                        }
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.ExpireDateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable"><a data-AttributeID="' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" data-expiredate="' + dateExpire + '" data-attrlable="' + $scope.attributedata[i].Lable + '" ng-click="openExpireAction($event)">{{fields.ExpireDateTime_' + $scope.attributedata[i].ID + '}}</a></span></div></div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.ExpireDateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 16) {
                        var dateExpireUTCval = "";
                        var dateExpireval = "";
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        var dateExpire;
                        if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
                            var dateExpireUTCval = "";
                            var dateExpireval = "";
                            dateExpireUTCval = $scope.attributedata[i].Value.substr(0, 10);
                            dateExpireval = new Date.create(dateExpireUTCval);
                            dateExpire = dateFormat(ConvertDateFromStringToString(ConvertDateToString(dateExpireval)).toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3").toString('dd-MM-yyyy'), $scope.DefaultSettings.DateFormat);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateExpire;
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = dateExpire;
                        }
                    } else if ($scope.attributedata[i].TypeID == 18) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        $scope.dyn_Cont += '<div ng-show= ShowOrHideAttributeToAttributeRelationInOverview(\'' + $scope.attributedata[i].ID + '_0\',' + $scope.attributedata[i].TypeID + ') class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                    } else if ($scope.attributedata[i].TypeID == 9) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["CheckBoxSelection_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value;
                        $scope.dyn_Cont += "<div ng-show= ShowHideAttributeOnRelation.Attribute_" + $scope.attributedata[i].ID + "  class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.attributedata[i].AttributeID + "\">" + $scope.attributedata[i].Caption + " : </label><div class=\"controls\"><input ng-click=\"saveDropdownTree(" + $scope.attributedata[i].ID + "," + $scope.attributedata[i].TypeID + "," + parseInt($stateParams.ID) + "," + $scope.fields["CheckBoxSelection_" + $scope.attributedata[i].ID] + "  )\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.attributedata[i].ID + "\" id=\"CheckBoxSelection_" + $scope.attributedata[i].ID + "\" placeholder=\"" + $scope.attributedata[i].Caption + "\"></div></div>";
                    }
                }
            }
            $timeout(function () {
                $(".dynamicdetail").empty();
                $(".dynamicdetail").append($scope.dyn_Cont);
                $compile($(".dynamicdetail").contents())($scope);
            }, 300);

            //$timeout(function () { getentityattributesdetails() }, 100);
            $scope.CurrentWorkflowStepName = "";
            $scope.workflowstepOptions = [];
            $scope.workflowSummaryList = [];
            $scope.SelectedWorkflowStep = 0;
            $scope.WorkFlowStepID = 0;
            $scope.WorkflowSummaryControls = '';
            $scope.WorkFlowDiable = true;
        }
        $scope.renderHtml = function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };
        $scope.UploadImagefile = function (attributeId) {
            $scope.UploadAttributeId = attributeId;
            $("#pickfiles100").click();
        }
        $timeout(function () {
            $scope.StrartUpload();
        }, 100);
        $scope.StrartUpload = function () {
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                var uploader = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfiles100',
                    container: 'filescontainer100',
                    max_file_size: '10mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: amazonURL + cloudsetup.BucketName,
                    multi_selection: false,
                    filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png"
                    }],
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }
                });
                uploader.bind('Init', function (up, params) {
                    uploader.splice();
                });
                uploader.init();
                uploader.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader.start();
                });
                uploader.bind('UploadProgress', function (up, file) {
                    $('#uploadprogress' + $scope.PopupModalGroupID + ' .bar').css("width", file.percent + "%");
                });
                uploader.bind('Error', function (up, err) {
                    up.refresh();
                });
                uploader.bind('FileUploaded', function (up, file, response) {
                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    SaveFileDetails(file, response.response);
                });
                uploader.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKey = (TenantFilePath + "UploadedImages/Temp/" + file.id + fileext).replace(/\\/g, "\/");
                    uploader.settings.multipart_params.key = uniqueKey;
                    uploader.settings.multipart_params.Filename = uniqueKey;
                });
            }
            else {
                var uploader = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfiles100',
                    container: 'filescontainer100',
                    max_file_size: '10mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: 'Handlers/UploadUploaderImage.ashx?Type=Attachment',
                    multi_selection: false,
                    filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png"
                    }]
                });
                uploader.bind('Init', function (up, params) {
                    uploader.splice();
                });
                uploader.init();
                uploader.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader.start();
                });
                uploader.bind('UploadProgress', function (up, file) {
                    $('#uploadprogress' + $scope.PopupModalGroupID + ' .bar').css("width", file.percent + "%");
                });
                uploader.bind('Error', function (up, err) {
                    up.refresh();
                });
                uploader.bind('FileUploaded', function (up, file, response) {
                    var fileid = response.response.split(',')[0];
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = fileid + "," + GetMIMEType(file.name) + "," + fileext;
                    SaveFileDetails(file, response.response)
                });
                uploader.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
            }

        }
        function SaveFileDetails(file, response) {
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");
            var uplaodImageObject = {};
            uplaodImageObject.Name = file.name;
            uplaodImageObject.VersionNo = 1;
            uplaodImageObject.MimeType = resultArr[1];
            uplaodImageObject.Extension = extension;
            uplaodImageObject.OwnerID = $cookies['UserId'];
            uplaodImageObject.CreatedOn = Date.now();
            uplaodImageObject.Checksum = "";
            uplaodImageObject.ModuleID = 1;
            uplaodImageObject.taskID = $scope.EntityID;
            uplaodImageObject.AttributeID = $scope.UploadAttributeId;
            uplaodImageObject.Size = file.size;
            uplaodImageObject.FileName = resultArr[0] + extension;
            var PreviewID = "UploaderPreview_" + $scope.UploadAttributeId;
            TaskEntityMetadataTemplateService.copyuploadedImage(uplaodImageObject.FileName).then(function (ImgRes) {
                if (ImgRes.Response != null) {
                    uplaodImageObject.FileName = ImgRes.Response;
                    TaskEntityMetadataTemplateService.UpdateImageNameforTask(uplaodImageObject).then(function (uplaodImageObjectResult) {
                        $('#UplaodImagediv').modal('hide');
                        if (uplaodImageObjectResult.Response) {
                            NotifySuccess($translate.instant('LanguageContents.Res_4808.Caption'));

                            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                                $('#' + PreviewID).attr('src', cloudpath + 'UploadedImages/' + uplaodImageObject.FileName);
                            }
                            else {
                                $('#' + PreviewID).attr('src', TenantFilePath + 'UploadedImages/' + uplaodImageObject.FileName);
                            }

                        }
                    });
                }
            })
        }
        $scope.openExpireAction = function (event) {
            var attrevent = event.currentTarget;
            $scope.SourceAttributeID = attrevent.dataset.attributeid;
            $scope.SourceAttributename = attrevent.dataset.attrlable;
            $scope.dateExpireval = $scope.fields["ExpireDateTime_" + $scope.SourceAttributeID];
            var sourcefrom = 3;
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/expirehandleractions.html',
                controller: "mui.muiexpireaddActionsCtrl",
                resolve: {
                    params: function () {
                        return {
                            dateExpireval: $scope.dateExpireval,
                            AssetEditID: $scope.EntityID,
                            assetEntityId: $scope.parentid,
                            sourcefrom: sourcefrom,
                            assettypeidfrbtn: $scope.Entitytypeid,
                            ProductionEntityID: $scope.parentid,
                            SourceAttributeID: $scope.SourceAttributeID,
                            SourceAttributename: $scope.SourceAttributename
                        };
                    }
                },
                scope: $scope,
                windowClass: 'expirehandlerAddActionsPopup popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }
        $scope.$on('updateExpireactiontaskEditvalues', function (event, SourceID, EntityID, AttributeID, Attributevalue) {
            $scope.fields["ExpireDateTime_" + AttributeID] = Attributevalue;
        });
        $scope.saveDropDownTreePricing = function (attrID, attributetypeid, entityTypeid, choosefromParent, inherritfromParent) {
            if (attributetypeid == 13) {
                var NewValue = [];
                NewValue = ReturnSelectedTreeNodes(attrID);
                var updateentityattrib = {};
                updateentityattrib.NewValue = NewValue;
                updateentityattrib.AttributetypeID = attributetypeid;
                updateentityattrib.EntityID = parseInt($scope.EntityID, 10);
                updateentityattrib.AttributeID = attrID;
                TaskEntityMetadataTemplateService.UpdateDropDownTreePricing(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        var GetTreeRes;
                        if (choosefromParent) {
                            TaskEntityMetadataTemplateService.GetDropDownTreePricingObjectFromParentDetail(attrID, inherritfromParent, choosefromParent, parseInt($scope.EntityID, 10), 0).then(function (GetTree) {
                                if (GetTree.Response != null) {
                                    var result = GetTree.Response;
                                    for (var p = 0, price; price = result[p++];) {
                                        var attributeLevelOptions = [];
                                        attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""], function (e) {
                                            return e.level == p;
                                        }));
                                        if (attributeLevelOptions[0] != undefined) {
                                            attributeLevelOptions[0].selection = price.selection;
                                            attributeLevelOptions[0].LevelOptions = price.LevelOptions;
                                            if (price.selection.length > 0) {
                                                var selectiontext = "";
                                                var valueMatches = [];
                                                if (price.selection.length > 0) valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                                    return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                                });
                                                if (valueMatches.length > 0) {
                                                    selectiontext = "";
                                                    for (var x = 0, val; val = valueMatches[x++];) {
                                                        selectiontext += val.caption;
                                                        if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                                        else selectiontext += "</br>";
                                                    }
                                                } else selectiontext = "-";
                                                $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = selectiontext;
                                            } else {
                                                $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = "-";
                                            }
                                        }
                                    }
                                }
                            });
                        } else {
                            TaskEntityMetadataTemplateService.GetDropDownTreePricingObjectFromParentDetail(attrID, inherritfromParent, choosefromParent, parseInt($scope.EntityID, 10), 0).then(function (GetTree) {
                                if (GetTree.Response != null) {
                                    var result = GetTree.Response;
                                    for (var p = 0, price; price = result[p++];) {
                                        var attributeLevelOptions = [];
                                        attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""], function (e) {
                                            return e.level == p;
                                        }));
                                        if (attributeLevelOptions[0] != undefined) {
                                            attributeLevelOptions[0].selection = price.selection;
                                            attributeLevelOptions[0].LevelOptions = price.LevelOptions;
                                            if (price.selection.length > 0) {
                                                var selectiontext = "";
                                                var valueMatches = [];
                                                if (price.selection.length > 0) valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                                    return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                                });
                                                if (valueMatches.length > 0) {
                                                    selectiontext = "";
                                                    for (var x = 0, val; val = valueMatches[x++];) {
                                                        selectiontext += val.caption;
                                                        if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                                        else selectiontext += "</br>";
                                                    }
                                                } else selectiontext = "-";
                                                $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = selectiontext;
                                            } else {
                                                $scope.treeTexts["multiselectdropdown_text_" + attrID + "_" + p] = "-";
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }

        function ReturnSelectedTreeNodes(attrID) {
            var selection = [];
            for (var y = 0, price; price = $scope.DropDownTreePricing["AttributeId_Levels_" + attrID + ""][y++];) {
                if (price.selection.length > 0) {
                    var matches = [];
                    matches = jQuery.grep(price.LevelOptions, function (relation) {
                        return price.selection.indexOf(relation.NodeId.toString()) != -1;
                    });
                    if (matches.length > 0) for (var z = 0, node; node = matches[z++];) {
                        selection.push({
                            "NodeId": node.NodeId,
                            "Level": price.level,
                            "value": node.value != "" ? node.value : "-1"
                        })
                    }
                }
            }
            return selection;
        }
        $scope.ShowOrHideAttributeToAttributeRelationInOverview = function (attrID, attrTypeID) {
            if ($scope.listAttriToAttriResult.length > 0) {
                var relationobj = $.grep($scope.listAttriToAttriResult, function (rel) {
                    return $.inArray(attrID, rel.AttributeRelationID.split(',')) != -1;
                });
                var ID = attrID.split("_");
                if (relationobj != undefined) {
                    if (relationobj.length > 0) {
                        for (var i = 0; i <= relationobj.length - 1; i++) {
                            Attributetypename = '';
                            if (relationobj[i].AttributeTypeID == 3) {
                                Attributetypename = 'NormalDropDown_' + relationobj[i].AttributeID;
                            } else if (relationobj[i].AttributeTypeID == 4) {
                                Attributetypename = 'NormalMultiDropDown_' + relationobj[i].AttributeID;
                            } else if (relationobj[i].AttributeTypeID == 6) {
                                Attributetypename = 'dropdown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                            } else if (relationobj[i].AttributeTypeID == 7) {
                                Attributetypename = 'Tree_' + relationobj[i].AttributeID;
                            } else if (relationobj[i].AttributeTypeID == 12) {
                                Attributetypename = 'multiselectdropdown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                            }
                            if (relationobj[i].AttributeTypeID == 3) {
                                if ($scope.fields[Attributetypename] != "" && $scope.fields[Attributetypename] != "-" && ($scope[Attributetypename] == undefined || $scope[Attributetypename] == "")) {
                                    if ($scope.normaltreeSources[Attributetypename] != undefined && $scope.normaltreeSources[Attributetypename] != "") {
                                        var tempvar = ($.grep($scope.normaltreeSources[Attributetypename], function (e) {
                                            return e.Caption == $scope.fields[Attributetypename]
                                        }))[0];
                                        $scope[Attributetypename] = tempvar.Id;
                                    }
                                }
                                if ($scope[Attributetypename] != undefined && $scope[Attributetypename] != "") if (relationobj[i].AttributeOptionID == $scope[Attributetypename]) {
                                    return true;
                                }
                            } else if (relationobj[i].AttributeTypeID == 4) {
                                if ($scope['IsChanged_' + relationobj[i].AttributeID] == true || $scope['IsChanged_' + relationobj[i].AttributeID] == undefined) {
                                    if ($scope.normaltreeSources[Attributetypename] != undefined && $scope.normaltreeSources[Attributetypename] != "") {
                                        $scope['IsChanged_' + relationobj[i].AttributeID] = false;
                                        var captions = $scope.fields[Attributetypename].split(',')
                                        $scope[Attributetypename] = [];
                                        var tempvar = [];
                                        for (var x = 0; x <= captions.length; x++) {
                                            if (captions[x] != undefined) {
                                                tempvar.push(($.grep($scope.normaltreeSources[Attributetypename], function (e) {
                                                    return e.Caption.trim() == captions[x].trim()
                                                }))[0]);
                                                $scope[Attributetypename].push(tempvar[x].Id);
                                            }
                                        }
                                    }
                                }
                                if ($scope[Attributetypename] != undefined && $scope[Attributetypename] != "") if ($.inArray(relationobj[i].AttributeOptionID, $scope[Attributetypename]) != -1) {
                                    return true;
                                }
                            } else if (relationobj[i].AttributeTypeID == 6) {
                                if ($scope['IsChanged_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel] == true || $scope['IsChanged_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel] == undefined) {
                                    if ($scope.treeSources["dropdown_" + relationobj[i].AttributeID] != undefined && $scope.treeSources["dropdown_" + relationobj[i].AttributeID] != "") {
                                        if (relationobj[i].AttributeLevel == 1) {
                                            $scope[Attributetypename] = ($.grep($scope.treeSources["dropdown_" + relationobj[i].AttributeID].Children, function (e) {
                                                return e.Caption.toString() == $scope.treeTexts['dropdown_text_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel].toString().trim()
                                            }))[0];
                                            $scope['IsChanged_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel] = false;
                                        } else {
                                            var node = ($.grep($scope.treeSources["dropdown_" + relationobj[i].AttributeID].Children, function (e) {
                                                return e.Caption.toString() == $scope.treeTexts['dropdown_text_' + relationobj[i].AttributeID + "_" + (relationobj[i].AttributeLevel - 1)].toString().trim()
                                            }))[0];
                                            if (node != undefined) {
                                                $scope[Attributetypename] = ($.grep(node.Children, function (e) {
                                                    return e.Caption.toString() == $scope.treeTexts['dropdown_text_' + relationobj[i].AttributeID + "_" + (relationobj[i].AttributeLevel)].toString().trim()
                                                }))[0];
                                                $scope['IsChanged_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel] = false;
                                            }
                                        }
                                    }
                                }
                                if ($scope[Attributetypename] != undefined && $scope[Attributetypename] != "") if ($scope[Attributetypename].id == relationobj[i].AttributeOptionID && $(".AttrID_" + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel).hasClass('ng-hide') == false) {
                                    return true;
                                }
                            } else if (relationobj[i].AttributeTypeID == 12) {
                                if ($scope['IsChanged_' + relationobj[i].AttributeID] == true || $scope['IsChanged_' + relationobj[i].AttributeID] == undefined) {
                                    if ($scope.treeSources["multiselectdropdown_" + relationobj[i].AttributeID] != undefined && $scope.treeSources["multiselectdropdown_" + relationobj[i].AttributeID] != "") {
                                        $scope[Attributetypename] = ($.grep($scope.treeSources["multiselectdropdown_" + relationobj[i].AttributeID].Children, function (e) {
                                            return e.Caption.toString() == $scope.treeTexts['multiselectdropdown_text_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel].toString().trim()
                                        }))[0];
                                    }
                                }
                                if ($scope[Attributetypename] != undefined && $scope[Attributetypename] != "") if ($scope[Attributetypename].id == relationobj[i].AttributeOptionID && $(".AttrID_" + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel).hasClass('ng-hide') == false) {
                                    return true;
                                }
                            } else if (relationobj[i].AttributeTypeID == 7) {
                                if ($scope[Attributetypename] != undefined && $scope[Attributetypename] != "") if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
                                    return true;
                                }
                            } else {
                                if ($scope[Attributetypename] != undefined && $scope[Attributetypename] != "") if ($scope.fields[Attributetypename] == relationobj[i].AttributeOptionID) {
                                    return true;
                                }
                            }
                        }
                        return false;
                    } else return true;
                } else return true;
            } else return true;
        };
        $scope.saveDropdownTree = function (attrID, attributetypeid, entityTypeid, optionarray) {
            if (attributetypeid == 6) {
                var updateentityattrib = {};
                updateentityattrib.EntityID = $scope.EntityID;
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                TaskEntityMetadataTemplateService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.Response == null) {
                        NotifyError($tanslate.instant('LanguageContents.Res_4351.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_5799.Caption'));
                        for (var i = 0; i < optionarray.length; i++) {
                            if (optionarray[i] != 0) {
                                var level = i + 1;
                            }
                        }
                        for (var j = 0; j < optionarray.length; j++) {
                            if (optionarray[j] != 0) {
                                $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                            }
                        }
                    }
                    $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 1 || attributetypeid == 2) {
                var updateentityattrib = {};
                updateentityattrib.EntityID = $scope.EntityID;
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = [optionarray];
                updateentityattrib.AttributetypeID = attributetypeid;
                if (attrID == 68) {
                    $(window).trigger("UpdateEntityName", optionarray);
                    $("ul li a[data-entityid='" + $stateParams.ID + "'] span[data-name='text']").text([optionarray]);
                    $("ul li a[data-entityid='" + $stateParams.ID + "']").attr('data-entityname', optionarray);
                    var Splitarr = [];
                }
                TaskEntityMetadataTemplateService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult == null || updateentityattribresult == false) NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_5799.Caption'));
                        $scope.fields['SingleLineTextValue_' + attrID] = optionarray;
                        if (attrID == 68) {
                            $('#breadcrumlink').text(optionarray);
                        }
                    }
                    $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 3 || attributetypeid == 4 || attributetypeid == 17) {
                var updateentityattrib = {};
                updateentityattrib.EntityID = $scope.EntityID;
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                TaskEntityMetadataTemplateService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult == null) NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                    else NotifySuccess($translate.instant('LanguageContents.Res_5799.Caption'));
                    $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 19) {
                $scope.newamount = "";
                $scope.newcurrencytypename = "";
                $scope.newamount = optionarray[0];
                $scope.newcurrencytypename = optionarray[2];
                var updateentityattrib = {};
                updateentityattrib.EntityID = $scope.EntityID;
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                TaskEntityMetadataTemplateService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                    else NotifySuccess($translate.instant('LanguageContents.Res_5799.Caption'));
                    $scope.fields["SingleLineTextValue_" + attrID] = (parseFloat($scope.newamount)).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    $scope.fields["currtypename_" + attrID] = $scope.newcurrencytypename;
                    $scope.fields["NormalDropDown_" + attrID] = optionarray[1];
                    $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 12) {
                var updateentityattrib = {};
                updateentityattrib.EntityID = $scope.EntityID;
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = optionarray;
                updateentityattrib.AttributetypeID = attributetypeid;
                TaskEntityMetadataTemplateService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                    else {
                        for (var j = 0; j < optionarray.length; j++) {
                            if (optionarray[j] != 0) {
                                $scope.ShowHideAttributeToAttributeRelations(attrID, (j) + 1, optionarray.length, attributetypeid, optionarray[j]);
                            }
                        }
                        NotifySuccess($translate.instant('LanguageContents.Res_5799.Caption'));
                    }
                    $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 8) {
                if (!isNaN(optionarray.toString())) {
                    var updateentityattrib = {};
                    updateentityattrib.EntityID = $scope.EntityID;
                    updateentityattrib.AttributeID = attrID;
                    updateentityattrib.Level = 0;
                    updateentityattrib.NewValue = optionarray == "" ? ["0"] : [optionarray];
                    updateentityattrib.AttributetypeID = attributetypeid;
                    TaskEntityMetadataTemplateService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                        if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                        else {
                            $scope.fields['SingleLineTextValue_' + attrID] = optionarray == "" ? 0 : parseInt(optionarray).formatMoney(0, ' ', ' ');
                            NotifySuccess($translate.instant('LanguageContents.Res_5799.Caption'));
                        }
                        $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                    });
                    $scope.treeSelection = [];
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                }
            } else if (attributetypeid == 5) {
                var sdate = "";
                if ($scope.fields['DateTime_Dir_' + attrID].toString() != "" && $scope.fields['DateTime_Dir_' + attrID] != null && $scope.fields['DateTime_Dir_' + attrID].toString() != "-") {
                    sdate = new Date.create($scope.fields['DateTime_Dir_' + attrID].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                    if (sdate == 'NaN-NaN-NaN') {
                        sdate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['DateTime_Dir_' + periodid].toString(), GlobalUserDateFormat))
                    }
                }
                var updateentityattrib = {};
                updateentityattrib.EntityID = $scope.EntityID;
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = [sdate];
                updateentityattrib.AttributetypeID = attributetypeid;
                TaskEntityMetadataTemplateService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_5799.Caption'));
                        if (sdate == "") $scope.fields["DateTime_" + attrID] = "-";
                        else {
                            $scope.fields["DateTime_" + attrID] = dateFormat(sdate, $scope.format);
                        }
                    }
                    $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                });
                $scope.treeSelection = [];
            } else if (attributetypeid == 9) {
                var updateentityattrib = {};
                updateentityattrib.EntityID = $scope.EntityID;
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.Level = 0;
                updateentityattrib.NewValue = ($('#CheckBoxSelection_' + attrID)[0].checked) ? [1] : [0];
                updateentityattrib.AttributetypeID = attributetypeid;
                TaskEntityMetadataTemplateService.SaveDetailBlockForLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                    else {
                        NotifySuccess($translate.instant('LanguageContents.Res_5799.Caption'));;
                        $scope.fields['CheckBoxSelection_' + attrID] = $('#CheckBoxSelection_' + attrID)[0].checked;
                    }
                    $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                });
            }
            $scope.treeSelection = [];
        };

        function UpdateTreeScope(ColumnName, Value) {
            for (var k = 0; k < $scope.ListViewDetails[0].data.Response.ColumnDefs.length; k++) {
                if (ColumnName == $scope.ListViewDetails[0].data.Response.ColumnDefs[k].Field) {
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            var EntityId = $scope.ListViewDetails[i].data.Response.Data[j]["Id"];
                            if (EntityId == parseInt($stateParams.ID, 10)) {
                                $scope.ListViewDetails[i].data.Response.Data[j][ColumnName] = Value;
                                return false;
                            }
                        }
                    }
                }
            }
        }
        $scope.Clearxeditabletreedropdown = function Clearxeditabletreedropdown(attrid, levelcnt, currentlevel) {
            currentlevel = currentlevel + 1;
            for (var i = currentlevel; i <= levelcnt; i++) {
                $scope['dropdown_' + attrid + '_' + i] = "";
            }
        }

        $scope.drpdirectiveSource = {};

        $scope.IsSourceformed = function (attrID, levelcnt, attributeLevel, attrType) {
            if (levelcnt > 0) {
                var dropdown_text = '', subid = 0;
                var currntlevel = attributeLevel + 1;
                if (attributeLevel == 1) {
                    var dropdown_text = 'dropdown_text_' + attrID + '_' + attributeLevel;
                    var idtomatch = $scope['dropdown_' + attrID + '_' + attributeLevel] != undefined ? ($scope['dropdown_' + attrID + '_' + attributeLevel].id != undefined ? $scope['dropdown_' + attrID + '_' + attributeLevel].id : $scope['dropdown_' + attrID + '_' + attributeLevel]) : 0;
                    if (idtomatch != 0)
                        $scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel] = $.grep($scope.treeSources["dropdown_" + attrID].Children,
                            function (e) {
                                return e.id == idtomatch;
                            }
                        )[0];
                }
                for (var j = currntlevel; j <= levelcnt; j++) {
                    dropdown_text = 'dropdown_text_' + attrID + '_' + j;
                    subid = $scope['dropdown_' + attrID + '_' + (j)] != undefined ? ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined ? $scope['dropdown_' + attrID + '_' + (j - 1)].id : $scope['dropdown_' + attrID + '_' + (j)]) : 0;
                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = {};
                    if (subid != 0 && subid > 0) {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + (j)] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children != undefined && $scope['dropdown_' + attrID + '_' + (j - 1)] != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = ($.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children, function (e) { return e.id == subid; }))[0];
                        }
                    }
                    else {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + j] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)] != undefined) {
                                var res = [];
                                if ($scope['dropdown_' + attrID + '_' + (j)] > 0) {
                                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)]['Children'], function (e) { return e.id == $scope['dropdown_' + attrID + '_' + (j)] })[0];
                                }
                                else {
                                    $scope['dropdown_' + attrID + '_' + (j)] = 0;
                                }
                            }
                        }
                    }
                }
            }
        }

        $scope.BindChildDropdownSource = function (attrID, levelcnt, attributeLevel, attrType) {

            if (levelcnt > 0) {
                var currntlevel = attributeLevel + 1;
                for (var j = currntlevel; j <= levelcnt; j++) {
                    $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].length);
                    if (attrType == 6) {
                        $scope["dropdown_" + attrID + "_" + j] = 0;
                        $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].length);
                    } else if (attrType == 12) {
                        if (j == levelcnt) $scope["multiselectdropdown_" + attrID + "_" + j] = [];
                        else $scope["multiselectdropdown_" + attrID + "_" + j] = "";
                    }
                }
                if (attrType == 6) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {
                        var children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["dropdown_" + attrID + "_" + attributeLevel]) })[0].Children;

                        if (children != undefined) {
                            var subleveloptions = [];
                            $.each(children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                } else if (attrType == 12) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {

                        var sublevel_res = [];
                        if ($scope["multiselectdropdown_" + attrID + "_" + attributeLevel] != undefined && $scope["multiselectdropdown_" + attrID + "_" + attributeLevel] > 0)
                            sublevel_res = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["multiselectdropdown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (sublevel_res != undefined) {
                            var subleveloptions = [];
                            $.each(sublevel_res, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                }
            }
        }

        $scope.Clearxeditablemultiselecttreedropdown = function Clearxeditablemultiselecttreedropdown(attrid, levelcnt, currentlevel) {
            currentlevel = currentlevel + 1;
            for (var i = currentlevel; i <= levelcnt; i++) {
                $scope['multiselectdropdown_' + attrid + '_' + i] = "";
            }
        }
        $scope.ShowHideAttributeOnRelations = function (attrVal) {
            var optionValue = attrVal;
            var attributesToHide = [];
            attributesToHide.push($.grep($scope.listAttriToAttriResult, function (e) {
                return e.AttributeOptionID == optionValue;
            })[0]);
            if (attributesToHide[0] != undefined) {
                for (var i = 0; i < attributesToHide.length; i++) {
                    var attrRelIDs = attributesToHide[i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j]] = false;
                        }
                    }
                }
            } else {
                attributesToHide = [];
                attributesToHide.push($.grep($scope.listAttriToAttriResult, function (e) {
                    return e.AttributeID == attrID;
                })[0]);
                if (attributesToHide[0] != undefined) {
                    for (var i = 0; i < attributesToHide.length; i++) {
                        var attrRelIDs = attributesToHide[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j]] = true;
                            }
                        }
                    }
                }
            }
        }
        $scope.AddDefaultEndDate = function (stratdateval, enddateval) {
            if ($scope.fields[stratdateval] == null) {
                $scope.fields[enddateval] = null;
            } else {
                var objsetenddate = new Date.create($scope.fields[stratdateval]);
                $scope.fields[enddateval] = (7).daysAfter(objsetenddate);
            }
        };

        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToHide = [];
            if (attrLevel > 0) {
                attributesToHide.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            } else {
                attributesToHide.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }
            if (attributesToHide[0] != undefined) {
                for (var i = 0; i < attributesToHide[0].length; i++) {
                    var attrRelIDs = attributesToHide[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType, attrVal) {
            var recursiveAttrID = '';
            var optionValue = '';
            var attributesToHide = [];
            var hideAttributeOtherThanSelected = [];
            if (attrID == SystemDefiendAttributes.FiscalYear) {
                $scope.changeCostCenterSource();
            }
            if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                return false;
            }
            RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);
            if (attrType == 3) {
                optionValue = attrVal;
                attributesToHide.push($.grep($scope.listAttriToAttriResult, function (e) {
                    return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                })[0]);
            } else if (attrType == 4) {
                optionValue = attrVal;
                attributesToHide = ($.grep($scope.listAttriToAttriResult, function (e) {
                    return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                }));
            } else if (attrType == 6) {
                var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                if (attrval != null) {
                    attributesToHide = [];
                    attributesToHide.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                    })[0]);
                }
            } else if (attrType == 7) {
                optionValue = attrVal;
                attributesToHide = ($.grep($scope.listAttriToAttriResult, function (e) {
                    return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                }));
            } else if (attrType == 12) {
                var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                if (attrval != null) {
                    attributesToHide = [];
                    attributesToHide.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                    })[0]);
                }
            }
            if (attributesToHide[0] != undefined) {
                for (var i = 0; i < attributesToHide.length; i++) {
                    var attrRelIDs = attributesToHide[i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                            } else {
                                if ($scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] == false) $scope.fields['SingleLineTextValue_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "-";
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                            }
                        }
                    }
                }
            }
        }
        $scope.savePeriodVal = function (attrID, attrTypeID, entityid, periodid) {
            var one_day = 1000 * 60 * 60 * 24;
            if (periodid == 0) {
                var newperiod = {};
                var sdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var edate = new Date.create($scope.fields['PeriodEndDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var maxPeriodDate = new Date.create(Math.max.apply(null, tempperioddates));
                var diffval = (parseInt(edate.getTime() - sdate.getTime()));
                if (diffval < 0) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_1956.Caption'));
                    return false;
                }
                var tempperioddates = [];
                for (var i = 0; i < perioddates.length; i++) {
                    tempperioddates.push(perioddates[i].value)
                }
                var one_day = 1000 * 60 * 60 * 24;
                var maxPeriodDate = new Date.create(Math.max.apply(null, tempperioddates));
                var diffval = (parseInt(sdate.getTime() - maxPeriodDate.getTime()));
                if (diffval < 1) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_1957.Caption'));
                    return false;
                }
                newperiod.EntityID = entityid;
                newperiod.StartDate = dateFormat($scope.fields['PeriodStartDate_Dir_' + periodid]);
                newperiod.EndDate = dateFormat($scope.fields['PeriodEndDate_Dir_' + periodid]);
                newperiod.Description = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                newperiod.SortOrder = 0;
                TaskEntityMetadataTemplateService.InsertEntityPeriod(newperiod).then(function (resultperiod) {
                    if (resultperiod.Response == null) NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                    else {
                        var newid = resultperiod.Response;
                        if ($scope.IsStartDateEmpty == true) {
                            $scope.IsStartDateEmpty = false;
                            $("[data-tempid=startendID]").remove();
                        }
                        perioddates.push({
                            ID: newid,
                            value: edate
                        });
                        $scope.dyn_Cont = '';
                        $scope.fields["PeriodStartDate_" + newid] = ConvertDateFromStringToString(ConvertDateToString(sdate));
                        $scope.fields["PeriodEndDate_" + newid] = ConvertDateFromStringToString(ConvertDateToString(edate))
                        $scope.fields["PeriodStartDate_Dir_" + newid] = dateFormat(new Date.create(sdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('dd-MM-yyyy'), $scope.format);
                        $scope.fields["PeriodEndDate_Dir_" + newid] = dateFormat(new Date.create(edate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('dd-MM-yyyy'), $scope.format);
                        $scope.fields["PeriodDateDesc_Dir_" + newid] = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                        if ($scope.fields['PeriodDateDesc_Dir_' + periodid] == "" || $scope.fields['PeriodDateDesc_Dir_' + periodid] == undefined) {
                            $scope.fields["PeriodDateDesc_" + newid] = "-";
                        } else {
                            $scope.fields["PeriodDateDesc_" + newid] = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                        }
                        $('#fsedateid').css("visibility", "hidden");
                        $scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + newid + '">';
                        $scope.dyn_Cont += '<div class="inputHolder nomargin span11">';
                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Start / End date </label>';
                        $scope.dyn_Cont += '<div class="controls">';
                        $scope.dyn_Cont += '<a  xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodstartdate_id=\"PeriodStartDate_' + newid + '\" data-ng-model=\"PeriodStartDate_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\" data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + newid + '}}</a>';
                        $scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodenddate_id=\"PeriodEndDate_' + newid + '\" data-ng-model=\"PeriodEndDate_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\"  data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + newid + '}}</a>';
                        $scope.dyn_Cont += '</div></div>';
                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Comment Start / End date </label>';
                        $scope.dyn_Cont += '<div class="controls">';
                        $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + newid + '" entityid="' + entityid + '" attributeTypeID="' + attrTypeID + '" attributeid="' + attrID + '" data-periodenddate_id=\"PeriodDateDesc_' + newid + '\" data-ng-model=\"PeriodDateDesc_' + newid + '\"  my-qtip2 qtip-content=\"Start/End Date\"  data-type=\"perdiodate' + newid + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + newid + '}}</a>';
                        $scope.dyn_Cont += '</div></div></div>';
                        if (perioddates.length != 1) {
                            $scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + newid + ')"><i class="icon-remove"></i></a></div></div>';
                        }
                        var divcompile = $compile($scope.dyn_Cont)($scope);
                        $(".dynamicdetail div[data-addperiodid]").append(divcompile);
                        $scope.fields["PeriodStartDate_Dir_0"] = "";
                        $scope.fields["PeriodEndDate_Dir_0"] = "";
                        $scope.fields["PeriodDateDesc_Dir_0"] = "";
                        NotifySuccess($translate.instant('LanguageContents.Res_5799.Caption'));
                        TaskEntityMetadataTemplateService.GetEntitiPeriodByIdForGantt(entityid).then(function (getGanttperiod) {
                            if (getGanttperiod.Response != null) {
                                for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                                    for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                        if ($scope.ListViewDetails[i].data.Response.Data[j]["Period"] == undefined) {
                                            return false;
                                        } else {
                                            if (entityid == $scope.ListViewDetails[i].data.Response.Data[j]["Id"]) {
                                                $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = getGanttperiod;
                                                return false;
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }
                    $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                });
            } else {
                var updateperiod = {};
                var temparryStartDate = [];
                $('[data-periodstartdate_id^=PeriodStartDate_]').each(function () {
                    if (parseInt(periodid) < parseInt(this.attributes['data-primaryid'].textContent)) {
                        var sdate;
                        if (this.text != "[ Add Start / End date ]") {
                            sdate = new Date.create(this.text.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                            temparryStartDate.push(sdate);
                        }
                    }
                });
                var temparryEndate = [];
                $('[data-periodenddate_id^=PeriodEndDate_]').each(function () {
                    if (parseInt(periodid) > parseInt(this.attributes['data-primaryid'].textContent)) {
                        var edate = new Date.create(this.text.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                        temparryEndate.push(edate);
                    }
                });
                var sdate = new Date.create($scope.fields['PeriodStartDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                if (sdate == 'NaN-NaN-NaN') {
                    sdate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['PeriodStartDate_Dir_' + periodid].toString(), GlobalUserDateFormat))
                }
                var edate = new Date.create($scope.fields['PeriodEndDate_Dir_' + periodid].toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd');
                if (edate == 'NaN-NaN-NaN') {
                    edate = ConvertDateToString(ConvertStringToDateByFormat($scope.fields['PeriodEndDate_Dir_' + periodid].toString(), GlobalUserDateFormat))
                }
                var diffval = ((parseInt(new Date.create(edate.toString('dd/MM/yyyy')).getTime()) - parseInt((new Date.create(sdate.toString('dd/MM/yyyy')).getTime()))));
                if (diffval < 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1956.Caption'));
                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    return false;
                }
                var maxPeriodEndDate = new Date.create(Math.max.apply(null, temparryEndate));
                var Convertsdate = new Date.create(sdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var diffvalend = (parseInt(Convertsdate.getTime() - maxPeriodEndDate.getTime()));
                if (parseInt(diffvalend) < 1) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    bootbox.alert($translate.instant('LanguageContents.Res_1992.Caption'));
                    return false;
                }
                var minPeroidStartDate = new Date.create(Math.min.apply(null, temparryStartDate));
                var Convertedate = new Date.create(edate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
                var diffvalstart = (parseInt(Convertedate.getTime() - minPeroidStartDate.getTime()));
                if (parseInt(diffvalstart) > 1) {
                    $scope.fields["PeriodStartDate_Dir_0"] = "";
                    $scope.fields["PeriodEndDate_Dir_0"] = "";
                    $scope.fields["PeriodDateDesc_Dir_0"] = "";
                    $scope.fields["PeriodStartDate_Dir_" + periodid] = $scope.fields["PeriodStartDate_" + periodid];
                    $scope.fields["PeriodEndDate_Dir_" + periodid] = $scope.fields["PeriodEndDate_" + periodid];
                    bootbox.alert($translate.instant('LanguageContents.Res_1991.Caption'));
                    return false;
                }
                updateperiod.ID = periodid;
                updateperiod.EntityID = entityid;
                updateperiod.StartDate = sdate;
                updateperiod.EndDate = edate;
                updateperiod.SortOrder = 0;
                updateperiod.Description = $scope.fields['PeriodDateDesc_Dir_' + periodid];
                TaskEntityMetadataTemplateService.PostEntityPeriod(updateperiod).then(function (resultperiod) {
                    if (resultperiod.Response == null) {
                        NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                    } else {
                        $scope.fields["PeriodStartDate_" + periodid] = ConvertDateFromStringToString(sdate);
                        $scope.fields["PeriodEndDate_" + periodid] = ConvertDateFromStringToString(edate);
                        $scope.fields["PeriodDateDesc_" + periodid] = $scope.fields['PeriodDateDesc_Dir_' + periodid] == "" ? "-" : $scope.fields['PeriodDateDesc_Dir_' + periodid];
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        TaskEntityMetadataTemplateService.GetEntitiPeriodByIdForGantt(entityid).then(function (getGanttperiod) {
                            if (resultperiod.StatusCode == 200) {
                                for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                                    for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                        if ($scope.ListViewDetails[i].data.Response.Data[j]["Period"] == undefined) {
                                            return false;
                                        } else {
                                            if (entityid == $scope.ListViewDetails[i].data.Response.Data[j]["Id"]) {
                                                $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = getGanttperiod;
                                                return false;
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }
                    $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
                });
            }
        }
        $scope.deletePeriodDate = function (periodid) {
            TaskEntityMetadataTemplateService.DeleteEntityPeriod(periodid).then(function (deletePerByIdRes) {
                if (deletePerByIdRes.Response != null || deletePerByIdRes.Response == true) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                    $(".dynamicdetail div[data-dynPeriodID = " + periodid + " ]").html('');
                    perioddates = $.grep(perioddates, function (val) {
                        return val.ID != periodid;
                    });
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4284.Caption'));
                }
                $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
            });
        }

        function GetTreeCheckedNodes(treeobj, attrID) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    $scope.fields["Tree_" + attrID].push(node.id);
                }
                if (node.Children.length > 0) GetTreeCheckedNodes(node.Children, attrID);
            }
        }
        $scope.savetreeDetail = function (attrID, attributetypeid, entityTypeid) {
            if (attributetypeid == 7) {
                $scope.treeNodeSelectedHolder = [];
                GetTreeObjecttoSave(attrID);
                var updateentityattrib = {};
                updateentityattrib.EntityID = entityTypeid;
                updateentityattrib.AttributeID = attrID;
                updateentityattrib.NewValue = $scope.treeNodeSelectedHolder;
                updateentityattrib.newTree = $scope.staticTreesrcdirec["Attr_" + attrID];
                updateentityattrib.oldTree = $scope.treesrcdirec["Attr_" + attrID];
                updateentityattrib.AttributetypeID = attributetypeid;
                TaskEntityMetadataTemplateService.SaveDetailBlockForTreeLevels(updateentityattrib).then(function (updateentityattribresult) {
                    if (updateentityattribresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                    } else {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        TaskEntityMetadataTemplateService.GetAttributeTreeNodeByEntityID(attrID, entityTypeid).then(function (GetTree) {
                            $scope.treesrcdirec["Attr_" + attrID] = JSON.parse(GetTree.Response).Children;
                            if ($scope.treeNodeSelectedHolder.length > 0) $scope.TreeEmptyAttributeObj["Attr_" + attrID] = true;
                            else $scope.TreeEmptyAttributeObj["Attr_" + attrID] = false;
                            $timeout(function () {
                                $scope.TimerForLatestFeed();
                            }, 3000);
                        });
                        $scope.fields["Tree_" + attrID].splice(0, $scope.fields["Tree_" + attrID].length);
                        GetTreeCheckedNodes($scope.treeNodeSelectedHolder, attrID);
                        $scope.ShowHideAttributeToAttributeRelations(attrID, 0, 0, attributetypeid, $scope.fields["Tree_" + attrID]);
                    }
                });
            }
            $scope.PageFullData.versionUpdatedDate = dateFormat(Date.now(), $scope.GetDefaultSettings.DateFormat);
        }

        function GetTreeObjecttoSave(attributeid) {
            GenerateTreeStructure($scope.staticTreesrcdirec["Attr_" + attributeid]);
        }

        function GenerateTreeStructure(treeobj) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == node.AttributeId && e.id == node.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(node);
                    }
                    treeformflag = false;
                    if (ischildSelected(node.Children)) {
                        GenerateTreeStructure(node.Children);
                    } else {
                        GenerateTreeStructure(node.Children);
                    }
                } else GenerateTreeStructure(node.Children);
            }
        }

        function ischildSelected(children) {
            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    treeformflag = true;
                    return treeformflag
                }
            }
            return treeformflag;
        }
        function IsNotEmptyTree(treeObj) {

            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                }
                else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }
        $scope.my_tree_handler = function (branch, parentArr) {
            var _ref;
            $scope.output = "You selected: " + branch.Caption;
            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }
            if (branch.ischecked == true) {
                var remainRecord = [];
                if ($scope.treeNodeSelectedHolder != undefined) {
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == branch.AttributeId && e.id == branch.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(branch);
                    }
                    for (var i = 0, parent; parent = parentArr[i++];) {
                        var remainRecord = [];
                        remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                            return e.AttributeId == parent.AttributeId && e.id == parent.id;
                        });
                        if (remainRecord.length == 0) {
                            $scope.treeNodeSelectedHolder.push(parent);
                        }
                    }
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                    if (branch.Children.length > 0) {
                        RemoveRecursiveChildTreenode(branch.Children);
                    }
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
        };

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == child.AttributeId && e.id == child.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }
            }
        }
        $scope.changeduedate_changed = function (duedate, ID) {
            if (duedate != null) {
                var test = isValidDate(duedate.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(duedate, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert($translate.instant('LanguageContents.Res_5747.Caption'));
                            $scope.fields["DatePart_" + ID] = "";
                        }
                    }
                } else {
                    $scope.fields["DatePart_" + ID] = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_5748.Caption'));
                }
            }
        }
        $scope.changeperioddate_changed = function (date, datetype) {
            if (date != null) {
                var test = isValidDate(date.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(date, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert($translate.instant('LanguageContents.Res_5747.Caption'));
                            if (datetype == "StartDate") $scope.items[0].startDate = "";
                            else $scope.item[0].endDate = "";
                        }
                    }
                } else {
                    if (datetype == "StartDate") $scope.items[0].startDate = "";
                    else $scope.item[0].endDate = "";
                    bootbox.alert($translate.instant('LanguageContents.Res_5748.Caption'));
                }
            }
        }

        function isValidDate(dateval, dateformat) {
            var formatlen;
            var defaultdateVal = [];
            defaultdateVal = dateval.length;
            formatlen = dateformat.length;
            if (formatlen == defaultdateVal || defaultdateVal > formatlen) return true;
            else return false;
        };
    }
    app.controller("mui.task.EntityMetadataTemplateCtrl", ['$window', '$location', '$timeout', '$scope', '$cookies', '$resource', '$compile', '$stateParams', '$translate', 'TaskEntityMetadataTemplateService', '$sce', '$modal', muitaskEntityMetadataTemplateCtrl]);
})(angular, app);