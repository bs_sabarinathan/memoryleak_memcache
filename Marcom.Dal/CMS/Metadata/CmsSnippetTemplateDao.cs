﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.CMS.Model
{
    public partial class CmsSnippetTemplateDao : BaseDao, ICloneable
    {
        #region Member variables

        protected int _id;
        protected string _snippetcontent;
        protected bool _active;
        protected string _thumbnailguid;
        protected string _createdon;
        protected string _defaultfirstcontent;
        protected string _defaultlastcontent;

        #endregion

        #region Constructors

        public CmsSnippetTemplateDao()
        {

        }

        public CmsSnippetTemplateDao(int pid)
        {
            this._id = pid;
        }

        public CmsSnippetTemplateDao(int pid, string psnippetcontent, bool pactive, string pthumbnailguid, string pcreatedon, string pdefaultfirstcontent, string pdefaultlastcontent)
        {
            this._id = pid;
            this._snippetcontent = psnippetcontent;
            this._active = pactive;
            this._thumbnailguid = pthumbnailguid;
            this._createdon = pcreatedon;
            this._defaultfirstcontent = pdefaultfirstcontent;
            this._defaultlastcontent = pdefaultlastcontent;
        }

        #endregion

        #region Public properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public virtual string SnippetContent
        {
            get { return _snippetcontent; }
            set { _snippetcontent = value; }
        }


        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public virtual string ThumbnailGuid
        {
            get { return _thumbnailguid; }
            set { _thumbnailguid = value; }
        }

        public virtual string CreatedOn
        {
            get { return _createdon; }
            set { _createdon = value; }
        }
        public virtual string DefaultFirstContent
        {
            get { return _defaultfirstcontent; }
            set { _defaultfirstcontent = value; }
        }
        public virtual string DefaultLastContent
        {
            get { return _defaultlastcontent; }
            set { _defaultlastcontent = value; }
        }
        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string CmsSnippetTemplateDao = "CmsSnippetTemplateDao";
            public const string ID = "ID";
            public const string SnippetContent = "SnippetContent";
            public const string Active = "Active";
            public const string ThumbnailGuid = "ThumbnailGuid";
            public const string CreatedOn = "CreatedOn";
            public const string DefaultFirstContent = "DefaultFirstContent";
            public const string DefaultLastContent = "DefaultLastContent";

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string CmsSnippetTemplateDao = "CmsSnippetTemplateDao";
            public const string ID = "ID";
            public const string SnippetContent = "SnippetContent";
            public const string Active = "Active";
            public const string ThumbnailGuid = "ThumbnailGuid";
            public const string CreatedOn = "CreatedOn";
            public const string DefaultFirstContent = "DefaultFirstContent";
            public const string DefaultLastContent = "DefaultLastContent";
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

    }
}
