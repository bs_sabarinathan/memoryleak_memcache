﻿using BrandSystems.Marcom.Core.Task.Interface;
using BrandSystems.Marcom.Dal;
using BrandSystems.Marcom.Dal.Task.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Task
{
    public partial class Entitytaskphases : IEntitytaskphases, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _TempID { get; set; }
        protected string _Name { get; set; }
        protected string _Description { get; set; }
        protected string _ColorCode { get; set; }
        protected int _EntityID { get; set; }
        protected int _SortOrder { get; set; }
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public Entitytaskphases() { }

        public Entitytaskphases(string Name, string TempID, string Description, string ColorCode, int EntityID, int SortOrder)
        {
            this._Name = Name;
            this._Description = Description;
            this._ColorCode = ColorCode;
            this._EntityID = EntityID;
            this._SortOrder = SortOrder;
            this._TempID = TempID;
        }

        public Entitytaskphases(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }
        public virtual string TempID
        {
            get { return _TempID; }
            set { _bIsChanged |= (_TempID != value); _TempID = value; }
        }
        public virtual string Name
        {
            get { return _Name; }
            set { _bIsChanged |= (_Name != value); _Name = value; }
        }
        public virtual string Description
        {
            get { return _Description; }
            set { _bIsChanged |= (_Description != value); _Description = value; }
        }

        public virtual string ColorCode
        {
            get { return _ColorCode; }
            set { _bIsChanged |= (_ColorCode != value); _ColorCode = value; }
        }

        public virtual int EntityID
        {
            get { return _EntityID; }
            set { _bIsChanged |= (_EntityID != value); _EntityID = value; }
        }

        public virtual int SortOrder
        {
            get { return _SortOrder; }
            set { _bIsChanged |= (_SortOrder != value); _SortOrder = value; }
        }

        public int Status { get; set; }

        public bool actionPermit { get; set; }

        public IList<EntitytaskstepsDao> StepsData { get; set; }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string TempID = "TempID";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string ColorCode = "ColorCode";
            public const string EntityID = "EntityID";
            public const string SortOrder = "SortOrder";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string TempID = "TempID";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string ColorCode = "ColorCode";
            public const string EntityID = "EntityID";
            public const string SortOrder = "SortOrder";
        }

        #endregion
    }
}
