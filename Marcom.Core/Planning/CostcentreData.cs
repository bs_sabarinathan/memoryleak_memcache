﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

    /// <summary>
    /// CostCenter object for table 'PM_CostCenter'.
    /// </summary>

    internal class CostCenterData : BaseEntity, ICostCentreData
    {
        #region Member Variables


        protected int _id;
        protected string _name;
        protected decimal? _assignedamount;
       protected IList<IAttributeData> _entityattributedata;
       protected IList<IEntityRoleUser> _entityMembers;
        

        #endregion

        #region Constructors
        public CostCenterData() { }

        public CostCenterData(int pId, string pName, decimal? pAssignedAmount)
        {
            this._id = pId;
            this._name = pName;
            this._assignedamount = pAssignedAmount;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
                _name = value;
            }

        }
       
        public decimal? AssignedAmount
        {
            get { return _assignedamount; }
            set { _assignedamount = value; }

        }

        public IList<IAttributeData> EntityAttributeData
        {
            get { return _entityattributedata; }
            set { _entityattributedata = value; }
        }
        public IList<IEntityRoleUser> EntityMembers
        {
            get { return _entityMembers; }
            set { _entityMembers = value; }
        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            CostCenter castObj = null;
            try
            {
                castObj = (CostCenter)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
