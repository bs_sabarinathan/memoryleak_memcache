using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IMailContent interface for table 'CM_MailContent'.
    /// </summary>
    public interface IMailContent
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Subject
        {
            get;
            set;

        }

        string Body
        {
            get;
            set;

        }

        string description
        {
            get;
            set;

        }
        int MailSubscriptionTypeID
        {
            get;
            set;

        }


        #endregion
    }
}
