﻿using System;
using System.Web;

/// <summary>
/// Summary description for PasswordSetting
/// </summary>
public class PasswordSetting
{
    public PasswordSetting()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    //password age , 80, 180, 360 days
    public int Duration { get; set; }

    //password minimum length
    public int MinLength { get; set; }

    //password maximum length
    public int MaxLength { get; set; }

    //password Numbers length
    public int NumsLength { get; set; }

    //password Upper letter length
    public int UpperLength { get; set; }

    //password Special character length
    public int SpecialLength { get; set; }

    //password valid special characters
    public string SpecialChars { get; set; }

    public int BarWidth { get; set; }

    //password valid special characters
    public int MultipleColors { get; set; }

    public String BarColor { get; set; }

    public int lockouttime { get; set; }

    public int attempts { get; set; }

    public int monthlyrepetition { get; set; }

    public int repetitiontime { get; set; }

    public int monthlyexpiration { get; set; }

    public bool expirevalidation { get; set; }

    public bool notifyuser { get; set; }

    public bool notifyadmin { get; set; }

    public string typeofpwdrule { get; set; }

    public int Max_attempts { get; set; }

   
}
