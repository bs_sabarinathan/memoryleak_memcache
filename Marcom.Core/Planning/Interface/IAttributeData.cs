﻿using BrandSystems.Marcom.Core.Metadata;
using System;
using System.Collections;
using System.Collections.Generic;
 
namespace BrandSystems.Marcom.Core.Planning.Interface
{
  public interface IAttributeData
    {
        int ID { get; set; }
        int TypeID { get; set; }
        dynamic Lable { get; set; }
        dynamic Caption { get; set; }
        dynamic Value { get; set; }
        string SpecialValue { get; set; }
        int Level { get; set; }
        bool IsSpecial { get; set; }
        bool IsInheritFromParent { get; set; }
        bool IsChooseFromParent { get; set; }
        bool IsReadOnly { get; set; }
        int AttributeRecordID { get; set; }
        bool IsLock { get; set; }
        int SortOrder { get; set; }
        string specialValue { get; set; }
        dynamic tree { get; set; }
        dynamic options { get; set; }
        int MinValue { get; set; }
        int MaxValue { get; set; }
        bool IsSearchable { get; set; }
        bool ShowAsColumn { get; set; }
        bool AttributeGroupTooltip { get; set; }
        IList<DropDownTreePricing> DropDownPricing { get; set; }
    }

    public interface ITreeDropDownLabel
    {
        int Level { get; set; }
        string Label { get; set; }
    }

    public interface ITreeDropDownCaption
    {
        int Level { get; set; }
        string Caption { get; set; }
    }

    public interface IValdiationWithAttributeRelationData
    {
        int AttributeID { get; set; }
        int EntityTypeID { get; set; }
        int RelationShipID { get; set; }
        string Name { get; set; }
        string ValueType { get; set; }
        string Value { get; set; }
        string ErrorMessage { get; set; }
        bool InheritFromParent { get; set; }
        bool ChooseFromParentOnly { get; set; }
        bool IsReadOnly { get; set; }
    }

}
