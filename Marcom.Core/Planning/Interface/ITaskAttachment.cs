﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IAttachments interface for table 'PM_Attachments'.
    /// </summary>
    public interface ITaskAttachment
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Entityid
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        int ActiveVersionNo
        {
            get;
            set;

        }

        int ActiveFileid
        {
            get;
            set;

        }

        int Typeid
        {
            get;
            set;

        }

        DateTime Createdon
        {
            get;
            set;

        }



        #endregion
    }
}
