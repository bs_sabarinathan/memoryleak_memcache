﻿(function (ng, app) {
    "use strict";

    function muiadminseacrhcriteriaCtrl($scope, $resource, $location, $translate, AdminService) {
        var availableattrs = [];
        var allAttributes = [];
        $scope.entitytype = 1;
        var AdminLogoSettings = 'ProductionCreation';
        var ListView = 'ProductionType';
        $("#sortableAttribute2").droppable({
            drop: function (event, ui) {
                var dropeedid = ui.helper[0].attributes["data-id"].value;
                var attrDelObj = $.grep(availableattrs, function (e) {
                    return e.Id == dropeedid;
                });
                availableattrs.splice($.inArray(attrDelObj[0], availableattrs), 1);
            }
        });
        $("#sortableAttribute1").droppable({
            drop: function (event, ui) {
                var dropeedid = ui.helper[0].attributes["data-id"].value;
                var attrAddObj = $.grep(allAttributes, function (e) {
                    return e.Id == dropeedid;
                })[0];
                availableattrs.push(attrAddObj);
            }
        });
        $(function () {
            $("#sortableAttribute1, #sortableAttribute2").sortable({
                connectWith: ".connectedAttributeSortable",
                appendTo: 'body',
                containment: 'window',
                scroll: false,
                helper: 'clone'
            }).disableSelection();
        });
        $scope.loadattributesfrSearchCriteria = function () {
            availableattrs = [];
            $('#sortableAttribute2').html('');
            $('#sortableAttribute1').html('');
            if ($scope.entitytype == 0) {
                return true;
            }
            AdminService.GetAttributeSearchCriteria($scope.entitytype).then(function (getattributesfromdb) {
                allAttributes = getattributesfromdb.Response;
                AdminService.GetSearchCriteriaAdminSettings(AdminLogoSettings, ListView, $scope.entitytype).then(function (getattribtuesResult) {
                    if (getattribtuesResult.Response != null) {
                        $scope.enableAutoExp = false;
                        if (getattribtuesResult.Response.m_Item2 == 1) {
                            $scope.enableAutoExp = true;
                        }
                        if (getattribtuesResult.Response.m_Item1 != undefined && getattribtuesResult.Response.m_Item1 != "") {
                            var getattribtues = JSON.parse(getattribtuesResult.Response.m_Item1);
                            if (getattribtues.ProductionType.Attributes != null) {
                                $scope.attributedatavalues = getattribtues.ProductionType.Attributes.Attribute;
                                var attribtueslength = getattribtues.ProductionType.Attributes.Attribute.length;
                                var Attrhtml2 = '';
                                if (attribtueslength != undefined) {
                                    for (var b = 0; b < attribtueslength; b++) {
                                        var attravailablility = $.grep(allAttributes, function (e) {
                                            return e.Id == parseInt(getattribtues.ProductionType.Attributes.Attribute[b].Id);
                                        });
                                        if (attravailablility.length >= 0)
                                            Attrhtml2 += '<li data-Id="' + getattribtues.ProductionType.Attributes.Attribute[b].Id + '" data-DisplayName="' + getattribtues.ProductionType.Attributes.Attribute[b].DisplayName + '" class="active DragableItem"><a href="JavaScript: void(0);">' + getattribtues.ProductionType.Attributes.Attribute[b].DisplayName + '</a></li>';
                                    }
                                    $('#sortableAttribute2').html(Attrhtml2);
                                } else {
                                    var attravailablility = $.grep(allAttributes, function (e) {
                                        return e.Id == parseInt(getattribtues.ProductionType.Attributes.Attribute.Id);
                                    });
                                    if (attravailablility.length > 0)
                                        Attrhtml2 += '<li data-Id="' + getattribtues.ProductionType.Attributes.Attribute.Id + '" data-DisplayName="' + getattribtues.ProductionType.Attributes.Attribute.DisplayName + '" class="active DragableItem"><a href="JavaScript: void(0);">' + getattribtues.ProductionType.Attributes.Attribute.DisplayName + '</a></li>';
                                    $('#sortableAttribute2').html(Attrhtml2);
                                }
                                var Attrhtml = '';
                                for (var i = 0; i < allAttributes.length; i++) {
                                    var AttrValues = $.grep($scope.attributedatavalues, function (e) {
                                        return e.Id == allAttributes[i].Id;
                                    });
                                    if (AttrValues.length == 0) {
                                        if ($scope.attributedatavalues.length == undefined) {
                                            if ($scope.attributedatavalues.Id == allAttributes[i].Id) { } else {
                                                Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>';
                                                availableattrs.push(allAttributes[i]);
                                            }
                                        } else {
                                            Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>';
                                            availableattrs.push(allAttributes[i]);
                                        }
                                    }
                                }
                                $('#sortableAttribute1').html(Attrhtml);
                            } else {
                                $('#sortableAttribute2').html(Attrhtml2);
                                var Attrhtml = '';
                                for (var i = 0; i < allAttributes.length; i++) {
                                    Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>';
                                    $('#sortableAttribute1').html(Attrhtml);
                                }
                            }
                        } else {
                            $('#sortableAttribute2').html('');
                            var Attrhtml = '';
                            if (allAttributes != null) {
                                for (var i = 0; i < allAttributes.length; i++) {
                                    Attrhtml += '<li data-Id="' + allAttributes[i].Id + '" data-DisplayName="' + allAttributes[i].Caption + '" class="active DragableItem"><a href="JavaScript: void(0);">' + allAttributes[i].Caption + '</a></li>';
                                    $('#sortableAttribute1').html(Attrhtml);
                                }
                            }
                        }

                    }
                });
            });
        };
        $scope.loadattributesfrSearchCriteria();

        $scope.searchsave = function () {
            var entitytype = 0;
            if ($scope.entitytype != undefined) {
                entitytype = $scope.entitytype;
            }
            var listSettings = {};
            listSettings.ProductionType = {
                "Attributes": [],
            };
            listSettings.LogoSettings = AdminLogoSettings;
            listSettings.Key = ListView;
            listSettings.TypeId = entitytype;
            var AttributeArray = {
                "Attribute": []
            };
            for (var j = 0; j < $("#sortableAttribute2").find('li').length; j++) {
                var SortOrder = j + 1;
                var Attribute = {
                    "Id": $("#sortableAttribute2").find('li').eq(j).attr("data-Id"),
                    "SortOrder": SortOrder,
                    "DisplayName": $("#sortableAttribute2").find('li').eq(j).attr("data-DisplayName"),
                };
                AttributeArray.Attribute.push(Attribute)
            }
            listSettings.ProductionType.Attributes.push(AttributeArray);
            listSettings.enableAutoExpand = $scope.enableAutoExp;
            AdminService.SearchadminSettingsforRootLevelInsertUpdate(listSettings).then(function () {
                NotifySuccess($translate.instant('LanguageContents.Res_4802.Caption'));
            });
        }

        $scope.autoExp = function (event) {
            var checkbox = event.target;
            if (checkbox.checked)
                $scope.enableAutoExp = true;
            else
                $scope.enableAutoExp = false;

        }

    }
    app.controller("mui.admin.seacrhcriteriaCtrl", ['$scope', '$resource', '$location', '$translate', 'AdminService', muiadminseacrhcriteriaCtrl]);
})(angular, app);