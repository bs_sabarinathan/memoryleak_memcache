﻿(function (ng, app) {
    function muiplanningtooldefaultCtrl($scope, $location, $timeout, ListService) {
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.cms.default.detail.listCtrl']"));
        });

        function GetListCmsRevisedContentByID(ID) {
            $scope.ShowFullPageAdd = false;
            $("#ShowFullPageAdd").css('display', 'none');
            ListService.GetRevisedContentByFeature(ID).then(function (data) {
                if (data.Response != null) {
                    $timeout(function () {
                        $('#htmlEntityContent').html(data.Response.Content);
                        $('.btnbrowse').css("display", 'none');
                    }, 200);
                }
            });
        }
    }
    app.controller('mui.cms.default.detail.listCtrl', ['$scope', '$location', '$timeout', 'ListService', muiplanningtooldefaultCtrl]);
    function ListService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetRevisedContentByFeature: GetRevisedContentByFeature
        });
        function GetRevisedContentByFeature(EntityID) { var request = $http({ method: "get", url: "api/cms/GetRevisedContentByFeature/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("ListService", ['$http', '$q', ListService]);
})(angular, app);