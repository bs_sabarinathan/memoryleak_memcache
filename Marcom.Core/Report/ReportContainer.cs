﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Report.Interface;

namespace BrandSystems.Marcom.Core.Report
{
    internal class ReportContainer : IReportContainer
    {
        public string Name
        {
            get;
            set;
        }

        public string ColorCode
        {
            get;
            set;
        }

        public string ShortDescription
        {
            get;
            set;
        }

        public Hashtable MetadataCollections
        {
            get;
            set;
        }

        public List<Hashtable> MemberCollections
        {
            get;
            set;
        }

        public Hashtable TaskOverviewSummary
        {
            get;
            set;
        }

        public List<Hashtable> TaskCollections
        {
            get;
            set;
        }

        public List<Hashtable> FinancialCollections
        {
            get;
            set;
        }

        public int ID
        {
            get;
            set;
        }

        public int TypeID
        {
            get;
            set;
        }

        public int Level
        {
            get;
            set;
        }

        public List<Hashtable> TaskListCollections 
        { 
            get; 
            set; 
        }
        public Dictionary<string, string> MetadataColumnCollection 
        { 
            get; 
            set; 
        }

      public  List<Hashtable> CostcentreCollections { get; set; }
      public List<Hashtable> ObjectiveCollections { get; set; }
    }

}
