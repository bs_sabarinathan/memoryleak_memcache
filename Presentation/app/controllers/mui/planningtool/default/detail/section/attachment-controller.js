﻿(function (ng, app) {
    "use strict";

    function muiplanningtooldefaultdetailattachmentCtrl($scope, $timeout, $http, $compile, $resource, $window, $cookies, $stateParams, DamNewService) {
        DamNewService.GetAllExtensionTypesforDAM().then(function (data) {
            $scope.DAMtypeswithExt = data.Response;
            $scope.AssetTasktypeswithExt = data.Response;
            $scope.damExtensionObj = {
                DAMtypeswithExt: $scope.DAMtypeswithExt,
                AssetTasktypeswithExt: $scope.AssetTasktypeswithExt
            }
        });
        DamNewService.GetMember($stateParams.ID).then(function (EntityMemberList) {
            $scope.EntityMemberList = EntityMemberList.Response;
            $scope.EntityMemberObj = {
                EntityMemberList: $scope.EntityMemberList
            };
        });
        $scope.$emit('mui_resetuploaderstatus');
        $scope.DAMentitytypes = [];
        $scope.IsToShowDAMEntity = false;
        //$scope.IsLock = false;
        if ($scope.MediaBankSettings.ProductionTypeID == 35 && $scope.MediaBankSettings.directfromMediaBank == true) {
            $scope.IsLock = false;
        }
        $scope.processingsrcobj = {
            processingid: $stateParams.ID,
            processingplace: "attachment",
            processinglock: $scope.IsLock
        };
        $scope.$on('ReloadEntitytaskLibrary', function (event, tasklistid) {
            $scope.$emit('RefreshEntitytaskLibrary', tasklistid);
        });
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.detail.attachmentCtrl']"));
        });
        $scope.$on('LoadAttachment', function (event, ID) {
            $scope.processingsrcobj = {
                processingid: $stateParams.ID,
                processingplace: "attachment",
                processinglock: $scope.IsLock
            };
            $scope.$broadcast('ReloadAssetView');
        });
    }
    app.controller("mui.planningtool.default.detail.attachmentCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$window', '$cookies', '$stateParams', 'DamNewService', muiplanningtooldefaultdetailattachmentCtrl]);
})(angular, app);