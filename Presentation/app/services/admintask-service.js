﻿app.service('AdminTaskService', function ($http, $q) {
    $http.defaults.headers.common.sessioncookie = $.cookie('Session');
    return ({
        UpdateEntityTaskSortOrder: UpdateEntityTaskSortOrder
        , DeleteAdminTaskCheckListByID: DeleteAdminTaskCheckListByID
        , GetTaskList: GetTaskList
        , GetAdminTasksByTaskListID: GetAdminTasksByTaskListID
        , GetAdminTasksById: GetAdminTasksById
        , getAdminTaskchecklist: getAdminTaskchecklist
        , InsertUpdateTaskList: InsertUpdateTaskList
        , UpdateTaskSortOrder: UpdateTaskSortOrder
        , InsertLinkInAdminTasks: InsertLinkInAdminTasks
        , InsertTaskAttachments: InsertTaskAttachments
        , InsertTaskWithAttachments: InsertTaskWithAttachments
        , UpdateAdminTask: UpdateAdminTask
        , DeleteTaskTemplateListById: DeleteTaskTemplateListById
        , InsertUpdateAdminTaskTemplate: InsertUpdateAdminTaskTemplate
        , DeleteSystemTaskList: DeleteSystemTaskList
        , DeleteAdminTask: DeleteAdminTask
        , UpdatetaskAdminAttachmentDescription: UpdatetaskAdminAttachmentDescription
        , GetEntityAttributesDetails: GetEntityAttributesDetails
        , GetEntityTaskAttachmentinfo: GetEntityTaskAttachmentinfo
        , InsertTaskLibImport: InsertTaskLibImport
        , ExportTaskList: ExportTaskList
        , GetAllApprovalFlowTemplates: GetAllApprovalFlowTemplates
        , UpdateAdminTasklistSortOrder: UpdateAdminTasklistSortOrder
        , UpdateAdmintaskPhaseSortOrder: UpdateAdmintaskPhaseSortOrder
        , DeletePhaseStep: DeletePhaseStep
        , GetAllAdminApprovalFlowPhasesSteps: GetAllAdminApprovalFlowPhasesSteps
        , RemovePhase: RemovePhase
        , GetAllExtensionTypesforDAM: GetAllExtensionTypesforDAM
        , GetAssetCategoryTree: GetAssetCategoryTree
        , GetDamAttributeRelation: GetDamAttributeRelation
        , GetAssetCategoryPathInAssetEdit: GetAssetCategoryPathInAssetEdit
        , SaveAsset: SaveAsset
        , GetTaskTypes: GetTaskTypes
        , GetEntityTypeAttributeRelationWithLevelsByID: GetEntityTypeAttributeRelationWithLevelsByID
        , GetValidationDationByEntitytype: GetValidationDationByEntitytype
        , SaveDetailBlockForLevels: SaveDetailBlockForLevels
        , SaveDetailBlockForTreeLevels: SaveDetailBlockForTreeLevels
        , GetAttributeTreeNodeByEntityID: GetAttributeTreeNodeByEntityID
        , UpdateDropDownTreePricing: UpdateDropDownTreePricing
        , GetDropDownTreePricingObjectFromParentDetail: GetDropDownTreePricingObjectFromParentDetail
        , GetDropDownTreePricingObject: GetDropDownTreePricingObject
        , GetApprovalRoles: GetApprovalRoles
        , UpdateImageName: UpdateImageName
        , DeleteEntityPeriod: DeleteEntityPeriod
        , InsertEntityPeriod: InsertEntityPeriod
        , PostEntityPeriod: PostEntityPeriod
        , InsertEntityPeriod: InsertEntityPeriod
        , GetCurrencyListFFsettings: GetCurrencyListFFsettings
        , GetCostCentreCurrencyRateById: GetCostCentreCurrencyRateById
        , GetTaskFulfillmentEntityTypes: GetTaskFulfillmentEntityTypes
        , GetTaskTemplateDetails: GetTaskTemplateDetails
        , GetTemplateAdminTaskList: GetTemplateAdminTaskList
        , GetTaskTemplateConditionByTaskTempId: GetTaskTemplateConditionByTaskTempId
        , GetFulfillmentAttribute: GetFulfillmentAttribute
        , GetFulfillmentAttributeOptions: GetFulfillmentAttributeOptions
        , InsertUpdateTaskTemplateCondition: InsertUpdateTaskTemplateCondition
        , DeleteTemplateConditionById: DeleteTemplateConditionById
        , InsertTempTaskList: InsertTempTaskList
        , InsertUpdateTemplate: InsertUpdateTemplate
        , DeleteAdminTemplateTaskRelationById: DeleteAdminTemplateTaskRelationById
        , UpdateTemplateTaskListSortOrder: UpdateTemplateTaskListSortOrder,
        GetTaskAttachmentFile: GetTaskAttachmentFile,
        savemultipleUploadedImg: savemultipleUploadedImg,
        UpdateImageNameforTask: UpdateImageNameforTask,
        copyuploadedImage: copyuploadedImage,
        deleteAssets: deleteAssets
    });
    //Manoj

    function UpdateEntityTaskSortOrder(formobj) { var request = $http({ method: "post", url: "api/task/UpdateEntityTaskSortOrder/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function DeleteAdminTaskCheckListByID(ID) { var request = $http({ method: "delete", url: "api/task/DeleteAdminTaskCheckListByID/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
    function GetTaskList() { var request = $http({ method: "get", url: "api/task/GetTaskList/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetAdminTasksByTaskListID(TaskListID) { var request = $http({ method: "get", url: "api/task/GetAdminTasksByTaskListID/" + TaskListID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetAdminTasksById(formobj) { var request = $http({ method: "post", url: "api/task/GetAdminTasksById/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function getAdminTaskchecklist(TaskID) { var request = $http({ method: "get", url: "api/task/getAdminTaskchecklist/" + TaskID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function InsertUpdateTaskList(formobj) { var request = $http({ method: "post", url: "api/task/InsertUpdateTaskList/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function UpdateTaskSortOrder(TaskId, TaskListID, SortOrderID) { var request = $http({ method: "put", url: "api/task/UpdateTaskSortOrder/" + TaskId + "/" + TaskListID + "/" + SortOrderID, params: { action: "update" } }); return (request.then(handleSuccess, handleError)); }
    function InsertLinkInAdminTasks(formobj) { var request = $http({ method: "post", url: "api/task/InsertLinkInAdminTasks/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function InsertTaskAttachments(formobj) { var request = $http({ method: "post", url: "api/task/InsertTaskAttachments/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function InsertTaskWithAttachments(formobj) { var request = $http({ method: "post", url: "api/task/InsertTaskWithAttachments/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function UpdateAdminTask(formobj) { var request = $http({ method: "post", url: "api/task/UpdateAdminTask/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function DeleteTaskTemplateListById(ID) { var request = $http({ method: "delete", url: "api/task/DeleteTaskTemplateListById/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
    function InsertUpdateAdminTaskTemplate(Phasestepdetails, EntityId) { var request = $http({ method: "post", url: "api/task/InsertUpdateAdminTaskTemplate/", params: { action: "add" }, data: { "PhaseStepDetails": Phasestepdetails, "EntityID": EntityId } }); return (request.then(handleSuccess, handleError)); }
    function DeleteSystemTaskList(ID) { var request = $http({ method: "delete", url: "api/task/DeleteSystemTaskList/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
    function DeleteAdminTask(TaskID) { var request = $http({ method: "delete", url: "api/task/DeleteAdminTask/" + TaskID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
    function UpdatetaskAdminAttachmentDescription(formobj) { var request = $http({ method: "put", url: "api/task/UpdatetaskAdminAttachmentDescription/", params: { action: "update" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function GetEntityAttributesDetails(TaskID) { var request = $http({ method: "get", url: "api/task/GetEntityAttributesDetails/" + TaskID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetEntityTaskAttachmentinfo(TaskID) { var request = $http({ method: "get", url: "api/task/GetEntityTaskAttachmentinfo/" + TaskID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function InsertTaskLibImport(formobj) { var request = $http({ method: "post", url: "api/task/InsertTaskLibImport/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function ExportTaskList(TaskListID, NewGuid, TaskLibName) { var request = $http({ method: "get", url: "api/task/ExportTaskList/" + TaskListID + "/" + NewGuid + "/" + TaskLibName, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetAllApprovalFlowTemplates() { var request = $http({ method: "get", url: "api/task/GetAllApprovalFlowTemplates/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function UpdateAdminTasklistSortOrder(formobj) { var request = $http({ method: "post", url: "api/task/UpdateAdminTasklistSortOrder/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function UpdateAdmintaskPhaseSortOrder(formobj) { var request = $http({ method: "post", url: "api/task/UpdateAdmintaskPhaseSortOrder/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function DeletePhaseStep(stepID, EntityID) { var request = $http({ method: "delete", url: "api/task/DeletePhaseStep/" + stepID + "/" + EntityID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
    function GetAllAdminApprovalFlowPhasesSteps(EntityId) { var request = $http({ method: "get", url: "api/task/GetAllAdminApprovalFlowPhasesSteps/" + EntityId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function RemovePhase(phaseID, EntityID) { var request = $http({ method: "delete", url: "api/task/RemovePhase/" + phaseID + "/" + EntityID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
    function GetAllExtensionTypesforDAM() { var request = $http({ method: "get", url: "api/dam/GetDAMExtensions/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetAssetCategoryTree(AsetTypeID) { var request = $http({ method: "Get", url: "api/dam/GetAssetCategoryTree/" + AsetTypeID, params: { action: "Get" } }); return (request.then(handleSuccess, handleError)); }
    function GetDamAttributeRelation(damid) { var request = $http({ method: "get", url: "api/dam/GetDamAttributeRelation/" + damid, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetAssetCategoryPathInAssetEdit(AssetTypeID) { var request = $http({ method: "Get", url: "api/dam/GetAssetCategoryPathInAssetEdit/" + AssetTypeID, params: { action: "Get" } }); return (request.then(handleSuccess, handleError)); }
    function SaveAsset(SaveAsset) { var request = $http({ method: "post", url: "api/dam/CreateAsset/", params: { action: "add" }, data: { saveAsset: SaveAsset } }); return (request.then(handleSuccess, handleError)); }
    function GetTaskTypes() { var request = $http({ method: "get", url: "api/Metadata/GetTaskTypes/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetEntityTypeAttributeRelationWithLevelsByID(ID, ParentID) { var request = $http({ method: "get", url: "api/Metadata/GetEntityTypeAttributeRelationWithLevelsByID/" + ID + "/" + ParentID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetValidationDationByEntitytype(EntityTypeID) { var request = $http({ method: "get", url: "api/Metadata/GetValidationDationByEntitytype/" + EntityTypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function SaveDetailBlockForLevels(jobj) { var request = $http({ method: "post", url: "api/Metadata/SaveDetailBlockForLevels/", params: { action: "add" }, data: jobj }); return (request.then(handleSuccess, handleError)); }
    function SaveDetailBlockForTreeLevels(formobj) { var request = $http({ method: "post", url: "api/Metadata/SaveDetailBlockForTreeLevels/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function GetAttributeTreeNodeByEntityID(AttributeID, EntityID) { var request = $http({ method: "get", url: "api/Metadata/GetAttributeTreeNodeByEntityID/" + AttributeID + "/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function UpdateDropDownTreePricing(formobj) { var request = $http({ method: "post", url: "api/Metadata/UpdateDropDownTreePricing/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function GetDropDownTreePricingObjectFromParentDetail(AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId) { var request = $http({ method: "get", url: "api/Metadata/GetDropDownTreePricingObjectFromParentDetail/" + AttributeID + "/" + IsInheritFromParent + "/" + Isfromparent + "/" + Entityid + "/" + ParentId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetDropDownTreePricingObject(AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId) { var request = $http({ method: "get", url: "api/Metadata/GetDropDownTreePricingObject/" + AttributeID + "/" + IsInheritFromParent + "/" + Isfromparent + "/" + Entityid + "/" + ParentId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetApprovalRoles() { var request = $http({ method: "get", url: "api/Metadata/GetApprovalRoles/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function UpdateImageName(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateImageName/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function DeleteEntityPeriod(ID) { var request = $http({ method: "delete", url: "api/Planning/DeleteEntityPeriod/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
    function InsertEntityPeriod(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertEntityPeriod/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function PostEntityPeriod(formobj) { var request = $http({ method: "post", url: "api/Planning/PostEntityPeriod/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function GetCurrencyListFFsettings() { var request = $http({ method: "get", url: "api/Planning/GetCurrencyListFFsettings/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetCostCentreCurrencyRateById(EntityId, CurrencyId, IsCostCentreCreation) { var request = $http({ method: "get", url: "api/Planning/GetCostCentreCurrencyRateById/" + EntityId + "/" + CurrencyId + "/" + IsCostCentreCreation, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetTaskFulfillmentEntityTypes() { var request = $http({ method: "get", url: "api/Metadata/GetTaskFulfillmentEntityTypes/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetTaskTemplateDetails() { var request = $http({ method: "get", url: "api/task/GetTaskTemplateDetails/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetTemplateAdminTaskList() { var request = $http({ method: "get", url: "api/task/GetTemplateAdminTaskList/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetTaskTemplateConditionByTaskTempId(TaskTempID) { var request = $http({ method: "get", url: "api/task/GetTaskTemplateConditionByTaskTempId/" + TaskTempID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetFulfillmentAttribute(EntityTypeID) { var request = $http({ method: "get", url: "api/Metadata/GetFulfillmentAttribute/" + EntityTypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function GetFulfillmentAttributeOptions(AttributeID, AttributeLevel) { var request = $http({ method: "get", url: "api/Metadata/GetFulfillmentAttributeOptions/" + AttributeID + "/" + AttributeLevel, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function InsertUpdateTaskTemplateCondition(formobj) { var request = $http({ method: "post", url: "api/task/InsertUpdateTaskTemplateCondition/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function DeleteTemplateConditionById(TemplateCondID) { var request = $http({ method: "delete", url: "api/task/DeleteTemplateConditionById/" + TemplateCondID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
    function InsertTempTaskList(formobj) { var request = $http({ method: "post", url: "api/task/InsertTempTaskList/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function InsertUpdateTemplate(formobj) { var request = $http({ method: "post", url: "api/task/InsertUpdateTemplate/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function DeleteAdminTemplateTaskRelationById(TaskListID, TemplateID) { var request = $http({ method: "delete", url: "api/task/DeleteAdminTemplateTaskRelationById/" + TaskListID + "/" + TemplateID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
    function UpdateTemplateTaskListSortOrder(TempID, TaskListID, SortOrderID) { var request = $http({ method: "put", url: "api/task/UpdateTemplateTaskListSortOrder/" + TempID + "/" + TaskListID + "/" + SortOrderID, params: { action: "update" } }); return (request.then(handleSuccess, handleError)); }
    function GetTaskAttachmentFile(TaskID) { var request = $http({ method: "get", url: "api/task/GetTaskAttachmentFile/" + TaskID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
    function savemultipleUploadedImg(uplImgArr) { var request = $http({ method: "post", url: "api/Planning/savemultipleUploadedImg/", params: { action: "add" }, data: { uplImgArr: uplImgArr } }); return (request.then(handleSuccess, handleError)); }
    function UpdateImageNameforTask(formobj) { var request = $http({ method: "post", url: "api/Task/UpdateImageNameforTask/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
    function copyuploadedImage(filename) { var request = $http({ method: "post", url: "api/Planning/copyuploadedImage/", params: { action: "save" }, data: { filename: filename } }); return (request.then(handleSuccess, handleError)); }
    function deleteAssets(idArr) { var request = $http({ method: "post", url: "api/dam/DeleteAssets/", params: { action: "add" }, data: idArr }); return (request.then(handleSuccess, handleError)); }
    function handleError(response) {
        if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
        return ($q.reject(response.data.message));
    }
    function handleSuccess(response) { return (response.data); }
});