using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
	/// FeedDao object for table 'CM_Feed'.
	/// </summary>

    public partial class FeedDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected int _actor;
		protected int _templateid;
		protected DateTimeOffset _happenedon;
		protected DateTimeOffset _commentedupdatedon;
		protected int? _entityid;
		protected string _typename;
		protected string _attributename;
		protected string _fromvalue;
		protected string _tovalue;
        protected string _attributegrouprecordname;
		protected IList<FeedCommentDao> _feedcomment;
        protected int _userid;
        protected int? _associatedentityid;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
        protected int _version;
        protected int? _phaseID;
        protected string _stepID;
		#endregion
		
		#region Constructors
		public FeedDao() {}

        public FeedDao(int pActor, int pTemplateid, DateTimeOffset pHappenedOn, DateTimeOffset pCommentedUpdatedOn, int? pEntityid, string pTypeName, string pAttributeName, string pFromValue, string pToValue, int pUserID, int? pAssocitedEntityID, string pAttributeGroupRecordName, int pVersion, int? pphaseID, string pstepID)
		{
			this._actor = pActor; 
			this._templateid = pTemplateid; 
			this._happenedon = pHappenedOn; 
			this._commentedupdatedon = pCommentedUpdatedOn; 
			this._entityid = pEntityid; 
			this._typename = pTypeName; 
			this._attributename = pAttributeName; 
			this._fromvalue = pFromValue; 
			this._tovalue = pToValue;
            this._userid = pUserID;
            this._associatedentityid = pAssocitedEntityID;
            this._attributegrouprecordname = pAttributeGroupRecordName;
            this._version = pVersion;
            this._phaseID = pphaseID;
            this._stepID = pstepID;
            
		}
				
		public FeedDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual int Actor
		{
			get { return _actor; }
			set { _bIsChanged |= (_actor != value); _actor = value; }
			
		}
		
		public virtual int Templateid
		{
			get { return _templateid; }
			set { _bIsChanged |= (_templateid != value); _templateid = value; }
			
		}
		
		public virtual DateTimeOffset HappenedOn
		{
			get { return _happenedon; }
			set { _bIsChanged |= (_happenedon != value); _happenedon = value; }
			
		}
		
		public virtual DateTimeOffset CommentedUpdatedOn
		{
			get { return _commentedupdatedon; }
			set { _bIsChanged |= (_commentedupdatedon != value); _commentedupdatedon = value; }
			
		}
		
		public virtual int? Entityid
		{
			get { return _entityid; }
			set { _bIsChanged |= (_entityid != value); _entityid = value; }
			
		}
		
		public virtual string TypeName
		{
			get { return _typename; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("TypeName", "TypeName value, cannot contain more than 50 characters");
			  _bIsChanged |= (_typename != value); 
			  _typename = value; 
			}
			
		}
        public virtual string AttributeGroupRecordName
        {
            get { return _attributegrouprecordname; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("AttributeGroupRecordName", "AttributeGroupRecordName value, cannot contain more than 50 characters");
                _bIsChanged |= (_attributegrouprecordname != value);
                _attributegrouprecordname = value;
            }

        }
		public virtual string AttributeName
		{
			get { return _attributename; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("AttributeName", "AttributeName value, cannot contain more than 250 characters");
			  _bIsChanged |= (_attributename != value); 
			  _attributename = value; 
			}
			
		}
		
		public virtual string FromValue
		{
			get { return _fromvalue; }
			set 
			{
			  if (value != null && value.Length > 2000)
			    throw new ArgumentOutOfRangeException("FromValue", "FromValue value, cannot contain more than 2000 characters");
			  _bIsChanged |= (_fromvalue != value); 
			  _fromvalue = value; 
			}
			
		}
		
		public virtual string ToValue
		{
			get { return _tovalue; }
			set 
			{
			  if (value != null && value.Length > 2000)
			    throw new ArgumentOutOfRangeException("ToValue", "ToValue value, cannot contain more than 2000 characters");
			  _bIsChanged |= (_tovalue != value); 
			  _tovalue = value; 
			}
			
		}
		
		public virtual IList<FeedCommentDao> FeedCommentDao
		{
			get { return _feedcomment; }
			set { _bIsChanged |= (_feedcomment != value); _feedcomment = value; }
			
		}

        public virtual int UserID
        {
            get { return _userid; }
            set { _bIsChanged |= (_userid != value); _userid = value; }

        }
        public virtual int? AssocitedEntityID
        {
            get { return _associatedentityid; }
            set { _bIsChanged |= (_associatedentityid != value); _associatedentityid = value; }
        }


        public virtual int Version
        {
            get { return _version; }
            set { _bIsChanged |= (_version != value); _version = value; }

        }
        public virtual int? PhaseID
        {
            get { return _phaseID; }
            set { _phaseID = value; }
        }
        public virtual string  StepID
        {
            get { return _stepID; }
            set { _stepID = value; }
        }
        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string FeedDao = "FeedDao";			
			public const string Id = "Id";			
			public const string Actor = "Actor";			
			public const string Templateid = "Templateid";			
			public const string HappenedOn = "HappenedOn";			
			public const string CommentedUpdatedOn = "CommentedUpdatedOn";			
			public const string Entityid = "Entityid";			
			public const string TypeName = "TypeName";			
			public const string AttributeName = "AttributeName";			
			public const string FromValue = "FromValue";			
			public const string ToValue = "ToValue";
			public const string FeedComment_FeedCommentCollection = "FeedComment_FeedCommentCollection";			
			public const string FeedCommentDao = "FeedCommentDao";
            public const string UserID = "UserID";
            public const string AssocitedEntityID = "AssocitedEntityID";
            public const string AttributeGroupRecordName = "AttributeGroupRecordName";
            public const string Version = "Version";
            public const string PhaseID = "PhaseID";
            public const string StepID = "StepID";

		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string FeedDao = "FeedDao";			
			public const string Id = "Id";			
			public const string Actor = "Actor";			
			public const string Templateid = "Templateid";			
			public const string HappenedOn = "HappenedOn";			
			public const string CommentedUpdatedOn = "CommentedUpdatedOn";			
			public const string Entityid = "Entityid";			
			public const string TypeName = "TypeName";			
			public const string AttributeName = "AttributeName";			
			public const string FromValue = "FromValue";			
			public const string ToValue = "ToValue";
			public const string FeedComment_FeedCommentCollection = "FeedComment_FeedCommentCollection";			
			public const string FeedCommentDao = "FeedCommentDao";
            public const string UserID = "UserID";
            public const string AssocitedEntityID = "AssocitedEntityID";
            public const string AttributeGroupRecordName = "AttributeGroupRecordName";
            public const string Version = "Version";
            public const string PhaseID = "PhaseID";
            public const string StepID = "StepID";
		}

		#endregion
		
		
	}
	
}
