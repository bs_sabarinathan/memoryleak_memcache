﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Task.Interface
{
    /// <summary>
    /// ITaskLinks interface for table 'TM_Links'.
    /// </summary>
    public interface ITasklinks
    {
        #region Public Properties

        int ID
        {
            get;
            set;

        }

        int EntityID
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        string URL
        {
            get;
            set;
        }

        int ActiveVersionNo
        {
            get;
            set;
        }


        int TypeID
        {
            get;
            set;
        }

        string CreatedOn
        {
            get;
            set;
        }
        int OwnerID
        {
            get;
            set;
        }

        int ModuleID
        {
            get;
            set;
        }
        Guid LinkGuid
        {
            get;
            set;
        }

        string Description
        {
            get;
            set;
        }

        string Extension { get; set; }

        int LinkType
        {
            get;
            set;

        }

        #endregion
    }
}
