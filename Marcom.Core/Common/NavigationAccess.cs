using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// NavigationAccess object for table 'CM_NavigationAccess'.
	/// </summary>
	
	internal class NavigationAccess : INavigationAccess 
	{
		#region Member Variables

		protected int _navigationid;
		protected int _globalroleid;
		
		
		#endregion
		
		#region Constructors
		public NavigationAccess() {}
			
		public NavigationAccess(int pNavigationid, int pGlobalRoleid)
		{
			this._navigationid = pNavigationid; 
			this._globalroleid = pGlobalRoleid; 
		}
				
		public NavigationAccess(int pNavigationid)
		{
			this._navigationid = pNavigationid; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Navigationid
		{
			get { return _navigationid; }
			set { _navigationid = value; }
			
		}
		
		public int GlobalRoleid
		{
			get { return _globalroleid; }
			set { _globalroleid = value; }
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			NavigationAccess castObj = null;
			try
			{
				castObj = (NavigationAccess)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._navigationid == castObj.Navigationid );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _navigationid.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
