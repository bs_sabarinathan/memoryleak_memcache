﻿using BrandSystems.MediaHandler.Input;
using BrandSystems.MediaHandler;
using BrandSystems.MediaHandler.Output;
using BrandSystems.MediaHandler.CustomType;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using BrandSystems.Marcom.Core.Utility;
using System.Xml;
using System.IO;
using System.Runtime.Serialization;
using System.Reflection;
using BrandSystems.Marcom.Core.Metadata;
using System.Text;
using BrandSystems.Marcom.DAM;
using Newtonsoft.Json;
using NHibernate.Mapping;
using System.Collections;
using BrandSystems.Marcom.Core.AmazonStorageHelper;
using BrandSystems.Marcom.Core.Core.Managers.Proxy;
using BrandSystems.Marcom.Core.Interface;
using System.Xml.Linq;

namespace BrandSystems.Marcom.Core.Utility
{
    public class MediahandlerMarcom
    {
        private decimal decFileSize;
        private const string FormatKB = "0";
        //private const decimal decInchToMMConst = 25.4;
        public const Decimal decInchToMMConst = 25;
        private const int maxFileSize = 400000;
        private string[] _mimeType = null;
        // private static BrandSystems.Marcom.Core.Utility.MediaHandler.MediaHandlerClient MHClient = new BrandSystems.Marcom.Core.Utility.MediaHandler.MediaHandlerClient(ConfigurationSettings.AppSettings.Item("MHUrl").ToString(), null, null, ConfigurationSettings.AppSettings.Item("IPAddress").ToString(), Convert.ToInt32(ConfigurationSettings.AppSettings.Item("MaxPort")), Convert.ToInt32(ConfigurationSettings.AppSettings.Item("MinPort")));
        //private static BrandSystems.Marcom.Core.Utility.MediaHandler.MediaHandlerClient MHClient = new BrandSystems.Marcom.Core.Utility.MediaHandler.MediaHandlerClient(ConfigurationManager.AppSettings["MHUrl"].ToString(), null, null, ConfigurationManager.AppSettings["IPAddress"].ToString(), Convert.ToInt32(ConfigurationManager.AppSettings["MaxPort"].ToString()), Convert.ToInt32(ConfigurationManager.AppSettings["MinPort"].ToString()));


        public bool MHClientReady()
        {
            bool status = false;
            try
            {
                //MHClient.Ping();
                status = true;
                return status;

            }
            catch (Exception ex)
            {
                LogHandler.LogInfo("MH not responding:" + ex.Message, LogHandler.LogType.Notify);
            }
            return status;
        }

        public string[] MimeType
        {
            get { return _mimeType; }
            set { _mimeType = value; }
        }


        public bool GereratePreviewWithmarcom(string sourcePath, string destinationPath, int maxHight, int maxWidth, bool ISs3amazonbucket = false, string s3accesskey = "", string s3securitykey = "", string s3region = "", string s3Foldername = "", string s3Filename = "", string s3FileSourcePath = "", string s3FileDestinationPath = "", int imgQuality = 65)
        {
            //MHClient.Ping();
            Guid gID = Guid.NewGuid();
            ImageRequestObject imgReq = new ImageRequestObject(Guid.NewGuid());
            imgReq.Thumbnail = true;
            imgReq.ClientID = 1000; //need to check
            imgReq.ApplicationID = 1000;

            imgReq.SourcePath = sourcePath;
            imgReq.DestinationPath = destinationPath;
            imgReq.ResizeHeight = maxHight;
            imgReq.ResizeWidth = maxWidth;
            imgReq.ColourSpace = ColourSpace.sRGB;
            imgReq.ISs3amazonbucket = ISs3amazonbucket;
            imgReq.s3accesskey = s3accesskey;
            imgReq.s3securitykey = s3securitykey;
            imgReq.s3region = s3region;
            imgReq.s3Foldername = s3Foldername;
            imgReq.s3Filename = s3Filename;
            imgReq.s3FileSourcePath = s3FileSourcePath;
            imgReq.s3FileDestinationPath = s3FileDestinationPath;
            //if (destinationPath.Contains("Small_") == true)            
            //    imgReq.Quality = 65;
            //else
            //    imgReq.Quality = 100;

            //imgReq.Quality = 65;
            imgReq.Quality = imgQuality;
            //imgReq.MetaDataNeeded = new MetaData[] { MetaData.ColourSpace, MetaData.Compression };
            //Need to create enum
            imgReq.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.Convert;


            //Validate Object ex. validateRequest();
            // Need to execute the task ex. mgResObj = MHClient.ExecuteSynchronousTask(imgReqObj)
            //ImageResponseObject imgResObj = new ImageResponseObject();
            ImageResponseObject imgResObj = new ImageResponseObject();
            //Execute Task
            imgResObj = (BrandSystems.MediaHandler.Output.ImageResponseObject) ExecuteSynchronousTaskwithmarcom(imgReq);

            //timeingly 
            //********************
            ////  imgReq.JobID = Guid.NewGuid();
            ////  imgReq.SourcePath = imgResObj.DestinationPath;
            ////  imgReq.MetaDataNeeded = new MetaData[] { MetaData.Height, MetaData.Width, MetaData.XResolution };

            ////  imgReq.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.GetInfo;

            ////  //Get Height Width for destination image
            ////  imgResObj = (BrandSystems.MediaHandler.Output.ImageResponseObject)MHClient.ExecuteSynchronousTask(imgReq);

            ////  return imgResObj;
            bool status = false;
            //********************
            if ((imgResObj.ErrorDetail == null | object.ReferenceEquals(imgResObj.ErrorDetail, "")))
            {
                // ErrDescription = imgResObj.ErrorDetail;
                status = true;


            }
            else
            {
                LogHandler.LogInfo("GereratePreview Issue:" + imgResObj.ErrorDetail, LogHandler.LogType.Notify);
                status = false;
            }

            return status;
            //////(BrandSystems.MediaHandler.Output.MediaHandlerResponseObject)
            ////Execute Task

            ////imgResObj = MHClient.ExecuteSynchronousTask(imgReq);
            //if (!(imgResObj.ErrorDetail == null | object.ReferenceEquals(imgResObj.ErrorDetail, "")))
            //{
            //   // ErrDescription = imgResObj.ErrorDetail;
            //    return null;
            //}

            //imgReq.JobID = Guid.NewGuid();
            //imgReq.SourcePath=imgResObj.DestinationPath;
            //imgReq.MetaDataNeeded = new MetaData[] {	MetaData.Height,MetaData.Width,	MetaData.XResolution};
            //imgReq.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.GetInfo;

            ////Get Height Width for destination image
            ////imgResObj = MHClient.ExecuteSynchronousTask(imgReq);

            //if ((imgResObj.ErrorDetail == null | object.ReferenceEquals(imgResObj.ErrorDetail, "")))
            //{
            //    return new int[] { (int)imgResObj.Height, (int)imgResObj.Width, (int)imgResObj.XResolution };
            //}
            //else
            //{
            //    //ErrDescription = imgResObj.ErrorDetail;
            //    return null;
            //}


        }
      
        public bool ValidateRequest()
        {


            return true;
        }

      
        public string getFileInfoWithmarcom(string strSourcePath, bool ISs3amazonbucket = false, string s3accesskey = "", string s3securitykey = "", string s3region = "", string s3Foldername = "", string s3Filename = "", string s3FileSourcePath = "")
        {
          //  MHClient.Ping();
            //Guid gID = Guid.NewGuid();
            StringBuilder fileinfo = new StringBuilder();
            ImageRequestObject imgReqObj = new ImageRequestObject(Guid.NewGuid());
            ImageResponseObject imgResObj = new ImageResponseObject();
            string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
            string retFilePath = baseDir + "//" + "ServiceErrorLog.txt";
            BrandSystems.Marcom.Core.Utility.ErrorLog.LogFilePath = retFilePath;
            try
            {
                //ErrDescription = "";



                imgReqObj.ClientID = 1000;
                imgReqObj.ApplicationID = 1000;

                imgReqObj.SourcePath = strSourcePath;




                imgReqObj.JobID = Guid.NewGuid();

                imgReqObj.MetaDataNeeded = new MetaData[] { MetaData.Height, MetaData.Width, MetaData.XResolution };
                imgReqObj.ISs3amazonbucket = ISs3amazonbucket;
                imgReqObj.s3accesskey = s3accesskey;
                imgReqObj.s3securitykey = s3securitykey;
                imgReqObj.s3region = s3region;
                imgReqObj.s3Foldername = s3Foldername;
                imgReqObj.s3Filename = s3Filename;
                imgReqObj.s3FileSourcePath = s3FileSourcePath;


                imgReqObj.TaskToPerform = BrandSystems.MediaHandler.CustomType.Task.GetInfo;

                //Get Height Width for destination image
                imgResObj = (BrandSystems.MediaHandler.Output.ImageResponseObject)ExecuteSynchronousTaskwithmarcom(imgReqObj);

                //return imgResObj;

                //    return imgResObj;
            }
            catch (Exception ex)
            {
            
                BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "<-----------------------getFileInfoWithmarcom  Exception--------------> " + ex.Message, DateTime.Now);
                BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "<-----------------------getFileInfoWithmarcom  InnerException--------------> " + ex.InnerException, DateTime.Now);
                BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "<-----------------------getFileInfoWithmarcom  StackTrace--------------> " + ex.StackTrace, DateTime.Now);
                BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "<-----------------------getFileInfoWithmarcom  Source--------------> " + ex.Source, DateTime.Now);
                return fileinfo.ToString();
            }



            if ((imgResObj.ErrorDetail == null | object.ReferenceEquals(imgResObj.ErrorDetail, "")))
            {

                DAMFileAdditionalinfo damfile = new DAMFileAdditionalinfo();
                damfile.Height = (int)imgResObj.Height;
                damfile.Width = (int)imgResObj.Width;
                damfile.Dpi = (int)imgResObj.XResolution;

                string fileinfodet = JsonConvert.SerializeObject(damfile);


                ////fileinfo.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                //fileinfo.Append("<Fileinfo>");
                //fileinfo.Append("<Height>" + (int)imgResObj.Height + "</Height>");
                //fileinfo.Append("<Width>" + (int)imgResObj.Width + "</Width>");
                //fileinfo.Append("<Dpi>" + (int)imgResObj.XResolution + "</Dpi>");
                //fileinfo.Append("</Fileinfo>");
                //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "<-----------------------getFileInfoWithmarcom fileinfo values  --------------> " + fileinfodet, DateTime.Now);
                return fileinfodet;

            }
            else
            {
                //BrandSystems.Marcom.Core.Utility.ErrorLog.CustomErrorRoutine(false, "<-----------------------getFileInfoWithmarcom  --------------> " + fileinfo, DateTime.Now);
                return fileinfo.ToString();
            }
            //}
            //catch (MHValidationFailException ex)
            //{
            //    //ErrDescription = GetErrorDetails(ex.ErrorCode());
            //}
            //catch (Exception ex)
            //{
            //    ErrDescription = ex.Message;
            //    return false;
            //}

            //return true;
        }

      

     

        private string GetServerPath(string strServerName, string strPath)
        {
            return "\\\\" + strServerName + "\\" + strPath.Replace(":", "$");
            //Return strPath
        }
    



        public bool FileIsGraphic(string strFileName)
        {
            strFileName = strFileName.ToLower();
            if (strFileName.EndsWith("gif") | strFileName.EndsWith("jpg") | strFileName.EndsWith("jpeg") | strFileName.EndsWith("eps") | strFileName.EndsWith("tif") | strFileName.EndsWith("tiff") | strFileName.EndsWith("bmp") | strFileName.EndsWith("png") | strFileName.EndsWith("psd") | strFileName.EndsWith("emf"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public decimal GetFileSize(string strFileName)
        {
            int intSafetyCounter = 0;
            decimal decReturn = default(decimal);
            while (!File.Exists(strFileName) & intSafetyCounter < 50)
            {
                System.Threading.Thread.Sleep(100);
                intSafetyCounter += 1;
            }
            if (File.Exists(strFileName))
            {
                FileInfo my_fileInfo = new FileInfo(strFileName);
                decReturn = my_fileInfo.Length;
            }
            else
            {
                decReturn = 0;
            }

            return decReturn;
        }

        private object GetErrorDetails(int intErrorCode)
        {
            string strErrordetail = ""; //objContent.GetText("miscErrcode" + intErrorCode);

            return strErrordetail;
        }

      

        public static string ReadS3TenantBasePath(int TenantID, int identity = 0)
        {
            TenantSelection tfp = new TenantSelection();
            string TenantFilePath = tfp.GetTenantFilePath(TenantID);
            if (identity != 0)
            {
                if ((int)AWSFilePathIdentity.DamOriginal == identity)
                    return (TenantFilePath + @"DAMFiles\Original\");
                else if ((int)AWSFilePathIdentity.DAMPreview == identity)
                    return (TenantFilePath + @"DAMFiles\Preview\");
                else return TenantFilePath;
            }
            else return TenantFilePath;
        }

        public MediaHandlerResponseObject ExecuteSynchronousTaskwithmarcom(MediaHandlerRequestObject requestObject)
        {

            BrandSystems.Marcom.Core.Utility.MediaHandler.ImageMagickProcessor mhimage = new BrandSystems.Marcom.Core.Utility.MediaHandler.ImageMagickProcessor(ConfigurationManager.AppSettings["ImageMagickPath"].ToString(), ConfigurationManager.AppSettings["ImageMagickPath"].ToString());
            try
            {
                if (MimeType == null)
                {
                    MimeType = this.GetMimeType();
                }

                //Validate(requestObject);

                switch (requestObject.GetType().Name)
                {
                    //needtoworke
                    case "ImageRequestObject":
                        if ((((ImageRequestObject)requestObject).TaskToPerform == BrandSystems.MediaHandler.CustomType.Task.Convert))
                        {
                            string sExt = System.IO.Path.GetExtension(((ImageRequestObject)requestObject).SourcePath).Replace(".", "").ToUpper();
                            string dExt = System.IO.Path.GetExtension(((ImageRequestObject)requestObject).DestinationPath).Replace(".", "").ToUpper();

                            if (!(SearchExt(MimeType, sExt) & SearchExt(MimeType, dExt)))
                            {
                                //throw new MediaHandler.MHValidationFailException("Media Handler Can't Handle provided File Format", null, null, requestObject.JobID, 25);
                                throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("Media Handler Can't Handle provided File Format", null, "", requestObject.JobID, 25);
                            }

                        }
                        else
                        {
                            string sExt = System.IO.Path.GetExtension(((ImageRequestObject)requestObject).SourcePath).Replace(".", "").ToUpper();

                            if (!(SearchExt(MimeType, sExt)))
                            {
                                //throw new MediaHandler.MHValidationFailException("Media Handler Can't Handle provided File Format", null, null, requestObject.JobID, 25);
                                throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("Media Handler Can't Handle provided File Format", null, " ", requestObject.JobID, 25);
                            }
                        }

                        MediaHandlerResponseObject objMHResObject1 = mhimage.ExecuteTask((ImageRequestObject)requestObject);


                        if ((objMHResObject1.ErrorDetail != null))
                        {
                            objMHResObject1.AditionalInfo = requestObject.AditionalInfo;
                            objMHResObject1.ErrorDetail = "Error Code : 26 Error Msg : " + objMHResObject1.ErrorDetail;
                        }


                        return objMHResObject1;

                                      
                   
                }



            }
            catch (MHValidationFailException MHex)
            {
                //Throw New System.ApplicationException("Validation Fail : " & MHex.Message & " For Job Id: " & MHex.JobID.ToString())
                throw MHex;
            }
            catch (Exception ex)
            {
                //Select Case requestObject.GetType().Name
                //    Case "ImageRequestObject"
                //        Dim objImageResObj As New ImageResponseObject
                //        objImageResObj.AditionalInfo = requestObject.AditionalInfo
                //        objImageResObj.JobID = requestObject.JobID
                //        objImageResObj.ErrorDetail = ex.Message
                //        Return objImageResObj
                //    Case "PDFRequestObject"

                //    Case "PPTRequestObject"

                //    Case "VideoRequestObject"

                //    Case Else
                //End Select
                if (ex.Message == "The operation has timed-out.")
                {
                    throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("Timeout", null, "", requestObject.JobID, 27);
                }
                else
                {
                    throw new BrandSystems.Marcom.Core.Utility.MediaHandler.MHValidationFailException("MediaHandler Internal Exception", ex, null, requestObject.JobID, 26);
                }
            }



            return null;
        }

        private bool SearchExt(string[] MimeTypeList, string Ext)
        {

            bool Status = false;

            foreach (string Item in MimeTypeList)
            {
                if (Item == Ext)
                {
                    Status = true;
                    break; // TODO: might not be correct. Was : Exit For
                }
            }
            return Status;
        }

        public string[] GetMimeType()
        {
            try
            {
                IList filesinfo = new List<string>();
                string xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"], "MediaHandlerSettings.xml");
                XDocument MHXmlDoc = XDocument.Load(xmlpath);
              
                var Croplist = MHXmlDoc.Descendants("MHSetting").Descendants("CropFileFormat").Descendants("Ext").Select(a => a.Value).ToList();

                filesinfo = Croplist.ToList();
                string[] strings = new string[filesinfo.Count];
                filesinfo.CopyTo(strings, 0);

                return strings;
            }
            catch (Exception)
            {
                return null;
            }

        }
    }

    #region "CLASS - ITEMREQUEST"
    [Serializable()]
    public class ItemRequest
    {

        #region "PRIVATE VARIABLES"
        ///Private m_intArchiveRefId As Integer 'commented as property type is changed to array from integer. by hashmat 02/Aug/2010
        ///Private m_intItemRefId As Integer    'commented as property type is changed to array from integer. by hashmat 02/Aug/2010
        //Added to change the property type. by hashmat 02/Aug/2010
        private int[] m_intArchiveRefId;
        //Added to change the property type. by hashmat 02/Aug/2010
        private int[] m_intItemRefId;
        private int m_intFormatRefId;
        private int m_intDimensionRefId;
        private int m_intTopLeftRadius;
        private int m_intTopRightRadius;
        private int m_intBottomLeftRadius;
        private int m_intBottomRightRadius;
        private string m_strColorspace;
        private string m_strDirection;
        private string m_strUnits;
        private string m_strCornerColor;
        private int m_intDegree;
        private int m_intQualityPercentage;
        private double m_dblCropTop;
        private double m_dblScaleInPercentage;
        private double m_dblResizeInPercentage;
        private double m_dblCropLeft;
        private double m_dblCropHeight;
        private double m_dblCropWidth;
        private double m_dblResizeHeight;
        private double m_dblResizeWidth;
        private double m_dblXResolutionDPI;
        private double m_dblYResolutionDPI;
        private double m_dblScaleHeight;
        private double m_dblScaleWidth;
        private string m_strImageType;
        #endregion
        private string m_strFormat;
        public Direction DirectionObj;
        public Units UnitsObj;
        #region "PROPERTIES"

        public enum Direction
        {
            Null,
            Horizontal,
            Vertical
        }

        public enum Units
        {
            Pixels,
            Inches,
            Millimeters
            //Centimeters
        }

        ///Public Property ArchiveRefId() As Integer
        public int[] ArchiveRefIds
        {
            get { return m_intArchiveRefId; }
            ///changed from ByVal Value As Integer() 
            set { m_intArchiveRefId = value; }
        }

        ///Public Property ItemRefId() As Integer
        public int[] ItemRefIds
        {
            get { return m_intItemRefId; }
            /// changed from Set(ByVal Value As Integer)
            set { m_intItemRefId = value; }
        }

        public double CropTop
        {
            get { return m_dblCropTop; }
            set { m_dblCropTop = value; }
        }

        public double CropLeft
        {
            get { return m_dblCropLeft; }
            set { m_dblCropLeft = value; }
        }

        public double CropWidth
        {
            get { return m_dblCropWidth; }
            set { m_dblCropWidth = value; }
        }

        public double ResizeHeight
        {
            get { return m_dblResizeHeight; }
            set { m_dblResizeHeight = value; }
        }

        public double ResizeWidth
        {
            get { return m_dblResizeWidth; }
            set { m_dblResizeWidth = value; }
        }

        public double CropHeight
        {
            get { return m_dblCropHeight; }
            set { m_dblCropHeight = value; }
        }

        public double ResizeInPercentage
        {
            get { return m_dblResizeInPercentage; }
            set { m_dblResizeInPercentage = value; }
        }

        public string ImageType
        {
            get { return m_strImageType; }
            set { m_strImageType = value; }
        }

        public int QualityPercentage
        {
            get { return m_intQualityPercentage; }
            set { m_intQualityPercentage = value; }
        }

        public double ScaleInPercentage
        {
            get { return m_dblScaleInPercentage; }
            set { m_dblScaleInPercentage = value; }
        }

        public string Format
        {
            get { return m_strFormat; }
            set { m_strFormat = value; }
        }

        public string Colorspace
        {
            get { return m_strColorspace; }
            set { m_strColorspace = value; }
        }

        public double ScaleHeight
        {
            get { return m_dblScaleHeight; }
            set { m_dblScaleHeight = value; }
        }

        public double ScaleWidth
        {
            get { return m_dblScaleWidth; }
            set { m_dblScaleWidth = value; }
        }

        public double XResolutionDPI
        {
            get { return m_dblXResolutionDPI; }
            set { m_dblXResolutionDPI = value; }
        }

        public double YResolutionDPI
        {
            get { return m_dblYResolutionDPI; }
            set { m_dblYResolutionDPI = value; }
        }

        public Units Unit
        {
            get { return UnitsObj; }
            set { UnitsObj = value; }
            //get {  return (Units)Enum.Parse(typeof(Units), m_strUnits); }
            //set
            //{
            //   // m_strUnits = (Units)Enum.Parse(typeof(Units).GetType(), value.ToString());
            // }
        }


        public int RotationAngle
        {
            get { return m_intDegree; }
            set { m_intDegree = value; }
        }

        public Direction FlipDirection
        {
            get { return DirectionObj; }
            set { DirectionObj = value; }
            //get { return (Direction)Enum.Parse(typeof(Direction), m_strDirection); }
            //set { m_strDirection = value; }//(BrandSystems.Marcom.Core.Utility.MediaHandler.ItemRequest.Direction)(value); }
        }

        public int FormatRefId
        {
            get { return m_intFormatRefId; }
            set { m_intFormatRefId = value; }
        }

        public int DimensionRefId
        {
            get { return m_intDimensionRefId; }
            set { m_intDimensionRefId = value; }
        }
        /// Following are the properties added by Syed on 09-Sep-2010 as Media Handler version changed to V2.3.3 
        public int TopLeftRadius
        {
            get { return m_intTopLeftRadius; }
            set { m_intTopLeftRadius = value; }
        }
        public int TopRightRadius
        {
            get { return m_intTopRightRadius; }
            set { m_intTopRightRadius = value; }
        }
        public int BottomLeftRadius
        {
            get { return m_intBottomLeftRadius; }
            set { m_intBottomLeftRadius = value; }
        }
        public int BottomRightRadius
        {
            get { return m_intBottomRightRadius; }
            set { m_intBottomRightRadius = value; }
        }
        public string CornerColor
        {
            get { return m_strCornerColor; }
            set { m_strCornerColor = value; }
        }
        /// Added by Syed ends here on 09-Sep-2010
        #endregion

        #region "FUNCTIONS AND METHODS"


        public ItemRequest()
        {
        }

        public void AssignUnit(string strUnitName, ref ItemRequest objItemRequest)
        {
            switch (strUnitName.ToLower())
            {
                case "px":
                    objItemRequest.Unit = Units.Pixels;
                    break;
                case "inches":
                    objItemRequest.Unit = Units.Inches;
                    break;
                case "mm":
                    objItemRequest.Unit = Units.Millimeters;
                    break;
            }
        }
        #endregion

    }
    #endregion

    #region "CLASS - MHValidationFailException"
    public class MHValidationFailException : Exception, ISerializable
    {

        public MHValidationFailException()
            : base()
        {
            // Add implementation.
        }

        public MHValidationFailException(string message)
            : base(message)
        {
            // Add implementation.
        }

        public MHValidationFailException(string message, Exception inner)
            : base(message, inner)
        {
            // Add implementation.
        }

        public MHValidationFailException(string message, Exception inner, string PropertyName, Guid JobID)
            : base(message, inner)
        {

            strPropertyName = PropertyName;
            gJobID = JobID;
            // Add implementation.
        }

        public MHValidationFailException(string message, Exception inner, string PropertyName, Guid JobID, int ErrorCode)
            : base(message, inner)
        {

            strPropertyName = PropertyName;
            gJobID = JobID;
            intErrorCode = ErrorCode;
            // Add implementation.
        }

        // This constructor is needed for serialization.
        protected MHValidationFailException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            // Add implementation.
        }


        private string strPropertyName;
        public string PropertyName
        {
            get { return strPropertyName; }
            set { strPropertyName = value; }
        }


        private Guid gJobID;
        public Guid JobID
        {
            get { return gJobID; }
            set { gJobID = value; }
        }

        private int intErrorCode;
        public int ErrorCode
        {
            get { return intErrorCode; }
            set { intErrorCode = value; }
        }

    }
    #endregion
}
