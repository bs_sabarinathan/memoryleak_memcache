﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{
    internal class CustomTabEntityTypeAcl : ICustomTabEntityTypeAcl
    {
        #region Member Variables

	protected int _id;
        protected int _customtabid;
        protected int _entitytypeid;
        protected int _globalroleid;
		
		#endregion
		
		#region Constructors
		public CustomTabEntityTypeAcl() {}

        public CustomTabEntityTypeAcl(int pId, int pCutomtabid, int pEntitytypeId, int pGlobalroleId)
		{
			this._id = pId;
            this._customtabid = pCutomtabid;
            this._entitytypeid = pEntitytypeId;
            this._globalroleid = pGlobalroleId; 
		}
		
		#endregion
		
		#region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int CustomTabID
        {
            get { return _customtabid; }
            set {  _customtabid = value; }

        }

        public virtual int EntityTypeID
        {
            get { return _entitytypeid; }
            set { _entitytypeid = value; }
        }

        public virtual int GlobalRoleID
        {
            get { return _globalroleid; }
            set { _globalroleid = value; }
        }


		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            CustomTabEntityTypeAcl castObj = null;
			try
			{
                castObj = (CustomTabEntityTypeAcl)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				this._id.Equals( castObj.ID );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
    }
}
