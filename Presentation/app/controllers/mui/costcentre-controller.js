﻿(function (ng, app) {
    "use strict"; function muiplanningtoolcostcentreCtrl($scope) {
         $scope.$on("$destroy", function () { RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentreCtrl']")); });
    }
    app.controller("mui.planningtool.costcentreCtrl", ['$scope', muiplanningtoolcostcentreCtrl]);
})(angular, app);