﻿(function (ng, app) {
    "use strict";

    function muiDAMassetcreateCtrl($window, $location, $timeout, $scope, $cookies, $resource, $compile, $translate, AssetCreationService, $modalInstance, params) {
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath;
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            imagesrcpath = cloudpath;
        }
        $scope.DAMSettings = {};
        var timeoutvariable = {};
        var model;
        $scope.GetNonBusinessDaysforDatePicker();

        var previd = 0;
        $scope.Calanderopen = function ($event, Call_from, id) {
            $event.preventDefault();
            $event.stopPropagation();
            if (id != previd) {
                $scope[Call_from + previd] = false;
            }
            $scope[Call_from + id] = true;
            previd = id;
        }
        $scope.PeriodCalanderopen = function ($event, item, place) {
            $event.preventDefault();
            $event.stopPropagation();
            if (place == "start") {
                item.calstartopen = true;
                item.calendopen = false;
            } else if (place == "end") {
                item.calstartopen = false;
                item.calendopen = true;
            }
        };
        $scope.welcommsg = "hello came";
        $scope.DAMentitytypes = [];
        $scope.IsToShowDAMEntity = false;
        $scope.attachmentAddIsLock = true;
        $scope.uploadingInProgress = false;
        $scope.Dropdown = {};
        $scope.items = [];
        $scope.lastSubmit = [];
        $scope.PercentageVisibleSettings = {};
        $scope.DropDownTreePricing = {};
        $scope.treesrcdirec = {};
        $scope.treeNodeSelectedHolder = [];
        $scope.treePreviewObj = {};
        $scope.my_tree = tree = {};
        $scope.FileIdForAssetCategory = "";
        $scope.AssetCategory = {};
        $scope.listAttriToAttriResult = [];
        var AssetTypeMetadataDets = {
            assetType: [],
            Attributes: [],
            AssetCategoryTree: [],
            TreeStructure: []
        };
        $scope.fileinfofrAssetThumbnailpreview = {
            fileuniqueid: [],
            filedets: [],
            fileobj: []
        };
        $scope.UploadAttributeId = 0;
        $scope.ImageFileName = '';
        $scope.EnableDisableControlsHolder = {};
        var currentUploaderID = [];
        $scope.UploadImagefileAssetCreate = function (uplUniqID, attributeId) {
            $scope.UploadAttributeId = attributeId;
            currentUploaderID.push(uplUniqID);
            $("#pickfilesUploaderAttrAsset").click();

        }

        $scope.Browsecheck = function () {
            $("#pickassetsAlternate").click();
        }

        function StrartUpload_UploaderAttr() {
            //$('.moxie-shim').remove();
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                var uploader_Attr = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAttrAsset',
                    container: 'filescontainerooAsset',
                    max_file_size: '10000mb',
                    url: amazonURL + cloudsetup.BucketName,
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    urlstream_upload: true,
                    multiple_queues: true,
                    file_data_name: 'file',
                    multipart: true,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }
                });
                uploader_Attr.bind('Init', function (up, params) { });
                uploader_Attr.init();
                uploader_Attr.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_Attr.start();
                });
                uploader_Attr.bind('UploadProgress', function (up, file) { });
                uploader_Attr.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_Attr.bind('FileUploaded', function (up, file, response) {
                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    var tempUplID = currentUploaderID[0];
                    currentUploaderID.splice(0, 1);
                    SaveFileDetails(file, response.response, tempUplID);
                });
                uploader_Attr.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKey = (TenantFilePath + "UploadedImages/Temp/" + file.id + fileext).replace(/\\/g, "\/");
                    uploader_Attr.settings.multipart_params.key = uniqueKey;
                    uploader_Attr.settings.multipart_params.Filename = uniqueKey;
                });
            } else {
                //$('.moxie-shim').remove();
                var uploader_Attr = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAttrAsset',
                    container: 'filescontainerooAsset',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: 'Handlers/UploadUploaderImage.ashx?Type=Attachment',
                    chunk: '64Kb',
                    multi_selection: false,
                    multipart_params: {}

                });
                uploader_Attr.bind('Init', function (up, params) { });

                uploader_Attr.init();
                uploader_Attr.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_Attr.start();
                });
                uploader_Attr.bind('UploadProgress', function (up, file) { });
                uploader_Attr.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_Attr.bind('FileUploaded', function (up, file, response) {
                    var tempUplID = currentUploaderID[0];
                    currentUploaderID.splice(0, 1);
                    var fileinfo = response.response.split(",");
                    response.response = fileinfo[0].split(".")[0] + "," + GetMIMEType(fileinfo[0]) + "," + '.' + fileinfo[0].split(".")[1];
                    SaveFileDetails(file, response.response, tempUplID);
                });
                uploader_Attr.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
            }
        }

        $timeout(function () {
            StrartUpload_UploaderAttr();
        }, 100)
        var uploaderforasset = [];
        function SaveFileDetails(file, response, uplID) {

            var resultArr = response.split(",");
            var ImageFileName = resultArr[0] + resultArr[2];
            uploaderforasset.push({ assetID: uplID, uploaderAttr: ImageFileName, UploadAttributeId: $scope.UploadAttributeId });
            var PreviewID = "UploaderPreviewAssetCreation_" + $scope.UploadAttributeId;
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                $('#div_' + uplID + ' #' + PreviewID).attr('src', cloudpath + 'UploadedImages/Temp/' + ImageFileName);
            }
            else {
                $('#div_' + uplID + ' #' + PreviewID).attr('src', TenantFilePath + 'UploadedImages/Temp/' + ImageFileName);
            }
        }

        function TooltipattachmentName(description) {
            if (description != null & description != "") {
                return description;
            } else {
                return "<label class=\"align-center\">No description</label>";
            }
        };
        $scope.popMe = function (e) {
            var TargetControl = $(e.target);
            var mypage = TargetControl.attr('data-Name');
            if (!(mypage.indexOf("http") == 0)) {
                mypage = "http://" + mypage;
            }
            var myname = TargetControl.attr('data-Name');
            var w = 1200;
            var h = 800
            var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
            var win = window.open(mypage, myname, winprops)
        };

        function parseSize(size) {
            var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"],
				tier = 0;
            while (size >= 1024) {
                size = size / 1024;
                tier++;
            }
            return Math.round(size * 10) / 10 + " " + suffix[tier];
        }
        $scope.set_color = function (clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            };
            else return '';
        }
        $scope.dyn_Cont = '';
        $scope.DAMtypeswithExt = [];
        $scope.DamExt = [];
        $scope.Fileids = [];

        function GetAllExtensionTypesforDAM() {
            $scope.FileIdForAssetCategory = "";
            AssetCreationService.GetAllExtensionTypesforDAM().then(function (data) {
                $scope.DAMtypeswithExt = data.Response;
            });
        }

        function S3Settings() {
            AssetCreationService.S3Settings().then(function (data) {
                var res = JSON.parse(data.Response);
                $scope.DAMSettings.BucketName = res.BucketName;
                $scope.DAMSettings.AWSAccessKeyID = res.AWSAccessKeyID;
                $scope.DAMSettings.AWSSecretAccessKey = res.AWSSecretAccessKey;
                $scope.DAMSettings.RequestEndpoint = res.RequestEndpoint;
                $scope.DAMSettings.PolicyDocument = res.PolicyDocument;
                $scope.DAMSettings.PolicyDocumentSignature = res.PolicyDocumentSignature;
                $scope.StartAssetUpload();
            });
        }
        var filecount = 0;
        var totalfilecount = 0;
        $scope.fnTimeOutfrAssetCreation = function (option) {
            $scope.uploadingInProgress = false;
            filecount = 0;
            totalfilecount = 0;
            $scope.Fileids = [];
            $scope.fields = {
                usersID: ''
            };
            $scope.AttachProgress = false;
            if (option == 0) {
                $scope.saveoption = 0
                $scope.CreateAsset = false;
                $scope.Browse = true;
                $scope.Savecaption = $translate.instant('LanguageContents.Res_4979.Caption');
                $scope.Assetcaption = $translate.instant('LanguageContents.Res_4040.Caption');
                $scope.dragAsset = true;
            } else {
                $scope.saveoption = 1;
                $scope.CreateAsset = true;
                $scope.Browse = false;
                $scope.Savecaption = $translate.instant('LanguageContents.Res_15.Caption');
                $scope.Assetcaption = $translate.instant('LanguageContents.Res_4177.Caption');
                $scope.dragAsset = false;
            }
            if ($scope.Dam_Directory.length > 0 || $scope.processingsrcobj.processingplace == "task") {
                GetAllExtensionTypesforDAM();
                $('#uploadassets').css('pointer-events', 'auto');
                $('#uploadassets').removeAttr('disabled', 'disabled');
                $('#uploadassets').css('display', 'inline-block');
                $('#pickassetsAlternate').removeAttr('disabled', 'disabled');
                $('#pickassetsAlternate').css('pointer-events', 'auto');
                $('#pickassetsAlternate').css('display', 'inline-block');
                $('#createblankAsset').removeAttr('disabled', 'disabled');
                $('#createblankAsset').css('pointer-events', 'auto');
                $('#createblankAsset').css('display', 'inline-block');
                $('#assetlistActivity').empty();
                $('#dragActivityassets').show();
                timeoutvariable.UploaderTimeout = $timeout(function () {
                    $("#totalAssetActivityAttachProgress").html();
                    $("#totalAssetActivityAttachProgress").append('<span class="pull-left count">0 of 0 Uploaded</span><span class="size">0 B / 0 B</span>' + '<div class="progress progress-striped active">' + '<div style="width: 0%" class="bar"></div>' + '</div>' + '<div id="uploaded" style="position: absolute; top: 42%; right: -30%;"></div>');
                    if ($scope.enableassetcreationplUploader != true) {
                        $scope.enableassetcreationplUploader = true;
                        $scope.StartAssetUpload();
                    } else if ($scope.processingsrcobj.processingplace == "task" && $scope.task_enableassetcreationplUploader != true) {
                        $scope.task_enableassetcreationplUploader = true;
                        $scope.StartAssetUpload();
                    }
                }, 300);
            } else {
                timeoutvariable.cancelTimeout = $timeout(function () {
                    $modalInstance.dismiss('cancel');
                }, 100)
                bootbox.alert($translate.instant('LanguageContents.Res_4565.Caption'));
            }
        };

        function applyRemoteData(newFriends) {
            var friends = newFriends;
        }
        var TotalCount = 0;
        var fileUniqueID = '';
        var saveBlankAssetsCalled = false;
        $('.pickassets').mouseenter(function () {
            uploader.settings.browse_button = $(this).attr('id');
            uploader.refresh();
        });
        var uploader = '';
        $scope.StartAssetUpload = function () {
            if (uploader != '') {
                uploader.destroy();
            }
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                uploader = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickassetsAlternate',
                    drop_element: 'dragActivityassets',
                    container: 'assetscontainer',
                    max_file_size: '10000mb',
                    url: amazonURL + cloudsetup.BucketName,
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    urlstream_upload: true,
                    multiple_queues: true,
                    file_data_name: 'file',
                    multipart: true,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    },
                });
                uploader.unbindAll();
                uploader.bind('Init', function (up, params) { });
                uploader.bind('UploadProgress', function (up, file) {
                    var fileid = file.id.replace("_", "");
                    $('#' + fileid + " .bar").css("width", file.percent + "%");
                    $('#' + fileid + " .size").html(plupload.formatSize(Math.round(file.size * (file.percent / 100))) + ' / ' + plupload.formatSize(file.size));
                    $('#' + fileid).attr('data-size-uploaded', Math.round(file.size * (file.percent / 100)));
                    TotalAssetUploadProgress();
                });
                uploader.bind('Error', function (up, err) {
                    $('#assetlistActivity').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");


                    var ErrorLogInfo = {};
                    ErrorLogInfo.code = err.code;
                    ErrorLogInfo.message = err.message;
                    ErrorLogInfo.response = err.response;
                    ErrorLogInfo.guid = err.file.id;
                    ErrorLogInfo.fileName = err.file.name;

                    AssetCreationService.writeUploadErrorlog(ErrorLogInfo);

                    up.refresh();
                });
                uploader.bind('FileUploaded', function (up, file, response) {
                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;


                    $('#' + fileid).attr('data-status', 'true');
                    var ind = FileAttributes.FileID.indexOf(fileid);
                    var assetType = FileAttributes.AssetType[ind];
                    if (assetType == 0) {
                        FileAttributes.FileID.splice(ind, 1);
                        FileAttributes.AssetType.splice(ind, 1);
                        FileAttributes.AttributeDataList.splice(ind, 1);
                        ind = FileAttributes.FileID.indexOf(fileid);
                    }
                    var uplAttrcount = $.grep(FileAttributes.AttributeDataList[ind], function (e) { return e.AttributeTypeID == 11 });
                    for (var i = 0; i < uplAttrcount.length; i++) {
                        var indexUpl = FileAttributes.AttributeDataList[ind].indexOf(uplAttrcount[i]);
                        FileAttributes.AttributeDataList[ind][indexUpl].NodeId = '';
                    }
                    var uplAttrobjbyAssetID = $.grep(uploaderforasset, function (e) { return e.assetID == fileid });
                    if (uplAttrobjbyAssetID.length > 0) {
                        for (var i = 0; i < uplAttrcount.length; i++) {
                            var uplAttrObj = $.grep(FileAttributes.AttributeDataList[ind], function (e) { return e.AttributeID == uplAttrcount[i].AttributeID })[0];
                            var uplAttrIndex = FileAttributes.AttributeDataList[ind].indexOf(uplAttrObj);
                            var attrfileName = $.grep(uplAttrobjbyAssetID, function (x) { return x.UploadAttributeId == uplAttrcount[i].AttributeID });
                            FileAttributes.AttributeDataList[ind][uplAttrIndex].NodeId = '';
                            if (attrfileName.length > 0) {
                                FileAttributes.AttributeDataList[ind][uplAttrIndex].NodeId = attrfileName[0].uploaderAttr;
                            }

                        }


                    }
                    $scope.SaveAssetFileDetails(file, response.response, FileAttributes.AttributeDataList[ind]);
                    TotalAssetUploadProgress();
                });
                uploader.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKeypath = TenantFilePath.replace(/\\/g, "\/");
                    var uniqueKey = (uniqueKeypath + "DAMFiles/Original/" + file.id + fileext);
                    uploader.settings.multipart_params.key = uniqueKey;
                    uploader.settings.multipart_params.Filename = uniqueKey;
                });
                uploader.bind('FilesAdded', function (up, files) {
                    $scope.AttachProgress = true;
                    totalfilecount = totalfilecount + files.length;
                    createAssetHTML(files);
                    $('#dragActivityassets').hide();
                    up.refresh();
                    timeoutvariable.AssetRefreshTimeout = $timeout(function () {
                        TotalAssetUploadProgress();
                    }, 500);
                });
                uploader.init();
            } else {
                uploader = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickassetsAlternate',
                    drop_element: 'dragActivityassets',
                    container: 'assetscontainer',
                    max_file_size: '10000mb',
                    url: 'Handlers/UploadHandler.ashx?Type=Attachment',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    urlstream_upload: true,
                    multiple_queues: true,
                    file_data_name: 'file',
                    chunk_size: '4mb',
                    max_retries: 3,
                    multipart_params: {}
                });
                uploader.unbindAll();
                uploader.bind('Init', function (up, params) { });
                uploader.bind('UploadProgress', function (up, file) {
                    var fileid = file.id.replace("_", "");
                    $('#' + fileid + " .bar").css("width", file.percent + "%");
                    $('#' + fileid + " .size").html(plupload.formatSize(Math.round(file.size * (file.percent / 100))) + ' / ' + plupload.formatSize(file.size));
                    $('#' + fileid).attr('data-size-uploaded', Math.round(file.size * (file.percent / 100)));
                    TotalAssetUploadProgress();
                });
                uploader.bind('Error', function (up, err) {
                    $('#assetlistActivity').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                    up.refresh();
                });
                uploader.bind('FileUploaded', function (up, file, response) {
                    $('#' + file.id).attr('data-status', 'true');
                    var fileid = file.id.replace("_", "");
                    var ind = FileAttributes.FileID.indexOf(fileid);
                    var assetType = FileAttributes.AssetType[ind];
                    if (assetType == 0) {
                        FileAttributes.FileID.splice(ind, 1);
                        FileAttributes.AssetType.splice(ind, 1);
                        FileAttributes.AttributeDataList.splice(ind, 1);
                        ind = FileAttributes.FileID.indexOf(fileid);
                    }

                    var uplAttrcount = $.grep(FileAttributes.AttributeDataList[ind], function (e) { return e.AttributeTypeID == 11 });

                    for (var i = 0; i < uplAttrcount.length; i++) {
                        var indexUpl = FileAttributes.AttributeDataList[ind].indexOf(uplAttrcount[i]);
                        FileAttributes.AttributeDataList[ind][indexUpl].NodeId = '';
                    }

                    var uplAttrobjbyAssetID = $.grep(uploaderforasset, function (e) { return e.assetID == fileid });
                    if (uplAttrobjbyAssetID.length > 0) {
                        for (var i = 0; i < uplAttrcount.length; i++) {
                            var uplAttrObj = $.grep(FileAttributes.AttributeDataList[ind], function (e) { return e.AttributeID == uplAttrcount[i].AttributeID })[0];
                            var uplAttrIndex = FileAttributes.AttributeDataList[ind].indexOf(uplAttrObj);
                            var attrfileName = $.grep(uplAttrobjbyAssetID, function (x) { return x.UploadAttributeId == uplAttrcount[i].AttributeID });

                            FileAttributes.AttributeDataList[ind][uplAttrIndex].NodeId = '';
                            if (attrfileName.length > 0) {
                                FileAttributes.AttributeDataList[ind][uplAttrIndex].NodeId = attrfileName[0].uploaderAttr;
                            }
                        }
                    }
                    $scope.SaveAssetFileDetails(file, response.response, FileAttributes.AttributeDataList[ind]);
                    TotalAssetUploadProgress();
                });
                uploader.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
                uploader.bind('FilesAdded', function (up, files) {
                    $scope.AttachProgress = true;
                    totalfilecount = totalfilecount + files.length;
                    createAssetHTML(files);
                    $('#dragActivityassets').hide();
                    up.refresh();
                    timeoutvariable.AssetRefreshTimeout2 = $timeout(function () {
                        TotalAssetUploadProgress();
                    }, 500);
                });
                uploader.init();
            }
            var dom = {
                uploader: $("#uploader"),
                uploads: $("ul.uploads")
            };
            $('#uploadassets').click(function (e) {
                if ($scope.UniqueidsfrBlankAsset.length + $scope.Fileids.length > 0) {
                    var count = 0;
                    var namecount = 0;
                    angular.forEach($scope.UniqueidsfrBlankAsset, function (key) {
                        if ($scope.fields['assettype_' + key] == '' || $scope.fields['assettype_' + key] == null || $scope.fields['assettype_' + key] == undefined) {
                            count = count + 1;
                        }
                        if ($scope.fields['assetname_' + key] == '' || $scope.fields['assetname_' + key] == null || $scope.fields['assetname_' + key] == undefined) {
                            namecount = namecount + 1;
                        }
                    });
                    angular.forEach($scope.Fileids, function (key) {
                        if ($scope.fields['assettype_' + key] == '' || $scope.fields['assettype_' + key] == null || $scope.fields['assettype_' + key] == undefined) {
                            count = count + 1;
                        }
                        if ($scope.fields['assetname_' + key] == '' || $scope.fields['assetname_' + key] == null || $scope.fields['assetname_' + key] == undefined) {
                            namecount = namecount + 1;
                        }
                    });
                    if (count == 0 && namecount == 0) {
                        $('#uploadassets').attr('disabled', 'disabled');
                        $('#uploadassets').css('pointer-events', 'none');
                        $('#clearAssetUploader').css('pointer-events', 'none');
                        $('#clearAssetUploader').attr('disabled', 'disabled');
                        $('#pickassetsAlternate').attr('disabled', 'disabled');
                        $('#pickassetsAlternate').css('pointer-events', 'none');
                        $('#createblankAsset').attr('disabled', 'disabled');
                        $('#createblankAsset').css('pointer-events', 'none');
                        if ($scope.UniqueidsfrBlankAsset.length > 0) {
                            if (saveBlankAssetsCalled == false) {
                                saveBlankAssetsCalled = true;
                                saveBlankAssets();
                            }
                        }
                        uploader.start();
                        e.preventDefault();
                        $scope.uploadingInProgress = true;
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_4594.Caption'));
                        $scope.uploadingInProgress = false;
                        return false;
                    }
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_4563.Caption'));
                    $scope.uploadingInProgress = false;
                    return false;
                }
            });
            $('#clearAssetUploader').click(function (e) {
                $('.moxie-shim').remove();
                timeoutvariable.uploadTimeout = $timeout(function () {
                    $('#uploadassets').css('pointer-events', 'auto');
                    $('#uploadassets').removeAttr('disabled', 'disabled');
                    $('#uploadassets').css('display', 'inline-block');
                    $('#pickassetsAlternate').removeAttr('disabled', 'disabled');
                    $('#pickassetsAlternate').css('pointer-events', 'auto');
                    $('#pickassetsAlternate').css('display', 'inline-block');
                    $('#createblankAsset').removeAttr('disabled', 'disabled');
                    $('#createblankAsset').css('pointer-events', 'auto');
                    $('#createblankAsset').css('display', 'inline-block');
                    $scope.Fileids = [];
                    $scope.UniqueidsfrBlankAsset = [];
                }, 100);
            });
            $('#pickassets').each(function () {
                var input = new mOxie.FileInput({
                    browse_button: this,
                    multiple: true
                });
                input.onchange = function (event) {
                    uploader.addFile(input.files);
                };
                input.init();
            });
            $('#assetlistActivity').on('click', '.removefile', function () {
                var fileToRemove = $(this).attr('data-fileid');
                var ind = $scope.UniqueidsfrBlankAsset.indexOf(fileToRemove);
                if (ind != -1) {
                    $('#' + fileToRemove).remove();
                    $scope.UniqueidsfrBlankAsset.splice(ind, 1);
                    if (totalfilecount > 0) totalfilecount = totalfilecount - 1;
                    TotalCount = TotalCount - 1;
                    $('#totalAssetActivityAttachProgress .count').html(0 + ' of ' + $('#assetlistActivity .attachmentBox').length + ' Uploaded');
                } else {
                    $.each(uploader.files, function (i, file) {
                        var fileid = file.id.replace("_", "");
                        if (fileid == fileToRemove) {
                            uploader.removeFile(file);
                            $('#' + fileid).remove();
                            var ind1 = $scope.Fileids.indexOf(fileid);
                            $scope.Fileids.splice(ind1, 1);
                            if (totalfilecount > 0) totalfilecount = totalfilecount - 1;
                            TotalCount = TotalCount - 1;
                            $('#totalAssetActivityAttachProgress .count').html(0 + ' of ' + $('#assetlistActivity .attachmentBox').length + ' Uploaded');
                        }
                    });
                }
                if ($('#assetlistActivity .attachmentBox').length == 0) {
                    $scope.AttachProgress = false;
                    $('#dragActivityassets').show();
                }
                TotalAssetUploadProgress();
            });
        }

        function createAssetHTML(files) {
            var descHtml = '';
            var damid = 0;
            var file = files[0];
            fileUniqueID = file.id.replace("_", "");
            $scope.Fileids.push(fileUniqueID);
            var ste = file.name.split('.')[file.name.split('.').length - 1];
            if (ste != undefined) ste = ste.toLowerCase();
            var damCount = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                return e.damExtention == ste
            }).length;
            if (damCount > 0) {
                var selecteddamtype = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                    return e.damExtention == ste
                });
            } else {
                var wildcardExt = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                    return e.damExtention.indexOf("*") != -1
                });
                var iswildcard = [];
                iswildcard = $.grep(wildcardExt, function (e) {
                    return e.damExtention.substring(0, e.damExtention.length - 1).indexOf(ste) != -1
                });
                if (iswildcard.length > 0) var selecteddamtype = iswildcard;
            }
            $scope.AllDamTypes = [];
            $scope.AssetCategory["treestructure_" + fileUniqueID] = '-';
            $scope.AssetCategory["AssetTypesList_" + fileUniqueID] = [];
            $scope.fields["assetname_" + fileUniqueID] = file.name;
            $scope.fields["tagAllOptions_" + fileUniqueID] = {
                multiple: false,
                allowClear: true,
                data: $scope.AssetCategory["AssetTypesList_" + fileUniqueID],
                formatResult: $scope.formatResult,
                formatSelection: $scope.formatSelection,
                dropdownCssClass: "bigdrop",
                escapeMarkup: function (m) {
                    return m;
                }
            };
            if (selecteddamtype != undefined && selecteddamtype != null) {
                damid = selecteddamtype[0].Id;
                var ownerID = parseInt($scope.OwnerID, 10);
                var tempval = [];
                var tempassty = "";
                for (var k = 0; k < selecteddamtype.length; k++) {
                    tempassty = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                        return e.id == selecteddamtype[k].Id
                    });
                    if (tempassty != undefined && tempassty.length > 0) var tempattr = $.grep(tempval, function (e) {
                        return e.id == tempassty[0].id
                    });
                    if (tempattr.length == 0) tempval.push(tempassty[0]);
                }
                if (tempval.length > 0) {
                    $scope.AssetCategory["AssetTypesList_" + fileUniqueID] = tempval;
                    $scope.fields["tagAllOptions_" + fileUniqueID].data = $scope.AssetCategory["AssetTypesList_" + fileUniqueID];
                } else { }
                var dataid = '';
                var rec = [];
                rec = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                    return e.id == selecteddamtype[0].Id
                });
                if (rec.length > 0) dataid = rec[0].id;
                $scope.fields["assettype_" + fileUniqueID] = rec[0];
                $scope.oldfiledetails["AssetType_" + fileUniqueID] = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                    return e.id == selecteddamtype[0].Id
                })[0].id;
                if ($scope.AssetCategory["AssetTypesList_" + fileUniqueID].length == 1) {
                    dataid = (dataid == undefined ? 0 : dataid);
                    AssetCreationService.GetAssetCategoryTree(dataid).then(function (data) {
                        if (data.Response != null) {
                            $scope.AssetCategory["AssetCategoryTree_" + fileUniqueID] = JSON.parse(data.Response);
                            $scope.AssetCategory["filterval_" + fileUniqueID] = '';
                            var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + fileUniqueID + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + fileUniqueID + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                            $("#dynamicAssetTypeTree").html($compile(htmltree)($scope));
                            createMetadataHTML(dataid, file, fileUniqueID, ste, files);
                        }
                    });
                    AssetCreationService.GetAssetCategoryPathInAssetEdit(dataid).then(function (datapath) {
                        if (datapath.Response != null) {
                            if (datapath.Response[1].AssetTypeIDs.length > 0) {
                                $scope.AssetCategory["treestructure_" + fileUniqueID] = datapath.Response[0].CategoryPath;
                            } else $scope.fields["assettype_" + fileUniqueID] = '';
                        }
                    });
                } else {
                    $scope.fields["assettype_" + fileUniqueID] = '';
                    $scope.oldfiledetails["AssetType_" + fileUniqueID] = '';
                    AssetCreationService.GetAssetCategoryTree(0).then(function (data) {
                        if (data.Response != null) {
                            $scope.AssetCategory["AssetCategoryTree_" + fileUniqueID] = JSON.parse(data.Response);
                            $scope.AssetCategory["filterval_" + fileUniqueID] = '';
                            var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + fileUniqueID + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + fileUniqueID + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                            $("#dynamicAssetTypeTree").html($compile(htmltree)($scope));
                            createMetadataHTML(0, file, fileUniqueID, ste, files);
                        }
                    });
                }
            } else {
                $scope.fields["assettype_" + fileUniqueID] = '';
                $scope.oldfiledetails["AssetType_" + fileUniqueID] = '';
                AssetCreationService.GetAssetCategoryTree(0).then(function (data) {
                    if (data.Response != null) {
                        $scope.AssetCategory["AssetCategoryTree_" + fileUniqueID] = JSON.parse(data.Response);
                        $scope.AssetCategory["filterval_" + fileUniqueID] = '';
                        var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + fileUniqueID + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + fileUniqueID + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                        $("#dynamicAssetTypeTree").html($compile(htmltree)($scope));
                        createMetadataHTML(0, file, fileUniqueID, ste, files);
                    }
                });
            }
        }

        function createMetadataHTML(damid, file, fileUniqueID, ste, files) {
            var indofAssetType = AssetTypeMetadataDets.assetType.indexOf(damid);
            if (indofAssetType == -1) {
                AssetCreationService.GetDamAttributeRelation(damid).then(function (attrreldata) {
                    AssetTypeMetadataDets.assetType.push(damid);
                    AssetTypeMetadataDets.Attributes.push(attrreldata.Response);
                    $scope.dyn_Cont = "";
                    if (attrreldata.Response != undefined && attrreldata.Response != null) {
                        $scope.DAMattributesRelationList = attrreldata.Response;
                        FileAttributes.FileID.push(fileUniqueID);
                        FileAttributes.AttributeDataList.push($scope.DAMattributesRelationList);
                        FileAttributes.AssetType.push(damid);
                        if ($scope.AssetCategory["AssetTypesList_" + fileUniqueID].length == 1) {
                            $scope.dyn_Cont += generateHTMLforAttributes($scope.DAMattributesRelationList, fileUniqueID);
                        }
                    }
                    $('#assetlistActivity').append($compile('<div id="' + fileUniqueID + '" class="attachmentBox" Data-role="Attachment" data-size="' + file.size + '" data-size-uploaded="0" data-status="false">' + '<div class="row-fluid">' + '<div class="span12">' + '<div class="span2 leftSection">' + ' <i id="icon_ok' + fileUniqueID + '" class="icon-ok-sign" style="display:none;"  ></i>' + ' <div class="fileType-Preview">' + ' <div id="Preview_' + fileUniqueID + '" class="fileType-PreviewDiv">' + ' </div>' + '</div>' + ' <div class="progress progress-striped active">' + ' <div style="width: 0%" class="bar"></div>' + '</div>' + ' <div class="progressInfo">' + '  <span class="progress-percent"></span>' + '<span class="fileSize">' + plupload.formatSize(file.size) + '' + '</span>' + '</div>' + '</div>' + ' <div class="span10 rightSection">' + '<div class="info">' + '  <span class="name">' + ' <i class="icon-file-alt"></i>' + file.name + '</span>' + ' <span class="size">' + ' <i class="icon-remove removefile" data-fileid="' + fileUniqueID + '"></i>' + ' </span>' + '</div>' + '<div  class="formInfo">' + ' <form class="form-float">' + '<div class=\"control-group fullSpace\">' + '<label class=\"control-label\"> Select asset type </label>' + '<div class=\"controls\">' + '<a ng-click="AssetTypeTreePopUp(\'' + fileUniqueID + '\')">{{AssetCategory.treestructure_' + fileUniqueID + '}}</a>' + '</div>' + '<div class=\"controls\">' + '<input ui-select2="fields.tagAllOptions_' + fileUniqueID + '"  type="hidden"  data-ng-model="fields.assettype_' + fileUniqueID + '" ng-change="onDamtypechange(\'' + fileUniqueID + '\')" > </input>' + '<span class="imgTypeSection">' + '<span class=\"imgTypeEditIcon\" ng-click="AssetTypeTreePopUp(\'' + fileUniqueID + '\')">' + '<img src="assets/img/treeIcon.png" />' + '</span>' + '</span>' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Asset name </label>' + '<div class=\"controls\">' + '<input class=\"txtbx\" type="text"  data-ng-model="fields.assetname_' + fileUniqueID + '" name="DescVal_' + fileUniqueID + '" id="desc_' + fileUniqueID + '" placeholder="Name">' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Description </label>' + '<div class=\"controls\">' + '<textarea class=\"small-textarea\"  name=\"fields.TextMultiLine_' + fileUniqueID + SystemDefiendAttributes.Description + '\" ng-model=\"fields.TextMultiLine_' + fileUniqueID + SystemDefiendAttributes.Description + '\" id=\"TextMultiLine_' + fileUniqueID + SystemDefiendAttributes.Description + '\" placeholder="Description" rows="3"></textarea>' + '</div>' + '</div>' + '<div id = "div_' + fileUniqueID + '">' + $scope.dyn_Cont + '</div>' + '</form>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>')($scope));
                    generatePreview(fileUniqueID, ste, file);
                    TotalAssetUploadProgress();
                    var ind = files.indexOf(files[0]);
                    files.splice(ind, 1);
                    if (files.length > 0) {
                        createAssetHTML(files);
                    }
                });
            } else {
                var metadata = AssetTypeMetadataDets.Attributes[indofAssetType];
                $scope.dyn_Cont = "";
                $scope.DAMattributesRelationList = metadata;
                FileAttributes.FileID.push(fileUniqueID);
                FileAttributes.AttributeDataList.push($scope.DAMattributesRelationList);
                FileAttributes.AssetType.push(damid);
                if ($scope.AssetCategory["AssetTypesList_" + fileUniqueID].length == 1) {
                    $scope.dyn_Cont += generateHTMLforAttributes($scope.DAMattributesRelationList, fileUniqueID);
                }
                if ($scope.AssetCategory['treestructure_' + fileUniqueID] == '-') {
                    $scope.AssetCategory['showcategroytext_' + fileUniqueID] = false;
                } else {
                    $scope.AssetCategory['showcategroytext_' + fileUniqueID] = true;
                }
                $('#assetlistActivity').append($compile('<div id="' + fileUniqueID + '" class="attachmentBox" Data-role="Attachment" data-size="' + file.size + '" data-size-uploaded="0" data-status="false">' + '<div class="row-fluid">' + '<div class="span12">' + '<div class="span2 leftSection">' + ' <i id="icon_ok' + fileUniqueID + '" class="icon-ok-sign" style="display:none;"  ></i>' + ' <div class="fileType-Preview">' + ' <div id="Preview_' + fileUniqueID + '" class="fileType-PreviewDiv">' + ' </div>' + '</div>' + ' <div class="progress progress-striped active">' + ' <div style="width: 0%" class="bar"></div>' + '</div>' + ' <div class="progressInfo">' + '  <span class="progress-percent"></span>' + '<span class="fileSize">' + plupload.formatSize(file.size) + '' + '</span>' + '</div>' + '</div>' + ' <div class="span10 rightSection">' + '<div class="info">' + '  <span class="name">' + ' <i class="icon-file-alt"></i>' + file.name + '</span>' + ' <span class="size">' + ' <i class="icon-remove removefile" data-fileid="' + fileUniqueID + '"></i>' + ' </span>' + '</div>' + '<div  class="formInfo">' + ' <form class="form-float">' + '<div class=\"control-group fullSpace\">' + '<label class=\"control-label\"> Select asset type </label>' + '<div class=\"controls\">' + '<a ng-click="AssetTypeTreePopUp(\'' + fileUniqueID + '\')">{{AssetCategory.treestructure_' + fileUniqueID + '}}</a>' + '</div>' + '<div class=\"controls\">' + '<input ui-select2="fields.tagAllOptions_' + fileUniqueID + '"  type="hidden"  data-ng-model="fields.assettype_' + fileUniqueID + '" ng-change="onDamtypechange(\'' + fileUniqueID + '\')" > </input>' + '<span class="imgTypeSection">' + '<span class=\"imgTypeEditIcon\" ng-click="AssetTypeTreePopUp(\'' + fileUniqueID + '\')">' + '<img src="assets/img/treeIcon.png"' + '</span>' + '</span>' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Asset name </label>' + '<div class=\"controls\">' + '<input class=\"txtbx\" type="text"  data-ng-model="fields.assetname_' + fileUniqueID + '" name="DescVal_' + fileUniqueID + '" id="desc_' + fileUniqueID + '" placeholder="Name">' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Description </label>' + '<div class=\"controls\">' + '<textarea class=\"small-textarea\"  name=\"fields.TextMultiLine_' + fileUniqueID + SystemDefiendAttributes.Description + '\" ng-model=\"fields.TextMultiLine_' + fileUniqueID + SystemDefiendAttributes.Description + '\" id=\"TextMultiLine_' + fileUniqueID + SystemDefiendAttributes.Description + '\" placeholder="Description" rows="3"></textarea>' + '</div>' + '</div>' + '<div id = "div_' + fileUniqueID + '">' + $scope.dyn_Cont + '</div>' + '</form>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>')($scope));
                generatePreview(fileUniqueID, ste, file);
                TotalAssetUploadProgress();
                var ind = files.indexOf(files[0]);
                files.splice(ind, 1);
                if (files.length > 0) {
                    createAssetHTML(files);
                }
            }
        }

        function TotalAssetUploadProgress() {
            var TotalSize = 0;
            var UploadedSize = 0;
            var UploadedCount = 0;
            $('#assetlistActivity .attachmentBox').each(function () {
                TotalSize += parseInt($(this).attr('data-size'));
                UploadedSize += parseInt($(this).attr('data-size-uploaded'));
                if ($(this).attr('data-status') == 'true') {
                    UploadedCount += 1;
                }
            });
            $('#totalAssetActivityAttachProgress .count').html(UploadedCount + ' of ' + $('#assetlistActivity .attachmentBox').length + ' Uploaded');
            $('#totalAssetActivityAttachProgress .size').html(plupload.formatSize(UploadedSize) + ' / ' + plupload.formatSize(TotalSize));
            $('#totalAssetActivityAttachProgress .bar').css("width", Math.round(((UploadedSize / TotalSize) * 100)) + "%");
            if ((UploadedCount == $('#assetlistActivity .attachmentBox').length) && UploadedCount > 0 && $('#assetlistActivity .attachmentBox').length > 0) {
                timeoutvariable.UploadassetTimeout = $timeout(function () {
                    $('#totalAssetActivityAttachProgress #uploaded').html('Upload Successful.');
                    $scope.uploadingInProgress = false;
                }, 100);
            }
        }

        function generatePreview(fileuniqueID, ste, file) {
            cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
            imagesrcpath = TenantFilePath;
            if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                imagesrcpath = cloudpath;
            }
            if (o.Mime.mimes[ste.toLowerCase()] == undefined) {
                var img = $('<img id="' + fileuniqueID + '">');
                img.attr('src', imagesrcpath + "DAMFiles/StaticPreview_small/" + ste.toUpperCase() + ".jpg?" + generateUniqueTracker());
                img.attr("onerror", "this.onerror=null; this.src='" + imagesrcpath + "DAMFiles/Preview/NoPreview.jpg?'" + generateUniqueTracker() + "'';");
                img.appendTo('#Preview_' + fileuniqueID);
            } else {
                if (o.Mime.mimes[ste.toLowerCase()].split('/')[0] == 'image') {
                    var img = new o.Image();
                    img.onload = function () {
                        var li = document.createElement('div');
                        li.id = this.uid;
                        document.getElementById('Preview_' + fileuniqueID).appendChild(li);
                        this.embed(li.id, {
                            width: 80,
                            height: 80,
                            crop: true
                        });
                    };
                    img.load(file.getSource());
                } else {
                    var img = $('<img id="' + fileuniqueID + '">');
                    img.attr('src', imagesrcpath + "DAMFiles/StaticPreview_small/" + ste.toUpperCase() + ".jpg?'" + generateUniqueTracker());
                    img.appendTo('#Preview_' + fileuniqueID);
                }
            }
        }
        $scope.onDamtypechange = function (fileID) {
            if ($scope.fields['assettype_' + fileID] != null) {
                var oldAssettype = $scope.oldfiledetails["AssetType_" + fileID];
                var newAssettype = parseInt($scope.fields['assettype_' + fileID].id);
                AssetCreationService.GetAssetCategoryTree(newAssettype).then(function (data) {
                    if (data.Response != null) {
                        $scope.AssetCategory["AssetCategoryTree_" + fileID] = JSON.parse(data.Response);
                        $scope.AssetCategory["filterval_" + fileID] = '';
                        var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + fileID + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + fileID + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                        $("#dynamicAssetTypeTree").html($compile(htmltree)($scope));
                    }
                });
                AssetCreationService.GetAssetCategoryPathInAssetEdit(newAssettype).then(function (datapath) {
                    if (datapath.Response != null) {
                        if (datapath.Response[1].AssetTypeIDs.length > 0) {
                            $scope.AssetCategory["treestructure_" + fileID] = datapath.Response[0].CategoryPath;
                        } else $scope.fields["assettype_" + fileID] = '';
                    }
                });
                $scope.AssetCategory['showcategroytext_' + fileID] = true;
                $('#div_' + fileID).html($compile("")($scope));
                $(".testdrop").select2("destroy").select2({});
                if (oldAssettype != '') {
                    var indofAssetType = AssetTypeMetadataDets.assetType.indexOf(oldAssettype);
                    if (indofAssetType == -1) {
                        AssetCreationService.GetDamAttributeRelation(oldAssettype).then(function (attrList) {
                            var attrreldata = attrList.Response;
                            var ind = FileAttributes.FileID.indexOf(fileID);
                            FileAttributes.FileID.splice(ind, 1);
                            FileAttributes.AttributeDataList.splice(ind, 1);
                            FileAttributes.AssetType.splice(ind, 1);
                            AssetTypeMetadataDets.assetType.push(oldAssettype);
                            AssetTypeMetadataDets.Attributes.push(attrreldata);
                            for (var i = 0; i < attrreldata.length; i++) {
                                if (attrreldata[i].AttributeTypeID == 1) {
                                    if (attrreldata[i].ID != 70) {
                                        if (attrreldata[i].ID == SystemDefiendAttributes.Name) {
                                            delete $scope.fields["TextSingleLine_" + fileID + attrreldata[i].ID];
                                        } else {
                                            delete $scope.fields["TextSingleLine_" + fileID + attrreldata[i].ID];
                                        }
                                    }
                                } else if (attrreldata[i].AttributeTypeID == 3) {
                                    if (attrreldata[i].ID == SystemDefiendAttributes.Owner) {
                                        delete $scope.fields["ListSingleSelection_" + fileID + attrreldata[i].ID];
                                    } else if (attrreldata[i].ID == SystemDefiendAttributes.FiscalYear) {
                                        delete $scope.OptionObj["option_" + fileID + attrreldata[i].ID];
                                        delete $scope.fields["ListSingleSelection_" + fileID + attrreldata[i].ID];
                                    } else {
                                        delete $scope.OptionObj["option_" + fileID + attrreldata[i].ID];
                                        delete $scope.fields["ListSingleSelection_" + fileID + attrreldata[i].ID];
                                    }
                                } else if (attrreldata[i].AttributeTypeID == 2) {
                                    delete $scope.fields["TextMultiLine_" + fileID + attrreldata[i].ID];
                                } else if (attrreldata[i].AttributeTypeID == 4) {
                                    delete $scope.OptionObj["option_" + fileID + attrreldata[i].ID];
                                    delete $scope.fields["ListMultiSelection_" + fileID + attrreldata[i].ID];
                                } else if (attrreldata[i].AttributeTypeID == 12) {
                                    $scope.Dropdown = {};
                                } else if (attrreldata[i].AttributeTypeID == 13) {
                                    $scope.PercentageVisibleSettings = {};
                                    $scope.DropDownTreePricing = {};
                                } else if (attrreldata[i].AttributeTypeID == 10) {
                                    $scope.items = [];
                                } else if (attrreldata[i].AttributeTypeID == 7) {
                                    $scope.treesrcdirec = {};
                                    $scope.treePreviewObj = {};
                                    $scope.treeNodeSelectedHolder = [];
                                }
                            }
                        });
                    } else {
                        var attrreldata = AssetTypeMetadataDets.Attributes[indofAssetType];
                        var ind = FileAttributes.FileID.indexOf(fileID);
                        FileAttributes.FileID.splice(ind, 1);
                        FileAttributes.AttributeDataList.splice(ind, 1);
                        FileAttributes.AssetType.splice(ind, 1);
                        for (var i = 0; i < attrreldata.length; i++) {
                            if (attrreldata[i].AttributeTypeID == 1) {
                                if (attrreldata[i].ID != 70) {
                                    if (attrreldata[i].ID == SystemDefiendAttributes.Name) {
                                        delete $scope.fields["TextSingleLine_" + fileID + attrreldata[i].ID];
                                    } else {
                                        delete $scope.fields["TextSingleLine_" + fileID + attrreldata[i].ID];
                                    }
                                }
                            } else if (attrreldata[i].AttributeTypeID == 3) {
                                if (attrreldata[i].ID == SystemDefiendAttributes.Owner) {
                                    delete $scope.fields["ListSingleSelection_" + fileID + attrreldata[i].ID];
                                } else if (attrreldata[i].ID == SystemDefiendAttributes.FiscalYear) {
                                    delete $scope.OptionObj["option_" + fileID + attrreldata[i].ID];
                                    delete $scope.fields["ListSingleSelection_" + fileID + attrreldata[i].ID];
                                } else {
                                    delete $scope.OptionObj["option_" + fileID + attrreldata[i].ID];
                                    delete $scope.fields["ListSingleSelection_" + fileID + attrreldata[i].ID];
                                }
                            } else if (attrreldata[i].AttributeTypeID == 2) {
                                delete $scope.fields["TextMultiLine_" + fileID + attrreldata[i].ID];
                            } else if (attrreldata[i].AttributeTypeID == 4) {
                                delete $scope.OptionObj["option_" + fileID + attrreldata[i].ID];
                                delete $scope.fields["ListMultiSelection_" + fileID + attrreldata[i].ID];
                            } else if (attrreldata[i].AttributeTypeID == 12) {
                                $scope.Dropdown = {};
                            } else if (attrreldata[i].AttributeTypeID == 13) {
                                $scope.PercentageVisibleSettings = {};
                                $scope.DropDownTreePricing = {};
                            } else if (attrreldata[i].AttributeTypeID == 10) {
                                $scope.items = [];
                            } else if (attrreldata[i].AttributeTypeID == 7) {
                                $scope.treesrcdirec = {};
                                $scope.treePreviewObj = {};
                                $scope.treeNodeSelectedHolder = [];
                            }
                        }
                    }
                }
                var indofnewAssetType = AssetTypeMetadataDets.assetType.indexOf(parseInt(newAssettype));
                if (indofnewAssetType == -1) {
                    AssetCreationService.GetDamAttributeRelation(newAssettype).then(function (attrList) {
                        //$scope.OptionObj = {};
                        var attrreldata = attrList.Response;
                        var newHTML = '';
                        FileAttributes.FileID.push(fileID);
                        FileAttributes.AttributeDataList.push(attrreldata);
                        FileAttributes.AssetType.push(newAssettype);
                        AssetTypeMetadataDets.assetType.push(newAssettype);
                        AssetTypeMetadataDets.Attributes.push(attrreldata);
                        newHTML = generateHTMLforAttributes(attrreldata, fileID);
                        $scope.oldfiledetails["AssetType_" + fileID] = newAssettype;
                        $('#div_' + fileID).append($compile(newHTML)($scope));
                    });
                } else {
                    //$scope.OptionObj = {};
                    var attrreldata = AssetTypeMetadataDets.Attributes[indofnewAssetType];
                    var newHTML = '';
                    FileAttributes.FileID.push(fileID);
                    FileAttributes.AttributeDataList.push(attrreldata);
                    FileAttributes.AssetType.push(newAssettype);
                    newHTML = generateHTMLforAttributes(attrreldata, fileID);
                    $scope.oldfiledetails["AssetType_" + fileID] = newAssettype;
                    $('#div_' + fileID).append($compile(newHTML)($scope));
                }
            }
        }
        $scope.dyn_Cont = '';
        $scope.OptionObj = {};
        $scope.fieldoptions = [];
        $scope.OwnerList = [];
        $scope.owner = {};
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.fields = {
            usersID: ''
        };
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });
        }
        $scope.DAMattributesRelationList = [];
        var ownername = $cookies['Username'];
        var ownerid = $cookies['UserId'];
        $scope.OwnerName = ownername;
        $scope.OwnerID = ownerid;
        $scope.ownerEmail = $cookies['UserEmail'];
        $scope.OwnerList = [];
        $scope.AttributeData = [];
        $scope.OwnerList.push({
            "Roleid": 1,
            "RoleName": "Owner",
            "UserEmail": $scope.ownerEmail,
            "DepartmentName": "-",
            "Title": "-",
            "Userid": parseInt($scope.OwnerID, 10),
            "UserName": $scope.OwnerName,
            "IsInherited": '0',
            "InheritedFromEntityid": '0'
        });
        var FileAttributes = {
            "FileID": [],
            "AttributeDataList": [],
            "AssetType": []
        };
        $scope.SaveAsset = [];
        $scope.oldfiledetails = {};
        $scope.SaveAssetFileDetails = function (file, response, fileAttributes) {
            var fileid = file.id.replace("_", "");
            if ($scope.fields['assettype_' + fileid] != undefined && $scope.fields['assettype_' + fileid] != null && $scope.fields['assettype_' + fileid] != '') {
                var AssetTypeID = $scope.fields['assettype_' + fileid].id;
                var AssetName = $scope.fields['assetname_' + fileid];
                var extension = file.name.substring(file.name.lastIndexOf("."));
                var resultArr = response.split(",");
                var AssetFileGuid = fileid;
                var AssetAttribute = fileAttributes;
                $scope.AttributeData = [];
                $scope.fields['TextMultiLine_' + AssetFileGuid + 3] = ($scope.fields['TextMultiLine_' + AssetFileGuid + 3] == undefined) ? '' : $scope.fields['TextMultiLine_' + AssetFileGuid + 3];
                $scope.AttributeData.push({
                    "AttributeID": "3",
                    "AttributeCaption": "Description",
                    "AttributeTypeID": "2",
                    "NodeID": $scope.fields['TextMultiLine_' + AssetFileGuid + 3],
                    "Level": 0
                });
                if (AssetAttribute != undefined) for (var i = 0; i < AssetAttribute.length; i++) {
                    if (AssetAttribute[i].AttributeTypeID == 6) {
                        for (var j = 0; j < AssetAttribute[i].Levels.length; j++) {
                            if ($scope.fields['DropDown_' + AssetFileGuid + AssetAttribute[i].ID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + AssetFileGuid + AssetAttribute[i].ID + '_' + (j + 1)] != undefined) {
                                $scope.AttributeData.push({
                                    "AttributeID": AssetAttribute[i].ID,
                                    "AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
                                    "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['DropDown_' + AssetFileGuid + AssetAttribute[i].ID + '_' + (j + 1)].id],
                                    "Level": $scope.fields['DropDown_' + AssetFileGuid + AssetAttribute[i].ID + '_' + (j + 1)].Level,
                                    "Value": "-1"
                                });
                            }
                        }
                    } else if (AssetAttribute[i].AttributeTypeID == 3) {
                        if (AssetAttribute[i].ID == SystemDefiendAttributes.Owner) {
                            $scope.AttributeData.push({
                                "AttributeID": AssetAttribute[i].ID,
                                "AttributeCaption": AssetAttribute[i].Caption,
                                "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                "Level": 0
                            });
                        } else if ($scope.fields['ListSingleSelection_' + AssetFileGuid + AssetAttribute[i].ID] != undefined && $scope.fields['ListSingleSelection_' + AssetFileGuid + AssetAttribute[i].ID] != "") {
                            var value = $scope.fields['ListSingleSelection_' + AssetFileGuid + AssetAttribute[i].ID];
                            $scope.AttributeData.push({
                                "AttributeID": AssetAttribute[i].ID,
                                "AttributeCaption": AssetAttribute[i].Caption,
                                "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                                "Level": 0
                            });
                        }
                    } else if (AssetAttribute[i].AttributeTypeID == 1) {
                        if (AssetAttribute[i].ID == SystemDefiendAttributes.Name) $scope.entityName = $scope.fields['TextSingleLine_' + AssetFileGuid + AssetAttribute[i].ID];
                        else {
                            $scope.AttributeData.push({
                                "AttributeID": AssetAttribute[i].ID,
                                "AttributeCaption": AssetAttribute[i].Caption,
                                "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                "NodeID": ($scope.fields['TextSingleLine_' + AssetFileGuid + AssetAttribute[i].ID] != null) ? $scope.fields['TextSingleLine_' + AssetFileGuid + AssetAttribute[i].ID].toString() : "",
                                "Level": 0
                            });
                        }
                    } else if (AssetAttribute[i].AttributeTypeID == 2) {
                        $scope.AttributeData.push({
                            "AttributeID": AssetAttribute[i].ID,
                            "AttributeCaption": AssetAttribute[i].Caption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": ($scope.fields['TextMultiLine_' + AssetFileGuid + AssetAttribute[i].ID] != null) ? $scope.fields['TextMultiLine_' + AssetFileGuid + AssetAttribute[i].ID].toString() : "",
                            "Level": 0
                        });
                    } else if (AssetAttribute[i].AttributeTypeID == 4) {
                        if ($scope.fields['ListMultiSelection_' + AssetFileGuid + AssetAttribute[i].ID] != "" && $scope.fields['ListMultiSelection_' + AssetFileGuid + AssetAttribute[i].ID] != undefined) {
                            if ($scope.fields['ListMultiSelection_' + AssetFileGuid + AssetAttribute[i].ID].length > 0) {
                                var multiselectiObject = $scope.fields['ListMultiSelection_' + AssetFileGuid + AssetAttribute[i].ID];
                                for (var k = 0; k < multiselectiObject.length; k++) {
                                    $scope.AttributeData.push({
                                        "AttributeID": AssetAttribute[i].ID,
                                        "AttributeCaption": AssetAttribute[i].Caption,
                                        "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                        "NodeID": parseInt(multiselectiObject[k], 10),
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    } else if (AssetAttribute[i].AttributeTypeID == 5 && AssetAttribute[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                        var MyDate = new Date();
                        var MyDateString;
                        if (AssetAttribute[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                            if ($scope.fields["DatePart_" + AssetFileGuid + AssetAttribute[i].AttributeID] != undefined) MyDateString = $scope.fields["DatePart_" + AssetFileGuid + AssetAttribute[i].AttributeID].toString('MM/dd/yyyy');
                            else MyDateString = "";
                        } else {
                            MyDateString = "";
                        }
                        $scope.AttributeData.push({
                            "AttributeID": AssetAttribute[i].ID,
                            "AttributeCaption": AssetAttribute[i].Caption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": MyDateString,
                            "Level": 0
                        });
                    } else if (AssetAttribute[i].AttributeTypeID == 16) {
                        if ($scope.fields["DatePart_" + AssetFileGuid + AssetAttribute[i].ID] != undefined) {
                            $scope.AttributeData.push({
                                "AttributeID": AssetAttribute[i].ID,
                                "AttributeCaption": AssetAttribute[i].Caption,
                                "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                "NodeID": $scope.fields["DatePart_" + AssetFileGuid + AssetAttribute[i].ID],
                                "Level": 0
                            });
                        }
                    } else if (AssetAttribute[i].AttributeTypeID == 7) {
                        var treenodes = [];
                        treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) {
                            return e.AttributeId == AssetAttribute[i].ID;
                        });
                        for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                            $scope.AttributeData.push({
                                "AttributeID": AssetAttribute[i].ID,
                                "AttributeCaption": AssetAttribute[i].Caption,
                                "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                "NodeID": [parseInt(nodeval.id, 10)],
                                "Level": parseInt(nodeval.Level, 10),
                                "Value": "-1"
                            });
                        }
                    } else if (AssetAttribute[i].AttributeTypeID == 12) {
                        for (var j = 0; j < AssetAttribute[i].Levels.length; j++) {
                            var levelCount = AssetAttribute[i].Levels.length;
                            if (levelCount == 1) {
                                for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)].length; k++) {
                                    $scope.AttributeData.push({
                                        "AttributeID": AssetAttribute[i].AttributeID,
                                        "AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
                                        "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)][k].id],
                                        "Level": $scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)][k].Level,
                                        "Value": "-1"
                                    });
                                }
                            } else {
                                if ($scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)] != undefined) {
                                    if (j == (AssetAttribute[i].Levels.length - 1)) {
                                        for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)].length; k++) {
                                            $scope.AttributeData.push({
                                                "AttributeID": AssetAttribute[i].AttributeID,
                                                "AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
                                                "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                                "NodeID": [$scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)][k].id],
                                                "Level": $scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)][k].Level,
                                                "Value": "-1"
                                            });
                                        }
                                    } else {
                                        $scope.AttributeData.push({
                                            "AttributeID": AssetAttribute[i].AttributeID,
                                            "AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
                                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)].id],
                                            "Level": $scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)].Level,
                                            "Value": "-1"
                                        });
                                    }
                                }
                            }
                        }
                    } else if (AssetAttribute[i].AttributeTypeID == 13) {
                        for (var j = 0; j < AssetAttribute[i].Levels.length; j++) {
                            var attributeLevelOptions = [];
                            attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + AssetFileGuid + AssetAttribute[i].AttributeID + ""], function (e) {
                                return e.level == (j + 1);
                            }));
                            if (attributeLevelOptions[0] != undefined) {
                                if (attributeLevelOptions[0].selection != undefined) {
                                    for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
                                        var valueMatches = [];
                                        if (attributeLevelOptions[0].selection.length > 1) valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                            return relation.NodeId.toString() === opt;
                                        });
                                        $scope.AttributeData.push({
                                            "AttributeID": AssetAttribute[i].AttributeID,
                                            "AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
                                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                            "NodeID": [opt],
                                            "Level": (j + 1),
                                            "Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
                                        });
                                    }
                                }
                            }
                        }
                    } else if (AssetAttribute[i].AttributeTypeID == 8) {
                        $scope.AttributeData.push({
                            "AttributeID": AssetAttribute[i].AttributeID,
                            "AttributeCaption": AssetAttribute[i].AttributeCaption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": $scope.fields['TextSingleLine_' + AssetFileGuid + AssetAttribute[i].AttributeID].toString(),
                            "Level": 0,
                            "Value": "-1"
                        });
                    } else if (AssetAttribute[i].AttributeTypeID == 17) {
                        if ($scope.fields['ListTagwords_' + AssetFileGuid + AssetAttribute[i].AttributeID] != "" && $scope.fields['ListTagwords_' + AssetFileGuid + AssetAttribute[i].AttributeID] != undefined) {
                            for (var j = 0; j < $scope.fields['ListTagwords_' + AssetFileGuid + AssetAttribute[i].AttributeID].length; j++) {
                                $scope.AttributeData.push({
                                    "AttributeID": AssetAttribute[i].AttributeID,
                                    "AttributeCaption": AssetAttribute[i].AttributeCaption,
                                    "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                    "NodeID": parseInt($scope.fields['ListTagwords_' + AssetFileGuid + AssetAttribute[i].AttributeID][j], 10),
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            }
                        }
                    } else if (AssetAttribute[i].AttributeTypeID == 11) {
                        if (AssetAttribute[i].NodeId == undefined || AssetAttribute[i].NodeId == null) {
                            AssetAttribute[i].NodeId = '';
                        }
                        $scope.AttributeData.push({
                            "AttributeID": AssetAttribute[i].AttributeID,
                            "AttributeCaption": AssetAttribute[i].AttributeCaption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": AssetAttribute[i].NodeId,
                            "Level": 0,
                            "Value": "-1"
                        });
                    } else if (AssetAttribute[i].AttributeTypeID == 18) {
                        $scope.AttributeData.push({
                            "AttributeID": AssetAttribute[i].AttributeID,
                            "AttributeCaption": AssetAttribute[i].AttributeCaption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": "",
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                }
                $scope.StartEndDate = [];
                if ($scope.fields["Period_" + AssetFileGuid] != undefined && $scope.fields["Period_" + AssetFileGuid]) {
                    $scope.perItems = [];
                    $scope.perItems = $scope.fields["Period_" + AssetFileGuid];
                    for (var m = 0; m < $scope.perItems.length; m++) {
                        $scope.StartEndDate.push({
                            startDate: ($scope.perItems[m].startDate.length != 0 ? ConvertDateToString($scope.perItems[m].startDate) : ''),
                            endDate: ($scope.perItems[m].endDate.length != 0 ? ConvertDateToString($scope.perItems[m].endDate) : ''),
                            comment: ($scope.perItems[m].comment.trim().length != 0 ? $scope.perItems[m].comment : ''),
                            sortorder: 0
                        });
                    }
                }
                $scope.SaveAsset = [];
                $scope.SaveAsset.push({
                    "Typeid": AssetTypeID,
                    "Active": true,
                    "FolderID": $scope.selectedTree.id,
                    "AttributeData": $scope.AttributeData,
                    "Status": 1,
                    "MimeType": resultArr[1],
                    "Size": file.size,
                    "FileGuid": resultArr[0],
                    "Extension": extension,
                    "Name": AssetName,
                    "VersionNo": 1,
                    "FileName": file.name,
                    "Description": file.name,
                    "EntityID": parseInt($scope.processingsrcobj.processingid),
                    "Periods": $scope.StartEndDate
                });
                var uploaderNodeID = [];

                var uploaderArr = $.grep($scope.SaveAsset[0].AttributeData, function (e) {
                    return e.AttributeTypeID == 11
                })
                if (uploaderArr.length > 0) {
                    for (var i = 0; i < uploaderArr.length; i++) {
                        if (uploaderArr[i].NodeID != null && uploaderArr[i].NodeID != undefined && uploaderArr[i].NodeID != "") {
                            uploaderNodeID.push(uploaderArr[i].NodeID);
                        }
                    }
                    if (uploaderNodeID.length > 0) {
                        AssetCreationService.savemultipleUploadedImg(uploaderNodeID).then(function (newIDs) {
                            if (newIDs.Response != null) {
                                for (var i = 0; i < newIDs.Response.length; i++) {
                                    var UploadAttr = $.grep($scope.SaveAsset[0].AttributeData, function (e) {
                                        return e.NodeID == newIDs.Response[i].m_Item1;
                                    })[0];
                                    var AttrIndex = $scope.SaveAsset[0].AttributeData.indexOf(UploadAttr);
                                    $scope.SaveAsset[0].AttributeData[AttrIndex].NodeID = newIDs.Response[i].m_Item2;
                                }
                                saveAssetService($scope.SaveAsset, fileid);
                            }
                        });
                    }
                    else {
                        saveAssetService($scope.SaveAsset, fileid);
                    }
                }
                else {
                    saveAssetService($scope.SaveAsset, fileid)
                }

            } else bootbox.alert($translate.instant('LanguageContents.Res_2511.Caption'));
        }
        function saveAssetService(SaveAssetObj, fileid) {
            if (isNaN(SaveAssetObj[0].EntityID)) {
                var AssetTypeObj=$.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                    return e.Id == SaveAssetObj[0].Typeid;
                })[0];
                SaveAssetObj[0].ShortDescription= AssetTypeObj.ShortDescription;SaveAssetObj[0].ColorCode=AssetTypeObj.ColorCode,SaveAssetObj[0].Caption=AssetTypeObj.damCaption;
                $scope.$emit('LoadAssetTOEntityCreation_frommui', $scope.SaveAsset, fileid);
                timeoutvariable.cancelTimeout = $timeout(function () {
                    $modalInstance.dismiss('cancel');
                }, 500);
            } else {
                AssetCreationService.SaveAsset(SaveAssetObj).then(function (result) {
                    document.getElementById('icon_ok' + fileid).style.display = 'block';
                    filecount = filecount + 1;
                    if (result.Response == 0) {
                        NotifyError($translate.instant('LanguageContents.Res_4331.Caption'));
                        $('#clearAssetUploader').removeAttr('disabled', 'disabled');
                        $('#clearAssetUploader').css('pointer-events', 'auto');
                        $('#uploadassets').css('display', 'none');
                        $('#pickassetsAlternate').css('display', 'none');
                        $('#createblankAsset').css('display', 'none');
                        totalfilecount = 0;
                        filecount = 0;
                        blankAssetsCnt = 0;
                        $scope.UniqueidsfrBlankAsset = [];
                        saveBlankAssetsCalled = false;
                    } else {
                        $scope.SaveAsset = [];
                        TotalCount = TotalCount + 1;
                        if (totalfilecount == filecount) {
                            NotifySuccess(filecount + " " + $translate.instant('LanguageContents.Res_4101.Caption'));

                            $scope.AssetFilesObj.PageNo = 1;
                            $scope.$emit('mui_CallBackThumbnailAssetView', $scope.selectedTree.id);
                            $('#clearAssetUploader').removeAttr('disabled', 'disabled');
                            $('#clearAssetUploader').css('pointer-events', 'auto');
                            $('#uploadassets').css('display', 'none');
                            $('#pickassetsAlternate').css('display', 'none');
                            $('#createblankAsset').css('display', 'none');
                            totalfilecount = 0;
                            filecount = 0;
                            blankAssetsCnt = 0;
                            $scope.UniqueidsfrBlankAsset = [];
                            $scope.Fileids = [];
                            saveBlankAssetsCalled = false;
                            var FileAttributes = {
                                "FileID": [],
                                "AttributeDataList": [],
                                "AssetType": []
                            };
                            timeoutvariable.cancelTimeout = $timeout(function () {
                                $modalInstance.dismiss('cancel');
                            }, 500);
                        }
                    }
                });
            }
        }

        $scope.fieldsfrDet = {
            usersID: ''
        };
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fieldsfrDet, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }

        function generateUUID() {
            var d = new Date.create().getTime();
            var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = (d + Math.random() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
            });
            return uuid;
        };
        $scope.tree = {};
        $scope.fieldKeys = [];
        $scope.options = {};
        $scope.NormalDropdownCaption = {};
        $scope.NormalDropdownCaptionObj = [];
        $scope.setNormalDropdownCaption = function () {
            var keys1 = [];
            angular.forEach($scope.NormalDropdownCaption, function (key) {
                keys1.push(key);
                $scope.NormalDropdownCaptionObj = keys1;
            });
        }
        $scope.treelevelsObj = [];
        $scope.settreelevels = function () {
            var keys1 = [];
            angular.forEach($scope.treelevels, function (key) {
                keys1.push(key);
                $scope.treelevelsObj = keys1;
            });
        }
        $scope.UploaderCaptionObj = [];
        $scope.setUploaderCaption = function () {
            var keys1 = [];
            angular.forEach($scope.UploderCaption, function (key) {
                keys1.push(key);
                $scope.UploaderCaptionObj = keys1;
            });
        }
        $scope.NormalMultiDropdownCaption = {};
        $scope.NormalMultiDropdownCaptionObj = [];
        $scope.setNormalMultiDropdownCaption = function () {
            var keys1 = [];
            angular.forEach($scope.NormalMultiDropdownCaption, function (key) {
                keys1.push(key);
                $scope.NormalMultiDropdownCaptionObj = keys1;
            });
        }
        $scope.ShowHideAttributeOnRelation = {};
        $scope.normaltreeSources = {};
        $scope.fileguid = '';
        $scope.UniqueidsfrBlankAsset = [];
        $scope.createblankAsset = function () {
            $scope.AttachProgress = false;
            $scope.dyn_ContfrBlankAsset = '';
            var uniqueid = generateUUID();
            $scope.fields["assetname_" + uniqueid] = '';
            $scope.fields["assettype_" + uniqueid] = '';
            $scope.AssetCategory["treestructure_" + uniqueid] = '-';
            $scope.AssetCategory["AssetTypesList_" + uniqueid] = [];
            $scope.fields["tagAllOptions_" + uniqueid] = {
                multiple: false,
                allowClear: true,
                data: $scope.AssetCategory["AssetTypesList_" + uniqueid],
                formatResult: $scope.formatResult,
                formatSelection: $scope.formatSelection,
                dropdownCssClass: "bigdrop",
                escapeMarkup: function (m) {
                    return m;
                }
            };
            AssetCreationService.GetAssetCategoryTree(0).then(function (data) {
                if (data.Response != null) {
                    $scope.AssetCategory["AssetCategoryTree_" + uniqueid] = JSON.parse(data.Response);
                    $scope.AssetCategory["filterval_" + uniqueid] = '';
                    var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + uniqueid + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + uniqueid + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                    $("#dynamicAssetTypeTree").html($compile(htmltree)($scope));
                }
            });
            $scope.oldfiledetails["AssetType_" + uniqueid] = '';
            $scope.UniqueidsfrBlankAsset.push(uniqueid);
            $('#assetlistActivity').append($compile('<div id="' + uniqueid + '" class="attachmentBox" Data-role="Attachment" data-size="' + 0 + '" data-size-uploaded="0" data-status="false">' + '<div class="row-fluid">' + '<div class="span12">' + '<div class="span2 leftSection">' + ' <i id="icon_ok' + uniqueid + '" class="icon-ok-sign" style="display:none;"  ></i>' + ' <div class="fileType-Preview">' + ' <div id="Preview_' + uniqueid + '" class="fileType-PreviewDiv">' + ' </div>' + '</div>' + '</div>' + ' <div class="span10 rightSection">' + '<div class="info">' + '  <span class="name" id="' + uniqueid + '">{{fields.assetname_' + uniqueid + '}}</span>' + ' <span class="size">' + ' <i class="icon-remove removefile" data-fileid="' + uniqueid + '"></i>' + ' </span>' + '</div>' + '<div  class="formInfo">' + ' <form class="form-float">' + '<div class=\"control-group fullSpace\">' + '<label class=\"control-label\"> Select asset type </label>' + '<div class=\"controls\">' + '<a ng-click="AssetTypeTreePopUp(\'' + uniqueid + '\')">{{AssetCategory.treestructure_' + uniqueid + '}}</a>' + '</div>' + '<div class=\"controls\">' + '<input ui-select2="fields.tagAllOptions_' + uniqueid + '"  type="hidden"  data-ng-model="fields.assettype_' + uniqueid + '" ng-change="onDamtypechange(\'' + uniqueid + '\')" > </input>' + '<span class="imgTypeSection">' + '<span class=\"imgTypeEditIcon\" ng-click="AssetTypeTreePopUp(\'' + uniqueid + '\')">' + '<img src="assets/img/treeIcon.png" />' + '</span>' + '</span>' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Asset name </label>' + '<div class=\"controls\">' + '<input class=\"txtbx\" type="text"  data-ng-model="fields.assetname_' + uniqueid + '" name="DescVal_' + uniqueid + '" id="desc_' + uniqueid + '" placeholder="Name">' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Description </label>' + '<div class=\"controls\">' + '<textarea class=\"small-textarea\"  name=\"fields.TextMultiLine_' + uniqueid + '\" ng-model=\"fields.TextMultiLine_' + uniqueid + SystemDefiendAttributes.Description + '\" id=\"TextMultiLine_' + uniqueid + '\" placeholder="Description" rows="3"></textarea>' + '</div>' + '</div>' + '<div id = "div_' + uniqueid + '">' + $scope.dyn_ContfrBlankAsset + '</div>' + '</form>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>')($scope));
            cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
            imagesrcpath = TenantFilePath;
            if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
                imagesrcpath = cloudpath;
            }
            var img = $('<img id="' + uniqueid + '">');
            img.attr('src', imagesrcpath + "DAMFiles/StaticPreview_small/BLANK.jpg?" + generateUniqueTracker());
            img.appendTo('#Preview_' + uniqueid);
            TotalCount = TotalCount + 1;
            $scope.AttachProgress = false;
            $('#totalAssetActivityAttachProgress .count').html(0 + ' of ' + $('#assetlistActivity .attachmentBox').length + ' Uploaded');
            $('#dragActivityassets').hide();
        }
        var blankAssetsCnt = 0;

        function saveBlankAssets() {
            angular.forEach($scope.UniqueidsfrBlankAsset, function (key) {
                var d = key;
                var BlankAssetTypeID = $scope.fields['assettype_' + key].id;
                var BlankAssetName = $scope.fields['assetname_' + key];
                var ind = FileAttributes.FileID.indexOf(key);
                var blankassetAttributeData = FileAttributes.AttributeDataList[ind];
                $scope.blnkAssetAttrData = [];
                $scope.fields['TextMultiLine_' + key + 3] = ($scope.fields['TextMultiLine_' + key + 3] == undefined) ? '' : $scope.fields['TextMultiLine_' + key + 3];
                $scope.blnkAssetAttrData.push({
                    "AttributeID": "3",
                    "AttributeCaption": "Description",
                    "AttributeTypeID": "2",
                    "NodeID": $scope.fields['TextMultiLine_' + key + 3],
                    "Level": 0
                });
                for (var i = 0; i < blankassetAttributeData.length; i++) {
                    if (blankassetAttributeData[i].AttributeTypeID == 3) {
                        if (blankassetAttributeData[i].ID == SystemDefiendAttributes.Owner) {
                            $scope.blnkAssetAttrData.push({
                                "AttributeID": blankassetAttributeData[i].ID,
                                "AttributeCaption": blankassetAttributeData[i].Caption,
                                "AttributeTypeID": blankassetAttributeData[i].AttributeTypeID,
                                "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                "Level": 0
                            });
                        } else if ($scope.fields['ListSingleSelection_' + key + blankassetAttributeData[i].ID] != undefined && $scope.fields['ListSingleSelection_' + key + blankassetAttributeData[i].ID] != "") {
                            var value = $scope.fields['ListSingleSelection_' + key + blankassetAttributeData[i].ID];
                            $scope.blnkAssetAttrData.push({
                                "AttributeID": blankassetAttributeData[i].ID,
                                "AttributeCaption": blankassetAttributeData[i].Caption,
                                "AttributeTypeID": blankassetAttributeData[i].AttributeTypeID,
                                "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                                "Level": 0
                            });
                        }
                    } else if (blankassetAttributeData[i].AttributeTypeID == 1) {
                        if (blankassetAttributeData[i].ID == SystemDefiendAttributes.Name) $scope.entityName = $scope.fields['TextSingleLine_' + key + blankassetAttributeData[i].ID];
                        else {
                            $scope.blnkAssetAttrData.push({
                                "AttributeID": blankassetAttributeData[i].ID,
                                "AttributeCaption": blankassetAttributeData[i].Caption,
                                "AttributeTypeID": blankassetAttributeData[i].AttributeTypeID,
                                "NodeID": ($scope.fields['TextSingleLine_' + key + blankassetAttributeData[i].ID] != null) ? $scope.fields['TextSingleLine_' + key + blankassetAttributeData[i].ID].toString() : "",
                                "Level": 0
                            });
                        }
                    } else if (blankassetAttributeData[i].AttributeTypeID == 2) {
                        if (blankassetAttributeData[i].AttributeID != 3) {
                            $scope.blnkAssetAttrData.push({
                                "AttributeID": blankassetAttributeData[i].ID,
                                "AttributeCaption": blankassetAttributeData[i].Caption,
                                "AttributeTypeID": blankassetAttributeData[i].AttributeTypeID,
                                "NodeID": ($scope.fields['TextMultiLine_' + key + blankassetAttributeData[i].ID] != null) ? $scope.fields['TextMultiLine_' + key + blankassetAttributeData[i].ID].toString() : "",
                                "Level": 0
                            });
                        }
                    } else if (blankassetAttributeData[i].AttributeTypeID == 4) {
                        if ($scope.fields['ListMultiSelection_' + key + blankassetAttributeData[i].ID] != "" && $scope.fields['ListMultiSelection_' + key + blankassetAttributeData[i].ID] != undefined) {
                            if ($scope.fields['ListMultiSelection_' + key + blankassetAttributeData[i].ID].length > 0) {
                                var multiselectiObject = $scope.fields['ListMultiSelection_' + key + blankassetAttributeData[i].ID];
                                for (var k = 0; k < multiselectiObject.length; k++) {
                                    $scope.blnkAssetAttrData.push({
                                        "AttributeID": blankassetAttributeData[i].ID,
                                        "AttributeCaption": blankassetAttributeData[i].Caption,
                                        "AttributeTypeID": blankassetAttributeData[i].AttributeTypeID,
                                        "NodeID": parseInt(multiselectiObject[k], 10),
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                    if (blankassetAttributeData[i].AttributeTypeID == 6) {
                        for (var j = 0; j < blankassetAttributeData[i].Levels.length; j++) {
                            if ($scope.fields['DropDown_' + key + blankassetAttributeData[i].ID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + key + blankassetAttributeData[i].ID + '_' + (j + 1)] != undefined) {
                                $scope.blnkAssetAttrData.push({
                                    "AttributeID": blankassetAttributeData[i].ID,
                                    "AttributeCaption": blankassetAttributeData[i].Levels[j].LevelName,
                                    "AttributeTypeID": blankassetAttributeData[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['DropDown_' + key + blankassetAttributeData[i].ID + '_' + (j + 1)].id],
                                    "Level": $scope.fields['DropDown_' + key + blankassetAttributeData[i].ID + '_' + (j + 1)].Level,
                                    "Value": "-1"
                                });
                            }
                        }
                    } else if (blankassetAttributeData[i].AttributeTypeID == 5 && blankassetAttributeData[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                        var MyDate = new Date();
                        var MyDateString;
                        if (blankassetAttributeData[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                            if ($scope.fields["DatePart_" + key + blankassetAttributeData[i].ID] != undefined) MyDateString = $scope.fields["DatePart_" + key + blankassetAttributeData[i].ID].toString('MM/dd/yyyy');
                            else MyDateString = "";
                        } else {
                            MyDateString = "";
                        }
                        $scope.blnkAssetAttrData.push({
                            "AttributeID": blankassetAttributeData[i].ID,
                            "AttributeCaption": blankassetAttributeData[i].Caption,
                            "AttributeTypeID": blankassetAttributeData[i].AttributeTypeID,
                            "NodeID": MyDateString,
                            "Level": 0
                        });
                    } else if (blankassetAttributeData[i].AttributeTypeID == 16) {
                        if ($scope.fields["DatePart_" + key + blankassetAttributeData[i].ID] != undefined) {
                            $scope.blnkAssetAttrData.push({
                                "AttributeID": blankassetAttributeData[i].ID,
                                "AttributeCaption": blankassetAttributeData[i].Caption,
                                "AttributeTypeID": blankassetAttributeData[i].AttributeTypeID,
                                "NodeID": $scope.fields["DatePart_" + key + blankassetAttributeData[i].ID],
                                "Level": 0
                            });
                        }
                    } else if (blankassetAttributeData[i].AttributeTypeID == 7) {
                        var treenodes = [];
                        treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) {
                            return e.AttributeId == blankassetAttributeData[i].ID;
                        });
                        for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                            $scope.blnkAssetAttrData.push({
                                "AttributeID": blankassetAttributeData[i].ID,
                                "AttributeCaption": blankassetAttributeData[i].Caption,
                                "AttributeTypeID": blankassetAttributeData[i].AttributeTypeID,
                                "NodeID": [parseInt(nodeval.id, 10)],
                                "Level": parseInt(nodeval.Level, 10),
                                "Value": "-1"
                            });
                        }
                    } else if (blankassetAttributeData[i].AttributeTypeID == 12) {
                        for (var j = 0; j < blankassetAttributeData[i].Levels.length; j++) {
                            var levelCount = blankassetAttributeData[i].Levels.length;
                            if (levelCount == 1) {
                                for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + key + blankassetAttributeData[i].AttributeID + '_' + (j + 1)].length; k++) {
                                    $scope.blnkAssetAttrData.push({
                                        "AttributeID": blankassetAttributeData[i].AttributeID,
                                        "AttributeCaption": blankassetAttributeData[i].Levels[j].LevelName,
                                        "AttributeTypeID": blankassetAttributeData[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + key + blankassetAttributeData[i].AttributeID + '_' + (j + 1)][k].id],
                                        "Level": $scope.fields['MultiSelectDropDown_' + key + blankassetAttributeData[i].AttributeID + '_' + (j + 1)][k].Level,
                                        "Value": "-1"
                                    });
                                }
                            } else {
                                if ($scope.fields['MultiSelectDropDown_' + key + blankassetAttributeData[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + key + blankassetAttributeData[i].AttributeID + '_' + (j + 1)] != undefined) {
                                    if (j == (blankassetAttributeData[i].Levels.length - 1)) {
                                        for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + key + blankassetAttributeData[i].AttributeID + '_' + (j + 1)].length; k++) {
                                            $scope.blnkAssetAttrData.push({
                                                "AttributeID": blankassetAttributeData[i].AttributeID,
                                                "AttributeCaption": blankassetAttributeData[i].Levels[j].LevelName,
                                                "AttributeTypeID": blankassetAttributeData[i].AttributeTypeID,
                                                "NodeID": [$scope.fields['MultiSelectDropDown_' + key + blankassetAttributeData[i].AttributeID + '_' + (j + 1)][k].id],
                                                "Level": $scope.fields['MultiSelectDropDown_' + key + blankassetAttributeData[i].AttributeID + '_' + (j + 1)][k].Level,
                                                "Value": "-1"
                                            });
                                        }
                                    } else {
                                        $scope.blnkAssetAttrData.push({
                                            "AttributeID": blankassetAttributeData[i].AttributeID,
                                            "AttributeCaption": blankassetAttributeData[i].Levels[j].LevelName,
                                            "AttributeTypeID": blankassetAttributeData[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['MultiSelectDropDown_' + key + blankassetAttributeData[i].AttributeID + '_' + (j + 1)].id],
                                            "Level": $scope.fields['MultiSelectDropDown_' + key + blankassetAttributeData[i].AttributeID + '_' + (j + 1)].Level,
                                            "Value": "-1"
                                        });
                                    }
                                }
                            }
                        }
                    } else if (blankassetAttributeData[i].AttributeTypeID == 13) {
                        for (var j = 0; j < blankassetAttributeData[i].Levels.length; j++) {
                            var attributeLevelOptions = [];
                            attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + key + blankassetAttributeData[i].AttributeID + ""], function (e) {
                                return e.level == (j + 1);
                            }));
                            if (attributeLevelOptions[0] != undefined) {
                                if (attributeLevelOptions[0].selection != undefined) {
                                    for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
                                        var valueMatches = [];
                                        if (attributeLevelOptions[0].selection.length > 1) valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                            return relation.NodeId.toString() === opt;
                                        });
                                        $scope.blnkAssetAttrData.push({
                                            "AttributeID": blankassetAttributeData[i].AttributeID,
                                            "AttributeCaption": blankassetAttributeData[i].Levels[j].LevelName,
                                            "AttributeTypeID": blankassetAttributeData[i].AttributeTypeID,
                                            "NodeID": [opt],
                                            "Level": (j + 1),
                                            "Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
                                        });
                                    }
                                }
                            }
                        }
                    } else if (blankassetAttributeData[i].AttributeTypeID == 8) {
                        $scope.blnkAssetAttrData.push({
                            "AttributeID": blankassetAttributeData[i].AttributeID,
                            "AttributeCaption": blankassetAttributeData[i].AttributeCaption,
                            "AttributeTypeID": blankassetAttributeData[i].AttributeTypeID,
                            "NodeID": $scope.fields['TextSingleLine_' + key + blankassetAttributeData[i].AttributeID].toString(),
                            "Level": 0,
                            "Value": "-1"
                        });
                    } else if (blankassetAttributeData[i].AttributeTypeID == 17) {
                        if ($scope.fields['ListTagwords_' + key + blankassetAttributeData[i].AttributeID] != "" && $scope.fields['ListTagwords_' + key + blankassetAttributeData[i].AttributeID] != undefined) {
                            for (var j = 0; j < $scope.fields['ListTagwords_' + key + blankassetAttributeData[i].AttributeID].length; j++) {
                                $scope.blnkAssetAttrData.push({
                                    "AttributeID": blankassetAttributeData[i].AttributeID,
                                    "AttributeCaption": blankassetAttributeData[i].AttributeCaption,
                                    "AttributeTypeID": blankassetAttributeData[i].AttributeTypeID,
                                    "NodeID": parseInt($scope.fields['ListTagwords_' + key + blankassetAttributeData[i].AttributeID][j], 10),
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            }
                        }
                    }
                }
                $scope.StartEndDate = [];
                if ($scope.fields["Period_" + key] != undefined && $scope.fields["Period_" + key]) {
                    $scope.perItems = [];
                    $scope.perItems = $scope.fields["Period_" + key];
                    for (var m = 0; m < $scope.perItems.length; m++) {
                        $scope.StartEndDate.push({
                            startDate: ($scope.perItems[m].startDate.length != 0 ? ConvertDateToString($scope.perItems[m].startDate) : ''),
                            endDate: ($scope.perItems[m].endDate.length != 0 ? ConvertDateToString($scope.perItems[m].endDate) : ''),
                            comment: ($scope.perItems[m].comment.trim().length != 0 ? $scope.perItems[m].comment : ''),
                            sortorder: 0
                        });
                    }
                }
                $scope.SaveBlankAsset = [];
                $scope.SaveBlankAsset.push({
                    "Typeid": BlankAssetTypeID,
                    "Active": true,
                    "FolderID": $scope.selectedTree.id,
                    "AttributeData": $scope.blnkAssetAttrData,
                    "Status": 1,
                    "Name": BlankAssetName,
                    "EntityID": parseInt($scope.processingsrcobj.processingid),
                    "Category": 1,
                    "Url": '',
                    'Periods': $scope.StartEndDate
                });
                AssetCreationService.SaveBlankAsset($scope.SaveBlankAsset).then(function (result) {
                    if (result.Response == 0) {
                        NotifyError($translate.instant('LanguageContents.Res_4332.Caption'));
                        var FileAttributes = {
                            "FileID": [],
                            "AttributeDataList": [],
                            "AssetType": []
                        };
                    } else {
                        $('#' + key).attr('data-status', 'true');
                        document.getElementById('icon_ok' + key).style.display = 'block';
                        blankAssetsCnt = blankAssetsCnt + 1;
                        if (blankAssetsCnt == $scope.UniqueidsfrBlankAsset.length) {
                            if (totalfilecount == 0) {
                                $('#totalAssetActivityAttachProgress .count').html($scope.UniqueidsfrBlankAsset.length + ' of ' + $scope.UniqueidsfrBlankAsset.length + ' Uploaded');
                                $('#totalAssetActivityAttachProgress .size').html(plupload.formatSize(0) + ' / ' + plupload.formatSize(0));
                                $('#totalAssetActivityAttachProgress .bar').css("width", "100%");
                                timeoutvariable.SuccessNotificationTimeout = $timeout(function () {
                                    NotifySuccess($translate.instant('LanguageContents.Res_4101.Caption'));
                                }, 500);

                                timeoutvariable.AssetRefreshTimeout = $timeout(function () {
                                    $('#totalAssetActivityAttachProgress #uploaded').html('Upload Successful.');
                                }, 500);
                                $scope.SaveAsset = [];
                                $scope.AssetFilesObj.PageNo = 1;
                                $scope.$emit('CallBackThumbnailAssetView', $scope.selectedTree.id);
                                $('#clearAssetUploader').removeAttr('disabled', 'disabled');
                                $('#clearAssetUploader').css('pointer-events', 'auto');
                                $('#uploadassets').css('display', 'none');
                                $('#pickassetsAlternate').css('display', 'none');
                                $('#createblankAsset').css('display', 'none');
                                totalfilecount = 0;
                                filecount = 0;
                                blankAssetsCnt = 0;
                                $scope.UniqueidsfrBlankAsset = [];
                                $scope.SaveBlankAsset = [];
                                saveBlankAssetsCalled = false;
                                FileAttributes = {
                                    "FileID": [],
                                    "AttributeDataList": [],
                                    "AssetType": []
                                };
                                timeoutvariable.cancelTimeout = $timeout(function () {
                                    $modalInstance.dismiss('cancel');
                                }, 500);
                            }
                        }
                    }
                });
            });
        }
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType, fileID) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToShow = [];
                var hideAttributeOtherThanSelected = [];
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.Dropdown["OptionValues" + fileID + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + fileID + attrID + "_" + j].data.length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + fileID + attrID + "_" + j] = "";
                        } else if (attrType == 12) {
                            if (j == levelcnt) $scope.fields["MultiSelectDropDown_" + fileID + attrID + "_" + j] = [];
                            else $scope.fields["MultiSelectDropDown_" + fileID + attrID + "_" + j] = "";
                        }
                    }
                    if (attrType == 6) {
                        if ($scope.fields["DropDown_" + fileID + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["DropDown_" + fileID + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + fileID + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    } else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + fileID + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + fileID + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + fileID + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    } catch (e) { }
                }
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType, fileID);
                if (attrType == 3) {
                    if ($scope.fields['ListSingleSelection_' + fileID + attrID] != null) {
                        optionValue = parseInt(($scope.fields['ListSingleSelection_' + fileID + attrID]), 10);
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    if ($scope.fields['ListMultiSelection_' + fileID + attrID] != null) {
                        optionValue = $scope.fields['ListMultiSelection_' + fileID + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + fileID + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                } else if (attrType == 7) {
                    if ($scope.fields['Tree_' + fileID + attrID] != null) {
                        optionValue = $scope.fields['Tree_' + fileID + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + fileID + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }

        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType, fileID) {
            var recursiveAttrID = '';
            var attributesToShow = [];
            if (attrLevel > 0) {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            } else {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }
            if (attributesToShow[0] != undefined) {
                for (var i = 0; i < attributesToShow[0].length; i++) {
                    var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrRelIDs[j].toString()] = false;
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType, fileID);
                                    }
                                }
                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + fileID + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType, fileID)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $scope.AddDefaultEndDate = function (objdateval) {
            $("#EntityMetadata").addClass('notvalidate');
            if (objdateval.startDate == null) {
                objdateval.endDate = null
            } else {
                objdateval.endDate = new Date.create(objdateval.startDate);
                objdateval.endDate = objdateval.endDate.addDays(7);
            }
        };
        var treeTextVisbileflag = false;

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        };
        $scope.AddDefaultEndDate = function (enddate, startdate, currentindex, fileid) {
            var enddate1 = null;
            if (currentindex != 0) {
                enddate1 = $scope.fields["Period_" + fileid][currentindex - 1].endDate;
            }
            if (enddate1 != null && enddate1 >= startdate) {
                bootbox.alert($translate.instant('LanguageContents.Res_1987.Caption'));
                $scope.fields["Period_" + fileid][currentindex].startDate = null;
                $scope.fields["Period_" + fileid][currentindex].endDate = null;
            } else {
                $("#EntityMetadata").addClass('notvalidate');
                if (startdate == null) {
                    $scope.fields["Period_" + fileid][currentindex].endDate = null;
                } else {
                    $scope.fields["Period_" + fileid][currentindex].endDate = (new Date.create(startdate)).addDays(7);
                }
            }
        };
        $scope.CheckPreviousStartDate = function (enddate, startdate, currentindex, fileid) {
            var enddate1 = null;
            if (currentindex != 0 || currentindex == 0) {
                enddate1 = $scope.fields["Period_" + fileid][currentindex].endDate;
            }
            if (enddate1 != null && startdate >= enddate1) {
                bootbox.alert($translate.instant('LanguageContents.Res_4240.Caption'));
                $scope.fields["Period_" + fileid][currentindex].endDate = null;
            }
        };
        $scope.addNew = function (fileid) {
            var ItemCnt = $scope.fields["Period_" + fileid].length;
            if (ItemCnt > 0) {
                if ($scope.fields["Period_" + fileid][ItemCnt - 1].startDate == null || $scope.fields["Period_" + fileid][ItemCnt - 1].startDate.length == 0 || $scope.fields["Period_" + fileid][ItemCnt - 1].endDate.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1986.Caption'));
                    return false;
                }
                $scope.fields["Period_" + fileid].push({
                    startDate: [],
                    endDate: [],
                    comment: '',
                    sortorder: 0,
                    fileid: fileid
                });
            }
        };
        $scope.submitOne = function (item) {
            $scope.lastSubmit = angular.copy(item);
        };
        $scope.deleteOne = function (item) {
            var fileid = item.fileid;
            $scope.lastSubmit.splice($.inArray(item, $scope.lastSubmit), 1);
            $scope.fields["Period_" + fileid].splice($.inArray(item, $scope.fields["Period_" + fileid]), 1);
        };
        $scope.submitAll = function () {
            $scope.lastSubmit = angular.copy($scope.items);
        }
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            var fileid = fileUniqueID;
            $scope.fields["Tree_" + fileid + branch.AttributeId].splice(0, $scope.fields["Tree_" + fileid + branch.AttributeId].length);
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                }
            }
            if ($scope.treesrcdirec["Attr_" + fileid + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + fileid + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + fileid + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + fileid + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + fileid + branch.AttributeId] = false;
            }
            if ($scope.treeNodeSelectedHolder.length > 0) {
                for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
                    $scope.fields["Tree_" + fileid + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
                }
            }
            $scope.ShowHideAttributeToAttributeRelations(branch.AttributeId, 0, 0, 7, fileid);
        };
        $scope.$on('CreateContextAsset', function (event) {
            $scope.fnTimeOutfrAssetCreation(1);
        });
        $scope.AssetTypeTreePopUp = function (fileid) {
            $scope.AssetCategory["filterval_" + fileid] = '';
            var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + fileid + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + fileid + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
            $("#dynamicAssetTypeTree").html($compile(htmltree)($scope));
            $scope.FileIdForAssetCategory = fileid;
            $("#AssetCategoryTree").modal('show');
        };
        $scope.AssetTypeCategorySelection = function (branch, parentArr) {
            var i = 0;
            if (branch.ischecked == true) {
                $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory] = "";
                for (i = parentArr.length - 1; i >= 0; i--) {
                    $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory] += parentArr[i].Caption + "/";
                }
                if ($scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory].length > 0) $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory] = $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory].substring(0, $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory].length - 1);
                var tempval = [];
                var tempasst = "";
                for (var k = 0; k < parentArr[0].Children.length; k++) {
                    tempasst = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                        return e.id == parentArr[0].Children[k].id
                    });
                    if (tempasst != undefined && tempasst.length > 0) tempval.push(tempasst[0]);
                }
                if (tempval.length > 0) {
                    $scope.AssetCategory["AssetTypesList_" + $scope.FileIdForAssetCategory] = tempval;
                    $scope.fields["tagAllOptions_" + $scope.FileIdForAssetCategory].data.splice(0, $scope.fields["tagAllOptions_" + $scope.FileIdForAssetCategory].data.length);
                    $.each($scope.AssetCategory["AssetTypesList_" + $scope.FileIdForAssetCategory], function (i, el) {
                        $scope.fields["tagAllOptions_" + $scope.FileIdForAssetCategory].data.push({
                            "id": el.Id,
                            "text": el.text,
                            "ShortDescription": el.ShortDescription,
                            "ColorCode": el.ColorCode
                        });
                    });
                    $scope.fields["assettype_" + $scope.FileIdForAssetCategory] = $.grep(tempval, function (e) {
                        return e.id == branch.id
                    })[0];
                }
                $("#AssetCategoryTree").modal('hide');
                $scope.AssetCategory['showcategroytext_' + $scope.FileIdForAssetCategory] = true;
                $scope.onDamtypechange($scope.FileIdForAssetCategory);
            } else {
                $('#div_' + $scope.FileIdForAssetCategory).html($compile("")($scope));
                $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory] = "-";
                $scope.AssetCategory['showcategroytext_' + $scope.FileIdForAssetCategory] = false;
                $scope.fields["assettype_" + $scope.FileIdForAssetCategory] = '';
                $scope.AssetCategory["AssetTypesList_" + $scope.FileIdForAssetCategory].splice(0, $scope.AssetCategory["AssetTypesList_" + $scope.FileIdForAssetCategory].length);
            }
        }

        function generateHTMLforAttributes(attributelist, fileUniqueID) {
            var attrHTML = '';
            $scope.DateObject = {};
            for (var i = 0; i < attributelist.length; i++) {

                if (attributelist[i].AttributeTypeID == 1) {
                    if (attributelist[i].ID != 70) {
                        if (attributelist[i].ID == SystemDefiendAttributes.Name) {
                            attrHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + fileUniqueID + attributelist[i].ID + "\">" + attributelist[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"fields.TextSingleLine_" + fileUniqueID + attributelist[i].ID + "\" id=\"TextSingleLine_" + fileUniqueID + attributelist[i].ID + "\" placeholder=\"" + attributelist[i].Caption + "\"></div></div>";
                            $scope.fields["TextSingleLine_" + fileUniqueID + attributelist[i].ID] = attributelist[i].DefaultValue;
                        } else {
                            attrHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + fileUniqueID + attributelist[i].ID + "\">" + attributelist[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"fields.TextSingleLine_" + fileUniqueID + attributelist[i].ID + "\"  id=\"TextSingleLine_" + fileUniqueID + attributelist[i].ID + "\" placeholder=\"" + attributelist[i].Caption + "\"></div></div>";
                            $scope.fields["TextSingleLine_" + fileUniqueID + attributelist[i].ID] = attributelist[i].DefaultValue;
                        }
                    }
                } else if (attributelist[i].AttributeTypeID == 3) {
                    if (attributelist[i].ID == SystemDefiendAttributes.Owner) {
                        attrHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + fileUniqueID + attributelist[i].ID + "\">" + attributelist[i].Caption + " </label><div class=\"controls\"> <input class=\"txtbx\" type=\"text\" ng-model=\"fields.ListSingleSelection_" + fileUniqueID + attributelist[i].ID + "\"  id=\"ListSingleSelection_" + fileUniqueID + attributelist[i].ID + "\" dirownernameautopopulate placeholder=\"" + attributelist[i].Caption + "\"></div></div>";
                        $scope.fields["ListSingleSelection_" + fileUniqueID + attributelist[i].ID] = $scope.OwnerList[0].UserName;
                    } else if (attributelist[i].ID == SystemDefiendAttributes.FiscalYear) {
                        $scope.OptionObj["option_" + fileUniqueID + attributelist[i].ID] = attributelist[i].Options;
                        attrHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + fileUniqueID + attributelist[i].ID + "\">" + attributelist[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileUniqueID + attributelist[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attributelist[i].AttributeID + ",0,0,3,'" + fileUniqueID + "')\" ng-model=\"fields.ListSingleSelection_" + fileUniqueID + attributelist[i].ID + "\"  id=\"ListSingleSelection_" + fileUniqueID + attributelist[i].ID + "\"> <option value=\"\"> Select " + attributelist[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + fileUniqueID + attributelist[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                        $scope.fields["ListSingleSelection_" + fileUniqueID + attributelist[i].ID] = attributelist[i].DefaultValue;
                    } else {
                        $scope.OptionObj["option_" + fileUniqueID + attributelist[i].ID] = attributelist[i].Options;
                        attrHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + fileUniqueID + attributelist[i].ID + "\">" + attributelist[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileUniqueID + attributelist[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attributelist[i].ID + ",0,0,3,'" + fileUniqueID + "')\" ng-model=\"fields.ListSingleSelection_" + fileUniqueID + attributelist[i].ID + "\"  id=\"ListSingleSelection_" + fileUniqueID + attributelist[i].ID + "\"> <option value=\"\"> Select " + attributelist[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + fileUniqueID + attributelist[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                        $scope.fields["ListSingleSelection_" + fileUniqueID + attributelist[i].ID] = attributelist[i].DefaultValue;
                    }
                } else if (attributelist[i].AttributeTypeID == 2) {
                    if (attributelist[i].ID != 3) {
                        attrHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + fileUniqueID + attributelist[i].ID + "\">" + attributelist[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileUniqueID + attributelist[i].ID + "\" name=\"fields.TextMultiLine_" + fileUniqueID + attributelist[i].ID + "\" ng-model=\"fields.TextMultiLine_" + fileUniqueID + attributelist[i].ID + "\" id=\"TextMultiLine_" + fileUniqueID + attributelist[i].ID + "\" placeholder=\"" + attributelist[i].Caption + "\" rows=\"3\"></textarea></div></div>";
                        $scope.fields["TextMultiLine_" + fileUniqueID + attributelist[i].ID] = attributelist[i].DefaultValue;
                    }
                } else if (attributelist[i].AttributeTypeID == 4) {
                    $scope.fields["ListMultiSelection_" + fileUniqueID + attributelist[i].ID] = [];
                    $scope.OptionObj["option_" + fileUniqueID + attributelist[i].ID] = attributelist[i].Options;
                    var defaultmultiselectvalue = attributelist[i].DefaultValue.split(',');
                    if (attributelist[i].DefaultValue != "") {
                        for (var v = 0, val; val = defaultmultiselectvalue[v++];) {
                            $scope.fields["ListMultiSelection_" + fileUniqueID + attributelist[i].ID].push(parseInt(val));
                        }
                    } else {
                        $scope.fields["ListMultiSelection_" + fileUniqueID + attributelist[i].ID] = [];
                    }
                    attrHTML += "<div class=\"control-group attachmentMultiselect\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + fileUniqueID + attributelist[i].ID + "\">" + attributelist[i].Caption + " </label><div class=\"controls\"> <select class=\"multiselect\" multiple=\"multiple\"  multiselect-dropdown data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileUniqueID + attributelist[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attributelist[i].ID + ",0,0,3,'" + fileUniqueID + "')\" ng-model=\"fields.ListMultiSelection_" + fileUniqueID + attributelist[i].ID + "\"  id=\"ListMultiSelection_" + fileUniqueID + attributelist[i].ID + "\" ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + fileUniqueID + attributelist[i].ID + "\"  > </select></div></div>";
                } else if (attributelist[i].AttributeTypeID == 5 && attributelist[i].AttributeID != SystemDefiendAttributes.ApproveTime && attributelist[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                    $scope.MinValue = attributelist[i].MinValue;
                    $scope.MaxValue = attributelist[i].MaxValue;
                    $scope.fields["DatePart_" + attributelist[i].AttributeID] = null;
                    $scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].ID] = new Date.create();
                    $scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].ID] = new Date.create();
                    if (attributelist[i].Caption == "Due Date") {
                        $scope.IsOpend = "DueDate" + fileUniqueID;
                        $scope.DateObject.DueDate = false;
                        var isopenedhtmlstr = "DueDate" + fileUniqueID + attributelist[i].ID;
                    } else if (attributelist[i].Caption == "StartDate") {
                        $scope.IsOpend = "StartDate" + fileUniqueID;
                        var isopenedhtmlstr = "StartDate" + fileUniqueID + attributelist[i].ID;
                        $scope.DateObject.StartDate = false;
                    } else {
                        $scope.IsOpend = "EndDate" + fileUniqueID;
                        $scope.DateObject.EndDate = false;
                        var isopenedhtmlstr = "EndDate" + fileUniqueID + attributelist[i].ID;
                    }
                    if ($scope.MinValue < 0) {
                        $scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].ID].setDate($scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].ID].getDate() + ($scope.MinValue + 1));
                    } else {
                        $scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].ID].setDate($scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].ID].getDate() + ($scope.MinValue));
                    }
                    if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                        $scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].ID].setDate($scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].ID].getDate() + ($scope.MaxValue - 1));
                    } else {
                        $scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].ID].setDate($scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].ID].getDate() + 100000);
                    }
                    var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].ID], $scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].ID]);
                    $scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].ID] = (temp.MinDate);
                    $scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].ID] = (temp.MaxDate);                  
                    attrHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_" + fileUniqueID + attributelist[i].ID + "\">" + attributelist[i].Caption + "</label><div class=\"controls\"><span class='editable'>{{fields.DateTime_" + attributelist[i].ID + "}}</span><input type=\"text\" class=\"txtbx DatePartctrl\" " + fileUniqueID + attributelist[i].ID + "\" id=\"DatePart_" + fileUniqueID + attributelist[i].ID + "\" ng-click=\"Calanderopen($event,'" + $scope.IsOpend + "'," + attributelist[i].ID + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"" + isopenedhtmlstr + "\" min-date=\"fields.DatePartMinDate_" + fileUniqueID + attributelist[i].ID + "\" min-date=\"fields.DatePartMaxDate_" + fileUniqueID + attributelist[i].ID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + attributelist[i].Caption + "\" ng-change=\"setTimeout(changeduedate_changed(fields.DatePart_" + fileUniqueID + attributelist[i].ID + ",fields.DatePartMinDate_" + fileUniqueID + attributelist[i].AttributeID + ",fields.DatePartMaxDate_" + fileUniqueID + attributelist[i].AttributeID + "," + fileUniqueID + attributelist[i].ID + "),1000)\" ng-model=\"fields.DatePart_" + fileUniqueID + attributelist[i].ID + "\"></div></div>";
                    $scope.fields["fields.DatePart_" + fileUniqueID + attributelist[i].ID] = false;
                } else if (attributelist[i].AttributeTypeID == 16) {
                    $scope.MinValue = attributelist[i].MinValue;
                    $scope.MaxValue = attributelist[i].MaxValue;
                    $scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].ID] = new Date.create();
                    $scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].ID] = new Date.create();
                    $scope.IsOpend = "TaskDateAction" + fileUniqueID;
                    var isopenedhtmlstr = "TaskDateAction" + fileUniqueID + attributelist[i].ID;
                    $scope.DateObject = {
                        "DateAction": false
                    };
                    if ($scope.MinValue < 0) {
                        $scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].ID].setDate($scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].ID].getDate() + ($scope.MinValue + 1));
                    } else {
                        $scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].ID].setDate($scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].ID].getDate() + ($scope.MinValue));
                    }
                    if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                        $scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].ID].setDate($scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].ID].getDate() + ($scope.MaxValue - 1));
                    } else {
                        $scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].ID].setDate($scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].ID].getDate() + 100000);
                    }
                    var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].ID], $scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].ID]);
                    $scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].ID] = (temp.MinDate);
                    $scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].ID] = (temp.MaxDate);                 
                    attrHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_" + fileUniqueID + attributelist[i].ID + "\">" + attributelist[i].Caption + "</label><div class=\"controls\"><span class='editable'>{{fields.DateTime_" + attributelist[i].ID + "}}</span><input type=\"text\" class=\"txtbx DatePartctrl\" " + fileUniqueID + attributelist[i].ID + "\" id=\"DatePart_" + fileUniqueID + attributelist[i].ID + "\" ng-click=\"Calanderopen($event,'" + $scope.IsOpend + "'," + attributelist[i].ID + ")\"  datepicker-popup=\"{{format}}\" is-open=\"" + isopenedhtmlstr + "\" min-date=\"fields.DatePartMinDate_" + fileUniqueID + attributelist[i].ID + "\" min-date=\"fields.DatePartMaxDate_" + fileUniqueID + attributelist[i].ID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + attributelist[i].Caption + "\" ng-change=\"setTimeout(changeduedate_changed(fields.DatePart_" + fileUniqueID + attributelist[i].ID + ",fields.DatePartMinDate_" + fileUniqueID + attributelist[i].AttributeID + ",fields.DatePartMaxDate_" + fileUniqueID + attributelist[i].AttributeID + "," + fileUniqueID + attributelist[i].ID + "),1000)\" ng-model=\"fields.DatePart_" + fileUniqueID + attributelist[i].ID + "\"></div></div>";
                    $scope.fields["fields.DatePart_" + fileUniqueID + attributelist[i].ID] = false;
                } else if (attributelist[i].AttributeTypeID == 12) {
                    var totLevelCnt1 = attributelist[i].Levels.length;
                    for (var j = 0; j < totLevelCnt1; j++) {
                        if (totLevelCnt1 == 1) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)] = {};
                            $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                return item.Caption
                            };
                            $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                return item.Caption
                            };
                            $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)].data = JSON.parse(attributelist[i].tree).Children;
                            $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)].multiple = true;
                            attrHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + fileUniqueID + attributelist[i].ID + "\">" + attributelist[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                            attrHTML += "<input ui-select2=\"Dropdown.OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileUniqueID + attributelist[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + attributelist[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12,'" + fileUniqueID + "' )\" ng-model=\"fields.MultiSelectDropDown_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                            $scope.setFieldKeys();
                        } else {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)] = {};
                            $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                return item.Caption
                            };
                            $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                return item.Caption
                            };
                            if (j == 0) {
                                $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)].data = JSON.parse(attributelist[i].tree).Children;
                                $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)].multiple = false;
                                attrHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + fileUniqueID + attributelist[i].ID + "\">" + attributelist[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                attrHTML += "<input ui-select2=\"Dropdown.OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileUniqueID + attributelist[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + attributelist[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12,'" + fileUniqueID + "' )\" ng-model=\"fields.MultiSelectDropDown_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                            } else {
                                $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)].data = [];
                                if (j == (attributelist[i].Levels.length - 1)) {
                                    $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)].multiple = true;
                                    attrHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + fileUniqueID + attributelist[i].ID + "\">" + attributelist[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                    attrHTML += "<input ui-select2=\"Dropdown.OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileUniqueID + attributelist[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + attributelist[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 ,'" + fileUniqueID + "')\" ng-model=\"fields.MultiSelectDropDown_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                } else {
                                    $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)].multiple = false;
                                    attrHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + fileUniqueID + attributelist[i].ID + "\">" + attributelist[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                    attrHTML += "<input ui-select2=\"Dropdown.OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileUniqueID + attributelist[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attributelist[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12,'" + fileUniqueID + "' )\" ng-model=\"fields.MultiSelectDropDown_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                }
                            }
                        }
                        $scope.setFieldKeys();
                    }
                } else if (attributelist[i].AttributeTypeID == 10) {
                    $scope.items = [];
                    $scope.ShowHideAttributeOnRelation["Attribute_" + fileUniqueID + attributelist[i].AttributeID] = true;
                    $scope.OptionObj["option_" + attributelist[i].AttributeID] = attributelist[i].Options;
                    $scope.setoptions();
                    $scope.items.push({
                        startDate: [],
                        endDate: [],
                        comment: '',
                        sortorder: 0,
                        fileid: fileUniqueID,
                        calstartopen: false,
                        calendopen: false
                    });
                    $scope.MinValue = attributelist[i].MinValue;
                    $scope.MaxValue = attributelist[i].MaxValue;
                    $scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].AttributeID] = new Date.create();
                    $scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].AttributeID] = new Date.create();
                    if ($scope.MinValue < 0) {
                        $scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].AttributeID].getDate() + ($scope.MinValue + 1));
                    } else {
                        $scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].AttributeID].getDate() + ($scope.MinValue));
                    }
                    if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                        $scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                    } else {
                        $scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].AttributeID].getDate() + 100000);
                    }
                    var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].AttributeID], $scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].AttributeID]);
                    $scope.fields["DatePartMinDate_" + fileUniqueID + attributelist[i].AttributeID] = (temp.MinDate);
                    $scope.fields["DatePartMaxDate_" + fileUniqueID + attributelist[i].AttributeID] = (temp.MaxDate);
                    attrHTML += "    <div class=\"control-group\"><label for=\"fields.TextSingleLine_ " + fileUniqueID + attributelist[i].AttributeID + "\" class=\"control-label\">" + attributelist[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                    attrHTML += "<div class=\"row-fluid\"><div class=\"inputHolder span5 noBorder\">";
                    attrHTML += "<input class=\"sdate margin-bottom0x Period_" + fileUniqueID + attributelist[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileUniqueID + attributelist[i].AttributeID + "\"  id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-change=\"changeperioddate_changed(item.startDate,fields.DatePartMinDate_" + fileUniqueID + $scope.atributesRelationList[i].AttributeID + ",fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + ",'StartDate')\" ng-model=\"item.startDate\" ng-click=\"PeriodCalanderopen($event,item,'start')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calstartopen\" min-date=\"fields.DatePartMinDate_" + fileUniqueID + attributelist[i].AttributeID + "\" min-date=\"fields.DatePartMaxDate_" + fileUniqueID + attributelist[i].AttributeID + "\" datepicker-popup=\"{{format}}\"  datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- Start date --\"/><input class=\"edate margin-bottom0x Period_" + fileUniqueID + attributelist[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileUniqueID + attributelist[i].AttributeID + "\" type=\"text\" name=\"enddate\" id=\"item.endDate\" ng-change=\"changeperioddate_changed(item.endDate,fields.DatePartMinDate_" + fileUniqueID + $scope.atributesRelationList[i].AttributeID + ",fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + ",'EndDate')\" ng-model=\"item.endDate\" ng-click=\"PeriodCalanderopen($event,item,'end')\" datepicker-popup=\"{{format}}\"  is-open=\"item.calendopen\" min-date=\"fields.DatePartMinDate_" + fileUniqueID + attributelist[i].AttributeID + "\" min-date=\"fields.DatePartMaxDate_" + fileUniqueID + attributelist[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- End date --\"/><input class=\"dateComment txtbx\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileUniqueID + attributelist[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + attributelist[i].Caption + " Comment --\" />";
                    attrHTML += "</div><div class=\"buttonHolder span1\" ng-show=\"$first==false\">";
                    attrHTML += "<a ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew('" + fileUniqueID + "')\">[Add " + attributelist[i].Caption + "]</a></div></div>";
                    $scope.fields["Period_" + fileUniqueID + attributelist[i].AttributeID] = "";
                    $scope.fields["DatePart_Calander_Open" + "item.startDate"] = false;
                    $scope.fields["DatePart_Calander_Open" + "item.endDate"] = false;
                    $scope.setFieldKeys();
                } else if (attributelist[i].AttributeTypeID == 13) {
                    $scope.PercentageVisibleSettings["AttributeId_Levels_" + fileUniqueID + attributelist[i].AttributeID.toString() + ""] = true;
                    $scope.DropDownTreePricing["AttributeId_Levels_" + fileUniqueID + attributelist[i].AttributeID.toString() + ""] = attributelist[i].DropDownPricing;
                    attrHTML += "<div drowdowntreepercentagemultiselectionasset data-purpose='entity' data-fileid=" + fileUniqueID.toString() + " data-attributeid=" + attributelist[i].AttributeID.toString() + "></div>";
                } else if (attributelist[i].AttributeTypeID == 8) {
                    $scope.ShowHideAttributeOnRelation["Attribute_" + fileUniqueID + attributelist[i].AttributeID] = true;
                    attrHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileUniqueID + attributelist[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + fileUniqueID + attributelist[i].AttributeID + "\">" + attributelist[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileUniqueID + attributelist[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + fileUniqueID + attributelist[i].AttributeID + "\" id=\"TextSingleLine_" + fileUniqueID + attributelist[i].AttributeID + "\" placeholder=\"" + attributelist[i].PlaceHolderValue + "\"></div></div>";
                    $scope.fields["TextSingleLine_" + fileUniqueID + attributelist[i].AttributeID] = attributelist[i].DefaultValue;
                } else if (attributelist[i].AttributeTypeID == 17) {
                    $scope.fields["ListTagwords_" + fileUniqueID + attributelist[i].AttributeID] = [];
                    $scope.ShowHideAttributeOnRelation["Attribute_" + fileUniqueID + attributelist[i].AttributeID] = true;
                    $scope.OptionObj["tagoption_" + fileUniqueID + attributelist[i].AttributeID] = [];
                    $scope.setoptions();
                    $scope.tempscope = [];
                    attrHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileUniqueID + attributelist[i].AttributeID + "\" class=\"control-group\">";
                    attrHTML += "<label class=\"control-label\" for=\"fields.ListTagwords_ " + fileUniqueID + attributelist[i].AttributeID + "\">" + attributelist[i].AttributeCaption + " </label>";
                    attrHTML += "<div class=\"controls\">";
                    attrHTML += "<directive-tagwords item-attrid = \"" + fileUniqueID + attributelist[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + fileUniqueID + attributelist[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                    attrHTML += "</div></div>";
                    if (attributelist[i].InheritFromParent) { } else { }
                    $scope.setFieldKeys();
                } else if (attributelist[i].AttributeTypeID == 6) {
                    var totLevelCnt = attributelist[i].Levels.length;
                    for (var j = 0; j < attributelist[i].Levels.length; j++) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)] = true;
                        $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)] = {};
                        $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)].multiple = false;
                        $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                            return item.Caption
                        };
                        $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                            return item.Caption
                        };
                        if (j == 0) {
                            $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)].data = JSON.parse(attributelist[i].tree).Children;
                            attrHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + fileUniqueID + attributelist[i].ID + "\">" + attributelist[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                            attrHTML += "<input ui-select2=\"Dropdown.OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileUniqueID + attributelist[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attributelist[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6,'" + fileUniqueID + "' )\"  ng-model=\"fields.DropDown_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                        } else {
                            $scope.Dropdown["OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1)].data = [];
                            attrHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + fileUniqueID + attributelist[i].ID + "\">" + attributelist[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                            attrHTML += "<input ui-select2=\"Dropdown.OptionValues" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileUniqueID + attributelist[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attributelist[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 ,'" + fileUniqueID + "')\"  ng-model=\"fields.DropDown_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + fileUniqueID + attributelist[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                        }
                    }
                } else if (attributelist[i].AttributeTypeID == 7) {
                    $scope.fields["Tree_" + fileUniqueID + attributelist[i].AttributeID] = [];
                    $scope.ShowHideAttributeOnRelation["Attribute_" + fileUniqueID + attributelist[i].AttributeID] = true;
                    $scope.treesrcdirec["Attr_" + fileUniqueID + attributelist[i].AttributeID] = JSON.parse(attributelist[i].tree).Children;
                    if ($scope.treesrcdirec["Attr_" + fileUniqueID + attributelist[i].AttributeID].length > 0) {
                        treeTextVisbileflag = false;
                        if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + fileUniqueID + attributelist[i].AttributeID])) {
                            $scope.treePreviewObj["Attr_" + fileUniqueID + attributelist[i].AttributeID] = true;
                        } else $scope.treePreviewObj["Attr_" + fileUniqueID + attributelist[i].AttributeID] = false;
                    } else {
                        $scope.treePreviewObj["Attr_" + fileUniqueID + attributelist[i].AttributeID] = false;
                    }
                    attrHTML += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + fileUniqueID + attributelist[i].AttributeID + '\" class="control-group treeNode-control-group">';
                    attrHTML += '    <label class="control-label">' + attributelist[i].AttributeCaption + '</label>';
                    attrHTML += '    <div class="controls treeNode-controls">';
                    attrHTML += '        <div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + fileUniqueID + attributelist[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + fileUniqueID + attributelist[i].AttributeID + '"></div>';
                    attrHTML += '        <div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + fileUniqueID + attributelist[i].AttributeID + '">';
                    attrHTML += '            <span ng-if="doing_async">...loading...</span>';
                    attrHTML += '            <abn-tree tree-filter="filterValue_' + fileUniqueID + attributelist[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + fileUniqueID + attributelist[i].AttributeID + '\" accessable="' + attributelist[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                    attrHTML += '        </div>';
                    attrHTML += '        <div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + fileUniqueID + attributelist[i].AttributeID + '\">';
                    attrHTML += '            <div class="controls">';
                    attrHTML += '                <eu-tree fileid=\"' + fileUniqueID + '\"  tree-data=\"treesrcdirec.Attr_' + fileUniqueID + attributelist[i].AttributeID + '\" node-attributeid="' + attributelist[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                    attrHTML += '        </div></div>';
                    attrHTML += '</div></div>';
                } else if (attributelist[i].AttributeTypeID == 11) {

                    //StrartUpload_UploaderAttr();                  
                    $scope.ShowHideAttributeOnRelation["Attribute_" + attributelist[i].AttributeID] = true;
                    $scope.EnableDisableControlsHolder["Uploader_" + attributelist[i].AttributeID] = true;
                    $scope.OptionObj["option_" + attributelist[i].AttributeID] = attributelist[i].Options;
                    $scope.setoptions();
                    attrHTML += '<div class="control-group"><label class="control-label"';
                    attrHTML += 'for="fields.Uploader_ ' + attributelist[i].AttributeID + '">' + attributelist[i].AttributeCaption + ': </label>';
                    attrHTML += '<div id="Uploader" class="controls">';
                    if (attributelist[i].Value == "" || attributelist[i].Value == null && attributelist[i].Value == undefined) {
                        attributelist[i].Value = "NoThumpnail.jpg";
                    }
                    attrHTML += '<img src="' + imagesrcpath + 'UploadedImages/' + attributelist[i].Value + '" alt="' + attributelist[i].Caption + '" id="UploaderPreviewAssetCreation_' + attributelist[i].AttributeID + '" ';
                    attrHTML += 'class="entityDetailImgPreview ng-pristine ng-valid" ng-model="fields.Uploader_' + attributelist[i].AttributeID + '" id="UploaderImageControl">';
                    var curFiletempobj = {};
                    curFiletempobj.fileID = "'" + fileUniqueID.toString() + "'";
                    curFiletempobj.AttrID = attributelist[i].AttributeID;
                    attrHTML += '<br><a ng-show="EnableDisableControlsHolder.Uploader_' + attributelist[i].AttributeID + '" ng-model="UploadImage" ng-click="UploadImagefileAssetCreate(' + curFiletempobj.fileID + ',' + curFiletempobj.AttrID + ')" class="ng-pristine ng-valid">Select Image</a>';
                    attrHTML += '</div></div>';
                    $scope.fields["Uploader_" + attributelist[i].AttributeID] = "";
                    $scope.setFieldKeys();
                }
                if (attributelist[i].IsReadOnly == true) {
                    $scope.EnableDisableControlsHolder["Selection_" + fileUniqueID + attributelist[i].AttributeID] = true;
                } else {
                    $scope.EnableDisableControlsHolder["Selection_" + fileUniqueID + attributelist[i].AttributeID] = false;
                }
            }
            return attrHTML;
        }
        $scope.ReassignMembersData = [];
        $scope.formatResult = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="vertical-align: middle; margin-top: -4px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelection = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="vertical-align: middle; margin-top: -4px; background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        assetcreationPage(params.Option, params.Object);

        function assetcreationPage(option, obj) {
            $scope.Dam_Directory = obj.directory;
            $scope.AssetFilesObj = obj.AssetFilesObj;
            $scope.selectedTree = obj.selectedTree;
            $scope.processingsrcobj = obj.processingsrcobj;
            $scope.fnTimeOutfrAssetCreation(option);
        }
        $scope.$on('resetuploaderstatus', function (event) {
            $scope.enableassetcreationplUploader = false;
            $scope.task_enableassetcreationplUploader = false;
        })
        $scope.closepopupfunction = function () {
            $modalInstance.dismiss('cancel');
        }
        $scope.$on("$destroy", function () {
            $timeout.cancel(timeoutvariable)
            $scope.fileinfofrAssetThumbnailpreview = {
                fileuniqueid: [],
                filedets: [],
                fileobj: []
            };
            RecursiveUnbindAndRemove($("[ng-controller='mui.DAM.assetcreateCtrl']"));
            var cloudpath, providerresponse, assetType, ind, ind1, dom, count, namecount, input, fileToRemove, ste, damCount, selecteddamtype, wildcardExt, iswildcard, imagesrcpath, model, fileDescription, fileid, fileext, uploader, providerresponse, keyName, descHtml, damid, file, keySplit, fileName, uniqueKeypath, extension, resultArr, PreviewID, uniqueKey, TargetControl, mypage, myname, w, h, winprops, win, suffix, res, filecount, totalfilecount, TotalCount, fileUniqueID, friends, saveBlankAssetsCalled, ownerID, tempval, tempassty, i, r, k, j, d, x, o, m, v, a, tempattr, dataid, rec, htmltree, indofAssetType, metadata, TotalSize, UploadedSize, UploadedCount, img, oldAssettype, newAssettype, indofAssetType, attrreldata, indofnewAssetType, newHTML, keys, formatlen, defaultdateVal, AssetTypeID, AssetName, AssetFileGuid, AssetAttribute, value, multiselectiObject, MyDate, MyDateString, treenodes, levelCount, valueMatches, attributeLevelOptions, uuid, keys1, uniqueid, blankAssetsCnt, BlankAssetTypeID, BlankAssetName, blankassetAttributeData, treenodes, recursiveAttrID, optionValue, attributesToShow, hideAttributeOtherThanSelected, currntlevel, attrval, attrRelIDs, treeTextVisbileflag, enddate1, ItemCnt, apple_selected, tree, treedata_avm, treedata_geography, remainRecord, tempasst, attrHTML, defaultmultiselectvalue, temp, totLevelCnt1, totLevelCnt, markup, test, AssetTypeMetadataDets, FileAttributes = null;
        });
    }
    app.controller('mui.DAM.assetcreateCtrl', ['$window', '$location', '$timeout', '$scope', '$cookies', '$resource', '$compile', '$translate', 'AssetCreationService', '$modalInstance', 'params', muiDAMassetcreateCtrl]);
})(angular, app);