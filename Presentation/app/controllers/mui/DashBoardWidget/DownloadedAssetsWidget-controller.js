﻿(function(ng, app) {
	"use strict";

	function muiDashBoardWidgetDownloadedAssetsWidgetCtrl($scope, $timeout, $http,$compile, $resource, $cookies,$location, $window, DownloadedassetswidgetService) {
		$scope.DownloadedAssetFeedsdivID = "DownloadedAssetFeedsdiv_" + $scope.WizardID;
		$scope.DownloadedAssetFeedsContentdivID = "DownloadedAssetContentdiv_" + $scope.WizardID;
		$scope.WizardFeedsgroupid = '-1';
		$timeout(function () { LoadNewsFeedPublished(); }, 100);
		$scope.IsEntitypermissionExist = function (entityId, event) {
		    var isobjective = $(event.target).attr('isobjective');
		    var entitytypeid = parseInt($(event.target).attr('entitytypeid'));
		    DownloadedassetswidgetService.CheckUserPermissionForEntity(entityId,event).then(function (CheckUserPermissionForEntity) {
		        if (CheckUserPermissionForEntity.Response == true) {
		            if (isobjective == "true")
		                $location.path('/mui/objective/detail/section/' + entityId + '/overview');
		            else if (entitytypeid == 35)
		                $location.path('/mui/calender/detail/section/' + entityId + '/overview');
		            else if (entitytypeid == 5)
		                $location.path('/mui/planningtool/costcentre/detail/section/' + entityId + '/overview');
                        else
		            $location.path('/mui/planningtool/default/detail/section/' + entityId + '/overview');
		        }
		        else {
		            bootbox.alert($translate.instant('LanguageContents.Res_1867.Caption'));
		        }
		    });
		}
		function LoadNewsFeedPublished() {
			try {
				$('#' + $scope.DownloadedAssetFeedsdivID + '').html('');
				var feeddivHtml = '';
				$scope.DownloadedAssetFeedsResultset = [];
				DownloadedassetswidgetService.GettingAssetsFeedSelectionDashbord(10, 111, 0).then(function(downgetEntityNewsFeedResult) {
					$scope.DownloadedAssetFeedsResultset = downgetEntityNewsFeedResult.Response;
					for (var i = 0; i < downgetEntityNewsFeedResult.Response.length; i++) {
						feeddivHtml = feeddivHtml + '<li>';
						feeddivHtml = feeddivHtml + '    <div class="newsFeed">';
						feeddivHtml = feeddivHtml + '        <div class="userAvatar">';
						feeddivHtml = feeddivHtml + '            <div class="AssetImgContainer">';
						feeddivHtml = feeddivHtml + ' <img data-role="user-avatar" src=\'Handlers/UserImage.ashx?id=' + parseInt(downgetEntityNewsFeedResult.Response[i].Actor) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\'>'
						feeddivHtml = feeddivHtml + '           </div>';
						feeddivHtml = feeddivHtml + '        </div>';
						feeddivHtml = feeddivHtml + '        <div class="cmnt">';
						feeddivHtml = feeddivHtml + '           <div class="cmntHeader">';
						feeddivHtml = feeddivHtml + '               <h5>' + downgetEntityNewsFeedResult.Response[i].UserName + '</h5>';
						feeddivHtml = feeddivHtml + '            </div>';
						feeddivHtml = feeddivHtml + '            <div class="cmntContent">';
						if (downgetEntityNewsFeedResult.Response[i].Versionno > 1) {
						    feeddivHtml = feeddivHtml + '<p>Downloaded <a class="read-only">' + downgetEntityNewsFeedResult.Response[i].AssetName + '</a> Version ' + downgetEntityNewsFeedResult.Response[i].Versionno + ' in <a type="button" href="javascript:void(0);" ng-click="IsEntitypermissionExist(' + downgetEntityNewsFeedResult.Response[i].EntityID + ',$event)" EntitytypeId = ' + downgetEntityNewsFeedResult.Response[i].EntitytypeId + '  isobjective = ' + downgetEntityNewsFeedResult.Response[i].Isobjective + '>' + downgetEntityNewsFeedResult.Response[i].Entityname + '</a></p>';
						} else {
						    feeddivHtml = feeddivHtml + '<p>Downloaded <a class="read-only">' + downgetEntityNewsFeedResult.Response[i].AssetName + '</a> in <a type="button" href="javascript:void(0);" ng-click="IsEntitypermissionExist(' + downgetEntityNewsFeedResult.Response[i].EntityID + ',$event)" EntitytypeId = ' + downgetEntityNewsFeedResult.Response[i].EntitytypeId + ' isobjective = ' + downgetEntityNewsFeedResult.Response[i].Isobjective + '>' + downgetEntityNewsFeedResult.Response[i].Entityname + '</a></p>';
						}
						feeddivHtml = feeddivHtml + '            </div>';
						feeddivHtml = feeddivHtml + '            <div class="cmntFooter">';
						feeddivHtml = feeddivHtml + '                <span class="cmntTime">' + downgetEntityNewsFeedResult.Response[i].FeedHappendTime + '</span>';
						feeddivHtml = feeddivHtml + '            </div>';
						feeddivHtml = feeddivHtml + '        </div>';
						feeddivHtml = feeddivHtml + '    </div>';
						feeddivHtml = feeddivHtml + '</li>';
					}
					$('#' + $scope.DownloadedAssetFeedsdivID + '').html($compile(feeddivHtml)($scope));
					$('#' + $scope.DownloadedAssetFeedsdivID + '').parents('.box-content').find('.InProgress').remove();
				});
			} catch (e) {}
		}
		
		$scope.$on("$destroy", function() {
			RecursiveUnbindAndRemove($("[ng-controller='mui.DashBoardWidget.DownloadedAssetsWidgetCtrl']"));
		});
	}
	app.controller("mui.DashBoardWidget.DownloadedAssetsWidgetCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$location', '$window', 'DownloadedassetswidgetService', muiDashBoardWidgetDownloadedAssetsWidgetCtrl]);
	function DownloadedassetswidgetService($http, $q) {
	    $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
	        GettingAssetsFeedSelectionDashbord: GettingAssetsFeedSelectionDashbord,
	        CheckUserPermissionForEntity: CheckUserPermissionForEntity
	    });
	    function GettingAssetsFeedSelectionDashbord(Topx, FeedTemplateID, Newsfeedid) { var request = $http({ method: "get", url: "api/common/GettingAssetsFeedSelectionDashbord/" + Topx + "/" + FeedTemplateID + "/" + Newsfeedid, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
	    function CheckUserPermissionForEntity(EntitiyID) { var request = $http({ method: "get", url: "api/common/CheckUserPermissionForEntity/" + EntitiyID, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
	    function handleError(response) {
	        if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
	        return ($q.reject(response.data.message));
	    }
	    function handleSuccess(response) { return (response.data); }
	}
	app.service("DownloadedassetswidgetService", ['$http', '$q', DownloadedassetswidgetService]);
})(angular, app);