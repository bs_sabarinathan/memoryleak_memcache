﻿(function (ng, app) {

  //  "use strict";

    //app.controller(
	//	"mui.planningtool.costcentre.detail.workflowCtrl",
    //	function ($scope, $timeout, $http, requestContext, _, $compile, $resource) {
          
       function muiplanningtoolcostcentredetailworkflowCtrl($scope, $timeout, $http, requestContext, _, $compile, $resource) {


		    // --- Define Controller Methods. ------------------- //


		    // ...


		    // --- Define Scope Methods. ------------------------ //

		    // ...


		    // --- Define Controller Variables. ----------------- //


		    // Get the render context local to this controller (and relevant params).
		    var renderContext = requestContext.getRenderContext("mui.planningtool.costcentre.detail.section.workflow");


		    // --- Define Scope Variables. ---------------------- //


		    // The subview indicates which view is going to be rendered on the page.
		    $scope.subview = renderContext.getNextSection();


		    // --- Bind To Scope Events. ------------------------ //


		    // I handle changes to the request context.
		    $scope.$on(
				"requestContextChanged",
				function () {
				    // Make sure this change is relevant to this controller.
				    if (!renderContext.isChangeRelevant()) {
				        return;
				    }
				    // Update the view that is being rendered.
				    $scope.subview = renderContext.getNextSection();
				}
			);

		    $scope.$on("$destroy", function () {
		        RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.detail.workflowCtrl']"));
		    });
		    // --- Initialize. ---------------------------------- //

		    // ...


		}
       app.controller("mui.planningtool.costcentre.detail.workflowCtrl", [$scope, $timeout, $http, requestContext, _, $compile, $resource, muiplanningtoolcostcentredetailworkflowCtrl]);

})(angular, app);