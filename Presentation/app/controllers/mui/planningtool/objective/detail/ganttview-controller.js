﻿(function (ng, app) {
    "use strict";

    function muiplanningtoolobjectivedetailganttviewCtrl($scope, $timeout, $rootScope, $http, $resource, $compile, $window, $translate, ObjectiveganttviewService) {
        $("#plandetailfilterdiv").css('display', 'block')
        $scope.IsPageLoad = false;
        $scope.copyrightYear = (new Date()).getFullYear();
        $scope.OnganttPeriodChange = function () {
            $scope.IsPageLoad = true;
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.objective.detail.ganttviewCtrl']"));
        });

        $scope.Calanderopen = function ($event, fromplace, obj) {
            $event.preventDefault();
            $event.stopPropagation();
            if (fromplace == "Startdate") {
                obj.s_IsOpened = true;
                obj.e_IsOpened = false;
            }
            else {
                obj.s_IsOpened = false;
                obj.e_IsOpened = true;
            }
        };

        $scope.GanttDetail = {
            GanttStartDate: "",
            GanttEndDate: "",
            GanttDescription: ""
        }
        $scope.data = {
            start: "",
            end: "",
            desc: "",
            SortOid: "",
            order: "",
            uniqueId: "",
            id: ""
        }
        var dataGantEdit = {
            start: "",
            end: "",
            desc: "",
            SortOid: "",
            order: "",
            uniqueId: "",
            id: ""
        }
        var IsResized = false;
        $scope.GanttHeaderList = null;
        $scope.DrawHeaderBar = function (zoomlevel) {
            $scope.settings.Mode = zoomlevel;
            $scope.IsPageLoad = false;
            GetAllGanttHeaderBar();
            lazyLoadGantt();
        };

        function GetAllGanttHeaderBar() {
            ObjectiveganttviewService.GetAllGanttHeaderBar().then(function (getGanttresult) {
                if (getGanttresult.Response != null) {
                    $scope.GanttHeaderList = getGanttresult.Response;
                }
                drawHeader($scope.dates);
                ScrollToCurrentDate();
            });
        }
        $scope.GanttCurrentDateCollections = [{}];
        $scope.settings = {
            Mode: 'monthly',
            MonthlyDayWidth: 30,
            YearlyDayWidth: 2,
            QuarterlyDayWidth: 6,
            WeeklyDayWidth: 16,
            VerticallyConnected: null,
            HorizontallyConnected: null,
            DataColumnWidth: 100,
            StartDate: Date.parse('-1year').set({
                day: 1,
                month: 0
            }).toString('yyyy-MM-dd'),
            EndDate: Date.parse('+1year').set({
                day: 31,
                month: 11
            }).toString('yyyy-MM-dd'),
            behavior: {
                clickable: true,
                draggable: true,
                resizable: true,
                onClick: function (data, BlockID) {
                    if (IsResized == false) {
                        $scope.GanttCurrentDateCollections = {};
                        ObjectiveganttviewService.GetEntityPeriod(data.id).then(function (ganttDateRes) {
                            if (ganttDateRes.Response != null) {
                                $scope.GanttCurrentDateCollections = [{
                                    Description: "",
                                    EndDate: "",
                                    Entityid: 0,
                                    Id: 0,
                                    SortOrder: 0,
                                    Startdate: "",
                                    s_IsOpened: false,
                                    e_IsOpened: false
                                }]
                                $scope.GanttCurrentDateCollections.splice(0, 1);
                                for (var i = 0; i < ganttDateRes.Response.length; i++) {
                                    var datStartUTCval = "";
                                    var datstartval = "";
                                    var datEndUTCval = "";
                                    var datendval = "";
                                    datStartUTCval = ganttDateRes.Response[i].Startdate.substr(0, 10)
                                    datstartval = new Date.create((datStartUTCval));
                                    datEndUTCval = ganttDateRes.Response[i].EndDate.substr(0, 10)
                                    datendval = new Date.create((datEndUTCval));
                                    $scope.GanttCurrentDateCollections.push({
                                        Description: ganttDateRes.Response[i].Description,
                                        EndDate: (ConvertDateToString(datendval)),
                                        Entityid: ganttDateRes.Response[i].Entityid,
                                        Id: ganttDateRes.Response[i].Id,
                                        SortOrder: ganttDateRes.Response[i].SortOrder,
                                        Startdate: (ConvertDateToString(datstartval)),
                                        s_IsOpened: false,
                                        e_IsOpened: false
                                    });
                                }
                            }
                        });
                        dataGantEdit.SortOid = data.SortOid
                        dataGantEdit.order = data.order
                        dataGantEdit.uniqueId = data.uniqueId
                        dataGantEdit.id = data.id
                        dataGantEdit.BlockID = BlockID
                        $("#moduleContextGantt").modal('show');
                    }
                    IsResized = false;
                },
                onDrag: function (data) {
                    EntityPeriodUpdate(data);
                },
                onResize: function (data) {
                    EntityPeriodUpdate(data);
                    IsResized = true;
                }
            },
        };
        $scope.AddNextDate = function (index, ID) {
            $scope.GanttCurrentDateCollections.splice((index + 1), 0, {
                Description: "",
                EndDate: "",
                Entityid: 0,
                Id: 0,
                SortOrder: 0,
                Startdate: "",
                s_IsOpened: false,
                e_IsOpened: false
            });
        }
        $scope.RemoveDate = function (index, ID) {
            if ($scope.GanttCurrentDateCollections.length > 1) {
                if (ID > 0) {
                    $scope.deletePeriodDate(ID);
                }
                $scope.GanttCurrentDateCollections.splice(index, 1);
            } else {
                $scope.GanttCurrentDateCollections.splice(index, 1, {
                    Description: "",
                    EndDate: "",
                    Entityid: 0,
                    Id: 0,
                    SortOrder: 0,
                    Startdate: "",
                    s_IsOpened: false,
                    e_IsOpened: false
                });
            }
        }
        $scope.deletePeriodDate = function (periodid, ID) {
            bootbox.confirm($translate.instant('LanguageContents.Res_4090.Caption'), function (result) {
                if (result) {
                    ObjectiveganttviewService.DeleteEntityPeriod(periodid).then(function (deletePerById) {
                        if (deletePerById.StatusCode == 200) {
                            NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                            UpdateEntityPeriodTree(dataGantEdit.id);
                            $scope.GanttsaveDetail();
                        } else {
                            NotifySuccess($translate.instant('LanguageContents.Res_4316.Caption'));
                        }
                    });
                }
            });
        }

        function UpdateEntityPeriodTree(entityid) {
            ObjectiveganttviewService.GetEntitiPeriodByIdForGantt(entityid).then(function (resultperiod) {
                if (resultperiod.StatusCode == 200) {
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            if ($scope.ListViewDetails[i].data.Response.Data[j]["Period"] == undefined) {
                                return false;
                            } else {
                                if (entityid == $scope.ListViewDetails[i].data.Response.Data[j]["Id"]) {
                                    $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = resultperiod.Response;
                                    return false;
                                }
                            }
                        }
                    }
                }
            });
        }
        $scope.GanttRedrawDetail = function (data) {
            dataGantEdit.start = data.start;
            dataGantEdit.end = data.end;
            dataGantEdit.desc = data.desc;
            var block = $('.ganttview-data-block[data-dateblockuniqueid="' + data.order + '-' + data.uniqueId + '"]');
            block.attr("title", "<b>Name:</b> " + block.attr("data-name") + " </br><b>Start Date: </b>" + ConvertDateFromStringToString(data.start) + "</br><b>End Date:</b> " + ConvertDateFromStringToString(data.end) + " </br><b>Comment:</b> " + data.desc + "").qtip({
                content: {
                    text: "<b>Name:</b> " + block.attr("data-name") + " </br><b>Start Date: </b>" + data.start + "</br><b>End Date:</b> " + data.end + " </br><b>Comment:</b> " + data.desc + "",
                },
                style: {
                    classes: "qtip-dark qtip-shadow qtip-rounded",
                    title: {
                        'display': 'none'
                    }
                },
                show: {
                    event: 'mouseenter click unfocus',
                    solo: true
                },
                events: {
                    hide: function (event, api) {
                        event: 'unfocus click mouseleave mouseup mousedown'
                    }
                },
                hide: {
                    delay: 0,
                    fixed: false,
                    effect: function () {
                        $(this).fadeOut(100);
                    },
                    event: 'unfocus click mouseleave mouseup mousedown'
                },
                position: {
                    my: 'bottom left',
                    at: 'top left',
                    viewport: $(window),
                    adjust: {
                        method: 'shift',
                        mouse: true
                    },
                    corner: {
                        target: 'bottom left',
                        tooltip: 'bottom left',
                        mimic: 'top'
                    },
                    target: 'mouse'
                }
            });
        }
        $scope.GanttsaveDetail = function () {
            var data = dataGantEdit;
            if (DateCollectionValidate(data)) {
                var UpdateEntityPeriod = {};
                UpdateEntityPeriod.EntityID = data.id;
                for (var i = 0; i < $scope.GanttCurrentDateCollections.length; i++) {
                    if ($scope.GanttCurrentDateCollections[i].Startdate != "" && $scope.GanttCurrentDateCollections[i].EndDate != "") {
                        if (typeof ($scope.GanttCurrentDateCollections[i].Startdate) != "string")
                            $scope.GanttCurrentDateCollections[i].Startdate = dateFormat($scope.GanttCurrentDateCollections[i].Startdate, "yyyy-MM-dd");
                        if (typeof ($scope.GanttCurrentDateCollections[i].EndDate) != "string")
                            $scope.GanttCurrentDateCollections[i].EndDate = dateFormat($scope.GanttCurrentDateCollections[i].EndDate, "yyyy-MM-dd");
                    }
                    else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1142.Caption'));
                        return false;
                    }
                }
                UpdateEntityPeriod.GanttCurrentDateCollections = $scope.GanttCurrentDateCollections;
                ObjectiveganttviewService.InsertUpdateEntityPeriodLst(UpdateEntityPeriod).then(function (EntityPeriodResult) {
                    if (EntityPeriodResult.Response.length > 0) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
                        $("#moduleContextGantt").modal('hide');
                        var constructUniquekey = '';
                        var CurrentUniquekey = '';
                        for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                            for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                                CurrentUniquekey = $scope.ListViewDetails[i].data.Response.Data[j]["UniqueKey"];
                                if ($scope.ListViewDetails[i].data.Response.Data[j]["TypeID"] == 10) {
                                    constructUniquekey = CurrentUniquekey + "-";
                                    CurrentUniquekey = '';
                                }
                                var UniqueKey = constructUniquekey + UniqueKEY(CurrentUniquekey);
                                if (UniqueKey == data.uniqueId) {
                                    $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = EntityPeriodResult.Response;
                                    var ganttDrawHtml = drawSpecifiedBlock($scope.ListViewDetails[i].data.Response.Data[j]);
                                    $('#ganttChart .ganttview-data-block-container[data-uniquekey="' + data.uniqueId + '"]').html(ganttDrawHtml);
                                    return false;
                                }
                            }
                        }
                    }
                    else {
                        NotifyError($translate.instant('LanguageContents.Res_4351.Caption'));
                        $("#moduleContextGantt").modal('hide');
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1903.Caption'));
            }
        }

        function DateCollectionValidate() {
            var IsOverlap = false;
            if ($scope.GanttCurrentDateCollections.length > 1) {
                for (var i = 1; i < $scope.GanttCurrentDateCollections.length; i++) {
                    if (i != $scope.GanttCurrentDateCollections.length - 1) {
                        if ($scope.GanttCurrentDateCollections[i].Startdate < $scope.GanttCurrentDateCollections[i - 1].EndDate && $scope.GanttCurrentDateCollections[i].EndDate > $scope.GanttCurrentDateCollections[i + 1].Startdate) {
                            return IsOverlap
                        }
                    } else {
                        if ($scope.GanttCurrentDateCollections[i].Startdate < $scope.GanttCurrentDateCollections[i - 1].EndDate) {
                            return IsOverlap
                        }
                    }
                }
            }
            return true;
        }

        function drawSpecifiedBlock(data) {
            var blockDivPage = '';
            var blockDivDataPage = '';
            var UniqueId = UniqueKEY(data["UniqueKey"]);
            var ClassName = GenateClass(UniqueId);
            ClassName += " mo" + UniqueId;
            blockDivPage += "<div data-over='true' data-EntityLevel='" + data["Level"] + "' data-uniquekey='" + UniqueId + "' class='" + ClassName + " ganttview-data-block-container'>";
            $($.parseXML(data['Period'])).find('p').each(function () {
                var startDt = $(this).attr('s');
                var endDt = $(this).attr('e');
                var duration = 0;
                var formLeft = 0;
                switch ($scope.settings.Mode) {
                    case 'daily':
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                        break;
                    case 'monthly':
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.YearlyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth);
                        break;
                    case 'quarterly':
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.QuarterlyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth);
                        break;
                    case 'weekly':
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.WeeklyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth);
                        break;
                    default:
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                        break;
                }
                startDt = ConvertDateFromStringToString(startDt);
                endDt = ConvertDateFromStringToString(endDt);
                if ($scope.GanttLock == true) {
                    if (data['Permission'] == 3 || data['Permission'] == 0) {
                        blockDivPage += "<div data-name='" + data["Name"].replace(/["']/g, "") + "'  data-DateBlockUniqueId='" + $(this).attr("o") + '-' + UniqueId + "'  title='<b>Name:</b> " + data["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block disableResize-GanttBlock disableDraggable-GanttBlock' data-id='" + data['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                            id: parseInt(data['Id']),
                            order: parseInt($(this).attr("o")),
                            start: $(this).attr("s"),
                            end: $(this).attr("e"),
                            desc: $(this).attr("d"),
                            uniqueId: UniqueId,
                            SortOid: $(this).attr("sid")
                        }) + "'></div>";
                    } else {
                        blockDivPage += "<div data-name='" + data["Name"].replace(/["']/g, "") + "'  data-DateBlockUniqueId='" + $(this).attr("o") + '-' + UniqueId + "'  title='<b>Name:</b> " + data["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block disableResize-GanttBlock disableDraggable-GanttBlock Ganttlockdummy' data-id='" + data['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                            id: parseInt(data['Id']),
                            order: parseInt($(this).attr("o")),
                            start: $(this).attr("s"),
                            end: $(this).attr("e"),
                            desc: $(this).attr("d"),
                            uniqueId: UniqueId,
                            SortOid: $(this).attr("sid")
                        }) + "'></div>";
                    }
                } else {
                    blockDivPage += "<div data-name='" + data["Name"].replace(/["']/g, "") + "'  data-DateBlockUniqueId='" + $(this).attr("o") + '-' + UniqueId + "'  title='<b>Name:</b> " + data["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block' data-id='" + data['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                        id: parseInt(data['Id']),
                        order: parseInt($(this).attr("o")),
                        start: $(this).attr("s"),
                        end: $(this).attr("e"),
                        desc: $(this).attr("d"),
                        uniqueId: UniqueId,
                        SortOid: $(this).attr("sid")
                    }) + "'></div>";
                }
            });
            $($.parseXML(data['MileStone'])).find('p').each(function () {
                var startDt = $(this).attr('s');
                var MileStoneStatus = $(this).attr('ms');
                var MileStoneName = $(this).attr('n');
                var formLeft = 0;
                switch ($scope.settings.Mode) {
                    case 'daily':
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth) + 3;
                        break;
                    case 'monthly':
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth) + 3;
                        break;
                    case 'quarterly':
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth) + 3;
                        break;
                    case 'weekly':
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth) + 3;
                        break;
                    default:
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth) + 3;
                        break;
                }
                if (startDt != undefined) {
                    var startDtFormat = ConvertDateFromStringToString(startDt);
                }
                var MileStoneStatusText = (MileStoneStatus == 1 ? "Reached" : "Not reached");
                blockDivPage += "<span title='<b>Name: </b> " + MileStoneName.replace(/["']/g, "") + " </br><b>Milestone Date: </b>" + startDtFormat + "</br><b>Descripton: </b>" + $(this).attr('d') + "</br><b>Milestone Status: </b>" + MileStoneStatusText + "'   style='margin-left: " + formLeft + "px;' class=" + (MileStoneStatus == 1 ? 'reached' : 'notreached') + " ><i class='icon-star'></i></span>";
            });
            blockDivPage += '</div>';
            return blockDivPage;
        }

        function EntityPeriodUpdate(data) {
            var UpdateEntityPeriod = {};
            UpdateEntityPeriod.ID = data.order;
            UpdateEntityPeriod.StartDate = data.start;
            UpdateEntityPeriod.EndDate = data.end;
            UpdateEntityPeriod.Description = data.desc;
            ObjectiveganttviewService.UpdateEntityPeriod(UpdateEntityPeriod).then(function (EntityPeriodResult) {
                if (EntityPeriodResult.Response.length > 0) {
                    var constructUniquekey = '';
                    var CurrentUniquekey = '';
                    for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                        for (var j = 0; j < $scope.ListViewDetails[i].data.Response.Data.length; j++) {
                            CurrentUniquekey = $scope.ListViewDetails[i].data.Response.Data[j]["UniqueKey"];
                            if ($scope.ListViewDetails[i].data.Response.Data[j]["TypeID"] == 10) {
                                constructUniquekey = CurrentUniquekey + "-";
                                CurrentUniquekey = '';
                            }
                            var UniqueKey = constructUniquekey + UniqueKEY(CurrentUniquekey);
                            if (UniqueKey == data.uniqueId) {
                                $scope.ListViewDetails[i].data.Response.Data[j]["Period"] = EntityPeriodResult.Response;
                                $scope.GanttRedrawDetail(data);
                                return false;
                            }
                        }
                    }
                }
            });
        }
        $scope.dates = getDates($scope.settings.StartDate, $scope.settings.EndDate);
        var colStatus = false;
        GanttViewLoad();
        $scope.GanttViewHeaders = {};
        $("#EntitiesTree").on("onListViewTemplateCreation", function (event, GanttViewDataPageTemplate, GanttViewBlockPageTemplate) {
            GanttViewLoad()
            $(window).AdjustHeightWidth();
        });

        function GanttViewLoad() {
            if ($("#EntitiesTree li").hasClass('active')) $("#EntitiesTree li.active").removeClass('active');
            if ($scope.ListTemplate.length > 0) {
                ObjectiveganttviewService.ListSetting("GanttView", 10).then(function (ganttsetting) {
                    if (ganttsetting.Response.Attributes != null) {
                        $("#ganttChart #GanttDataContainer").html($scope.GanttViewDataPageTemplate);
                        $("#ganttChart #GanttBlockContainer").html($scope.GanttViewBlockPageTemplate);
                        GetAllGanttHeaderBar();
                        $('#ganttChart .ganttview-data').scroll(function () {
                            $("#ganttChart .ganttview-slide-container").scrollLeft($(this).scrollLeft());
                            $("#ganttChart .ganttview-list-container-data").scrollTop($(this).scrollTop());
                            $("#EntitiesTree").scrollTop($(this).scrollTop());
                        });
                        $scope.GanttViewHeaders = ganttsetting.Response.Attributes;
                        drawDataHeader(ganttsetting.Response.Attributes);
                        for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                            LoadGanttView($scope.ListViewDetails[i].data, $scope.ListViewDetails[i].PageIndex, $scope.ListViewDetails[i].UniqueID);
                        }
                        ScrollToCurrentDate();
                    }
                });
            }
            $(window).AdjustHeightWidth();
        }
        $("#EntitiesTree").on('onListViewTemplateCreation', function (event, ListTemplate) {
            $("#ListHolder").html(ListTemplate);
        });

        function ScrollToCurrentDate() {
            var ToDay;
            var DaysToScroll;
            switch ($scope.settings.Mode) {
                case 'daily':
                    ToDay = new Date();
                    DaysToScroll = (Math.round((ToDay - GetCalenderStartDate()) / (1000 * 60 * 60 * 24)) - 1) * $scope.settings.MonthlyDayWidth;
                    break;
                case 'quarterly':
                    ToDay = Date.today().set({
                        day: 1
                    });
                    DaysToScroll = (Math.round((ToDay - GetCalenderStartDate()) / (1000 * 60 * 60 * 24)) - 1) * $scope.settings.QuarterlyDayWidth;
                    break;
                case 'weekly':
                    ToDay = Date.today().set({
                        day: 1
                    });
                    DaysToScroll = (Math.round((ToDay - GetCalenderStartDate()) / (1000 * 60 * 60 * 24)) - 1) * $scope.settings.WeeklyDayWidth;
                    break;
                default:
                    ToDay = Date.today().set({
                        day: 1
                    });
                    DaysToScroll = (Math.round((ToDay - GetCalenderStartDate()) / (1000 * 60 * 60 * 24)) - 1) * $scope.settings.YearlyDayWidth;
                    break;
            }
            $(".ganttview-data").scrollLeft(DaysToScroll);
        }

        function GetCalenderStartDate() {
            return new Date(parseInt((new Date).getFullYear()) - 1, 0, 1);
        }
        $scope.$on('onTreePageCreation', function (event, data) {
            LoadGanttView(data[0], data[1], data[2]);
        });

        function LoadGanttView(ListContentData, PageIndex, UniqueID) {
            var value = drawBlock(ListContentData.Response, UniqueID);
            if (UniqueID != "") {
                $('#ganttChart .ganttview-data-block-container[data-uniquekey="' + UniqueID + '"]').after(value.Block);
                $('#ganttChart tr[data-uniquekey="' + UniqueID + '"]').after(value.Data);
            } else {
                $('#ganttChart .ganttview-data-blocks-page[data-page="' + PageIndex + '"]').html(value.Block).removeAttr('style');
                $('#ganttChart tbody[data-page="' + PageIndex + '"]').html(value.Data).removeAttr('style');
            }
            $(window).AdjustHeightWidth();
            $(function () {
                $(".ganttview-data-block, .reached, .notreached ").qtip({
                    style: {
                        classes: "qtip-dark qtip-shadow qtip-rounded",
                        title: {
                            'display': 'none'
                        }
                    },
                    show: {
                        event: 'mouseenter click unfocus',
                        solo: true
                    },
                    events: {
                        hide: function (event, api) {
                            event: 'unfocus click mouseleave mouseup mousedown'
                        }
                    },
                    hide: {
                        delay: 0,
                        fixed: false,
                        effect: function () {
                            $(this).fadeOut(100);
                        },
                        event: 'unfocus click mouseleave mouseup mousedown'
                    },
                    position: {
                        my: 'bottom left',
                        at: 'top left',
                        viewport: $(window),
                        adjust: {
                            method: 'shift',
                            mouse: true
                        },
                        corner: {
                            target: 'bottom left',
                            tooltip: 'bottom left',
                            mimic: 'top'
                        },
                        target: 'mouse'
                    }
                });
            });
            $timeout(function () {
                CallBehavior();
            }, 1000);
        }

        function CallBehavior() {
            apply($('#ganttChart'), $scope.settings);
        }

        function UniqueKEY(UniKey) {
            var substr = UniKey.split('.');
            var id = "";
            var row = substr.length;
            if (row > 1) {
                for (var i = 0; i < row; i++) {
                    id += substr[i];
                    if (i != row - 1) {
                        id += "-";
                    }
                }
                return id;
            }
            return UniKey;
        }

        function GenateClass(UniKey) {
            var ToLength = UniKey.lastIndexOf('-');
            if (ToLength == -1) {
                return "";
            }
            var id = UniKey.substring(0, ToLength);
            var afterSplit = id.split('-');
            var result = "";
            for (var i = 0; i < afterSplit.length; i++) {
                result += SplitClss(id, i + 1);
            }
            return result;
        }

        function SplitClss(NewId, lengthofdata) {
            var afterSplit = NewId.split('-');
            var finalresult = " p";
            for (var i = 0; i < lengthofdata; i++) {
                finalresult += afterSplit[i];
                if (i != lengthofdata - 1) {
                    finalresult += "-"
                }
            }
            return finalresult;
        }

        function daysBetween(start, end) {
            if (!start || !end) {
                return 0;
            }
            start = Date.parse(start);
            end = Date.parse(end);
            if (start.getYear() == 1901 || end.getYear() == 8099) {
                return 0;
            }
            return Math.round((end - start) / (1000 * 60 * 60 * 24));
        }

        function isWeekend(date) {
            return date.getDay() % 6 == 0;
        }

        function getWeek(d) {
            var target = new Date(d.valueOf());
            var dayNr = (d.getDay() + 6) % 7;
            target.setDate(target.getDate() - dayNr + 3);
            var jan4 = new Date(target.getFullYear(), 0, 4);
            var dayDiff = (target - jan4) / 86400000;
            var weekNr = 1 + Math.ceil(dayDiff / 7);
            return weekNr;
        }

        function getDates(start, end) {
            start = Date.parse(start);
            end = Date.parse(end);
            var dates = [];
            dates[start.getFullYear()] = [];
            dates[start.getFullYear()][start.getMonth()] = [start];
            var last = start;
            while (last.compareTo(end) == -1) {
                var next = last.clone().addDays(1);
                if (!dates[next.getFullYear()]) {
                    dates[next.getFullYear()] = [];
                }
                if (!dates[next.getFullYear()][next.getMonth()]) {
                    dates[next.getFullYear()][next.getMonth()] = [];
                }
                dates[next.getFullYear()][next.getMonth()].push(next);
                last = next;
            }
            return dates;
        }

        function lazyLoadGantt() {
            for (var i = 0; i < $scope.ListViewDetails.length; i++) {
                LoadGanttView($scope.ListViewDetails[i].data, $scope.ListViewDetails[i].PageIndex, $scope.ListViewDetails[i].UniqueID);
            }
        }

        function drawHeader(dates) {
            var totalW = 0;
            var mainHeaderDivContent = '';
            var subHeaderDivContent = '';
            var rowDivContent = '';
            var ganttHeaderBar = '';
            switch ($scope.settings.Mode) {
                case 'daily':
                    for (var y in dates) {
                        if (!isNaN(y)) {
                            for (var m in dates[y]) {
                                if (!isNaN(m)) {
                                    var w = dates[y][m].length * $scope.settings.MonthlyDayWidth;
                                    totalW = totalW + w;
                                    mainHeaderDivContent += '<div class="ganttview-header-main" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM - yyyy') + '</div>';
                                    for (var d in dates[y][m]) {
                                        if (!isNaN(d)) {
                                            if (isWeekend(dates[y][m][d])) {
                                                subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd">' + dates[y][m][d].getDate() + '</div>';
                                                rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + ($scope.settings.MonthlyDayWidth - 1) + 'px;"></div>';
                                            } else {
                                                subHeaderDivContent += '<div class="ganttview-header-sub">' + dates[y][m][d].getDate() + '</div>';
                                                rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + ($scope.settings.MonthlyDayWidth - 1) + 'px;"></div>';
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    for (var x in $scope.GanttHeaderList) {
                        var startDt = $scope.GanttHeaderList[x].Startdate;
                        var endDt = $scope.GanttHeaderList[x].EndDate;
                        var duration = 0;
                        var formLeft = 0;
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth);
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                        var name = $scope.GanttHeaderList[x].Name != undefined ? $scope.GanttHeaderList[x].Name.replace(/["']/g, "") : "";
                        ganttHeaderBar += '<div class="qtip-header-block" title="<b>Name:' + name + '</b></br><b>Start Date:' + $scope.GanttHeaderList[x].Startdate + ' </b></br><b>End Date:' + $scope.GanttHeaderList[x].EndDate + '</b> </br><b>Descripton: ' + $scope.GanttHeaderList[x].Description + '</b>"  style="background: #' + $scope.GanttHeaderList[x].ColorCode + ';width: ' + duration + 'px; left: ' + formLeft + 'px;"></div>';
                    }
                    break;
                case 'monthly':
                    var possion = 1;
                    for (var y in dates) {
                        if (!isNaN(y)) {
                            var yw = 0;
                            for (var m in dates[y]) {
                                if (!isNaN(m)) {
                                    var w = dates[y][m].length * $scope.settings.YearlyDayWidth;
                                    totalW = totalW + w;
                                    yw = yw + w;
                                    if ((possion % 2) == 1) {
                                        subHeaderDivContent += '<div class="ganttview-header-sub" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM') + '</div>';
                                        rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + (w - 1) + 'px;"></div>';
                                    } else {
                                        subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM') + '</div>';
                                        rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + (w - 1) + 'px;"></div>';
                                    }
                                    possion += 1;
                                }
                            }
                            mainHeaderDivContent += '<div class="ganttview-header-main" style="width: ' + (yw - 1) + 'px;">' + y + '</div>';
                        }
                    }
                    for (var x in $scope.GanttHeaderList) {
                        var startDt = $scope.GanttHeaderList[x].Startdate;
                        var endDt = $scope.GanttHeaderList[x].EndDate;
                        var duration = 0;
                        var formLeft = 0;
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.YearlyDayWidth);
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth);
                        var name = $scope.GanttHeaderList[x].Name != undefined ? $scope.GanttHeaderList[x].Name.replace(/["']/g, "") : "";
                        ganttHeaderBar += '<div class="qtip-header-block" title="<b>Name:' + name + '</b></br><b>Start Date:' + $scope.GanttHeaderList[x].Startdate + ' </b></br><b>End Date:' + $scope.GanttHeaderList[x].EndDate + '</b> </br><b>Descripton: ' + $scope.GanttHeaderList[x].Description + '</b>"  style="background: #' + $scope.GanttHeaderList[x].ColorCode + ';width: ' + duration + 'px; left: ' + formLeft + 'px;"></div>';
                    }
                    break;
                case 'quarterly':
                    var possion = 1;
                    for (var y in dates) {
                        if (!isNaN(y)) {
                            var yw = 0;
                            var Qno = 0;
                            for (var m in dates[y]) {
                                if (!isNaN(m)) {
                                    var w = dates[y][m].length * $scope.settings.QuarterlyDayWidth;
                                    totalW = totalW + w;
                                    yw = yw + w;
                                    if ((possion % 2) == 1) {
                                        subHeaderDivContent += '<div class="ganttview-header-sub" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM') + '</div>';
                                        rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + (w - 1) + 'px;"></div>';
                                    } else {
                                        subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM') + '</div>';
                                        rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + (w - 1) + 'px;"></div>';
                                    }
                                    possion += 1;
                                    if (Qno != Math.floor((parseInt(m) + 1) / 3)) {
                                        mainHeaderDivContent += '<div class="ganttview-header-main" style="width: ' + (yw - 1) + 'px;">Q' + (Qno + 1) + ' - ' + y + '</div>';
                                        yw = 0;
                                        Qno = Math.floor((parseInt(m) + 1) / 3);
                                    }
                                }
                            }
                        }
                    }
                    for (var x in $scope.GanttHeaderList) {
                        var startDt = $scope.GanttHeaderList[x].Startdate;
                        var endDt = $scope.GanttHeaderList[x].EndDate;
                        var duration = 0;
                        var formLeft = 0;
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.QuarterlyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth);
                        var name = $scope.GanttHeaderList[x].Name != undefined ? $scope.GanttHeaderList[x].Name.replace(/["']/g, "") : "";
                        ganttHeaderBar += '<div class="qtip-header-block" title="<b>Name:' + name + '</b></br><b>Start Date:' + $scope.GanttHeaderList[x].Startdate + ' </b></br><b>End Date:' + $scope.GanttHeaderList[x].EndDate + '</b> </br><b>Descripton: ' + $scope.GanttHeaderList[x].Description + '</b>"  style="background: #' + $scope.GanttHeaderList[x].ColorCode + ';width: ' + duration + 'px; left: ' + formLeft + 'px;"></div>';
                    }
                    break;
                case 'weekly':
                    var possion = 1;
                    var weekno = getWeek(Date.parse($scope.settings.StartDate));
                    var weekLen = 0;
                    for (var y in dates) {
                        if (!isNaN(y)) {
                            for (var m in dates[y]) {
                                if (!isNaN(m)) {
                                    var w = dates[y][m].length * $scope.settings.WeeklyDayWidth;
                                    totalW = totalW + w;
                                    mainHeaderDivContent += '<div class="ganttview-header-main" style="width: ' + (w - 1) + 'px;">' + Date.parse(y + '-' + (parseInt(m) + 1) + '-1').toString('MMM - yyyy') + '</div>';
                                    for (var d in dates[y][m]) {
                                        if (!isNaN(d)) {
                                            if (weekno != getWeek(dates[y][m][d])) {
                                                if ((possion % 2) == 1) {
                                                    subHeaderDivContent += '<div class="ganttview-header-sub" style="width: ' + (weekLen - 1) + 'px;">' + weekno + '</div>';
                                                    rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + (weekLen - 1) + 'px;"></div>';
                                                } else {
                                                    subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd" style="width: ' + (weekLen - 1) + 'px;">' + weekno + '</div>';
                                                    rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + (weekLen - 1) + 'px;"></div>';
                                                }
                                                possion += 1;
                                                weekno = getWeek(dates[y][m][d]);
                                                weekLen = 0;
                                            }
                                            weekLen += $scope.settings.WeeklyDayWidth;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if ((possion % 2) == 1) {
                        subHeaderDivContent += '<div class="ganttview-header-sub" style="width: ' + (weekLen - 1) + 'px;">' + weekno + '</div>';
                        rowDivContent += '<div class="ganttview-grid-row-cell" style="width: ' + (weekLen - 1) + 'px;"></div>';
                    } else {
                        subHeaderDivContent += '<div class="ganttview-header-sub ganttview-odd" style="width: ' + (weekLen - 1) + 'px;">' + weekno + '</div>';
                        rowDivContent += '<div class="ganttview-grid-row-cell ganttview-odd" style="width: ' + (weekLen - 1) + 'px;"></div>';
                    }
                    for (var x in $scope.GanttHeaderList) {
                        var startDt = $scope.GanttHeaderList[x].Startdate;
                        var endDt = $scope.GanttHeaderList[x].EndDate;
                        var duration = 0;
                        var formLeft = 0;
                        duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.WeeklyDayWidth) - 3;
                        formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth);
                        var name = $scope.GanttHeaderList[x].Name != undefined ? $scope.GanttHeaderList[x].Name.replace(/["']/g, "") : "";
                        ganttHeaderBar += '<div class="qtip-header-block" title="<b>Name:' + name + '</b></br><b>Start Date:' + $scope.GanttHeaderList[x].Startdate + ' </b></br><b>End Date:' + $scope.GanttHeaderList[x].EndDate + '</b> </br><b>Descripton: ' + $scope.GanttHeaderList[x].Description + '</b>"  style="background: #' + $scope.GanttHeaderList[x].ColorCode + ';width: ' + duration + 'px; left: ' + formLeft + 'px;"></div>';
                    }
                    break;
                default:
            }
            $('#ganttChart .ganttview-header .ganttview-header-mains').html(mainHeaderDivContent);
            $('#ganttChart .ganttview-header .ganttview-header-subs').html(subHeaderDivContent);
            $('#ganttChart .ganttview-header .ganttview-header-block').html(ganttHeaderBar);
            $('#ganttChart .ganttview-header').css("width", totalW + "px");
            $('#ganttChart .ganttview-grid').css("width", totalW + "px");
            $('#ganttChart .ganttview-grid-row').html(rowDivContent);
            $('#ganttChart #GanttBlockContainer').css("width", totalW + "px");
            $(function () {
                $(".qtip-header-block").qtip({
                    style: {
                        classes: "qtip-dark qtip-shadow qtip-rounded"
                    },
                    show: {
                        event: 'mouseenter click unfocus',
                        solo: true
                    },
                    events: {
                        hide: function (event, api) {
                            event: 'unfocus click mouseleave mouseup mousedown'
                        }
                    },
                    hide: {
                        delay: 0,
                        fixed: false,
                        effect: function () {
                            $(this).fadeOut(100);
                        },
                        event: 'unfocus click mouseleave mouseup mousedown'
                    },
                    position: {
                        my: 'bottom left',
                        at: 'top left',
                        viewport: $(window),
                        adjust: {
                            method: 'shift',
                            mouse: true
                        },
                        corner: {
                            target: 'bottom left',
                            tooltip: 'bottom left',
                            mimic: 'top'
                        },
                        target: 'mouse'
                    }
                });
            });
        }

        function drawBlock(data, UniqueID) {
            var blockDivPage = '';
            var blockDivDataPage = '';
            for (var i = 0; i < data.Data.length; i++) {
                var UniqueId = '';
                var Isobjective = false;
                var GeneralUniquekey = UniqueKEY(data.Data[i]["class"].toString());
                var ClassName = GenateClass(GeneralUniquekey);
                ClassName += " mo" + GeneralUniquekey;
                if (data.Data[i]["TypeID"] == 10) {
                    Isobjective = true;
                }
                blockDivPage += "<div data-isobjective='" + Isobjective + "' data-EntityLevel='" + data.Data[i]["Level"] + "' data-over='true' data-uniquekey='" + GeneralUniquekey + "' class='" + ClassName + " ganttview-data-block-container'>";
                $($.parseXML(data.Data[i]['Period'])).find('p').each(function () {
                    var startDt = $(this).attr('s');
                    var endDt = $(this).attr('e');
                    var duration = 0;
                    var formLeft = 0;
                    switch ($scope.settings.Mode) {
                        case 'daily':
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                            break;
                        case 'monthly':
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.YearlyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth);
                            break;
                        case 'quarterly':
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.QuarterlyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth);
                            break;
                        case 'weekly':
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.WeeklyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth);
                            break;
                        default:
                            duration = ((daysBetween(startDt, endDt) + 1) * $scope.settings.MonthlyDayWidth) - 3;
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth);
                            break;
                    }
                    startDt = ConvertDateFromStringToString(startDt);
                    endDt = ConvertDateFromStringToString(endDt);
                    if ($scope.GanttLock == true) {
                        if (data.Data[i]['Permission'] == 3 || data.Data[i]['Permission'] == 0) {
                            blockDivPage += "<div data-DateBlockUniqueId='" + $(this).attr("o") + '-' + GeneralUniquekey + "'  title='<b>Name:</b> " + data.Data[i]["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block disableResize-GanttBlock disableDraggable-GanttBlock' data-id='" + data.Data[i]['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data.Data[i]["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                                id: parseInt(data.Data[i]['Id']),
                                order: parseInt($(this).attr("o")),
                                start: $(this).attr("s"),
                                end: $(this).attr("e"),
                                desc: $(this).attr("d"),
                                uniqueId: GeneralUniquekey,
                                SortOid: $(this).attr("sid")
                            }) + "'></div>";
                        } else {
                            blockDivPage += "<div data-DateBlockUniqueId='" + $(this).attr("o") + '-' + GeneralUniquekey + "'  title='<b>Name:</b> " + data.Data[i]["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block disableResize-GanttBlock disableDraggable-GanttBlock Ganttlockdummy' data-id='" + data.Data[i]['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data.Data[i]["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                                id: parseInt(data.Data[i]['Id']),
                                order: parseInt($(this).attr("o")),
                                start: $(this).attr("s"),
                                end: $(this).attr("e"),
                                desc: $(this).attr("d"),
                                uniqueId: GeneralUniquekey,
                                SortOid: $(this).attr("sid")
                            }) + "'></div>";
                        }
                    } else {
                        blockDivPage += "<div data-DateBlockUniqueId='" + $(this).attr("o") + '-' + GeneralUniquekey + "'  title='<b>Name:</b> " + data.Data[i]["Name"].replace(/["']/g, "") + " </br><b>Start Date: </b>" + startDt + "</br><b>End Date:</b> " + endDt + " </br><b>Comment:</b> " + $(this).attr("d") + "' class='ganttview-data-block' data-id='" + data.Data[i]['Id'] + "' style='width: " + duration + "px; margin-left: " + formLeft + "px; background-color: #" + data.Data[i]["ColorCode"].trim() + ";' block-data='" + JSON.stringify({
                            id: parseInt(data.Data[i]['Id']),
                            order: parseInt($(this).attr("o")),
                            start: $(this).attr("s"),
                            end: $(this).attr("e"),
                            desc: $(this).attr("d"),
                            uniqueId: GeneralUniquekey,
                            SortOid: $(this).attr("sid")
                        }) + "'></div>";
                        updateganttlocksettings();
                    }
                });
                $($.parseXML(data.Data[i]['MileStone'])).find('p').each(function () {
                    var startDt = $(this).attr('s');
                    var MileStoneStatus = $(this).attr('ms');
                    var MileStoneName = $(this).attr('n');
                    var formLeft = 0;
                    switch ($scope.settings.Mode) {
                        case 'daily':
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth) + 3;
                            break;
                        case 'monthly':
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.YearlyDayWidth) + 3;
                            break;
                        case 'quarterly':
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.QuarterlyDayWidth) + 3;
                            break;
                        case 'weekly':
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.WeeklyDayWidth) + 3;
                            break;
                        default:
                            formLeft = (daysBetween($scope.settings.StartDate, startDt) * $scope.settings.MonthlyDayWidth) + 3;
                            break;
                    }
                    if (startDt != undefined) {
                        var startDtFormat = ConvertDateFromStringToString(startDt);
                    }
                    var MileStoneStatusText = (MileStoneStatus == 1 ? "Reached" : "Not reached");
                    blockDivPage += "<span title='<b>Name: </b> " + MileStoneName.replace(/["']/g, "") + " </br><b>Milestone Date: </b>" + startDtFormat + "</br><b>Descripton: </b>" + $(this).attr('d') + "</br><b>Milestone Status: </b>" + MileStoneStatusText + "'   style='margin-left: " + formLeft + "px;' class=" + (MileStoneStatus == 1 ? 'reached' : 'notreached') + " ><i class='icon-star'></i></span>";
                });
                blockDivPage += '</div>';
                blockDivDataPage += "<tr data-isobjective='" + Isobjective + "' data-EntityLevel='" + data.Data[i]["Level"] + "' data-over='true' data-uniquekey='" + GeneralUniquekey + "' class='" + ClassName + " noBGimg' >";
                for (var k = 0; k < $scope.GanttViewHeaders.length; k++) {
                    if (SystemDefiendAttributes.ParentEntityName.toString() == $scope.GanttViewHeaders[k].Type.toString() && data.Data[i].ParentID != 0) {
                        var splitNames = data.Data[i][$scope.GanttViewHeaders[k].Field].split('!@#');
                        blockDivDataPage += "<td><span><span style='background-color: #" + splitNames[2].trim() + ";'  class='eicon-s margin-right5x'> " + splitNames[1] + "</span><span>" + (splitNames[0] == null ? "-" : splitNames[0]) + "</span></span></td>";
                    } else if ($scope.GanttViewHeaders[k].Field != "68" && $scope.GanttViewHeaders[k].Type != "10") {
                        var bclr = '';
                        var txtclr = '';
                        if ($scope.GanttViewHeaders[k].Id == "71") {
                            var txtcolor = 'ffffff';
                            var backgroundcolor = data.Data[i].StatusColorCode.trim();
                            txtcolor = data.Data[i].StatusColorCode.trim();
                            if (txtcolor == null || txtcolor.trim() == "" || txtcolor.trim() == "-") {
                                txtcolor = 'ffffff';
                                backgroundcolor = 'ffffff';
                            }
                            var r = parseInt(txtcolor.substr(0, 2), 16);
                            var g = parseInt(txtcolor.substr(2, 2), 16);
                            var b = parseInt(txtcolor.substr(4, 2), 16);
                            var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
                            txtcolor = (yiq >= 225) ? 'black' : 'white';

                            bclr = 'style="background-color:#' + backgroundcolor + ';"';
                            txtclr = 'style = "color: ' + txtcolor + ';"';
                        }
                        if ($scope.GanttViewHeaders[k].Type != "5")
                            blockDivDataPage += '<td ' + bclr + ' ><span ' + txtclr + ' >' + (data.Data[i][$scope.GanttViewHeaders[k].Field] == null ? "-" : data.Data[i][$scope.GanttViewHeaders[k].Field]) + '</span></td>';
                        else blockDivDataPage += '<td><span>' + (data.Data[i][$scope.GanttViewHeaders[k].Field] == null ? "-" : dateFormat(data.Data[i][$scope.GanttViewHeaders[k].Field], $scope.DefaultSettings.DateFormat)) + '</span></td>';
                    }
                }
                blockDivDataPage += '</tr>';
            }
            return {
                Block: blockDivPage,
                Data: blockDivDataPage
            };
        }

        function drawData(data, UniqueID) {
            var blockDivPage = '';
            for (var i = 0; i < data.Data.length; i++) {
                var UniqueId = '';
                var Isobjective = false;
                var GeneralUniquekey = UniqueKEY(data.Data[i]["class"].toString());
                var ClassName = GenateClass(GeneralUniquekey);
                ClassName += " mo" + GeneralUniquekey;
                if (data.Data[i]["TypeID"] == 10) {
                    Isobjective = true;
                }
                blockDivPage += "<tr data-isobjective='" + Isobjective + "' data-EntityLevel='" + data.Data[i]["Level"] + "' data-over='true' data-uniquekey='" + GeneralUniquekey + "' class='" + ClassName + "' >";
                for (var k = 0; k < $scope.GanttViewHeaders.length; k++) {
                    if (SystemDefiendAttributes.ParentEntityName.toString() == $scope.GanttViewHeaders[k].Type.toString() && data.Data[i].ParentID != 0) {
                        var splitNames = data.Data[i][$scope.GanttViewHeaders[k].Field].split('!@#');
                        blockDivPage += "<td><span><span style='background-color: #" + splitNames[2].trim() + ";'  class='eicon-s margin-right5x'> " + splitNames[1] + "</span><span>" + (splitNames[0] == null ? "-" : splitNames[0]) + "</span></span></td>";
                    } else if ($scope.GanttViewHeaders[k].Field != "68" && $scope.GanttViewHeaders[k].Type != "10") {
                        blockDivPage += '<td><span>' + (data.Data[i][$scope.GanttViewHeaders[k].Field] == null ? "-" : data.Data[i][$scope.GanttViewHeaders[k].Field]) + '</span></td>';
                    }
                }
                blockDivPage += '</tr>';
            }
            return blockDivPage;
        }

        function drawDataHeader(ColumnDefs) {
            var HeaderHtml = '';
            var totalColumn = 0;
            for (var j = 0; j < ColumnDefs.length; j++) {
                if (ColumnDefs[j].Field != "68" && ColumnDefs[j].Type != "10") {
                    HeaderHtml += '<th><span>' + ColumnDefs[j].DisplayName + '</span></th>';
                    totalColumn += 1;
                }
            }
            $('#ganttChart #ganttviewDataHeader').html(HeaderHtml);
            $('.ganttview-left').width(totalColumn * 100);
            $('.ganttview-right').css('margin-left', (totalColumn * 100) + 'px');
        }

        function apply(parent, settings) {
            if (settings.behavior.clickable) {
                bindBlockClick(parent, settings.behavior.onClick);
            }
            if (settings.behavior.resizable) {
                switch (settings.Mode) {
                    case 'daily':
                        bindBlockResize(parent, settings.MonthlyDayWidth, settings.StartDate, settings.behavior.onResize, settings);
                        break;
                    case 'monthly':
                        bindBlockResize(parent, settings.YearlyDayWidth, settings.StartDate, settings.behavior.onResize, settings);
                        break;
                    case 'quarterly':
                        bindBlockResize(parent, settings.QuarterlyDayWidth, settings.StartDate, settings.behavior.onResize, settings);
                        break;
                    case 'weekly':
                        bindBlockResize(parent, settings.WeeklyDayWidth, settings.StartDate, settings.behavior.onResize, settings);
                        break;
                    default:
                }
            }
            if (settings.behavior.draggable) {
                switch (settings.Mode) {
                    case 'daily':
                        bindBlockDrag(parent, settings.MonthlyDayWidth, settings.StartDate, settings.behavior.onDrag, settings);
                        break;
                    case 'monthly':
                        bindBlockDrag(parent, settings.YearlyDayWidth, settings.StartDate, settings.behavior.onDrag, settings);
                        break;
                    case 'quarterly':
                        bindBlockDrag(parent, settings.QuarterlyDayWidth, settings.StartDate, settings.behavior.onDrag, settings);
                        break;
                    case 'weekly':
                        bindBlockDrag(parent, settings.WeeklyDayWidth, settings.StartDate, settings.behavior.onDrag, settings);
                        break;
                    default:
                }
            }
        }

        function bindBlockClick(parent, callback) {
            $("div.ganttview-data-block", parent).on("click", function () {
                var islock = $("#EntitiesTree").find('a[data-entityid="' + JSON.parse($(this).attr("block-data")).id + '"]').attr('data-Permission');
                if (islock == 3 || islock == 0) {
                    $scope.IsLock = true;
                } else {
                    $scope.IsLock = false;
                }
                if ($scope.IsLock == true || $scope.GanttLock == true) {
                    block.css('position', 'absolute');
                    $("#moduleContextGantt").modal('hide');
                } else if (callback) {
                    if ($(this).attr('data-ResizeDrag') == 'true') {
                        $(this).removeAttr('data-ResizeDrag');
                    } else {
                        callback(JSON.parse($(this).attr("block-data")), $(this).attr('data-dateblockuniqueid'));
                    }
                }
            });
        }

        function bindBlockResize(parent, cellWidth, startDate, callback, settings) {
            $("div.ganttview-data-block", parent).resizable({
                grid: cellWidth,
                handles: "e,w",
                start: function (event) {
                    var block = $(this);
                    var islock = $("#EntitiesTree").find('a[data-entityid="' + JSON.parse($(this).attr("block-data")).id + '"]').attr('data-Permission');
                    if (islock == 3 || islock == 0) {
                        $scope.IsLock = true;
                    } else {
                        $scope.IsLock = false;
                    }
                    if ($scope.IsLock == true || $scope.GanttLock == true) {
                        block.css('position', 'absolute');
                        $("#moduleContextGantt").modal('hide');
                    }
                },
                stop: function (event) {
                    var block = $(this);
                    var islock = $("#EntitiesTree").find('a[data-entityid="' + JSON.parse($(this).attr("block-data")).id + '"]').attr('data-Permission');
                    if (islock == 3 || islock == 0) {
                        $scope.IsLock = true;
                    } else {
                        $scope.IsLock = false;
                    }
                    if ($scope.IsLock == true || $scope.GanttLock == true) {
                        block.css('position', 'absolute');
                        $("#moduleContextGantt").modal('hide');
                    } else {
                        var data = JSON.parse($(this).attr("block-data"));
                        updateDataAndPosition(parent, block, cellWidth, startDate);
                        if (DateValidate(JSON.parse($(this).attr("block-data")))) {
                            if (callback) {
                                callback(JSON.parse($(this).attr("block-data")));
                            }
                        } else {
                            bootbox.alert($translate.instant('LanguageContents.Res_1903.Caption'));
                            var startDt = data.start;
                            var endDt = data.end;
                            var duration = 0;
                            var formLeft = 0;
                            switch (settings.Mode) {
                                case 'daily':
                                    duration = ((daysBetween(startDt, endDt) + 1) * settings.MonthlyDayWidth) - 9;
                                    formLeft = (daysBetween(settings.StartDate, startDt) * settings.MonthlyDayWidth) + 3;
                                    break;
                                case 'monthly':
                                    duration = ((daysBetween(startDt, endDt) + 1) * settings.YearlyDayWidth) - 9;
                                    formLeft = (daysBetween(settings.StartDate, startDt) * settings.YearlyDayWidth) + 3;
                                    break;
                                case 'quarterly':
                                    duration = ((daysBetween(startDt, endDt) + 1) * settings.QuarterlyDayWidth) - 9;
                                    formLeft = (daysBetween(settings.StartDate, startDt) * settings.QuarterlyDayWidth) + 3;
                                    break;
                                case 'weekly':
                                    duration = ((daysBetween(startDt, endDt) + 1) * settings.WeeklyDayWidth) - 9;
                                    formLeft = (daysBetween(settings.StartDate, startDt) * settings.WeeklyDayWidth) + 3;
                                    break;
                                default:
                                    duration = ((daysBetween(startDt, endDt) + 1) * settings.MonthlyDayWidth) - 9;
                                    formLeft = (daysBetween(settings.StartDate, startDt) * settings.MonthlyDayWidth) + 3;
                                    break;
                            }
                            block.css('width', duration + 'px');
                            block.css('margin-left', formLeft + 'px');
                            block.attr('block-data', JSON.stringify(data));
                        }
                    }
                }
            });
        }

        function bindBlockDrag(parent, cellWidth, startDate, callback, settings) {
            $("div.ganttview-data-block", parent).draggable({
                axis: "x",
                grid: [cellWidth, cellWidth],
                start: function () {
                    var block = $(this);
                    var islock = $("#EntitiesTree").find('a[data-entityid="' + JSON.parse($(this).attr("block-data")).id + '"]').attr('data-Permission');
                    if (islock == 3 || islock == 0) {
                        $scope.IsLock = true;
                    } else {
                        $scope.IsLock = false;
                    }
                    if ($scope.IsLock == true || $scope.GanttLock == true) {
                        block.css('position', 'absolute');
                        block.removeAttr('position', 'relative');
                        return false;
                    } else {
                        block.css('position', 'relative');
                    }
                },
                stop: function () {
                    var block = $(this);
                    var islock = $("#EntitiesTree").find('a[data-entityid="' + JSON.parse($(this).attr("block-data")).id + '"]').attr('data-Permission');
                    if (islock == 3 || islock == 0) {
                        $scope.IsLock = true;
                    } else {
                        $scope.IsLock = false;
                    }
                    if ($scope.IsLock == true || $scope.GanttLock == true) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1915.Caption'));
                        block.css('position', 'absolute');
                    } else {
                        ValidateAndUpdate(block, parent, cellWidth, startDate, callback, settings);
                    }
                }
            });
        }

        function ValidateAndUpdate(block, parent, cellWidth, startDate, callback, settings) {
            block.css('position', 'absolute');
            block.attr('data-ResizeDrag', 'true');
            var data = JSON.parse(block.attr("block-data"));
            updateDataAndPosition(parent, block, cellWidth, startDate);
            if (DateValidate(JSON.parse(block.attr("block-data")))) {
                data = JSON.parse(block.attr("block-data"));
                block.attr("title", "<b>Name:</b> " + block.attr("data-name") + " </br><b>Start Date: </b>" + data.start + "</br><b>End Date:</b> " + data.end + " </br><b>Comment:</b> " + data.desc + "").qtip({
                    content: {
                        text: "<b>Name:</b> " + block.attr("data-name") + " </br><b>Start Date: </b>" + data.start + "</br><b>End Date:</b> " + data.end + " </br><b>Comment:</b> " + data.desc + "",
                    },
                    style: {
                        classes: "qtip-dark qtip-shadow qtip-rounded",
                        title: {
                            'display': 'none'
                        }
                    },
                    show: {
                        event: 'mouseenter click unfocus',
                        solo: true
                    },
                    events: {
                        hide: function (event, api) {
                            event: 'unfocus click mouseleave mouseup mousedown'
                        }
                    },
                    hide: {
                        delay: 0,
                        fixed: false,
                        effect: function () {
                            $(this).fadeOut(100);
                        },
                        event: 'unfocus click mouseleave mouseup mousedown'
                    },
                    position: {
                        my: 'bottom left',
                        at: 'top left',
                        viewport: $(window),
                        adjust: {
                            method: 'shift',
                            mouse: true
                        },
                        corner: {
                            target: 'bottom left',
                            tooltip: 'bottom left',
                            mimic: 'top'
                        },
                        target: 'mouse'
                    }
                });
                if (callback) {
                    callback(JSON.parse(block.attr("block-data")));
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_1903.Caption'));
                var startDt = data.start;
                var endDt = data.end;
                var duration = 0;
                var formLeft = 0;
                switch (settings.Mode) {
                    case 'daily':
                        duration = ((daysBetween(startDt, endDt) + 1) * settings.MonthlyDayWidth) - 9;
                        formLeft = (daysBetween(settings.StartDate, startDt) * settings.MonthlyDayWidth) + 3;
                        break;
                    case 'monthly':
                        duration = ((daysBetween(startDt, endDt) + 1) * settings.YearlyDayWidth) - 9;
                        formLeft = (daysBetween(settings.StartDate, startDt) * settings.YearlyDayWidth) + 3;
                        break;
                    case 'quarterly':
                        duration = ((daysBetween(startDt, endDt) + 1) * settings.QuarterlyDayWidth) - 9;
                        formLeft = (daysBetween(settings.StartDate, startDt) * settings.QuarterlyDayWidth) + 3;
                        break;
                    case 'weekly':
                        duration = ((daysBetween(startDt, endDt) + 1) * settings.WeeklyDayWidth) - 9;
                        formLeft = (daysBetween(settings.StartDate, startDt) * settings.WeeklyDayWidth) + 3;
                        break;
                    default:
                        duration = ((daysBetween(startDt, endDt) + 1) * settings.MonthlyDayWidth) - 9;
                        formLeft = (daysBetween(settings.StartDate, startDt) * settings.MonthlyDayWidth) + 3;
                        break;
                }
                block.css('width', duration + 'px');
                block.css('margin-left', formLeft + 'px');
                block.attr('block-data', JSON.stringify(data));
            }
        }

        function updateDataAndPosition(parent, block, cellWidth, startDate) {
            startDate = Date.parse(startDate);
            var container = $("div.ganttview-slide-container", parent);
            var scroll = container.scrollLeft();
            var offset = block.offset().left - container.offset().left - 1 + scroll;
            var blockdata = JSON.parse(block.attr("block-data"));
            var daysFromStart = Math.round(offset / cellWidth);
            var newStart = startDate.clone().addDays(daysFromStart);
            blockdata.start = newStart.toString("yyyy-MM-dd");
            var width = block.outerWidth();
            var numberOfDays = Math.round(width / cellWidth) - 1;
            blockdata.end = newStart.clone().addDays(numberOfDays).toString("yyyy-MM-dd");
            block.attr('block-data', JSON.stringify(blockdata));
            block.css("top", "").css("left", "").css("position", "relative").css("margin-left", offset + "px");
        }

        function DateValidate(data) {
            var IsOverlap = false;
            var MainObject = $("div[data-dateblockuniqueid='" + data.order + "-" + data.uniqueId + "']").parent();
            var currentHtml = MainObject.html();
            var childCount = MainObject.children(".ganttview-data-block").length;
            var NextjsonData = $("div[data-dateblockuniqueid='" + data.order + "-" + data.uniqueId + "']").next().attr('block-data');
            var PrevjsonData = $("div[data-dateblockuniqueid='" + data.order + "-" + data.uniqueId + "']").prev().attr('block-data');
            if (NextjsonData != undefined) {
                var NextObjectData = jQuery.parseJSON(NextjsonData);
            }
            if (PrevjsonData != undefined) {
                var PrevObjectData = jQuery.parseJSON(PrevjsonData);
            }
            if (childCount == 1) {
                IsOverlap = true;
            } else if (data.SortOid == 1) {
                if (data.end < NextObjectData.start) {
                    IsOverlap = true;
                }
            } else if (data.SortOid == childCount) {
                if (data.start > PrevObjectData.end) {
                    IsOverlap = true;
                }
            } else if (childCount > 1 && data.SortOid < childCount) {
                if (data.end < NextObjectData.start && data.start > PrevObjectData.end) {
                    IsOverlap = true;
                }
            }
            return IsOverlap
        }
        $scope.SetGanttLockObj = function () {
            var ganttlock = $("#ObjGanttLock").hasClass("icon-lock");
            if (ganttlock == true) {
                $("#ObjGanttLock").removeClass("icon-lock");
                $("#ObjGanttLock").addClass("icon-unlock");
                $scope.GanttLock = false;
                $('.ganttview-data-block.Ganttlockdummy').removeClass('disableResize-GanttBlock')
                $('.ganttview-data-block.Ganttlockdummy').removeClass('disableDraggable-GanttBlock')
            } else {
                $("#ObjGanttLock").removeClass("icon-unlock");
                $("#ObjGanttLock").addClass("icon-lock");
                $scope.GanttLock = true;
                $('.ganttview-data-block.Ganttlockdummy').addClass('disableResize-GanttBlock')
                $('.ganttview-data-block.Ganttlockdummy').addClass('disableDraggable-GanttBlock')
            }
        }

        function updateganttlocksettings() {
            var ganttlock = $("#SetGanttLock").hasClass("icon-lock");
            if (ganttlock == false) {
                $timeout(function () {
                    CallBehavior();
                }, 1000);
                $('.ganttview-data-block.Ganttlockdummy').removeClass('disableResize-GanttBlock')
                $('.ganttview-data-block.Ganttlockdummy').removeClass('disableDraggable-GanttBlock')
                $('.ganttview-data-block.Ganttlockdummy').addClass('ui-resizable')
                $('.ganttview-data-block.Ganttlockdummy').addClass('ui-draggable')
            } else {
                $('.ganttview-data-block.Ganttlockdummy').addClass('disableResize-GanttBlock')
                $('.ganttview-data-block.Ganttlockdummy').addClass('disableDraggable-GanttBlock')
                $('.ganttview-data-block.Ganttlockdummy').removeClass('ui-resizable')
                $('.ganttview-data-block.Ganttlockdummy').removeClass('ui-draggable')
            }
        }
    }
    app.controller("mui.planningtool.objective.detail.ganttviewCtrl", ['$scope', '$timeout', '$rootScope', '$http', '$resource', '$compile', '$window', '$translate', 'ObjectiveganttviewService', muiplanningtoolobjectivedetailganttviewCtrl]);
})(angular, app);