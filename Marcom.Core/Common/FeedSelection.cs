﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{
   internal class FeedSelection : IFeedSelection
    {
       public int FeedId { get; set; }
       public string UserName { get; set; }
       public string UserEmail { get; set; }
       public string UserImage { get; set; }
       public string FeedHappendTime { get; set; }
       public string FeedText { get; set; }
       public int Actor { get; set; }
       public IList<IFeedComment> FeedComment { get; set; }
    }

}
