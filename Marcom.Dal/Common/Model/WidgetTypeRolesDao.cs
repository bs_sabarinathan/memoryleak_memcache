﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Common.Model
{
    /// <summary>
    /// WidgetTypeRoles object for table 'CM_WidgetTypeRoles'.
    /// </summary>
    /// 
    public partial class WidgetTypeRolesDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _WidgetTypeID;
        protected int _RoleID;

        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public WidgetTypeRolesDao() { }

        public WidgetTypeRolesDao(int pWidgetTypeID, int pRoleID)
        {
            this._WidgetTypeID = pWidgetTypeID;
            this._RoleID = pRoleID;
        }

        public WidgetTypeRolesDao(int pId)
        {
            this._id = pId;
        }

        #endregion
        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int WidgetTypeID
        {
            get { return _WidgetTypeID; }
            set { _bIsChanged |= (_WidgetTypeID != value); _WidgetTypeID = value; }

        }

        public virtual int RoleID
        {
            get { return _RoleID; }
            set { _bIsChanged |= (_RoleID != value); _RoleID = value; }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string WidgetTypeRolesDao = "WidgetTypeRolesDao";
            public const string Id = "Id";
            public const string WidgetTypeID = "WidgetTypeID";
            public const string RoleID = "RoleID";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string WidgetTypeRolesDao = "WidgetTypeRolesDao";
            public const string Id = "Id";
            public const string WidgetTypeID = "WidgetTypeID";
            public const string RoleID = "RoleID";
        }

        #endregion

    }
}
