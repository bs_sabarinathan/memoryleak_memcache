﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Task.Interface
{
    public interface IEntityTaskList
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int EntityID
        {
            get;
            set;

        }


        string Name
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }

        int TaskListID
        {
            get;
            set;

        }
        int Sortorder
        {
            get;
            set;

        }

        int OnTimeStatus
        {
            get;
            set;

        }

        string OnTimeComment
        {
            get;
            set;
        }


        int TaskInProgress
        {
            get;
            set;

        }

        int UnAssignedTasks
        {
            get;
            set;

        }


        int OverDueTasks
        {
            get;
            set;

        }


        int UnableToComplete
        {
            get;
            set;

        }

        int ActiveEntityStateID
        {
            get;
            set;
        }

        int EntityParentID { get; set; }

        
        #endregion
    }
}
