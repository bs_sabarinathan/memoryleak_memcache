using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Access.Interface
{
    /// <summary>
    /// IGlobalRole interface for table 'AM_GlobalRole'.
    /// </summary>
    public interface IGlobalRole
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }

        bool IsAssetAccess
        { get; set; }


        #endregion
    }
}
