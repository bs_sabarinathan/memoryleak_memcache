﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Metadata.Model
    {
    public class EntityTypeAttributeGroupRelationDao : BaseDao , ICloneable
        {
        #region Member Variables
        protected int _id;
        protected int _entitytypeid;
        protected int _attributegroupid;
        protected string _caption;
        protected int _IsLocationType;
        protected bool _IsRepresentationtype;
        protected int _sortorder;
        protected int _pagesize;
        protected bool _IsTabInEntityCreation;
        protected bool _IsAttrGrpInheritFromParent;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Extensibility Method Definitions


        #endregion

        #region Constructors
        public EntityTypeAttributeGroupRelationDao() { }

        public EntityTypeAttributeGroupRelationDao(int pID, int pAttributeGroupId, int pEntityTypeId, string pCaption, int pIsIndependentBlock, bool pIsAttributeGroupWizardTab, int pSortOrder, int pPageSize, bool pIsTabInEntityCreation, bool pIsAttrGrpInheritFromParent)
        {
            this._id = pID;
            this._attributegroupid = pAttributeGroupId;
            this._entitytypeid = pEntityTypeId;
            this._caption = pCaption;
            this._IsLocationType = pIsIndependentBlock;
            this._IsRepresentationtype = pIsAttributeGroupWizardTab;
            this._sortorder = pSortOrder;
            this._pagesize = pPageSize;
            this._IsTabInEntityCreation = pIsTabInEntityCreation;
            this._IsAttrGrpInheritFromParent = pIsAttrGrpInheritFromParent;
        }

        public EntityTypeAttributeGroupRelationDao(int pID)
        {
            this._id = pID;
        }

        #endregion

        #region Public Properties
        public virtual int ID
            {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }
            }

        public virtual string Caption
            {
            get { return _caption; }
            set { _bIsChanged |= (_caption != value); _caption = value; }
            }

        public virtual int AttributeGroupID
            {
            get { return _attributegroupid; }
            set { _bIsChanged |= (_attributegroupid != value); _attributegroupid = value; }
            }

        public virtual int EntityTypeID
            {
            get { return _entitytypeid; }
            set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }
            }

        public virtual int LocationType
            {
            get { return _IsLocationType; }
            set { _bIsChanged |= (_IsLocationType != value); _IsLocationType = value; }
            }

        public virtual bool RepresentationType
            {
            get { return _IsRepresentationtype; }
            set { _bIsChanged |= (_IsRepresentationtype != value); _IsRepresentationtype = value; }
            }
        public virtual int SortOrder
            {
            get { return _sortorder; }
            set { _bIsChanged |= (_sortorder != value); _sortorder = value; }
            }

        public virtual int PageSize
        {
            get { return _pagesize; }
            set { _bIsChanged |= (_pagesize != value); _pagesize = value; }
        }
        public virtual bool IsTabInEntityCreation
        {
            get { return _IsTabInEntityCreation; }
            set { _bIsChanged |= (_IsTabInEntityCreation != value); _IsTabInEntityCreation = value; }
        }
        public virtual bool IsAttrGrpInheritFromParent
        {
            get { return _IsAttrGrpInheritFromParent; }
            set { _bIsChanged |= (_IsAttrGrpInheritFromParent != value); _IsAttrGrpInheritFromParent = value; }
        }

        public virtual bool IsDeleted
            {
            get
                {
                return _bIsDeleted;
                }
            set
                {
                _bIsDeleted = value;
                }
            }

        public virtual bool IsChanged
            {
            get
                {
                return _bIsChanged;
                }
            set
                {
                _bIsChanged = value;
                }
            }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
            {
            return this.MemberwiseClone();
            }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string EntityTypeAttributeGroupRelationDao = "EntityTypeAttributeGroupRelationDao";
            public const string ID = "ID";
            public const string EntityTypeID = "EntityTypeID";
            public const string AttributeGroupID = "AttributeGroupID";
            public const string Caption = "Caption";
            public const string LocationType = "LocationType";
            public const string RepresentationType = "RepresentationType";
            public const string SortOrder = "SortOrder";
            public const string PageSize = "PageSize";
            public const string IsTabInEntityCreation = "IsTabInEntityCreation";
            public const string IsAttrGrpInheritFromParent = "IsAttrGrpInheritFromParent";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string EntityTypeAttributeGroupRelationDao = "EntityTypeAttributeGroupRelationDao";
            public const string ID = "ID";
            public const string EntityTypeID = "EntityTypeID";
            public const string AttributeGroupID = "AttributeGroupID";
            public const string Caption = "Caption";
            public const string LocationType = "LocationType";
            public const string RepresentationType = "RepresentationType";
            public const string SortOrder = "SortOrder";
            public const string PageSize = "PageSize";
            public const string IsTabInEntityCreation = "IsTabInEntityCreation";
            public const string IsAttrGrpInheritFromParent = "IsAttrGrpInheritFromParent";
        }

        #endregion
        }
    }
