using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    /// <summary>
    /// IFeature interface for table 'MM_Feature'.
    /// </summary>
    public interface IRoleFeature
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }

        int ModuleID
        {
            get;
            set;

        }

        bool IsChecked
        {
            get;
            set;

        }

        int GlobalAclID
        { get; set; }
        #endregion
    }
}
