﻿using System;
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    internal class EntityAmountCurrencyType : IEntityAmountCurrencyType
    {
        #region Member Variables

        protected int _id;
        protected int _EntityID;
        protected int _Currencytypeid;
        protected decimal _Amount;
        protected int _Attributeid;
        #endregion
        #region Constructors
		public EntityAmountCurrencyType() {}

        public EntityAmountCurrencyType(int pId, int pEntityID, int pCurrencytypeid, decimal pAmount, int pAttributeid)
		{
            this._id = pId;
            this._EntityID = pEntityID;
            this._Currencytypeid = pCurrencytypeid; 
            this._Amount = pAmount;
            this._Attributeid = pAttributeid;
		}
        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int EntityID
        {
            get { return _EntityID; }
            set { _EntityID = value; }

        }

        public int Currencytypeid
        {
            get { return _Currencytypeid; }
            set { _Currencytypeid = value; }

        }

        public decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; }

        }
        public int Attributeid
        {
            get { return _Attributeid; }
            set { _Attributeid = value; }

        }

        #endregion 
		
        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityPeriod castObj = null;
            try
            {
                castObj = (EntityPeriod)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {

            return this.GetType().FullName.GetHashCode();

        }
        #endregion
		
    }
}
