﻿using plUploadHandler;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Presentation.Handlers
{
    /// <summary>
    /// This class is an application level implementation of an uploader
    /// 
    /// This uploader subclasses plUploadFileHandler which downloads files
    /// into a temporary folder (~/tempuploads) and then resizes the image,
    /// renames it and copies it a final destination (~/UploadedImages)
    /// 
    /// This handler also deletes old files in both of those folders
    /// just to keep the size of this demo reasonable.
    /// </summary>
    public class AlternateDamFileHandler : plUploadFileHandler
    {

        static string tenanturl = HttpContext.Current.Request.Url.Host;
        static BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
        string TenantFilePath = tfp.GetTenantFilePathByHostName(tenanturl);


        const string ImageStoragePath = "~/UploadedImages";
        public static int ImageHeight = 480;

        public AlternateDamFileHandler()
        {
            // Normally you'd set these values from config values
            FileUploadPhysicalPath = ReadAdminXML("FileManagment", TenantFilePath);
            MaxUploadSize = 2147483647;
            AllowedExtensions = ".jpg,.jpeg,.png,.gif,.bmp";
        }

        protected override void OnUploadCompleted(string fileName, string TemplateType, string mimeType, string fileid)
        {
            var Server = Context.Server;
            string imageExtension = fileName;
            imageExtension = imageExtension.Substring(imageExtension.LastIndexOf("."));

            try
            {

            }
            catch (Exception ex)
            {
                WriteErrorResponse("Unable to write out uploaded file: " + ex.Message);
                return;
            }

            // return just a string that contains the url path to the file
            WriteUploadCompletedMessage(Context.Request["id"] + "," + mimeType + "," + imageExtension);
        }

        protected override bool OnUploadStarted(int chunk, int chunks, string name, string templateType)
        {
            // time out files after 15 minutes - temporary upload files
            //DeleteTimedoutFiles(Path.Combine(FileUploadPhysicalPath, "*.*"), 900);

            // clean out final image folder too
            //DeleteTimedoutFiles(Path.Combine(Context.Server.MapPath(ImageStoragePath), "*.*"), 900);
            if (templateType == "worddocument")
                FileUploadPhysicalPath = TenantFilePath + "~/DAMFiles/Templates/Word/Documents/";
            else
                FileUploadPhysicalPath = ReadAdminXML("FileManagment", TenantFilePath);


            return base.OnUploadStarted(chunk, chunks, name, templateType);
        }






        private string ReadAdminXML(string elementNode, string TenantHost)
        {
            //Is this below section of code required?
            //BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
            //string TenantFilePath = tfp.GetTenantFilePathByHostName(TenantHost);
            string xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + TenantHost, "AdminSettings.xml");
            XDocument xDoc = XDocument.Load(xmlpath);
            if (xDoc.Root.Elements(elementNode).Count() > 0)
            {
                return (string)xDoc.Root.Elements(elementNode).FirstOrDefault().Elements("FolderPath").First();
            }
            //


            string uploadImagePath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + TenantHost);
            return uploadImagePath = uploadImagePath + "DAMFiles\\Original\\";
        }

        #region Sample Helpers
        /// <summary>
        /// Deletes files based on a file spec and a given timeout.
        /// This routine is useful for cleaning up temp files in 
        /// Web applications.
        /// </summary>
        /// <param name="filespec">A filespec that includes path and/or wildcards to select files</param>
        /// <param name="seconds">The timeout - if files are older than this timeout they are deleted</param>
        public static void DeleteTimedoutFiles(string filespec, int seconds)
        {
            string path = Path.GetDirectoryName(filespec);
            string spec = Path.GetFileName(filespec);
            string[] files = Directory.GetFiles(path, spec);

            foreach (string file in files)
            {
                try
                {
                    if (File.GetLastWriteTimeUtc(file) < DateTime.UtcNow.AddSeconds(seconds * -1))
                        File.Delete(file);
                }
                catch { }  // ignore locked files
            }
        }



        #endregion

    }
}