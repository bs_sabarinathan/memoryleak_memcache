﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    public  interface IWidgetTypeRoles
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int WidgetTypeID
        {
            get;
            set;

        }

        int RoleID
        {
            get;
            set;

        }




        #endregion
    }
}
