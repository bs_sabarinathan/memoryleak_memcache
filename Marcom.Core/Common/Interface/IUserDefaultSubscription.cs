using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IUserDefaultSubscription interface for table 'CM_UserDefaultSubscription'.
    /// </summary>
    public interface IUserDefaultSubscription
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }
        int Userid
        {
            get;
            set;

        }

        string SubscriptionTypeid
        {
            get;
            set;

        }
        string MailSubscriptionTypeid
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
