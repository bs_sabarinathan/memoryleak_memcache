﻿using Amazon.S3;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Utility
{
    class TenantsDal
    {
        public int TenantID { get; set; }

        public string TenantHost { get; set; }

        public string TenantPath { get; set; }

        public string TenantName { get; set; }

        public StorageBlock AwsStorage { get; set; }

    }

    public class StorageBlock
    {
        public string AWSAccessKeyID { get; set; }
        public string AWSSecretAccessKey { get; set; }
        public string BucketName { get; set; }
        public string ServiceURL { get; set; }
        public string RegionEndpoint { get; set; }
        public AmazonS3Client S3 { get; set; }

        public int storageType { get; set; }

        public string Uploaderurl { get; set; }

    }


}
