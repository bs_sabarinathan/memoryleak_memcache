using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
	/// FeedCommentDao object for table 'CM_Feed_Comment'.
	/// </summary>
	public partial class FeedCommentDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected int _feedid;
		protected int _actor;
		protected string _comment;
		protected DateTimeOffset _commentedon;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public FeedCommentDao() {}
			
		public FeedCommentDao(int pFeedid, int pActor, string pComment, DateTimeOffset pCommentedOn)
		{
			this._feedid = pFeedid; 
			this._actor = pActor; 
			this._comment = pComment; 
			this._commentedon = pCommentedOn; 
		}
				
		public FeedCommentDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		public virtual int Feedid
		{
			get { return _feedid; }
			set { _bIsChanged |= (_feedid != value); _feedid = value; }
			
		}
		
		public virtual int Actor
		{
			get { return _actor; }
			set { _bIsChanged |= (_actor != value); _actor = value; }
			
		}
		
		public virtual string Comment
		{
			get { return _comment; }
			set 
			{
			  if (value != null && value.Length > 4000)
			    throw new ArgumentOutOfRangeException("Comment", "Comment value, cannot contain more than 4000 characters");
			  _bIsChanged |= (_comment != value); 
			  _comment = value; 
			}
			
		}
		
		public virtual DateTimeOffset CommentedOn
		{
			get { return _commentedon; }
			set { _bIsChanged |= (_commentedon != value); _commentedon = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string FeedCommentDao = "FeedCommentDao";			
			public const string Id = "Id";			
			public const string Feedid = "Feedid";			
			public const string Actor = "Actor";			
			public const string Comment = "Comment";			
			public const string CommentedOn = "CommentedOn";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string FeedCommentDao = "FeedCommentDao";			
			public const string Id = "Id";			
			public const string Feedid = "Feedid";			
			public const string Actor = "Actor";			
			public const string Comment = "Comment";			
			public const string CommentedOn = "CommentedOn";			
		}

		#endregion
		
		
	}
	
}
