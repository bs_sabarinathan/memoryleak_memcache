using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IMilestone interface for table 'PM_Milestone'.
    /// </summary>
    public interface IMilestone
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Entityid
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }

        int Status
        {
            get;
            set;

        }

        DateTimeOffset DueDate
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
