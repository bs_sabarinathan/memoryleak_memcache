﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common;


namespace BrandSystems.Marcom.Core.Common.Interface
{

    /// <summary>
    ///  object for table 'PM_Objective_Unit'.
    /// </summary>
   public interface IUnits
    {
       
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }
        #endregion

    }
}
