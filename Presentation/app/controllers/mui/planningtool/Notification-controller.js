﻿(function (ng, app) {
    "use strict";
    //app.controller(
	//	"mui.planningtool.notificationCtrl",
    function muiplanningtoolnotificationCtrl($scope, $location, $resource, $timeout, $cookies, requestContext, $compile, $window, _,CommonService) {
		    $scope.FilterFields = [];
		    // Get the render context local to this controller (and relevant params).
		    var renderContext = requestContext.getRenderContext("mui.planningtool.notificationCtrl");
		    // --- Define Scope Variables. ---------------------- //
		    // The subview indicates which view is going to be rendered on the page.
		    $scope.subview = renderContext.getNextSection();
		    // --- Bind To Scope Events. ------------------------ //
		    // I handle changes to the request context.
		    $scope.$on(
                "requestContextChanged",
                function () {
                    // Make sure this change is relevant to this controller.
                    if (!renderContext.isChangeRelevant()) {
                        return;
                    }
                    // Update the view that is being rendered.
                    $scope.subview = renderContext.getNextSection();
                }
            );

		    $scope.$on("$destroy", function () {
		        RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.notificationCtrl']"));
		    });
		    $('#nonotificationforviewall').hide();
		    $scope.Allnotifications = [];
		    $scope.UnreadNotification = '';
	//	    var GetNotification = $resource('common/GetNotification/:Flag', { Flag: 1 });
		    CommonService.GetNotification(1).then(function (getnotification) {
	//	    var getnotification = GetNotification.get(function () {
		        if (getnotification.Response.Item1 == '') {
		            $('#nonotificationforviewall').show();
		        }
		        else {
		            $scope.Allnotifications = getnotification.Response.Item1;
		            $('#nonotificationforviewall').hide();
		        }
		    });
		}
    //);
    app.controller("mui.planningtool.notificationCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', 'requestContext', '$compile', '$window', '_', 'CommonService', muiplanningtoolnotificationCtrl]);
})(angular, app);