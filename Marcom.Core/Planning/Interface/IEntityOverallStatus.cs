﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    public interface IEntityOverallStatus
    {
        #region Public Properties

        int ID
        {
            get;
            set;

        }

        int EntityTypeId
        {
            get;
            set;

        }

        int OverallStatus
        {
            get;
            set;

        }

        int AttrID
        {
            get;
            set;

        }

        int TemplateID
        {
            get;
            set;
        }
        int Days
        {
            get;
            set;
        }

        #endregion
    }
}


