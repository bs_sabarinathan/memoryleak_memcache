using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

    /// <summary>
    /// UserVisibleInfoDao object for table 'MM_Attribute'.
    /// </summary>

    public partial class UserVisibleInfoDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _attributeid;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public UserVisibleInfoDao() { }

        public UserVisibleInfoDao(int pId,  int pAttributeId)
        {
            this._id = pId;
            this._attributeid = pAttributeId;
        }

        public UserVisibleInfoDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int AttributeId
        {
            get { return _attributeid; }
            set { _bIsChanged |= (_attributeid != value); _attributeid = value; }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string UserVisibleInfoDao = "UserVisibleInfoDao";
            public const string Id = "Id";
            public const string AttributeId = "AttributeId";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string UserVisibleInfoDao = "UserVisibleInfoDao";
            public const string Id = "Id";
            public const string AttributeId = "AttributeId";
        }

        #endregion


    }

}
