﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Utility
{


    public class Paginator
    {

        public virtual ArrayLocation calculateArrayLocation(int totalHits, int pageNumber, int pageSize)
        {
            ArrayLocation al = new ArrayLocation();

            if (totalHits < 1 || pageNumber < 1 || pageSize < 1)
            {
                al.Start = 0;
                al.End = 0;
                return al;
            }

            int start = 1 + (pageNumber - 1) * pageSize;
            int end = Math.Min(pageNumber * pageSize, totalHits);
            if (start > end)
            {
                start = Math.Max(1, end - pageSize);
            }

            al.Start = start;
            al.End = end;
            return al;
        }
    }
}
