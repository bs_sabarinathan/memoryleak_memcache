﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.DAM.Model
{

    /// <summary>
    /// FileDao object for table 'DAM_File'.
    /// </summary>

    public partial class DAMFileDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _name;
        protected int _versionno;
        protected string _mimetype;
        protected string _extension;
        protected long _size;
        protected int _ownerid;
        protected DateTimeOffset _createdon;
        protected string _checksum;
        protected int _moduleid;
        protected int _assetid;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        protected Guid _fileguid;
        protected string _description;
        protected int _status;
        protected string _additionalinfo;
        protected int _ProcessType;
        #endregion

        #region Constructors
        public DAMFileDao() { }

        public DAMFileDao(string pName, int pVersionNo, string pMimeType, string pExtension, long pSize, int pOwnerid, DateTimeOffset pCreatedOn, string pChecksum, int pModuleid, int passetid, Guid pFileguid, string pDescription, int pStatus, string padditionalinfo, int pProcessType)
        {
            this._name = pName;
            this._versionno = pVersionNo;
            this._mimetype = pMimeType;
            this._extension = pExtension;
            this._size = pSize;
            this._ownerid = pOwnerid;
            this._createdon = pCreatedOn;
            this._checksum = pChecksum;
            this._moduleid = pModuleid;
            this._assetid = passetid;
            this._fileguid = pFileguid;
            this._description = pDescription;
            this._status = pStatus;
            this._additionalinfo = padditionalinfo;
            this._ProcessType = pProcessType;
        }

        public DAMFileDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
                _bIsChanged |= (_name != value);
                _name = value;
            }

        }

        public virtual int VersionNo
        {
            get { return _versionno; }
            set { _bIsChanged |= (_versionno != value); _versionno = value; }

        }

        public virtual string MimeType
        {
            get { return _mimetype; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("MimeType", "MimeType value, cannot contain more than 250 characters");
                _bIsChanged |= (_mimetype != value);
                _mimetype = value;
            }

        }

        public virtual string Extension
        {
            get { return _extension; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Extension", "Extension value, cannot contain more than 50 characters");
                _bIsChanged |= (_extension != value);
                _extension = value;
            }

        }
        public virtual string Description
        {
            get { return _description; }
            set
            {

                _bIsChanged |= (_description != value);
                _description = value;
            }

        }

        public virtual string Additionalinfo
        {
            get { return _additionalinfo; }
            set
            {

                _bIsChanged |= (_additionalinfo != value);
                _additionalinfo = value;
            }

        }

        public virtual long Size
        {
            get { return _size; }
            set { _bIsChanged |= (_size != value); _size = value; }

        }

        public virtual int OwnerID
        {
            get { return _ownerid; }
            set { _bIsChanged |= (_ownerid != value); _ownerid = value; }

        }

        public virtual DateTimeOffset CreatedOn
        {
            get { return _createdon; }
            set { _bIsChanged |= (_createdon != value); _createdon = value; }

        }

        public virtual string Checksum
        {
            get { return _checksum; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Checksum", "Checksum value, cannot contain more than 50 characters");
                _bIsChanged |= (_checksum != value);
                _checksum = value;
            }

        }

        public virtual int AssetID
        {
            get { return _assetid; }
            set { _bIsChanged |= (_assetid != value); _assetid = value; }

        }

        public virtual Guid FileGuid
        {
            get { return _fileguid; }
            set { _bIsChanged |= (_fileguid != value); _fileguid = value; }

        }



        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }




        public virtual int Status
        {
            get { return _status; }
            set { _status = value; }

        }


        public virtual int ProcessType
        {
            get { return _ProcessType; }
            set { _bIsChanged |= (_ProcessType != value); _ProcessType = value; }

        }
        //public virtual int ProcessType
        //{
        //    get { return _ProcessType; }
        //    set { _status = value; }

        //}
      
        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string DAMFileDao = "DAMFileDao";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string VersionNo = "VersionNo";
            public const string MimeType = "MimeType";
            public const string Extension = "Extension";
            public const string Size = "Size";
            public const string OwnerID = "OwnerID";
            public const string CreatedOn = "CreatedOn";
            public const string Checksum = "Checksum";
            public const string AssetID = "AssetID";
            public const string FileGuid = "FileGuid";
            public const string Description = "Description";
            public const string Status = "Status";
            public const string Additionalinfo = "Additionalinfo";
            public const string ProcessType = "ProcessType";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string DAMFileDao = "DAMFileDao";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string VersionNo = "VersionNo";
            public const string MimeType = "MimeType";
            public const string Extension = "Extension";
            public const string Size = "Size";
            public const string OwnerID = "OwnerID";
            public const string CreatedOn = "CreatedOn";
            public const string Checksum = "Checksum";
            public const string AssetID = "AssetID";
            public const string FileGuid = "FileGuid";
            public const string Description = "Description";
            public const string Status = "Status";
            public const string Additionalinfo = "Additionalinfo";
            public const string ProcessType = "ProcessType";
        }

        #endregion


    }

}
