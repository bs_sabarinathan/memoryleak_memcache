﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Report.Model
{
    public partial class FinancialReportSettingsDao : BaseDao, ICloneable
    {
        #region Member Variables
        protected int _id;
       
        protected string _reportname;
        protected string _reportimage;
        protected string _reportdescription;
        
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public FinancialReportSettingsDao() { }

        public FinancialReportSettingsDao(string pReportname, string pReportImage, string pReportdescription)
        {
            this._reportname = pReportname;
            this._reportimage = pReportImage;
            this._reportdescription = pReportdescription;
        }


        public FinancialReportSettingsDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual string ReportName
        {
            get { return _reportname; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Reportname", "Reportname value, cannot contain more than 250 characters");
                _bIsChanged |= (_reportname != value);
                _reportname = value;
            }

        }

        public virtual string ReportImage
        {
            get { return _reportimage; }
            set
            {

                _bIsChanged |= (_reportimage != value);
                _reportimage = value;
            }

        }

        public virtual string ReportDescription
        {
            get { return _reportdescription; }
            set
            {

                _bIsChanged |= (_reportdescription != value);
                _reportdescription = value;
            }

        }
        

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string FinancialReportSettingsDao = "FinancialReportSettingsDao";
            public const string Id = "Id";
            public const string ReportName = "ReportName";
            public const string ReportImage = "ReportImage";
            public const string ReportDescription = "ReportDescription";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string FinancialReportSettingsDao = "FinancialReportSettingsDao";
            public const string Id = "Id";
            public const string ReportName = "ReportName";
            public const string ReportImage = "ReportImage";
            public const string ReportDescription = "ReportDescription";
        }

        #endregion

    }
}
