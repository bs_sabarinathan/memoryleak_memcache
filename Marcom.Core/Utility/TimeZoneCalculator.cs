﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Utility
{
    public static class TimeZoneCalculator
    {
        public static DateTime ConvertTimeFromUtcDateTime(string utcdate, string timeZone)
        {
            if (utcdate == null || timeZone == null)
            {
                return DateTime.UtcNow;
            }
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
            //utcdate should be in this format "2015-05-27T09:46:33.3601037Z";
            DateTime GMTtime = TimeZoneInfo.ConvertTimeFromUtc(DateTimeOffset.Parse(utcdate).UtcDateTime, tz);
            return GMTtime;
        }
        public static string ConvertTimeFromUtcDateString(string utcdate, string timeZone)
        {
            if (utcdate == null || timeZone == null)
            {
                return "";
            }
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
            //utcdate should be in this format "2015-05-27T09:46:33.3601037Z";
            DateTime GMTtime = TimeZoneInfo.ConvertTimeFromUtc(DateTimeOffset.Parse(utcdate).UtcDateTime, tz);
            return GMTtime.ToString("yyyy-MM-dd");
        }
    }
}
