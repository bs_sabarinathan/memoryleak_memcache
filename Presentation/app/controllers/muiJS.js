///#source 1 1 /assets/js/Angularjs/angucomplete-alt.js
app.directive('angucompleteAlt', ['$q', '$parse', '$http', '$sce', '$timeout', '$templateCache', '$location', function ($q, $parse, $http, $sce, $timeout, $templateCache, $location) {
    var KEY_DW = 40;
    var KEY_RT = 39;
    var KEY_UP = 38;
    var KEY_LF = 37;
    var KEY_ES = 27;
    var KEY_EN = 13;
    var KEY_BS = 8;
    var KEY_DEL = 46;
    var KEY_TAB = 9;
    var MIN_LENGTH = 0;
    var PAUSE = 500;
    var BLUR_TIMEOUT = 300;
    var REQUIRED_CLASS = 'autocomplete-required';
    var TEXT_SEARCHING = 'Searching...';
    var TEXT_NORESULTS = 'No results found';
    var TEMPLATE_URL = '';
    $templateCache.put(TEMPLATE_URL, '<div class="angucomplete-holder" ng-class="{\'angucomplete-dropdown-visible\': showDropdown}">' + ' <input id="{{id}}_value" ng-model="searchStr" ng-disabled="disableInput" type="text" placeholder="{{placeholder}}" ng-focus="onFocusHandler()" class="{{inputClass}}" ng-focus="resetHideResults()" ng-blur="hideResults($event)" autocapitalize="off" autocorrect="off" ng-change="inputChangeHandler(searchStr)"/>' + ' <div id="{{id}}_dropdown" class="angucomplete-dropdown" ng-show="showDropdown">' + ' <div class="angucomplete-row" ng-repeat="result in results" ng-click="selectResult(result)" ng-mouseenter="hoverRow($index)" ng-class="{\'angucomplete-selected-row\': $index == currentIndex}">' + ' <div ng-if="imageField" class="angucomplete-image-holder">' + ' <img ng-if="result.image && result.image != \'\'" ng-src="{{result.image}}" class="angucomplete-image"/>' + ' <div ng-if="!result.image && result.image != \'\'" class="angucomplete-image-default"></div>' + ' </div>' + ' <div class="angucomplete-title" ng-if="matchClass" ng-bind-html="result.title"></div>' + ' <div class="angucomplete-title" ng-if="!matchClass">{{ result.title }}</div>' + ' <div ng-show="false" ng-if="matchClass && result.description && result.description != \'\'" class="angucomplete-description" ng-bind-html="result.description"></div>' + ' <div ng-show="false" ng-if="!matchClass && result.description && result.description != \'\'" class="angucomplete-description">{{result.description}}</div>' + ' </div>' + ' </div>' + '</div>');
    return {
        restrict: 'EA',
        require: '^?form',
        scope: {
            selectedObject: '=',
            disableInput: '=',
            initialValue: '@',
            localData: '=',
            remoteUrlRequestFormatter: '=',
            remoteUrlResponseFormatter: '=',
            remoteUrlErrorCallback: '=',
            id: '@',
            placeholder: '@',
            onSelect: '&',
            remoteUrl: '@',
            remoteUrlDataField: '@',
            titleField: '@',
            descriptionField: '@',
            imageField: '@',
            inputClass: '@',
            pause: '@',
            searchFields: '@',
            minlength: '@',
            matchClass: '@',
            clearSelected: '@',
            overrideSuggestions: '@',
            fieldRequired: '@',
            fieldRequiredClass: '@',
            inputChanged: '=',
            autoMatch: '@',
            focusOut: '&',
            focusIn: '&',
            searchtype: '=',
            control: '=',
            clickhandler: '&'
        },
        templateUrl: function (element, attrs) {
            return attrs.templateUrl || TEMPLATE_URL;
        },
        link: function (scope, elem, attrs, ctrl) {
            var inputField = elem.find('input');
            var minlength = MIN_LENGTH;
            var searchTimer = null;
            var hideTimer;
            var requiredClassName = REQUIRED_CLASS;
            var responseFormatter;
            var validState = null;
            var httpCanceller = null;
            var dd = elem[0].querySelector('.angucomplete-dropdown');
            var isScrollOn = false;
            var mousedownOn = null;
            var unbindInitialValue;
            elem.on('mousedown', function (event) {
                mousedownOn = event.target.id;
            });
            scope.currentIndex = null;
            scope.searching = false;
            scope.searchStr = scope.initialValue;
            unbindInitialValue = scope.$watch('initialValue', function (newval, oldval) {
                if (newval && newval.length > 0) {
                    scope.searchStr = scope.initialValue;
                    handleRequired(true);
                    unbindInitialValue();
                }
            });
            scope.$on('angucomplete-alt:clearInput', function (event, elementId) {
                if (!elementId) {
                    scope.searchStr = null;
                    clearResults();
                } else {
                    if (scope.id === elementId) {
                        scope.searchStr = null;
                        clearResults();
                    }
                }
            });
            scope.reloadcustompage = function (result) {
                scope.searchStr = extractTitle(result.originalObject);
                if (scope.searchStr != "") {
                    var lastRoute = $location.path();
                    if (lastRoute.contains("/mui/customsearchresult")) if (lastRoute == "/mui/customsearchresults") $location.path("/mui/customsearchresult");
                    else $location.path("/mui/customsearchresults");
                    else $location.path("/mui/customsearchresult");
                }
            }
            scope.internalControl = scope.control || {};
            scope.internalControl.ClearTextSearch = function () {
                scope.searchStr = "";
            }

            function ie8EventNormalizer(event) {
                return event.which ? event.which : event.keyCode;
            }

            function callOrAssign(value) {
                if (typeof scope.selectedObject === 'function') {
                    scope.selectedObject(value);
                } else {
                    scope.selectedObject = value;
                }
                if (value) {
                    handleRequired(true);
                } else {
                    handleRequired(false);
                }
            }

            function callFunctionOrIdentity(fn) {
                return function (data) {
                    return scope[fn] ? scope[fn](data) : data;
                };
            }

            function setInputString(str) {
                callOrAssign({
                    originalObject: str
                });
                if (scope.clearSelected) {
                    scope.searchStr = null;
                }
                clearResults();
            }

            function extractTitle(data) {
                return scope.titleField.split(',').map(function (field) {
                    return extractValue(data, field);
                }).join(' ');
            }

            function extractValue(obj, key) {
                var keys, result;
                if (key) {
                    keys = key.split('.');
                    result = obj;
                    keys.forEach(function (k) {
                        result = result[k];
                    });
                } else {
                    result = obj;
                }
                return result;
            }

            function findMatchString(target, str) {
                var words = str;
                var settings = {
                    caseSensitive: false,
                    wordsOnly: false
                };
                if (words.constructor === String) {
                    words = [words];
                }
                words = jQuery.grep(words, function (word, i) {
                    return word != '';
                });
                words = jQuery.map(words, function (word, i) {
                    return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
                });
                if (words.length == 0) {
                    return this;
                };
                var flag = settings.caseSensitive ? "" : "i";
                var patternstr = "(" + words.join("|") + ")";
                if (settings.wordsOnly) {
                    pattern = "\\b" + patternstr + "\\b";
                }
                var result, matches, re = new RegExp(patternstr, flag);
                if (!target) {
                    return;
                }
                matches = target.match(re);
                if (matches) {
                    result = target.replace(re, '<span class="' + scope.matchClass + '">' + matches[0] + '</span>');
                } else {
                    result = target;
                }
                return $sce.trustAsHtml(result);
            }

            function handleRequired(valid) {
                validState = scope.searchStr;
                if (scope.fieldRequired && ctrl) {
                    ctrl.$setValidity(requiredClassName, valid);
                }
            }

            function keyupHandler(event) {
                var which = ie8EventNormalizer(event);
                if (which === KEY_LF || which === KEY_RT) {
                    return;
                }
                if (which === KEY_UP || which === KEY_EN) {
                    event.preventDefault();
                } else if (which === KEY_DW) {
                    event.preventDefault();
                    if (!scope.showDropdown && scope.searchStr && scope.searchStr.length >= minlength) {
                        initResults();
                        scope.searching = true;
                        searchTimerComplete(scope.searchStr);
                    }
                } else if (which === KEY_ES) {
                    clearResults();
                    scope.$apply(function () {
                        inputField.val(scope.searchStr);
                    });
                } else {
                    if (!scope.searchStr || scope.searchStr === '') {
                        scope.showDropdown = false;
                    } else if (scope.searchStr.length >= minlength) {
                        initResults();
                        if (searchTimer) {
                            $timeout.cancel(searchTimer);
                        }
                        scope.searching = true;
                        searchTimer = $timeout(function () {
                            searchTimerComplete(scope.searchStr);
                        }, scope.pause);
                    }
                    if (validState && validState !== scope.searchStr && !scope.clearSelected) {
                        callOrAssign(undefined);
                    }
                }
            }

            function handleOverrideSuggestions(event) {
                if (scope.overrideSuggestions && !(scope.selectedObject && scope.selectedObject.originalObject === scope.searchStr)) {
                    if (event) {
                        event.preventDefault();
                    }
                    setInputString(scope.searchStr);
                }
            }

            function dropdownRowOffsetHeight(row) {
                var css = getComputedStyle(row);
                return row.offsetHeight + parseInt(css.marginTop, 10) + parseInt(css.marginBottom, 10);
            }

            function dropdownHeight() {
                return dd.getBoundingClientRect().top + parseInt(getComputedStyle(dd).maxHeight, 10);
            }

            function dropdownRow() {
                return elem[0].querySelectorAll('.angucomplete-row')[scope.currentIndex];
            }

            function dropdownRowTop() {
                return dropdownRow().getBoundingClientRect().top - (dd.getBoundingClientRect().top + parseInt(getComputedStyle(dd).paddingTop, 10));
            }

            function dropdownScrollTopTo(offset) {
                dd.scrollTop = dd.scrollTop + offset;
            }

            function updateInputField() {
                var current = scope.results[scope.currentIndex];
                if (scope.matchClass) {
                    inputField.val(extractTitle(current.originalObject));
                } else {
                    inputField.val(current.title);
                }
            }

            function keydownHandler(event) {
                var which = ie8EventNormalizer(event);
                var row = null;
                var rowTop = null;
                if (which === KEY_EN && scope.results) {
                    if (scope.currentIndex >= 0 && scope.currentIndex < scope.results.length) {
                        event.preventDefault();
                        scope.selectResult(scope.results[scope.currentIndex]);
                    } else {
                        handleOverrideSuggestions(event);
                        clearResults();
                    }
                    scope.$apply();
                } else if (which === KEY_DW && scope.results) {
                    event.preventDefault();
                    if ((scope.currentIndex + 1) < scope.results.length && scope.showDropdown) {
                        scope.$apply(function () {
                            scope.currentIndex++;
                            updateInputField();
                        });
                        if (isScrollOn) {
                            row = dropdownRow();
                            if (dropdownHeight() < row.getBoundingClientRect().bottom) {
                                dropdownScrollTopTo(dropdownRowOffsetHeight(row));
                            }
                        }
                    }
                } else if (which === KEY_UP && scope.results) {
                    event.preventDefault();
                    if (scope.currentIndex >= 1) {
                        scope.$apply(function () {
                            scope.currentIndex--;
                            updateInputField();
                        });
                        if (isScrollOn) {
                            rowTop = dropdownRowTop();
                            if (rowTop < 0) {
                                dropdownScrollTopTo(rowTop - 1);
                            }
                        }
                    } else if (scope.currentIndex === 0) {
                        scope.$apply(function () {
                            scope.currentIndex = -1;
                            inputField.val(scope.searchStr);
                        });
                    }
                } else if (which === KEY_TAB) {
                    if (scope.results && scope.results.length > 0 && scope.showDropdown) {
                        if (scope.currentIndex === -1 && scope.overrideSuggestions) {
                            handleOverrideSuggestions();
                        } else {
                            if (scope.currentIndex === -1) {
                                scope.currentIndex = 0;
                            }
                            scope.selectResult(scope.results[scope.currentIndex]);
                            scope.$digest();
                        }
                    } else {
                        if (scope.searchStr && scope.searchStr.length > 0) {
                            handleOverrideSuggestions();
                        }
                    }
                }
            }

            function cancelHttpRequest() {
                if (httpCanceller) {
                    httpCanceller.resolve();
                }
            }

            function getRemoteResults(str) {
                $http.post('api/Planning/QuickLuceneSearch', {
                    Text: str,
                    Searchtype: scope.searchtype
                }).success(function (response) {
                    scope.searching = false;
                    if (response.Response != null) {
                        if (response.Response.items.length > 0) {
                            scope.showDropdown = true;
                            processResults(extractValue(responseFormatter(response.Response), scope.remoteUrlDataField), str);
                        } else scope.showDropdown = false;
                    } else scope.showDropdown = false;
                }).error(function (response) { })
            }

            function clearResults() {
                scope.showDropdown = false;
                scope.results = [];
                if (dd) {
                    dd.scrollTop = 0;
                }
            }

            function initResults() {
                scope.currentIndex = -1;
                scope.results = [];
            }

            function getLocalResults(str) {
                var i, match, s, value, searchFields = scope.searchFields.split(','),
					matches = [];
                scope.showDropdown = true;
                for (i = 0; i < scope.localData.length; i++) {
                    match = false;
                    for (s = 0; s < searchFields.length; s++) {
                        value = extractValue(scope.localData[i], searchFields[s]) || '';
                        match = match || (value.toLowerCase().indexOf(str.toLowerCase()) >= 0);
                    }
                    if (match) {
                        matches[matches.length] = scope.localData[i];
                    }
                }
                scope.searching = false;
                processResults(matches, str);
            }

            function checkExactMatch(result, obj, str) {
                for (var key in obj) {
                    if (obj[key].toLowerCase() === str.toLowerCase()) {
                        scope.selectResult(result);
                        return;
                    }
                }
            }

            function searchTimerComplete(str) {
                if (str.length < minlength) {
                    return;
                }
                if (scope.localData) {
                    scope.$apply(function () {
                        getLocalResults(str);
                    });
                } else {
                    getRemoteResults(str);
                }
            }

            function processResults(responseData, str) {
                var i, description, image, text, formattedText, formattedDesc;
                if (responseData && responseData.length > 0) {
                    scope.results = [];
                    for (i = 0; i < responseData.length; i++) {
                        if (scope.titleField && scope.titleField !== '') {
                            text = formattedText = extractTitle(responseData[i]);
                        }
                        description = '';
                        if (scope.descriptionField) {
                            description = formattedDesc = extractValue(responseData[i], scope.descriptionField);
                        }
                        image = '';
                        if (scope.imageField) {
                            image = extractValue(responseData[i], scope.imageField);
                        }
                        if (scope.matchClass) {
                            formattedText = findMatchString(text, str);
                            formattedDesc = findMatchString(description, str);
                        }
                        scope.results[scope.results.length] = {
                            title: formattedText,
                            description: formattedDesc,
                            image: image,
                            originalObject: responseData[i]
                        };
                        if (scope.autoMatch) {
                            checkExactMatch(scope.results[scope.results.length - 1], {
                                title: text,
                                desc: description || ''
                            }, scope.searchStr);
                        }
                    }
                } else {
                    scope.results = [];
                }
            }
            scope.onFocusHandler = function () {
                if (scope.focusIn) {
                    scope.focusIn();
                }
            };
            scope.hideResults = function (event) {
                if (mousedownOn === scope.id + '_dropdown') {
                    mousedownOn = null;
                } else {
                    hideTimer = $timeout(function () {
                        clearResults();
                        scope.$apply(function () {
                            if (scope.searchStr && scope.searchStr.length > 0) {
                                inputField.val(scope.searchStr);
                            }
                        });
                    }, BLUR_TIMEOUT);
                    cancelHttpRequest();
                    if (scope.focusOut) {
                        scope.focusOut();
                    }
                }
            };
            scope.resetHideResults = function () {
                if (hideTimer) {
                    $timeout.cancel(hideTimer);
                }
            };
            scope.hoverRow = function (index) {
                scope.currentIndex = index;
            };
            scope.selectResult = function (result) {
                if (scope.matchClass) {
                    result.title = extractTitle(result.originalObject);
                    result.description = extractValue(result.originalObject, scope.descriptionField);
                }
                if (scope.clearSelected) {
                    scope.searchStr = null;
                } else {
                    scope.searchStr = result.title;
                }
                callOrAssign(result);
                clearResults();
                scope.clickhandler({
                    message: scope.searchStr
                });
                $timeout(function () {
                    if (scope.searchStr != "") {
                        var lastRoute = $location.path();
                        if (lastRoute.contains("/mui/customsearchresult")) if (lastRoute == "/mui/customsearchresults") $location.path("/mui/customsearchresult");
                        else $location.path("/mui/customsearchresults");
                        else $location.path("/mui/customsearchresult");
                    }
                }, 100);
            };
            scope.inputChangeHandler = function (str) {
                if (str.length < minlength) {
                    clearResults();
                }
                if (scope.inputChanged) {
                    str = scope.inputChanged(str);
                }
                return scope.onSelect({
                    text: str
                });
            };
            if (scope.fieldRequiredClass && scope.fieldRequiredClass !== '') {
                requiredClassName = scope.fieldRequiredClass;
            }
            if (scope.minlength && scope.minlength !== '') {
                minlength = scope.minlength;
            }
            if (!scope.pause) {
                scope.pause = PAUSE;
            }
            if (!scope.clearSelected) {
                scope.clearSelected = false;
            }
            if (!scope.overrideSuggestions) {
                scope.overrideSuggestions = false;
            }
            if (scope.fieldRequired && ctrl) {
                if (scope.initialValue) {
                    handleRequired(true);
                } else {
                    handleRequired(false);
                }
            }
            scope.textSearching = attrs.textSearching ? attrs.textSearching : TEXT_SEARCHING;
            scope.textNoResults = attrs.textNoResults ? attrs.textNoResults : TEXT_NORESULTS;
            inputField.on('keydown', keydownHandler);
            inputField.on('keyup', keyupHandler);
            responseFormatter = callFunctionOrIdentity('remoteUrlResponseFormatter');
            scope.$on('$destroy', function () {
                handleRequired(true);
            });
            $timeout(function () {
                var css = getComputedStyle(dd);
                isScrollOn = css.maxHeight && css.overflowY === 'auto';
            });
        }
    };
}]);
///#source 1 1 /app/controllers/mui-controller.js
(function (ng, app) {
    "use strict";

    function muiCtrl($window, $location, $localStorage, $timeout, $scope, $cookies, $rootScope, $filter, $compile, MuiService, $translate, $modal) {
        setTaskCreationAttachAssetObj();
        $scope.damtaskEntitySelectionVar = {
            SelectedAssetFile: []
        };
        $scope.PageAsset = {
            CmsAssetSelectionFiles: []
        };
        $scope.IsNavFromAssetToPage = {
            IsFromAsset: false
        };
        $scope.IsfromassetlibrarytoCMS = {
            Isfromassetlibrary: false,
            IstoCMSattachments: false
        };
        $scope.SelectedAssetIdFolderID = {
            "SelectedAssetIDs": []
        };
        $scope.EntityLevelfrFin = {
            levelfrFinancial: 0
        };
        $scope.ganttviewprelaoddates = {
            Dt: ""
        };
        $scope.GlobalNavigationCaption = "";
        $scope.GlobalNavigationID = {
            NavigationID: 0
        };
        $scope.GanttLock = true;
        $scope.muiiscalender = {
            Iscalender: false,
            CalenderID: 0
        };
        $scope.Isfromcalender_mui = false;
        $scope.CalenderisEdit_mui = false;
        $scope.EntitiesfrCalfromPlan = [];
        $scope.selectedentitiesforcalender_mui = [];
        $("#footercopyright").text("Copyright \u00A9 - " + (new Date).getFullYear() + " BrandSystems | ");
        $scope.tempNotificationID = 0;
        $scope.IsOlderMetadataVersion = {
            IsOlderVersion: false
        };
        var CurrentMetadataVersionID = localStorage.getItem("CurrentMetadataVersionID");
        var CurrentMetadataVersionName = localStorage.getItem("CurrentMetadataVersionName");
        $scope.CurrentMetadataVersionInfo = {
            ID: CurrentMetadataVersionID,
            Name: CurrentMetadataVersionName
        };
        $scope.Username = $.cookie('Username');
        $cookies['Username'] = $.cookie('Username');
        $scope.Activity = {
            IsActivitySectionLoad: false
        };
        $scope.Tgl = {};
        $scope.AssetFilesObj_Temp = {
            AssetFiles: []
        };
        $scope.LanguageContents = {};
        $scope.financialAttrType = 0;
        $scope.Islock_temp = false;
        $scope.Folderselect = {
            ID: 0,
            EntityID: 0,
            foldername: ''
        };
        $scope.SearchAsset = {
            Access: 0
        };
        $scope.settings = {};
        $scope.settingsM = {};
        $scope.settingsMetadata = {};
        $scope.GeneralSettings = {};
        $scope.Gnttsettings = {
            StartDate: "",
            EndDate: ""
        };
        $scope.selectedTemplateType = {};
        $scope.DefaultImageSettings = {
            'ImageSpan': new Date.create().getTime().toString()
        };
        $scope.DecimalSettings = [];
        $scope.DecimalSettings['FormatMoney'] = {
            Financial_FormatMoney: 0,
            Objective_FormatMoney: 0
        };
        $scope.DecimalSettings['FinancialAutoNumeric'] = {
            aSep: ' ',
            vMax: '99999999'
        };
        $scope.DecimalSettings['ObjectiveAutoNumeric'] = {
            aSep: ' ',
            vMax: '99999999'
        };
        $scope.TemplateAsset = {};
        $scope.currentCalendarEntities = {};
        $scope.IsShowallAssets = {
            Fromshowallassets: 0,
            DamViewName: 0
        };
        $scope.contentDatafrCMS = {};
        $scope.DamAssetTaskselectionFiles = {
            "AssetFiles": [],
            "ThumbnailSettings": [],
            "AssetDynamicData": [],
            "AssetSelectionFiles": []
        };
        if ($cookies["GlobalNavigationID"] != undefined && $cookies["GlobalNavigationID"] != 0) {
            $scope.GlobalNavigationID.NavigationID = $cookies["GlobalNavigationID"];
        }
        $scope.GetDefaultSettings = {
            DateFormat: ""
        };
        var langsettings = 1;
        $scope.LanguageContentsChange = function (val) {
            $scope.LanguageContents = val;
            $scope.Tgl.ShowAll = $translate.instant('LanguageContents.Res_2.Caption');
        }
        $scope.AssetSelectionFiles = [];
        $scope.AdminTaskCheckList = [];
        $scope.groupbySubLevelTaskObj = [];
        $scope.processingsrcobj = {
            processingplace: "task",
            processinglock: false
        };

        function setTaskCreationAttachAssetObj() {
            $scope.TaskCreationAttachAsset = {
                DSelectedAssetFiles: [],
                DAssetFiles: [],
                DAssetDynamicData: [],
                DAssetSelectionFiles: [],
                Dhtml: "",
                DattrRelation: []
            };
        }
        if ($.cookie('StartPage') == 30) GlobalIsWorkspaces = true;
        $('#headeruserimg').attr('src', "Handlers/UserImage.ashx?id=" + parseInt($cookies['UserId']) + "&time=" + $scope.DefaultImageSettings.ImageSpan);
        $scope.NewUserImgSrc = "Handlers/UserImage.ashx?id=" + parseInt($cookies['UserId']) + "&time=" + $scope.DefaultImageSettings.ImageSpan;
        MuiService.GetDecimalSettingsValue().then(function (Decimalresult) {
            if (Decimalresult.Response != null && Decimalresult.Response.length > 0) {
                $scope.DecimalSettings['FormatMoney'].Financial_FormatMoney = parseInt(Decimalresult.Response[0]);
                $scope.DecimalSettings['FinancialAutoNumeric'].vMax = Decimalresult.Response[1];
                $scope.DecimalSettings['FormatMoney'].Objective_FormatMoney = parseInt(Decimalresult.Response[2]);
                $scope.DecimalSettings['ObjectiveAutoNumeric'].vMax = Decimalresult.Response[3];
                if (parseInt(Decimalresult.Response[0]) > 0) {
                    if (parseInt(Decimalresult.Response[0]) == 1) $scope.DecimalSettings['FinancialAutoNumeric'].vMin = '0.0';
                    $scope.DecimalSettings['FinancialAutoNumeric'].aDec = ".";
                } else $scope.DecimalSettings['FinancialAutoNumeric'].vMin = '0';
                if (parseInt(Decimalresult.Response[2]) > 0) {
                    if (parseInt(Decimalresult.Response[2]) == 1) $scope.DecimalSettings['FinancialAutoNumeric'].vMin = '0.0';
                    $scope.DecimalSettings['FinancialAutoNumeric'].aDec = ".";
                } else $scope.DecimalSettings['ObjectiveAutoNumeric'].vMin = '0';
            } else {
                $scope.DecimalSettings['FinancialAutoNumeric'].vMin = '0';
                $scope.DecimalSettings['ObjectiveAutoNumeric'].vMin = '0';
            }
        });
        $window.TemplateID = 0;
        $window.ISTemplate = false;
        $window.XeditableDirectivesSource = {};
        $('body').on('mouseover', '[data-over="true"]', function () {
            var attr = $(this).attr('data-id');
            if (typeof attr !== 'undefined' && attr !== false) {
                var id = $(this).attr('data-id');
                var cssStyle = '<style type="text/css" id="MouseOverCss">';
                $('#MouseOverCss').remove();
                cssStyle += "a.mo" + id + "{background-color: #F3F8FB;";
                cssStyle += "background-image: linear-gradient(to bottom, #F7FBFC, #ECF4F9);";
                cssStyle += "background-repeat: repeat-x;";
                cssStyle += "border-left: 4px solid #80D4FF;";
                cssStyle += "text-decoration: none;}";
                cssStyle += "tr.mo" + id + " td{background-color: #F3F8FB;";
                cssStyle += "background-image: linear-gradient(to bottom, #F7FBFC, #ECF4F9);";
                cssStyle += "background-repeat: repeat-x;}";
                $('head').append(cssStyle + "</style>");
            } else {
                var id = $(this).attr('data-uniqueKey');
                var cssStyle = '<style type="text/css" id="MouseOverCss">';
                $('#MouseOverCss').remove();
                cssStyle += "li.mo" + id + " a {background-color: #F3F8FB;";
                cssStyle += "background-image: linear-gradient(to bottom, #F7FBFC, #ECF4F9);";
                cssStyle += "background-repeat: repeat-x;";
                cssStyle += "border-left: 4px solid #80D4FF;";
                cssStyle += "text-decoration: none;}";
                cssStyle += "tr.mo" + id + " td{background-color: #F3F8FB;";
                cssStyle += "background-image: linear-gradient(to bottom, #F7FBFC, #ECF4F9);";
                cssStyle += "background-repeat: repeat-x;}";
                cssStyle += "div.mo" + id + "{background-color: #F3F8FB;";
                cssStyle += "background-image: linear-gradient(to bottom, #F7FBFC, #ECF4F9);";
                cssStyle += "background-repeat: repeat-x;}";
                $('head').append(cssStyle + "</style>");
            }
        });
        $('body').on('mouseout', '[data-over="true"]', function () {
            $('#MouseOverCss').remove();
        });
        MuiService.GetAdditionalSettings().then(function (GetAdditionalresult) {
            if (GetAdditionalresult.StatusCode == 200) {
                $scope.GetDefaultSettings.DateFormat = GetAdditionalresult.Response[0].SettingValue;
            } else {
                $scope.DateFormat = "yyyy-mm-dd";
            }
        });
        var timeoutcancel = "";
        $scope.notifyLoad = false;
        $scope.LiveTaskListIDCollection = {
            "TasklistIDList": []
        };
        $scope.notifications = [];
        $scope.UnreadNotification = '';
        $scope.viewed = '';
        var countofunread = 0;
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            $.cookie('LogoImage', amazonURL + cloudsetup.BucketName + "/" + TenantFilePath + "logo.png");
        }
        $scope.logopath = $.cookie('LogoImage');
        GetNotificationFunction();

        function GetNotificationFunction() {
            MuiService.GetNotification(1).then(function (getnotification) {
                if (getnotification.StatusCode == 405) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1807.Caption'), function () {
                        $.removeCookie("UserId");
                        $.removeCookie("Username");
                        $.removeCookie("UserImage");
                        $.removeCookie("UserEmail");
                        window.location.replace("login.html" + GetReturnURL());
                    });
                }
                if (getnotification.Response != null) {
                    $scope.LiveTaskListIDCollection.TasklistIDList = [];
                    if (($('#notifyul').is(':visible') == false) && ($('#nonotification').is(':visible') == false)) {
                        if (getnotification.Response.m_Item1.length != 0 && getnotification.Response.m_Item1 != undefined) {
                            if ($scope.tempNotificationID != getnotification.Response.m_Item1[0].Notificationid) {
                                $scope.notifications = [];
                                $scope.UnreadNotification = '';
                                $('#nonotification').css('display', 'none');
                                for (var i = 0, obj; obj = getnotification.Response.m_Item1[i++];) {
                                    $scope.viewed = obj.Isviewed;
                                    $scope.notifications.push(obj);
                                    if (obj.Typeid == 4 || obj.Typeid == 6) if ($scope.notifyLoad) if (obj.Isviewed) {
                                        var tasklistList = [];
                                        tasklistList = $.grep($scope.LiveTaskListIDCollection.TasklistIDList, function (e) {
                                            return e.TaskLiStID == obj.TaskListID;
                                        });
                                        if (tasklistList.length == 0) $scope.LiveTaskListIDCollection.TasklistIDList.push({
                                            "TaskLiStID": obj.TaskListID,
                                            "EntityID": obj.TaskEntityID
                                        });
                                    }
                                }
                                countofunread = getnotification.Response.m_Item2;
                                if (countofunread == 0) {
                                    $scope.UnreadNotification = '';
                                } else {
                                    $scope.UnreadNotification = countofunread;
                                }
                                $scope.tempNotificationID = getnotification.Response.m_Item1[0].Notificationid;
                            } else if ($scope.tempNotificationID == getnotification.Response.m_Item1[0].Notificationid) {
                                for (var i = 0, obj; obj = getnotification.Response.m_Item1[i++];) {
                                    var dataobj = $.grep($scope.notifications, function (item, i) {
                                        return (item.Notificationid == obj.Notificationid);
                                    })[0];
                                    if (dataobj != null) {
                                        dataobj.NotificationHappendTime = obj.NotificationHappendTime;
                                    }
                                }
                                countofunread = getnotification.Response.m_Item2;
                                if (countofunread == 0) {
                                    $scope.UnreadNotification = '';
                                } else {
                                    $scope.UnreadNotification = countofunread;
                                }
                                $scope.tempNotificationID = getnotification.Response.m_Item1[0].Notificationid;
                            }
                            if ($scope.LiveTaskListIDCollection.TasklistIDList.length > 0) {
                                $scope.$broadcast('LiveTaskListUpdate', $scope.LiveTaskListIDCollection.TasklistIDList);
                            }
                        } else {
                            $scope.LiveTaskListIDCollection.TasklistIDList = [];
                            $scope.tempNotificationID = 0;
                            $scope.notifications = [];
                            $('#nonotification').css('display', 'block');
                        }
                    }
                    if (getnotification.Response.m_Item3.length != 0) {
                        $scope.broadcastmsglist = [];
                        if (getnotification.Response.m_Item3[0].Messages != null) {
                            for (var i = 0; i < getnotification.Response.m_Item3.length; i++) {
                                var datstartval = new Date(getnotification.Response.m_Item3[i]["Createdate"]);
                                var condatstartval = ConvertDateFromStringToString(ConvertDateToString(datstartval));
                                var resultdate = dateFormat(condatstartval.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3").toString('dd-MM-yyyy'), $scope.GetDefaultSettings.DateFormat);
                                getnotification.Response.m_Item3[i].Createdate = resultdate;
                                $scope.broadcastmsglist.push({
                                    Messages: getnotification.Response.m_Item3[i].Messages,
                                    Createdate: getnotification.Response.m_Item3[i].Createdate
                                });
                            }
                            $("#systemBroadcastMessage").modal('show');
                        } else {
                            $("#systemBroadcastMessage").modal('hide');
                        }
                    }
                    setTimeout(function () {
                        $scope.notifyLoad = true;
                        GetNotificationFunction();
                    }, Notifycyclestatementsdisplaytime);
                }
            });
        }
        var cancelRefresh;
        var liveTaskUpdateIntervalFunction = function () {
            cancelRefresh = setTimeout(function myFunction() {
                MuiService.GetTaskLIveUpdateRecords().then(function (result) {
                    var tasklistList = [];
                    if (result.Response != null) {
                        for (var i = 0, rec; rec = result.Response[i++];) {
                            tasklistList = $.grep($scope.LiveTaskListIDCollection.TasklistIDList, function (e) {
                                return e.TaskLiStID == rec.TaskListID;
                            });
                            if (tasklistList.length == 0) $scope.LiveTaskListIDCollection.TasklistIDList.push({
                                "TaskLiStID": rec.TaskListID,
                                "EntityID": rec.TaskEntityID
                            });
                        }
                        if ($scope.LiveTaskListIDCollection.TasklistIDList.length > 0) {
                            $scope.$broadcast('LiveTaskListUpdate', $scope.LiveTaskListIDCollection.TasklistIDList);
                        }
                    }
                    setTimeout(liveTaskUpdateIntervalFunction, Notifycyclestatementsdisplaytime);
                });
            }, Notifycyclestatementsdisplaytime);
        };
        setTimeout(liveTaskUpdateIntervalFunction, Notifycyclestatementsdisplaytime);
        $('body').on('click', 'i[data-id="UpdateMsgStatus"]', function (event) {
            MuiService.updateBroadcastMessagesbyuser().then(function (UpdateBroadcastMessages) {
                var a = UpdateBroadcastMessages.Response;
                $("#systemBroadcastMessage").modal('hide');
            });
        });
        $scope.SetUnreadClass = function (isviewed) {
            if (isviewed == false) return "Unread";
            else return "read";
        }
        $scope.AdjustNotificationBlockHeight = function () {
            $('#notifyulfooter').css('top', ($('#notifyul').height() + 85) + 'px');
        };
        $scope.attachmenteditpermission = false;
        MuiService.GetAttachmentEditFeature().then(function (attachementpermission) {
            $scope.attachmenteditpermission = attachementpermission.Response;
        });
        $scope.NotificationRedirectEntityPath = function (entityid) {
            var IsObjective = false;
            if ($(event.target).attr('data-IsObjective') != undefined)
                IsObjective = $(event.target).attr('data-IsObjective');
            var entitytypeid = $(event.target).attr('data-typeid');
            MuiService.IsActiveEntity(entityid).then(function (result) {
                if (result.Response == true) {
                    var entityId = entityid;
                    MuiService.CheckUserPermissionForEntity(entityid).then(function (CheckUserPermissionForEntity) {
                        if (CheckUserPermissionForEntity.Response == true) {
                            if (entitytypeid == 35) {
                                $location.path('/mui/calender/detail/section/' + entityId + '/overview');
                            }
                            else if (IsObjective == "True")
                                $location.path('/mui/planningtool/objective/detail/section/' + entityId + '/overview');
                            else
                                $location.path('/mui/planningtool/default/detail/section/' + entityId + '/overview');
                        }
                        else
                            bootbox.alert($translate.instant('LanguageContents.Res_1882.Caption'));
                    });
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1866.Caption'));
                }
            });
        }
        $scope.OpenTopNotificationTaskPopUp = function (taskid, entityid, typeid) {
            MuiService.IsActiveEntity(taskid).then(function (result) {
                if (result.Response == true) {
                    var modalInstance = $modal.open({
                        templateUrl: 'views/mui/task/TaskEdit.html',
                        controller: "mui.task.muitaskFlowEditCtrl",
                        resolve: {
                            items: function () {
                                return {
                                    TaskId: taskid,
                                    EntityId: entityid
                                };
                            }
                        },
                        scope: $scope,
                        windowClass: 'taskImprovementPopup TaskPopup TaskPopupTabbed popup-widthL',
                        backdrop: "static"
                    });
                    modalInstance.result.then(function (selectedItem) {
                        $scope.selected = selectedItem;
                    }, function () { });
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1866.Caption'));
                }
            });
        }
        $scope.OpenFundRequestActionfromNotify = function (fundReqID) {
            MuiService.IsActiveEntity(fundReqID).then(function (result) {
                if (result.Response == true) {
                    $("#feedFundingRequestModal").modal("show");
                    //$('#feedFundingRequestModal').trigger("onNewsfeedCostCentreFundingRequestsAction", [fundReqID]);
                    onNewsfeedCostCentreFundingRequestsAction_mui(fundreqid);
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1866.Caption'));
                }
            });
        }
        $scope.NotificationRedirectAssetPath = function (Assetid) {           
            assetedit(Assetid, false, false, '');
        }
        $(window).on("ontaskActionnotification", function (event) {
            $("#loadNotificationtask").modal("hide");
        });
        $scope.IsViewStateUpdate = function (flag) {
            var updatestae = {};
            updatestae.UserID = $scope.UserId;
            updatestae.FirstFiveNotifications = "";
            updatestae.Flag = 1;
            MuiService.UpdateIsviewedStatusNotification(updatestae).then(function (updateviewednotifications) {
                $scope.UnreadNotification = '';
            });
        }
        $("#mailicon").focusout(function () {
            MuiService.UpdatetopIsviewedStatusNotification().then(function (res) {
                var a = res.Response;
                if (res.StatusCode == 200) {
                    $('#mailicon').find('#notifyul').find('li:lt(5)').removeClass("Unread");
                    GetNotificationFunction();
                }
            });
        });
        $(document).on("OnUserImageChange", function (event, changedimageURL) {
            $('#headeruserimg').attr('src', "Handlers/UserImage.ashx?id=" + parseInt($cookies['UserId']) + "&time=" + $scope.DefaultImageSettings.ImageSpan);
            $scope.NewUserImgSrc = "Handlers/UserImage.ashx?id=" + parseInt($cookies['UserId']) + "&time=" + $scope.DefaultImageSettings.ImageSpan;
        });
        $(document).on("OnUserNameChange", function (event, changedUserName) {
            $scope.Username = changedUserName;
            $cookies["UserName"] = $scope.Username;
        });
        $(document).on("OnViewedAllNotifications", function (event) {
            GetNotificationFunction();
        });
        if (!angular.isDefined($localStorage.settings)) {
            $localStorage.settings = {};
        }
        setTimeout(getnavigationsource, 100);

        function getnavigationsource() {
            if (angular.isDefined($localStorage.settings)) {
                if (angular.isDefined($localStorage.settings["Navigation"])) {
                    clickHandler();
                } else {
                    clickHandler();
                }
            } else {
                makenavigationcall();
            }
        }

        function makenavigationcall() {
            MuiService.GetTopNavigation().then(function (getnavigations) {
                $localStorage.settings["Navigation"] = getnavigations.Response;
            });
        }

        function drawnavigation(Response) {
            var getnavigation = Response.TopMenu;
            var getnavigationChildren = Response.ChildMenu;
            var topNavigation = "<ul id=\"topmenucontainer\" class=\"nav pull-right\">";
            for (var i = 0; i < getnavigation.length; i++) {
                topNavigation += "<li class=\"divider-vertical\"></li>";
                var topItem = '';
                var childItem = '';
                for (var y = 0; y < getnavigationChildren.length; y++) {
                    if (getnavigationChildren[y].Parentid == getnavigation[i].Id) {
                        if (topItem.length == 0) {
                            if ($.cookie('StartPage') == getnavigation[i].Featureid) {
                                topItem = "<li  class=\"dropdown selected\"  data-url=\"/" + getnavigation[i].Url + "\"> <a data-NavID=" + getnavigation[i].Id + " data-Navcaption='" + getnavigation[i].Caption.toString() + "' data-searchtype=" + getnavigation[i].SearchType + " data-featureID=" + getnavigation[i].Featureid + " class=\"dropdown-toggle navclick\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-delay=\"500\" data-close-others=\"true\"><span class=\"caption\">" + getnavigation[i].Caption + "</span><b class=\"caret\"></b></a><ul class=\"dropdown-menu\">";
                            } else {
                                topItem = "<li class=\"dropdown\"  data-url=\"/" + getnavigation[i].Url + "\"> <a data-NavID=" + getnavigation[i].Id + " data-Navcaption='" + getnavigation[i].Caption.toString() + "' data-featureID=" + getnavigation[i].Featureid + " data-searchtype=" + getnavigation[i].SearchType + " class=\"dropdown-toggle navclick\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-delay=\"500\" data-close-others=\"true\"><span class=\"caption\">" + getnavigation[i].Caption + "</span><b class=\"caret\"></b></a><ul class=\"dropdown-menu\">";
                            }
                        }
                        childItem += "<li data-url=\"/" + getnavigationChildren[y].Url + "\"><a data-NavID=" + getnavigation[i].Id + " data-searchtype=" + getnavigation[i].SearchType + " data-Navcaption='" + getnavigationChildren[y].Caption.toString() + "' data-featureID=" + getnavigation[i].Featureid + " class=\"topsubmenu navclick\"  href=\"#/" + getnavigationChildren[y].Url + ChooseUrl(getnavigationChildren[y].Featureid, getnavigationChildren[y].Id) + "\">" + getnavigationChildren[y].Caption + "</a></li>";
                    }
                }
                if (topItem.length == 0) if ($.cookie('StartPage') == getnavigation[i].Featureid) {
                    topItem = "<li class=\"selected\" data-url=\"/" + getnavigation[i].Url + "\"><a data-NavID=" + getnavigation[i].Id + " data-searchtype=" + getnavigation[i].SearchType + " data-Navcaption='" + getnavigation[i].Caption.toString() + "' data-featureID=" + getnavigation[i].Featureid + " class=\"topmenu navclick\"   href=\"#/" + getnavigation[i].Url + ChooseUrl(getnavigation[i].Featureid, getnavigation[i].Id) + "\"><span class=\"caption\">" + getnavigation[i].Caption + "</span></a>";
                } else {
                    topItem = "<li data-url=\"/" + getnavigation[i].Url + "\"><a data-NavID=" + getnavigation[i].Id + " data-Navcaption='" + getnavigation[i].Caption.toString() + "' data-searchtype=" + getnavigation[i].SearchType + " data-featureID=" + getnavigation[i].Featureid + " class=\"topmenu navclick\"  href=\"#/" + getnavigation[i].Url + ChooseUrl(getnavigation[i].Featureid, getnavigation[i].Id) + "\"><span class=\"caption\">" + getnavigation[i].Caption + "</span></a>";
                } else childItem += "</ul>";
                topNavigation += topItem + childItem + "</li>";
            }
            topNavigation += "</ul>";
            $("#topnav").html($compile(topNavigation)($scope));
            clickHandler();
            $scope.focusTopMenu();
        }

        function clickHandler() {
            $(".navclick").click(function () {
                if ($('#cmsSnippetsContainer').data('contentbuilder')) $('#cmsSnippetsContainer').data('contentbuilder').destroy();
                $scope.GlobalNavigationID.NavigationID = $(this).attr("data-NavID");
                $cookies["GlobalNavigationID"] = $(this).attr("data-NavID");
                $scope.GlobalNavigationCaption = $(this).attr("data-Navcaption");
                $scope.searchtext = "";
                $scope.myData = [];
                var ExternalLinkFeatureID = "17"
                $scope.$broadcast('navigationClick', $(this).attr("data-featureID"));
                var searchCategory = $(this).attr("data-searchtype");
                var searchobj = $.grep($scope.searchObject.SearchOptions, function (e) {
                    return e.id === parseInt(searchCategory) && e.isshow == true;
                })[0];
                if (searchobj != null) {
                    $scope.searchObject.SearchText = searchobj.Lable;
                    $scope.searchObject.CurrentSearchType = searchobj.Lable;
                    $scope.searchObject.ActualSearchTerm = searchobj.Actual_Type;
                    $scope.searchObject.SearchTypeID = searchobj.id;
                }
                if ($(this).attr("data-featureID") == ExternalLinkFeatureID) {
                    $scope.$broadcast('loadexternalurl', $(this).attr("data-featureID"));
                    $scope.subview = " ";
                }
                // default tab
                localStorage.setItem("lasttab", "overview");

                if ($(this).attr("data-Navcaption") !== "My Brief")
                    window.location = "testindex.html" + $(this).attr("href");
                else {
                    $("#topmenucontainer li").removeClass('selected');
                    $(this).parents('li').removeClass('selected');
                    $(this).parents('li').removeClass('selected'); $(this).parents('li').addClass("selected");

                }
            });
        }



        $scope.focusTopMenu = function () {
            $('.topmenu').click(function (event) {
                $('#topmenucontainer .active').removeClass('active');
                $('#topmenucontainer .selected').removeClass('selected');
                $(this).parent().addClass('selected');
            });
            $('.topsubmenu').click(function (event) {
                $('#topmenucontainer .active').removeClass('active');
                $('#topmenucontainer .selected').removeClass('selected');
                $(this).parent().parent().parent().addClass('selected');
                $(this).parent().addClass('active');
            });
        }
        MuiService.SetIsCurrentWorkingXml().then(function (result) { });
        $scope.searchresponse = [];
        $scope.myData = [];
        $scope.stype = "";
        MuiService.GetSearchtype($scope.UserId).then(function (objGetSearchtype) {
            if (objGetSearchtype.Response != null) {
                if (objGetSearchtype.Response.length != 0) {
                    if (objGetSearchtype.Response[0] != undefined) {
                        $scope.stype = objGetSearchtype.Response[0].SearchType;
                        $scope.searchObject.SearchText = $scope.searchObject.SearchOptions[$scope.stype].Lable;
                    }
                }
            }
        });
        MuiService.GetassignedAcess($scope.UserId).then(function (objGetassignedAcess) {
            var rslt = $.map(objGetassignedAcess.Response, function (rel) {
                return rel.caption;
            }).join(" ,");
            if (rslt.contains("Asset library")) {
                $scope.searchObject.SearchOptions[4].isshow = true;
            }
            if (rslt.contains("Attachments")) {
                $scope.searchObject.SearchOptions[3].isshow = true;
            }
            if (rslt.contains("Tasks")) {
                $scope.searchObject.SearchOptions[2].isshow = true;
            }
            if (rslt.contains("Pages")) {
                $scope.searchObject.SearchOptions[5].isshow = true;
            }
            if (rslt.contains("Tags")) {
                $scope.searchObject.SearchOptions[6].isshow = true;
            }
            if (rslt.contains("Productions")) {
                $scope.searchObject.SearchOptions[1].isshow = true;
            }
            if (rslt.contains("Productions,Tasks,Attachments,Asset library,Pages,Tags")) {
                var blist = [];
                blist = $.grep($scope.searchObject.SearchOptions, function (e) {
                    return e.isshow == false
                });
                for (var i = 0; i < blist.length; i++) {
                    $scope.searchObject.SearchOptions[blist[i].id].isshow = true;
                }
            }
            if (objGetassignedAcess.Response.length == 0) {
                $scope.searchObject.SearchText = "";
                $scope.searchObject.SearchOptions = "";
                $scope.slistdisplay = false;
            }
            if (objGetassignedAcess.Response.length > 1) {
                $scope.searchObject.SearchOptions[0].isshow = true;
            }
            if ($scope.stype != "" && $scope.stype != null && $scope.stype != undefined) {
                if ($scope.searchObject.SearchOptions[$scope.stype].isshow == false) {
                    var alist = [];
                    alist = $.grep($scope.searchObject.SearchOptions, function (e) {
                        return e.isshow == true
                    });
                    $scope.searchObject.SearchText = alist[0].Lable;
                }
            }
        });
        $scope.slistdisplay = true;
        $scope.searchObject = {
            "SearchOptions": [{
                Lable: "All",
                id: 0,
                isSelected: true,
                "Actual_Type": "All",
                "isshow": false
            }, {
                Lable: "Productions",
                id: 1,
                isSelected: true,
                "Actual_Type": "Productions",
                "isshow": false
            }, {
                Lable: "Tasks",
                id: 2,
                isSelected: true,
                "Actual_Type": "Task",
                "isshow": false
            }, {
                Lable: "Attachments",
                id: 3,
                isSelected: true,
                "Actual_Type": "Attachments",
                "isshow": false
            }, {
                Lable: "Asset library",
                id: 4,
                isSelected: true,
                "Actual_Type": "Assetlibrary",
                "isshow": false
            }, {
                Lable: "Pages",
                id: 5,
                isSelected: true,
                "Actual_Type": "CmsNavigation",
                "isshow": false
            }, {
                Lable: "Tags",
                id: 6,
                isSelected: true,
                "Actual_Type": "Tags",
                "isshow": false
            }],
            "SearchText": "All",
            "CurrentSearchType": "All",
            "ActualSearchTerm": "All",
            "SearchTypeID": 0,
            "SearchAssetLibrary": "",
        };
        $('.dropdown-toggle').dropdown();
        $('.collapse').collapse('hide');
        $scope.setSearchType = function (option) {
            if ($scope.searchassetlibrary == true) {
                if (option.id != 4) {
                    $scope.searchassetlibrary = false;
                }
            } else if ($scope.searchassetlibrary == false) {
                if (option.id == 4) {
                    $scope.searchassetlibrary = true;
                }
            }
            $('.dropdown-toggle').dropdown();
            $('.collapse').collapse('hide');
            $scope.searchObject.SearchText = option.Lable;
            $scope.searchObject.CurrentSearchType = option.Lable;
            $scope.searchObject.ActualSearchTerm = option.Actual_Type;
            $scope.searchObject.SearchTypeID = option.id;
        }
        $scope.searchtextchange = function (text) {
            $scope.searchtext = text;
        };
        $scope.OpenTaskPopUp = function () {
            $(document).on('click', 'a[data-id="openpopupfortaskeditforviewall"]', function (event) {
                event.stopImmediatePropagation();
                var taskID = $(this).attr('data-taskid');
                var entityID = $(this).attr('data-entityid');
                var modalInstance = $modal.open({
                    templateUrl: 'views/mui/task/TaskEdit.html',
                    controller: "mui.task.muitaskFlowEditCtrl",
                    resolve: {
                        items: function () {
                            return {
                                TaskId: taskID,
                                EntityId: entityID
                            };
                        }
                    },
                    scope: $scope,
                    windowClass: 'taskImprovementPopup TaskPopup TaskPopupTabbed popup-widthL',
                    backdrop: "static"
                });
                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () { });
            });
        }
        $scope.LoadSearch = function () {
            $(document).click(function () {
                $('#searchlist').parent().removeClass('open');
                $scope.searchtext = "";
            });
            var Text = $scope.searchtext;
            var ModuleIds = 1;
            if (Text.length > 0) {
                $scope.searchresponse = [];
                $scope.myData = [];
                var search = {};
                search.Text = Text;
                search.ModuleIds = ModuleIds;
                search.IsGlobalAdmin = IsGlobalAdmin;
                $scope.search = $scope.searchtext;
                MuiService.QuickSearch(search).then(function (searchresult) {
                    $scope.searchresponse = [];
                    $scope.myData = [];
                    $scope.searchresponse = searchresult.Response;
                    $scope.myData = $scope.searchresponse;
                    $('#searchlist').parent().addClass('open');
                });
            } else {
                $scope.myData = [];
                $('#searchlist').parent().removeClass('open');
            }
        };
        $scope.onSelect = function (text) {
            console.log(text);
        }
        $scope.searchassetlibrary = false;
        $scope.loadresultpage = function (event) {
            if ($scope.searchtext != "") {
                $scope.searchassetlibrary = false;
                var lastRoute = $location.path();
                if (lastRoute.contains("/mui/customsearchresult")) if (lastRoute == "/mui/customsearchresults") $location.path("/mui/customsearchresult");
                else $location.path("/mui/customsearchresults");
                else $location.path("/mui/customsearchresult");
                var currentpath = lastRoute.replace("/", '');
                if (currentpath == "/mui/marcommediabank") {
                    $scope.searchassetlibrary = true;
                    $scope.searchObject.SearchAssetLibrary = [{
                        Lable: "Asset library",
                        id: 4,
                        isSelected: true,
                        "Actual_Type": "Assetlibrary",
                        "isshow": true
                    }];
                }
            }
        }

        function funloadoverview(id, event) {
            $location.path('/mui/planningtool/default/detail/section/' + id + '/overview');
        }
        $scope.searchloadactivity = function (event, id, typeid, parentid, level, ThumbnailUrl) {
            SearchEntityID = id;
            $scope.EntityTypeID = typeid;
            EntityTypeID = typeid;
            SearchEntityLevel = level;
            if (ThumbnailUrl == "DAM") {
                $scope.$broadcast('openassetpopup', id, true);
            } else {
                if (typeid == 6) {
                    $location.path('/mui/planningtool/default/detail/section/' + id + '/overview');
                } else if (typeid == 5) {
                    $location.path('/mui/planningtool/costcentre/detail/section/' + id + '/overview');
                } else if (typeid == 10) {
                    $location.path('/mui/objective/detail/section/' + id + '/overview');
                } else if (typeid == 2 || typeid == 3 || typeid == 31 || typeid == 30) {
                    $scope.TaskID = id;
                    $scope.EntityTypeID = typeid;
                    $scope.EntityID = parentid;
                    $location.path('/mui/task');
                } else if (typeid == 4) { } else {
                    $location.path('/mui/planningtool/default/detail/section/' + id + '/overview');
                }
            }
            $scope.searchtext = "";
            $('#searchlist').parent().removeClass('open');
        }

        function ViewRootLevelEntity(ID, event) {
            var IDList = new Array();
            $window.ListofEntityID = [];
            if (ID == null) {
                IDList = GetRootLevelSelectedAll();
            } else {
                IDList.push(ID);
            }
            if (IDList.length !== 0) {
                $window.ListofEntityID = IDList;
                var localScope = $(event.target).scope();
                $location.path("/mui/planningtool/default/detail/ganttview");
            }
        }

        function ViewRootLevelEntityCostCenter(ID, event) {
            var IDList = new Array();
            $window.ListofEntityID = [];
            if (ID == null) {
                IDList = GetRootLevelSelectedAll();
            } else {
                IDList.push(ID);
            }
            if (IDList.length !== 0) {
                $window.ListofEntityID = IDList;
                event.preventDefault();
                var localScope = $(event.target).scope();
                $location.path("mui/planningtool/costcentre/detail/ganttview");
            }
        }

        function ViewRootLevelEntityObjective(ID, event) {
            var IDList = new Array();
            $window.ListofEntityID = [];
            IDList.push(ID);
            if (IDList.length !== 0) {
                $window.ListofEntityID = IDList;
                event.preventDefault();
                var localScope = $(event.target).scope();
                $location.path("/mui/objective/detail/listview");
            }
        }
        $('#result').click(function (event) {
            var localScope = $(event.target).scope();
            localScope.$apply(function () {
                if ($('.searchresultAvailability').length > 0) {
                    $(window).trigger('loadsearchresult')
                } else {
                    $location.path('/mui/searchresult/');
                }
            });
        });
        $scope.popMe = function (mypage, myname, w, h) {
            var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
            var win = window.open(mypage, myname, winprops)
        };

        function ChooseUrl(Featureid, navID) {
            switch (Featureid) {
                case 10, 11, 12, 13, 14, 15, 16, 21:
                    return "";
                    break;
                case 17:
                    return "/" + navID;
                    break;
                default:
                    return "";
            }
        }
        $scope.LogoutUser = function () {
            $scope.Logout();
        };
        $scope.DuplicateEntityPageType = "";
        $scope.EntitytoDuplicate = [];
        $scope.EntityleveltoDuplicate = 0;
        $scope.ClearDuplicateVar = function () {
            $scope.varDuplicate.IsEntity = false;
            $scope.varDuplicate.IsEntityStatus = false;
            $scope.varDuplicate.IsAttachments = false;
            $scope.varDuplicate.IsMember = false;
            $scope.varDuplicate.IsMilestone = false;
            $scope.varDuplicate.IsTasks = false;
            $scope.varDuplicate.IsTaskCheckList = false;
            $scope.varDuplicate.IsTaskAttachment = false;
            $scope.varDuplicate.IsCostcentre = false;
            $scope.varDuplicate.IsPlannedBudget = false;
            $("#duplicateEnitynames").html("");
        }
        $scope.DuplicateEntity = function (duplicatedata, entitylevel, PageName) {
            $scope.DuplicateEntityPageType = PageName;
            $("#duplicateEnitynames").html("");
            if ($scope.DuplicateEntityPageType == "DETAIL") $("#nooftimes").show();
            else if ($scope.DuplicateEntityPageType == "LIST") {
                if (duplicatedata.length == 1) $("#nooftimes").show();
                else $("#nooftimes").hide();
            }
            $scope.EntitytoDuplicate = duplicatedata;
            $scope.EntityleveltoDuplicate = entitylevel;
            $scope.duplicateTimes = 1;
            $scope.DiscardChildDuplicate = false;
            $scope.varDuplicate = {
                IsEntity: false,
                IsEntityStatus: false,
                IsAttachments: false,
                IsMember: false,
                IsMilestone: false,
                IsTasks: false,
                IsTaskCheckList: false,
                IsTaskAttachment: false,
                IsCostcentre: false,
                IsPlannedBudget: false
            }
            var appendHtml = "<label class='control-label'>Entity Name 1</label><div class='controls margin-bottom10x'><input id='duplicatentityid0' type='text'/></div>";
            $("#duplicateEnitynames").append(appendHtml);
            $("#duplicateentity").show();
            $("#duplicatetask").hide();
            $("#duplicatecostcentre").hide();
        }
        $scope.CreateDuplicateEntitys = function () {
            if ($scope.duplicateTimes == "" || $scope.duplicateTimes == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4591.Caption'));
                return false;
            }
            if ($scope.varDuplicate.IsEntity == false && $scope.varDuplicate.IsTasks == false && $scope.varDuplicate.IsCostcentre == false) {
                bootbox.alert($translate.instant('LanguageContents.Res_4606.Caption'));
                return false;
            }
            var tempentitynames = [];
            var stas = false;
            if ($scope.DuplicateEntityPageType == "DETAIL" || ($scope.DuplicateEntityPageType == "LIST" && $scope.EntitytoDuplicate.length == 1)) {
                $('#duplicateEnitynames div').each(function (index) {
                    if ($('#duplicatentityid' + index).val() == "") {
                        bootbox.alert($translate.instant('LanguageContents.Res_4592.Caption'));
                        stas = true;
                        return false;
                    }
                    tempentitynames.push($('#duplicatentityid' + index).val());
                });
                if (stas == true) return false;
            } else tempentitynames.push("");
            $("#showhideprogress").show();
            $('#DuplicateEntity').attr('disabled', true);
            var duplicate = {};
            duplicate.EntityID = $scope.EntitytoDuplicate;
            duplicate.ParentLevel = $scope.EntityleveltoDuplicate;
            duplicate.duplicateEntityNames = tempentitynames;
            duplicate.DuplicateList = $scope.varDuplicate;
            if ($scope.duplicateTimes == undefined || $scope.duplicateTimes == "") duplicate.DuplicateTimes = 1;
            else duplicate.DuplicateTimes = $scope.duplicateTimes;
            duplicate.IsDuplicateChild = $scope.DiscardChildDuplicate;
            MuiService.DuplicateEntities(duplicate).then(function (SaveDuplicate) {
                if (SaveDuplicate.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4321.Caption'));
                } else {
                    $scope.duplicateTimes = "";
                    $scope.DiscardChildDuplicate = false;
                    $scope.ClearDuplicateVar();
                    var duplicateLists = SaveDuplicate.Response;
                    if ($scope.DuplicateEntityPageType == "DETAIL") $scope.$broadcast("DETAILEVENT", duplicateLists);
                    else if ($scope.DuplicateEntityPageType == "LIST") $scope.$broadcast("LISTEVENT", duplicateLists);
                    setTimeout(function () {
                        $("#showhideprogress").hide();
                        $('#DuplicateEntity').attr('disabled', false);
                        $('#DuplicateModels').modal('hide');
                        NotifySuccess($translate.instant('LanguageContents.Res_4226.Caption'));
                    }, 1000);
                }
            });
        }
        $("#duplicateTimes").keypress(function (event) {
            var valid = (event.which >= 48 && event.which <= 57);
            if (!valid) {
                event.preventDefault();
                return false;
            }
        });
        $("#duplicateTimes").keyup(function (event) {
            var appendHtml = "";
            $("#duplicateEnitynames").html("");
            if ($scope.duplicateTimes == "" || $scope.duplicateTimes == 0) {
                event.preventDefault();
                return false;
            }
            var cnt = 1;
            for (var i = 0; i < parseInt($scope.duplicateTimes) ; i++) {
                appendHtml = appendHtml + "<label class='control-label'>Entity Name " + cnt + "</label><div class='controls margin-bottom10x'><input id='duplicatentityid" + i + "' type='text'/></div>"
                cnt++;
            }
            $("#duplicateEnitynames").append(appendHtml);
        });
        $("#entitytype").click(function (e) {
            $("#duplicateentity").show();
            $("#duplicatetask").hide();
            $("#duplicatecostcentre").hide();
        });
        $("#tasktype").click(function (e) {
            $("#duplicatetask").show();
            $("#duplicateentity").hide();
            $("#duplicatecostcentre").hide();
        });
        $("#costcentretype").click(function (e) {
            $("#duplicatecostcentre").show();
            $("#duplicatetask").hide();
            $("#duplicateentity").hide();
        });
        $("#chkentitytype").click(function (e) {
            if ($scope.varDuplicate.IsEntity == false) $("#entitytype").addClass("color-primary");
            else {
                $("#entitytype").removeClass("color-primary");
                $scope.varDuplicate.IsEntityStatus = false;
                $scope.varDuplicate.IsMilestone = false;
                $scope.varDuplicate.IsAttachments = false;
                $scope.DiscardChildDuplicate = false;
                $scope.varDuplicate.IsMember = false;
            }
        });
        $("#chktasktype").click(function (e) {
            if ($scope.varDuplicate.IsTasks == false) $("#tasktype").addClass("color-primary");
            else {
                $("#tasktype").removeClass("color-primary");
                $scope.varDuplicate.IsTaskAttachment = false;
                $scope.varDuplicate.IsTaskCheckList = false;
            }
        });
        $("#chkcostcentretype").click(function (e) {
            if ($scope.varDuplicate.IsCostcentre == false) $("#costcentretype").addClass("color-primary");
            else {
                $("#costcentretype").removeClass("color-primary");
                $scope.varDuplicate.IsPlannedBudget = false;
            }
        });
        function assetedit(Assetid, IsLock, isNotify, ViewType) {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/DAM/assetedit.html',
                controller: "mui.DAM.asseteditCtrl",
                resolve: {
                    params: function () {
                        return {
                            AssetID: Assetid,
                            IsLock: IsLock,
                            isNotify: true,
                            viewtype: 'ViewType'
                        };
                    }
                },
                scope: $scope,
                windowClass: 'iv-Popup',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }
        $scope.$on('callBackAssetEditinMUI', function (event, ID, isLock, isNotify, viewtype) {
            $scope.Islock_temp = isLock;
            setTimeout(function () {
                assetedit(ID, isLock, isNotify, viewtype);
            }, 100);
        });
        $scope.$on('callBackExpireactionAssetEditvalues', function (event, SourceID, EntityID, AttributeID, Attributevalue) {
            setTimeout(function () {
                $scope.$broadcast('updateExpireactionAssetEditvalues', SourceID, EntityID, AttributeID, Attributevalue);
            }, 100);
        });
        $scope.$on('callBackExpireactionEntityEditvalues', function (event, SourceID, EntityID, AttributeID, Attributevalue) {
            setTimeout(function () {
                $scope.$broadcast('updateExpireactionEntityEditvalues', SourceID, EntityID, AttributeID, Attributevalue);
            }, 100);
        });
        $scope.$on('callBackExpireactionAssetEditvalues', function (event, SourceID, EntityID, AttributeID, Attributevalue) {
            setTimeout(function () {
                $scope.$broadcast('updateExpireactionAssetEditvalues', SourceID, EntityID, AttributeID, Attributevalue);
            }, 100);
        });
        $scope.$on('callBackExpireactionEntityEditvalues', function (event, SourceID, EntityID, AttributeID, Attributevalue) {
            setTimeout(function () {
                $scope.$broadcast('updateExpireactionEntityEditvalues', SourceID, EntityID, AttributeID, Attributevalue);
            }, 100);
        });
        $scope.$on('callBackExpireactiontaskEditvalues', function (event, SourceID, EntityID, AttributeID, Attributevalue) {
            setTimeout(function () {
                $scope.$broadcast('updateExpireactiontaskEditvalues', SourceID, EntityID, AttributeID, Attributevalue);
            }, 100);
        });

        function loadExpireAction(dateExpireval, SourceID, SourceEnityID, SourceFrom, SourcetypeID, productionEntityID, SourceAttributeID, SourceAttributename) {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/expirehandleractions.html',
                controller: "mui.muiexpireaddActionsCtrl",
                resolve: {
                    params: function () {
                        return {
                            dateExpireval: dateExpireval,
                            AssetEditID: SourceID,
                            assetEntityId: SourceEnityID,
                            sourcefrom: SourceFrom,
                            assettypeidfrbtn: SourcetypeID,
                            ProductionEntityID: productionEntityID,
                            SourceAttributeID: SourceAttributeID,
                            SourceAttributename: SourceAttributename
                        };
                    }
                },
                scope: $scope,
                windowClass: 'expirehandlerAddActionsPopup popup-widthM',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }

        $scope.$on('CallExpireaction', function (event, dateExpireval, SourceID, SourceEnityID, SourceFrom, SourcetypeID, productionEntityID, SourceAttributeID, SourceAttributename) {
            setTimeout(function () {
                $scope.$broadcast('OpenExpireactionpopup', dateExpireval, SourceID, SourceEnityID, SourceFrom, SourcetypeID, productionEntityID, SourceAttributeID, SourceAttributename);
            }, 100);
            loadExpireAction(dateExpireval, SourceID, SourceEnityID, SourceFrom, SourcetypeID, productionEntityID, SourceAttributeID, SourceAttributename);
        });
        $scope.$on('callBackObjectiveExpireactionEntityEditvalues', function (event, SourceID, EntityID, AttributeID, Attributevalue) {

            $timeout(
                function () {
                    $scope.$broadcast('updateObjectiveExpireactionEntityEditvalues', SourceID, EntityID, AttributeID, Attributevalue);
                }, 100);
        });
        $scope.optmakerObj = {
            EntityID: 0,
            FolderID: 0,
            AssetTypeID: 0,
            CreatedByID: 0,
            IsWord: false
        };
        $scope.SetOptmakerObj = function (eid, fid, atype, uid, isWord) {
            $scope.optmakerObj.EntityID = eid;
            $scope.optmakerObj.FolderID = fid;
            $scope.optmakerObj.AssetTypeID = atype;
            $scope.optmakerObj.CreatedByID = uid;
            $scope.optmakerObj.IsWord = isWord;
        }
        $scope.getOptmakerObj = function () {
            return $scope.optmakerObj;
        }
        $scope.$on('CallBackThumbnailAssetViewfrmMUI', function (event, ID) {
            $scope.$broadcast('CallBackThumbnailAssetView', ID);
        });
        $scope.damlightbox = {
            lightboxselection: []
        };
        $scope.damclipboard = {
            assetselection: [],
            actioncode: 0
        };
        $scope.SearchObjtxt = {
            SearchType: 1
        };
        $scope.copyrightYear = (new Date()).getFullYear();
        $scope.LoadassetsfrmOptimaker = function () {
            $scope.$broadcast('CallbackfromOptiIfrm', 0);
        };
        $scope.$on("$destroy", function () {
            setTimeout.cancel(cancelRefresh);
        });
        $scope.set_color = function (clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            }
            else {
                return ''
            };
        }
        $scope.searchSupport = function (keyEvent) {
            if (keyEvent.which === 13) if ($scope.searchtext != "") {
                $.cookie("searchtext", $scope.searchObject.searchtext);
                $scope.loadresultpage(keyEvent);
            } else return false;
            else return false;
        }
        $scope.focusinControl = {};
        $scope.clearSearchTerm = function (event) {
            $scope.searchtext = "";
            $scope.myData = [];
            $scope.focusinControl.ClearTextSearch();
        }
        $scope.$on("clearsearchtextforcefully", function () {
            $scope.searchtext = "";
            $scope.myData = [];
            $scope.focusinControl.ClearTextSearch();
        });
        $scope.remoteUrlRequestFn = function (str) {
            return {
                q: str
            };
        };
        $scope.UiColorPickerobj = {
            "UiColorPicker": {
                preferredFormat: "hex",
                showInput: true,
                showAlpha: false,
                allowEmpty: true,
                showPalette: true,
                showPaletteOnly: false,
                togglePaletteOnly: true,
                togglePaletteMoreText: 'more',
                togglePaletteLessText: 'less',
                showSelectionPalette: true,
                chooseText: "Choose",
                cancelText: "Cancel",
                showButtons: true,
                clickoutFiresChange: true,
                palette: [
					["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
					["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)", "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
					["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)", "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)", "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)", "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            }
        };
        $scope.focusState = 'None';
        $scope.focusIn = function () {
            var focusInputElem = document.getElementById('ex12_value');
            $scope.focusState = 'In';
            focusInputElem.classList.remove('small-input');
        }
        $scope.focusOut = function () {
            var focusInputElem = document.getElementById('ex12_value');
            $scope.focusState = 'Out';
            focusInputElem.classList.add('small-input');
        }
        $scope.disableInput = true;
        $scope.SearchAssetLibrarySettings = {};
        setTimeout(function () {
            GetDAMViewSettings();
        }, 100);

        function GetDAMViewSettings() {
            MuiService.GetDAMViewSettings().then(function (data) {
                if (data.Response != null) {
                    $scope.SearchAssetLibrarySettings["ThumbnailSettings"] = data.Response[0].ThumbnailSettings;
                    $scope.SearchAssetLibrarySettings["SummaryViewSettings"] = data.Response[0].SummaryViewSettings;
                    $scope.SearchAssetLibrarySettings["ListViewSettings"] = data.Response[0].ListViewSettings;
                }
            });
        }
        $scope.MediaBankSettings = {
            Redirectfromdam: false,
            FolderId: '',
            folderName: '',
            EntityId: '',
            ViewType: '',
            directfromMediaBank: false,
            ProductionTypeID: '',
            FolderName: '',
            EditableAssetGuid: '',
            EditableAssetExt: '',
            AssetID: 0,
            frmCMS: false
        };
        $scope.$on('ThumbnailInitiator', function (event, thumbnailId, assetId, folderid) {
            $scope.$broadcast('StartchangePreview', thumbnailId, assetId, folderid);
        });
        $scope.$on('LoadMuiAttachment', function (event, id) {
            $scope.$broadcast('ReloadAssetView');
        });
        $scope.$on('ChangeAssetPreview', function (event, assetid) {
            $scope.$broadcast('ReloadAssetPreview', assetid);
        });
        $scope.$on('onMyfundingRequestkAction', function (event) {
            $scope.$broadcast('onMyfundingRequestkActions');
        });
        $scope.$on("onCostcentreFinancialAccept", function (event, urEntityId, sttus) {
            $scope.$broadcast("RefreshFundingRequest");
        });
        $scope.handlersearchtext = function handler(message) {
            $scope.searchtext = message;
        }
        $scope.$on('redirectMUICreateTask', function (event, place, tasktype, id, assetid) {
            var opentaskpopupdata = {};
            opentaskpopupdata.AssetID = assetid;
            opentaskpopupdata.IsTask = true;
            opentaskpopupdata.IsLock = false;
            opentaskpopupdata.TaskType = tasktype;
            opentaskpopupdata.Fromplace = "Task";
            opentaskpopupdata.Type = "addtaskfromasset";
            $scope.$broadcast('callTaskCreationPage', opentaskpopupdata);
        });
        $scope.$on('callTaskCreationPage_Temp', function (event, opentaskpopupdata) {
            $scope.$broadcast('callTaskCreationPage', opentaskpopupdata);
        });
        $scope.$on('ReformatInitiator', function (event, thumbnailId, assetId, folderid, EntityID) {
            $scope.$broadcast('StartReformat', thumbnailId, assetId, folderid, EntityID);
        });
        $scope.$on('CallbackStartReformat', function (event, thumbnailId, assetId, folderid, EntityID) {
            $scope.$broadcast('StartReformat', thumbnailId, assetId, folderid, EntityID);
        });
        $scope.$on('CallbackStartReformatclose', function (event, assetId, folderid) {
            if (folderid > 0) {
                setTimeout(function () {
                    $scope.$broadcast('CallbackLoadAssets', folderid, $scope.Folderselect.foldername);
                }, 200);
            } else {
                setTimeout(function () {
                    $scope.$broadcast('ReloadAssetViewreformate');
                }, 100);
            }
        });
        $scope.$on('ReformatInitiatorDam', function (event, thumbnailId, assetId, folderid, EntityID) {
            $scope.$broadcast('StartReformat', thumbnailId, assetId, folderid, EntityID);
        });
        $scope.$on('loadAssetDetailSectionfrThumbnail', function (event, assetList) {
            $scope.$broadcast('loadAssetDetailSection_th', assetList);
        });
        $scope.$on('loadAssetDetailSectionfrSummary', function (event, assetList) {
            $scope.$broadcast('loadAssetDetailSection_th_Summary', assetList);
        });
        $scope.$on('loadAssetDetailSectionfrList', function (event, assetList) {
            $scope.$broadcast('loadAssetDetailSection_th_List', assetList);
        });
        $scope.$on('loadShowallAssetDetailSection', function (event, assetList, view) {
            $scope.$broadcast('loadShowallAssetDetailSectionfr_th', assetList, view);
        });
        $scope.$on('resetTaskCreationAttachAssetObj', function (event) {
            setTaskCreationAttachAssetObj();
        });
        function onNewsfeedCostCentreFundingRequestsAction_mui(fundreqid) {
            fundreqid = parseInt(fundreqid.toString());
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/planningtool/costcentre/CostCentreFundRequestAction.html',
                controller: "mui.planningtool.costcentre.CostCentreFundRequestActionCtrl",
                resolve: {
                    params: function () {
                        return {
                            ID: fundreqid
                        };
                    }
                },
                scope: $scope,
                windowClass: 'FundingRequestPopup popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }

        $scope.$on('onNewsfeedCostCentreFundingRequestsAction', function (event,fundreqid) {
            // $scope.$broadcast('onNewsfeedCostCentreFundingRequestsAction_mui', fundreqid);
            onNewsfeedCostCentreFundingRequestsAction_mui(fundreqid)
        });

        $scope.IsSuperAdmin = {
            access: false
        };
        MuiService.GetGlobalRoleUserByID($scope.UserId).then(function (getroleuserbyid) {
            $scope.RolesOfUser = getroleuserbyid.Response;
            var res = Math.min.apply(Math, $scope.RolesOfUser)
            if (res == -1) {
                $scope.settingsM0 = true;            
                $scope.IsSuperAdmin = {
                    access: true
                }
            }
            var Roles = $.grep($scope.RolesOfUser, function (e) {
                e.globalroleid != 1
            });
            MuiService.GetSuperglobalacl().then(function (objgetgetsuperglobalacl) {
                $scope.resul = objgetgetsuperglobalacl.Response;
                if (objgetgetsuperglobalacl.Response != null) {
                    var Res = [];
                    var Result = [];
                    var MenuList;
                    var Isvalidation = false;
                    for (var i = 0; i < objgetgetsuperglobalacl.Response.length; i++) {
                        if (res != -1) {
                            if (objgetgetsuperglobalacl.Response[i]["AccessPermission"] == true && objgetgetsuperglobalacl.Response[i].GlobalRoleid == res) {
                                var Res = objgetgetsuperglobalacl.Response[i]["Featureid"];
                                if (objgetgetsuperglobalacl.Response[i]["Menuid"] == 1) {
                                    var Result = 1;
                                    $scope.settingsM1 = true;
                                }
                                if (objgetgetsuperglobalacl.Response[i]["Menuid"] == 2) {
                                    var Result = 2;
                                    $scope.settingsM2 = true;
                                }
                                if (objgetgetsuperglobalacl.Response[i]["Menuid"] == 3) {
                                    var Result = 3;
                                    $scope.settingsM3 = true;
                                }
                                if (objgetgetsuperglobalacl.Response[i]["Menuid"] == 4) {
                                    var Result = 4;
                                    $scope.settingsM4 = true;
                                }
                                if (objgetgetsuperglobalacl.Response[i]["Menuid"] == 5) {
                                    var Result = 5;
                                    $scope.settingsM5 = true;
                                }
                                if (objgetgetsuperglobalacl.Response[i]["Menuid"] == 6) {
                                    var Result = 6;
                                    $scope.settingsM6 = true;
                                }
                                if (objgetgetsuperglobalacl.Response[i]["Menuid"] == 7) {
                                    var Result = 7;
                                    $scope.settingsM7 = true;
                                }
                                if (objgetgetsuperglobalacl.Response[i]["Menuid"] == 8) {
                                    var Result = 8;
                                    $scope.settingsM8 = true;
                                }
                                if (objgetgetsuperglobalacl.Response[i]["Menuid"] == 9) {
                                    var Result = 9;
                                    $scope.settingsM9= true;
                                }
                                if (objgetgetsuperglobalacl.Response[i]["Menuid"] == 10) {
                                    var Result = 10;
                                    $scope.settingsM10 = true;
                                }
                                $scope.settings[Res] = true;
                            } else {
                                var Res = objgetgetsuperglobalacl.Response[i]["Featureid"];
                                $scope.settings[Res] = true;
                            }
                        }
                        if (res == -1) {
                            var Res = objgetgetsuperglobalacl.Response[i]["Featureid"];
                            $scope.settings[Res] = true;
                        }
                    }
                    $scope.IsSuperAdminSetMenu = {
                        Id: 0
                    };
                    if (res != -1) {
                        MenuList = ($.grep(objgetgetsuperglobalacl.Response, function (e) {
                            return e.GlobalRoleid == res && e.AccessPermission == true && e.Featureid != 0;
                        }));
                        if (MenuList.length != 0) {
                            $scope.IsSuperAdminSetMenu = {
                                Id: MenuList[0].Featureid
                            };
                            var path = $('#' + MenuList[0].Featureid + ' > a').attr("href");
                            if (path != undefined) {
                                $location.path($('#' + MenuList[0].Featureid + ' > a').attr("href").substring(1));
                                $scope.parentid = $('#' + MenuList[0].Featureid + ' > a').parents("div:first").parent().attr("id")
                                var CollapsingId = $('#' + MenuList[0].Featureid).parents(".accordion-body").attr('id');
                                $("#adminTreeMenu").find('.collapsed').removeClass('collapsed').height(0)
                                $("#" + CollapsingId).removeClass('collapse');
                                $("#" + CollapsingId).addClass('in collapse');
                            }
                        }
                        if (MenuList.length == 0) {
                            $scope.settingsM1 = true;
                            $scope.settingsM2 = true;
                            $scope.settingsM3 = true;
                            $scope.settingsM4 = true;
                            $scope.settingsM5 = true;
                            $scope.settingsM6 = true;
                            $scope.settingsM7 = true;
                            $scope.settingsM8 = true;
                            $scope.settingsM9 = true;
                            $scope.settingsM10 = true;
                        }
                    }
                    if (res == -1) {
                            $scope.settingsM1 = true;
                            $scope.settingsM2 = true;
                            $scope.settingsM3 = true;
                            $scope.settingsM4 = true;
                            $scope.settingsM5 = true;
                            $scope.settingsM6 = true;
                            $scope.settingsM7 = true;
                            $scope.settingsM8 = true;
                            $scope.settingsM9 = true;
                            $scope.settingsM10 = true;
                    }
                }
            });
        });
        $scope.$on('gotoDamTaskopenpopup', function (event) {
            $scope.$broadcast('LoadDamTaskopenpopup');
        });
        $scope.$on('ClearAllTimerAttachment', function (event) {
            $scope.$broadcast('ClearTimerOnClose');
        });
        $scope.$on('mui_LoadTaskList', function (event) {
            $scope.$broadcast('LoadTaskList');
        });
        $scope.$on('CallBackAttachtakdraw', function (event, ID) {
            $scope.$broadcast('mui_CallBackAttachtakdraw', ID);
        });
        $scope.SnippetObj = {
            snippetuiclass: false
        };
        MuiService.GetThemeValues().then(function (GetThemeValuesRes) {
            var Res = [];
            Res = GetThemeValuesRes.Response.m_Item1;
            if (GetThemeValuesRes.Response.m_Item1 != undefined) {
                for (var i = 0; i < Res.length; i++) {
                    $scope.ThemeDataBlcks = GetThemeValuesRes.Response.m_Item1;
                }
                $scope.selected = [];
                $scope.selected = $.grep(GetThemeValuesRes.Response.m_Item1, function (e) {
                    return e.Isactive == 1;
                });
                if ($scope.selected != null && $scope.selected.length != 0) {
                    $scope.ThemeDataBlck = $scope.selected[0].NAME;
                    MuiService.GetThemeData($scope.selected[0].ID).then(function (ThemeResultData) {
                        var rst = [];
                        rst = JSON.parse(ThemeResultData.Response);
                        $scope.Themedata = rst;
                        $scope.Themedata.root.ThemeDataBlck.Theme = $scope.selected[0].ID;
                    });
                }
                var Result = "";
                Result = GetThemeValuesRes.Response.m_Item2;
                $("#theme-style-id").html(Result);
            }
        });
        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        var re = /m/gi;
        var str = $scope.DefaultSettings.DateFormat;
        $scope.format = str.replace(re, "M");
        $scope.nonbusinessdays = {};
        $scope.tempddays = [];
        $scope.tempholidays = [];
        setTimeout(function () {
            $scope.GetHolidays();
            $scope.GetNonBusinessDaysforDatePicker();
        }, 100);
        $scope.GetHolidays = function () {
            MuiService.GetHolidaysDetails().then(function (res) {
                if (res.Response != null) {
                    $scope.listOfHolidays = res.Response;
                    for (var a = 0; a < $scope.listOfHolidays.length; a++) {
                        $scope.tempholidays[a] = dateFormat($scope.listOfHolidays[a].HolidayDate, $scope.DefaultSettings.DateFormat);
                    }
                } else {
                    $scope.tempholidays = [];
                }
            });
        }
        $scope.$on("pingMUITaskEdit", function (event, obj) {
            //$scope.$broadcast("pingTaskEdit", obj);
            $scope.populateTaskEditPopup(obj.TaskId, obj.EntityId, '')
        });
        $scope.populateTaskEditPopup = function (taskID, EntityId, taskStatus) {
            $scope.ReorderOverviewStructure = false;
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/task/TaskEdit.html',
                controller: "mui.task.muitaskFlowEditCtrl",
                resolve: {
                    items: function () {
                        return {
                            TaskId: taskID,
                            EntityId: EntityId,
                            taskStatus: taskStatus
                        };
                    }
                },
                scope: $scope,
                windowClass: 'taskImprovementPopup TaskPopup TaskPopupTabbed popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                //console.log('Modal dismissed at: ' + new Date());
            });
        }
        $scope.$on("muiRefreshTaskByID", function (event, id) {
            $scope.$broadcast("RefreshTaskByID", id);
        });

        $scope.$on("muiRefreshTasklistByID", function (event, obj) {
            $scope.$broadcast("RefreshEntityTaskListCollection", obj);
        });



        $scope.$on('mui_assetcreationPage', function (event, option, obj) {
            $scope.$broadcast('assetcreationPage', option, obj)
        })
        $scope.$on('mui_CallBackThumbnailAssetView', function (event, ID) {
            $scope.$broadcast('CallBackThumbnailAssetView', ID);
        })
        $scope.$on('mui_resetuploaderstatus', function (event) {
            $scope.$broadcast('resetuploaderstatus');
        })
        $scope.HolidayListCalculation = function (mindate, maxdate) {
            if (mindate != null || maxdate != null) {
                var today = new Date.create();
                var mindates_exist = $.grep($scope.tempholidays, function (e) {
                    return mindate <= new Date.create(e) && new Date.create(e) <= today;
                });
                if (mindates_exist != null) {
                    if (mindates_exist.length > 0) {
                        mindate.addDays(-(mindates_exist.length));
                    }
                }
                var maxdates_exist = $.grep($scope.tempholidays, function (e) {
                    return maxdate >= new Date.create(e) && new Date.create(e) >= today;
                });
                if (maxdates_exist != null) {
                    if (maxdates_exist.length > 0) {
                        maxdate.addDays((maxdates_exist.length));
                    }
                }
                var tempdates = [];
                if (mindate > today) { tempdates = getDates(today, mindate); } else { tempdates = getDates(mindate, today); }
                if (tempdates != null) {
                    var mindates_exist_nonbusinessdays = $.grep(tempdates, function (e) {
                        return (($scope.nonbusinessdays.sunday === "true" && e.getDay() === 0) || ($scope.nonbusinessdays.monday === "true" && e.getDay() === 1) || ($scope.nonbusinessdays.tuesday === "true" && e.getDay() === 2) || ($scope.nonbusinessdays.wednesday === "true" && e.getDay() === 3) || ($scope.nonbusinessdays.thursday === "true" && e.getDay() === 4) || ($scope.nonbusinessdays.friday === "true" && e.getDay() === 5) || ($scope.nonbusinessdays.saturday === "true" && e.getDay() === 6));
                    });
                }
                if (mindates_exist_nonbusinessdays != null) {
                    if (mindates_exist_nonbusinessdays.length > 0) {
                        if (mindate > today) { mindate.addDays((mindates_exist_nonbusinessdays.length)); } else { mindate.addDays(-(mindates_exist_nonbusinessdays.length)); }
                    }
                }
                if (maxdate > today) { tempdates = getDates(today, maxdate); } else { tempdates = getDates(maxdate, today); }
                if (tempdates != null) {
                    var maxdates_exist_nonbusinessdays = $.grep(tempdates, function (e) {
                        return (($scope.nonbusinessdays.sunday === "true" && e.getDay() === 0) || ($scope.nonbusinessdays.monday === "true" && e.getDay() === 1) || ($scope.nonbusinessdays.tuesday === "true" && e.getDay() === 2) || ($scope.nonbusinessdays.wednesday === "true" && e.getDay() === 3) || ($scope.nonbusinessdays.thursday === "true" && e.getDay() === 4) || ($scope.nonbusinessdays.friday === "true" && e.getDay() === 5) || ($scope.nonbusinessdays.saturday === "true" && e.getDay() === 6));
                    });
                }
                if (maxdates_exist_nonbusinessdays != null) {
                    if (maxdates_exist_nonbusinessdays.length > 0) {
                        if (maxdate > today) { maxdate.addDays((maxdates_exist_nonbusinessdays.length)); } else { maxdate.addDays(-(maxdates_exist_nonbusinessdays.length)); }
                    }
                }
            }
            return {
                "MinDate": getbussinessday(mindate),
                "MaxDate": getbussinessday(maxdate)
            };
        };

        function getbussinessday(date) {
            if (date != null) {
                if (($scope.nonbusinessdays.sunday === "true" && date.getDay() === 0) || ($scope.nonbusinessdays.monday === "true" && date.getDay() === 1) || ($scope.nonbusinessdays.tuesday === "true" && date.getDay() === 2) || ($scope.nonbusinessdays.wednesday === "true" && date.getDay() === 3) || ($scope.nonbusinessdays.thursday === "true" && date.getDay() === 4) || ($scope.nonbusinessdays.friday === "true" && date.getDay() === 5) || ($scope.nonbusinessdays.saturday === "true" && date.getDay() === 6)) {
                    if (date < new Date.create()) {
                        date.addDays(-1);
                    }
                    else {
                        date.addDays(1);
                    }
                    getbussinessday(date);

                }
                return date;
            }
            return date;
        }

        function getDates(startDate, stopDate) {

            var now = new Date.create(stopDate);
            var daysOfYear = [];
            for (var d = new Date.create(startDate) ; d <= now; d.setDate(d.getDate() + 1)) {
                daysOfYear.push(new Date(d));
            }
            return daysOfYear;
        }
        $scope.GetNonBusinessDaysforDatePicker = function () {
            MuiService.GetNonBusinessDays().then(function (result) {
                if (result.Response != null) {
                    $scope.nonbusinessdays = result.Response[0];
                }
            });
        };
        $scope.disabled = function (date, mode) {
            if ($scope.tempholidays.length > 0) {
                return (mode === 'day' && (($scope.nonbusinessdays.sunday === "true" && date.getDay() === 0) || ($scope.nonbusinessdays.monday === "true" && date.getDay() === 1) || ($scope.nonbusinessdays.tuesday === "true" && date.getDay() === 2) || ($scope.nonbusinessdays.wednesday === "true" && date.getDay() === 3) || ($scope.nonbusinessdays.thursday === "true" && date.getDay() === 4) || ($scope.nonbusinessdays.friday === "true" && date.getDay() === 5) || ($scope.nonbusinessdays.saturday === "true" && date.getDay() === 6) || $scope.tempholidays.indexOf($filter('date')(date, $scope.format)) !== -1));
            } else {
                return (mode === 'day' && (($scope.nonbusinessdays.sunday === "true" && date.getDay() === 0) || ($scope.nonbusinessdays.monday === "true" && date.getDay() === 1) || ($scope.nonbusinessdays.tuesday === "true" && date.getDay() === 2) || ($scope.nonbusinessdays.wednesday === "true" && date.getDay() === 3) || ($scope.nonbusinessdays.thursday === "true" && date.getDay() === 4) || ($scope.nonbusinessdays.friday === "true" && date.getDay() === 5) || ($scope.nonbusinessdays.saturday === "true" && date.getDay() === 6)));
            }
        };

        function refreshApp() { }
    }
    app.controller('muiCtrl', ['$window', '$location', '$localStorage', '$timeout', '$scope', '$cookies', '$rootScope', '$filter', '$compile', 'MuiService', '$translate', '$modal', muiCtrl]);
    app.directive('compile', ['$compile', function ($compile) {
        return function (scope, element, attrs) {
            scope.$watch(function (scope) {
                return scope.$eval(attrs.compile);
            }, function (value) {
                element.html(value);
                $compile(element.contents())(scope);
            });
        };
    }]);
})(angular, app);
///#source 1 1 /app/services/mui-service.js
(function(ng,app){"use strict";function MuiService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({SetIsCurrentWorkingXml:SetIsCurrentWorkingXml,GetDecimalSettingsValue:GetDecimalSettingsValue,GetAdditionalSettings:GetAdditionalSettings,GetNotification:GetNotification,GetTaskLIveUpdateRecords:GetTaskLIveUpdateRecords,updateBroadcastMessagesbyuser:updateBroadcastMessagesbyuser,IsActiveEntity:IsActiveEntity,CheckUserPermissionForEntity:CheckUserPermissionForEntity,UpdatetopIsviewedStatusNotification:UpdatetopIsviewedStatusNotification,UpdateIsviewedStatusNotification:UpdateIsviewedStatusNotification,GetTopNavigation:GetTopNavigation,GetSearchtype:GetSearchtype,GetassignedAcess:GetassignedAcess,GetThemeValues:GetThemeValues,GetThemeData:GetThemeData,GetHolidaysDetails:GetHolidaysDetails,GetNonBusinessDays:GetNonBusinessDays,GetAttachmentEditFeature:GetAttachmentEditFeature,QuickSearch:QuickSearch,DuplicateEntities:DuplicateEntities,GetDAMViewSettings:GetDAMViewSettings,GetGlobalRoleUserByID:GetGlobalRoleUserByID,GetSuperglobalacl:GetSuperglobalacl});function SetIsCurrentWorkingXml(){var request=$http({method:"post",url:"api/Metadata/SetIsCurrentWorkingXml/",params:{action:"add"},});return(request.then(handleSuccess,handleError));}
function GetDecimalSettingsValue(){var request=$http({method:"get",url:"api/common/GetDecimalSettingsValue/",params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function GetAdditionalSettings(){var request=$http({method:"get",url:"api/common/GetAdditionalSettings/",params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function GetNotification(flag){var request=$http({method:"get", ignoreLoadingBar: true,url:"api/common/GetNotification/"+flag,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetTaskLIveUpdateRecords() { var request = $http({ method: "get", ignoreLoadingBar: true, url: "api/common/GetTaskLIveUpdateRecords/", params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
function updateBroadcastMessagesbyuser(){var request=$http({method:"post",url:"api/common/updateBroadcastMessagesbyuser/",params:{action:"add",}});return(request.then(handleSuccess,handleError));}
function IsActiveEntity(EntityID){var request=$http({method:"get",url:"api/common/IsActiveEntity/"+EntityID,params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function CheckUserPermissionForEntity(EntitiyID){var request=$http({method:"get",url:"api/common/CheckUserPermissionForEntity/"+EntitiyID,params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function UpdatetopIsviewedStatusNotification(jsonparameter) { var request = $http({ method: "post", ignoreLoadingBar: true, url: "api/common/UpdatetopIsviewedStatusNotification", params: { action: "add" }, data: jsonparameter }); return (request.then(handleSuccess, handleError)); }
function UpdateIsviewedStatusNotification(updatestae){var request=$http({method:"post",url:"api/common/UpdateIsviewedStatusNotification/",params:{action:"add"},data:{UserID:updatestae.UserID,FirstFiveNotifications:updatestae.FirstFiveNotifications,Flag:updatestae.Flag}});return(request.then(handleSuccess,handleError));}
function GetTopNavigation(){var request=$http({method:"get",url:"api/common/GetTopNavigation/",params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function GetSearchtype(userid){var request=$http({method:"get",url:"api/common/GetSearchtype/"+userid,params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function GetassignedAcess(userid){var request=$http({method:"get",url:"api/common/GetassignedAcess/"+userid,params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function GetThemeValues(){var request=$http({method:"get",url:"api/common/GetThemeValues/",params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function GetThemeData(Theme){var request=$http({method:"get",url:"api/common/GetThemeData/"+Theme,params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function GetHolidaysDetails(){var request=$http({method:"get",url:"api/Common/GetHolidaysDetails/",params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function GetNonBusinessDays(){var request=$http({method:"get",url:"api/Common/GetNonBusinessDays/",params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function GetAttachmentEditFeature(){var request=$http({method:"get",url:"api/Planning/GetAttachmentEditFeature/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function QuickSearch(formobj){var request=$http({method:"post",url:"api/Planning/QuickSearch/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function QuickSearch1(formobj){var request=$http({method:"post",url:"api/Planning/QuickSearch1/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function DuplicateEntities(formobj){var request=$http({method:"post",url:"api/Planning/DuplicateEntities/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetDAMViewSettings(){var request=$http({method:"get",url:"api/dam/GetDAMViewSettings/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetGlobalRoleUserByID(ID){var request=$http({method:"get",url:"api/access/GetGlobalRoleUserByID/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetSuperglobalacl(){var request=$http({method:"get",url:"api/access/GetSuperglobalacl/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){response.data.StatusCode=response.status;return(response.data);}}
app.service("MuiService",['$http','$q',MuiService]);})(angular,app);
