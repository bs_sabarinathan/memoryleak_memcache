using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
    /// LockedEntities object for table 'PM_LockedEntities'.
	/// </summary>
	public partial class LockedEntitiesDao : BaseDao, ICloneable 
	{
		#region Member Variables
        protected int _id;
        protected int _entityid;
        protected int _lockedby;
        protected string _lockedon;
		#endregion
		
		#region Constructors
		public LockedEntitiesDao() {}
			
		public LockedEntitiesDao(int pID,int pEntityID, int pLockedBy, string pLockedOn)
		{
			this._id=pID;
            this._entityid = pEntityID;
            this._lockedby = pLockedBy;
            this._lockedon = pLockedOn;
		}

        	
		#endregion
		
		#region Public Properties
        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int EntityID
        {
            get { return _entityid; }
            set { _entityid = value; }

        }
        public virtual int LockedBy
        {
            get { return _lockedby; }
            set { _lockedby = value; }

        }

        public virtual string LockedOn
        {
            get { return _lockedon; }
            set
            {
                _lockedon = value;
            }

        }
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string LockedEntitiesDao = "LockedEntitiesDao";
            public const string ID = "ID";
            public const string EntityID = "EntityID";
            public const string LockedBy = "LockedBy";
            public const string LockedOn = "LockedOn";		
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string LockedEntitiesDao = "LockedEntitiesDao";
            public const string ID = "ID";
            public const string EntityID = "EntityID";
            public const string LockedBy = "LockedBy";
            public const string LockedOn = "LockedOn";		
		}

		#endregion
		
		
	}
	
}
