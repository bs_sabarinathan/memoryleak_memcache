using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IFeedTemplate interface for table 'CM_Feed_Template'.
    /// </summary>
    public interface IFeedTemplate
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Moduleid
        {
            get;
            set;

        }

        int Featureid
        {
            get;
            set;

        }

        string Template
        {
            get;
            set;

        }
        string Action
        {
            get;
            set;

        }
        
        

        #endregion
    }
}
