﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
   internal class ObjectiveEntitycondition : IObjectiveEntitycondition
    {
      public int ObjectiveId { get; set; }
      public int ConditionType { get; set; }
      public int EntityTypeId { get; set; }
      public int AttributeId { get; set; }
      public string AttributeOptions { get; set; }
    }
}
