﻿(function (ng, app) {
    app.directive('dirownernameautopopulate', ['$resource', 'PlanningToolService', function ($resource, PlanningToolService) {
        return function (scope, element, attrs) {
            // scope.format = scope.DefaultSettings.DateFormat.replace(/m/gi, "M");
            element.autocomplete({
                source: function (request, response) {
                    PlanningToolService.AutoCompleteMemberList(request.term, 6).then(function (optionlist) {
                        if (optionlist.Response.length == 0) {
                            bootbox.alert('Invalid Owner');
                            element.val(scope.OwnerList[0].UserName);
                        } else response(JSON.parse(optionlist.Response));
                    });
                },
                minLength: 3,
                focus: function (event, ui) {
                    element.val(ui.item.FirstName + ' ' + ui.item.LastName);
                    return false;
                },
                select: function (event, ui) {
                    scope.OwnerList = [];
                    scope.OwnerList.push({
                        "Roleid": 1,
                        "RoleName": "Owner",
                        "Userid": parseInt(ui.item.Id, 10),
                        "UserName": ui.item.FirstName + ' ' + ui.item.LastName,
                        "UserEmail": ui.item.Email,
                        "IsInherited": '0',
                        "InheritedFromEntityid": '0'
                    });
                    scope.$apply;
                    return false;
                },
                open: function (event, ui) {
                    if ($('.ui-autocomplete').find('li').length > 3) {
                        $('.ui-autocomplete').css('padding-right', '20px').css('width', '340px').css('max-height', '225px').css('overflow-y', 'auto').addClass('dropdown-menu');
                    } else {
                        $('.ui-autocomplete').css('padding-right', '2px').css('width', '340px').css('max-height', '225px').css('overflow-y', 'auto').addClass('dropdown-menu');
                    }
                    $(".ui-helper-hidden-accessible").hide();
                    $(this).autocomplete('widget').css('z-index', 10000);
                    return false;
                },
                change: function (event, ui) {
                    if (element.val().length < 3) {
                        bootbox.alert('Invalid Owner');
                        element.val(scope.OwnerList[0].UserName);
                    }
                    if (ui.item === null) {
                        scope.myModelId = null;
                    }
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                var html = '';
                html = html + '<a class="membersClues" href="javascript:;" tabindex="-1">';
                html = html + '<div class="chooseMember">';
                html = html + '<div class="row-fluid">';
                html = html + '<div class="span2 memberImage">';
                html = html + '<img src="Handlers/UserImage.ashx?id=' + item.Id + '">';
                html = html + '</div>';
                html = html + '<div class="span10">';
                html = html + '<div class="row-fluid memberDetail">';
                html = html + '<div class="span2">';
                html = html + '<span>Name</span><br>';
                html = html + '<span>Email</span>';
                html = html + '</div>';
                html = html + '<div class="span10 memberValues">';
                html = html + '<span>' + item.FirstName + ' ' + item.LastName + '</span><br>';
                html = html + '<span>' + item.Email + '</span>';
                html = html + '</div>';
                html = html + '</div>';
                html = html + '</div>';
                html = html + '</div>';
                html = html + '</div>';
                html = html + '</a>';
                return $("<li></li>").data("item.autocomplete", item).append(html).appendTo(ul);
            };
        }
    }]);
    app.directive('autocomp', ['$resource', '$translate', 'PlanningToolService', function ($resource, $translate, PlanningToolService) {
        return function (scope, element, attrs) {
            element.autocomplete({
                source: function (request, response) {
                    PlanningToolService.AutoCompleteMemberList(request.term, 6).then(function (optionlist) {
                        response(JSON.parse(optionlist.Response));
                    });
                },
                minLength: 3,
                focus: function (event, ui) {
                    element.val(ui.item.FirstName + ' ' + ui.item.LastName);
                    return false;
                },
                select: function (event, ui) {
                    scope.AutoCompleteSelectedObj = [];
                    scope.AutoCompleteSelectedObj.push(ui.item);
                    scope.AutoCompleteSelectedUserObj.UserSelection = ui.item;
                    return false;
                },
                open: function (event, ui) {
                    if ($('.ui-autocomplete').find('li').length > 3) {
                        $('.ui-autocomplete').css('padding-right', '20px').css('width', '340px').css('max-height', '225px').css('overflow-y', 'auto').addClass('dropdown-menu');
                    } else {
                        $('.ui-autocomplete').css('padding-right', '2px').css('width', '340px').css('max-height', '225px').css('overflow-y', 'auto').addClass('dropdown-menu');
                    }
                    $(".ui-helper-hidden-accessible").hide();
                    $(this).autocomplete('widget').css('z-index', 100000);
                    return false;
                },
                change: function (event, ui) {
                    if (ui.item === null) {
                        scope.myModelId = null;
                    }
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                var html = '';
                html = html + '<a class="membersClues" href="javascript:;" tabindex="-1">';
                html = html + '<div class="chooseMember">';
                html = html + '<div class="row-fluid">';
                html = html + '<div class="span2 memberImage">';
                html = html + '<img src="Handlers/UserImage.ashx?id=' + item.Id + '">';
                html = html + '</div>';
                html = html + '<div class="span10">';
                html = html + '<div class="row-fluid memberDetail">';
                html = html + '<div class="span2">';
                html = html + '<span>Name</span><br>';
                html = html + '<span>Email</span>';
                html = html + '</div>';
                html = html + '<div class="span10 memberValues">';
                html = html + '<span>' + item.FirstName + ' ' + item.LastName + '</span><br>';
                html = html + '<span>' + item.Email + '</span>';
                html = html + '</div>';
                html = html + '</div>';
                html = html + '</div>';
                html = html + '</div>';
                html = html + '</div>';
                html = html + '</a>';
                return $("<li></li>").data("item.autocomplete", item).append(html).appendTo(ul);
            };
        }
    }]);
    app.directive('userautocomplete', ['$resource', '$translate', 'PlanningToolService', function ($resource, $translate, PlanningToolService) {
        return function (scope, element, attrs) {
            element.autocomplete({
                source: function (request, response) {
                    PlanningToolService.AutoCompleteMemberList(request.term, 6).then(function (optionlist) {
                        response(JSON.parse(optionlist.Response));
                    });
                },
                minLength: 3,
                focus: function (event, ui) {
                    element.val(ui.item.FirstName + ' ' + ui.item.LastName);
                    scope.AutoCompleteSelectedObj = [];
                    scope.AutoCompleteSelectedObj.push(ui.item);
                    scope.AutoCompleteSelectedUserObj.UserSelection = ui.item;
                    return false;
                },
                select: function (event, ui) {
                    scope.AutoCompleteSelectedObj = [];
                    scope.AutoCompleteSelectedObj.push(ui.item);
                    scope.AutoCompleteSelectedUserObj.UserSelection = ui.item;
                    return false;
                },
                open: function (event, ui) {
                    if ($('.ui-autocomplete').find('li').length > 3) {
                        $('.ui-autocomplete').css('padding-right', '20px').css('width', '340px').css('max-height', '225px').css('overflow-y', 'auto').addClass('dropdown-menu');
                    } else {
                        $('.ui-autocomplete').css('padding-right', '2px').css('width', '340px').css('max-height', '225px').css('overflow-y', 'auto').addClass('dropdown-menu');
                    }
                    $(".ui-helper-hidden-accessible").hide();
                    $(this).autocomplete('widget').css('z-index', 100000);
                    return false;
                },
                change: function (event, ui) {
                    if (ui.item === null) {
                        scope.myModelId = null;
                    }
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                var html = '';
                html = html + '<a class="membersClues" href="javascript:;" tabindex="-1">';
                html = html + '<div class="chooseMember">';
                html = html + '<div class="row-fluid">';
                html = html + '<div class="span2 memberImage">';
                html = html + '<img src="Handlers/UserImage.ashx?id=' + item.Id + '">';
                html = html + '</div>';
                html = html + '<div class="span10">';
                html = html + '<div class="row-fluid memberDetail">';
                html = html + '<div class="span2">';
                html = html + '<span>Name</span><br>';
                html = html + '<span>Email</span>';
                html = html + '</div>';
                html = html + '<div class="span10 memberValues">';
                html = html + '<span>' + item.FirstName + ' ' + item.LastName + '</span><br>';
                html = html + '<span>' + item.Email + '</span>';
                html = html + '</div>';
                html = html + '</div>';
                html = html + '</div>';
                html = html + '</div>';
                html = html + '</div>';
                html = html + '</a>';
                return $("<li></li>").data("item.autocomplete", item).append(html).appendTo(ul);
            };
        }
    }]);
    app.directive('xeditablefinancialamount', ['$timeout', '$compile', '$resource', '$translate', '$window', function ($timeout, $compile, $resource, $translate, $window) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var attributeID = attrs.attributeid;
                var Address = function (options) {
                    this.init(attributeID, options, Address.defaults);
                };
                $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                var inlinePopup = '';
                inlinePopup += '<div><label><input class="ngcompile" type="text" id="SingleTextXeditalble_' + attrs.attributeid + '" data-ng-model="SingleTextValue_' + attrs.attributeid + '"></label></div>';
                Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                    tpl: inlinePopup,
                    inputclass: ''
                });
                $.fn.editabletypes[attributeID] = Address;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.originalTitle,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $('[id=SingleTextXeditalble_' + attrs.attributeid + ']').autoNumeric('init', scope.DecimalSettings['FinancialAutoNumeric']);
                            $('[id=SingleTextXeditalble_' + attrs.attributeid + ']').css('text-align', 'right');
                            $timeout(function () {
                                $('[id=SingleTextXeditalble_' + attrs.attributeid + ']:enabled:visible:first').focus().select()
                            }, 100);
                            $compile($('.ngcompile'))(scope);
                            scope['SingleTextValue_' + attrs.attributeid] = scope.fields['TotalAssignedAmount'];
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    scope.fields['TotalAssignedAmount'] = scope['SingleTextValue_' + attrs.attributeid].toString().trim() == "" ? "0" : scope['SingleTextValue_' + attrs.attributeid];
                    scope.savefinanicalamount(scope.fields['TotalAssignedAmount']);
                });
            }
        };
    }]);
    app.service('sharedProperties', [function () {
        var stringValue = 'test string value';
        var objectValue = {
            data: 'test object value'
        };
        var itemID = 0;
        return {
            getString: function () {
                return stringValue;
            },
            getID: function () {
                return itemID;
            },
            setString: function (value, id) {
                stringValue = value;
                itemID = id;
            },
            getObject: function () {
                return objectValue;
            }
        }
    }]);
    app.directive('xeditablefinancialtext', ['$timeout', '$compile', '$resource', '$translate', '$window', function ($timeout, $compile, $translate, $resource, $window) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var CurrentEntityID = parseInt(attrs.finid, 10);
                var attributeID = attrs.attributeid;
                var finid = attrs.finid;
                var editablefinid = attrs.editablefinid;
                var Address = function (options) {
                    this.init(editablefinid, options, Address.defaults);
                };
                $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                var inlinePopup = '';
                if (parseInt(attrs.attributetypeid) == 1) inlinePopup += '<div><label><input class="ngcompile" type="text" id="SingleTextXeditalble' + attrs.finid + '_' + attributeID + '" data-ng-model="SingleTextValue_' + attrs.finid + '_' + attributeID + '"></label></div>';
                else if (parseInt(attrs.attributetypeid) == 2) inlinePopup += '<div><label><textarea class="ngcompile" id="SingleTextXeditalble' + attrs.finid + '_' + attributeID + '" data-ng-model="SingleTextValue_' + attrs.finid + '_' + attributeID + '" ></textarea></label></div>';
                Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                    tpl: inlinePopup,
                    inputclass: ''
                });
                $.fn.editabletypes[editablefinid] = Address;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.originalTitle,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompile'))(scope);
                            scope['SingleTextValue_' + attrs.finid + '_' + attributeID] = scope.fields['SingleLineTextValue_' + attrs.finid + '_' + attributeID] == "-" ? "" : scope.fields['SingleLineTextValue_' + attrs.finid + '_' + attributeID];
                            $timeout(function () {
                                $('#SingleTextXeditalble' + attrs.finid + '_' + attributeID).focus().select()
                            }, 10);
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    scope.saveFinancialMetadataValues(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope['SingleTextValue_' + attrs.finid + '_' + attributeID]);
                });
            }
        };
    }]);
    app.directive('xeditablefinancialdropdown', ['$timeout', '$compile', '$resource', '$translate', '$window', function ($timeout, $compile, $translate, $resource, $window) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var Version = 1;
                var attributeName = attrs.attributename;
                var attributeID = attrs.attributeid;
                var finid = attrs.finid;
                var dttype = attrs.editablefinid;
                var Currentfinid = parseInt(attrs.finid, 10)
                var isDataTypeExists = false;
                for (var variable in $.fn.editabletypes) {
                    if (dttype == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(dttype, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopup = '';
                    inlinePopup += '<div><label><select ui-select2 data-ng-model="NormalDropDown_' + attrs.finid + '_' + attributeID + '" name="city" class="ngcompileDropdown"><option ng-repeat=\"ndata in normaltreeSources.NormalDropDown_' + attrs.finid + '_' + attributeID + '\" value=\"{{ndata.ID}}\">{{ndata.Caption}}</option></select></label></div>';
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopup,
                        inputclass: ''
                    });
                    $.fn.editabletypes[dttype] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompileDropdown'))(scope);
                            var tempvar = ($.grep(scope.normaltreeSources["NormalDropDown_" + attrs.finid + '_' + attributeID], function (e) {
                                return e.Caption == scope.fields['NormalDropDown_' + attrs.finid + '_' + attributeID]
                            }))[0];
                            if (tempvar != undefined) scope['NormalDropDown_' + attrs.finid + '_' + attributeID] = tempvar.ID;
                            else scope['NormalDropDown_' + attrs.finid + '_' + attributeID] = '';
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    scope.treeSelection = [];
                    var dropdowntext = 'NormalDropDown_' + attrs.finid + '_' + attributeID;
                    if (scope['NormalDropDown_' + attrs.finid + '_' + attributeID] != undefined) {
                        var tempvar = ($.grep(scope.normaltreeSources["NormalDropDown_" + attrs.finid + '_' + attributeID], function (e) {
                            return parseInt(e.ID) == parseInt(scope['NormalDropDown_' + attrs.finid + '_' + attributeID])
                        }))[0];
                        scope.fields[dropdowntext] = tempvar.Caption;
                        scope.treeSelection.push(tempvar.ID);
                        scope.$apply();
                    } else {
                        scope.fields[dropdowntext] = '-';
                        scope.treeSelection.push(0);
                        scope.$apply();
                    }
                    scope.saveFinancialMetadataValues(attrs.attributeid, attrs.attributetypeid, Currentfinid, scope.treeSelection);
                });
            }
        };
    }]);
    app.directive('xeditablefinancialmultiselectdropdown', ['$timeout', '$compile', '$resource', '$translate', '$window', function ($timeout, $compile, $resource, $translate, $window) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var Version = 1;
                var attributeName = attrs.attributename;
                var finid = attrs.finid;
                var attributeID = attrs.attributeid;
                var dttype = attrs.editablefinid;
                var Currentfinid = parseInt(attrs.finid, 10)
                var isDataTypeExists = false;
                for (var variable in $.fn.editabletypes) {
                    if (dttype == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(dttype, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopup = '';
                    inlinePopup += '<div><label> \
                                                <select multiselect-dropdown ng-options="ndata.ID as ndata.Caption for ndata in normaltreeSources.NormalMultiDropDown_' + attrs.finid + '_' + attributeID + '\" id="NormalMultiDropDown1_' + attrs.finid + '_' + attributeID + '" multiple=\"multiple\" data-ng-model="NormalMultiDropDown_' + attrs.finid + '_' + attributeID + '" name="city" class="ngcompileDropdown multiselect"> \
                                                </select> \
                                            </label></div>';
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopup,
                        inputclass: ''
                    });
                    $.fn.editabletypes[dttype] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompileDropdown'))(scope);
                            var captions = scope.fields['NormalMultiDropDown_' + attrs.finid + '_' + attributeID].split(',')
                            scope['NormalMultiDropDown_' + attrs.finid + '_' + attributeID] = [];
                            var tempvar = [];
                            for (var x = 0; x <= captions.length - 1; x++) {
                                if (captions[x] != undefined && captions[x] != '-') {
                                    tempvar.push(($.grep(scope.normaltreeSources["NormalMultiDropDown_" + attrs.finid + '_' + attributeID], function (e) {
                                        return e.Caption.trim() == captions[x].trim()
                                    }))[0]);
                                    scope['NormalMultiDropDown_' + attrs.finid + '_' + attributeID].push(tempvar[x].ID);
                                } else {
                                    scope['NormalMultiDropDown_' + attrs.finid + '_' + attributeID] = [];
                                }
                            }
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    scope.treeSelection = [];
                    var dropdowntext = 'NormalMultiDropDown_' + attrs.finid + '_' + attributeID;
                    var tempcap = [];
                    if (scope['NormalMultiDropDown_' + attrs.finid + '_' + attributeID] != undefined && scope['NormalMultiDropDown_' + attrs.finid + '_' + attributeID].length > 0) {
                        var tempvar = [];
                        var tempval = scope['NormalMultiDropDown_' + attrs.finid + '_' + attributeID];
                        for (var x = 0; x < tempval.length; x++) {
                            tempvar.push(($.grep(scope.normaltreeSources["NormalMultiDropDown_" + attrs.finid + '_' + attributeID], function (e) {
                                return parseInt(e.ID) == parseInt(tempval[x]);
                            }))[0]);
                        }
                        for (var j = 0; j < tempvar.length; j++) {
                            tempcap.push(tempvar[j].Caption);
                            scope.treeSelection.push(tempvar[j].ID);
                            scope.$apply();
                        }
                        scope.fields['NormalMultiDropDown_' + attrs.finid + '_' + attributeID] = tempcap.join(',');
                    } else {
                        scope.fields[dropdowntext] = '-';
                        scope.treeSelection.push(0);
                        scope.$apply();
                    }
                    scope.saveFinancialMetadataValues(attrs.attributeid, attrs.attributetypeid, Currentfinid, scope.treeSelection);
                });
            }
        };
    }]);
    app.directive('xeditablepredefineobjecitve', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var loadXeditable = function () {
                    angular.element(element).editable({
                        display: function (value, srcData) {
                            ngModel.$setViewValue(value);
                            element.html(value);
                            scope.$apply();
                        }
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('save', function (e, params) {
                    scope.savepredefineobjectiveData(attrs.plannedtarget, attrs.targetoutcome, attrs.objectiveid, attrs.targettype, params.newValue);
                });
            }
        };
    }]);
    app.directive('xeditableadditionalobjecitve', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var objid = attrs.objid;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        display: function (value, srcData) {
                            ngModel.$setViewValue(value);
                            element.html(value);
                            scope.$apply();
                        }
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('save', function (e, params) {
                    scope.saveadditionalobjecitveData(attrs.plannedtarget, attrs.targetoutcome, attrs.objid, attrs.targettype, params.newValue);
                    $('#AddtionalObjectivename').val = '';
                });
            }
        };
    }]);
    app.directive('xeditabletext', ['$timeout', '$compile', '$resource', '$translate', '$window', function ($timeout, $compile, $resource, $translate, $window) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var CurrentEntityID = parseInt(attrs.entityid, 10);
                var attributeID = attrs.attributeid;
                var Address = function (options) {
                    this.init(attributeID, options, Address.defaults);
                };
                $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                var inlinePopup = '';
                if (parseInt(attrs.attributetypeid) == 1 || parseInt(attrs.attributetypeid) == 8) inlinePopup += '<div><label><input class="ngcompile" type="text" id="SingleTextXeditalble_' + attrs.attributeid + '" data-ng-model="SingleTextValue_' + attrs.attributeid + '"></label></div>';
                else if (parseInt(attrs.attributetypeid) == 2) inlinePopup += '<div><label><textarea class="small-textarea ngcompile" id="SingleTextXeditalble_' + attrs.attributeid + '" data-ng-model="SingleTextValue_' + attrs.attributeid + '" ></textarea></label></div>';
                Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                    tpl: inlinePopup,
                    inputclass: ''
                });
                $.fn.editabletypes[attributeID] = Address;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.originalTitle,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompile'))(scope);
                            if (parseInt(attrs.attributetypeid) == 8) {
                                $('#SingleTextXeditalble_' + attrs.attributeid).autoNumeric('init', {
                                    aSep: ' ',
                                    vMin: "0",
                                    mDec: "0.0"
                                });
                            }
                            scope['SingleTextValue_' + attrs.attributeid] = scope.fields['SingleLineTextValue_' + attrs.attributeid] == "-" ? "" : scope.fields['SingleLineTextValue_' + attrs.attributeid];
                            $timeout(function () {
                                $('#SingleTextXeditalble_' + attrs.attributeid).focus().select()
                            }, 10);
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    if (scope['SingleTextValue_' + attrs.attributeid] == "") scope['SingleTextValue_' + attrs.attributeid] = '-'
                    if (scope['SingleTextValue_' + attrs.attributeid] != undefined && scope['SingleTextValue_' + attrs.attributeid].length <= 0) {
                        var isValidationReq = $.grep(scope.listAttributeValidationResult, function (e) {
                            return (e.AttributeID == parseInt(attrs.attributeid));
                        });
                        $("#overviewDetialblock").removeClass('warning');
                        if (isValidationReq.length > 0 && isValidationReq[0].RelationShipID != 0) {
                            var metrics = [
								['#SingleTextXeditalble_' + attrs.attributeid + '', isValidationReq[0].ValueType, isValidationReq[0].ErrorMessage]
                            ];
                            var options = {
                                'groupClass': 'warning',
                            };
                            $("#overviewDetialblock").nod(metrics, options);
                            bootbox.alert(isValidationReq[0].ErrorMessage);
                        }
                    } else {
                        if (parseInt(attrs.attributetypeid) == 8) {
                            scope['SingleTextValue_' + attrs.attributeid] = scope['SingleTextValue_' + attrs.attributeid].replace(/ /g, '');
                        }
                        scope.saveDropdownTree(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope['SingleTextValue_' + attrs.attributeid]);
                    }
                });
            }
        };
    }]);
    app.directive('xeditabletextforcurrencyamount', ['$timeout', '$compile', '$resource', '$translate', '$window', 'PlanningToolService', function ($timeout, $compile, $resource, $translate, $window, PlanningToolService) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var CurrentEntityID = parseInt(attrs.entityid, 10);
                var attributeID = attrs.attributeid;
                var Address = function (options) {
                    this.init(attributeID, options, Address.defaults);
                };
                $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                PlanningToolService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
                    if (CurrencyListResult.Response != null) {
                        $window.XeditableDirectivesSource["NormalDropDown_" + attrs.attributeid] = CurrencyListResult.Response;
                        scope.normaltreeSources["NormalDropDown_" + attrs.attributeid] = CurrencyListResult.Response;
                    }
                });
                var inlinePopup = '';
                if (scope.isfromtask != 1) inlinePopup += '<div><input class="margin-right5x ngcompile" type="text" id="SingleTextXeditalble_' + attrs.attributeid + '" data-ng-model="SingleTextValue_' + attrs.attributeid + '" ng-change= Getamountentered(' + attrs.attributeid + ')><select ui-select2 ng-model="NormalDropDown_' + attrs.attributeid + '" class="currencySelector ngcompileDropdown"  id="NormalDropDown_' + attrs.attributeid + '" ng-change= GetCostCentreCurrencyRateById(' + attrs.attributeid + ')><option ng-repeat="ndata in normaltreeSources.NormalDropDown_' + attrs.attributeid + '" value="{{ndata.Id}}">{{ndata.ShortName}}</option></select></div>';
                else if (scope.isfromtask == 1) inlinePopup += '<div><input class="margin-right5x ngcompile" type="text" id="SingleTextXeditalble_' + attrs.attributeid + '" data-ng-model="SingleTextValue_' + attrs.attributeid + '" ng-change= Getamountenteredforedit(' + attrs.attributeid + ')><select ui-select2 ng-model="NormalDropDown_' + attrs.attributeid + '" class="currencySelector ngcompileDropdown"  id="NormalDropDown_' + attrs.attributeid + '" ng-change= GetCostCentreCurrencyRateByIdforedit(' + attrs.attributeid + ')><option ng-repeat="ndata in normaltreeSources.NormalDropDown_' + attrs.attributeid + '" value="{{ndata.Id}}">{{ndata.ShortName}}</option></select></div>';
                Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                    tpl: inlinePopup,
                    inputclass: ''
                });
                $.fn.editabletypes[attributeID] = Address;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.originalTitle,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            mar = (scope.DecimalSettings.FinancialAutoNumeric.vMax).substr((scope.DecimalSettings.FinancialAutoNumeric.vMax).indexOf(".") + 1);
                            scope.mindec = "";
                            if (mar.length == 1) {
                                $('#SingleTextXeditalble_' + attrs.attributeid).autoNumeric('init', {
                                    aSep: ' ',
                                    vMin: "0",
                                    mDec: "1"
                                });
                            }
                            if (mar.length != 1) {
                                if (mar.length < 5) {
                                    scope.mindec = "0.";
                                    for (i = 0; i < mar.length; i++) {
                                        scope.mindec = scope.mindec + "0";
                                    }
                                    $('#SingleTextXeditalble_' + attrs.attributeid).autoNumeric('init', {
                                        aSep: ' ',
                                        vMin: scope.mindec
                                    });
                                } else {
                                    $('#SingleTextXeditalble_' + attrs.attributeid).autoNumeric('init', {
                                        aSep: ' ',
                                        vMin: "0",
                                        mDec: "0"
                                    });
                                }
                            }
                            $compile($('.ngcompile'))(scope);
                            $compile($('.ngcompileDropdown'))(scope);
                            scope['SingleTextValue_' + attrs.attributeid] = scope.fields['SingleLineTextValue_' + attrs.attributeid] == "-" ? "" : scope.fields['SingleLineTextValue_' + attrs.attributeid];
                            $timeout(function () {
                                $('#SingleTextXeditalble_' + attrs.attributeid).focus().select()
                            }, 10);
                            scope['ischanged_' + attrs.attributeid] = false;
                            var tempvar = ($.grep(scope.normaltreeSources["NormalDropDown_" + attrs.attributeid], function (e) {
                                return e.Id == scope.fields['NormalDropDown_' + attrs.attributeid]
                            }))[0];
                            scope['NormalDropDown_' + attrs.attributeid] = tempvar.Id;
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    var curr = ($.grep(scope.normaltreeSources["NormalDropDown_" + attrs.attributeid], function (e) {
                        return e.Id == scope['NormalDropDown_' + attrs.attributeid];
                    }));
                    scope.amountcurrencytypear = [];
                    if (scope['SingleTextValue_' + attrs.attributeid] == "") {
                        scope['SingleTextValue_' + attrs.attributeid] = 0;
                    }
                    scope.amountcurrencytypear.push((scope['SingleTextValue_' + attrs.attributeid].toString()).replace(/ /g, ''));
                    scope.amountcurrencytypear.push(scope['NormalDropDown_' + attrs.attributeid]);
                    scope.amountcurrencytypear.push(curr[0]["ShortName"]);
                    scope.amountcurrencytypear.push(attrs.attributeid);
                    if (scope['SingleTextValue_' + attrs.attributeid] != undefined && scope['SingleTextValue_' + attrs.attributeid].length <= 0) {
                        var isValidationReq = $.grep(scope.listAttributeValidationResult, function (e) {
                            return (e.AttributeID == parseInt(attrs.attributeid));
                        });
                        $("#overviewDetialblock").removeClass('warning');
                        if (isValidationReq.length > 0 && isValidationReq[0].RelationShipID != 0) {
                            var metrics = [
								['#SingleTextXeditalble_' + attrs.attributeid + '', isValidationReq[0].ValueType, isValidationReq[0].ErrorMessage]
                            ];
                            var options = {
                                'groupClass': 'warning',
                            };
                            $("#overviewDetialblock").nod(metrics, options);
                            bootbox.alert(isValidationReq[0].ErrorMessage);
                        }
                    } else {
                        if (scope.isfrommypage != 1) {
                            scope.saveDropdownTree(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope.amountcurrencytypear);
                        } else scope.saveDropdownTreeFromMyPage(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope.amountcurrencytypear);
                    }
                });
            }
        };
    }]);
    app.directive('xeditabletextforname', ['$timeout', '$compile', '$resource', '$translate', '$window', function ($timeout, $compile, $resource, $translate, $window) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var CurrentEntityID = parseInt(attrs.entityid, 10);
                var attributeID = attrs.attributeid;
                var Address = function (options) {
                    this.init(attributeID, options, Address.defaults);
                };
                $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                var inlinePopup = '';
                if (parseInt(attrs.attributetypeid) == 1) inlinePopup += '<div><label><input class="ngcompile" type="text" id="SingleTextXeditalble' + attrs.attributeid + '" data-ng-model="SingleTextValue_' + attrs.attributeid + '" ></label></div>';
                else if (parseInt(attrs.attributetypeid) == 2) inlinePopup += '<div><label><textarea class="ngcompile" id="SingleTextXeditalble' + attrs.attributeid + '" data-ng-model="SingleTextValue_' + attrs.attributeid + '" ></textarea></label></div>';
                Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                    tpl: inlinePopup,
                    inputclass: ''
                });
                $.fn.editabletypes[attributeID] = Address;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.originalTitle,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompile'))(scope);
                            scope['SingleTextValue_' + attrs.attributeid] = scope.RootLevelEntityNameTemp;
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    scope.ActivityName = params.newValue.toString().trim() == "" ? "-" : params.newValue.text();
                    scope.saveDropdownTree(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, params.newValue.toString());
                });
            }
        };
    }]);
    app.directive('xeditable', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var CurrentEntityID = parseInt(attrs.entityid, 10);
                var attributeName = attrs.attributename;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        display: function (value, srcData) {
                            ngModel.$setViewValue(value);
                            element.text(value);
                            scope.$apply();
                        }
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('save', function (e, params) {
                    scope.saveDropdownTree(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, params.newValue.toString());
                });
            }
        };
    }]);
    app.directive('xeditabletaskcomment', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var CurrentEntityID = parseInt(attrs.entityid, 10);
                var attributeName = attrs.attributename;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        display: function (value, srcData) {
                            ngModel.$setViewValue(value);
                            element.html(value);
                            scope.DynamicTaskListDescriptionObj = value;
                            scope.$apply();
                        }
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('save', function (e, params) {
                    scope.DynamicTaskListDescriptionObj = params.newValue;
                    scope.UpdateEntityOverAllStatus();
                });
            }
        };
    }]);
    app.directive('xeditablemilestone', ['$timeout', '$compile', '$resource', '$translate', function ($timeout, $compile, $resource, $translate) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                if (scope.IsLock == true) {
                    return false;
                }
                var Version = 1;
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var isDataTypeExists = false;
                for (var variable in $.fn.editabletypes) {
                    if (attributeName == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(attributeName, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopup = '';
                    inlinePopup += '<div><label><select ui-select2 data-ng-model="xeditmilestonestatus" name="city" class="ngcompileDropdown">';
                    inlinePopup += '<option ng-repeat="opt in MilstoneStatus" value="{{opt.StatusDescription}}">{{opt.StatusDescription}}</option>';
                    inlinePopup += '</label></div>';
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopup,
                        inputclass: ''
                    });
                    $.fn.editabletypes[attributeName] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompileDropdown'))(scope);
                            scope['xeditmilestonestatus'] = scope.milestone.Status;
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    var milestoneSatusId = 0;
                    if (scope['xeditmilestonestatus'] != undefined) {
                        scope.milestone.Status = scope['xeditmilestonestatus'];
                        if (scope.milestone.Status == "Not Reached") milestoneSatusId = 0;
                        else milestoneSatusId = 1;
                    }
                    scope.saveMilestStatus(attrs.milestoneid, attrs.milestonename, attrs.milestoneduedate, attrs.milestoneshortdescription, milestoneSatusId);
                });
            }
        };
    }]);
    app.directive('xeditabletreedropdown', ['$timeout', '$compile', '$resource', '$translate', '$window', 'PlanningToolService', function ($timeout, $compile, $resource, $translate, $window, PlanningToolService) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var levelCount = scope.treelevels['dropdown_levels_' + attrs.attributeid].length
                var isDataTypeExists = false;
                var modelVal = {};
                if (attrs.choosefromparent == "false") {
                    PlanningToolService.GetTreeNode(attrs.attributeid).then(function (GetTree) {
                        scope.treeSources["dropdown_" + attrs.attributeid] = JSON.parse(GetTree.Response);
                        DesignXeditable();
                    });
                } else {
                    PlanningToolService.GetTreeNodeByEntityID(attrs.attributeid, attrs.entityid).then(function (GetTree) {
                        scope.treeSources["dropdown_" + attrs.attributeid] = JSON.parse(GetTree.Response);
                        DesignXeditable();
                    });
                }

                function DesignXeditable() {
                    PlanningToolService.GetTreeNode(attrs.attributeid).then(function (GetTree) {
                        scope.treeSources["dropdown_" + attrs.attributeid] = JSON.parse(GetTree.Response);
                        for (var variable in $.fn.editabletypes) {
                            if ((attributeName + attrs.attributeid) == variable) isDataTypeExists = true;
                        }
                        for (var i = 1; i <= levelCount; i++) {
                            if (i == 1) {
                                scope.DropDownTreeOptionValues["Options" + attrs.attributeid + "_" + i] = [];
                                scope['ddtoption_' + attrs.attributeid + '_' + i] = {
                                    multiple: false,
                                    formatResult: function (item) {
                                        return item.text;
                                    },
                                    formatSelection: function (item) {
                                        return item.text;
                                    }
                                };
                                scope.DropDownTreeOptionValues["Options" + attrs.attributeid + "_" + i] = scope.treeSources['dropdown_' + attrs.attributeid].Children;
                            } else {
                                scope.DropDownTreeOptionValues["Options" + attrs.attributeid + "_" + i] = [];
                                scope['ddtoption_' + attrs.attributeid + '_' + i] = {
                                    multiple: false,
                                    formatResult: function (item) {
                                        return item.text;
                                    },
                                    formatSelection: function (item) {
                                        return item.text;
                                    }
                                };
                            }
                        }
                        if (isDataTypeExists === false) {
                            var Address = function (options) {
                                this.init((attributeName + attrs.attributeid), options, Address.defaults);
                            };
                            $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                            var inlinePopupDesign1 = '<table>';
                            for (var i = 1; i <= levelCount; i++) {
                                if (i == 1) {
                                    var levelcaption = scope.treelevels['dropdown_levels_' + attrs.attributeid][i - 1].caption;
                                    inlinePopupDesign1 += '<tr><td><label><span>' + levelcaption + ': </span></label></td><td><label>';
                                    inlinePopupDesign1 += '<select ui-select2=\"ddtoption_' + attrs.attributeid + '_' + i + '\" type="hidden"  data-ng-model=\"dropdown_' + attrs.attributeid + '_' + i + '\" class=\"ngcompile\" id=\"xDropDownTree_' + attrs.attributeid + '_' + i + '\" ng-click=\"BindChildDropdownSource(' + attrs.attributeid + ',' + levelCount + ',' + i + ', 6)\" >';
                                    inlinePopupDesign1 += '<option ng-repeat=\"item in DropDownTreeOptionValues.Options' + attrs.attributeid + '_' + i + '\" value=\"{{item.id}}\">{{item.Caption}}</option> ';
                                    inlinePopupDesign1 += '</select></label></td></tr>';
                                } else {
                                    var levelcaption = scope.treelevels['dropdown_levels_' + attrs.attributeid][i - 1].caption;
                                    inlinePopupDesign1 += '<tr><td>  <label><span>' + levelcaption + ': </span></label></td><td><label>';
                                    inlinePopupDesign1 += '<select ui-select2=\"ddtoption_' + attrs.attributeid + '_' + i + '\" type="hidden"  data-ng-model=\"dropdown_' + attrs.attributeid + '_' + i + '\" class=\"ngcompile\" id=\"xDropDownTree_' + attrs.attributeid + '_' + i + '\" ng-click=\"BindChildDropdownSource(' + attrs.attributeid + ',' + levelCount + ',' + i + ', 6)\" >';
                                    inlinePopupDesign1 += '<option ng-repeat=\"item in DropDownTreeOptionValues.Options' + attrs.attributeid + '_' + i + '\" value=\"{{item.id}}\">{{item.Caption}}</option> ';
                                    inlinePopupDesign1 += '</select></label></td></tr>';
                                }
                            }
                            inlinePopupDesign1 += '</table>';
                            Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                                tpl: inlinePopupDesign1,
                                inputclass: ''
                            });
                            $.fn.editabletypes[(attributeName + attrs.attributeid)] = Address;
                        }
                        var loadXeditable = function () {
                            angular.element(element).editable({
                                title: attrs.title,
                                display: function (value, srcData) { },
                                savenochange: true
                            });
                        }
                        $timeout(function () {
                            loadXeditable();
                        }, 10);
                        angular.element(element).on('hide', function (e, editable) {
                            for (var i = 1; i <= levelCount; i++) {
                                scope['IsChanged_' + attrs.attributeid + '_' + i] = true;
                                scope['dropdown_' + attrs.attributeid + '_' + i] = {};
                                scope.drpdirectiveSource['dropdown_' + attrs.attributeid + '_' + i] = {};
                            }
                            scope.$apply();
                        });
                        angular.element(element).on('shown', function (e, editable) {
                            $timeout(function () {
                                if (editable != undefined) {
                                    modelVal = {};
                                    $compile($('.ngcompile'))(scope);
                                    for (var i = 1; i <= levelCount; i++) {
                                        scope['IsChanged_' + attrs.attributeid + '_' + i] = false;
                                        var dropdown_text = 'dropdown_text_' + attrs.attributeid + '_' + i;
                                        if (i == 1) {
                                            scope.drpdirectiveSource['dropdown_' + attrs.attributeid + '_' + i] = ($.grep(scope.treeSources["dropdown_" + attrs.attributeid].Children, function (e) {
                                                return e.Caption.toString() == scope.treeTexts[dropdown_text].toString().trim()
                                            }))[0];
                                            scope.DropDownTreeOptionValues["Options" + attrs.attributeid + "_" + i] = scope.treeSources["dropdown_" + attrs.attributeid].Children;
                                            modelVal = ($.grep(scope.treeSources["dropdown_" + attrs.attributeid].Children, function (e) {
                                                return e.Caption.toString() == scope.treeTexts[dropdown_text].toString().trim()
                                            }))[0];
                                            scope['dropdown_' + attrs.attributeid + '_' + i] = 0;
                                            if (modelVal != undefined) {
                                                scope['dropdown_' + attrs.attributeid + '_' + i] = modelVal.id;
                                            }
                                        } else {
                                            if (scope.DropDownTreeOptionValues['Options' + attrs.attributeid + '_' + (i - 1)] != undefined) {
                                                var sublevel_result = $.grep(scope.DropDownTreeOptionValues['Options' + attrs.attributeid + '_' + (i - 1)], function (e) { return e.id == scope["dropdown_" + attrs.attributeid + "_" + (i - 1)] })[0];

                                                if (sublevel_result != undefined) {
                                                    modelVal = ($.grep(sublevel_result.Children, function (e) {
                                                        return e.Caption.toString() == scope.treeTexts[dropdown_text].toString().trim()
                                                    }))[0];

                                                    scope['dropdown_' + attrs.attributeid + '_' + i] = 0;

                                                    scope.BindChildDropdownSource((attrs.attributeid), levelCount, (i - 1), 6, 0);

                                                    if (modelVal != undefined) {
                                                        scope['dropdown_' + attrs.attributeid + '_' + i] = modelVal.id;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }, 30);
                        });
                        angular.element(element).on('save', function (e, params) {
                            scope.treeSelection = [];
                            if (attrs.attributetypeid != 6) {
                                for (var i = 1; i <= levelCount; i++) {
                                    scope['IsChanged_' + attrs.attributeid + '_' + i] = true;
                                    var dropdown_text = 'DropDown_' + attrs.attributeid + '_' + i;
                                    if ((scope['dropdown_' + attrs.attributeid + '_' + i] != undefined) && (scope['dropdown_' + attrs.attributeid + '_' + i] != "")) {
                                        scope.fields[dropdown_text] = scope['dropdown_' + attrs.attributeid + '_' + i];
                                        if (scope['dropdown_' + attrs.attributeid + '_' + i].id == undefined || scope['dropdown_' + attrs.attributeid + '_' + i].id == "") {
                                            scope.treeSelection.push(0);
                                        } else {
                                            scope.treeSelection.push(scope['dropdown_' + attrs.attributeid + '_' + i].id);
                                        }
                                        scope.treeTexts['dropdown_text_' + attrs.attributeid + '_' + i] = scope['dropdown_' + attrs.attributeid + '_' + i].Caption;
                                    } else {
                                        scope.fields[dropdown_text] = '-';
                                        scope.treeSelection.push(0);
                                        scope.treeTexts['dropdown_text_' + attrs.attributeid + '_' + i] = '-';
                                    }
                                    scope.$apply();
                                }
                            }
                            else {
                                for (var i = 1; i <= levelCount; i++) {
                                    scope['IsChanged_' + attrs.attributeid + '_' + i] = true;
                                    var dropdown_text = 'DropDown_' + attrs.attributeid + '_' + i;
                                    if ((scope['dropdown_' + attrs.attributeid + '_' + i] != undefined) && (scope['dropdown_' + attrs.attributeid + '_' + i] != "")) {
                                        scope.fields[dropdown_text] = scope['dropdown_' + attrs.attributeid + '_' + i];
                                        if (scope['dropdown_' + attrs.attributeid + '_' + i] == undefined || scope['dropdown_' + attrs.attributeid + '_' + i] == "") {
                                            scope.treeSelection.push(0);
                                        } else {
                                            scope.treeSelection.push(parseInt(scope['dropdown_' + attrs.attributeid + '_' + i]));
                                        }
                                        scope.treeTexts['dropdown_text_' + attrs.attributeid + '_' + i] = $.grep(scope.DropDownTreeOptionValues["Options" + attrs.attributeid + "_" + i], function (e) { return e.id == scope["dropdown_" + attrs.attributeid + "_" + i] })[0].Caption;
                                    } else {
                                        scope.fields[dropdown_text] = '-';
                                        scope.treeSelection.push(0);
                                        scope.treeTexts['dropdown_text_' + attrs.attributeid + '_' + i] = '-';
                                    }
                                    scope.$apply();
                                }
                            }
                            scope.saveDropdownTree(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope.treeSelection);
                        });
                    });
                }
            }
        };
    }]);
    app.directive('xeditableoptimizedtreedropdown', ['$timeout', '$compile', '$resource', '$translate', '$window', 'PlanningToolService', function ($timeout, $compile, $resource, $translate, $window, PlanningToolService) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var levelCount = scope.treelevels['dropdown_levels_' + attrs.attributeid].length
                var isDataTypeExists = false;
                DesignXeditable();

                function DesignXeditable() {
                    PlanningToolService.GetTreeNode(attrs.attributeid).then(function (GetTree) {
                        scope.treeSources["dropdown_" + attrs.attributeid] = JSON.parse(GetTree.Response);
                        for (var variable in $.fn.editabletypes) {
                            if (attributeName == variable) isDataTypeExists = true;
                        }
                        for (var i = 1; i <= levelCount; i++) {
                            if (i == 1) {
                                scope['ddtoption_' + attrs.attributeid + '_' + i] = {};
                                scope['ddtoption_' + attrs.attributeid + '_' + i].multiple = false;
                                scope['ddtoption_' + attrs.attributeid + '_' + i].data = scope.treeSources['dropdown_' + attrs.attributeid].Children;
                                scope['ddtoption_' + attrs.attributeid + '_' + i].formatResult = function (item) {
                                    return item.Caption
                                };
                                scope['ddtoption_' + attrs.attributeid + '_' + i].formatSelection = function (item) {
                                    return item.Caption
                                };
                            } else {
                                scope['ddtoption_' + attrs.attributeid + '_' + i] = {};
                                scope['ddtoption_' + attrs.attributeid + '_' + i].multiple = false;
                                scope['ddtoption_' + attrs.attributeid + '_' + i].data = [];
                                scope['ddtoption_' + attrs.attributeid + '_' + i].formatResult = function (item) {
                                    return item.Caption
                                };
                                scope['ddtoption_' + attrs.attributeid + '_' + i].formatSelection = function (item) {
                                    return item.Caption
                                };
                            }
                        }
                        if (isDataTypeExists === false) {
                            var Address = function (options) {
                                this.init(attributeName, options, Address.defaults);
                            };
                            $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                            var inlinePopupDesign1 = '<table>';
                            for (var i = 1; i <= levelCount; i++) {
                                if (i == 1) {
                                    var levelcaption = scope.treelevels['dropdown_levels_' + attrs.attributeid][i - 1].caption;
                                    inlinePopupDesign1 += '<tr><td><label><span>' + levelcaption + ': </span></label></td><td><label>';
                                    inlinePopupDesign1 += '<input ui-select2=\"ddtoption_' + attrs.attributeid + '_' + i + '\" type="hidden"  data-ng-model=\"dropdown_' + attrs.attributeid + '_' + i + '\" class=\"ngcompile\" ng-change=\"BindChildDropdownSource(' + attrs.attributeid + ',' + levelCount + ',' + i + ', 6)\" />';
                                    inlinePopupDesign1 += '</label></td></tr>';
                                } else {
                                    var levelcaption = scope.treelevels['dropdown_levels_' + attrs.attributeid][i - 1].caption;
                                    inlinePopupDesign1 += '<tr><td>  <label><span>' + levelcaption + ': </span></label></td><td><label>';
                                    inlinePopupDesign1 += '<input ui-select2=\"ddtoption_' + attrs.attributeid + '_' + i + '\"  data-ng-model=\"dropdown_' + attrs.attributeid + '_' + i + '\" class=\"ngcompile\" ng-change=\"BindChildDropdownSource(' + attrs.attributeid + ',' + levelCount + ',' + i + ', 6)\" />';
                                    inlinePopupDesign1 += '</label></td></tr>';
                                }
                            }
                            inlinePopupDesign1 += '</table>';
                            Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                                tpl: inlinePopupDesign1,
                                inputclass: ''
                            });
                            $.fn.editabletypes[attributeName] = Address;
                        }
                        var loadXeditable = function () {
                            angular.element(element).editable({
                                title: attrs.title,
                                display: function (value, srcData) { },
                                savenochange: true
                            });
                        }
                        $timeout(function () {
                            loadXeditable();
                        }, 10);
                        angular.element(element).on('shown', function (e, editable) {
                            $timeout(function () {
                                if (editable != undefined) {
                                    $compile($('.ngcompile'))(scope);
                                    for (var i = 1; i <= levelCount; i++) {
                                        var dropdown_text = 'dropdown_text_' + attrs.attributeid + '_' + i;
                                        if (i == 1) {
                                            scope['dropdown_' + attrs.attributeid + '_' + i] = ($.grep(scope.treeSources["dropdown_" + attrs.attributeid].Children, function (e) {
                                                return e.Caption.toString() == scope.treeTexts[dropdown_text].toString().trim()
                                            }))[0];
                                        } else {
                                            if (scope['dropdown_' + attrs.attributeid + '_' + (i - 1)] != undefined) {
                                                if (scope['dropdown_' + attrs.attributeid + '_' + (i - 1)].Children != undefined) {
                                                    scope.BindChildDropdownSource((attrs.attributeid), levelCount, (i - 1), 6);
                                                    scope['dropdown_' + attrs.attributeid + '_' + i] = ($.grep(scope['dropdown_' + attrs.attributeid + '_' + (i - 1)].Children, function (e) {
                                                        return e.Caption.toString() == scope.treeTexts[dropdown_text].toString().trim()
                                                    }))[0];
                                                }
                                            }
                                        }
                                    }
                                }
                            }, 10);
                        });
                        angular.element(element).on('save', function (e, params) {
                            scope.treeSelection = [];
                            for (var i = 1; i <= levelCount; i++) {
                                var dropdown_text = 'DropDown_' + attrs.attributeid + '_' + i;
                                if ((scope['dropdown_' + attrs.attributeid + '_' + i] != undefined) && (scope['dropdown_' + attrs.attributeid + '_' + i] != "")) {
                                    scope.fields[dropdown_text] = scope['dropdown_' + attrs.attributeid + '_' + i];
                                    if (scope['dropdown_' + attrs.attributeid + '_' + i].id == undefined || scope['dropdown_' + attrs.attributeid + '_' + i].id == "") {
                                        scope.treeSelection.push(0);
                                    } else {
                                        scope.treeSelection.push(scope['dropdown_' + attrs.attributeid + '_' + i].id);
                                    }
                                    scope.treeTexts['dropdown_text_' + attrs.attributeid + '_' + i] = scope['dropdown_' + attrs.attributeid + '_' + i].Caption;
                                } else {
                                    scope.fields[dropdown_text] = '-';
                                    scope.treeSelection.push(0);
                                    scope.treeTexts['dropdown_text_' + attrs.attributeid + '_' + i] = '-';
                                }
                                scope.$apply();
                            }
                            scope.saveDropdownTree(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope.treeSelection);
                        });
                    });
                }
            }
        };
    }]);
    app.directive('xeditabledropdown', ['$timeout', '$compile', '$resource', '$translate', '$window', 'PlanningToolService', function ($timeout, $compile, $resource, $translate, $window, PlanningToolService) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var Version = 1;
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var isDataTypeExists = false;
                var GetOptionListByID = '';
                if (attrs.isdam == null) {
                    PlanningToolService.GetOptionDetailListByID(attrs.attributeid, CurrentEntityID).then(function (optionlist) {
                        $window.XeditableDirectivesSource["NormalDropDown_" + attrs.attributeid] = optionlist.Response;
                        scope.normaltreeSources["NormalDropDown_" + attrs.attributeid] = optionlist.Response;
                    });
                } else {
                    if (attrs.isdam = true) {
                        PlanningToolService.GetOptionDetailListByAssetID(attrs.attributeid, CurrentEntityID).then(function (optionlist) {
                            $window.XeditableDirectivesSource["NormalDropDown_" + attrs.attributeid] = optionlist.Response;
                            scope.normaltreeSources["NormalDropDown_" + attrs.attributeid] = optionlist.Response;
                        });
                    }
                }
                for (var variable in $.fn.editabletypes) {
                    if (attributeName == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(attributeName, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopup = '';
                    inlinePopup += '<div><label><select ui-select2 data-ng-model="NormalDropDown_' + attrs.attributeid + '" name="city" class="ngcompileDropdown"><option ng-repeat=\"ndata in normaltreeSources.NormalDropDown_' + attrs.attributeid + '\" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></label></div>';
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopup,
                        inputclass: ''
                    });
                    $.fn.editabletypes[attributeName] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('hide', function (e, editable) {
                    scope['NormalDropDown_' + attrs.attributeid] = "";
                    scope.$apply();
                });
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompileDropdown'))(scope);
                            scope['ischanged_' + attrs.attributeid] = false;
                            var tempvar = ($.grep(scope.normaltreeSources["NormalDropDown_" + attrs.attributeid], function (e) {
                                return e.Caption == scope.fields['NormalDropDown_' + attrs.attributeid]
                            }))[0];
                            if (tempvar != null) scope['NormalDropDown_' + attrs.attributeid] = tempvar.Id;
                            else scope['NormalDropDown_' + attrs.attributeid] = '';
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    scope['ischanged_' + attrs.attributeid] = true;
                    scope.treeSelection = [];
                    var dropdowntext = 'NormalDropDown_' + attrs.attributeid;
                    if (scope['NormalDropDown_' + attrs.attributeid] != undefined) {
                        var tempvar = ($.grep(scope.normaltreeSources["NormalDropDown_" + attrs.attributeid], function (e) {
                            return parseInt(e.Id) == parseInt(scope['NormalDropDown_' + attrs.attributeid])
                        }))[0];
                        scope.fields[dropdowntext] = tempvar.Caption;
                        scope.treeSelection.push(tempvar.Id);
                        scope.$apply();
                    } else {
                        scope.fields[dropdowntext] = '-';
                        scope.treeSelection.push(0);
                        scope.$apply();
                    }
                    scope.saveDropdownTree(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope.treeSelection);
                });
            }
        };
    }]);
    app.directive('xeditablemultiselectdropdown', ['$timeout', '$compile', '$resource', '$translate', '$window', 'PlanningToolService', function ($timeout, $compile, $resource, $translate, $window, PlanningToolService) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var Version = 1;
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var isDataTypeExists = false;
                scope.normaltreeSources["NormalMultiDropDown_" + attrs.attributeid] = [];
                var GetOptionListByID = ''
                if (attrs.isdam == null) {
                    PlanningToolService.GetOptionDetailListByID(attrs.attributeid, CurrentEntityID).then(function (optionlist) {
                        $window.XeditableDirectivesSource["NormalMultiDropDown_" + attrs.attributeid] = optionlist.Response;
                        scope.normaltreeSources["NormalMultiDropDown_" + attrs.attributeid] = optionlist.Response;
                    });
                } else {
                    if (attrs.isdam = true) {
                        PlanningToolService.GetOptionDetailListByAssetID(attrs.attributeid, CurrentEntityID).then(function (optionlist) {
                            $window.XeditableDirectivesSource["NormalMultiDropDown_" + attrs.attributeid] = optionlist.Response;
                            scope.normaltreeSources["NormalMultiDropDown_" + attrs.attributeid] = optionlist.Response;
                        });
                    }
                }
                for (var variable in $.fn.editabletypes) {
                    if (attributeName == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(attributeName, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopup = '';
                    inlinePopup += '<div><label> \
                                                <select multiselect-dropdown ng-options="ndata.Id as ndata.Caption for ndata in normaltreeSources.NormalMultiDropDown_' + attrs.attributeid + '\" id="NormalMultiDropDown1_' + attrs.attributeid + '" multiple=\"multiple\" data-ng-model="NormalMultiDropDown_' + attrs.attributeid + '" name="city" class="ngcompileDropdown multiselect"> \
                                                </select> \
                                            </label></div>';
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopup,
                        inputclass: ''
                    });
                    $.fn.editabletypes[attributeName] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('hide', function (e, editable) {
                    scope['IsChanged_' + attrs.attributeid] = true;
                    scope['NormalMultiDropDown_' + attrs.attributeid] = [];
                    scope.$apply();
                });
                angular.element(element).on('shown', function (e, editable) {
                    scope['IsChanged_' + attrs.attributeid] = false;
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompileDropdown'))(scope);
                            var captions = scope.fields['NormalMultiDropDown_' + attrs.attributeid].split(',')
                            scope['NormalMultiDropDown_' + attrs.attributeid] = [];
                            var tempvar = [];
                            for (var x = 0; x <= captions.length; x++) {
                                if (captions[x] != undefined) {
                                    if ($.grep(scope.normaltreeSources["NormalMultiDropDown_" + attrs.attributeid], function (e) {
										return e.Caption.trim() == captions[x].trim()
                                    }).length > 0) {
                                        tempvar.push(($.grep(scope.normaltreeSources["NormalMultiDropDown_" + attrs.attributeid], function (e) {
                                            return e.Caption.trim() == captions[x].trim()
                                        }))[0]);
                                        scope['NormalMultiDropDown_' + attrs.attributeid].push(tempvar[x].Id);
                                    }
                                }
                            }
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    scope['IsChanged_' + attrs.attributeid] = true;
                    scope.treeSelection = [];
                    var dropdowntext = 'NormalMultiDropDown_' + attrs.attributeid;
                    var tempcap = [];
                    if (scope['NormalMultiDropDown_' + attrs.attributeid] != undefined && scope['NormalMultiDropDown_' + attrs.attributeid].length > 0) {
                        var tempvar = [];
                        var tempval = scope['NormalMultiDropDown_' + attrs.attributeid];
                        for (var x = 0; x < tempval.length; x++) {
                            tempvar.push(($.grep(scope.normaltreeSources["NormalMultiDropDown_" + attrs.attributeid], function (e) {
                                return parseInt(e.Id) == parseInt(tempval[x]);
                            }))[0]);
                        }
                        for (var j = 0; j < tempvar.length; j++) {
                            tempcap.push(tempvar[j].Caption);
                            scope.treeSelection.push(tempvar[j].Id);
                            scope.$apply();
                        }
                        scope.fields['NormalMultiDropDown_' + attrs.attributeid] = tempcap.join(',');
                    }
                    if (scope.listAttributeValidationResult != undefined) {
                        var isValidationReq = $.grep(scope.listAttributeValidationResult, function (e) {
                            return (e.AttributeID == parseInt(attrs.attributeid));
                        });
                        $("#overviewDetialblock").removeClass('warning');
                        if (isValidationReq.length > 0 && isValidationReq[0].RelationShipID != 0 && tempvar == undefined) {
                            var metrics = [
								['#NormalMultiDropDown1_' + attrs.attributeid + '', isValidationReq[0].ValueType, isValidationReq[0].ErrorMessage]
                            ];
                            var options = {
                                'groupClass': 'warning',
                            };
                            $("#overviewDetialblock").nod(metrics, options);
                            bootbox.alert(isValidationReq[0].ErrorMessage);
                        } else {
                            scope.fields[dropdowntext] = scope.fields['NormalMultiDropDown_' + attrs.attributeid];
                            scope.treeSelection.push(0);
                            scope.$apply();
                            scope.saveDropdownTree(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope.treeSelection);
                        }
                    } else {
                        if (scope['NormalMultiDropDown_' + attrs.attributeid] != undefined && scope['NormalMultiDropDown_' + attrs.attributeid].length > 0) { } else {
                            scope.fields[dropdowntext] = '-';
                            scope.treeSelection.push(0);
                            scope.$apply();
                        }
                        scope.saveDropdownTree(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope.treeSelection);
                    }
                });
            }
        };
    }]);
    app.directive('xeditabletreedropdown1', ['$timeout', '$compile', '$resource', '$translate', '$window', function ($timeout, $compile, $resource, $translate, $window) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var attributeName = attrs.attributename + attrs.primaryid;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var periodstartdateid = attrs.periodstartdateId;
                var periodenddateid = attrs.periodenddateId;
                var attrID = attrs.primaryid;
                var isDataTypeExists = false;
                var isDam = attrs.isdam;
                for (var variable in $.fn.editabletypes) {
                    if (attributeName == variable) isDataTypeExists = true;
                }
                // scope.format = scope.DefaultSettings.DateFormat.replace(/m/gi, "M");
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(attributeName, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopupDesign1 = '';
                    if (isDam == "true") {
                        inlinePopupDesign1 += '<div class="row-fluid"><div class="inputHolder">';
                        inlinePopupDesign1 += '<div class="ngcompile"><input class="width2x" placeholder="-- Start date --" id="' + periodstartdateid + '" data-ng-model="fields.PeriodStartDate_Dir_' + attrID + '" type="text" ng-click=\"PerioddirectiveCalanderopen($event,' + attrs.primaryid + ',\'start\')\" datepicker-popup=\"{{format}}\" is-open=\"fields.PeriodStartDateopen_' + attrID + '\" min-date="fields.DatePartMinDate_' + attrID + '" max-date="fields.DatePartMaxDate_' + attrID + '" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" data-date-format="' + Defaultdate + '" name="startDate" required="" class="ng-pristine ng-invalid ng-invalid-required"></div>';
                        inlinePopupDesign1 += '<div class="ngcompile"><input class="width2x" placeholder="-- End date --" id="' + periodenddateid + '" data-ng-model="fields.PeriodEndDate_Dir_' + attrID + '" type="text"  ng-click=\"PerioddirectiveCalanderopen($event,' + attrs.primaryid + ',\'end\')\"  datepicker-popup=\"{{format}}\" is-open=\"fields.PeriodEndDateopen_' + attrID + '\" min-date="fields.DatePartMinDate_' + attrID + '" max-date="fields.DatePartMaxDate_' + attrID + '" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" required="" name="EndDate"  data-date-format="' + Defaultdate + '" class="ng-pristine ng-invalid ng-invalid-required margin-bottom10x"></div>';
                        inlinePopupDesign1 += '<div class="ngcompile"><input class="width2x" placeholder="-- Comment Start/End Date --" type="text" data-ng-model="fields.PeriodDateDesc_Dir_' + attrID + ' " name="comment" class="ng-pristine ng-invalid ng-invalid-required"></div>';
                        inlinePopupDesign1 += '</div></div>';
                        scope.fields["fields.PeriodStartDateopen_" + attrID] = false;
                        scope.fields["fields.PeriodEndDateopen_" + attrID] = false;
                    } else {
                        inlinePopupDesign1 += '<div class="row-fluid"><div class="inputHolder">';
                        inlinePopupDesign1 += '<div class="ngcompile"><input class="width2x" placeholder="-- Start date --" id="' + periodstartdateid + '" data-ng-model="fields.PeriodStartDate_Dir_' + attrID + '" type="text"  data-date-format="' + Defaultdate + '" ng-click=\"PerioddirectiveCalanderopen($event,' + attrs.primaryid + ',\'start\')\"  datepicker-popup=\"{{format}}\"  is-open=\"fields.PeriodStartDateopen_' + attrID + '\" min-date="fields.DatePartMinDate_' + attrID + '" max-date="fields.DatePartMaxDate_' + attrID + '" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" name="startDate" class="ng-pristine ng-invalid ng-invalid-required"></div>';
                        inlinePopupDesign1 += '<div class="ngcompile"><input class="width2x" placeholder="-- End date --" id="' + periodenddateid + '" data-ng-model="fields.PeriodEndDate_Dir_' + attrID + '" type="text" data-date-format="' + Defaultdate + '" ng-click=\"PerioddirectiveCalanderopen($event,' + attrs.primaryid + ',\'end\')\"  datepicker-popup=\"{{format}}\"  is-open=\"fields.PeriodEndDateopen_' + attrID + '\" min-date="fields.DatePartMinDate_' + attrID + '" max-date="fields.DatePartMaxDate_' + attrID + '" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" required="" name="EndDate" class="ng-pristine ng-invalid ng-invalid-required margin-bottom10x"></div>';
                        inlinePopupDesign1 += '<div class="ngcompile"><input class="width2x" placeholder="-- Comment Start/End Date --" type="text" data-ng-model="fields.PeriodDateDesc_Dir_' + attrID + ' " name="comment" class="ng-pristine ng-invalid ng-invalid-required"></div>';
                        inlinePopupDesign1 += '</div></div>';
                        scope.fields["fields.PeriodStartDateopen_" + attrID] = false;
                        scope.fields["fields.PeriodEndDateopen_" + attrID] = false;
                    }
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopupDesign1,
                        inputclass: ''
                    });
                    $.fn.editabletypes[attributeName] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompile'))(scope);
                            scope.fields["PeriodStartDate_Dir_0"] = "";
                            scope.fields["PeriodEndDate_Dir_0"] = "";
                            scope.fields["PeriodDateDesc_Dir_0"] = "";
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    var a = $.grep(scope.tempholidays, function (e) {
                        return e == dateFormat(scope.fields['PeriodStartDate_Dir_' + attrs.primaryid], scope.format);
                    });
                    var b = $.grep(scope.tempholidays, function (e) {
                        return e == dateFormat(scope.fields['PeriodEndDate_Dir_' + attrs.primaryid], scope.format);
                    });
                    if (a != null || b != null) {
                        if (a.length > 0) {
                            bootbox.alert("Selected startdate should not same as non businessday");
                            scope.fields['PeriodStartDate_Dir_' + attrs.primaryid] = "";
                        } else if (b.length > 0) {
                            bootbox.alert("Selected enddate should not same as non businessday");
                            scope.fields['PeriodEndDate_Dir_' + attrs.primaryid] = "";
                        } else {
                            scope.savePeriodVal(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, attrs.primaryid);
                            //scope.saveDropdownTree(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope.fields["DateTime_Dir_" + attrID]);
                        }
                    }
                });
            }
        };
    }]);
    app.directive('xeditabledropdown12', ['$timeout', '$compile', '$resource', '$translate', '$window', function ($timeout, $compile, $resource, $translate, $window) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var Version = 1;
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var isDataTypeExists = false;
                for (var variable in $.fn.editabletypes) {
                    if (attributeName == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(attributeName, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopup = '';
                    inlinePopup += '<div><label><select ui-select2 ng-options=\"ndata as ndata.StepName for ndata in workflowstepOptions" data-ng-model="xeditWorkflowStep" name="city" class="ngcompileDropdown" /></label></div>';
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopup,
                        inputclass: ''
                    });
                    $.fn.editabletypes[attributeName] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompileDropdown'))(scope);
                            scope['xeditWorkflowStep'] = ($.grep(scope.workflowstepOptions, function (e) {
                                return e.StepName == scope.CurrentWorkflowStepName
                            }))[0];
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    if (scope['xeditWorkflowStep'] != undefined) {
                        scope.CurrentWorkflowStepName = scope['xeditWorkflowStep'].StepName;
                        scope.SelectedWorkflowStep = scope['xeditWorkflowStep'].StepID;
                        scope.WorkFlowStepID = scope['xeditWorkflowStep'].StepID;
                        scope.$apply();
                    } else {
                        scope.CurrentWorkflowStepName = '-';
                        $scope.SelectedWorkflowStep = 0;
                        scope.WorkFlowStepID = 0;
                        scope.$apply();
                    }
                    scope.UpdateEntityWorkFlowStep(scope.SelectedWorkflowStep);
                });
            }
        };
    }]);
    app.directive('xeditablemultiselecttreedropdown', ['$timeout', '$compile', '$resource', '$translate', '$window', 'PlanningToolService', function ($timeout, $compile, $resource, $translate, $window, PlanningToolService) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var levelCount = scope.treelevels['multiselectdropdown_levels_' + attrs.attributeid].length
                var isDataTypeExists = false;
                if (attrs.isdam == null || attrs.isdam == undefined) {
                    if (attrs.choosefromparent == "false") {
                        PlanningToolService.GetDamTreeNode(attrs.attributeid).then(function (GetTree) {
                        scope.treeSources["multiselectdropdown_" + attrs.attributeid] = JSON.parse(GetTree.Response);
                        DesignXeditable();
                        });
                    } else {
                        PlanningToolService.GetDamTreeNodeByEntityID(attrs.attributeid, attrs.entityid).then(function (GetTree) {
                        scope.treeSources["multiselectdropdown_" + attrs.attributeid] = JSON.parse(GetTree.Response);
                        DesignXeditable();
                        });
                    }
                } else if (attrs.isdam == "true") {
                    if (attrs.choosefromparent == "false") {
                        PlanningToolService.GetDamTreeNode(attrs.attributeid).then(function (GetTree) {
                            scope.treeSources["multiselectdropdown_" + attrs.attributeid] = JSON.parse(GetTree.Response);
                        DesignXeditable();
                        });
                    } else {
                        PlanningToolService.GetDamTreeNodeByEntityID(attrs.attributeid, attrs.entityid).then(function (GetTree) {
                            scope.treeSources["multiselectdropdown_" + attrs.attributeid] = JSON.parse(GetTree.Response);
                        DesignXeditable();
                        });
                    }
                }

                function DesignXeditable() {
                    for (var variable in $.fn.editabletypes) {
                        if (attributeName == variable) isDataTypeExists = true;
                    }
                    for (var i = 1; i <= levelCount; i++) {
                        scope['ddtoption_' + attrs.attributeid + '_' + i] = {
                            multiple: false,
                            formatResult: function (item) {
                                return item.text;
                            },
                            formatSelection: function (item) {
                                return item.text;
                            }
                        };
                        if (levelCount == 1) {
                            scope.DropDownTreeOptionValues["Options" + attrs.attributeid + "_" + i] = [];
                            scope['ddtoption_' + attrs.attributeid + '_' + i].multiple = true;
                            scope.DropDownTreeOptionValues["Options" + attrs.attributeid + "_" + i] = scope.treeSources['multiselectdropdown_' + attrs.attributeid].Children;
                        } else {
                            if (i == 1) {
                                scope['ddtoption_' + attrs.attributeid + '_' + i].multiple = false;
                                scope.DropDownTreeOptionValues["Options" + attrs.attributeid + "_" + i] = scope.treeSources['multiselectdropdown_' + attrs.attributeid].Children;
                            } else {
                                if (i == levelCount) scope['ddtoption_' + attrs.attributeid + '_' + i].multiple = true;
                                else scope['ddtoption_' + attrs.attributeid + '_' + i].multiple = false;
                                scope.DropDownTreeOptionValues["Options" + attrs.attributeid + "_" + i] = [];
                            }
                        }
                    }
                    if (isDataTypeExists === false) {
                        var Address = function (options) {
                            this.init(attributeName, options, Address.defaults);
                        };
                        $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                        var inlinePopupDesign1 = '';
                        inlinePopupDesign1 += '<table>';
                        for (var i = 1; i <= levelCount; i++) {
                            var levelcaption = scope.treelevels['multiselectdropdown_levels_' + attrs.attributeid][i - 1].caption;
                            if (levelCount == 1) {
                                var levelcaption = scope.treelevels['multiselectdropdown_levels_' + attrs.attributeid][i - 1].caption;
                                inlinePopupDesign1 += '<tr><td><label class="control-label">' + levelcaption + ': </label></td><td><label>';
                                inlinePopupDesign1 += '<select multiple ui-select2=\"ddtoption_' + attrs.attributeid + '_' + i + '\" type="hidden"  data-ng-model=\"multiselectdropdown_' + attrs.attributeid + '_' + i + '\" class=\"ngcompile\" >';
                                inlinePopupDesign1 += '<option ng-repeat=\"item in DropDownTreeOptionValues.Options' + attrs.attributeid + '_' + i + '\" value=\"{{item.id}}\">{{item.Caption}}</option> ';
                                inlinePopupDesign1 += '</select></label></td></tr>';
                            } else {
                                if (i == 1) {
                                    var levelcaption = scope.treelevels['multiselectdropdown_levels_' + attrs.attributeid][i - 1].caption;
                                    inlinePopupDesign1 += '<tr><td><label class="control-label">' + levelcaption + ': </label></td><td><label>';
                                    inlinePopupDesign1 += '<select ui-select2=\"ddtoption_' + attrs.attributeid + '_' + i + '\" type="hidden"  data-ng-model=\"multiselectdropdown_' + attrs.attributeid + '_' + i + '\" class=\"ngcompile\" ng-click=\"BindChildDropdownSource(' + attrs.attributeid + ',' + levelCount + ',' + i + ', 12)\" >';
                                    inlinePopupDesign1 += '<option ng-repeat=\"item in DropDownTreeOptionValues.Options' + attrs.attributeid + '_' + i + '\" value=\"{{item.id}}\">{{item.Caption}}</option> ';
                                    inlinePopupDesign1 += '</select></label></td></tr>';
                                } else {
                                    var levelcaption = scope.treelevels['multiselectdropdown_levels_' + attrs.attributeid][i - 1].caption;
                                    inlinePopupDesign1 += '<tr><td><label class="control-label">' + levelcaption + ': </label></td><td><label>';
                                    if (i == levelCount)
                                        inlinePopupDesign1 += '<select multiple ui-select2=\"ddtoption_' + attrs.attributeid + '_' + i + '\" type="hidden"  data-ng-model=\"multiselectdropdown_' + attrs.attributeid + '_' + i + '\" class=\"ngcompile\" ng-click=\"BindChildDropdownSource(' + attrs.attributeid + ',' + levelCount + ',' + i + ', 12)\" >';
                                    else
                                        inlinePopupDesign1 += '<select ui-select2=\"ddtoption_' + attrs.attributeid + '_' + i + '\" type="hidden"  data-ng-model=\"multiselectdropdown_' + attrs.attributeid + '_' + i + '\" class=\"ngcompile\" ng-click=\"BindChildDropdownSource(' + attrs.attributeid + ',' + levelCount + ',' + i + ', 12)\" >';
                                    inlinePopupDesign1 += '<option ng-repeat=\"item in DropDownTreeOptionValues.Options' + attrs.attributeid + '_' + i + '\" value=\"{{item.id}}\">{{item.Caption}}</option> ';
                                    inlinePopupDesign1 += '</select></label></td></tr>';
                                }
                            }
                        }
                        inlinePopupDesign1 += '</table>';
                        Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                            tpl: inlinePopupDesign1,
                            inputclass: ''
                        });
                        $.fn.editabletypes[attributeName] = Address;
                    }
                    var loadXeditable = function () {
                        angular.element(element).editable({
                            title: attrs.title,
                            display: function (value, srcData) { },
                            savenochange: true
                        });
                    }
                    $timeout(function () {
                        loadXeditable();
                    }, 10);
                    angular.element(element).on('hide', function (e, editable) {
                        for (var i = 1; i <= levelCount; i++) {
                            scope['IsChanged_' + attrs.attributeid + '_' + i] = true;
                            scope['multiselectdropdown_' + attrs.attributeid + '_' + i] = [];
                        }
                        scope.$apply();
                    });
                    angular.element(element).on('shown', function (e, editable) {
                        $timeout(function () {
                            if (editable != undefined) {
                                $compile($('.ngcompile'))(scope);
                                if (levelCount == 1) {
                                    for (var tl = 1; tl <= levelCount; tl++) {
                                        scope['IsChanged_' + attrs.attributeid + '_' + i] = false;
                                        var dropdown_text = 'multiselectdropdown_text_' + attrs.attributeid + '_' + tl;
                                        if (tl == levelCount) {
                                            var tempcaption = scope.treeTexts[dropdown_text].toString().trim().split(',');
                                            scope['multiselectdropdown_' + attrs.attributeid + '_' + tl] = [];
                                            var tempval = [];
                                            var tempres = [];
                                            scope.DropDownTreeOptionValues["Options" + attrs.attributeid + "_" + i] = scope.treeSources["multiselectdropdown_" + attrs.attributeid].Children;
                                            for (var k = 0; k < tempcaption.length; k++) {
                                                if (tempcaption[k] != "" && tempcaption[k] != "-") {
                                                    tempres.push(($.grep(scope.treeSources["multiselectdropdown_" + attrs.attributeid].Children, function (e) {
                                                        return e.Caption.toString() == tempcaption[k].trim();
                                                    }))[0].id);
                                                }
                                            }
                                            scope['multiselectdropdown_' + attrs.attributeid + '_' + tl] = tempres;
                                        }
                                    }
                                } else {
                                    for (var i = 1; i <= levelCount; i++) {
                                        scope['IsChanged_' + attrs.attributeid + '_' + i] = false;
                                        var dropdown_text = 'multiselectdropdown_text_' + attrs.attributeid + '_' + i;
                                        if (i == 1) {
                                            scope.DropDownTreeOptionValues["Options" + attrs.attributeid + "_" + i] = scope.treeSources["multiselectdropdown_" + attrs.attributeid].Children;
                                            var modelVal = ($.grep(scope.treeSources["multiselectdropdown_" + attrs.attributeid].Children, function (e) {
                                                return e.Caption.toString() == scope.treeTexts[dropdown_text].toString().trim()
                                            }))[0];
                                            scope['multiselectdropdown_' + attrs.attributeid + '_' + i] = 0;
                                            if (modelVal != undefined)
                                                scope['multiselectdropdown_' + attrs.attributeid + '_' + i] = modelVal.id;
                                        } else {
                                            if (scope.DropDownTreeOptionValues['Options' + attrs.attributeid + '_' + (i - 1)] != undefined) {
                                                var sublevel_result = $.grep(scope.DropDownTreeOptionValues['Options' + attrs.attributeid + '_' + (i - 1)], function (e) { return e.id == scope["multiselectdropdown_" + attrs.attributeid + "_" + (i - 1)] })[0];

                                                var modelVal = [];
                                                if (sublevel_result != undefined) {
                                                    modelVal = ($.grep(sublevel_result.Children, function (e) {
                                                        return e.Caption.toString() == scope.treeTexts[dropdown_text].toString().trim()
                                                    }))[0];
                                                }
                                                scope.BindChildDropdownSource((attrs.attributeid), levelCount, (i - 1), 12);
                                                if (i == levelCount) {
                                                    var tempcaption = scope.treeTexts[dropdown_text].toString().trim().split(',');
                                                    scope['multiselectdropdown_' + attrs.attributeid + '_' + i] = [];
                                                    var tempval = [];
                                                    var res = [];
                                                    for (var k = 0; k < tempcaption.length; k++) {
                                                        if (scope.DropDownTreeOptionValues['Options' + attrs.attributeid + '_' + (i - 1)] != undefined)
                                                            var selection = [];
                                                        if (scope.DropDownTreeOptionValues['Options' + attrs.attributeid + '_' + (i - 1)].length > 0 && scope['multiselectdropdown_' + attrs.attributeid + '_' + (i - 1)] != undefined)
                                                            selection = $.grep(scope.DropDownTreeOptionValues['Options' + attrs.attributeid + '_' + (i - 1)], function (e) { return e.id == scope['multiselectdropdown_' + attrs.attributeid + '_' + (i - 1)] })[0].Children;

                                                        if (tempcaption[k] != "" && tempcaption[k] != "-")
                                                            res.push(($.grep(selection, function (e) { return e.Caption.toString() == tempcaption[k].trim(); }))[0].id);
                                                    }
                                                    scope['multiselectdropdown_' + attrs.attributeid + '_' + i] = res;
                                                } else {
                                                    scope['multiselectdropdown_' + attrs.attributeid + '_' + i] = 0;
                                                    if (modelVal != undefined)
                                                        scope['multiselectdropdown_' + attrs.attributeid + '_' + i] = modelVal.id;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }, 10);
                    });
                    angular.element(element).on('save', function (e, params) {
                        scope.treeSelection = [];
                        for (var li = 1; li <= levelCount; li++) {
                            scope['IsChanged_' + attrs.attributeid + '_' + i] = true;
                            var dropdown_text = 'MultiSelectDropDown_' + attrs.attributeid + '_' + li;
                            if (li == levelCount) {
                                if ((scope['multiselectdropdown_' + attrs.attributeid + '_' + li] != undefined) && (scope['multiselectdropdown_' + attrs.attributeid + '_' + li] != "")) {
                                    var tempval = [];
                                    for (var k = 0; k < (scope['multiselectdropdown_' + attrs.attributeid + '_' + li].length) ; k++) {
                                        scope.fields[dropdown_text] = scope['multiselectdropdown_' + attrs.attributeid + '_' + li][k];
                                        if (scope['multiselectdropdown_' + attrs.attributeid + '_' + li][k] == undefined || scope['multiselectdropdown_' + attrs.attributeid + '_' + li][k] == "") {
                                            scope.treeSelection.push(0);
                                        } else {
                                            scope.treeSelection.push(parseInt(scope['multiselectdropdown_' + attrs.attributeid + '_' + li][k]));
                                        }
                                        tempval.push($.grep(scope.DropDownTreeOptionValues["Options" + attrs.attributeid + "_" + li], function (e) { return e.id == parseInt(scope['multiselectdropdown_' + attrs.attributeid + '_' + li][k]) })[0].Caption);
                                    }
                                    if (tempval.length > 0) scope.treeTexts['multiselectdropdown_text_' + attrs.attributeid + '_' + li] = tempval.join(", ");
                                    else scope.treeTexts['multiselectdropdown_text_' + attrs.attributeid + '_' + li] = '-';
                                } else {
                                    scope.fields[dropdown_text] = '-';
                                    scope.treeSelection.push(0);
                                    scope.treeTexts['multiselectdropdown_text_' + attrs.attributeid + '_' + li] = '-';
                                }
                            } else {
                                if ((scope['multiselectdropdown_' + attrs.attributeid + '_' + li] != undefined) && (scope['multiselectdropdown_' + attrs.attributeid + '_' + li] != "")) {
                                    scope.fields[dropdown_text] = scope['multiselectdropdown_' + attrs.attributeid + '_' + li];
                                    if (scope['multiselectdropdown_' + attrs.attributeid + '_' + li] == undefined || scope['multiselectdropdown_' + attrs.attributeid + '_' + li] == "") {
                                        scope.treeSelection.push(0);
                                    } else {
                                        scope.treeSelection.push(parseInt(scope['multiselectdropdown_' + attrs.attributeid + '_' + li]));
                                    }
                                    scope.treeTexts['multiselectdropdown_text_' + attrs.attributeid + '_' + li] = $.grep(scope.DropDownTreeOptionValues["Options" + attrs.attributeid + "_" + li], function (e) { return e.id == parseInt(scope['multiselectdropdown_' + attrs.attributeid + '_' + li]) })[0].Caption;
                                } else {
                                    scope.fields[dropdown_text] = '-';
                                    scope.treeSelection.push(0);
                                    scope.treeTexts['multiselectdropdown_text_' + attrs.attributeid + '_' + li] = '-';
                                }
                            }
                            scope.$apply();
                        }
                        scope.saveDropdownTree(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope.treeSelection);
                    });
                }
            }
        };
    }]);
    app.directive('xeditabletreedatetime', ['$timeout', '$compile', '$resource', '$translate', '$window', function ($timeout, $compile, $resource, $window) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var attributeName = attrs.attributename + attrs.primaryid;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var datetimeid = attrs.datetimeId;
                var attrID = attrs.primaryid;
                var isDataTypeExists = false;
                for (var variable in $.fn.editabletypes) {
                    if (attributeName == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(attributeName, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopupDesign1 = '';
                    inlinePopupDesign1 += '<div class="ngcompile">';
                    inlinePopupDesign1 += '<input type="text" class="width2x" ng-click="Calanderopen($event, ' + attrID + ')"  datepicker-popup="{{format}}" ng-model="fields.DateTime_Dir_' + attrID + '" is-open="calanderopened" min-date="fields.DatePartMinDate_' + attrID + '" max-date="fields.DatePartMaxDate_' + attrID + '" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" close-text="Close" />'
                    inlinePopupDesign1 += '</div>';
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopupDesign1,
                        inputclass: ''
                    });
                    $.fn.editabletypes[attributeName] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            scope.opened = true;
                            $compile($('.ngcompile'))(scope);
                            if (scope.fields["DateTime_" + attrID] != "" && scope.fields["DateTime_" + attrID] != "-") if (scope.fields["DateTime_" + attrID] instanceof Date) scope.fields["DateTime_Dir_" + attrID] = scope.fields["DateTime_" + attrID];
                            else {
                                //scope.fields["DateTime_Dir_" + attrID] = (dateFormat(scope.fields["DateTime_" + attrID], scope.format));
                                scope.fields["DateTime_Dir_" + attrID] = scope.fields["DateTime_" + attrID];
                            } else scope.fields["DateTime_Dir_" + attrID] = null;
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    if (scope.fields["DateTime_Dir_" + attrID] == null && scope.fields["DateTime_Dir_" + attrID] == undefined) scope.fields["DateTime_Dir_" + attrID] = "";
                    var a = $.grep(scope.tempholidays, function (e) {
                        return e == dateFormat(scope.fields["DateTime_Dir_" + attrID], scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert("Selected date should not same as non businessday");
                            scope.fields["DateTime_Dir_" + attrID] = "";
                        } else {
                            scope.saveDropdownTree(attrs.attributeid, attrs.attributetypeid, CurrentEntityID, scope.fields["DateTime_Dir_" + attrID]);
                        }
                    }
                });
            }
        };
    }]);
    //app.directive("redactor", [function() {
    //	return {
    //		require: '?ngModel',
    //		link: function($scope, elem, attrs, controller) {
    //			controller.$render = function() {
    //				elem.redactor({
    //					keyupCallback: function() {
    //						$scope.$apply(controller.$setViewValue(elem.getCode()));
    //					},
    //					execCommandCallback: function() {
    //						$scope.$apply(controller.$setViewValue(elem.getCode()));
    //					}
    //				});
    //				if (controller.$viewValue != undefined) {
    //					elem.setCode(controller.$viewValue);
    //				}
    //			};
    //		}
    //	};
    //}]);
    app.directive('choiceTree', [function () {
        return {
            template: '<ul><choice ng-repeat="choice in tree"></choice></ul>',
            replace: true,
            transclude: true,
            restrict: 'E',
            scope: {
                tree: '=ngModel',
            }
        };
    }]);
    app.directive('choice', ['$compile', function ($compile) {
        return {
            restrict: 'E',
            template: '<li>' + '<label class="checkbox" ng-click="choiceClicked(choice)">' + '<input type="checkbox" ng-checked="choice.checked1"> <span>{{choice.name}}</span>' + '</label>' + '</li>',
            require: "^ngController",
            link: function (scope, elm, attrs, myGreatParentControler) {
                scope.choiceClicked = function (choice) {
                    choice.checked1 = !choice.checked1;
                    myGreatParentControler.setSelected(choice);

                    function checkChildren(c) {
                        angular.forEach(c.children, function (c) {
                            c.checked1 = choice.checked1;
                            if (c.id != choice.id) {
                                myGreatParentControler.setSelected(c);
                            }
                            checkChildren(c);
                        });
                    }
                    checkChildren(choice);
                };
                if (scope.choice.children.length > 0) {
                    var childChoice = $compile('<choice-tree ng-model="choice.children"></choice-tree>')(scope)
                    elm.append(childChoice);
                }
            }
        };
    }]);
    app.directive('bDatepicker', [function () {
        return {
            restrict: 'A',
            link: function (scope, el, attr) {
                el.datepicker();
            }
        };
    }]);
    app.directive('cellHighlight', [function () {
        return {
            restrict: 'C',
            link: function postLink(scope, iElement, iAttrs) {
                iElement.find('i').mouseover(function () {
                    $(this).parent('li').css('opacity', '0.7');
                }).mouseout(function () {
                    $(this).parent('li').css('opacity', '1.0');
                    $(this).parent('li').removeClass('open');
                });
            }
        };
    }]);
    app.directive('context', [function () {
        return {
            restrict: 'A',
            compile: function compile(tElement, tAttrs, transclude, scope) {
                return {
                    post: function postLink(scope, iElement, iAttrs, controller) {
                        var ul = $('#' + iAttrs.context),
							last = null;
                        ul.css({
                            'display': 'none'
                        });
                        $(iElement).click(function (event) {
                            event.stopPropagation();
                            var top = event.clientY + 10;
                            if ((ul.height() + top + 14) > $(window).height()) {
                                top = top - (ul.height() + 50);
                            } else { }
                            ul.css({
                                position: "fixed",
                                display: "block",
                                left: event.clientX - 220 + 'px',
                                top: top + 'px',
                                zIndex: 999999
                            });
                            var target = $(event.target);
                            var menuOption = target.attr('data-role');
                            if (menuOption == 'TaskMenuOption') {
                                var taskStatus = target.attr('data-taskstatus');
                                scope.OrganizeTaskMenuOption(parseInt(taskStatus));
                                scope.ShowHideSettoComplete(parseInt(target.attr('data-tasktypeid')), parseInt(target.attr('data-taskstatus')));
                                scope.contextTaskID = target.attr('data-taskid');
                                scope.SetContextTaskID(scope.contextTaskID);
                                scope.ContextTaskListID = target.attr('data-tasklist');
                                scope.SetContextTaskListID(scope.ContextTaskListID);
                                scope.ContextTaskEntityID = parseInt(target.attr('data-entityid'));
                                scope.SetContextTaskListEntityID(scope.ContextTaskEntityID);
                            } else if (menuOption == 'TaskListMenuOption') {
                                scope.ContextTaskListID = target.attr('data-menuid');
                                scope.ContextTaskEntityID = parseInt(target.attr('data-entityid'));
                                scope.SetContextTaskListID(scope.ContextTaskListID);
                                scope.SetContextTaskListEntityID(scope.ContextTaskEntityID);
                            } else if (menuOption == 'TaskFlagOption') {
                                scope.ContextTaskListID = target.attr('data-tasklist');
                                scope.SetContextTaskListID(scope.ContextTaskListID);
                                scope.contextTaskID = target.attr('data-taskid');
                                scope.SetContextTaskID(scope.contextTaskID);
                            } else if (menuOption == 'TaskDetailsMember') {
                                scope.ContextMemberID = target.attr('data-id');
                                scope.SetContextMemberID(scope.ContextMemberID);
                            } else if (menuOption == 'TaskFileDetail') {
                                scope.contextFileID = target.attr('data-FileID');
                                scope.SetContextFileID(scope.contextFileID);
                            } else if (menuOption == 'TaskChecklistOptions') {
                                scope.contextCheckListID = target.attr('data-id');
                                scope.contextCheckListIndex = target.attr('data-indexval');
                                scope.SetcontextCheckListID(target.attr('data-id'), target.attr('data-indexval'));
                            } else if (menuOption == 'tasklistExportMenuholder') { } else if (menuOption == 'TaskFileAttachmentMenuOption') {
                                var top = event.clientY + 10;
                                if ((ul.height() + top + 14) > $(window).height()) {
                                    top = top - (ul.height() + 40);
                                } else { }
                                ul.css({
                                    position: "fixed",
                                    display: "block",
                                    left: event.clientX + 10 + 'px',
                                    top: top + 'px',
                                    zIndex: 999999
                                });
                            }
                            last = event.timeStamp;
                            if (event.stopPropagation) event.stopPropagation();
                            if (event.preventDefault) event.preventDefault();
                            event.cancelBubble = true;
                            event.returnValue = false;
                        });
                        $(document).click(function (event) {
                            var target = $(event.target);
                            if (!target.is(".popover") && !target.parents().is(".popover")) {
                                if (last === event.timeStamp) return;
                                ul.css({
                                    'display': 'none'
                                });
                            }
                        });
                        $(window).scroll(function (event) {
                            var target = $(event.target);
                            if (!target.is(".popover") && !target.parents().is(".popover")) {
                                if (last === event.timeStamp) return;
                                ul.css({
                                    'display': 'none'
                                });
                            }
                        });
                    }
                };
            }
        };
    }]);
    app.directive('contextmenuoptions', [function () {
        return {
            restrict: 'A',
            compile: function compile(tElement, tAttrs, transclude, scope) {
                return {
                    post: function postLink(scope, iElement, iAttrs, controller) {
                        var ul = $('#' + iAttrs.contextmenuoptions),
							last = null;
                        ul.css({
                            'display': 'none'
                        });
                        $(iElement).click(function (event) {
                            event.stopPropagation();
                            var top = event.clientY + 10;
                            if ((ul.height() + top + 14) > $(window).height()) {
                                top = top - (ul.height() + 50);
                            } else { }
                            if (top < 0) {
                                ul.css({
                                    position: "fixed",
                                    display: "block",
                                    left: event.clientX - 220 + 'px',
                                    top: 10 + 'px',
                                    zIndex: 999999
                                });
                            } else {
                                ul.css({
                                    position: "fixed",
                                    display: "block",
                                    left: event.clientX - 220 + 'px',
                                    top: (top) + 'px',
                                    zIndex: 999999
                                });
                            }
                            var target = $(event.target);
                            last = event.timeStamp;
                            if (event.stopPropagation) event.stopPropagation();
                            if (event.preventDefault) event.preventDefault();
                            event.cancelBubble = true;
                            event.returnValue = false;
                        });
                        $(document).click(function (event) {
                            var target = $(event.target);
                            if (!target.is(".popover") && !target.parents().is(".popover")) {
                                if (last === event.timeStamp) return;
                                ul.css({
                                    'display': 'none'
                                });
                            }
                        });
                        $(window).scroll(function (event) {
                            var target = $(event.target);
                            if (!target.is(".popover") && !target.parents().is(".popover")) {
                                if (last === event.timeStamp) return;
                                ul.css({
                                    'display': 'none'
                                });
                            }
                        });
                    }
                };
            }
        };
    }]);
    app.directive('contextmenuoptions', [function () {
        return {
            restrict: 'A',
            compile: function compile(tElement, tAttrs, transclude, scope) {
                return {
                    post: function postLink(scope, iElement, iAttrs, controller) {
                        var ul = $('#' + iAttrs.contextmenuoptions),
							last = null;
                        ul.css({
                            'display': 'none'
                        });
                        $(iElement).click(function (event) {
                            event.stopPropagation();
                            var top = event.clientY + 10;
                            if ((ul.height() + top + 14) > $(window).height()) {
                                top = top - (ul.height() + 50);
                            } else { }
                            if (top < 0) {
                                ul.css({
                                    position: "fixed",
                                    display: "block",
                                    left: event.clientX - 220 + 'px',
                                    top: 10 + 'px',
                                    zIndex: 999999
                                });
                            } else {
                                ul.css({
                                    position: "fixed",
                                    display: "block",
                                    left: event.clientX - 220 + 'px',
                                    top: (top) + 'px',
                                    zIndex: 999999
                                });
                            }
                            var target = $(event.target);
                            last = event.timeStamp;
                            if (event.stopPropagation) event.stopPropagation();
                            if (event.preventDefault) event.preventDefault();
                            event.cancelBubble = true;
                            event.returnValue = false;
                        });
                        $(document).click(function (event) {
                            var target = $(event.target);
                            if (!target.is(".popover") && !target.parents().is(".popover")) {
                                if (last === event.timeStamp) return;
                                ul.css({
                                    'display': 'none'
                                });
                            }
                        });
                        $(window).scroll(function (event) {
                            var target = $(event.target);
                            if (!target.is(".popover") && !target.parents().is(".popover")) {
                                if (last === event.timeStamp) return;
                                ul.css({
                                    'display': 'none'
                                });
                            }
                        });
                    }
                };
            }
        };
    }]);
    app.directive("myQtip2", ['$compile', '$parse', function ($compile, $parse) {
        return function (scope, element, attrs) {
            scope.qtipSkin = (attrs.skin ? "qtip-" + attrs.skin : "qtip-dark qtip-desc");
            scope.closeButton = (attrs.closeButton == null ? false : true)
            scope.qtipPointerPos = "top center";
            scope.qtipContentPos = "top center";
            $(element).qtip({
                content: {
                    text: function (api) {
                        return ($(this).attr('qtip-content') != "") ? $(this).attr('qtip-content') : "-";
                    },
                    button: scope.closeButton
                },
                style: {
                    classes: scope.qtipSkin + " qtip-shadow qtip-rounded qtip-desc",
                    title: {
                        'display': 'none'
                    }
                },
                show: {
                    event: 'mouseenter click unfocus',
                    solo: true
                },
                events: {
                    hide: function (event, api) {
						event: 'unfocus click mouseleave'
                    }
                },
                hide: {
                    delay: 0,
                    fixed: false,
                    effect: function () {
                        $(this).fadeOut(0);
                    },
                    event: 'unfocus click mouseleave'
                },
                position: {
                    my: 'bottom center',
                    at: 'top center',
                    viewport: $(window),
                    adjust: {
                        method: 'shift',
                        mouse: true
                    },
                    corner: {
                        target: 'bottom center',
                        tooltip: 'bottom center',
                        mimic: 'top'
                    },
                    target: 'mouse'
                }
            });
            scope.$on("$destroy", function () {
                $(element).qtip('destroy', true);
                $(window).off("resize.Viewport");
            });
        }
    }]);
    app.directive("myQtipt2b", ['$compile', '$parse', function ($compile, $parse) {
        return function (scope, element, attrs) {
            scope.qtipSkin = (attrs.skin ? "qtip-" + attrs.skin : "qtip-dark qtip-desc margin-top10x");
            scope.closeButton = (attrs.closeButton == null ? false : true)
            scope.qtipPointerPos = "bottom center";
            scope.qtipContentPos = "bottom center";
            $(element).qtip({
                content: {
                    text: function (api) {
                        return ($(this).attr('qtip-content') != "") ? $(this).attr('qtip-content') : "-";
                    },
                    button: scope.closeButton
                },
                style: {
                    classes: scope.qtipSkin + " qtip-shadow qtip-rounded qtip-desc",
                    title: {
                        'display': 'none'
                    }
                },
                show: {
                    event: 'mouseenter click unfocus',
                    solo: true
                },
                events: {
                    hide: function (event, api) {
						event: 'unfocus click mouseleave'
                    }
                },
                hide: {
                    delay: 0,
                    fixed: false,
                    effect: function () {
                        $(this).fadeOut(0);
                    },
                    event: 'unfocus click mouseleave'
                },
                position: {
                    my: 'top center',
                    at: 'bottom center',
                    viewport: $(window),
                    adjust: {
                        method: 'shift',
                        mouse: true
                    },
                    corner: {
                        target: 'top center',
                        tooltip: 'top center',
                        mimic: 'bottom'
                    },
                    target: 'mouse'
                }
            });
            scope.$on("$destroy", function () {
                $(element).qtip('destroy', true);
                $(window).off("resize.Viewport");
            });
        }
    }]);
    app.directive("bsQtipStatic", ['$compile', '$parse', function ($compile, $parse) {
        return function (scope, element, attrs) {
            scope.qtipSkin = (attrs.skin ? "qtip-" + attrs.skin : "qtip-dark");
            scope.closeButton = (attrs.closeButton == null ? false : true)
            scope.qtipPointerPos = "top center";
            scope.qtipContentPos = "top center";
            $(element).qtip({
                content: {
                    text: function (api) {
                        return $(this).attr('qtip-content') == null ? '<div><img position="right" style="width:15px; margin-right:5px;" src="assets/img/loading.gif"> Loading..</div>' : $(this).attr('qtip-content');
                    },
                    button: scope.closeButton
                },
                style: {
                    classes: scope.qtipSkin + " qtip-shadow qtip-rounded",
                    title: {
                        'display': 'none'
                    }
                },
                show: {
                    event: 'mouseenter click unfocus',
                    solo: true
                },
                events: {
                    hide: function (event, api) {
						event: 'unfocus click mouseleave'
                    }
                },
                hide: {
                    delay: 0,
                    fixed: false,
                    effect: function () {
                        $(this).fadeOut(0);
                    },
                    event: 'unfocus click mouseleave'
                },
                position: {
                    my: 'bottom center',
                    at: 'top center',
                    viewport: $(window),
                    adjust: {
                        method: 'shift',
                        mouse: true
                    },
                    corner: {
                        target: 'bottom center',
                        tooltip: 'bottom center',
                        mimic: 'top'
                    },
                    target: 'mouse'
                }
            });
            scope.$on("$destroy", function () {
                $(element).qtip('destroy', true);
                $(window).off("resize.Viewport");
            });
        }
    }]);
    app.directive("bsQtip2Dynamic", ['$compile', '$parse', function ($compile, $parse) {
        return function (scope, element, attrs) {
            scope.qtipSkin = (attrs.skin ? "qtip-" + attrs.skin : "qtip-dark");
            scope.closeButton = (attrs.closeButton == null ? false : true)
            scope.qtipPointerPos = "top center";
            scope.qtipContentPos = "top center";
            var attrslst = attrs;
            $(element).qtip({
                content: {
                    text: '<div><img position="right" style="width:15px; margin-right:5px;" src="assets/img/loading.gif"> Loading..</div>',
                },
                events: {
                    show: function (event, api) {
                        var qtip_id = api.elements.content.attr('id'),
							entityid = attrslst.entityid,
							metadatatype = attrslst.metadatatype,
							costcenterid = attrslst.costcenterid,
							placeholder = attrslst.resultplace;
                        if (parseInt(metadatatype) == 1) scope.loadtQtipContent(qtip_id, entityid, costcenterid, placeholder);
                        else scope.loadtQtipSpentContent(qtip_id, entityid, costcenterid, placeholder);
                    }
                },
                style: {
                    classes: scope.qtipSkin + " qtip-shadow qtip-rounded",
                    title: {
                        'display': 'none'
                    }
                },
                show: {
                    event: 'mouseenter click unfocus',
                    solo: true
                },
                hide: {
                    delay: 0,
                    fixed: false,
                    effect: function () {
                        $(this).fadeOut(0);
                    },
                    event: 'unfocus click mouseleave'
                },
                position: {
                    my: 'top right',
                    at: 'top left',
                    viewport: $(window),
                    corner: {
                        target: 'top right',
                        tooltip: 'top right',
                        mimic: 'top'
                    },
                    target: 'mouse'
                }
            });
            scope.$on("$destroy", function () {
                $(element).qtip('destroy', true);
                $(window).off("resize.Viewport");
            });
        }
    }]);
    app.directive('multiselectDropdown', [function () {
        return function (scope, element, attributes) {
            element = $(element[0]);
            element.multiselect({
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                includeSelectAllOption: false,
                dropRight: true
            });
            scope.$watch(function () {
                return element[0].length;
            }, function () {
                element.multiselect('rebuild');
            });
            scope.$watchCollection(attributes.ngModel, function () {
                element.multiselect('refresh');
            });
            scope.$on("$destroy", function () {
                $(window).off("resize.Viewport");
            });
        }
    }]);
    app.directive('multiselectpartial', [function () {
        return function (scope, element, attributes) {
            element = $(element[0]);
            element.multiselectpartial({});
            scope.$watch(function () {
                return element[0].length;
            }, function () {
                element.multiselectpartial('rebuild');
            });
            scope.$watch(attributes.ngModel, function () {
                element.multiselectpartial('refresh');
            });
            scope.$on("$destroy", function () {
                $(window).off("resize.Viewport");
            });
        }
    }]);
    app.filter('unique', [function () {
        return function (input, key) {
            var unique = {};
            var uniqueList = [];
            for (var i = 0; i < input.length; i++) {
                if (typeof unique[input[i][key]] == "undefined") {
                    unique[input[i][key]] = "";
                    uniqueList.push(input[i]);
                }
            }
            return uniqueList;
        };
    }]);
    app.filter('AvoidAssetApprovalTask', function () {
        return function (items) {
            var filtered = [];
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                if (item.TaskTypeId != 32 && item.TaskTypeId != 36) {
                    filtered.push(item);
                }
            }
            return filtered;
        };
    });
    app.service('sharedProperties', [function () {
        var stringValue = 'test string value';
        var objectValue = {
            data: 'test object value'
        };
        var itemID = 0;
        return {
            getString: function () {
                return stringValue;
            },
            getID: function () {
                return itemID;
            },
            setString: function (value, id) {
                stringValue = value;
                itemID = id;
            },
            getObject: function () {
                return objectValue;
            }
        }
    }]);
    app.directive('multiselect', ['$parse', '$document', '$compile', 'optionParser', function ($parse, $document, $compile, optionParser) {
        return {
            restrict: 'E',
            require: 'ngModel',
            link: function (originalScope, element, attrs, modelCtrl) {
                var exp = attrs.options,
					parsedResult = optionParser.parse(exp),
					isMultiple = attrs.multiple ? true : false,
					required = false,
					scope = originalScope.$new(),
					changeHandler = attrs.change || anguler.noop;
                scope.items = [];
                scope.header = '';
                scope.multiple = isMultiple;
                scope.disabled = false;
                originalScope.$on('$destroy', function () {
                    scope.$destroy();
                });
                var popUpEl = angular.element('<multiselect-popup></multiselect-popup>');
                if (attrs.required || attrs.ngRequired) {
                    required = true;
                }
                attrs.$observe('required', function (newVal) {
                    required = newVal;
                });
                scope.$watch(function () {
                    return $parse(attrs.disabled)(originalScope);
                }, function (newVal) {
                    scope.disabled = newVal;
                });
                scope.$watch(function () {
                    return $parse(attrs.multiple)(originalScope);
                }, function (newVal) {
                    isMultiple = newVal || false;
                });
                scope.$watch(function () {
                    return parsedResult.source(originalScope);
                }, function (newVal) {
                    if (angular.isDefined(newVal)) parseModel();
                }, true);
                scope.$watch(function () {
                    return modelCtrl.$modelValue;
                }, function (newVal, oldVal) {
                    if (angular.isDefined(newVal)) {
                        markChecked(newVal);
                        scope.$eval(changeHandler);
                    }
                    getHeaderText();
                    modelCtrl.$setValidity('required', scope.valid());
                }, true);

                function parseModel() {
                    scope.items.length = 0;
                    var model = parsedResult.source(originalScope);
                    if (!angular.isDefined(model)) return;
                    for (var i = 0; i < model.length; i++) {
                        var local = {};
                        local[parsedResult.itemName] = model[i];
                        scope.items.push({
                            label: parsedResult.viewMapper(local),
                            model: model[i],
                            checked: false
                        });
                    }
                }
                parseModel();
                element.append($compile(popUpEl)(scope));

                function getHeaderText() {
                    if (is_empty(modelCtrl.$modelValue)) return scope.header = '';
                    if (isMultiple) {
                        scope.header = modelCtrl.$modelValue.length + ' ' + 'selected';
                    } else {
                        var local = {};
                        local[parsedResult.itemName] = modelCtrl.$modelValue;
                        scope.header = parsedResult.viewMapper(local);
                    }
                }

                function is_empty(obj) {
                    if (!obj) return true;
                    if (obj.length && obj.length > 0) return false;
                    for (var prop in obj) if (obj[prop]) return false;
                    return true;
                };
                scope.valid = function validModel() {
                    if (!required) return true;
                    var value = modelCtrl.$modelValue;
                    return (angular.isArray(value) && value.length > 0) || (!angular.isArray(value) && value != null);
                };

                function selectSingle(item) {
                    if (item.checked) {
                        scope.uncheckAll();
                    } else {
                        scope.uncheckAll();
                        item.checked = !item.checked;
                    }
                    setModelValue(false);
                }

                function selectMultiple(item) {
                    item.checked = !item.checked;
                    setModelValue(true);
                }

                function setModelValue(isMultiple) {
                    var value;
                    if (isMultiple) {
                        value = [];
                        angular.forEach(scope.items, function (item) {
                            if (item.checked) value.push(item.model);
                        })
                    } else {
                        angular.forEach(scope.items, function (item) {
                            if (item.checked) {
                                value = item.model;
                                return false;
                            }
                        })
                    }
                    modelCtrl.$setViewValue(value);
                }

                function markChecked(newVal) {
                    if (!angular.isArray(newVal)) {
                        angular.forEach(scope.items, function (item) {
                            if (angular.equals(item.model, newVal)) {
                                item.checked = true;
                                return false;
                            }
                        });
                    } else {
                        angular.forEach(newVal, function (i) {
                            angular.forEach(scope.items, function (item) {
                                if (angular.equals(item.model, i)) {
                                    item.checked = true;
                                }
                            });
                        });
                    }
                }
                scope.checkAll = function () {
                    if (!isMultiple) return;
                    angular.forEach(scope.items, function (item) {
                        item.checked = true;
                    });
                    setModelValue(true);
                };
                scope.uncheckAll = function () {
                    angular.forEach(scope.items, function (item) {
                        item.checked = false;
                    });
                    setModelValue(true);
                };
                scope.$watch('clearscope', function () {
                    angular.forEach(scope.items, function (item) {
                        item.checked = false;
                    });
                    setModelValue(true);
                });
                scope.select = function (item) {
                    if (isMultiple === false) {
                        selectSingle(item);
                        scope.toggleSelect();
                    } else {
                        selectMultiple(item);
                    }
                }
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }])
    app.directive('multiselectPopup', ['$document', function ($document) {
        return {
            restrict: 'E',
            scope: false,
            replace: true,
            templateUrl: 'views/mui/multiselect.tmpl.html',
            link: function (scope, element, attrs) {
                scope.isVisible = false;
                scope.toggleSelect = function () {
                    if (element.hasClass('open')) {
                        element.removeClass('open');
                        $document.unbind('click', clickHandler);
                    } else {
                        element.addClass('open');
                        $document.bind('click', clickHandler);
                        scope.focus();
                    }
                };

                function clickHandler(event) {
                    if (elementMatchesAnyInArray(event.target, element.find(event.target.tagName))) return;
                    element.removeClass('open');
                    $document.unbind('click', clickHandler);
                    scope.$apply();
                }
                scope.focus = function focus() {
                    var searchBox = element.find('input')[0];
                    searchBox.focus();
                }
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
                var elementMatchesAnyInArray = function (element, elementArray) {
                    for (var i = 0; i < elementArray.length; i++)
                        if (element == elementArray[i]) return true;
                    return false;
                }
            }
        }
    }]);
    app.factory('optionParser', ['$parse', function ($parse) {
        var TYPEAHEAD_REGEXP = /^\s*(.*?)(?:\s+as\s+(.*?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+(.*)$/;
        return {
            parse: function (input) {
                var match = input.match(TYPEAHEAD_REGEXP),
					modelMapper, viewMapper, source;
                if (!match) {
                    throw new Error("Expected typeahead specification in form of '_modelValue_ (as _label_)? for _item_ in _collection_'" + " but got '" + input + "'.");
                }
                return {
                    itemName: match[3],
                    source: $parse(match[4]),
                    viewMapper: $parse(match[2] || match[1]),
                    modelMapper: $parse(match[1])
                };
            }
        };
    }])
    app.directive('xeditableobjective', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var CurrentEntityID = parseInt(attrs.entityid, 10);
                var attributeName = attrs.attributename;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        display: function (value, srcData) {
                            ngModel.$setViewValue(value);
                            element.text(value);
                            scope.$apply();
                        }
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('save', function (e, params) {
                    if (attrs.objectiveedittype == 111) {
                        if (scope.ObjectiveDescription == undefined) {
                            scope.SaveObjectiveDeatils(attrs.objectiveid, $('<div />').html(attrs.objectivename).text(), attrs.objectiveedittype, $('<div />').html(attrs.objectivedescripton).text(), params.newValue);
                        } else {
                            scope.SaveObjectiveDeatils(attrs.objectiveid, $('<div />').html(attrs.objectivename).text(), attrs.objectiveedittype, scope.ObjectiveDescription, params.newValue);
                        }
                    } else {
                        if (scope.ObjectiveName == undefined) {
                            scope.SaveObjectiveDeatils(attrs.objectiveid, $('<div />').html(attrs.objectivename).text(), attrs.objectiveedittype, $('<div />').html(attrs.objectivedescripton).text(), params.newValue);
                        } else {
                            scope.SaveObjectiveDeatils(attrs.objectiveid, scope.ObjectiveName, attrs.objectiveedittype, $('<div />').html(attrs.objectivedescripton).text(), params.newValue);
                        }
                    }
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditablecalender', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var CurrentEntityID = parseInt(attrs.entityid, 10);
                var attributeName = attrs.attributename;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        display: function (value, srcData) {
                            ngModel.$setViewValue(value);
                            element.text(value);
                            scope.$apply();
                        }
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('save', function (e, params) {
                    if (attrs.calenderedittype == 111) {
                        if (scope.CalenderDescription == undefined) {
                            scope.SaveCalenderDeatils(attrs.calenderid, $('<div />').html(attrs.calendername).text(), attrs.calenderedittype, $('<div />').html(attrs.calenderdescripton).text(), params.newValue);
                        } else {
                            scope.SaveCalenderDeatils(attrs.calenderid, $('<div />').html(attrs.calendername).text(), attrs.calenderedittype, scope.CalenderDescription, params.newValue);
                        }
                    } else {
                        if (scope.CalenderName == undefined) {
                            scope.SaveCalenderDeatils(attrs.calenderid, $('<div />').html(attrs.calendername).text(), attrs.calenderedittype, $('<div />').html(attrs.calenderdescripton).text(), params.newValue);
                        } else {
                            scope.SaveCalenderDeatils(attrs.calenderid, scope.CalenderName, attrs.calenderedittype, $('<div />').html(attrs.calenderdescripton).text(), params.newValue);
                        }
                    }
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditableobjectiveownerlist', ['$timeout', '$compile', '$resource', '$translate', function ($timeout, $compile, $resource, $translate) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var Version = 1;
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var isDataTypeExists = false;
                for (var variable in $.fn.editabletypes) {
                    if (attributeName == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(attributeName, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopup = '';
                    inlinePopup += '<div><label><select ui-select2 data-ng-model="xeditobjectiveowner" name="city" class="ngcompileDropdown">';
                    inlinePopup += '<option ng-repeat="ndata in ObjectiveOwnerList" value=\"{{ndata.UserId}}\">{{ndata.UserName}}</option></select></label></div>';
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopup,
                        inputclass: ''
                    });
                    $.fn.editabletypes[attributeName] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompileDropdown'))(scope);
                            scope['xeditobjectiveowner'] = ($.grep(scope.ObjectiveOwnerList, function (e) {
                                return e.UserName == scope.ObjectiveFulfillDeatils.OwnerName
                            }))[0].UserId;
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    if (scope['xeditobjectiveowner'] != undefined) {
                        var tempvar = ($.grep(scope.ObjectiveOwnerList, function (e) {
                            return parseInt(e.UserId, 10) == parseInt(scope['xeditobjectiveowner'], 10)
                        }))[0];
                        scope.ObjectiveOwnerName = tempvar.UserName;
                        scope.objectiveRoleId = tempvar.RoleId;
                        scope.ObjectiveOwnerId = tempvar.UserId;
                    } else {
                        scope.CurrentWorkflowStepName = '-';
                        $scope.SelectedWorkflowStep = 0;
                        scope.WorkFlowStepID = 0;
                        scope.$apply();
                    }
                    scope.UpdateObjectiveOwner(scope.ObjectiveOwnerId, scope.objectiveRoleId, scope.ObjectiveOwnerName);
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditablecalenderownerlist', ['$timeout', '$compile', '$resource', '$translate', function ($timeout, $compile, $resource, $translate) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var Version = 1;
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var isDataTypeExists = false;
                for (var variable in $.fn.editabletypes) {
                    if (attributeName == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(attributeName, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopup = '';
                    inlinePopup += '<div><label><select ui-select2 data-ng-model="xeditcalenderowner" name="city" class="ngcompileDropdown">';
                    inlinePopup += '<option ng-repeat="ndata in CalenderOwnerList" value=\"{{ndata.UserId}}\">{{ndata.UserName}}</option></select></label></div>';
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopup,
                        inputclass: ''
                    });
                    $.fn.editabletypes[attributeName] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompileDropdown'))(scope);
                            scope['xeditcalenderowner'] = ($.grep(scope.CalenderOwnerList, function (e) {
                                return e.UserName == scope.CalenderFulfillDeatils.OwnerName
                            }))[0].UserId;
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    if (scope['xeditcalenderowner'] != undefined) {
                        var tempvar = ($.grep(scope.CalenderOwnerList, function (e) {
                            return parseInt(e.UserId, 10) == parseInt(scope['xeditcalenderowner'], 10)
                        }))[0];
                        scope.CalenderOwnerName = tempvar.UserName;
                        scope.calenderRoleId = tempvar.RoleId;
                        scope.CalenderOwnerId = tempvar.UserId;
                    } else {
                        scope.CurrentWorkflowStepName = '-';
                        $scope.SelectedWorkflowStep = 0;
                        scope.WorkFlowStepID = 0;
                        scope.$apply();
                    }
                    scope.UpdateCalenderOwner(scope.CalenderOwnerId, scope.calenderRoleId, scope.CalenderOwnerName);
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditableobjectivestatus', ['$timeout', '$compile', '$resource', '$translate', function ($timeout, $compile, $resource, $translate) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var Version = 1;
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var isDataTypeExists = false;
                for (var variable in $.fn.editabletypes) {
                    if (attributeName == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(attributeName, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopup = '';
                    inlinePopup += '<div><label><select ui-select2 data-ng-model="xeditobjectivestatus" name="city" class="ngcompileDropdown">';
                    inlinePopup += '<option ng-repeat="ndata in ObjectivestatusList" value=\"{{ndata.StatusId}}\">{{ndata.StatusDescription}}</option></select></label></div>';
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopup,
                        inputclass: ''
                    });
                    $.fn.editabletypes[attributeName] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompileDropdown'))(scope);
                            scope['xeditobjectivestatus'] = ($.grep(scope.ObjectivestatusList, function (e) {
                                return e.StatusDescription == scope.ObjectiveStatus
                            }))[0].StatusId;
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    var objSatusId = 0;
                    if (scope['xeditobjectivestatus'] != undefined) {
                        scope.ObjectiveStatus = ($.grep(scope.ObjectivestatusList, function (e) {
                            return parseInt(e.StatusId, 10) == parseInt(scope['xeditobjectivestatus'], 10)
                        }))[0].StatusDescription;
                        objSatusId = scope['xeditobjectivestatus'];
                    }
                    scope.UpdateObjectivestatus(objSatusId);
                });
                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);
    app.directive('xeditablecmspublishdate', ['$timeout', '$compile', '$resource', '$translate', '$window', function ($timeout, $compile, $resource, $translate, $window) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var attributeName = attrs.attributename + attrs.primaryid;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var datetimeid = attrs.datetimeId;
                var attrID = attrs.primaryid;
                var isDataTypeExists = false;
                for (var variable in $.fn.editabletypes) {
                    if (attributeName == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(attributeName, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopupDesign1 = '';
                    inlinePopupDesign1 += '<div class="row-fluid"><div class="inputHolder">';
                    inlinePopupDesign1 += '     <div class="ngcompile control-group">';
                    inlinePopupDesign1 += '         <label class="control-label">Published Date</label>';
                    inlinePopupDesign1 += '         <div class="controls">';
                    inlinePopupDesign1 += '               <input placeholder="-- Date Time --" id="' + datetimeid + '" data-ng-model="Publishfields.PublishDate_Dir" type="text"  data-date-format="' + Defaultdate + '"  required="" name="startDate" ng-click="Calanderopen($event,calanderopened)"  datepicker-popup="{{format}}"  is-open="calanderopened" min-date="minDate" max-date="maxDate" datepicker-options="dateOptions" date-disabled="disabled(date, mode)"  close-text="Close" class="ng-pristine ng-invalid ng-invalid-required">';
                    inlinePopupDesign1 += '     </div></div>';
                    inlinePopupDesign1 += '     <div class="control-group">';
                    inlinePopupDesign1 += '         <label class="control-label">Published Time</label>';
                    inlinePopupDesign1 += '         <div class="controls">';
                    inlinePopupDesign1 += '             <select ui-select2 data-ng-model="Publishfields.PublishTime_Dir" name="city" class="ngcompile"><option ng-repeat=\"pdata in PublishedTime\" value=\"{{pdata.name}}\">{{pdata.name}}</option></select></label>';
                    inlinePopupDesign1 += ' </div></div></div></div>';
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopupDesign1,
                        inputclass: ''
                    });
                    $.fn.editabletypes[attributeName] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompile'))(scope);
                            scope.Publishfields["PublishDate_Dir"] = scope.Publishfields["CmsPagePublishDate"];
                            scope.Publishfields["PublishTime_Dir"] = scope.Publishfields["CmsPagePublishTime"];
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    if (typeof scope.Publishfields["PublishDate_Dir"] != "string") {
                        var datstartval = "";
                        datstartval = new Date(scope.Publishfields["PublishDate_Dir"].toString());
                        if (datstartval == 'NaN-NaN-NaN') {
                            scope.Publishfields["PublishDate_Dir"] = ConvertDateToString(ConvertStringToDateByFormat(scope.Publishfields["PublishDate_Dir"].toString(), GlobalUserDateFormat))
                        } else scope.Publishfields["PublishDate_Dir"] = ConvertDateToString(datstartval);
                    }
                    scope.saveCmsEntityDetailsValue("PublishDateTime", CurrentEntityID, scope.Publishfields["PublishDate_Dir"] + " @ " + scope.Publishfields["PublishTime_Dir"]);
                });
            }
        };
    }]);
    app.directive('xeditablecmsrevisions', ['$timeout', '$compile', '$resource', '$translate', '$window', function ($timeout, $compile, $resource, $translate, $window) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var attributeName = attrs.attributename + attrs.primaryid;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var datetimeid = attrs.datetimeId;
                var attrID = attrs.primaryid;
                var isDataTypeExists = false;
                for (var variable in $.fn.editabletypes) {
                    if (attributeName == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(attributeName, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopupDesign1 = '';
                    inlinePopupDesign1 += '<div><label>Revisions <select ui-select2 data-ng-model="Publishfields.Revisions_Dir" name="city" class="ngcompile"><option ng-repeat=\"pdata in Publishfields.Revisions\" value=\"{{pdata.ContentVersionID}}\">Created On: {{pdata.CreatedOn}}</option></select></label></div></div></div>';
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopupDesign1,
                        inputclass: ''
                    });
                    $.fn.editabletypes[attributeName] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompile'))(scope);
                            scope.Publishfields["Revisions_Dir"] = scope.RevisionID;
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    scope.saveCmsEntityDetailsValue("Revisions", CurrentEntityID, scope.Publishfields["Revisions_Dir"].toString());
                });
            }
        };
    }]);
    app.directive('xeditabletagwords', ['$timeout', '$compile', '$resource', '$translate', '$window', function ($timeout, $compile, $resource, $translate, $window) {
        return {
            restrict: 'A',
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var Version = 1;
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10)
                var isDataTypeExists = false;
                scope.normaltreeSources["TagWords_" + attrs.attributeid] = [];
                scope.normaltreeSources["TagWordsShowHide_" + attrs.attributeid] = false;
                var GetOptionListByID = ''
                var attrId = attrs.attributeid.split('_');
                attrId = attrId[0];
                for (var variable in $.fn.editabletypes) {
                    if (attributeName == variable) isDataTypeExists = true;
                }
                if (isDataTypeExists === false) {
                    var Address = function (options) {
                        this.init(attributeName, options, Address.defaults);
                    };
                    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                    var inlinePopup = '';
                    inlinePopup += '<div><div class="ngcompiletagwords loadIcon-small" ng-hide = "TagWordsShowHide_' + attrId + '" class="loadIcon-small width1halfx"></div> \
                                                <div name="tagwords" class="ngcompiletagwords"> \
                                                <directive-tagwords item-attrid = "' + attrs.attributeid + '" item-show-hide-progress = "TagWordsShowHide_' + attrId + '" item-tagword-id="TagWords_' + attrId + '" item-tagword-list="normaltreeSources.TagWords_' + attrId + '"></directive-tagwords class="ngcompiletagwords"></div> \
                                            </div>';
                    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                        tpl: inlinePopup,
                        inputclass: ''
                    });
                    $.fn.editabletypes[attributeName] = Address;
                }
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            scope.normaltreeSources["TagWordsShowHide_" + attrId]
                            $timeout(function () {
                                $compile($('.ngcompiletagwords'))(scope);
                                $('.ngcompiletagwords').parents(".popover-content").css("min-width", "286px").css("min-height", "120px");
                            }, 10);
                            var tagwrds = new Array();
                            for (var i = 0; i < scope.fields['TagWordsSeleted_' + attrId].length; i++) {
                                tagwrds[i] = scope.fields['TagWordsSeleted_' + attrId][i];
                            }
                            scope['TagWords_' + attrId] = tagwrds;
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    scope.treeSelection = [];
                    scope.fields['TagWordsSeleted_' + attrId] = [];
                    var dropdowntext = 'TagWords_' + attrId;
                    var tempcap = [];
                    if (scope['TagWords_' + attrId] != undefined && scope['TagWords_' + attrId].length > 0) {
                        var tempvar = [];
                        scope.treeSelection = scope['TagWords_' + attrId];
                        scope.fields['TagWordsSeleted_' + attrId] = scope['TagWords_' + attrId];
                        for (var j = 0; j < scope['TagWords_' + attrId].length; j++) {
                            tempcap.push($.grep(scope.normaltreeSources["TagWords_" + attrId], function (e) {
                                return e.Id == scope['TagWords_' + attrId][j];
                            })[0].Caption);
                        }
                        scope.fields['TagWordsCaption_' + attrId] = tempcap.join(',');
                    } else {
                        scope.treeSelection.push(0);
                        scope.fields['TagWordsCaption_' + attrId] = "-";
                        scope.fields['TagWordsSeleted_' + attrId] = [];
                    }
                    scope.saveDropdownTree(attrId, attrs.attributetypeid, CurrentEntityID, scope.treeSelection);
                });
            }
        };
    }]);
    app.directive("dirattributegrouplistview", ['$compile', '$parse', function ($compile, $parse) {
        return function (scope, element, attrs) {
            scope.qtipSkin = (attrs.skin ? "qtip-" + attrs.skin : "qtip-dark");
            scope.closeButton = (attrs.closeButton == null ? false : true)
            scope.qtipPointerPos = "top center";
            scope.qtipContentPos = "top center";
            var attrslst = attrs;
            $(element).qtip({
                content: {
                    text: '<div><img position="right" style="width:15px; margin-right:5px;" src="assets/img/loading.gif"> Loading..</div>',
                },
                events: {
                    show: function (event, api) {
                        var qtip_id = api.elements.content.attr('id');
                        var id = attrslst.id;
                        scope.LoadAttributeGroupToolTip(qtip_id, id);
                    }
                },
                style: {
                    classes: scope.qtipSkin + " qtip-shadow qtip-rounded",
                    title: {
                        'display': 'none'
                    }
                },
                show: {
                    event: 'mouseenter click unfocus',
                    solo: true
                },
                hide: {
                    delay: 0,
                    fixed: false,
                    effect: function () {
                        $(this).fadeOut(0);
                    },
                    event: 'unfocus click mouseleave'
                },
                position: {
                    my: 'top right',
                    at: 'top left',
                    viewport: $(window),
                    adjust: {
                        method: 'shift',
                        mouse: true
                    },
                    corner: {
                        target: 'top right',
                        tooltip: 'top right',
                        mimic: 'top'
                    },
                    target: 'mouse'
                }
            });
            scope.$on("$destroy", function () {
                $(element).qtip('destroy', true);
                $(window).off("resize.Viewport");
            });
        }
    }]);
    app.directive('costcentreTree', ['$timeout', function ($timeout) {
        return {
            restrict: 'E',
            template: "<ul  class=\"nav nav-list nav-pills nav-stacked abn-tree treesearchcls\">\n  <li ng-show=\"row.branch.isShow\" ng-repeat=\"row in tree_rows | filter:filterItem track by row.branch.uid\" ng-animate=\"'abn-tree-animate'\" ng-class=\"'level-' + {{ row.level }} + (row.branch.selected ? ' active':'')\" class=\"abn-tree-row treesearchcls\">\n   " + "<a  ng-hide=\"treeAccessable\" class=\"treesearchcls\"  ng-click=\"row.branch.ischecked = !row.branch.ischecked; user_clicks_branch_1(row.branch,$event)\">\n    <label  class=\"inlineBlock checkbox checkbox-custom treesearchcls\"> <input class=\"treesearchcls\"   type=\"checkbox\">  <i class=\"treesearchcls\"   ng-class=\"{'checkbox checked': row.branch.ischecked , 'checkbox': !row.branch.ischecked}\"/> </label> <span  class=\"indented tree-label treesearchcls\"  my-qtip2 qtip-content=\"{{row.Caption}}\">{{ row.Caption }} </span>\n    </a>\n" + "<a  ng-show=\"treeAccessable\"  class=\"treesearchcls\" ng-click=\"user_clicks_branch_Expand_Collapse(row.branch,$event)\">\n   <label  class=\"inlineBlock checkbox checkbox-custom treesearchcls\"> <input class=\"treesearchcls\"   type=\"checkbox\">  <i class=\"treesearchcls\"   ng-class=\"{'checkbox checked': row.branch.ischecked , 'checkbox': !row.branch.ischecked}\"/> </label> <span  class=\"indented tree-label treesearchcls\"  my-qtip2 qtip-content=\"{{row.Caption}}\">{{ row.Caption }} </span>\n    </a>\n" + "</div></li>\n</ul>",
            scope: {
                treeData: '=',
                treeFilter: '=',
                onSelect: '&',
                initialSelection: '@',
                accessable: '@',
                treeControl: '='
            },
            link: function (scope, element, attrs) {
                var error, expand_all_parents, expand_level, for_all_ancestors, for_each_branch, get_parent, n, on_treeData_change, select_branch, selected_branch, tree;
                error = function (s) {
                    console.log('ERROR:' + s);
                    return void 0;
                };
                if (attrs.iconExpand == null) {
                    attrs.iconExpand = 'icon-caret-right';
                }
                if (attrs.iconCollapse == null) {
                    attrs.iconCollapse = 'icon-caret-down';
                }
                if (attrs.iconLeaf == null) {
                    attrs.iconLeaf = 'icon-fixed-width icon-blank';
                }
                if (attrs.expandLevel == null) {
                    attrs.expandLevel = '100';
                }
                expand_level = parseInt(attrs.expandLevel, 10);
                if (!scope.treeData) {
                    console.log('no treeData defined for the tree!');
                    return;
                }
                if (scope.treeData.length == null) {
                    if (treeData.Caption != null) {
                        scope.treeData = [treeData];
                    } else {
                        console.log('treeData should be an array of root branches');
                        return;
                    }
                }
                for_each_branch = function (f) {
                    var do_f, root_branch, _i, _len, _ref, _results;
                    do_f = function (branch, level) {
                        var child, _i, _len, _ref, _results;
                        f(branch, level);
                        if (branch.Children != null) {
                            _ref = branch.Children;
                            _results = [];
                            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                                child = _ref[_i];
                                _results.push(do_f(child, level + 1));
                            }
                            return _results;
                        }
                    };
                    _ref = scope.treeData;
                    _results = [];
                    if (_ref != undefined) for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                        root_branch = _ref[_i];
                        _results.push(do_f(root_branch, 1));
                    }
                    return _results;
                };
                selected_branch = null;
                select_branch = function (branch, parentArr) {
                    if (!branch) {
                        if (selected_branch != null) {
                            selected_branch.selected = false;
                        }
                        selected_branch = null;
                        return;
                    }
                    if (branch !== undefined) {
                        if (selected_branch != null) {
                            selected_branch.selected = false;
                        }
                        branch.selected = true;
                        selected_branch = branch;
                        expand_all_parents(branch);
                        if (branch.onSelect != null) {
                            return $timeout(function () {
                                var test = [];
                                var parent;
                                parent = get_parent(branch);
                                if (parent != null) {
                                    test.push(parent);
                                    scope.parenttester = [];
                                    var ty = recursiveparent(parent);
                                    if (ty != undefined) {
                                        if (ty.length > 0) {
                                            for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                                test.push(obj);
                                            }
                                        }
                                    }
                                }
                                return branch.onSelect(branch, test);
                            });
                        } else {
                            if (scope.onSelect != null) {
                                return $timeout(function () {
                                    var test = [];
                                    var parent;
                                    parent = get_parent(branch);
                                    if (parent != null) {
                                        test.push(parent);
                                        scope.parenttester = [];
                                        var ty = recursiveparent(parent);
                                        if (ty != undefined) {
                                            if (ty.length > 0) {
                                                for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                                    test.push(obj);
                                                }
                                            }
                                        }
                                    }
                                    return scope.onSelect({
                                        branch: branch,
                                        parent: test
                                    });
                                });
                            }
                        }
                    }
                };
                scope.parenttester = [];
                scope.user_clicks_branch = function (branch) {
                    if (branch !== selected_branch) {
                        var test = [];
                        var parent;
                        parent = get_parent(branch);
                        if (parent != null) {
                            test.push(parent);
                            scope.parenttester = [];
                            var ty = recursiveparent(parent);
                            if (ty != undefined) {
                                if (ty.length > 0) {
                                    for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                        test.push(obj);
                                    }
                                }
                            }
                        }
                        return select_branch(branch, test);
                    }
                };
                scope.filterItem = function (item) {
                    if (!scope.treeFilter) {
                        item.isShow = true;
                        if (item.expanded != undefined) {
                            item.expanded = true;
                        }
                        return true;
                    }
                    var found = item.Caption.toLowerCase().indexOf(scope.treeFilter.toLowerCase()) != -1;
                    if (!found) {
                        var itemColl = item.branch != undefined ? item.branch.Children : item.Children;
                        angular.forEach(itemColl, function (item) {
                            var match = scope.filterItem(item);
                            if (match) {
                                found = true;
                                item.isShow = true;
                            }
                        });
                    }
                    return found;
                };

                function collapseOnemptySearch(item) {
                    var itemColl = item.branch != undefined ? item.branch.Children : item.Children;
                    angular.forEach(itemColl, function (item) {
                        item.isShow = true;
                    });
                }
                scope.user_clicks_branch_Expand_Collapse = function (branch, event) {
                    var test = [];
                    var parent;
                    parent = get_parent(branch);
                    var target = $(event.target);
                    if (target.hasClass("tree-icon")) {
                        for (var z = 0, childObj; childObj = branch.Children[z++];) {
                            childObj.isShow = branch.expanded;
                            Expandcollapse(childObj.Children, branch.expanded);
                        }
                        return false;
                    }
                };
                scope.user_clicks_branch_1 = function (branch, event) {
                    var test = [];
                    var parent;
                    parent = get_parent(branch);
                    var target = $(event.target);
                    if (target.hasClass("tree-icon")) {
                        branch.ischecked = !branch.ischecked
                        for (var z = 0, childObj; childObj = branch.Children[z++];) {
                            childObj.isShow = branch.expanded;
                            Expandcollapse(childObj.Children, branch.expanded);
                        }
                        return false;
                    } else {
                        branch.ischecked = branch.ischecked;
                        return select_branch(branch, test);
                    }
                };

                function Expandcollapse(Children, expanded) {
                    for (var z = 0, childObj; childObj = Children[z++];) {
                        childObj.isShow = expanded;
                        if (childObj.Children.length > 0) {
                            Expandcollapse(childObj.Children, expanded);
                        }
                    }
                }

                function recursiveparent(parent) {
                    var parent1;
                    parent1 = get_parent(parent);
                    if (parent1 != null && parent1 != undefined) {
                        scope.parenttester.push(parent1);
                        recursiveparent(parent1);
                    }
                    return scope.parenttester;
                }
                get_parent = function (child) {
                    var parent;
                    parent = void 0;
                    if (child.parent_uid) {
                        for_each_branch(function (b) {
                            if (b.uid === child.parent_uid) {
                                return parent = b;
                            }
                        });
                    }
                    return parent;
                };
                for_all_ancestors = function (child, fn) {
                    var parent;
                    parent = get_parent(child);
                    if (parent != null) {
                        fn(parent);
                        return for_all_ancestors(parent, fn);
                    }
                };
                expand_all_parents = function (child) {
                    return for_all_ancestors(child, function (b) {
                        return b.expanded = true;
                    });
                };
                scope.tree_rows = [];
                scope.treeAccessable = attrs.accessable != null ? (attrs.accessable == "false" ? false : true) : false;
                on_treeData_change = function () {
                    var add_branch_to_list, root_branch, _i, _len, _ref, _results;
                    for_each_branch(function (b, level) {
                        if (!b.uid) {
                            return b.uid = "" + Math.random();
                        }
                    });
                    for_each_branch(function (b) {
                        var child, _i, _len, _ref, _results;
                        if (angular.isArray(b.Children)) {
                            _ref = b.Children;
                            _results = [];
                            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                                child = _ref[_i];
                                _results.push(child.parent_uid = b.uid);
                            }
                            return _results;
                        }
                    });
                    scope.tree_rows = [];
                    for_each_branch(function (branch) {
                        var child, f;
                        if (branch.Children) {
                            if (branch.Children.length > 0) {
                                f = function (e) {
                                    if (typeof e === 'string') {
                                        return {
                                            Caption: e,
                                            Children: []
                                        };
                                    } else {
                                        return e;
                                    }
                                };
                                return branch.Children = (function () {
                                    var _i, _len, _ref, _results;
                                    _ref = branch.Children;
                                    _results = [];
                                    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                                        child = _ref[_i];
                                        _results.push(f(child));
                                    }
                                    return _results;
                                })();
                            }
                        } else {
                            return branch.Children = [];
                        }
                    });
                    add_branch_to_list = function (level, branch, visible) {
                        var child, child_visible, tree_icon, _i, _len, _ref, _results;
                        if (branch.expanded == null) {
                            branch.expanded = false;
                        }
                        if (!branch.Children || branch.Children.length === 0) {
                            tree_icon = attrs.iconLeaf;
                        } else {
                            if (branch.expanded) {
                                tree_icon = attrs.iconCollapse;
                            } else {
                                tree_icon = attrs.iconExpand;
                            }
                        }
                        scope.tree_rows.push({
                            level: level,
                            branch: branch,
                            Caption: branch.Caption,
                            tree_icon: tree_icon,
                            visible: visible,
                            ischecked: branch.ischecked != undefined ? branch.ischecked : false,
                            isShow: branch.isShow != undefined ? branch.isShow : true,
                        });
                        if (branch.Children != null) {
                            _ref = branch.Children;
                            _results = [];
                            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                                child = _ref[_i];
                                child_visible = visible && branch.expanded;
                                _results.push(add_branch_to_list(level + 1, child, child_visible));
                            }
                            return _results;
                        }
                    };
                    _ref = scope.treeData;
                    _results = [];
                    if (_ref != undefined) for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                        root_branch = _ref[_i];
                        _results.push(add_branch_to_list(1, root_branch, true));
                    }
                    return _results;
                };
                scope.$watch('treeData', on_treeData_change, true);
                if (attrs.initialSelection != null) {
                    for_each_branch(function (b) {
                        if (b.Caption === attrs.initialSelection) {
                            var test = [];
                            var parent;
                            parent = get_parent(branch);
                            if (parent != null) {
                                test.push(parent);
                                scope.parenttester = [];
                                var ty = recursiveparent(parent);
                                if (ty != undefined) {
                                    if (ty.length > 0) {
                                        for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                            test.push(obj);
                                        }
                                    }
                                }
                            }
                            return $timeout(function () {
                                return select_branch(b, test);
                            });
                        }
                    });
                }
                n = scope.treeData.length;
                for_each_branch(function (b, level) {
                    b.level = level;
                    return b.expanded = b.level < expand_level;
                });
                if (scope.treeControl != null) {
                    if (angular.isObject(scope.treeControl)) {
                        tree = scope.treeControl;
                        tree.expand_all = function () {
                            return for_each_branch(function (b, level) {
                                return b.expanded = true;
                            });
                        };
                        tree.collapse_all = function () {
                            return for_each_branch(function (b, level) {
                                return b.expanded = false;
                            });
                        };
                        tree.get_first_branch = function () {
                            n = scope.treeData.length;
                            if (n > 0) {
                                return scope.treeData[0];
                            }
                        };
                        tree.select_first_branch = function () {
                            var b;
                            b = tree.get_first_branch();
                            var test = [];
                            var parent;
                            parent = get_parent(branch);
                            if (parent != null) {
                                test.push(parent);
                                scope.parenttester = [];
                                var ty = recursiveparent(parent);
                                if (ty != undefined) {
                                    if (ty.length > 0) {
                                        for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                            test.push(obj);
                                        }
                                    }
                                }
                            }
                            return tree.select_branch(b, test);
                        };
                        tree.get_selected_branch = function () {
                            return selected_branch;
                        };
                        tree.get_parent_branch = function (b) {
                            return get_parent(b);
                        };
                        tree.select_branch = function (b, parentArr) {
                            var test = [];
                            var parent;
                            parent = get_parent(branch);
                            if (parent != null) {
                                test.push(parent);
                                scope.parenttester = [];
                                var ty = recursiveparent(parent);
                                if (ty != undefined) {
                                    if (ty.length > 0) {
                                        for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                            test.push(obj);
                                        }
                                    }
                                }
                            }
                            select_branch(b, test);
                            return b;
                        };
                        tree.get_children = function (b) {
                            return b.Children;
                        };
                        tree.select_parent_branch = function (b) {
                            var p;
                            if (b == null) {
                                b = tree.get_selected_branch();
                            }
                            if (b != null) {
                                p = tree.get_parent_branch(b);
                                var test = [];
                                var parent;
                                parent = get_parent(branch);
                                if (parent != null) {
                                    test.push(parent);
                                    scope.parenttester = [];
                                    var ty = recursiveparent(parent);
                                    if (ty != undefined) {
                                        if (ty.length > 0) {
                                            for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                                test.push(obj);
                                            }
                                        }
                                    }
                                }
                                if (p != null) {
                                    tree.select_branch(p, test);
                                    return p;
                                }
                            }
                        };
                        tree.add_branch = function (parent, new_branch) {
                            if (parent != null) {
                                parent.Children.push(new_branch);
                                parent.expanded = true;
                            } else {
                                scope.treeData.push(new_branch);
                            }
                            return new_branch;
                        };
                        tree.add_root_branch = function (new_branch) {
                            tree.add_branch(null, new_branch);
                            return new_branch;
                        };
                        tree.expand_branch = function (b) {
                            if (b == null) {
                                b = tree.get_selected_branch();
                            }
                            if (b != null) {
                                b.expanded = true;
                                return b;
                            }
                        };
                        tree.collapse_branch = function (b) {
                            if (b == null) {
                                b = selected_branch;
                            }
                            if (b != null) {
                                b.expanded = false;
                                return b;
                            }
                        };
                    }
                }
            }
        };
    }]);

    /******* Attribute Group Related Directives ******/

    app.directive('attributeGroup', [function () {
        return {
            restrict: 'EA',
            templateUrl: 'views/mui/planningtool/component/attributegroup.html',
            controller: 'mui.planningtool.component.attributegroupCtrl',
            replace: true,
            scope: {
                itemIndex: '@',
                itemGroupId: '@',
                itemTitle: '@',
                itemProcessedNumber: '=itemProcessed',
                itemIsLock: '@',
                itemLanguageContent: '=',
                itemImagecrop: '=',
                itemImagefilename: '@',
                itemPredefined: '@',
                itemPagesize: '@'
            }
        }
    }]);
    app.directive('attributegroupIndetailblock', [function () {
        return {
            restrict: 'EA',
            templateUrl: 'views/mui/planningtool/component/attributegroupIndetailblock.html',
            controller: 'mui.planningtool.component.attributegroupCtrl',
            replace: true,
            scope: {
                itemIndex: '@',
                itemGroupId: '@',
                itemTitle: '@',
                itemProcessedNumber: '=itemProcessed',
                itemIsLock: '@',
                itemLanguageContent: '=',
                itemImagecrop: '=',
                itemImagefilename: '@',
                itemPredefined: '@',
                itemPagesize: '@'
            }
        }
    }]);
    app.directive('attributeGrouplistview', [function () {
        return {
            restrict: 'EA',
            templateUrl: 'views/mui/planningtool/component/attributegroupListView.html',
            controller: 'mui.planningtool.component.attributegroupCtrl',
            replace: true,
            scope: {
                itemIndex: '@',
                itemGroupId: '@',
                itemTitle: '@',
                itemProcessedNumber: '=itemProcessed',
                itemIsLock: '@',
                itemLanguageContent: '=',
                itemImagecrop: '=',
                itemImagefilename: '@',
                itemPredefined: '@',
                itemPagesize: '@'
            }
        }
    }]);
    app.directive('attributeGrouplistviewindetail', [function () {
        return {
            restrict: 'EA',
            templateUrl: 'views/mui/planningtool/component/attributegroupListViewInDetailBlock.html',
            controller: 'mui.planningtool.component.attributegroupCtrl',
            replace: true,
            scope: {
                itemIndex: '@',
                itemGroupId: '@',
                itemTitle: '@',
                itemProcessedNumber: '=itemProcessed',
                itemIsLock: '@',
                itemLanguageContent: '=',
                itemImagecrop: '=',
                itemImagefilename: '@',
                itemPredefined: '@',
                itemPagesize: '@'
            }
        }
    }]);
    app.directive('attributeGrouplistganttviewincustomtab', [function () {
        return {
            restrict: 'EA',
            replace: false,
            transclude: true,
            templateUrl: 'views/mui/planningtool/component/attributegroupListGanttViewInCustomTab.html',
            controller: 'mui.planningtool.component.attributegrouplistganttviewincustomtabCtrl',
            scope: {
                itemIndex: '@',
                itemGroupId: '@',
                itemTitle: '@',
                itemProcessedNumber: '=itemProcessed',
                itemIsLock: '@',
                itemLanguageContent: '=',
                itemImagecrop: '=',
                itemImagefilename: '@',
                itemPredefined: '@',
                itemPagesize: '@'
            }
        }
    }]);

    app.directive('attributegroupIntabcreation', [function () {
        return {
            restrict: 'EA',
            templateUrl: 'views/mui/planningtool/component/attributegroupInCreationTab.html',
            controller: 'mui.planningtool.component.attributegroupInCreationTabCtrl',
            replace: true,
            scope: {
                itemAttributerecordid: '@',
                itemParentid: '@',
                itemIsinherit: '@',
                itemGroupId: '@',
                itemTitle: '@',
                itemPredefined: '@',
                itemLanguageContent: '=',
                itemImagecrop: '=',
                itemImagefilename: '@',
                itemAddedattrgrps: '='
            }

        }
    }]);

    app.directive('abnTree', ['$timeout', function ($timeout) {
        return {
            restrict: 'E',
            template: "<ul  class=\"nav nav-pills nav-stacked abn-tree treesearchcls\">\n  <li ng-show=\"row.branch.isShow\" ng-repeat=\"row in tree_rows | filter:filterItem track by row.branch.uid\" ng-animate=\"'abn-tree-animate'\" ng-class=\"'level-' + {{ row.level }} + (row.branch.selected ? ' active':'')\" class=\"abn-tree-row treesearchcls\">\n   " + "<a  ng-hide=\"treeAccessable\" class=\"treesearchcls\"  ng-click=\"row.branch.ischecked = !row.branch.ischecked; user_clicks_branch_1(row.branch,$event)\">\n   <i  ng-class=\"row.tree_icon\" ng-click=\"row.branch.expanded = !row.branch.expanded\"  class=\"indented tree-icon treesearchcls\"> </i>\n <label  class=\"inlineBlock checkbox checkbox-custom treesearchcls\"> <input class=\"treesearchcls\"   type=\"checkbox\">  <i class=\"treesearchcls\"   ng-class=\"{'checkbox checked': row.branch.ischecked , 'checkbox': !row.branch.ischecked}\"/> </label> <span  class=\"indented tree-label treesearchcls\"  my-qtip2 qtip-content=\"{{row.Caption}}\">{{ row.Caption }} </span>\n    </a>\n" + "<a  ng-show=\"treeAccessable\"  class=\"treesearchcls\" ng-click=\"user_clicks_branch_Expand_Collapse(row.branch,$event)\">\n   <i  ng-class=\"row.tree_icon\" ng-click=\"row.branch.expanded = !row.branch.expanded\"   class=\"indented tree-icon treesearchcls\"> </i>\n <label  class=\"inlineBlock checkbox checkbox-custom treesearchcls\"> <input class=\"treesearchcls\"   type=\"checkbox\">  <i class=\"treesearchcls\"   ng-class=\"{'checkbox checked': row.branch.ischecked , 'checkbox': !row.branch.ischecked}\"/> </label> <span  class=\"indented tree-label treesearchcls\"  my-qtip2 qtip-content=\"{{row.Caption}}\">{{ row.Caption }} </span>\n    </a>\n" + "</div></li>\n</ul>",
            scope: {
                treeData: '=',
                treeFilter: '=',
                onSelect: '&',
                initialSelection: '@',
                accessable: '@',
                treeControl: '='
            },
            link: function (scope, element, attrs) {
                var error, expand_all_parents, expand_level, for_all_ancestors, for_each_branch, get_parent, n, on_treeData_change, select_branch, selected_branch, tree;
                error = function (s) {
                    console.log('ERROR:' + s);
                    return void 0;
                };
                if (attrs.iconExpand == null) {
                    attrs.iconExpand = 'icon-caret-right';
                }
                if (attrs.iconCollapse == null) {
                    attrs.iconCollapse = 'icon-caret-down';
                }
                if (attrs.iconLeaf == null) {
                    attrs.iconLeaf = 'icon-fixed-width icon-blank';
                }
                if (attrs.expandLevel == null) {
                    attrs.expandLevel = '100';
                }
                expand_level = parseInt(attrs.expandLevel, 10);
                if (!scope.treeData) {
                    console.log('no treeData defined for the tree!');
                    return;
                }
                if (scope.treeData.length == null) {
                    if (treeData.Caption != null) {
                        scope.treeData = [treeData];
                    } else {
                        console.log('treeData should be an array of root branches');
                        return;
                    }
                }
                for_each_branch = function (f) {
                    var do_f, root_branch, _i, _len, _ref, _results;
                    do_f = function (branch, level) {
                        var child, _i, _len, _ref, _results;
                        f(branch, level);
                        if (branch.Children != null) {
                            _ref = branch.Children;
                            _results = [];
                            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                                child = _ref[_i];
                                _results.push(do_f(child, level + 1));
                            }
                            return _results;
                        }
                    };
                    _ref = scope.treeData;
                    _results = [];
                    if (_ref != undefined) for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                        root_branch = _ref[_i];
                        _results.push(do_f(root_branch, 1));
                    }
                    return _results;
                };
                selected_branch = null;
                select_branch = function (branch, parentArr) {
                    if (!branch) {
                        if (selected_branch != null) {
                            selected_branch.selected = false;
                        }
                        selected_branch = null;
                        return;
                    }
                    if (branch !== undefined) {
                        if (selected_branch != null) {
                            selected_branch.selected = false;
                        }
                        branch.selected = true;
                        selected_branch = branch;
                        expand_all_parents(branch);
                        if (branch.onSelect != null) {
                            return $timeout(function () {
                                var test = [];
                                var parent;
                                parent = get_parent(branch);
                                if (parent != null) {
                                    test.push(parent);
                                    scope.parenttester = [];
                                    var ty = recursiveparent(parent);
                                    if (ty != undefined) {
                                        if (ty.length > 0) {
                                            for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                                test.push(obj);
                                            }
                                        }
                                    }
                                }
                                return branch.onSelect(branch, test);
                            });
                        } else {
                            if (scope.onSelect != null) {
                                return $timeout(function () {
                                    var test = [];
                                    var parent;
                                    parent = get_parent(branch);
                                    if (parent != null) {
                                        test.push(parent);
                                        scope.parenttester = [];
                                        var ty = recursiveparent(parent);
                                        if (ty != undefined) {
                                            if (ty.length > 0) {
                                                for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                                    test.push(obj);
                                                }
                                            }
                                        }
                                    }
                                    return scope.onSelect({
                                        branch: branch,
                                        parent: test
                                    });
                                });
                            }
                        }
                    }
                };
                scope.parenttester = [];
                scope.user_clicks_branch = function (branch) {
                    if (branch !== selected_branch) {
                        var test = [];
                        var parent;
                        parent = get_parent(branch);
                        if (parent != null) {
                            test.push(parent);
                            scope.parenttester = [];
                            var ty = recursiveparent(parent);
                            if (ty != undefined) {
                                if (ty.length > 0) {
                                    for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                        test.push(obj);
                                    }
                                }
                            }
                        }
                        return select_branch(branch, test);
                    }
                };
                scope.filterItem = function (item) {
                    if (!scope.treeFilter) {
                        item.isShow = true;
                        if (item.expanded != undefined) {
                            item.expanded = true;
                        }
                        return true;
                    }
                    var found = item.Caption.toLowerCase().indexOf(scope.treeFilter.toLowerCase()) != -1;
                    if (!found) {
                        var itemColl = item.branch != undefined ? item.branch.Children : item.Children;
                        angular.forEach(itemColl, function (item) {
                            var match = scope.filterItem(item);
                            if (match) {
                                found = true;
                                item.isShow = true;
                            }
                        });
                    }
                    return found;
                };

                function collapseOnemptySearch(item) {
                    var itemColl = item.branch != undefined ? item.branch.Children : item.Children;
                    angular.forEach(itemColl, function (item) {
                        item.isShow = true;
                    });
                }
                scope.user_clicks_branch_Expand_Collapse = function (branch, event) {
                    var test = [];
                    var parent;
                    parent = get_parent(branch);
                    var target = $(event.target);
                    if (target.hasClass("tree-icon")) {
                        for (var z = 0, childObj; childObj = branch.Children[z++];) {
                            childObj.isShow = branch.expanded;
                            Expandcollapse(childObj.Children, branch.expanded);
                        }
                        return false;
                    }
                };
                scope.user_clicks_branch_1 = function (branch, event) {
                    var test = [];
                    var parent;
                    parent = get_parent(branch);
                    var target = $(event.target);
                    if (target.hasClass("tree-icon")) {
                        branch.ischecked = !branch.ischecked
                        for (var z = 0, childObj; childObj = branch.Children[z++];) {
                            childObj.isShow = branch.expanded;
                            Expandcollapse(childObj.Children, branch.expanded);
                        }
                        return false;
                    } else {
                        branch.ischecked = branch.ischecked;
                        return select_branch(branch, test);
                    }
                };

                function Expandcollapse(Children, expanded) {
                    for (var z = 0, childObj; childObj = Children[z++];) {
                        childObj.isShow = expanded;
                        if (childObj.Children.length > 0) {
                            Expandcollapse(childObj.Children, expanded);
                        }
                    }
                }

                function recursiveparent(parent) {
                    var parent1;
                    parent1 = get_parent(parent);
                    if (parent1 != null && parent1 != undefined) {
                        scope.parenttester.push(parent1);
                        recursiveparent(parent1);
                    }
                    return scope.parenttester;
                }
                get_parent = function (child) {
                    var parent;
                    parent = void 0;
                    if (child.parent_uid) {
                        for_each_branch(function (b) {
                            if (b.uid === child.parent_uid) {
                                return parent = b;
                            }
                        });
                    }
                    return parent;
                };
                for_all_ancestors = function (child, fn) {
                    var parent;
                    parent = get_parent(child);
                    if (parent != null) {
                        fn(parent);
                        return for_all_ancestors(parent, fn);
                    }
                };
                expand_all_parents = function (child) {
                    return for_all_ancestors(child, function (b) {
                        return b.expanded = true;
                    });
                };
                scope.tree_rows = [];
                scope.treeAccessable = attrs.accessable != null ? (attrs.accessable == "false" ? false : true) : false;
                on_treeData_change = function () {
                    var add_branch_to_list, root_branch, _i, _len, _ref, _results;
                    for_each_branch(function (b, level) {
                        if (!b.uid) {
                            return b.uid = "" + Math.random();
                        }
                    });
                    for_each_branch(function (b) {
                        var child, _i, _len, _ref, _results;
                        if (angular.isArray(b.Children)) {
                            _ref = b.Children;
                            _results = [];
                            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                                child = _ref[_i];
                                _results.push(child.parent_uid = b.uid);
                            }
                            return _results;
                        }
                    });
                    scope.tree_rows = [];
                    for_each_branch(function (branch) {
                        var child, f;
                        if (branch.Children) {
                            if (branch.Children.length > 0) {
                                f = function (e) {
                                    if (typeof e === 'string') {
                                        return {
                                            Caption: e,
                                            Children: []
                                        };
                                    } else {
                                        return e;
                                    }
                                };
                                return branch.Children = (function () {
                                    var _i, _len, _ref, _results;
                                    _ref = branch.Children;
                                    _results = [];
                                    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                                        child = _ref[_i];
                                        _results.push(f(child));
                                    }
                                    return _results;
                                })();
                            }
                        } else {
                            return branch.Children = [];
                        }
                    });
                    add_branch_to_list = function (level, branch, visible) {
                        var child, child_visible, tree_icon, _i, _len, _ref, _results;
                        if (branch.expanded == null) {
                            branch.expanded = false;
                        }
                        if (!branch.Children || branch.Children.length === 0) {
                            tree_icon = attrs.iconLeaf;
                        } else {
                            if (branch.expanded) {
                                tree_icon = attrs.iconCollapse;
                            } else {
                                tree_icon = attrs.iconExpand;
                            }
                        }
                        scope.tree_rows.push({
                            level: level,
                            branch: branch,
                            Caption: branch.Caption,
                            tree_icon: tree_icon,
                            visible: visible,
                            ischecked: branch.ischecked != undefined ? branch.ischecked : false,
                            isShow: branch.isShow != undefined ? branch.isShow : true,
                        });
                        if (branch.Children != null) {
                            _ref = branch.Children;
                            _results = [];
                            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                                child = _ref[_i];
                                child_visible = visible && branch.expanded;
                                _results.push(add_branch_to_list(level + 1, child, child_visible));
                            }
                            return _results;
                        }
                    };
                    _ref = scope.treeData;
                    _results = [];
                    if (_ref != undefined) for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                        root_branch = _ref[_i];
                        _results.push(add_branch_to_list(1, root_branch, true));
                    }
                    return _results;
                };
                scope.$watch('treeData', on_treeData_change, true);
                if (attrs.initialSelection != null) {
                    for_each_branch(function (b) {
                        if (b.Caption === attrs.initialSelection) {
                            var test = [];
                            var parent;
                            parent = get_parent(branch);
                            if (parent != null) {
                                test.push(parent);
                                scope.parenttester = [];
                                var ty = recursiveparent(parent);
                                if (ty != undefined) {
                                    if (ty.length > 0) {
                                        for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                            test.push(obj);
                                        }
                                    }
                                }
                            }
                            return $timeout(function () {
                                return select_branch(b, test);
                            });
                        }
                    });
                }
                n = scope.treeData.length;
                for_each_branch(function (b, level) {
                    b.level = level;
                    return b.expanded = b.level < expand_level;
                });
                if (scope.treeControl != null) {
                    if (angular.isObject(scope.treeControl)) {
                        tree = scope.treeControl;
                        tree.expand_all = function () {
                            return for_each_branch(function (b, level) {
                                return b.expanded = true;
                            });
                        };
                        tree.collapse_all = function () {
                            return for_each_branch(function (b, level) {
                                return b.expanded = false;
                            });
                        };
                        tree.get_first_branch = function () {
                            n = scope.treeData.length;
                            if (n > 0) {
                                return scope.treeData[0];
                            }
                        };
                        tree.select_first_branch = function () {
                            var b;
                            b = tree.get_first_branch();
                            var test = [];
                            var parent;
                            parent = get_parent(branch);
                            if (parent != null) {
                                test.push(parent);
                                scope.parenttester = [];
                                var ty = recursiveparent(parent);
                                if (ty != undefined) {
                                    if (ty.length > 0) {
                                        for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                            test.push(obj);
                                        }
                                    }
                                }
                            }
                            return tree.select_branch(b, test);
                        };
                        tree.get_selected_branch = function () {
                            return selected_branch;
                        };
                        tree.get_parent_branch = function (b) {
                            return get_parent(b);
                        };
                        tree.select_branch = function (b, parentArr) {
                            var test = [];
                            var parent;
                            parent = get_parent(branch);
                            if (parent != null) {
                                test.push(parent);
                                scope.parenttester = [];
                                var ty = recursiveparent(parent);
                                if (ty != undefined) {
                                    if (ty.length > 0) {
                                        for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                            test.push(obj);
                                        }
                                    }
                                }
                            }
                            select_branch(b, test);
                            return b;
                        };
                        tree.get_children = function (b) {
                            return b.Children;
                        };
                        tree.select_parent_branch = function (b) {
                            var p;
                            if (b == null) {
                                b = tree.get_selected_branch();
                            }
                            if (b != null) {
                                p = tree.get_parent_branch(b);
                                var test = [];
                                var parent;
                                parent = get_parent(branch);
                                if (parent != null) {
                                    test.push(parent);
                                    scope.parenttester = [];
                                    var ty = recursiveparent(parent);
                                    if (ty != undefined) {
                                        if (ty.length > 0) {
                                            for (var k = 0, obj; obj = scope.parenttester[k++];) {
                                                test.push(obj);
                                            }
                                        }
                                    }
                                }
                                if (p != null) {
                                    tree.select_branch(p, test);
                                    return p;
                                }
                            }
                        };
                        tree.add_branch = function (parent, new_branch) {
                            if (parent != null) {
                                parent.Children.push(new_branch);
                                parent.expanded = true;
                            } else {
                                scope.treeData.push(new_branch);
                            }
                            return new_branch;
                        };
                        tree.add_root_branch = function (new_branch) {
                            tree.add_branch(null, new_branch);
                            return new_branch;
                        };
                        tree.expand_branch = function (b) {
                            if (b == null) {
                                b = tree.get_selected_branch();
                            }
                            if (b != null) {
                                b.expanded = true;
                                return b;
                            }
                        };
                        tree.collapse_branch = function (b) {
                            if (b == null) {
                                b = selected_branch;
                            }
                            if (b != null) {
                                b.expanded = false;
                                return b;
                            }
                        };
                    }
                }
            }
        };
    }]);
    app.directive('treecontext', [function () {
        return {
            restrict: 'A',
            compile: function compile(tElement, tAttrs, transclude, scope) {
                return {
                    post: function postLink(scope, iElement, iAttrs, controller) {
                        var ul = $('#' + iAttrs.treecontext),
							last = null;
                        var id = $('#' + iAttrs.treecontext);
                        ul.css({
                            'display': 'none'
                        });
                        $(iElement).click(function (event) {
                            event.stopPropagation();
                            var top = event.clientY + 10;
                            if ((ul.height() + top + 14) > $(window).height()) {
                                top = top - (ul.height() + 50);
                            } else { }
                            ul.css({
                                display: "block",
                                zIndex: 999999
                            });
                            var target = $(event.target);
                            last = event.timeStamp;
                            if (event.stopPropagation) event.stopPropagation();
                            if (event.preventDefault) event.preventDefault();
                            event.cancelBubble = true;
                            event.returnValue = false;
                            jQuery('#' + iAttrs.treecontext).show().appendTo(jQuery(this).parent())
                        });
                        $(document).click(function (event) {
                            var target = $(event.target);
                            if (target.hasClass("treesearchcls")) {
                                return;
                            }
                            if (!target.is(".popover") && !target.parents().is(".popover")) {
                                if (last === event.timeStamp) return;
                                ul.css({
                                    'display': 'none'
                                });
                                jQuery('#' + iAttrs.treecontext).hide().appendTo(jQuery(this).parent())
                            }
                        });
                        $(window).scroll(function (event) {
                            var target = $(event.target);
                            if (!target.is(".popover") && !target.parents().is(".popover")) {
                                if (last === event.timeStamp) return;
                                ul.css({
                                    'display': 'none'
                                });
                                jQuery('#' + iAttrs.treecontext).hide().appendTo(jQuery(this).parent())
                            }
                        });
                    }
                };
            }
        };
    }]);
    app.directive('euTree', ['$compile', function ($compile) {
        return {
            restrict: 'E',
            link: function (scope, element, attrs) {
                scope.selectedNode = null;
                var treeformflag = false;
                var treenodetree = new Array();

                function CallBackTreeStructure(treeobj) {
                    treenodetree = new Array();
                    processrecords(treeobj);
                    var dtstring = treenodetree.join("");
                    return dtstring;
                }

                function processrecords(treeobj) {
                    if (treeobj != undefined) {
                        for (var i = 0, node; node = treeobj[i++];) {
                            if (node.ischecked == true) {
                                if (treenodetree.length > 0) if (treenodetree[treenodetree.length - 1] != "[") treenodetree.push(",");
                                treenodetree.push(node.Caption);
                                treeformflag = false;
                                if (ischildSelected(node.Children)) {
                                    treenodetree.push("[");
                                    processrecords(node.Children);
                                    treenodetree.push("]");
                                } else {
                                    processrecords(node.Children);
                                }
                            } else processrecords(node.Children);
                        }
                    }
                }

                function ischildSelected(children) {
                    for (var j = 0, child; child = children[j++];) {
                        if (child.ischecked == true) {
                            treeformflag = true;
                            return treeformflag
                        }
                    }
                    return treeformflag;
                }
                scope.$watch(attrs.treeData, function (val) {
                    var treestring = '';
                    treenodetree = new Array();
                    treestring = (attrs.fileid == undefined || attrs.fileid == null) ? CallBackTreeStructure(scope.treesrcdirec["Attr_" + attrs.nodeAttributeid]) : CallBackTreeStructure(scope.treesrcdirec["Attr_" + attrs.fileid + attrs.nodeAttributeid]);
                    var template = '';
                    var placeoftree = (attrs.treeplace != null || attrs.treeplace != undefined) ? attrs.treeplace : "inline";
                    if (placeoftree === "detail") template = angular.element('<a href="javscript:void(0)" class="treeAttributeSelectionText">' + treestring + '</a>');
                    else template = angular.element('<span class="treeAttributeSelectionText">' + treestring + '</span>');
                    var linkFunction = $compile(template);
                    linkFunction(scope);
                    element.html(null).append(template);
                }, true);
            }
        };
    }]);

    app.directive('xeditabletree', ['$timeout', '$compile', '$resource', '$window', '$translate', 'PlanningToolService', function ($timeout, $compile, $resource, $window, $translate, PlanningToolService) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs, ngModel) {
                var attributeName = attrs.attributename;
                var CurrentEntityID = parseInt(attrs.entityid, 10);
                var attributeID = attrs.attributeid;
                var dttype = attrs.editabletypeid;
                var isdam = attrs.isdam;
                var isDataTypeExists = false;
                var access = true;
                access = attrs.isreadonly != undefined ? (attrs.isreadonly == "true" ? true : false) : true;
                for (var variable in $.fn.editabletypes) {
                    if (dttype == variable) isDataTypeExists = true;
                }
                var Address = function (options) {
                    this.init(dttype, options, Address.defaults);
                };
                $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
                var inlinePopup = '';
                inlinePopup += '<div class="ngcompiletree">';
                inlinePopup += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="ngcompiletree form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + attrs.attributeid + '" placeholder="Search" treecontext="detailtreeNodeSearchDropdown_Attr_' + attrs.attributeid + '"></div>';
                inlinePopup += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu upsideDown" style="display: none;" id="detailtreeNodeSearchDropdown_Attr_' + attrs.attributeid + '">';
                inlinePopup += '<span ng-if="doing_async">...' + scope.LanguageContents.Res_68 + '...</span>';
                inlinePopup += '<abn-tree accessable="' + access + '" tree-filter="filterValue_' + attrs.attributeid + '"   tree-data=\"staticTreesrcdirec.Attr_' + attrs.attributeid + '\" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                inlinePopup += '</div></div>';
                Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
                    tpl: $compile(inlinePopup)(scope),
                    inputclass: ''
                });
                $.fn.editabletypes[dttype] = Address;
                var loadXeditable = function () {
                    angular.element(element).editable({
                        title: attrs.title,
                        display: function (value, srcData) { },
                        savenochange: true
                    });
                }
                $timeout(function () {
                    loadXeditable();
                }, 10);
                angular.element(element).on('shown', function (e, editable) {
                    $timeout(function () {
                        if (editable != undefined) {
                            $compile($('.ngcompiletree'))(scope);
                            var treeCollection = [],
								treeData = [],
								remainRecord = [];
                            if (isdam == "true") {
                                PlanningToolService.GetAttributeTreeNodedam(attrs.attributeid, parseInt(CurrentEntityID), attrs.choosefromparent).then(function (GetTree) {
                                    treeData = JSON.parse(GetTree.Response).Children;
                                    GetTreeShowObjecttoSave(attrs.attributeid);
                                    remainRecord = [];
                                    GetSavedTreeNode();
                                    TickSavedTreeNode(attrs.attributeid);
                                });
                            } else {
                                PlanningToolService.GetAttributeTreeNode(attrs.attributeid, parseInt(CurrentEntityID), attrs.choosefromparent).then(function (GetTree) {
                                    treeData = JSON.parse(GetTree.Response).Children;
                                    GetTreeShowObjecttoSave(attrs.attributeid);
                                    remainRecord = [];
                                    GetSavedTreeNode();
                                    TickSavedTreeNode(attrs.attributeid);
                                });
                            }
                        }

                        function GetTreeShowObjecttoSave(attributeid) {
                            for (var i = 0, branch; branch = scope.staticTreesrcdirec["Attr_" + attributeid][i++];) {
                                branch.isShow = true;
                                if (branch.Children.length > 0) {
                                    FormTreeRecursiveChildTreenode(branch.Children);
                                }
                            }
                        }

                        function FormTreeRecursiveChildTreenode(children) {
                            for (var j = 0, child; child = children[j++];) {
                                child.isShow = true;
                                if (child.Children.length > 0) {
                                    FormTreeRecursiveChildTreenode(child.Children);
                                }
                            }
                        }

                        function GetSavedTreeNode(attributeid) {
                            for (var i = 0, branch; branch = treeData[i++];) {
                                if (branch.ischecked == true) {
                                    remainRecord = $.grep(treeCollection, function (e) {
                                        return e.AttributeId == branch.AttributeId && e.id == branch.id;
                                    });
                                    if (remainRecord.length == 0) {
                                        treeCollection.push(branch);
                                    }
                                    if (branch.Children.length > 0) {
                                        GetRecursiveChildTreenode(branch.Children);
                                    }
                                } else {
                                    if (branch.Children.length > 0) {
                                        GetRecursiveChildTreenode(branch.Children);
                                    }
                                }
                            }
                        }

                        function GetRecursiveChildTreenode(children) {
                            for (var j = 0, child; child = children[j++];) {
                                if (child.ischecked == true) {
                                    remainRecord = $.grep(treeCollection, function (e) {
                                        return e.AttributeId == child.AttributeId && e.id == child.id;
                                    });
                                    if (remainRecord.length == 0) {
                                        treeCollection.push(child);
                                        if (child.Children.length > 0) {
                                            GetRecursiveChildTreenode(child.Children);
                                        }
                                    }
                                } else {
                                    GetRecursiveChildTreenode(child.Children);
                                }
                            }
                        }

                        function TickSavedTreeNode(attributeid) {
                            for (var i = 0, branch; branch = scope.staticTreesrcdirec["Attr_" + attributeid][i++];) {
                                var tickremainRecord = [];
                                tickremainRecord = $.grep(treeCollection, function (e) {
                                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                                });
                                if (tickremainRecord.length > 0) {
                                    branch.ischecked = true;
                                }
                                TickRecursiveChildTreenode(branch.Children);
                            }
                        }

                        function TickRecursiveChildTreenode(children) {
                            for (var j = 0, child; child = children[j++];) {
                                var tickremainRecord = [];
                                tickremainRecord = $.grep(treeCollection, function (e) {
                                    return e.AttributeId == child.AttributeId && e.id == child.id;
                                });
                                if (tickremainRecord.length > 0) {
                                    child.ischecked = true;
                                    if (child.Children.length > 0) {
                                        TickRecursiveChildTreenode(child.Children);
                                    }
                                }
                            }
                        }
                    }, 10);
                });
                angular.element(element).on('save', function (e, params) {
                    scope.savetreeDetail(attrs.attributeid, attrs.attributetypeid, CurrentEntityID);
                });
            }
        };
    }]);

})(angular, app);
(function (ng, app) {
    "use strict";

    function PlanningToolService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            AutoCompleteMemberList: AutoCompleteMemberList,
            GetCurrencyListFFsettings: GetCurrencyListFFsettings,
            GetTreeNode: GetTreeNode,
            GetTreeNodeByEntityID: GetTreeNodeByEntityID,
            GetOptionDetailListByID: GetOptionDetailListByID,
            GetOptionDetailListByAssetID: GetOptionDetailListByAssetID,
            GetDamTreeNode: GetDamTreeNode,
            GetDamTreeNodeByEntityID: GetDamTreeNodeByEntityID,
            GetAttributeTreeNodedam: GetAttributeTreeNodedam,
            GetAttributeTreeNode: GetAttributeTreeNode

        });

        function AutoCompleteMemberList(QueryStr, GroupID) {
            var request = $http({
                method: "post",
                url: "api/user/AutoCompleteMemberList",
                data: {
                    QueryString: QueryStr
                },
                params: {
                    action: "add"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCurrencyListFFsettings() {
            var request = $http({
                method: "get",
                url: "api/Planning/GetCurrencyListFFsettings/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreeNode(AttributeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreeNode/" + AttributeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTreeNodeByEntityID(AttributeID, ParentEntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetTreeNodeByEntityID/" + AttributeID + "/" + ParentEntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionDetailListByID(ID, EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetOptionDetailListByID/" + ID + "/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetOptionDetailListByAssetID(ID, AssetID) {
            var request = $http({
                method: "Get",
                url: "api/dam/GetOptionDetailListByAssetID/" + ID + "/" + AssetID + "/",
                params: {
                    action: "Get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDamTreeNode(AttributeID) {
            var request = $http({
                method: "Get",
                url: "api/dam/GetDamTreeNode/" + AttributeID,
                params: {
                    action: "Get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDamTreeNodeByEntityID(AttributeID, ParentEntityID) {
            var request = $http({
                method: "Get",
                url: "api/dam/GetDamTreeNodeByEntityID/" + AttributeID + "/" + ParentEntityID,
                params: {
                    action: "Get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeTreeNodedam(AttributeID, Entityid, Ischoosefromparent) {
            var request = $http({
                method: "Get",
                url: "api/dam/GetAttributeTreeNode/" + AttributeID + "/" + Entityid + "/" + Ischoosefromparent,
                params: {
                    action: "Get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function GetAttributeTreeNode(AttributeID, EntityID, Ischoosefromparent) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeTreeNode/" + AttributeID + "/" + EntityID + "/" + Ischoosefromparent,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.service("PlanningToolService", ['$http', '$q', PlanningToolService]);
})(angular, app);