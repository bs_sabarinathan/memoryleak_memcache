///#source 1 1 /assets/js/Nod/nod.js
(function ($) {


    var Checker,
      __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; };

    Checker = (function () {

        function Checker($el, field) {
            this.run = __bind(this.run, this);

            var sel;
            sel = field[0], this.metric = field[1];
            this.getVal = this.makeGetVal($el, sel);
        }

        Checker.prototype.run = function () {
            return this.verify(this.metric, this.getVal());
        };

        Checker.prototype.makeGetVal = function ($el, sel) {
            var inputs, name, type;
            type = $el.attr('type');
            if (type === 'checkbox') {
                return function () {
                    return $el.is(':checked');
                };
            } else if (type === 'radio') {
                name = $el.attr('name');
                return function () {
                    return $('[name="' + name + '"]').filter(':checked').val();
                };
            } else {
                if (this.metric === 'one-of') {
                    inputs = $(sel);
                    return function () {
                        return inputs.map(function () {
                            return $.trim(this.value);
                        }).get().join('');
                    };
                } else {
                    return function () {
                        return $.trim($el.val());
                    };
                }
            }
        };

        Checker.prototype.verify = function (m, v) {
            var arg, sec, type, _ref;
            if (!!(m && m.constructor && m.call && m.apply)) {
                return m(v);
            }
            if (m instanceof RegExp) {
                return m.test(v);
            }
            _ref = $.map(m.split(':'), $.trim), type = _ref[0], arg = _ref[1], sec = _ref[2];
            if (type === 'same-as' && $(arg).length !== 1) {
                throw new Error('same-as selector must target one and only one element');
            }
            if (!v && type !== 'presence' && type !== 'one-of') {
                return true;
            }
            switch (type) {
                case 'presence':
                    return !!v;
                case 'one-of':
                    return !!v;
                case 'exact':
                    return v === arg;
                case 'not':
                    return v !== arg;
                case 'same-as':
                    return v === $(arg).val();
                case 'min-num':
                    return +v >= +arg;
                case 'max-num':
                    return +v <= +arg;
                case 'between-num':
                    return +v >= +arg && +v <= +sec;
                case 'min-length':
                    return v.length >= +arg;
                case 'max-length':
                    return v.length <= +arg;
                case 'exact-length':
                    return v.length === +arg;
                case 'between':
                    return v.length >= +arg && v.length <= +sec;
                case 'integer':
                    return /^\s*\d+\s*$/.test(v);
                case 'float':
                    return /^[-+]?[0-9]+(\.[0-9]+)?$/.test(v);
                case 'email':
                    return this.email(v);
                default:
                    throw new Error('I don\'t know ' + type + ', sorry.');
            }
        };

        Checker.prototype.email = function (v) {
            var RFC822;
            RFC822 = /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$/;
            return RFC822.test(v);
        };

        return Checker;

    })();

    var Listener,
      __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; };

    Listener = (function () {

        function Listener(el, get, field) {
            this.get = get;
            this.field = field;
            this.change_status = __bind(this.change_status, this);

            this.runCheck = __bind(this.runCheck, this);

            this.delayedCheck = __bind(this.delayedCheck, this);

            this.events = __bind(this.events, this);

            this.destroy = __bind(this.destroy, this);

            this.eventHandlers = [];
            this.$el = $(el);
            this.delayId = "";
            this.status = null;
            this.checker = new Checker(this.$el, this.field);
            this.msg = new Msg(this.$el, this.get, this.field);
            this.events();
        }

        Listener.prototype.destroy = function () {
            this.msg.destroy();
            if (this.$el.attr('type') === 'radio') {
                return $('[name="' + this.$el.attr("name") + '"]').off('change', '**', this.eventHandlers.radio);
            } else {
                this.$el.off('change', '**', this.eventHandlers.change);
                this.$el.off('blur', '**', this.eventHandlers.blur);
                if (this.field[1] === 'one-of') {
                    $(window).off('nod-run-one-of', '**', this.eventHandlers.window);
                }
                if (this.get.delay) {
                    return this.$el.off('keyup', '**', this.eventHandlers.keyup);
                }
            }
        };

        Listener.prototype.events = function () {
            this.eventHandlers['radio'] = this.runCheck;
            this.eventHandlers['change'] = this.runCheck;
            this.eventHandlers['blur'] = this.runCheck;
            this.eventHandlers['window'] = this.runCheck;
            this.eventHandlers['keyup'] = this.delayedCheck;
            if (this.$el.attr('type') === 'radio') {
                return $('[name="' + this.$el.attr("name") + '"]').on('change', this.eventHandlers.radio);
            } else {
                this.$el.on('change', this.eventHandlers.change);
                this.$el.on('blur', this.eventHandlers.blur);
                if (this.field[1] === 'one-of') {
                    $(window).on('nod-run-one-of', this.eventHandlers.window);
                }
                if (this.get.delay) {
                    return this.$el.on('keyup', this.eventHandlers.keyup);
                }
            }
        };

        Listener.prototype.delayedCheck = function () {
            clearTimeout(this.delayId);
            return this.delayId = setTimeout(this.runCheck, this.get.delay);
        };

        Listener.prototype.runCheck = function () {
            return $.when(this.checker.run()).then(this.change_status);
        };

        Listener.prototype.change_status = function (status) {
            var isCorrect;
            try {
                status = eval(status);
            } catch (_error) { }
            isCorrect = !!status;
            if (this.status === isCorrect) {
                return;
            }
            this.status = isCorrect;
            this.msg.toggle(this.status);
            $(this).trigger('nod_toggle');
            if (this.field[1] === 'one-of' && status) {
                return $(window).trigger('nod-run-one-of');
            }
        };

        return Listener;

    })();

    var Msg,
      __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; };

    Msg = (function () {

        function Msg($el, get, field) {
            this.$el = $el;
            this.get = get;
            this.createShowMsg = __bind(this.createShowMsg, this);

            this.toggle = __bind(this.toggle, this);

            this.createMsg = __bind(this.createMsg, this);

            this.destroy = __bind(this.destroy, this);

            this.$msg = this.createMsg(field[2]);
            this.showMsg = this.createShowMsg();
        }

        Msg.prototype.destroy = function () {
            this.toggle(true);
            this.$msg = null;
            return this.showMsg = null;
        };

        Msg.prototype.createMsg = function (msg) {
           
            return $('<i/>', {
                'title': msg,
                'class': this.get.helpSpanDisplay + ' ' + this.get.errorClass + ' ' + 'icon-warning-sign',
                'style': 'padding-left: 3px; color: #B94A48; vertical-align: super; cursor: help;'
            });
        };

        Msg.prototype.toggle = function (status) {
            if (status) {
                return this.$msg.remove();
            } else {
                this.showMsg();
                if (this.get.broadcastError) {
                    return this.broadcast();
                }
            }
        };

        Msg.prototype.createShowMsg = function () {
            var pos, type;
            type = this.$el.attr('type');
            if (type === 'checkbox' || type === 'radio') {
                return function () {
                    return this.$el.parent().append(this.$msg);
                };
            } else {
                pos = this.findPos(this.$el);
                return function () {
                    return pos.after(this.$msg);
                };
            }
        };

        Msg.prototype.findPos = function ($el) {
            if (this.elHasClass('parent', $el)) {
                return this.findPos($el.parent());
            }
            if (this.elHasClass('next', $el)) {
                return this.findPos($el.next());
            }
            return $el;
        };

        Msg.prototype.elHasClass = function (dir, $el) {
            var sel, _i, _len, _ref;
            _ref = this.get.errorPosClasses;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                sel = _ref[_i];
                if ($el[dir](sel).length) {
                    return true;
                }
            }
            return false;
        };

        Msg.prototype.broadcast = function () {
            return $(window).trigger('nod_error_fired', {
                el: this.$el,
                msg: this.$msg.html()
            });
        };

        return Msg;

    })();

    var Nod,
      __bind = function (fn, me) { return function () { return fn.apply(me, arguments); }; };

    Nod = (function () {

        function Nod(form, fields, options) {
            this.form = form;
            this.formIsErrorFree = __bind(this.formIsErrorFree, this);

            this.submitForm = __bind(this.submitForm, this);

            this.toggleSubmitBtnText = __bind(this.toggleSubmitBtnText, this);

            this.toggleSubmitBtn = __bind(this.toggleSubmitBtn, this);

            this.toggleGroupClass = __bind(this.toggleGroupClass, this);

            this.toggle_status = __bind(this.toggle_status, this);

            this.massCheck = __bind(this.massCheck, this);

            this.listenForEnter = __bind(this.listenForEnter, this);

            this.events = __bind(this.events, this);

            this.destroy = __bind(this.destroy, this);

            this.destroyListeners = __bind(this.destroyListeners, this);

            this.createListeners = __bind(this.createListeners, this);

            if (!fields) {
                return;
            }
            this.form[0].__nod = this;
            this.get = $.extend({
                'delay': 700,
                'disableSubmitBtn': true,
                'helpSpanDisplay': 'help-inline',
                'groupClass': 'error',
                'submitBtnSelector': '[type=submit]',
                'metricsSplitter': ':',
                'errorPosClasses': ['.help-inline', '.add-on', 'button', '.input-append'],
                'silentSubmit': false,
                'broadcastError': false,
                'errorClass': 'nod_msg',
                'groupSelector': '.control-group'
            }, options);
            this.listeners = this.createListeners(fields);
            this.submit = this.form.find(this.get.submitBtnSelector);
            this.checkIfElementsExist(this.form, this.submit, this.get.disableSubmitBtn);
            this.events();
        }

        Nod.prototype.createListeners = function (fields) {
            var el, field, listeners, _i, _j, _len, _len1, _ref;
            listeners = [];
            for (_i = 0, _len = fields.length; _i < _len; _i++) {
                field = fields[_i];
                if (field.length !== 3) {
                    this["throw"]('field', field);
                }
                _ref = this.form.find(field[0]);
                for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
                    el = _ref[_j];
                    listeners.push(new Listener(el, this.get, field));
                }
            }
            return listeners;
        };

        Nod.prototype.destroyListeners = function () {
            var listener, _i, _len, _ref;
            _ref = this.listeners;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                listener = _ref[_i];
                listener.destroy();
            }
        };

        Nod.prototype.destroy = function () {
            $(this.get.groupSelector).removeClass(this.get.groupClass);
            this.submit.removeClass('disabled');
            this.submit.removeAttr('disabled');
            return this.destroyListeners();
        };

        Nod.prototype.events = function () {
            var l, _i, _len, _ref;
            _ref = this.listeners;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                l = _ref[_i];
                $(l).on('nod_toggle', this.toggle_status);
            }
            if (this.submit.length) {
                return this.submit.on('click', this.massCheck);
            } else {
                return this.form.on('keyup', this.listenForEnter);
            }
        };

        Nod.prototype.listenForEnter = function (event) {
            if (event.keyCode === 13) {
                return this.massCheck();
            }
        };

        Nod.prototype.massCheck = function (event) {
            var checks, l, _i, _len, _ref;
            if (event != null) {
                event.preventDefault();
            }
            checks = [];
            _ref = this.listeners;
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                l = _ref[_i];
                checks.push(l.runCheck());
            }
            this.toggleSubmitBtnText();
            return $.when.apply($, checks).then(this.submitForm).then(this.toggleSubmitBtnText);
        };

        Nod.prototype.toggle_status = function (event) {
            this.toggleGroupClass(event.target.$el.parents(this.get.groupSelector));
            if (this.get.disableSubmitBtn) {
                return this.toggleSubmitBtn();
            }
        };

        Nod.prototype.toggleGroupClass = function ($group) {
            if ($group.find('.' + this.get.errorClass).length) {
                return $group.addClass(this.get.groupClass);
            } else {
                return $group.removeClass(this.get.groupClass);
            }
        };

        Nod.prototype.toggleSubmitBtn = function () {
            if (this.formIsErrorFree()) {
                return this.submit.removeClass('disabled').removeAttr('disabled');
            } else {
                return this.submit.addClass('disabled').attr('disabled', 'disabled');
            }
        };

        Nod.prototype.toggleSubmitBtnText = function () {
            var tmp;
            tmp = this.submit.attr('data-loading-text');
            if (tmp) {
                this.submit.attr('data-loading-text', this.submit.html());
                return this.submit.html(tmp);
            }
        };

        Nod.prototype.submitForm = function () {
            var $form;
            if (!this.formIsErrorFree()) {
                return;
            }
            if (this.get.silentSubmit) {
                $form = $(this.form);
                return $form.trigger('silentSubmit', $form.serialize());
            } else {
                return this.form.submit();
            }
        };

        Nod.prototype.formIsErrorFree = function () {
            return !$(this.listeners).filter(function () {
                if (this.status === null) {
                    this.runCheck();
                }
                return !this.status;
            }).length;
        };

        Nod.prototype.checkIfElementsExist = function (form, submit, disableSubmitBtn) {
            if (!form.selector || !form.length) {
                this["throw"]('form', form);
            }
            if (!submit.length && disableSubmitBtn) {
                return this["throw"]('submit', submit);
            }
        };

        Nod.prototype["throw"] = function (type, el) {
            var txt;
            switch (type) {
                case 'form':
                    txt = 'Couldn\'t find form: ';
                    break;
                case 'submit':
                    txt = 'Couldn\'t find submit button: ';
                    break;
                case 'field':
                    txt = 'Metrics for each field must have three parts: ';
            }
            throw new Error(txt + el);
        };

        return Nod;

    })();


    $.fn.nod = function (fields, settings) {
        if (!(fields || settings)) {
            return this[0].__nod;
        }
        new Nod(this, fields, settings);
        return this;
    };

})(jQuery);

///#source 1 1 /assets/js/InHouse/jquery.AdjustWidth.js
(function ($) {

    $.fn.AdjustHeightWidth = function () {

        ResizeMe();
        $(window).off("resize", ResizeMe);
        $(window).resize(ResizeMe);
        $(window).off("scroll", TreeScroll);

        var ResizeLeft = "if ($('#fixedHeight').length > 0) {" +
            "$('#left').css('min-height', '');" +
            "$('#separator').css('min-height', '');" +
            "$('#left').css('min-height', $('#fixedHeight').height() + 'px');" +
            "$('#separator').css('min-height', $('#fixedHeight').height() + 'px');" +
            "} else {" +
            "$('#left').css('min-height', '');" +
            "$('#separator').css('min-height', '');" +
            "$('#left').css('min-height', ($('#container').height() - $('#FilterSettingsModal').outerHeight()) + 'px');" +
            "$('#separator').css('min-height', ($('#container').height() - $('#FilterSettingsModal').outerHeight()) + 'px');" +
            "$('#mediaBankPage #left').css('min-height', ($('#container').height() - $('#FilterSettingsModal').outerHeight() - $('#singlePageTitle').outerHeight() - 126) + 'px');" +  //For MEDIA BANK Screen (mediabank.html)
            "$('#mediaBankPage #separator').css('min-height', ($('#container').height() - $('#FilterSettingsModal').outerHeight() - $('#singlePageTitle').outerHeight() - 126) + 'px');" +  //For MEDIA BANK Screen (mediabank.html)
            "}" +
            //For News feed Section in Overview Tab
            "$('#OverviewDiv #EntityFeedsdiv').height($('#OverviewDiv .span5').height() - 156);" +      //For the Height of News Feed in Overview Tab - Plans
            "$('#CostcentreOverviewDiv #EntityFeedsdiv').height($('#CostcentreOverviewDiv .span5').height() - 156);" +      //For the Height of News Feed in Overview Tab - Cost Centres
            "$('#ObjectiveOverviewDiv #EntityFeedsdiv').height($('#ObjectiveOverviewDiv .span5').height() - 156);" +        //For the Height of News Feed in Overview Tab - Objectives
            "$('#CalenderOverviewDiv #CalenderFeedsdiv').height($('#CalenderOverviewDiv .span5').height() - 156);" +        //For the Height of News Feed in Overview Tab - Calendar
            "$('#cmssectionview #OverviewDiv #CmsPageFeedsdiv').height($('#cmssectionview #OverviewDiv .span5').height() - 156);" +        //For the Height of News Feed in Overview Tab - CMS
            //For Tree Folder Structure in Attachments
            "$('#AttachmentsDiv #left_folderTree').css('min-height', ($('#AttachmentsDiv #right_DamView').height()));" +  //For the Folder Tree appearing in Attachments Tab
            "$('#damentityselect #left_entity_folderTree').css('min-height', ($('#damentityselect #entitycontentScroll').height()));" +   //For the Folder Tree appearing while Adding new Sub Entity
            "$('#attachfromdam #assetscontainerattach #left_entity_folderTree').css('min-height', ($('#attachfromdam #assetscontainerattach #entitycontentScroll').height()));" +   //For the Folder Tree appearing while Adding new attachment from DAM in Tasks
            "$('#damtask #left_attach_folderTree').css('min-height', ($('#damtask #contentScroll').height()));" +   //For the Folder Tree appearing while attaching from DAM in Tasks
            //For Task Popups News Feed Section
                    //For the New Feed Section height in Task popups of ENTITY
            "$('#WorkTask .rightSection #Taskfeeddivforworktask').height($('#WorkTask .leftSection').height() - 80);" +   //For the Height of News Feed in Entity Work Task Popup
            "$('#ApprovalTask .rightSection #Taskfeeddivforapprovaltask').height($('#ApprovalTask .leftSection').height() - 80);" +   //For the Height of News Feed in Entity Approval Task Popup
            "$('#ReviewTask .rightSection #feeddivforreviewTask').height($('#ReviewTask .leftSection').height() - 80);" +   //For the Height of News Feed in Entity Review Task Popup
            "$('#UnassignedTask .rightSection #feeddivforunassignedassigned').height($('#UnassignedTask .leftSection').height() - 80);" +   //For the Height of News Feed in Entity Unassigned Task Popup
                    //For the New Feed Section height in Task popups of NOTIFICATION
            "$('#NotifyWorkMyTaskPopup .rightSection #Notifyfeeddivworktaskforedit').height($('#NotifyWorkMyTaskPopup .leftSection').height() - 80);" +   //For the Height of News Feed in Notification Work Task Popup
            "$('#NotifyApprovalMyTaskPopup .rightSection #Notifyfeeddivapprovaltaskforedit').height($('#NotifyApprovalMyTaskPopup .leftSection').height() - 80);" +   //For the Height of News Feed in Notification Approval Task Popup
            "$('#NotifyReviewMyTaskPopup .rightSection #Notifyfeeddivforreviewtaskforedit').height($('#NotifyReviewMyTaskPopup .leftSection').height() - 80);" +   //For the Height of News Feed in Notification Review Task Popup
            "$('#NotifyUnassignedTask .rightSection #notifyfeeddivforunassignedassigned').height($('#NotifyUnassignedTask .leftSection').height() - 80);" +   //For the Height of News Feed in Notification Unassigned Task Popup
                    //For the New Feed Section height in Task popups of MY TASKS
            "$('#TaskEditWorkTaskPopup .rightSection #feeddivforworktaskedit').height($('#TaskEditWorkTaskPopup .leftSection').height() - 80);" +   //For the Height of News Feed in MyTask Work Task Popup
            "$('#TaskEditApprovalTaskPopup .rightSection #feeddivapprovaltaskedit').height($('#TaskEditApprovalTaskPopup .leftSection').height() - 80);" +   //For the Height of News Feed in MyTask Approval Task Popup
            "$('#TaskEditReviewTaskPopup .rightSection #feeddivforreviewtaskforedit').height($('#TaskEditReviewTaskPopup .leftSection').height() - 80);" +   //For the Height of News Feed in MyTask Review Task Popup
                    //For the Left Section Height in ADD TASK Popup
            "$('#addTask.addTaskPopup .modal-body #lftSideForm').css('min-height', ($('#addTask.addTaskPopup .modal-body #rytSideAttachments').height()) - 10);";       //For the Height of Left Side Form in Add New Task Popup
        var i = setInterval(ResizeLeft, 500);
    };

    function ResizeMe() {
      
        var contentHeight = $('html').outerHeight() - ($('#footer').outerHeight()
                    + $('#header').outerHeight()
                    + $('#FilterSettingsModal').outerHeight()  //Added for the situation, when filter is open
                   );

        var dataHeight = $('html').outerHeight() - ($('#footer').outerHeight()
                    + $('#header').outerHeight()
                    + $('#content .contentHeader').outerHeight()
                    + $('#FilterSettingsModal').outerHeight()  //Added for the situation, when filter is open
                    + $('.title').outerHeight() + 1);

        $('#container').css('min-height', contentHeight + 'px');

        $('#mypagenotification').height(dataHeight - 120);

        //For CMS Edit Page Tabs
        var cmsTreeHeight = $('#cmsTreeHolder').outerHeight();
        if (cmsTreeHeight != null && cmsTreeHeight > dataHeight) {
            $('#cmssectionview').css("min-height", (cmsTreeHeight - 70));
            $('#DivCMSContentBind').css("min-height", (cmsTreeHeight - 123));
        }
        else {
            $('#cmssectionview').css("min-height", (dataHeight - 28));
            $('#DivCMSContentBind').css("min-height", (dataHeight - 81));
        }

        if ($('#treeHolder').length > 0) {
            //Root level list
            $('#treeHolder').css('overflow', 'hidden').height(dataHeight);
            $('#ListContainer').css('overflow-y', 'scroll').height(dataHeight);
        }
        else if ($('#EntitiesTree').length > 0) {
            if ($('#ListContainer').length > 0) {
                //listview
                $('#EntitiesTree').css('overflow', 'hidden').height(dataHeight);
                $('#ListContainer').css('overflow-y', 'scroll').height(dataHeight);
            } else if ($('#ganttChart').length > 0) {
                $('#EntitiesTree').css('overflow', 'hidden').height(dataHeight);
                $('#ganttChart .ganttview-data').height(dataHeight);
                $('#ganttChart .ganttview-list').height(dataHeight + 35);
                $('#ganttChart .ganttview-list-container').height(dataHeight + 35);
                $('#ganttChart .ganttview-list-container-data').height(dataHeight);
                $('#ganttChart .ganttview-grid').height(dataHeight);
                $('#ganttChart .ganttview-grid-row').height($('#ganttChart #GanttBlockContainer').height());
                $('#left').css('min-height', '');
                $('#separator').css('min-height', '');

                //ganttview
                if ($('#SectionTabs').length > 0) {
                    var sectiondataHeight = dataHeight - 147;
                    /*This Condition is for presenting Gantt View Inside the Tabs. We are deducting extra pixels getting consumed by the Tabs section*/
                    $('#AttributeGroupInCustomTab #EntitiesTree').css('overflow', 'hidden').height(sectiondataHeight);
                    $('#AttributeGroupInCustomTab #ganttChart .ganttview-data').height(sectiondataHeight);
                    $('#AttributeGroupInCustomTab #ganttChart .ganttview-list').height(sectiondataHeight + 35);
                    $('#AttributeGroupInCustomTab #ganttChart .ganttview-list-container').height(sectiondataHeight + 35);
                    $('#AttributeGroupInCustomTab #ganttChart .ganttview-list-container-data').height(sectiondataHeight);
                    $('#AttributeGroupInCustomTab #ganttChart .ganttview-grid').height(sectiondataHeight);
                    $('#AttributeGroupInCustomTab #ganttChart .ganttview-grid-row').height($('#ganttChart #GanttBlockContainer').height());
                }
            } else {
                //detail
                $('#EntitiesTree').removeAttr("style");
                $(window).on("scroll", TreeScroll);
            }
        }

        $('#resultsearchvalues').height(dataHeight + 35);

        if ($('#left').length == 0) {
            $('#DefaultTasks').css('min-height', contentHeight + 'px');
        }
        //For dynamically positioning Modal Footer Dropdowns
        if (($('.modal-footer #btnDropdownDynamic > ul.dropdown-menu').offset + $('.modal-footer #btnDropdownDynamic > ul.dropdown-menu').height) > $(window).height) {
            $('.modal-footer #btnDropdownDynamic').addClass('dropup');
        }
        else {
            $('.modal-footer #btnDropdownDynamic').removeClass('dropup');
        }
    };

    function TreeScroll() {
        $('#EntitiesTree').trigger("onTreeScroll");
    };
})(jQuery);

$(document).ready(function () {
    $(window).AdjustHeightWidth();
});
///#source 1 1 /assets/js/Bootstrap/Fuelux/wizard.js
/*
 * Fuel UX Wizard
 * https://github.com/ExactTarget/fuelux
 *
 * Copyright (c) 2012 ExactTarget
 * Licensed under the MIT license.
 */

!function ($) {


    // WIZARD CONSTRUCTOR AND PROTOTYPE

    var Wizard = function (element, options) {
        var kids;

        this.$element = $(element);
        this.options = $.extend({}, $.fn.wizard.defaults, options);
        this.currentStep = 1;
        this.numSteps = this.$element.find('li').length;
        this.$prevBtn = this.$element.find('button.btn-prev');
        this.$nextBtn = this.$element.find('button.btn-next');

        kids = this.$nextBtn.children().detach();
        this.nextText = $.trim(this.$nextBtn.text());
        this.$nextBtn.append(kids);

        // handle events
        this.$prevBtn.on('click', $.proxy(this.previous, this));
        this.$nextBtn.on('click', $.proxy(this.next, this));
        this.$element.on('click', 'li.complete', $.proxy(this.stepclicked, this));
    };

    Wizard.prototype = {

        constructor: Wizard,

        setState: function () {
            var canMovePrev = (this.currentStep > 1);
            var firstStep = (this.currentStep === 1);
            var lastStep = (this.currentStep === this.numSteps);

            // disable buttons based on current step
            this.$prevBtn.attr('disabled', (firstStep === true || canMovePrev === false));

            // change button text of last step, if specified
            var data = this.$nextBtn.data();
            if (data && data.last) {
                this.lastText = data.last;
                if (typeof this.lastText !== 'undefined') {
                    // replace text
                    var text = (lastStep !== true) ? this.nextText : this.lastText;
                    var kids = this.$nextBtn.children().detach();
                    this.$nextBtn.text(text).append(kids);
                }
            }

            // reset classes for all steps
            var $steps = this.$element.find('li');
            $steps.removeClass('active').removeClass('complete');
            $steps.find('span.badge').removeClass('badge-info').removeClass('badge-success');

            // set class for all previous steps
            var prevSelector = 'li:lt(' + (this.currentStep - 1) + ')';
            var $prevSteps = this.$element.find(prevSelector);
            $prevSteps.addClass('complete');
            $prevSteps.find('span.badge').addClass('badge-success');

            // set class for current step
            var currentSelector = 'li:eq(' + (this.currentStep - 1) + ')';
            var $currentStep = this.$element.find(currentSelector);
            $currentStep.addClass('active');
            $currentStep.find('span.badge').addClass('badge-info');

            // set display of target element
            if ($currentStep.data() != null) {
            var target = $currentStep.data().target;
            $('.step-pane').removeClass('active');
            $(target).addClass('active');
            }

            this.$element.trigger('changed');
        },

        stepclicked: function (e) {
            var li = $(e.currentTarget);
            var curul ;
            curul = e.delegateTarget.firstElementChild.getAttribute('id');

            if (li[0].attributes['data-target'].value == "#step11" || li[0].attributes['data-target'].value == "#step22") {
                var index = $('.stepsObj li').index(li);
            }
            else if (curul != undefined)
            {
                var index = $("#" + curul + ' li').index(li);
            }
            else {
                var index = $('.steps li').index(li);
            }
            var evt = $.Event('stepclick');
            this.$element.trigger(evt, { step: index + 1 });
            if (evt.isDefaultPrevented()) return;

            this.currentStep = (index + 1);
            this.setState();
        },


        stepLoaded: function () {
           
            var evt = $.Event('stepLoad');
            this.$element.trigger(evt, { step:  1 });
            if (evt.isDefaultPrevented()) return;

            this.currentStep = 1;
            this.setState();
        },

        previous: function () {
            var canMovePrev = (this.currentStep > 1);
            if (canMovePrev) {
                var e = $.Event('change');
                this.$element.trigger(e, { step: this.currentStep, direction: 'previous' });
                if (e.isDefaultPrevented()) return;

                this.currentStep -= 1;
                this.setState();
            }
        },

        manualpreviousstep: function (val) {
            var canMovePrev = (this.currentStep > 1);
            if (canMovePrev) {
                var e = $.Event('change');
                this.$element.trigger(e, { step: this.currentStep, direction: 'previous' });
                if (e.isDefaultPrevented()) return;

                this.currentStep = val;
                this.setState();
            }
        },

        next: function () {
            var canMoveNext = (this.currentStep + 1 <= this.numSteps);
            var lastStep = (this.currentStep === this.numSteps);

            if (canMoveNext) {
                var e = $.Event('change');
                this.$element.trigger(e, { step: this.currentStep, direction: 'next' });

                if (e.isDefaultPrevented()) return;

                this.currentStep += 1;
                this.setState();
            }
            else if (lastStep) {
                this.$element.trigger('finished');
            }
        },

        manualnextstep: function (val) {
            var canMoveNext = (this.currentStep + 1 <= this.numSteps);
            var lastStep = (this.currentStep === this.numSteps);

            if (canMoveNext) {
                var e = $.Event('change');
                this.$element.trigger(e, { step: this.currentStep, direction: 'next' });

                if (e.isDefaultPrevented()) return;

                this.currentStep = val;
                this.setState();
            }
            else if (lastStep) {
                this.$element.trigger('finished');
            }
        },

        selectedItem: function (val) {
            return {
                step: this.currentStep
            };
        },
        totnumsteps: function (val) {
            return {
                totstep: this.numSteps
            };
        },
        IsLastStep: function (val) {
            return (this.currentStep === this.numSteps);
        }
    };


    // WIZARD PLUGIN DEFINITION

    $.fn.wizard = function (option, value) {
        var methodReturn;

        var $set = this.each(function () {
            var $this = $(this);
            var data = $this.data('wizard');
            var options = typeof option === 'object' && option;

            if (!data) $this.data('wizard', (data = new Wizard(this, options)));
            if (typeof option === 'string') methodReturn = data[option](value);
        });

        return (methodReturn === undefined) ? $set : methodReturn;
    };

    $.fn.wizard.defaults = {};

    $.fn.wizard.Constructor = Wizard;


    // WIZARD DATA-API
    $(document).on('click', '.wizard', function (e) {

        var $this = $(e.target);
        if ($this.data('wizard')) return;
        $this.wizard($this.data());

    });

    //$(function () {
    //	$('body').on('mousedown.wizard.data-api', '.wizard', function () {
    //		var $this = $(this);
    //		if ($this.data('wizard')) return;
    //		$this.wizard($this.data());
    //	});
    //});

}(window.jQuery);

