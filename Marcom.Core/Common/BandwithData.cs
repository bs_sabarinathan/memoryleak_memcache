﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Common
{
    internal class BandwithData : IBandwithData
    {
         #region Member Variables
        protected int _days;
        protected int _id;
        protected double _cvalue;
        #endregion

        #region Constructors
        public BandwithData() { }
        public BandwithData(int id ,int days, double cvalue)
            {
              this._id = id;
             this._days = days;
             this._cvalue = cvalue;
            
            }
        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }
        public int Days
            {
                get { return _days; }
                set { _days = value; }

            }
       
        public double Cvalue
            {
                get { return _cvalue; }
                set { _cvalue = value; }
            }
        #endregion 

        #region Equals And HashCode Overrides

        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
            {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
            }
        #endregion
		
    }
}
