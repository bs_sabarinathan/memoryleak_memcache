﻿(function (ng, app) {
    "use strict"; function muiadminPoNumberCntrl($scope, $location, $resource, $timeout, $translate, AdminService) {
        $scope.poOrderResult = []; $scope.myValue = false; var PoSettings = 'PurchaseOrderSettings_Table'; AdminService.GetPoSSettings(PoSettings).then(function (res) { $scope.poOrderResult = res.Response; var count = res.Response.length; $scope.id_prefix = $scope.poOrderResult[count - 1].Prefix; $scope.id_dateformat = $scope.poOrderResult[count - 1].DateFormat; $scope.id_numberofdigits = $scope.poOrderResult[count - 1].NumberCount; $scope.id_randomnumber = $scope.poOrderResult[count - 1].DigitFormat; }); AdminService.GetCurrentPONumber().then(function (res) { $scope.CurrentRunningPOnumber = res.Response; }); $scope.format = ''; $scope.GenerateNumber = function () {
            var datemissing = false; if ($scope.id_dateformat == '') { datemissing = true; }
            if ($scope.id_numberofdigits == undefined) { bootbox.alert($translate.instant('LanguageContents.Res_1832.Caption')); return false; }
            if ($scope.id_dateformat == undefined && datemissing == false) { bootbox.alert($translate.instant('LanguageContents.Res_1833.Caption')); return false; }
            if (datemissing == false) {
                var today = new Date().toString('' + $scope.id_dateformat + '')
                if (today == undefined) { bootbox.alert($translate.instant('LanguageContents.Res_1834.Caption')); return false; }
            }
            if ($scope.id_numberofdigits > 8) { bootbox.alert($translate.instant('LanguageContents.Res_1835.Caption')); return false; }
            var ExactRandomNumber = ''; if ($scope.id_randomnumber.toString().length > $scope.id_numberofdigits) { bootbox.alert($translate.instant('LanguageContents.Res_1836.Caption')); return false; }
            else if ($scope.id_randomnumber.toString().length == $scope.id_numberofdigits) { ExactRandomNumber = $scope.id_randomnumber; }
            else {
                var IncludeZeros = ''; var TotalZero = $scope.id_numberofdigits - $scope.id_randomnumber.toString().length; IncludeZeros = '0'
                var k = 0; for (k = 0; k < TotalZero - 1; k++) { IncludeZeros += '0'; }
                ExactRandomNumber = IncludeZeros + $scope.id_randomnumber;
            }
            if (datemissing == false) { $scope.format = $scope.id_prefix + today + ExactRandomNumber; $scope.myValue = true; } else { $scope.format = $scope.id_prefix + ExactRandomNumber; $scope.myValue = true; }
        }
        $("#id_randomnumber").keypress(function (e) { if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { bootbox.alert($translate.instant('LanguageContents.Res_1837.Caption')); return false; } }); $("#id_numberofdigits").keypress(function (e) { if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { bootbox.alert($translate.instant('LanguageContents.Res_1837.Caption')); return false; } }); $scope.SavePoFormat = function () {
            var datemissing = false; if ($scope.id_dateformat == '') { datemissing = true; }
            if ($scope.id_numberofdigits == undefined) {
                bootbox.alert($translate.instant('LanguageContents.Res_1832.Caption'))
                return false;
            }
            if ($scope.id_dateformat == undefined && datemissing == false) {
                bootbox.alert($translate.instant('LanguageContents.Res_1833.Caption'))
                return false;
            }
            if (datemissing == false) {
                var today = new Date().toString('' + $scope.id_dateformat + '')
                if (today == undefined) { bootbox.alert($translate.instant('LanguageContents.Res_1834.Caption')); return false }
            }
            if ($scope.id_randomnumber.toString().length > $scope.id_numberofdigits) { bootbox.alert($translate.instant('LanguageContents.Res_1836.Caption')); return false; }
            bootbox.confirm($translate.instant('LanguageContents.Res_1838.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var addnewpoSettings = {}; addnewpoSettings.Prefix = $scope.id_prefix; if (datemissing == false) { addnewpoSettings.DateFormat = $scope.id_dateformat; }
                        else { addnewpoSettings.DateFormat = ''; }
                        addnewpoSettings.DigitFormat = $scope.id_randomnumber; addnewpoSettings.NumberCount = $scope.id_numberofdigits; AdminService.InsertPoSettingXML(addnewpoSettings).then(function (res) { bootbox.alert($translate.instant('LanguageContents.Res_1839.Caption')); });
                    }, 100);
                }
                else { return null; }
            });
        }
        AdminService.GetFinancialforecastData().then(function (res) { if (res.Response[2] == true) { $scope.disableforExternalsource = true; } else { $scope.disableforExternalsource = false; } });
    }
    app.controller("mui.admin.PoNumberCntrl", ['$scope', '$location', '$resource', '$timeout', '$translate', 'AdminService', muiadminPoNumberCntrl]);
})(angular, app);