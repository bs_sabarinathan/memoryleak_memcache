﻿(function (ng, app) {
    function muiadminacclockCntrl($scope, $location, $resource, $timeout, $translate, AdminService) {
        $('#Attempts').mask('00000000'); $('#Duration').mask('00000000'); LoadAccLockDetails(); GetLockeduserDetails(); function GetLockeduserDetails() { AdminService.GetLockuserDetails().then(function (res) { $scope.selectedAllusers = false; if (res.Response != null) { $scope.LockedUserList = res.Response; } }); }
        function LoadAccLockDetails() { AdminService.GetAccLockDetails().then(function (userlockdata) { var details = userlockdata.Response; $scope.lockouttime = details[0].lockouttime; $scope.attempts = details[0].attempts; $scope.monthlyrepetition = details[0].monthlyrepetition; $scope.repetitiontime = details[0].repetitiontime; $scope.monthlyexpiration = details[0].monthlyexpiration; $scope.expirevalidation = details[0].expirevalidation; $scope.notifyuser = details[0].notifyuser; $scope.notifyadmin = details[0].notifyadmin; }); }
        $scope.SavePwdDetails = function () {
            if ($scope.attempts > 0) { var pwddata = {}; pwddata.lockouttime = $scope.lockouttime; pwddata.attempts = $scope.attempts; pwddata.monthlyrepetition = $scope.monthlyrepetition; pwddata.repetitiontime = $scope.repetitiontime; pwddata.monthlyexpiration = $scope.monthlyexpiration; pwddata.expirevalidation = $scope.expirevalidation; pwddata.notifyuser = $scope.notifyuser; pwddata.notifyadmin = $scope.notifyadmin; AdminService.UpdateAccLockPasswordPolicy(pwddata).then(function (response) { if (response) { NotifySuccess($translate.instant("LanguageContents.Res_4805.Caption")); LoadAccLockDetails(); } }); }
            else { bootbox.alert($translate.instant('LanguageContents.Res_5725.Caption')); }
        }
        $scope.UnlockRequest = function () {
            if ($('#UnlockRequestBtn').hasClass('disabled')) { return; }

$('#UnlockRequestBtn').addClass('disabled');var UserCollection=$.grep($scope.LockedUserList,function(e){return e.Selected==true;});if(UserCollection.length==0){bootbox.alert($translate.instant('LanguageContents.Res_154.Caption'));$('#UnlockRequestBtn').removeClass('disabled');return false;}
else {
    var obj = {}; obj.SelectedUser = UserCollection; AdminService.SendPasswordResetMail(obj).then(function (Userresult) {
        if (Userresult.StatusCode == 405) { NotifyError($translate.instant('LanguageContents.Res_4349.Caption')); $('#UnlockRequestBtn').removeClass('disabled'); $scope.selectedAllusers = false; }
else{if(Userresult.Response==null){NotifyError($translate.instant('LanguageContents.Res_4349.Caption'));$('#UnlockRequestBtn').removeClass('disabled');$scope.selectedAllusers=false;}
else{$('#UnlockRequestBtn').removeClass('disabled');NotifySuccess($translate.instant("LanguageContents.Res_5678.Caption"));$scope.LockedUserList=null;GetLockeduserDetails();$scope.selectedAllusers=false;}}});}}
$scope.checkAllUsers=function(){if($scope.selectedAllusers){$scope.selectedAllusers=true;}else{$scope.selectedAllusers=false;}
angular.forEach($scope.LockedUserList,function(item){item.Selected=$scope.selectedAllusers;});};$scope.RemoveCheckallSelection=function(list,$event){var checkbox=$event.target;list.Selected=checkbox.checked;var UserCollection=$.grep($scope.LockedUserList,function(e){return e.Selected==true;}).length;if(UserCollection!=$scope.LockedUserList.length)
$scope.selectedAllusers=false;else
$scope.selectedAllusers=true;}}
    app.controller("mui.admin.muiadminacclockCntrl", ['$scope', '$location', '$resource', '$timeout', '$translate', 'AdminService', muiadminacclockCntrl]);
})(angular, app);