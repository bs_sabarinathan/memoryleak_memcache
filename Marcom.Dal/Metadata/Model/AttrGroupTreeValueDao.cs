using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

	/// <summary>
    /// TreeLevelDao object for table 'MM_AttrGroupTreeValue'.
	/// </summary>
	
	public partial class AttrGroupTreeValueDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected int _attributeid;
        protected int _Nodeid;
        protected int _level;
        protected int _groupid;
        protected int _grouprecordid;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public AttrGroupTreeValueDao() {}

        public AttrGroupTreeValueDao(int pId, int pAttributeid, int pNodeid,int pLevel,int pGroupID,int pGroupRecordID)
		{
			this._id = pId; 
			this._attributeid = pAttributeid; 
            this._Nodeid = pNodeid;
            this._level = pLevel;
            this._groupid = pGroupID;
            this._grouprecordid = pGroupRecordID;
		}

        public AttrGroupTreeValueDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual int Attributeid
		{
			get { return _attributeid; }
            set { _attributeid = value; }

        }
        public virtual int Nodeid
        {
            get { return _Nodeid; }
            set { _Nodeid = value; }

		}
        public virtual int Level
        {
            get { return _level ; }
            set { _level = value; }

        }
        public virtual int GroupID
        {
            get { return _groupid; }
            set { _groupid = value; }
        }
        public virtual int GroupRecordID
        {
            get { return _grouprecordid; }
            set { _grouprecordid = value; }
        }
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string TreeValueDao = "TreeValueDao";			
			public const string Id = "Id";
            public const string Attributeid = "Attributeid";
            public const string Nodeid = "Nodeid";
            public const string Level = "Level";
            public const string GroupID = "GroupID";
            public const string GroupRecordID = "GroupRecordID";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string TreeValueDao = "TreeValueDao";
            public const string Id = "Id";
            public const string Attributeid = "Attributeid";
            public const string Nodeid = "Nodeid";
            public const string Level = "Level";
            public const string GroupID = "GroupID";
            public const string GroupRecordID = "GroupRecordID";
		}

		#endregion
		
		
	}
	
}
