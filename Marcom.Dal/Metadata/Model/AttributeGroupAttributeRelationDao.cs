﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{
    public class AttributeGroupAttributeRelationDao : BaseDao, ICloneable
    {
        #region Member Variables
        protected int _id;
        protected int _attributegroupid;
        protected int _attributeid;
        protected string _caption;
        protected int _sortorder;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        protected bool _issearchable;
        protected bool _showascolumn;
        #endregion

        #region Extensibility Method Definitions

        //public override bool Equals(object obj)
        //{
        //    AttributeGroupAttributeRelation toCompare = obj as AttributeGroupAttributeRelation;
        //    if (toCompare == null)
        //    {
        //        return false;
        //    }

        //    if (!Object.Equals(this.EntityTypeID, toCompare.EntityTypeID))
        //        return false;
        //    if (!Object.Equals(this.AttributeID, toCompare.AttributeID))
        //        return false;
        //    if (EntityTypeID == this.EntityTypeID && AttributeID == this.AttributeID)
        //        return true;
        //    return false;
        //}

        //public override int GetHashCode()
        //{
        //    int hashCode = 13;
        //    hashCode = (hashCode * 7) + EntityTypeID.GetHashCode();
        //    hashCode = (hashCode * 7) + AttributeID.GetHashCode();
        //    return hashCode;
        //}

        #endregion

        #region Constructors
        public AttributeGroupAttributeRelationDao() { }

        public AttributeGroupAttributeRelationDao(int pID, int pAttributeGroupId, int pAttributeId, string pCaption, int pSortOrder, bool pIsSearchable, bool pShowAsColumn)
        {
            this._id = pID;
            this._attributegroupid = pAttributeGroupId;
            this._attributeid = pAttributeId;
            this._caption = pCaption;
            this._sortorder = pSortOrder;
            this._issearchable = pIsSearchable;
            this._showascolumn = pShowAsColumn;
        }

        public AttributeGroupAttributeRelationDao(int pID)
        {
            this._id = pID;
        }

        #endregion

        #region Public Properties
        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }
        }
        public virtual int AttributeGroupID
        {
            get { return _attributegroupid; }
            set { _bIsChanged |= (_attributegroupid != value); _attributegroupid = value; }

        }

        public virtual int AttributeID
        {
            get { return _attributeid; }
            set { _bIsChanged |= (_attributeid != value); _attributeid = value; }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        public virtual string Caption
        {
            get { return _caption; }
            set { _bIsChanged |= (_caption != value); _caption = value; }

        }

        public virtual int SortOrder
        {
            get { return _sortorder; }
            set { _bIsChanged |= (_sortorder != value); _sortorder = value; }

        }

        public virtual bool IsSearchable
        {
            get { return _issearchable; }
            set { _issearchable = value; }
        }

        public virtual bool ShowAsColumn
        {
            get { return _showascolumn; }
            set { _showascolumn = value; }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AttributeGroupAttributeRelationDao = "AttributeGroupAttributeRelationDao";
            public const string ID = "ID";
            public const string AttributeGroupID = "AttributeGroupID";
            public const string AttributeID = "AttributeID";
            public const string Caption = "Caption";
            public const string SortOrder = "SortOrder";
            public const string IsSearchable = "IsSearchable";
            public const string ShowAsColumn = "ShowAsColumn";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AttributeGroupAttributeRelationDao = "AttributeGroupAttributeRelationDao";
            public const string ID = "ID";
            public const string AttributeGroupID = "AttributeGroupID";
            public const string AttributeID = "AttributeID";
            public const string Caption = "Caption";
            public const string SortOrder = "SortOrder";
            public const string IsSearchable = "IsSearchable";
            public const string ShowAsColumn = "ShowAsColumn";
        }

        #endregion
    }
}
