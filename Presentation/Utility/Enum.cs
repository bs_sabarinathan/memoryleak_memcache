﻿ 

namespace Presentation.Utility
{
    public enum Operation
    {
        //Read = 1,
        View = 1,
        Edit = 2,
        Delete = 4,
        Create = 8,
        Self = 16,
        Simulate = 32,
        Allow = 64
    }

    public enum Modules
    {
        Common = 1,
        Admin = 2,
        Planning = 3,
        UserAccess = 4

    }

    public enum Feature
    {
        GanttView = 1,
        ListView = 2,
        Report = 3,
        Calender = 4,
        NewsFeed = 5,
        Notification = 6,
        Financial = 7,
        Workflow = 8,
        Member = 9,
        Objective = 10,
        Attachment = 11,
        Presentation = 12,
        Milestone = 13,
        Metadata = 14,
        StartEndDate = 15,
        User = 16,
        SimulateUser = 17,
        DynamicEntity = 18

    }
}