﻿using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Presentation.Controllers
{
    public class CMSController : ApiController
    {
        ServiceResponse result;
        StreamReader reader;
        // GET api/<controller>
        [Route("api/CMS/InsertCMSEntity")]
        [HttpPost]
        public ServiceResponse InsertCMSEntity([FromBody]JObject jobject)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;

                //int version=(int)jobject["CmsEntityObj"]["Version"];
                result.Response = marcomManager.CmsManager.InsertCMSEntity((int)jobject["CmsEntityObj"]["Version"], (string)jobject["CmsEntityObj"]["Description"], (int)jobject["CmsEntityObj"]["Level"], (string)jobject["CmsEntityObj"]["Name"], (int)jobject["CmsEntityObj"]["NavID"], (int)jobject["CmsEntityObj"]["ParentID"],
                        (string)jobject["CmsEntityObj"]["PublishedDate"], (string)jobject["CmsEntityObj"]["PublishedTime"], (int)jobject["CmsEntityObj"]["TemplateID"], (string)jobject["CmsEntityObj"]["UniqueKey"], (string)jobject["CmsEntityObj"]["Tag"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/CMS/GetAllCmsEntitiesByNavID/{NavID}/{StartpageNo}/{MaxPageNo}")]
        [HttpGet]
        public ServiceResponse GetAllCmsEntitiesByNavID(int NavID, int StartpageNo, int MaxPageNo)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.GetAllCmsEntitiesByNavID((int)NavID, (int)StartpageNo, (int)MaxPageNo);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/CMS/DeleteCmsEntity/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteCmsEntity(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.DeleteCmsEntity(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;

        }
        [Route("api/CMS/InsertRevisedEntityContent")]
        [HttpPost]
        public ServiceResponse InsertRevisedEntityContent([FromBody]JObject jobject)
        {
            result = new ServiceResponse();

            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.InsertRevisedEntityContent((int)jobject["EntityID"], (string)jobject["Content"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/CMS/UpdateRevisedEntityContent")]
        [HttpPost]
        public ServiceResponse UpdateRevisedEntityContent([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.UpdateRevisedEntityContent((int)jobj["ReviseEntityObj"]["EntityID"], (string)jobj["ReviseEntityObj"]["Content"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/CMS/DeleteRevisedEntityContentID/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteRevisedEntityContentID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.DeleteRevisedEntityContentID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/CMS/GetRevisedContentByFeature/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetRevisedContentByFeature(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.GetRevisedContentByFeature(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/CMS/GetCmsEntityAttributeDetails/{CmsEntityID}")]
        [HttpGet]
        public ServiceResponse GetCmsEntityAttributeDetails(int CmsEntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.GetCmsEntityAttributeDetails(CmsEntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;

        }
        [Route("api/CMS/UpdateCmsEntityDetailsBlockValues")]
        [HttpPost]
        public ServiceResponse UpdateCmsEntityDetailsBlockValues([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.UpdateCmsEntityDetailsBlockValues((int)jobj["CmsEntityID"], (string)jobj["newValue"], (string)jobj["attrID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/CMS/SaveUploaderImage")]
        [HttpPost]
        public ServiceResponse SaveUploaderImage([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.SaveUploaderImage((string)jobj["CmsFileUploader"]["imgsourcepath"], (string)jobj["CmsFileUploader"]["imgDestinationfolder"], (int)jobj["CmsFileUploader"]["imgwidth"], (int)jobj["CmsFileUploader"]["imgheight"], (int)jobj["CmsFileUploader"]["imgX"], (int)jobj["CmsFileUploader"]["imgY"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/CMS/SaveCMSEntityColor")]
        [HttpPost]
        public ServiceResponse SaveCMSEntityColor([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.SaveCMSEntityColor((string)jobj["CmsEntityColor"]["Shortdescription"], (string)jobj["CmsEntityColor"]["taskflagColorCodeGlobal"], marcomManager.User.TenantID);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/CMS/GetCMSBreadCrum/{ID}")]
        [HttpGet]
        public ServiceResponse GetCMSBreadCrum(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.GetCMSBreadCrum(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/CMS/GetCmsEntityPageAccess/{CmsEntityID}")]
        [HttpGet]
        public ServiceResponse GetCmsEntityPageAccess(int CmsEntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.GetCmsEntityPageAccess(CmsEntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;

        }
        [Route("api/CMS/UpdateCmsEntityPageAccess")]
        [HttpPost]
        public ServiceResponse UpdateCmsEntityPageAccess([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray jarr = (JArray)jobj["CmsEntityPageAccess"]["RoleID"];
                int[] entityidList = jarr.Select(jv => (int)jv["RoleID"]).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.UpdateCmsEntityPageAccess(entityidList, (int)jobj["CmsEntityPageAccess"]["CmsEntityID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/CMS/GetCmsEntityPublishVersion/{CmsEntityID}")]
        [HttpGet]
        public ServiceResponse GetCmsEntityPublishVersion(int CmsEntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.GetCmsEntityPublishVersion(CmsEntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;

        }

        [Route("api/CMS/GettingCmsFeedsByEntityID/{CmsEntityID}/{pageNo}/{IsRealTime}/{grouplist}")]
        [HttpGet]
        public ServiceResponse GettingCmsFeedsByEntityID(int CmsEntitiyID, int pageNo, bool IsRealTime, string grouplist)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.GettingCmsFeedsByEntityID((CmsEntitiyID.ToString()), pageNo, IsRealTime, CmsEntitiyID, 0, grouplist);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/CMS/InsertCmsFeed")]
        [HttpPost]
        public ServiceResponse InsertCmsFeed([FromBody]JObject jobject)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.SaveUpdateFeed((int)jobject["CmsNewsFeed"]["Actor"], (int)jobject["CmsNewsFeed"]["TemplateID"], (int)jobject["CmsNewsFeed"]["EntityID"], (string)jobject["CmsNewsFeed"]["TypeName"], (string)jobject["CmsNewsFeed"]["AttributeName"], (string)jobject["CmsNewsFeed"]["FromValue"], (string)jobject["CmsNewsFeed"]["ToValue"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/CMS/IsActiveEntity/{CmsEntityID}")]
        [HttpGet]
        public ServiceResponse IsActiveEntity(int CmsEntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.IsActiveEntity(CmsEntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/CMS/DuplicateCmsEntities")]
        [HttpPost]
        public ServiceResponse DuplicateCmsEntities([FromBody]JObject jobject)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrentitynames = (JArray)jobject["CmsEntityDupobj"]["dupEntityName"];
                List<string> listEntityNamesToDuplicate = new List<string>();

                if (arrentitynames != null)
                {
                    foreach (var arm in arrentitynames)
                    {
                        listEntityNamesToDuplicate.Add(arm.ToString());
                    }
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.DuplicateEntity((int)jobject["CmsEntityDupobj"]["CmsEntityID"], (int)jobject["CmsEntityDupobj"]["ParentID"], (int)jobject["CmsEntityDupobj"]["DuplicateTimes"], (bool)jobject["CmsEntityDupobj"]["IsDuplicateChild"], listEntityNamesToDuplicate);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/CMS/GetCmsEntitiesByID/{CmsEntityID}")]
        [HttpGet]
        public ServiceResponse GetCmsEntitiesByID(int CmsEntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                string strIDs = CmsEntityID.ToString();
                int[] CmsEntityID1 = Array.ConvertAll(strIDs.Split(','), int.Parse);
                result.Response = marcomManager.CmsManager.GetCmsEntitiesByID(CmsEntityID1);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/CMS/GetIsEditFeatureEnabled")]
        [HttpGet]
        public ServiceResponse GetIsEditFeatureEnabled()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.GetIsEditFeatureEnabled();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }


        [Route("api/CMS/GetAllCmsSnippetTemplates")]
        [HttpGet]
        public ServiceResponse GetAllCmsSnippetTemplates()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.GetAllCmsSnippetTemplates();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;


        }
        //InsertUpdateCmsSnippetTemplate
        [Route("api/CMS/InsertCmsSnippetTemplate")]
        [HttpPost]
        public ServiceResponse InsertCmsSnippetTemplate([FromBody]JObject jobject)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.InsertCmsSnippetTemplate((int)jobject["Id"], (string)jobject["snippet"], (bool)jobject["active"], (string)jobject["thumnailguid"], (string)jobject["templateHTML"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }


        [Route("api/CMS/DeleteCmsSnippetTemplate/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteCmsSnippetTemplate(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.DeleteCmsSnippetTemplate(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/CMS/LinkpublishedfilestoCMS")]
        [HttpPost]
        public ServiceResponse LinkpublishedfilestoCMS([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrFilesObj = (JArray)jobj["AssetArr"];
                int[] assetIdArr = arrFilesObj.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.LinkpublishedfilestoCMS(assetIdArr, (int)jobj["EntityID"], (int)jobj["FolderID"], (string)jobj["AttachType"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/CMS/UpdateSnippetTemplate")]
        [HttpPost]
        public ServiceResponse UpdateSnippetTemplate([FromBody]JObject jobject)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.UpdateSnippetTemplate((int)jobject["Id"], (string)jobject["snippet"], (bool)jobject["active"], (string)jobject["thumnailguid"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        //InsertUpdateCmsSnippetTemplate
        [Route("api/CMS/DuplicateCmsSnippetTemplate")]
        [HttpPost]
        public ServiceResponse DuplicateCmsSnippetTemplate([FromBody]JObject jobject)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request,Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CmsManager.DuplicateCmsSnippetTemplate((string)jobject["content"], (string)jobject["defaultFirstContent"], (string)jobject["defaultLastContent"], (string)jobject["thumnailguid"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

    }
}