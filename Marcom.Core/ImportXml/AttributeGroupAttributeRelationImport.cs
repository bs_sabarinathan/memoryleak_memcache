﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.ImportXml
{
    internal class AttributeGroupAttributeRelationImport
    {
        #region Member Variables
        protected int _id;
        protected int _newid;
        protected int _attributegroupid;
        protected string _caption;
        protected int _attributeid;
        protected int _sortorder;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;

        #endregion

        #region Constructors
        public AttributeGroupAttributeRelationImport() { }

        public AttributeGroupAttributeRelationImport(int pID, int pAttributeGroupId, int pAttributeId, string pCaption, int pNewId)
        {
            this._id = pID;
            this._attributegroupid = pAttributeGroupId;
            this._attributeid = pAttributeId;
            this._caption = pCaption;
            this._newid = pNewId;

        }

        public AttributeGroupAttributeRelationImport(int pID)
        {
            this._id = pID;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public int NewId
        {
            get { return _newid; }
            set { _newid = value; }

        }

        public int AttributeGroupID
        {
            get { return _attributegroupid; }
            set { _attributegroupid = value; }
        }

        public int AttributeID
        {
            get { return _attributeid; }
            set { _attributeid = value; }
        }

        public virtual string Caption
        {
            get { return _caption; }
            set { _caption = value; }
        }

        public int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }
        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            AttributeGroupAttributeRelationImport castObj = null;
            try
            {
                castObj = (AttributeGroupAttributeRelationImport)obj;
            }
            catch (Exception) { return false; }
            return castObj.GetHashCode() == this.GetHashCode();
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {

            return this.GetType().FullName.GetHashCode();

        }
        #endregion
    }
}
