﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Report.Interface;
using System.Security.Cryptography;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Report
{
    // <summary>
    /// IUser interface for table 'DataView'.
    /// Database :DevExpressReportServer
    /// </summary>
    public class DataView : IDataView, ICloneable
    {
        #region Member Variables

		protected int _dataviewID;
		protected string _dataviewname;
		

		#endregion
		
		#region Constructors
		public DataView() {}

        public DataView(string pdataviewname)
		{
            this._dataviewname = pdataviewname; 
			
		}

        public DataView(int pdataviewID)
		{
            this._dataviewID = pdataviewID; 
		}
		
		#endregion
		
		#region Public Properties

        public int DataviewID
		{
            get { return _dataviewID; }
            set { _dataviewID = value; }
			
		}

        public string Dataviewname
		{
            get { return _dataviewname; }
			set 
			{
			  _dataviewname = value; 
			}
			
		}
		
				#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            DataView castObj = null;
			try
			{
                castObj = (DataView)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
                (this._dataviewID == castObj.DataviewID);
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57;
            hash = 27 * hash * _dataviewID.GetHashCode();
			return hash; 
		}

      
		#endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }
}
