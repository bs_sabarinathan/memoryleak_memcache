using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IEntityPeriod interface for table 'PM_EntityPeriod'.
    /// </summary>
    public interface IEntityPeriod
    {
        #region Public Properties


        int Id
        {
            get;
            set;

        }
        
        int Entityid
        {
            get;
            set;

        }

        DateTime Startdate
        {
            get;
            set;

        }

        DateTime EndDate
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }

        int SortOrder
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
