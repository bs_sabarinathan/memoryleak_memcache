using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
	/// FileDefaultInfoDao object for table 'CM_FileDefaultInfo'.
	/// </summary>
	
	public partial class FileDefaultInfoDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected string _property;
		protected string _value;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion

        #region Extensibility Method Definitions

        public override bool Equals(object obj)
        {
            FileDefaultInfoDao toCompare = obj as FileDefaultInfoDao;
            if (toCompare == null)
            {
                return false;
            }

            if (!Object.Equals(this.Id, toCompare.Id))
                return false;
            if (!Object.Equals(this.Property, toCompare.Property))
                return false;

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 13;
            hashCode = (hashCode * 7) + Id.GetHashCode();
            hashCode = (hashCode * 7) + Property.GetHashCode();
            return hashCode;
        }

        #endregion
		
		#region Constructors
		public FileDefaultInfoDao() {}
			
		public FileDefaultInfoDao(int pId, string pProperty, string pValue)
		{
			this._id = pId; 
			this._property = pProperty; 
			this._value = pValue; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual string Property
		{
			get { return _property; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Property", "Property value, cannot contain more than 50 characters");
			  _bIsChanged |= (_property != value); 
			  _property = value; 
			}
			
		}
		
		public virtual string Value
		{
			get { return _value; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("Value", "Value value, cannot contain more than 250 characters");
			  _bIsChanged |= (_value != value); 
			  _value = value; 
			}
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 		
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string FileDefaultInfoDao = "FileDefaultInfoDao";			
			public const string Id = "Id";			
			public const string Property = "Property";			
			public const string Value = "Value";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string FileDefaultInfoDao = "FileDefaultInfoDao";			
			public const string Id = "Id";			
			public const string Property = "Property";			
			public const string Value = "Value";			
		}

		#endregion
		
		
	}
	
}
