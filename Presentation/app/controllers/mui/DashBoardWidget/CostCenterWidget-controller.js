﻿(function(ng, app) {
	"use strict";

	function muiDashBoardWidgetCostCenterWidgetCtrl($scope, $timeout, $http, $compile, $resource, $cookies, $location, $window, WidgetcostcentreService) {
		$scope.costCenterchartMetrics = {
			"Assigned Amount": "1",
			"In requests": "2",
			"Approved budgets": "3",
			"Approved allocated amount": "4",
			"Total spent": "5",
			"Available to spent": "6"
		};
		$scope.GlobalChartType = {
			1: "column",
			2: "pie"
		};
		$scope.userimg = parseInt($cookies['UserId']);
		$timeout(function () { drawdetail(); }, 100);
		$scope.CostCenterWidgetContentID = $scope.WizardID;

		function drawdetail() {
			$scope.costCenterchartpoints = [];
			var UserMetrics = $scope.metrics.split(",");
			var visualtype = $scope.visualType;
			WidgetcostcentreService.GetDynamicwidgetContentUserID(parseInt($cookies['UserId'], 10), $scope.WizardTypeID, $scope.WizardRealID, $scope.DimensionID).then(function (GetDynamicwidgetContentUserID) {
				var costCenterResult = GetDynamicwidgetContentUserID.Response;
				var data = [];
				if (costCenterResult != undefined) {
					for (var i = 0; i < costCenterResult.length; i++) {
						var metricsOptionNo = $scope.costCenterchartMetrics[costCenterResult[i].NAME];
						var found = $.inArray(metricsOptionNo, UserMetrics) > -1;
						if (found) {
							if ($scope.GlobalChartType[$scope.visualType] == "column") data.push({
								y: costCenterResult[i].VALUE,
								label: costCenterResult[i].NAME,
								name: costCenterResult[i].NAME
							});
							else data.push({
								y: costCenterResult[i].VALUE,
								legendText: costCenterResult[i].NAME + ": " + costCenterResult[i].VALUE,
								indexLabel: "",
								name: costCenterResult[i].NAME
							});
						}
					}
				}
				$('#' + $scope.CostCenterWidgetContentID + '').attr('data-visualType', $scope.GlobalChartType[$scope.visualType]);
				$('#' + $scope.CostCenterWidgetContentID + '').attr('data-value', JSON.stringify(data));
				$('#' + $scope.CostCenterWidgetContentID + '').attr('data-charttitle', 'Cost Centre Summary');
				RenderBarChart($scope.WizardID);
				$('#' + $scope.CostCenterWidgetContentID + '').parents('.box-content').find('.InProgress').remove();
			});
		};

		function RenderBarChart(WizardID) {
			$('#' + WizardID + '').height($('#' + WizardID + '').parents('.box-content').height() - 20);
			var data = JSON.parse($('#' + WizardID + '').attr('data-value'));
			var charttitle = $('#' + WizardID + '').attr('data-charttitle');
			CanvasJS.addColorSet("finColorSet1", ["#BDF2A1", "#9ECF6E", "#DDDD69", "#4DA3CE", "#B08BEB", "#3EA0DD", "#F5A52A", "#23BFAA", "#FAA586", "#EB8CC6"]);
			if ($('#' + WizardID + '').attr('data-visualType') == "column") {
				var chart = new CanvasJS.Chart(WizardID, {
					title: {
						text: charttitle,
						verticalAlign: "top",
						horizontalAlign: "center",
						fontSize: 15,
						fontFamily: "Calibri",
						fontWeight: "normal",
						fontColor: "black",
						fontStyle: "normal",
						borderThickness: 0,
						borderColor: "black",
						cornerRadius: 0,
						backgroundColor: null,
						margin: 0
					},
					animationEnabled: true,
					colorSet: "finColorSet1",
					height: $('#' + WizardID + '').parents('.box-content').height() - 10,
					axisX: {
						tickColor: "white",
						tickLength: 5,
						labelFontFamily: "sans-serif",
						labelFontColor: "#7F7F7F",
						labelFontSize: 10,
						labelFontWeight: "normal"
					},
					axisY: {
						tickLength: 0,
						labelFontSize: 0,
						lineThickness: 0,
						gridThickness: 0,
						includeZero: true,
					},
					data: [{
						type: "column",
						indexLabel: "{y}",
						indexLabelFontSize: 12,
						indexLabelFontWeight: "normal",
						indexLabelPlacement: "outside",
						indexLabelOrientation: "horizontal",
						indexLabelFontFamily: "sans-serif",
						indexLabelFontColor: "darkgrey",
						legendMarkerColor: "gray",
						showInLegend: false,
						toolTipContent: "{name}:<strong>{y}</strong>",
						dataPoints: data
					}]
				});
			} else {
				var chart = new CanvasJS.Chart(WizardID, {
					title: {
						text: charttitle,
						verticalAlign: "top",
						horizontalAlign: "center",
						fontSize: 15,
						fontFamily: "Calibri",
						fontWeight: "normal",
						fontColor: "black",
						fontStyle: "normal",
						borderThickness: 0,
						borderColor: "black",
						cornerRadius: 0,
						backgroundColor: null,
						margin: 0
					},
					animationEnabled: true,
					height: $('#' + WizardID + '').parents('.box-content').height() - 10,
					legend: {
						verticalAlign: "center",
						horizontalAlign: "right",
						fontSize: 12,
						fontweight: "normal",
						padding: 0,
						fontColor: "darkgrey",
						fontFamily: "sans-serif"
					},
					data: [{
						indexLabelFontSize: 12,
						indexLabelLineThickness: 0,
						indexLabelFontStyle: "italic",
						indexLabelFontWeight: "normal",
						indexLabelFontFamily: "sans-serif",
						indexLabelFontColor: "darkgrey",
						indexLabelLineColor: "darkgrey",
						indexLabelPlacement: "outside",
						type: "pie",
						showInLegend: true,
						toolTipContent: "{name}:<strong>{y}</strong>",
						dataPoints: data
					}]
				});
			}
			chart.render();
		}
		$(document).on("OnChartLoad", function(event, setChartID) {
			if (setChartID != undefined) {
				$('#' + setChartID + '').html('');
				RenderBarChart(setChartID);
			}
		});
	
		$scope.$on("$destroy", function() {
			RecursiveUnbindAndRemove($("[ng-controller='mui.DashBoardWidget.CostCenterWidgetCtrl']"));
		});
	}
	app.controller("mui.DashBoardWidget.CostCenterWidgetCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$location', '$window', 'WidgetcostcentreService', muiDashBoardWidgetCostCenterWidgetCtrl]);
	function WidgetcostcentreService($http, $q) {
	    $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
	        GetDynamicwidgetContentUserID: GetDynamicwidgetContentUserID
	    });
	    function GetDynamicwidgetContentUserID(UserID, WidgetTypeID, WidgetID, DimensionID) { var request = $http({ method: "get", url: "api/common/GetDynamicwidgetContentUserID/" + UserID + "/" + WidgetTypeID + "/" + WidgetID + "/" + DimensionID, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
	    function handleError(response) {
	        if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
	        return ($q.reject(response.data.message));
	    }
	    function handleSuccess(response) { return (response.data); }
	}
	app.service("WidgetcostcentreService", ['$http', '$q', WidgetcostcentreService]);
})(angular, app);