﻿(function (ng, app) {
    "use strict";

  function muiplanningtoolcostcentredetailsectionpresentationCtrl($scope, $timeout, $http, $compile, $window, $resource, $stateParams, PresentationService) {
        $scope.date2 = new Date();
        $scope.presentationList = [];
        $scope.SelectionList = [];
        var routeid = $stateParams.ID;
        if ($scope.IsLock == false) {
            $scope.UnShowEditPage = true;
            $scope.UnShowSave = false;
        } else {
            $scope.UnShowEditPage = false;
            $scope.UnShowSave = false;
        }
        this.setSelected = function (c) {
            var checkopt = $.grep($scope.SelectionList, function (e) {
                return e == c.id;
            });
            if (c.checked1 == true && checkopt.length == 0) {
                $scope.SelectionList.push(c.id);
            } else if (c.checked1 == false && checkopt.length > 0) {
                $scope.SelectionList.splice($.inArray(c.id, $scope.SelectionList), 1);
            }
        }
        $scope.toggledate = function () {
            $scope.isDateteVisible = !$scope.isDateteVisible;
        };
        $scope.isDateteVisible = false;
        $scope.toggletext = "Show publication for sub levels";
        $scope.isVisible = false;
        $scope.EnableEdit = {
            EditStatus: true,
            EditText: "Edit"
        };
        $scope.editPage = function () {
            if ($scope.EnableEdit.EditStatus == true) {
                $scope.EnableEdit.EditStatus = false;
                $scope.EnableEdit.EditText = "Close";
                $scope.showEditor = true;
                $scope.showHolder = false;
                $scope.htmlEditorText = $scope.presentationList.Content;
            } else {
                $scope.EnableEdit.EditStatus = true;
                $scope.EnableEdit.EditText = "Edit";
                $scope.showEditor = false;
                $scope.showHolder = false;
                $scope.htmlEditorText = $scope.presentationList.Content;
            }
            $scope.UnShowEditPage = false;
            $scope.UnShowSave = true;
        };
        $scope.myTree = [];
        loadPresentation();
        $scope.$on('LoadPresentations', function (event, ID) {
            $scope.myTree = [];
            $('#EditorHolder').html('');
            routeid = ID;
            loadPresentation();
        });

        function loadPresentation() {
            PresentationService.GetEntitydescendants(routeid).then(function (GetTree) {
                $scope.myTree.push(JSON.parse(GetTree.Response));
            });
            PresentationService.GetPresentationById(routeid).then(function (EntityPresentationlist) {
                if (EntityPresentationlist.Response != null) {
                    $scope.showEditor = false;
                    if (EntityPresentationlist.Response != null) {
                        $scope.presentationList = EntityPresentationlist.Response;
                        $('#EditorHolder').html($scope.presentationList.Content);
                    }
                    $scope.showHolder = true;
                }
            });
        }
        $scope.SaveEntityPresentationContent = function () {
            var presentationObj = {};
            presentationObj.EntityID = routeid;
            presentationObj.entityList = $scope.SelectionList;
            presentationObj.PublishedOn = $scope.date2;
            if ($scope.htmlEditorText == undefined) {
                presentationObj.Content = null;
            } else {
                presentationObj.Content = $scope.htmlEditorText;
            }
            PresentationService.InsertPresentation(presentationObj).then(function () {
                $('#userModal').modal('hide');
                $scope.showEditor = false;
                $scope.showHolder = true;
                $scope.UnShowEditPage = true;
                $scope.UnShowSave = false;
                $('#EditorHolder').html($scope.htmlEditorText);
                $scope.EnableEdit.EditStatus = true;
            });
        };
        $scope.$on("$destroy", function () {
             RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.detail.section.presentationCtrl']"));
        });
    }
      app.controller("mui.planningtool.costcentre.detail.section.presentationCtrl", ['$scope', '$timeout', '$http', '$compile', '$window', '$resource', '$stateParams', 'PresentationService', muiplanningtoolcostcentredetailsectionpresentationCtrl]);
})(angular, app);