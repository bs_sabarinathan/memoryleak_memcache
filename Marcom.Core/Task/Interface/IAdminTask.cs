﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Core.Access.Interface;
using System.Linq;
using BrandSystems.Marcom.Dal.Task.Model;

namespace BrandSystems.Marcom.Core.Task.Interface
{
    public interface IAdminTask
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }



        string Description
        {
            get;
            set;
        }

        string Caption
        {
            get;
            set;

        }

        int TaskListID
        {
            get;
            set;

        }

        int TaskType
        {
            get;
            set;

        }


        int SortOder
        {
            get;
            set;

        }

        string TaskTypeName { get; set; }
        int TotalTaskAttachment { get; set; }
        dynamic AttributeData { get; set; }
        string AttachmentCollection { get; set; }
        IList<AdminTaskCheckListDao> AdminTaskCheckList { get; set; }
        string ColorCode { get; set; }
        string ShortDesc { get; set; }
        #endregion
    }
}
