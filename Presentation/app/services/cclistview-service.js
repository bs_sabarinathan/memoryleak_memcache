﻿(function(ng,app){"use strict";function CclistviewService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({CostCentreDetail:CostCentreDetail});function CostCentreDetail(formobj){var request=$http({method:"post",url:"api/Metadata/CostCentreDetail/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
    app.service("CclistviewService", ['$http', '$q', CclistviewService]);
})(angular, app);