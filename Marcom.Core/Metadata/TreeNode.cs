/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

	/// <summary>
    /// Module object for table 'MM_TreeNode'.
	/// </summary>
	
	public class TreeNode : ITreeNode
	{
		#region Member Variables

		protected int _id;
        protected int _nodeid;
		protected int _parentpodeid;
		protected int _level;
        protected string _key;
		protected int _attributeid;
		protected string _caption;
        protected int _sortOrder;
        protected string _colorcode;
		
		#endregion
		
		#region Constructors
        public TreeNode() { }

        public TreeNode(int pId, int pNodeid, int pParentnodeid, int pLevel, string pKEY, int pAttributeid, string pCaption, int pSortOrder,string pColorCode)
		{
			this._id = pId; 
            this._nodeid = pNodeid;
			this._level = pLevel;
			this._key = pKEY;
            this._parentpodeid = pParentnodeid;
			this._attributeid = pAttributeid;
            this._caption = pCaption;
            this._sortOrder = pSortOrder;
            this._colorcode = pColorCode;
		}

        public TreeNode(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int NodeID
        {
            get { return _nodeid; }
            set { _nodeid = value; }

        }
        public virtual int ParentNodeID
        {
            get { return _parentpodeid; }
            set { _parentpodeid = value; }

        }
        public virtual int Level
        {
            get { return _level; }
            set { _level = value; }

        }

        public virtual string KEY
        {
            get { return _key; }
            set { _key = value; }

        }

        public virtual string ColorCode
        {
            get { return _colorcode; }
            set { _colorcode = value; }

        }

        public virtual int AttributeID
        {
            get { return _attributeid; }
            set { _attributeid = value; }

        }

        public virtual string Caption
        {
            get { return _caption; }
            set { _caption = value; }

        }

        public virtual int SortOrder
        {
            get { return _sortOrder; }
            set { _sortOrder = value; }

        }

        public List<TreeNode> Children { get; set; }
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
        public override bool Equals(object obj)
		{
            if (this == obj) return true;
			Module castObj = null;
			try
			{
				castObj = (Module)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}

    public class UITreeNode
    {

        public string Caption { get; set; }
        public string Description { get; set; }
        public int Level { get; set; }
        public int id { get; set; }
        public int AttributeId { get; set; }
        public bool IsDeleted { get; set; }
        public int SortOrder { get; set; }
        public bool ischecked { get; set; }
        public bool isShow { get; set; }
        public string Key { get; set; }
        public string ColorCode { get; set; }
        public List<UITreeNode> Children { get; set; }
    }

    [Serializable]
    public class UIEntityTypeTreeNode
    {
        public string Caption { get; set; }
        public int Id { get; set; }
        public List<UIEntityTypeTreeNode> Children { get; set; }

        public static T DeepClone<T>(T a)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, a);
                stream.Position = 0;
                return (T)formatter.Deserialize(stream);
            }
        }
    }


	
}
