using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// NotificationType object for table 'CM_NotificationType'.
	/// </summary>
	
	internal class NotificationType : INotificationType 
	{
		#region Member Variables

		protected int _id;
		protected string _caption;
		protected ISubscriptionType _subscriptiontypeid;
		protected string _template;
		
		
		#endregion
		
		#region Constructors
		public NotificationType() {}
			
		public NotificationType(string pCaption, SubscriptionType pSubscriptionTypeid, string pTemplate)
		{
			this._caption = pCaption; 
			this._subscriptiontypeid = pSubscriptionTypeid; 
			this._template = pTemplate; 
		}
				
		public NotificationType(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
			  _caption = value; 
			}
			
		}
		
		public ISubscriptionType SubscriptionTypeid
		{
			get { return _subscriptiontypeid; }
			set { _subscriptiontypeid = value; }
			
		}
		
		public string Template
		{
			get { return _template; }
			set 
			{
			  if (value != null && value.Length > 1000)
			    throw new ArgumentOutOfRangeException("Template", "Template value, cannot contain more than 1000 characters");
			  _template = value; 
			}
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			NotificationType castObj = null;
			try
			{
				castObj = (NotificationType)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
