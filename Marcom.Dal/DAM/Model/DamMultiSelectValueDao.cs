﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
    /// <summary>
    /// EntityTypeFeatureDao object for table 'MM_DAM_MultiSelectValue'.
    /// </summary>

    public partial class DamMultiSelectValueDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _assetid;
        protected int _attributeid;
        protected int _optionid;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        //#region Extensibility Method Definitions

        //public override bool Equals(object obj)
        //{
        //    MultiSelectDao toCompare = obj as MultiSelectDao;
        //    if (toCompare == null)
        //    {
        //        return false;
        //    }

        //    if (!Object.Equals(this.Attributeid, toCompare.Attributeid))
        //        return false;
        //    if (!Object.Equals(this.Entityid, toCompare.Entityid))
        //        return false;
        //    if (!Object.Equals(this.Optionid, toCompare.Optionid))
        //        return false;         


        //    return true;
        //}

        //public override int GetHashCode()
        //{
        //    int hashCode = 13;
        //    hashCode = (hashCode * 7) + Entityid.GetHashCode();
        //    hashCode = (hashCode * 7) + Attributeid.GetHashCode();
        //    hashCode = (hashCode * 7) + Optionid.GetHashCode();
        //    return hashCode;
        //}

        //#endregion

        #region Constructors
        public DamMultiSelectValueDao() { }

        public DamMultiSelectValueDao(int pId, int pAssetID, int pAttributeid, int pOptionid)
        {
            this._id = pId;
            this._assetid = pAssetID;
            this._optionid = pOptionid;
            this._attributeid = pAttributeid;
        }


        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int AssetID
        {
            get { return _assetid; }
            set { _bIsChanged |= (_assetid != value); _assetid = value; }

        }
        public virtual int AttributeID
        {
            get { return _attributeid; }
            set { _bIsChanged |= (_attributeid != value); _attributeid = value; }

        }
        public virtual int OptionID
        {
            get { return _optionid; }
            set { _bIsChanged |= (_optionid != value); _optionid = value; }

        }


        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string ID = "ID";
            public const string DamMultiSelectValueDao = "DamMultiSelectValueDao";
            public const string AttributeID = "AttributeID";
            public const string AssetID = "AssetID";
            public const string OptionID = "OptionID";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string ID = "ID";
            public const string DamMultiSelectValueDao = "DamMultiSelectValueDao";
            public const string AttributeID = "AttributeID";
            public const string AssetID = "AssetID";
            public const string OptionID = "OptionID";
        }

        #endregion


    }
	
}
