﻿'use strict';

angular.module("app", [
    'ui.select2',
    'ngCookies',
    'ngResource',
    '$strap.directives',
    'ngSanitize',
    'ngGrid',
    'ngStorage',
    'ui.router',
    'ui.bootstrap',
    'ui.utils',
    'ui.load',
    'ui.jq',
    'oc.lazyLoad',
    'pascalprecht.translate'

]);
