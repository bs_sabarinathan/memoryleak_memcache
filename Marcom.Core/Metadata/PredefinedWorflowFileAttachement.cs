﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
   internal class PredefinedWorflowFileAttachement : IPredefinedWorflowFileAttachement
    {
         #region Member Variables

        protected int _id;
        protected int _predefinedid;
        protected string _name;
        protected DateTime _createdon;
        protected Guid _fileguid;

        protected string _extension;
        protected long _size;
        protected string _mimetype;
        protected int _ownerid;
        protected int _versionno;

        #endregion

        #region Constructors

        public PredefinedWorflowFileAttachement() { }

        public PredefinedWorflowFileAttachement(int predefinedid, string pName, DateTime pCreatedon)
        {
            this._predefinedid = predefinedid;
            this._name = pName;
            this._createdon = pCreatedon;
        }

        public PredefinedWorflowFileAttachement(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public int PredefinedID
        {
            get { return _predefinedid; }
            set { _predefinedid = value; }

        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
                _name = value;
            }

        }

        public Guid Fileguid
        {
            get { return _fileguid; }
            set { _fileguid = value; }

        }

        public virtual DateTime Createdon
        {
            get { return _createdon; }
            set { _createdon = value; }

        }

        public string Extension
        {
            get { return _extension; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("Extension", "Extension value, cannot contain more than 100 characters");
                _extension = value;
            }

        }

        public long Size
        {
            get { return _size; }
            set { _size = value; }

        }

        public string MimeType
        {
            get { return _mimetype; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("MimeType", "MimeType value, cannot contain more than 500 characters");
                _mimetype = value;
            }

        }

        public int OwnerID
        {
            get { return _ownerid; }
            set { _ownerid = value; }

        }

        public int VersionNo
        {
            get { return _versionno; }
            set { _versionno = value; }

        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            PredefinedWorflowFileAttachement castObj = null;
            try
            {
                castObj = (PredefinedWorflowFileAttachement)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.ID);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}
