﻿/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.ImportXml
{

    /// <summary>
    /// DamTypeFileExtension object for table 'MM_DamType_FileExtension'.
    /// </summary>

    internal class DamTypeFileExtensionImport
    {
        #region Member Variables

        protected int _id;
        protected int _newid;
        protected int _entitytypeid;
        protected string _ExtensionOptions;
        protected int _sortorder;
        protected bool _isdeleted;
        #endregion

        #region Constructors
        public DamTypeFileExtensionImport() { }

        public DamTypeFileExtensionImport(int pID, int pEntityTypeID, string pExtensionOptions, int pSortOrder, bool IsRemoved)
        {
            this._id = pID;
            this._entitytypeid = pEntityTypeID;
            this._ExtensionOptions = pExtensionOptions;
            this._sortorder = pSortOrder;
            this._isdeleted = IsRemoved;
        }

        public DamTypeFileExtensionImport(int pID)
        {
            this._id = pID;
        }

        #endregion

        #region Public Properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public int NewId
        {
            get { return _newid; }
            set { _newid = value; }

        }

        public int EntityTypeID
        {
            get { return _entitytypeid; }
            set
            {
                _entitytypeid = value;
            }

        }

        public string ExtensionOptions
        {
            get { return _ExtensionOptions; }
            set
            {
                _ExtensionOptions = value;
            }

        }
        public int SortOrder
        {
            get { return _sortorder; }
            set
            {
                _sortorder = value;
            }

        }
        public bool IsRemoved
        {
            get { return _isdeleted; }
            set
            {
                _isdeleted = value;
            }

        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            DamTypeFileExtensionImport castObj = null;
            try
            {
                castObj = (DamTypeFileExtensionImport)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.ID);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
