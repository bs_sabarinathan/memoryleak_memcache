﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace Presentation.Handlers
{
    /// <summary>
    /// Summary description for CustomHandler
    /// </summary>
    public class CustomHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string path = context.Request.QueryString["Path"];
            Decimal chunk = context.Request["chunk"] != null ? Decimal.Parse(context.Request["chunk"]) : 0;
            string fileType = context.Request["typeoffile"] != null ? context.Request["typeoffile"] : string.Empty;
            string fileName = context.Request["name"] != null ? context.Request["name"] : string.Empty;

            //TODO: change as needed for your application

            string uploadPath = context.Server.MapPath("~/" + path);

            //HttpContext.Current.Request.Files[0];
            HttpPostedFile fileUpload = HttpContext.Current.Request.Files[0];

            fileName = Guid.NewGuid().ToString() + new FileInfo(fileName).Extension;

            if (fileType == "Doc")
            {
                using (var fs = new FileStream(Path.Combine(uploadPath, fileName), chunk == 0 ? FileMode.Create : FileMode.Append))
                {
                    var buffer = new byte[fileUpload.InputStream.Length];
                    fileUpload.InputStream.Read(buffer, 0, buffer.Length);

                    fs.Write(buffer, 0, buffer.Length);
                }
                context.Response.Write(fileName);
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}