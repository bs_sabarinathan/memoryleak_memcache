﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IAdditionalObjectiveEntityValues interface for table 'PM_AdditionalObjectiveRating'.
    /// </summary>
   public interface IAddtionalObjectiveRating
    {
       int Id
       {
           get;
           set;
       }
       string Ratings
       {
           get;
           set;
       }
       int ObjectiveId
       {
           get;
           set;
       }
       int SortOrder
       {
           get;
           set;
       }
    }
}
