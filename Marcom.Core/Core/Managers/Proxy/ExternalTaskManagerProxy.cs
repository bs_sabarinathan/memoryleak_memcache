﻿
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Interface.Managers;
using BrandSystems.Marcom.Core.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Managers.Proxy
{
    internal partial class ExternalTaskManagerProxy : IExternalTaskManager, IManagerProxy
    {
        // Reference to the MarcomManager
        private MarcomManager _marcomManager = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommonManagerProxy"/> class.
        /// </summary>
        /// <param name="marcomManager">The marcom manager.</param>
        internal ExternalTaskManagerProxy(MarcomManager marcomManager)
        {
            _marcomManager = marcomManager;

            // Do some initialization.... 
            // i.e. cache logged in user specific things (or maybe use lazy loading for that)
        }

        // Reference to the MarcomManager (only internal)
        /// <summary>
        /// Gets the marcom manager.
        /// </summary>
        /// <value>
        /// The marcom manager.
        /// </value>
        internal MarcomManager MarcomManager
        {
            get { return _marcomManager; }
        }

        public void TestDalimRPCConnector(int taskId, int assetId)
        {
            ExternalTaskManager.Instance.TestDalimRPCConnector(this, taskId, assetId);
        }

        public Dictionary<string, string> GetDocumentViewerInfo(int taskId, string usrname)
        {
            return ExternalTaskManager.Instance.GetDocumentViewerInfo(this, taskId, usrname);
        }

        #region Dalim User call back

        public bool UpdateDalimUserTaskStatus(int DalimID, int dalimstepId, int stepStatus, string stepName)
        {
            return ExternalTaskManager.Instance.UpdateDalimUserTaskStatus(this, DalimID, dalimstepId, stepStatus, stepName);
        }

        //public bool UpdateDalimUserTaskStatus(int DalimID, string userEmail, int stepId, string status, bool isExternalTask = true)
        //{
        //    return ExternalTaskManager.Instance.UpdateDalimUserTaskStatus(this, DalimID, userEmail, stepId, status, isExternalTask);
        //}
        #endregion

        public bool UpdateDalimDocumentFile(int taskId, string filePath)
        {
            return ExternalTaskManager.Instance.UpdateDalimDocumentFile(this, taskId, filePath);
        }

    }
}
