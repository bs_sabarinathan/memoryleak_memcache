using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.User.Model
{

	/// <summary>
    /// UserDao object for table 'UM_ResetPasswordTrack'.
	/// </summary>
	public partial class ResetPasswordDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
        protected Guid _TrackingID;
        protected int _UserID;
        protected DateTime _ResetPasswordDate;
        protected bool _bIsChanged;
        

		#endregion
		
		#region Constructors
		public ResetPasswordDao() {}

        public ResetPasswordDao(int pid, Guid pTrackingID, int pUserID, DateTime pResetPasswordDate)
		{
			this._id = pid ;
            this._TrackingID = pTrackingID;
			this._UserID = pUserID; 
			this._ResetPasswordDate = pResetPasswordDate; 

		}

        public ResetPasswordDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}

        public virtual Guid TrackingID
        {
            get { return _TrackingID; }
            set { _bIsChanged |= (_TrackingID != value); _TrackingID = value; }

        }

        public virtual int UserID
        {
            get { return _UserID; }
            set { _bIsChanged |= (_UserID != value); _UserID = value; }

        }

        public virtual DateTime ResetPasswordDate
        {

            get { return _ResetPasswordDate; }
            set { _bIsChanged |= (_ResetPasswordDate != value); _ResetPasswordDate = value; }

        }
		

		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string ResetPasswordDao = "ResetPasswordDao";			
			public const string Id = "Id";
            public const string TrackingID = "TrackingID";
            public const string UserID = "UserID";
            public const string ResetPasswordDate = "ResetPasswordDate";			

		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string ResetPasswordDao = "ResetPasswordDao";
            public const string Id = "Id";
            public const string TrackingID = "TrackingID";
            public const string UserID = "UserID";
            public const string ResetPasswordDate = "ResetPasswordDate";			
			

		}

		#endregion
		
		
	}
	
}
