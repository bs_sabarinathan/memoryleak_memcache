﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Access.Interface
{
    public interface ISuperAdminDetails
    {

        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int MenuID
        {
            get;
            set;

        }
        int IsTopnavigation
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }
        bool AccessPermission
        {
            get;
            set;

        }

        int FeatureID
        {
            get;
            set;

        }


        #endregion
    }
}
