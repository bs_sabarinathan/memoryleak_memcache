using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// INotificationType interface for table 'CM_NotificationType'.
    /// </summary>
    public interface INotificationType
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }

        ISubscriptionType SubscriptionTypeid
        {
            get;
            set;

        }

        string Template
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
