﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    public interface IAttrGroupDynamicAttributes
    {

        int Id { get; set; }
        int EntityId { get; set; }
        // List<KeyValuePair<string, string>> Attributes { get; set; }
        IDictionary Attributes { get; set; }
    }
}
