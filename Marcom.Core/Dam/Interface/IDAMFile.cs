﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.DAM.Interface
{
    /// <summary>
    /// IFile interface for table 'DM_File'.
    /// </summary>
    public interface IDAMFile
    {
        #region Public Properties

        int ID
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }



        string MimeType
        {
            get;
            set;

        }

        string Extension
        {
            get;
            set;

        }


        long Size
        {
            get;
            set;

        }

        int Ownerid
        {
            get;
            set;

        }

        DateTimeOffset CreatedOn
        {
            get;
            set;

        }

        string Checksum
        {
            get;
            set;

        }



        int AssetID
        {
            get;
            set;

        }

        Guid Fileguid
        {
            get;
            set;

        }



        string OwnerName
        {
            get;
            set;

        }
        int VersionNo
        {
            get;
            set;

        }
        int Status
        { get; set; }
        string Description { get; set; }
        string Additionalinfo { get; set; }
        string StrCreatedDate { get; set; }
        string StrCreatedDateTime { get; set; }
        Boolean Activestatus { get; set; }
        int ProcessType { get; set; }
        #endregion
    }
}
