using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

	/// <summary>
	/// FeedTemplateDao object for table 'CM_Feed_Template'.
	/// </summary>
	
	public partial class FeedTemplateDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected int _moduleid;
		protected int _featureid;
		protected string _template;
        protected string _action;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public FeedTemplateDao() {}
			
		public FeedTemplateDao(int pModuleid, int pFeatureid, string pTemplate,string pAction)
		{
			this._moduleid = pModuleid; 
			this._featureid = pFeatureid; 
			this._template = pTemplate;
            this._action = pAction; 
		}
				
		public FeedTemplateDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual int Moduleid
		{
			get { return _moduleid; }
			set { _bIsChanged |= (_moduleid != value); _moduleid = value; }
			
		}
		
		public virtual int Featureid
		{
			get { return _featureid; }
			set { _bIsChanged |= (_featureid != value); _featureid = value; }
			
		}
		
		public virtual string Template
		{
			get { return _template; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Template", "Template value, cannot contain more than 1073741823 characters");
			  _bIsChanged |= (_template != value); 
			  _template = value; 
			}
			
		}

        public virtual string Action
        {
            get { return _action; }
            set
            {
                _bIsChanged |= (_action != value);
                _action = value;
            }

        }

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 			
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string FeedTemplateDao = "FeedTemplateDao";			
			public const string Id = "Id";			
			public const string Moduleid = "Moduleid";			
			public const string Featureid = "Featureid";			
			public const string Template = "Template";
            public const string Action = "Action";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string FeedTemplateDao = "FeedTemplateDao";			
			public const string Id = "Id";			
			public const string Moduleid = "Moduleid";			
			public const string Featureid = "Featureid";			
			public const string Template = "Template";
            public const string Action = "Action";
		}

		#endregion
		
		
	}
	
}
