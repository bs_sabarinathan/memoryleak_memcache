﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Dal.Common.Model;
using BrandSystems.Marcom.Core.Common.Interface;
using System.Xml.Linq;
using System.IO;
using BrandSystems.Marcom.Core.Metadata.Interface;
using System.Collections;
using Mail;
using BrandSystems.Marcom.Core.Common;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Core.Task.Interface;
using BrandSystems.Marcom.Dal.Task.Model;
using Newtonsoft.Json.Linq;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Core.Report.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Metadata;
using BrandSystems.Marcom.Dal.Access.Model;

namespace BrandSystems.Marcom.Core.Interface.Managers
{
    public interface ICommonManager
    {

        #region Instance of Classes In ServiceLayer reference
        /// <summary>
        /// Returns File class.
        /// </summary>
        IFile Fileservice();

        #endregion

        /// <summary>
        /// Initializes the type of the isubscription.
        /// </summary>
        /// <param name="strBody">The STR body.</param>
        /// <returns>ISubscriptionType</returns>
        ISubscriptionType initializeIsubscriptionType(string strBody);

        /// <summary>
        /// Initializes the inavigation.
        /// </summary>
        /// <param name="strBody">The STR body.</param>
        /// <returns>INavigation</returns>
        INavigation initializeInavigation(string strBody);

        /// <summary>
        /// Initializes the I user mail subscription.
        /// </summary>
        /// <param name="strBody">The STR body.</param>
        /// <returns>IUserMailSubscription</returns>
        IUserMailSubscription initializeIUserMailSubscription(string strBody);

        /// <summary>
        /// Gets the type of all subscription type.
        /// </summary>
        /// <returns>IList</returns>
        IList<ISubscriptionType> GetAllSubscriptionType();

        /// <summary>
        /// Get Widget List for the user
        /// </summary>
        /// <param name="proxy"></param>
        /// <param name="userid"> </param>
        /// <returns>IWidgetContainer </returns>
        IList<IWidget> GetWidgetDetailsByUserID(int userid, bool isAdmin, int GlobalTemplateID);

        /// <summary>
        /// Get Dynamic Widget Content
        /// </summary>
        /// <param name="proxy"></param>
        /// <param name="userid"> </param>
        /// <param name="WidgetId"> </param>
        /// <param name="WidgetTypeID"> </param>
        /// <param name="IsDynamic"> </param>
        /// <returns>List<object> </returns>
        List<object> GetDynamicwidgetContentUserID(int userid, int widgetTypeid, string widgetId, int dimensionid);
        /// <summary>
        /// Gets the user subscription settings.
        /// </summary>
        /// <param name="SubscribtionTypeID">The subscribtion type ID.</param>
        /// <returns>IUserSubscription</returns>
        IUserSubscription GetUserSubscriptionSettings(string SubscribtionTypeID);

        /// <summary>
        /// Updates the user subscription settings.
        /// </summary>
        /// <param name="UserId">The user id.</param>
        /// <param name="subscriptiontype">The subscriptiontype.</param>
        /// <returns>IUserDefaultSubscription</returns>
        bool UpdateUserSubscriptionSettings(int UserId, ISubscriptionType subscriptiontype);

        /// <summary>
        /// Updates the user subscription settings.
        /// </summary>
        /// <param name="UserId">The user id.</param>
        /// <param name="Id">The id.</param>
        /// <returns>IUserDefaultSubscription</returns>
        bool UpdateUserSubscriptionSettings(int UserId, int Id);

        /// <summary>
        /// Gets the notification by ids.
        /// </summary>
        /// <param name="notificationid">The notificationid.</param>
        /// <param name="UserId">The user id.</param>
        /// <returns>IUserNotification</returns>
        Tuple<IList<INotificationSelection>, int, IList> GetNotification(int flag, int pageNo = 0);


        int UpdateIsviewedStatusNotification(int UserId, string ids, int flag);

        int UpdatetopIsviewedStatusNotification();


        bool CheckUserPermissionForEntity(int entityID);
        /// <summary>
        ///Insert Navigation.
        /// </summary>
        /// <param name="navigation">The navigation.</param>
        /// <returns>last inserted id</returns>
        int Navigation_Insert(INavigation navigation);

        /// <summary>
        /// Insert Navigation.
        /// </summary>
        /// <param name="Id">The id.</param>
        /// <param name="Parentid">The parentid.</param>
        /// <param name="Moduleid">The moduleid.</param>
        /// <param name="Featureid">The featureid.</param>
        /// <param name="Caption">The caption.</param>
        /// <param name="Description">The description.</param>
        /// <param name="Url">The URL.</param>
        /// <param name="JavaScript">The java script.</param>
        /// <param name="IsActive">if set to <c>true</c> [is active].</param>
        /// <param name="IsPopup">if set to <c>true</c> [is popup].</param>
        /// <param name="IsIframe">if set to <c>true</c> [is iframe].</param>
        /// <param name="IsDynamicPage">if set to <c>true</c> [is dynamic page].</param>
        /// <param name="IsExternal">if set to <c>true</c> [is external].</param>
        /// <param name="AddUserName">if set to <c>true</c> [add user name].</param>
        /// <param name="AddUserEmail">if set to <c>true</c> [add user email].</param>
        /// <param name="Imageurl">The imageurl.</param>
        /// <param name="GlobalRoleid">The global roleid.</param>
        /// <returns>INavigation</returns>
        int Navigation_Insert(int Id, int Typeid, int Parentid, int Moduleid, int Featureid, string Caption, string Description, string Url, string JavaScript, bool IsActive, bool IsPopup, bool IsIframe, bool IsDynamicPage, bool IsExternal, bool AddUserName, bool AddUserEmail, bool IsDefault, string ExternalUrl, string Imageurl, int GlobalRoleid, bool AddUserID, bool AddLanguageCode, int SearchType);

        /// <summary>
        /// Update Navigation.
        /// </summary>
        /// <param name="Id">The id.</param>
        /// <param name="Parentid">The parentid.</param>
        /// <param name="Moduleid">The moduleid.</param>
        /// <param name="Featureid">The featureid.</param>
        /// <param name="Caption">The caption.</param>
        /// <param name="Description">The description.</param>
        /// <param name="Url">The URL.</param>
        /// <param name="JavaScript">The java script.</param>
        /// <param name="IsActive">if set to <c>true</c> [is active].</param>
        /// <param name="IsPopup">if set to <c>true</c> [is popup].</param>
        /// <param name="IsIframe">if set to <c>true</c> [is iframe].</param>
        /// <param name="IsDynamicPage">if set to <c>true</c> [is dynamic page].</param>
        /// <param name="IsExternal">if set to <c>true</c> [is external].</param>
        /// <param name="AddUserName">if set to <c>true</c> [add user name].</param>
        /// <param name="AddUserEmail">if set to <c>true</c> [add user email].</param>
        /// <param name="Imageurl">The imageurl.</param>
        /// <param name="GlobalRoleid">The global roleid.</param>
        /// <param name="IsDefault">if set to <c>true</c> [is Default].</param>
        /// <param name="IsExternal">if set to <c>true</c> [is External].</param>
        /// <param name="External Url">The External URL.</param>
        /// <returns>
        /// bool 
        /// </returns>
        bool Navigation_Update(int ID, int typeID, int parentId, int moduleid, int featureid, string caption, string Description, string URL, string externalurl, bool IsExternal, bool IsDefault, bool IsEnable, bool AddUserID, bool AddLanguageCode, bool AddUserEmail, bool AddUserName, int SortOrder, int SelectedSearchType);

        /// <summary>
        /// select Navigation.
        /// </summary>
        /// <param name="NavigationID">The navigation ID.</param>
        /// <param name="ParentID">The parent ID.</param>
        /// <returns>IList</returns>
        IList<INavigation> Navigation_Select(bool IsParentID, int UserID, int flag);

        bool UpdateNavigationSortOrder(int Id, int SortOrder);

        /// <summary>
        /// Deletes Navigation.
        /// </summary>
        /// <param name="navigation">The navigation.</param>
        /// <returns>string</returns>
        bool Navigation_Delete(INavigation navigation);

        /// <summary>
        /// Deletes Navigation.
        /// </summary>
        /// <param name="Id">The id.</param>
        /// <returns>bool</returns>
        bool Navigation_Delete(int Id);

        /// <summary>
        /// Gets the group ID for navigation.
        /// </summary>
        /// <param name="NavID">The nav ID.</param>
        /// <param name="UserID">The user ID.</param>
        /// <returns>INavigationAccess</returns>
        INavigationAccess GetGroupIDForNavigation(int NavID, int UserID);

        //subscription settings 
        /// <summary>
        /// Inserts the subscription notificationsettings.
        /// </summary>
        /// <param name="userid">The userid.</param>
        /// <param name="lastsenton">The lastsenton.</param>
        /// <param name="lastupdatedon">The lastupdatedon.</param>
        /// <param name="Timing">The timing.</param>
        /// <param name="IsEmailEnable">if set to <c>true</c> [is email enable].</param>
        /// <param name="DayName">Name of the day.</param>
        /// <param name="RecapReport">if set to <c>true</c> [recap report].</param>
        /// <returns>int</returns>
        int InsertSubscriptionNotificationsettings(int userid, DateTimeOffset lastsenton, DateTimeOffset lastupdatedon, TimeSpan Timing, bool IsEmailEnable = true, string DayName = "Daily", bool RecapReport = true);

        /// <summary>
        /// Updates the recap notificationsettings.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="usrsubscrption">The usrsubscrption.</param>
        /// <returns>IUserMailSubscription</returns>
        bool UpdateRecapNotificationsettings(int id, IUserMailSubscription usrsubscrption);

        /// <summary>
        /// Updates the recap notificationsettings.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="Userid">The userid.</param>
        /// <param name="LastSentOn">The last sent on.</param>
        /// <param name="LastUpdatedOn">The last updated on.</param>
        /// <param name="IsEmailEnable">if set to <c>true</c> [is email enable].</param>
        /// <param name="DayName">Name of the day.</param>
        /// <param name="Timing">The timing.</param>
        /// <param name="RecapReport">if set to <c>true</c> [recap report].</param>
        /// <returns>IUserMailSubscription</returns>
        bool UpdateRecapNotificationsettings(int id, int Userid, DateTimeOffset LastSentOn, DateTimeOffset LastUpdatedOn, bool IsEmailEnable, string DayName, TimeSpan Timing, bool RecapReport);

        //Feed Comment
        /// <summary>
        /// Gets the feed by ID.
        /// </summary>
        /// <param name="enityid">The enityid.</param>
        /// <returns>IList</returns>
        IList<IFeed> GetFeedByID(int enityid);

        /// <summary>
        /// Inserts the feed comment.
        /// </summary>
        /// <param name="feedid">The feedid.</param>
        /// <param name="actor">The actor.</param>
        /// <param name="comment">The comment.</param>
        /// <param name="commentupdatedon">The commentupdatedon.</param>
        /// <returns>IFeedComment</returns>
        string InsertFeedComment(int feedid, int actor, string comment);

        /// <summary>
        /// Inserts the content of the update mail.
        /// </summary>
        /// <param name="Subject">The subject.</param>
        /// <param name="Body">The body.</param>
        /// <param name="description">The description.</param>
        /// <param name="id">The id.</param>
        /// <returns>int</returns>
        int InsertUpdateMailContent(string Subject, string Body, string description, int id = 0);

        /// <summary>
        /// Deletes the content of the mail.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        bool DeleteMailContent(int id);
        /// <summary>
        /// Inserts the update mail footer.
        /// </summary>
        /// <param name="Body">The body.</param>
        /// <param name="description">The description.</param>
        /// <param name="id">The id.</param>
        /// <returns>int</returns>
        int InsertUpdateMailFooter(string Body, string description, int id = 0);

        /// <summary>
        /// Deletes the mail footer.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns>bool</returns>
        bool DeleteMailFooter(int id);

        //Auto Subscription
        /// <summary>
        /// Updates the user single entity subscription.
        /// </summary>
        /// <param name="EntityId">The entity id.</param>
        /// <param name="SubscriptionTypeIDs">The subscription type I ds.</param>
        /// <param name="UserID">The user ID.</param>
        /// <returns>bool</returns>
        bool UpdateUserSingleEntitySubscription(int EntityId, SubscriptionTypeDao SubscriptionTypeIDs, int UserID);

        /// <summary>
        /// Updates the user single entity subscription.
        /// </summary>
        /// <param name="EntityId">The entity id.</param>
        /// <param name="Id">The id.</param>
        /// <param name="Caption">The caption.</param>
        /// <param name="IsAutomated">if set to <c>true</c> [is automated].</param>
        /// <param name="Userid">The userid.</param>
        /// <returns>bool</returns>
        bool UpdateUserSingleEntitySubscription(int EntityId, int Id, string Caption, bool IsAutomated, int Userid);

        //MultiSubscription Load
        /// <summary>
        /// Loads User multi subscription.
        /// </summary>
        /// <param name="EntitiyId">The entitiy id.</param>
        /// <param name="UserId">The user id.</param>
        /// <returns>int</returns>
        Tuple<int[], int, int> UserMultiSubscriptionLoad(int EntitiyId, int UserId);

        //MultiSubscription save and update
        /// <summary>
        /// Saves the update multi subscription.
        /// </summary>
        /// <param name="levels">The levels.</param>
        /// <param name="EntitiyId">The entitiy id.</param>
        /// <param name="UserId">The user id.</param>
        /// <param name="IsMultiLevel">if set to <c>true</c> [is multi level].</param>
        /// <param name="EntityTypeId">The entity type id.</param>
        /// <returns>String</returns>
        String SaveUpdateMultiSubscription(int[] levels, int EntityId, int Userid, bool IsMultiLevel, int EntityTypeId, DateTimeOffset SubscribedOn, DateTimeOffset LastUpdatedOn, int filteroption);

        //MultiSubscription Unscubscribe
        /// <summary>
        /// Unsubscribes the multi subscription.
        /// </summary>
        /// <param name="EntitiyId">The entitiy id.</param>
        /// <param name="UserId">The user id.</param>
        /// <returns>String</returns>
        bool UnsubscribeMultiSubscription(int EntitiyId, int UserId);

        /// <summary>
        /// Saves the update feed template.
        /// </summary>
        /// <param name="ModuleId">The module id.</param>
        /// <param name="FeatureId">The feature id.</param>
        /// <param name="Template">The template.</param>
        /// <returns>int</returns>
        int SaveUpdateFeedTemplate(int ModuleId, int FeatureId, String Template);

        /// <summary>
        /// Saves the update feed.
        /// </summary>
        /// <param name="Actor">The actor.</param>
        /// <param name="TemplateId">The template id.</param>
        /// <param name="EntityId">The entity id.</param>
        /// <param name="TypeName">Name of the type.</param>
        /// <param name="AttributeName">Name of the attribute.</param>
        /// <param name="FromValue">From value.</param>
        /// <param name="ToValue">To value.</param>
        /// <returns>Last Inserted Feed ID</returns>
        int SaveUpdateFeed(int Actor, int TemplateId, int EntityId, String TypeName, String AttributeName, String FromValue, String ToValue, int UserId = 0, int associatedentityid = 0, string attributeGroupRecordName = null, int Version = 0, int PhaseID = 0, string StepID = "", int[] PersonalUserId = null);

        /// <summary>
        /// Gets the type of the notification BY.
        /// </summary>
        /// <param name="pCaption">The p caption.</param>
        /// <returns>INotificationType</returns>
        INotificationType GetNotificationBYType(string pCaption);

        /// <summary>
        /// Gets the user default subscription.
        /// </summary>
        /// <param name="SubScriptionTypeId">The sub scription type id.</param>
        /// <returns>IList</returns>
        IList<IUserDefaultSubscription> GetUserDefaultSubscription(ISubscriptionType SubScriptionTypeId);

        /// <summary>
        /// Gets the user default subscription.
        /// </summary>
        /// <param name="Id">The id.</param>
        /// <param name="Caption">The caption.</param>
        /// <param name="IsAutomated">if set to <c>true</c> [is automated].</param>
        /// <returns>IList</returns>
        IList<IUserDefaultSubscription> GetUserDefaultSubscription(int Id, string Caption, bool IsAutomated);


        /// <summary>
        /// Insert User notification.
        /// </summary>
        /// <param name="Id">The id.</param>
        /// <param name="Userid">The userid.</param>
        /// <param name="Entityid">The entityid.</param>
        /// <param name="Actorid">The actorid.</param>
        /// <param name="CreatedOn">The created on.</param>
        /// <param name="Typeid">The typeid.</param>
        /// <param name="IsViewed">if set to <c>true</c> [is viewed].</param>
        /// <param name="IsSentInMail">if set to <c>true</c> [is sent in mail].</param>
        /// <param name="NotificationText">The notification text.</param>
        /// <returns>bool</returns>
        bool UserNotification_Insert(int Entityid, int Actorid, DateTimeOffset CreatedOn, int Typeid, bool IsViewed, bool IsSentInMail, string Typename, string Attributename, string Fromvalue, string Tovalue, int mailtemplateid = 0, int userid = 0, int AssocitedEntityID = 0, int PhaseID = 0, string StepID = "", bool IsPersonalized = false);
        bool Insert_Mail(int mailtemplateid, IList<UserNotificationDao> listofusernotification);
        bool InsertMultiAssignedTaskMail(int mailTemplateid, int actorId, List<int> multiTasks, IList<TaskMembersDao> taskMembers);

        /// <summary>
        /// Inerting and Updating AdminSettings.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="key">The SettingKey.</param>
        /// <returns>True (or) False</returns>
        bool AdminSettingsInsertUpdate(string jsondata, string key, string data, string Appdata, JObject ImageChanged);

        bool AdminSettingsforRootLevelInsertUpdate(string jsondata, string key, int typeid);

        bool AdminSettingsforReportInsertUpdate(string jsondata, string key);

        bool AdminSettingsforGanttViewInsertUpdate(string jsondata, string key);

        bool AdminSettingsforListViewInsertUpdate(string jsondata, string key);

        bool AdminSettingsforRootLevelFilterSettingsInsertUpdate(string jsondata, string key, int EntityTypeID);

        bool AdminSettingsforDetailFilterInsertUpdate(string jsondata, string key);

        bool AdminSettingsForRootLevelDelete(string key, int EntityTypeID);
        bool AdminSettingsForRootLevelDelete(string key, int EntityTypeID, int AttributeID);
        /// <summary>
        /// Get Navigation Config.
        /// </summary>
        /// <returns>String</returns>
        Tuple<string, string> GetNavigationConfig();
        /// <summary>
        /// Get Navigation Config.
        /// </summary>
        /// <returns>String</returns>
        string GetMediabankNavigationConfig();

        /// <summary>
        /// Get Navigation ExternalLink.
        /// </summary>
        /// <param name="proxy">The Typeid.</param>
        /// <param name="proxy">The proxy.</param>
        /// <returns>IList<INavigation></returns>
        string GetNavigationExternalLinksByID(int typeid);

        string GetAdminSettings(string LogoSettings, int typeid);
        string GetAdminSettingselemntnode(string LogoSettings, string elemntnode, int typeid);

        string GetAdminLayoutSettings(string LogoSettings, int typeid);
        string GetAdminLayoutFinSettings(string LogoSettings, int typeid);
        string GetAdminLayoutObjectiveSettings(string LogoSettings, int typeid);
        bool LayoutDesign(string jsondata, string key, int typeid);
        /// <summary>
        /// InsertFile.
        /// </summary>
        /// <param name="proxy">file Parameter</param>
        /// <returns>int</returns>
        int InsertFile(string Name, int VersionNo, string MimeType, string Extension, long Size, int OwnerID, DateTime CreatedOn, string Checksum, int ModuleID, int EntityID, String FileGuid, string Description, bool IsPlanEntity = false);

        /// <summary>
        /// InsertLink
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="VersionNo"></param>
        /// <param name="MimeType"></param>
        /// <param name="Extension"></param>
        /// <param name="Size"></param>
        /// <param name="OwnerID"></param>
        /// <param name="CreatedOn"></param>
        /// <param name="Checksum"></param>
        /// <param name="ModuleID"></param>
        /// <param name="EntityID"></param>
        /// <param name="FileGuid"></param>
        /// <returns>int</returns>
        int InsertLink(int EntityID, string Name, string URL, string Description, int ActiveVersionNo, int TypeID, string CreatedOn, int OwnerID, int ModuleID);
        /// 

        /// <summary>
        /// DeleteFileByID.
        /// </summary>
        /// <param name="proxy">ID Parameter</param>
        /// <returns>bool</returns>
        bool DeleteFileByID(int ID);


        /// <summary>
        /// DeleteLinkByID.
        /// </summary>
        /// <param name="proxy">ID Parameter</param>
        /// <returns>bool</returns>
        bool DeleteLinkByID(int ID);

        /// <summary>
        /// Get File By  Entity ID.
        /// </summary>
        /// <param name="proxy">EntityID</param>
        /// <returns>int</returns>
        IList<IFile> GetFileByEntityID(int EntityID);


        /// <summary>
        /// Get Links By  Entity ID.
        /// </summary>
        /// <param name="proxy">EntityID</param>
        /// <returns>int</returns>
        IList<IFile> GetFilesandLinksByEntityID(int EntityID);



        /// <summary>
        /// Getting Feeds by EntiyID
        /// </summary>
        /// <param name="FeedId">The EntityID</param>
        /// <param name="lastFeedRequestedTime">The Last Requested Time</param>
        /// <returns>IList<IFeedSelection></IFeedSelection></returns>
        IList<IFeedSelection> GetFeedsByEntityID(string entityId, int pageNo, int entityIdForReference = 0, string newsfeedgroupid = "");

        string GetEntityIdsForFeed(int entityId);

        /// <summary>
        /// GettingFeedsByAsset
        /// </summary>
        /// <param name="FeedId">The AssetId</param>
        /// <param name="lastFeedRequestedTime">The Last Requested Time</param>
        /// <returns>IList<IFeedSelection></IFeedSelection></returns>
        IList<IFeedSelection> GettingFeedsByAsset(int AssetId, int pageNo);


        /// <summary>
        /// GettingFeedsByAsset
        /// </summary>
        /// <param name="FeedId">The AssetId</param>
        /// <param name="lastFeedRequestedTime">The Last Requested Time</param>
        /// <returns>IList<IFeedSelection></IFeedSelection></returns>
        IList<IFeedSelection> GettingLatestFeedsByAsset(int AssetId);

        /// <summary>
        /// Getting Feeds by EntiyID
        /// </summary>
        /// <param name="FeedId">The EntityID</param>
        /// <param name="lastFeedRequestedTime">The Last Requested Time</param>
        /// <returns>IList<IFeedSelection></IFeedSelection></returns>
        IList<IFeedSelection> GettingFeedsByEntityIDandFundingrequest(int entityId, int pageNo, bool islatestfeed);

        /// <summary>
        /// Getting Feeds by EntiyID and Last requested time
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="FeedId">The EntityID</param>
        /// <param name="lastFeedRequestedTime">The Last Requested Time</param>
        /// <returns>IList<IFeedSelection></IFeedSelection></returns>
        IList<IFeedSelection> GetLatestFeedsByEntityID(string entityId, int entityIdForReference, string newsfeedgroupid = "");

        /// <summary>
        /// Gets the user default subscription by user id.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="SubScriptionTypeId">The sub scription user id.</param>
        /// <returns>IList</returns>
        Tuple<IList<ISubscriptionType>, string[], string[]> GetUserDefaultSubscriptionByUserID();

        /// <summary>
        /// update the user default subscription by user id.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="SubScriptionTypeId">The sub scription user id.</param>
        /// <returns>IList</returns>
        bool SaveSelectedDefaultSubscription(string subscriptionTypeIds = null, string mailSubscritpitonTypeIds = null);

        bool InsertUserSingleEntitySubscription(int UserId, int EntityId, int EntitytypeId, DateTimeOffset SubscribedOn, DateTimeOffset LastUpdatedOn, string issubscribe, int filteroption);
        IList<IEntityType> GetEntityTypeforSubscription(int ID);

        String GetAutoSubscriptionDetails(int UserID, int EntityID);
        /// <summary>
        /// Update Notification by email.
        /// </summary>
        /// <param name="proxy"> </param>
        /// <returns>bool</returns>
        bool SaveNotificationByMail(string ColumnName, string ColumnValue);

        /// <summary>
        /// Update Notification by Task.
        /// </summary>
        /// <param name="proxy"> </param>
        /// <returns>bool</returns>
        bool SaveTaskNotificationByMail(string ColumnName, string ColumnValue);

        /// <summary>
        /// select WidgetTemplate.
        /// </summary>
        /// <param name="TemplateID">The WidgetTemplate ID.</param>
        /// <returns>IList</returns>
        IList<IWidgetTemplate> WidgetTemplate_Select(int TemplateID);

        /// <summary>
        /// select WidgetTypes.
        /// </summary>
        /// <param name="TypeID">The WidgetTypes ID.</param>
        /// <returns>IList</returns>
        IList<IWidgetTypes> WidgetTypes_Select(int userId, bool isAdmin, int typeId = 0);

        /// <summary>
        /// select WidgetTypeRoles.
        /// </summary>
        /// <param name="WidgetTypeID">The WidgetType ID.</param>
        /// <returns>int[] </returns>
        List<int> GetWidgetTypeRolesByID(int WidgetTypeID);

        /// <summary>
        /// select WidgetTypeDimension.
        /// </summary>
        /// <param name="WidgetTypeID">The WidgetType ID.</param>
        /// <returns>IList</returns>
        IList<IWidgetTypeDimension> WidgetTypeDimension_Select(int WidgetTypeID);

        /// <summary>
        /// Inserts the Widget Template comment.
        /// </summary>
        /// <param name="TemplateName">The Template Name.</param>
        /// /// <param name="TemplateDescription">The Template Description.</param>
        ///  /// <returns>int</returns>
        int InsertWidgetTemplate(string TemplateName, string TemplateDescription);


        /// <summary>
        /// Update WidgetTemplate.
        /// </summary>
        /// <param name="Id">The id.</param>
        /// <param name="TemplateName">The TemplateName.</param>
        /// <param name="TemplateDescription">The TemplateDescription.</param>

        /// <returns>
        /// bool 
        /// </returns>
        bool WidgetTemplate_Update(int ID, string TemplateName, string TemplateDescription);


        /// <summary>
        /// Inserts the WidgetTypeRoles.
        /// </summary>
        /// <param name="widgetTypeID">The widgetTypeID.</param>
        /// <param name="roleID">The roleID.</param>
        /// <returns>int</returns>
        int InsertWidgetTypeRoles(int widgetTypeID, int roleID);



        /// <summary>
        /// Inserts the WidgetTemplateRoles.
        /// </summary>
        /// <param name="widgetTypeID">The WidgetTemplateID.</param>
        /// <param name="roleID">The roleID.</param>
        /// <returns>int</returns>
        int InsertWidgetTemplateRoles(int WidgetTemplateID, int roleID);

        /// Get the WidgetTemplateRoles  By TemplateID.
        /// </summary>
        /// <param name="WidgetTemplateID">The WidgetTemplateID.</param>
        /// <returns>int[] </returns>
        int[] GetWidgetTemplateRolesByTemplateID(int WidgetTemplateID);

        /// Delete the WidgetTemplateRoles
        /// </summary>
        /// <param name="WidgetTemplateID">The TemplateID.</param>
        /// <returns>bool</returns>
        bool DeleteWidgetTemplateRoles(int WidgetTemplateID);

        /// Delete the WidgetTypeRoles
        /// </summary>
        /// <param name="WidgetTypeID">The WidgetTypeID.</param>
        /// <returns>bool</returns>
        bool DeleteWidgetTypeRoles(int WidgetTypeID);

        /// <summary>
        /// Insert widget.
        /// </summary>
        /// <returns>Iwidget Object.</returns>
        string InsertUpdateWidget(string templateid, string widgetid, string caption, string description, int widgettypeid, int filterid, int attributeid, bool isdynamic, string widgetQuery, int dimensionid, string matrixid, int columnval, int rowval, int sizeXval, int sizeYval, bool IsAdminPage, int visualtypeid, int NoOfItem, string listofentityid, string listofSelectEntityID, int NoOfYear, int NoOfMonth);

        /// <summary>
        /// Gets the widget details.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <returns>List of IWidget</returns>
        IList<IWidget> GetWidgetDetails(string widgetid, string templateid, bool IsAdminPage);

        /// <summary>
        /// Delete widget.
        /// </summary>
        /// <returns>Iwidget Object.</returns>
        bool DeleteWidget(string templateid, string widgetid, bool IsAdminPage);

        /// <summary>
        /// Insert widget.
        /// </summary>
        /// <returns>Iwidget Object.</returns>
        string WidgetDragEdit(IList<IWidget> widgetdata, bool IsAdminPage);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ApplicationPath"></param>
        /// <param name="EntityID"></param>
        /// <param name="typeID"></param>
        /// <param name="UserId"></param>
        /// <param name="isSsoUser"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        string GetEntityPathforMail(string ApplicationPath, int EntityID, int typeID, int UserId, bool isSsoUser, int parentID);

        /// <summary>
        /// Get subscription by user id.
        /// </summary>
        /// <param name="proxy"> </param>
        /// <returns>bool</returns>
        IUserMailSubscription GetSubscriptionByUserId();

        IUserTaskNotificationMailSettings GetTaskSubscriptionByUserId();


        bool GetIsSubscribedFromSettings();
        Tuple<IList<BrandSystems.Marcom.Dal.Metadata.Model.EntityTypeDao>, string[]> GetNotAssociateEntityTypes();

        #region Instance of Classes In ServiceLayer reference
        /// <summary>
        /// Returns widget class.
        /// </summary>
        IWidget Widgetservice();

        #endregion


        bool InsertMail(BrandSystems.Marcom.Core.Common.MailHolder mailHolder, string subject = null, string body = null);

        bool HandleUnScheduledMail(BrandSystems.Marcom.Core.Common.MailHolder mailHolder, string subject = null, string body = null);

        bool HandleSendMail();

        IList<int> GetListofUserIdForNotificationbyMail(int notificationtemplateid, int entityid);

        bool InsertPoSettingXML(string Prefix, string DateFormat, string DigitFormat, string NumberCount);

        IList<PurchaseOrderSettingsDao> GetPoSSettings(string PoSettings);

        bool InsertUpdateAdditionalSettings(int id, string Settingname, string settingValue);

        IList<IAdditionalSettings> GetAdditionalSettings();


        bool Insert_AdminEmail(string jsondata);
        string GetEmailids();

        IList<ILanguageType> GetLanguageTypes();
        bool GetLanguageExport(int ID, string languageTypename, string Filename);

        bool SaveNewLanguage(int InheritedId, string Name, string Description);

        IList GetLanguageContent(int StartRows, int NextRows);

        bool UpdateLanguageContent(int LangTypeID, int ContentID, string newValue);

        string GetLanguageSettings(int LangID);

        /// <summary>
        /// Get Running PO Number
        /// </summary>
        /// <param name="proxy"></param>
        /// <returns>Running PO number<Iwidget> </returns>
        string GetCurrentPONumber();

        bool UpdateLanguageName(int LangTypeID, string NewValue, int NameOrDesc);

        bool SetDefaultLanguage(int LangID);

        IList LanguageSearch(int langid, string searchtext, string searchdate, int StartRows);

        IList LanguageSearchs(int langid, string searchtext);

        int GetDefaultLangFromXML();

        IUserTaskNotificationMailSettings GetUserDefaultTaskNotificationMailSettings();

        string GetEditorText();

        bool InsertEditortext(int[] entityList, String Content = null);

        IList<SSO> GetSSODetails();
        bool UpdateSSOSettings(string key, string iv, string algo, string paddingmode, string ciphermode, string tokenmode, string SSOTimeDifference, string UpdateSSOSettings, string ClientIntranetUrl, string ssoglobalaccess, JArray SAMLRoles);

        bool IsActiveEntity(int EntityID);

        IList<IUnits> GetUnits();

        IList<IUnits> GetUnitsByID(int ID);

        bool InsertUpdateUnits(string caption, int id);
        IList<IFeedFilterGroup> GetFilterGroup();
        IList<IFeedTemplate> GetFeedTemplates();
        int InsertUpdateFeedFilterGroup(int Id, string feedfiltergroupname, string feedactions);
        bool DeleteFeedGroupByid(int id);
        /// <summary>
        /// Delete global ACL.
        /// </summary>
        /// <param name="ID">ID.</param>
        /// <returns>int</returns>
        bool DeleteUnitsByid(int ID);


        TopNavigation GetTopNavigation();

        int InsertAdminNotificationSettings(JArray subscriptionObject);

        bool InsertCurrencyFormat(string ShortName, string Name, string Symbol, int Id = 0);

        /// <param name="year"> </param>
        /// <param name="month"> </param>
        /// <returns>Ilist </returns>
        IList GetUniqueuserhit(int year, int month);


        /// <param name="year"> </param>
        /// <param name="month"> </param>
        /// <returns>Ilist </returns>
        IList GetApplicationhit(int year, int month);

        /// <param name="year"> </param>
        /// <param name="month"> </param>
        /// <returns>Ilist </returns>
        IList GetBrowserStatistic(int year, int month);

        IList GetBrowserVersionStatistic(int year, int month);

        IList GetUserStatistic();

        IList GetOSStatistic();
        IList GetstartpageStatistic();

        IList GetUserRoleStatistic();
        IList GetEnityStatistic();
        IList GetEnityCreateationStatistic(int year, int month);

        int InsertBroadcastMessages(int userId, string username, string broadcastmsg);
        IList GetBroadcastMessages();
        IList GetBroadcastMessagesbyuser();
        int updateBroadcastMessagesbyuser();
        IList<IBandwithData> GetbandwidthStatistic(int year, int month);

        IList<IReportContainer> GetAPIEntityDetails(bool IsshowFinancialDetl, bool IsDetailIncluded, bool IsshowTaskDetl, bool IsshowMemberDetl, int ExpandingEntityIDStr, bool IncludeChildrenStr, bool IsRootLevelEntity, int offsetstart = 0, int offsetend = 20);

        int InsertUpdateGanttHeaderBar(int Id, string name, string description, DateTime startdate, DateTime enddate, string colorcode);

        IList<IGanttviewHeaderBar> GetAllGanttHeaderBar();

        bool DeleteGanttHeaderBar(int Id);

        IList<ICurrencyConverter> getCurrencyconverterData();

        bool DeleteCurrencyconverterData(int id);

        bool Insertupdatecurrencyconverter(DateTime Startdate, DateTime Enddate, string Currencytype, double Currencyrate, int id);

        IList<ICurrencyConverter> GetRatesByID(int id);

        IList<ICurrencyConverter> GetExchangesratesbyCurrencytype(int id);

        IList<ICustomTab> GetCustomTabsByTypeID(int TypeID);

        int[] InsertUpdateCustomTab(int ID, int Typeid, string Name, string ExternalUrl, bool AddEntityID, bool AddLanguageCode, bool AddUserEmail, bool AddUserName, bool AddUserID, int tabencryID, string encryKey, string encryIV, string algorithm, string paddingMode, string cipherMode, string entitytypeids, string globalids);


        bool DeleteCustomtabByID(int ID, int AttributeTypeID, int EntityTypeID);


        bool UpdateCustomTabSortOrder(int ID, int sortorder);

        bool InsertUpdateApplicationUrlTrack(Guid TrackID, string TrackValue);

        string GetApplicationUrlTrackByID(Guid TrackID);

        int InsertEntityAttachmentsVersion(int EntityID, IList<IAttachments> EntityAttachments, IList<IFile> EntityFiles, int FileID, int VersioningFileId);

        IList<ICustomTab> GetCustomEntityTabsByTypeID(int TypeID, int CalID, int EntityTypeID = 0, int EntityID = 0);

        string GetCustomTabUrlTabsByTypeID(int tabID, int entityID);

        bool UpdateCustomTabSettings(string key, string iv, string algo, string paddingmode, string ciphermode, string tokenmode);

        IList<SSO> GetCustomTabSettingDetails();

        IConvertedcurrencies GetConvertedcurrencies(int Amount, int ID, string Currencytype, DateTime Duedate);
        IList<IConvertedcurrencies> CurrencyConvertJSON(JObject curr);
        Tuple<IUpdateSettings, string> GetUpdateSettings();
        IList<PasswordSetting> GetPasswordPolicyDetails();
        bool UpdatePasswordPolicy(string MinLength, string Maxlength, string Numlength, string UpperLength, string SpecialLength, string SpecialChars, string BarWidth, string MultipleColors);

        //Get include plan tabs using GetEntitytabsettings
        Hashtable GetPlantabsettings();

        //Updatee plan tabs using UpdatePlanTabsettings passing typeID
        bool UpdatePlanTabsettings(string jsondata);

        IList<ITabEncryption> GetCustomTabEncryptionByID();

        /// <summary>
        /// Get Optimaker settings Address points.
        /// </summary>
        /// <returns>String</returns>
        string GetOptimakerAddresspoints();
        bool IsAvailableAsset(int AssetID);

        IList<ICustomTabEntityTypeAcl> GetCustomTabAccessByID(int TabID);

        IList<IAttribute> GetAttributeSearchCriteria(int entityTypeid);

        bool SearchadminSettingsforRootLevelInsertUpdate(string jsondata, string LogoSettings, string key, int typeid, int isExpand);


        IList<ICustomTab> GetCustomEntityTabsfrCalID(int TypeId, int CalID);
        int[] GetSearchCriteriaTypesIds(int searchtype);
        IList<ICustomTab> GetCustomEntityTabsByTypeID(int TypeID);

        bool LayoutSettingsApplyChanges(string TabType, string TabLocation);

        List<object> GetProofHQSSettings();

        bool UpdateProofHQSSettings(string userName, string password);
        string GetCalendarNavigationConfig();
        bool UpdateExpirytime(string ActualTime);
        string Getexpirytime();
        bool UpdatePopularTagWordsToShow(int TotalTagWords);
        bool InsertLanguageImport(string FileImport, int LangTypeId);
        int GetTotalPopularTagWordsToShow();
        IList<IAssetsFeedSelection> GettingAssetsFeedSelectionDashbord(int Topx, int feedTemplateID, int newsfeedid = 0);
        IList GetAssetCreateationStatistic(int year, int month);
        IList GetProofInitiatorStatistic();
        IList GetSearchtype(int userid);
        IList GetassignedAcess(int userid);
        bool UpdateThemeSettings(string themename, int themeID, JObject jsonXML, JObject Greyvariation, JObject objBasecolorvariations);
        string GetThemeData(int themeID);
        //IList GetThemeValues();
        Tuple<IList, string> GetThemeValues();
        bool Insert_notifydisplaytime(string jsondata);

        bool Insert_notifycycledisplaytime(string jsondata);
        string Getnotifytimesetbyuser();

        string Getnotifyrecycletimesetbyuser();

        /// <summary>
        /// Gets the live task updates
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <returns></returns>
        IList GetTaskLIveUpdateRecords();


        /// <summary>
        /// Restore the default
        /// </summary>
        /// <returns>boolean</returns>
        bool RestoreDefaultTheme();

        bool UpdateTitleSettings(string titlename, int titleID, JObject jsonXML, JObject ImageChanged);
        string GetTitleLogoSettings(string LogoSettings);

        /// <summary>
        /// Get all the table with column metadata.
        /// </summary>
        /// <returns>IList</returns>
        List<object> GetAlltableswithmetadata(string tablename);

        /// <summary>
        /// Executes the sql query editor query.
        /// </summary>
        /// <param name="strBody">The query STR body.</param>
        /// <returns>Tuple<IList<List<List<EditorResult>>>, string></returns>
        Tuple<IList<List<List<EditorResult>>>, string, double> ManipulateQueryEditorQuery(string queryBody, string TenantHost);
        IList GetAlltablesnames();
        IList GetModuleID(int EntityId);
        string getTenantClientPath();
        List<object> GetObjectLanguageContent(int LangEditID);
        List<object> GetlanguageContentObjectByID(int languageTypeID);
        bool UpdateJsonLanguageContent(string JsonUpdatedLanguage, int selectedLangID, string LangKey, int TranslationPending);
        bool InsertNewLanguage(string Name, string Description, int InheritedID);
        string GetDefaultLanguageKey();
        string getRedirectPath(int entityID);

        bool UpdateAccLockPasswordPolicy(int lockouttime, int attempts, int monthlyrepetition, int repetitiontime, int monthlyexpiration, bool expirevalidation, bool notifyuser, bool notifyadmin);


        IList<PasswordSetting> GetAccLockDetails();


        bool InsertUpdateDecimalSettings(int deciamlplaces, string pagename);

        List<string> GetDecimalSettingsValue();

        List<object> GetDalimSettings();

        bool UpdateDalimSettings(string dalimcustomerId, string dalimcustomername, string dalimdefaultusername, string dalimdefaultuserpassword, string dalimorgUnit, string dalimdefaultprofile, string dalimprojecttemplatename, string dalimProtocol, string externalurl, string dalimDomain, JArray TemplateCollection);

        bool updateDefaultFolderInfo(JObject DefaultFolderObj);

        List<object> getDefaultFolderInfo();

        int[] GettingFeedFilter(int[] GlobalRoleID);

        int[] GetEntityRoleByEnitityID(int EntityID, int UserID);

        int GetCorporateRolebyEntityID(int EntitiyID);

        bool SaveHolidayDetails(string day);

        IList<HolidayDetailsDao> GetNonBusinessDays();

        bool InsertHolidayDetails(JObject HolidayObj);

        IList<HolidayDetailsDao> GetHolidaysDetails();

        bool DeleteHoliday(int ID);

        bool UpdateNewFeedconfig(JObject newsFeedConfigObj);

        List<object> GetNewsFeedConfigInfo();
        bool savecloudsettings(JObject jObj);

        object getcloudsettings();

        Tuple<string, string> GetNavigationandLanguageConfig();

        string GetObjectiveTabAdminSettings(string LogoSettings, int typeid);

        bool AdminSettingsObjectivetabRootLevelInsertUpdate(string jsondata, string key, int typeid);


        IList<BrandSystems.Marcom.Dal.Metadata.Model.AttributeGroupDao> GetAllAttributeGroup();

        List<object> GetAttrGroupTooltipValues(int GroupID);

        bool UpdateAttributeGroupTooltip(int GroupID, int AttributeID, bool istooltip, IList<object> attrDetails);

        IList<FinancialViewListDao> GetFinancialViews();
        IList<FinancialViewColumnsDao> GetFinancialViewColumns(int viewId);
        bool InsertFinacialColumns(JObject jobj);
        bool UpdateFinancialViews(JObject jobj);
        bool DeleteFinancialViews(JObject jobj);
        FinancialView EditFinancialView(int viewId);
        List<FinancialView> GetListFinancialView();

        string GetUserRegAdminSettings(string LogoSettings, int typeid);

        string GetUserRegLogoSettings(string LogoSettings);
        bool AdminSettingsUserRegInsertUpdate(string jsondata, string key, string data, JObject ImageChanged);
        bool UpdateUserRegSettings(int titleID, JObject jsonXML, JObject ImageChanged);
    }
}
