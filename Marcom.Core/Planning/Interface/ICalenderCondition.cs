using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// ICalenderCondition interface for table 'PM_CalenderCondition'.
    /// </summary>
    public interface ICalenderCondition
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Calenderid
        {
            get;
            set;

        }

        int? EntityTypeid
        {
            get;
            set;

        }

        int? Attributeid
        {
            get;
            set;

        }

        int ConditionType
        {
            get;
            set;

        }

        int SortOrder
        {
            get;
            set;

        }

        int AttributeLevel
        {
            get;
            set;
        }
        

        #endregion
    }
}
