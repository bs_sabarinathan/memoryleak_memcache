﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{

    public interface IFinancialDetail
    {
        #region Public Properties

        int FinId { get; set; }
        decimal Spent { get; set; }
        decimal InRequest { get; set; }
        decimal ApprovedBudget { get; set; }
        int CostCenterOwnerID { get; set; }
        string EntityName { get; set; }
        string CostCenterName { get; set; }
        string UniqueKey { get; set; }
        string ApprovedBudgetDate { get; set; }
        decimal NonResAlloc { get; set; }
        int EntityID { get; set; }
        int ParentID { get; set; }
        decimal ApprovedAllocatedAmount { get; set; }
        decimal NonResPlan { get; set; }
        decimal PlannedAmount { get; set; }
        decimal SubAllocated { get; set; }
        int TypeId { get; set; }
        decimal SubPlanned { get; set; }
        decimal Commited { get; set; }
        decimal available { get; set; }
        int CostCenterID { get; set; }
        decimal BudgetDeviation { get; set; }
        IList<IFinancialDetail> CostCenterList { get; set; }
        IList<IFinancialMetadataAttributewithValues> DynamicData { get; set; }

        #endregion
    }
}
