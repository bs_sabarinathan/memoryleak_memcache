using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// INavigation interface for table 'CM_Navigation'.
    /// </summary>
    public interface INavigation
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Parentid
        {
            get;
            set;

        }

        int Typeid
        {
            get;
            set;

        }

        int Moduleid
        {
            get;
            set;

        }

        int Featureid
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }

        string Url
        {
            get;
            set;

        }

        string ExternalUrl
        {
            get;
            set;

        }




        string JavaScript
        {
            get;
            set;

        }

        bool IsActive
        {
            get;
            set;

        }

        bool IsPopup
        {
            get;
            set;

        }

        bool IsDefault
        {
            get;
            set;

        }

        bool IsIframe
        {
            get;
            set;

        }

        bool IsDynamicPage
        {
            get;
            set;

        }

        bool IsExternal
        {
            get;
            set;

        }

        bool AddUserName
        {
            get;
            set;

        }

        bool AddUserEmail
        {
            get;
            set;

        }

        bool AddUserID
        {
            get;
            set;

        }

        bool AddLanguageCode
        {
            get;
            set;

        }

        string Imageurl
        {
            get;
            set;

        }

        int SortOrder
        {
            get;
            set;
        }
        int SearchType
        {
            get;
            set;
        }

        #endregion
    }
}
