﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Microsoft.VisualBasic;
using System.Collections;
using System.Data;
using System.Diagnostics;
using BrandSystems.Library.Threading;
using BrandSystems.Library.Threading.Internal;
using System.Reflection;
using System.Configuration;
using BrandSystems.Marcom.Core.Managers;
using BrandSystems.Marcom.Utility;
using System.Drawing;
using System.Net.Mail;
using System.Web;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Managers.Proxy;
using BrandSystems.Marcom.Dal.Common.Model;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Configuration;
using System.Net.Configuration;
using System.IO;
using System.Xml.Linq;
using BrandSystems.Marcom.Core.Common;
using System.Data.SqlClient;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Dal.Task.Model;
using BrandSystems.Marcom.Dal.User.Model;
using BrandSystems.Marcom.Dal.Metadata.Model;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Dal.Planning.Model;
using System.Xml;
using BrandSystems.Marcom.Core.AmazonStorageHelper;
using BrandSystems.Marcom.Core.Utility;


namespace Mail
{
    public class MailServer
    {
        // IMarcomManager marcomManager=null;
        //MarcomManager marcomManager = new MarcomManager();

        public string TenantsConnStr;
        public MailServer()
        {
            //do nothing
        }

        //public MailServer(string TenantHost)
        //{
        //    string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() ;
        //    TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
        //    System.Xml.XmlDocument xdcDocument = new System.Xml.XmlDocument();
        //    xdcDocument.Load(TenantInfoPath);
        //    System.Xml.XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@TenantHost='" + TenantHost + "']");
        //    this.tenantconnectionstr = xn["conn"].InnerText;
        //}

        private static MailServer _instance = new MailServer();
        //private static string _header;
        //public static Image _logo;

        public static MailServer Instance
        {
            get { return _instance; }
            set { _instance = value; }
        }
        //public static string Header
        //{
        //    get { return _header; }
        //    set { _header = value; }
        //}
        //public static Image Logo
        //{
        //    get { return _logo; }
        //    set { _logo = value; }
        //}


        string nuller = "";
        public bool Initialize(String smtpServer)
        {
            try
            {

                MailServer.Instance.mailStatusUpdate += MailStatusUpdateToDb;
                var client = new SmtpClient();
                //{
                //    Credentials = new System.Net.NetworkCredential("marcom.noreply@brandsystems.in", "bsi1234$"),
                //    Host = "webmail.brandsystems.in"
                //};
                //var maildisplayname = System.Configuration.ConfigurationManager.AppSettings["EmailDisplay"].ToString() != null ? System.Configuration.ConfigurationManager.AppSettings["EmailDisplay"].ToString() : "Marcom";
                //client.From = new System.Net.Mail.MailAddress(System.Configuration.ConfigurationManager.AppSettings["Email"].ToString(), maildisplayname);

                return true;
            }
            catch (Exception)
            {

                throw;
            }

            return false;
        }

        //Create properties for Logo, Header 

        public Dictionary<string, string> ApplicationSettings = new Dictionary<string, string>();
        private Dictionary<Guid, IWorkItemsGroup> _workItemGroupList = new Dictionary<Guid, IWorkItemsGroup>();
        private List<Guid> _workItemGroupListIDs = new List<Guid>();

        private static BSThreadPool _bsThreadPool;
        //private static IWorkItemsGroup _workItemGroup;
        public delegate void MailStatusUpdate(MailHolder mailHolder, MailStatus status);
        public event MailStatusUpdate mailStatusUpdate;

        #region "Start and Stop Engine"

        /// <summary>
        /// For Starting the Engine
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool StartEngine()
        {
            try
            {
                BSTPStartInfo bstpStartInfo = new BSTPStartInfo();
                bstpStartInfo.IdleTimeout = 30 * 1000; //30 Sec
                bstpStartInfo.MaxWorkerThreads = 30; //Max 30 thread we will create                
                bstpStartInfo.MinWorkerThreads = 0;
                bstpStartInfo.PerformanceCounterInstanceName = "Marcom Mail BSThreadPool";
                _bsThreadPool = new BSThreadPool(bstpStartInfo);

            }
            catch (Exception)
            {
                return false;

            }
            finally
            {
            }

            return true;
        }

        /// <summary>
        /// For Stoping the engine
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool StopEngine()
        {


            try
            {
                _bsThreadPool.Shutdown();
                _bsThreadPool.Dispose();
                _bsThreadPool = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();

            }
            catch (Exception)
            {
                return false;

            }
            finally
            {
            }

            return true;
        }

        #endregion

        /// <summary>
        /// Checking memory For Synchronous and ASynchronous tasks
        /// </summary>
        /// <param name="obj">MediaHandlerObject</param>
        /// <returns>True or false</returns>
        /// <remarks></remarks>
        public bool AddToQueque(MailHolder obj, string flag)
        {

            try
            {
                var pworkPriority = WorkItemPriority.Highest;
                var sworkPriority = WorkItemPriority.Normal;
                if (flag == "insert")
                {
                    //loop through each client and insert 
                    string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
                    TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
                    XmlDocument xdcDocument = new XmlDocument();
                    xdcDocument.Load(TenantInfoPath);
                    XmlElement xelRoot = xdcDocument.DocumentElement;
                    XmlNodeList xnlNodes = xelRoot.SelectNodes("/Tenants/Tenant");
                    foreach (XmlNode xndNode in xnlNodes)
                    {
                        TenantsConnStr = xndNode.Attributes["TenantHost"].Value;
                        var workItemCallBackInsertToDb = new WorkItemCallback(HandleMail);
                        _bsThreadPool.QueueWorkItem(workItemCallBackInsertToDb, obj);

                    }

                }
                else if (flag == "RecapReport")
                {
                    var workPriority1 = WorkItemPriority.Lowest;

                    if (obj.PrimaryTo != null)
                    {
                        workPriority1 = pworkPriority;
                    }
                    if (obj.SecondaryTo != null)
                    {
                        workPriority1 = sworkPriority;

                    }
                    obj.isrecap = true;
                    var workItemCallBack1 = new WorkItemCallback(SendMailEvent);
                    var postExeWorkItemCallBack1 = new PostExecuteWorkItemCallback(PostSendMailEvent);
                    _bsThreadPool.QueueWorkItem(workItemCallBack1, obj, postExeWorkItemCallBack1, workPriority1);
                }
                else
                {
                    var workPriority = WorkItemPriority.Lowest;

                    if (obj.PrimaryTo != null)
                    {
                        workPriority = pworkPriority;
                    }
                    if (obj.SecondaryTo != null)
                    {
                        workPriority = sworkPriority;

                    }
                    var workItemCallBack = new WorkItemCallback(SendMailEvent);
                    var postExeWorkItemCallBack = new PostExecuteWorkItemCallback(PostSendMailEvent);
                    _bsThreadPool.QueueWorkItem(workItemCallBack, obj, postExeWorkItemCallBack, workPriority);

                }

                return true;
            }
            catch (Exception)
            {

                return true;
            }

            return true;

        }
        /// <summary>
        /// For executing single Job.
        /// </summary>
        /// <param name="state"></param>
        /// <returns>Media Handler Object</returns>
        /// <remarks></remarks>
        public object SendMailEvent(object state)
        {
            //Send mail here;

            var mailHolder = (MailHolder)state;
            try
            {

                var mail = new System.Net.Mail.MailMessage
                    {
                        Subject = mailHolder.Subject,
                        SubjectEncoding = System.Text.Encoding.UTF8,
                        Body = mailHolder.Body,
                        BodyEncoding = System.Text.Encoding.UTF8,
                        IsBodyHtml = true,
                        Priority = MailPriority.Normal
                    };

                var client = new SmtpClient();
                //Add the Creddentials- use your own email id and password
                var maildisplayname = System.Configuration.ConfigurationManager.AppSettings["EmailDisplay"].ToString() != null ? System.Configuration.ConfigurationManager.AppSettings["EmailDisplay"].ToString() : "Marcom";
                mail.From = new System.Net.Mail.MailAddress(System.Configuration.ConfigurationManager.AppSettings["Email"].ToString(), maildisplayname);
                //client.Credentials = new System.Net.NetworkCredential("marcom.noreply@brandsystems.in", "bsi1234$");
                //client.Host = "webmail.brandsystems.in";

                foreach (var mails in mailHolder.PrimaryTo)
                {
                    mail.To.Add(mails.ToString());
                    try
                    {
                        client.SendAsync(mail, null);
                    }
                    catch (Exception ex)
                    {
                        Exception ex2 = ex;
                        string errorMessage = string.Empty;
                        while (ex2 != null)
                        {
                            errorMessage += ex2.ToString();
                            ex2 = ex2.InnerException;
                        }
                        HttpContext.Current.Response.Write(errorMessage);
                    } // end try 
                }


                mailStatusUpdate(mailHolder, MailStatus.Pending);

            }
            catch (Exception)
            {
                throw;
            }

            return mailHolder;
        }
        /// <summary>
        ///  For a single job if  response is nothing it will provide some basic information like error details ,AditionalInfo,GroupID,JobID
        /// </summary>
        /// <param name="result">Provides details like Exception ,In progress,Done.. ,</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public void PostSendMailEvent(IWorkItemResult result)
        {

            MailHolder mailHolder = (MailHolder)result.State;



            if (result.Exception != null)
            {

                if (mailStatusUpdate != null)
                {
                    mailStatusUpdate(mailHolder, MailStatus.Error);
                }
                //Fail
                //Read the mail id and update the database status to inprogress and increase the iteration


            }
            else
            {
                if (mailStatusUpdate != null)
                {
                    mailStatusUpdate(mailHolder, MailStatus.Inprogress);
                }

                //Success
                //Read tha mail and update the status
            }



        }
        public MailHolder InsertMailIntoDb(MailHolder obj)
        {
            MailServer.Instance.AddToQueque(obj, "insert");

            return obj;
        }
        public object HandleMail(object state)
        {

            MailHolder mailHolder = (MailHolder)state;

            ClsDb clsDb = new ClsDb(TenantsConnStr);
            DataSet dataSet = new DataSet();
            StringBuilder strquery = new StringBuilder();
            try
            {
                MailDao mailDao = new MailDao();

                dataSet = clsDb.MailData("SELECT cmc.ID, cmc.[Subject], cmc.Body, cmc.[description],cmc.ContainerID  FROM CM_MailContent cmc where cmc.ID='" + mailHolder.MailType + "' and cmc.ContainerID !=0 and cmc.ContainerID NOT IN (SELECT id FROM CM_MailContent cmc2 WHERE cmc2.ContainerID=0) ;", CommandType.Text);

                if (mailHolder.MailType != 0)
                {

                    mailDao.ActualTime = DateTimeOffset.UtcNow;
                    StringBuilder mailBody = new StringBuilder();
                    mailBody.Append(dataSet.Tables[0].Rows[0]["Body"].ToString());
                    foreach (var key in mailHolder.MailTemplateDictionary.Keys)
                    {
                        if (mailBody.ToString().Contains(key))
                        {
                            mailBody.Replace(key, mailHolder.MailTemplateDictionary[key]);
                        }

                        mailDao.Body = mailBody.ToString();

                    }
                    mailDao.Subject = dataSet.Tables[0].Rows[0]["Subject"].ToString();
                }
                else
                {
                    mailDao.Subject = mailHolder.Subject;
                    mailDao.Body = mailHolder.Body;
                }
                if (mailHolder.PrimaryTo != null)
                {
                    foreach (var mail in mailHolder.PrimaryTo)
                    {
                        mailDao.ToMail = mail;
                        strquery.AppendLine("INSERT INTO CM_Mail(ToMail,[Subject],Body,ActualTime,[Status])VALUES('" + mailDao.ToMail + "','" + mailDao.Subject + "','" + mailDao.Body + "','" + DateTimeOffset.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "','" + MailStatus.Pending + "','" + mailHolder.MailType + "',0); SELECT SCOPE_IDENTITY()");
                        dataSet = clsDb.MailData(strquery.ToString(), CommandType.Text);
                        mailHolder.Id = dataSet.Tables[0].Rows[0][0].ToString();
                    }
                }
                else
                {
                    foreach (var mail in mailHolder.SecondaryTo)
                    {
                        mailDao.ToMail = mail;
                        strquery.AppendLine("declare @emailenable as bit");
                        strquery.AppendLine("select @emailenable=isemailenable from  CM_UserMailSubscription where UserID in( select id from UM_User where Email='" + mailDao.ToMail + "')");
                        strquery.AppendLine("if @emailenable =1");
                        strquery.AppendLine("begin");
                        strquery.AppendLine("INSERT INTO CM_Mail(ToMail,[Subject],Body,ActualTime,[Status])VALUES('" + mailDao.ToMail + "','" + mailDao.Subject + "','" + mailDao.Body + "','" + DateTimeOffset.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "','" + MailStatus.Pending + "','" + mailHolder.MailType + "',1); SELECT SCOPE_IDENTITY()");
                        strquery.AppendLine("end");
                        dataSet = clsDb.MailData(strquery.ToString(), CommandType.Text);
                        mailHolder.Id = dataSet.Tables[0].Rows[0][0].ToString();
                    }
                }
            }

            catch
            {

            }
            return mailHolder;
        }
        public object HandleUnScheduledMail(object state, int TenantID, string TenantHost)   //insert immediately and  call fetch to send mail immediately
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, MarcomManagerFactory.GetSystemSession(TenantID));
            MailHolder mailHolder = (MailHolder)state;
            ClsDb clsDb = new ClsDb(TenantHost);
            DataSet dataSet = new DataSet();
            StringBuilder mailBody = new StringBuilder();
            StringBuilder mailSubjectHeader = new StringBuilder();
            StringBuilder mailHeader = new StringBuilder();
            StringBuilder mailFormatted = new StringBuilder();
            StringBuilder strquery = new StringBuilder();


            try
            {
                MailDao mailDao = new MailDao();
                string TenantFilePath = GetTenantFilePath(TenantID);
                dataSet = clsDb.MailData("SELECT cmc.ID, cmc.[Subject], cmc.Body, cmc.[description],cmc.ContainerID  FROM CM_MailContent cmc where cmc.ID='" + mailHolder.MailType + "' and cmc.ContainerID !=0 and cmc.ContainerID NOT IN (SELECT id FROM CM_MailContent cmc2 WHERE cmc2.ContainerID=0) ;", CommandType.Text);

                XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);

                //The Key is root node current Settings
                string xelementName = "ApplicationURL";

                var xmlElement = xelementFilepath.Element(xelementName);
                if (mailHolder.MailType != 0)
                {

                    mailDao.ActualTime = DateTimeOffset.UtcNow;
                    mailDao.Subject = dataSet.Tables[0].Rows[0]["Subject"].ToString();
                    mailBody.Append(dataSet.Tables[0].Rows[0]["Body"].ToString());
                    foreach (var key in mailHolder.MailTemplateDictionary.Keys)
                    {
                        if (mailBody.ToString().Contains(key))
                        {
                            mailBody.Replace(key, mailHolder.MailTemplateDictionary[key]);
                        }

                    }

                    if (marcomManager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        string cloudurl = (marcomManager.User.AwsStorage.Uploaderurl + "/" + marcomManager.User.AwsStorage.BucketName + "/" + GetTenantFilePath(TenantID)).Replace("\\", "/");
                        mailBody = mailBody.Replace("@ImagePath@", cloudurl + "logo.png"); // Need to change the Path of the logo here.
                    }
                    else
                    {
                        mailBody = mailBody.Replace("@ImagePath@", xmlElement.Value.ToString() + GetTenantFilePath(TenantID) + "/logo.png"); // Need to change the Path of the logo here.
                    }

                }
                else
                {
                    mailDao.Subject = mailHolder.Subject;
                    mailDao.Body = mailHolder.Body;
                    if (marcomManager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        string cloudurl = (marcomManager.User.AwsStorage.Uploaderurl + "/" + marcomManager.User.AwsStorage.BucketName + "/" + GetTenantFilePath(TenantID)).Replace("\\", "/");
                        mailBody = mailBody.Replace("@ImagePath@", cloudurl + "logo.png"); // Need to change the Path of the logo here.
                    }
                    else
                    {
                        mailBody = mailBody.Replace("@ImagePath@", xmlElement.Value.ToString() + GetTenantFilePath(TenantID) + "/logo.png"); // Need to change the Path of the logo here.
                    }

                }
                mailDao.Body = mailBody.ToString();
                if (mailHolder.PrimaryTo != null)
                {
                    foreach (var mail in mailHolder.PrimaryTo)
                    {
                        mailDao.ToMail = mail;
                        strquery.AppendLine("INSERT INTO CM_Mail(ToMail,[Subject],Body,ActualTime,[Status],mailtype,isrecapmail)VALUES('" + mailDao.ToMail + "','" + mailDao.Subject + "','" + mailDao.Body + "','" + DateTimeOffset.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "','" + MailStatus.Pending + "','" + mailHolder.MailType + "',0); SELECT SCOPE_IDENTITY()");
                        dataSet = clsDb.MailData(strquery.ToString(), CommandType.Text);
                    }
                }
                if (mailHolder.SecondaryTo != null)
                {
                    foreach (var mail in mailHolder.SecondaryTo)
                    {
                        mailDao.ToMail = mail;
                        strquery.AppendLine("declare @emailenable as bit");
                        strquery.AppendLine("select @emailenable=isemailenable from  CM_UserMailSubscription where UserID in( select id from UM_User where Email='" + mailDao.ToMail + "')");
                        strquery.AppendLine("if @emailenable =1");
                        strquery.AppendLine("begin");
                        strquery.AppendLine("INSERT INTO CM_Mail(ToMail,[Subject],Body,ActualTime,[Status],mailtype,isrecapmail)VALUES('" + mailDao.ToMail + "','" + mailDao.Subject + "','" + mailDao.Body + "','" + DateTimeOffset.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "','" + MailStatus.Pending + "','" + mailHolder.MailType + "',1); SELECT SCOPE_IDENTITY()");
                        strquery.AppendLine("end");
                        dataSet = clsDb.MailData(strquery.ToString(), CommandType.Text);
                    }
                }
                mailHolder.Id = dataSet.Tables[0].Rows[0][0].ToString();

                mailHolder.Subject = mailDao.Subject;
                mailHolder.PrimaryTo = new List<string>();
                mailHolder.PrimaryTo.Add(mailDao.ToMail);
                //IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null,MarcomManagerFactory.GetSystemSession(TenantID));
                string SSOUserQueryString = "";
                DataSet ds = new DataSet();
                StringBuilder formatmail = new StringBuilder();
                using (ITransaction tx = marcomManager.GetTransaction(TenantID))
                {
                    var userDetails = (from tt in tx.PersistenceManager.CommonRepository[marcomManager.User.TenantID].Query<UserDao>() where tt.Email == mailDao.ToMail select tt).FirstOrDefault();
                    bool isSsoUser = userDetails.IsSSOUser;
                    if (isSsoUser)
                    {
                        SSOUserQueryString = "&IsSSO=true";
                    }

                    ds = clsDb.MailData("SELECT cmc.ID, cmc.[Subject], cmc.Body, cmc.[description],cmc.ContainerID  FROM CM_MailContent cmc", CommandType.Text);
                    var subjidfetch = (from cc in ds.Tables[0].AsEnumerable() where cc.Field<int>("ID") == Convert.ToInt32(mailHolder.MailType) select cc).FirstOrDefault();
                    var subjectcontenttemplate = (from cc in ds.Tables[0].AsEnumerable() where cc.Field<int>("ID") == Convert.ToInt32(subjidfetch.Field<int>("ContainerID")) select cc).FirstOrDefault();
                    var headertemplate = (from cont in ds.Tables[0].AsEnumerable() where cont.Field<int>("ID") == Convert.ToInt32(subjectcontenttemplate.Field<int>("ContainerID")) select cont).FirstOrDefault();
                    formatmail.Append(headertemplate.Field<string>("Body"));
                    formatmail.Replace("@##@", subjectcontenttemplate.Field<string>("Body").ToString());
                    formatmail.Replace("@##@", mailDao.Body.ToString());
                    formatmail.Replace("@Subject@", mailDao.Subject.ToString());
                    formatmail.Replace("@AccSettings@", xmlElement.Value + "/login.html?ReturnURL=" + System.Web.HttpUtility.UrlEncode("#/mui/mypage") + SSOUserQueryString);
                    formatmail.Replace("@UserName@", userDetails.FirstName);
                    if (marcomManager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
                    {
                        string cloudurl = (marcomManager.User.AwsStorage.Uploaderurl + "/" + marcomManager.User.AwsStorage.BucketName + "/" + GetTenantFilePath(TenantID)).Replace("\\", "/");
                        formatmail.Replace("@ImagePath@", cloudurl + "logo.png");
                    }
                    else
                    {
                        formatmail.Replace("@ImagePath@", xmlElement.Value.ToString() + GetTenantFilePath(TenantID) + "/logo.png");
                    }
                    tx.Commit();
                }
                mailHolder.Body = formatmail.ToString();

                MailServer.Instance.AddToQueque(mailHolder, "fetch");  //as soon as inserted send mail 


            }

            catch
            {

            }
            return mailHolder;
        }
        public MailHolder HandleSendMail(int TenantID) //send task mails , mails for actions that are subscribed by the user and also recap mail.
        {

            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, MarcomManagerFactory.GetSystemSession(TenantID));
                MailHolder obj = new MailHolder();
                MailDao mailDao = new MailDao();
                StringBuilder mailBody = new StringBuilder();
                StringBuilder mailSubjectHeader = new StringBuilder();
                StringBuilder mailHeader = new StringBuilder();
                StringBuilder mailFormatted = new StringBuilder();
                DataTable dataTableHeader = new DataTable("Header");
                DataTable dataTableSubjectHeader = new DataTable("SubjectHeader");
                DataTable dataTableSubject = new DataTable("Subject");
                ClsDb clsDb = new ClsDb(marcomManager.User.TenantHost);
                DataSet dataSet = new DataSet();
                DataSet collectionRecapReport = new DataSet();
                dataSet = clsDb.MailData("SELECT * FROM CM_Mail cm WHERE CM.NoOfTrial < 10 AND CM.ActualTime < '" + DateTime.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "' AND CM.[Status]='Pending' ;", CommandType.Text);
                collectionRecapReport = clsDb.MailData("SELECT * FROM CM_Mail cm WHERE CM.NoOfTrial < 10 AND CM.RecapTime < '" + DateTime.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "' AND CM.isrecapSent=0 ;", CommandType.Text);
                var user = nuller;
                string TenantFilePath = GetTenantFilePath(TenantID);

                XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);
                //The Key is root node current Settings
                string xelementName = "ApplicationURL";

                var xmlElement = xelementFilepath.Element(xelementName);
                string logopath1 = "";
                string ImagePath1 = "";

                DataTable dataTableFetchForMailClub = new DataTable("FetchForMailClub");
                string recapsubjectstring = "Recap Report";
                StringBuilder recapmailbody = new StringBuilder();
                if (marcomManager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
                {
                    string cloudurl = (marcomManager.User.AwsStorage.Uploaderurl + "/" + marcomManager.User.AwsStorage.BucketName + "/" + GetTenantFilePath(marcomManager.User.TenantID)).Replace("\\", "/");
                    logopath1 = cloudurl + "logo.png";
                }
                else
                {
                    logopath1 = xmlElement.Value.ToString() + GetTenantFilePath(marcomManager.User.TenantID) + "/logo.png";
                }
                string TenantPath = GetTenantFilePath(marcomManager.User.TenantID);
                string SSOUserQueryString = "";
                int mainmailcontainerid = 0;
                if (dataSet.Tables.Count > 0)
                {
                    using (ITransaction tx = marcomManager.GetTransaction(marcomManager.User.TenantID))
                    {
                        DataSet ds = new DataSet();
                        StringBuilder formatmail = new StringBuilder();
                        ds = clsDb.MailData("SELECT cmc.ID, cmc.[Subject], cmc.Body, cmc.[description],cmc.ContainerID  FROM CM_MailContent cmc", CommandType.Text);
                        var NotificationMails = dataSet.Tables[0].AsEnumerable().ToList().GroupBy(s => s.Field<string>("ToMail"));

                        foreach (var usergroup in NotificationMails)
                        {
                            try
                            {

                                var userdetails = (from tt in tx.PersistenceManager.CommonRepository[marcomManager.User.TenantID].Query<UserDao>() where tt.Email == usergroup.Key select tt).FirstOrDefault();
                                var taskmails = (from uu in usergroup where uu.Field<int?>("mailtype") == 55 || uu.Field<int?>("mailtype") == 57 select uu).ToList();
                                ImagePath1 = xmlElement.Value.ToString() + GetTenantFilePath(marcomManager.User.TenantID) + "/UserImages/" + userdetails.Id + ".jpg";
                                //  if (System.IO.File.Exists(ImagePath1))
                                if (marcomManager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
                                {
                                    string cloudurl = (marcomManager.User.AwsStorage.Uploaderurl + "/" + marcomManager.User.AwsStorage.BucketName + "/" + GetTenantFilePath(marcomManager.User.TenantID)).Replace("\\", "/");
                                    string s3key = GetTenantFilePath(marcomManager.User.TenantID) + "UserImages/" + userdetails.Id + ".jpg";
                                    ImagePath1 = cloudurl + "UserImages/" + userdetails.Id + ".jpg";
                                    bool result = AWSHelper.isKeyExists(marcomManager.User.AwsStorage.S3, marcomManager.User.AwsStorage.BucketName, s3key);
                                    if (!result)
                                    {
                                        ImagePath1 = cloudurl + "UserImages/noimage.jpg";
                                    }
                                }
                                else
                                {
                                    if (System.IO.File.Exists((Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + TenantPath + "/UserImages/", userdetails.Id + ".jpg"))) == false)
                                        ImagePath1 = xmlElement.Value.ToString() + GetTenantFilePath(marcomManager.User.TenantID) + "/UserImages/" + userdetails.Id + ".jpg";
                                    else
                                        ImagePath1 = xmlElement.Value.ToString() + GetTenantFilePath(marcomManager.User.TenantID) + "/UserImages/noimage.jpg";
                                }


                                bool isSsoUser = userdetails.IsSSOUser;
                                if (isSsoUser)
                                {
                                    SSOUserQueryString = "&IsSSO=true";
                                }
                                if (taskmails.Count() > 0)
                                {

                                    foreach (var shoe in taskmails)
                                    {
                                        formatmail = new StringBuilder();

                                        var subject = shoe.Field<string>("Subject").ToString();
                                        var mainmail = (from cont in ds.Tables[0].AsEnumerable() where cont.Field<int>("ID") == Convert.ToInt32(shoe.Field<int>("mailtype")) select cont).FirstOrDefault();
                                        if (mainmail != null)
                                        {
                                            var subjectcontenttemplate = (from cc in ds.Tables[0].AsEnumerable() where cc.Field<int>("ID") == Convert.ToInt32(mainmail.Field<int>("ContainerID")) select cc).FirstOrDefault();
                                            var headertemplate = (from cont in ds.Tables[0].AsEnumerable() where cont.Field<int>("ID") == Convert.ToInt32(subjectcontenttemplate.Field<int>("ContainerID")) select cont).FirstOrDefault();
                                            string subjectstring = subjectcontenttemplate.Field<string>("Body").ToString().Replace("@##@", mainmail.Field<string>("Body").ToString());
                                            formatmail.Append(headertemplate.Field<string>("Body"));
                                            formatmail.Replace("@##@", subjectcontenttemplate.Field<string>("Body"));
                                            formatmail.Replace("@##@", shoe.Field<string>("Body").ToString());

                                            //formatmail.Append(shoe.Field<string>("Body").ToString());
                                            formatmail.Replace("@ImagePath@", logopath1);
                                            formatmail.Replace("@UserName@", userdetails.FirstName);
                                            formatmail.Replace("@AccSettings@", xmlElement.Value + "/login.html?ReturnURL=" + System.Web.HttpUtility.UrlEncode("#/mui/mypage") + SSOUserQueryString);

                                            mailDao.Subject = subject.ToString();
                                            mailDao.ToMail = usergroup.Key;
                                            mailDao.Body = formatmail.ToString();
                                            obj.Body = mailDao.Body;
                                            obj.Subject = mailDao.Subject;
                                            obj.PrimaryTo = new List<string>();
                                            obj.PrimaryTo.Add(mailDao.ToMail);
                                            LogHandler.LogInfo("task mail sent", LogHandler.LogType.Notify);
                                            MailServer.Instance.AddToQueque(obj, "fetch");
                                        }
                                    }

                                }
                                foreach (var shoe in usergroup)     //to send mails for each action when performed.
                                {
                                    formatmail = new StringBuilder();
                                    if (shoe.Field<int>("mailtype") != 55 && shoe.Field<int>("mailtype") != 57)
                                    {
                                        var subject = shoe.Field<string>("Subject").ToString();
                                        var mainmail = (from cont in ds.Tables[0].AsEnumerable() where cont.Field<int>("ID") == Convert.ToInt32(shoe.Field<int>("mailtype")) select cont).FirstOrDefault();
                                        if (mainmail != null)
                                        {
                                            var subjectcontenttemplate = (from cc in ds.Tables[0].AsEnumerable() where cc.Field<int>("ID") == Convert.ToInt32(mainmail.Field<int>("ContainerID")) select cc).FirstOrDefault();
                                            var headertemplate = (from cont in ds.Tables[0].AsEnumerable() where cont.Field<int>("ID") == Convert.ToInt32(subjectcontenttemplate.Field<int>("ContainerID")) select cont).FirstOrDefault();
                                            string subjectstring = subjectcontenttemplate.Field<string>("Body").ToString().Replace("@##@", mainmail.Field<string>("Body").ToString());
                                            formatmail.Append(headertemplate.Field<string>("Body"));
                                            formatmail.Replace("@##@", subjectcontenttemplate.Field<string>("Body"));
                                            formatmail.Replace("@##@", shoe.Field<string>("Body"));
                                            formatmail.Replace("@Subject@", subject.ToString());
                                            formatmail.Replace("@ImagePath@", logopath1);
                                            formatmail.Replace("@UserName@", userdetails.FirstName);
                                            formatmail.Replace("@AccSettings@", xmlElement.Value + "/login.html?ReturnURL=" + System.Web.HttpUtility.UrlEncode("#/mui/mypage") + SSOUserQueryString);
                                            mailDao.Subject = subject.ToString();

                                            mailDao.ToMail = usergroup.Key;
                                            mailDao.Body = formatmail.ToString();
                                            obj.Body = mailDao.Body;
                                            obj.Subject = mailDao.Subject;
                                            obj.PrimaryTo = new List<string>();
                                            obj.PrimaryTo.Add(mailDao.ToMail);

                                            LogHandler.LogInfo("individual mail sent", LogHandler.LogType.Notify);

                                            MailServer.Instance.AddToQueque(obj, "fetch");
                                        }
                                    }
                                }


                            }
                            catch (Exception ex)
                            {
                                LogHandler.LogInfo("exception generated while sending mail" + ex.Message, LogHandler.LogType.Notify);
                            }

                        }
                        //Recap Report!
                        if (collectionRecapReport.Tables.Count > 0 && collectionRecapReport != null)
                        {
                            var RecapMails = collectionRecapReport.Tables[0].AsEnumerable().ToList().GroupBy(s => s.Field<string>("ToMail"));

                            foreach (var usergroup in RecapMails)
                            {
                                var userdetails = (from tt in tx.PersistenceManager.CommonRepository[marcomManager.User.TenantID].Query<UserDao>() where tt.Email == usergroup.Key select tt).FirstOrDefault();
                                var mailsubscriptiondetails = (from tt in tx.PersistenceManager.CommonRepository[marcomManager.User.TenantID].Query<UserMailSubscriptionDao>() where tt.Userid == Convert.ToInt32(userdetails.Id) select tt).FirstOrDefault();

                                if (mailsubscriptiondetails != null)
                                {
                                    if (mailsubscriptiondetails.RecapReport == true)
                                    {
                                        formatmail = new StringBuilder();
                                        var recapmailgroup = (from tt in usergroup.AsEnumerable().ToList() where tt.Field<bool>("isrecapmail") == true && (tt.Field<int>("mailtype") != 55 || tt.Field<int>("mailtype") != 57) select tt).ToList();
                                        foreach (var recapmails in recapmailgroup)
                                        {
                                            var mainmail = (from cont in ds.Tables[0].AsEnumerable() where cont.Field<int>("ID") == Convert.ToInt32(recapmails.ItemArray[9]) select cont).FirstOrDefault();
                                            recapmailbody.Append(recapmails.ItemArray[3].ToString());
                                            mainmailcontainerid = mainmail.Field<int>("ContainerID");

                                        }
                                        if (mainmailcontainerid != 0 && mainmailcontainerid != null)
                                        {
                                            var subjectcontenttemplate = (from cc in ds.Tables[0].AsEnumerable() where cc.Field<int>("ID") == Convert.ToInt32(mainmailcontainerid) select cc).FirstOrDefault();
                                            var headertemplate = (from cont in ds.Tables[0].AsEnumerable() where cont.Field<int>("ID") == Convert.ToInt32(subjectcontenttemplate.Field<int>("ContainerID")) select cont).FirstOrDefault();
                                            formatmail.Append(headertemplate.Field<string>("Body"));
                                            formatmail.Replace("@##@", subjectcontenttemplate.Field<string>("Body"));
                                            formatmail.Replace("@##@", recapmailbody.ToString());
                                            recapsubjectstring = recapsubjectstring;
                                            formatmail.Replace("@Subject@", recapsubjectstring.ToString());
                                            formatmail.Replace("@ImagePath@", logopath1);
                                            formatmail.Replace("@UserName@", userdetails.FirstName);
                                            formatmail.Replace("@AccSettings@", xmlElement.Value + "/login.html?ReturnURL=" + System.Web.HttpUtility.UrlEncode("#/mui/mypage") + SSOUserQueryString);
                                            mailDao.Subject = recapsubjectstring.ToString();

                                            mailDao.ToMail = usergroup.Key;
                                            mailDao.Body = formatmail.ToString();
                                            obj.Body = mailDao.Body;
                                            obj.Subject = mailDao.Subject;
                                            obj.PrimaryTo = new List<string>();
                                            obj.PrimaryTo.Add(mailDao.ToMail);
                                            LogHandler.LogInfo("recap mail sent", LogHandler.LogType.Notify);
                                            MailServer.Instance.AddToQueque(obj, "RecapReport");
                                        }
                                    }
                                }
                            }
                        }
                        tx.Commit();
                    }

                }

                return obj;
            }
            catch (Exception ex)
            {
                LogHandler.LogInfo("exception generated while sending mail" + ex.Message, LogHandler.LogType.Notify);
                return null;
            }
        }

        public void MailStatusUpdateToDb(MailHolder mailHolder, MailStatus status)   //to update the status of mail and sent time once scheduler runs 
        {
            //loop through each client and pdate the status of mail and sent time once scheduler runs 
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            XmlDocument xdcDocument = new XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            XmlElement xelRoot = xdcDocument.DocumentElement;
            XmlNodeList xnlNodes = xelRoot.SelectNodes("/Tenants/Tenant");
            foreach (XmlNode xndNode in xnlNodes)
            {
                string TenantsHost = xndNode.Attributes["TenantHost"].Value;
                ClsDb clsDb = new ClsDb(TenantsHost);
                DataSet dataSet = new DataSet();
                DataSet RecapData = new DataSet();

                string updateStatus = null;
                DataTable datatablepending = new DataTable();
                DataTable datatableInprogress = new DataTable();
                if (status == MailStatus.Pending)
                {
                    updateStatus = MailStatus.Inprogress.ToString();
                }
                else if (status == MailStatus.Inprogress)
                {
                    updateStatus = MailStatus.Sent.ToString();
                }
                else if (status == MailStatus.Error)
                {
                    updateStatus = MailStatus.Error.ToString();
                }

                dataSet = clsDb.MailData("SELECT * FROM CM_Mail cm WHERE cm.[ActualTime] < '" + DateTimeOffset.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "' ", CommandType.Text);
                RecapData = clsDb.MailData("SELECT * FROM CM_Mail cm WHERE cm.recaptime < '" + DateTimeOffset.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "' ", CommandType.Text);

                try
                {

                    if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
                    {

                        var dp = (from ds in dataSet.Tables[0].AsEnumerable() where ds.Field<string>("Status") == MailStatus.Pending.ToString() select ds);

                        var dip = (from ds in dataSet.Tables[0].AsEnumerable() where ds.Field<string>("Status") == MailStatus.Inprogress.ToString() select ds);
                        if (dp.Count() > 0)
                        {
                            datatablepending = dp.CopyToDataTable();
                        }
                        if (dip.Count() > 0)
                        {
                            datatableInprogress = dip.CopyToDataTable();
                        }
                    }

                }
                catch (Exception e)
                {

                }
                if (datatablepending.Rows.Count != 0)
                {
                    for (int i = 0; i < datatablepending.Rows.Count; i++)
                    {

                        dataSet = clsDb.MailData("UPDATE CM_Mail SET [Status] = '" + updateStatus + "',LastProccessedTime = '" + DateTimeOffset.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "' WHERE Id='" + datatablepending.Rows[i]["Id"].ToString() + "'", CommandType.Text);
                    }

                }
                if (datatableInprogress.Rows.Count != 0)
                {
                    for (int i = 0; i < datatableInprogress.Rows.Count; i++)
                    {
                        dataSet = clsDb.MailData("UPDATE CM_Mail SET [Status] = '" + updateStatus + "',SentTime = '" + DateTimeOffset.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "',LastProccessedTime = '" + DateTimeOffset.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "' WHERE Id='" + datatableInprogress.Rows[i]["Id"].ToString() + "'", CommandType.Text);
                    }

                }
                if (status == MailStatus.Error)
                {
                    int nooftrials = 0;
                    if (dataSet.Tables.Count > 0)
                    {
                        for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                        {
                            nooftrials = Convert.ToInt16(dataSet.Tables[0].Rows[i]["NoOfTrial"].ToString());
                            nooftrials = nooftrials + 1;
                            clsDb.MailData("UPDATE CM_Mail SET  NoOfTrial ='" + nooftrials + "',[Status] = '" + MailStatus.Error + "',LastProccessedTime = '" + DateTimeOffset.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "' WHERE Id='" + dataSet.Tables[i].Rows[i]["Id"].ToString() + "'", CommandType.Text);
                        }
                    }
                }
                if (mailHolder.isrecap == true)
                {
                    if (RecapData != null && RecapData.Tables.Count > 0)
                    {
                        for (int i = 0; i < RecapData.Tables[0].Rows.Count; i++)
                        {
                            clsDb.MailData("UPDATE CM_Mail SET isrecapsent=1,LastProccessedTime = '" + DateTimeOffset.UtcNow.ToString("yyyy-MM-dd hh:mm:ss.fff tt") + "' WHERE Id='" + RecapData.Tables[0].Rows[i]["Id"].ToString() + "'", CommandType.Text);
                        }
                    }
                }
            }

        }

        public void InsertIntoMailNotification(int TenantID)    //To insert task reminder and overdue tasks into mail table ,when task scheduler runs
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, MarcomManagerFactory.GetSystemSession(TenantID));

            using (ITransaction txInnerloop = marcomManager.GetTransaction(marcomManager.User.TenantID))
            {
                ClsDb clsDb = new ClsDb(marcomManager.User.TenantHost);
                DataSet dataSet = new DataSet();
                DataSet ds = new DataSet();
                StringBuilder strquery = new StringBuilder();
                try
                {
                    string TenantPath = GetTenantFilePath(TenantID);
                    string xmlpath = "";
                    XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, TenantID);
                    //The Key is root node current Settings
                    string xelementName = "ApplicationURL";

                    var xmlElement = xelementFilepath.Element(xelementName);


                    strquery.AppendLine("select et.ID,et.EntityID,et.DueDate, et.TaskStatus,tm.RoleID,tm.UserID,et.Name,et.TaskType,tm.OverdueMailSent,tm.IsTaskalertSent,STUFF(ISNULL( ( SELECT  ',' + cast(ttm.userID AS NVARCHAR(1000))  FROM   TM_Task_Members ttm WHERE  ttm.TaskID = et.ID AND ttm.RoleID = 1 FOR XML PATH('') ), ',' ),1,1,'') AS 'TaskAssigned', STUFF(ISNULL( (SELECT DISTINCT  ',' +  FirstName + ' ' + LASTNAME FROM   UM_USER WHERE  ID = ((SELECT TOP 1 userID FROM   TM_Task_Members ttm WHERE  ttm.TaskID = et.ID AND ttm.RoleID = 1 ) ) FOR XML PATH('') ), ',' ),1,1,'') AS 'OwnerName' into #temp1 from TM_EntityTask et inner join TM_Task_Members tm on et.ID=tm.TaskID  INNER JOIN PM_Entity pe ON pe.id = et.id  where tm.RoleID=4  AND pe.Active = 1 AND et.TaskStatus=1");
                    strquery.AppendLine("select distinct tm.RoleID,tm.UserID,tm.TaskID,tun.NoOfDays,tun.NotificationTiming  into #temp2 from TM_Task_Members tm inner join TM_UserNotificationMailSettings  tun on tm.UserID=tun.UserID where tun.IsEmailEnable=1 and tm.RoleID=4");
                    strquery.AppendLine("select  distinct a.ID, a.EntityID,a.TaskType,a.Name,a.TaskStatus,a.RoleID,a.UserID,b.NoOfDays,a.DueDate,DATEDIFF(day, GETDATE(), a.DueDate) as 'overduedays',a.OverdueMailSent,0 as 'IsTaskalertSent',a.TaskAssigned,a.OwnerName,b.NotificationTiming  into #temp3 from #temp1 a inner join #temp2 b on a.ID=b.TaskID and a.UserID = b.UserID where DATEDIFF(day, GETDATE(), a.DueDate) < 0 and (CONVERT(date,'" + DateTime.UtcNow.ToString("yyyy.MM.dd") + "',102)<>CONVERT(date, a.OverdueMailSent) or a.overduemailsent  IS NULL)");//this to fetch overdue tasks remove IsTaskalertSent
                    //strquery.AppendLine("select a.ID, a.EntityID,a.TaskType,a.Name,a.TaskStatus,a.RoleID,a.UserID,b.NoOfDays,a.DueDate,DATEDIFF(day, GETDATE(), a.DueDate) as 'overduedays',a.OverdueMailSent,a.IsTaskalertSent,a.TaskAssigned,a.OwnerName,b.NotificationTiming  into #temp3 from #temp1 a inner join #temp2 b on a.ID=b.TaskID and a.UserID = b.UserID where DATEDIFF(day, GETDATE(), a.DueDate) < 0 and (CONVERT(date,'" + DateTime.UtcNow.ToString("yyyy.MM.dd") + "',102)<>CONVERT(date, a.OverdueMailSent) or a.overduemailsent  IS NULL)");//this to fetch overdue tasks
                    strquery.AppendLine("select a.ID, a.EntityID,a.TaskType,a.Name,a.TaskStatus,a.RoleID,a.UserID,b.NoOfDays,a.DueDate,DATEDIFF(day, GETDATE(), a.DueDate) as 'duedays',a.OverdueMailSent,a.IsTaskalertSent,b.NotificationTiming into #temp4 from #temp1 a inner join #temp2 b on a.ID=b.TaskID and a.UserID = b.UserID where DATEDIFF(day, GETDATE(), a.DueDate)= b.NoOfDays and (a.IsTaskalertSent =0 or a.IsTaskalertSent IS NULL) ");//this to fetch alert according to no of days


                    strquery.AppendLine("select et.ID,et.EntityID,et.DueDate, et.TaskStatus,tm.RoleID,tm.UserID,et.Name,et.TaskType,tm.OverdueMailSent,tm.IsTaskalertSent,STUFF(ISNULL( ( SELECT  ',' + cast(ttm.userID AS NVARCHAR(1000))  FROM   TM_Task_Members ttm WHERE  ttm.TaskID = et.ID AND ttm.RoleID =4 FOR XML PATH('') ), ',' ),1,1,'') AS 'TaskAssigned', STUFF(ISNULL( (SELECT DISTINCT  ',' +  FirstName + ' ' + LASTNAME FROM   UM_USER WHERE  ID IN ((SELECT userID FROM   TM_Task_Members ttm WHERE  ttm.TaskID = et.ID AND ttm.RoleID = 4 ) ) FOR XML PATH('') ), ',' ),1,1,'') AS 'OwnerName' into #temp5 from TM_EntityTask et inner join TM_Task_Members tm on et.ID=tm.TaskID  INNER JOIN PM_Entity pe ON pe.ID = et.ID where tm.RoleID=1 AND pe.Active = 1 and et.TaskStatus=1 ");
                    strquery.AppendLine("select distinct tm.RoleID,tm.UserID,tm.TaskID,tun.NoOfDays,tun.NotificationTiming into #temp6 from TM_Task_Members tm inner join TM_UserNotificationMailSettings  tun on tm.UserID=tun.UserID where tun.IsEmailEnable=1 and tm.RoleID=1 ");
                    strquery.AppendLine("select  distinct a.ID, a.EntityID,a.TaskType,a.Name,a.TaskStatus,a.RoleID,a.UserID,b.NoOfDays,a.DueDate,DATEDIFF(day, GETDATE(), a.DueDate) as 'overduedays',a.OverdueMailSent,0 as 'IsTaskalertSent',a.TaskAssigned,a.OwnerName,b.NotificationTiming  into #temp7 from #temp5 a inner join #temp6 b on a.ID=b.TaskID and a.UserID = b.UserID where DATEDIFF(day, GETDATE(), a.DueDate) < 0 and (CONVERT(date,'" + DateTime.UtcNow.ToString("yyyy.MM.dd") + "',102)<>CONVERT(date, a.OverdueMailSent) or a.overduemailsent  IS NULL) ");//this remove IsTaskalertSent
                    //strquery.AppendLine("select a.ID, a.EntityID,a.TaskType,a.Name,a.TaskStatus,a.RoleID,a.UserID,b.NoOfDays,a.DueDate,DATEDIFF(day, GETDATE(), a.DueDate) as 'overduedays',a.OverdueMailSent,a.IsTaskalertSent,a.TaskAssigned,a.OwnerName,b.NotificationTiming  into #temp7 from #temp5 a inner join #temp6 b on a.ID=b.TaskID and a.UserID = b.UserID where DATEDIFF(day, GETDATE(), a.DueDate) < 0 and (CONVERT(date,'" + DateTime.UtcNow.ToString("yyyy.MM.dd") + "',102)<>CONVERT(date, a.OverdueMailSent) or a.overduemailsent  IS NULL) ");




                    strquery.AppendLine("select * from #temp3 ORDER BY DueDate ");
                    strquery.AppendLine("select * from #temp4");
                    strquery.AppendLine("SELECT * FROM #temp7 ORDER BY DueDate ");
                    strquery.AppendLine("SELECT userid FROM #temp3 UNION SELECT userid FROM #temp7");
                    strquery.AppendLine("drop table #temp1");
                    strquery.AppendLine("drop table #temp2");
                    strquery.AppendLine("drop table #temp3");
                    strquery.AppendLine("drop table #temp4");
                    strquery.AppendLine("DROP TABLE #temp5");
                    strquery.AppendLine("DROP TABLE #temp6");
                    strquery.AppendLine("DROP TABLE #temp7");

                    dataSet = clsDb.MailData(strquery.ToString(), CommandType.Text);
                    if (dataSet.Tables.Count == 0)
                    {
                        txInnerloop.Commit();
                        return;
                    }
                    var taskOverdueData = dataSet.Tables[0].AsEnumerable();
                    var taskReminderData = dataSet.Tables[1].AsEnumerable();
                    var taskOverdueDataHimself = dataSet.Tables[2].AsEnumerable();
                    var distinctUserIdColllection = dataSet.Tables[3].AsEnumerable();

                    StringBuilder stroverdue = new StringBuilder();
                    IList<MailDao> userNotifyfortasklist = new List<MailDao>();
                    DataSet dsnew = new DataSet();
                    dsnew = clsDb.MailData("SELECT cmc.ID, cmc.[Subject], cmc.Body, cmc.[description],cmc.ContainerID  FROM CM_MailContent cmc where Id in (54, 55)", CommandType.Text);
                    var body = (from sub in dsnew.Tables[0].AsEnumerable() select sub.Field<string>("Body")).ToList();

                    string overdueInnerContent = body[0].ToString();
                    string overdueCountHeader = body[1].ToString();
                    overdueCountHeader = overdueCountHeader.ToString().Replace("@Whom@", "TO").Replace("@toyouby@", "to you by");
                    string overdueCountHeaderHimeself = body[1].ToString();
                    overdueCountHeaderHimeself = overdueCountHeaderHimeself.ToString().Replace("@Whom@", "BY").Replace("@toyouby@", "by you to");
                    StringBuilder strlistofoverduetasks = new StringBuilder();
                    var OverDueTasks = dataSet.Tables[0].AsEnumerable().ToList().GroupBy(s => s.Field<int>("UserID"));
                    var OverDueAssignedToTasks = dataSet.Tables[2].AsEnumerable().ToList().GroupBy(s => s.Field<int>("UserID"));
                    TaskMembersDao tmd = new TaskMembersDao();
                    MatchCollection matches = Regex.Matches(overdueInnerContent, @"@(.+?)@");
                    string mailContent = "";
                    MailDao userNotifyfortask = new MailDao();
                    StringBuilder overdueInnerContentUser = new StringBuilder();

                    UserDao userDetails = new UserDao();
                    foreach (var uid in distinctUserIdColllection)
                    {
                        int TaskTOCount = 0;
                        int TaskBYCount = 0;
                        bool assignedtocontent = false;
                        bool assignedbycontent = false;
                        overdueInnerContentUser = new StringBuilder();
                        mailContent = "";
                        userDetails = (from users in txInnerloop.PersistenceManager.CommonRepository[marcomManager.User.TenantID].Query<UserDao>() where users.Id == Convert.ToInt32(uid.ItemArray[0]) select users).FirstOrDefault();
                        TimeSpan userTimeZone = new TimeSpan(Convert.ToInt32(userDetails.TimeZone.Split(':')[0]), Convert.ToInt32((userDetails.TimeZone.Split(':'))[1]), 0);
                        string IsEmailAssignedEnable = (from mailsettings in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<UserTaskNotificationMailSettingsDao>() where mailsettings.Userid == Convert.ToInt32(uid.ItemArray[0]) select mailsettings.IsEmailAssignedEnable).FirstOrDefault().ToString();
                        TimeSpan OverDueuserTimeZoneForNotify;
                        double OverDuenotifyhour = 0;
                        double OverDuenotifyMinutes = 0;
                        foreach (var userOverdueTask in OverDueTasks)
                        {
                            userNotifyfortask = new MailDao();
                            assignedtocontent = false;
                            assignedbycontent = false;

                            foreach (var tasks in userOverdueTask)
                            {
                                if (Convert.ToInt32(tasks.ItemArray[6]) == userDetails.Id)
                                {

                                    assignedbycontent = true;
                                    TaskTOCount++;
                                    OverDueuserTimeZoneForNotify = new TimeSpan(Convert.ToInt32(tasks.ItemArray[14].ToString().Split(':')[0]), Convert.ToInt32(tasks.ItemArray[14].ToString().Split(':')[1]), 0);
                                    OverDuenotifyhour = OverDueuserTimeZoneForNotify.Hours;
                                    OverDuenotifyMinutes = OverDueuserTimeZoneForNotify.Minutes;
                                    var parentEntityDetails = (from item in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityDao>() where item.Id == Convert.ToInt32(tasks.ItemArray[1]) select item).FirstOrDefault();
                                    var tasktype = (from type in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where type.Id == Convert.ToInt32(tasks.ItemArray[2]) select type.Caption).FirstOrDefault();
                                    StringBuilder taskContent = new StringBuilder(overdueInnerContent);

                                    foreach (Match match in matches)
                                    {
                                        switch (match.Value.Trim())
                                        {
                                            case "@DaysAgo@":
                                                {
                                                    TimeSpan difference = DateTime.UtcNow.Add(userTimeZone) - Convert.ToDateTime(tasks.ItemArray[8].ToString()).Add(userTimeZone);
                                                    taskContent.Replace(match.Value, difference.Days.ToString());
                                                    break;
                                                }
                                            case "@TaskDate@":
                                                {
                                                    taskContent.Replace(match.Value, Convert.ToDateTime(tasks.ItemArray[8].ToString()).ToString("yyyy-MM-dd"));
                                                    break;
                                                }
                                            case "@TaskType@":
                                                {
                                                    taskContent.Replace(match.Value, tasktype);
                                                    break;
                                                }
                                            case "@TaskName@":
                                                {
                                                    taskContent.Replace(match.Value, "<a href=" + marcomManager.CommonManager.GetEntityPathforMail(xmlElement.Value.ToString(), (int)tasks.ItemArray[0], 30, userDetails.Id, userDetails.IsSSOUser, (int)tasks.ItemArray[1]) + ">" + tasks.ItemArray[3].ToString() + "</a>");
                                                    taskContent.Replace(match.Value, tasks.ItemArray[3].ToString());
                                                    break;
                                                }
                                            case "@Path@":
                                                {
                                                    //Need to give link for path and above task
                                                    taskContent.Replace(match.Value, "<a href=" + marcomManager.CommonManager.GetEntityPathforMail(xmlElement.Value.ToString(), parentEntityDetails.Id, parentEntityDetails.Typeid, userDetails.Id, userDetails.IsSSOUser, 0) + ">" + parentEntityDetails.Name + "</a>");
                                                    break;
                                                }

                                            case "@UserNameTO@":
                                                {
                                                    string[] SplitArr = tasks.ItemArray[12].ToString().Split(',');
                                                    if (SplitArr.Contains(tasks.ItemArray[6].ToString()))
                                                    //Need to give link for path and above task
                                                    //if (tasks.ItemArray[6].ToString() == tasks.ItemArray[12].ToString())
                                                    {
                                                        taskContent.Replace(match.Value, "Oneself");
                                                    }
                                                    else
                                                    {
                                                        taskContent.Replace(match.Value, tasks.ItemArray[13].ToString());
                                                    }
                                                    break;
                                                }
                                            case "@userImagepath@":
                                                {
                                                    var UserImagePath = xmlElement.Value.ToString() + GetTenantFilePath(marcomManager.User.TenantID) + "/UserImages/" + tasks.ItemArray[12] + ".jpg";

                                                    if (marcomManager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
                                                    {
                                                        string cloudUrl = (marcomManager.User.AwsStorage.Uploaderurl + "/" + marcomManager.User.AwsStorage.BucketName + "/" + GetTenantFilePath(marcomManager.User.TenantID)).Replace("\\", "/");
                                                        string s3Key = GetTenantFilePath(marcomManager.User.TenantID) + "UserImages/" + tasks.ItemArray[12] + ".jpg";
                                                        UserImagePath = cloudUrl + "UserImages/" + tasks.ItemArray[12] + ".jpg";
                                                        bool result = AWSHelper.isKeyExists(marcomManager.User.AwsStorage.S3, marcomManager.User.AwsStorage.BucketName, s3Key);
                                                        if (!result)
                                                        {
                                                            UserImagePath = cloudUrl + "UserImages/noimage.jpg";
                                                        }
                                                        if (tasks.ItemArray[12].ToString().Contains(","))
                                                            UserImagePath = cloudUrl + "UserImages/Groups.jpg";
                                                    }
                                                    else
                                                    {
                                                        if (System.IO.File.Exists(Path.Combine(Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + TenantPath + "/UserImages/", tasks.ItemArray[12].ToString() + ".jpg"))) == false)
                                                            UserImagePath = xmlElement.Value.ToString() + GetTenantFilePath(marcomManager.User.TenantID) + "/UserImages/noimage.jpg";
                                                        if (tasks.ItemArray[12].ToString().Contains(","))
                                                            UserImagePath = xmlElement.Value.ToString() + GetTenantFilePath(marcomManager.User.TenantID) + "/UserImages/Groups.jpg";
                                                    }
                                                    taskContent.Replace(match.Value, UserImagePath);

                                                    break;
                                                }

                                        }


                                    }
                                    overdueInnerContentUser.Append(taskContent);
                                    var str = clsDb.InsertUpdateMailData("update TM_Task_Members set OverdueMailSent='" + DateTime.UtcNow.ToString("yyyy.MM.dd") + "' where userid=" + Convert.ToInt32(uid.ItemArray[0]) + " and TaskID=" + Convert.ToInt32(tasks.ItemArray[0]) + " AND RoleID = 4", CommandType.Text);
                                }
                            }
                            if (assignedbycontent)
                                mailContent += overdueCountHeader.Replace("@##@", overdueInnerContentUser.ToString()).Replace("@TaskCount@", TaskTOCount.ToString().Replace("@toyouby@", "to you by"));

                        }

                        if (IsEmailAssignedEnable == "True")
                        {
                            foreach (var userOverdueTask in OverDueAssignedToTasks)
                            {
                                userNotifyfortask = new MailDao();
                                overdueInnerContentUser = new StringBuilder();

                                assignedtocontent = false;
                                assignedbycontent = false;

                                foreach (var tasks in userOverdueTask)
                                {
                                    if (Convert.ToInt32(tasks.ItemArray[6]) == userDetails.Id)
                                    {
                                        assignedtocontent = true;
                                        TaskBYCount++;
                                        var parentEntityDetails = (from item in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityDao>() where item.Id == Convert.ToInt32(tasks.ItemArray[1]) select item).FirstOrDefault();
                                        var tasktype = (from type in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where type.Id == Convert.ToInt32(tasks.ItemArray[2]) select type.Caption).FirstOrDefault();
                                        StringBuilder taskContent = new StringBuilder(overdueInnerContent);

                                        foreach (Match match in matches)
                                        {
                                            switch (match.Value.Trim())
                                            {
                                                case "@DaysAgo@":
                                                    {
                                                        TimeSpan difference = DateTime.UtcNow.Add(userTimeZone) - Convert.ToDateTime(tasks.ItemArray[8].ToString()).Add(userTimeZone);
                                                        taskContent.Replace(match.Value, difference.Days.ToString());
                                                        break;
                                                    }
                                                case "@TaskDate@":
                                                    {
                                                        taskContent.Replace(match.Value, Convert.ToDateTime(tasks.ItemArray[8].ToString()).ToString("yyyy-MM-dd"));
                                                        break;
                                                    }
                                                case "@TaskType@":
                                                    {
                                                        taskContent.Replace(match.Value, tasktype);
                                                        break;
                                                    }
                                                case "@TaskName@":
                                                    {
                                                        taskContent.Replace(match.Value, "<a href=" + marcomManager.CommonManager.GetEntityPathforMail(xmlElement.Value.ToString(), (int)tasks.ItemArray[0], 30, userDetails.Id, userDetails.IsSSOUser, (int)tasks.ItemArray[1]) + ">" + tasks.ItemArray[3].ToString() + "</a>");
                                                        taskContent.Replace(match.Value, tasks.ItemArray[3].ToString());
                                                        break;
                                                    }
                                                case "@Path@":
                                                    {
                                                        //Need to give link for path and above task
                                                        taskContent.Replace(match.Value, "<a href=" + marcomManager.CommonManager.GetEntityPathforMail(xmlElement.Value.ToString(), parentEntityDetails.Id, parentEntityDetails.Typeid, userDetails.Id, userDetails.IsSSOUser, 0) + ">" + parentEntityDetails.Name + "</a>");
                                                        break;
                                                    }

                                                case "@UserNameTO@":
                                                    {
                                                        //Need to give link for path and above task
                                                        //string[] SplitArr = tasks.ItemArray[12].ToString().Split(',');
                                                        string[] SplitNameArr = tasks.ItemArray[13].ToString().Split(',');
                                                        string CurrentUser = userDetails.FirstName + ' ' + userDetails.LastName;
                                                        string splitString = "";
                                                        for (var i = 0; i < SplitNameArr.Count(); i++)
                                                        {

                                                            if (CurrentUser == SplitNameArr[i])
                                                            {
                                                                splitString += "Oneself";
                                                            }
                                                            else
                                                            {
                                                                splitString += SplitNameArr[i];
                                                            }
                                                            splitString += ",";

                                                        }
                                                        taskContent.Replace(match.Value, splitString.Remove(splitString.ToString().Length - 1, 1));


                                                        break;

                                                    }
                                                case "@userImagepath@":
                                                    {
                                                        var UserImagePath = xmlElement.Value.ToString() + GetTenantFilePath(marcomManager.User.TenantID) + "/UserImages/" + tasks.ItemArray[12] + ".jpg";

                                                        if (marcomManager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
                                                        {
                                                            string cloudUrl = (marcomManager.User.AwsStorage.Uploaderurl + "/" + marcomManager.User.AwsStorage.BucketName + "/" + GetTenantFilePath(marcomManager.User.TenantID)).Replace("\\", "/");
                                                            string s3Key = GetTenantFilePath(marcomManager.User.TenantID) + "UserImages/" + tasks.ItemArray[12] + ".jpg";
                                                            UserImagePath = cloudUrl + "UserImages/" + tasks.ItemArray[12] + ".jpg"; ;
                                                            bool result = AWSHelper.isKeyExists(marcomManager.User.AwsStorage.S3, marcomManager.User.AwsStorage.BucketName, s3Key);
                                                            if (!result)
                                                            {
                                                                UserImagePath = cloudUrl + "UserImages/noimage.jpg";
                                                            }
                                                            if (tasks.ItemArray[12].ToString().Contains(","))
                                                                UserImagePath = cloudUrl + "UserImages/Groups.jpg";
                                                        }
                                                        else
                                                        {
                                                            if (System.IO.File.Exists(Path.Combine(Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + TenantPath + "/UserImages/", tasks.ItemArray[12].ToString() + ".jpg"))) == false)
                                                                UserImagePath = xmlElement.Value.ToString() + GetTenantFilePath(marcomManager.User.TenantID) + "/UserImages/noimage.jpg";
                                                            if (tasks.ItemArray[12].ToString().Contains(","))
                                                                UserImagePath = xmlElement.Value.ToString() + GetTenantFilePath(marcomManager.User.TenantID) + "/UserImages/Groups.jpg";
                                                        }
                                                        taskContent.Replace(match.Value, UserImagePath);

                                                        break;
                                                    }

                                            }


                                        }
                                        overdueInnerContentUser.Append(taskContent);
                                        var str = clsDb.InsertUpdateMailData("update TM_Task_Members set OverdueMailSent='" + DateTime.UtcNow.ToString("yyyy.MM.dd") + "' where userid=" + Convert.ToInt32(uid.ItemArray[0]) + " and TaskID=" + Convert.ToInt32(tasks.ItemArray[0]) + " AND RoleID = 1", CommandType.Text);
                                    }
                                }
                                if (assignedtocontent)
                                    mailContent += overdueCountHeaderHimeself.Replace("@##@", overdueInnerContentUser.ToString()).Replace("@TaskCount@", TaskBYCount.ToString().Replace("@toyouby@", "by you to"));
                            }
                        }


                        if (mailContent.Length > 0)
                        {
                            DataSet lastsendmail = new DataSet();
                            lastsendmail = clsDb.MailData("SELECT TOP 1 cm.SentTime AS SentTime  FROM   CM_Mail cm  where cm.mailtype=55 and cm.ToMail='" + userDetails.Email + "'ORDER BY cm.SentTime DESC ", CommandType.Text);
                            var sendtime = (from item in lastsendmail.Tables[0].AsEnumerable() select item.Field<DateTimeOffset>("SentTime")).ToList();
                            int differentday = 0;
                            bool blnoverdue = false;
                            if (sendtime.Count > 0)
                            {
                                TimeSpan difference1 = DateTime.UtcNow.Add(userTimeZone) - Convert.ToDateTime(sendtime[0].ToString());
                                differentday = Convert.ToInt32(difference1.Days.ToString());
                                if (differentday > 0)
                                {
                                    blnoverdue = true;
                                }
                            }
                            else
                            {
                                blnoverdue = true;
                            }
                            if (blnoverdue)
                            {
                                userNotifyfortask.ToMail = userDetails.Email;
                                userNotifyfortask.Body = mailContent;
                                //userNotifyfortask.ActualTime = DateTimeOffset.UtcNow;
                                userNotifyfortask.ActualTime = DateTime.UtcNow.Date.AddHours(OverDuenotifyhour).AddMinutes(OverDuenotifyMinutes).AddHours(-userTimeZone.Hours).AddMinutes(-userTimeZone.Minutes);
                                userNotifyfortask.Status = "Pending";
                                userNotifyfortask.Subject = "Task(s) Overdue Notice";
                                userNotifyfortask.mailtype = 55;
                                userNotifyfortask.isrecapmail = false;
                                userNotifyfortasklist.Add(userNotifyfortask);
                            }

                        }
                    }
                    DataSet dataSetTaskAlert = new DataSet();
                    dataSetTaskAlert = clsDb.MailData("SELECT cmc.ID, cmc.[Subject], cmc.Body, cmc.[description],cmc.ContainerID  FROM CM_MailContent cmc where Id in (56,57)", CommandType.Text);
                    var bodyTaskAlert = (from item in dataSetTaskAlert.Tables[0].AsEnumerable() select item.Field<string>("Body")).ToList();

                    string reminderInnerContent = bodyTaskAlert[0].ToString();
                    string reminderCountHeader = bodyTaskAlert[1].ToString();
                    StringBuilder strlistofTaskAlerts = new StringBuilder();
                    var TaskAlert = taskReminderData.AsEnumerable().ToList().GroupBy(s => s.Field<int>("UserID"));
                    TaskMembersDao taskMembers = new TaskMembersDao();
                    MatchCollection reminderMailMatches = Regex.Matches(reminderInnerContent, @"@(.+?)@");
                    foreach (var userReminderTask in TaskAlert)
                    {
                        MailDao userNotifyforTaskReminder = new MailDao();
                        StringBuilder reminerInnerContentUser = new StringBuilder();
                        var reminderUserDetails = (from users in txInnerloop.PersistenceManager.CommonRepository[marcomManager.User.TenantID].Query<UserDao>() where users.Id == userReminderTask.Key select users).FirstOrDefault();
                        TimeSpan userTimeZoneForReminder = new TimeSpan(Convert.ToInt32(reminderUserDetails.TimeZone.Split(':')[0]), Convert.ToInt32((reminderUserDetails.TimeZone.Split(':'))[1]), 0);
                        TimeSpan UserTaskAlertZoneForNotify;
                        double UserTaskAlertZoneForNotifyhour = 0;
                        double UserTaskAlertZoneForNotifyMinutes = 0;
                        foreach (var tasks in userReminderTask)
                        {
                            var parentEntityDetailsForReminder = (from item in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityDao>() where item.Id == Convert.ToInt32(tasks.ItemArray[1]) select item).FirstOrDefault();
                            var tasktypeforremindertasks = (from type in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where type.Id == Convert.ToInt32(tasks.ItemArray[2]) select type.Caption).FirstOrDefault();
                            StringBuilder reminderTaskContent = new StringBuilder(reminderInnerContent);
                            UserTaskAlertZoneForNotify = new TimeSpan(Convert.ToInt32(tasks.ItemArray[12].ToString().Split(':')[0]), Convert.ToInt32(tasks.ItemArray[12].ToString().Split(':')[1]), 0);
                            UserTaskAlertZoneForNotifyhour = UserTaskAlertZoneForNotify.Hours;
                            UserTaskAlertZoneForNotifyMinutes = UserTaskAlertZoneForNotify.Minutes;
                            foreach (Match match in reminderMailMatches)
                            {
                                switch (match.Value.Trim())
                                {
                                    case "@DueIn@":
                                        {
                                            TimeSpan differenceforremindertasks = Convert.ToDateTime(tasks.ItemArray[8].ToString()).Add(userTimeZoneForReminder) - DateTime.UtcNow.Add(userTimeZoneForReminder);
                                            if (differenceforremindertasks.Days.ToString() == "0")
                                                reminderTaskContent.Replace(match.Value, "1");
                                            else
                                                reminderTaskContent.Replace(match.Value, differenceforremindertasks.Days.ToString());
                                            break;
                                        }
                                    case "@TaskDate@":
                                        {
                                            reminderTaskContent.Replace(match.Value, Convert.ToDateTime(tasks.ItemArray[8].ToString()).ToString("yyyy-MM-dd"));
                                            break;
                                        }
                                    case "@TaskType@":
                                        {
                                            reminderTaskContent.Replace(match.Value, tasktypeforremindertasks);
                                            break;
                                        }
                                    case "@TaskName@":
                                        {
                                            reminderTaskContent.Replace(match.Value, "<a href=" + marcomManager.CommonManager.GetEntityPathforMail(xmlElement.Value.ToString(), (int)tasks.ItemArray[0], 30, reminderUserDetails.Id, reminderUserDetails.IsSSOUser, (int)tasks.ItemArray[1]) + ">" + tasks.ItemArray[3].ToString() + "</a>");
                                            reminderTaskContent.Replace(match.Value, tasks.ItemArray[3].ToString());
                                            break;
                                        }
                                    case "@Path@":
                                        {
                                            //Need to give link for path and above task
                                            reminderTaskContent.Replace(match.Value, "<a href=" + marcomManager.CommonManager.GetEntityPathforMail(xmlElement.Value.ToString(), parentEntityDetailsForReminder.Id, parentEntityDetailsForReminder.Typeid, parentEntityDetailsForReminder.Id, reminderUserDetails.IsSSOUser, 0) + ">" + parentEntityDetailsForReminder.Name + "</a>");
                                            break;
                                        }


                                }


                            }
                            reminerInnerContentUser.Append(reminderTaskContent);
                            var str = clsDb.MailData("update TM_Task_Members set IsTaskalertSent=1 where userid=" + userReminderTask.Key + " and TaskID=" + Convert.ToInt32(tasks.ItemArray[0]) + "", CommandType.Text);

                        }

                        string reminderMailContent = reminderCountHeader.Replace("@##@", reminerInnerContentUser.ToString()).Replace("@TaskCount@", userReminderTask.Count().ToString());

                        userNotifyforTaskReminder.ToMail = reminderUserDetails.Email;
                        userNotifyforTaskReminder.Body = reminderMailContent;
                        userNotifyforTaskReminder.ActualTime = DateTime.UtcNow.Date.AddHours(UserTaskAlertZoneForNotifyhour).AddMinutes(UserTaskAlertZoneForNotifyMinutes).AddHours(-userTimeZoneForReminder.Hours).AddMinutes(-userTimeZoneForReminder.Minutes);
                        //// userNotifyforTaskReminder.ActualTime = DateTimeOffset.UtcNow;
                        userNotifyforTaskReminder.Status = "Pending";
                        userNotifyforTaskReminder.Subject = "Task Alert";
                        userNotifyforTaskReminder.mailtype = 57;
                        userNotifyforTaskReminder.isrecapmail = false;
                        userNotifyfortasklist.Add(userNotifyforTaskReminder);
                    }
                    txInnerloop.PersistenceManager.CommonRepository[marcomManager.User.TenantID].Save<MailDao>(userNotifyfortasklist);
                    txInnerloop.Commit();

                }
                catch (Exception ex)
                {
                    txInnerloop.Rollback();
                }
                finally
                {

                }
            }
        }

        public void UpdateNotification(int TenantID)
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, MarcomManagerFactory.GetSystemSession(TenantID));
            using (ITransaction txInnerloop = marcomManager.GetTransaction(marcomManager.User.TenantID))
            {
                ClsDb clsDb = new ClsDb(marcomManager.User.TenantHost);
                DataSet dataSet = new DataSet();
                DataSet ds = new DataSet();
                StringBuilder strquery = new StringBuilder();
                StringBuilder ChktoUpdateEntityStatus = new StringBuilder();
                StringBuilder ChktoUpdtEntityStatusPeriod = new StringBuilder();
                StringBuilder strqueryEntityStatus = new StringBuilder();
                StringBuilder CheckToUpdate = new StringBuilder();
                try
                {
                    //------------------- For Entity status--------------------//

                    CheckToUpdate.Append("  Select B.ID from MM_EntityTypeStatus  A");
                    CheckToUpdate.Append("  Inner Join PM_Entity B On A.EntityTypeId= B.TypeID and B.Active=1");
                    CheckToUpdate.Append("  Inner Join  MM_attribute C on A.AttrID= C.ID and C.AttributeTypeID=10 ");
                    CheckToUpdate.Append("  where A.OverallStatus not in (select StatusID from MM_EntityStatus where EntityID=b.ID) ");
                    var ChckUpdateRes = txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].ExecuteQuery(CheckToUpdate.ToString());
                    if (ChckUpdateRes.Count > 0)
                    {

                        ChktoUpdtEntityStatusPeriod.AppendLine("  Select B.EntityID ");
                        ChktoUpdtEntityStatusPeriod.AppendLine(" from PM_Entity A Inner Join PM_EntityPeriod B on  A.ID=B.EntityID ");
                        ChktoUpdtEntityStatusPeriod.AppendLine(" Inner Join MM_EntityType C on C.ID= A.TypeID  ");
                        ChktoUpdtEntityStatusPeriod.AppendLine("  Inner Join  MM_EntityTypeStatus_Options D on D.EntityTypeID=C.ID   ");
                        ChktoUpdtEntityStatusPeriod.AppendLine(" Inner Join  MM_EntityTypeStatus E ON E.OverallStatus=D.ID ");
                        ChktoUpdtEntityStatusPeriod.AppendLine(" Inner Join MM_attribute F On E.AttrID= F.ID where OverallStatus in (select ID from MM_EntityStatus)  and DATEADD(day, E.Days, EndDate) = convert(varchar(10), getdate(),120)  and  F.AttributeTypeID=10 and  A.Active=1 ");
                        var Ress = txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].ExecuteQuery(ChktoUpdtEntityStatusPeriod.ToString());
                        if (Ress.Count > 0)
                        {
                            using (ITransaction tx = marcomManager.GetTransaction(marcomManager.User.TenantID))
                            {
                                try
                                {
                                    var GetStatusDtls = tx.PersistenceManager.TaskRepository[TenantID].ExecuteQuery("Select A.EntityTypeId,AttrID,OverallStatus, AttributeTypeID,C.ID,D.StatusOptions from MM_EntityTypeStatus A inner Join MM_Attribute B on A.AttrID=B.ID inner Join PM_entity C on A.EntityTypeId=C.TypeID Inner Join MM_EntityTypeStatus_Options D on A.OverallStatus=D.ID  Inner Join PM_EntityPeriod E On C.ID=E.EntityID where DATEADD(day, A.Days, EndDate) = convert(varchar(10), getdate(),120) and  B.AttributeTypeID=10 and C.Active=1").Cast<Hashtable>().ToList();
                                    foreach (var oldstatuss in GetStatusDtls)
                                    {
                                        NotificationFeedObjects obj = new NotificationFeedObjects();
                                        FeedNotificationServer fs = new FeedNotificationServer(TenantID);
                                        string oldstatus = "";

                                        var StatusDtls = txInnerloop.PersistenceManager.TaskRepository[TenantID].ExecuteQuery("Select * from MM_EntityStatus where EntityID =" + Convert.ToInt32(oldstatuss["ID"])).Cast<Hashtable>().ToList();
                                        foreach (var items in StatusDtls)
                                        {

                                            int EntityID = Convert.ToInt32(items["EntityID"]);
                                            obj.EntityId = Convert.ToInt32(EntityID);
                                            oldstatus = (from uu in tx.PersistenceManager.TaskRepository[TenantID].Query<EntityTypeStatusOptionsDao>() where uu.ID == Convert.ToInt32(items["StatusID"]) select uu.StatusOptions).FirstOrDefault();
                                            obj.FromValue = oldstatus.ToString();
                                            obj.action = "entity status update";
                                            obj.Actorid = -1;
                                            var newstatus = tx.PersistenceManager.TaskRepository[TenantID].ExecuteQuery(" Select D.StatusOptions from MM_EntityTypeStatus A inner Join MM_Attribute B on A.AttrID=B.ID inner Join PM_entity C on A.EntityTypeId=C.TypeID Inner Join MM_EntityTypeStatus_Options D on A.OverallStatus=D.ID  where  B.AttributeTypeID=10 and C.ID in(select EntityID from MM_EntityStatus) and A.OverallStatus = " + Convert.ToInt32(oldstatuss["OverallStatus"]) + " and C.ID=" + EntityID).Cast<Hashtable>().ToList();
                                            //foreach (var itms in newstatus)
                                            //{
                                            //    obj.ToValue = itms["StatusOptions"].ToString();
                                            //}
                                            obj.ToValue = (from ststuss in newstatus select ststuss["StatusOptions"]).FirstOrDefault().ToString();
                                            obj.AttributeName = "System update the entity status";
                                            if (obj.FromValue == obj.ToValue)
                                                break;
                                            else
                                                fs.AsynchronousNotify(obj);
                                        }
                                    }

                                    strqueryEntityStatus.AppendLine(" CREATE TABLE #T1(EntityId int,EndDate date,AttrID int,StatusOptions varchar(50),EntityTypeID int,StatusID int) ");
                                    strqueryEntityStatus.AppendLine(" INSERT INTO #T1(EntityId,EndDate,AttrID,StatusOptions,EntityTypeID,StatusID) ");
                                    strqueryEntityStatus.AppendLine(" (select B.EntityID,max(b.EndDate) as EndDate,E.AttrID,D.StatusOptions,c.ID as 'EntityTypeID',d.ID ");
                                    strqueryEntityStatus.AppendLine(" from PM_Entity A Inner Join PM_EntityPeriod B on  A.ID=B.EntityID ");
                                    strqueryEntityStatus.AppendLine(" Inner Join MM_EntityType C on C.ID= A.TypeID ");
                                    strqueryEntityStatus.AppendLine(" Inner Join MM_EntityTypeStatus_Options D on D.EntityTypeID=C.ID  ");
                                    strqueryEntityStatus.AppendLine(" Inner Join MM_EntityTypeStatus E on E.OverallStatus=D.ID where  DATEADD(day, E.Days, EndDate) = convert(varchar(10), getdate(),120)  and AttrID not in(56,63) ");
                                    strqueryEntityStatus.AppendLine("group by B.EntityID,b.EndDate,E.AttrID,D.StatusOptions,c.ID,d.ID) ");
                                    strqueryEntityStatus.AppendLine(" Update B set B.StatusID=A.StatusID");
                                    strqueryEntityStatus.AppendLine(" from  #T1 A");
                                    strqueryEntityStatus.AppendLine(" Inner join MM_EntityStatus B  On A.EntityId=B.EntityID");
                                    strqueryEntityStatus.AppendLine("  DROP table #T1");

                                    var Res = txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].ExecuteQuery(strqueryEntityStatus.ToString());
                                }
                                catch { }
                                tx.Commit();
                            }
                        }
                    }

                }
                catch { }
                txInnerloop.Commit();
            }

            using (ITransaction tx = marcomManager.GetTransaction(marcomManager.User.TenantID))
            {
                try
                {
                    var GetStatusDtls = tx.PersistenceManager.TaskRepository[TenantID].ExecuteQuery("Select A.EntityTypeId,AttrID,OverallStatus, AttributeTypeID,C.ID,D.StatusOptions from MM_EntityTypeStatus A inner Join MM_Attribute B on A.AttrID=B.ID inner Join PM_entity C on A.EntityTypeId=C.TypeID Inner Join MM_EntityTypeStatus_Options D on A.OverallStatus=D.ID  where  B.AttributeTypeID=5").Cast<Hashtable>().ToList();
                    foreach (var oldstatuss in GetStatusDtls)
                    {
                        NotificationFeedObjects obj = new NotificationFeedObjects();
                        FeedNotificationServer fs = new FeedNotificationServer(TenantID);
                        string oldstatus = "";
                        var StatusDtls = tx.PersistenceManager.TaskRepository[TenantID].ExecuteQuery("Select * from MM_EntityStatus where EntityID =" + Convert.ToInt32(oldstatuss["ID"])).Cast<Hashtable>().ToList();
                        foreach (var items in StatusDtls)
                        {
                            int EntityID = Convert.ToInt32(items["EntityID"]);
                            obj.EntityId = Convert.ToInt32(EntityID);
                            oldstatus = (from uu in tx.PersistenceManager.TaskRepository[TenantID].Query<EntityTypeStatusOptionsDao>() where uu.ID == Convert.ToInt32(items["StatusID"]) select uu.StatusOptions).FirstOrDefault();
                            obj.FromValue = oldstatus.ToString();
                            obj.action = "entity status update";
                            obj.Actorid = -1;
                            var newstatus = tx.PersistenceManager.TaskRepository[TenantID].ExecuteQuery(" Select D.StatusOptions from MM_EntityTypeStatus A inner Join MM_Attribute B on A.AttrID=B.ID inner Join PM_entity C on A.EntityTypeId=C.TypeID Inner Join MM_EntityTypeStatus_Options D on A.OverallStatus=D.ID  where  B.AttributeTypeID=5 and C.ID in(select EntityID from MM_EntityStatus) and A.OverallStatus = " + Convert.ToInt32(oldstatuss["OverallStatus"]) + " and C.ID=" + EntityID).Cast<Hashtable>().ToList();
                            //foreach (var itms in newstatus)
                            //{
                            //    obj.ToValue = itms["StatusOptions"].ToString();
                            //}
                            obj.ToValue = (from ststuss in newstatus select ststuss["StatusOptions"]).FirstOrDefault().ToString();
                            obj.AttributeName = "System update the entity status";
                            if (obj.FromValue == obj.ToValue)
                                break;
                            else
                                fs.AsynchronousNotify(obj);
                        }
                    }
                }
                catch { }
                tx.Commit();
            }

            using (ITransaction txs = marcomManager.GetTransaction(marcomManager.User.TenantID))
            {
                try
                {
                    StringBuilder strqueryEntityStatusAttr = new StringBuilder();

                    strqueryEntityStatusAttr.AppendLine("  CREATE TABLE #T2([ID][int] IDENTITY(1, 1) NOT NULL,EntityTypeid Int,AttrID Int,OverallStatus Int,AttributeTypeID Int,EntityID int,StatusOption varchar(50)) 	");
                    strqueryEntityStatusAttr.AppendLine(" 	INSERT Into #T2(EntityTypeid,AttrID,OverallStatus,AttributeTypeID,EntityID,StatusOption)	");
                    strqueryEntityStatusAttr.AppendLine("  (Select A.EntityTypeId,AttrID,OverallStatus, AttributeTypeID,C.ID,D.StatusOptions from MM_EntityTypeStatus A inner Join MM_Attribute B on A.AttrID=B.ID 	");
                    strqueryEntityStatusAttr.AppendLine("  inner Join PM_entity C on A.EntityTypeId=C.TypeID	");
                    strqueryEntityStatusAttr.AppendLine("  Inner Join MM_EntityTypeStatus_Options D on A.OverallStatus=D.ID  where  B.AttributeTypeID=5)		");
                    strqueryEntityStatusAttr.AppendLine("  DECLARE @sqlText as nvarchar(max)	");
                    strqueryEntityStatusAttr.AppendLine("  DECLARE @sqlRes as nvarchar(max)	");
                    strqueryEntityStatusAttr.AppendLine("  DECLARE @CurrentEntityType as int	");
                    strqueryEntityStatusAttr.AppendLine("  Declare @attrID as Int	");
                    strqueryEntityStatusAttr.AppendLine("  Declare @GetRows as Int	");
                    strqueryEntityStatusAttr.AppendLine("  Declare @Flag as int	");
                    strqueryEntityStatusAttr.AppendLine("  SET @Flag =1	");
                    strqueryEntityStatusAttr.AppendLine(" SELECT @GetRows = (select COUNT(1) from #T2)	");
                    strqueryEntityStatusAttr.AppendLine("  WHILE (@Flag <= @GetRows) 	");
                    strqueryEntityStatusAttr.AppendLine(" BEGIN 		");
                    strqueryEntityStatusAttr.AppendLine(" SET @CurrentEntityType=(select EntityTypeid from #T2 WHERE ID=@Flag) 	");
                    strqueryEntityStatusAttr.AppendLine(" SET @attrID=(select AttrID from #T2 WHERE ID=@Flag)	");
                    strqueryEntityStatusAttr.AppendLine(" SET @sqlText = 'select  Attr_'+ cast(@attrID as nvarchar(100))+ ', A.ID,StatusOptions,D.EntityTypeID,B.Overallstatus from mm_attributeRecord_' + cast(@CurrentEntityType as nvarchar(100)) +	");
                    strqueryEntityStatusAttr.AppendLine(" ' A inner Join #T2 B ON A.ID =B.entityid Inner Join MM_EntityTypeStatus_Options D On B.EntityTypeID = D.EntityTypeID WHERE Attr_'+ cast(@attrID as nvarchar(100)) +' <= getdate() and Attr_'+ cast(@attrID as nvarchar(100))+ ' is not null'	");
                    strqueryEntityStatusAttr.AppendLine("   Update B set b.StatusID=A.OverallStatus	");
                    strqueryEntityStatusAttr.AppendLine("  from  #T2 A	");
                    strqueryEntityStatusAttr.AppendLine(" Inner join MM_EntityStatus B on A.EntityID=B.EntityID ");
                    strqueryEntityStatusAttr.AppendLine(" EXEC (@sqlText)		");
                    strqueryEntityStatusAttr.AppendLine(" 	    SET @Flag = @Flag + 1 	");
                    strqueryEntityStatusAttr.AppendLine(" END    	");
                    strqueryEntityStatusAttr.AppendLine(" DROP TABLE #T2	");

                    var Results = txs.PersistenceManager.TaskRepository[marcomManager.User.TenantID].ExecuteQuery(strqueryEntityStatusAttr.ToString()).Cast<Hashtable>().ToList();
                }
                catch { }
                txs.Commit();
            }
        }

        public string GetTenantFilePath(int TenantID)
        {
            //Get the tenant related file path
            BrandSystems.Marcom.Core.Utility.TenantSelection tfp = new BrandSystems.Marcom.Core.Utility.TenantSelection();
            return tfp.GetTenantFilePath(TenantID);
        }
    }


}
