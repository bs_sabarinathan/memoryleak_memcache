﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using BrandSystems.Marcom.Core.Report.Interface;
using System.Security.Cryptography;
using BrandSystems.Marcom.Core.Access;
namespace BrandSystems.Marcom.Core.Report
{
    /// <summary>
    /// User object for table 'RM_ReportCredential'.
    /// </summary>
    public class FinancialReportSettings : IFinancialReportSettings, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _reportname;
        protected string _reportimage;
        protected string _reportdescription;

        #endregion

        #region Constructors
        public FinancialReportSettings() { }

        public FinancialReportSettings(string pReportname, string pReportImage, string pReportdescription)
        {
            this._reportname = pReportname;
            this._reportimage = pReportImage;
            this._reportdescription = pReportdescription;
        }

        public FinancialReportSettings(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public string ReportName
        {
            get { return _reportname; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("ReportName", "ReportName value, cannot contain more than 250 characters");
                _reportname = value;
            }

        }

        public string ReportImage
        {
            get { return _reportimage; }
            set
            {
                _reportimage = value;
            }

        }

        public string ReportDescription
        {
            get { return _reportdescription; }
            set
            {
                _reportdescription = value;
            }

        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            FinancialReportSettings castObj = null;
            try
            {
                castObj = (FinancialReportSettings)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }


        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

    }

}
