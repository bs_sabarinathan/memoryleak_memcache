﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{
    /// <summary>
    /// FilterSettingsDao object for table 'PM_Filter'.
	/// </summary>

    public partial class FilterSettingsDao : ICloneable
    {
        #region Member Variables

        protected int _filterid;
        protected string _filtername;
        protected string _keyword;
        protected int _userid;
        protected int _typeid;
        protected string _entitytypeid;
        protected string _startdate;
        protected string _enddate;
        protected string _wherecondition;
        protected int _isdetailfilter;
        protected string _entitymemberid;
        #endregion

        #region Constructors
		  public FilterSettingsDao() { }

          public FilterSettingsDao(int pFilterId, string pFiletName, string PKeyword, int pUserId, int pTypeId, string pEntityTypeId, string pStartDate, string pEndDate, string pWhereCondition, int pIsDetailFilter, string pEntitymemberId)
        {
            this._filterid = pFilterId;
            this._filtername = pFiletName;
            this._keyword = PKeyword;
            this._userid = pUserId;
            this._typeid = pTypeId;
            this._entitytypeid = pEntityTypeId;
            this._startdate = pStartDate;
            this._enddate = pEndDate;
            this._wherecondition = pWhereCondition;
            this._isdetailfilter = pIsDetailFilter;
            this._entitymemberid = pEntitymemberId;
        }
		
		#endregion

          #region Public Properties


          public virtual int FilterID
          {
              get { return _filterid; }
              set { _filterid = value; }

          }

          public virtual string FilterName
          {
              get { return _filtername; }
              set { _filtername = value; }

          }

          public virtual string Keyword
          {
              get { return _keyword; }
              set { _keyword = value; }

          }

          public virtual int UserID
          {
              get { return _userid; }
              set { _userid = value; }

          }

          public virtual int TypeID
          {
              get { return _typeid; }
              set { _typeid = value; }
          }

          public virtual string EntityTypeID
          {
              get { return _entitytypeid; }
              set { _entitytypeid = value; }
          }
          public virtual string StartDate
          {
              get { return _startdate; }
              set { _startdate = value; }
          }

          public virtual string EndDate
          {
              get { return _enddate; }
              set { _enddate = value; }
          }
          public virtual string WhereCondition
          {
              get { return _wherecondition; }
              set { _wherecondition = value; }
          }
          public virtual int IsDetailFilter
          {
              get { return _isdetailfilter; }
              set { _isdetailfilter = value; }
          }

          public virtual string EntityMemberID
          {
              get { return _entitymemberid; }
              set { _entitymemberid = value; }
          }
          #endregion

          #region ICloneable methods

          public virtual object Clone()
          {
              return this.MemberwiseClone();
          }

          #endregion

          #region Public Property and Mapping Constants

          //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
          public static class PropertyNames
          {
              public const string FilterID = "FilterID";
              public const string FilterSettingsDao = "FilterSettingsDao";
              public const string FilterName = "FilterName";
              public const string Keyword = "Keyword";
              public const string UserID = "UserID";
              public const string TypeID = "TypeID";
              public const string EntityTypeID = "EntityTypeID";
              public const string StartDate = "StartDate";
              public const string EndDate = "EndDate";
              public const string WhereCondition = "WhereCondition";
              public const string IsDetailFilter = "IsDetailFilter";
              public const string EntityMemberID = "EntityMemberID";
          }

          //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
          public static class MappingNames
          {
              public const string FilterID = "FilterID";
              public const string FilterSettingsDao = "FilterSettingsDao";
              public const string FilterName = "FilterName";
              public const string Keyword = "Keyword";
              public const string UserID = "UserID";
              public const string TypeID = "TypeID";
              public const string EntityTypeID = "EntityTypeID";
              public const string StartDate = "StartDate";
              public const string EndDate = "EndDate";
              public const string WhereCondition = "WhereCondition";
              public const string IsDetailFilter = "IsDetailFilter";
              public const string EntityMemberID = "EntityMemberID";
          }

          #endregion
    }
}
