﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Common.Model
{

    /// <summary>
    ///  Purchase Order detail for xml file.
    /// </summary>
    public partial class PurchaseOrderSettingsDao : BaseDao, ICloneable
    {
        #region Member Variables
        protected DateTime _CreateDate;
        protected int _ID;
        protected string _Prefix;
        protected string _DateFormat;
        protected string _DigitFormat;
        protected string _NumberCount;
        protected bool _bIsChanged;

        #endregion

        #region Constructors

        public PurchaseOrderSettingsDao() { }



        public PurchaseOrderSettingsDao(string Prefix, string DateFormat, string DigitFormat,string NumberCount,DateTime CreateDate)
        {
            // TODO: Complete member initialization
            this._Prefix = Prefix;
            this._DateFormat = DateFormat;
            this._DigitFormat = DigitFormat;
            this._NumberCount = NumberCount;
          
            this._CreateDate = CreateDate;
            
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _ID; }
            set { _bIsChanged |= (_ID != value); _ID = value; }

        }

        public virtual string Prefix
        {
            get { return _Prefix; }
            set { _bIsChanged |= (_Prefix != value); _Prefix = value; }

        }

      
        public virtual string DateFormat
        {
            get { return _DateFormat; }
            set
            {
              
                _bIsChanged |= (_DateFormat != value);
                _DateFormat = value;
            }

        }

        public virtual string DigitFormat
        {
            get { return _DigitFormat; }
            set
            {
                _bIsChanged |= (_DigitFormat != value);
                _DigitFormat = value;
            }

        }

        public virtual string NumberCount
        {
            get { return _NumberCount; }
            set { _bIsChanged |= (_NumberCount != value); _NumberCount = value; }

        }

        public virtual DateTime CreateDate
        {
            get { return _CreateDate; }
            set { _bIsChanged |= (_CreateDate != value); _CreateDate = value; }

        }

       
        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion


    }
}
