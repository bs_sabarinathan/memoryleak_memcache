﻿(function (ng, app) {
    "use strict";

    function CcfinancialService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            GetEntityFinancialdForecastHeadings: GetEntityFinancialdForecastHeadings,
            GettingCostcentreFinancialOverview: GettingCostcentreFinancialOverview,
            GetFinancialForecastsettings: GetFinancialForecastsettings,
            GetForeCastForCCDetl: GetForeCastForCCDetl,
            GetCostCentreCurrencyRateById: GetCostCentreCurrencyRateById,
            UpdateTotalAssignedAmount: UpdateTotalAssignedAmount,
            UpdateTotalAssigAmtInFinancialTransaction: UpdateTotalAssigAmtInFinancialTransaction,
            GetDynamicfinancialattributes: GetDynamicfinancialattributes,
          
        });

        function GetEntityFinancialdForecastHeadings(EntityID, DivisionID, Iscc) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetEntityFinancialdForecastHeadings/" + EntityID + "/" + DivisionID + "/" + Iscc,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingCostcentreFinancialOverview(EntityId) {
            var request = $http({
                method: "get",
                url: "api/Planning/GettingCostcentreFinancialOverview/" + EntityId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFinancialForecastsettings() {
            var request = $http({
                method: "get",
                url: "api/Planning/GetFinancialForecastsettings/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetForeCastForCCDetl(CostcenterId) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetForeCastForCCDetl/" + CostcenterId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetCostCentreCurrencyRateById(EntityId, CurrencyId, IsCostCentreCreation) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetCostCentreCurrencyRateById/" + EntityId + "/" + CurrencyId + "/" + IsCostCentreCreation,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateTotalAssignedAmount(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/UpdateTotalAssignedAmount/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateTotalAssigAmtInFinancialTransaction(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/UpdateTotalAssigAmtInFinancialTransaction/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }
        function GetDynamicfinancialattributes() {
            var request = $http({
                method: "get",
                url: "api/Planning/GetDynamicfinancialattributes",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.service("CcfinancialService", ['$http', '$q', CcfinancialService]);
})(angular, app);