﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

    /// <summary>
    /// Option object for table 'MM_EntityTaskType'.
    /// </summary>

    internal class Entitytasktype : IEntityTaskType
    {
        #region Member Variables

        protected int _id;
        protected int _entitytypeid;
        protected int _tasktypeid;


        #endregion

        #region Constructors
        public Entitytasktype() { }

        public Entitytasktype(int pId, int pEntitytypeid, int pTasktypeid)
        {
            this._id = pId;
            this._entitytypeid = pEntitytypeid;
            this._tasktypeid = pTasktypeid;
        }

        public Entitytasktype(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }



        public virtual int EntitytypeId
        {
            get { return _entitytypeid; }
            set { _entitytypeid = value; }

        }
        public virtual int TaskTypeId
        {
            get { return _tasktypeid; }
            set { _tasktypeid = value; }

        }




        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            Entitytasktype castObj = null;
            try
            {
                castObj = (Entitytasktype)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
