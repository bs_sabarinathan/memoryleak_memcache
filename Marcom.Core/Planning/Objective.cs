using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

	/// <summary>
	/// Objective object for table 'PM_Objective'.
	/// </summary>
	
	internal class Objective : BaseEntity, IObjective 
	{
		#region Member Variables

		protected int _id;
		protected int _typeid;
		protected string _name;
        protected string _description;
		protected string _instruction;
		protected bool _isenablefeedback;
		protected DateTime _startdate;
		protected DateTime _enddate;
		protected int? _daterule;
		protected bool? _ismandatory;
		protected IObjectiveNumeric _numeric;
		protected IList<IObjectiveRating> _ratings;
		protected IList<IObjectiveCondition> _conditions;
		protected IList<IObjectiveEntityValue> _value;
        protected IList<IAttributeData> _objectiveattributes;
        protected bool _objectivestatus;
		
		#endregion
		
		#region Constructors
		public Objective() {}

        public Objective(int pId, int pTypeid, string pName, string pDescription, string pInstruction, bool pIsEnableFeedback, DateTime pStartDate, DateTime pEndDate, int? pDateRule, bool? pIsMandatory, bool pObjectiveStatus)
		{
			this._id = pId; 
			this._typeid = pTypeid; 
			this._name = pName;
            this._description = pDescription;
			this._instruction = pInstruction; 
			this._isenablefeedback = pIsEnableFeedback; 
			this._startdate = pStartDate; 
			this._enddate = pEndDate; 
			this._daterule = pDateRule; 
			this._ismandatory = pIsMandatory;
            this._objectivestatus = pObjectiveStatus; 
		}
		
		#endregion
		
		#region Public Properties

		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public int Typeid
		{
			get { return _typeid; }
			set { _typeid = value; }
			
		}
		
		public string Name
		{
			get { return _name; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 50 characters");
			  _name = value; 
			}
			
		}

        public string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _description = value;
            }

        }
		public string Instruction
		{
			get { return _instruction; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Instruction", "Instruction value, cannot contain more than 1073741823 characters");
			  _instruction = value; 
			}
			
		}
		
		public bool IsEnableFeedback
		{
			get { return _isenablefeedback; }
			set { _isenablefeedback = value; }
			
		}
		
		public DateTime StartDate
		{
			get { return _startdate; }
			set { _startdate = value; }
			
		}
		
		public DateTime EndDate
		{
			get { return _enddate; }
			set { _enddate = value; }
			
		}
		
		public int? DateRule
		{
			get { return _daterule; }
			set { _daterule = value; }
			
		}
		
		public bool? IsMandatory
		{
			get { return _ismandatory; }
			set { _ismandatory = value; }
			
		}
		
		public IObjectiveNumeric Numeric
		{
			get { return _numeric; }
			set { _numeric = value; }
			
		}
		
		public IList<IObjectiveRating> Ratings
		{
			get { return _ratings; }
			set { _ratings = value; }
			
		}
		
		public IList<IObjectiveCondition> Conditions
		{
			get { return _conditions; }
			set { _conditions = value; }
			
		}
		
		public IList<IObjectiveEntityValue> Value
		{
			get { return _value; }
			set { _value = value; }
			
		}
        public IList<IAttributeData> ObjectiveAttributes
        {
            get { return _objectiveattributes; }
            set { _objectiveattributes = value; }
        }

        public bool ObjectiveStatus
        {
            get { return _objectivestatus; }
            set { _objectivestatus = value; }

        }

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			Objective castObj = null;
			try
			{
				castObj = (Objective)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				this._id.Equals( castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
