﻿(function (ng, app) {
    "use strict";

    function muitaskTaskCreationCtrl($window, $location, $timeout, $scope, $cookies, $resource, $compile, $stateParams, $translate, TaskCreationService, $modalInstance, params, $modal) {
        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imagesrcpath = TenantFilePath;
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            imagesrcpath = cloudpath;
        }       
        $scope.Dropdown = {};
        $scope.DropDownTreeOptionValues = {};
        $scope.TaskTypeObject = { TaskType: 0, NewTaskName: '', AssetTaskList: '', TaskDescription: '', TaskNote: '', TaskDueDate: '' };

        $scope.IsTaskDueDate = false;
        $scope.Timestamp = new Date.create().getTime().toString();
        var oldpopup = null;
        $scope.Calanderopen = function ($event, Call_from) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope[oldpopup] = false;
            $scope[Call_from] = true;
            oldpopup = Call_from;
        }
        $scope.PeriodCalanderopen = function ($event, item, place) {
            $event.preventDefault();
            $event.stopPropagation();
            if (place == "start") {
                item.calstartopen = true;
                item.calendopen = false;
            } else if (place == "end") {
                item.calstartopen = false;
                item.calendopen = true;
            }
        };
        var timerObj = {};
        $scope.AssetTaskTypeObj = {
            "AssetTaskType": 0
        };
        $scope.GetNonBusinessDaysforDatePicker();
        var treeTextVisbileflag = false;
        var TaskListRefresh;
        $scope.IsAttachmentPresent = false;
        $scope.ngShowUnassigned = false;
        $scope.ngShowTaskCreation = true;
        $scope.TaskCreationFromPlace = "";
        $scope.OwnerList = [];
        $scope.OwnerList.push({
            "Roleid": 1,
            "RoleName": "Owner",
            "UserEmail": $scope.ownerEmail,
            "DepartmentName": "-",
            "Title": "-",
            "Userid": parseInt($scope.OwnerID, 10),
            "UserName": $scope.OwnerName,
            "IsInherited": '0',
            "InheritedFromEntityid": '0'
        });
        $scope.AutoCompleteSelectedUserObj = {
            UserSelection: {},
            userrole: ''
        };
        $scope.TaskBriefDetails = {
            taskID: 0,
            taskTypeId: 0,
            taskListUniqueID: 0,
            taskTypeName: "",
            EntityID: 0,
            ownerId: 0,
            taskName: "No Name",
            dueIn: 0,
            dueDate: "",
            strDueDate: "",
            status: "",
            statusID: 0,
            taskOwner: "",
            Description: "",
            Note: "",
            taskmembersList: [],
            taskAttachmentsList: [],
            taskProgressCount: "",
            totalTaskMembers: [],
            TaskMembersRoundTripGroup: [],
            taskCheckList: [],
            WorkTaskInprogressStatus: "",
            LatestMaxRound: 0,
            Totalassetcount: 0,
            Totalassetsize: ""
        };
        var TempStepID = 0;
        $scope.globalEntityMemberLists = [];
        $scope.NewTime = $scope.DefaultImageSettings.ImageSpan;
        $scope.rytSideApprovalButtonPane = false;
        $scope.addataskfromGenAttachments = false;
        $scope.ngShowtempTaskCreation = false;
        $scope.TaskActionCaption = "Add task";
        $scope.AddTemplateFromDalim = false;
        var AddedStepDetails = [],
			AddedPhaseDetails = [];
        $scope.TaskMemberLists = [];
        $scope.StepTaskMemberLists = [];
        $scope.ApprovalTaskGlobalUsers = {
            EntityUser: [],
            globalUser: []
        };

        function RefreshAllTaskFlowcreationScopes() {
            AddedStepDetails = [], AddedPhaseDetails = [];
            $scope.TaskMemberLists = [];
            $scope.StepTaskMemberLists = [];
            $scope.templateData = {
                "TemplateList": [],
                "TemplateLibraryID": 0,
                "TemplateLibraryName": "",
                PhaseData: {
                    AllPhaseList: [],
                    PhaseList: [{
                        "ID": 0,
                        "Name": "",
                        "Description": "",
                        "Colorcode": ""
                    }],
                    StepsData: {
                        StepList: [],
                        MemberList: {},
                        "StepID": 0,
                        "Name": "",
                        "Description": "",
                        "Stepduration": "+1 days",
                        "StepMinApproval": 1,
                        "Steproles": "0",
                        "IsMandatory": 0,
                        "PhaseId": 0
                    }
                },
                templateSrcData: [],
                UserRoles: [],
                phaseTempData: {},
                StepTempData: {},
                SortOrder: []
            };
            $scope.PhaseDuration = [{
                ID: 1,
                Caption: "+1 days"
            }, {
                ID: 2,
                Caption: "+2 days"
            }, {
                ID: 3,
                Caption: "+3 days"
            }, {
                ID: 4,
                Caption: "+4 days"
            }, {
                ID: 5,
                Caption: "+5 days"
            }, {
                ID: 6,
                Caption: "+6 days"
            }, {
                ID: 7,
                Caption: "+1 week"
            }, {
                ID: 14,
                Caption: "+2 weeks"
            }, {
                ID: 21,
                Caption: "+3 weeks"
            }, {
                ID: 30,
                Caption: "+1 month"
            }];
            $scope.PhaseMinApproval = ["1", "2", "3", "4", "5", "6", "7", "10", "15", "20", "25", "30"];
            $scope.alluselectedusers = [];
            GetAllUserRoles();
        }
        RefreshAllTaskFlowcreationScopes();
        $scope.AssetSelectionFiles = [];
        $scope.selectedAlltemplates = false;
        $scope.thumbnailActionMenu = false;
        $scope.approvalflowTempPopup = false;
        $scope.AddGlobalMemberforTask = false;

        function GenarateUniqueId() {
            var date = new Date();
            var components = [date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()];
            return components.join('');
        }

        function callTaskCreationPage(opentaskpopupdata) {
            Page_Load_Methods();
            RefreshAllTaskFlowcreationScopes();
            ClearAllScopeVariables();
            $scope.rytSideApprovalButtonPane = false;
            $scope.addfromComputer = false;
            $scope.TaskTypeObject.TaskType = 0;
            $scope.TaskMemberList = [];
            $scope.TaskCreationFromPlace = opentaskpopupdata.Type;
            $scope.TaskTypeObject.AssetTaskList = params.data.TaskListID;
            $scope.AddTemplateFromDalim = false;
            $scope.addTaskMemberfromGlobal = 0;
            $scope.addTaskMemberfromStep = false;
            $scope.IsAttachmentPresent = false;
            $scope.freezeTaskList = false;
            $scope.IsaddAssetsfromComputer = false;
            $('#assetassettaskentityattachassetdata').empty();
            $scope.IscontextAssetAttachmentsInTaskPopup = true;
            $scope.addMemberButtonforwithoutstepTask = true;
            $scope.alluselectedusers = [];
            $scope.TaskStepMembers = false;
            $scope.AddGlobalMemberforTask = false;
            $('#CreateNewTaskBtn').removeAttr("disabled");
            getTaskApprovalRolesUsers();
            getEntityApprovalRoleUsers();
            $scope.userid = parseInt($.cookie('UserId'), 10);
            if ($scope.memberList == null || $scope.memberList == undefined) {
                $scope.memberList = [];
            }
            if ($scope.EntityMemberObj != null && $scope.EntityMemberObj != undefined) {
                if ($scope.EntityMemberObj.EntityMemberList != null && $scope.EntityMemberObj.EntityMemberList != undefined) {
                    $scope.memberList = $scope.EntityMemberObj.EntityMemberList;
                }
            }
            if (opentaskpopupdata.Type == "addtask") {
                $scope.AssetTaskList = opentaskpopupdata.TaskListID;
                $scope.EntityLockTask = opentaskpopupdata.EntityLockTask;
                $scope.TaskBriefDetails = opentaskpopupdata.TaskBriefDetails;
                $scope.TaskAssetID = 0;
            }
            if (opentaskpopupdata.Type == "addtaskfromasset") {
                $scope.IscontextAssetAttachmentsInTaskPopup = false;
                $scope.freezeTaskList = true;
                $scope.addataskfromGenAttachments = true;
                $scope.IsAttachmentPresent = true;
                $scope.ShowDamAssetApprovalType = 0;
                $scope.TaskMemberList = [];
                $scope.TaskAssetID = opentaskpopupdata.AssetID;
                $scope.showAttach = opentaskpopupdata.IsTask;
                if (opentaskpopupdata.Fromplace == "Task") {
                    if ($scope.AssetTaskList > 0 || $scope.TaskTypeObject.AssetTaskList > 0) $scope.freezeTaskList = true;
                }
                if (opentaskpopupdata.TaskType == taskType.Asset_Approval_Task) $scope.ShowDamAssetApprovalType = 1;
                else if (opentaskpopupdata.TaskType == taskType.Proof_approval_task) $scope.ShowDamAssetApprovalType = 2;
                else if (opentaskpopupdata.TaskType == taskType.dalim_approval_task) $scope.ShowDamAssetApprovalType = 3;
                else $scope.ShowDamAssetApprovalType = 0;
                LoadThumbnailBlocks();
                ResetVariables();
                $scope.userisProofInitiator = false;
                $scope.EntityID = ($scope.DamAssetTaskselectionFiles.AssetFiles[0] != undefined) ? $scope.DamAssetTaskselectionFiles.AssetFiles[0].EntityID : parseInt($stateParams.ID, 10);
                var loggedinUserDets = $.grep($scope.memberList, function (e) {
                    return e.Userid == $scope.userid
                });
                var loggedinUserRoles = $.grep(loggedinUserDets, function (e) {
                    return e.BackendRoleID == EntityRoles.ProofInitiator
                });
                $scope.TaskTypeList = ($scope.TaskTypeList != null ? $scope.TaskTypeList : []);
                if (loggedinUserRoles.length > 0) {
                    $scope.userisProofInitiator = true;
                    TaskCreationService.GetTaskTypes().then(function (TaskTypeData) {
                        $scope.TaskTypeList = TaskTypeData.Response;
                    });
                } else if ($scope.userisProofInitiator == false && $scope.TaskTypeList.length > 0) {
                    var DelTaskType = [],
						data = [];
                    DelTaskType = $.grep($scope.TaskTypeList, function (e) {
                        return e.TaskTypeId == taskType.Asset_Approval_Task;
                    });
                    if (DelTaskType.length > 0) {
                        $scope.TaskTypeList.splice($.inArray(DelTaskType[0], $scope.TaskTypeList), 1);
                    }
                    DelTaskType = $.grep($scope.TaskTypeList, function (e) {
                        return e.TaskTypeId == taskType.Proof_approval_task;
                    });
                    if (DelTaskType.length > 0) {
                        $scope.TaskTypeList.splice($.inArray(DelTaskType[0], $scope.TaskTypeList), 1);
                    }
                    DelTaskType = $.grep($scope.TaskTypeList, function (e) {
                        return e.TaskTypeId == taskType.dalim_approval_task;
                    });
                    if (DelTaskType.length > 0) {
                        $scope.TaskTypeList.splice($.inArray(DelTaskType[0], $scope.TaskTypeList), 1);
                    }
                }
                LoadTaskList();
                if (opentaskpopupdata.Fromplace == "Assetedit" || opentaskpopupdata.Fromplace == "Dam" || opentaskpopupdata.Fromplace == "marcommediabank") {
                    GetentityMembers($scope.DamAssetTaskselectionFiles.AssetFiles[0].EntityID);
                }
            }
            $scope.Islock_temp = opentaskpopupdata.IsLock;
            GetentityMembers($stateParams.ID);
            $scope.IsLock = opentaskpopupdata.IsLock;
            $scope.processingsrcobj.processingid = 0;
            $scope.SelectedTaskLIstID = opentaskpopupdata.TaskListID;
            $scope.processingsrcobj = {
                processingid: $scope.TaskBriefDetails.taskID,
                processingplace: "task",
                processinglock: $scope.IsLock
            };
        }

        function LoadTaskList() {
            TaskCreationService.GetEntityTaskListWithoutTasks($stateParams.ID).then(function (TaskListLibraryData) {

                //$scope.TaskLibraryList = TaskListLibraryData.Response;
                //if ($scope.TaskLibraryList.length == 1) {
                //    $scope.AssetTaskList = $scope.TaskLibraryList[0].ID;
                //}
                $scope.TaskLibraryList = TaskListLibraryData.Response;
                if ($scope.TaskLibraryList.length == 1) {
                    $scope.TaskTypeObject.AssetTaskList = $scope.TaskLibraryList[0].ID;
                }
            });
        }
        $scope.getphasewithstepforDalim = function (templateid) {
            $scope.myObjtasktemplate.selectedItems = templateid;
            if ($scope.TaskMemberLists.length > 0 && $scope.myObjtasktemplate.selectedItems != 0) {
                bootbox.confirm($translate.instant('LanguageContents.Res_5796.Caption'), function (result) {
                    if (result) {
                        $scope.TaskMemberLists = [];
                        $scope.IsTemplateFlow = false;
                        if (templateid != 0) {
                            $scope.IsTemplateFlow = true;
                        }
                        $scope.selectedTemplateData = $.grep($scope.Templatesdetails, function (e) {
                            return e.TemplateFlow.ID == $scope.myObjtasktemplate.selectedItems
                        });
                        if ($scope.selectedTemplateData[0] != null && $scope.selectedTemplateData[0] != undefined) {
                            var templateid = $scope.selectedTemplateData[0].TemplateFlow.ID;
                            var templatename = $scope.selectedTemplateData[0].TemplateFlow.Caption;
                            $scope.templateData.PhaseData.AllPhaseList = [];
                            $scope.templateData.TemplateLibraryID = templateid;
                            $scope.templateData.TemplateLibraryName = templatename;
                            var obj = {
                                TemplateId: templateid,
                                TemplateName: templatename,
                                Typeid: $scope.TaskTypeObject.TaskType.id
                            }
                            TaskCreationService.CollectApprovalFlowSteps(obj).then(function (data) {
                                if (data != null) {
                                    if (data.Response != null && data.Response != "") {
                                        var src = JSON.parse(data.Response);
                                        $scope.templateData.PhaseData.AllPhaseList = [];
                                        for (var i = 0, item; item = src[i++];) {
                                            for (var j = 0; j < item.StepsData.length; j++) {
                                                var today = new Date();
                                                var estDelivery = today.addDays(item.StepsData[j].Duration);
                                                item.StepsData[j].EsitimatedDelivery = dateFormat(estDelivery, $scope.GetDefaultSettings.DateFormat);
                                            }
                                            $scope.templateData.PhaseData.AllPhaseList.push(item);
                                        }
                                    }
                                } else {
                                    timerObj.getTmeplvalue = $timeout(function () { ($scope.myObjtasktemplate.selectedItems = 0) }, 5);
                                }
                            });
                        }
                    } else {
                        timerObj.getTmeplvalue = $timeout(function () { ($scope.myObjtasktemplate.selectedItems = 0) }, 5);
                    }
                })
            } else {
                $scope.IsTemplateFlow = false;
                if (templateid != 0) {
                    $scope.IsTemplateFlow = true;
                }
                $scope.templateData.PhaseData.AllPhaseList = [];
                $scope.selectedTemplateData = $.grep($scope.Templatesdetails, function (e) {
                    return e.TemplateFlow.ID == $scope.myObjtasktemplate.selectedItems
                });
                if ($scope.selectedTemplateData[0] != null && $scope.selectedTemplateData[0] != undefined) {
                    var templateid = $scope.selectedTemplateData[0].TemplateFlow.ID;
                    var templatename = $scope.selectedTemplateData[0].TemplateFlow.Caption;
                    $scope.templateData.TemplateLibraryID = templateid;
                    $scope.templateData.TemplateLibraryName = templatename;
                    var obj = {
                        TemplateId: templateid,
                        TemplateName: templatename,
                        Typeid: $scope.TaskTypeObject.TaskType.id
                    }
                    TaskCreationService.CollectApprovalFlowSteps(obj).then(function (data) {
                        if (data != null) {
                            if (data.Response != null) {
                                var src = JSON.parse(data.Response);
                                $scope.templateData.PhaseData.AllPhaseList = [];
                                for (var i = 0, item; item = src[i++];) {
                                    for (var j = 0; j < item.StepsData.length; j++) {
                                        var today = new Date();
                                        var estDelivery = today.addDays(item.StepsData[j].Duration);
                                        item.StepsData[j].EsitimatedDelivery = dateFormat(estDelivery, $scope.GetDefaultSettings.DateFormat);
                                    }
                                    $scope.templateData.PhaseData.AllPhaseList.push(item);
                                }
                            }
                        } else {
                            timerObj.getTmeplvalue = $timeout(function () { ($scope.myObjtasktemplate.selectedItems = 0) }, 5);
                        }
                    });
                }
            }
        }

        $scope.myObjtasktemplate = { selectedItems: 0 };

        $scope.getphasewithstepofTemplate = function (templateid) {
            $scope.showEstimatedDelivery = true;
            if ($scope.TaskMemberLists.length > 0 && $scope.myObjtasktemplate.selectedItems != 0) {
                bootbox.confirm($translate.instant('LanguageContents.Res_5796.Caption'), function (result) {
                    if (result) {
                        $scope.TaskMemberLists = [];
                        $scope.IsTemplateFlow = false;
                        if (templateid != 0) {
                            $scope.IsTemplateFlow = true;
                        }
                        $scope.templateData.PhaseData.AllPhaseList = [];
                        $scope.selectedTemplateData = $.grep($scope.Templatesdetails, function (e) {
                            return e.ID == $scope.myObjtasktemplate.selectedItems
                        });
                        if ($scope.selectedTemplateData[0] != null && $scope.selectedTemplateData[0] != undefined) {
                            var templateid = $scope.selectedTemplateData[0].ID;
                            var templatename = $scope.selectedTemplateData[0].Name;
                            $scope.templateData.TemplateLibraryID = templateid;
                            $scope.templateData.TemplateLibraryName = templatename;
                            var obj = {
                                TemplateId: templateid,
                                TemplateName: templatename,
                                Typeid: $scope.TaskTypeObject.TaskType.id
                            }
                            TaskCreationService.CollectApprovalFlowSteps(obj).then(function (data) {
                                if (data != null) {
                                    if (data.Response != null) {
                                        var src = JSON.parse(data.Response);
                                        $scope.templateData.PhaseData.AllPhaseList = [];
                                        for (var i = 0, item; item = src[i++];) {
                                            for (var j = 0; j < item.StepsData.length; j++) {
                                                var today = new Date();
                                                var estDelivery = today.addDays(item.StepsData[j].Duration);
                                                item.StepsData[j].EsitimatedDelivery = dateFormat(estDelivery, $scope.GetDefaultSettings.DateFormat);
                                            }
                                            $scope.templateData.PhaseData.AllPhaseList.push(item);
                                        }
                                    }
                                }
                            });
                        }
                    } else {
                        timerObj.getTmeplvalue = $timeout(function () { ($scope.myObjtasktemplate.selectedItems = 0) }, 5);
                    }
                })
            } else {
                $scope.IsTemplateFlow = false;
                if (templateid != 0) {
                    $scope.IsTemplateFlow = true;
                }
                $scope.templateData.PhaseData.AllPhaseList = [];
                $scope.selectedTemplateData = $.grep($scope.Templatesdetails, function (e) {
                    return e.ID == $scope.myObjtasktemplate.selectedItems
                });
                if ($scope.selectedTemplateData[0] != null && $scope.selectedTemplateData[0] != undefined) {
                    var templateid = $scope.selectedTemplateData[0].ID;
                    var templatename = $scope.selectedTemplateData[0].Name;
                    $scope.templateData.TemplateLibraryID = templateid;
                    $scope.templateData.TemplateLibraryName = templatename;
                    var obj = {
                        TemplateId: templateid,
                        TemplateName: templatename,
                        Typeid: $scope.TaskTypeObject.TaskType.id
                    }
                    TaskCreationService.CollectApprovalFlowSteps(obj).then(function (data) {
                        if (data != null) {
                            if (data.Response != null && data.Response != "") {
                                var src = JSON.parse(data.Response);
                                $scope.templateData.PhaseData.AllPhaseList = [];
                                for (var i = 0, item; item = src[i++];) {
                                    for (var j = 0; j < item.StepsData.length; j++) {
                                        var today = new Date();
                                        var estDelivery = today.addDays(item.StepsData[j].Duration);
                                        item.StepsData[j].EsitimatedDelivery = dateFormat(estDelivery, $scope.GetDefaultSettings.DateFormat);
                                    }
                                    $scope.templateData.PhaseData.AllPhaseList.push(item);
                                }
                            }
                        } else { }
                    });
                }
            }
        }

        function getphasewithsteps(templateid, templatename, typeid) {
            $scope.IsTemplateFlow = true;
            $scope.templateData.TemplateLibraryID = templateid;
            $scope.templateData.TemplateLibraryName = templatename;
            var obj = {
                TemplateId: templateid,
                TemplateName: templatename,
                Typeid: $scope.TaskTypeObject.TaskType.id
            }
            TaskCreationService.CollectApprovalFlowSteps(obj).then(function (data) {
                if (data != null) {
                    if (data.Response != null) {
                        var src = JSON.parse(data.Response);
                        for (var i = 0, item; item = src[i++];) {
                            for (var j = 0; j < item.StepsData.length; j++) {
                                var today = new Date();
                                var estDelivery = today.addDays(item.StepsData[j].Duration);
                                item.StepsData[j].EsitimatedDelivery = dateFormat(estDelivery, $scope.GetDefaultSettings.DateFormat);
                            }
                            $scope.templateData.PhaseData.AllPhaseList.push(item);
                        }
                    }
                } else { }
            });
        }

        function xmlToJson(xml) {
            var obj = {};
            if (xml.nodeType == 1) {
                if (xml.attributes.length > 0) {
                    obj["@attributes"] = {};
                    for (var j = 0; j < xml.attributes.length; j++) {
                        var attribute = xml.attributes.item(j);
                        obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
                    }
                }
            } else if (xml.nodeType == 3) {
                obj = xml.nodeValue;
            }
            if (xml.hasChildNodes()) {
                for (var i = 0; i < xml.childNodes.length; i++) {
                    var item = xml.childNodes.item(i);
                    var nodeName = item.nodeName;
                    if (typeof (obj[nodeName]) == "undefined") {
                        obj[nodeName] = xmlToJson(item);
                    } else {
                        if (typeof (obj[nodeName].push) == "undefined") {
                            var old = obj[nodeName];
                            obj[nodeName] = [];
                            obj[nodeName].push(old);
                        }
                        obj[nodeName].push(xmlToJson(item));
                    }
                }
            }
            return obj;
        }

        function FormSourceData(data) {
            var PhaseData = [],
				steps = [];
            for (var i = 0, val; val = data[i++];) {
                steps = [];
                var stepjson = xmlToJson($.parseXML(val.steps));
                if (stepjson != null) {
                    if (stepjson.A.p != null) {
                        if (Object.prototype.toString.call(stepjson.A.p) !== '[object Array]') {
                            var rolearray = (stepjson.A.p['Roles']["#text"] != undefined || stepjson.A.p['Roles']["#text"] != null) ? stepjson.A.p['Roles']["#text"].split(",") : [];
                            for (var r = 0; r <= rolearray.length - 1; r++) {
                                rolearray[r] = parseInt(rolearray[r]);
                            }
                            steps.push({
                                "Name": stepjson.A.p['Name']["#text"],
                                "ID": stepjson.A.p['ID']["#text"],
                                "MinApproval": stepjson.A.p['MinApproval']["#text"],
                                "Duration": stepjson.A.p['Duration']["#text"],
                                "Description": stepjson.A.p['Description']["#text"],
                                "PhaseId": stepjson.A.p['PhaseId']["#text"],
                                "Roles": rolearray,
                                IsMandatory: Boolean(stepjson.A.p['IsMandatory']["#text"].toString().toLowerCase() == "1" ? true : false),
                                isopened: true
                            });
                        } else {
                            for (var j = 0, st; st = stepjson.A.p[j++];) {
                                var rolearray = (st['Roles']["#text"] != undefined || st['Roles']["#text"] != null) ? st['Roles']["#text"].split(",") : [];
                                for (var r = 0; r <= rolearray.length - 1; r++) {
                                    rolearray[r] = parseInt(rolearray[r]);
                                }
                                steps.push({
                                    "Name": st['Name']["#text"],
                                    "ID": st['ID']["#text"],
                                    "MinApproval": st['MinApproval']["#text"],
                                    "Duration": st['Duration']["#text"],
                                    "Description": st['Description']["#text"],
                                    "PhaseId": st['PhaseId']["#text"],
                                    "Roles": rolearray,
                                    IsMandatory: st['IsMandatory']["#text"],
                                    isopened: true
                                });
                            }
                        }
                    }
                }
                val.StepsData = steps;
                PhaseData.push(val);
            }
            $scope.templateData.PhaseData.AllPhaseList.push(PhaseData);
        }
        $scope.ColorOptions = {
            preferredFormat: "hex",
            showInput: true,
            showAlpha: false,
            allowEmpty: true,
            showPalette: true,
            showPaletteOnly: false,
            togglePaletteOnly: true,
            togglePaletteMoreText: 'more',
            togglePaletteLessText: 'less',
            showSelectionPalette: true,
            chooseText: "Choose",
            cancelText: "Cancel",
            showButtons: true,
            clickoutFiresChange: true,
            palette: [
				["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
				["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)", "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
				["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)", "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)", "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)", "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
            ]
        };
        $scope.colorchange = function (color) { };
        $scope.checkAllTemplates = function () {
            if ($scope.selectedAlltemplates) {
                $scope.selectedAlltemplates = false;
            } else {
                $scope.selectedAlltemplates = true;
            }
            angular.forEach($scope.templateData.templateSrcData, function (item) {
                item.Selected = $scope.selectedAlltemplates;
            });
        };
        $scope.RemoveCheckallSelection = function (list, $event) {
            var checkbox = $event.target;
            list.Selected = checkbox.checked;
            var TemaplateCollection = $.grep($scope.templateData.templateSrcData, function (e) {
                return e.Selected == true;
            }).length;
            if (TemaplateCollection != $scope.templateData.templateSrcData.length) $scope.selectedAlltemplates = false;
            else $scope.selectedAlltemplates = true;
        }
        $scope.Selecteduser = function (list, $event) {
            var checkbox = $event.target;
            list.Selected = checkbox.checked;
            var UserCollection = $.grep($scope.globalEntityMemberLists, function (e) {
                return e.Selected == true;
            }).length;
            if (UserCollection != $scope.globalEntityMemberLists.length) $scope.globalusercheckbox = false;
            else $scope.globalusercheckbox = true;
        };
        $scope.AddNewStep = function (phaseid) {
            $scope.isAddStep = true;
            $scope.stepPopupName = "Add stage";
            $scope.TaskTemplateStepName = "";
            $scope.TaskTemplateStepDescription = "";
            $scope.TaskTemplateStepduration = "";
            $scope.TaskTemplateStepMinApproval = "";
            $scope.TaskTemplateSteproles = "";
            $scope.TaskTemplateIsMandatory = false;
            $('#NewTaskTemplateStep').modal('show');
            $scope.templateData.PhaseData.StepsData.PhaseId = phaseid;
        };
        $scope.OpenCreateTaskTemplatePhasePopup = function () {
            $scope.Phasename = "";
            $scope.Phasedescription = "";
            $scope.Phasecolorcode = "";
            $('#NewTaskTemplatePhase').modal('show');
        }
        $scope.OpenCreateTaskTemplateMemberPopup = function () {
            $('#selectApproverPopup').modal('show');
        }
        $scope.dragoverCallback = function (event, index, external, type) {
            $scope.logListEvent('dragged over', event, index, external, type);
            return index >= 0;
        };
        $scope.dropCallback = function (event, index, item, external, type, allowedType) {
            $scope.logListEvent('dropped at', event, index, external, type);
            if (external) {
                if (allowedType === 'itemType' && !item.label) return false;
                if (allowedType === 'containerType' && !angular.isArray(item)) return false;
            }
            var Dest_Phase_Details = this.phase;
            var sortorder = {};
            var phasedetails = $.grep($scope.templateData.PhaseData.AllPhaseList, function (res) {
                return res.ID = item.PhaseId;
            })[0];
            var stepdetails = $.grep(phasedetails.StepsData, function (res) {
                return res.ID = item.ID;
            })[0];
            if (allowedType == "itemType") {
                if (Dest_Phase_Details.ID == item.PhaseId) {
                    stepdetails.SortOrder = index;
                } else {
                    stepdetails.PhaseId = Dest_Phase_Details.ID;
                    stepdetails.SortOrder = index;
                }
            } else {
                phasedetails.SortOrder = index;
            }
            return item;
        };
        $scope.logListEvent = function (action, event, index, external, type) {
            var message = external ? 'External ' : '';
            message += type + ' element is ' + action + ' position ' + index;
        };
        $scope.AddStep = function (buttontype) {
            if (buttontype == 1) {
                if ($scope.TaskTemplateStepName == "" || $scope.TaskTemplateStepName == null || $scope.TaskTemplateStepduration == null || $scope.TaskTemplateStepduration == 0 || $scope.TaskTemplateStepMinApproval == null) {
                    bootbox.alert($translate.instant('LanguageContents.Res_5770.Caption'));
                    return false;
                }
                $scope.templateData.PhaseData.StepsData.Name = $scope.TaskTemplateStepName;
                $scope.templateData.PhaseData.StepsData.Description = $scope.TaskTemplateStepDescription;
                $scope.templateData.PhaseData.StepsData.Stepduration = $scope.TaskTemplateStepduration;
                $scope.templateData.PhaseData.StepsData.StepMinApproval = $scope.TaskTemplateStepMinApproval;
                $scope.templateData.PhaseData.StepsData.Steproles = $scope.TaskTemplateSteproles;
                $scope.templateData.PhaseData.StepsData.IsMandatory = Boolean($scope.TaskTemplateIsMandatory.toString().toLowerCase() == "1" ? true : false);
            }
            $('#NewTaskTemplateStep').modal('hide');
            var record = {
                "PhaseId": $scope.templateData.PhaseData.StepsData.PhaseId,
                "Name": $scope.templateData.PhaseData.StepsData.Name,
                "Description": $scope.templateData.PhaseData.StepsData.Description,
                "Duration": $scope.templateData.PhaseData.StepsData.Stepduration,
                SortOrder: 1,
                "MinApproval": $scope.templateData.PhaseData.StepsData.StepMinApproval,
                "Roles": $scope.templateData.PhaseData.StepsData.Steproles.toString(),
                IsMandatory: $scope.templateData.PhaseData.StepsData.IsMandatory,
                "ID": GenarateUniqueId(),
                isopened: true
            };
            var srcphase = $.grep($scope.templateData.PhaseData.AllPhaseList, function (rel) {
                return rel.ID == $scope.templateData.PhaseData.StepsData.PhaseId;
            })[0];
            if (srcphase != null) srcphase.StepsData.push(record);
            var phaseMaxduration = 0;
            for (var i = 0; i < srcphase.StepsData.length; i++) {
                phaseMaxduration = parseInt(srcphase.StepsData[i].Duration) > phaseMaxduration ? parseInt(srcphase.StepsData[i].Duration) : phaseMaxduration;
            }
            srcphase.maxduration = phaseMaxduration;
        }
        $scope.deleteStepmember = function (stepid, phaseid, userid) {
            var srcphase = $.grep($scope.templateData.PhaseData.AllPhaseList, function (rel) {
                return rel.ID == phaseid;
            })[0];
            var stepdetails = $.grep(srcphase.StepsData, function (rel) {
                return rel.ID == stepid;
            })[0];
            var member = $.grep(stepdetails.MemberList, function (rel) {
                return rel.Userid == userid;
            })[0];
            stepdetails.MemberList.splice($.inArray(member, stepdetails.MemberList), 1);
            var glmember = $.grep($scope.TaskMemberLists, function (rel) {
                return rel.Userid == userid;
            })[0];
            $scope.TaskMemberLists.splice($.inArray(glmember, $scope.TaskMemberLists), 1);
        }
        $scope.deleteglobaltaskmember = function (userid) {
            var glmember = $.grep($scope.TaskMemberLists, function (rel) {
                return rel.Userid == userid;
            })[0];
            $scope.TaskMemberLists.splice($.inArray(glmember, $scope.TaskMemberLists), 1);
        }
        $scope.UpdateExistingStep = function (buttontype) {
            var srcphase = $.grep($scope.templateData.PhaseData.AllPhaseList, function (rel) {
                return rel.ID == $scope.templateData.PhaseData.StepsData.PhaseId;
            })[0];
            var stepdetails = $.grep(srcphase.StepsData, function (rel) {
                return rel.ID == $scope.templateData.PhaseData.StepsData.StepID;
            })[0];
            $scope.templateData.PhaseData.StepsData.Name = $scope.TaskTemplateStepName;
            $scope.templateData.PhaseData.StepsData.Description = $scope.TaskTemplateStepDescription;
            $scope.templateData.PhaseData.StepsData.Stepduration = $scope.TaskTemplateStepduration;
            $scope.templateData.PhaseData.StepsData.StepMinApproval = $scope.TaskTemplateStepMinApproval;
            $scope.templateData.PhaseData.StepsData.Steproles = $scope.TaskTemplateSteproles;
            $scope.templateData.PhaseData.StepsData.IsMandatory = ($scope.TaskTemplateIsMandatory == undefined) ? false : $scope.TaskTemplateIsMandatory;
            if (stepdetails != null) {
                stepdetails["Name"] = $scope.templateData.PhaseData.StepsData.Name;
                stepdetails["Description"] = $scope.templateData.PhaseData.StepsData.Description;
                stepdetails["Duration"] = $scope.templateData.PhaseData.StepsData.Stepduration;
                stepdetails["MinApproval"] = $scope.templateData.PhaseData.StepsData.StepMinApproval;
                stepdetails["Roles"] = $scope.templateData.PhaseData.StepsData.Steproles;
                stepdetails["IsMandatory"] = $scope.templateData.PhaseData.StepsData.IsMandatory;
                stepdetails["isopened"] = true;
            }
            var PhaseIndex = $scope.templateData.PhaseData.AllPhaseList.indexOf(srcphase);
            var maxDuration = 0;
            for (var j = 0; j < $scope.templateData.PhaseData.AllPhaseList[PhaseIndex].StepsData.length; j++) {
                maxDuration = parseInt($scope.templateData.PhaseData.AllPhaseList[PhaseIndex].StepsData[j].Duration) > maxDuration ? parseInt($scope.templateData.PhaseData.AllPhaseList[PhaseIndex].StepsData[j].Duration) : maxDuration;
            }
            $scope.templateData.PhaseData.AllPhaseList[PhaseIndex].maxduration = maxDuration;
            $('#NewTaskTemplateStep').modal('hide');
            $scope.templateData.PhaseData.StepsData.PhaseId = 0;
            $scope.templateData.PhaseData.StepsData.StepID = 0;
            $scope.TaskTemplateStepName = "";
            $scope.TaskTemplateStepduration = "";
            $scope.TaskTemplateStepMinApproval = "";
            $scope.TaskTemplateSteproles = "";
            $scope.TaskTemplateIsMandatory = false;
            $scope.TaskTemplateStepDescription = "";
        }
        $scope.AddNewPhase = function () {
            if ($scope.Phasename == null || $scope.Phasename == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_5740.Caption'));
                return false;
            }
            var record = {};
            record = {
                Description: $scope.Phasedescription,
                ID: GenarateUniqueId(),
                Name: $scope.Phasename,
                SortOrder: 0,
                StepsData: [],
                maxduration: 1
            };
            if ($scope.TaskMemberLists.length > 0) {
                var step = {
                    "PhaseId": record.ID,
                    "Name": "Stage 1",
                    "Description": "Stage 1",
                    "Duration": 1,
                    SortOrder: 1,
                    "MinApproval": 1,
                    "Roles": 0,
                    IsMandatory: false,
                    "ID": GenarateUniqueId(),
                    isopened: true,
                    "MemberList": []
                };
                for (var i = 0, mem; mem = $scope.TaskMemberLists[i++];) {
                    step.MemberList.push(mem);
                    $scope.TaskMemberLists = [];
                }
                record.StepsData.push(step);
                $scope.TaskMemberLists = [];
            } else if ($scope.TaskMemberLists.length == 0 || $scope.templateData.PhaseData.AllPhaseList.length == 0) {
                var step = {
                    "PhaseId": record.ID,
                    "Name": "Stage 1",
                    "Description": "Stage 1",
                    "Duration": 1,
                    SortOrder: 1,
                    "MinApproval": 1,
                    "Roles": 0,
                    IsMandatory: false,
                    "ID": GenarateUniqueId(),
                    isopened: true,
                    "MemberList": []
                };
                for (var i = 0, mem; mem = $scope.TaskMemberLists[i++];) {
                    step.MemberList.push(mem);
                }
                record.StepsData.push(step);
                $scope.TaskMemberLists = [];
            }
            $scope.templateData.PhaseData.AllPhaseList.push(record);
            $('#NewTaskTemplatePhase').modal('hide');
            $scope.Phasename = "";
            $scope.Phasedescription = "";
            $scope.Phasecolorcode = "";
            $scope.approvalflowTempPopup = true;
            $scope.addMemberButtonforwithoutstepTask = false;
            $scope.TaskMemberLists = [];
        }
        $scope.UpdateStep = function (stepid, phaseid) {
            $scope.isAddStep = false;
            $scope.stepPopupName = "Edit stage";
            $scope.templateData.PhaseData.StepsData.StepID = stepid;
            $scope.templateData.PhaseData.StepsData.PhaseId = phaseid;
            $('#NewTaskTemplateStep').modal('show');
            var phasedetails = $.grep($scope.templateData.PhaseData.AllPhaseList, function (rel) {
                return rel.ID == phaseid;
            })[0];
            var stepdetails = $.grep(phasedetails.StepsData, function (rel) {
                return rel.ID == stepid;
            })[0];
            $scope.TaskTemplateStepName = stepdetails.Name;
            $scope.TaskTemplateStepDescription = stepdetails.Description;
            $scope.TaskTemplateStepMinApproval = stepdetails.MinApproval;
            $scope.TaskTemplateStepduration = stepdetails.Duration;
            $scope.TaskTemplateSteproles = stepdetails.Roles;
            $scope.TaskTemplateIsMandatory = Boolean(stepdetails.IsMandatory.toString().toLowerCase());
        };
        $scope.DeleteStep = function (stepid, phaseid) {
            var srcphase = $.grep($scope.templateData.PhaseData.AllPhaseList, function (rel) {
                return rel.ID == phaseid;
            })[0];
            if (srcphase.StepsData.length > 1) {
                bootbox.confirm($translate.instant('LanguageContents.Res_5791.Caption')+"?", function (confirm) {
                    if (confirm) {
                        var stepdetails = $.grep(srcphase.StepsData, function (rel) {
                            return rel.ID == stepid;
                        })[0];
                        srcphase.StepsData.splice($.inArray(stepdetails, srcphase.StepsData), 1);
                        var phaseMaxduration = 0;
                        for (var i = 0; i < srcphase.StepsData.length; i++) {
                            phaseMaxduration = parseInt(srcphase.StepsData[i].Duration) > phaseMaxduration ? parseInt(srcphase.StepsData[i].Duration) : phaseMaxduration;
                        }
                        srcphase.maxduration = phaseMaxduration;
                    }
                })
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_5771.Caption'));
            }
        };
        $scope.AddExistingTemplate = function () {
            var SelectedTemplates = $.grep($scope.templateData.templateSrcData, function (e) {
                return e.Selected == true;
            });
            if (SelectedTemplates.length == 0) {
                NotifyError($translate.instant('LanguageContents.Res_5826.Caption'));
            }
            if (SelectedTemplates.length == 1) {
                $('#existingTemplatePopup').modal('hide');
                $scope.approvalflowTempPopup = true;
                getphasewithsteps(SelectedTemplates[0].ID, SelectedTemplates[0].Name);
            } else {
                NotifyError($translate.instant('LanguageContents.Res_5829.Caption'));
            }
        }
        $scope.items = [];
        $scope.lastSubmit = [];

        $scope.addNew = function () {
            var ItemCnt = $scope.items.length;
            if (ItemCnt > 0) {
                if ($scope.items[ItemCnt - 1].startDate == null || $scope.items[ItemCnt - 1].startDate.length == 0 || $scope.items[ItemCnt - 1].endDate.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1986.Caption'));
                    return false;
                }
                $scope.items.push({
                    startDate: [],
                    endDate: [],
                    comment: '',
                    sortorder: 0
                });
            }
        };
        $scope.submitOne = function (item) {
            $scope.lastSubmit = angular.copy(item);
        };
        $scope.deleteOne = function (item) {
            $scope.lastSubmit.splice($.inArray(item, $scope.lastSubmit), 1);
            $scope.items.splice($.inArray(item, $scope.items), 1);
        };
        $scope.submitAll = function () {
            $scope.lastSubmit = angular.copy($scope.items);
        }
        $scope.CheckPreviousStartDate = function (enddate, startdate, currentindex) {
            var enddate1 = null;
            if (currentindex != 0 || currentindex == 0) {
                enddate1 = $scope.items[currentindex].endDate;
            }
            if (enddate1 != null && startdate >= enddate1) {
                bootbox.alert($translate.instant('LanguageContents.Res_4240.Caption'));
                $scope.items[currentindex].endDate = null;
            }
        };
        function GetEntityApprovalTaskTemplatedetails(EntityID) {
            TaskCreationService.GetAllAdminApprovalFlowPhasesSteps(EntityID).then(function (TaskDetails) {
                if (TaskDetails.Response != null) {
                    FormSourceData(TaskDetails.Response);
                }
            });
        }       
        $scope.createdynamicControls = function (rootID) {          
            $scope.IsTemplateFlow = false;
            if ($scope.fields == undefined) {
                $scope.fields = { usersID: '' };
            }
            $scope.fields.usersID = '';
            
            if (rootID.id != "" && rootID.id != undefined) {
                var Tasktype_value = $.grep($scope.TaskTypeList, function (rel) {
                    return rel.Id == rootID.id;
                })[0];
                if (Tasktype_value != undefined) {
                    if (Tasktype_value.TaskTypeId == taskType.Work_Task) {
                        ClearAllScopeVariables();
                        RefreshAllTaskFlowcreationScopes();
                        $scope.rytSideApprovalButtonPane = false;
                        $scope.ChecklistVisible = true;
                        $scope.AdminTaskCheckList = [{
                            ID: 0,
                            NAME: ""
                        }];
                        $scope.approvalflowTempPopup = false;
                        $scope.AddTemplateFromDalim = false;
                        $scope.AddGlobalMemberforTask = true;
                    } else if (Tasktype_value.TaskTypeId == taskType.Approval_Task || Tasktype_value.TaskTypeId == taskType.Asset_Approval_Task) {
                        $scope.ChecklistVisible = false;
                        ClearAllScopeVariables();
                        RefreshAllTaskFlowcreationScopes();
                        TaskCreationService.GetApprovalFlowLibraryList(Tasktype_value.TaskTypeId, false).then(function (Templatesdetails) {
                            if (Templatesdetails.Response != null || Templatesdetails.Response != " ") {
                                $scope.Templatesdetails = JSON.parse(Templatesdetails.Response);
                                $scope.ApprTaskFlowList = $scope.Templatesdetails;
                                if ($scope.Templatesdetails.length == 1) {
                                    timerObj.setTemplatevalue = $timeout(function () {
                                        $scope.myObjtasktemplate.selectedItems = parseInt($scope.ApprTaskFlowList[0].ID);
                                        $scope.getphasewithstepofTemplate($scope.myObjtasktemplate.selectedItems);
                                    }, 5);
                                } else {
                                    $scope.myObjtasktemplate.selectedItems = 0;
                                }
                                $scope.rytSideApprovalButtonPane = true;
                                $scope.AddTemplateFromDalim = false;
                                $scope.AddGlobalMemberforTask = false;
                            }
                        });
                    } else if (Tasktype_value.TaskTypeId == taskType.dalim_approval_task) {
                        ClearAllScopeVariables();
                        RefreshAllTaskFlowcreationScopes();
                        TaskCreationService.GetApprovalFlowLibraryList(taskType.dalim_approval_task, false).then(function (Templatesdetails) {
                            if (Templatesdetails.Response != null || Templatesdetails.Response != " ") {
                                $scope.Templatesdetails = JSON.parse(Templatesdetails.Response);
                                $scope.DalimTaskFlowList = $scope.Templatesdetails;
                                if ($scope.DalimTaskFlowList.length == 1) {
                                    timerObj.dalimflow = $timeout(function () {
                                        $scope.myObjtasktemplate.selectedItems = parseInt($scope.DalimTaskFlowList[0].TemplateFlow.ID);
                                        $scope.getphasewithstepforDalim($scope.myObjtasktemplate.selectedItems);
                                    }, 5);
                                } else {
                                    $scope.myObjtasktemplate.selectedItems = 0;
                                }
                                $scope.ChecklistVisible = false;
                                $scope.rytSideApprovalButtonPane = false;
                                $scope.AddTemplateFromDalim = true;
                                $scope.AddGlobalMemberforTask = false;
                            }
                        });
                    } else {
                        $scope.rytSideApprovalButtonPane = false;
                        $scope.approvalflowTempPopup = false;
                        ClearAllScopeVariables();
                        RefreshAllTaskFlowcreationScopes();
                        $scope.ChecklistVisible = false;
                        $scope.AddTemplateFromDalim = false;
                        $scope.AdminTaskCheckList = [{
                            ID: 0,
                            NAME: ""
                        }];
                        $scope.AddGlobalMemberforTask = true;
                    }
                }
            } else {
                $scope.AdminTaskCheckList = [{
                    ID: 0,
                    NAME: ""
                }];
                $scope.ChecklistVisible = false;
            }
            $("#DynamicTaskControlsAttributies").html("");
            $("#DynamicTaskControlsAttributies").append($compile("")($scope));
            $scope.treeNodeSelectedHolder = [];
            $scope.treesrcdirec = {};
            $scope.treePreviewObj = {};
            $scope.PercentageVisibleSettings = {};
            $scope.DropDownTreePricing = {};
            if (params.data.IsAssetedit != undefined) {
                if (params.data.IsAssetedit == false)
                    $scope.ClearModelObject($scope.fields);
            }
            else
                $scope.ClearModelObject($scope.fields);
            $scope.dyn_Cont = '';
            $scope.ShowHideAttributeOnRelation = {};
            $scope.EnableDisableControlsHolder = {};
            $scope.OptionObj = {};
            $scope.items = [];           
            $scope.DateObject = {};
            if (rootID.id != null && rootID.id != "") {              
                TaskCreationService.GetEntityTypeAttributeRelationWithLevelsByID(rootID.id, 0).then(function (entityAttributesRelation) {
                    $scope.atributesRelationList = entityAttributesRelation.Response;
                    for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                        if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                            if ($scope.atributesRelationList[i].AttributeID != 70) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                if ($scope.atributesRelationList[i].InheritFromParent) $scope.atributesRelationList[i].DefaultValue = $scope.atributesRelationList[i].ParentValue[0];
                                if ($scope.atributesRelationList[i].AttributeID !== SystemDefiendAttributes.Name) {
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                                    $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            if ($scope.atributesRelationList[i].IsSpecial == true) {
                                if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                    $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"> <input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.OwnerList[0].UserName;
                                }
                            } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.FiscalYear) {
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,3)\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                if ($scope.atributesRelationList[i].InheritFromParent) $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                                else $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.EntityStatus) { } else {
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,3)\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                if ($scope.atributesRelationList[i].InheritFromParent) $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                                else $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                            var totLevelCnt = $scope.atributesRelationList[i].Levels.length;
                            for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                //    return item.Caption
                                //};
                                //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                //    return item.Caption
                                //};

                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {
                                    multiple: false,
                                    formatResult: function (item) {
                                        return item.text;
                                    },
                                    formatSelection: function (item) {
                                        return item.text;
                                    }
                                };

                                if (j == 0) {
                                    $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                    $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                    //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                    //$scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";

                                    $scope.dyn_Cont += "<select ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                    $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                    $scope.dyn_Cont += "</select></div></div>";

                                    //$scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                    //$scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                } else {
                                    $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                    //$scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                    $scope.dyn_Cont += "<select ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                    $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                    $scope.dyn_Cont += "</select></div></div>";
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                            if (($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TakeDescription) && ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskNotes)) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea></div></div>";
                                if ($scope.atributesRelationList[i].InheritFromParent) $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                                else $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select  class=\"multiselect\"   data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\"  multiselect-dropdown ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,4)\"   ></select></div></div>";
                            if ($scope.atributesRelationList[i].InheritFromParent) {
                                var defaultmultiselectvalue1 = $scope.atributesRelationList[i].ParentValue.split(',');
                                $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = defaultmultiselectvalue1;
                            } else {
                                var defaultmultiselectvalue = $scope.atributesRelationList[i].DefaultValue.split(',');
                                if ($scope.atributesRelationList[i].DefaultValue != "") {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = defaultmultiselectvalue;
                                } else {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                            $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                            if ($scope.MinValue < 0) {
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                            } else {
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                            }
                            if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                            } else {
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                            }
                            var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                            $scope.items.push({
                                startDate: [],
                                endDate: [],
                                comment: '',
                                sortorder: 0,
                                calstartopen: false,
                                calendopen: false
                            });
                            $scope.dyn_Cont += "    <div class=\"control-group\"><label for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.atributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                            $scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span5\">";
                            $scope.dyn_Cont += "<input class=\"sdate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-click=\"PeriodCalanderopen($event,item,'start')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calstartopen\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-change=\"changeperioddate_changed(item.startDate,fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + ",'StartDate')\" ng-model=\"item.startDate\" placeholder=\"-- Start date --\"/><input class=\"edate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" name=\"enddate\" id=\"item.endDate\" ng-change=\"changeperioddate_changed(item.endDate,fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + ",'EndDate')\" ng-model=\"item.endDate\" ng-click=\"PeriodCalanderopen($event,item,'end')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calendopen\" datepicker-popup=\"{{format}}\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\"  date-disabled=\"disabled(date, mode)\" datepicker-options=\"dateOptions\"  close-text=\"Close\" placeholder=\"-- End date --\"/><input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
                            $scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
                            $scope.fields["DatePart_Calander_Open" + "item.startDate"] = false;
                            $scope.fields["DatePart_Calander_Open" + "item.endDate"] = false;
                            $scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
                            $scope.fields["Period_" + $scope.atributesRelationList[i].AttributeID] = "";
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                            $scope.fields["Tree_" + $scope.atributesRelationList[i].AttributeID] = [];
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                            if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                treeTextVisbileflag = false;
                                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID])) {
                                    $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = true;
                                } else $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                            } else {
                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                            }
                            $scope.dyn_Cont += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + $scope.atributesRelationList[i].AttributeID + '\" class="control-group treeNode-control-group">';
                            $scope.dyn_Cont += '<label class="control-label">' + $scope.atributesRelationList[i].AttributeCaption + '</label>';
                            $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                            $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '"></div>';
                            $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '">';
                            $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                            $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" accessable="' + $scope.atributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                            $scope.dyn_Cont += '</div></div></div>';
                            $scope.dyn_Cont += '<div class="control-group staticTreeGroup margin-top0x" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationList[i].AttributeID + '\">';
                            $scope.dyn_Cont += '<div class="controls">';
                            $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.atributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                            $scope.dyn_Cont += '</div></div>';
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.MTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"MTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                            $scope.fields["MTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                            $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = true;
                            $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = $scope.atributesRelationList[i].DropDownPricing;
                            $scope.dyn_Cont += "<div drowdowntreepercentagemultiselection data-purpose='entity' data-attributeid=" + $scope.atributesRelationList[i].AttributeID.toString() + "></div>";
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                            if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskDueDate) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = false;
                                $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                } else {
                                    $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                } else {
                                    $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                                }
                                var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.DateTime_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-change=\"setTimeout(changeduedate_changed(fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + ",fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "," + $scope.atributesRelationList[i].AttributeID + "),1000)\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event,'Date_" + $scope.atributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Date_" + $scope.atributesRelationList[i].AttributeID + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                var param1 = new Date.create();
                                var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                                $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = false;
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.CurrencyFormatsList;
                            $scope['origninalamountvalue_' + $scope.atributesRelationList[i].AttributeID] = '0';
                            $scope['currRate_' + $scope.atributesRelationList[i].AttributeID] = 1;
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls currenycontrol\"><input type=\"text\"   ng-change=\"Getamountentered(" + $scope.atributesRelationList[i].AttributeID + ")\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  class=\"currencySelector\" id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"GetCostCentreCurrencyRateById(" + $scope.atributesRelationList[i].AttributeID + ")\"><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.ShortName}}</option></select></div>";
                            $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = '0';
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.DefaultSettings.CurrencyFormat.Id;
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
                            $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = [];
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                            $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                            $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                            if ($scope.MinValue < 0) {
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                            } else {
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                            }
                            if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                            } else {
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                            }
                            var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                            $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" class=\"DatePartctrl\" " + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\" ng-click=\"Calanderopen($event,'Date_" + $scope.atributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Date_" + $scope.atributesRelationList[i].AttributeID + "\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-change=\"setTimeout(changeduedate_changed(fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + ",fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "," + $scope.atributesRelationList[i].AttributeID + "),1000)\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\"></div></div>";

                            var param1 = new Date.create();
                            var param2 = param1.getDate() + '/' + (param1.getMonth() + 1) + '/' + param1.getFullYear();
                            $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                            $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                            $scope.setFieldKeys();
                            $scope.fields["fields.DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                            var totLevelCnt1 = $scope.atributesRelationList[i].Levels.length;
                            for (var j = 0; j < totLevelCnt1; j++) {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {
                                    multiple: false,
                                    formatResult: function (item) {
                                        return item.text;
                                    },
                                    formatSelection: function (item) {
                                        return item.text;
                                    }
                                };
                                if (totLevelCnt1 == 1) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                    //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                    //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                    //    return item.Caption
                                    //};
                                    //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                    //    return item.Caption
                                    //};
                                    //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                    $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                    $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";                                    
                                    $scope.dyn_Cont += "<select multiple ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-click=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                    $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                    $scope.dyn_Cont += "</select></div></div>";
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                    //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                    //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                    //    return item.Caption
                                    //};
                                    //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                    //    return item.Caption
                                    //};
                                    if (j == 0) {
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                        $scope.dyn_Cont += "<select ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-click=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                        $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                        $scope.dyn_Cont += "</select></div></div>";
                                    } else {
                                        //$scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                        $scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = [];
                                        if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                            $scope.dyn_Cont += "<select multiple ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-click=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                            $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                            $scope.dyn_Cont += "</select></div></div>";
                                        } else {
                                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                            $scope.dyn_Cont += "<select ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" >";
                                            $scope.dyn_Cont += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                            $scope.dyn_Cont += "</select></div></div>";
                                        }
                                    }
                                }
                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            $scope.setoptions();
                            $scope.dyn_Cont += '<div class="control-group"><label class="control-label"';
                            $scope.dyn_Cont += 'for="fields.Uploader_ ' + $scope.atributesRelationList[i].AttributeID + '">' + $scope.atributesRelationList[i].AttributeCaption + ': </label>';
                            $scope.dyn_Cont += '<div id="Uploader" class="controls">';
                            //$scope.dyn_Cont += '<img id="UploaderPreview_' + $scope.atributesRelationList[i].AttributeID + '" class="ng-pristine ng-valid entityImgPreview"';
                            //$scope.dyn_Cont += ' ng-model="fields.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" id="UploaderImageControl" src="" alt="No thumbnail present">';
                            if ($scope.atributesRelationList[i].Value == "" || $scope.atributesRelationList[i].Value == null && $scope.atributesRelationList[i].Value == undefined) {
                                $scope.atributesRelationList[i].Value = "NoThumpnail.jpg";
                            }
                            $scope.dyn_Cont += '<img id="UploaderPreview_' + $scope.atributesRelationList[i].AttributeID + '" src="' + imagesrcpath + 'UploadedImages/' + $scope.atributesRelationList[i].Value + '" alt="' + $scope.atributesRelationList[i].Caption + '"';
                            $scope.dyn_Cont += 'ng-model="fields.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" class="ng-pristine ng-valid entityDetailImgPreview" id="UploaderImageControl" >';
                            $scope.dyn_Cont += '<br><a ng-show="EnableDisableControlsHolder.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" ng-model="UploadImage" ng-click="UploadImagefileTaskCreate(' + $scope.atributesRelationList[i].AttributeID + ')" class="ng-pristine ng-valid">Select Image</a>';
                            $scope.dyn_Cont += '</div></div>';
                            $scope.fields["Uploader_" + $scope.atributesRelationList[i].AttributeID] = "";
                            $scope.setFieldKeys();
                        }
                        if ($scope.atributesRelationList[i].IsReadOnly == true) {
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        } else {
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        }
                    }
                    $scope.dyn_Cont += '<input style="display:none" type="submit" id="damtskbtnTemp" class="ng-scope" invisible>';
                    $scope.dyn_Cont += '<input style="display:none" type="submit" id="tskbtnTemp" class="ng-scope" invisible>';
                    $("#DynamicTaskControlsAttributies").append($compile($scope.dyn_Cont)($scope));
                    var mar = ($scope.DecimalSettings.FinancialAutoNumeric.vMax).substr(($scope.DecimalSettings.FinancialAutoNumeric.vMax).indexOf(".") + 1);
                    $scope.mindec = "";
                    if (mar.length == 1) {
                        $("[id^='dTextSingleLine_']").autoNumeric('init', {
                            aSep: ' ',
                            vMin: "0",
                            mDec: "1"
                        });
                    }
                    if (mar.length != 1) {
                        if (mar.length < 5) {
                            $scope.mindec = "0.";
                            for (i = 0; i < mar.length; i++) {
                                $scope.mindec = $scope.mindec + "0";
                            }
                            $("[id^='dTextSingleLine_']").autoNumeric('init', {
                                aSep: ' ',
                                vMin: $scope.mindec
                            });
                        } else {
                            $("[id^='dTextSingleLine_']").autoNumeric('init', {
                                aSep: ' ',
                                vMin: "0",
                                mDec: "0"
                            });
                        }
                    }
                    $("[id^='MTextSingleLine_']").autoNumeric('init', {
                        aSep: ' ',
                        vMin: "0",
                        mDec: "0"
                    });
                    $("[id^='MTextSingleLine_']").keydown(function (event) {
                        if (event.which != 8 && isNaN(String.fromCharCode(event.which)) && (event.keyCode < 96 || event.keyCode > 105) && event.which != 46) {
                            bootbox.alert($translate.instant('Please enter only Number'));
                            event.preventDefault();
                        }
                    });
                    $("#DamTaskDynamicMetadata").scrollTop(0);
                    $scope.rootDisplayName = $scope.fields["TextSingleLine_" + SystemDefiendAttributes.Name];
                    timerObj.hideattribute = $timeout(function () {
                        HideAttributeToAttributeRelationsOnPageLoad();
                    }, 200);
                    GetValidationList(rootID.id);
                    $("#DamTaskDynamicMetadata").addClass('notvalidate');
                    $("#DynamicTaskControlsAttributies").addClass('notvalidate');

                });
            }
        }
        $scope.ClearModelObject = function (ModelObject) {
            for (var variable in ModelObject) {
                if (typeof ModelObject[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        ModelObject[variable] = "";
                    }
                } else if (typeof ModelObject[variable] === "number") {
                    ModelObject[variable] = null;
                } else if (Array.isArray(ModelObject[variable])) {
                    ModelObject[variable] = [];
                } else if (typeof ModelObject[variable] === "object") {
                    ModelObject[variable] = {};
                }
            }
        }

        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }
        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad = function (attrID, attributeLevel, attrVal, attrType) {
            try {
                var optionValue = attrVal;
                var attributesToShow = [];
                if (attrType == 3) {
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 7) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6 || attrType == 12) {
                    if (attrVal != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrVal != null) ? parseInt(attrVal, 10) : 0) && e.AttributeLevel == ((attributeLevel != null) ? parseInt(attributeLevel, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }

        function GetValidationList(rootID) {
            TaskCreationService.GetValidationDationByEntitytype(rootID).then(function (GetValidationresult) {
                if (GetValidationresult.Response != null) {
                    $scope.listValidationResult = GetValidationresult.Response;
                    $scope.listofValidations = GetValidationresult.Response;
                    if ($scope.listValidationResult.length != 0) {
                        for (var i = 0 ; i < $scope.listofValidations.length; i++) {
                            if ($scope.listofValidations[i][1] == "presence") {
                                $($scope.listofValidations[i][0]).parent().addClass('relative');
                                if ($($scope.listofValidations[i][0]).parent().find('ul').length >= 1) {
                                    $("<i class=\"icon-asterisk validationmark margin-right5x\"></i>").insertAfter($($scope.listofValidations[i][0]).parent().find('ul'));
                                } else {
                                    $("<i class=\"icon-asterisk validationmark margin-right5x\"></i>").insertAfter($scope.listofValidations[i][0]);
                                }
                            }
                        }
                    }
                    if ($scope.listAttriToAttriResult != null) {
                        for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                            var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                            if (attrRelIDs != undefined) {
                                for (var j = 0; j < attrRelIDs.length; j++) {
                                    if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                        });
                                    } else {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                        });
                                    }
                                }
                            }
                        }                                           
                    }
                    $("#TaskDynamicMetadata").nod($scope.listValidationResult, {
                        'delay': 200,
                        'submitBtnSelector': '#tskbtnTemp',
                        'silentSubmit': 'true'
                    });
                }
            });
        }
        $scope.CloseAddTask = function () {
            $scope.DamAssetTaskselectionFiles.AssetSelectionFiles = [];
            $scope.$emit('resetTaskCreationAttachAssetObj');
            closecreatetaskpopup();
        }
        $scope.TaskTypeObject.TaskType = 0;




        $scope.TaskTypeListdata = [];
        $scope.tagAllOptionsTasktypelist = {
            multiple: false,
            allowClear: true,
            data: $scope.TaskTypeListdata,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.AttributeData = [];
        $scope.Entityamountcurrencytypeitem = [];
        $scope.CreateNewTask = function () {
            $("#TaskDynamicMetadata").removeClass('notvalidate');
            $("#DynamicTaskControlsAttributies").removeClass('notvalidate');

            $timeout(function () {
                $("#tskbtnTemp").click();
            },3);
            $("#TaskDynamicMetadata").removeClass('notvalidate');
                     
            if ($("#TaskDynamicMetadata .error").length > 0) {              
                return false;
            }

            if (parseInt($scope.TaskTypeObject.TaskType.id) == taskType.dalim_approval_task && $scope.myObjtasktemplate.selectedItems == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_5584.Caption'));
            } else {
                   $("#tskbtnTemp").click();
                    if ($('#CreateNewTaskBtn').hasClass('disabled')) {
                        return;
                    }
                    $('#CreateNewTaskBtn').addClass('disabled');
                    $scope.addtaskvallid = 0;
                    if ($("#assetassettaskentityattachassetdata > div").length > 0) {
                        $scope.uploadingInProgress = true;
                    }
                    var percentageflag = false;
                    $('div[data-role="formpercentagetotalcontainer"]').children().find('span[data-role="percentageerror"]').each(function (index, value) {
                        if (($(this).attr('data-selection') != undefined) && ($(this).attr('data-ispercentage') != undefined) && ($(this).attr('data-isnotfilter') != undefined)) {
                            if (((parseInt($(this).attr('data-selection')) > 1) && ($(this).attr('data-ispercentage') == "true") && ($(this).attr('data-isnotfilter') == "true")) && ($(this).hasClass("result lapse"))) {
                                percentageflag = true;
                            }
                        }
                    });
                    if (percentageflag) {
                        return false;
                        $scope.uploadingInProgress = false;
                    }                                        
                        $("#TaskDynamicMetadata").removeClass('notvalidate');
                    if ($("#TaskDynamicMetadata .error").length > 0) {
                        $('#CreateNewTaskBtn').removeClass('disabled');
                        return false;
                        $scope.uploadingInProgress = false;
                    }
                    var alertcount = 0;
                    $scope.IsNotVersioning = true;
                    var dateval = new Date.create();
                    dateval = new Date.create(dateFormat(dateval, $scope.DefaultSettings.DateFormat));
                    var alertText = "";
                    $scope.AttributeData = [];
                    var taskMembersAvailable = [];
                    if ($scope.TaskMembersList == undefined || $scope.TaskMembersList == "") taskMembersAvailable = [];
                    else taskMembersAvailable = $scope.TaskMembersList;
                    if ($scope.TaskTypeObject.TaskType == "" || $scope.TaskTypeObject.TaskType == undefined || $scope.TaskTypeObject.TaskType == 0) alertText += $translate.instant('LanguageContents.Res_4637.Caption') + "\n";
                    var Tasktype_value;
                    if ($scope.TaskTypeObject.TaskType != null && $scope.TaskTypeObject.TaskType != undefined) {
                        Tasktype_value = $.grep($scope.TaskTypeObject.TaskType, function (rel) {
                            return rel.Id == ($scope.TaskTypeObject.TaskType.id == undefined ? $scope.TaskTypeObject.TaskType : $scope.TaskTypeObject.TaskType.id);
                        })[0];
                    }
                    if (Tasktype_value != undefined) if (Tasktype_value.TaskTypeId == "" || Tasktype_value.TaskTypeId == undefined) {
                        if (!alertText.contains($translate.instant('LanguageContents.Res_4637.Caption'))) alertText += $translate.instant('LanguageContents.Res_4637.Caption');
                    }
                    if ($scope.TaskTypeObject.NewTaskName == "") alertText += $translate.instant('LanguageContents.Res_4587.Caption') + "\n";
                    if ($scope.ChecklistVisible == true) {
                        if ($scope.AdminTaskCheckList.length > 1) {
                            for (var i = 0; i < $scope.AdminTaskCheckList.length; i++) {
                                if ($scope.AdminTaskCheckList[i]["Name"] == "" || $scope.AdminTaskCheckList[i]["Name"] == null || $scope.AdminTaskCheckList[i]["Name"] == undefined) {
                                    if (alertcount == 0) {
                                        alertText += $translate.instant('LanguageContents.Res_4588.Caption');
                                        alertcount = 1;
                                    }
                                }
                            }
                        }
                    }
                    if (alertText == "") {
                        $("#TaskDynamicMetadata").removeClass('notvalidate');
                        if ($("#TaskDynamicMetadata .error").length > 0) {
                            return false;
                        }
                        $scope.addtaskvallid = 1;
                        $scope.AttributeData = [];
                        if ($("#assetassettaskentityattachassetdata > div").length > 0 && $scope.addtaskvallid > 0) {
                            if ($scope.UniqueidsfrBlankAsset.length + $scope.Fileids.length > 0 && $("#assetassettaskentityattachassetdata > div").length > 0) {
                                var count = 0;
                                var namecount = 0;
                                angular.forEach($scope.UniqueidsfrBlankAsset, function (key) {
                                    if ($scope.assetfields['assettype_' + key] == '' || $scope.assetfields['assettype_' + key] == null || $scope.assetfields['assettype_' + key] == undefined) {
                                        count = count + 1;
                                    }
                                    if ($scope.assetfields['assetname_' + key] == '' || $scope.assetfields['assetname_' + key] == null || $scope.assetfields['assetname_' + key] == undefined) {
                                        namecount = namecount + 1;
                                    }
                                });
                                angular.forEach($scope.Fileids, function (key) {
                                    if ($scope.assetfields['assettype_' + key] == '' || $scope.assetfields['assettype_' + key] == null || $scope.assetfields['assettype_' + key] == undefined) {
                                        count = count + 1;
                                    }
                                    if ($scope.assetfields['assetname_' + key] == '' || $scope.assetfields['assetname_' + key] == null || $scope.assetfields['assetname_' + key] == undefined) {
                                        namecount = namecount + 1;
                                    }
                                });
                                if (count != 0 || namecount != 0) {
                                    $('#CreateNewTaskBtn').removeClass('disabled');
                                    bootbox.alert($translate.instant('LanguageContents.Res_4594.Caption'));
                                    $scope.uploadingInProgress = false;
                                    return false;
                                }
                            }
                        }
                        var Tasktype_value = $.grep($scope.TaskTypeList, function (rel) {
                            return rel.Id == ($scope.TaskTypeObject.TaskType.id == undefined ? $scope.TaskTypeObject.TaskType : $scope.TaskTypeObject.TaskType.id);
                        })[0];
                        var AssetidforAssetapproval = 0;
                        if (Tasktype_value.TaskTypeId == taskType.Asset_Approval_Task || Tasktype_value.TaskTypeId == taskType.Proof_approval_task || Tasktype_value.TaskTypeId == taskType.dalim_approval_task) {
                            //  if ($scope.DamAssetTaskselectionFiles.AssetSelectionFiles.length == 1 || $scope.Fileids.length == 1) AssetidforAssetapproval = $scope.DamAssetTaskselectionFiles.AssetSelectionFiles.length == 0 ? $scope.Fileids[0] : $scope.DamAssetTaskselectionFiles.AssetSelectionFiles[0];
                             if ($scope.DamAssetTaskselectionFiles.AssetSelectionFiles.length == 1) AssetidforAssetapproval =  $scope.DamAssetTaskselectionFiles.AssetSelectionFiles[0];
                            else {
                                $('#CreateNewTaskBtn').removeClass('disabled');
                                if ($scope.DamAssetTaskselectionFiles.AssetSelectionFiles.length > 1 || $scope.Fileids.length > 1) bootbox.alert($translate.instant('LanguageContents.Res_4918.Caption'));
                                else if ($scope.DamAssetTaskselectionFiles.AssetSelectionFiles.length == 0 && $scope.Fileids.length == 0) {
                                    bootbox.alert($translate.instant('LanguageContents.Res_5756.Caption'));
                                    return false;
                                }
                               // else if ($scope.DamAssetTaskselectionFiles.AssetSelectionFiles.length == 0 || $scope.Fileids.length == 0) bootbox.alert("Please select at least one asset to proceed");                               
                            }
                        }
                        var SaveTask = {};
                        SaveTask.TaskType = Tasktype_value.TaskTypeId;
                        SaveTask.Typeid = parseInt($scope.TaskTypeObject.TaskType.id);
                        SaveTask.Name = $scope.TaskTypeObject.NewTaskName;
                        SaveTask.TaskListID = $scope.TaskTypeObject.AssetTaskList;
                        SaveTask.Description = $scope.TaskTypeObject.TaskDescription;
                        SaveTask.Note = $scope.TaskTypeObject.TaskNote;
                        SaveTask.TaskAttachments = [];
                        SaveTask.TaskAttachments = $scope.AttachmentFilename;
                        SaveTask.TaskFiles = $scope.FileList;
                        SaveTask.ParentEntityID = $stateParams.ID;
                        SaveTask.AssetID = (Tasktype_value.TaskTypeId == taskType.Asset_Approval_Task || Tasktype_value.TaskTypeId == taskType.Proof_approval_task || Tasktype_value.TaskTypeId == taskType.dalim_approval_task) ? AssetidforAssetapproval : 0;
                        SaveTask.DueDate = $scope.TaskTypeObject.TaskDueDate != null ? ConvertDateToString($scope.TaskTypeObject.TaskDueDate) : "";
                        SaveTask.TaskMembers = [];
                        var taskOwnerObj = [];
                        taskOwnerObj = $scope.OwnerList[0];
                        if ($scope.TaskMemberLists == undefined || $scope.TaskMemberLists == "") $scope.TaskMembersList = [];
                        else SaveTask.TaskMembers = $scope.TaskMemberLists;
                        if (SaveTask.TaskMembers.length == 0) {
                            for (var m = 0, ph = {}; ph = $scope.templateData.PhaseData.AllPhaseList[m++];) {
                                for (var n = 0, st = {}; st = ph.StepsData[n++];) {
                                    if (st.MemberList != null) {
                                        for (var k = 0, mem = {}; mem = st.MemberList[k++];) {
                                            $scope.TaskMembersList.push(mem);
                                        }
                                    }
                                }
                            }
                            SaveTask.TaskMembers = $scope.TaskMembersList;
                        }
                        for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                            if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                                for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                    if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "-" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [parseInt($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)])],
                                            "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)]) })[0].Level,
                                            "Value": "-1"
                                        });
                                    }
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                                for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                    var levelCount = $scope.atributesRelationList[i].Levels.length;
                                    if (levelCount == 1) {
                                        if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                            for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                                $scope.AttributeData.push({
                                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                    "NodeID": [parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k])],
                                                    "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]) })[0].Level,
                                                    "Value": "-1"
                                                });
                                            }
                                        }
                                    } else {
                                        if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                            if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                                for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                                    $scope.AttributeData.push({
                                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                        "NodeID": [parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k])],
                                                        "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k]) })[0].Level,
                                                        "Value": "-1"
                                                    });
                                                }
                                            } else {
                                                $scope.AttributeData.push({
                                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                    "NodeID": [parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)])],
                                                    "Level": $.grep($scope.DropDownTreeOptionValues["Options" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)]) })[0].Level,
                                                    "Value": "-1"
                                                });
                                            }
                                        }
                                    }
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                                for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                    var attributeLevelOptions = [];
                                    attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID + ""], function (e) {
                                        return e.level == (j + 1);
                                    }));
                                    if (attributeLevelOptions[0] != undefined) {
                                        if (attributeLevelOptions[0].selection != undefined) {
                                            for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
                                                var valueMatches = [];
                                                if (attributeLevelOptions[0].selection.length > 1) valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                                    return relation.NodeId.toString() === opt;
                                                });
                                                $scope.AttributeData.push({
                                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                    "NodeID": [opt],
                                                    "Level": (j + 1),
                                                    "Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
                                                });
                                            }
                                        }
                                    }
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                                if ($scope.atributesRelationList[i].IsSpecial == true) {
                                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                            "Level": 0,
                                            "Value": "-1"
                                        });
                                    }
                                } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                                    if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                                        var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID];
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                                            "Level": 0,
                                            "Value": "-1"
                                        });
                                    }
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                                if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                                else {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": ($scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID] != null) ? $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                                        "Level": 0,
                                        "Value": "-1"
                                    });
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                                if (($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TakeDescription) && ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskNotes)) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": ($scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID] != null) ? $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                                        "Level": 0,
                                        "Value": "-1"
                                    });
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                                if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                        var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                                        for (var k = 0; k < multiselectiObject.length; k++) {
                                            $scope.AttributeData.push({
                                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                "NodeID": parseInt(multiselectiObject[k], 10),
                                                "Level": 0,
                                                "Value": "-1"
                                            });
                                        }
                                    }
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                                var MyDate = new Date.create();
                                var MyDateString;
                                if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                                    if ($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != undefined) MyDateString = ConvertDateToString($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID]).toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3").toString('dd-MM-yyyy');
                                    else MyDateString = "";
                                } else {
                                    MyDateString = "";
                                }
                                if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskDueDate) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": MyDateString,
                                        "Level": 0,
                                        "Value": "-1"
                                    });
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                                var treenodes = [];
                                treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) {
                                    return e.AttributeId == $scope.atributesRelationList[i].AttributeID;
                                });
                                for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [parseInt(nodeval.id, 10)],
                                        "Level": parseInt(nodeval.Level, 10),
                                        "Value": "-1"
                                    });
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": (($scope.fields['MTextSingleLine_' + $scope.atributesRelationList[i].AttributeID]).toString()).replace(/ /g, ''),
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
                                var MyDateString = "";
                                if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskDueDate) {
                                    if ($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] != undefined) MyDateString = ConvertDateToString($scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID]).toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3").toString('dd-MM-yyyy');
                                    else MyDateString = "";
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": MyDateString,
                                        "Level": 0,
                                        "Value": "-1"
                                    });
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": $scope.ImageFileName,
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 18) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": "",
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                                if ($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                    for (var j = 0; j < $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID].length; j++) {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": parseInt($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID][j], 10),
                                            "Level": 0,
                                            "Value": "-1"
                                        });
                                    }
                                }
                            } else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                                if ($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] == "") {
                                    $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = "0";
                                }
                                $scope.Entityamountcurrencytypeitem.push({
                                    amount: parseFloat($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID].replace(/ /g, '')),
                                    currencytype: $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID],
                                    Attributeid: $scope.atributesRelationList[i].AttributeID
                                });
                            }
                        }
                        SaveTask.AttributeData = $scope.AttributeData;
                        SaveTask.Periods = [];
                        $scope.savesubentityperiods = [];
                        for (var m = 0; m < $scope.items.length; m++) {
                            if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                                if ($scope.items[m].startDate.length != 0 && $scope.items[m].endDate.length != 0) {
                                    $scope.savesubentityperiods.push({
                                        startDate: ($scope.items[m].startDate.length != 0 ? ConvertDateToString($scope.items[m].startDate) : ''),
                                        endDate: ($scope.items[m].endDate.length != 0 ? ConvertDateToString($scope.items[m].endDate) : ''),
                                        comment: ($scope.items[m].comment.trim().length != 0 ? $scope.items[m].comment : ''),
                                        sortorder: 0
                                    });
                                }
                            }
                        }
                        SaveTask.Periods.push($scope.savesubentityperiods);
                        var taskResObj = $.grep($scope.AdminTaskCheckList, function (e) {
                            return e.NAME != "";
                        });
                        SaveTask.AdminTaskCheckList = $scope.AdminTaskCheckList;
                        SaveTask.Entityamountcurrencytype = [];
                        $scope.curram = [];
                        for (var m = 0; m < $scope.Entityamountcurrencytypeitem.length; m++) {
                            $scope.curram.push({
                                amount: $scope.Entityamountcurrencytypeitem[m].amount,
                                currencytype: $scope.Entityamountcurrencytypeitem[m].currencytype,
                                Attributeid: $scope.Entityamountcurrencytypeitem[m].Attributeid
                            });
                        }
                        SaveTask.Entityamountcurrencytype.push($scope.curram);
                        SaveTask.StepMembers = $scope.TaskMemberLists;
                        SaveTask.phaseStepList = $scope.templateData.PhaseData.AllPhaseList;
                        SaveTask.AssetArr = $scope.TaskCreationAttachAsset.DAssetSelectionFiles;
                        SaveTask.AdditionalObjects = {};
                        if ($scope.TaskTypeObject.TaskType.id == taskType.dalim_approval_task) {
                            $scope.DalimtemplateData = $.grep($scope.Templatesdetails, function (e) {
                                return e.TemplateFlow.ID == $scope.myObjtasktemplate.selectedItems
                            });
                            if ($scope.DalimtemplateData.length > 0) {
                                SaveTask.AdditionalObjects.TemplateLibraryID = $scope.DalimtemplateData[0].TemplateFlow.ID;
                                SaveTask.AdditionalObjects.TemplateLibraryName = $scope.DalimtemplateData[0].TemplateFlow.Caption;
                            }
                            SaveTask.AdditionalObjects.AssetArr = $scope.DamAssetTaskselectionFiles.AssetSelectionFiles;
                        } else if ($scope.TaskTypeObject.TaskType.id == taskType.Approval_Task || $scope.TaskTypeObject.TaskType.id == taskType.Asset_Approval_Task || $scope.TaskTypeObject.TaskType.id == taskType.Proof_approval_task) {
                            $scope.ApprtemplateData = $.grep($scope.Templatesdetails, function (e) {
                                return e.ID == $scope.myObjtasktemplate.selectedItems
                            })[0];
                            if ($scope.ApprtemplateData != null && $scope.ApprtemplateData != undefined) {
                                SaveTask.AdditionalObjects.TemplateLibraryID = $scope.ApprtemplateData.ID;
                                SaveTask.AdditionalObjects.TemplateLibraryName = $scope.ApprtemplateData.Name;
                            }
                            SaveTask.AdditionalObjects.AssetArr = $scope.DamAssetTaskselectionFiles.AssetSelectionFiles;
                        }
                        var uploaderNodeID = [];

                        var uploaderArr = $.grep(SaveTask.AttributeData, function (e) {
                            return e.AttributeTypeID == 11
                        })
                        if (uploaderArr.length > 0) {
                            for (var i = 0; i < uploaderArr.length; i++) {
                                uploaderNodeID.push(uploaderArr[i].NodeID);
                            }
                            if (uploaderNodeID.length > 0) {
                                TaskCreationService.savemultipleUploadedImg(uploaderNodeID).then(function (newIDs) {
                                    if (newIDs.Response != null) {
                                        for (var i = 0; i < newIDs.Response.length; i++) {
                                            var UploadAttr = $.grep(SaveTask.AttributeData, function (e) {
                                                return e.NodeID == newIDs.Response[i].m_Item1;
                                            })[0];
                                            var AttrIndex = SaveTask.AttributeData.indexOf(UploadAttr);
                                            SaveTask.AttributeData[AttrIndex].NodeID = newIDs.Response[i].m_Item2;
                                        }
                                        InsertEntityTaskWithAttachments(SaveTask);
                                    }
                                });
                            }
                        }
                        else {
                            InsertEntityTaskWithAttachments(SaveTask);
                        }


                    } else {
                        $('#CreateNewTaskBtn').removeClass('disabled');
                        bootbox.alert(alertText);
                        alertcount = 0;
                        $scope.uploadingInProgress = false;
                        return false;
                    }
                    var Tasktype_value = $.grep($scope.TaskTypeList, function (rel) {
                        return rel.Id == ($scope.TaskTypeObject.TaskType.id == undefined ? $scope.TaskType : $scope.TaskTypeObject.TaskType.id);
                    })[0];
                }            
        }

        function InsertEntityTaskWithAttachments(SaveTask) {
            TaskCreationService.InsertEntityTaskWithAttachments(SaveTask).then(function (SaveTaskResult) {
                if (SaveTaskResult.Response == null || SaveTaskResult.Response == 0) {
                    $scope.DamAssetTaskselectionFiles.AssetSelectionFiles = [];
                    $('#CreateNewTaskBtn').removeClass('disabled');
                    NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
                    $("#taskcreating").hide();
                    closecreatetaskpopup();
                }
                else {
                    var returnObj = {};
                    if (SaveTaskResult.Response != null && SaveTaskResult.Response.m_Item1 != 0) {
                        returnObj = SaveTaskResult.Response.m_Item2;
                        $scope.newTaskid = SaveTaskResult.Response.m_Item1;
                        $scope.SelectedTaskID = $scope.newTaskid;
                        $scope.returnObj = returnObj;
                        var attchasset = {};


                        attchasset.AssetArr = $scope.DamAssetTaskselectionFiles.AssetSelectionFiles;
                        attchasset.EntityID = $scope.newTaskid;
                        attchasset.FolerId = 0;
                        if ($scope.AssetTaskTypeObj.AssetTaskType == 36 || $scope.AssetTaskTypeObj.AssetTaskType == 37) timerObj.createprooftask = $timeout(function () {
                            CreateProofTask($scope.newAssetTaskid, assetArr, $scope.AssetTaskTypeObj.AssetTaskType);
                        }, 20);
                        if ($scope.showAttach == "Task") {
                            $scope.LiveTaskListIDCollection.push({
                                "TaskLiStID": $scope.TaskTypeObject.AssetTaskList,
                                "EntityID": $stateParams.ID
                            });
                            $scope.$emit('LiveTaskListUpdate', $scope.LiveTaskListIDCollection);
                        }
                        if ($scope.showAttach == "Dam") {
                            timerObj.forDam = $timeout(function () {
                                $scope.$emit('RefreshAssetSelectionforTask', $scope.TaskTypeObject.AssetTaskList);
                                $scope.TaskTypeObject.AssetTaskList = 0;
                                refreshModel();
                            }, 10);
                        }
                        ClearAllScopeVariables();
                        if ($("#assetfromattachment  > div").length > 0) {
                            if ($scope.TaskCreationAttachAsset.DAssetSelectionFiles.length > 0) {
                                AttachAssets($scope.returnObj);
                            }
                        }
                        if ($("#assetassettaskentityattachassetdata > div").length == 0) {
                            $('#CreateNewTaskBtn').removeClass('disabled');
                            NotifySuccess($translate.instant('LanguageContents.Res_4820.Caption'));
                            if ($scope.TaskActionFor == 1) RereshTaskObj(returnObj);
                            else RereshTaskObj(returnObj);

                            closecreatetaskpopup();
                        } else if ($("#assetassettaskentityattachassetdata  > div").length > 0) {
                            if ($scope.TaskCreationFromPlace == "addtaskfromasset") {
                                AttachAssets(returnObj);
                            } else {
                                $scope.uploadingInProgress = true;
                                setTimeout($("#tempTaskCreation").click(), 100);
                                $('#CreateNewTaskBtn').removeClass('disabled');
                            }
                        }
                    } else {
                        $('#CreateNewTaskBtn').removeClass('disabled');
                        NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
                        $scope.uploadingInProgress = false;
                    }


                    $scope.FileList = [];
                    $scope.SelectedTaskLIstID = 0;
                    $scope.AttachmentFilename = [];

                    $scope.returnObj = [];
                    $scope.DamAssetTaskselectionFiles.AssetSelectionFiles = [];
                    $scope.addtaskfromGenAttachments = false;
                    $scope.$emit('LoadTaskList');
                    $scope.$emit('muiRefreshTaskByID', $scope.newTaskid);// Reload Task
                    $scope.$emit('muiRefreshTasklistByID', returnObj);// Reload Tasklist

                    //closecreatetaskpopup();
                }

            });

        }


        function AttachAssets(returnObj) {
            var asset = ($scope.TaskCreationAttachAsset.DAssetSelectionFiles.length > 0) ? $scope.TaskCreationAttachAsset.DAssetSelectionFiles : $scope.DamAssetTaskselectionFiles.AssetSelectionFiles;
            TaskCreationService.AttachAssets(asset, $scope.newTaskid, 0).then(function (result) {
                if (result.Response == 0) {
                    $('#CreateNewTaskBtn').removeClass('disabled');
                    NotifyError($translate.instant('LanguageContents.Res_4327.Caption'));
                } else {

                    $scope.TaskCreationAttachAsset.DSelectedAssetFiles = [];
                    $scope.TaskCreationAttachAsset.Dhtml = '';
                    GetEntityTaskAttachmentinfo($scope.newTaskid);
                    if ($("#assetlistActivityTask > div").length == 0) {
                        $('#CreateNewTaskBtn').removeClass('disabled');
                        $("#taskcreating").hide();
                        if ($scope.TaskActionFor == 1) RereshTaskObj(returnObj);
                        else RereshTaskObj(returnObj);
                        $scope.FileList = [];
                        $scope.SelectedTaskLIstID = 0;
                        $scope.AttachmentFilename = [];
                        $scope.TaskCreationAttachAsset.DAssetDynamicData = [];
                        $scope.TaskCreationAttachAsset.DAssetFiles = [];
                        $scope.TaskCreationAttachAsset.DAssetSelectionFiles = [];
                        $scope.DamAssetTaskselectionFiles.AssetFiles = [];
                        $scope.DamAssetTaskselectionFiles.ThumbnailSettings = [];
                        $scope.DamAssetTaskselectionFiles.AssetDynamicData = [];
                        $scope.DamAssetTaskselectionFiles.AssetSelectionFiles = [];
                        refreshModel();
                        $scope.newTaskid = 0;
                        $scope.returnObj = [];
                        $scope.addfromGenAttachments = false;
                        $scope.TaskCreationAttachAsset.Dhtml = '';
                        $('#addTask').modal('hide');
                    } else if ($("#assetlistActivityTask  > div").length > 0) {
                        $scope.uploadingInProgress = true;
                        setTimeout($("#tempTaskCreation").click(), 100);
                    }
                    NotifySuccess($translate.instant('LanguageContents.Res_4820.Caption'));
                    closecreatetaskpopup();
                }
            });
        }
        $scope.OpenWorkTask = function (tasktype, tasklist) {
            $scope.AttachmentFilename = [];
            $scope.FileList = [];
            $scope.$broadcast('Cleasetaskpopup', "Task", tasktype, tasklist, 0);
        }
        $scope.LoadTreeData = function () {
            $scope.$broadcast('OpenTree', "Task", 2);
        }
        $scope.AssetId = 0;
        $scope.$on("HandOverDamFiles", function (event, files, assetid) {
            $scope.AssetId = 0;
            $scope.AssetId = assetid;
            if (files != null) if (files.length > 0) for (var i = 0, response; response = files[i++];) {
                $scope.AttachmentFilename.push({
                    "FileName": response.Name,
                    "Extension": response.Extension,
                    "Size": parseSize(response.Size),
                    "FileGuid": response.Fileguid,
                    "FileDescription": response.Description,
                    "URL": '',
                    "LinkType": "",
                    "ID": 0,
                    "LinkID": 0
                });
                $scope.FileList.push({
                    "Name": response.Name,
                    "VersionNo": response.VersionNo,
                    "MimeType": response.MimeType,
                    "Extension": response.Extension,
                    "OwnerID": $cookies['UserId'],
                    "CreatedOn": Date.now(),
                    "Checksum": "",
                    "ModuleID": 1,
                    "EntityID": $stateParams.ID,
                    "Size": response.Size,
                    "FileGuid": response.Fileguid,
                    "FileDescription": response.Description,
                    "IsExists": true
                });
            }
        });
        $scope.UploaderCaptionObj = [];
        $scope.uploadingInProgress = false;
        $scope.setUploaderCaption = function () {
            var keys1 = [];
            angular.forEach($scope.UploderCaption, function (key) {
                keys1.push(key);
                $scope.UploaderCaptionObj = keys1;
            });
        }
        timerObj.startuploader = $timeout(function () {
            $("#totalAssetActivityAttachProgress_Version").empty();
            $("#totalAssetActivityAttachProgress_Version").append('<div class="progress progress-striped active">' + '<div style="width: 0%" class="bar"></div>' + '</div>');
            StrartUpload_Version();
        }, 50);
        var uploader_V = '';

        function StrartUpload_Version() {
            if (uploader_V != '') {
                uploader_V.destroy();
            }
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {

                uploader_V = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfiles_V',
                    container: 'filescontainer_V',
                    max_file_size: '10mb',
                    url: amazonURL + cloudsetup.BucketName,
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    urlstream_upload: true,
                    multiple_queues: true,
                    file_data_name: 'file',
                    multipart: true,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    },
                });
                uploader_V.bind('Init', function (up, params) { });
                uploader_V.init();
                $('#pickfiles_V').each(function () {
                    var input = new mOxie.FileInput({
                        browse_button: this,
                        multiple: false
                    });
                    input.onchange = function (event) {
                        uploader_V.addFile(input.files);
                    };
                    input.init();
                });
                uploader_V.bind('FilesAdded', function (up, files) {
                    TotalUploadProgress_V(files[0]);
                    up.refresh();
                    uploader_V.start();
                    $('#totalAssetActivityAttachProgress_Version').show();
                    $('#totalAssetActivityAttachProgress_Version .bar').css('width', '0');
                });
                uploader_V.bind('UploadProgress', function (up, file) {
                    $('#totalAssetActivityAttachProgress_Version .bar').css("width", file.percent + "%");
                    TotalUploadProgress_V(file);
                });
                uploader_V.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_V.bind('FileUploaded', function (up, file, response) {
                    SaveFileDetails_V(file, response.response);
                    TotalUploadProgress_V(file);
                });
                uploader_V.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKeypath = TenantFilePath.replace(/\\/g, "\/");
                    var uniqueKey = (uniqueKeypath + "DAMFiles/Original/" + file.id + fileext);
                    uploader.settings.multipart_params.key = uniqueKey;
                    uploader.settings.multipart_params.Filename = uniqueKey;
                });
                uploader_V.bind('FileUploaded', function (up, file, response) {
                    var obj = response;
                });
            } else {
                uploader_V = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfiles_V',
                    container: 'filescontainer_V',
                    max_file_size: '10mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: 'Handlers/DamFileHandler.ashx?Type=Attachment',
                    multi_selection: false,
                    multipart_params: {}
                });
                uploader_V.bind('Init', function (up, params) { });
                uploader_V.init();
                $('#pickfiles_V').each(function () {
                    var input = new mOxie.FileInput({
                        browse_button: this,
                        multiple: false
                    });
                    input.onchange = function (event) {
                        uploader_V.addFile(input.files);
                    };
                    input.init();
                });
                uploader_V.bind('FilesAdded', function (up, files) {
                    TotalUploadProgress_V(files[0]);
                    up.refresh();
                    uploader_V.start();
                    $('#totalAssetActivityAttachProgress_Version').show();
                    $('#totalAssetActivityAttachProgress_Version .bar').css('width', '0');
                });
                uploader_V.bind('UploadProgress', function (up, file) {
                    $('#totalAssetActivityAttachProgress_Version .bar').css("width", file.percent + "%");
                    TotalUploadProgress_V(file);
                });
                uploader_V.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_V.bind('FileUploaded', function (up, file, response) {
                    SaveFileDetails_V(file, response.response);
                    TotalUploadProgress_V(file);
                });
                uploader_V.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
                uploader_V.bind('FileUploaded', function (up, file, response) {
                    var obj = response;
                });
            }

            function TotalUploadProgress_V(file) {
                $('#totalAssetActivityAttachProgress_Version .bar').css("width", file.percent + "%");
            };
        }

        function SaveFileDetails_V(file, response) {
            $scope.SaveFiletoAsset = [];
            var extension = file.name.substring(file.name.lastIndexOf("."));
            var resultArr = response.split(",");
            $scope.SaveFiletoAsset.push({
                "AssetID": $scope.AssetId,
                "Status": 0,
                "MimeType": resultArr[1],
                "Size": file.size,
                "FileGuid": resultArr[0],
                "CreatedOn": Date.now(),
                "Extension": extension,
                "Name": file.name,
                "VersionNo": 0,
                "Description": file.name,
                "OwnerID": $cookies['UserId']
            });
            TaskCreationService.SaveFiletoAsset($scope.SaveFiletoAsset).then(function (result) {
                if (result.Response != null && result.Response != undefined) {
                    $('#totalAssetActivityAttachProgress_Version').hide();
                    NotifySuccess($translate.instant('LanguageContents.Res_4786.Caption'));
                    ReloadTaskAttachments($scope.TaskBriefDetails.taskID);
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4266.Caption'));
                }
            });
        }
        $scope.CloseDampopup = function () {
            if ($scope.AssetfileTemplateLoading != null) {
                $scope.SetCurrentTabid("task-MetaData-" + $scope.AssetfileTemplateLoading, "task-Attachments-" + $scope.AssetfileTemplateLoading)
                $("#task-Attachments-" + $scope.AssetfileTemplateLoading + "dam").empty();
                $("#task-Attachments-" + $scope.AssetfileTemplateLoading + "dam").unbind();
                $scope.AssetfileTemplateLoading = "Null";
            }
            $scope.fromunassignedtask = false;
        };
        $scope.DAMentitytypes = [];

        function TooltipattachmentName(description) {
            if (description != null & description != "") {
                return description;
            } else {
                return "<label class=\"align-center\">No description</label>";
            }
        };
        $scope.popMe = function (e) {
            var TargetControl = $(e.target);
            var mypage = TargetControl.attr('data-Name');
            if (!(mypage.indexOf("http") == 0)) {
                mypage = "http://" + mypage;
            }
            var myname = TargetControl.attr('data-Name');
            var w = 1200;
            var h = 800
            var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
            var win = window.open(mypage, myname, winprops)
        };
        $scope.set_bgcolor = function (clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            };
            else return '';
        }
        $scope.set_color = function (clr) {
            if (clr != null) return {
                'color': "#" + clr.toString().trim()
            };
            else return '';
        }
        $scope.dyn_ContAsset = '';
        $scope.DAMtypeswithExt = [];
        $scope.DamExt = [];
        $scope.Fileids = [];

        function GetAllExtensionTypesforDAM() {
            if ($scope.damExtensionObj != null && $scope.damExtensionObj != undefined) {
                $scope.DAMtypeswithExt = $scope.damExtensionObj.DAMtypeswithExt;
                $scope.AssetTasktypeswithExt = $scope.damExtensionObj.AssetTasktypeswithExt;
            }
        }
        var filecount = 0;
        var totalfilecount = 0;
        timerObj.assetcreationntask = $timeout(function () {
            $scope.fnTimeOutfrAssetCreationTask();
        }, 50);
        $scope.fnTimeOutfrAssetCreationTask = function () {
            if ($scope.processingsrcobj.processingplace == "task") {
                $('#pickassetinTaskCreation').removeAttr('disabled', 'disabled');
                $('#assetassettaskentityattachassetdata').empty();
                $("#totalAssetActivityAttachProgresstask").empty();
                $('#taskuploaded').empty();
                $("#totalAssetActivityAttachProgresstask").append('<span class="pull-left count">0 of 0 Uploaded</span><span class="size">0 B / 0 B</span>' + '<div class="progress progress-striped active">' + '<div style="width: 0%" class="bar"></div>' + '</div>' + '<div id="taskuploaded" style="position: absolute; top: 42%; right: -30%;"></div>');
                $scope.StartAssetUploadTask();
            } else { }
        };

        function applyRemoteData(newFriends) {
            var friends = newFriends;
        }
        var TotalCount = 0;
        var saveBlankAssetsCalled = false;
        var uploaderAssetforTaskcreation = "";
        $scope.StartAssetUploadTask = function () {
            $('.moxie-shim').remove();
            if (uploaderAssetforTaskcreation != '') {
                uploaderAssetforTaskcreation.destroy();
            }
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                uploaderAssetforTaskcreation = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickassetinTaskCreation',
                    container: 'assetscontainerTaskCreation',
                    max_file_size: '10000mb',
                    url: amazonURL + cloudsetup.BucketName,
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    urlstream_upload: true,
                    multiple_queues: true,
                    file_data_name: 'file',
                    multipart: true,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    },
                });
                uploaderAssetforTaskcreation.bind('Init', function (up, params) {
                    uploaderAssetforTaskcreation.splice();
                });
                uploaderAssetforTaskcreation.init();
                var dom = {
                    uploaderAssetforTaskcreation: $("#uploaderAssetforTaskcreation"),
                    uploads: $("ul.uploads")
                };
                $("#tempTaskCreation").click(function (e) {
                    if ($("#assetassettaskentityattachassetdata > div").length > 0 && $scope.addtaskvallid > 0) {
                        var str = $('#taskuploaded').text().replace('Upload Successful.', '');
                        $('#taskuploaded').text(str);
                        if ($scope.UniqueidsfrBlankAsset.length + $scope.Fileids.length > 0 && $("#assetassettaskentityattachassetdata > div").length > 0) {
                            var count = 0;
                            var namecount = 0;
                            angular.forEach($scope.UniqueidsfrBlankAsset, function (key) {
                                if ($scope.assetfields['assettype_' + key] == '' || $scope.assetfields['assettype_' + key] == null || $scope.assetfields['assettype_' + key] == undefined) {
                                    count = count + 1;
                                }
                                if ($scope.assetfields['assetname_' + key] == '' || $scope.assetfields['assetname_' + key] == null || $scope.assetfields['assetname_' + key] == undefined) {
                                    namecount = namecount + 1;
                                }
                            });
                            angular.forEach($scope.Fileids, function (key) {
                                if ($scope.assetfields['assettype_' + key] == '' || $scope.assetfields['assettype_' + key] == null || $scope.assetfields['assettype_' + key] == undefined) {
                                    count = count + 1;
                                }
                                if ($scope.assetfields['assetname_' + key] == '' || $scope.assetfields['assetname_' + key] == null || $scope.assetfields['assetname_' + key] == undefined) {
                                    namecount = namecount + 1;
                                }
                            });
                            if (count == 0 && namecount == 0) {
                                $('#CreateNewTaskBtn').attr('disabled', 'disabled');
                                $('#CreateNewTaskBtn').css('pointer-events', 'none');
                                $('#pickassetinTaskCreation').attr('disabled', 'disabled');
                                $('#pickassetinTaskCreation').css('pointer-events', 'none');
                                if ($scope.UniqueidsfrBlankAsset.length > 0) {
                                    if (saveBlankAssetsCalled == false) {
                                        saveBlankAssetsCalled = true;
                                        saveBlankAssets();
                                    }
                                }
                                $scope.addfromComputer = true;
                                uploader.start();
                                e.preventDefault();
                            } else {
                                $('#CreateNewTaskBtn').removeClass('disabled');
                                bootbox.alert($translate.instant('LanguageContents.Res_4594.Caption'));
                                $scope.uploadingInProgress = false;
                                return false;
                            }
                        } else {
                            if ($scope.TaskCreationAttachAsset.DAssetSelectionFiles.length > 0) {
                                AttachAssets($scope.returnObj);
                            } else {
                                bootbox.alert($translate.instant('LanguageContents.Res_4563.Caption'));
                                $scope.uploadingInProgress = false;
                                return false;
                            }
                        }
                    }
                });
                uploaderAssetforTaskcreation.bind('FilesAdded', function (up, files) {
                    $scope.IsaddAssetsfromComputer = true;
                    $scope.addfromComputer = true;
                    totalfilecount = totalfilecount + files.length;
                    $.each(files, function (i, file) {
                        var damid = 0;
                        $scope.Fileids.push(file.id);
                        var ste = file.name.split('.')[file.name.split('.').length - 1];
                        if (ste != undefined) ste = ste.toLowerCase();
                        var damCount = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                            return e.damExtention == ste
                        }).length;
                        if (damCount > 0) {
                            var selecteddamtype = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                                return e.damExtention == ste
                            });
                        } else {
                            var wildcardExt = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                                return e.damExtention.indexOf("*") != -1
                            });
                            var iswildcard = [];
                            iswildcard = $.grep(wildcardExt, function (e) {
                                return e.damExtention.substring(0, e.damExtention.length - 1).indexOf(ste) != -1
                            });
                            if (iswildcard.length > 0) var selecteddamtype = iswildcard;
                        }
                        $scope.AllDamTypes = [];
                        $scope.assetfields["tagAllOptions_" + file.id] = {
                            multiple: false,
                            allowClear: true,
                            data: $scope.AssetCategory["AssetTypesList_" + file.id],
                            formatResult: $scope.formatResult,
                            formatSelection: $scope.formatSelection,
                            dropdownCssClass: "bigdrop",
                            escapeMarkup: function (m) {
                                return m;
                            }
                        };
                        if (selecteddamtype != undefined && selecteddamtype != null) {
                            damid = selecteddamtype[0].Id;
                            var ownerID = parseInt($scope.OwnerID, 10);
                            var tempval = [];
                            var tempassty = "";
                            for (var k = 0; k < selecteddamtype.length; k++) {
                                tempassty = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                                    return e.id == selecteddamtype[k].Id
                                });
                                if (tempassty != undefined && tempassty.length > 0) var tempattr = $.grep(tempval, function (e) {
                                    return e.id == tempassty[0].id
                                });
                                if (tempattr.length == 0) tempval.push(tempassty[0]);
                            }
                            if (tempval.length > 0) {
                                $scope.AssetCategory["AssetTypesList_" + file.id] = tempval;
                                $scope.assetfields["tagAllOptions_" + file.id].data = $scope.AssetCategory["AssetTypesList_" + file.id];
                            } else { }
                        }
                        TaskCreationService.GetAssetCategoryTree(damid).then(function (data) {
                            if (data.Response != null) {
                                $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                $scope.AssetCategory["filterval_" + file.id] = '';
                                var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                            }
                        });
                        $scope.AssetCategory["treestructure_" + file.id] = '-';
                        if ($scope.AssetCategory["AssetTypesList_" + file.id] != undefined) if ($scope.AssetCategory["AssetTypesList_" + file.id].length > 1 || $scope.AssetCategory["AssetTypesList_" + file.id].length == 0) damid = 0;
                        TaskCreationService.GetDamAttributeRelation(damid).then(function (attrreldata) {
                            $scope.dyn_ContAsset = "";
                            if (attrreldata.Response != undefined && attrreldata.Response != null) {
                                $scope.dyn_ContAsset = "";
                                $scope.DAMattributesRelationList = attrreldata.Response;
                                FileAttributes.AssetType.push(damid);                            
                                for (var i = 0; i < $scope.DAMattributesRelationList.length; i++) {
                                    if ($scope.DAMattributesRelationList[i].AttributeTypeID == 1) {
                                        if ($scope.DAMattributesRelationList[i].ID != 70) {
                                            if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.Name) {
                                                $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                                $scope.assetfields["TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                            } else {
                                                $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                                $scope.assetfields["TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                            }
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 3) {
                                        if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.Owner) {
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" dirownernameautopopulate placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.OwnerList[0].UserName;
                                        } else if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.FiscalYear) {
                                            $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"> <option value=\"\"> Select " + $scope.DAMattributesRelationList[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                        } else {
                                            $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"> <option value=\"\"> Select " + $scope.DAMattributesRelationList[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 2) {
                                        $scope.dyn_ContAsset += "<div class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"assetfields.TextMultiLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" name=\"assetfields.TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-model=\"assetfields.TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\" rows=\"3\"></textarea></div></div>";
                                        $scope.assetfields["TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 4) {
                                        $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = [];
                                        $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                        var defaultmultiselectvalue = $scope.DAMattributesRelationList[i].DefaultValue.split(',');
                                        if ($scope.DAMattributesRelationList[i].DefaultValue != "") {
                                            for (var v = 0, val; val = defaultmultiselectvalue[v++];) {
                                                $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID].push(parseInt(val));
                                            }
                                        } else {
                                            $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = [];
                                        }
                                        $scope.dyn_ContAsset += "<div class=\"control-group attachmentMultiselect\"><label class=\"control-label\" for=\"assetfields.ListMultiSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select class=\"multiselect\" multiple=\"multiple\"  multiselect-dropdown data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  > </select></div></div>";
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 5 && $scope.DAMattributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.DAMattributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                                        $scope.calanderopened = false;
                                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                        $scope.MaxValue = $scope.DAMattributesRelationList[i].MaxValue;
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                        } else {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                        } else {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + 100000);
                                        }
                                        var temp = $scope.HolidayListCalculation($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID], $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID]);
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = (temp.MinDate);
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = (temp.MaxDate);
                                        $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + "</label><div class=\"controls\"><span class='editable'>{{assetfields.DateTime_" + $scope.DAMattributesRelationList[i].ID + "}}</span><input type=\"text\" class=\"txtbx DatePartctrl\" " + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-click=\"Calanderopen($event,'Date_" + $scope.atributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"assetfields.DatePart_Calander_Open" + file.id + $scope.DAMattributesRelationList[i].ID + "\" min-date=\"assetfields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\" ng-change=\"setTimeout(changeduedate_changed(assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + ",assetfields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",assetfields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "," + $scope.DAMattributesRelationList[i].ID + "),1000)\" ng-model=\"assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"></div></div>";
                                        $scope.assetfields["assetfields.DatePart_Calander_Open" + file.id + $scope.DAMattributesRelationList[i].ID] = false;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 16) {
                                        $scope.calanderopened = false;
                                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                        $scope.MaxValue = $scope.DAMattributesRelationList[i].MaxValue;
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                        } else {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                        } else {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + 100000);
                                        }
                                        $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + "</label><div class=\"controls\"><span class='editable'>{{assetfields.DateTime_" + $scope.DAMattributesRelationList[i].ID + "}}</span><input type=\"text\" class=\"txtbx DatePartctrl\" " + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-click=\"Calanderopen($event,'Date_" + $scope.atributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"assetfields.DatePart_Calander_Open" + file.id + $scope.DAMattributesRelationList[i].ID + "\" min-date=\"assetfields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\" ng-model=\"assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"></div></div>";
                                        $scope.assetfields["assetfields.DatePart_Calander_Open" + file.id + $scope.DAMattributesRelationList[i].ID] = false;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 12) {
                                        var totLevelCnt1 = $scope.DAMattributesRelationList[i].Levels.length;
                                        $scope.dyn_Cont += "<div class=\"control-group pull-left\">";
                                        for (var j = 0; j < totLevelCnt1; j++) {
                                            if (totLevelCnt1 == 1) {
                                                $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                                    return item.Caption
                                                };
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                                    return item.Caption
                                                };
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                                $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12,'" + file.id + "' )\" ng-model=\"assetfields.MultiSelectDropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                                $scope.setFieldKeys();
                                            } else {
                                                $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                                    return item.Caption
                                                };
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                                    return item.Caption
                                                };
                                                if (j == 0) {
                                                    $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                                    $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                                    $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                                    $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12,'" + file.id + "' )\" ng-model=\"assetfields.MultiSelectDropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                                } else {
                                                    $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                                    if (j == ($scope.DAMattributesRelationList[i].Levels.length - 1)) {
                                                        $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                                        $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                        $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 ,'" + file.id + "')\" ng-model=\"assetfields.MultiSelectDropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                                    } else {
                                                        $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                                        $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                        $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12,'" + file.id + "' )\" ng-model=\"assetfields.MultiSelectDropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                                    }
                                                }
                                            }
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 10) {
                                        $scope.items = [];
                                        $scope.MinValue = $scope.DAMatributesRelationList[i].MinValue;
                                        $scope.MaxValue = $scope.DAMattributesRelationList[i].MaxValue;
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                        } else {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                        } else {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + 100000);
                                        }
                                        var temp = $scope.HolidayListCalculation($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID], $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID]);
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = (temp.MinDate);
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = (temp.MaxDate);
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                        $scope.OptionObj["option_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].Options;
                                        $scope.items.push({
                                            startDate: [],
                                            endDate: [],
                                            comment: '',
                                            sortorder: 0,
                                            fileid: 0
                                        });
                                        $scope.assetfields["Period_" + file.id] = [];
                                        $scope.assetfields["Period_" + file.id].push({
                                            startDate: [],
                                            endDate: [],
                                            comment: '',
                                            sortorder: 0,
                                            fileid: file.id
                                        });
                                        $scope.dyn_ContAsset += "    <div class=\"control-group\"><label for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.DAMattributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in assetfields.Period_" + file.id + " \" ng-form=\"subForm\">";
                                        $scope.dyn_ContAsset += "<div class=\"row-fluid\"><div class=\"inputHolder span5 noBorder\">";
                                        $scope.dyn_ContAsset += "<input class=\"sdate margin-bottom0x Period_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-change=\"changeperioddate_changed(item.startDate,assetfields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",assetfields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + ",'StartDate')\" ng-model=\"item.startDate\" ng-click=\"Calanderopen($event,'Date_" + $scope.atributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Date_" + $scope.atributesRelationList[i].AttributeID + "\" min-date=\"assetfields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  placeholder=\"-- Start date --\"/><input class=\"edate margin-bottom0x Period_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" type=\"text\" name=\"enddate\" id=\"item.endDate\" ng-change=\"changeperioddate_changed(item.endDate,fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + ",'EndDate')\" ng-model=\"item.endDate\" ng-click=\"Calanderopen($event,'Start_Date_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Start_Date_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" min-date=\"assetfields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- End date --\"/><input class=\"dateComment txtbx\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + $scope.DAMattributesRelationList[i].Caption + " Comment --\" />";
                                        $scope.dyn_ContAsset += "</div><div class=\"buttonHolder span1\" ng-show=\"$first==false\">";
                                        $scope.dyn_ContAsset += "<a ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew('" + file.id + "')\">[Add " + $scope.DAMattributesRelationList[i].Caption + "]</a></div></div>";
                                        $scope.assetfields["Period_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = "";
                                        $scope.assetfields["assetfields.DatePart_Calander_Open" + "item.startDate"] = false;
                                        $scope.assetfields["assetfields.DatePart_Calander_Open" + "item.endDate"] = false;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 13) {
                                        $scope.PercentageVisibleSettings["AttributeId_Levels_" + file.id + $scope.DAMattributesRelationList[i].AttributeID.toString() + ""] = true;
                                        $scope.DropDownTreePricing["AttributeId_Levels_" + file.id + $scope.DAMattributesRelationList[i].AttributeID.toString() + ""] = $scope.DAMattributesRelationList[i].DropDownPricing;
                                        $scope.dyn_ContAsset += "<div drowdowntreepercentagemultiselectionasset data-purpose='entity' data-fileid=" + file.id.toString() + " data-attributeid=" + $scope.DAMattributesRelationList[i].AttributeID.toString() + "></div>";
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 8) {
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                        $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-model=\"assetfields.TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                                        $scope.assetfields["TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 17) {
                                        $scope.assetfields["ListTagwords_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = [];
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                        $scope.OptionObj["tagoption_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = [];
                                        $scope.tempscope = [];
                                        $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" class=\"control-group\">";
                                        $scope.dyn_ContAsset += "<label class=\"control-label\" for=\"assetfields.ListTagwords_ " + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].AttributeCaption + " </label>";
                                        $scope.dyn_ContAsset += "<div class=\"controls\">";
                                        $scope.dyn_ContAsset += "<directive-tagwords item-attrid = \"" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"assetfields.ListTagwords_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                                        $scope.dyn_ContAsset += "</div></div>";
                                        if ($scope.DAMattributesRelationList[i].InheritFromParent) { } else { }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 6) {
                                        var totLevelCnt = $scope.DAMattributesRelationList[i].Levels.length;
                                        for (var j = 0; j < $scope.DAMattributesRelationList[i].Levels.length; j++) {
                                            $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                            $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                            $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                            $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                                return item.Caption
                                            };
                                            $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                                return item.Caption
                                            };
                                            if (j == 0) {
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                                $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                                $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6,'" + file.id + "' )\"  ng-model=\"assetfields.DropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                            } else {
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                                $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 ,'" + file.id + "')\"  ng-model=\"assetfields.DropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                            }
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 7) {
                                        $scope.assetfields["Tree_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = [];
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                        $scope.treesrcdirec["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                        if ($scope.treesrcdirec["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID].length > 0) {
                                            treeTextVisbileflag = false;
                                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID])) {
                                                $scope.treePreviewObj["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                            } else $scope.treePreviewObj["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = false;
                                        } else {
                                            $scope.treePreviewObj["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = false;
                                        }
                                        $scope.dyn_ContAsset += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '\" class="control-group treeNode-control-group">';
                                        $scope.dyn_ContAsset += '    <label class="control-label">' + $scope.DAMattributesRelationList[i].AttributeCaption + '</label>';
                                        $scope.dyn_ContAsset += '    <div class="controls treeNode-controls">';
                                        $scope.dyn_ContAsset += '        <div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '"></div>';
                                        $scope.dyn_ContAsset += '        <div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '">';
                                        $scope.dyn_ContAsset += '            <span ng-if="doing_async">...loading...</span>';
                                        $scope.dyn_ContAsset += '            <abn-tree tree-filter="filterValue_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '\" accessable="' + $scope.DAMattributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                                        $scope.dyn_ContAsset += '        </div>';
                                        $scope.dyn_ContAsset += '        <div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '\">';
                                        $scope.dyn_ContAsset += '            <div class="controls">';
                                        $scope.dyn_ContAsset += '                <eu-tree tree-data=\"treesrcdirec.Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.DAMattributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                        $scope.dyn_ContAsset += '        </div></div>';
                                        $scope.dyn_ContAsset += '</div></div>';
                                    }
                                }
                            }
                            $scope.assetfields["assetname_" + file.id] = file.name;
                            if (selecteddamtype != undefined && selecteddamtype != null) {
                                var dataid = '';
                                var rec = [];
                                rec = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                                    return e.Id == selecteddamtype[0].Id
                                });
                                if (rec.length > 0) dataid = rec[0].Id;
                                if ($scope.assetfields["tagAllOptions_" + file.id].data.length == 1) $scope.assetfields["assettype_" + file.id] = rec[0];
                                if ($scope.AssetCategory["AssetTypesList_" + file.id].length == 1) {
                                    TaskCreationService.GetAssetCategoryTree(dataid).then(function (data) {
                                        if (data.Response != null) {
                                            $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                            $scope.AssetCategory["filterval_" + file.id] = '';
                                            var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                            $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                                        }
                                    });
                                    if ($scope.assetfields["tagAllOptions_" + file.id].data.length == 1) {
                                        TaskCreationService.GetAssetCategoryPathInAssetEdit(dataid).then(function (datapath) {
                                            if (datapath.Response != null) {
                                                if (datapath.Response[1].AssetTypeIDs.length > 0) {
                                                    $scope.AssetCategory["treestructure_" + file.id] = datapath.Response[0].CategoryPath;
                                                } else $scope.assetfields["assettype_" + file.id] = '';
                                            }
                                        });
                                    } else $scope.assetfields["assettype_" + file.id] = '';
                                } else {
                                    $scope.assetfields["assettype_" + file.id] = rec[0];
                                    $scope.oldfiledetails["AssetType_" + file.id] = '';
                                    TaskCreationService.GetAssetCategoryTree(0).then(function (data) {
                                        if (data.Response != null) {
                                            $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                            $scope.AssetCategory["filterval_" + file.id] = '';
                                            var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                            $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                                        }
                                    });
                                }
                            } else {
                                $scope.assetfields["tagAllOptions_" + file.id] = {
                                    multiple: false,
                                    allowClear: true,
                                    data: [],
                                    formatResult: $scope.formatResult,
                                    formatSelection: $scope.formatSelection,
                                    dropdownCssClass: "bigdrop",
                                    escapeMarkup: function (m) {
                                        return m;
                                    }
                                };
                                $scope.oldfiledetails["AssetType_" + file.id] = [];
                                TaskCreationService.GetAssetCategoryTree(0).then(function (data) {
                                    if (data.Response != null) {
                                        $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                        $scope.AssetCategory["filterval_" + file.id] = '';
                                        var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                        $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                                    }
                                });
                            }
                            $scope.IsAttachmentPresent = true;
                            $('#assetassettaskentityattachassetdata').append($compile('<div id="' + file.id + '" class="attachmentBox" Data-role="Attachment" data-size="' + file.size + '" data-size-uploaded="0" data-status="false">' + '<div class="row-fluid">' + '<div class="span12">' + '<div class="span2 leftSection">' + ' <i id="icon_ok' + file.id + '" class="icon-ok-sign" style="display:none;"  ></i>' + ' <div class="fileType-Preview">' + ' <div id="Preview_' + file.id + '" class="fileType-PreviewDiv">' + ' </div>' + '</div>' + ' <div class="progress progress-striped active">' + ' <div style="width: 0%" class="bar"></div>' + '</div>' + ' <div class="progressInfo">' + '  <span class="progress-percent"></span>' + '<span class="fileSize">' + plupload.formatSize(file.size) + '' + '</span>' + '</div>' + '</div>' + ' <div class="span10 rightSection">' + '<div class="info">' + '  <span class="name">' + ' <i class="icon-file-alt"></i>' + file.name + '</span>' + ' <span class="size">' + ' <i class="icon-remove removefile" data-fileid="' + file.id + '"></i>' + ' </span>' + '</div>' + '<div  class="formInfo">' + ' <form class="form-float">' + '<div class=\"control-group fullSpace\">' + '<label class=\"control-label\">' + $translate.instant('LanguageContents.Res_5064.Caption') + ' </label>' + '<div class=\"controls\">' + '<a ng-click="AssetTypeTreePopUp(\'' + file.id + '\')">{{AssetCategory.treestructure_' + file.id + '}}</a>' + '</div>' + '<div class=\"controls\">' + '<input ui-select2="assetfields.tagAllOptions_' + file.id + '"  type="hidden"  ng-model="assetfields.assettype_' + file.id + '" ng-change="onDamtypechange(\'' + file.id + '\')" > </input>' + '<span class="imgTypeSection">' + '<i class=\"icon-reorder imgTypeEditIcon displayNone\" ng-click="AssetTypeTreePopUp(\'' + file.id + '\')"></i>' + '<span class=\"imgTypeEditIcon\" ng-click="AssetTypeTreePopUp(\'' + file.id + '\')">' + '<img src="assets/img/treeIcon.png"' + '</span>' + '</span>' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Asset name </label>' + '<div class=\"controls\">' + '<input class=\"txtbx\" type="text"  data-ng-model="assetfields.assetname_' + file.id + '" name="DescVal_' + file.id + '" id="desc_' + file.id + '" placeholder="Name">' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Description </label>' + '<div class=\"controls\">' + '<textarea class=\"small-textarea\"  name=\"assetfields.TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" ng-model=\"assetfields.TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" id=\"TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" placeholder="Description" rows="3"></textarea>' + '</div>' + '</div>' + '<div id = "div_' + file.id + '">' + $scope.dyn_ContAsset + '</div>' + '</form>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>')($scope));
                            if (o.Mime.mimes[ste.toLowerCase()] == undefined) {
                                var img = $('<img id="' + file.id + '">');
                                img.attr('src', imagesrcpath + "DAMFiles/StaticPreview_small/" + ste.toUpperCase() + ".jpg");
                                img.attr("onerror", "this.onerror=null; this.src=" + imagesrcpath + "'DAMFiles/Preview/NoPreview.jpg';");
                                img.appendTo('#Preview_' + file.id);
                            } else {
                                if (o.Mime.mimes[ste.toLowerCase()].split('/')[0] == 'image') {
                                    var img = new o.Image();
                                    img.onload = function () {
                                        var li = document.createElement('div');
                                        li.id = this.uid;
                                        document.getElementById('Preview_' + file.id).appendChild(li);
                                        this.embed(li.id, {
                                            width: 80,
                                            height: 80,
                                            crop: true
                                        });
                                    };
                                    img.load(file.getSource());
                                } else {
                                    var img = $('<img id="' + file.id + '">');
                                    img.attr('src', imagesrcpath + "DAMFiles/StaticPreview_small/" + ste.toUpperCase() + ".jpg");
                                    img.appendTo('#Preview_' + file.id);
                                }
                            }
                        });
                    });
                    up.refresh();
                    timerObj.uploadprogress = $timeout(function () {
                        TotalAssetUploadProgress();
                    }, 500);
                });
                uploaderAssetforTaskcreation.bind('UploadProgress', function (up, file) {
                    if ($scope.addtaskvallid > 0) {
                        $('#' + file.id + " .bar").css("width", file.percent + "%");
                        $('#' + file.id + " .size").html(plupload.formatSize(Math.round(file.size * (file.percent / 100))) + ' / ' + plupload.formatSize(file.size));
                        $('#' + file.id).attr('data-size-uploaded', Math.round(file.size * (file.percent / 100)));
                        TotalAssetUploadProgress();
                    }
                });
                uploaderAssetforTaskcreation.bind('Error', function (up, err) {
                    $('#assetassettaskentityattachassetdata').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                    up.refresh();
                });
                uploaderAssetforTaskcreation.bind('FileUploaded', function (up, file, response) {
                    $('#' + file.id).attr('data-status', 'true');
                    if (FileAttributes.FileID == undefined || FileAttributes.FileID.length == 0) {
                        var ste = file.name.split('.')[file.name.split('.').length - 1];
                        if (ste != undefined) ste = ste.toLowerCase();
                        var selecteddamtype = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                            return e.damExtention == ste
                        });
                        LoadAssetMetadata(file.id, selecteddamtype[0].Id);
                    }
                    var ind = FileAttributes.FileID.indexOf(file.id);
                    if ($scope.newTaskid > 0) {
                        $scope.SaveAssetFileDetails(file, response.response, FileAttributes.AttributeDataList[ind]);
                        TotalAssetUploadProgress();
                    }
                });
                uploaderAssetforTaskcreation.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKeypath = TenantFilePath.replace(/\\/g, "\/");
                    var uniqueKey = (uniqueKeypath + "DAMFiles/Original/" + file.id + fileext);
                    uploader.settings.multipart_params.key = uniqueKey;
                    uploader.settings.multipart_params.Filename = uniqueKey;
                });
            } else {
                uploaderAssetforTaskcreation = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickassetinTaskCreation',
                    container: 'assetscontainerTaskCreation',
                    max_file_size: '10000mb',
                    url: 'Handlers/DamFileHandler.ashx?Type=Attachment',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    chunk_size: '64Kb',
                    multipart_params: {}
                });
                uploaderAssetforTaskcreation.bind('Init', function (up, params) {
                    uploaderAssetforTaskcreation.splice();
                });
                uploaderAssetforTaskcreation.init();
                var dom = {
                    uploaderAssetforTaskcreation: $("#uploaderAssetforTaskcreation"),
                    uploads: $("ul.uploads")
                };
                $("#tempTaskCreation").click(function (e) {
                    if ($("#assetassettaskentityattachassetdata > div").length > 0 && $scope.addtaskvallid > 0) {
                        var str = $('#taskuploaded').text().replace('Upload Successful.', '');
                        $('#taskuploaded').text(str);
                        if ($scope.UniqueidsfrBlankAsset.length + $scope.Fileids.length > 0 && $("#assetassettaskentityattachassetdata > div").length > 0) {
                            var count = 0;
                            var namecount = 0;
                            angular.forEach($scope.UniqueidsfrBlankAsset, function (key) {
                                if ($scope.assetfields['assettype_' + key] == '' || $scope.assetfields['assettype_' + key] == null || $scope.assetfields['assettype_' + key] == undefined) {
                                    count = count + 1;
                                }
                                if ($scope.assetfields['assetname_' + key] == '' || $scope.assetfields['assetname_' + key] == null || $scope.assetfields['assetname_' + key] == undefined) {
                                    namecount = namecount + 1;
                                }
                            });
                            angular.forEach($scope.Fileids, function (key) {
                                if ($scope.assetfields['assettype_' + key] == '' || $scope.assetfields['assettype_' + key] == null || $scope.assetfields['assettype_' + key] == undefined) {
                                    count = count + 1;
                                }
                                if ($scope.assetfields['assetname_' + key] == '' || $scope.assetfields['assetname_' + key] == null || $scope.assetfields['assetname_' + key] == undefined) {
                                    namecount = namecount + 1;
                                }
                            });
                            if (count == 0 && namecount == 0) {
                                $('#CreateNewTaskBtn').attr('disabled', 'disabled');
                                $('#CreateNewTaskBtn').css('pointer-events', 'none');
                                $('#pickassetinTaskCreation').attr('disabled', 'disabled');
                                $('#pickassetinTaskCreation').css('pointer-events', 'none');
                                if ($scope.UniqueidsfrBlankAsset.length > 0) {
                                    if (saveBlankAssetsCalled == false) {
                                        saveBlankAssetsCalled = true;
                                        saveBlankAssets();
                                    }
                                }
                                $scope.addfromComputer = true;
                                uploader.start();
                                e.preventDefault();
                            } else {
                                $('#CreateNewTaskBtn').removeClass('disabled');
                                bootbox.alert($translate.instant('LanguageContents.Res_4594.Caption'));
                                $scope.uploadingInProgress = false;
                                return false;
                            }
                        } else {
                            if ($scope.TaskCreationAttachAsset.DAssetSelectionFiles.length > 0) {
                                AttachAssets($scope.returnObj);
                            } else {
                                bootbox.alert($translate.instant('LanguageContents.Res_4563.Caption'));
                                $scope.uploadingInProgress = false;
                                return false;
                            }
                        }
                    }
                });
                uploaderAssetforTaskcreation.bind('FilesAdded', function (up, files) {
                    $scope.IsaddAssetsfromComputer = true;
                    $scope.addfromComputer = true;
                    totalfilecount = totalfilecount + files.length;
                    $.each(files, function (i, file) {
                        var damid = 0;
                        $scope.Fileids.push(file.id);
                        var ste = file.name.split('.')[file.name.split('.').length - 1];
                        if (ste != undefined) ste = ste.toLowerCase();
                        var damCount = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                            return e.damExtention == ste
                        }).length;
                        if (damCount > 0) {
                            var selecteddamtype = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                                return e.damExtention == ste
                            });
                        } else {
                            var wildcardExt = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                                return e.damExtention.indexOf("*") != -1
                            });
                            var iswildcard = [];
                            iswildcard = $.grep(wildcardExt, function (e) {
                                return e.damExtention.substring(0, e.damExtention.length - 1).indexOf(ste) != -1
                            });
                            if (iswildcard.length > 0) var selecteddamtype = iswildcard;
                        }
                        $scope.AllDamTypes = [];
                        $scope.assetfields["tagAllOptions_" + file.id] = {
                            multiple: false,
                            allowClear: true,
                            data: $scope.AssetCategory["AssetTypesList_" + file.id],
                            formatResult: $scope.formatResult,
                            formatSelection: $scope.formatSelection,
                            dropdownCssClass: "bigdrop",
                            escapeMarkup: function (m) {
                                return m;
                            }
                        };
                        if (selecteddamtype != undefined && selecteddamtype != null) {
                            damid = selecteddamtype[0].Id;
                            var ownerID = parseInt($scope.OwnerID, 10);
                            var tempval = [];
                            var tempassty = "";
                            for (var k = 0; k < selecteddamtype.length; k++) {
                                tempassty = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                                    return e.id == selecteddamtype[k].Id
                                });
                                if (tempassty != undefined && tempassty.length > 0) var tempattr = $.grep(tempval, function (e) {
                                    return e.id == tempassty[0].id
                                });
                                if (tempattr.length == 0) tempval.push(tempassty[0]);
                            }
                            if (tempval.length > 0) {
                                $scope.AssetCategory["AssetTypesList_" + file.id] = tempval;
                                $scope.assetfields["tagAllOptions_" + file.id].data = $scope.AssetCategory["AssetTypesList_" + file.id];
                            } else { }
                        }
                        TaskCreationService.GetAssetCategoryTree(damid).then(function (data) {
                            if (data.Response != null) {
                                $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                $scope.AssetCategory["filterval_" + file.id] = '';
                                var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                            }
                        });
                        $scope.AssetCategory["treestructure_" + file.id] = '-';
                        if ($scope.AssetCategory["AssetTypesList_" + file.id] != undefined) if ($scope.AssetCategory["AssetTypesList_" + file.id].length > 1 || $scope.AssetCategory["AssetTypesList_" + file.id].length == 0) damid = 0;
                        TaskCreationService.GetDamAttributeRelation(damid).then(function (attrreldata) {
                            $scope.dyn_ContAsset = "";
                            if (attrreldata.Response != undefined && attrreldata.Response != null) {
                                $scope.dyn_ContAsset = "";
                                $scope.DAMattributesRelationList = attrreldata.Response;
                                FileAttributes.AssetType.push(damid);
                                for (var i = 0; i < $scope.DAMattributesRelationList.length; i++) {
                                    if ($scope.DAMattributesRelationList[i].AttributeTypeID == 1) {
                                        if ($scope.DAMattributesRelationList[i].ID != 70) {
                                            if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.Name) {
                                                $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                                $scope.assetfields["TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                            } else {
                                                $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                                $scope.assetfields["TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                            }
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 3) {
                                        if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.Owner) {
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" dirownernameautopopulate placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.OwnerList[0].UserName;
                                        } else if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.FiscalYear) {
                                            $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"> <option value=\"\"> Select " + $scope.DAMattributesRelationList[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                        } else {
                                            $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"> <option value=\"\"> Select " + $scope.DAMattributesRelationList[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 2) {
                                        $scope.dyn_ContAsset += "<div class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"assetfields.TextMultiLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" name=\"assetfields.TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-model=\"assetfields.TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\" rows=\"3\"></textarea></div></div>";
                                        $scope.assetfields["TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 4) {
                                        $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = [];
                                        $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                        var defaultmultiselectvalue = $scope.DAMattributesRelationList[i].DefaultValue.split(',');
                                        if ($scope.DAMattributesRelationList[i].DefaultValue != "") {
                                            for (var v = 0, val; val = defaultmultiselectvalue[v++];) {
                                                $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID].push(parseInt(val));
                                            }
                                        } else {
                                            $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = [];
                                        }
                                        $scope.dyn_ContAsset += "<div class=\"control-group attachmentMultiselect\"><label class=\"control-label\" for=\"assetfields.ListMultiSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select class=\"multiselect\" multiple=\"multiple\"  multiselect-dropdown data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  > </select></div></div>";
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 5 && $scope.DAMattributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.DAMattributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                                        $scope.calanderopened = false;
                                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                        $scope.MaxValue = $scope.DAMattributesRelationList[i].MaxValue;
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                        } else {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                        } else {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + 100000);
                                        }
                                        var temp = $scope.HolidayListCalculation($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID], $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID]);
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = (temp.MinDate);
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = (temp.MaxDate);
                                        $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + "</label><div class=\"controls\"><span class='editable'>{{assetfields.DateTime_" + $scope.DAMattributesRelationList[i].ID + "}}</span><input type=\"text\" class=\"txtbx DatePartctrl\" " + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-click=\"Calanderopen($event,'Date_" + file.id + $scope.DAMattributesRelationList[i].ID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Date_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" min-date=\"assetfields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\" ng-change=\"setTimeout(changeduedate_changed(assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + ",assetfields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",assetfields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "," + file.id + $scope.DAMattributesRelationList[i].ID + "),1000)\" ng-model=\"assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"></div></div>";
                                        $scope.assetfields["assetfields.DatePart_Calander_Open" + file.id + $scope.DAMattributesRelationList[i].ID] = false;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 16) {
                                        $scope.calanderopened = false;
                                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                        $scope.MaxValue = $scope.DAMattributesRelationList[i].MaxValue;
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                        } else {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                        } else {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + 100000);
                                        }
                                        var temp = $scope.HolidayListCalculation($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID], $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID]);
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = (temp.MinDate);
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = (temp.MaxDate);
                                        $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + "</label><div class=\"controls\"><span class='editable'>{{assetfields.DateTime_" + $scope.DAMattributesRelationList[i].ID + "}}</span><input type=\"text\" class=\"txtbx DatePartctrl\" " + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-click=\"Calanderopen($event,'Date_" + file.id + $scope.DAMattributesRelationList[i].ID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Date_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" min-date=\"assetfields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\" ng-change=\"setTimeout(changeduedate_changed(assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + ",assetfields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",assetfields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "," + file.id + $scope.DAMattributesRelationList[i].ID + "),1000)\" ng-model=\"assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"></div></div>";
                                        $scope.assetfields["assetfields.DatePart_Calander_Open" + file.id + $scope.DAMattributesRelationList[i].ID] = false;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 12) {
                                        var totLevelCnt1 = $scope.DAMattributesRelationList[i].Levels.length;
                                        $scope.dyn_Cont += "<div class=\"control-group pull-left\">";
                                        for (var j = 0; j < totLevelCnt1; j++) {
                                            if (totLevelCnt1 == 1) {
                                                $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                                    return item.Caption
                                                };
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                                    return item.Caption
                                                };
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                                $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12,'" + file.id + "' )\" ng-model=\"assetfields.MultiSelectDropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                                $scope.setFieldKeys();
                                            } else {
                                                $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                                    return item.Caption
                                                };
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                                    return item.Caption
                                                };
                                                if (j == 0) {
                                                    $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                                    $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                                    $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                                    $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12,'" + file.id + "' )\" ng-model=\"assetfields.MultiSelectDropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                                } else {
                                                    $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                                    if (j == ($scope.DAMattributesRelationList[i].Levels.length - 1)) {
                                                        $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                                        $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                        $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 ,'" + file.id + "')\" ng-model=\"assetfields.MultiSelectDropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                                    } else {
                                                        $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                                        $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                        $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12,'" + file.id + "' )\" ng-model=\"assetfields.MultiSelectDropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                                    }
                                                }
                                            }
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 10) {
                                        $scope.items = [];
                                        $scope.MinValue = $scope.DAMatributesRelationList[i].MinValue;
                                        $scope.MaxValue = $scope.DAMattributesRelationList[i].MaxValue;
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                        } else {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                        } else {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + 100000);
                                        }
                                        var temp = $scope.HolidayListCalculation($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID], $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID]);
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = (temp.MinDate);
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = (temp.MaxDate);
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                        $scope.OptionObj["option_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].Options;
                                        $scope.items.push({
                                            startDate: [],
                                            endDate: [],
                                            comment: '',
                                            sortorder: 0,
                                            fileid: 0
                                        });
                                        $scope.assetfields["Period_" + file.id] = [];
                                        $scope.assetfields["Period_" + file.id].push({
                                            startDate: [],
                                            endDate: [],
                                            comment: '',
                                            sortorder: 0,
                                            fileid: file.id
                                        });
                                        $scope.dyn_ContAsset += "    <div class=\"control-group\"><label for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.DAMattributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in assetfields.Period_" + file.id + " \" ng-form=\"subForm\">";
                                        $scope.dyn_ContAsset += "<div class=\"row-fluid\"><div class=\"inputHolder span5 noBorder\">";
                                        $scope.dyn_ContAsset += "<input class=\"sdate margin-bottom0x Period_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-change=\"changeperioddate_changed(item.startDate,assetfields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",assetfields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + ",'StartDate')\" ng-model=\"item.startDate\" ng-click=\"Calanderopen($event,'StartDate_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"StartDate_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" min-date=\"assetfields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  placeholder=\"-- Start date --\"/><input class=\"edate margin-bottom0x Period_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" type=\"text\" name=\"enddate\" id=\"item.endDate\" ng-change=\"changeperioddate_changed(item.endDate,assetfields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",assetfields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + ",'EndDate')\" ng-model=\"item.endDate\" ng-click=\"Calanderopen($event,'EndDate_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"EndDate_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" min-date=\"assetfields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- End date --\"/><input class=\"dateComment txtbx\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + $scope.DAMattributesRelationList[i].Caption + " Comment --\" />";
                                        $scope.dyn_ContAsset += "</div><div class=\"buttonHolder span1\" ng-show=\"$first==false\">";
                                        $scope.dyn_ContAsset += "<a ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew('" + file.id + "')\">[Add " + $scope.DAMattributesRelationList[i].Caption + "]</a></div></div>";
                                        $scope.assetfields["Period_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = "";
                                        $scope.assetfields["assetfields.DatePart_Calander_Open" + "item.startDate"] = false;
                                        $scope.assetfields["assetfields.DatePart_Calander_Open" + "item.endDate"] = false;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 13) {
                                        $scope.PercentageVisibleSettings["AttributeId_Levels_" + file.id + $scope.DAMattributesRelationList[i].AttributeID.toString() + ""] = true;
                                        $scope.DropDownTreePricing["AttributeId_Levels_" + file.id + $scope.DAMattributesRelationList[i].AttributeID.toString() + ""] = $scope.DAMattributesRelationList[i].DropDownPricing;
                                        $scope.dyn_ContAsset += "<div drowdowntreepercentagemultiselectionasset data-purpose='entity' data-fileid=" + file.id.toString() + " data-attributeid=" + $scope.DAMattributesRelationList[i].AttributeID.toString() + "></div>";
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 8) {
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                        $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-model=\"assetfields.TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                                        $scope.assetfields["TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 17) {
                                        $scope.assetfields["ListTagwords_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = [];
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                        $scope.OptionObj["tagoption_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = [];
                                        $scope.tempscope = [];
                                        $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" class=\"control-group\">";
                                        $scope.dyn_ContAsset += "<label class=\"control-label\" for=\"assetfields.ListTagwords_ " + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].AttributeCaption + " </label>";
                                        $scope.dyn_ContAsset += "<div class=\"controls\">";
                                        $scope.dyn_ContAsset += "<directive-tagwords item-attrid = \"" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"assetfields.ListTagwords_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                                        $scope.dyn_ContAsset += "</div></div>";
                                        if ($scope.DAMattributesRelationList[i].InheritFromParent) { } else { }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 6) {
                                        var totLevelCnt = $scope.DAMattributesRelationList[i].Levels.length;
                                        for (var j = 0; j < $scope.DAMattributesRelationList[i].Levels.length; j++) {
                                            $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                            $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                            $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                            $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                                return item.Caption
                                            };
                                            $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                                return item.Caption
                                            };
                                            if (j == 0) {
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                                $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                                $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6,'" + file.id + "' )\"  ng-model=\"assetfields.DropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                            } else {
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                                $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 ,'" + file.id + "')\"  ng-model=\"assetfields.DropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                            }
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 7) {
                                        $scope.assetfields["Tree_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = [];
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                        $scope.treesrcdirec["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                        if ($scope.treesrcdirec["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID].length > 0) {
                                            treeTextVisbileflag = false;
                                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID])) {
                                                $scope.treePreviewObj["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                            } else $scope.treePreviewObj["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = false;
                                        } else {
                                            $scope.treePreviewObj["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = false;
                                        }
                                        $scope.dyn_ContAsset += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '\" class="control-group treeNode-control-group">';
                                        $scope.dyn_ContAsset += '    <label class="control-label">' + $scope.DAMattributesRelationList[i].AttributeCaption + '</label>';
                                        $scope.dyn_ContAsset += '    <div class="controls treeNode-controls">';
                                        $scope.dyn_ContAsset += '        <div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '"></div>';
                                        $scope.dyn_ContAsset += '        <div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '">';
                                        $scope.dyn_ContAsset += '            <span ng-if="doing_async">...loading...</span>';
                                        $scope.dyn_ContAsset += '            <abn-tree tree-filter="filterValue_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '\" accessable="' + $scope.DAMattributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                                        $scope.dyn_ContAsset += '        </div>';
                                        $scope.dyn_ContAsset += '        <div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '\">';
                                        $scope.dyn_ContAsset += '            <div class="controls">';
                                        $scope.dyn_ContAsset += '                <eu-tree tree-data=\"treesrcdirec.Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.DAMattributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                        $scope.dyn_ContAsset += '        </div></div>';
                                        $scope.dyn_ContAsset += '</div></div>';
                                    }
                                }
                            }
                            $scope.assetfields["assetname_" + file.id] = file.name;
                            if (selecteddamtype != undefined && selecteddamtype != null) {
                                var dataid = '';
                                var rec = [];
                                rec = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                                    return e.Id == selecteddamtype[0].Id
                                });
                                if (rec.length > 0) dataid = rec[0].Id;
                                if ($scope.assetfields["tagAllOptions_" + file.id].data.length == 1) $scope.assetfields["assettype_" + file.id] = rec[0];
                                if ($scope.AssetCategory["AssetTypesList_" + file.id].length == 1) {
                                    TaskCreationService.GetAssetCategoryTree(dataid).then(function (data) {
                                        if (data.Response != null) {
                                            $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                            $scope.AssetCategory["filterval_" + file.id] = '';
                                            var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                            $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                                        }
                                    });
                                    if ($scope.assetfields["tagAllOptions_" + file.id].data.length == 1) {
                                        TaskCreationService.GetAssetCategoryPathInAssetEdit(dataid).then(function (datapath) {
                                            if (datapath.Response != null) {
                                                if (datapath.Response[1].AssetTypeIDs.length > 0) {
                                                    $scope.AssetCategory["treestructure_" + file.id] = datapath.Response[0].CategoryPath;
                                                } else $scope.assetfields["assettype_" + file.id] = '';
                                            }
                                        });
                                    } else $scope.assetfields["assettype_" + file.id] = '';
                                } else {
                                    $scope.assetfields["assettype_" + file.id] = rec[0];
                                    $scope.oldfiledetails["AssetType_" + file.id] = '';
                                    TaskCreationService.GetAssetCategoryTree(0).then(function (data) {
                                        if (data.Response != null) {
                                            $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                            $scope.AssetCategory["filterval_" + file.id] = '';
                                            var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                            $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                                        }
                                    });
                                }
                            } else {
                                $scope.assetfields["tagAllOptions_" + file.id] = {
                                    multiple: false,
                                    allowClear: true,
                                    data: [],
                                    formatResult: $scope.formatResult,
                                    formatSelection: $scope.formatSelection,
                                    dropdownCssClass: "bigdrop",
                                    escapeMarkup: function (m) {
                                        return m;
                                    }
                                };
                                $scope.oldfiledetails["AssetType_" + file.id] = [];
                                TaskCreationService.GetAssetCategoryTree(0).then(function (data) {
                                    if (data.Response != null) {
                                        $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                        $scope.AssetCategory["filterval_" + file.id] = '';
                                        var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                        $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                                    }
                                });
                            }
                            $scope.IsAttachmentPresent = true;
                            $('#assetassettaskentityattachassetdata').append($compile('<div id="' + file.id + '" class="attachmentBox" Data-role="Attachment" data-size="' + file.size + '" data-size-uploaded="0" data-status="false">' + '<div class="row-fluid">' + '<div class="span12">' + '<div class="span2 leftSection">' + ' <i id="icon_ok' + file.id + '" class="icon-ok-sign" style="display:none;"  ></i>' + ' <div class="fileType-Preview">' + ' <div id="Preview_' + file.id + '" class="fileType-PreviewDiv">' + ' </div>' + '</div>' + ' <div class="progress progress-striped active">' + ' <div style="width: 0%" class="bar"></div>' + '</div>' + ' <div class="progressInfo">' + '  <span class="progress-percent"></span>' + '<span class="fileSize">' + plupload.formatSize(file.size) + '' + '</span>' + '</div>' + '</div>' + ' <div class="span10 rightSection">' + '<div class="info">' + '  <span class="name">' + ' <i class="icon-file-alt"></i>' + file.name + '</span>' + ' <span class="size">' + ' <i class="icon-remove removefile" data-fileid="' + file.id + '"></i>' + ' </span>' + '</div>' + '<div  class="formInfo">' + ' <form class="form-float">' + '<div class=\"control-group fullSpace\">' + '<label class=\"control-label\">' + $translate.instant('LanguageContents.Res_5064.Caption') + ' </label>' + '<div class=\"controls\">' + '<a ng-click="AssetTypeTreePopUp(\'' + file.id + '\')">{{AssetCategory.treestructure_' + file.id + '}}</a>' + '</div>' + '<div class=\"controls\">' + '<input ui-select2="assetfields.tagAllOptions_' + file.id + '"  type="hidden"  ng-model="assetfields.assettype_' + file.id + '" ng-change="onDamtypechange(\'' + file.id + '\')" > </input>' + '<span class="imgTypeSection">' + '<i class=\"icon-reorder imgTypeEditIcon displayNone\" ng-click="AssetTypeTreePopUp(\'' + file.id + '\')"></i>' + '<span class=\"imgTypeEditIcon\" ng-click="AssetTypeTreePopUp(\'' + file.id + '\')">' + '<img src="assets/img/treeIcon.png"' + '</span>' + '</span>' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Asset name </label>' + '<div class=\"controls\">' + '<input class=\"txtbx\" type="text"  data-ng-model="assetfields.assetname_' + file.id + '" name="DescVal_' + file.id + '" id="desc_' + file.id + '" placeholder="Name">' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Description </label>' + '<div class=\"controls\">' + '<textarea class=\"small-textarea\"  name=\"assetfields.TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" ng-model=\"assetfields.TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" id=\"TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" placeholder="Description" rows="3"></textarea>' + '</div>' + '</div>' + '<div id = "div_' + file.id + '">' + $scope.dyn_ContAsset + '</div>' + '</form>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>')($scope));
                            if (o.Mime.mimes[ste.toLowerCase()] == undefined) {
                                var img = $('<img id="' + file.id + '">');
                                img.attr('src', imagesrcpath + "DAMFiles/StaticPreview_small/" + ste.toUpperCase() + ".jpg");
                                img.attr("onerror", "this.onerror=null; this.src=" + imagesrcpath + "'DAMFiles/Preview/NoPreview.jpg';");
                                img.appendTo('#Preview_' + file.id);
                            } else {
                                if (o.Mime.mimes[ste.toLowerCase()].split('/')[0] == 'image') {
                                    var img = new o.Image();
                                    img.onload = function () {
                                        var li = document.createElement('div');
                                        li.id = this.uid;
                                        document.getElementById('Preview_' + file.id).appendChild(li);
                                        this.embed(li.id, {
                                            width: 80,
                                            height: 80,
                                            crop: true
                                        });
                                    };
                                    img.load(file.getSource());
                                } else {
                                    var img = $('<img id="' + file.id + '">');
                                    img.attr('src', imagesrcpath + "DAMFiles/StaticPreview_small/" + ste.toUpperCase() + ".jpg");
                                    img.appendTo('#Preview_' + file.id);
                                }
                            }
                        });
                    });
                    up.refresh();
                    timerObj.uploadprogress = $timeout(function () {
                        TotalAssetUploadProgress();
                    }, 500);
                });
                uploaderAssetforTaskcreation.bind('UploadProgress', function (up, file) {
                    if ($scope.addtaskvallid > 0) {
                        $('#' + file.id + " .bar").css("width", file.percent + "%");
                        $('#' + file.id + " .size").html(plupload.formatSize(Math.round(file.size * (file.percent / 100))) + ' / ' + plupload.formatSize(file.size));
                        $('#' + file.id).attr('data-size-uploaded', Math.round(file.size * (file.percent / 100)));
                        TotalAssetUploadProgress();
                    }
                });
                uploaderAssetforTaskcreation.bind('Error', function (up, err) {
                    $('#assetassettaskentityattachassetdata').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                    up.refresh();
                });
                uploaderAssetforTaskcreation.bind('FileUploaded', function (up, file, response) {
                    $('#' + file.id).attr('data-status', 'true');
                    if (FileAttributes.FileID == undefined || FileAttributes.FileID.length == 0) {
                        var ste = file.name.split('.')[file.name.split('.').length - 1];
                        if (ste != undefined) ste = ste.toLowerCase();
                        var selecteddamtype = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                            return e.damExtention == ste
                        });
                        LoadAssetMetadata(file.id, selecteddamtype[0].Id);
                    }
                    var ind = FileAttributes.FileID.indexOf(file.id);
                    if ($scope.newTaskid > 0) {
                        $scope.SaveAssetFileDetails(file, response.response, FileAttributes.AttributeDataList[ind]);
                        TotalAssetUploadProgress();
                    }
                });
                uploaderAssetforTaskcreation.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
            }
        }
        $scope.dyn_ContAsset = '';
        $scope.fieldoptions = [];
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.assetfields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });
        }
        $scope.DAMattributesRelationList = [];
        $scope.AttributeData = [];
        var FileAttributes = {
            "FileID": [],
            "AttributeDataList": [],
            "AssetType": []
        };
        $scope.SaveAsset = [];
        $scope.oldfiledetails = {};
        $scope.SaveAssetFileDetails = function (file, response, fileAttributes) {
            $scope.AttributeData = [];
            if ($scope.assetfields['assettype_' + file.id] == undefined || $scope.assetfields['assettype_' + file.id] == null || $scope.assetfields['assettype_' + file.id] == '') {
                bootbox.alert($translate.instant('LanguageContents.Res_2511.Caption'));
                return false;
            }
            var AssetTypeID = $scope.assetfields['assettype_' + file.id].id;
            var AssetName = $scope.assetfields['assetname_' + file.id];
            var extension = file.name.substring(file.name.lastIndexOf("."));
            var resultArr = response.split(",");
            var AssetFileGuid = file.id;
            var AssetAttribute = (fileAttributes == undefined) ? [] : fileAttributes;
            $scope.assetfields['TextMultiLine_' + AssetFileGuid + 3] = ($scope.assetfields['TextMultiLine_' + AssetFileGuid + 3] == undefined) ? '' : $scope.assetfields['TextMultiLine_' + AssetFileGuid + 3];
            $scope.AttributeData.push({
                "AttributeID": "3",
                "AttributeCaption": "Description",
                "AttributeTypeID": "2",
                "NodeID": $scope.assetfields['TextMultiLine_' + AssetFileGuid + 3],
                "Level": 0
            });
            for (var i = 0; i < AssetAttribute.length; i++) {
                if (AssetAttribute[i].AttributeTypeID == 3) {
                    if (AssetAttribute[i].ID == SystemDefiendAttributes.Owner) {
                        $scope.AttributeData.push({
                            "AttributeID": AssetAttribute[i].ID,
                            "AttributeCaption": AssetAttribute[i].Caption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                            "Level": 0
                        });
                    } else if ($scope.assetfields['ListSingleSelection_' + AssetFileGuid + AssetAttribute[i].ID] != undefined && $scope.assetfields['ListSingleSelection_' + AssetFileGuid + AssetAttribute[i].ID] != "") {
                        var value = $scope.assetfields['ListSingleSelection_' + AssetFileGuid + AssetAttribute[i].ID];
                        $scope.AttributeData.push({
                            "AttributeID": AssetAttribute[i].ID,
                            "AttributeCaption": AssetAttribute[i].Caption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                            "Level": 0
                        });
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 1) {
                    if (AssetAttribute[i].ID == SystemDefiendAttributes.Name) $scope.entityName = $scope.assetfields['TextSingleLine_' + AssetFileGuid + AssetAttribute[i].ID];
                    else {
                        $scope.AttributeData.push({
                            "AttributeID": AssetAttribute[i].ID,
                            "AttributeCaption": AssetAttribute[i].Caption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": ($scope.assetfields['TextSingleLine_' + AssetFileGuid + AssetAttribute[i].ID] != null) ? $scope.assetfields['TextSingleLine_' + AssetFileGuid + AssetAttribute[i].ID].toString() : "",
                            "Level": 0
                        });
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 2) {
                    $scope.AttributeData.push({
                        "AttributeID": AssetAttribute[i].ID,
                        "AttributeCaption": AssetAttribute[i].Caption,
                        "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                        "NodeID": ($scope.assetfields['TextMultiLine_' + AssetFileGuid + AssetAttribute[i].ID] != null) ? $scope.assetfields['TextMultiLine_' + AssetFileGuid + AssetAttribute[i].ID].toString() : "",
                        "Level": 0
                    });
                } else if (AssetAttribute[i].AttributeTypeID == 4) {
                    if ($scope.assetfields['ListMultiSelection_' + AssetFileGuid + AssetAttribute[i].ID] != "" && $scope.assetfields['ListMultiSelection_' + AssetFileGuid + AssetAttribute[i].ID] != undefined) {
                        if ($scope.assetfields['ListMultiSelection_' + AssetFileGuid + AssetAttribute[i].ID].length > 0) {
                            var multiselectiObject = $scope.assetfields['ListMultiSelection_' + AssetFileGuid + AssetAttribute[i].ID];
                            for (var k = 0; k < multiselectiObject.length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": AssetAttribute[i].ID,
                                    "AttributeCaption": AssetAttribute[i].Caption,
                                    "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                    "NodeID": parseInt(multiselectiObject[k], 10),
                                    "Level": 0
                                });
                            }
                        }
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 6) {
                    for (var j = 0; j < AssetAttribute[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + AssetFileGuid + AssetAttribute[i].ID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + AssetFileGuid + AssetAttribute[i].ID + '_' + (j + 1)] != undefined) {
                            $scope.AttributeData.push({
                                "AttributeID": AssetAttribute[i].ID,
                                "AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
                                "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                "NodeID": [parseInt($scope.fields['DropDown_' + AssetFileGuid + AssetAttribute[i].ID + '_' + (j + 1)])],                                
                                "Level": $.grep($scope.DropDownTreeOptionValues["Options" + AssetFileGuid + AssetAttribute[i].ID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['DropDown_' + AssetFileGuid + AssetAttribute[i].ID + '_' + (j + 1)]) })[0].Level,
                                "Value": "-1"
                            });
                        }
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 5 && AssetAttribute[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                    var MyDate = new Date.create();
                    var MyDateString;
                    if (AssetAttribute[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        if ($scope.assetfields["DatePart_" + AssetFileGuid + AssetAttribute[i].ID] != undefined) MyDateString = $scope.assetfields["DatePart_" + AssetFileGuid + AssetAttribute[i].ID].toString('MM/dd/yyyy');
                        else MyDateString = "";
                    } else {
                        MyDateString = "";
                    }
                    $scope.AttributeData.push({
                        "AttributeID": AssetAttribute[i].ID,
                        "AttributeCaption": AssetAttribute[i].Caption,
                        "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                        "NodeID": MyDateString,
                        "Level": 0
                    });
                } else if (AssetAttribute[i].AttributeTypeID == 16) {
                    if ($scope.assetfields["DatePart_" + AssetFileGuid + AssetAttribute[i].ID] != undefined) {
                        $scope.AttributeData.push({
                            "AttributeID": AssetAttribute[i].ID,
                            "AttributeCaption": AssetAttribute[i].Caption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": $scope.assetfields["DatePart_" + AssetFileGuid + AssetAttribute[i].ID],
                            "Level": 0
                        });
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) {
                        return e.AttributeId == AssetAttribute[i].ID;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        $scope.AttributeData.push({
                            "AttributeID": AssetAttribute[i].ID,
                            "AttributeCaption": AssetAttribute[i].Caption,
                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                            "NodeID": [parseInt(nodeval.id, 10)],
                            "Level": parseInt(nodeval.Level, 10),
                            "Value": "-1"
                        });
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 12) {
                    for (var j = 0; j < AssetAttribute[i].Levels.length; j++) {
                        var levelCount = AssetAttribute[i].Levels.length;
                        if (levelCount == 1) {
                            for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)].length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": AssetAttribute[i].AttributeID,
                                    "AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
                                    "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)][k]],
                                    "Level": $.grep($scope.DropDownTreeOptionValues["Options" + AssetFileGuid + AssetAttribute[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)][k]) })[0].Level,
                                    "Value": "-1"
                                });
                            }
                        } else {
                            if ($scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)] != undefined) {
                                if (j == (AssetAttribute[i].Levels.length - 1)) {
                                    for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)].length; k++) {
                                        $scope.AttributeData.push({
                                            "AttributeID": AssetAttribute[i].AttributeID,
                                            "AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
                                            "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)][k]],
                                            "Level": $.grep($scope.DropDownTreeOptionValues["Options" + AssetFileGuid + AssetAttribute[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)][k]) })[0].Level,
                                            "Value": "-1"
                                        });
                                    }
                                } else {
                                    $scope.AttributeData.push({
                                        "AttributeID": AssetAttribute[i].AttributeID,
                                        "AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
                                        "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)]],
                                        "Level": $.grep($scope.DropDownTreeOptionValues["Options" + AssetFileGuid + AssetAttribute[i].AttributeID + "_" + (j + 1)], function (e) { return e.id == parseInt($scope.fields['MultiSelectDropDown_' + AssetFileGuid + AssetAttribute[i].AttributeID + '_' + (j + 1)]) })[0].Level,
                                        "Value": "-1"
                                    });
                                }
                            }
                        }
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 13) {
                    for (var j = 0; j < AssetAttribute[i].Levels.length; j++) {
                        var attributeLevelOptions = [];
                        attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + AssetFileGuid + AssetAttribute[i].AttributeID + ""], function (e) {
                            return e.level == (j + 1);
                        }));
                        if (attributeLevelOptions[0] != undefined) {
                            if (attributeLevelOptions[0].selection != undefined) {
                                for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
                                    var valueMatches = [];
                                    if (attributeLevelOptions[0].selection.length > 1) valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                        return relation.NodeId.toString() === opt;
                                    });
                                    $scope.AttributeData.push({
                                        "AttributeID": AssetAttribute[i].AttributeID,
                                        "AttributeCaption": AssetAttribute[i].Levels[j].LevelName,
                                        "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                        "NodeID": [opt],
                                        "Level": (j + 1),
                                        "Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
                                    });
                                }
                            }
                        }
                    }
                } else if (AssetAttribute[i].AttributeTypeID == 8) {
                    $scope.AttributeData.push({
                        "AttributeID": AssetAttribute[i].AttributeID,
                        "AttributeCaption": AssetAttribute[i].AttributeCaption,
                        "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                        "NodeID": $scope.assetfields['TextSingleLine_' + AssetFileGuid + AssetAttribute[i].AttributeID].toString(),
                        "Level": 0,
                        "Value": "-1"
                    });
                } else if (AssetAttribute[i].AttributeTypeID == 17) {
                    if ($scope.assetfields['ListTagwords_' + AssetFileGuid + AssetAttribute[i].AttributeID] != "" && $scope.assetfields['ListTagwords_' + AssetFileGuid + AssetAttribute[i].AttributeID] != undefined) {
                        for (var j = 0; j < $scope.assetfields['ListTagwords_' + AssetFileGuid + AssetAttribute[i].AttributeID].length; j++) {
                            $scope.AttributeData.push({
                                "AttributeID": AssetAttribute[i].AttributeID,
                                "AttributeCaption": AssetAttribute[i].AttributeCaption,
                                "AttributeTypeID": AssetAttribute[i].AttributeTypeID,
                                "NodeID": parseInt($scope.assetfields['ListTagwords_' + AssetFileGuid + AssetAttribute[i].AttributeID][j], 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                }
            }
            $scope.StartEndDate = [];
            if ($scope.assetfields["Period_" + AssetFileGuid] != undefined) {
                $scope.perItems = [];
                $scope.perItems = $scope.assetfields["Period_" + AssetFileGuid];
                for (var m = 0; m < $scope.perItems.length; m++) {
                    $scope.StartEndDate.push({
                        startDate: ($scope.perItems[m].startDate.length != 0 ? dateFormat($scope.perItems[m].startDate) : ''),
                        endDate: ($scope.perItems[m].endDate.length != 0 ? dateFormat($scope.perItems[m].endDate) : ''),
                        comment: ($scope.perItems[m].comment.trim().length != 0 ? $scope.perItems[m].comment : ''),
                        sortorder: 0
                    });
                }
            }
            $scope.SaveAsset = [];
            $scope.SaveAsset.push({
                "Typeid": AssetTypeID,
                "Active": true,
                "FolderID": 0,
                "AttributeData": $scope.AttributeData,
                "Status": 1,
                "MimeType": resultArr[1],
                "Size": file.size,
                "FileGuid": resultArr[0],
                "Extension": extension,
                "Name": AssetName,
                "VersionNo": 1,
                "FileName": file.name,
                "Description": file.name,
                "EntityID": $scope.newTaskid,
                "Periods": $scope.StartEndDate
            });
            TaskCreationService.SaveAsset($scope.SaveAsset).then(function (result) {
                filecount = filecount + 1;
                if (result.Response == 0) {
                    NotifyError($translate.instant('LanguageContents.Res_4331.Caption'));
                    $('#CreateNewTask').css('display', 'none');
                    $('#pickassetinTaskCreation').css('display', 'none');
                    $('#createblankAsset').css('display', 'none');
                    $('#CreateNewTaskBtn').removeClass('disabled');
                    closecreatetaskpopup();
                    $("#taskcreating").hide();
                    totalfilecount = 0;
                    filecount = 0;
                    $scope.UniqueidsfrBlankAsset = [];
                    saveBlankAssetsCalled = false;
                    $scope.uploadingInProgress = false;
                } else {
                    $scope.SaveAsset = [];
                    TotalCount = TotalCount + 1;
                    if (totalfilecount == filecount) {
                        $('#CreateNewTaskBtn').removeClass('disabled');
                        $('#CreateNewTask').css('display', 'none');
                        $('#pickassetinTaskCreation').css('display', 'none');
                        $('#createblankAsset').css('display', 'none');
                        $('#pickassetinTaskCreation').removeAttr('disabled', 'disabled');
                        $('#pickassetinTaskCreation').css('pointer-events', 'auto');
                        $('#pickassetinTaskCreation').css('display', 'block');
                        $('#CreateNewTaskBtn').removeAttr('disabled', 'disabled');
                        $('#CreateNewTaskBtn').css('pointer-events', 'auto');
                        totalfilecount = 0;
                        filecount = 0;
                        $scope.UniqueidsfrBlankAsset = [];
                        $scope.Fileids = [];
                        var FileAttributes = {
                            "FileID": [],
                            "AttributeDataList": [],
                            "AssetType": []
                        };
                        $scope.uploadingInProgress = false;
                        if ($scope.TaskActionFor == 1) RereshTaskObj($scope.returnObj);
                        else RereshTaskObj($scope.returnObj);
                        $scope.FileList = [];
                        $scope.SelectedTaskLIstID = 0;
                        $scope.AttachmentFilename = [];
                        refreshModel();
                        timerObj.subtasklistcount = $timeout(function () {
                            $scope.ApplySubTaskListCountDetails();
                        }, 10);
                        $scope.returnObj = [];
                        $scope.newTaskid = 0;
                        $scope.addtaskfromGenAttachments = false;
                        $scope.addfromComputer = false;
                        $scope.IsaddAssetsfromComputer = false;
                        $('#totalAssetActivityAttachProgresstask .bar').css("width", Math.round((0 * 100)) + "%");
                        $scope.TaskCreationAttachAsset.Dhtml = '';
                        $("#assetassettaskentityattachassetdata").empty();
                        closecreatetaskpopup();
                        closecreatetaskpopup();
                        $("#taskcreating").hide();
                        NotifySuccess($translate.instant('LanguageContents.Res_4820.Caption'));
                        var str = $('#taskuploaded').text().replace('Upload Successful.', '');
                        $('#taskuploaded').text(str);
                        $scope.uploadingInProgress = false;
                    }
                }
            });
        }
        $scope.fieldsfrDet = {
            usersID: ''
        };
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fieldsfrDet, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.tree = {};
        $scope.fieldKeys = [];
        $scope.options = {};
        $scope.fileguid = '';
        $scope.UniqueidsfrBlankAsset = [];
        $scope.PopupClassApply = function () {
            return $scope.TaskBriefDetails.taskpoupClass;
        }
        $scope.processingsrcobj = {
            processingid: $scope.TaskBriefDetails.taskID,
            processingplace: "task",
            processinglock: $scope.IsLock
        };
        $scope.AssetfileTemplateLoading = "Null";
        $scope.AssetTaskentityselection = "Null";
        $scope.SetCurrentTabid = function (activecalss, deactiveclass) {
            $("#li" + activecalss + "").removeClass('active');
            $("#" + activecalss + "").removeClass('tab-pane active');
            $("#" + activecalss + "").removeClass('tab-pane');
            $("#li" + activecalss + "").addClass('active');
            $("#" + activecalss + "").addClass('tab-pane active');
            $("#li" + deactiveclass + "").removeClass('active');
            $("#" + deactiveclass + "").removeClass('tab-pane active');
            $("#" + deactiveclass + "").removeClass('tab-pane');
            $("#" + deactiveclass + "").addClass('tab-pane');
        }

        function refreshAssetobjects() {
            $scope.PageNoobj = {
                pageno: 1
            };
            $scope.TaskStatusObj = [{
                "Name": $translate.instant('LanguageContents.Res_4857.Caption'),
                ID: 1
            }, {
                "Name": $translate.instant('LanguageContents.Res_4811.Caption'),
                ID: 2
            }, {
                "Name": $translate.instant('LanguageContents.Res_663.Caption'),
                ID: 3
            }];
            $scope.SettingsDamAttributes = {};
            $scope.OrderbyObj = [{
                "Name": "Name (Ascending)",
                ID: 1
            }, {
                "Name": "Name (Descending)",
                ID: 2
            }, {
                "Name": "Creation date (Ascending)",
                ID: 3
            }, {
                "Name": "Creation date (Descending)",
                ID: 4
            }];
            $scope.FilterStatus = {
                filterstatus: 1
            };
            $scope.DamViewName = {
                viewName: "Thumbnail"
            };
            $scope.OrderbyName = {
                orderbyname: "Creation date (Descending)"
            };
            $scope.OrderBy = {
                order: 4
            };
            $scope.AssetSelectionFiles = [];
        }
        refreshAssetobjects();
        $scope.AttachAsset = function () {
            if ($scope.processingsrcobj.processingplace == "task") {
                $scope.$broadcast('CreateAssetAttach', $stateParams.ID);
            }
        }
        $scope.$on('CallBackAttachtak', function (event, ID) {
            refreshAssetobjects();
            GetEntityTaskAttachmentinfo($scope.TaskBriefDetails.taskID);
            timerObj.reloadasset = $timeout(function () {
                $scope.processingsrcobj.processingid = ID;
                $scope.processingsrcobj.processingplace = "task";
                $scope.$broadcast('ReloadAssetView');
            }, 100);
        });
        $scope.$on('mui_CallBackAttachtakdraw', function (event, ID) {
            timerObj.attachasset = $timeout(function () {
                var html = '';
                $("#assetfromattachment").html(html);
                LoadThumbnailView('Task');
            }, 50);
        });
        $scope.$on('CallBackAttachtakdrawFromDam', function (event, ID) {
            timerObj.loadthumpnailview = $timeout(function () {
                var html = '';
                $("#assetassettaskentityattachassetdata").html(html);
                $scope.TaskCreationAttachAsset.DAssetSelectionFiles = ID.AssetSelectionFiles;
                $scope.TaskCreationAttachAsset.DAssetFiles = ID.AssetFiles;
                $scope.TaskCreationAttachAsset.DAssetDynamicData = ID.AssetDynamicData;
                LoadThumbnailView('Task');
            }, 50);
        });
        $scope.removeasset = function (assetid) {
            $("#assetfromattachment").children("#Asset_" + assetid + "").remove();
            if ($("#assetfromattachment > div").length == 0) {
                $scope.addtaskfromGenAttachments = false;
                $scope.TaskCreationAttachAsset.Dhtml = '';
            } else {
                $scope.TaskCreationAttachAsset.Dhtml = '';
                $scope.TaskCreationAttachAsset.Dhtml = $("#assetfromattachment").html();
            }
            $scope.DamAssetTaskselectionFiles.AssetSelectionFiles.splice($.inArray(assetid, $scope.DamAssetTaskselectionFiles.AssetSelectionFiles), 1);
            $scope.TaskCreationAttachAsset.DAssetSelectionFiles.splice($.inArray(assetid, $scope.TaskCreationAttachAsset.DAssetSelectionFiles), 1);
            var remainRecord = [];
            var remainRecorddata = [];
            remainRecord = $.grep($scope.TaskCreationAttachAsset.DAssetFiles, function (e) {
                return e.AssetUniqueID == assetid;
            });
            if (remainRecord.length > 0) {
                $scope.TaskCreationAttachAsset.DAssetFiles.splice($.inArray(remainRecord[0], $scope.TaskCreationAttachAsset.DAssetFiles), 1);
            }
            remainRecorddata = $.grep($scope.TaskCreationAttachAsset.DAssetDynamicData, function (e) {
                return e.ID == assetid;
            });
            if (remainRecorddata.length > 0) {
                $scope.TaskCreationAttachAsset.DAssetDynamicData.splice($.inArray(remainRecorddata[0], $scope.TaskCreationAttachAsset.DAssetDynamicData), 1);
            }
        }

        function GetEntityTaskAttachmentinfo(taskID) {
            TaskCreationService.GetEntityTaskAttachmentinfo(taskID).then(function (TaskAttachmentinfo) {
                if (TaskAttachmentinfo.Response != null) {
                    $scope.TaskBriefDetails.Totalassetcount = TaskAttachmentinfo.Response[0].totalcount;
                    $scope.TaskBriefDetails.Totalassetsize = parseSize(TaskAttachmentinfo.Response[0].totalfilesize);
                }
            });
        }
        $scope.onDamtypechange = function (fileID) {
            if ($scope.assetfields['assettype_' + fileID] != null) {
                var oldAssettype = $scope.oldfiledetails["AssetType_" + fileID];
                var newAssettype = parseInt($scope.assetfields['assettype_' + fileID].id);
                TaskCreationService.GetAssetCategoryTree(newAssettype).then(function (data) {
                    if (data.Response != null) {
                        $scope.AssetCategory["AssetCategoryTree_" + fileID] = JSON.parse(data.Response);
                        $scope.AssetCategory["filterval_" + fileID] = '';
                        var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + fileID + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + fileID + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                        $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                    }
                });
                TaskCreationService.GetAssetCategoryPathInAssetEdit(newAssettype).then(function (datapath) {
                    if (datapath.Response != null) {
                        if (datapath.Response[1].AssetTypeIDs.length > 0) {
                            $scope.AssetCategory["treestructure_" + fileID] = datapath.Response[0].CategoryPath;
                        } else $scope.assetfields["assettype_" + fileID] = '';
                    }
                });
                $scope.AssetCategory['showcategroytext_' + fileID] = true;
                $('#div_' + fileID).html($compile("")($scope));
                $(".testdrop").select2("destroy").select2({});
                $('#div_' + fileID).html('');
                if (oldAssettype != '') {
                    TaskCreationService.GetDamAttributeRelation(oldAssettype).then(function (attrList) {
                        var attrreldata = attrList.Response;
                        var ind = FileAttributes.FileID.indexOf(fileID);
                        FileAttributes.FileID.splice(ind, 1);
                        FileAttributes.AttributeDataList.splice(ind, 1);
                        FileAttributes.AssetType.splice(ind, 1);
                        for (var i = 0; i < attrreldata.length; i++) {
                            if (attrreldata[i].TypeID == 1) {
                                if (attrreldata[i].ID != 70) {
                                    if (attrreldata[i].ID == SystemDefiendAttributes.Name) {
                                        delete $scope.assetfields["TextSingleLine_" + fileID + attrreldata[i].ID];
                                    } else {
                                        delete $scope.assetfields["TextSingleLine_" + fileID + attrreldata[i].ID];
                                    }
                                }
                            } else if (attrreldata[i].TypeID == 3) {
                                if (attrreldata[i].ID == SystemDefiendAttributes.Owner) {
                                    delete $scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID];
                                } else if (attrreldata[i].ID == SystemDefiendAttributes.FiscalYear) {
                                    delete $scope.OptionObj["option_" + fileID + attrreldata[i].ID];
                                    delete $scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID];
                                } else {
                                    delete $scope.OptionObj["option_" + fileID + attrreldata[i].ID];
                                    delete $scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID];
                                }
                            } else if (attrreldata[i].TypeID == 2) {
                                delete $scope.assetfields["TextMultiLine_" + fileID + attrreldata[i].ID];
                            } else if (attrreldata[i].TypeID == 4) {
                                delete $scope.OptionObj["option_" + fileID + attrreldata[i].ID];
                                delete $scope.assetfields["ListMultiSelection_" + fileID + attrreldata[i].ID];
                            } else if (attrreldata[i].AttributeTypeID == 12) {
                                $scope.Dropdown = {};
                            } else if (attrreldata[i].AttributeTypeID == 13) {
                                $scope.PercentageVisibleSettings = {};
                                $scope.DropDownTreePricing = {};
                            } else if (attrreldata[i].AttributeTypeID == 10) {
                                $scope.items = [];
                            } else if (attrreldata[i].AttributeTypeID == 7) {
                                $scope.treesrcdirec = {};
                                $scope.treePreviewObj = {};
                                $scope.treeNodeSelectedHolder = [];
                            }
                        }
                    });
                }
                //if (FileAttributes.FileID.length == 0) {
                LoadAssetMetadata(fileID, (newAssettype == undefined || newAssettype == 0 ? oldAssettype : newAssettype));
                //}
                $scope.addfromComputer = true;
            }
        }
        $scope.dyn_ContAsset = '';
        $scope.fieldoptions = [];
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.assetfields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });
        }
        $scope.DAMattributesRelationList = [];
        $scope.AttributeData = [];
        var FileAttributes = {
            "FileID": [],
            "AttributeDataList": [],
            "AssetType": []
        };
        $scope.deleteAttachedFiles = function (fileObject) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2027.Caption'), function (result) {
                if (result) {
                    timerObj.deleteattachment = $timeout(function () {
                        if (fileObject.Extension != "link") {
                            var attachmentDelete = $.grep($scope.AttachmentFilename, function (e) {
                                return e.FileGuid == fileObject.FileGuid
                            });
                            $scope.AttachmentFilename.splice($.inArray(attachmentDelete[0], $scope.AttachmentFilename), 1);
                            var fileToDelete = $.grep($scope.FileList, function (e) {
                                return e.FileGuid == fileObject.FileGuid
                            });
                            $scope.FileList.splice($.inArray(fileToDelete[0], $scope.FileList), 1);
                            if (attachmentDelete[0].ID > 0) {
                                TaskCreationService.DeleteAttachments(attachmentDelete[0].ID).then(function (data) {
                                    if (data != null && data.Response != false) {
                                        NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                                        ReloadTaskAttachments($scope.SelectedTaskID);
                                    }
                                });
                            }
                        } else {
                            if (fileObject.ID == 0) {
                                var attachmentDelete = $.grep($scope.AttachmentFilename, function (e) {
                                    return e.LinkID == fileObject.LinkID
                                });
                                $scope.AttachmentFilename.splice($.inArray(attachmentDelete[0], $scope.AttachmentFilename), 1);
                            } else if (fileObject.ID > 0) {
                                var attachmentDelete = $.grep($scope.AttachmentFilename, function (e) {
                                    return e.ID == fileObject.ID
                                });
                                $scope.AttachmentFilename.splice($.inArray(attachmentDelete[0], $scope.AttachmentFilename), 1);
                                TaskCreationService.DeleteLinkByID(attachmentDelete[0].ID).then(function (data) {
                                    if (data != null && data.Response != false) {
                                        NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                                        ReloadTaskAttachments($scope.SelectedTaskID);
                                    }
                                });
                            }
                        }
                    }, 100);
                }
            });
        }

        function LoadAssetMetadata(fileID, newAssettype) {
            TaskCreationService.GetDamAttributeRelation(newAssettype).then(function (attrList) {
                var attrreldata = attrList.Response;
                var newHTML = '';
                FileAttributes.FileID.push(fileID);
                FileAttributes.AttributeDataList.push(attrreldata);
                FileAttributes.AssetType.push(newAssettype);
                for (var i = 0; i < attrreldata.length; i++) {
                    if (attrreldata[i].AttributeTypeID == 1) {
                        if (attrreldata[i].ID != 70) {
                            if (attrreldata[i].ID == SystemDefiendAttributes.Name) {
                                newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + fileID + attrreldata[i].ID + "\" id=\"TextSingleLine_" + fileID + attrreldata[i].ID + "\" placeholder=\"" + attrreldata[i].Caption + "\"></div></div>";
                                $scope.assetfields["TextSingleLine_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
                            } else {
                                newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + fileID + attrreldata[i].ID + "\" id=\"TextSingleLine_" + fileID + attrreldata[i].ID + "\" placeholder=\"" + attrreldata[i].Caption + "\"></div></div>";
                                $scope.assetfields["TextSingleLine_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
                            }
                        }
                    } else if (attrreldata[i].AttributeTypeID == 3) {
                        if (attrreldata[i].ID == SystemDefiendAttributes.Owner) {
                            newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"> <input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.ListSingleSelection_" + fileID + attrreldata[i].ID + "\"  id=\"ListSingleSelection_" + fileID + attrreldata[i].ID + "\" dirownernameautopopulate placeholder=\"" + attrreldata[i].Caption + "\"></div></div>";
                            $scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID] = $scope.OwnerList[0].UserName;
                        } else if (attrreldata[i].ID == SystemDefiendAttributes.FiscalYear) {
                            $scope.OptionObj["option_" + fileID + attrreldata[i].ID] = attrreldata[i].Options;
                            newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].AttributeID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + fileID + attrreldata[i].ID + "\"  id=\"ListSingleSelection_" + fileID + attrreldata[i].ID + "\"> <option value=\"\"> Select " + attrreldata[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + fileID + attrreldata[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                            $scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
                        } else {
                            $scope.OptionObj["option_" + fileID + attrreldata[i].ID] = attrreldata[i].Options;
                            newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + fileID + attrreldata[i].ID + "\"  id=\"ListSingleSelection_" + fileID + attrreldata[i].ID + "\"> <option value=\"\"> Select " + attrreldata[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + fileID + attrreldata[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                            $scope.assetfields["ListSingleSelection_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
                        }
                    } else if (attrreldata[i].AttributeTypeID == 2) {
                        if (attrreldata[i].ID != 3) newHTML += "<div class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"assetfields.TextMultiLine_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].ID + "\" name=\"assetfields.TextMultiLine_" + fileID + attrreldata[i].ID + "\" ng-model=\"assetfields.TextMultiLine_" + fileID + attrreldata[i].ID + "\" id=\"TextMultiLine_" + fileID + attrreldata[i].ID + "\" placeholder=\"" + attrreldata[i].Caption + "\" rows=\"3\"></textarea></div></div>";
                        $scope.assetfields["TextMultiLine_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
                    } else if (attrreldata[i].AttributeTypeID == 4) {
                        $scope.OptionObj["option_" + fileID + attrreldata[i].ID] = attrreldata[i].Options;
                        newHTML += "<div class=\"control-group attachmentMultiselect\"><label class=\"control-label\" for=\"assetfields.ListMultiSelection_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"> <select class=\"multiselect\" multiple=\"multiple\"  multiselect-dropdown data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListMultiSelection_" + fileID + attrreldata[i].ID + "\"  id=\"ListMultiSelection_" + fileID + attrreldata[i].ID + "\" ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + fileID + attrreldata[i].ID + "\"  > </select></div></div>";
                        $scope.assetfields["ListMultiSelection_" + fileID + attrreldata[i].ID] = attrreldata[i].DefaultValue;
                    } else if (attrreldata[i].AttributeTypeID == 5 && attrreldata[i].AttributeID != SystemDefiendAttributes.ApproveTime && attrreldata[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        $scope.calanderopened = false;
                        $scope.MinValue = attrreldata[i].MinValue;
                        $scope.MaxValue = attrreldata[i].MaxValue;
                        $scope.assetfields["DatePartMinDate_" + attrreldata[i].ID] = new Date.create();
                        $scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.assetfields["DatePartMinDate_" + attrreldata[i].ID].setDate($scope.assetfields["DatePartMinDate_" + attrreldata[i].ID].getDate() + ($scope.MinValue + 1));
                        } else {
                            $scope.assetfields["DatePartMinDate_" + attrreldata[i].ID].setDate($scope.assetfields["DatePartMinDate_" + attrreldata[i].ID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID].setDate($scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID].getDate() + ($scope.MaxValue - 1));
                        } else {
                            $scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID].setDate($scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID].getDate() + 100000);
                        }
                        var temp = $scope.HolidayListCalculation($scope.assetfields["DatePartMinDate_" + attrreldata[i].ID], $scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID]);
                        $scope.assetfields["DatePartMinDate_" + attrreldata[i].ID] = (temp.MinDate);
                        $scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID] = (temp.MaxDate);
                        newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DatePart_" + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + "</label><div class=\"controls\"><span class='editable'>{{assetfields.DateTime_" + fileID + attrreldata[i].ID + "}}</span><input type=\"text\" class=\"txtbx DatePartctrl\" " + attrreldata[i].ID + "\" id=\"DatePart_" + fileID + attrreldata[i].ID + "\" placeholder=\"" + attrreldata[i].Caption + "\" ng-click=\"Calanderopen($event,'Date_" + fileID + attrreldata[i].ID + "')\" min-date=\"assetfields.DatePartMinDate_" + attrreldata[i].ID + "\" max-date=\"assetfields.DatePartMaxDate_" + attrreldata[i].ID + "\" datepicker-popup=\"{{format}}\"  is-open=\"Date_" + fileID + attrreldata[i].ID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-change=\"setTimeout(changeduedate_changed(assetfields.DatePart_" + fileID + attrreldata[i].ID + ",assetfields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",assetfields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "," + fileID + attrreldata[i].ID + "),1000)\" ng-model=\"assetfields.DatePart_" + fileID + attrreldata[i].ID + "\"></div></div>";
                        $scope.assetfields["DatePart_" + fileID + attrreldata[i].ID] = "";
                        $scope.assetfields["assetfields.DatePart_Calander_Open" + fileID + attrreldata[i].ID] = false;
                    } else if (attrreldata[i].AttributeTypeID == 16) {
                        $scope.TaskDueDate1 = false;
                        $scope.MinValue = attrreldata[i].MinValue;
                        $scope.MaxValue = attrreldata[i].MaxValue;
                        $scope.assetfields["DatePartMinDate_" + attrreldata[i].ID] = new Date.create();
                        $scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.assetfields["DatePartMinDate_" + attrreldata[i].ID].setDate($scope.assetfields["DatePartMinDate_" + attrreldata[i].ID].getDate() + ($scope.MinValue + 1));
                        } else {
                            $scope.assetfields["DatePartMinDate_" + attrreldata[i].ID].setDate($scope.assetfields["DatePartMinDate_" + attrreldata[i].ID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID].setDate($scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID].getDate() + ($scope.MaxValue - 1));
                        } else {
                            $scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID].setDate($scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID].getDate() + 100000);
                        }
                        var temp = $scope.HolidayListCalculation($scope.assetfields["DatePartMinDate_" + attrreldata[i].ID], $scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID]);
                        $scope.assetfields["DatePartMinDate_" + attrreldata[i].ID] = (temp.MinDate);
                        $scope.assetfields["DatePartMaxDate_" + attrreldata[i].ID] = (temp.MaxDate);                                                                                                                                                                                                                                                                                                                                                                  
                        newHTML += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DatePart_" + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Caption + "</label><div class=\"controls\"><span class='editable'>{{assetfields.DateTime_" + fileID + attrreldata[i].ID + "}}</span><input type=\"text\" class=\"txtbx DatePartctrl\" " + attrreldata[i].ID + "\" id=\"DatePart_" + fileID + attrreldata[i].ID + "\" ng-click=\"Calanderopen($event,'DatePart__Calander_Open" + fileID + attrreldata[i].ID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart__Calander_Open" + fileID + attrreldata[i].ID + "\" min-date=\"assetfields.DatePartMinDate_" + attrreldata[i].ID + "\" max-date=\"assetfields.DatePartMaxDate_" + attrreldata[i].ID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + attrreldata[i].Caption + "\" ng-change=\"setTimeout(changeduedate_changed(assetfields.DatePart_" + fileID + attrreldata[i].ID + ",assetfields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",assetfields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "," + fileID + attrreldata[i].ID + "),1000)\" ng-model=\"assetfields.DatePart_" + fileID + attrreldata[i].ID + "\"></div></div>";
                        $scope.assetfields["assetfields.DatePart_Calander_Open" + fileID + attrreldata[i].ID] = false;                                                                                                                                                                                                                                                                                                                                                                                                    
                    } else if (attrreldata[i].AttributeTypeID == 12) {
                        newHTML += "<div class\"control-group pull-left\">";
                        var totLevelCnt1 = attrreldata[i].Levels.length;
                        for (var j = 0; j < totLevelCnt1; j++) {
                            $scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = {
                                multiple: false,
                                formatResult: function (item) {
                                    return item.text;
                                },
                                formatSelection: function (item) {
                                    return item.text;
                                }
                            };
                            if (totLevelCnt1 == 1) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = true;                                                                                                
                                $scope.DropDownTreeOptionValues["Options" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = [];
                                $scope.DropDownTreeOptionValues["Options" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = JSON.parse(attrreldata[i].tree).Children;
                                $scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + (j + 1)].multiple = true;

                                newHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                newHTML += "<select multiple ui-select2 =\"Dropdown.OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 ,'" + fileID + "')\" ng-model=\"fields.MultiSelectDropDown_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" >";
                                newHTML += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                newHTML += "</select></div></div>";
                                $scope.setFieldKeys();

                            } else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = true;                                
                                if (j == 0) {
                                    $scope.DropDownTreeOptionValues["Options" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = [];
                                    $scope.DropDownTreeOptionValues["Options" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = JSON.parse(attrreldata[i].tree).Children;
                                    $scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].multiple = false;
                                    newHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                    newHTML += "<select ui-select2 =\"Dropdown.OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 ,'" + fileID + "')\" ng-model=\"fields.MultiSelectDropDown_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" >";
                                    newHTML += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                    newHTML += "</select></div></div>";
                                } else {
                                    $scope.DropDownTreeOptionValues["Options" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = [];
                                    if (j == (attrreldata[i].Levels.length - 1)) {
                                        $scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].multiple = true;
                                        newHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                        newHTML += "<select multiple ui-select2 =\"Dropdown.OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 ,'" + fileID + "')\" ng-model=\"fields.MultiSelectDropDown_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" >";
                                        newHTML += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                        newHTML += "</select></div></div>";
                                    } else {
                                        $scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)].multiple = false;
                                        newHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                        newHTML += "<select ui-select2 =\"Dropdown.OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 ,'" + fileID + "')\" ng-model=\"fields.MultiSelectDropDown_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" >";
                                        newHTML += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                        newHTML += "</select></div></div>";
                                    }
                                }
                            }
                        }
                        newHTML += "</div>";
                    } else if (attrreldata[i].AttributeTypeID == 10) {
                        $scope.items = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrreldata[i].AttributeID] = true;
                        $scope.OptionObj["option_" + fileID + attrreldata[i].AttributeID] = attrreldata[i].Options;
                        $scope.setoptions();
                        $scope.items.push({
                            startDate: [],
                            endDate: [],
                            comment: '',
                            sortorder: 0,
                            fileid: fileID
                        });
                        $scope.assetfields["Period_" + fileID] = $scope.items;
                        $scope.MinValue = attrreldata[i].MinValue;
                        $scope.MaxValue = attrreldata[i].MaxValue;
                        $scope.assetfields["DatePartMinDate_" + attrreldata[i].AttributeID] = new Date.create();
                        $scope.assetfields["DatePartMaxDate_" + attrreldata[i].AttributeID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.assetfields["DatePartMinDate_" + attrreldata[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + attrreldata[i].AttributeID].getDate() + ($scope.MinValue + 1));
                        } else {
                            $scope.assetfields["DatePartMinDate_" + attrreldata[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + attrreldata[i].AttributeID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.assetfields["DatePartMaxDate_" + attrreldata[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + attrreldata[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                        } else {
                            $scope.assetfields["DatePartMaxDate_" + attrreldata[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + attrreldata[i].AttributeID].getDate() + 100000);
                        }
                        var temp = $scope.HolidayListCalculation($scope.assetfields["DatePartMinDate_" + attrreldata[i].AttributeID], $scope.assetfields["DatePartMaxDate_" + attrreldata[i].AttributeID]);
                        $scope.assetfields["DatePartMinDate_" + attrreldata[i].AttributeID] = (temp.MinDate);
                        $scope.assetfields["DatePartMaxDate_" + attrreldata[i].AttributeID] = (temp.MaxDate);
                        newHTML += "    <div class=\"control-group\"><label for=\"assetfields.TextSingleLine_ " + fileID + attrreldata[i].AttributeID + "\" class=\"control-label\">" + attrreldata[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in assetfields.Period_" + fileID + "\" ng-form=\"subForm\">";
                        newHTML += "<div class=\"row-fluid\"><div class=\"inputHolder span5 noBorder\">";
                        newHTML += "<input class=\"sdate margin-bottom0x Period_" + fileID + attrreldata[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\" id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-change=\"changeperioddate_changed(item.startDate,assetfields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",assetfields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + ",'StartDate')\" ng-model=\"item.startDate\" ng-click=\"Calanderopen($event,'Start_Date_" + fileID + attrreldata[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Start_Date_" + fileID + attrreldata[i].AttributeID + "\" min-date=\"assetfields.DatePartMinDate_" + attrreldata[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + attrreldata[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- Start date --\"/><input class=\"edate margin-bottom0x Period_" + fileID + attrreldata[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\" type=\"text\" name=\"enddate\" id=\"item.endDate\" ng-change=\"changeperioddate_changed(item.endDate,assetfields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + ",assetfields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + ",'EndDate')\" ng-model=\"item.endDate\" ng-click=\"Calanderopen($event,'End_Date_" + fileID + attrreldata[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"End_Date_" + fileID + attrreldata[i].AttributeID + "\" min-date=\"assetfields.DatePartMinDate_" + attrreldata[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + attrreldata[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- End date --\"/><input class=\"dateComment txtbx\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + attrreldata[i].Caption + " Comment --\" />";
                        newHTML += "</div><div class=\"buttonHolder span1\" ng-show=\"$first==false\">";
                        newHTML += "<a ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew('" + fileID + "')\">[Add " + attrreldata[i].Caption + "]</a></div></div>";
                        $scope.assetfields["Period_" + fileID + attrreldata[i].AttributeID] = "";
                        $scope.fields["assetfields.DatePart_Calander_Open" + "item.startDate"] = false;
                        $scope.fields["assetfields.DatePart_Calander_Open" + "item.endDate"] = false;
                    } else if (attrreldata[i].AttributeTypeID == 13) {
                        $scope.PercentageVisibleSettings["AttributeId_Levels_" + fileID + attrreldata[i].AttributeID.toString() + ""] = true;
                        $scope.DropDownTreePricing["AttributeId_Levels_" + fileID + attrreldata[i].AttributeID.toString() + ""] = attrreldata[i].DropDownPricing;
                        newHTML += "<div drowdowntreepercentagemultiselectionasset data-purpose='entity'  data-fileid=" + fileID.toString() + "  data-attributeid=" + attrreldata[i].AttributeID.toString() + "></div>";
                    } else if (attrreldata[i].AttributeTypeID == 8) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrreldata[i].AttributeID] = true;
                        newHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileID + attrreldata[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + fileID + attrreldata[i].AttributeID + "\">" + attrreldata[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + fileID + attrreldata[i].AttributeID + "\" ng-model=\"assetfields.TextSingleLine_" + fileID + attrreldata[i].AttributeID + "\" id=\"TextSingleLine_" + fileID + attrreldata[i].AttributeID + "\" placeholder=\"" + attrreldata[i].PlaceHolderValue + "\"></div></div>";
                        $scope.assetfields["TextSingleLine_" + fileID + attrreldata[i].AttributeID] = attrreldata[i].DefaultValue;
                    } else if (attrreldata[i].AttributeTypeID == 17) {
                        $scope.assetfields["ListTagwords_" + fileID + attrreldata[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrreldata[i].AttributeID] = true;
                        $scope.OptionObj["tagoption_" + fileID + attrreldata[i].AttributeID] = [];
                        $scope.setoptions();
                        $scope.tempscope = [];
                        newHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileID + attrreldata[i].AttributeID + "\" class=\"control-group\">";
                        newHTML += "<label class=\"control-label\" for=\"assetfields.ListTagwords_ " + fileID + attrreldata[i].AttributeID + "\">" + attrreldata[i].Caption + " </label>";
                        newHTML += "<div class=\"controls\">";
                        newHTML += "<directive-tagwords item-attrid = \"" + fileID + attrreldata[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"assetfields.ListTagwords_" + fileID + attrreldata[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                        newHTML += "</div></div>";
                        if (attrreldata[i].InheritFromParent) { } else { }
                    } else if (attrreldata[i].AttributeTypeID == 6) {
                        var totLevelCnt = attrreldata[i].Levels.length;
                        for (var j = 0; j < attrreldata[i].Levels.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.Dropdown["OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = {
                                multiple: false,
                                formatResult: function (item) {
                                    return item.text;
                                },
                                formatSelection: function (item) {
                                    return item.text;
                                }
                            };
                            
                            if (j == 0) {
                                $scope.DropDownTreeOptionValues["Options" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = [];
                                $scope.DropDownTreeOptionValues["Options" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = JSON.parse(attrreldata[i].tree).Children;
                                newHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DropDown_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Levels[j].LevelName + " </label><div class=\"controls\"><select ui-select2=\"Dropdown.OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6,'" + fileID + "' )\" id=\"DropDownTree_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" ng-model=\"fields.DropDown_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" >";
                                newHTML += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                newHTML += "</select></div></div>";
                                
                            } else {
                                $scope.DropDownTreeOptionValues["Options" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = [];
                                $scope.DropDownTreeOptionValues["Options" + fileID + attrreldata[i].AttributeID + "_" + (j + 1)] = JSON.parse(attrreldata[i].tree).Children;
                                newHTML += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DropDown_ " + fileID + attrreldata[i].ID + "\">" + attrreldata[i].Levels[j].LevelName + " </label><div class=\"controls\"><select ui-select2=\"Dropdown.OptionValues" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrreldata[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6,'" + fileID + "' )\" id=\"DropDownTree_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" ng-model=\"fields.DropDown_" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" >";
                                newHTML += "<option ng-repeat=\"Aitem in DropDownTreeOptionValues.Options" + fileID + attrreldata[i].AttributeID + "_" + (j + 1) + "\" value=\"{{Aitem.id}}\">{{Aitem.Caption}}</option>";
                                newHTML += "</select></div></div>";
                            }
                        }
                    } else if (attrreldata[i].AttributeTypeID == 7) {
                        $scope.assetfields["Tree_" + fileID + attrreldata[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + fileID + attrreldata[i].AttributeID] = true;
                        $scope.treesrcdirec["Attr_" + fileID + attrreldata[i].AttributeID] = JSON.parse(attrreldata[i].tree).Children;
                        if ($scope.treesrcdirec["Attr_" + fileID + attrreldata[i].AttributeID].length > 0) {
                            treeTextVisbileflag = false;
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + fileID + attrreldata[i].AttributeID])) {
                                $scope.treePreviewObj["Attr_" + fileID + attrreldata[i].AttributeID] = true;
                            } else $scope.treePreviewObj["Attr_" + fileID + attrreldata[i].AttributeID] = false;
                        } else {
                            $scope.treePreviewObj["Attr_" + fileID + attrreldata[i].AttributeID] = false;
                        }
                        newHTML += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + fileID + attrreldata[i].AttributeID + '\" class="control-group treeNode-control-group">';
                        newHTML += '    <label class="control-label">' + attrreldata[i].Caption + '</label>';
                        newHTML += '    <div class="controls treeNode-controls">';
                        newHTML += '        <div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + fileID + attrreldata[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + fileID + attrreldata[i].AttributeID + '"></div>';
                        newHTML += '        <div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + fileID + attrreldata[i].AttributeID + '">';
                        newHTML += '            <span ng-if="doing_async">...loading...</span>';
                        newHTML += '            <abn-tree tree-filter="filterValue_' + fileID + attrreldata[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + fileID + attrreldata[i].AttributeID + '\" accessable="' + attrreldata[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                        newHTML += '        </div>';
                        newHTML += '        <div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + fileID + attrreldata[i].AttributeID + '\">';
                        newHTML += '            <div class="controls">';
                        newHTML += '                <eu-tree tree-data=\"treesrcdirec.Attr_' + fileID + attrreldata[i].AttributeID + '\" node-attributeid="' + attrreldata[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                        newHTML += '        </div></div>';
                        newHTML += '</div></div>';
                    }
                }
                $scope.oldfiledetails["AssetType_" + fileID] = newAssettype;
                $('#div_' + fileID).append($compile(newHTML)($scope));
            });
        }

        function RereshTaskObj(taskObj) {
            if ($scope.TaskActionFor == 1) {
                var taskListResObj = $.grep($scope.groups, function (e) {
                    return e.ID == taskObj.TaskListID;
                });
                if (taskListResObj.length > 0) {
                    if (taskListResObj[0].TaskList != null) {
                        var taskResObj = $.grep(taskListResObj[0].TaskList, function (e) {
                            return e.Id == taskObj.Id;
                        });
                        if (taskResObj.length > 0) {
                            UpdateTaskObject(taskResObj, taskObj)
                        } else {
                            taskListResObj[0].TaskList.push(taskObj);
                        }
                    } else {
                        taskListResObj[0].TaskList.push(taskObj);
                    }
                }
            } else {
                var EntityTaskObj = $.grep($scope.groupbySubLevelTaskObj, function (e) {
                    return e.EntityID == taskObj.EntityID;
                });
                if (EntityTaskObj.length > 0) {
                    var taskListGroup = $.grep(EntityTaskObj[0].TaskListGroup, function (e) {
                        return e.ID == taskObj.TaskListID;
                    });
                    if (taskListGroup.length > 0) {
                        var taskObjHolder = $.grep(taskListGroup[0].TaskList, function (e) {
                            return e.Id == taskObj.Id;
                        });
                        if (taskObjHolder.length > 0) {
                            UpdateTaskObject(taskObjHolder, taskObj)
                        } else {
                            taskListGroup[0].TaskList.push(taskObj);
                        }
                    }
                    $scope.ApplySubTaskListCountDetails();
                }
            }

        }

        function dateDiffBetweenDates(dateTo) {
            var _MS_PER_DAY = 1000 * 60 * 60 * 24;
            var dateToday = new Date.create();
            if (dateTo != null && dateTo != undefined && dateTo != "" && dateTo != 0) {
                var utcDateSet = Date.UTC(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate());
            }
            var utcToday = Date.UTC(dateToday.getFullYear(), dateToday.getMonth(), dateToday.getDate());
            return Math.floor((utcDateSet - utcToday) / _MS_PER_DAY);
        }

        function refreshModel() {
            $scope.TaskBriefDetails = {
                taskID: 0,
                taskListUniqueID: 0,
                taskTypeId: 0,
                taskTypeName: "",
                EntityID: 0,
                ownerId: 0,
                taskName: "",
                dueIn: 0,
                dueDate: "",
                status: "",
                statusID: 0,
                taskOwner: "",
                Description: "",
                Note: "",
                taskmembersList: [],
                taskAttachmentsList: [],
                taskProgressCount: "",
                totalTaskMembers: [],
                TaskMembersRoundTripGroup: [],
                taskCheckList: [],
                WorkTaskInprogressStatus: ""
            };
            $scope.FileList = [];
            $scope.AttachmentFilename = [];
            $scope.TaskType = '';
            $scope.FileIdForAssetCategory = "";
            $scope.AssetCategory = {};
        }
        $scope.ContextTaskListID = 0;
        $scope.contextTaskID = 0;
        $scope.ContextMemberID = 0;
        $scope.UploadImagefileTaskCreate = function (attributeId) {
            $scope.UploadAttributeId = attributeId;
            setTimeout($("#pickfilesUploaderAttrTaskCreate").click(), 100);
        }
        $timeout(function () {
            StrartUpload_UploaderAttr();
        }, 100)
        var uploader_Attr = '';
        function StrartUpload_UploaderAttr() {
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                $('.moxie-shim').remove();
                uploader_Attr = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAttrTaskCreate',
                    container: 'filescontainerooTaskCreate',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: amazonURL + cloudsetup.BucketName,
                    multi_selection: false,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }
                });
                uploader_Attr.bind('Init', function (up, params) { });
                uploader_Attr.init();
                uploader_Attr.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_Attr.start();
                });
                uploader_Attr.bind('UploadProgress', function (up, file) { });
                uploader_Attr.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_Attr.bind('FileUploaded', function (up, file, response) {
                    //var fileid = file.id.replace("_", "");
                    //var fileext = "." + file.name.split('.').pop();
                    //var providerresponse = response;
                    //response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    UpdateImagescope(file, response);
                });
                uploader_Attr.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKey = (TenantFilePath + "UploadedImages/Temp/" + file.id + fileext).replace(/\\/g, "\/");
                    uploader_Attr.settings.multipart_params.key = uniqueKey;
                    uploader_Attr.settings.multipart_params.Filename = uniqueKey;
                });
            }
            else {

                $('.moxie-shim').remove();
                 uploader_Attr = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAttrTaskCreate',
                    container: 'filescontainerooTaskCreate',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: 'Handlers/UploadUploaderImage.ashx?Type=Attachment',
                    chunk: '64Kb',
                    multi_selection: false,
                    multipart_params: {}

                });
                uploader_Attr.bind('Init', function (up, params) { });

                uploader_Attr.init();
                uploader_Attr.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_Attr.start();
                });
                uploader_Attr.bind('UploadProgress', function (up, file) { });
                uploader_Attr.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_Attr.bind('FileUploaded', function (up, file, response) {
                    var fileinfo = response.response.split(",");
                    var file = { id: fileinfo[0].split(".")[0], name: fileinfo[0] };
                    UpdateImagescope(file, response);
                });
                uploader_Attr.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
            }
        }
        $scope.ImageFileName = '';

        function UpdateImagescope(file, response) {
            var fileid = file.id.replace("_", "");
            var fileext = "." + file.name.split('.').pop();
            var providerresponse = response;
            response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.response.split(",");
            $scope.ImageFileName = resultArr[0] + extension;
            var PreviewID = "UploaderPreview_" + $scope.UploadAttributeId;
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                $('#' + PreviewID).attr('src', imagesrcpath + 'UploadedImages/Temp/' + $scope.ImageFileName);
            }
            else {
                $('#' + PreviewID).attr('src', imagesrcpath + 'UploadedImages/Temp/' + $scope.ImageFileName);
            }
        }
        $scope.checkSelection = function (assetid, activefileid, event) {
            var checkbox = event.target;
            var assetselection = "";
            assetselection = checkbox.checked == true ? "th-Selection selected" : "th-Selection";
            $scope.AssetListSelection.AssetChkClass["asset_" + assetid + ""] = checkbox.checked == true ? "checkbox checked" : "checkbox";
            $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + assetid + ""] = assetselection;
            $scope.AssetListSelection.AssetSelection["asset_'" + assetid + ""] = checkbox.checked;
            var res = [];
            res = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                return e.AssetUniqueID == assetid;
            });
            if (res.length > 0) {
                DownloadAssets(checkbox, assetid, res[0].FileName, res[0].Extension, res[0].FileGuid, res[0].Category);
            }
            if (checkbox.checked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                    return e == assetid;
                });
                if (remainRecord.length == 0) {
                    $scope.AssetFilesObj.AssetSelection.push(assetid);
                }
                if (res[0].Category == 0) {
                    AddAttachmentsToTask(assetid, activefileid, true);
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                    return e == assetid;
                });
                if (remainRecord.length > 0) {
                    $scope.AssetFilesObj.AssetSelection.splice($.inArray(assetid, $scope.AssetFilesObj.AssetSelection), 1);
                }
                if (res[0].Category == 0) {
                    AddAttachmentsToTask(assetid, activefileid, false);
                }
            }
            if (!checkbox.checked) {
                $scope.$emit('checkselectall', false);
            }
        }

        function AddAttachmentsToTask(assetid, activefileid, ischecked) {
            if (ischecked == true) {
                var response = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                    return e.ActiveFileID == activefileid
                })[0];
                if (response != null) {
                    data = $.grep($scope.AssetFilesObj.AssetDynamicData, function (e) {
                        return e.ID == assetid;
                    });
                    var flagattTask = $.grep($scope.DamAssetTaskselectionFiles.AssetFiles, function (e) {
                        return e.FileGuid == response.FileGuid
                    })[0];
                    if (flagattTask == null) {
                        $scope.DamAssetTaskselectionFiles.AssetFiles.push(response);
                        if (data[0] != undefined) {
                            $scope.DamAssetTaskselectionFiles.AssetDynamicData.push(data[0]);
                        }
                    }
                }
            } else {
                var response = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                    return e.ActiveFileID == activefileid
                })[0];
                var flagattTask = $.grep($scope.DamAssetTaskselectionFiles.AssetFiles, function (e) {
                    return e.FileGuid == response.FileGuid
                })[0];
                var flagdata = $.grep($scope.DamAssetTaskselectionFiles.AssetDynamicData, function (e) {
                    return e.ID == assetid
                })[0];
                $scope.DamAssetTaskselectionFiles.AssetFiles.splice($.inArray(flagattTask, $scope.DamAssetTaskselectionFiles.AssetFiles), 1);
                $scope.DamAssetTaskselectionFiles.AssetDynamicData.splice($.inArray(flagdata, $scope.DamAssetTaskselectionFiles.AssetDynamicData), 1);
            }
        }
        $scope.AddFilesFromDAM = function () {
            if ($scope.Dam_Directory.length > 0) {
                var branch;
                branch = tree.get_selected_branch();
                $scope.MediaBankSettings.Redirectfromdam = true;
                $scope.MediaBankSettings.FolderId = $scope.selectedTree.id;
                $scope.MediaBankSettings.EntityId = $scope.processingsrcobj.processingid;
                $scope.MediaBankSettings.ViewType = $scope.MulitipleFilterStatus;
                $scope.MediaBankSettings.directfromMediaBank = false;
                $scope.MediaBankSettings.ProductionTypeID = $scope.EntityTypeID;
                $scope.MediaBankSettings.FolderName = branch.Caption;
                $location.path("/mui/mediabank");
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4565.Caption'));
            }
        }
        $scope.OpenWorkTask = function (tasktype) {
            if ($scope.DamAssetTaskselectionFiles.AssetFiles.length > 0) {
                if (tasktype == taskType.Asset_Approval_Task || tasktype == taskType.Proof_approval_task) {
                    if ($scope.DamAssetTaskselectionFiles.AssetFiles.length > 1) {
                        bootbox.alert($translate.instant('LanguageContents.Res_4918.Caption'));
                        refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                        return false;
                    }
                }
                var assetid = 0;
                if (tasktype == taskType.Asset_Approval_Task || tasktype == taskType.Proof_approval_task) assetid = $scope.DamAssetTaskselectionFiles.AssetFiles[0].AssetUniqueID;
                assetid = $scope.DamAssetTaskselectionFiles.AssetFiles[0].AssetUniqueID;
                var tempDamAssetSelected = $scope.DamAssetTaskselectionFiles.AssetFiles;
                var tempSelectedAssetDynData = $scope.DamAssetTaskselectionFiles.AssetDynamicData;
                if ($('#icon_selectassets').attr('class') != 'icon-check') {
                    if ($scope.selectall == false) {
                        selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                        selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                    } else {
                        selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                    }
                }
                $scope.DamAssetTaskselectionFiles.AssetFiles = tempDamAssetSelected;
                $scope.DamAssetTaskselectionFiles.AssetDynamicData = tempSelectedAssetDynData;
            } else bootbox.alert($translate.instant('LanguageContents.Res_4608.Caption'));
        }
        $scope.AssetTasktypeswithExt = [];
        $scope.DamExt = [];
        $scope.Fileids = [];
        timerObj.getallExtn = $timeout(function () { GetAllExtensionTypesforDAM() }, 100);

        function LoadThumbnailBlocks() {
            timerObj.loadThumpnailBlock = $timeout(function () {
                var html = '';
                $scope.AssetTaskAttachAssetHtml = "";
                $("#assetassettaskentityattachassetdata").html(html);
                LoadThumbnailView('AssetTask');
                $scope.DamAssetTaskselectionFiles.AssetFiles = [];
                $scope.DamAssetTaskselectionFiles.ThumbnailSettings = [];
                $scope.DamAssetTaskselectionFiles.AssetDynamicData = [];
            }, 100);
        }

        function LoadThumbnailView(CallingFromPlace) {
            if (CallingFromPlace == 'Task') {
                LoadThumbnailViewFromTask();
            } else {
                LoadThumbnailViewFromAsset();
            }
        }

        function LoadThumbnailViewFromAsset() {
            if ($scope.DamAssetTaskselectionFiles.AssetFiles != null) {
                $scope.addtaskfromGenAttachments = true;
                var assets = [];
                assets = $scope.DamAssetTaskselectionFiles.AssetFiles;
                for (var i = 0, asset; asset = assets[i++];) {
                    var rec = [];
                    rec = $.grep($scope.AssetTasktypeswithExt.m_Item1, function (e) {
                        return e.Id == asset.AssetTypeid
                    });
                    var remainAssetFilesRecord = [];
                    remainAssetFilesRecord = $.grep($scope.DamAssetTaskselectionFiles.AssetSelectionFiles, function (e) {
                        return e == asset.AssetUniqueID;
                    });
                    if (remainAssetFilesRecord.length == 0) {
                        if (asset.AssetUniqueID != undefined) {
                            $scope.DamAssetTaskselectionFiles.AssetSelectionFiles.push(asset.AssetUniqueID);
                        }
                    }
                    $scope.AssetTaskAttachAssetHtml += '<div id="Asset_' + asset.AssetUniqueID + '" class="attachmentBox">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="row-fluid">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="span12">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="span2 leftSection">';
                    $scope.AssetTaskAttachAssetHtml += '<i class="icon-ok-sign" style="display:none;"></i>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="fileType-Preview">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="fileType-PreviewDiv">';
                    if (asset.Category == 0) {
                        if (asset.Extension != null) {
                            if (asset.ProcessType > 0) {
                                if (asset.Status == 2) $scope.AssetTaskAttachAssetHtml += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imagesrcpath + 'DAMFiles/Preview/Small_' + asset.FileGuid + '.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 1 || asset.Status == 0) $scope.AssetTaskAttachAssetHtml += '<img class="loadingImg" id=' + asset.ActiveFileID + ' data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset(' + asset.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                else if (asset.Status == 3) {
                                    $scope.AssetTaskAttachAssetHtml += '<img  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                                    $scope.AssetTaskAttachAssetHtml += '<div class="th-Error-preview">';
                                    $scope.AssetTaskAttachAssetHtml += '<span class="errorMsg"><i class="icon-info-sign"></i>';
                                    $scope.AssetTaskAttachAssetHtml += '<span class="errorTxt">Unable to generate Preview</span></span>';
                                    $scope.AssetTaskAttachAssetHtml += '</div>';
                                }
                            } else {
                                $scope.AssetTaskAttachAssetHtml += '<img  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                            }
                        } else $scope.AssetTaskAttachAssetHtml += '<img  src="' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 1) {
                        $scope.AssetTaskAttachAssetHtml += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    } else if (asset.Category == 2) {
                        $scope.AssetTaskAttachAssetHtml += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
                    }
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="progress progress-striped active">';
                    $scope.AssetTaskAttachAssetHtml += '<div style="width: 0%" class="bar"></div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="progressInfo">';
                    $scope.AssetTaskAttachAssetHtml += '<span class="progress-percent"></span>';
                    if (asset.Category == 0) {
                        if (parseSize(asset.Size) == "NaN bytes") {
                            $scope.AssetTaskAttachAssetHtml += '<span class="fileSize">' + (asset.Size) + ' </span>';
                        } else {
                            $scope.AssetTaskAttachAssetHtml += '<span class="fileSize">' + parseSize(asset.Size) + ' </span>';
                        }
                    } else {
                        $scope.AssetTaskAttachAssetHtml += '<span class="fileSize"></span>';
                    }
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="span10 rightSection">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="info">';
                    $scope.AssetTaskAttachAssetHtml += '<span class="name">';
                    if (asset.Category == 0) {
                        $scope.AssetTaskAttachAssetHtml += '<i class="icon-file-alt"></i>';
                    }
                    $scope.AssetTaskAttachAssetHtml += asset.AssetName;
                    $scope.AssetTaskAttachAssetHtml += '</span>';
                    $scope.AssetTaskAttachAssetHtml += '<span class="size" ng-click="removeasset(' + asset.AssetUniqueID + ')">';
                    $scope.AssetTaskAttachAssetHtml += '<i class="icon-remove removefile" data-fileid="' + asset.AssetUniqueID + '"></i>';
                    $scope.AssetTaskAttachAssetHtml += '</span>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="formInfo">';
                    $scope.AssetTaskAttachAssetHtml += '<form class="form-float">';
                    $scope.AssetTaskAttachAssetHtml += '<div class="control-group fullSpace">';
                    $scope.AssetTaskAttachAssetHtml += '<label class="control-label"> Asset type </label>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="controls">';
                    $scope.AssetTaskAttachAssetHtml += '<label class="control-label">' + rec[0] != undefined ? rec[0].damCaption : "" + '</label>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="control-group">';
                    $scope.AssetTaskAttachAssetHtml += '<label class="control-label"> Asset name </label>';
                    $scope.AssetTaskAttachAssetHtml += '<div class="controls">';
                    $scope.AssetTaskAttachAssetHtml += '<label class="control-label">' + asset.AssetName + '</label>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</form>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                    $scope.AssetTaskAttachAssetHtml += '</div>';
                }
                $scope.IsAttachmentPresent = true;
                $("#assetassettaskentityattachassetdata").append($compile($scope.AssetTaskAttachAssetHtml)($scope));
            } else { }
        }

        function LoadThumbnailViewFromTask() {
            if ($scope.TaskCreationAttachAsset.DAssetFiles != null) {
                $scope.addtaskfromGenAttachments = true;
                var assets = [];
                assets = $scope.TaskCreationAttachAsset.DAssetFiles;
                $scope.TaskCreationAttachAsset.Dhtml = '';
                for (var i = 0, asset; asset = assets[i++];) {
                    var rec = [];
                    rec = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                        return e.Id == asset.AssetTypeid
                    });
                    var remainAssetFilesRecord = [];
                    remainAssetFilesRecord = $.grep($scope.TaskCreationAttachAsset.DAssetSelectionFiles, function (e) {
                        return e.AssetID == asset.AssetUniqueID;
                    });
                    if (remainAssetFilesRecord.length == 0) {
                        if (asset.AssetUniqueID != undefined) {
                            $scope.DamAssetTaskselectionFiles.AssetSelectionFiles.push(asset.AssetUniqueID);
                        }
                    }
                    $scope.TaskCreationAttachAsset.Dhtml += '<div id="Asset_' + asset.AssetUniqueID + '" class="attachmentBox">';
                    $scope.TaskCreationAttachAsset.Dhtml += '<div class="row-fluid">';
                    $scope.TaskCreationAttachAsset.Dhtml += '<div class="span12">';
                    $scope.TaskCreationAttachAsset.Dhtml += '<div class="span2 leftSection">';
                    $scope.TaskCreationAttachAsset.Dhtml += '<i class="icon-ok-sign" style="display:none;"></i>';
                    $scope.TaskCreationAttachAsset.Dhtml += '<div class="fileType-Preview">';
                    $scope.TaskCreationAttachAsset.Dhtml += '<div class="fileType-PreviewDiv">';
                    if (asset.Category == 0) {
                        if (asset.Extension != null) {
                            if (asset.ProcessType > 0) {
                                if (asset.Status == 2) $scope.TaskCreationAttachAsset.Dhtml += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imagesrcpath + 'DAMFiles/Preview/Small_' + asset.FileGuid + '.jpg" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg\'" />';
                                else if (asset.Status == 1 || asset.Status == 0) $scope.TaskCreationAttachAsset.Dhtml += '<img class="loadingImg" id=' + asset.ActiveFileID + ' data-extn=' + asset.Extension + ' data-src=' + asset.FileGuid + ' ng-click=\"Editasset(' + asset.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                                else if (asset.Status == 3) {
                                    $scope.TaskCreationAttachAsset.Dhtml += '<img  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                                }
                            } else {
                                $scope.TaskCreationAttachAsset.Dhtml += '<img  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + asset.Extension.substring(1, asset.Extension.length).toUpperCase() + '.jpg' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                            }
                        } else $scope.TaskCreationAttachAsset.Dhtml += '<img  src="' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg' + '" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                    } else if (asset.Category == 1) {
                        $scope.TaskCreationAttachAsset.Dhtml += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/BLANK.jpg" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                    } else if (asset.Category == 2) {
                        $scope.TaskCreationAttachAsset.Dhtml += '<img id="file_' + asset.AssetUniqueID + '"  src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/LINK.jpg" alt="Image"   onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg\'" />';
                    }
                    $scope.TaskCreationAttachAsset.Dhtml += '</div>';
                    $scope.TaskCreationAttachAsset.Dhtml += '</div>';
                    $scope.TaskCreationAttachAsset.Dhtml += '<div class="progress progress-striped active">';
                    $scope.TaskCreationAttachAsset.Dhtml += '<div style="width: 0%" class="bar"></div>';
                    $scope.TaskCreationAttachAsset.Dhtml += '</div>';
                    $scope.TaskCreationAttachAsset.Dhtml += '<div class="progressInfo">';
                    $scope.TaskCreationAttachAsset.Dhtml += '<span class="progress-percent"></span>';
                    if (asset.Category == 0) {
                        $scope.TaskCreationAttachAsset.Dhtml += '<span class="fileSize">' + parseSize(asset.Size) + ' </span>';
                    } else {
                        $scope.TaskCreationAttachAsset.Dhtml += '<span class="fileSize"></span>';
                    }
                    $scope.TaskCreationAttachAsset.Dhtml += '</div>';
                    $scope.TaskCreationAttachAsset.Dhtml += '</div>';
                    $scope.TaskCreationAttachAsset.Dhtml += '<div class="span10 rightSection">';
                    $scope.TaskCreationAttachAsset.Dhtml += '<div class="info">';
                    $scope.TaskCreationAttachAsset.Dhtml += '<span class="name">';
                    if (asset.Category == 0) {
                        $scope.TaskCreationAttachAsset.Dhtml += '<i class="icon-file-alt"></i>';
                    }
                    $scope.TaskCreationAttachAsset.Dhtml += asset.AssetName;
                    $scope.TaskCreationAttachAsset.Dhtml += '</span>';
                    $scope.TaskCreationAttachAsset.Dhtml += '<span class="size" ng-click="removeasset(' + asset.AssetUniqueID + ')">';
                    $scope.TaskCreationAttachAsset.Dhtml += '<i class="icon-remove removefile" data-fileid="' + asset.AssetUniqueID + '"></i>';
                    $scope.TaskCreationAttachAsset.Dhtml += '</span>';
                    $scope.TaskCreationAttachAsset.Dhtml += '</div>';
                    $scope.TaskCreationAttachAsset.Dhtml += '<div class="formInfo">';
                    $scope.TaskCreationAttachAsset.Dhtml += '<form class="form-float">';
                    $scope.TaskCreationAttachAsset.Dhtml += '<div class="control-group fullSpace">';
                    $scope.TaskCreationAttachAsset.Dhtml += '<label class="control-label"> Asset type </label>';
                    $scope.TaskCreationAttachAsset.Dhtml += '<div class="controls">';
                    $scope.TaskCreationAttachAsset.Dhtml += '<label class="control-label">' + rec[0].damCaption + '</label>';
                    $scope.TaskCreationAttachAsset.Dhtml += '</div>';
                    $scope.TaskCreationAttachAsset.Dhtml += '</div>';
                    $scope.TaskCreationAttachAsset.Dhtml += '<div class="control-group">';
                    $scope.TaskCreationAttachAsset.Dhtml += '<label class="control-label"> Asset name </label>';
                    $scope.TaskCreationAttachAsset.Dhtml += '<div class="controls">';
                    $scope.TaskCreationAttachAsset.Dhtml += '<label class="control-label">' + asset.AssetName + '</label>';
                    $scope.TaskCreationAttachAsset.Dhtml += '</div>';
                    $scope.TaskCreationAttachAsset.Dhtml += '</div>';
                    $scope.TaskCreationAttachAsset.Dhtml += '</form>';
                    $scope.TaskCreationAttachAsset.Dhtml += '</div>';
                    $scope.TaskCreationAttachAsset.Dhtml += '</div>';
                    $scope.TaskCreationAttachAsset.Dhtml += '</div>';
                    $scope.TaskCreationAttachAsset.Dhtml += '</div>';
                    $scope.TaskCreationAttachAsset.Dhtml += '</div>';
                }
                $scope.IsAttachmentPresent = true;
                $("#assetfromattachment").append($compile($scope.TaskCreationAttachAsset.Dhtml)($scope));
            } else { }
        }

        function GenereteDetailBlock(asset) {
            var attrRelation = [];
            attrRelation = $.grep($scope.DamAssetTaskselectionFiles.ThumbnailSettings, function (e) {
                return e.assetType == asset.AssetTypeid;
            });
            var htmldata = '';
            if (attrRelation.length > 0) {
                for (var i = 0, attr; attr = attrRelation[i++];) {
                    var cls = attr.ID == 68 ? "th-infoMain" : "th-infoSub";
                    if ($scope.DamAssetTaskselectionFiles.AssetDynamicData.length > 0) {
                        var data = [];
                        data = $.grep($scope.DamAssetTaskselectionFiles.AssetDynamicData, function (e) {
                            return e.ID == asset.AssetUniqueID;
                        });
                        if (data.length > 0) {
                            var attrval = [];
                            if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null && data[0]["" + attr.ID + ""] != undefined) htmldata += '<span class="' + cls.toString() + '">' + data[0]["" + attr.ID + ""] + '</span>';
                        } else htmldata += '<span class="' + cls.toString() + '">-</span>';
                    } else htmldata += '<span class="' + cls.toString() + '">-</span>';
                }
            }
            return htmldata;
        }
        $scope.tasktypeobj = {
            tasktype: taskType.Work_Task
        };
        $scope.$on("OpenTree", function (event, fromPlace, tasktype) {
            $scope.showAttach = fromPlace;
            $scope.AssetTaskTypeObj.AssetTaskType = tasktype;
            $scope.$broadcast('PopulateTree', fromPlace, 2);
        });
        $scope.TaskTypeFilter = function (item) {
            if ($scope.ShowDamAssetApprovalType > 0) if (item.TaskTypeId == taskType.Asset_Approval_Task || item.TaskTypeId == taskType.Proof_approval_task || item.TaskTypeId == taskType.dalim_approval_task) return false;
            else return true;
            else {
                if (item.TaskTypeId == taskType.Asset_Approval_Task || item.TaskTypeId == taskType.Proof_approval_task || item.TaskTypeId == taskType.dalim_approval_task) return false;
                else return true;
                return true;
            }
        }

        function Page_Load_Methods() {
            GetTaskTypeDetails();
            LoadMemberRoles();
            GetAllUserRoles();
        }

        function GetTaskTypeDetails() {
            $scope.TaskTypeListdata = [];
            $scope.userid = parseInt($.cookie('UserId'), 10);
            $scope.userisProofInitiator = false;
            if ($scope.memberList == null || $scope.memberList == undefined) {
                $scope.memberList = [];
            }
            if ($scope.EntityMemberObj != null && $scope.EntityMemberObj != undefined) {
                if ($scope.EntityMemberObj.EntityMemberList != null && $scope.EntityMemberObj.EntityMemberList != undefined) {
                    $scope.memberList = $scope.EntityMemberObj.EntityMemberList;
                }
            }
            var loggedinUserDets = $.grep($scope.memberList, function (e) {
                return e.Userid == $scope.userid
            });
            var loggedinUserRoles = $.grep(loggedinUserDets, function (e) {
                return e.BackendRoleID == EntityRoles.ProofInitiator
            });
            if (loggedinUserRoles.length > 0) {
                $scope.userisProofInitiator = true;
                TaskCreationService.GetTaskTypes().then(function (TaskTypeData) {
                    $scope.TaskTypeList = TaskTypeData.Response;
                    timerObj.gettasktype = $timeout(function () {
                        var tasktypelist = $scope.TaskTypeList;
                        $scope.tagAllOptionsTasktypelist.data.splice(0, $scope.tagAllOptionsTasktypelist.data.length);
                        $scope.TaskTypeListdata = [];
                        $.each(tasktypelist, function (i, el) {
                            $scope.tagAllOptionsTasktypelist.data.push({
                                "id": el.Id,
                                "text": el.Caption,
                                "ShortDescription": el.ShortDescription,
                                "ColorCode": el.ColorCode,
                                "Caption": el.Caption
                            });
                        });
                    }, 3);
                });
            } else if ($scope.userisProofInitiator == false) {
                TaskCreationService.GetTaskTypes().then(function (TaskTypeData) {
                    timerObj.gettasktypes = $timeout(function () {
                        $scope.TaskTypeList = TaskTypeData.Response;
                        var tasktypelist = $scope.TaskTypeList;
                        $scope.tagAllOptionsTasktypelist.data.splice(0, $scope.tagAllOptionsTasktypelist.data.length);
                        $scope.TaskTypeListdata = [];
                        $.each(tasktypelist, function (i, el) {
                            if (el.Id == 36 || el.Id == 37) { } else {
                                $scope.tagAllOptionsTasktypelist.data.push({
                                    "id": el.Id,
                                    "text": el.Caption,
                                    "ShortDescription": el.ShortDescription,
                                    "ColorCode": el.ColorCode,
                                    "Caption": el.Caption
                                });
                            }
                        });
                    }, 5);
                });
            }
        }

        function LoadMemberRoles() {
            TaskCreationService.GetEntityDetailsByID($stateParams.ID).then(function (GetEntityresult) {
                if (GetEntityresult.Response != null) {
                    TaskCreationService.GetEntityTypeRoleAccess(GetEntityresult.Response.Typeid).then(function (role) {
                        $scope.Roles = role.Response;
                    });
                }
            });
        }
        $scope.EntityMemberList = [];
        var cancelevent;
        TaskCreationService.gettaskflagstatus().then(function (flagstat) {
            if (flagstat.Response != null) {
                if (flagstat.Response) {
                    $scope.IsShowTaskFlag = true;
                } else {
                    $scope.IsShowTaskFlag = false;
                }
                LoadTaskList();
            }
        });
        $scope.groupBy = function (attribute, ISglobal, taskStatus) {
            if ($scope.groups == undefined) {
                $scope.groups.splice(0, $scope.groups.length);
            }
            for (var i = 0, friend; friend = $scope.TaskLibraryList[i++];) {
                var taskList = friend.TaskList;
                if (ISglobal != "All") {
                    var group = {
                        EntityParentID: friend.EntityParentID,
                        LibraryName: friend.LibraryName,
                        LibraryDescription: friend.LibraryDescription,
                        ID: friend.ID,
                        SortOrder: friend.SortOrder,
                        TaskCount: friend.TaskCount,
                        IsGetTasks: false,
                        IsExpanded: false,
                        TaskList: []
                    }
                    for (var j = 0; j < taskList.length; j++) {
                        if (taskList[j]["TaskStatus"] === taskStatus) {
                            group.TaskList.push(taskList[j]);
                        }
                    }
                    $scope.groups.push(group);
                } else if (ISglobal == "All") {
                    var group = {
                        EntityParentID: friend.EntityParentID,
                        LibraryName: friend.LibraryName,
                        LibraryDescription: friend.LibraryDescription,
                        ID: friend.ID,
                        SortOrder: friend.SortOrder,
                        TaskCount: friend.TaskCount,
                        IsGetTasks: false,
                        IsExpanded: false,
                        TaskList: []
                    };
                    group.TaskList = taskList;
                    $scope.groups.push(group);
                }
            }
        };

        function parseSize(size) {
            var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"],
                tier = 0;
            while (size >= 1024) {
                size = size / 1024;
                tier++;
            }
            return Math.round(size * 10) / 10 + " " + suffix[tier];
        }
        $scope.Close = function () {
            $scope.approvalflowTempPopup = false;
            $scope.DisableIsAdminTask = true;
            $scope.TaskActionCaption = "Add task";
            $scope.TaskMembersList = "";
            $scope.TaskTypeObject.NewTaskName = "";
            $scope.TaskTypeObject.TaskDueDate = new Date.create();
            $scope.TaskTypeObject.TaskDueDate = null;
            $scope.TaskTypeObject.TaskDescription = "";
            $scope.SelectedTaskID = 0;
            $scope.dyn_Cont = '';
            $scope.DamAssetTaskselectionFiles.AssetFiles = [];
            $scope.DamAssetTaskselectionFiles.ThumbnailSettings = [];
            $scope.DamAssetTaskselectionFiles.AssetDynamicData = [];
            $scope.DamAssetTaskselectionFiles.AssetSelectionFiles = [];
            $("#damtaskDynamicTaskControls").html("");
            $("#damtaskDynamicTaskControls").append($compile("")($scope));
            $scope.AdminTaskCheckList = [{
                ID: 0,
                NAME: ""
            }];
            $("#DynamicTaskControls").html("");
        }

        function ResetVariables() {
            $scope.approvalflowTempPopup = false;
            $scope.ngShowUnassigned = false;
            $scope.ngShowTaskCreation = true;
            $scope.DisableIsAdminTask = false;
            if ($scope.ShowDamAssetApprovalType > 0) $scope.DisableIsAdminTask = true;
            $scope.TaskActionCaption = "Add task";
            $scope.TaskMembersList = "";
            $scope.TaskTypeObject.NewTaskName = "";
            $scope.TaskTypeObject.TaskDueDate = new Date.create();
            $scope.TaskTypeObject.TaskDueDate = null;
            $scope.TaskTypeObject.TaskDescription = "";
            $scope.SelectedTaskID = 0;
            $scope.dyn_Cont = '';
            $("#damtaskDynamicTaskControls").html("");
            $("#damtaskDynamicTaskControls").append($compile("")($scope));
            $scope.AdminTaskCheckList = [{
                ID: 0,
                NAME: ""
            }];
            $("#DynamicTaskControls").html("");
            $scope.createdynamicControls($scope.AssetTaskTypeObj.AssetTaskType);
            $("#loadworktask").modal('show');
            refreshModel();
            timerObj.addtaskcaption = $timeout(function () {
                $('#addtaskcaption').focus();
            }, 1000);
        }

        function IsNotEmptyTree(treeObj) {
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                } else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }
        $scope.CloseCheckListPopUp = function (Index, CheCklistExisting, ChklistName) {
            if ($scope.TaskBriefDetails.taskCheckList[Index].Id == 0) {
                $scope.TaskBriefDetails.taskCheckList.splice(Index, 1);
            } else {
                $scope.TaskBriefDetails.taskCheckList[Index].Name = CurrentCheckListName;
                $scope.TaskBriefDetails.taskCheckList[Index].IsExisting = true;
            }
            $("#NonEditableCL" + Index).show();
            $("#EditableCL" + Index).hide();
        }
        $scope.ChecklistisSelected = function ($event, id) {
            var checkbox = $event.target;
            TaskCreationService.ChecksTaskCheckList(id, checkbox.checked).then(function (TaskStatusResult) {
                if (TaskStatusResult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4375.Caption'));
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                    GetTaskCheckLists();
                }
            });
        };
        $scope.AddCheckList = function (Index) {
            if ($scope.AdminTaskCheckList[Index].Name == undefined || $scope.AdminTaskCheckList[Index].Name.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4589.Caption'));
                return false;
            }
            if ($scope.SelectedTaskID != 0) $scope.AdminTaskCheckList.push({
                Id: 0,
                Name: "",
                SortOrder: 0,
                Status: false,
                UserName: "",
                UserId: 0,
                CompletedOnValue: "",
                OwnerId: 0,
                OwnerName: "",
                CompletedOn: ""
            });
            else $scope.AdminTaskCheckList.push({
                ID: 0,
                NAME: ""
            });
        }
        $scope.RemoveCheckList = function (Index, ID) {
            if ($scope.AdminTaskCheckList.length > 1) {
                bootbox.confirm($translate.instant('LanguageContents.Res_2012.Caption'), function (result) {
                    if (result) {
                        timerObj.deletechecklist = $timeout(function () {
                            if (ID > 0) {
                                TaskCreationService.DeleteEntityCheckListByID(ID).then(function (result) {
                                    if (result.StatusCode == 200) {
                                        $scope.AdminTaskCheckList.splice(Index, 1);
                                    }
                                });
                            } else {
                                $scope.AdminTaskCheckList.splice(Index, 1);
                            }
                        }, 100);
                    }
                });
            } else {
                bootbox.confirm($translate.instant('LanguageContents.Res_2012.Caption'), function (result) {
                    if (result) {
                        timerObj.deletechecklist = $timeout(function () {
                            if (ID > 0) {
                                TaskCreationService.DeleteEntityCheckListByID(ID).then(function (result) {
                                    if (result.StatusCode == 200) {
                                        $scope.AdminTaskCheckList[Index].NAME = "";
                                    }
                                });
                            } else {
                                $scope.AdminTaskCheckList[Index].NAME = "";
                            }
                        }, 100);
                    }
                });
            }
            timerObj.apprTaskFeed = $timeout(function () {
                feedforApprovalTask($scope.SelectedTaskID, $scope.divid, true);
            }, 5000);
        }
        $scope.CheckListSelection = function (status) {
            var baseClass = "checkbox";
            if (status) baseClass += " checked";
            return baseClass;
        }
        $scope.contextCheckListID = 0;
        $scope.contextCheckListIndex = 0;
        $scope.SetcontextCheckListID = function (id, indexval) {
            $scope.contextCheckListID = id;
            $scope.contextCheckListIndex = indexval;
        };
        $scope.DeleteEntityChecklists = function () {
            bootbox.confirm($translate.instant('LanguageContents.Res_2012.Caption'), function (result) {
                if (result) {
                    timerObj.contextChecklist = $timeout(function () {
                        if ($scope.contextCheckListID != 0) {
                            TaskCreationService.DeleteEntityCheckListByID($scope.contextCheckListID).then(function (data) {
                                if (data.StatusCode == 200) {
                                    var fileObj = $.grep($scope.TaskBriefDetails.taskCheckList, function (e) {
                                        return e.Id == $scope.contextCheckListID;
                                    });
                                    $scope.TaskBriefDetails.taskCheckList.splice($.inArray(fileObj[0], $scope.TaskBriefDetails.taskCheckList), 1);
                                    RefreshCurrentTask();
                                    feedforApprovalTask($scope.SelectedTaskID, $scope.divid, true);
                                }
                            });
                        } else {
                            $scope.TaskBriefDetails.taskCheckList.splice($scope.contextCheckListIndex, 1);
                        }
                    }, 100);
                }
            });
        }
        $scope.AddCheckListNext = function () {
            var Object = $.grep($scope.TaskBriefDetails.taskCheckList, function (n, i) {
                if (n.IsExisting == false) {
                    return $scope.TaskBriefDetails.taskCheckList[i];
                }
            });
            if (Object.length > 0) {
                if (Object[0].Name.length == 0) {
                    if (Object[0].Id == 0) {
                        $scope.TaskBriefDetails.taskCheckList.splice($scope.TaskBriefDetails.taskCheckList.indexOf(Object[0]), 1);
                    } else {
                        Object[0].IsExisting = true;
                        Object[0].Name = CurrentCheckListName;
                    }
                } else {
                    Object[0].Name = CurrentCheckListName;
                    Object[0].IsExisting = true;
                }
            }
            if ($scope.contextCheckListIndex == -1) {
                $scope.TaskBriefDetails.taskCheckList.push({
                    Id: 0,
                    Name: '',
                    OwnerId: 0,
                    OwnerName: 0,
                    SortOrder: 1,
                    Status: false,
                    TaskId: $scope.TaskBriefDetails.taskID,
                    UserId: 0,
                    UserName: "",
                    CompletedOnValue: "",
                    CompletedOn: null,
                    IsExisting: false
                });
            } else {
                $scope.TaskBriefDetails.taskCheckList.splice(parseInt($scope.contextCheckListIndex) + 1, 0, {
                    Id: 0,
                    Name: '',
                    OwnerId: 0,
                    OwnerName: 0,
                    SortOrder: 1,
                    Status: false,
                    TaskId: $scope.TaskBriefDetails.taskID,
                    UserId: 0,
                    UserName: "",
                    CompletedOnValue: "",
                    CompletedOn: null,
                    IsExisting: false
                });
            }
            timerObj.briefData = $timeout(function () {
                $('[datafocus-id =0]').focus();
            }, 10);
        }
        timerObj.uploaderAttr = $timeout(function () {
            StrartUpload_UploaderAttrTask();
        }, 100);
        var uploader_AttrTask = '';

        function StrartUpload_UploaderAttrTask() {
            $('.moxie-shim').remove();
            if (uploader_AttrTask != '') {
                uploader_AttrTask.destroy();
            }
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                uploader_AttrTask = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAttrTask',
                    container: 'filescontainerfrTask',
                    max_file_size: '10000mb',
                    url: amazonURL + cloudsetup.BucketName,
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    urlstream_upload: true,
                    multiple_queues: true,
                    file_data_name: 'file',
                    multipart: true,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    },
                });
                uploader_AttrTask.bind('Init', function (up, params) { });
                uploader_AttrTask.init();
                uploader_AttrTask.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_AttrTask.start();
                });
                uploader_AttrTask.bind('UploadProgress', function (up, file) { });
                uploader_AttrTask.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_AttrTask.bind('FileUploaded', function (up, file, response) {
                    SaveFileDetails(file, response.response);
                });
                uploader_AttrTask.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKeypath = TenantFilePath.replace(/\\/g, "\/");
                    var uniqueKey = (uniqueKeypath + "DAMFiles/Original/" + file.id + fileext);
                    uploader.settings.multipart_params.key = uniqueKey;
                    uploader.settings.multipart_params.Filename = uniqueKey;
                });
            } else {
                uploader_AttrTask = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesUploaderAttrTask',
                    container: 'filescontainerfrTask',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: 'Handlers/UploadHandler.ashx?Type=Attachment',
                    chunk_size: '64Kb',
                    multi_selection: false,
                    multipart_params: {}
                });
                uploader_AttrTask.bind('Init', function (up, params) { });
                uploader_AttrTask.init();
                uploader_AttrTask.bind('FilesAdded', function (up, files) {
                    up.refresh();
                    uploader_AttrTask.start();
                });
                uploader_AttrTask.bind('UploadProgress', function (up, file) { });
                uploader_AttrTask.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    up.refresh();
                });
                uploader_AttrTask.bind('FileUploaded', function (up, file, response) {
                    SaveFileDetails(file, response.response);
                });
                uploader_AttrTask.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
            }
        }

        function GetTreeCheckedNodes(treeobj, attrID) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    $scope.fields["Tree_" + attrID].push(node.id);
                }
                if (node.Children.length > 0) GetTreeCheckedNodes(node.Children, attrID);
            }
        }
        $scope.treeNodeSelectedHolder = [];
        var tree;
        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            if ($scope.treeNodeSelectedHolder.length > 0) {
                for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
                    $scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
                }
            }
            $scope.ShowHideAttributeToAttributeRelations(branch.AttributeId, 0, 0, 7);
        };

        function LoadTaskMetadata(EntAttrDet, ID, tasktype, fromassignedplace) {
            assignedforperiod = fromassignedplace;
            $scope.StopeUpdateStatusonPageLoad = false;
            $scope.detailsLoader = false;
            $scope.detailsData = true;
            $scope.dyn_Cont = '';
            $scope.dyn_Cont = "";
            $scope.items = [];
            $scope.attributedata = EntAttrDet;
            if ($scope.attributedata != null) {
                for (var i = 0; i < $scope.attributedata.length; i++) {
                    if ($scope.attributedata[i].TypeID == 6) {
                        $scope.dyn_Cont2 = '';
                        var CaptionObj = $scope.attributedata[i].Caption.split(",");
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            if (j == 0) {
                                if (CaptionObj[j] != undefined) {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                } else {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                }
                            } else {
                                if (CaptionObj[j] != undefined) {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                } else {
                                    $scope.items.push({
                                        caption: $scope.attributedata[i].Lable[j].Label,
                                        level: j + 1
                                    });
                                    $scope.treeTexts["dropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.fields["DropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                }
                            }
                        }
                        $scope.treelevels["dropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                        $scope.items = [];
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                            var inlineEditabletitile = $scope.treelevels['dropdown_levels_' + $scope.attributedata[i].ID][j].caption;
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditableoptimizedtreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.DropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.dropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 12) {
                        $scope.dyn_Cont2 = '';
                        var CaptionObj = $scope.attributedata[i].Caption;
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            if ($scope.attributedata[i].Lable.length == 1) {
                                var k = j;
                                var treeTexts = [];
                                var fields = [];
                                $scope.items.push({
                                    caption: $scope.attributedata[i].Lable[j].Label,
                                    level: j + 1
                                });
                                if (k == CaptionObj.length) {
                                    $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                } else {
                                    if (CaptionObj[k] != undefined) {
                                        for (k; k < CaptionObj.length; k++) {
                                            treeTexts.push(CaptionObj[k]);
                                            fields.push(CaptionObj[k]);
                                        }
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                    } else {
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    }
                                }
                            } else {
                                if (j == 0) {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                    } else {
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                    }
                                } else {
                                    var k = j;
                                    if (j == ($scope.attributedata[i].Lable.length - 1)) {
                                        var treeTexts = [];
                                        var fields = [];
                                        $scope.items.push({
                                            caption: $scope.attributedata[i].Lable[j].Label,
                                            level: j + 1
                                        });
                                        if (k == CaptionObj.length) {
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        } else {
                                            if (CaptionObj[k] != undefined) {
                                                for (k; k < CaptionObj.length; k++) {
                                                    treeTexts.push(CaptionObj[k]);
                                                    fields.push(CaptionObj[k]);
                                                }
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = treeTexts.join(", ");
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = fields.join(", ");
                                            } else {
                                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                                $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            }
                                        }
                                    } else {
                                        if (CaptionObj[j] != undefined) {
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = CaptionObj[j];
                                        } else {
                                            $scope.items.push({
                                                caption: $scope.attributedata[i].Lable[j].Label,
                                                level: j + 1
                                            });
                                            $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                            $scope.fields["MultiSelectDropDown_" + $scope.attributedata[i].ID + "_" + (j + 1)] = "-";
                                        }
                                    }
                                }
                            }
                        }
                        $scope.treelevels["multiselectdropdown_levels_" + $scope.attributedata[i].ID] = $scope.items;
                        $scope.items = [];
                        for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID + "_" + (j + 1)] = true;
                            var inlineEditabletitile = $scope.treelevels['multiselectdropdown_levels_' + $scope.attributedata[i].ID][j].caption;
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><a xeditablemultiselecttreedropdown href=\"javascript:;\" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + (j + 1) + '" data-ng-model=\"multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  attributename=' + inlineEditabletitile + ' data-type=' + inlineEditabletitile + ' >{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + '_' + (j + 1) + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label> <div class="controls"><span class="editable">{{treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + (j + 1) + '}}</span></div></div>';
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext   href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"   data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 19) {
                        $scope.isfromtask = 1;
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                            if ($scope.attributedata[i].Value != null) {
                                $scope['origninalamountvalue_' + $scope.attributedata[i].ID] = $scope.attributedata[i].Value.Amount;
                                $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html((($scope.attributedata[i].Value.Amount).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' '))).text();
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value.Currencytypeid;
                                var currtypeid = $scope.attributedata[i].Value.Currencytypeid;
                                $scope.currtypenameobj = ($.grep($scope.CurrencyFormatsList, function (e) {
                                    return e.Id == currtypeid;
                                }));
                                $scope.fields["currtypename_" + $scope.attributedata[i].ID] = $scope.currtypenameobj[0]["ShortName"];
                            } else {
                                $scope['origninalamountvalue_' + $scope.attributedata[i].ID] = 0;
                                $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.DefaultSettings.CurrencyFormat.Id;
                                $scope.fields["currtypename_" + $scope.attributedata[i].ID] = "-";
                            }
                            $scope.currencytypeslist = $scope.CurrencyFormatsList;
                            $scope.setFieldKeys();
                            $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label widthauto ng-binding">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label><span class="va-middle inlineBlock padding-top5x margin-left5x color-info ng-binding">{{fields.currtypename_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletextforcurrencyamount   href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"  my-qtip2 qtip-content=\"' + $scope.attributedata[i].Lable + '\"  data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}<span class="margin-left5x">{{fields.currtypename_' + $scope.attributedata[i].ID + '}}</span></a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group AttrID_' + $scope.attributedata[i].ID + '_0\"><label class=\"control-label"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><label class="control-label widthauto ng-binding">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</label><span class="va-middle inlineBlock margin-left5x color-info ng-binding">{{fields.currtypename_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 2) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
                        }
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\"  attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"MultiLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\" data-type="' + $scope.attributedata[i].ID + '" data-original-title=\"' + $scope.attributedata[i].Lable + '\">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 11) {
                        StrartUpload_UploaderAttrTask();
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                        $scope.UploaderCaption["uploader_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                        $scope.setUploaderCaption();
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group ng-scope\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable.toString() + '</label>';
                        $scope.dyn_Cont += '<div class=\"controls\">';
                        //$scope.dyn_Cont += '<img src="UploadedImages/' + $scope.attributedata[i].Value + '" alt="' + $scope.attributedata[i].Caption + '"';
                        //$scope.dyn_Cont += 'class="entityDetailImgPreview UploaderImagePreview1_' + $scope.attributedata[i].ID + '" id="UploaderPreview1_' + $scope.attributedata[i].ID + '">';
                        if ($scope.attributedata[i].Value == "" || $scope.attributedata[i].Value == null && $scope.attributedata[i].Value == undefined) {
                            $scope.attributedata[i].Value = "NoThumpnail.jpg";
                        }
                        $scope.dyn_Cont += '<div class="entityDetailImgPreviewHolder"><img src="' + imagesrcpath + 'UploadedImages/' + $scope.attributedata[i].Value + '" alt="' + $scope.attributedata[i].Caption + '"';
                        $scope.dyn_Cont += 'class="entityDetailImgPreview UploaderImagePreview1_' + $scope.attributedata[i].ID + '" id="UploaderPreview1_' + $scope.attributedata[i].ID + '"></div>';

                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '</div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += "<a ng-click='UploadImagefileTask(" + $scope.attributedata[i].ID + ")' attributeTypeID='" + $scope.attributedata[i].TypeID + "'";
                                $scope.dyn_Cont += 'entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="Uploader_' + $scope.attributedata[i].ID + '"';
                                $scope.dyn_Cont += 'my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                $scope.dyn_Cont += ' class="ng-pristine ng-valid" attributename=\"' + $scope.UploaderCaption["UploaderCaption_" + $scope.attributedata[i].ID] + '\">Select Image';
                                $scope.dyn_Cont += '</a></div></div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '</div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 3) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                    }
                                }
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                    $scope.dyn_Cont += '<div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span>';
                                    $scope.dyn_Cont += '</div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                        $scope.dyn_Cont += '<div class="controls"><a  xeditabledropdown href=\"javascript:;\"';
                                        $scope.dyn_Cont += 'attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '"';
                                        $scope.dyn_Cont += 'attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"';
                                        $scope.dyn_Cont += 'data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '"';
                                        $scope.dyn_Cont += 'attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\"';
                                        $scope.dyn_Cont += 'data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a>';
                                        $scope.dyn_Cont += '</div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
                                        $scope.dyn_Cont += '<div class="controls"><span>{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span>';
                                        $scope.dyn_Cont += '</div></div>';
                                    }
                                }
                            }
                        } else {
                            if ($scope.attributedata[i].Caption[0] != undefined) {
                                if ($scope.attributedata[i].Caption[0].length > 1) {
                                    $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
                                    $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                    if ($scope.attributedata[i].IsReadOnly == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                    } else {
                                        if ($scope.IsLock == false) {
                                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                        } else if ($scope.IsLock == true) {
                                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                        }
                                    }
                                }
                            } else {
                                $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
                                $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabledropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                    }
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 4) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 1) {
                                $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                                $scope.setNormalMultiDropdownCaption();
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                    }
                                }
                            }
                        } else {
                            $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
                            $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                            $scope.setNormalMultiDropdownCaption();
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditablemultiselectdropdown href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="NormalMultiDropDown_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '\" my-qtip2 qtip-content="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '\" >{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.NormalMultiDropDown_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                }
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 10) {
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        $scope.dyn_Cont += '<div class="period control-group nomargin" data-periodcontainerID="periodcontainerID">';
                        if ($scope.attributedata[i].Value == "-") {
                            $scope.IsStartDateEmpty = true;
                            $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';
                            $scope.dyn_Cont += '</div>';
                        } else {
                            for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
                                var datStartUTCval = "";
                                var datstartval = "";
                                var datEndUTCval = "";
                                var datendval = "";
                                $scope.MinValue = $scope.attributedata[i].MinValue;
                                $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id] = new Date.create();
                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id] = new Date.create();
                                if ($scope.MinValue < 0) {
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MinValue + 1));
                                } else {
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MinValue));
                                }
                                if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].getDate() + ($scope.MaxValue - 1));
                                } else {
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id].getDate() + 100000);
                                }
                                var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id]);
                                $scope.fields["DatePartMinDate_" + $scope.attributedata[i].Value[j].Id] = (temp.MinDate);
                                $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].Value[j].Id] = (temp.MaxDate);
                                datstartval = new Date.create($scope.attributedata[i].Value[j].Startdate);
                                datendval = new Date.create($scope.attributedata[i].Value[j].EndDate);
                                perioddates.push({
                                    ID: $scope.attributedata[i].Value[j].Id,
                                    value: datendval
                                });
                                $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datstartval);
                                $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = dateFormat(datendval);
                                $scope.fields["PeriodStartDate_Dir_" + $scope.attributedata[i].Value[j].Id] = formatteddateFormat(datstartval, "dd/MM/yyyy")
                                $scope.fields["PeriodEndDate_Dir_" + $scope.attributedata[i].Value[j].Id] = formatteddateFormat(datendval, "dd/MM/yyyy");
                                if ($scope.attributedata[i].Value[j].Description == undefined) {
                                    $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = "-";
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "";
                                } else {
                                    $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                    $scope.fields["PeriodDateDesc_Dir_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
                                }
                                $('#fsedateid').css("visibility", "hidden");
                                $scope.dyn_Cont += '<div class="control-group nomargin" data-dynPeriodID="' + $scope.attributedata[i].Value[j].Id + '">';
                                $scope.dyn_Cont += '<div class="inputHolder span11">';
                                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + inlineEditabletitile + '</label>';
                                $scope.dyn_Cont += '<div class="controls">';
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<span>{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                    $scope.dyn_Cont += '<span> to </span><span>{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                        $scope.dyn_Cont += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<span>{{fields.PeriodStartDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                        $scope.dyn_Cont += '<span> to </span><span>{{fields.PeriodEndDate_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                    }
                                }
                                $scope.dyn_Cont += '</div></div>';
                                $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
                                $scope.dyn_Cont += '<div class="controls">';
                                if ($scope.attributedata[i].IsReadOnly == true) {
                                    $scope.dyn_Cont += '<span>{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                } else {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<a xeditabletreedropdown1 data-primaryid="' + $scope.attributedata[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodenddate_id=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\" data-ng-model=\"PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + $scope.attributedata[i].Value[j].Id + '\" href=\"javascript:;\" attributename="perdiodate">{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</a>';
                                    } else if ($scope.IsLock == true) {
                                        $scope.dyn_Cont += '<span>{{fields.PeriodDateDesc_' + $scope.attributedata[i].Value[j].Id + '}}</span>';
                                    }
                                }
                                $scope.dyn_Cont += '</div></div></div>';
                                if (j != 0) {
                                    if ($scope.IsLock == false) {
                                        $scope.dyn_Cont += '<div class="buttonHolder span1"><a ng-click="deletePeriodDate(' + $scope.attributedata[i].Value[j].Id + ')"><i class="icon-remove"></i></a></div>';
                                    }
                                }
                                $scope.dyn_Cont += '</div>';
                                if (j == ($scope.attributedata[i].Value.length - 1)) {
                                    $scope.dyn_Cont += '<div class="control-group nomargin" data-addperiodID="addperiodID">';
                                    $scope.dyn_Cont += '</div>';
                                }
                            }
                        }
                        $scope.dyn_Cont += ' </div>';
                        $scope.MinValue = $scope.attributedata[i].MinValue;
                        $scope.MaxValue = $scope.attributedata[i].MaxValue;
                        $scope.fields["DatePartMinDate_0"] = new Date.create();
                        $scope.fields["DatePartMaxDate_0"] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.fields["DatePartMinDate_0"].setDate($scope.fields["DatePartMinDate_0"].getDate() + ($scope.MinValue + 1));
                        } else {
                            $scope.fields["DatePartMinDate_0"].setDate($scope.fields["DatePartMinDate_0"].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_0"].setDate($scope.fields["DatePartMaxDate_0"].getDate() + ($scope.MaxValue - 1));
                        } else {
                            $scope.fields["DatePartMaxDate_0"].setDate($scope.fields["DatePartMaxDate_0"].getDate() + 100000);
                        }
                        $scope.dyn_Cont += '<div class="control-group nomargin">';
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<label  data-tempid="startendID" class="control-label" for="label">Start date / End date</label>';
                            $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[Add Start / End Date ]</a>';
                            $scope.dyn_Cont += '<span>[Add Start / End Date ]</span>';
                        } else {
                            if ($scope.IsLock == false) {
                                if ($scope.attributedata[i].Value == "-") {
                                    $scope.dyn_Cont += '<label id="fsedateid"  class="control-label" for="label">' + inlineEditabletitile + '</label>';
                                }
                                $scope.dyn_Cont += '<div class="controls">';
                                $scope.dyn_Cont += '<a class="ng-pristine ng-valid editable editable-click" xeditabletreedropdown1 data-primaryid="0" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_0" data-ng-model=\"PeriodStartDate_0"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate0" href=\"javascript:;\" attributename="perdiodate">[Add ' + inlineEditabletitile + ' ]</a>';
                                $scope.dyn_Cont += '</div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<span class="controls">[Add ' + inlineEditabletitile + ' ]</span>';
                            }
                        }
                        $scope.dyn_Cont += '</div>';
                    } else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
                        var datStartUTCval = "";
                        var datstartval = "";
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].Value != null) {
                            datStartUTCval = $scope.attributedata[i].Value.substr(0, 10);
                            datstartval = new Date.create(datStartUTCval);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateFormat(datstartval, $scope.GetDefaultSettings.DateFormat);
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = formatteddateFormat(datstartval, "dd/MM/yyyy");
                        } else {
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                        }
                        if ($scope.attributedata[i].ID != SystemDefiendAttributes.CreationDate) {
                            if ($scope.attributedata[i].IsReadOnly == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            } else {
                                if ($scope.IsLock == false) {
                                    $scope.MinValue = $scope.attributedata[i].MinValue;
                                    $scope.MaxValue = $scope.attributedata[i].MaxValue;
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = new Date.create();
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = new Date.create();
                                    if ($scope.MinValue < 0) {
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue + 1));
                                    } else {
                                        $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MinValue));
                                    }
                                    if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + ($scope.MaxValue - 1));
                                    } else {
                                        $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].setDate($scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID].getDate() + 100000);
                                    }
                                    var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID], $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID]);
                                    $scope.fields["DatePartMinDate_" + $scope.attributedata[i].ID] = (temp.MinDate);
                                    $scope.fields["DatePartMaxDate_" + $scope.attributedata[i].ID] = (temp.MaxDate);
                                    $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a xeditabletreedatetime data-primaryid="' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" data-datetime_id=\"DateTime_' + $scope.attributedata[i].ID + '\" data-ng-model=\"DateTime_' + $scope.attributedata[i].ID + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"datetime' + $scope.attributedata[i].ID + '\" href=\"javascript:;\" attributename="datetime">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                                    $scope.fields["DatePart_" + $scope.attributedata[i].ID] = null;
                                } else if ($scope.IsLock == true) {
                                    $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><span class="editable">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                                }
                            }
                        } else {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        }
                    } else if ($scope.attributedata[i].TypeID == 7) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["Tree_" + $scope.attributedata[i].ID] = [];
                        $scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                        GetTreeCheckedNodes($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID], $scope.attributedata[i].ID);
                        $scope.staticTreesrcdirec["Attr_" + $scope.attributedata[i].ID] = JSON.parse($scope.attributedata[i].tree).Children;
                        $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class="control-group">';
                        $scope.dyn_Cont += '<label class="control-label">' + $scope.attributedata[i].Lable + ' </label>';
                        $scope.dyn_Cont += '<div class="controls">';
                        if ($scope.IsLock == false) {
                            $scope.dyn_Cont += '<div xeditabletree  editabletypeid="treeType_' + $scope.attributedata[i].ID + '" attributename=\"' + $scope.attributedata[i].Lable + '\" isreadonly="' + $scope.attributedata[i].IsReadOnly + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '"  data-type="treeType_' + $scope.attributedata[i].ID + '" entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"' + $scope.attributedata[i].ID + '\" data-ng-model=\"tree_' + $scope.attributedata[i].ID + '"\    data-original-title=\"' + $scope.attributedata[i].Lable + '\">';
                            if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                                treeTextVisbileflag = false;
                                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                                    $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                                } else $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            } else {
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            }
                            $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\" treeplace="detail" node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                            $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';
                            $scope.dyn_Cont += ' </div>';
                        } else {
                            if ($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID].length > 0) {
                                treeTextVisbileflag = false;
                                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.attributedata[i].ID])) {
                                    $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = true;
                                } else $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            } else {
                                $scope.TreeEmptyAttributeObj["Attr_" + $scope.attributedata[i].ID] = false;
                            }
                            $scope.dyn_Cont += '<eu-tree ng-show=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\" tree-data=\"treesrcdirec.Attr_' + $scope.attributedata[i].ID + '\"  node-attributeid="' + $scope.attributedata[i].ID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                            $scope.dyn_Cont += '<span ng-hide=\"TreeEmptyAttributeObj.Attr_' + $scope.attributedata[i].ID + '\">-<span>';
                        }
                        $scope.dyn_Cont += '</div></div>';
                    } else if ($scope.attributedata[i].TypeID == 8) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
                        if ($scope.attributedata[i].Caption != undefined) {
                            $scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html(($scope.attributedata[i].Caption).formatMoney(0, ' ', ' ')).text();
                        }
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><a xeditabletext href=\"javascript:;\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id=\"SingleLineText_' + $scope.attributedata[i].ID + '\" data-ng-model=\"fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '\"  data-type="' + $scope.attributedata[i].ID + '"   data-original-title=' + $scope.attributedata[i].Lable + '>{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">{{fields.SingleLineTextValue_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 13) {
                        $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.attributedata[i].ID + ""] = $scope.attributedata[i].DropDownPricing;
                        $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.attributedata[i].ID + ""] = true;
                        for (var j = 0, price; price = $scope.attributedata[i].DropDownPricing[j++];) {
                            if (price.selection.length > 0) {
                                var selectiontext = "";
                                var valueMatches = [];
                                if (price.selection.length > 0) valueMatches = jQuery.grep(price.LevelOptions, function (relation) {
                                    return price.selection.indexOf(relation.NodeId.toString()) != -1;
                                });
                                if (valueMatches.length > 0) {
                                    selectiontext = "";
                                    for (var x = 0, val; val = valueMatches[x++];) {
                                        selectiontext += val.caption;
                                        if (val.value != "") selectiontext += " - " + val.value + "% </br>";
                                        else selectiontext += "</br>";
                                    }
                                } else selectiontext = "-";
                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + j] = selectiontext;
                            } else {
                                $scope.treeTexts["multiselectdropdown_text_" + $scope.attributedata[i].ID + "_" + j] = "-";
                            }
                            if ($scope.IsLock == false) {
                                $scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = false;
                                $scope.dyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><a  href=\"javascript:;\" xeditablepercentage entityid="' + ID + '" attributeTypeID="' + $scope.attributedata[i].TypeID + '" InheritFromParent="' + $scope.attributedata[i].IsInheritFromParent + '" ChooseFromParent="' + $scope.attributedata[i].IsChooseFromParent + '" attributeid="' + $scope.attributedata[i].ID + '" id="fields.MultiSelectDropDown_' + $scope.attributedata[i].ID + "_" + j + '" editabletypeid="percentagetype' + $scope.attributedata[i].ID + '_' + j + '" data-type="percentagetype' + $scope.attributedata[i].ID + '_' + j + '"  my-qtip2 qtip-content=\"' + price.LevelName + '\"  attributename=\"' + price.LevelName + '\"  ><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + j + ')"></span></a></div></div>';
                            } else {
                                $scope.EnableDisableControlsHolder["Selection_" + $scope.attributedata[i].ID] = true;
                                $scope.dyn_Cont += '<div  class=\"control-group targetPricingList\"><label class=\"control-label\" for=\"label\">' + price.LevelName + '</label> <div class="controls"><span class="editable"><span  ng-bind-html="renderHtml(treeTexts.multiselectdropdown_text_' + $scope.attributedata[i].ID + '_' + j + ')"></span></span></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 17) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 0) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value;
                            }
                        } else {
                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = [];
                        }
                        $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        } else {
                            if ($scope.Islock_temp == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabletagwords href=\"javascript:;\" data-mode=\"inline\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '" id="TagWordsCaption_' + $scope.attributedata[i].ID + '"  data-ng-model=\"fields.TagWordsCaption_' + $scope.attributedata[i].ID + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" data-original-title="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\"  >{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            } else if ($scope.Islock_temp == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 17) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        if ($scope.attributedata[i].Caption[0] != undefined) {
                            if ($scope.attributedata[i].Caption.length > 0) {
                                $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
                                $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Value;
                            }
                        } else {
                            $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["TagWordsSeleted_" + $scope.attributedata[i].ID] = [];
                        }
                        $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><a  xeditabletagwords href=\"javascript:;\" data-mode=\"inline\" attributeTypeID="' + $scope.attributedata[i].TypeID + '" entityid="' + ID + '" attributeid="' + $scope.attributedata[i].ID + '_taskedit"   data-ng-model=\"fields.TagWordsCaption_' + $scope.attributedata[i].ID + '\" data-type=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\" data-original-title="' + $scope.attributedata[i].Lable + '" attributename=\"' + $scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] + '\"  >{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</a></div></div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.TagWordsCaption_' + $scope.attributedata[i].ID + '}}</label></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 16) {
                        var dateExpireUTCval = "";
                        var dateExpireval = "";
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
                        var inlineEditabletitile = $scope.attributedata[i].Caption;
                        var dateExpire;
                        if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
                            var dateExpireUTCval = "";
                            var dateExpireval = "";
                            dateExpireval = new Date.create($scope.attributedata[i].Value);
                            dateExpire = dateFormat(dateExpireval);
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = dateExpire;
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = dateExpire;
                        } else {
                            dateExpire = "-"
                            $scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
                            $scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
                        }
                        $scope.fields["ExpireDateTime_" + $scope.attributedata[i].ID] = dateExpire;
                        if ($scope.attributedata[i].IsReadOnly == true) {
                            $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span>{{fields.ExpireDateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                        } else {
                            if ($scope.IsLock == false) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span><a data-AttributeID="' + $scope.attributedata[i].ID + '" data-expiredate="' + dateExpire + '" data-attrlable="' + $scope.attributedata[i].Lable + '" ng-click="openExpireAction($event)">{{fields.ExpireDateTime_' + $scope.attributedata[i].ID + '}}</a></span></div></div>';
                            } else if ($scope.IsLock == true) {
                                $scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span>{{fields.ExpireDateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
                            }
                        }
                    } else if ($scope.attributedata[i].TypeID == 18) {
                        $scope.dyn_Cont += '<div class=\"control-group\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls\"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
                    }
                }
                if (fromassignedplace) {
                    if (tasktype == taskType.Work_Task) $("#workAssetdynamicdetail").html($compile($scope.dyn_Cont)($scope));
                    if (tasktype == taskType.Approval_Task) $("#approvalworktaskdynamicHolder").html($compile($scope.dyn_Cont)($scope));
                    if (tasktype == taskType.Reviewal_Task) $("#reviewtaskdynamicholder").html($compile($scope.dyn_Cont)($scope));
                    if (tasktype == taskType.Proof_approval_task) $("#proofHQtaskdynamicholder").html($compile($scope.dyn_Cont)($scope));
                    if (tasktype == taskType.dalim_approval_task) $("#dalimtaskdynamicholder").html($compile($scope.dyn_Cont)($scope));
                } else $("#unassignedtaskdynamicdataholder").html($compile($scope.dyn_Cont)($scope));
                HideAttributeToAttributeRelationsOnPageLoad();
                for (i = 0; i < $scope.attributedata.length; i++) {
                    if (($scope.attributedata[i].TypeID == 3 || $scope.attributedata[i].TypeID == 4 || $scope.attributedata[i].TypeID == 6 || $scope.attributedata[i].TypeID == 7 || $scope.attributedata[i].TypeID == 12) && $scope.attributedata[i].IsSpecial == false) {
                        if ($scope.attributedata[i].TypeID == 12) {
                            for (var j = 0; j < $scope.attributedata[i].Lable.length; j++) {
                                if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null) {
                                    if (($scope.attributedata[i].Lable.length - 1) == j) {
                                        var k = $scope.attributedata[i].Value.length - $scope.attributedata[i].Lable.length;
                                        for (k; k < $scope.attributedata[i].Value.length; k++) {
                                            $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[k].Nodeid, 12);
                                        }
                                    } else {
                                        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid, 12);
                                    }
                                }
                            }
                        } else if ($scope.attributedata[i].TypeID == 6) {
                            for (var j = 0; j < $scope.attributedata[i].Lable.length - 1; j++) {
                                if ($scope.attributedata[i].Value[j] != undefined && $scope.attributedata[i].Value[j] != null) $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Lable[j].Level, $scope.attributedata[i].Value[j].Nodeid, 6);
                            }
                        } else if ($scope.attributedata[i].TypeID == 4) {
                            $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.attributedata[i].Value, 4);
                        } else if ($scope.attributedata[i].TypeID == 7) {
                            $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.fields["Tree_" + $scope.attributedata[i].ID], 7);
                        } else {
                            $scope.ShowHideAttributeToAttributeRelationsOnpageLoad($scope.attributedata[i].ID, $scope.attributedata[i].Level, $scope.attributedata[i].Value, $scope.attributedata[i].TypeID);
                        }
                    }
                }
            }
        }
        $scope.fnTimeOut = function () {
            $('#TaskfilelistWorkFlow').empty();
            $('#TaskdragfilesAttachment').show();
            $("#totalProgress").empty();
            $("#totalProgress").append('<span class="pull-left count">0 of 0 Uploaded</span><span class="size">0 B / 0 B</span>' + '<div class="progress progress-striped active">' + '<div style="width: 0%" class="bar"></div>' + '</div>');
            $scope.StrartUpload();
        }
        timerObj.Loaduploader = $timeout(function () {
            $scope.StrartUpload();
        }, 100);
        var uploader = '';
        $scope.StrartUpload = function () {
            if (uploader != '') {
                uploader.destroy();
            }
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                uploader = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfilesforTask',
                    container: 'filescontainerofTask',
                    max_file_size: '10000mb',
                    url: amazonURL + cloudsetup.BucketName,
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    urlstream_upload: true,
                    multiple_queues: true,
                    file_data_name: 'file',
                    multipart: true,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    },
                });
                uploader.bind('Init', function (up, params) {
                    uploader.splice();
                });
                $('#uploadfiles').click(function (e) {
                    uploader.start();
                    e.preventDefault();
                });
                $('#clearUploader').click(function (e) {
                    uploader.destroy();
                    $('.moxie-shim').remove();
                });
                uploader.init();
                $('#pickfilesforTask1').each(function () {
                    var input = new mOxie.FileInput({
                        browse_button: this,
                        multiple: $scope.IsNotVersioning
                    });
                    input.onchange = function (event) {
                        uploader.addFile(input.files);
                    };
                    input.init();
                });
                uploader.bind('FilesAdded', function (up, files) {
                    totalfilecount = totalfilecount + files.length;
                    $.each(files, function (i, file) {
                        var damid = 0;
                        $scope.Fileids.push(file.id);
                        var ste = file.name.split('.')[file.name.split('.').length - 1];
                        if (ste != undefined) ste = ste.toLowerCase();
                        var damCount = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                            return e.damExtention == ste
                        }).length;
                        if (damCount > 0) {
                            var selecteddamtype = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                                return e.damExtention == ste
                            });
                        } else {
                            var wildcardExt = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                                return e.damExtention.indexOf("*") != -1
                            });
                            var iswildcard = [];
                            iswildcard = $.grep(wildcardExt, function (e) {
                                return e.damExtention.substring(0, e.damExtention.length - 1).indexOf(ste) != -1
                            });
                            if (iswildcard.length > 0) var selecteddamtype = iswildcard;
                        }
                        $scope.AllDamTypes = [];
                        $scope.assetfields["tagAllOptions_" + file.id] = {
                            multiple: false,
                            allowClear: true,
                            data: $scope.AssetCategory["AssetTypesList_" + file.id],
                            formatResult: $scope.formatResult,
                            formatSelection: $scope.formatSelection,
                            dropdownCssClass: "bigdrop",
                            escapeMarkup: function (m) {
                                return m;
                            }
                        };
                        if (selecteddamtype != undefined && selecteddamtype != null) {
                            damid = selecteddamtype[0].Id;
                            var ownerID = parseInt($scope.OwnerID, 10);
                            var tempval = [];
                            var tempassty = "";
                            for (var k = 0; k < selecteddamtype.length; k++) {
                                tempassty = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                                    return e.id == selecteddamtype[k].Id
                                });
                                if (tempassty != undefined && tempassty.length > 0) var tempattr = $.grep(tempval, function (e) {
                                    return e.id == tempassty[0].id
                                });
                                if (tempattr.length == 0) tempval.push(tempassty[0]);
                            }
                            if (tempval.length > 0) {
                                $scope.AssetCategory["AssetTypesList_" + file.id] = tempval;
                                $scope.assetfields["tagAllOptions_" + file.id].data = $scope.AssetCategory["AssetTypesList_" + file.id];
                            } else { }
                        }
                        TaskCreationService.GetAssetCategoryTree(damid).then(function (data) {
                            if (data.Response != null) {
                                $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                $scope.AssetCategory["filterval_" + file.id] = '';
                                var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                            }
                        });
                        $scope.AssetCategory["treestructure_" + file.id] = '-';
                        if ($scope.AssetCategory["AssetTypesList_" + file.id] != undefined) if ($scope.AssetCategory["AssetTypesList_" + file.id].length > 1 || $scope.AssetCategory["AssetTypesList_" + file.id].length == 0) damid = 0;
                        TaskCreationService.GetDamAttributeRelation(damid).then(function (attrreldata) {
                            $scope.dyn_ContAsset = "";
                            if (attrreldata.Response != undefined && attrreldata.Response != null) {
                                $scope.dyn_ContAsset = "";
                                $scope.DAMattributesRelationList = attrreldata.Response;
                                FileAttributes.AssetType.push(damid);                                
                                for (var i = 0; i < $scope.DAMattributesRelationList.length; i++) {
                                    if ($scope.DAMattributesRelationList[i].AttributeTypeID == 1) {
                                        if ($scope.DAMattributesRelationList[i].ID != 70) {
                                            if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.Name) {
                                                $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                                $scope.assetfields["TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                            } else {
                                                $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                                $scope.assetfields["TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                            }
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 3) {
                                        if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.Owner) {
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" dirownernameautopopulate placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.OwnerList[0].UserName;
                                        } else if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.FiscalYear) {
                                            $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"> <option value=\"\"> Select " + $scope.DAMattributesRelationList[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                        } else {
                                            $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"> <option value=\"\"> Select " + $scope.DAMattributesRelationList[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 2) {
                                        $scope.dyn_ContAsset += "<div class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"assetfields.TextMultiLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" name=\"assetfields.TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-model=\"assetfields.TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\" rows=\"3\"></textarea></div></div>";
                                        $scope.assetfields["TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 4) {
                                        $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = [];
                                        $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                        var defaultmultiselectvalue = $scope.DAMattributesRelationList[i].DefaultValue.split(',');
                                        if ($scope.DAMattributesRelationList[i].DefaultValue != "") {
                                            for (var v = 0, val; val = defaultmultiselectvalue[v++];) {
                                                $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID].push(parseInt(val));
                                            }
                                        } else {
                                            $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = [];
                                        }
                                        $scope.dyn_ContAsset += "<div class=\"control-group attachmentMultiselect\"><label class=\"control-label\" for=\"assetfields.ListMultiSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select class=\"multiselect\" multiple=\"multiple\"  multiselect-dropdown data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  > </select></div></div>";
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 5 && $scope.DAMattributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.DAMattributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                                        $scope.calanderopened = false;
                                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                        $scope.MaxValue = $scope.DAMattributesRelationList[i].MaxValue;
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                        } else {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                        } else {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + 100000);
                                        }
                                        $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + "</label><div class=\"controls\"><span class='editable'>{{assetfields.DateTime_" + $scope.DAMattributesRelationList[i].ID + "}}</span><input type=\"text\" class=\"txtbx DatePartctrl\" " + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-click=\"Calanderopen($event,Date_" + file.id + $scope.DAMattributesRelationList[i].ID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Date_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" min-date=\"assetfields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\" ng-model=\"assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"></div></div>";
                                        $scope.assetfields["assetfields.DatePart_Calander_Open" + file.id + $scope.DAMattributesRelationList[i].ID] = false;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 16) {
                                        $scope.calanderopened = false;
                                        $scope.MinValue = $scope.DAMattributesRelationList[i].MinValue;
                                        $scope.MaxValue = $scope.DAMattributesRelationList[i].MaxValue;
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                        } else {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                        } else {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + 100000);
                                        }
                                        $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + "</label><div class=\"controls\"><span class='editable'>{{assetfields.DateTime_" + $scope.DAMattributesRelationList[i].ID + "}}</span><input type=\"text\" class=\"txtbx DatePartctrl\" " + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-click=\"Calanderopen($event,'Date_" + file.id + $scope.DAMattributesRelationList[i].ID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Date_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" min-date=\"assetfields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\" ng-model=\"assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"></div></div>";
                                        $scope.assetfields["assetfields.DatePart_Calander_Open" + file.id + $scope.DAMattributesRelationList[i].ID] = false;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 12) {
                                        var totLevelCnt1 = $scope.DAMattributesRelationList[i].Levels.length;
                                        $scope.dyn_Cont += "<div class=\"control-group pull-left\">";
                                        for (var j = 0; j < totLevelCnt1; j++) {
                                            if (totLevelCnt1 == 1) {
                                                $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                                    return item.Caption
                                                };
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                                    return item.Caption
                                                };
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                                $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12,'" + file.id + "' )\" ng-model=\"assetfields.MultiSelectDropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                                $scope.setFieldKeys();
                                            } else {
                                                $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                                    return item.Caption
                                                };
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                                    return item.Caption
                                                };
                                                if (j == 0) {
                                                    $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                                    $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                                    $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                                    $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12,'" + file.id + "' )\" ng-model=\"assetfields.MultiSelectDropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                                } else {
                                                    $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                                    if (j == ($scope.DAMattributesRelationList[i].Levels.length - 1)) {
                                                        $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                                        $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                        $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 ,'" + file.id + "')\" ng-model=\"assetfields.MultiSelectDropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                                    } else {
                                                        $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                                        $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                        $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12,'" + file.id + "' )\" ng-model=\"assetfields.MultiSelectDropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                                    }
                                                }
                                            }
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 10) {
                                        $scope.items = [];
                                        $scope.MinValue = $scope.DAMatributesRelationList[i].MinValue;
                                        $scope.MaxValue = $scope.DAMattributesRelationList[i].MaxValue;
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                        } else {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                        } else {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + 100000);
                                        }
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                        $scope.OptionObj["option_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].Options;
                                        $scope.items.push({
                                            startDate: [],
                                            endDate: [],
                                            comment: '',
                                            sortorder: 0,
                                            fileid: 0
                                        });
                                        $scope.assetfields["Period_" + file.id] = [];
                                        $scope.assetfields["Period_" + file.id].push({
                                            startDate: [],
                                            endDate: [],
                                            comment: '',
                                            sortorder: 0,
                                            fileid: file.id
                                        });
                                        $scope.dyn_ContAsset += "    <div class=\"control-group\"><label for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.DAMattributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in assetfields.Period_" + file.id + " \" ng-form=\"subForm\">";
                                        $scope.dyn_ContAsset += "<div class=\"row-fluid\"><div class=\"inputHolder span5 noBorder\">";
                                        $scope.dyn_ContAsset += "<input class=\"sdate margin-bottom0x Period_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-model=\"item.startDate\" ng-click=\"Calanderopen($event,'Start_Date_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Start_Date_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" min-date=\"assetfields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  placeholder=\"-- Start date --\"/><input class=\"edate margin-bottom0x Period_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" type=\"text\" name=\"enddate\" id=\"item.endDate\" ng-model=\"item.endDate\" ng-click=\"Calanderopen($event,'End_Date" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"assetfields.DatePart_Calander_Open" + "item.endDate" + "\" min-date=\"assetfields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- End date --\"/><input class=\"dateComment txtbx\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + $scope.DAMattributesRelationList[i].Caption + " Comment --\" />";
                                        $scope.dyn_ContAsset += "</div><div class=\"buttonHolder span1\" ng-show=\"$first==false\">";
                                        $scope.dyn_ContAsset += "<a ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew('" + file.id + "')\">[Add " + $scope.DAMattributesRelationList[i].Caption + "]</a></div></div>";
                                        $scope.assetfields["Period_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = "";
                                        $scope.assetfields["assetfields.DatePart_Calander_Open" + "item.startDate"] = false;
                                        $scope.assetfields["assetfields.DatePart_Calander_Open" + "item.endDate"] = false;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 13) {
                                        $scope.PercentageVisibleSettings["AttributeId_Levels_" + file.id + $scope.DAMattributesRelationList[i].AttributeID.toString() + ""] = true;
                                        $scope.DropDownTreePricing["AttributeId_Levels_" + file.id + $scope.DAMattributesRelationList[i].AttributeID.toString() + ""] = $scope.DAMattributesRelationList[i].DropDownPricing;
                                        $scope.dyn_ContAsset += "<div drowdowntreepercentagemultiselectionasset data-purpose='entity' data-fileid=" + file.id.toString() + " data-attributeid=" + $scope.DAMattributesRelationList[i].AttributeID.toString() + "></div>";
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 8) {
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                        $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-model=\"assetfields.TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                                        $scope.assetfields["TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 17) {
                                        $scope.assetfields["ListTagwords_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = [];
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                        $scope.OptionObj["tagoption_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = [];
                                        $scope.tempscope = [];
                                        $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" class=\"control-group\">";
                                        $scope.dyn_ContAsset += "<label class=\"control-label\" for=\"assetfields.ListTagwords_ " + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].AttributeCaption + " </label>";
                                        $scope.dyn_ContAsset += "<div class=\"controls\">";
                                        $scope.dyn_ContAsset += "<directive-tagwords item-attrid = \"" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"assetfields.ListTagwords_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                                        $scope.dyn_ContAsset += "</div></div>";
                                        if ($scope.DAMattributesRelationList[i].InheritFromParent) { } else { }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 6) {
                                        var totLevelCnt = $scope.DAMattributesRelationList[i].Levels.length;
                                        for (var j = 0; j < $scope.DAMattributesRelationList[i].Levels.length; j++) {
                                            $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                            $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                            $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                            $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                                return item.Caption
                                            };
                                            $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                                return item.Caption
                                            };
                                            if (j == 0) {
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                                $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                                $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6,'" + file.id + "' )\"  ng-model=\"assetfields.DropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                            } else {
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                                $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 ,'" + file.id + "')\"  ng-model=\"assetfields.DropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                            }
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 7) {
                                        $scope.assetfields["Tree_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = [];
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                        $scope.treesrcdirec["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                        if ($scope.treesrcdirec["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID].length > 0) {
                                            treeTextVisbileflag = false;
                                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID])) {
                                                $scope.treePreviewObj["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                            } else $scope.treePreviewObj["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = false;
                                        } else {
                                            $scope.treePreviewObj["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = false;
                                        }
                                        $scope.dyn_ContAsset += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '\" class="control-group treeNode-control-group">';
                                        $scope.dyn_ContAsset += '    <label class="control-label">' + $scope.DAMattributesRelationList[i].AttributeCaption + '</label>';
                                        $scope.dyn_ContAsset += '    <div class="controls treeNode-controls">';
                                        $scope.dyn_ContAsset += '        <div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '"></div>';
                                        $scope.dyn_ContAsset += '        <div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '">';
                                        $scope.dyn_ContAsset += '            <span ng-if="doing_async">...loading...</span>';
                                        $scope.dyn_ContAsset += '            <abn-tree tree-filter="filterValue_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '\" accessable="' + $scope.DAMattributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                                        $scope.dyn_ContAsset += '        </div>';
                                        $scope.dyn_ContAsset += '        <div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '\">';
                                        $scope.dyn_ContAsset += '            <div class="controls">';
                                        $scope.dyn_ContAsset += '                <eu-tree tree-data=\"treesrcdirec.Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.DAMattributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                        $scope.dyn_ContAsset += '        </div></div>';
                                        $scope.dyn_ContAsset += '</div></div>';
                                    }
                                }
                            }
                            $scope.assetfields["assetname_" + file.id] = file.name;
                            if (selecteddamtype != undefined && selecteddamtype != null) {
                                var dataid = '';
                                var rec = [];
                                rec = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                                    return e.Id == selecteddamtype[0].Id
                                });
                                if (rec.length > 0) dataid = rec[0].Id;
                                if ($scope.assetfields["tagAllOptions_" + file.id].data.length == 1) $scope.assetfields["assettype_" + file.id] = rec[0];
                                if ($scope.AssetCategory["AssetTypesList_" + file.id].length == 1) {
                                    TaskCreationService.GetAssetCategoryTree(dataid).then(function (data) {
                                        if (data.Response != null) {
                                            $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                            $scope.AssetCategory["filterval_" + file.id] = '';
                                            var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                            $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                                        }
                                    });
                                    if ($scope.assetfields["tagAllOptions_" + file.id].data.length == 1) {
                                        TaskCreationService.GetAssetCategoryPathInAssetEdit(dataid).then(function (datapath) {
                                            if (datapath.Response != null) {
                                                if (datapath.Response[1].AssetTypeIDs.length > 0) {
                                                    $scope.AssetCategory["treestructure_" + file.id] = datapath.Response[0].CategoryPath;
                                                } else $scope.assetfields["assettype_" + file.id] = '';
                                            }
                                        });
                                    } else $scope.assetfields["assettype_" + file.id] = '';
                                } else {
                                    $scope.assetfields["assettype_" + file.id] = rec[0];
                                    $scope.oldfiledetails["AssetType_" + file.id] = '';
                                    TaskCreationService.GetAssetCategoryTree(0).then(function (data) {
                                        if (data.Response != null) {
                                            $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                            $scope.AssetCategory["filterval_" + file.id] = '';
                                            var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                            $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                                        }
                                    });
                                }
                            } else {
                                $scope.assetfields["tagAllOptions_" + file.id] = {
                                    multiple: false,
                                    allowClear: true,
                                    data: [],
                                    formatResult: $scope.formatResult,
                                    formatSelection: $scope.formatSelection,
                                    dropdownCssClass: "bigdrop",
                                    escapeMarkup: function (m) {
                                        return m;
                                    }
                                };
                                $scope.oldfiledetails["AssetType_" + file.id] = [];
                                TaskCreationService.GetAssetCategoryTree(0).then(function (data) {
                                    if (data.Response != null) {
                                        $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                        $scope.AssetCategory["filterval_" + file.id] = '';
                                        var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                        $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                                    }
                                });
                            }
                            $scope.IsAttachmentPresent = true;
                            $('#assetassettaskentityattachassetdata').append($compile('<div id="' + file.id + '" class="attachmentBox" Data-role="Attachment" data-size="' + file.size + '" data-size-uploaded="0" data-status="false">' + '<div class="row-fluid">' + '<div class="span12">' + '<div class="span2 leftSection">' + ' <i id="icon_ok' + file.id + '" class="icon-ok-sign" style="display:none;"  ></i>' + ' <div class="fileType-Preview">' + ' <div id="Preview_' + file.id + '" class="fileType-PreviewDiv">' + ' </div>' + '</div>' + ' <div class="progress progress-striped active">' + ' <div style="width: 0%" class="bar"></div>' + '</div>' + ' <div class="progressInfo">' + '  <span class="progress-percent"></span>' + '<span class="fileSize">' + plupload.formatSize(file.size) + '' + '</span>' + '</div>' + '</div>' + ' <div class="span10 rightSection">' + '<div class="info">' + '  <span class="name">' + ' <i class="icon-file-alt"></i>' + file.name + '</span>' + ' <span class="size">' + ' <i class="icon-remove removefile" data-fileid="' + file.id + '"></i>' + ' </span>' + '</div>' + '<div  class="formInfo">' + ' <form class="form-float">' + '<div class=\"control-group fullSpace\">' + '<label class=\"control-label\">' + $translate.instant('LanguageContents.Res_5064.Caption') + ' </label>' + '<div class=\"controls\">' + '<a ng-click="AssetTypeTreePopUp(\'' + file.id + '\')">{{AssetCategory.treestructure_' + file.id + '}}</a>' + '</div>' + '<div class=\"controls\">' + '<input ui-select2="assetfields.tagAllOptions_' + file.id + '"  type="hidden"  ng-model="assetfields.assettype_' + file.id + '" ng-change="onDamtypechange(\'' + file.id + '\')" > </input>' + '<span class="imgTypeSection">' + '<i class=\"icon-reorder imgTypeEditIcon displayNone\" ng-click="AssetTypeTreePopUp(\'' + file.id + '\')"></i>' + '<span class=\"imgTypeEditIcon\" ng-click="AssetTypeTreePopUp(\'' + file.id + '\')">' + '<img src="assets/img/treeIcon.png"' + '</span>' + '</span>' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Asset name </label>' + '<div class=\"controls\">' + '<input class=\"txtbx\" type="text"  data-ng-model="assetfields.assetname_' + file.id + '" name="DescVal_' + file.id + '" id="desc_' + file.id + '" placeholder="Name">' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Description </label>' + '<div class=\"controls\">' + '<textarea class=\"small-textarea\"  name=\"assetfields.TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" ng-model=\"assetfields.TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" id=\"TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" placeholder="Description" rows="3"></textarea>' + '</div>' + '</div>' + '<div id = "div_' + file.id + '">' + $scope.dyn_ContAsset + '</div>' + '</form>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>')($scope));
                            if (o.Mime.mimes[ste.toLowerCase()] == undefined) {
                                var img = $('<img id="' + file.id + '">');
                                img.attr('src', imagesrcpath + "DAMFiles/StaticPreview_small/" + ste.toUpperCase() + ".jpg");
                                img.attr("onerror", "this.onerror=null; this.src=" + imagesrcpath + "'DAMFiles/Preview/NoPreview.jpg';");
                                img.appendTo('#Preview_' + file.id);
                            } else {
                                if (o.Mime.mimes[ste.toLowerCase()].split('/')[0] == 'image') {
                                    var img = new o.Image();
                                    img.onload = function () {
                                        var li = document.createElement('div');
                                        li.id = this.uid;
                                        document.getElementById('Preview_' + file.id).appendChild(li);
                                        this.embed(li.id, {
                                            width: 80,
                                            height: 80,
                                            crop: true
                                        });
                                    };
                                    img.load(file.getSource());
                                } else {
                                    var img = $('<img id="' + file.id + '">');
                                    img.attr('src', imagesrcpath + "DAMFiles/StaticPreview_small/" + ste.toUpperCase() + ".jpg");
                                    img.appendTo('#Preview_' + file.id);
                                }
                            }
                        });
                    });
                    up.refresh();
                    timerObj.uploaderProgress = $timeout(function () {
                        TotalAssetUploadProgress();
                    }, 500);
                });
                uploader.bind('UploadProgress', function (up, file) {
                    $('#' + file.id + " .bar").css("width", file.percent + "%");
                    $('#' + file.id + " .size").html(plupload.formatSize(Math.round(file.size * (file.percent / 100))) + ' / ' + plupload.formatSize(file.size));
                    $('#' + file.id).attr('data-size-uploaded', Math.round(file.size * (file.percent / 100)));
                    TotalAssetUploadProgress();
                });
                uploader.bind('Error', function (up, err) {
                    $('#TaskfilelistWorkFlow').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                    up.refresh();
                });
                uploader.bind('FileUploaded', function (up, file, response) {
                    $('#' + file.id).attr('data-status', 'true');
                    var fileDescription = $('#Form_' + file.id + '').find('input[name="DescVal_' + file.id + '"]').val();
                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    $scope.SaveAssetFileDetails(file, response.response, fileDescription);
                    $scope.globalAttachment = true;
                    TotalAssetUploadProgress();
                });
                uploader.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKeypath = TenantFilePath.replace(/\\/g, "\/");
                    var uniqueKey = (uniqueKeypath + "DAMFiles/Original/" + file.id + fileext);
                    uploader.settings.multipart_params.key = uniqueKey;
                    uploader.settings.multipart_params.Filename = uniqueKey;
                });
            } else {
                uploader = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    //browse_button: 'pickfilesforTask',
                    browse_button: 'pickfilesforTask',
                    container: 'filescontainerofTask',
                    max_file_size: '10000mb',
                    url: 'Handlers/DamFileHandler.ashx?Type=Attachment',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    chunk_size: '1mb',
                    max_retries: 3,
                    multipart_params: {}
                });
                uploader.bind('Init', function (up, params) {
                    uploader.splice();
                });
                $('#uploadfiles').click(function (e) {
                    uploader.start();
                    e.preventDefault();
                });
                $('#clearUploader').click(function (e) {
                    uploader.destroy();
                    $('.moxie-shim').remove();
                });
                uploader.init();
                $('#pickfilesforTask1').each(function () {
                    var input = new mOxie.FileInput({
                        browse_button: this,
                        multiple: $scope.IsNotVersioning
                    });
                    input.onchange = function (event) {
                        uploader.addFile(input.files);
                    };
                    input.init();
                });
                uploader.bind('FilesAdded', function (up, files) {
                    totalfilecount = totalfilecount + files.length;
                    $.each(files, function (i, file) {
                        var damid = 0;
                        $scope.Fileids.push(file.id);
                        var ste = file.name.split('.')[file.name.split('.').length - 1];
                        if (ste != undefined) ste = ste.toLowerCase();
                        var damCount = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                            return e.damExtention == ste
                        }).length;
                        if (damCount > 0) {
                            var selecteddamtype = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                                return e.damExtention == ste
                            });
                        } else {
                            var wildcardExt = $.grep($scope.DAMtypeswithExt.m_Item2, function (e) {
                                return e.damExtention.indexOf("*") != -1
                            });
                            var iswildcard = [];
                            iswildcard = $.grep(wildcardExt, function (e) {
                                return e.damExtention.substring(0, e.damExtention.length - 1).indexOf(ste) != -1
                            });
                            if (iswildcard.length > 0) var selecteddamtype = iswildcard;
                        }
                        $scope.AllDamTypes = [];
                        $scope.assetfields["tagAllOptions_" + file.id] = {
                            multiple: false,
                            allowClear: true,
                            data: $scope.AssetCategory["AssetTypesList_" + file.id],
                            formatResult: $scope.formatResult,
                            formatSelection: $scope.formatSelection,
                            dropdownCssClass: "bigdrop",
                            escapeMarkup: function (m) {
                                return m;
                            }
                        };
                        if (selecteddamtype != undefined && selecteddamtype != null) {
                            damid = selecteddamtype[0].Id;
                            var ownerID = parseInt($scope.OwnerID, 10);
                            var tempval = [];
                            var tempassty = "";
                            for (var k = 0; k < selecteddamtype.length; k++) {
                                tempassty = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                                    return e.id == selecteddamtype[k].Id
                                });
                                if (tempassty != undefined && tempassty.length > 0) var tempattr = $.grep(tempval, function (e) {
                                    return e.id == tempassty[0].id
                                });
                                if (tempattr.length == 0) tempval.push(tempassty[0]);
                            }
                            if (tempval.length > 0) {
                                $scope.AssetCategory["AssetTypesList_" + file.id] = tempval;
                                $scope.assetfields["tagAllOptions_" + file.id].data = $scope.AssetCategory["AssetTypesList_" + file.id];
                            } else { }
                        }
                        TaskCreationService.GetAssetCategoryTree(damid).then(function (data) {
                            if (data.Response != null) {
                                $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                $scope.AssetCategory["filterval_" + file.id] = '';
                                var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                            }
                        });
                        $scope.AssetCategory["treestructure_" + file.id] = '-';
                        if ($scope.AssetCategory["AssetTypesList_" + file.id] != undefined) if ($scope.AssetCategory["AssetTypesList_" + file.id].length > 1 || $scope.AssetCategory["AssetTypesList_" + file.id].length == 0) damid = 0;
                        TaskCreationService.GetDamAttributeRelation(damid).then(function (attrreldata) {
                            $scope.dyn_ContAsset = "";
                            if (attrreldata.Response != undefined && attrreldata.Response != null) {
                                $scope.dyn_ContAsset = "";
                                $scope.DAMattributesRelationList = attrreldata.Response;
                                 FileAttributes.AssetType.push(damid);
                                for (var i = 0; i < $scope.DAMattributesRelationList.length; i++) {
                                    if ($scope.DAMattributesRelationList[i].AttributeTypeID == 1) {
                                        if ($scope.DAMattributesRelationList[i].ID != 70) {
                                            if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.Name) {
                                                $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                                $scope.assetfields["TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                            } else {
                                                $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                                $scope.assetfields["TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                            }
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 3) {
                                        if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.Owner) {
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <input class=\"txtbx\" type=\"text\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" dirownernameautopopulate placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\"></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.OwnerList[0].UserName;
                                        } else if ($scope.DAMattributesRelationList[i].ID == SystemDefiendAttributes.FiscalYear) {
                                            $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"> <option value=\"\"> Select " + $scope.DAMattributesRelationList[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                        } else {
                                            $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                            $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.ListSingleSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"> <option value=\"\"> Select " + $scope.DAMattributesRelationList[i].Caption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                            $scope.assetfields["ListSingleSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 2) {
                                        $scope.dyn_ContAsset += "<div class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"assetfields.TextMultiLine_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" name=\"assetfields.TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-model=\"assetfields.TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\" rows=\"3\"></textarea></div></div>";
                                        $scope.assetfields["TextMultiLine_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 4) {
                                        $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = [];
                                        $scope.OptionObj["option_" + file.id + $scope.DAMattributesRelationList[i].ID] = $scope.DAMattributesRelationList[i].Options;
                                        var defaultmultiselectvalue = $scope.DAMattributesRelationList[i].DefaultValue.split(',');
                                        if ($scope.DAMattributesRelationList[i].DefaultValue != "") {
                                            for (var v = 0, val; val = defaultmultiselectvalue[v++];) {
                                                $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID].push(parseInt(val));
                                            }
                                        } else {
                                            $scope.assetfields["ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID] = [];
                                        }
                                        $scope.dyn_ContAsset += "<div class=\"control-group attachmentMultiselect\"><label class=\"control-label\" for=\"assetfields.ListMultiSelection_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"> <select class=\"multiselect\" multiple=\"multiple\"  multiselect-dropdown data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].ID + ",0,0,3)\" ng-model=\"assetfields.ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  id=\"ListMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"  > </select></div></div>";
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 5 && $scope.DAMattributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.DAMattributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                                        $scope.calanderopened = false;
                                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                                        $scope.MaxValue = $scope.DAMattributesRelationList[i].MaxValue;
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                        } else {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                        } else {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + 100000);
                                        }
                                        $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + "</label><div class=\"controls\"><span class='editable'>{{assetfields.DateTime_" + $scope.DAMattributesRelationList[i].ID + "}}</span><input type=\"text\" class=\"txtbx DatePartctrl\" " + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-click=\"Calanderopen($event,'Date_" + file.id + $scope.DAMattributesRelationList[i].ID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Date_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" min-date=\"assetfields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\" ng-model=\"assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"></div></div>";
                                        $scope.assetfields["assetfields.DatePart_Calander_Open" + file.id + $scope.DAMattributesRelationList[i].ID] = false;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 16) {
                                        $scope.calanderopened = false;
                                        $scope.MinValue = $scope.DAMattributesRelationList[i].MinValue;
                                        $scope.MaxValue = $scope.DAMattributesRelationList[i].MaxValue;
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                        } else {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                        } else {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + 100000);
                                        }
                                        $scope.dyn_ContAsset += "<div class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Caption + "</label><div class=\"controls\"><span class='editable'>{{assetfields.DateTime_" + $scope.DAMattributesRelationList[i].ID + "}}</span><input type=\"text\" class=\"txtbx DatePartctrl\" " + file.id + $scope.DAMattributesRelationList[i].ID + "\" id=\"DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" ng-click=\"Calanderopen($event,'Date_" + file.id + $scope.DAMattributesRelationList[i].ID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"Date_" + file.id + $scope.DAMattributesRelationList[i].ID + "\" min-date=\"assetfields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.DAMattributesRelationList[i].Caption + "\" ng-model=\"assetfields.DatePart_" + file.id + $scope.DAMattributesRelationList[i].ID + "\"></div></div>";
                                        $scope.assetfields["assetfields.DatePart_Calander_Open" + file.id + $scope.DAMattributesRelationList[i].ID] = false;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 12) {
                                        var totLevelCnt1 = $scope.DAMattributesRelationList[i].Levels.length;
                                        $scope.dyn_Cont += "<div class=\"control-group pull-left\">";
                                        for (var j = 0; j < totLevelCnt1; j++) {
                                            if (totLevelCnt1 == 1) {
                                                $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                                    return item.Caption
                                                };
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                                    return item.Caption
                                                };
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                                $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12,'" + file.id + "' )\" ng-model=\"assetfields.MultiSelectDropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                                $scope.setFieldKeys();
                                            } else {
                                                $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                                    return item.Caption
                                                };
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                                    return item.Caption
                                                };
                                                if (j == 0) {
                                                    $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                                    $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                                    $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                                    $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12,'" + file.id + "' )\" ng-model=\"assetfields.MultiSelectDropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                                } else {
                                                    $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                                    if (j == ($scope.DAMattributesRelationList[i].Levels.length - 1)) {
                                                        $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                                        $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                        $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 ,'" + file.id + "')\" ng-model=\"assetfields.MultiSelectDropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                                    } else {
                                                        $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                                        $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.MultiSelectDropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                        $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12,'" + file.id + "' )\" ng-model=\"assetfields.MultiSelectDropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                                    }
                                                }
                                            }
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 10) {
                                        $scope.items = [];
                                        $scope.MinValue = $scope.DAMatributesRelationList[i].MinValue;
                                        $scope.MaxValue = $scope.DAMattributesRelationList[i].MaxValue;
                                        $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID] = new Date.create();
                                        if ($scope.MinValue < 0) {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                                        } else {
                                            $scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                                        }
                                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                                        } else {
                                            $scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].setDate($scope.assetfields["DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID].getDate() + 100000);
                                        }
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                        $scope.OptionObj["option_" + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].Options;
                                        $scope.items.push({
                                            startDate: [],
                                            endDate: [],
                                            comment: '',
                                            sortorder: 0,
                                            fileid: 0
                                        });
                                        $scope.assetfields["Period_" + file.id] = [];
                                        $scope.assetfields["Period_" + file.id].push({
                                            startDate: [],
                                            endDate: [],
                                            comment: '',
                                            sortorder: 0,
                                            fileid: file.id
                                        });
                                        $scope.dyn_ContAsset += "    <div class=\"control-group\"><label for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.DAMattributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in assetfields.Period_" + file.id + " \" ng-form=\"subForm\">";
                                        $scope.dyn_ContAsset += "<div class=\"row-fluid\"><div class=\"inputHolder span5 noBorder\">";
                                        $scope.dyn_ContAsset += "<input class=\"sdate margin-bottom0x Period_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-model=\"item.startDate\" ng-click=\"Calanderopen($event,'start_date_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"start_date_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" min-date=\"assetfields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  placeholder=\"-- Start date --\"/><input class=\"edate margin-bottom0x Period_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" type=\"text\" name=\"enddate\" id=\"item.endDate\" ng-model=\"item.endDate\" ng-click=\"Calanderopen($event,'end_date_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"end_date_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" min-date=\"assetfields.DatePartMinDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" max-date=\"assetfields.DatePartMaxDate_" + $scope.DAMattributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- End date --\"/><input class=\"dateComment txtbx\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + $scope.DAMattributesRelationList[i].Caption + " Comment --\" />";
                                        $scope.dyn_ContAsset += "</div><div class=\"buttonHolder span1\" ng-show=\"$first==false\">";
                                        $scope.dyn_ContAsset += "<a ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew('" + file.id + "')\">[Add " + $scope.DAMattributesRelationList[i].Caption + "]</a></div></div>";
                                        $scope.assetfields["Period_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = "";
                                        $scope.assetfields["assetfields.DatePart_Calander_Open" + "item.startDate"] = false;
                                        $scope.assetfields["assetfields.DatePart_Calander_Open" + "item.endDate"] = false;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 13) {
                                        $scope.PercentageVisibleSettings["AttributeId_Levels_" + file.id + $scope.DAMattributesRelationList[i].AttributeID.toString() + ""] = true;
                                        $scope.DropDownTreePricing["AttributeId_Levels_" + file.id + $scope.DAMattributesRelationList[i].AttributeID.toString() + ""] = $scope.DAMattributesRelationList[i].DropDownPricing;
                                        $scope.dyn_ContAsset += "<div drowdowntreepercentagemultiselectionasset data-purpose='entity' data-fileid=" + file.id.toString() + " data-attributeid=" + $scope.DAMattributesRelationList[i].AttributeID.toString() + "></div>";
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 8) {
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                        $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.TextSingleLine_ " + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].Caption + " </label><div class=\"controls\"><input class=\"txtbx\" type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-model=\"assetfields.TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.DAMattributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                                        $scope.assetfields["TextSingleLine_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = $scope.DAMattributesRelationList[i].DefaultValue;
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 17) {
                                        $scope.assetfields["ListTagwords_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = [];
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                        $scope.OptionObj["tagoption_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = [];
                                        $scope.tempscope = [];
                                        $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" class=\"control-group\">";
                                        $scope.dyn_ContAsset += "<label class=\"control-label\" for=\"assetfields.ListTagwords_ " + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\">" + $scope.DAMattributesRelationList[i].AttributeCaption + " </label>";
                                        $scope.dyn_ContAsset += "<div class=\"controls\">";
                                        $scope.dyn_ContAsset += "<directive-tagwords item-attrid = \"" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"assetfields.ListTagwords_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                                        $scope.dyn_ContAsset += "</div></div>";
                                        if ($scope.DAMattributesRelationList[i].InheritFromParent) { } else { }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 6) {
                                        var totLevelCnt = $scope.DAMattributesRelationList[i].Levels.length;
                                        for (var j = 0; j < $scope.DAMattributesRelationList[i].Levels.length; j++) {
                                            $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                            $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                            $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                            $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) {
                                                return item.Caption
                                            };
                                            $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) {
                                                return item.Caption
                                            };
                                            if (j == 0) {
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                                $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                                $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6,'" + file.id + "' )\"  ng-model=\"assetfields.DropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                            } else {
                                                $scope.Dropdown["OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                                $scope.dyn_ContAsset += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"assetfields.DropDown_ " + file.id + $scope.DAMattributesRelationList[i].ID + "\">" + $scope.DAMattributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                                $scope.dyn_ContAsset += "<input ui-select2=\"Dropdown.OptionValues" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.DAMattributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 ,'" + file.id + "')\"  ng-model=\"assetfields.DropDown_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + file.id + $scope.DAMattributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                            }
                                        }
                                    } else if ($scope.DAMattributesRelationList[i].AttributeTypeID == 7) {
                                        $scope.assetfields["Tree_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = [];
                                        $scope.ShowHideAttributeOnRelation["Attribute_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                        $scope.treesrcdirec["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = JSON.parse($scope.DAMattributesRelationList[i].tree).Children;
                                        if ($scope.treesrcdirec["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID].length > 0) {
                                            treeTextVisbileflag = false;
                                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID])) {
                                                $scope.treePreviewObj["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = true;
                                            } else $scope.treePreviewObj["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = false;
                                        } else {
                                            $scope.treePreviewObj["Attr_" + file.id + $scope.DAMattributesRelationList[i].AttributeID] = false;
                                        }
                                        $scope.dyn_ContAsset += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '\" class="control-group treeNode-control-group">';
                                        $scope.dyn_ContAsset += '    <label class="control-label">' + $scope.DAMattributesRelationList[i].AttributeCaption + '</label>';
                                        $scope.dyn_ContAsset += '    <div class="controls treeNode-controls">';
                                        $scope.dyn_ContAsset += '        <div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '"></div>';
                                        $scope.dyn_ContAsset += '        <div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '">';
                                        $scope.dyn_ContAsset += '            <span ng-if="doing_async">...loading...</span>';
                                        $scope.dyn_ContAsset += '            <abn-tree tree-filter="filterValue_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '\" accessable="' + $scope.DAMattributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                                        $scope.dyn_ContAsset += '        </div>';
                                        $scope.dyn_ContAsset += '        <div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '\">';
                                        $scope.dyn_ContAsset += '            <div class="controls">';
                                        $scope.dyn_ContAsset += '                <eu-tree tree-data=\"treesrcdirec.Attr_' + file.id + $scope.DAMattributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.DAMattributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                        $scope.dyn_ContAsset += '        </div></div>';
                                        $scope.dyn_ContAsset += '</div></div>';
                                    }
                                }
                            }
                            $scope.assetfields["assetname_" + file.id] = file.name;
                            if (selecteddamtype != undefined && selecteddamtype != null) {
                                var dataid = '';
                                var rec = [];
                                rec = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                                    return e.Id == selecteddamtype[0].Id
                                });
                                if (rec.length > 0) dataid = rec[0].Id;
                                if ($scope.assetfields["tagAllOptions_" + file.id].data.length == 1) $scope.assetfields["assettype_" + file.id] = rec[0];
                                if ($scope.AssetCategory["AssetTypesList_" + file.id].length == 1) {
                                    TaskCreationService.GetAssetCategoryTree(dataid).then(function (data) {
                                        if (data.Response != null) {
                                            $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                            $scope.AssetCategory["filterval_" + file.id] = '';
                                            var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                            $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                                        }
                                    });
                                    if ($scope.assetfields["tagAllOptions_" + file.id].data.length == 1) {
                                        TaskCreationService.GetAssetCategoryPathInAssetEdit(dataid).then(function (datapath) {
                                            if (datapath.Response != null) {
                                                if (datapath.Response[1].AssetTypeIDs.length > 0) {
                                                    $scope.AssetCategory["treestructure_" + file.id] = datapath.Response[0].CategoryPath;
                                                } else $scope.assetfields["assettype_" + file.id] = '';
                                            }
                                        });
                                    } else $scope.assetfields["assettype_" + file.id] = '';
                                } else {
                                    $scope.assetfields["assettype_" + file.id] = rec[0];
                                    $scope.oldfiledetails["AssetType_" + file.id] = '';
                                    TaskCreationService.GetAssetCategoryTree(0).then(function (data) {
                                        if (data.Response != null) {
                                            $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                            $scope.AssetCategory["filterval_" + file.id] = '';
                                            var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                            $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                                        }
                                    });
                                }
                            } else {
                                $scope.assetfields["tagAllOptions_" + file.id] = {
                                    multiple: false,
                                    allowClear: true,
                                    data: [],
                                    formatResult: $scope.formatResult,
                                    formatSelection: $scope.formatSelection,
                                    dropdownCssClass: "bigdrop",
                                    escapeMarkup: function (m) {
                                        return m;
                                    }
                                };
                                $scope.oldfiledetails["AssetType_" + file.id] = [];
                                TaskCreationService.GetAssetCategoryTree(0).then(function (data) {
                                    if (data.Response != null) {
                                        $scope.AssetCategory["AssetCategoryTree_" + file.id] = JSON.parse(data.Response);
                                        $scope.AssetCategory["filterval_" + file.id] = '';
                                        var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + file.id + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + file.id + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
                                        $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
                                    }
                                });
                            }
                            $scope.IsAttachmentPresent = true;
                            $('#assetassettaskentityattachassetdata').append($compile('<div id="' + file.id + '" class="attachmentBox" Data-role="Attachment" data-size="' + file.size + '" data-size-uploaded="0" data-status="false">' + '<div class="row-fluid">' + '<div class="span12">' + '<div class="span2 leftSection">' + ' <i id="icon_ok' + file.id + '" class="icon-ok-sign" style="display:none;"  ></i>' + ' <div class="fileType-Preview">' + ' <div id="Preview_' + file.id + '" class="fileType-PreviewDiv">' + ' </div>' + '</div>' + ' <div class="progress progress-striped active">' + ' <div style="width: 0%" class="bar"></div>' + '</div>' + ' <div class="progressInfo">' + '  <span class="progress-percent"></span>' + '<span class="fileSize">' + plupload.formatSize(file.size) + '' + '</span>' + '</div>' + '</div>' + ' <div class="span10 rightSection">' + '<div class="info">' + '  <span class="name">' + ' <i class="icon-file-alt"></i>' + file.name + '</span>' + ' <span class="size">' + ' <i class="icon-remove removefile" data-fileid="' + file.id + '"></i>' + ' </span>' + '</div>' + '<div  class="formInfo">' + ' <form class="form-float">' + '<div class=\"control-group fullSpace\">' + '<label class=\"control-label\">' + $translate.instant('LanguageContents.Res_5064.Caption') + ' </label>' + '<div class=\"controls\">' + '<a ng-click="AssetTypeTreePopUp(\'' + file.id + '\')">{{AssetCategory.treestructure_' + file.id + '}}</a>' + '</div>' + '<div class=\"controls\">' + '<input ui-select2="assetfields.tagAllOptions_' + file.id + '"  type="hidden"  ng-model="assetfields.assettype_' + file.id + '" ng-change="onDamtypechange(\'' + file.id + '\')" > </input>' + '<span class="imgTypeSection">' + '<i class=\"icon-reorder imgTypeEditIcon displayNone\" ng-click="AssetTypeTreePopUp(\'' + file.id + '\')"></i>' + '<span class=\"imgTypeEditIcon\" ng-click="AssetTypeTreePopUp(\'' + file.id + '\')">' + '<img src="assets/img/treeIcon.png"' + '</span>' + '</span>' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Asset name </label>' + '<div class=\"controls\">' + '<input class=\"txtbx\" type="text"  data-ng-model="assetfields.assetname_' + file.id + '" name="DescVal_' + file.id + '" id="desc_' + file.id + '" placeholder="Name">' + '</div>' + '</div>' + '<div class=\"control-group\">' + '<label class=\"control-label\"> Description </label>' + '<div class=\"controls\">' + '<textarea class=\"small-textarea\"  name=\"assetfields.TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" ng-model=\"assetfields.TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" id=\"TextMultiLine_' + file.id + SystemDefiendAttributes.Description + '\" placeholder="Description" rows="3"></textarea>' + '</div>' + '</div>' + '<div id = "div_' + file.id + '">' + $scope.dyn_ContAsset + '</div>' + '</form>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>')($scope));
                            if (o.Mime.mimes[ste.toLowerCase()] == undefined) {
                                var img = $('<img id="' + file.id + '">');
                                img.attr('src', imagesrcpath + "DAMFiles/StaticPreview_small/" + ste.toUpperCase() + ".jpg");
                                img.attr("onerror", "this.onerror=null; this.src=" + imagesrcpath + "'DAMFiles/Preview/NoPreview.jpg';");
                                img.appendTo('#Preview_' + file.id);
                            } else {
                                if (o.Mime.mimes[ste.toLowerCase()].split('/')[0] == 'image') {
                                    var img = new o.Image();
                                    img.onload = function () {
                                        var li = document.createElement('div');
                                        li.id = this.uid;
                                        document.getElementById('Preview_' + file.id).appendChild(li);
                                        this.embed(li.id, {
                                            width: 80,
                                            height: 80,
                                            crop: true
                                        });
                                    };
                                    img.load(file.getSource());
                                } else {
                                    var img = $('<img id="' + file.id + '">');
                                    img.attr('src', imagesrcpath + "DAMFiles/StaticPreview_small/" + ste.toUpperCase() + ".jpg");
                                    img.appendTo('#Preview_' + file.id);
                                }
                            }
                        });
                    });
                    up.refresh();
                    timerObj.uploaderProgress = $timeout(function () {
                        TotalAssetUploadProgress();
                    }, 500);
                });
                uploader.bind('UploadProgress', function (up, file) {
                    $('#' + file.id + " .bar").css("width", file.percent + "%");
                    $('#' + file.id + " .size").html(plupload.formatSize(Math.round(file.size * (file.percent / 100))) + ' / ' + plupload.formatSize(file.size));
                    $('#' + file.id).attr('data-size-uploaded', Math.round(file.size * (file.percent / 100)));
                    TotalAssetUploadProgress();
                });
                uploader.bind('Error', function (up, err) {
                    $('#TaskfilelistWorkFlow').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
                    up.refresh();
                });
                uploader.bind('FileUploaded', function (up, file, response) {
                    $('#' + file.id).attr('data-status', 'true');
                    var fileDescription = $('#Form_' + file.id + '').find('input[name="DescVal_' + file.id + '"]').val();
                    var ind = FileAttributes.FileID.indexOf(file.id);
                    if ($scope.newTaskid > 0) {
                        $scope.SaveAssetFileDetails(file, response.response, FileAttributes.AttributeDataList[ind]);
                        TotalAssetUploadProgress();
                        $scope.globalAttachment = true;
                    }                                        
                });
                uploader.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
            }
            $('#assetassettaskentityattachassetdata').on('click', '.removefile', function () {
                var fileToRemove = $(this).attr('data-fileid');
                if (uploader.files == 0) {
                    var id = uploader_Attr.id;
                    uploader_Attr.removeFile(id);
                    $('#Asset_' + fileToRemove).remove();
                    totalfilecount = totalfilecount - 1;
                } else {
                    $.each(uploader.files, function (i, file) {
                        if (file != null && file != undefined) {
                            if (file.id == fileToRemove) {
                                uploader.removeFile(file);
                                $('#' + file.id).remove();
                                totalfilecount = totalfilecount - 1;
                            }
                        }
                    });
                }
                //if ($("#assetassettaskentityattachassetdata > div").length == 0) {
                //    $scope.addfromComputer = false;
                //    $scope.IsaddAssetsfromComputer = false;
                //    $('#totalAssetActivityAttachProgresstask .bar').css("width", Math.round((0 * 100)) + "%");
                //}
                TotalAssetUploadProgress();
            });

            function TotalAssetUploadProgress() {
                var TotalSize = 0;
                var UploadedSize = 0;
                var UploadedCount = 0;
                $('#totalAssetActivityAttachProgresstask #taskuploaded').html('');
                $('#assetassettaskentityattachassetdata .attachmentBox').each(function () {
                    TotalSize += parseInt($(this).attr('data-size'));
                    UploadedSize += parseInt($(this).attr('data-size-uploaded'));
                    if ($(this).attr('data-status') == 'true') {
                        UploadedCount += 1;
                    }
                });
                $('#totalAssetActivityAttachProgresstask .count').html(UploadedCount + ' of ' + $('#assetassettaskentityattachassetdata .attachmentBox').length + ' Uploaded');
                $('#totalAssetActivityAttachProgresstask .size').html(plupload.formatSize(UploadedSize) + ' / ' + plupload.formatSize(TotalSize));
                $('#totalAssetActivityAttachProgresstask .bar').css("width", Math.round(((UploadedSize / TotalSize) * 100)) + "%");
                if ((UploadedCount == $('#assetassettaskentityattachassetdata .attachmentBox').length) && UploadedCount > 0 && $('#assetassettaskentityattachassetdata .attachmentBox').length > 0) {
                    timerObj.assetactivity = $timeout(function () {
                        $('#totalAssetActivityAttachProgresstask #taskuploaded').html('Upload Successful.');
                    }, 100);
                }
                $scope.IsaddAssetsfromComputer = true;
                $scope.addfromComputer = true;
                if ($("#assetassettaskentityattachassetdata > div").length == 0) {
                    $scope.addfromComputer = false;
                    $scope.IsaddAssetsfromComputer = false;
                    $('#totalAssetActivityAttachProgresstask .bar').css("width", Math.round((0 * 100)) + "%");
                }
            }
        }

        function GetEntityAttributesDetails(taskID) {
            TaskCreationService.GetEntityAttributesDetails(taskID).then(function (TaskDetailList) {
                LoadTaskMetadata(TaskDetailList.Response, $scope.TaskBriefDetails.taskID, $scope.TaskBriefDetails.taskTypeId, $scope.fromAssignedpopup);
            });
        }
        $scope.AssetTypeTreePopUp = function (fileid) {
            $scope.AssetCategory["filterval_" + fileid] = '';
            var htmltree = '<category-tree tree-filter=\"AssetCategory.filterval_' + fileid + '\" tree-data=\"AssetCategory.AssetCategoryTree_' + fileid + '\" accessable="false" tree-control="my_tree" on-select="AssetTypeCategorySelection(branch,parent)" expand-level=\"100\"></category-tree>';
            $("#dynamicAssetTypeTreeInTask").html($compile(htmltree)($scope));
            $scope.FileIdForAssetCategory = fileid;
            $("#AssetCategoryTreeInTask").modal('show');
        };
        $scope.AssetTypeCategorySelection = function (branch, parentArr) {
            var i = 0;
            if (branch.ischecked == true) {
                $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory] = "";
                for (i = parentArr.length - 1; i >= 0; i--) {
                    $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory] += parentArr[i].Caption + "/";
                }
                if ($scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory].length > 0) $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory] = $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory].substring(0, $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory].length - 1);
                var tempval = [];
                var tempasst = "";
                for (var k = 0; k < parentArr[0].Children.length; k++) {
                    tempasst = $.grep($scope.DAMtypeswithExt.m_Item1, function (e) {
                        return e.Id == parentArr[0].Children[k].id
                    });
                    if (tempasst != undefined && tempasst.length > 0) tempval.push(tempasst[0]);
                }
                if (tempval.length > 0) {
                    $scope.AssetCategory["AssetTypesList_" + $scope.FileIdForAssetCategory] = tempval;
                    $scope.assetfields["tagAllOptions_" + $scope.FileIdForAssetCategory].data.splice(0, $scope.assetfields["tagAllOptions_" + $scope.FileIdForAssetCategory].data.length);
                    $.each($scope.AssetCategory["AssetTypesList_" + $scope.FileIdForAssetCategory], function (i, el) {
                        $scope.assetfields["tagAllOptions_" + $scope.FileIdForAssetCategory].data.push({
                            "id": el.Id,
                            "text": el.damCaption,
                            "ShortDescription": el.ShortDescription,
                            "ColorCode": el.ColorCode
                        });
                    });
                    $scope.assetfields["assettype_" + $scope.FileIdForAssetCategory] = $.grep(tempval, function (e) {
                        return e.id == branch.id
                    })[0];
                }
                $("#AssetCategoryTreeInTask").modal('hide');
                $scope.AssetCategory['showcategroytext_' + $scope.FileIdForAssetCategory] = true;
                $scope.onDamtypechange($scope.FileIdForAssetCategory);
            } else {
                $('#div_' + $scope.FileIdForAssetCategory).html($compile("")($scope));
                $scope.AssetCategory["treestructure_" + $scope.FileIdForAssetCategory] = "-";
                $scope.AssetCategory['showcategroytext_' + $scope.FileIdForAssetCategory] = false;
                $scope.assetfields["assettype_" + $scope.FileIdForAssetCategory] = '';
                $scope.AssetCategory["AssetTypesList_" + $scope.FileIdForAssetCategory].splice(0, $scope.AssetCategory["AssetTypesList_" + $scope.FileIdForAssetCategory].length);
            }
        }

        function SaveFileDetails(file, response, fileDescription) {
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");
            $scope.FileList.push({
                "Name": file.name,
                "VersionNo": 1,
                "MimeType": resultArr[1],
                "Extension": extension,
                "OwnerID": $cookies['UserId'],
                "CreatedOn": Date.now(),
                "Checksum": "",
                "ModuleID": 1,
                "EntityID": $stateParams.ID,
                "Size": file.size,
                "FileGuid": resultArr[0],
                "FileDescription": fileDescription,
                "IsExists": false
            });
            $scope.AttachmentFilename.push({
                "FileName": file.name,
                "Extension": extension,
                "Size": parseSize(file.size),
                "FileGuid": resultArr[0],
                "FileDescription": fileDescription,
                "URL": "",
                "LinkType": "",
                "ID": 0,
                "LinkID": 0
            });
            if ($scope.IsNotVersioning == true) {
                $scope.InsertableFileList.push({
                    "Name": file.name,
                    "VersionNo": 1,
                    "MimeType": resultArr[1],
                    "Extension": extension,
                    "OwnerID": $cookies['UserId'],
                    "CreatedOn": Date.now(),
                    "Checksum": "",
                    "ModuleID": 1,
                    "EntityID": $stateParams.ID,
                    "Size": file.size,
                    "FileGuid": resultArr[0],
                    "FileDescription": fileDescription
                });
                $scope.InsertableAttachmentFilename.push({
                    "FileName": file.name,
                    "Extension": extension,
                    "Size": parseSize(file.size),
                    "FileGuid": resultArr[0],
                    "FileDescription": fileDescription,
                    "ID": 0
                });
                if ($scope.SelectedTaskID > 0) {
                    var SaveTask = {};
                    SaveTask.TaskID = $scope.SelectedTaskID;
                    SaveTask.TaskAttachments = $scope.InsertableAttachmentFilename;
                    SaveTask.TaskFiles = $scope.InsertableFileList;
                    TaskCreationService.InsertEntityTaskAttachments(SaveTask).then(function (SaveTaskResult) {
                        if (SaveTaskResult.StatusCode == 405) {
                            NotifyError($translate.instant('LanguageContents.Res_4275.Caption'));
                            $scope.globalAttachment = true;
                        } else {
                            NotifySuccess($translate.instant('LanguageContents.Res_4388.Caption'));
                            $scope.InsertableFileList = [];
                            $scope.InsertableAttachmentFilename = [];
                            ReloadTaskAttachments($scope.SelectedTaskID);
                            RefreshCurrentTask();
                        }
                        closecreatetaskpopup();
                        $("#taskcreating").hide();
                        NotifySuccess($translate.instant('LanguageContents.Res_4820.Caption'));
                    });
                }
            } else {
                $scope.InsertableFileListforVersion.push({
                    "Name": file.name,
                    "VersionNo": 1,
                    "MimeType": resultArr[1],
                    "Extension": extension,
                    "OwnerID": $cookies['UserId'],
                    "CreatedOn": Date.now(),
                    "Checksum": "",
                    "ModuleID": 1,
                    "EntityID": $stateParams.ID,
                    "Size": file.size,
                    "FileGuid": resultArr[0],
                    "FileDescription": fileDescription
                });
                $scope.InsertableAttachmentFilenameforVersion.push({
                    "FileName": file.name,
                    "Extension": extension,
                    "Size": parseSize(file.size),
                    "FileGuid": resultArr[0],
                    "FileDescription": fileDescription,
                    "ID": 0
                });
                if ($scope.SelectedTaskID > 0) {
                    var objfile = $.grep($scope.TaskBriefDetails.taskAttachmentsList, function (n, i) {
                        return $scope.TaskBriefDetails.taskAttachmentsList[i].Id == $scope.FileID;
                    });
                    if (objfile != null && objfile.length > 0) {
                        var SaveTask = {};
                        SaveTask.TaskID = $scope.SelectedTaskID;
                        SaveTask.TaskAttachments = $scope.InsertableAttachmentFilenameforVersion;
                        SaveTask.TaskFiles = $scope.InsertableFileListforVersion;
                        SaveTask.VersioningFileId = objfile[0].VersioningFileId;
                        SaveTask.FileId = $scope.FileID;
                        TaskCreationService.InsertEntityTaskAttachmentsVersion(SaveTask).then(function (SaveTaskResult) {
                            if (SaveTaskResult.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4275.Caption'));
                                $scope.globalAttachment = true;
                            } else {
                                ReloadTaskAttachments($scope.SelectedTaskID);
                                ReloadTaskVersionDetails();
                                $scope.contextFileID = SaveTaskResult.Response;
                                $scope.FileID = SaveTaskResult.Response;
                                NotifySuccess($translate.instant('LanguageContents.Res_4388.Caption'));
                                $scope.InsertableAttachmentFilenameforVersion = [];
                                $scope.InsertableFileList = [];
                                $scope.InsertableFileListforVersion = [];
                                $scope.InsertableAttachmentFilename = [];
                                RefreshCurrentTask();
                            }
                        });
                    }
                }
            }
        }

        function GetUserSelectedAll() {
            var IDList = [];
            $('.AdditionalTaskMembers  > tbody input:checked').each(function () {
                IDList.push({
                    "Userid": ($(this).attr('data-userid')),
                    "Id": ($(this).attr('data-id')),
                    "TaskID": ($(this).attr('data-taskid')),
                    "Roleid": 4,
                    "UserName": ($(this).attr('data-username')),
                    "UserEmail": ($(this).attr('data-UserEmail')),
                    "DepartmentName": ($(this).attr('data-departmentname')),
                    "Role": ($(this).attr('data-role')),
                    "Title": ($(this).attr('data-title')),
                    StepID: (($scope.stepid) == null ? 0 : ($scope.stepid)),
                    "FirstName": $(this).attr('data-firstname'),
                    "LastName": $(this).attr('data-lastname'),
                    "ApprovalRoleID": ''
                });
            });
            return IDList;
        }

        function GetGlobalUserSelectedAll() {
            var GlobalUserList = [];
            $('.AdditionalTaskGlobalMembers  > tbody input:checked').each(function () {
                GlobalUserList.push({
                    "Userid": ($(this).attr('data-userid')),
                    "Id": 0,
                    "TaskID": 0,
                    "Roleid": 4,
                    "UserName": ($(this).attr('data-username')),
                    "Title": '',
                    "UserEmail": ($(this).attr('data-UserEmail')),
                    "DepartmentName": ($(this).attr('data-departmentname')),
                    "Role": ($(this).attr('data-role')),
                    StepID: (($scope.stepid) == null ? 0 : ($scope.stepid)),
                    "FirstName": $(this).attr('data-firstname'),
                    "LastName": $(this).attr('data-lastname'),
                    "ApprovalRoleID": ''
                });
            });
            return GlobalUserList;
        }
        $scope.addAdditionalMemberforTask = function (stepid, phaseid) {
            if (stepid != null && phaseid != null && stepid != 0 && phaseid != 0) {
                $scope.templateData.PhaseData.StepsData.Steproles = (this.step.Roles) == null ? 0 : ((this.step.Roles));
                var currentStepRoleIDs = (this.step.Roles) == null ? 0 : ((this.step.Roles));
            }
            if (TempStepID == stepid) {
                TempStepID = stepid;
            } else {
                TempStepID = stepid;
                $scope.TaskMemberLists = [];
            }
            $scope.ApprovalTaskGlobalUsers.EntityUser = [];
            $scope.ApprovalTaskGlobalUsers.globalUser = [];
            if (this.step != null) {
                getEntityApprovalRoleUsers(this.step.Roles);
            }
            $scope.AddUnAssign = false;
            $scope.globalTempcount = 1;
            $scope.AutoCompleteSelectedUserObj.userrole = '';
            $scope.taskGlobaluserforTaskcreation = '';
            $scope.AutoCompleteSelectedUserObj = {
                UserSelection: {}
            };
            $("#AddTaskMemberModalPopup").modal("show");
            if (stepid == 0) {
                $scope.addTaskMemberfromGlobal = 1;
                $scope.addTaskMemberfromStep = false;
                SeperateUsers("AddGlobalMember");
            } else {
                SeperateUsers("FromtStep");
                $scope.addTaskMemberfromGlobal = 1;
                $scope.addTaskMemberfromStep = true;
            }
            $scope.globalEntityMemberLists = [];
            $scope.stepid = stepid == null ? 0 : stepid;
            $scope.PhaseId = phaseid == null ? 0 : phaseid;
            $("#AddTaskMemberModalPopup").modal("show");
        }
        $scope.AddTaskAdditionalMembers = function () {
            if ($('#ngAddTaskMember').hasClass('disabled')) {
                return;
            }
            $('#ngAddTaskMember').addClass('disabled');
            var memberList = [];
            var GlobalMembers = [];
            memberList = GetUserSelectedAll();
            if ($scope.stepid != 0 && $scope.stepid != null) {
                GlobalMembers = GetGlobalUserSelectedAll();
                $scope.globalEntityMemberLists = GlobalMembers;
            }
            if (memberList.length > 0) {
                for (var i = 0, mem; mem = memberList[i++];) {
                    var isPresence = $.grep($scope.TaskMemberLists, function (rel) {
                        return rel.Userid == mem.Userid && rel.StepID == mem.StepID;
                    })[0];
                    if (isPresence == null || isPresence.length == 0) {
                        $scope.TaskMemberLists.push(mem);
                    }
                }
            }
            $('#AdditionalTaskMembers > tbody input:checked').each(function () {
                $(this).prop('checked', false);
                $(this).next('i').removeClass('checked');
            });
            if ($scope.globalEntityMemberLists.length > 0) {
                for (var i = 0, mem; mem = $scope.globalEntityMemberLists[i++];) {
                    var isPresence = $.grep($scope.TaskMemberLists, function (rel) {
                        return rel.Userid == mem.Userid && rel.stepid == mem.stepid;
                    })[0];
                    if (isPresence == null || isPresence.length == 0) {
                        $scope.TaskMemberLists.push(mem);
                    }
                }
            }
            if ($scope.stepid > 0) {
                var phasedetails = $.grep($scope.templateData.PhaseData.AllPhaseList, function (rel) {
                    return rel.ID == $scope.PhaseId;
                })[0];
                var stepdetails = $.grep(phasedetails.StepsData, function (rel) {
                    return rel.ID == $scope.stepid;
                })[0];
                for (var m = 0, mem = {}; mem = $scope.TaskMemberLists[m++];) {
                    if (stepdetails != null) {
                        if (stepdetails.MemberList != null) {
                            var isAlready = $.grep(stepdetails.MemberList, function (rel) {
                                return rel.Userid == mem.Userid;
                            });
                            if (isAlready.length == 0) {
                                stepdetails.MemberList.push(mem);
                            }
                        } else {
                            stepdetails.MemberList = [];
                            stepdetails.MemberList.push(mem);
                        }
                    }
                }
                $scope.TaskMemberLists = [];
            }
            $('#ngAddTaskMember').removeClass('disabled');
            $('#AddTaskMemberModalPopup').modal('hide');
        }

        function SeperateUsers(fromplace) {
            var UniqueTaskMembers = [];
            var dupes = {};
            if (fromplace != "AddGlobalMember") {
                if ($scope.TaskMemberList != null && $scope.TaskMemberList.count > 0) $scope.TaskMemberList = SelectedroleUserforDisplay($scope.TaskMemberList);
                if ($scope.EntityMemberList != null) $scope.EntityMemberList = SelectedroleUserforDisplay($scope.EntityMemberList);
            }
            if ($scope.TaskMemberList != null) {
                for (var k = 0, el; el = $scope.TaskMemberList[k++];) {
                    if (!dupes[el.UserID]) {
                        dupes[el.UserID] = true;
                        UniqueTaskMembers.push(el);
                    }
                }
            }
            var UniqueEntityMembers = [];
            dupes = {};
            if ($scope.EntityMemberList != null) {
                for (var i = 0, el; el = $scope.EntityMemberList[i++];) {
                    if (el.IsInherited != true) if (!dupes[el.Userid]) {
                        dupes[el.Userid] = true;
                        UniqueEntityMembers.push(el);
                    }
                }
            }
            $scope.RemainMembers = [];
            if (UniqueEntityMembers != null) {
                for (var j = 0, value; value = UniqueEntityMembers[j++];) {
                    var MemberList = $.grep(UniqueTaskMembers, function (e) {
                        return e.Userid == value.Userid
                    });
                    if (MemberList.length == 0) {
                        $scope.RemainMembers.push(value);
                    }
                    var MemberList = $.grep($scope.ApprovalTaskGlobalUsers.EntityUser, function (e) {
                        return e.Userid == value.Userid
                    });
                    if (MemberList.length == 0) {
                        $scope.ApprovalTaskGlobalUsers.EntityUser.push({
                            Userid: value.Userid,
                            UserEmail: value.UserEmail,
                            UserName: value.UserName,
                            FirstName: value.FirstName,
                            LastName: value.LastName,
                            ApprovalRoleName: value.Role,
                            ApprovalId: 0,
                            EntityRole: value.Role,
                            ApprovalRoleforEntityUser: '',
                            IsGlobal: false,
                            Approvalroles: [],
                            ischecked: false
                        });
                    }
                }
            }
        }

        function SelectedroleUserforDisplay(selectedmembers) {
            var tempmember = [];
            if ($scope.templateData.PhaseData.StepsData.Steproles.length > 0) {
                for (var i = 0; i < $scope.templateData.PhaseData.StepsData.Steproles.length; i++) {
                    var member = $.grep(selectedmembers, function (e) {
                        return e.BackendRoleID == $scope.templateData.PhaseData.StepsData.Steproles[i];
                    });
                    for (var j = 0; j < member.length; j++) {
                        tempmember.push(member[j]);
                    }
                }
                return tempmember
            }
        }
        $scope.addUsers = function () {
            var result = $.grep($scope.globalEntityMemberLists, function (e) {
                return e.Userid == parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10);
            });
            if ($scope.TaskMemberList != null) var entitymemberresult = $.grep($scope.ApprovalTaskGlobalUsers.EntityUser, function (e) {
                return e.Userid == parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10) && (e.IsInherited == false);
            });
            else var entitymemberresult = $.grep($scope.EntityMemberList, function (e) {
                return e.Userid == parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10) && (e.IsInherited == false);
            });
            if (result.length == 0 && entitymemberresult.length == 0) {
                var membervalues = $.grep($scope.Roles, function (e) {
                    return e.ID == $scope.AutoCompleteSelectedUserObj.userrole;
                })[0];
                if (membervalues != undefined) {
                    $scope.globalEntityMemberLists.push({
                        "TID": $scope.globalTempcount,
                        "UserEmail": $scope.AutoCompleteSelectedUserObj.UserSelection.Email,
                        "DepartmentName": $scope.AutoCompleteSelectedUserObj.UserSelection.Designation,
                        "Title": $scope.AutoCompleteSelectedUserObj.UserSelection.Title,
                        "Roleid": 4,
                        "StepRoleid": 0,
                        "RoleName": membervalues.Caption,
                        "Userid": parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10),
                        "UserName": $scope.AutoCompleteSelectedUserObj.UserSelection.FirstName + ' ' + $scope.AutoCompleteSelectedUserObj.UserSelection.LastName,
                        "IsInherited": '0',
                        "InheritedFromEntityid": '0',
                        "FromGlobal": 0,
                        "CostCentreID": 0
                    });
                    $scope.globalTempcount = $scope.globalTempcount + 1;
                    $scope.AutoCompleteSelectedUserObj.userrole = '';
                    $scope.taskGlobaluserforTaskcreation = '';
                    $("#taskGlobaluserforTaskcreation").val("");
                    $scope.AutoCompleteSelectedObj = [];
                    var isExist = [];
                    isExist = $.grep($scope.ApprovalTaskGlobalUsers.EntityUser, function (rel) {
                        return rel.Userid == $scope.AutoCompleteSelectedUserObj.UserSelection.Id;
                    });
                    if (isExist == null || isExist.length == 0) {
                        $scope.ApprovalTaskGlobalUsers.globalUser.push({
                            UserEmail: $scope.AutoCompleteSelectedUserObj.UserSelection.Email,
                            Userid: parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10),
                            UserName: $scope.AutoCompleteSelectedUserObj.UserSelection.FirstName + " " + $scope.AutoCompleteSelectedUserObj.UserSelection.LastName,
                            FirstName: $scope.AutoCompleteSelectedUserObj.UserSelection.FirstName,
                            LastName: $scope.AutoCompleteSelectedUserObj.UserSelection.LastName,
                            ApprovalRoleName: '',
                            ApprovalId: 0,
                            EntityRoleID: membervalues.ID,
                            ApprovalRoleforGlobalUser: '',
                            EntityRoleforGlobalUser: membervalues.Caption,
                            IsGlobal: true,
                            Approvalroles: [],
                            ischecked: true
                        });
                    }
                    else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
                    }
                }
            } else {
                $scope.AutoCompleteSelectedUserObj.userrole = '';
                $scope.taskGlobaluserforTaskcreation = '';
                $("#taskGlobaluserforTaskcreation").val("");
                $scope.AutoCompleteSelectedObj = [];
                bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
            }
        };
        $scope.deleteGlobalTaskMembers = function (item) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2025.Caption'), function (result) {
                if (result) {
                    timerObj.memberlist = $timeout(function () {
                        $scope.globalEntityMemberLists.splice($.inArray(item, $scope.globalEntityMemberLists), 1);
                    }, 100);
                }
            });
        };
        $scope.clearUserFilter = function () {
            userFilter.value = "";
        };

        function ClearAllScopeVariables() {
            $scope.approvalflowTempPopup = false;
            $scope.rytSideApprovalButtonPane = false;
            $scope.TaskTypeObject.NewTaskName = "";
            $scope.TaskTypeObject.TaskDescription = "";
            $scope.TaskTypeObject.TaskNote = "";
            $scope.TaskTypeObject.TaskDueDate = "";
            $scope.TaskMembersList = "";
            $scope.TaskMemberLists = [];
            AddedStepDetails = [];
            AddedPhaseDetails = [];
        }

        function getEntityApprovalRoleUsers(steproles) {
            var approvalRoles = [];
            if (steproles != null) {
                if (typeof steproles === 'string') {
                    steproles = [steproles];
                }
                if (!isNaN(steproles)) {
                    steproles = [steproles];
                }
            }
            if (steproles != null && steproles.length > 0) {
                if (steproles == 0) {
                    approvalRoles = [];
                } else {
                    approvalRoles = steproles;
                }
            } else {
                if (steproles == 0) {
                    approvalRoles = [];
                } else {
                    approvalRoles = steproles;
                }
            }
            $scope.addTaskMemberfromGlobal = false;
            $scope.addTaskMemberfromStep = true;
            $scope.ApprovalTaskGlobalUsers.EntityUser = [];
            var jobj = {
                "approvalRoles": approvalRoles,
                EntityId: $stateParams.ID
            };
            TaskCreationService.getTaskApprovalRolesUsers(jobj).then(function (EntityMemberList) {
                $scope.ApprovalTaskGlobalUsers.EntityUser = [];
                if (EntityMemberList.Response != null) {
                    for (var i = 0, mem; mem = EntityMemberList.Response[i++];) {
                        var GroupResult = [],
                            memberroles = [],
                            roles = [],
                            isExist = [];
                        isExist = $.grep($scope.ApprovalTaskGlobalUsers.EntityUser, function (rel) {
                            return rel.Userid == mem.ID;
                        });
                        if (isExist == null || isExist.length == 0) {
                            GroupResult = $.grep(EntityMemberList.Response, function (rel) {
                                return rel.ID == mem.ID;
                            });
                            if (GroupResult.length > 0) {
                                roles = [];
                                for (var j = 0, role; role = GroupResult[j++];) {
                                    roles.push({
                                        "Name": role.ApprovalRoles,
                                        "ID": role.ApprovalId
                                    })
                                }
                            }
                            var MemberList = $.grep($scope.ApprovalTaskGlobalUsers.EntityUser, function (e) {
                                return e.Userid == mem.ID
                            });
                            if (MemberList.length == 0) {
                                $scope.ApprovalTaskGlobalUsers.EntityUser.push({
                                    UserEmail: mem.Email,
                                    Userid: mem.ID,
                                    UserName: mem.FirstName + " " + mem.LastName,
                                    FirstName: mem.FirstName,
                                    LastName: mem.LastName,
                                    ApprovalRoleName: "",
                                    ApprovalId: 0,
                                    IsGlobal: false,
                                    EntityRole: mem.Roles,
                                    ApprovalRoleforEntityUser: '',
                                    Approvalroles: roles,
                                    ischecked: false
                                });
                            }
                        }
                    }
                }
            });
        }

        function getTaskApprovalRolesUsers(steproles) {
            var approvalRoles = [];
            if (steproles != null && steproles.length > 0) {
                if (steproles == 0) {
                    approvalRoles = [];
                } else {
                    approvalRoles = steproles;
                }
            } else {
                if (steproles == 0) {
                    approvalRoles = [];
                } else {
                    approvalRoles = steproles;
                }
            }
            $scope.ApprovalTaskGlobalUsers.globalUser = [];
            var jobj = {
                "approvalRoles": approvalRoles,
                EntityId: $stateParams.ID
            };
            TaskCreationService.getTaskApprovalRolesUsers(jobj).then(function (EntityMemberList) {
                $scope.ApprovalTaskGlobalUsers.globalUser = [];
                if (EntityMemberList.Response != null) {
                    for (var i = 0, mem; mem = EntityMemberList.Response[i++];) {
                        var GroupResult = [],
                            memberroles = [],
                            roles = [],
                            isExist = [];
                        isExist = $.grep($scope.ApprovalTaskGlobalUsers.globalUser, function (rel) {
                            return rel.Userid == mem.ID;
                        });
                        if (isExist == null || isExist.length == 0) {
                            GroupResult = $.grep(EntityMemberList.Response, function (rel) {
                                return rel.ID == mem.ID;
                            });
                            if (GroupResult.length > 0) {
                                roles = [];
                                for (var j = 0, role; role = GroupResult[j++];) {
                                    roles.push({
                                        "Name": role.ApprovalRoles,
                                        "ID": role.ApprovalId
                                    })
                                }
                            }
                            var MemberList = $.grep($scope.ApprovalTaskGlobalUsers.globalUser, function (e) {
                                return e.Userid == mem.ID
                            });
                            if (MemberList.length == 0) {
                                $scope.ApprovalTaskGlobalUsers.globalUser.push({
                                    UserEmail: mem.Email,
                                    Userid: mem.ID,
                                    UserName: mem.FirstName + " " + mem.LastName,
                                    FirstName: mem.FirstName,
                                    LastName: mem.LastName,
                                    ApprovalId: 0,
                                    ApprovalRoleName: "",
                                    ApprovalRoleforGlobalUser: '',
                                    EntityRoleforGlobalUser: '',
                                    Approvalroles: roles,
                                    IsGlobal: true,
                                    EntityRole: '',
                                    EntityRoleID: 0,
                                    ischecked: false
                                });
                            }
                        }
                    }
                }
            });
        }
        $scope.ShowOrHideAttributeToAttributeRelation = function (attrID, attrTypeID) {
            return true;
        }
        $scope.HandlingMembers = function () {
            var memberList = [];
            var GlobalMembers = [];
            if ($('#ngAddTaskMember').hasClass('disabled')) {
                return;
            }
            $('#ngAddTaskMember').addClass('disabled');
            if ($scope.ApprovalTaskGlobalUsers.EntityUser != null) memberList = $.grep($scope.ApprovalTaskGlobalUsers.EntityUser, function (rel) {
                return rel.ischecked == true;
            });
            if (memberList != null) {
                if (memberList.length > 0) {
                    for (var i = 0, mem; mem = memberList[i++];) {
                        var isPresence = $.grep($scope.TaskMemberLists, function (rel) {
                            return rel.Userid == mem.Userid;
                        })[0];
                        if (isPresence == null || isPresence.length == 0) {
                            mem.StepRoleID = (mem.ApprovalRoleforEntityUser == null || mem.ApprovalRoleforEntityUser == '') ? 0 : mem.ApprovalRoleforEntityUser;
                            mem.ApprovalRoleID = (mem.ApprovalRoleforEntityUser == null || mem.ApprovalRoleforEntityUser == '') ? 0 : mem.ApprovalRoleforEntityUser;
                            var ApprovalRoleName = $.grep($scope.templateData.UserRoles, function (e) {
                                return e.Id == mem.ApprovalRoleID;
                            })[0];
                            if (ApprovalRoleName != null) mem.ApprovalRoleName = (ApprovalRoleName.Caption == null || ApprovalRoleName.Caption == '') ? '-' : ApprovalRoleName.Caption;
                            else mem.ApprovalRoleName = '-';
                            $scope.TaskMemberLists.push(mem);
                        }
                    }
                }
            }
            if ($scope.ApprovalTaskGlobalUsers.globalUser != null) GlobalMembers = $.grep($scope.ApprovalTaskGlobalUsers.globalUser, function (rel) {
                return rel.ischecked == true;
            });
            if (GlobalMembers.length > 0) {
                for (var i = 0, mem; mem = GlobalMembers[i++];) {
                    var isPresence = $.grep($scope.TaskMemberLists, function (rel) {
                        return rel.Userid == mem.Userid;
                    })[0];
                    if (isPresence == null || isPresence.length == 0) {
                        mem.StepRoleID = (mem.ApprovalRoleforGlobalUser == null || mem.ApprovalRoleforGlobalUser == '') ? 0 : mem.ApprovalRoleforGlobalUser;
                        mem.ApprovalRoleID = (mem.ApprovalRoleforGlobalUser == null || mem.ApprovalRoleforGlobalUser == '') ? 0 : mem.ApprovalRoleforGlobalUser;
                        if (mem.ApprovalRoleID > 0) {
                            var ApprovalRoleName = $.grep($scope.templateData.UserRoles, function (e) {
                                return e.Id == mem.ApprovalRoleforGlobalUser;
                            })[0];
                            if (ApprovalRoleName != null) mem.ApprovalRoleName = ApprovalRoleName.Caption;
                            else mem.ApprovalRoleName = '-';
                        } else {
                            mem.ApprovalRoleName = '-';
                        }
                        mem.EntityRole = (mem.EntityRoleforGlobalUser == null || mem.EntityRoleforGlobalUser == '') ? 0 : mem.EntityRoleforGlobalUser;
                        $scope.TaskMemberLists.push(mem);
                    }
                }
            }
            if ($scope.stepid > 0) {
                var phasedetails = $.grep($scope.templateData.PhaseData.AllPhaseList, function (rel) {
                    return rel.ID == $scope.PhaseId;
                })[0];
                var stepdetails = $.grep(phasedetails.StepsData, function (rel) {
                    return rel.ID == $scope.stepid;
                })[0];
                for (var m = 0, mem = {}; mem = $scope.TaskMemberLists[m++];) {
                    if (stepdetails != null) {
                        if (stepdetails.MemberList != null) {
                            var isAlready = $.grep(stepdetails.MemberList, function (rel) {
                                return rel.Userid == mem.Userid;
                            });
                            if (isAlready.length == 0) {
                                stepdetails.MemberList.push(mem);
                            }
                        } else {
                            stepdetails.MemberList = [];
                            stepdetails.MemberList.push(mem);
                        }
                    }
                }
                $scope.TaskMemberLists = [];
            }
            for (var i = 0, mem; mem = $scope.ApprovalTaskGlobalUsers.globalUser[i++];) {
                mem.ischecked = false;
            }
            for (var i = 0, mem; mem = $scope.ApprovalTaskGlobalUsers.EntityUser[i++];) {
                mem.ischecked = false;
            }
            $('#ngAddTaskMember').removeClass('disabled');
            $('#AddTaskMemberModalPopup').modal('hide');
        }

        function GetentityMembers(entityID) {
            $scope.EntityMemberList = [];
            TaskCreationService.GetDirectEntityMember(entityID).then(function (EntityMemberList) {
                $scope.EntityMemberList = EntityMemberList.Response;
                $scope.EntityDistinctMembers = [];
                var dupes = {};
                if ($scope.EntityMemberList != null) {
                    for (var i = 0, el; el = $scope.EntityMemberList[i++];) {
                        if (el.IsInherited != true) {
                          if (!dupes[el.Userid]) {
                              dupes[el.Userid] = true;
                                $scope.EntityDistinctMembers.push(el);
                           }
                        }
                    }
                }
            });
        }

        function RefreshCurrentTask() {
            $scope.TaskDetails = [];
            TaskCreationService.GetEntityTaskDetails($scope.TaskBriefDetails.taskID).then(function (EntityTaskDetails) {
                $scope.TaskDetails = EntityTaskDetails.Response;
                RefreshUnassignedTaskInObject($scope.TaskDetails);
                if ($scope.TaskDetails != null) if ($scope.TaskDetails.TaskStatus != 0) BindFundRequestTaskDetails($scope.TaskBriefDetails.taskTypeId, $scope.TaskDetails);
                else {
                    LoadUnassignedTaskDetl($scope.TaskBriefDetails.taskTypeId, $scope.TaskDetails);
                }
            });
        }

        function ReloadTaskAttachments(taskID) {
            TaskCreationService.GetEntityTaskAttachmentFile(taskID).then(function (TaskDetailList) {
                $scope.attachedFiles = TaskDetailList.Response;
                $scope.TaskBriefDetails.taskAttachmentsList = TaskDetailList.Response;
                if ($scope.TaskActionFor == 1) {
                    var taskListResObj = $.grep($scope.groups, function (e) {
                        return e.ID == $scope.TaskBriefDetails.taskListUniqueID;
                    });
                    var taskResObj = $.grep(taskListResObj[0].TaskList, function (e) {
                        return e.Id == $scope.TaskBriefDetails.taskID;
                    });
                    if (taskResObj.length > 0) {
                        taskResObj[0].taskAttachment = TaskDetailList.Response;
                    }
                } else {
                    var taskListResObj = $.grep($scope.groupbySubLevelTaskObj, function (e) {
                        return e.EntityID == $scope.TaskBriefDetails.EntityID;
                    });
                    var taskResObj = $.grep(taskListResObj[0].TaskListGroup, function (e) {
                        return e.ID == $scope.TaskBriefDetails.taskListUniqueID;
                    });
                    if (taskResObj.length > 0) {
                        var taskObj = $.grep(taskResObj[0].TaskList, function (e) {
                            return e.Id == $scope.TaskBriefDetails.taskID;
                        });
                        if (taskObj.length > 0) taskObj[0].taskAttachment = TaskDetailList.Response;
                    }
                }
            });
        }

        function GetAllUserRoles() {
            TaskCreationService.GetApprovalRoles().then(function (Result) {
                $scope.templateData.UserRoles = [];
                if (Result.Response.length != null) {
                    for (var i = 0, role; role = Result.Response[i++];) {
                        $scope.templateData.UserRoles.push({
                            "Id": role.ID,
                            "Caption": role.Caption,
                            "Description": role.Description
                        });
                    }
        }
                    });           
        }
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType, AssetFileID) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToShow = [];
                var hideAttributeOtherThanSelected = [];

                var fileId_AttributeID = 0;
                if (AssetFileID != null && AssetFileID != undefined)
                    fileId_AttributeID = AssetFileID + attrID;
                else
                    fileId_AttributeID = attrID;
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.DropDownTreeOptionValues["Options" + fileId_AttributeID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + fileId_AttributeID + "_" + j].length);
                        if (attrType == 6) {
                            //if ($scope.fields["DropDown_" + attrID + "_" + (j - 1)] == undefined)
                            //    $scope.fields["DropDown_" + attrID + "_" + (j - 1)] = "";
                            $scope.fields["DropDown_" + fileId_AttributeID + "_" + j] = "";
                        } else if (attrType == 12) {
                            if (j == levelcnt) $scope.fields["MultiSelectDropDown_" + fileId_AttributeID + "_" + j] = [];
                            else $scope.fields["MultiSelectDropDown_" + fileId_AttributeID + "_" + j] = "";
                        }
                    }
                    if (attrType == 6) {
                        var Childrens = $.grep($scope.DropDownTreeOptionValues["Options" + fileId_AttributeID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope.fields["DropDown_" + fileId_AttributeID + "_" + attributeLevel]) })[0].Children;
                        if (Childrens != undefined) {
                            var subleveloptions = [];
                            $.each(Childrens, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + fileId_AttributeID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        //if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        //	$.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function(i, el) {
                        //		$scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        //	});
                        //}
                    } else if (attrType == 12) {
                        var Children = $.grep($scope.DropDownTreeOptionValues["Options" + fileId_AttributeID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope.fields["MultiSelectDropDown_" + fileId_AttributeID + "_" + attributeLevel]) })[0].Children;
                        if (Children != undefined) {
                            var subleveloptions = [];
                            $.each(Children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + fileId_AttributeID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        //if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        //	$.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function(i, el) {
                        //		$scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        //	});
                        //}
                    }
                }
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    } catch (e) { }
                }
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);
                if (attrType == 3) {
                    if ($scope.fields['ListSingleSelection_' + attrID] != null) {
                        optionValue = parseInt(($scope.fields['ListSingleSelection_' + attrID]), 10);
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                } else if (attrType == 4) {
                    if ($scope.fields['ListMultiSelection_' + attrID] != null) {
                        optionValue = $scope.fields['ListMultiSelection_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                } else if (attrType == 7) {
                    if ($scope.fields['Tree_' + attrID] != null) {
                        optionValue = $scope.fields['Tree_' + attrID];
                    } else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                } else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                } else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            } catch (e) { }
        }
        $scope.addfromattachment = function () {
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/DAM/damtaskentityselection.html',
                controller: "mui.DAM.taskentitydamselectionCtrl",
                resolve: {
                    params: function () {
                        return 0;
                    }
                },
                scope: $scope,
                windowClass: 'drpD-Link popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }
        timerObj.openpopup = $timeout(function () { callTaskCreationPage(params.data) }, 100);

        function closecreatetaskpopup() {
            $modalInstance.dismiss('cancel');
        }
        $scope.$on("$destroy", function () {
            $timeout.cancel(timerObj);
        });
    }
    app.controller("mui.task.muitaskTaskCreationCtrl", ['$window', '$location', '$timeout', '$scope', '$cookies', '$resource', '$compile', '$stateParams', '$translate', 'TaskCreationService', '$modalInstance', 'params', '$modal', muitaskTaskCreationCtrl]);
})(angular, app);