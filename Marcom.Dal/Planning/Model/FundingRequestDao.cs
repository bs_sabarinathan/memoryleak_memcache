using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// FundingRequestDao object for table 'PM_FundingRequest'.
	/// </summary>
	
	public partial class FundingRequestDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected int _requestedby;
		protected int _costcenterid;
		protected int _entityid;
        protected int _FundRequestSTATUS;
		protected DateTimeOffset _lastupdatedon;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public FundingRequestDao() {}

        public FundingRequestDao(int pRequestedBy, int pCostCenterid, int pEntityid, int pFundRequestSTATUS, DateTimeOffset pLastUpdatedOn)
		{
			
			this._requestedby = pRequestedBy; 
			this._costcenterid = pCostCenterid; 
			this._entityid = pEntityid; 
			this._lastupdatedon = pLastUpdatedOn;
            this._FundRequestSTATUS = pFundRequestSTATUS; 
		}
				
		public FundingRequestDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		
		
	
		public virtual int RequestedBy
		{
			get { return _requestedby; }
			set { _bIsChanged |= (_requestedby != value); _requestedby = value; }
			
		}
		
		public virtual int CostCenterid
		{
			get { return _costcenterid; }
			set { _bIsChanged |= (_costcenterid != value); _costcenterid = value; }
			
		}

        public virtual int FundRequestSTATUS
		{
            get { return _FundRequestSTATUS; }
            set { _bIsChanged |= (_FundRequestSTATUS != value); _FundRequestSTATUS = value; }
			
		}

        public virtual int Entityid
        {
            get { return _entityid; }
            set { _bIsChanged |= (_entityid != value); _entityid = value; }

        }
		
		
		public virtual DateTimeOffset LastUpdatedOn
		{
			get { return _lastupdatedon; }
			set { _bIsChanged |= (_lastupdatedon != value); _lastupdatedon = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string FundingRequestDao = "FundingRequestDao";			
			public const string Id = "Id";			
			public const string RequestedBy = "RequestedBy";			
			public const string CostCenterid = "CostCenterid";			
			public const string Entityid = "Entityid";
            public const string FundRequestSTATUS = "FundRequestSTATUS";		
			public const string LastUpdatedOn = "LastUpdatedOn";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string FundingRequestDao = "FundingRequestDao";			
			public const string Id = "Id";			
			public const string RequestedBy = "RequestedBy";			
			public const string CostCenterid = "CostCenterid";			
			public const string Entityid = "Entityid";
            public const string FundRequestSTATUS = "FundRequestSTATUS";		
			public const string LastUpdatedOn = "LastUpdatedOn";			
		}

		#endregion
		
		
	}
	
}
