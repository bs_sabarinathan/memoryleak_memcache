﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    internal class PredefineObjectives : IPredefineObjectives
    {
      public int ObjectiveEntityId { get; set; }
      public int ObjectiveId { get; set; }
      public string ObjectiveName { get; set; }
      public string ObjectiveDescription { get; set; }
      public int ObjectiveTypeId { get; set; }
      public decimal PlannedTarget { get; set; }
      public decimal TargetOutcome { get; set; }
      public int Rating { get; set; }
      public string RatingCaption { get; set; }
      public int FulfillmentState { get; set; }
      public string UnitName { get; set; }
      public int Satus { get; set; }
      public int UnitId { get; set; }
      public string ObjectiveComments { get; set; }
      public bool IsEnableFeedback { get; set; }
      public IList<IEntityRatings> ObjectiveRatings { get; set; }
    }
    internal class EntityRatings : IEntityRatings
    {
      public int RatingId { get; set; }
      public string RatingName { get; set; }
      public int SortOrder { get; set; }
    }
}
