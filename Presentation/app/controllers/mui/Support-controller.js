﻿(function (ng, app) {
    "use strict";
    function muisupportCtrl($scope, $timeout, $http, $compile, $window, $resource, SupportService) {
        $scope.SelectionList = [];
        $scope.SaveSupportPageContent = function () {
            var presentationObj = {};
            presentationObj.entityList = $scope.SelectionList;
            if ($scope.htmlEditorText == undefined) {
                presentationObj.Content = null;
            }
            else {
                presentationObj.Content = $scope.htmlEditorText;
            }
            SupportService.InsertEditortext(presentationObj).then(function () {
                $scope.showEditor = false;
                $scope.showHolder = true;
                $('#EditorHolder').html($scope.htmlEditorText);
            })
        };
        $scope.SupportContent = [];
        $scope.EnableEdit = {
            EditStatus: true,
            EditText: "Edit"
        };
        var SupportText = '';
        SupportService.GetSupportText().then(function (geteditordata) {
            $scope.showHolder = true;
            $("#EditorHolder").html(geteditordata.Response)
        });
        $scope.SupportContent = "";
        $scope.EditSupport = function () {
            $scope.showEditor = true;
            $scope.showHolder = false;
            $scope.htmlEditorText = $("#EditorHolder").html();
        };
    }
    app.controller("mui.supportCtrl", ['$scope', '$timeout', '$http', '$compile', '$window', '$resource', 'SupportService', muisupportCtrl]);

    function SupportService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetSupportText: GetSupportText,
            InsertEditortext: InsertEditortext
        });
        function GetSupportText() { var request = $http({ method: "get", url: "api/common/GetSupportText/", params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function InsertEditortext(jobj) { var request = $http({ method: "post", url: "api/common/InsertEditortext/", params: { action: "add", }, data: jobj }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { response.data.StatusCode = response.status; return (response.data); }
    }
    app.service("SupportService", ['$http', '$q', SupportService]);
})(angular, app);