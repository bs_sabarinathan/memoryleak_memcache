﻿(function (ng, app) {
    "use strict";

    function muiadminbusinessdaysCtrl($scope, $location, $translate, AdminService) {
        $scope.Curr_HolidayID = 0;
        $scope.listOfHolidays = {};
        $scope.addorupdate = "Add";
        $scope.days = {
            "0": {
                Caption: "Sunday",
                IsChecked: false
            },
            "1": {
                Caption: "Monday",
                IsChecked: false
            },
            "2": {
                Caption: "Tuesday",
                IsChecked: false
            },
            "3": {
                Caption: "Wednesday",
                IsChecked: false
            },
            "4": {
                Caption: "Thursday",
                IsChecked: false
            },
            "5": {
                Caption: "Friday",
                IsChecked: false
            },
            "6": {
                Caption: "Saturday",
                IsChecked: false
            }
        };
        var weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var model;
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            $scope.enddate1 = false;
            model = model1;
        };
        LoadNonBusinessDays();
        LoadHolidayList();
        $scope.SaveNonBusinessDay = function (subitems) {
            var noofNonBusinessDays = 0;
            if (subitems.IsChecked) {
                if ($scope.days[0].IsChecked) {
                    noofNonBusinessDays++;
                }
                if ($scope.days[1].IsChecked) {
                    noofNonBusinessDays++;
                }
                if ($scope.days[2].IsChecked) {
                    noofNonBusinessDays++;
                }
                if ($scope.days[3].IsChecked) {
                    noofNonBusinessDays++;
                }
                if ($scope.days[4].IsChecked) {
                    noofNonBusinessDays++;
                }
                if ($scope.days[5].IsChecked) {
                    noofNonBusinessDays++;
                }
                if ($scope.days[6].IsChecked) {
                    noofNonBusinessDays++;
                }
                if (noofNonBusinessDays != null) {
                    if (noofNonBusinessDays > 6) {
                        bootbox.alert($translate.instant('LanguageContents.Res_5658.Caption'));
                        LoadNonBusinessDays();
                    } else {
                        AdminService.SaveHolidayDetails(subitems.Caption).then(function (response) {
                            if (response) {
                                NotifySuccess(($translate.instant('LanguageContents.Res_4805.Caption')));
                            }
                            LoadNonBusinessDays();
                        });
                    }
                } else {
                    AdminService.SaveHolidayDetails(subitems.Caption).then(function (response) {
                        if (response) {
                            NotifySuccess(($translate.instant('LanguageContents.Res_4805.Caption')));
                        }
                        LoadNonBusinessDays();
                    });
                }
            } else {
                AdminService.SaveHolidayDetails(subitems.Caption).then(function (response) {
                    if (response) {
                        NotifySuccess(($translate.instant('LanguageContents.Res_4805.Caption')));
                    }
                    LoadNonBusinessDays();
                });
            }
        };
        $scope.SaveHolidayData = function () {
            if ($scope.holidayname != "" && $scope.Setdate != "") {
                if ($('#saveHoliday').hasClass('disabled')) {
                    return;
                }
                $('#saveHoliday').addClass('disabled');
                var holiday = {};
                holiday.holidayname = $scope.holidayname;
                holiday.holidaydate = dateFormat($scope.Setdate.toString(), "yyyy/MM/dd");
                var date = $scope.Setdate;
                holiday.ID = $scope.Curr_HolidayID;
                AdminService.InsertHolidayDetails(holiday).then(function (result) {
                    if (result.Response) {
                        NotifySuccess(($translate.instant('LanguageContents.Res_5659.Caption')));
                        LoadHolidayList();
                        $scope.Curr_HolidayID = 0;
                        $scope.holidayname = "";
                        $scope.Setdate = "";
                    } else {
                        NotifyError(($translate.instant('LanguageContents.Res_4329.Caption')));
                    }
                    $('#saveHoliday').removeClass('disabled');
                    $scope.addorupdate = "Add";
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_5660.Caption'));
            }
        };

        function LoadNonBusinessDays() {
            AdminService.GetNonBusinessDays().then(function (result) {
                var data = result.Response;
                if (data[0] != null) {
                    $scope.nonbusinessdays = data[0];
                    $scope.days = {
                        "0": {
                            Caption: "Sunday",
                            IsChecked: (data[0].sunday) == "true" ? true : false
                        },
                        "1": {
                            Caption: "Monday",
                            IsChecked: (data[0].monday) == "true" ? true : false
                        },
                        "2": {
                            Caption: "Tuesday",
                            IsChecked: (data[0].tuesday) == "true" ? true : false
                        },
                        "3": {
                            Caption: "Wednesday",
                            IsChecked: (data[0].wednesday) == "true" ? true : false
                        },
                        "4": {
                            Caption: "Thursday",
                            IsChecked: (data[0].thursday) == "true" ? true : false
                        },
                        "5": {
                            Caption: "Friday",
                            IsChecked: (data[0].friday) == "true" ? true : false
                        },
                        "6": {
                            Caption: "Saturday",
                            IsChecked: (data[0].saturday) == "true" ? true : false
                        }
                    };
                }
            });
        }

        function LoadHolidayList() {
            AdminService.GetHolidaysDetails().then(function (result) {
                if (result.Response != null) {
                    $scope.listOfHolidays = result.Response;
                    for (var a = 0; a < $scope.listOfHolidays.length; a++) {                      
                        $scope.listOfHolidays[a].HolidayDate = dateFormat($scope.listOfHolidays[a].HolidayDate, $scope.DefaultSettings.DateFormat);;
                        $scope.tempholidays[a] = $scope.listOfHolidays[a].HolidayDate;
                        var temp = new Date.create($scope.listOfHolidays[a].HolidayDate, $scope.format).getDay();
                        $scope.listOfHolidays[a].Day = weekday[temp];
                    }
                } else {
                    $scope.listOfHolidays = {};
                }
            });
        }
        $scope.UpdateHoliday = function (row) {
            var holidaysData = $.grep($scope.listOfHolidays, function (e) {
                return e.HolidayName == row.HolidayName;
            });
            $scope.addorupdate = "Update";
            $scope.holidayname = holidaysData[0].HolidayName;
            $scope.Setdate = holidaysData[0].HolidayDate;
            $scope.Curr_HolidayID = holidaysData[0].ID;
        };
        $scope.DeleteHoliday = function (row) {
            AdminService.DeleteHoliday(row.ID).then(function (res) {
                if (res.Response) {
                    NotifySuccess(($translate.instant('LanguageContents.Res_4206.Caption')));
                    LoadHolidayList();
                } else {
                    NotifyError(($translate.instant('LanguageContents.Res_4284.Caption')));
                }
            });
        }
        $scope.Clearscope = function () {
            $scope.holidayname = "";
            $scope.Setdate = "";
        }
        $scope.days = {};
        $scope.holidayname = "";
        $scope.Setdate = "";
        $scope.Curr_HolidayID = 0;
        $scope.listOfHolidays = {};
    }
    app.controller("mui.admin.muiadminbusinessdaysCtrl", ['$scope', '$location', '$translate', 'AdminService', muiadminbusinessdaysCtrl]);
})(angular, app);