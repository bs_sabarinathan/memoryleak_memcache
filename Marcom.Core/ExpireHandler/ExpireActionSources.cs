﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.DAM.Interface;
using BrandSystems.Marcom.Core.ExpireHandler.Interface;
namespace BrandSystems.Marcom.Core.ExpireHandler
{
    public class ExpireActionSources : IExpireActionSources
    {
        #region Member Variables
        protected int _id;
        protected int _actionID;
        protected int _sourceID;
        protected int _sourceEnityID;
        protected int _sourceFrom;
        protected string _actionexutedays;
        protected DateTime _actionexutedate;
        protected bool _actionexute;
        protected int _actionownerId;
        #endregion

        #region Constructors
        public ExpireActionSources() { }

        public ExpireActionSources(int pactionID, int psourceID, int psourceEnityID, int psourceFrom, string pactionexutedays, DateTime pactionexutedate, bool pactionexute, int pactionownerId)
        {
            this._actionID = pactionID;
            this._sourceID = psourceID;
            this._sourceEnityID = psourceEnityID;
            this._sourceFrom = psourceFrom;
            this._actionexutedays = pactionexutedays;
            this._actionexutedate = pactionexutedate;
            this._actionexute = pactionexute;
            this._actionownerId = pactionownerId;
        }

        public ExpireActionSources(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public virtual int ActionID
        {
            get { return _actionID; }
            set { _actionID = value; }

        }
        public virtual int SourceID
        {
            get { return _sourceID; }
            set { _sourceID = value; }

        }

        public virtual int SourceEnityID
        {
            get { return _sourceEnityID; }
            set { _sourceEnityID = value; }

        }
        public virtual int SourceFrom
        {
            get { return _sourceFrom; }
            set { _sourceFrom = value; }

        }

        public virtual string Actionexutedays
        {
            get { return _actionexutedays; }
            set { _actionexutedays = value; }
        }

        public virtual DateTime Actionexutedate
        {
            get { return _actionexutedate; }
            set { _actionexutedate = value; }
        }

        public virtual bool Actionexute
        {
            get { return _actionexute; }
            set { _actionexute = value; }
        }

        public virtual int ActionownerId
        {
            get { return _actionownerId; }
            set { _actionownerId = value; }

        }
        public IList<IAttributeData> AttributeData { get; set; }
        public IList<IExpireActionSourcedatas> ActionSourcedatas { get; set; }
        #endregion





    }
}
