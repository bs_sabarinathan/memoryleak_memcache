using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IEntityPresentation interface for table 'PM_Entity_Presentation'.
    /// </summary>
    public interface IEntityPresentation
    {
        #region Public Properties

        int EntityId
        {
            get;
            set;

        }

        DateTimeOffset PublishedOn
        {
            get;
            set;

        }

        string Content
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
