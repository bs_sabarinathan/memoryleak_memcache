﻿(function(ng, app) {
	"use strict";

	function muiDashBoardWidgetMilestoneWidgetCtrl($scope, $timeout, $http, $compile, $resource, $cookies, $location, $window, $translate, MilestonewidgetService) {
		$scope.userimg = parseInt($cookies['UserId'])
		$scope.MilestonedivID = "EntityFeedsdiv_" + $scope.WizardID;
		$timeout(function () { LoadNewsFeed(); }, 100);

		function LoadNewsFeed() {
			$scope.MilestoneID = 0;
			$scope.milestones = [];
			$scope.milestoneRowIndex = 0;
			MilestonewidgetService.GetMilestoneforWidget(0, 1).then(function(GetAllMilestones) {
				var milestonesData = GetAllMilestones.Response;
				$('#' + $scope.MilestonedivID + '').parents('.box-content').find('.InProgress').remove();
				for (var i = 0; i < milestonesData.length; i++) {
					var milestone = {};
					milestone["MileStoneId"] = milestonesData[i].EntityId;
					milestone["ParentEntityId"] = milestonesData[i].ParentEntityId;
					milestone["ParentEntityName"] = milestonesData[i].ParentEntityName;
					milestone["ParentEntityTypeShortDescription"] = milestonesData[i].ParentEntityTypeShortDescription;
					milestone["ParentEntityTypeColorCode"] = milestonesData[i].ParentEntityTypeColorCode;
					for (var j = 0; j < milestonesData[i].AttributeData.length; j++) {
						if (milestonesData[i].AttributeData[j].Caption.toLowerCase() == "status") {
							if (milestonesData[i].AttributeData[j].Value == true) {
								milestone[milestonesData[i].AttributeData[j].Caption.replace(/\s/g, '')] = "Reached";
							} else {
								milestone[milestonesData[i].AttributeData[j].Caption] = "Not Reached";
							}
						} else {
							milestone[milestonesData[i].AttributeData[j].Caption.replace(/\s/g, '')] = milestonesData[i].AttributeData[j].Value;
						}
					}
					$scope.milestones.push(milestone);
				}
			});
			try {
				$scope.addMilestone = function() {
					$scope.milestoneDueDate = {};
					$scope.milestone = 'Add Milestone';
					$scope.MilestoneName = '';
					$scope.milestoneDueDate = null;
					$scope.Description = '';
					$scope.EnableAdd = true;
					$scope.EnableUpdate = false;
				};
				$scope.MilestoneName = '';
				$scope.milestoneDueDate = null;
				$scope.Description = '';
				$scope.MilestoneID = 0;
				$scope.MilestonePrevStatus = '';
				$scope.GetMilestoneByID = function(row) {
					$scope.milestoneRowIndex = row.$index;
					$scope.MilestoneName = row.milestone.Name;
					$scope.milestoneDueDate = row.milestone.DueDate
					$scope.Description = row.milestone.Shortdescription;
					$scope.MilestoneID = row.milestone.MileStoneId;
					$scope.MilestonePrevStatus = row.milestone.Status;
					$scope.EnableAdd = false;
					$scope.EnableUpdate = true;
				};
				$scope.milestonechecked = function(row) {
					$scope.GetMilestoneByID(row);
				}
				$scope.save = function() {
					if ($scope.MilestoneName == '' || $scope.milestoneDueDate == null) {
						bootbox.alert($translate.instant('LanguageContents.Res_1860.Caption'));
						return false;
					}
					MilestonewidgetService.CreateMilestone().then(function (insertmilestone) {
						insertmilestone.EntityID = parseInt($routeParams.ID);
						insertmilestone.Name = $scope.MilestoneName;
						insertmilestone.Description = $scope.Description;
						$scope.DueDate = $scope.milestoneDueDate;
						var Attributes = [];
						Attributes = [{
							ID: 56,
							TypeID: 5,
							Caption: 'DueDate',
							Value: $scope.DueDate
						}, {
							ID: 58,
							TypeID: 1,
							Caption: 'Name',
							Value: $scope.MilestoneName
						}, {
							ID: 2,
							TypeID: 2,
							Caption: 'ShortDescription',
							Value: $scope.Description
						}, {
							ID: 66,
							TypeID: 3,
							Caption: 'EntityID',
							Value: insertmilestone.EntityID
						}, {
							ID: 67,
							TypeID: 9,
							Caption: 'Status',
							Value: 0
						}];
						insertmilestone.attributes = Attributes;
						var milestone = {};
						milestone["MileStoneId"] = $scope.MilestoneID;
						milestone["Name"] = $scope.MilestoneName;
						milestone["DueDate"] = $scope.milestoneDueDate.toISOString().split('T')[0];
						milestone["ShortDescription"] = $scope.Description;
						milestone["EntityID"] = insertmilestone.EntityID;
						milestone["Status"] = 'Not Reached';
						$scope.milestones.push(milestone);
					});
					$scope.DueDate = null;
					$('#AddMilestoneModel').modal('hide');
					NotifySuccess($translate.instant('LanguageContents.Res_4947.Caption'));
				};
				$scope.duedateFormatforMilestone = function(dateval) {
					if (dateval != undefined) {
						return dateFormat(dateval, $scope.DefaultSettings.DateFormat)
					}
				}
				$scope.update = function() {
					var milstoneNewStatus = '';
					if ($scope.MilestonePrevStatus == 'Not Reached') {
						milstoneNewStatus = 0;
					} else {
						milstoneNewStatus = 1;
					}
					if ($scope.MilestoneName == '' || $scope.milestoneDueDate == null) {
						bootbox.alert($translate.instant('LanguageContents.Res_1860.Caption'));
						return false;
					}
					MilestonewidgetService.UpdateMilestone($scope.MilestoneID).then(function(updateMilestoneData) {
						updateMilestoneData.EntityID = parseInt($routeParams.ID);
						updateMilestoneData.Name = $scope.Description;
						updateMilestoneData.DueDate = $scope.milestoneDueDate;
						var Attributes = [];
						Attributes = [{
							ID: 56,
							TypeID: 5,
							Caption: 'DueDate',
							Value: updateMilestoneData.DueDate
						}, {
							ID: 58,
							TypeID: 1,
							Caption: 'Name',
							Value: $scope.MilestoneName
						}, {
							ID: 2,
							TypeID: 2,
							Caption: 'ShortDescription',
							Value: $scope.Description
						}, {
							ID: 66,
							TypeID: 3,
							Caption: 'EntityID',
							Value: updateMilestoneData.EntityID
						}, {
							ID: 67,
							TypeID: 9,
							Caption: 'Status',
							Value: milstoneNewStatus
						}];
						updateMilestoneData.attributes = Attributes;
						$scope.milestones[$scope.milestoneRowIndex]["MileStoneId"] = $scope.MilestoneID;
						$scope.milestones[$scope.milestoneRowIndex]["Name"] = $scope.MilestoneName;
						$scope.milestones[$scope.milestoneRowIndex]["DueDate"] = $scope.milestoneDueDate.toISOString().split('T')[0];
						$scope.milestones[$scope.milestoneRowIndex]["ShortDescription"] = $scope.Description;
						$scope.milestones[$scope.milestoneRowIndex]["EntityID"] = insertmilestone.EntityID;
						$scope.milestones[$scope.milestoneRowIndex]["Status"] = 'Not Reached';
					});
					$scope.DueDate = null;
					$('#AddMilestoneModel').modal('hide');
					NotifySuccess($translate.instant('LanguageContents.Res_4486.Caption'));
				};
				$scope.DeleteMilestone = function DeleteMilestoneByID(row) {
					bootbox.confirm($translate.instant('LanguageContents.Res_2020.Caption'), function(result) {
						if (result) {
							$timeout(function() {
								var ID = row.milestone.MileStoneId;
								MilestonewidgetService.DeleteMileStone(ID).then(function() {
									var index = $.inArray(ID, $scope.milestones);
									$scope.milestones.splice(index, 1);
								});
								NotifySuccess($translate.instant('LanguageContents.Res_4484.Caption'));
								MilestonewidgetService.DeleteEntity([row.milestone.MileStoneId], $scope.entityName).then(function() {});
							}, 100);
						}
					});
				};
				$scope.saveMilestStatus = function(milestoneid, milestonename, milestoneduedate, milestoneshortdescription, optionarray) {
					MilestonewidgetService.UpdateMilestone(milestoneid).then(function(updateMilestoneData) {
						var Attributes = [];
						Attributes = [{
							ID: 56,
							TypeID: 5,
							Caption: 'DueDate',
							Value: milestoneduedate
						}, {
							ID: 58,
							TypeID: 1,
							Caption: 'Name',
							Value: milestonename
						}, {
							ID: 2,
							TypeID: 2,
							Caption: 'ShortDescription',
							Value: milestoneshortdescription
						}, {
							ID: 66,
							TypeID: 3,
							Caption: 'EntityID',
							Value: parseInt($routeParams.ID)
						}, {
							ID: 67,
							TypeID: 9,
							Caption: 'Status',
							Value: parseInt(optionarray)
						}];
						updateMilestoneData.attributes = Attributes;
						updateMilestoneData.$update();
						NotifySuccess($translate.instant('LanguageContents.Res_4485.Caption'));
					});
				};
			} catch (e) {}
		}
	
		$scope.$on("$destroy", function() {
			RecursiveUnbindAndRemove($("[ng-controller='mui.DashBoardWidget.MilestoneWidgetCtrl']"));
		});
	}
	app.controller("mui.DashBoardWidget.MilestoneWidgetCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$location', '$window', '$translate', 'MilestonewidgetService', muiDashBoardWidgetMilestoneWidgetCtrl]);
	function MilestonewidgetService($http, $q) {
	    $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
	        GetMilestoneforWidget: GetMilestoneforWidget,
	       // InsertMilestone: InsertMilestone,
	        CreateMilestone:CreateMilestone,
	        UpdateMilestone: UpdateMilestone,
	        DeleteMileStone: DeleteMileStone,
	        DeleteEntity: DeleteEntity
	    });
	    function GetMilestoneforWidget(EntityID, EntityTypeId) { var request = $http({ method: "get", url: "api/Planning/GetMilestoneforWidget/" + EntityID + "/" + EntityTypeId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
	    function CreateMilestone(formobj) { var request = $http({ method: "post", url: "api/Planning/CreateMilestone/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
	    function UpdateMilestone(formobj) { var request = $http({ method: "post", url: "api/Planning/UpdateMilestone/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
	    function DeleteMileStone(ID) { var request = $http({ method: "delete", url: "api/Planning/DeleteMileStone/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
	    function DeleteEntity(formobj) { var request = $http({ method: "post", url: "api/Planning/DeleteEntity/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
	    function handleError(response) {
	        if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
	        return ($q.reject(response.data.message));
	    }
	    function handleSuccess(response) { return (response.data); }
	}
	app.service("MilestonewidgetService", ['$http', '$q', MilestonewidgetService]);
})(angular, app);