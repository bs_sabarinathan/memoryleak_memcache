﻿(function (ng, app) {
    function muicmsmemberCtrl($scope, $timeout, $window, $http, $compile, $resource, $stateParams, MemberService, $translate) {
        function sortOn(collection, name) {
            collection.sort(function (a, b) {
                if (a[name] <= b[name]) {
                    return (-1);
                }
                return (1);
            });
        }

        function GetLevelfromUniqueKey(UniqueKey) {
            var substr = UniqueKey.split('.');
            return substr.length;
        }
        $scope.groupBy = function (attribute, ISglobal) {
            $scope.groups = [];
            var GroupEntityID = "_Inavlid_EntityID";
            var groupValue = "_INVALID_GROUP_VALUE_";
            sortOn($scope.memberList, attribute);
            sortOn($scope.memberList, "InheritedFromEntityid");
            for (var i = 0, friend; friend = $scope.memberList[i++];) {
                if (ISglobal == false) {
                    $scope.ShowMemberEntityHeader = ISglobal;
                    if (friend["IsInherited"] === false) {
                        if (friend["InheritedFromEntityid"] !== GroupEntityID) {
                            var NewEntityGroup = {
                                Name: friend.InheritedFromEntityName,
                                ColorCode: friend.ColorCode,
                                ShortText: friend.ShortText,
                                Groups: []
                            };
                            GroupEntityID = friend["InheritedFromEntityid"];
                            $scope.groups.push(NewEntityGroup);
                        }
                        if (GroupEntityID == friend["InheritedFromEntityid"]) {
                            if (friend[attribute] !== groupValue) {
                                var group = {
                                    label: friend[attribute],
                                    friends: [],
                                    rolename: friend.Role
                                };
                                groupValue = group.label;
                                NewEntityGroup.Groups.push(group);
                            }
                            group.friends.push(friend);
                        }
                    }
                } else if (ISglobal == true) {
                    $scope.ShowMemberEntityHeader = ISglobal;
                    if (friend["InheritedFromEntityid"] !== GroupEntityID) {
                        var NewEntityGroup = {
                            Name: friend.InheritedFromEntityName,
                            ColorCode: friend.ColorCode,
                            ShortText: friend.ShortText,
                            Groups: []
                        };
                        GroupEntityID = friend["InheritedFromEntityid"];
                        $scope.groups.push(NewEntityGroup);
                    }
                    if (GroupEntityID == friend["InheritedFromEntityid"]) {
                        if (friend[attribute] !== groupValue) {
                            var group = {
                                label: friend[attribute],
                                friends: [],
                                rolename: friend.Role
                            };
                            groupValue = group.label;
                            NewEntityGroup.Groups.push(group)
                        }
                        group.friends.push(friend);
                    }
                }
            }
        };
        $scope.UserimageNewTime = new Date().getTime().toString();
        $scope.memberList = [];
        $scope.groups = [];
        $scope.$on('LoadMembersDetail', function (event, ID) {
            $scope.EntityID = ID;
            $scope.load();
            $scope.GlobalAccessStatus = {
                GlobalText: "Show global access",
                GlobalStatus: 0
            };
        });
        $scope.AutoCompleteSelectedObj = [];
        $scope.EntityID = $stateParams.ID;
        $scope.IsInherited = true
        $scope.InheritedFromEntityid = $stateParams.ID;
        $scope.UserLists = {};
        $scope.Roles = {};
        $scope.load = function (parameters) {
            $scope.AutoCompleteSelectedObj = [];
            $scope.EntityID = $stateParams.ID;
            $scope.IsInherited = true
            $scope.InheritedFromEntityid = $stateParams.ID;
            $scope.UserLists = {};
            $scope.Roles = {};
            MemberService.GetMember($scope.EntityID).then(function (member) {
                var resp = member.Response;
                $scope.memberList = member.Response;
                $scope.groupBy('Roleid', false);
                $scope.QuickInfo1AttributeCaption = $scope.memberList[0].QuickInfo1AttributeCaption;
                $scope.QuickInfo2AttributeCaption = $scope.memberList[0].QuickInfo2AttributeCaption;
                MemberService.GetUsers().then(function (UserbyNamelist) {
                    $scope.UserLists = UserbyNamelist.Response;
                });
            });
            MemberService.GetEntityDetailsByID($scope.EntityID).then(function (GetEntityresult) {
                if (GetEntityresult.Response != null) {
                    var levelid = GetEntityresult.Response.Level;
                    $scope.ShowGlobalAccess = levelid > 0 ? true : false;
                    MemberService.GetEntityTypeRoleAccess(GetEntityresult.Response.Typeid).then(function (role) {
                        $scope.Roles = role.Response;
                    });
                }
            });
        };
        $scope.ddlrole = '';
        $scope.addMember = function () {
            $("#memberModal").modal('show');
            $timeout(function () {
                $('#memberModal').modal('show');
                $('#ddluser').focus();
            }, 1000)
        };
        $scope.AutoCompleteSelectedUserObj = {};
        $scope.AutoCompleteSelectedUserObj.UserSelection = {};
        $scope.addEntityMember = function () {
            try {
                if ($scope.ddlrole == '' || $scope.ddlrole.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1906.Caption'));
                    $('#addentitymemberID').removeClass('disabled');
                    return false;
                } else {
                    $('#addentitymemberID').addClass('disabled');
                }
                var userval = $('#ddluser').val();
                if (userval.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_5749.Caption'));
                    $('#addentitymemberID').removeClass('disabled');
                    return false;
                }
                var membervalues = $.grep($scope.Roles, function (e) {
                    return e.ID == parseInt($scope.ddlrole, 10)
                })[0];
                var insertmember = {};
                var result = $.grep($scope.memberList, function (e) {
                    return (e.Userid == parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10) && e.Roleid == membervalues.ID) && (e.IsInherited == false);
                });
                if (result.length == 0) {
                    insertmember.EntityID = $scope.EntityID;
                    insertmember.RoleID = membervalues.ID;
                    insertmember.Assignee = parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10);
                    insertmember.IsInherited = false;
                    insertmember.InheritedFromEntityid = 0;
                    MemberService.PostMember(insertmember).then(function (member) {
                        MemberService.GetMember($scope.EntityID).then(function (member) {
                            var resp = member.Response;
                            $scope.memberList = member.Response;
                            var GlobalAccessToshow = ($scope.GlobalAccessStatus.GlobalStatus == 1 ? true : false)
                            $scope.groupBy('Roleid', GlobalAccessToshow);
                        });
                        $('#ddlrole').select2('val', '');
                        $scope.ddlrole = '';
                        $('#addentitymemberID').removeClass('disabled');
                        $scope.ddlUser = '';
                        $scope.AutoCompleteSelectedObj = [];
                        $scope.AutoCompleteSelectedUserObj = {};
                        $scope.AutoCompleteSelectedUserObj.UserSelection = {};
                        NotifySuccess($translate.instant('LanguageContents.Res_4479.Caption'));
                    });
                } else {
                    $('#ddlrole').select2('val', '');
                    $scope.ddlrole = '';
                    $scope.ddlUser = '';
                    $scope.AutoCompleteSelectedObj = [];
                    bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
                    $('#addentitymemberID').removeClass('disabled');
                }
            } catch (e) {
                $('#addentitymemberID').removeClass('disabled');
            }
        };
        $scope.updateEntityMember = function (ID) {
            var UpdateMember = {};
            UpdateMember.ID = ID;
            UpdateMember.EntityID = $scope.EntityID;
            UpdateMember.RoleID = $scope.ddlrole;
            UpdateMember.Assignee = parseInt($scope.AutoCompleteSelectedUserObj.UserSelection.Id, 10);
            UpdateMember.IsInherited = $scope.IsInherited;
            UpdateMember.InheritedFromEntityid = $scope.InheritedFromEntityid;
            MemberService.UpdateMemberInEntity(UpdateMember).then(function (updatemember) { });
        };
        $scope.deleteEntityMember = function (ID, Name) {
            if ($scope.IsLock == true) {
                return false;
            }
            bootbox.confirm($translate.instant('LanguageContents.Res_1851.Caption') + " " + Name + " ?", function (result) {
                if (result) {
                    $timeout(function () {
                        MemberService.Member(ID).then(function (deletemember) {
                            MemberService.GetMember($scope.EntityID).then(function (member) {
                                var resp = member.Response;
                                $scope.memberList = member.Response;
                                var GlobalAccessToshow = ($scope.GlobalAccessStatus.GlobalStatus == 1 ? true : false)
                                $scope.groupBy('Roleid', GlobalAccessToshow);
                            });
                        });
                    }, 100);
                }
            });
        };
        $scope.GlobalAccessStatus = {
            GlobalText: "Show global access",
            GlobalStatus: 0
        };
        $scope.toggleAutoUserAccess = function () {
            if ($scope.GlobalAccessStatus.GlobalStatus == 0) {
                $scope.GlobalAccessStatus.GlobalStatus = 1;
                $scope.GlobalAccessStatus.GlobalText = "Hide global access";
                $scope.groupBy('Roleid', true);
            } else {
                $scope.GlobalAccessStatus.GlobalStatus = 0;
                $scope.GlobalAccessStatus.GlobalText = "Show global access";
                $scope.groupBy('Roleid', false);
            }
        }
        $scope.CloseAddmember = function () {
            $('#ddlrole').select2('val', '');
            $scope.ddlrole = '';
            $('#addentitymemberID').removeClass('disabled');
            $scope.ddlUser = '';
            $scope.AutoCompleteSelectedObj = [];
        }

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.cms.default.detail.memberCtrl']"));
        });
        $timeout(function () {
            $scope.load();
        }, 0);
    }
    app.controller("mui.cms.default.detail.memberCtrl", ['$scope', '$timeout', '$window', '$http', '$compile', '$resource', '$stateParams', 'MemberService', '$translate', muicmsmemberCtrl]);
})(angular, app);