﻿(function (ng, app) {
    "use strict";

    function muiexpireaddActionsCtrl($window, $location, $timeout, $scope, $cookies, $resource, $translate, ExpireActionService, $sce, $compile, $modalInstance, params) {
        //$scope.welcommsg1 = "hello came";
        var model;
        var timerObj = {};
        $scope.GetNonBusinessDaysforDatePicker();
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            $scope[model1] = true;
            model = model1;
        };
        $scope.PeriodCalanderopen = function ($event, item, place) {
            $event.preventDefault();
            $event.stopPropagation();
            if (place == "start") {
                item.calstartopen = true;
                item.calendopen = false;
            } else if (place == "end") {
                item.calstartopen = false;
                item.calendopen = true;
            }
        };
        $scope.DateActionObject = { "dueExpireDate": '', 'dateExpirevalue': '', 'editable': '' };
        $scope.ClearModelObject = function (ModelObject) {
            for (var variable in ModelObject) {
                if (typeof ModelObject[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        ModelObject[variable] = "";
                    }
                }
                else if (typeof ModelObject[variable] === "number") {
                    ModelObject[variable] = null;
                }
                else if (Array.isArray(ModelObject[variable])) {
                    ModelObject[variable] = [];
                }
                else if (typeof ModelObject[variable] === "object") {
                    ModelObject[variable] = {};
                }
            }

        }

        timerObj.openexpireactionpopup = $timeout(function () { OpenExpireactionpopup(params.dateExpireval, params.AssetEditID, params.assetEntityId, params.sourcefrom, params.assettypeidfrbtn, params.ProductionEntityID, params.SourceAttributeID, params.SourceAttributename); }, 100);
        function OpenExpireactionpopup(dateExpireval, SourceID, SourceEnityID, SourceFrom, SourcetypeID, productionEntityID, SourceAttributeID, SourceAttributename) {
            $scope.DateActionObject.dateExpirevalue = dateExpireval == undefined ? "-" : dateExpireval;
            $scope.dateExpirevalue1 = dateExpireval;
            $scope.SourceID = SourceID;
            $scope.SourceEnityID = SourceEnityID;
            $scope.SourceFrom = SourceFrom;
            $scope.SourcetypeID = SourcetypeID;
            $scope.SourceAttributeID = SourceAttributeID;
            $scope.SourceAttributename = SourceAttributename;
            $scope.ProductionEntityID = productionEntityID;
            $scope.unpublishactionData = []
            $scope.Dropdown = {};
            $scope.ShowHideAttributeOnRelation = {};
            $scope.EnableDisableControlsHolder = {};
            $scope.DateActionObject.editable = '';
            cleanpopupscope();
            GetTaskType();
            getenitymenber();

            //GetDestinationEntityRoles($scope.ProductionEntityID);
            LoadCustomMetadataPart();
            $scope.savevalidation = { ValidActionday: false }
            $("#expirepopup").modal('show');
            if ($scope.dateExpirevalue1 != "-" && $scope.ProductionEntityID > 0) {
                $('#Addactionmain').removeClass('disabled');
                $scope.Actionmain = true;
                if ($scope.SourceFrom == 1) {
                    if ($scope.SourceEnityID != $scope.ProductionEntityID) {
                        $scope.UnpublishAction = false;
                        $scope.CreateTaskAction = false;
                        $scope.UpdatemetadataAction = true;
                    }
                    else {
                        $scope.UnpublishAction = true;
                        $scope.UpdatemetadataAction = true;
                        $scope.CreateTaskAction = true;
                    }

                }
                else if ($scope.SourceFrom == 2) {
                    $scope.UnpublishAction = false;
                    $scope.UpdatemetadataAction = true;
                    $scope.CreateTaskAction = true;
                } else if ($scope.SourceFrom == 3) {
                    $scope.UnpublishAction = false;
                    $scope.UpdatemetadataAction = true;
                    $scope.CreateTaskAction = false;
                }
                else if ($scope.SourceFrom == 4) {
                    $scope.UnpublishAction = false;
                    $scope.UpdatemetadataAction = true;
                    $scope.CreateTaskAction = true;
                }

            }
            else {
                $('#Addactionmain').addClass('disabled');
                $scope.Actionmain = false;
            }

            $scope.CloseExpire = function () {
                //cleanpopupscope();
                $modalInstance.dismiss('cancel');
                RefreshAddNewTaskScopes();
            };



            if ($scope.SourceID != undefined) {
                ExpireActionService.GetExpireActions($scope.SourceID, $scope.SourceAttributeID, -1)
              .then(
              function (result) {

                  if (result.Response == 0) {

                      $scope.ngShowUnpublishpopupbodyopen = true;
                      $scope.showCTiconopen = true;


                  }
                  else {

                      $scope.unpublishactionResData = $.grep(result.Response, function (e) { return e.ActionType == 1; })[0];
                      if ($scope.unpublishactionResData != undefined) {
                          $scope.showUnpublishpopup = true;
                          var upActionexutedays = $.grep($scope.ExecutionDateList, function (e) { return e.id == $scope.unpublishactionResData.TimeDuration; })[0];
                          $scope.upActionexutedays = upActionexutedays.name;
                          $scope.UpExecutionDate = $scope.unpublishactionResData.TimeDuration;

                          $scope.Upsavecapation = "update";
                          $scope.UpActionId = $scope.unpublishactionResData.Id;
                          $scope.ngShowUnpublishpopupbodyopen = true;
                          $scope.showUnpublishdayoption = false;
                          $scope.ngShowUnpublishpopupbodyclose = false;
                          $scope.UpActionSourcedatas = JSON.parse($scope.unpublishactionResData.ActionSrcValue);
                          if ($scope.UpActionSourcedatas != undefined)
                              $scope.publishSelection = $scope.UpActionSourcedatas.Ispublish;
                          else $scope.publishSelection = false;
                      }
                      else {
                          $scope.showUnpublishpopup = false;
                          $scope.upExecutionDate = 0;
                          $scope.publishSelection = 0;
                          $scope.Upsavecapation = "Save";
                          $scope.UpActionId = 0;
                          $scope.ngShowUnpublishpopupbodyopen = false;
                          $scope.ngShowUnpublishpopupbodyclose = false;
                          $scope.publishSelection = false;
                      }
                      $scope.CreateTaskactionResData = $.grep(result.Response, function (e) { return e.ActionType == 2; });

                      for (var k = 0, data; data = $scope.CreateTaskactionResData[k++];) {
                          data["IsExpaned"] = false;
                      }

                      if ($scope.CreateTaskactionResData != undefined) {

                          if ($scope.CreateTaskactionResData.length > 0) {
                              $scope.ShowCreateTaskpopup = true;
                              $scope.showCTiconopen = true;
                              $scope.showCTiconclose = false;
                              $scope.showCTdayoption = false;
                              $scope.CTsavecapation = "update"
                          }
                          else {
                              $scope.CTActionId = 0;
                              $scope.ShowCreateTaskpopup = false;
                              $scope.showCTiconopen = true;
                              $scope.showCTiconclose = false;
                              $scope.showCTdayoption = false;
                              $scope.CTsavecapation = "Save";

                          }

                      }
                      else {
                          $scope.CTActionId = 0;
                          $scope.ShowCreateTaskpopup = false;
                          $scope.showCTiconopen = true;
                          $scope.showCTiconclose = false;
                          $scope.showCTdayoption = false;
                          $scope.CTsavecapation = "Save";

                      }
                      $scope.SaveExpireAction = [];
                      $scope.updatemeataactionResData = $.grep(result.Response, function (e) { return e.ActionType == 3; })[0];
                      if ($scope.updatemeataactionResData != undefined) {
                          $scope.UMActionId = $scope.updatemeataactionResData.Id;

                          var UMActionexutedays = $.grep($scope.ExecutionDateList, function (e) { return e.id == $scope.updatemeataactionResData.TimeDuration; })[0];
                          $scope.UMActionexutedays = UMActionexutedays.name;
                          $scope.UMExecutionDate = $scope.updatemeataactionResData.TimeDuration;
                          $scope.UMActionSourcedatas = JSON.parse($scope.updatemeataactionResData.ActionSrcValue).ActionTaskSrcValue.AttributeData;

                          $scope.ExpireEntityMetadataCollection = [];
                          if ($scope.UMActionSourcedatas != null) {
                              for (var v = 0, val; val = $scope.UMActionSourcedatas[v++];) {
                                  var exisoption = $.grep($scope.ExpireEntityMetadataCollection, function (rel) {
                                      return rel.Id == val.AttributeID;
                                  });
                                  if (exisoption == null) {
                                      var attr = { "Id": val.AttributeID.toString(), "Text": val.AttributeCaption, "HtmlControl": "" };
                                      $scope.GenerateMetadataControl(attr);
                                      $scope.ExpireEntityMetadataCollection.push(attr);
                                  }
                                  else if (exisoption.length == 0) {
                                      var attr = { "Id": val.AttributeID.toString(), "Text": val.AttributeCaption, "HtmlControl": "" };
                                      $scope.GenerateMetadataControl(attr);
                                      $scope.ExpireEntityMetadataCollection.push(attr);
                                  }

                              }
                          }

                          $scope.ShowUpdateMetadatapopup = true;
                          $scope.showUMiconopen = true;
                          $scope.showUMiconclose = false;
                          $scope.showUMdayoption = false;
                          $scope.UMsavecapation = "update"
                          //GetExpireHandlermetadata($scope.UMActionId);
                      }
                      else {
                          $scope.UMActionId = 0;
                          $scope.ShowUpdateMetadatapopup = false;
                          $scope.showUMiconopen = true;
                          $scope.showUMiconclose = false;
                          $scope.showUMdayoption = false;
                          $scope.UMsavecapation = "Save";

                      }

                  }
              });
            }
        }
        //});
        $scope.retriveTaskValuestodate = function (ActionId) {

            if (ActionId > 0) {
                var taskActionSrc = $.grep($scope.CreateTaskactionResData, function (e) { return e.Id == ActionId; })[0];
                var ctActionexutedays = $.grep($scope.ExecutionDateList, function (e) { return e.id == taskActionSrc.TimeDuration; })[0];
                return ctActionexutedays.name;
            }
            else { return "-"; }
        }
        $scope.retriveTaskValuestoBind = function (ActionId) {

            //$scope.CTActionId = ActionId;
            var taskActionSrc = $.grep($scope.CreateTaskactionResData, function (e) { return e.Id == $scope.CTActionId; })[0];
            var ctActionexutedays = $.grep($scope.ExecutionDateList, function (e) { return e.id == taskActionSrc.TimeDuration; })[0];
            $scope.CTActionexutedays = ctActionexutedays.name;
            $scope.TaskcommonObjects.CTActionexutedays = ctActionexutedays.name;
            $scope.TaskcommonObjects.CTExecutionDate = taskActionSrc.TimeDuration;
            $scope.CTActionSourcedatas = JSON.parse(taskActionSrc.ActionSrcValue);
            if (taskActionSrc != undefined) {
                if ($scope.CTActionSourcedatas.ActionTaskSrcValue != undefined);
                {
                    //$scope.TaskcommonObjects.NewTaskName = "";
                    //$scope.TaskcommonObjects.TaskDescription = "";
                    //$scope.TaskcommonObjects.TaskNote = "";
                    //$scope.TaskcommonObjects.TaskType = 0;
                    //$scope.TaskcommonObjects.CTdueExecutionDate = "";
                    //$scope.TaskcommonObjects.TaskMembersList = "";
                    //$scope.TaskcommonObjects.NewTaskName = "";
                    //$scope.TaskcommonObjects.TaskNote = "";
                    //$scope.TaskcommonObjects.SelectedTaskID = 0;
                    $scope.AdminTaskCheckList = [{ ID: 0, Name: "" }];
                    var taskobj = $scope.CTActionSourcedatas.ActionTaskSrcValue;
                    if (taskobj != undefined) {
                        $scope.TaskcommonObjects.TaskType = taskobj.Typeid;
                        $scope.TaskcommonObjects.NewTaskName = taskobj.Name;
                        $scope.TaskcommonObjects.TaskDescription = taskobj.Description;
                        $scope.TaskcommonObjects.TaskNote = taskobj.Note;
                        $scope.TaskcommonObjects.CTdueExecutionDate = (taskobj.DueDateoptions != null) ? taskobj.DueDateoptions : 1;
                        if (taskobj.TaskMembers != undefined && taskobj.TaskMembers.length > 0)
                            $scope.TaskcommonObjects.TaskMembersList = taskobj.TaskMembers;
                        if (taskobj.AdminTaskCheckList != undefined && taskobj.AdminTaskCheckList.length > 0)
                            $scope.AdminTaskCheckList = taskobj.AdminTaskCheckList;
                        //AttributeData
                        $scope.createdynamicControls(taskobj.Typeid, 1);
                        //LoadTaskMetadata(taskobj.AttributeData, -1, taskobj.Typeid, false);
                    }
                }
            }

        }

        function cleanpopupscope() {
            $scope.TaskcommonObjects = { "TaskType": 0, "NewTaskName": "", "TaskDescription": "", "TaskNote": "", "CTExecutionDate": 1, "CTdueExecutionDate": 1, "TaskMembersList": "", "CTActionexutedays": "" };
            $scope.DestinationMemberRoles = [{ ExpireRoleid: -1002, Caption: "Editor", Roleid: 2 }, { ExpireRoleid: -1003, Caption: "Viewer", Roleid: 3 }, { ExpireRoleid: -1005, Caption: "External Participant", Roleid: 5 }, { ExpireRoleid: -1008, Caption: "Budget Approver", Roleid: 8 }]
            $scope.ngShowUnassigned = false;
            $scope.showUnpublishpopup = false;
            $scope.ShowCreateTaskpopup = false;
            $scope.ShowUpdateMetadatapopup = false;
            $scope.UpExecutionDate = 1;
            $scope.upActionexutedays = "-";
            $scope.CTActionexutedays = "-";
            $scope.UMActionexutedays = "-";
            $scope.CTExecutionDate = 1;
            $scope.CTdueExecutionDate = 1;
            $scope.Upsavecapation = "Save";
            $scope.CTsavecapation = "Save";
            $scope.UpActionId = 0;
            $scope.CTActionId = 0;
            $scope.UMActionId = 0;
            $scope.UMExecutionDate = 1;
            $scope.UMsavecapation = "Save";
            $scope.publishSelection = 0;
            $scope.ExpireEntityMetadataCollection = [{ "Id": "0", "Text": $translate.instant('LanguageContents.Res_39.Caption'), "HtmlControl": "" }];
            $scope.CreateTaskactionResData = [];
            $scope.ClearModelObject($scope.fields);
        }

        $scope.saveaction = function (id) {

            if (id == 1) {
                $scope.showUnpublishpopup = true;
            }
            else if (id == 2) {
                $scope.ShowCreateTaskpopup = true;

            }
            else if (id == 3) {
                $scope.UpdateMetadatapopup = true;
            }

        }

        $scope.DatepickerShow = function (CurrentDueDate, CurrentId) {
            if (CurrentDueDate.length < 2) {
                $('#' + CurrentId).val('');
                $('#' + CurrentId).attr('AlredyDateSelected', 'true');
            }
            else {
                $('#' + CurrentId).attr('AlredyDateSelected', 'false');
            }
            timerObj.calender = $timeout(function () { $('[id=' + CurrentId + ']:enabled:visible:first').focus().select(); }, 100);
            timerObj.datepicker = $timeout(function () {
                // $('#' + CurrentId).datepicker('show');
                $scope.fields["DatePart_Calander_Open" + model] = true;
            }, 100);
        }

        $scope.fired = false;
        $scope.changedate = function () {
            if ($scope.fired == false) {
                $scope.fired = true;
                timerObj.dueecpiredate = $timeout(function () {
                    if ($scope.DateActionObject.dateExpirevalue != null) {

                        var test = isValidDate($('#ExpireDatepicker').val().toString(), $scope.DefaultSettings.DateFormat.toString());

                        if (test) {

                            if (dateDiffBetweenDates($scope.DateActionObject.dateExpirevalue) >= 0) {
                                $scope.SaveAttributeDate('ExpireDatedue');

                                $scope.DateActionObject.editable = '';
                                $scope.fired = false;
                                $('#ExpireDatepicker').attr('AlredyDateSelected', 'false');

                                $('#ExpireDatepicker').val('');
                            }
                            else {
                                bootbox.alert($translate.instant('LanguageContents.Res_1143.Caption'));
                                $scope.DateActionObject.dateExpirevalue = "-";
                                $scope.fired = false;
                            }
                            // $('#ExpireDatepicker').datepicker("hide");
                            $scope.fields["DatePart_Calander_Open" + model] = false;

                        }
                        else {
                            $scope.DateActionObject.dateExpirevalue = new Date.create();
                            $scope.DateActionObject.dateExpirevalue = null;
                            $('#ExpireDatepicker').val('');
                            $scope.fired = false;
                            $scope.saveEmptyDueDate('ExpireDatedue');
                            $scope.DateActionObject.editable = '';
                            $('#ExpireDatepicker').attr('AlredyDateSelected', 'false');
                            $('#ExpireDatepicker').val('');
                            $scope.DateActionObject.dateExpirevalue = "-";
                            $scope.fired = false;
                            // $('#ExpireDatepicker').datepicker("hide");
                            $scope.fields["DatePart_Calander_Open" + model] = false;

                        }

                    }
                    else {

                        $scope.DateActionObject.editable = '';
                        $('#ExpireDatepicker').attr('AlredyDateSelected', 'false');
                        $('#ExpireDatepicker').val('');
                        $scope.DateActionObject.dateExpirevalue = "-";
                        $scope.fired = false;
                        //  $('#ExpireDatepicker').datepicker("hide");
                        $scope.fields["DatePart_Calander_Open" + model] = false;
                    }
                }, 100);
            }
        }

        // Validates that the input string is a valid date 
        function isValidDate(dateval, dateformat) {
            var formatlen;
            var defaultdateVal = [];
            defaultdateVal = dateval.length;
            formatlen = dateformat.length;
            if (formatlen == defaultdateVal || defaultdateVal > formatlen)
                return true;
            else
                return false;
        };

        function dateDiffBetweenDates(dateTo) {

            var _MS_PER_DAY = 1000 * 60 * 60 * 24;
            var dateToday = new Date.create();

            var utcDateSet = Date.UTC(dateTo.getFullYear(), dateTo.getMonth(), dateTo.getDate());
            var utcToday = Date.UTC(dateToday.getFullYear(), dateToday.getMonth(), dateToday.getDate());

            return Math.floor((utcDateSet - utcToday) / _MS_PER_DAY);

        }

        $scope.SaveAttributeDate = function (type) {
            if (type == "ExpireDatedue") {
                if ($scope.DateActionObject.dateExpirevalue != "" && $scope.DateActionObject.dateExpirevalue != undefined && $scope.DateActionObject.dateExpirevalue != null) {
                    $scope.DateActionObject.dateExpirevalue = ConvertDateToString($scope.DateActionObject.dateExpirevalue, $scope.DefaultSettings.DateFormat);
                    $scope.DateActionObject.dateExpirevalue = ConvertDateFromStringToString(ConvertDateToString($scope.DateActionObject.dateExpirevalue));
                    //var tempduedate1 = ConvertStringToDateByFormat($scope.dateExpirevalue1, $scope.DefaultSettings.DateFormat);
                    //if (ConvertDateToString(tempduedate1) == ConvertDateToString($scope.DateActionObject.dateExpirevalue, $scope.DefaultSettings.DateFormat)) {
                    //    $scope.updatetaskEdit = false;
                    //}
                    //else { $scope.updatetaskEdit = true; }
                } else {
                    $scope.DateActionObject.dateExpirevalue = '';
                }


                $scope.UpdateExpireActionDate = [];
                $scope.UpdateExpireActionDate.push({
                    "SourceID": $scope.SourceID, "DateActionexpiredate": $scope.DateActionObject.dateExpirevalue, "ActionType": -1, "Actionexutedays": "-1", "SourcetypeID": $scope.SourcetypeID, "SourceAttributeID": $scope.SourceAttributeID, "SourceFrom": $scope.SourceFrom, "ProductionEntityID": $scope.ProductionEntityID
                });
                ExpireActionService.UpdateExpireActionDate($scope.UpdateExpireActionDate).then(function (result) {

                    if (result.Response == false) {
                        NotifyError($translate.instant('LanguageContents.Res_1144.Caption'));
                        $("#expirepopup").modal("hide");
                        $scope.SaveExpireAction = [];

                    }
                    else {
                        $scope.SaveExpireAction = [];
                        NotifySuccess($translate.instant('LanguageContents.Res_4782.Caption'));
                        if ($scope.SourceFrom == 1) {
                            timerObj.saveexpireacion = $timeout(function () {
                                $scope.$emit('callBackExpireactionAssetEditvalues', $scope.SourceID, $scope.ProductionEntityID, $scope.SourceAttributeID, $scope.DateActionObject.dateExpirevalue)
                            }, 100);
                        } else if ($scope.SourceFrom == 2) {
                            timerObj.saveexpireacion = $timeout(function () {
                                $scope.$emit('callBackExpireactionEntityEditvalues', $scope.SourceID, $scope.ProductionEntityID, $scope.SourceAttributeID, $scope.DateActionObject.dateExpirevalue)
                            }, 100);
                        }
                        else if ($scope.SourceFrom == 3) {
                            timerObj.saveexpireacion = $timeout(function () {
                                $scope.$emit('callBackExpireactiontaskEditvalues', $scope.SourceID, $scope.ProductionEntityID, $scope.SourceAttributeID, $scope.DateActionObject.dateExpirevalue)
                            }, 100);
                        }
                        else if ($scope.SourceFrom == 4) {
                            timerObj.saveexpireacion = $timeout(function () {
                                $scope.$emit('callBackObjectiveExpireactionEntityEditvalues', $scope.SourceID, $scope.ProductionEntityID, $scope.SourceAttributeID, $scope.DateActionObject.dateExpirevalue)
                            }, 100);
                        }
                        timerObj.saveexpireacion = $timeout(function () {
                            //$scope.$emit('CallExpireaction', $scope.DateActionObject.dateExpirevalue, $scope.SourceID, $scope.SourceEnityID, $scope.SourceFrom, $scope.SourcetypeID, $scope.ProductionEntityID, $scope.SourceAttributeID, $scope.SourceAttributename);
                            OpenExpireactionpopup($scope.DateActionObject.dateExpirevalue, $scope.SourceID, $scope.SourceEnityID, $scope.SourceFrom, $scope.SourcetypeID, $scope.ProductionEntityID, $scope.SourceAttributeID, $scope.SourceAttributename);
                        }, 100);

                        //$("#expirepopup").modal("hide");

                    }
                });
            }


        }
        $scope.ValidExpireActionDates = function (Actionexutedays, ActionType, DueActionexutedays) {
            $scope.savevalidation.ValidActionday = false;
            if (ActionType == 1)
                $('#ExpireUnpublishbtn').removeClass('disabled');
            if (ActionType == 2)
                $('#ExpireCreateNewTaskBtn').removeClass('disabled');

            if (ActionType == 3)
                $('#ExpireUMBtn').removeClass('disabled');
            if (Actionexutedays != DueActionexutedays) {
                $scope.ValidExpireActionDate = [];
                $scope.ValidExpireActionDate.push({
                    "SourceID": $scope.SourceID, "DateActionexpiredate": $scope.DateActionObject.dateExpirevalue, "ActionType": ActionType, "Actionexutedays": Actionexutedays, "SourcetypeID": $scope.SourcetypeID, "SourceAttributeID": $scope.SourceAttributeID, "SourceFrom": $scope.SourceFrom, "ProductionEntityID": $scope.ProductionEntityID, "DueActionexutedays": DueActionexutedays
                });
                ExpireActionService.ValidExpireActionDate($scope.ValidExpireActionDate)
                   .then(function (result) {

                       if (result.Response == false) {
                           //NotifyError($translate.instant('LanguageContents.Res_1144.Caption'));
                           $scope.savevalidation.ValidActionday = false;
                           if (ActionType == 1) {
                               bootbox.alert($translate.instant('LanguageContents.Res_1140.Caption'));
                               $('#ExpireUnpublishbtn').addClass('disabled');
                               return false;
                           }
                           else if (ActionType == 2) {
                               bootbox.alert($translate.instant('LanguageContents.Res_1141.Caption'));
                               $('#ExpireCreateNewTaskBtn').addClass('disabled');
                               return false;
                           }
                           else if (ActionType == 3) {
                               bootbox.alert($translate.instant('LanguageContents.Res_1140.Caption'));
                               $('#ExpireUMBtn').addClass('disabled');
                               return false;
                           }
                           //$("#expirepopup").modal("hide");
                           $scope.SaveExpireAction = [];

                       }
                       else {
                           $scope.SaveExpireAction = [];
                           //NotifySuccess("success");
                           $scope.savevalidation.ValidActionday = true;
                           return true;

                       }
                   });
            }
            else {
                $scope.savevalidation.ValidActionday = true;
                return true;

            }
        }

        $scope.saveupdateactions = function (ActionType, ExecutionDate, ActionsourceId) {
            if ($('#ExpireUnpublishbtn').hasClass('disabled')) { return; }
            $('#ExpireUnpublishbtn').addClass('disabled');

            $scope.ValidExpireActionDate = [];
            $scope.ValidExpireActionDate.push({
                "SourceID": $scope.SourceID, "DateActionexpiredate": $scope.DateActionObject.dateExpirevalue, "ActionType": 1, "Actionexutedays": ExecutionDate, "SourcetypeID": $scope.SourcetypeID, "SourceAttributeID": $scope.SourceAttributeID, "SourceFrom": $scope.SourceFrom, "ProductionEntityID": $scope.ProductionEntityID, "DueActionexutedays": "-1"
            });
            ExpireActionService.ValidExpireActionDate($scope.ValidExpireActionDate)
               .then(function (result) {

                   if (result.Response == false) {
                       //NotifyError($translate.instant('LanguageContents.Res_1144.Caption'));
                       $scope.savevalidation.ValidActionday = false;
                       if (ActionType == 1) {
                           bootbox.alert($translate.instant('LanguageContents.Res_1140.Caption'));
                           //$('#ExpireUnpublishbtn').addClass('disabled');
                           $('#ExpireUnpublishbtn').removeClass('disabled');
                           return false;
                       }
                   } else {
                       $scope.SaveExpireAction = [];
                       //NotifySuccess("success");
                       $scope.savevalidation.ValidActionday = true;
                       if ($scope.SourceID != undefined && $scope.DateActionObject.dateExpirevalue != "-") {
                           $scope.processaction = ActionType;
                           $scope.SaveExpireAction = [];
                           $scope.SaveExpireAction = {
                               "ActionType": $scope.processaction, "SourceID": $scope.SourceID, "SourceEnityID": $scope.SourceEnityID, "SourceFrom": $scope.SourceFrom, "Actionexutedays": ExecutionDate,
                               "DateActionexpiredate": $scope.DateActionObject.dateExpirevalue, "ispublish": $scope.publishSelection, "ActionsourceId": ActionsourceId, "AttributeData": $scope.AttributeData, "ActionSrcValue": { "Ispublish": $scope.publishSelection, "ProductionEntityID": $scope.ProductionEntityID }, "AttributeID": $scope.SourceAttributeID
                           };
                           var SaveExpireAction = { SaveExpireAction: $scope.SaveExpireAction };
                           ExpireActionService.CreateExpireAction(SaveExpireAction)
                           .then(
                           function (result) {

                               if (result.Response == 0) {
                                   NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
                                   $("#expirepopup").modal("hide");
                                   $scope.SaveExpireAction = [];
                                   $('#ExpireUnpublishbtn').removeClass('disabled');
                               }
                               else {
                                   $scope.SaveExpireAction = [];
                                   NotifySuccess($translate.instant('LanguageContents.Res_4782.Caption'));
                                   $('#ExpireUnpublishbtn').removeClass('disabled');
                                   timerObj.callexpireacion = $timeout(function () {
                                       //$scope.$emit('CallExpireaction', $scope.DateActionObject.dateExpirevalue, $scope.SourceID, $scope.SourceEnityID, $scope.SourceFrom, $scope.SourcetypeID, $scope.ProductionEntityID, $scope.SourceAttributeID, $scope.SourceAttributename);
                                       OpenExpireactionpopup($scope.DateActionObject.dateExpirevalue, $scope.SourceID, $scope.SourceEnityID, $scope.SourceFrom, $scope.SourcetypeID, $scope.ProductionEntityID, $scope.SourceAttributeID, $scope.SourceAttributename);
                                   }, 100);
                                   //$("#expirepopup").modal("hide");

                               }
                           });
                       }
                       else {
                           bootbox.alert($translate.instant('LanguageContents.Res_1142.Caption'));
                           $('#ExpireUnpublishbtn').removeClass('disabled');
                           return false;

                       }


                   }
               });
        }

        $scope.DeleteExpireAction = function (ActionID) {

            if (ActionID > 0) {
                ExpireActionService.DeleteExpireAction(ActionID)
                 .then(
                function (result) {

                    if (result.Response == false) {
                        NotifyError($translate.instant('LanguageContents.Res_4331.Caption'));
                        $("#expirepopup").modal("hide");

                    }
                    else {

                        NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                        timerObj.callexpireacion = $timeout(function () {
                            //$scope.$emit('CallExpireaction', $scope.DateActionObject.dateExpirevalue, $scope.SourceID, $scope.SourceEnityID, $scope.SourceFrom, $scope.SourcetypeID, $scope.ProductionEntityID, $scope.SourceAttributeID, $scope.SourceAttributename);
                            OpenExpireactionpopup($scope.DateActionObject.dateExpirevalue, $scope.SourceID, $scope.SourceEnityID, $scope.SourceFrom, $scope.SourcetypeID, $scope.ProductionEntityID, $scope.SourceAttributeID, $scope.SourceAttributename);
                        }, 100);
                        //$("#expirepopup").modal("hide");

                    }
                });
            }
        }
        $scope.Actionopen = function (type, item) {

            if (type == "UPActionopen") {
                $scope.showUnpublishpopup = true;
                $scope.ngShowUnpublishpopupbodyopen = false;
                $scope.ngShowUnpublishpopupbodyclose = true;
                $scope.showUnpublishdayoption = true;
                if ($scope.ShowCreateTaskpopup == true) {
                    $scope.showCTiconopen = true;
                    $scope.showCTiconclose = false;
                    $scope.showCTdayoption = false;
                    if ($scope.CreateTaskactionResData != undefined) {
                        var taskActionSrc = $.grep($scope.CreateTaskactionResData, function (e) { return e.Id !== 0; });
                        for (var k = 0, data; data = taskActionSrc[k++];) {
                            data["IsExpaned"] = false;
                        }
                    }
                }
                if ($scope.ShowUpdateMetadatapopup == true) {
                    $scope.showUMiconopen = true;
                    $scope.showUMiconclose = false;
                    $scope.showUMdayoption = false;
                }

            }
            else if (type == "UPActionclose") {
                $scope.ngShowUnpublishpopupbodyopen = true;
                $scope.ngShowUnpublishpopupbodyclose = false;
                $scope.showUnpublishdayoption = false;
            }
            else if (type == "CTActionopen") {

                if (item != "" && item.Id > 0) {
                    $scope.CTActionId = item.Id;
                    item.IsExpaned = !item.IsExpaned;
                    var taskActionSrc = $.grep($scope.CreateTaskactionResData, function (e) { return e.Id !== $scope.CTActionId; });
                    for (var k = 0, data; data = taskActionSrc[k++];) {
                        data["IsExpaned"] = false;
                    }
                    $scope.retriveTaskValuestoBind($scope.CTActionId);
                }
                else {
                    $scope.CTActionId == 0
                    $scope.CTsavecapation = "Save";
                    var taskActionSrcnew = $.grep($scope.CreateTaskactionResData, function (e) { return e.Id == 0; });
                    if (taskActionSrcnew.length == 0) {
                        $scope.CreateTaskactionResData.push({ ActionPlace: $scope.SourceFrom, ActionSrcID: $scope.SourceID, ActionSrcValue: "", ActionType: 2, ActionownerId: 0, AttributeID: $scope.SourceAttributeID, EntityID: $scope.ProductionEntityID, ExecutionDate: "", Id: 0, IsChanged: false, IsDeleted: false, IsExpaned: true, Status: 0, TimeDuration: 1, UpdatedOn: "" });
                    }

                    var taskActionSrc = $.grep($scope.CreateTaskactionResData, function (e) { return e.Id !== 0; });
                    for (var k = 0, data; data = taskActionSrc[k++];) {
                        data["IsExpaned"] = false;
                    }
                    //$scope.retriveTaskValuestoBind($scope.CTActionId);
                }
                $scope.ShowCreateTaskpopup = true;
                $scope.showCTiconopen = false;
                $scope.showCTiconclose = true;
                $scope.showCTdayoption = true;
                if ($scope.CTActionId == 0) {
                    RefreshAddNewTaskScopes();
                    getenitymenber();
                }
                if ($scope.showUnpublishpopup == true) {
                    $scope.ngShowUnpublishpopupbodyopen = true;
                    $scope.ngShowUnpublishpopupbodyclose = false;
                    $scope.showUnpublishdayoption = false;
                }
                if ($scope.ShowUpdateMetadatapopup == true) {
                    $scope.showUMiconopen = true;
                    $scope.showUMiconclose = false;
                    $scope.showUMdayoption = false;
                }
            }
            else if (type == "CTActionclose") {
                $scope.showCTiconopen = true;
                $scope.showCTiconclose = false;
                $scope.showCTdayoption = false;
                for (var k = 0, data; data = $scope.CreateTaskactionResData[k++];) {
                    data["IsExpaned"] = false;
                }

            }
            else if (type == "UMActionopen") {
                $scope.ShowUpdateMetadatapopup = true;
                $scope.showUMiconopen = false;
                $scope.showUMiconclose = true;
                $scope.showUMdayoption = true;
                //LoadCustomMetadataPart()
                if ($scope.showUnpublishpopup == true) {
                    $scope.ngShowUnpublishpopupbodyopen = true;
                    $scope.ngShowUnpublishpopupbodyclose = false;
                    $scope.showUnpublishdayoption = false;
                }
                if ($scope.ShowCreateTaskpopup == true) {
                    $scope.showCTiconopen = true;
                    $scope.showCTiconclose = false;
                    $scope.showCTdayoption = false;
                    if ($scope.CreateTaskactionResData != undefined) {
                        var taskActionSrc = $.grep($scope.CreateTaskactionResData, function (e) { return e.Id !== 0; });
                        for (var k = 0, data; data = taskActionSrc[k++];) {
                            data["IsExpaned"] = false;
                        }
                    }
                }

            }
            else if (type == "UMActionclose") {
                $scope.showUMiconopen = true;
                $scope.showUMiconclose = false;
                $scope.showUMdayoption = false;
            }
        }

        //-------------------------------Task------
        //Getting Task Types from Enum
        $scope.TaskTypeList = [];
        function GetTaskType() {
            //var GetTaskTypeListServ = $resource('metadata/GetTaskTypes', { get: { method: 'GET' } });
            //var TaskTypeData = GetTaskTypeListServ.get(function () {
            ExpireActionService.GetTaskTypes().then(function (TaskTypeData) {
                $scope.TaskTypeList = TaskTypeData.Response;

            });
        }


        $scope.EntityMemberList = [];
        //$scope.EntityMemberDistinctRoleList = [];
        $scope.EntityDistinctMembers = [];

        function getenitymenber() {

            ExpireActionService.GetMember($scope.SourceEnityID).then(function (EntityMemberList) {

                $scope.EntityMemberList = EntityMemberList.Response;

                $scope.EntityDistinctMembers = [];
                var dupes = {};
                if ($scope.EntityMemberList != null) {
                    for (var i = 0, el; el = $scope.EntityMemberList[i++];) {
                        if (el.IsInherited != true) {
                            if (!dupes[el.Userid]) {
                                dupes[el.Userid] = true;
                                $scope.EntityDistinctMembers.push({ "Userid": el.Userid, "UserName": el.UserName });
                                $scope.tagAllOptions.data.push({
                                    "id": el.Userid,
                                    "text": el.UserName,
                                    "UserName": el.UserName,
                                    "UserEmail": el.UserEmail,
                                });
                                //var result = $.map($scope.EntityMemberList, function (rel) {
                                //    if (rel.Userid == el.Userid) {
                                //        return rel.ExpireRoleID;
                                //    }
                                //}).join(', ');

                                //var exit = $.grep($scope.EntityMemberDistinctRoleList, function (e) { return e.userId == el.Userid; });
                                //if (exit.length == 0)
                                //    $scope.EntityMemberDistinctRoleList.push({ "userId": el.Userid, "userExpireRoleid": result });
                            }
                        }
                    }

                }

                timerObj.loadmemberroles = $timeout(function () {
                    LoadMemberRoles();
                }, 300);

            });
        }

        $scope.AddCheckList = function (Index) {

            if ($scope.AdminTaskCheckList[Index].Name == undefined || $scope.AdminTaskCheckList[Index].Name.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4589.Caption'));
                return false;
            }

            if ($scope.SelectedTaskID != 0)
                $scope.AdminTaskCheckList.push({ Id: 0, Name: "", SortOrder: 0, Status: false, UserName: "", UserId: 0, CompletedOnValue: "", OwnerId: 0, OwnerName: "", CompletedOn: "" });
            else
                $scope.AdminTaskCheckList.push({ ID: 0, Name: "" });

        }



        function RefreshAddNewTaskScopes() {
            $scope.TaskcommonObjects = { "TaskType": 0, "NewTaskName": "", "TaskDescription": "", "TaskNote": "", "CTExecutionDate": 1, "CTdueExecutionDate": 1, "TaskMembersList": "", "CTActionexutedays": "" };
            $scope.ngShowTaskCreation = true;
            $scope.DisableIsAdminTask = false;
            $scope.TaskActionCaption = "Add task";
            $scope.TaskMembersList = "";
            $scope.NewTaskName = "";
            $scope.TaskType = 0;
            $scope.TaskDueDate = new Date.create();
            $scope.TaskDueDate = null;
            $scope.TaskDescription = "";
            $scope.TaskNote = "";
            $scope.SelectedTaskID = 0;
            $scope.dyn_Cont = '';
            $scope.dyn_ContAsset = '';
            $scope.AdminTaskCheckList = [{ ID: 0, Name: "" }];
            $("#taskDynamicTaskControls").html("");
            $scope.treeNodeSelectedHolder = [];
            $scope.treesrcdirec = {};
            $scope.treePreviewObj = {};
            $('#assetlistActivityTask').empty();
            //$("#totalAssetActivityAttachProgresstask").empty();
            $("#taskentityattachassetdata").empty();
            $scope.addfromGenAttachments = false;
            $scope.addfromComputer = false;
            // $("#addTask").modal('show');
            refreshModel();
            timerObj.addtaskcaption = $timeout(function () { $('#addtaskcaption').focus(); }, 1000);
        }

        $scope.RemoveCheckList = function (Index, ID) {
            if ($scope.AdminTaskCheckList.length > 1) {
                bootbox.confirm($translate.instant('LanguageContents.Res_2012.Caption'), function (result) {
                    if (result) {
                        timerObj.removechecklist = $timeout(function () {
                            if (ID > 0) {
                                ExpireActionService.DeleteEntityCheckListByID(ID).then(function (result) {
                                    if (result.StatusCode == 200) {
                                        $scope.AdminTaskCheckList.splice(Index, 1);
                                    }

                                });
                            } else {
                                $scope.AdminTaskCheckList.splice(Index, 1);
                            }
                        }, 100);
                    }
                });
            }
            else {
                bootbox.confirm($translate.instant('LanguageContents.Res_2012.Caption'), function (result) {
                    if (result) {
                        timerObj.deleteentitychecklist = $timeout(function () {
                            if (ID > 0) {
                                //var DeleteChkList = $resource("task/DeleteEntityCheckListByID/:ID", { ID: ID });
                                //var result = DeleteChkList.delete(function () {
                                ExpireActionService.DeleteEntityCheckListByID(ID).then(function (result) {
                                    if (result.StatusCode == 200) {
                                        $scope.AdminTaskCheckList[Index].Name = "";
                                    }

                                });
                            } else {
                                $scope.AdminTaskCheckList[Index].Name = "";
                            }
                        }, 100);
                    }
                });
            }
            timerObj.approvaltaskfeed = $timeout(function () { feedforApprovalTask($scope.SelectedTaskID, $scope.divid, true); }, 5000);
        }

        $scope.fields = {
            usersID: ''
        };
        $scope.assetfields = {
            usersID: ''
        };
        $scope.EnableDisableControlsHolder = {};
        $scope.atributesRelationList = {};
        $scope.optionsLists = [];
        $scope.EnableDisableControlsHolder = {};
        $scope.Inherritingtreelevels = {};
        $scope.InheritingLevelsitems = [];
        $scope.treeTexts = {};
        $scope.treeSources = {};
        $scope.treeSourcesObj = [];
        $scope.UploadAttributeData = [];
        $scope.OptionObj = {

        };
        $scope.PercentageVisibleSettings = {};
        $scope.DropDownTreePricing = {};
        $scope.listAttributeValidationResult = [];
        $scope.listAttriToAttriResult = [];
        $scope.items = [];
        $scope.treeSources = {};
        $scope.treelevels = {};
        $scope.DropDownTreeOptionValues = {};
        $scope.NormalDropdownCaption = {};
        $scope.uploader = {};
        $scope.UploaderCaption = {};
        $scope.NormalMultiDropdownCaption = {};
        $scope.treeTexts = {};
        $scope.treeSelection = [];
        $scope.normaltreeSources = {};

        //Tree multiselection part STARTED

        $scope.treeNodeSelectedHolder = new Array();

        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            var _ref;
            $scope.output = "You selected: " + branch.Caption;


            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }

            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }
                for (var i = 0, parent; parent = parentArr[i++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == parent.AttributeId && e.id == parent.id; });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolder.push(parent);
                    }
                }
            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);
                    if (branch.Children.length > 0) {
                        RemoveRecursiveChildTreenode(branch.Children);
                    }
                }
            }

            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                }
                else
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }


        };

        $scope.renderHtml = function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };

        var treeTextVisbileflag = false;
        function IsNotEmptyTree(treeObj) {

            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                }
                else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }


        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == child.AttributeId && e.id == child.id; });
                if (remainRecord.length > 0) {

                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }

            }
        }

        $scope.treesrcdirec = {};

        $scope.my_tree = tree = {};

        $scope.ChecklistVisible = false;
        //Tree  multiselection part ended



        $scope.createdynamicControls = function (rootID, retrive) {


            if (rootID != "" && rootID != undefined) {
                var Tasktype_value = $.grep($scope.TaskTypeList, function (rel) {
                    return rel.Id == $scope.TaskcommonObjects.TaskType;
                })[0];

                if (Tasktype_value.TaskTypeId == 2) {
                    $scope.ChecklistVisible = true;
                    if (retrive == 0) {
                        $scope.AdminTaskCheckList = [{ ID: 0, Name: "" }];
                    }
                    else if (retrive == 1) {
                        var taskResObj = $.grep($scope.AdminTaskCheckList, function (e) { return e.Name != ""; });
                        if (taskResObj.length == 0)
                            $scope.AdminTaskCheckList = [{ ID: 0, Name: "" }];
                    }
                }
                else {
                    $scope.ChecklistVisible = false;
                    if (retrive == 0) {
                        $scope.AdminTaskCheckList = [{ ID: 0, Name: "" }];
                    }
                    else if (retrive == 1) {
                        var taskResObj = $.grep($scope.AdminTaskCheckList, function (e) { return e.Name != ""; });
                        if (taskResObj.length == 0)
                            $scope.AdminTaskCheckList = [{ ID: 0, Name: "" }];
                    }
                }
            }
            else {
                $scope.AdminTaskCheckList = [{ ID: 0, Name: "" }];
                $scope.ChecklistVisible = false;
            }

            $("#taskDynamicTaskControls").html("");
            $("#taskDynamicTaskControls").append(
                             $compile("")($scope));
            $scope.treeNodeSelectedHolder = [];
            $scope.listAttriToAttriResult = [];
            $scope.treesrcdirec = {};
            $scope.treePreviewObj = {};
            $scope.PercentageVisibleSettings = {};
            $scope.DropDownTreePricing = {};
            $scope.ClearModelObject($scope.fields);
            $scope.dyn_Cont = '';
            $scope.items = [];
            var perioddates = [];
            //var resourceEntityTypeAttributeRelationByID = $resource('metadata/GetEntityTypeAttributeRelationWithLevelsByID/:ID/:ParentID', { ID: rootID, ParentID: 0 }, { get: { method: 'GET' } });
            //var entityAttributesRelation = resourceEntityTypeAttributeRelationByID.get({ ID: rootID }, function () {
            if (rootID != "" && rootID != undefined) {
                ExpireActionService.GetEntityTypeAttributeRelationWithLevelsByID(rootID, 0).then(function (entityAttributesRelation) {
                    $scope.atributesRelationList = entityAttributesRelation.Response;
                    for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                        if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                            if ($scope.atributesRelationList[i].AttributeID != 70) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                if ($scope.atributesRelationList[i].InheritFromParent)
                                    $scope.atributesRelationList[i].DefaultValue = $scope.atributesRelationList[i].ParentValue[0];

                                if ($scope.atributesRelationList[i].AttributeID !== SystemDefiendAttributes.Name) {

                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                                    $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                }

                                if ($scope.CTActionSourcedatas != null) {
                                    try {
                                        var val = $.grep($scope.CTActionSourcedatas.ActionTaskSrcValue.AttributeData, function (rel) {
                                            return rel.AttributeID == $scope.atributesRelationList[i].AttributeID
                                        })[0];
                                        if (val != null) {
                                            $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = val.NodeID;
                                        }
                                    } catch (e) { }

                                }
                            }
                        }
                        else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            if ($scope.atributesRelationList[i].IsSpecial == true) {
                                if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                    $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"> <input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.OwnerList[0].UserName;

                                }
                            } else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.FiscalYear) {
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;

                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,3)\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                if ($scope.atributesRelationList[i].InheritFromParent)
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                                else
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;


                                if ($scope.CTActionSourcedatas != null) {
                                    try {
                                        var val = $.grep($scope.CTActionSourcedatas.ActionTaskSrcValue.AttributeData, function (rel) {
                                            return rel.AttributeID == $scope.atributesRelationList[i].AttributeID
                                        })[0];
                                        if (val != null) {
                                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = val.NodeID;
                                        }
                                    } catch (e) { }

                                }

                            }

                            else {
                                $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;

                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,3)\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"> <option value=\"\"> Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                                if ($scope.atributesRelationList[i].InheritFromParent)
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                                else
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;

                                if ($scope.CTActionSourcedatas != null) {
                                    try {
                                        var val = $.grep($scope.CTActionSourcedatas.ActionTaskSrcValue.AttributeData, function (rel) {
                                            return rel.AttributeID == $scope.atributesRelationList[i].AttributeID
                                        })[0];
                                        if (val != null) {
                                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = val.NodeID[0];
                                        }
                                    } catch (e) { }

                                }

                            }
                        }

                        else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                            var totLevelCnt = $scope.atributesRelationList[i].Levels.length;
                            for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                                if (j == 0) {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;

                                    if ($scope.CTActionSourcedatas != null) {
                                        try {
                                            var val = $.grep($scope.CTActionSourcedatas.ActionTaskSrcValue.AttributeData, function (rel) {
                                                return rel.AttributeID == $scope.atributesRelationList[i].AttributeID && rel.Level == (j + 1)
                                            })[0];
                                            if (val != null) {
                                                $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = $.grep($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data, function (res) {
                                                    return res.id == val.NodeID[0];
                                                })[0];
                                            }
                                        } catch (e) { }

                                    }

                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                    $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";

                                }
                                else {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                    if ($scope.CTActionSourcedatas != null) {
                                        $scope.ShowHideAttributeToAttributeRelations($scope.atributesRelationList[i].AttributeID, (j), totLevelCnt, 6);
                                    }

                                    if ($scope.CTActionSourcedatas != null) {
                                        try {
                                            var val = $.grep($scope.CTActionSourcedatas.ActionTaskSrcValue.AttributeData, function (rel) {
                                                return rel.AttributeID == $scope.atributesRelationList[i].AttributeID && rel.Level == (j + 1)
                                            })[0];
                                            if (val != null) {
                                                $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = $.grep($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data, function (res) {
                                                    return res.id == val.NodeID[0];
                                                })[0];
                                            }
                                        } catch (e) { }

                                    }
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                    $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                }
                            }

                        }
                        else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                            if (($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TakeDescription) && ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskNotes)) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea></div></div>";
                                if ($scope.atributesRelationList[i].InheritFromParent)
                                    $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue[0];
                                else
                                    $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                                if ($scope.CTActionSourcedatas != null) {
                                    try {
                                        var val = $.grep($scope.CTActionSourcedatas.ActionTaskSrcValue.AttributeData, function (rel) {
                                            return rel.AttributeID == $scope.atributesRelationList[i].AttributeID
                                        })[0];
                                        if (val != null) {
                                            $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = val.NodeID;
                                        }
                                    } catch (e) { }

                                }

                            }
                        } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;

                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select  class=\"multiselect\"   data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\"  multiselect-dropdown ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ",0,0,4)\"   ></select></div></div>";

                            if ($scope.atributesRelationList[i].InheritFromParent) {
                                var defaultmultiselectvalue1 = $scope.atributesRelationList[i].ParentValue.split(',');
                                $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = defaultmultiselectvalue1;
                            }
                            else {
                                var defaultmultiselectvalue = $scope.atributesRelationList[i].DefaultValue.split(',');
                                if ($scope.atributesRelationList[i].DefaultValue != "") {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = defaultmultiselectvalue;
                                }
                                else {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                                }
                            }
                            if ($scope.CTActionSourcedatas != null) {
                                try {
                                    var obj = $.grep($scope.CTActionSourcedatas.ActionTaskSrcValue.AttributeData, function (rel) {
                                        return rel.AttributeID == $scope.atributesRelationList[i].AttributeID;
                                    });
                                    if (obj != null) {
                                        var vals = $.map(obj, function (rel) {
                                            return parseInt(rel.NodeID);
                                        });
                                        $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = vals;
                                    }
                                } catch (e) { }

                            }

                        }
                        else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                            var ID = $scope.SourceEnityID;
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            //  $scope.setoptions();
                            $scope.items.push({
                                startDate: null,
                                endDate: null,
                                comment: '',
                                sortorder: 0,
                                calstartopen: false,
                                calendopen: false
                            });
                            $scope.dyn_Cont += "    <div class=\"control-group\"><label for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.atributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" id=\"periodcontrols\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                            $scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span5\">";
                            $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                            $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                            if ($scope.MinValue < 0) {
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                            } else {
                                $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                            }
                            if ($scope.MaxValue > 0 && $scope.MinValue < $scope.MaxValue) {
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                            } else if ($scope.MinValue < $scope.MaxValue) {
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + $scope.MaxValue);
                            } else {
                                $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                            }
                            var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                            if ($scope.CTActionSourcedatas != null) {
                                $scope.Periodval = $.grep($scope.CTActionSourcedatas.ActionTaskSrcValue.AttributeData, function (rel) {
                                    return rel.AttributeID == $scope.atributesRelationList[i].AttributeID
                                })[0];
                                if ($scope.Periodval != null) {
                                    $scope.items = [];
                                    for (var m = 0; m < $scope.Periodval["NodeID"].length; m++) {
                                        if ($scope.Periodval["NodeID"][m].startDate != null && $scope.Periodval["NodeID"][m].endDate != null) {
                                            if ($scope.Periodval["NodeID"][m].startDate.toString().length != 0 && $scope.Periodval["NodeID"][m].endDate.toString().length != 0) {
                                                $scope.items.push({
                                                    startDate: ($scope.Periodval["NodeID"][m].startDate.toString().length != 0 ? ($scope.Periodval["NodeID"][m].startDate) : ''),
                                                    endDate: ($scope.Periodval["NodeID"][m].endDate.toString().length != 0 ? ($scope.Periodval["NodeID"][m].endDate) : ''),
                                                    comment: ($scope.Periodval["NodeID"][m].comment.trim().length != 0 ? $scope.Periodval["NodeID"][m].comment : ''),
                                                    sortorder: 0
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                            $scope.dyn_Cont += "<input class=\"sdate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"PeriodCalanderopen($event,item,'start')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calstartopen\" min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"item.startDate\" type=\"text\" name=\"startDate\" ng-change=\"changeperioddate_changed(item.startDate,'StartDate')\" ng-model=\"item.startDate\" placeholder=\"-- Start date --\"/><input class=\"edate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-click=\"PeriodCalanderopen($event,item,'end')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calendopen\"  min-date=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max-date=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  name=\"enddate\" id=\"item.endDate\" ng-change=\"changeperioddate_changed(item.endDate,'EndDate')\" ng-model=\"item.endDate\" placeholder=\"-- End date --\"/><input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\"  ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
                            $scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
                            $scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
                            $scope.fields["Period_" + $scope.atributesRelationList[i].AttributeID] = "";
                            $scope.setFieldKeys();
                        }
                        else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                            $scope.fields["Tree_" + $scope.atributesRelationList[i].AttributeID] = [];
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                            if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                treeTextVisbileflag = false;
                                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID])) {
                                    $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = true;
                                }
                                else
                                    $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                            }
                            else {
                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                            }
                            $scope.dyn_Cont += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + $scope.atributesRelationList[i].AttributeID + '\" class="control-group treeNode-control-group">';
                            $scope.dyn_Cont += '<label class="control-label">' + $scope.atributesRelationList[i].AttributeCaption + '</label>';
                            $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                            $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '"></div>';
                            $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '">';
                            $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                            $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" accessable="' + $scope.atributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                            $scope.dyn_Cont += '</div></div></div>';
                            $scope.dyn_Cont += '<div class="control-group staticTreeGroup margin-top0x" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationList[i].AttributeID + '\">';
                            $scope.dyn_Cont += '<div class="controls">';
                            $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.atributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                            $scope.dyn_Cont += '</div></div>';
                        }
                        else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                            $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            if ($scope.CTActionSourcedatas != null) {
                                try {
                                    var val = $.map($scope.CTActionSourcedatas.ActionTaskSrcValue.AttributeData, function (rel) {
                                        return rel.AttributeID == $scope.atributesRelationList[i].AttributeID
                                    })[0];
                                    if (val != null) {
                                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = val.NodeID[0];
                                    }
                                } catch (e) { }

                            }
                        }
                        else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                            $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = true;
                            $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = $scope.atributesRelationList[i].DropDownPricing;
                            $scope.dyn_Cont += "<div drowdowntreepercentagemultiselection data-purpose='entity' data-attributeid=" + $scope.atributesRelationList[i].AttributeID.toString() + "></div>";


                        }

                        else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                            if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskDueDate) {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                                $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = false;
                                $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.DateTime_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event,'date_" + $scope.atributesRelationList[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"date_" + $scope.atributesRelationList[i].AttributeID + "\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                                var param1 = new Date.create();
                                var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                                $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                                if ($scope.CTActionSourcedatas != null) {
                                    try {
                                        var val = $.grep($scope.CTActionSourcedatas.ActionTaskSrcValue.AttributeData, function (rel) {
                                            return rel.AttributeID == $scope.atributesRelationList[i].AttributeID
                                        })[0];
                                        if (val != null) {
                                            $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = ConvertDateFromStringToString(ConvertDateToString(val.NodeID));
                                        }
                                    }
                                    catch (e) {

                                    }
                                }
                            }

                        }
                        else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                            var totLevelCnt1 = $scope.atributesRelationList[i].Levels.length;
                            for (var j = 0; j < totLevelCnt1; j++) {

                                if (totLevelCnt1 == 1) {

                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;

                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                    $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                    $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";

                                    if ($scope.CTActionSourcedatas != null) {
                                        try {
                                            var val = $.grep($scope.CTActionSourcedatas.ActionTaskSrcValue.AttributeData, function (rel) {
                                                return rel.AttributeID == $scope.atributesRelationList[i].AttributeID && rel.Level == (j + 1)
                                            })[0];
                                            if (val != null) {
                                                var vals = $.map(val, function (rel) {
                                                    return rel.NodeID
                                                });
                                                $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = vals;
                                            }
                                        } catch (e) { }

                                    }

                                }
                                else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                                    if (j == 0) {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;

                                        $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                        $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";


                                    } else {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];

                                        if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                            $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                        }
                                        else {
                                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                            $scope.dyn_Cont += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                            $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                        }

                                        try {
                                            if ($scope.CTActionSourcedatas != null) {
                                                var val = $.grep($scope.CTActionSourcedatas.ActionTaskSrcValue.AttributeData, function (rel) {
                                                    return rel.AttributeID == $scope.atributesRelationList[i].AttributeID && rel.Level == (j + 1)
                                                })[0];

                                                if (val != null) {
                                                    if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                                        var vals = $.map(val, function (rel) {
                                                            return rel.NodeID
                                                        });
                                                        $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = vals;
                                                    }
                                                    else {
                                                        $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = val.NodeID[0];
                                                    }

                                                }
                                            }
                                        } catch (e) { }
                                    }
                                }

                            }
                        }

                        if ($scope.atributesRelationList[i].IsReadOnly == true) {
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        }
                        else {
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        }
                    }

                    $scope.dyn_Cont += '<input style="display:none" type="submit" id="tskbtnTemp" class="ng-scope" invisible>';
                    $("#taskDynamicTaskControls").append(
                                   $compile($scope.dyn_Cont)($scope));
                    //setTimeout(function () { $('[id^=TextSingleLine]:enabled:visible:first').focus().select() }, 1000);
                    $("#TaskDynamicMetadata").scrollTop(0);
                    $scope.rootDisplayName = $scope.fields["TextSingleLine_" + SystemDefiendAttributes.Name];

                    //--------> HIDE ALL THE ATTRIBUTE TO ATTRIBUTE RELAIONS AFTER LOADING <--------------------
                    timerObj.hideattributerelation = $timeout(function () {
                        HideAttributeToAttributeRelationsOnPageLoad();
                    }, 200);
                    GetValidationList(rootID);
                    $("#TaskDynamicMetadata").addClass('notvalidate');
                });
            }
        }
        $scope.addNew = function () {
            var ItemCnt = $scope.items.length;
            if (ItemCnt > 0) {
                if ($scope.items[ItemCnt - 1].startDate == null || $scope.items[ItemCnt - 1].startDate.length == 0 || $scope.items[ItemCnt - 1].endDate.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1986.Caption'));
                    return false;
                }
                $scope.items.push({
                    startDate: null,
                    endDate: null,
                    comment: '',
                    sortorder: 0
                });
            }
        };
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.deleteOne = function (item) {
            $scope.lastSubmit.splice($.inArray(item, $scope.lastSubmit), 1);
            $scope.items.splice($.inArray(item, $scope.items), 1);
        };
        $scope.submitAll = function () {
            $scope.lastSubmit = angular.copy($scope.items);
        }
        $scope.AddDefaultEndDate = function (objdateval) {
            $("#TaskDynamicMetadata").addClass('notvalidate');
            if (objdateval.startDate == null) {
                objdateval.endDate = null
            }
            else {
                objdateval.endDate = new Date.create(objdateval.startDate);
                objdateval.endDate = objdateval.endDate.addDays(7);
            }
        };

        $scope.Roles = [];                          // Global member role scope
        $scope.globalEntityMemberLists = [];        // to hold the global entity task members from the system
        $scope.globalTempcount = 1;                 // temporary id count for global members
        $scope.AutoCompleteSelectedObj = [];        // auto complete member data holder

        // to load the entity roles for adding global members from the system
        function LoadMemberRoles() {


            ExpireActionService.GetEntityDetailsByID($scope.ProductionEntityID).then(function (GetEntityresult) {
                if (GetEntityresult.Response != null) {
                    ExpireActionService.GetEntityTypeExpireRoleAccess(GetEntityresult.Response.Typeid).then(function (role) {
                        $scope.Roles = role.Response;
                        if ($scope.Roles != null) {

                            for (var i = 0, el; el = $scope.Roles[i++];) {
                                $scope.EntityDistinctMembers.push({ "Userid": el.ExpireRoleID, "UserName": el.Caption });
                            }
                        }
                    });
                }

            });


        }


        $scope.DestinationMemberRoles1 = {};
        function GetDestinationEntityRoles(entityid) {
            ExpireActionService.GetDestinationEntityIdRoleAccess(entityid).then(function (EntityMemberList) {
                if (EntityMemberList.Response != null) {
                    $scope.DestinationMemberRoles1 = EntityMemberList.Response;
                }
            });
        }

        $scope.AddDefaultEndDate = function (enddate, startdate, currentindex) {
            var enddate1 = null;
            if (currentindex != 0) {
                enddate1 = $scope.items[currentindex - 1].endDate;
            }
            if (enddate1 != null && enddate1 >= startdate) {
                bootbox.alert($translate.instant('LanguageContents.Res_1987.Caption'));
                $scope.items[currentindex].startDate = null;
                $scope.items[currentindex].endDate = null;
            }
            else {
                $("#TaskDynamicMetadata").addClass('notvalidate');
                if (startdate == null) {
                    $scope.items[currentindex].endDate = null;
                }
                else {
                    $scope.items[currentindex].endDate = (new Date.create(startdate)).addDays(7);
                }
            }
        };

        $scope.CheckPreviousStartDate = function (enddate, startdate, currentindex) {
            var enddate1 = null;

            if (currentindex != 0 || currentindex == 0) {
                enddate1 = $scope.items[currentindex].endDate;
            }
            if (enddate1 != null && startdate >= enddate1) {
                bootbox.alert($translate.instant('LanguageContents.Res_4240.Caption'));
                $scope.items[currentindex].endDate = null;
            }
        };

        //------------> RECURSIVE FUNCTION TO HIDE ALL THE ATTRIBUTE RELATIONS FOR THE SELECTED ATTRIBUTE <--------------
        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToShow = [];

            //----------> CHECK THE ATTRIBUTE ON ATTRIBUTE ID AND LEVEL AND GET THE ATTRIBUTE INFO TO HIDE <--------------
            if (attrLevel > 0) {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            }
            else {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }

            if (attributesToShow[0] != undefined) {
                for (var i = 0; i < attributesToShow[0].length; i++) {
                    var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                //----------> HIDE THE ATTRIBUTE AND CLEAR THE SCOPE <----------
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                //----------> FIND FOR NEXT ATTRIBUTE IF THE ATTRIBUTE RELATIONS EXISTS <-------------
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                //---------> IF THE ATTRIBUTE HAS RELATION WITH OTHER ATTRIBUTE START RECURSIVE AGAIN
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            }
                            else {
                                //----------> HIDE THE ATTRIBUTE AND CLEAR THE SCOPE <----------
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";

                                //----------> FIND FOR NEXT ATTRIBUTE IF THE ATTRIBUTE RELATIONS EXISTS <-------------
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));

                                //---------> IF THE ATTRIBUTE HAS RELATION WITH OTHER ATTRIBUTE START RECURSIVE AGAIN
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //--------------------> SHOW OR HIDE ATTRIBUTES FOR DROPDOWN ON SINGLE SELECTION <-----------------------
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToShow = [];
                var hideAttributeOtherThanSelected = [];
                //---------> 
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    //-----------> CLEAR THE SUB LEVEL ON SELECTING THE PARENT LEVEL
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        try {
                            if ($scope.Dropdown["OptionValues" + attrID + "_" + j] != undefined && $scope.Dropdown["OptionValues" + attrID + "_" + j] != "")
                                $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        } catch (e) { }
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        }
                        else if (attrType == 12) {
                            if (j == levelcnt)
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }

                    //-----------------> LOAD THE SUB LEVELS ON SELECTING PARENT LEVEL <----------------
                    if (attrType == 6) {
                        if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                    else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
                //----------------------------
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    }
                    catch (e) { }
                }
                //--------------> IF THERE IS NO ATTRIBUTE TO ATTRIBUTE RELATATIONS THEN RETURN BACK <--------------------------
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }

                //------------> RECURSIVE FUNCTION TO HIDE ATTRIBUTE ON RELATIONS
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);

                //-----> IF SINGLE SELECTION DROPDOWN SELECTED
                if (attrType == 3) {
                    if ($scope.fields['ListSingleSelection_' + attrID] != null) {
                        optionValue = parseInt(($scope.fields['ListSingleSelection_' + attrID]), 10);
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                }
                    //-----> IF MULTI SELECTION DROPDOWN SELECTED
                else if (attrType == 4) {
                    if ($scope.fields['ListMultiSelection_' + attrID] != null) {
                        optionValue = $scope.fields['ListMultiSelection_' + attrID];
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                    //-----> IF DROPDOWN TREE SELECTED
                else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                    //-----> IF TREE SELECTED
                else if (attrType == 7) {
                    if ($scope.fields['Tree_' + attrID] != null) {
                        optionValue = $scope.fields['Tree_' + attrID];
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }

                //----------> IF OPTION AVAILABLE FOR THE ATTRIBUTE SHOW THE ATTRIBUTE
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                //----------------> CHECK IF THE SELECTED ATTRIBUTE IS SINGLE SELECTION DROPDOWN OR DROPDOWN TREE <--------------------
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                }
                                else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (e) { }
        }

        //--------> HIDE ALL THE ATTRIBUTE RELAIONS AFTER LOADING <--------------------
        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                }
                                else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (e) { }
        }


        //Validation part
        function GetValidationList(rootID) {
            //var ValidationAttribute = $resource('metadata/GetValidationDationByEntitytype/:EntityTypeID', { EntityTypeID: rootID });
            //var GetValidationresult = ValidationAttribute.get(function () {
            ExpireActionService.GetValidationDationByEntitytype(rootID).then(function (GetValidationresult) {
                if (GetValidationresult.Response != null) {
                    $scope.listValidationResult = GetValidationresult.Response;
                    if ($scope.listAttriToAttriResult != null) {
                        for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                            var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                            if (attrRelIDs != undefined) {
                                for (var j = 0; j < attrRelIDs.length; j++) {
                                    if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                        });
                                    }
                                    else {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                        });
                                    }
                                }
                            }
                        }
                        $("#TaskDynamicMetadata").nod($scope.listValidationResult, {
                            'delay': 200,
                            'submitBtnSelector': '#tskbtnTemp',
                            'silentSubmit': 'true'
                        });
                    }


                }
            });
        }

        var treeTextVisbileflag = false;
        function IsNotEmptyTree(treeObj) {

            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                }
                else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        function GetTreeCheckedNodes(treeobj, attrID) {
            for (var i = 0, node; node = treeobj[i++];) {
                if (node.ischecked == true) {
                    $scope.fields["Tree_" + attrID].push(node.id);
                }
                if (node.Children.length > 0)
                    GetTreeCheckedNodes(node.Children, attrID);
            }
        }

        //Tree multiselection part STARTED

        $scope.treeNodeSelectedHolder = [];


        $scope.treesrcdirec = {};

        $scope.my_tree = tree = {};

        //Tree  multiselection part ended
        $scope.LoadDropDownChildLevels = function (attrID, attributeLevel, levelcnt, attrType) {
            if (levelcnt > 0) {
                var currntlevel = attributeLevel + 1;

                //-----------> CLEAR THE SUB LEVEL ON SELECTING THE PARENT LEVEL
                for (var j = currntlevel; j <= levelcnt; j++) {
                    $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);

                    if (attrType == 6) {
                        $scope.fields["DropDown_" + attrID + "_" + j] = "";
                    }
                    else if (attrType == 12) {
                        if (j == levelcnt)
                            $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                        else
                            $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                    }
                }

                //-----------------> LOAD THE SUB LEVELS ON SELECTING PARENT LEVEL <----------------

                if (attrType == 6) {
                    if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                            $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        });
                    }
                }
                else if (attrType == 12) {
                    if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                        $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                            $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                        });
                    }
                }
            }
        }


        //--------------------> SHOW OR HIDE ATTRIBUTES FOR DROPDOWN ON PAGE LOAD <-----------------------
        $scope.ShowHideAttributeToAttributeRelationsOnpageLoad = function (attrID, attributeLevel, attrVal, attrType) {
            try {
                var optionValue = attrVal;
                var attributesToShow = [];

                //-----> IF SINGLE SELECTION DROPDOWN SELECTED
                if (attrType == 3) {
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeOptionID == optionValue;
                    })[0]);
                }
                    //-----> IF MULTI SELECTION DROPDOWN SELECTED
                else if (attrType == 4) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                    //-----> IF TREE SELECTED
                else if (attrType == 7) {
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                    //-----> IF DROPDOWN TREE SELECTED
                else if (attrType == 6 || attrType == 12) {
                    if (attrVal != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrVal != null) ? parseInt(attrVal, 10) : 0) && e.AttributeLevel == ((attributeLevel != null) ? parseInt(attributeLevel, 10) : 0));
                        })[0]);
                    }
                }

                //----------> IF OPTION AVAILABLE FOR THE ATTRIBUTE SHOW THE ATTRIBUTE
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                //----------------> CHECK IF THE SELECTED ATTRIBUTE IS SINGLE SELECTION DROPDOWN OR DROPDOWN TREE <--------------------
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                }
                                else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (e) { }
        }

        $scope.drpdirectiveSource = {};

        $scope.IsSourceformed = function (attrID, levelcnt, attributeLevel, attrType) {
            if (levelcnt > 0) {
                var dropdown_text = '', subid = 0;
                var currntlevel = attributeLevel + 1;
                if (attributeLevel == 1) {
                    var dropdown_text = 'dropdown_text_' + attrID + '_' + attributeLevel;
                    var idtomatch = $scope['dropdown_' + attrID + '_' + attributeLevel] != undefined ? ($scope['dropdown_' + attrID + '_' + attributeLevel].id != undefined ? $scope['dropdown_' + attrID + '_' + attributeLevel].id : $scope['dropdown_' + attrID + '_' + attributeLevel]) : 0;
                    if (idtomatch != 0)
                        $scope.drpdirectiveSource["dropdown_" + attrID + "_" + attributeLevel] = $.grep($scope.treeSources["dropdown_" + attrID].Children,
                            function (e) {
                                return e.id == idtomatch;
                            }
                        )[0];
                }
                for (var j = currntlevel; j <= levelcnt; j++) {
                    dropdown_text = 'dropdown_text_' + attrID + '_' + j;
                    subid = $scope['dropdown_' + attrID + '_' + (j)] != undefined ? ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined ? $scope['dropdown_' + attrID + '_' + (j - 1)].id : $scope['dropdown_' + attrID + '_' + (j)]) : 0;
                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = {};
                    if (subid != 0 && subid > 0) {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + (j)] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children != undefined && $scope['dropdown_' + attrID + '_' + (j - 1)] != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = ($.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)].Children, function (e) { return e.id == subid; }))[0];
                        }
                    }
                    else {
                        if ($scope['dropdown_' + attrID + '_' + (j - 1)].id != undefined) {
                            if ($scope['dropdown_' + attrID + '_' + (j - 1)].Children != undefined)
                                $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $scope['dropdown_' + attrID + '_' + (j - 1)].Children;
                            $scope['dropdown_' + attrID + '_' + j] = 0;
                        }
                        else {
                            if ($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)] != undefined) {
                                var res = [];
                                if ($scope['dropdown_' + attrID + '_' + (j)] > 0)
                                    $scope.drpdirectiveSource["dropdown_" + attrID + "_" + j] = $.grep($scope.drpdirectiveSource["dropdown_" + attrID + "_" + (j - 1)]['Children'], function (e) { return e.id == $scope['dropdown_' + attrID + '_' + (j)] })[0];
                                else
                                    $scope['dropdown_' + attrID + '_' + (j)] = 0;
                            }
                        }
                    }
                }
            }
        }

        $scope.BindChildDropdownSource = function (attrID, levelcnt, attributeLevel, attrType) {

            if (levelcnt > 0) {
                var currntlevel = attributeLevel + 1;
                for (var j = currntlevel; j <= levelcnt; j++) {
                    $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + j].length);
                    if (attrType == 6) {
                        $scope["dropdown_" + attrID + "_" + j] = 0;
                        $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].splice(0, $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)].length);
                    } else if (attrType == 12) {
                        if (j == levelcnt) $scope["multiselectdropdown_" + attrID + "_" + j] = [];
                        else $scope["multiselectdropdown_" + attrID + "_" + j] = "";
                    }
                }
                if (attrType == 6) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {
                        var children = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["dropdown_" + attrID + "_" + attributeLevel]) })[0].Children;

                        if (children != undefined) {
                            var subleveloptions = [];
                            $.each(children, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                } else if (attrType == 12) {
                    if ($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != null && $scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel] != undefined) {

                        var sublevel_res = [];
                        if ($scope["multiselectdropdown_" + attrID + "_" + attributeLevel] != undefined && $scope["multiselectdropdown_" + attrID + "_" + attributeLevel] > 0)
                            sublevel_res = $.grep($scope.DropDownTreeOptionValues["Options" + attrID + "_" + attributeLevel], function (e) { return e.id == parseInt($scope["multiselectdropdown_" + attrID + "_" + attributeLevel]) })[0].Children;
                        if (sublevel_res != undefined) {
                            var subleveloptions = [];
                            $.each(sublevel_res, function (i, el) {
                                subleveloptions.push(el);
                            });
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = subleveloptions;
                        }
                        else
                            $scope.DropDownTreeOptionValues["Options" + attrID + "_" + (attributeLevel + 1)] = [];
                    }
                }
            }
        }


        //task metadata action methods comes here





        $scope.OwnerList = [];
        $scope.OwnerList.push({ "Roleid": 1, "RoleName": "Owner", "UserEmail": $scope.ownerEmail, "DepartmentName": "-", "Title": "-", "Userid": parseInt($scope.OwnerID, 10), "UserName": $scope.OwnerName, "IsInherited": '0', "InheritedFromEntityid": '0' });

        // Initialize with Objects.


        $scope.ReassignMembersData = [];


        $scope.formatResult = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<img src="Handlers/UserImage.ashx?id=' + item.id + '">';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.UserName + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };

        $scope.formatSelection = function (item) {
            return item.UserName;
        };

        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
            escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
        };

        function refreshModel() {
            $scope.TaskBriefDetails = {
                taskID: 0,
                taskListUniqueID: 0,
                taskTypeId: 0,
                taskTypeName: "",
                EntityID: 0,
                ownerId: 0,
                taskName: "",
                dueIn: 0,
                dueDate: "",
                status: "",
                statusID: 0,
                taskOwner: "",
                Description: "",
                Note: "",
                taskmembersList: [],
                taskAttachmentsList: [],
                taskProgressCount: "",
                totalTaskMembers: [],
                TaskMembersRoundTripGroup: [],
                taskCheckList: [],
                WorkTaskInprogressStatus: ""
            };
            $scope.FileList = [];
            $scope.AttachmentFilename = [];

        }

        //task is getting saved here
        $scope.ExpireCreateNewTaskAction = function (inputtaskObj, ActionType, ExecutionDate, ActionsourceId) {


            //console.log("inputtaskObj", inputtaskObj);
            if ($('#ExpireCreateNewTaskBtn').hasClass('disabled')) { return; }
            $('#ExpireCreateNewTaskBtn').addClass('disabled');
            //$scope.ValidExpireActionDates($scope.CTExecutionDate, 2, $scope.CTdueExecutionDate);
            $scope.ValidExpireActionDate = [];
            $scope.ValidExpireActionDate.push({
                "SourceID": $scope.SourceID, "DateActionexpiredate": $scope.DateActionObject.dateExpirevalue, "ActionType": 2, "Actionexutedays": inputtaskObj.CTExecutionDate, "SourcetypeID": $scope.SourcetypeID, "SourceAttributeID": $scope.SourceAttributeID, "SourceFrom": $scope.SourceFrom, "ProductionEntityID": $scope.ProductionEntityID, "DueActionexutedays": inputtaskObj.CTdueExecutionDate
            });
            ExpireActionService.ValidExpireActionDate($scope.ValidExpireActionDate)
               .then(function (result) {

                   if (result.Response == false) {
                       //NotifyError($translate.instant('LanguageContents.Res_1144.Caption'));
                       $scope.savevalidation.ValidActionday = false;
                       if (ActionType == 2) {
                           bootbox.alert($translate.instant('LanguageContents.Res_1141.Caption'));
                           $('#ExpireCreateNewTaskBtn').removeClass('disabled');
                           return false;
                       }
                   }
                   else {
                       $scope.SaveExpireAction = [];
                       //NotifySuccess("success");
                       $scope.savevalidation.ValidActionday = true;
                       //return true;
                       timerObj.saveexpireacion = $timeout(function () {
                           if ($scope.SourceID != undefined && $scope.DateActionObject.dateExpirevalue != "-") {


                               var dateval = new Date.create();
                               dateval = new Date.create(dateFormat(dateval, $scope.DefaultSettings.DateFormat));
                               var alertText = "";
                               $scope.AttributeData = [];
                               var taskMembersAvailable = [];
                               if (inputtaskObj.TaskMembersList == undefined || inputtaskObj.TaskMembersList == "")
                                   taskMembersAvailable = [];
                               else
                                   taskMembersAvailable = $scope.TaskMembersList;
                               if (inputtaskObj.TaskType == "" || inputtaskObj.TaskType == undefined || inputtaskObj.TaskType == 0)
                                   alertText += $translate.instant('LanguageContents.Res_4637.Caption');

                               var Tasktype_value = $.grep($scope.TaskTypeList, function (rel) {
                                   return rel.Id == inputtaskObj.TaskType;
                               })[0];
                               if (Tasktype_value != undefined)
                                   if (Tasktype_value.TaskTypeId == "" || Tasktype_value.TaskTypeId == undefined) {
                                       if (!alertText.contains($translate.instant('LanguageContents.Res_4637.Caption')))
                                           alertText += $translate.instant('LanguageContents.Res_4637.Caption');
                                   }
                               if (inputtaskObj.NewTaskName == "")
                                   alertText += $translate.instant('LanguageContents.Res_4587.Caption');
                               if ($scope.TaskDueDate != undefined && dateDiffBetweenDates($scope.TaskDueDate) < 0)
                                   alertText += ($translate.instant('LanguageContents.Res_1909.Caption'));
                               var ChecklistName = $scope.AdminTaskCheckList[$scope.AdminTaskCheckList.length - 1].Name;
                               if ($scope.AdminTaskCheckList.length != 1) {
                                   if (ChecklistName == "" || ChecklistName == undefined) {
                                       alertText += $translate.instant('LanguageContents.Res_4588.Caption');
                                   }
                               }


                               if (alertText == "") {
                                   $scope.addtaskvallid = 1;
                                   $scope.AttributeData = [];
                                   //var SaveTaskData = $resource('exphandler/CreateExpireAction/');

                                   var Tasktype_value = $.grep($scope.TaskTypeList, function (rel) {
                                       return rel.Id == inputtaskObj.TaskType;
                                   })[0];

                                   $scope.SaveCreateTaskExpiry = [];


                                   var SaveTask = {};
                                   SaveTask.ActionType = ActionType;
                                   SaveTask.SourceID = $scope.SourceID;
                                   SaveTask.SourceEnityID = $scope.SourceEnityID;
                                   SaveTask.SourceFrom = $scope.SourceFrom;
                                   SaveTask.Actionexutedays = ExecutionDate;
                                   SaveTask.DateActionexpiredate = $scope.DateActionObject.dateExpirevalue,
                                   SaveTask.ispublish = 0;
                                   SaveTask.ActionsourceId = ActionsourceId;
                                   SaveTask.TaskType = Tasktype_value.TaskTypeId;
                                   SaveTask.Typeid = parseInt(inputtaskObj.TaskType);
                                   SaveTask.Name = inputtaskObj.NewTaskName;
                                   SaveTask.TaskListID = 0;
                                   SaveTask.Description = inputtaskObj.TaskDescription;
                                   SaveTask.Note = inputtaskObj.TaskNote;
                                   SaveTask.TaskAttachments = [];
                                   SaveTask.TaskAttachments = $scope.AttachmentFilename;
                                   SaveTask.TaskFiles = $scope.FileList;
                                   SaveTask.ParentEntityID = $scope.SourceEnityID;
                                   SaveTask.AssetID = Tasktype_value.TaskTypeId == 32 ? Tasktype_value.TaskTypeId : 0;
                                   SaveTask.DueDate = $scope.TaskDueDate != null ? $scope.TaskDueDate : "";
                                   SaveTask.DueDateoptions = inputtaskObj.CTdueExecutionDate;
                                   SaveTask.ProductionEntityID = $scope.ProductionEntityID;
                                   SaveTask.TaskMembers = [];
                                   SaveTask.TaskMembersexpireroles = [];
                                   var taskOwnerObj = [];
                                   taskOwnerObj = $scope.OwnerList[0];
                                   if (inputtaskObj.TaskMembersList == undefined || inputtaskObj.TaskMembersList == "")
                                       $scope.TaskMembersList = [];
                                   else {
                                       SaveTask.TaskMembers = inputtaskObj.TaskMembersList;
                                       //var temparr = [], TaskMembersexpireroles= [] ;
                                       //angular.forEach(inputtaskObj.TaskMembersList, function (item) {
                                       //    temparr = ($.grep($scope.EntityMemberDistinctRoleList, function (e) {
                                       //        return e.userId == item;
                                       //    }));

                                       //    if (temparr != null)
                                       //        if (temparr.length > 0)
                                       //            SaveTask.TaskMembersexpireroles.push(temparr);

                                       //});


                                   }



                                   //attribute data

                                   for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                                       if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                                           for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                               if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                                   $scope.AttributeData.push({
                                                       "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                       "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                       "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                       "NodeID": [$scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                                       "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
                                                       "Value": "-1"
                                                   });
                                               }
                                           }
                                       }
                                       else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                                           for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                               var levelCount = $scope.atributesRelationList[i].Levels.length;
                                               if (levelCount == 1) {
                                                   for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                                       $scope.AttributeData.push({
                                                           "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                           "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                           "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                           "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                                           "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level,
                                                           "Value": "-1"
                                                       });
                                                   }
                                               }
                                               else {
                                                   if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                                       if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                                           for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                                               $scope.AttributeData.push({
                                                                   "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                                   "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                                   "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                                   "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                                                   "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level,
                                                                   "Value": "-1"
                                                               });
                                                           }
                                                       }
                                                       else {
                                                           $scope.AttributeData.push({

                                                               "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                               "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                               "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                               "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                                               "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
                                                               "Value": "-1"
                                                           });
                                                       }
                                                   }
                                               }
                                           }
                                       }
                                       else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                                           for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {

                                               var attributeLevelOptions = [];
                                               attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID + ""], function (e) { return e.level == (j + 1); }));
                                               if (attributeLevelOptions[0] != undefined) {
                                                   if (attributeLevelOptions[0].selection != undefined) {
                                                       for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
                                                           var valueMatches = [];
                                                           if (attributeLevelOptions[0].selection.length > 1)
                                                               valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                                                   return relation.NodeId.toString() === opt;
                                                               });
                                                           $scope.AttributeData.push({
                                                               "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                               "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                               "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                               "NodeID": [opt],
                                                               "Level": (j + 1),
                                                               "Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
                                                           });
                                                       }
                                                   }
                                               }
                                           }
                                       }
                                       else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                                           if ($scope.atributesRelationList[i].IsSpecial == true) {
                                               if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                                   $scope.AttributeData.push({
                                                       "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                       "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                                       "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                       "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                                       "Level": 0,
                                                       "Value": "-1"
                                                   });
                                               }
                                           }
                                           else if ($scope.atributesRelationList[i].IsSpecial == false) {
                                               if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                                                   var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID];
                                                   $scope.AttributeData.push({
                                                       "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                       "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                                       "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                       "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                                                       "Level": 0,
                                                       "Value": "-1"
                                                   });
                                               }

                                           }
                                       }
                                       else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                                           if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name)
                                               $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                                           else {
                                               $scope.AttributeData.push({
                                                   "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                   "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                                   "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                   "NodeID": ($scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID] != null) ? $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                                                   "Level": 0,
                                                   "Value": "-1"
                                               });
                                           }
                                       }
                                       else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                                           if (($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TakeDescription) && ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskNotes)) {
                                               $scope.AttributeData.push({
                                                   "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                   "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                                   "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                   "NodeID": ($scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID] != null) ? $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString() : "",
                                                   "Level": 0,
                                                   "Value": "-1"
                                               });
                                           }
                                       }
                                       else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                                           var MyDate = new Date.create();
                                           var MyDateString;

                                           if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                                               MyDateString = $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID];
                                           }
                                           else {
                                               MyDateString = "";
                                           }
                                           if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.TaskDueDate) {
                                               $scope.AttributeData.push({
                                                   "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                   "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                                   "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                   "NodeID": MyDateString,
                                                   "Level": 0,
                                                   "Value": "-1"
                                               });
                                           }
                                       }
                                       else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                                           if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                               if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                                   var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                                                   for (var k = 0 ; k < multiselectiObject.length; k++) {
                                                       $scope.AttributeData.push({
                                                           "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                           "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                                           "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                           "NodeID": parseInt(multiselectiObject[k], 10),
                                                           "Level": 0,
                                                           "Value": "-1"
                                                       });
                                                   }
                                               }
                                           }
                                       }
                                       else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                                           $scope.savesubentityperiods = [];
                                           for (var m = 0; m < $scope.items.length; m++) {
                                               if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                                                   if ($scope.items[m].startDate.toString().length != 0 && $scope.items[m].endDate.toString().length != 0) {
                                                       $scope.savesubentityperiods.push({
                                                           startDate: ($scope.items[m].startDate.toString().length != 0 ? dateFormat(new Date.create($scope.items[m].startDate.toString(), $scope.format), $scope.format) : ''),
                                                           endDate: ($scope.items[m].endDate.toString().length != 0 ? dateFormat(new Date.create($scope.items[m].endDate.toString(), $scope.format), $scope.format) : ''),
                                                           comment: ($scope.items[m].comment.trim().length != 0 ? $scope.items[m].comment : ''),
                                                           sortorder: 0
                                                       });
                                                   }
                                               }
                                           }
                                           $scope.AttributeData.push({
                                               "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                               "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                               "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                               "NodeID": $scope.savesubentityperiods,
                                               "Level": 0,
                                               "Value": "-1"
                                           });
                                       }
                                       else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                                           var treenodes = [];
                                           treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == $scope.atributesRelationList[i].AttributeID; });
                                           for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                                               $scope.AttributeData.push({
                                                   "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                   "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                                   "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                   "NodeID": [parseInt(nodeval.id, 10)],
                                                   "Level": parseInt(nodeval.Level, 10),
                                                   "Value": "-1"
                                               });
                                           }
                                       }
                                       else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                                           $scope.AttributeData.push({
                                               "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                               "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                               "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                               "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                                               "Level": 0,
                                               "Value": "-1"
                                           });
                                       }
                                       
                                   }
                                   SaveTask.AttributeData = $scope.AttributeData;
                                   var taskResObj = $.grep($scope.AdminTaskCheckList, function (e) { return e.Name != ""; });
                                   SaveTask.AdminTaskCheckList = $scope.AdminTaskCheckList;

                                   $scope.SaveExpireAction = [];
                                   
                                   $scope.SaveExpireAction.push({
                                       "ActionType": SaveTask.ActionType, "SourceID": $scope.SourceID, "SourceEnityID": $scope.SourceEnityID, "SourceFrom": $scope.SourceFrom, "Actionexutedays": ExecutionDate,
                                       "DateActionexpiredate": $scope.DateActionObject.dateExpirevalue, "ispublish": 0, "ActionsourceId": ActionsourceId, "AttributeData": SaveTask.AttributeData, "ActionSrcValue": { "ActionTaskSrcValue": SaveTask }, "AttributeID": $scope.SourceAttributeID
                                   });
                                   var SaveExpireAction = { SaveExpireAction: $scope.SaveExpireAction[0] };
                                   ExpireActionService.CreateExpireAction(SaveExpireAction).then(function (SaveTaskResult) {
                                       // var SaveTaskResult = SaveTaskData.save(SaveExpireAction, function () {
                                       $('#ExpireCreateNewTaskBtn').removeClass('disabled');
                                       if (SaveTaskResult.StatusCode == 405) {
                                           $('#ExpireCreateNewTaskBtn').removeClass('disabled');
                                           NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
                                           $scope.SaveExpireAction = [];
                                       }
                                       else {
                                           NotifySuccess($translate.instant('LanguageContents.Res_4782.Caption'));
                                           if (SaveTaskResult.Response != null && SaveTaskResult.Response.Item1 != 0) {
                                               $scope.SaveExpireAction = [];
                                               timerObj.callexpireaction2 = $timeout(function () {
                                                   //$scope.$emit('CallExpireaction', $scope.DateActionObject.dateExpirevalue, $scope.SourceID, $scope.SourceEnityID, $scope.SourceFrom, $scope.SourcetypeID, $scope.ProductionEntityID, $scope.SourceAttributeID, $scope.SourceAttributename);
                                                   OpenExpireactionpopup($scope.DateActionObject.dateExpirevalue, $scope.SourceID, $scope.SourceEnityID, $scope.SourceFrom, $scope.SourcetypeID, $scope.ProductionEntityID, $scope.SourceAttributeID, $scope.SourceAttributename);
                                               }, 100);
                                           }
                                       }
                                   });
                               }
                               else {
                                   $('#ExpireCreateNewTaskBtn').removeClass('disabled');
                                   bootbox.alert(alertText);
                                   return false;
                               }
                           }
                           else {
                               $('#ExpireCreateNewTaskBtn').removeClass('disabled');
                               //bootbox.alert("Please enter the Expire Date");
                               return false;
                           }
                       }, 100);

                   }
               });

        }
        //-----------------update metadata--------------------

        $scope.saveexpirehandlerMetadata = function (ActionType, ExecutionDate, ActionsourceId) {
            if ($('#ExpireUMBtn').hasClass('disabled')) { return; }
            $('#ExpireUMBtn').addClass('disabled');

            $scope.ValidExpireActionDate = [];
            $scope.ValidExpireActionDate.push({
                "SourceID": $scope.SourceID, "DateActionexpiredate": $scope.DateActionObject.dateExpirevalue, "ActionType": 3, "Actionexutedays": ExecutionDate, "SourcetypeID": $scope.SourcetypeID, "SourceAttributeID": $scope.SourceAttributeID, "SourceFrom": $scope.SourceFrom, "ProductionEntityID": $scope.ProductionEntityID, "DueActionexutedays": "-1"
            });
            ExpireActionService.ValidExpireActionDate($scope.ValidExpireActionDate)
               .then(function (result) {

                   if (result.Response == false) {
                       //NotifyError($translate.instant('LanguageContents.Res_1144.Caption'));
                       $scope.savevalidation.ValidActionday = false;
                       if (ActionType == 3) {
                           bootbox.alert($translate.instant('LanguageContents.Res_1140.Caption'));
                           $('#ExpireUMBtn').removeClass('disabled');
                           return false;
                       }
                       //$("#expirepopup").modal("hide");
                       $scope.SaveExpireAction = [];

                   }
                   else {
                       $scope.SaveExpireAction = [];
                       //NotifySuccess("success");
                       $scope.savevalidation.ValidActionday = true;
                       // return true;
                       if ($scope.SourceID != undefined && $scope.DateActionObject.dateExpirevalue != "-") {
                           var dateval = new Date.create();
                           dateval = new Date.create(dateFormat(dateval, $scope.DefaultSettings.DateFormat));
                           var alertText = "";
                           if (alertText == "") {
                               $scope.addtaskvallid = 1;
                               $scope.AttributeData = [];
                               // var SaveexpireHandlerData = $resource('exphandler/CreateExpireActionwithMetadata/');

                               var Tasktype_value = $.grep($scope.TaskTypeList, function (rel) {
                                   return rel.Id == $scope.TaskType;
                               })[0];

                               $scope.SaveCreateTaskExpiry = [];
                               //  var SaveTask = new SaveexpireHandlerData();
                               var SaveTask = {};
                               //SaveTask.ActionID = ActionID;
                               SaveTask.SourceID = $scope.SourceID;
                               SaveTask.SourceEnityID = $scope.SourceEnityID;
                               SaveTask.SourceFrom = $scope.SourceFrom;
                               SaveTask.Actionexutedays = ExecutionDate;
                               SaveTask.DateActionexpiredate = $scope.DateActionObject.dateExpirevalue,
                               SaveTask.ispublish = 0;
                               SaveTask.ActionsourceId = ActionsourceId;
                               //SaveTask.TaskType = Tasktype_value.TaskTypeId;
                               SaveTask.Typeid = parseInt($scope.SourcetypeID);
                               SaveTask.Name = $scope.NewTaskName;
                               SaveTask.TaskListID = 0;
                               SaveTask.Description = $scope.TaskDescription;
                               SaveTask.Note = $scope.TaskNote;
                               SaveTask.TaskAttachments = [];
                               SaveTask.TaskAttachments = $scope.AttachmentFilename;
                               SaveTask.TaskFiles = $scope.FileList;
                               SaveTask.ParentEntityID = $scope.SourceEnityID;
                               SaveTask.ProductionEntityID = $scope.ProductionEntityID;
                               //SaveTask.AssetID = Tasktype_value.TaskTypeId == 32 ? Tasktype_value.TaskTypeId : 0;
                               SaveTask.DueDate = $scope.TaskDueDate != null ? $scope.TaskDueDate : "";
                               SaveTask.TaskMembers = [];
                               SaveTask.ActionType = ActionType;
                               var taskOwnerObj = [];
                               taskOwnerObj = $scope.OwnerList[0];
                               if ($scope.TaskMembersList == undefined || $scope.TaskMembersList == "")
                                   $scope.TaskMembersList = [];
                               else
                                   SaveTask.TaskMembers = $scope.TaskMembersList;

                               $scope.AttributeData = [];

                               var selectedRelation = $.map($scope.ExpireEntityMetadataCollection, function (e) { return e.Id; }), selectionAttrs = [];

                               if (selectedRelation != null)
                                   $scope.atributesRelationList = $.grep($scope.ExpireDynamicMetadataSource, function (rel) { return selectedRelation.indexOf(rel.AttributeID.toString()) != -1; })

                               for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                                   if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                                       for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                           if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                               $scope.AttributeData.push({
                                                   "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                   "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                   "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                   "NodeID": [$scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                                   "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
                                                   "Value": "-1"
                                               });
                                           }
                                       }
                                   }
                                   else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                                       for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                                           var levelCount = $scope.atributesRelationList[i].Levels.length;
                                           if (levelCount == 1) {
                                               for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                                   $scope.AttributeData.push({
                                                       "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                       "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                       "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                       "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                                       "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level,
                                                       "Value": "-1"
                                                   });
                                               }


                                               /*  SaveTask.attributeData = $scope.AttributeData;
                                                 ExpireActionService.CreateExpireActionwithMetadata(SaveTask).then(function (SaveTaskResult) {
                                                // var SaveTaskResult = SaveexpireHandlerData.save(SaveTask, function () {
                                                     if (SaveTaskResult.StatusCode == 405) {
                                                         $('#ExpireUMBtn').removeClass('disabled');
                                                         NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
                                                         */
                                           }
                                           else {
                                               if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                                   if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                                       for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                                           $scope.AttributeData.push({
                                                               "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                               "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                               "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                               "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                                               "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level,
                                                               "Value": "-1"
                                                           });
                                                       }
                                                   }
                                                   else {
                                                       $scope.AttributeData.push({

                                                           "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                           "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                           "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                           "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                                           "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
                                                           "Value": "-1"
                                                       });
                                                   }
                                               }
                                           }
                                       }
                                   }
                                   else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                                       for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {

                                           var attributeLevelOptions = [];
                                           attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID + ""], function (e) { return e.level == (j + 1); }));
                                           if (attributeLevelOptions[0] != undefined) {
                                               if (attributeLevelOptions[0].selection != undefined) {
                                                   for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
                                                       var valueMatches = [];
                                                       if (attributeLevelOptions[0].selection.length > 1)
                                                           valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                                               return relation.NodeId.toString() === opt;
                                                           });
                                                       $scope.AttributeData.push({
                                                           "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                           "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                                           "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                           "NodeID": [opt],
                                                           "Level": (j + 1),
                                                           "Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
                                                       });
                                                   }
                                               }
                                           }
                                       }
                                   }
                                   else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                                       if ($scope.atributesRelationList[i].IsSpecial == true) {
                                           if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                               $scope.AttributeData.push({
                                                   "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                   "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                                   "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                   "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                                   "Level": 0,
                                                   "Value": "-1"
                                               });
                                           }
                                       }
                                       else if ($scope.atributesRelationList[i].IsSpecial == false) {
                                           if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                                               var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID];
                                               $scope.AttributeData.push({
                                                   "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                   "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                                   "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                   "NodeID": (value != "" || value != undefined) ? parseInt(value, 10) : 0,
                                                   "Level": 0,
                                                   "Value": "-1"
                                               });
                                           }

                                       }
                                   }
                                   else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                                       var MyDate = new Date.create();
                                       var MyDateString;

                                       if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                                           MyDateString = $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID];
                                       }
                                       else {
                                           MyDateString = "";
                                       }
                                       $scope.AttributeData.push({
                                           "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                           "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                           "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                           "NodeID": MyDateString,
                                           "Level": 0,
                                           "Value": "-1"
                                       });
                                   }
                                   else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                                       if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name)
                                           $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                                       else {
                                           $scope.AttributeData.push({
                                               "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                               "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                               "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                               "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                                               "Level": 0,
                                               "Value": "-1"
                                           });
                                       }
                                   }
                                   else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {

                                       $scope.AttributeData.push({
                                           "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                           "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                           "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                           "NodeID": $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                                           "Level": 0,
                                           "Value": "-1"
                                       });
                                   }
                                   else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                                       if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                           if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                                               var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                                               for (var k = 0 ; k < multiselectiObject.length; k++) {
                                                   $scope.AttributeData.push({
                                                       "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                       "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                                       "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                       "NodeID": parseInt(multiselectiObject[k], 10),
                                                       "Level": 0,
                                                       "Value": "-1"
                                                   });
                                               }
                                           }
                                       }
                                   }
                                   else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                                       var treenodes = [];
                                       treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == $scope.atributesRelationList[i].AttributeID; });
                                       for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                                           $scope.AttributeData.push({
                                               "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                               "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                               "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                               "NodeID": [parseInt(nodeval.id, 10)],
                                               "Level": parseInt(nodeval.Level, 10),
                                               "Value": "-1"
                                           });
                                       }
                                   }
                                   else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                                       $scope.AttributeData.push({
                                           "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                           "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                           "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                           "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                                           "Level": 0,
                                           "Value": "-1"
                                       });
                                   }
                                   else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                                       if ($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                           for (var j = 0; j < $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID].length; j++) {
                                               $scope.AttributeData.push({
                                                   "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                                   "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                                   "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                                   "NodeID": parseInt($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID][j], 10),
                                                   "Level": 0,
                                                   "Value": "-1"
                                               });
                                           }
                                       }
                                   }
                               }
                               SaveTask.AttributeData = $scope.AttributeData;

                               //console.log("dynamic data to be updated########", SaveTask.AttributeData);


                               $scope.SaveExpireAction = [];

                               $scope.SaveExpireAction.push({
                                   "ActionType": SaveTask.ActionType, "SourceID": $scope.SourceID, "SourceEnityID": $scope.SourceEnityID, "SourceFrom": $scope.SourceFrom, "Actionexutedays": ExecutionDate,
                                   "DateActionexpiredate": $scope.DateActionObject.dateExpirevalue, "ispublish": 0, "ActionsourceId": ActionsourceId, "AttributeData": SaveTask.AttributeData, "ActionSrcValue": { "ActionTaskSrcValue": SaveTask }, "AttributeID": $scope.SourceAttributeID

                               });
                               var SaveExpireAction = { SaveExpireAction: $scope.SaveExpireAction[0] };


                               //  var SaveTaskResult = SaveexpireHandlerData.save(SaveExpireAction, function () 
                               ExpireActionService.CreateExpireAction(SaveExpireAction).then(function (SaveTaskResult) {
                                   if (SaveTaskResult.StatusCode == 405) {
                                       $('#ExpireUMBtn').removeClass('disabled');
                                       NotifyError($translate.instant('LanguageContents.Res_4345.Caption'));
                                   }
                                   else {
                                       NotifySuccess($translate.instant('LanguageContents.Res_4782.Caption'));
                                       if (SaveTaskResult.Response != null && SaveTaskResult.Response != 0) {
                                           $('#ExpireUMBtn').removeClass('disabled');

                                           timerObj.callexpireacion2 = $timeout(function () {
                                               //$scope.$emit('CallExpireaction', $scope.DateActionObject.dateExpirevalue, $scope.SourceID, $scope.SourceEnityID, $scope.SourceFrom, $scope.SourcetypeID, $scope.ProductionEntityID, $scope.SourceAttributeID, $scope.SourceAttributename);
                                               OpenExpireactionpopup($scope.DateActionObject.dateExpirevalue, $scope.SourceID, $scope.SourceEnityID, $scope.SourceFrom, $scope.SourcetypeID, $scope.ProductionEntityID, $scope.SourceAttributeID, $scope.SourceAttributename);
                                           }, 100);
                                       }
                                   }
                               });
                           }
                           else {
                               $('#ExpireUMBtn').removeClass('disabled');
                               bootbox.alert(alertText);
                               return false;
                           }
                       }
                       else {
                           bootbox.alert($translate.instant('LanguageContents.Res_1142.Caption'));
                           return false;
                       }

                   }
               });


        }

        //
        //------------------------------
        $scope.ExecutionDateList = [
        {
            id: 0, name: "+ 0 day(s) "
        },
        {
            id: 1, name: "+ 1 day(s) "
        },
        {
            id: 2, name: "+ 2 day(s)"
        },
        {
            id: 3, name: "+ 3 day(s)"
        },
        {
            id: 4, name: "+ 4 day(s)"
        },
        {
            id: 5, name: "+ 5 day(s)"
        },
        {
            id: 6, name: "+ 6 day(s)"
        },
        {
            id: 7, name: "+ 1 week"
        },
        {
            id: 8, name: "+ 2 weeks"
        },
        {
            id: 9, name: "+ 3 weeks"
        },
        {
            id: 10, name: "+ 1 month"
        },
        {
            id: 11, name: "+ 2 months"
        },
        {
            id: 12, name: "+ 3 months"
        },
        {
            id: 13, name: "+ 4 months"
        },
        {
            id: 14, name: "+ 5 months"
        },
        {
            id: 15, name: "+ 6 months"
        },
        {
            id: 16, name: "+ 7 months"
        },
        {
            id: 17, name: "+ 8 months"
        },
        {
            id: 18, name: "+ 9 months"
        },
        {
            id: 19, name: "+ 10 months"
        },
        {
            id: 20, name: "+ 11 months"
        },
        {
            id: 21, name: "+ 12 months"
        },
        {
            id: 22, name: "- 1 day "
        },
        {
            id: 23, name: "- 2 days"
        },
        {
            id: 24, name: "- 3 days"
        },
        {
            id: 25, name: "- 4 days"
        },
        {
            id: 26, name: "- 5 days"
        },
        {
            id: 27, name: "- 6 days"
        },
        {
            id: 28, name: "- 1 week"
        },
        {
            id: 29, name: "- 2 weeks"
        },
        {
            id: 30, name: "- 3 weeks"
        },
        {
            id: 31, name: "- 1 month"
        },
        {
            id: 32, name: "- 2 months"
        },
        {
            id: 33, name: "- 3 months"
        },
        {
            id: 34, name: "- 4 months"
        },
        {
            id: 35, name: "- 5 months"
        },
        {
            id: 36, name: "- 6 months"
        },
        {
            id: 37, name: "- 7 months"
        },
        {
            id: 38, name: "- 8 months"
        },
        {
            id: 39, name: "- 9 months"
        },
        {
            id: 40, name: "- 10 months"
        },
        {
            id: 41, name: "- 11 months"
        },
        {
            id: 42, name: "- 12 months"
        }

        ]

        $scope.DuEExecutionDateList = [
        {
            id: 0, name: "+ 0 day(s) "
        },
        {
            id: 1, name: "+ 1 day(s) "
        },
        {
            id: 2, name: "+ 2 day(s)"
        },
        {
            id: 3, name: "+ 3 day(s)"
        },
        {
            id: 4, name: "+ 4 day(s)"
        },
        {
            id: 5, name: "+ 5 day(s)"
        },
        {
            id: 6, name: "+ 6 day(s)"
        },
        {
            id: 7, name: "+ 1 week"
        },
        {
            id: 8, name: "+ 2 weeks"
        },
        {
            id: 9, name: "+ 3 weeks"
        },
        {
            id: 10, name: "+ 1 month"
        },
        {
            id: 11, name: "+ 2 months"
        },
        {
            id: 12, name: "+ 3 months"
        },
        {
            id: 13, name: "+ 4 months"
        },
        {
            id: 14, name: "+ 5 months"
        },
        {
            id: 15, name: "+ 6 months"
        },
        {
            id: 16, name: "+ 7 months"
        },
        {
            id: 17, name: "+ 8 months"
        },
        {
            id: 18, name: "+ 9 months"
        },
        {
            id: 19, name: "+ 10 months"
        },
        {
            id: 20, name: "+ 11 months"
        },
        {
            id: 21, name: "+ 12 months"
        },
        {
            id: 22, name: "- 1 day "
        },
        {
            id: 23, name: "- 2 days"
        },
        {
            id: 24, name: "- 3 days"
        },
        {
            id: 25, name: "- 4 days"
        },
        {
            id: 26, name: "- 5 days"
        },
        {
            id: 27, name: "- 6 days"
        },
        {
            id: 28, name: "- 1 week"
        },
        {
            id: 29, name: "- 2 weeks"
        },
        {
            id: 30, name: "- 3 weeks"
        },
        {
            id: 31, name: "- 1 month"
        },
        {
            id: 32, name: "- 2 months"
        },
        {
            id: 33, name: "- 3 months"
        },
        {
            id: 34, name: "- 4 months"
        },
        {
            id: 35, name: "- 5 months"
        },
        {
            id: 36, name: "- 6 months"
        },
        {
            id: 37, name: "- 7 months"
        },
        {
            id: 38, name: "- 8 months"
        },
        {
            id: 39, name: "- 9 months"
        },
        {
            id: 40, name: "- 10 months"
        },
        {
            id: 41, name: "- 11 months"
        },
        {
            id: 42, name: "- 12 months"
        }

        ]



        $scope.ExpireEntityMetadataCollection = [{ "Id": "0", "Text": $translate.instant('LanguageContents.Res_39.Caption'), "HtmlControl": "" }];
        $scope.ExpireDynamicMetadataSource = [];
        $scope.renderControlHtml = function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };

        $scope.pushmoreAttribute = function () {
            $scope.ExpireEntityMetadataCollection.push({ "Id": "0", "Text": $translate.instant('LanguageContents.Res_39.Caption'), "HtmlControl": "" });
        }

        $scope.splicemoreAttribute = function (attr) {
            if ($scope.ExpireEntityMetadataCollection.length == 1) {
                $scope.ExpireEntityMetadataCollection = [{ "Id": "0", "Text": $translate.instant('LanguageContents.Res_39.Caption'), "HtmlControl": "" }];

            }
            else {
                $scope.ExpireEntityMetadataCollection.splice($.inArray(attr, $scope.ExpireEntityMetadataCollection), 1);
            }
        }

        $scope.checkcontent = function (attr) {
            if (attr.HtmlControl == "")
                return false;
            else
                return true;
        }

        $scope.attrAvailabilitytracker = function (item) {



            var isExistsCollection = $.grep($scope.ExpireEntityMetadataCollection, function (rel) {
                return rel.Id == item.AttributeID;
            });
            if (isExistsCollection == null) return true;
            if (isExistsCollection.length == 0) return true;
            else return false;

        };


        function LoadCustomMetadataPart() {
            ExpireActionService.GetEntityTypeMetadataAttributes($scope.ProductionEntityID)
                .then(
               function (result) {
                   if (result.Response == null) {

                   }
                   else {
                       $scope.ExpireDynamicMetadataSource = result.Response;


                   }
               });
        }



        $scope.GenerateMetadataControl = function (attr) {

            if (attr.Id != "") {

                var dynhtml = "";
                var attrOption = $.grep($scope.ExpireDynamicMetadataSource, function (rel) {
                    return rel.AttributeID == attr.Id;
                });
                for (var i = 0; i < attrOption.length; i++) {
                    if (attrOption[i].AttributeTypeID == 1) {
                        if (attrOption[i].AttributeID != 70) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrOption[i].AttributeID] = true;
                            if (attrOption[i].InheritFromParent)
                                attrOption[i].DefaultValue = attrOption[i].ParentValue[0];

                            if (attrOption[i].AttributeID == SystemDefiendAttributes.Name) {

                                dynhtml += "<div class=\"control-group\"><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + attrOption[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + attrOption[i].AttributeID + "\" id=\"TextSingleLine_" + attrOption[i].AttributeID + "\" placeholder=\"" + attrOption[i].Caption + "\"></div></div>";
                                $scope.fields["TextSingleLine_" + attrOption[i].AttributeID] = attrOption[i].DefaultValue;
                            } else {

                                dynhtml += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + attrOption[i].AttributeID + "\" class=\"control-group\"><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + attrOption[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + attrOption[i].AttributeID + "\" id=\"TextSingleLine_" + attrOption[i].AttributeID + "\" placeholder=\"" + attrOption[i].PlaceHolderValue + "\"></div></div>";
                                $scope.fields["TextSingleLine_" + attrOption[i].AttributeID] = attrOption[i].DefaultValue;

                                if ($scope.UMActionSourcedatas != null) {
                                    try {
                                        var val = $.grep($scope.UMActionSourcedatas, function (rel) {
                                            return rel.AttributeID == attrOption[i].AttributeID
                                        })[0];
                                        if (val != null) {
                                            $scope.fields["TextSingleLine_" + attrOption[i].AttributeID] = val.NodeID;
                                        }
                                    } catch (e) { }

                                }
                            }
                        }
                    }
                    else if (attrOption[i].AttributeTypeID == 3) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + attrOption[i].AttributeID] = true;
                        if (attrOption[i].IsSpecial == true) {
                            if (attrOption[i].AttributeID == SystemDefiendAttributes.Owner) {

                                dynhtml += "<div class=\"control-group\"><div class=\"controls\"> <input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + attrOption[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + attrOption[i].AttributeID + "\"  id=\"ListSingleSelection_" + attrOption[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + attrOption[i].Caption + "\"></div></div>";
                                $scope.fields["ListSingleSelection_" + attrOption[i].AttributeID] = $scope.OwnerList[0].UserName;
                            }
                        } else if (attrOption[i].AttributeID == SystemDefiendAttributes.FiscalYear) {
                            $scope.OptionObj["option_" + attrOption[i].AttributeID] = attrOption[i].Options;
                            dynhtml += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + attrOption[i].AttributeID + "\" class=\"control-group\"><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + attrOption[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrOption[i].AttributeID + ",0,0,3)\" ng-model=\"fields.ListSingleSelection_" + attrOption[i].AttributeID + "\"  id=\"ListSingleSelection_" + attrOption[i].AttributeID + "\"> <option value=\"\"> Select " + attrOption[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + attrOption[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                            if (attrOption[i].InheritFromParent)
                                $scope.fields["ListSingleSelection_" + attrOption[i].AttributeID] = attrOption[i].ParentValue[0];
                            else
                                $scope.fields["ListSingleSelection_" + attrOption[i].AttributeID] = attrOption[i].DefaultValue;

                            if ($scope.UMActionSourcedatas != null) {
                                try {
                                    var val = $.grep($scope.UMActionSourcedatas, function (rel) {
                                        return rel.AttributeID == attrOption[i].AttributeID
                                    })[0];
                                    if (val != null) {
                                        $scope.fields["ListSingleSelection_" + attrOption[i].AttributeID] = val.NodeID;
                                    }
                                } catch (e) { }

                            }
                        }
                        else if (attrOption[i].AttributeID == SystemDefiendAttributes.EntityStatus) {
                        }
                        else {
                            $scope.OptionObj["option_" + attrOption[i].AttributeID] = attrOption[i].Options;

                            dynhtml += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + attrOption[i].AttributeID + "\" class=\"control-group\"><div class=\"controls\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.Selection_" + attrOption[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrOption[i].AttributeID + ",0,0,3)\" ng-model=\"fields.ListSingleSelection_" + attrOption[i].AttributeID + "\"  id=\"ListSingleSelection_" + attrOption[i].AttributeID + "\"> <option value=\"\"> Select " + attrOption[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + attrOption[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Caption}}</option></select></div></div>";
                            if (attrOption[i].InheritFromParent)
                                $scope.fields["ListSingleSelection_" + attrOption[i].AttributeID] = attrOption[i].ParentValue[0];
                            else
                                $scope.fields["ListSingleSelection_" + attrOption[i].AttributeID] = attrOption[i].DefaultValue;

                            if ($scope.UMActionSourcedatas != null) {
                                try {
                                    var val = $.grep($scope.UMActionSourcedatas, function (rel) {
                                        return rel.AttributeID == attrOption[i].AttributeID
                                    })[0];
                                    if (val != null) {
                                        $scope.fields["ListSingleSelection_" + attrOption[i].AttributeID] = val.NodeID;
                                    }
                                } catch (e) { }

                            }

                        }
                    }
                    else if (attrOption[i].AttributeTypeID == 5) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + attrOption[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["DateTime_" + attrOption[i].AttributeID] = false;

                        dynhtml += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + attrOption[i].AttributeID + "\" class=\"control-group\"><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.DateTime_" + attrOption[i].AttributeID + "\" type=\"text\" ng-model=\"fields.DatePart_" + attrOption[i].AttributeID + "\" id=\"DatePart_" + attrOption[i].AttributeID + "\" ng-click=\"Calanderopen($event,'date_" + attrOption[i].AttributeID + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"date_'" + attrOption[i].AttributeID + "\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + attrOption[i].AttributeCaption + "\"></div></div>";
                        $scope.fields["DatePart_Calander_Open" + attrOption[i].AttributeID] = false;
                        var param1 = new Date.create();
                        var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                        $scope.fields["DatePart_" + attrOption[i].AttributeID] = new Date.create();
                        $scope.fields["DatePart_" + attrOption[i].AttributeID] = null;
                        $scope.fields["DatePart_Calander_Open" + attrOption[i].AttributeID] = false;
                    }

                    else if (attrOption[i].AttributeTypeID == 6) {
                        var totLevelCnt = attrOption[i].Levels.length;
                        for (var j = 0; j < attrOption[i].Levels.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + attrOption[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)] = {};
                            $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].multiple = false;
                            $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                            $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                            if (j == 0) {
                                $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].data = JSON.parse(attrOption[i].tree).Children;

                                if ($scope.UMActionSourcedatas != null) {
                                    try {
                                        var val = $.grep($scope.UMActionSourcedatas, function (rel) {
                                            return rel.AttributeID == attrOption[i].AttributeID && rel.Level == (j + 1)
                                        })[0];
                                        if (val != null) {
                                            $scope.fields["DropDown_" + attrOption[i].AttributeID + "_" + (j + 1)] = $.grep($scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].data, function (e) {
                                                return e.id == val.NodeID[0];
                                            })[0];
                                        }

                                    } catch (e) { }
                                }
                                dynhtml += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + attrOption[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><div class=\"controls\">";
                                dynhtml += "<input ui-select2=\"Dropdown.OptionValues" + attrOption[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + attrOption[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrOption[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + attrOption[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + attrOption[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                            }
                            else {
                                $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].data = [];
                                
                                if ($scope.UMActionSourcedatas != null) {
                                    $scope.ShowHideAttributeToAttributeRelations(attrOption[i].AttributeID, j, totLevelCnt, 6);
                                }

                                if ($scope.UMActionSourcedatas != null) {
                                    try {
                                        var val = $.grep($scope.UMActionSourcedatas, function (rel) {
                                            return rel.AttributeID == attrOption[i].AttributeID && rel.Level == (j + 1)
                                        })[0];
                                        if (val != null) {
                                            $scope.fields["DropDown_" + attrOption[i].AttributeID + "_" + (j + 1)] = $.grep($scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].data, function (e) {
                                                return e.id == val.NodeID[0];
                                            })[0];
                                        }
                                    } catch (e) { }
                                }
                                dynhtml += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + attrOption[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><div class=\"controls\"> ";
                                dynhtml += "<input ui-select2=\"Dropdown.OptionValues" + attrOption[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Selection_" + attrOption[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrOption[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\"  ng-model=\"fields.DropDown_" + attrOption[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + attrOption[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                            }                            
                        }
                    }
                    else if (attrOption[i].AttributeTypeID == 2) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + attrOption[i].AttributeID] = true;

                        dynhtml += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + attrOption[i].AttributeID + "\" class=\"control-group control-group-textarea\"><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + attrOption[i].AttributeID + "\" name=\"fields.TextMultiLine_" + attrOption[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + attrOption[i].AttributeID + "\" id=\"TextMultiLine_" + attrOption[i].AttributeID + "\" placeholder=\"" + attrOption[i].PlaceHolderValue + "\" rows=\"3\"></textarea></div></div>";
                        if (attrOption[i].InheritFromParent)
                            $scope.fields["TextMultiLine_" + attrOption[i].AttributeID] = attrOption[i].ParentValue[0];
                        else
                            $scope.fields["TextMultiLine_" + attrOption[i].AttributeID] = attrOption[i].DefaultValue;
                        if ($scope.UMActionSourcedatas != null) {
                            try {
                                var val = $.grep($scope.UMActionSourcedatas, function (rel) {
                                    return rel.AttributeID == attrOption[i].AttributeID
                                })[0];
                                if (val != null) {
                                    $scope.fields["TextMultiLine_" + attrOption[i].AttributeID] = val.NodeID;
                                }
                            } catch (e) { }

                        }

                    } else if (attrOption[i].AttributeTypeID == 4) {
                        $scope.fields["ListMultiSelection_" + attrOption[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + attrOption[i].AttributeID] = true;
                        $scope.OptionObj["option_" + attrOption[i].AttributeID] = attrOption[i].Options;

                        dynhtml += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + attrOption[i].AttributeID + "\" class=\"control-group\"><div class=\"controls\"> <select  class=\"multiselect\"   data-placeholder=\"Select filter\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + attrOption[i].AttributeID + "\" multiple=\"multiple\"  multiselect-dropdown ng-model=\"fields.ListMultiSelection_" + attrOption[i].AttributeID + "\"  id=\"ListMultiSelection_" + attrOption[i].AttributeID + "\"     ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + attrOption[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + attrOption[i].AttributeID + ",0,0,4)\"   ></select></div></div>";

                        if (attrOption[i].InheritFromParent && attrOption[i].ParentValue != null) {
                            var defaultmultiselectvalue1 = attrOption[i].ParentValue.split(',');
                            $scope.fields["TextMultiLine_" + attrOption[i].AttributeID] = defaultmultiselectvalue1;
                        }
                        else {
                            var defaultmultiselectvalue = attrOption[i].DefaultValue.split(',');
                            if (attrOption[i].DefaultValue != "") {
                                $scope.fields["ListMultiSelection_" + attrOption[i].AttributeID] = defaultmultiselectvalue;
                            }
                            else {
                                $scope.fields["ListMultiSelection_" + attrOption[i].AttributeID] = "";
                            }
                        }
                        if ($scope.UMActionSourcedatas != null) {
                            try {
                                var obj = $.grep($scope.UMActionSourcedatas, function (rel) {
                                    return rel.AttributeID == attrOption[i].AttributeID;
                                });
                                if (obj != null) {
                                    var vals = $.map(obj, function (rel) {
                                        return parseInt(rel.NodeID);
                                    });
                                    $scope.fields["ListMultiSelection_" + attrOption[i].AttributeID] = vals;
                                }
                            } catch (e) { }

                        }
                    }
                    else if (attrOption[i].AttributeTypeID == 10) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + attrOption[i].AttributeID] = true;
                        $scope.OptionObj["option_" + attrOption[i].AttributeID] = attrOption[i].Options;
                        $scope.items.push({
                            startDate: '',
                            endDate: '',
                            comment: '',
                            sortorder: 0
                        });


                        $scope.MinValue = attrOption[i].MinValue;
                        $scope.MaxValue = attrOption[i].MaxValue;
                        $scope.fields["DatePartMinDate_" + attrOption[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMaxDate_" + attrOption[i].AttributeID] = new Date.create();

                        if ($scope.MinValue < 0) {
                            $scope.fields["DatePartMinDate_" + attrOption[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + attrOption[i].AttributeID].getDate() + ($scope.MinValue + 1));
                        } else {
                            $scope.fields["DatePartMinDate_" + attrOption[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + attrOption[i].AttributeID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_" + attrOption[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + attrOption[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                        } else {
                            $scope.fields["DatePartMaxDate_" + attrOption[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + attrOption[i].AttributeID].getDate() + 100000);
                        }
                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + attrOption[i].AttributeID], $scope.fields["DatePartMaxDate_" + attrOption[i].AttributeID]);
                        $scope.fields["DatePartMinDate_" + attrOption[i].AttributeID] = (temp.MinDate);
                        $scope.fields["DatePartMaxDate_" + attrOption[i].AttributeID] = (temp.MaxDate);
                        if ($scope.CTActionSourcedatas != null) {
                            $scope.Periodval = $.grep($scope.CTActionSourcedatas.ActionTaskSrcValue.AttributeData, function (rel) {
                                return rel.AttributeID == $scope.atributesRelationList[i].AttributeID
                            })[0];
                            if ($scope.Periodval != null) {
                                $scope.items = [];
                                for (var m = 0; m < $scope.Periodval["NodeID"].length; m++) {
                                    if ($scope.Periodval["NodeID"][m].startDate != null && $scope.Periodval["NodeID"][m].endDate != null) {
                                        if ($scope.Periodval["NodeID"][m].startDate.toString().length != 0 && $scope.Periodval["NodeID"][m].endDate.toString().length != 0) {
                                            $scope.items.push({
                                                startDate: ($scope.Periodval["NodeID"][m].startDate.toString().length != 0 ? ($scope.Periodval["NodeID"][m].startDate) : ''),
                                                endDate: ($scope.Periodval["NodeID"][m].endDate.toString().length != 0 ? ($scope.Periodval["NodeID"][m].endDate) : ''),
                                                comment: ($scope.Periodval["NodeID"][m].comment.trim().length != 0 ? $scope.Periodval["NodeID"][m].comment : ''),
                                                sortorder: 0
                                            });
                                        }
                                    }
                                }
                            }
                        }
                        dynhtml += '<a xeditabletreedropdown1 data-primaryid="' + attrOption[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + attrOption[i].TypeID + '" attributeid="' + attrOption[i].ID + '" data-periodstartdate_id=\"PeriodStartDate_' + attrOption[i].Value[j].Id + '\" data-ng-model=\"PeriodStartDate_' + attrOption[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\" data-type=\"perdiodate' + attrOption[i].Value[j].Id + '\" href=\"javascript:void(0);\" attributename="perdiodate">{{fields.PeriodStartDate_' + attrOption[i].Value[j].Id + '}}</a>';
                        dynhtml += '<a> to </a><a xeditabletreedropdown1 data-primaryid="' + attrOption[i].Value[j].Id + '" entityid="' + ID + '" attributeTypeID="' + attrOption[i].TypeID + '" attributeid="' + attrOption[i].ID + '" data-periodenddate_id=\"PeriodEndDate_' + attrOption[i].Value[j].Id + '\" data-ng-model=\"PeriodEndDate_' + attrOption[i].Value[j].Id + '\"  my-qtip2 qtip-content=\"' + inlineEditabletitile + '\"  data-type=\"perdiodate' + attrOption[i].Value[j].Id + '\" href=\"javascript:void(0);\" attributename="perdiodate">{{fields.PeriodEndDate_' + attrOption[i].Value[j].Id + '}}</a>';
                        $scope.fields["Period_" + attrOption[i].AttributeID] = '';
                        $scope.fields["DatePart_Calander_Open" + "item.startDate"] = false;
                        $scope.fields["DatePart_Calander_Open" + "item.endDate"] = false;

                    }
                    else if (attrOption[i].AttributeTypeID == 7) {
                        $scope.fields["Tree_" + attrOption[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + attrOption[i].AttributeID] = true;
                        $scope.treesrcdirec["Attr_" + attrOption[i].AttributeID] = JSON.parse(attrOption[i].tree).Children;
                        if ($scope.treesrcdirec["Attr_" + attrOption[i].AttributeID].length > 0) {
                            treeTextVisbileflag = false;
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + attrOption[i].AttributeID])) {
                                $scope.treePreviewObj["Attr_" + attrOption[i].AttributeID] = true;
                            }
                            else
                                $scope.treePreviewObj["Attr_" + attrOption[i].AttributeID] = false;
                        }
                        else {
                            $scope.treePreviewObj["Attr_" + attrOption[i].AttributeID] = false;
                        }
                        dynhtml += '<div ng-show=\"ShowHideAttributeOnRelation.Attribute_' + attrOption[i].AttributeID + '\" class="control-group treeNode-control-group">';
                        //dynhtml += '<label class="control-label">' + attrOption[i].AttributeCaption + '</label>';
                        dynhtml += '<div class="controls treeNode-controls">';
                        dynhtml += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + attrOption[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + attrOption[i].AttributeID + '"></div>';
                        dynhtml += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + attrOption[i].AttributeID + '">';
                        dynhtml += '<span ng-if="doing_async">...loading...</span>';
                        dynhtml += '<abn-tree tree-filter="filterValue_' + attrOption[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + attrOption[i].AttributeID + '\" accessable="' + attrOption[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                        dynhtml += '</div></div></div>';
                        dynhtml += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + attrOption[i].AttributeID + '\">';
                        dynhtml += '<div class="controls">';
                        dynhtml += '<eu-tree tree-data=\"treesrcdirec.Attr_' + attrOption[i].AttributeID + '\" node-attributeid="' + attrOption[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                        dynhtml += '</div></div>';
                    }
                    else if (attrOption[i].AttributeTypeID == 8) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + attrOption[i].AttributeID] = true;

                        dynhtml += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + attrOption[i].AttributeID + "\" class=\"control-group\"><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + attrOption[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + attrOption[i].AttributeID + "\" id=\"TextSingleLine_" + attrOption[i].AttributeID + "\" placeholder=\"" + attrOption[i].PlaceHolderValue + "\"></div></div>";
                        $scope.fields["TextSingleLine_" + attrOption[i].AttributeID] = attrOption[i].DefaultValue;

                        if ($scope.CTActionSourcedatas != null) {
                            try {
                                var val = $.map($scope.CTActionSourcedatas.ActionTaskSrcValue.AttributeData, function (rel) {
                                    return rel.AttributeID == attrOption[i].AttributeID
                                })[0];
                                if (val != null) {
                                    $scope.fields["TextSingleLine_" + attrOption[i].AttributeID] = val.NodeID[0];
                                }
                            } catch (e) { }

                        }
                    }
                    else if (attrOption[i].AttributeTypeID == 13) {
                        $scope.PercentageVisibleSettings["AttributeId_Levels_" + attrOption[i].AttributeID.toString() + ""] = true;
                        $scope.DropDownTreePricing["AttributeId_Levels_" + attrOption[i].AttributeID.toString() + ""] = attrOption[i].DropDownPricing;
                        dynhtml += "<div drowdowntreepercentagemultiselection data-purpose='entity' data-attributeid=" + attrOption[i].AttributeID.toString() + "></div>";


                    }
                    else if (attrOption[i].AttributeTypeID == 12) {
                        var totLevelCnt1 = attrOption[i].Levels.length;
                        for (var j = 0; j < totLevelCnt1; j++) {

                            if (totLevelCnt1 == 1) {

                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrOption[i].AttributeID + "_" + (j + 1)] = true;

                                $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)] = {};
                                $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                                $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                                $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].data = JSON.parse(attrOption[i].tree).Children;
                                $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].multiple = true;

                                dynhtml += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + attrOption[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><div class=\"controls\"> ";
                                dynhtml += "<input ui-select2 =\"Dropdown.OptionValues" + attrOption[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + attrOption[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + attrOption[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + attrOption[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + attrOption[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";

                                if ($scope.UMActionSourcedatas != null) {
                                    try {
                                        var val = $.grep($scope.UMActionSourcedatas, function (rel) {
                                            return rel.AttributeID == attrOption[i].AttributeID && rel.Level == (j + 1)
                                        })[0];
                                        if (val != null) {
                                            var vals = $.map(val, function (rel) {
                                                return rel.NodeID
                                            });
                                            $scope.fields["MultiSelectDropDown_" + attrOption[i].AttributeID + "_" + (j + 1)] = vals;
                                        }
                                    } catch (e) { }

                                }

                            }
                            else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrOption[i].AttributeID + "_" + (j + 1)] = true;
                                $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)] = {};
                                $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                                $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                                if (j == 0) {
                                    $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].data = JSON.parse(attrOption[i].tree).Children;
                                    $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].multiple = false;


                                    dynhtml += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + attrOption[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><div class=\"controls\">";
                                    dynhtml += "<input ui-select2 =\"Dropdown.OptionValues" + attrOption[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + attrOption[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + attrOption[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + attrOption[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + attrOption[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";

                                } else {
                                    $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].data = [];

                                    if (j == (attrOption[i].Levels.length - 1)) {
                                        $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].multiple = true;

                                        dynhtml += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + attrOption[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><div class=\"controls\"> ";
                                        dynhtml += "<input ui-select2 =\"Dropdown.OptionValues" + attrOption[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + attrOption[i].AttributeID + "\"  ng-change=\"ShowHideAttributeToAttributeRelations(" + attrOption[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + attrOption[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + attrOption[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                    }
                                    else {
                                        $scope.Dropdown["OptionValues" + attrOption[i].AttributeID + "_" + (j + 1)].multiple = false;

                                        dynhtml += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + attrOption[i].AttributeID + "_" + (j + 1) + "\" class=\"control-group\"><div class=\"controls\"> ";
                                        dynhtml += "<input ui-select2 =\"Dropdown.OptionValues" + attrOption[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + attrOption[i].AttributeID + "\" ng-change=\"ShowHideAttributeToAttributeRelations(" + attrOption[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + attrOption[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + attrOption[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                    }
                                    try {
                                        if ($scope.UMActionSourcedatas != null) {
                                            var val = $.grep($scope.UMActionSourcedatas, function (rel) {
                                                return rel.AttributeID == attrOption[i].AttributeID && rel.Level == (j + 1)
                                            })[0];

                                            if (val != null) {
                                                if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                                    var vals = $.map(val, function (rel) {
                                                        return rel.NodeID
                                                    });
                                                    $scope.fields["MultiSelectDropDown_" + attrOption[i].AttributeID + "_" + (j + 1)] = vals;
                                                }
                                                else {
                                                    $scope.fields["MultiSelectDropDown_" + attrOption[i].AttributeID + "_" + (j + 1)] = val.NodeID[0];
                                                }

                                            }
                                        }
                                    } catch (e) { }
                                }
                            }
                        }
                    } else if (attrOption[i].AttributeTypeID == 17) {
                        $scope.fields["ListTagwords_" + attrOption[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + attrOption[i].AttributeID] = true;
                        $scope.OptionObj["tagoption_" + attrOption[i].AttributeID] = [];
                        $scope.tempscope = [];


                        dynhtml += "<div ng-show=\"ShowHideAttributeOnRelation.Attribute_" + attrOption[i].AttributeID + "\" class=\"control-group\">";

                        dynhtml += "<div class=\"controls\">";
                        dynhtml += "<directive-tagwords item-attrid = \"" + attrOption[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + attrOption[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                        dynhtml += "</div></div>";

                        if (attrOption[i].InheritFromParent) {
                        }
                        else {
                        }
                    }

                    if (attrOption[i].IsReadOnly == true) {
                        $scope.EnableDisableControlsHolder["Selection_" + attrOption[i].AttributeID] = true;
                    }
                    else {
                        $scope.EnableDisableControlsHolder["Selection_" + attrOption[i].AttributeID] = false;
                    }
                }
                attr.HtmlControl = dynhtml;
            }
        }

        /************ call destroy event after close the popup **********/

        $scope.$on("$destroy", function () {

            $timeout.cancel(timerObj);

        });

    }
    app.controller("mui.muiexpireaddActionsCtrl", ['$window', '$location', '$timeout', '$scope', '$cookies', '$resource', '$translate', 'ExpireActionService', '$sce', '$compile', '$modalInstance', 'params', muiexpireaddActionsCtrl]);


    app.directive("bindCompiledHtml", function ($compile, $timeout) {
        return {
            template: '<div></div>',
            scope: {
                rawHtml: '=bindCompiledHtml'
            },
            link: function (scope, elem, attrs) {
                scope.$watch('rawHtml', function (value) {
                    if (!value) return;
                    // we want to use the scope OUTSIDE of this directive
                    // (which itself is an isolate scope).
                    var newElem = $compile(value)(scope.$parent);
                    elem.contents().remove();
                    elem.append(newElem);
                });
            }
        };
    });

    function ExpireActionService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetTaskTypes: GetTaskTypes,
            GetEntityTypeAttributeRelationWithLevelsByID: GetEntityTypeAttributeRelationWithLevelsByID,
            GetValidationDationByEntitytype: GetValidationDationByEntitytype,
            GetMember: GetMember,
            GetEntityDetailsByID: GetEntityDetailsByID,
            DeleteEntityCheckListByID: DeleteEntityCheckListByID,
            GetDestinationEntityIdRoleAccess: GetDestinationEntityIdRoleAccess,
            GetExpireActions: GetExpireActions,
            UpdateExpireActionDate: UpdateExpireActionDate,
            ValidExpireActionDate: ValidExpireActionDate,
            CreateExpireAction: CreateExpireAction,
            DeleteExpireAction: DeleteExpireAction,
            getEntityAttributes: getEntityAttributes,
            GetEntityTypeMetadataAttributes: GetEntityTypeMetadataAttributes,
            GetExpireHandlermetadata: GetExpireHandlermetadata,
            GetEntityTypeExpireRoleAccess: GetEntityTypeExpireRoleAccess
        });
        function GetTaskTypes() { var request = $http({ method: "get", url: "api/Metadata/GetTaskTypes/", params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityTypeAttributeRelationWithLevelsByID(ID, ParentID) { var request = $http({ method: "get", url: "api/Metadata/GetEntityTypeAttributeRelationWithLevelsByID/" + ID + "/" + ParentID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetValidationDationByEntitytype(EntityTypeID) { var request = $http({ method: "get", url: "api/Metadata/GetValidationDationByEntitytype/" + EntityTypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetMember(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetMember/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityDetailsByID(EntityID) { var request = $http({ method: "get", url: "api/Planning/GetEntityDetailsByID/" + EntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DeleteEntityCheckListByID(ID) { var request = $http({ method: "delete", url: "api/task/DeleteEntityCheckListByID/" + ID, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function GetDestinationEntityIdRoleAccess(destinationEntityId) { var request = $http({ method: "get", url: "api/task/GetDestinationEntityIdRoleAccess/" + destinationEntityId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function UpdateExpireActionDate(UpdateExpireActionDate) { var request = $http({ method: "post", url: "api/ExpireHandler/UpdateExpireActionDate/", params: { action: "add" }, data: { UpdateExpireActionDate: UpdateExpireActionDate } }); return (request.then(handleSuccess, handleError)); }
        function GetExpireActions(sourceID, AttributeID, ActionID) { var request = $http({ method: "get", url: "api/ExpireHandler/GetExpireActions/" + sourceID + "/" + AttributeID + "/" + ActionID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DeleteExpireAction(ActionsourceId) { var request = $http({ method: "delete", url: "api/ExpireHandler/DeleteExpireAction/" + ActionsourceId, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function getEntityAttributes(entityID) { var request = $http({ method: "get", url: "api/ExpireHandler/GetEntityAttributes/" + entityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityTypeMetadataAttributes(EntityId) { var request = $http({ method: "get", url: "api/ExpireHandler/GetEntityTypeAttributeRelationWithLevelsByID/" + EntityId, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function ValidExpireActionDate(ValidExpireActionDate) { var request = $http({ method: "post", url: "api/ExpireHandler/ValidExpireActionDate/", params: { action: "add" }, data: { ValidExpireActionDate: ValidExpireActionDate } }); return (request.then(handleSuccess, handleError)); }
        function GetExpireHandlermetadata(ActionSourceID) { var request = $http({ method: "get", url: "api/ExpireHandler/GetExpireHandlermetadata/" + ActionSourceID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GetEntityTypeExpireRoleAccess(EntityTypeID) { var request = $http({ method: "get", url: "api/ExpireHandler/GetEntityTypeExpireRoleAccess/" + EntityTypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function CreateExpireAction(CreateExpireAction) { var request = $http({ method: "post", url: "api/ExpireHandler/CreateExpireAction/", params: { action: "add" }, data: { CreateExpireAction: CreateExpireAction } }); return (request.then(handleSuccess, handleError)); }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("ExpireActionService", ['$http', '$q', ExpireActionService]);

})(angular, app);