﻿(function (ng, app) {
    "use strict";

    function muiadmincredentialmanagementCtrl($scope, $location, $resource, $timeout, AdminService, $translate) {
        $scope.ReportCredential = [];
        $scope.reportID = 0;
        $scope.inputAdminUserName = "";
        $scope.DataviewOption = [];
        $scope.CategoryOptions = [];
        $scope.DataviewOptions = 0;
        $timeout(function () {
            ValidateCredential();
            $("#Credential").addClass('notvalidate');
        }, 300);

        function ValidateCredential() {
            var viewValues = [
				['#inputReportUrl', 'presence', 'Please enter the ReportUrl'],
				['#inputAdminUserName', 'presence', 'Please enter the AdminUserName'],
				['#inputAdminPassword', 'presence', 'Please enter the AdminPassword'],
				['#inputViewerUserName', 'presence', 'Please enter the ViewerUserName'],
				['#inputViewerPassword', 'presence', 'Please enter the ViewerPassword'],
				['#Cat', 'presence', 'Please select the Category'],
				['#DataviewOptions', 'presence', 'Please select the Dataview']
            ];
            if ($("#Credential").length > 0) {
                $("#Credential").nod(viewValues, {
                    'delay': 200,
                    'submitBtnSelector': '#btnTemp1',
                    'silentSubmit': 'true'
                });
            }
        }

        function calldataview(selectid) {
            var getDataviews = {};
            getDataviews.AdminUsername = $scope.inputAdminUserName;
            getDataviews.DataViewID = 0;
            AdminService.GetDataviewbyusername(getDataviews).then(function (saveviewscomment) {
                if (saveviewscomment.Response != null && saveviewscomment.Response.length > 0) {
                    $scope.DataviewOption = saveviewscomment.Response;
                    if (selectid > 0) {
                        $scope.SelectedDataview = selectid;
                    } else {
                        $scope.SelectedDataview = "";
                    }
                } else {
                    $scope.SelectedDataview = "0";
                    $scope.DataviewOption = "";
                }
            });
        }
        AdminService.GetReportCredentialByID($scope.ID == undefined ? 0 : $scope.ID).then(function (reportcredential) {
            $scope.ReportCredential = reportcredential.Response;
            var count = reportcredential.Response.length;
            if (count > 0) {
                var cateid = $scope.ReportCredential[count - 1].Category;
                var dataviewid = $scope.ReportCredential[count - 1].DataViewID;
                $scope.reportID = 0;
                $scope.inputReportUrl = $scope.ReportCredential[count - 1].ReportUrl;
                $scope.inputAdminUserName = $scope.ReportCredential[count - 1].AdminUsername;
                $scope.inputAdminPassword = $scope.ReportCredential[count - 1].AdminPassword;
                $scope.inputViewerUserName = $scope.ReportCredential[count - 1].ViewerUsername;
                $scope.inputViewerPassword = $scope.ReportCredential[count - 1].ViewerPassword;
                $scope.CategoryOptions = $scope.ReportCredential[count - 1].Category;
                $scope.ID = $scope.ReportCredential[count - 1].ID;
                $scope.reportID = $scope.ReportCredential[count - 1].ID;
                validateuseronload(cateid);
                calldataview(dataviewid);
            }
        });
        $timeout(function () {
            calldataview(0);
        }, 10);
        $scope.save = function () {
            $("#btnTemp1").click();
            $("#Credential").removeClass('notvalidate');
            if ($("#Credential .error").length > 0) {
                return false;
            }
            var res = {};
            res.ReportUrl = $scope.inputReportUrl;
            res.AdminUsername = $scope.inputAdminUserName;
            res.AdminPassword = $scope.inputAdminPassword;
            res.ViewerUsername = $scope.inputViewerUserName;
            res.ViewerPassword = $scope.inputViewerPassword;
            res.Category = $scope.SelectedCategory;
            res.DataView = $scope.SelectedDataview;
            res.ID = $scope.reportID;
            AdminService.validateReportCredential(res).then(function (validatereportcredentials) {
                if (validatereportcredentials.Response == 1) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4185.Caption'));
                } else if (validatereportcredentials.Response == 2) {
                    NotifyError($translate.instant('LanguageContents.Res_4384.Caption'));
                } else if (validatereportcredentials.Response == 3) {
                    NotifyError($translate.instant('LanguageContents.Res_4386.Caption'));
                } else if (validatereportcredentials.Response == 4) {
                    NotifyError($translate.instant('LanguageContents.Res_4506.Caption'));
                } else if (validatereportcredentials.Response == 5) {
                    NotifyError($translate.instant('LanguageContents.Res_4507.Caption'));
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4385.Caption'));
                    return false;
                }
            });
        }
        $scope.ReportServerLogin = function () {
            var res = {};
            res.ReportUrl = $scope.inputReportUrl;
            res.ViewerUsername = $scope.inputViewerUserName;
            res.ViewerPassword = $scope.inputViewerPassword;
            AdminService.ReportLogin(res).then(function (savereportcredentials) {
                if (savereportcredentials.Response != null) {
                    $scope.CategoryOptions = savereportcredentials.Response;
                } else {
                    $scope.CategoryOptions = 0;
                    NotifyError($translate.instant('LanguageContents.Res_4386.Caption'));
                }
            });
        }
        $("#inputReportUrl").focusout(function () {
            if ($scope.inputAdminUserName != undefined && $scope.inputReportUrl != undefined && $scope.inputAdminPassword != undefined && $scope.inputViewerUserName != undefined && $scope.inputViewerPassword != undefined) {
                if ($scope.inputAdminUserName.length > 0 && $scope.inputReportUrl.length > 0 && $scope.inputAdminPassword.length > 0) {
                    $("#Credential").addClass('notvalidate');
                    validateadmin();
                    calldataview(0);
                }
                if ($scope.inputViewerUserName.length > 0 && $scope.inputReportUrl.length > 0 && $scope.inputViewerPassword.length > 0) {
                    $("#Credential").addClass('notvalidate');
                    validateuser();
                    validateuseronload(0);
                }
            }
        });
        $("#inputAdminUserName").focusout(function () {
            if ($scope.inputAdminUserName != undefined && $scope.inputReportUrl != undefined && $scope.inputAdminPassword != undefined) {
                if ($scope.inputAdminUserName.length > 0 && $scope.inputReportUrl.length > 0 && $scope.inputAdminPassword.length > 0) {
                    $("#Credential").addClass('notvalidate');
                    validateadmin();
                    calldataview(0);
                }}
        });
        $("#inputAdminPassword").focusout(function () {
            if ($scope.inputAdminUserName != undefined && $scope.inputReportUrl != undefined && $scope.inputAdminPassword != undefined) {
            if ($scope.inputAdminUserName.length > 0 && $scope.inputReportUrl.length > 0 && $scope.inputAdminPassword.length > 0) {
                $("#Credential").addClass('notvalidate');
                validateadmin();
                calldataview(0);
            }
        }
        });
        $("#inputViewerUserName").focusout(function () {
            $("#Credential").addClass('notvalidate');
            if ($scope.inputViewerUserName != undefined && $scope.inputReportUrl != undefined && $scope.inputViewerPassword != undefined) {
                if ($scope.inputViewerUserName.length > 0 && $scope.inputReportUrl.length > 0 && $scope.inputViewerPassword.length > 0) {
                    validateuser();
                    validateuseronload(0);
                }
            }
        });
        $("#inputViewerPassword").focusout(function () {
            $("#Credential").addClass('notvalidate');
            if ($scope.inputViewerUserName != undefined && $scope.inputReportUrl != undefined && $scope.inputViewerPassword != undefined) {
                if ($scope.inputViewerUserName.length > 0 && $scope.inputReportUrl.length > 0 && $scope.inputViewerPassword.length > 0) {
                    validateuser();
                    validateuseronload(0);
                }
            }
        });

        function validateadmin() {
            var res = {};
            res.ReportUrl = $scope.inputReportUrl;
            res.ViewerUsername = $scope.inputAdminUserName;
            res.ViewerPassword = $scope.inputAdminPassword;
            AdminService.ReportLogin(res).then(function (savereportcredentialsadmin) {
                if (savereportcredentialsadmin.Response != null) { } else { }
            });
        }
        $timeout(function () {
            validateuseronload(0);
        }, 10);

        function validateuseronload(selectid) {
            var res = {};
            res.ReportUrl = $scope.inputReportUrl;
            res.ViewerUsername = $scope.inputViewerUserName;
            res.ViewerPassword = $scope.inputViewerPassword;
            AdminService.ReportLogin(res).then(function (savereportcredentialsuser) {
                if (savereportcredentialsuser.Response != null && savereportcredentialsuser.Response.length > 0) {
                    $scope.CategoryOptions = savereportcredentialsuser.Response;
                    if (selectid > 0) {
                        $scope.SelectedCategory = selectid;
                    } else {
                        $scope.SelectedCategory = "";
                    }
                } else {
                    $scope.SelectedCategory = "0";
                    $scope.CategoryOptions = "";
                }
            });
        }

        function validateuser() {
            var res = {};
            res.ReportUrl = $scope.inputReportUrl;
            res.ViewerUsername = $scope.inputViewerUserName;
            res.ViewerPassword = $scope.inputViewerPassword;
            AdminService.ReportLogin(res).then(function (savereportcredentialsuser) {
                if (savereportcredentialsuser.Response != null) {
                    $scope.CategoryOptions = savereportcredentialsuser.Response;
                } else { }
            });
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.credentialmanagementCtrl']"));
        });

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.controller("mui.admin.credentialmanagementCtrl", ['$scope', '$location', '$resource', '$timeout', 'AdminService', '$translate', muiadmincredentialmanagementCtrl]);
})(angular, app);