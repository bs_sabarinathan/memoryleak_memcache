﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Task.Interface
{
    public interface ITaskTemplate
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }



        string Description
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        IList<ITaskList> TaskListDetails
        {
            get;
            set;
        }

        IList<ITasktemplateCondition> TempCondDetails
        {
            get;
            set;
        }

        string TemplateCriteria
        {
            get;
            set;

        }

        #endregion
    }
}
