using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// CalenderTabDao object for table 'PM_CalenderTab'.
	/// </summary>
	
	public partial class CalenderTabDao : BaseDao, ICloneable 
	{
		#region Member Variables

        protected int _id;
		protected int _calenderid;
		protected int _customtabid;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public CalenderTabDao() {}

        public CalenderTabDao(int pId, int pCalenderid, int pCustomTabid)
		{
            this._id = pId;
            this._calenderid = pCalenderid; 
			this._customtabid = pCustomTabid; 			
		}
		
		#endregion
		
		#region Public Properties
		
        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }
        }
		public virtual int Calenderid
		{
			get { return _calenderid; }
            set { _bIsChanged |= (_calenderid != value); _calenderid = value; }
			
		}
		
		public virtual int CustomTabid
		{
			get { return _customtabid; }
            set { _bIsChanged |= (_customtabid != value); _customtabid = value; }
			
		}
        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string CalenderTabDao = "CalenderTabDao";			
            public const string Id = "Id";
            public const string Calenderid = "Calenderid";
            public const string CustomTabid = "CustomTabid";						
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string CalenderTabDao = "CalenderTabDao";			
            public const string Id = "Id";
            public const string Calenderid = "Calenderid";
            public const string CustomTabid = "CustomTabid";							
		}

		#endregion
		
		
	}
	
}
