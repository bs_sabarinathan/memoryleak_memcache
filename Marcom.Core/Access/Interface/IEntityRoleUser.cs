using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Access.Interface
{
    /// <summary>
    /// IEntityRoleUser interface for table 'AM_Entity_Role_User'.
    /// </summary>
    public interface IEntityRoleUser
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }
        
        int Entityid
        {
            get;
            set;

        }

        int Roleid
        {
            get;
            set;

        }

        int Userid
        {
            get;
            set;

        }

        bool IsInherited
        {
            get;
            set;

        }

        int InheritedFromEntityid
        {
            get;
            set;

        }

        string UserName
        {
            get;
            set;

        }

        string DepartmentName
        {
            get;
            set;

        }

        string Role
        {
            get;
            set;

        }

        string Title
        {
            get;
            set;

        }

        string UserEmail
        {
            get;
            set;

        }

        string InheritedFromEntityName
        {
            get;
            set;

        }

        string QuickInfo1
        {
            get;
            set;

        }

        string QuickInfo2
        {
            get;
            set;

        }
        string QuickInfo1AttributeCaption
        {
            get;
            set;

        }

        string QuickInfo2AttributeCaption
        {
            get;
            set;

        }

        String ShortText
        {
            get;
            set;
        }

        String ColorCode
        {
            get;
            set;
        }

        int BackendRoleID
        {
            get;
            set;
        }

        string FirstName
        {
            get;
            set;
        }

        string LastName
        {
            get;
            set;
        }

        #endregion
    }
}
