﻿using BrandSystems.Marcom.Core.Interface;
using MarcomDalimLib.ExceptionHandling.Rpc;
using MarcomDalimLib.RPC.Connector;
using MarcomDalimLib.Services.Coins.Base;
using MarcomDalimLib.Services.Coins.Bitcoin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using MarcomDalimLib.Auxiliary;
using MarcomDalimLib.Responses;
using System.Net;
using Newtonsoft.Json.Linq;
using BrandSystems.Marcom.Core.Managers.Proxy;
using System.Collections;
using BrandSystems.Marcom.Core.Utility;
using System.Xml;
using System.Xml.Linq;
using BrandSystems.Marcom.Dal.Task.Model;
using BrandSystems.Marcom.Dal.User.Model;
using System.IO;
using Newtonsoft.Json;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Core.AmazonStorageHelper;


namespace BrandSystems.Marcom.Core.Managers
{
    internal partial class ExternalTaskManager : IManager
    {
        void IManager.Initialize(IMarcomManager marcomManager)
        {
            //throw new System.NotImplementedException();
        }

        void IManager.CommitCaches()
        {
            // throw new System.NotImplementedException();
        }

        void IManager.RollbackCaches()
        {
            //throw new System.NotImplementedException();
        }
        private static ExternalTaskManager instance = new ExternalTaskManager();
        internal static ExternalTaskManager Instance
        {
            get { return instance; }
        }
        /// <summary>
        /// <summary>
        /// The instance
        /// </summary>

        //private static readonly IDalimBaseService CoinService = new DalimClientService(useTestnet: true);

        public string GetTenantFilePath(int TenantID)
        {
            //Get the tenant related file path
            TenantSelection tfp = new TenantSelection();
            return tfp.GetTenantFilePath(TenantID);
        }

        public void TestDalimRPCConnector(ExternalTaskManagerProxy proxy, int taskID, int assetId)
        {
            try
            {

                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID)) // initialize the transaction
                {
                    IList taskDetails = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery("SELECT ID, Name, DueDate,Description FROM [dbo].[TM_EntityTask] WHERE ID = " + taskID); // assets attached for this task
                    string TaskName = null;
                    string Taskdescription = null;
                    string deadline = null;
                    if (taskDetails != null && taskDetails.Count > 0)
                    {
                        TaskName = Convert.ToString((string)((System.Collections.Hashtable)(taskDetails[0]))["Name"]);
                        Taskdescription = Convert.ToString((string)((System.Collections.Hashtable)(taskDetails[0]))["Description"]);
                        if (((System.Collections.Hashtable)(taskDetails[0]))["DueDate"] != null)
                        {
                            deadline = DateTime.Parse(Convert.ToString((DateTime)((System.Collections.Hashtable)(taskDetails[0]))["DueDate"])).ToString("yyyy-MM-dd hh:mm");
                        }

                    }

                    IList taskAssigneesList = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery("SELECT uu.ID, uu.FirstName, uu.LastName, uu.Email FROM   UM_User uu WHERE  uu.ID IN (SELECT ttm.UserID FROM TM_Task_Members ttm WHERE  ttm.TaskID = " + taskID + " AND ttm.RoleID = 4)"); // assets attached for this task

                    string assigneermail = "";
                    foreach (var item in taskAssigneesList)
                    {
                        // assigneermail = item.
                        if (assigneermail != "")
                        {
                            assigneermail = assigneermail + ";";
                        }


                        var FirstName = Convert.ToString((string)((System.Collections.Hashtable)(item))["FirstName"]);
                        var LastName = Convert.ToString((string)((System.Collections.Hashtable)(item))["LastName"]);
                        var Email = Convert.ToString((string)((System.Collections.Hashtable)(item))["Email"]);

                        assigneermail = assigneermail + Email;
                    }
                    //if (proofName == null)
                    //{
                    //    proofName = uploadfilename;
                    //}
                    //Console.Write("\n\nConnecting to {0} {1}Net via RPC at {2}...", CoinService.Parameters.CoinLongName, (CoinService.Parameters.UseTestnet ? "Test" : "Main"), CoinService.Parameters.SelectedDaemonUrl);
                    string response = "";
                    //response = RawRpcConnector.MakeGetRequest("[{'id':1,'method' : 'admin.login'},{'id':2,'method':'production.list','params':{'class':'Customer','ID':664}},{'id':3,'method':'production.list','params':{'class':'Folder','ID':28432}},{'id':4,'method' : 'directory.getOrganization','params':{'ID':'thelab'}},{'id':5,'method':'document.get','params':{'ID':32328,'path':'/My Customer/BS:Test/Documents','withXMP':true}},{'id':6,'method':'job.get','params':{'ID':32315,'withXMP':true}},{'id':7,'method':'admin.logout'}]", "http://198.46.125.167:8080/Esprit/public/Interface/rpc", "brandsystems", "development1");
                    response = RawRpcConnector.MakeGetRequest("[{'id':1,'method' : 'admin.login'},{'id':2,'method':'job.get','params':{'ID':32315,'withXMP':false}},{'id':3,'method':'document.approvalStatus','params':{'ID':32328}},{'id':4,'method':'document.approvalStatus','params':{'ID':32336}},{'id':5,'method':'admin.logout'}]", "http://198.46.125.167:8080/Esprit/public/Interface/rpc", "brandsystems", "development1");
                    //response = RawRpcConnector.MakeGetRequest("[{'id':1,'method' : 'admin.login'},{'id':2,'method':'document.create','params':{'jobID':32315,'name': 'BS API TEST3','description':'description','URL':'http://www.adobe.com/content/dam/Adobe/en/devnet/acrobat/pdfs/pdf_open_parameters.pdf','moveFile':false}},{'id':3,'method':'admin.logout'}]", "http://198.46.125.167:8080/Esprit/public/Interface/rpc", "brandsystems", "development1");
                    //response = RawRpcConnector.MakeGetRequest("[{'id':1,'method' : 'admin.login'},{'id':2,'method':'document.create','params':{'jobID':32315,'name': '" + TaskName + "','description':'" + Taskdescription + "','URL':'http://dev.brandsystems.in/test.pdf','moveFile':true}},{'id':3,'method':'admin.logout'}]", "http://198.46.125.167:8080/Esprit/public/Interface/rpc", "brandsystems", "development1");
                    // response = RawRpcConnector.MakeGetRequest("[{'id':1,'method' : 'admin.login'},{'id':2,'method':'document.create','params':{'jobID':32315,'name': '" + TaskName + "','description':'" + Taskdescription + "','metadatas':[['Taskmetadate','taskdescp','taskdsample']], 'URL':'http://dev.brandsystems.in/test.pdf','moveFile':true}},{'id':3,'method':'admin.logout'}]", "http://198.46.125.167:8080/Esprit/public/Interface/rpc", "brandsystems","development1");
                    response = RawRpcConnector.MakeGetRequest("[{'id':1,'method' : 'admin.login'},{'id':2,'method':'document.create','params':{'jobID':32315,'name': '" + TaskName + "','description':'" + Taskdescription + "','metadatas':[['NS1','KEY1','VALUE1'],['NS2','KEY2','VALUE2']], 'URL':'http://dev.brandsystems.in/test.pdf','moveFile':true}},{'id':3,'method':'admin.logout'}]", "http://198.46.125.167:8080/Esprit/public/Interface/rpc", "brandsystems", "development1");
                    //JObject routeData = JObject.Parse(response);
                    JArray Ja = JArray.Parse(response);
                    JObject documentIDarrayObject = (JObject)Ja[1];
                    int newdocumantid = 0;
                    newdocumantid = Convert.ToInt32(documentIDarrayObject["result"]["ID"].ToString());
                    response = RawRpcConnector.MakeGetRequest("[{'id':1,'method' : 'admin.login'},{'id':2,'method':'document.get','params':{'ID':" + newdocumantid + ",'withXMP':false}},{'id':3,'method':'admin.logout'}]", "http://198.46.125.167:8080/Esprit/public/Interface/rpc", "brandsystems", "development1");
                    Console.Write("\n\n method result {0}", response);
                    Console.ReadLine();

                    tx.Commit();
                }

            }
            catch (RpcInternalServerErrorException exception)
            {
                Int32 errorCode = 0;
                String errorMessage = String.Empty;

                if (exception.RpcErrorCode.GetHashCode() != 0)
                {
                    errorCode = exception.RpcErrorCode.GetHashCode();
                    errorMessage = exception.RpcErrorCode.ToString();
                }

                Console.WriteLine("[Failed] {0} {1} {2}", exception.Message, errorCode != 0 ? "Error code: " + errorCode : String.Empty, !String.IsNullOrWhiteSpace(errorMessage) ? errorMessage : String.Empty);
            }
            catch (Exception exception)
            {
                Console.WriteLine("[Failed]\n\nPlease check your configuration and make sure that the daemon is up and running and that it is synchronized. \n\nException: " + exception);
            }
        }

        public Dictionary<string, string> GetDocumentViewerInfo(ExternalTaskManagerProxy proxy, int taskId, string usrname)
        {
            try
            {

                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID)) // initialize the transaction
                {
                    Dictionary<string, string> externalInfo = new Dictionary<string, string>();

                    IList taskDetails = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery("SELECT DalimID FROM [dbo].[TM_EntityTask] WHERE ID = " + taskId); // assets attached for this task
                    if (taskDetails != null && taskDetails.Count > 0)
                    {
                        externalInfo.Add("DalimID", Convert.ToString((int)((System.Collections.Hashtable)(taskDetails[0]))["DalimID"]));
                    }
                    XDocument adminXmlDoc = MarcomCache<XDocument>.ReadXDocument(xmlType.Admin, proxy.MarcomManager.User.TenantID);
                    XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, proxy.MarcomManager.User.TenantID);
                    string xelementName = "DalimDefaultSettings_Table";
                    var usrprofile = xelementFilepath.Element(xelementName).Element("DalimSettings").Element("defaultProfile");
                    var externalUrl = xelementFilepath.Element(xelementName).Element("DalimSettings").Element("ExternalDalimUrl");

                    externalInfo.Add("userprofile", usrprofile.Value.ToString());
                    externalInfo.Add("externalUrl", externalUrl.Value.ToString());

                    List<string> usr = new List<string>();
                    string str = "SELECT top 1 deu.UserName,deu.[Password] FROM DM_External_User deu WHERE deu.UserName = '" + usrname + "' ORDER BY deu.Id DESC ";

                    var usrlist = tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery(str.ToString());

                    if (usrlist.Count > 0)
                    {
                        foreach (var obj in usrlist.Cast<Hashtable>().ToList())
                        {
                            externalInfo.Add("UserName", Convert.ToString(obj["UserName"]));
                            externalInfo.Add("Password", Convert.ToString(obj["Password"]));
                        }
                    }
                    tx.Commit();
                    return externalInfo;
                }
            }
            catch { }
            return null;
        }


        #region Dalim User call back

        public bool UpdateDalimUserTaskStatus(ExternalTaskManagerProxy proxy, int DalimID, int dalimstepId, int stepStatus, string stepName)
        {
            int prevPhaseID = 0;
            int currentPhaseStepId = 0;
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID)) // initialize the transaction
                {
                    //find the task id from the dalim document Id
                    EntityTaskDao tDao = new EntityTaskDao();
                    tDao = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].Query<EntityTaskDao>().Where(a => a.DalimID == DalimID).FirstOrDefault();
                    prevPhaseID = tDao.CurrentPhaseId;
                    //get all the step members from the current step

                    StringBuilder bldr = new StringBuilder();

                    bldr.AppendLine("select tee.ExternalId from TM_EntityTask tet ");
                    bldr.AppendLine("inner join TM_Entitytaskphases tep ");
                    bldr.AppendLine("on tet.ID = tep.EntityID ");
                    bldr.AppendLine("inner join TM_Entitytasksteps tes ");
                    bldr.AppendLine("on tes.PhaseId = tep.ID ");
                    bldr.AppendLine("Inner join TM_ExternalTaskStepInfo tee ");
                    bldr.AppendLine("on tes.ID = tee.MarcomStepId ");
                    bldr.AppendLine("where tet.ID = " + tDao.ID + " and tes.Name = '" + stepName + "' ");

                    var ExternalstepId = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery(bldr.ToString());
                    if (ExternalstepId.Count > 0)
                    {
                        dalimstepId = (int)((System.Collections.Hashtable)(ExternalstepId)[0])["ExternalId"];
                    }
                    else
                    {
                        tx.Commit();
                        return false;
                    }

                    currentPhaseStepId = (from data in tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].Query<EntitytaskphasesDao>() where data.EntityID == tDao.ID && data.ID == tDao.CurrentPhaseId select data.StatusCode).FirstOrDefault();

                    bldr.Clear();
                    bldr.AppendLine(" UPDATE TM_Task_Members ");
                    bldr.AppendLine(" SET    ApprovalStatus = " + stepStatus + " ");
                    bldr.AppendLine(" WHERE  id IN (SELECT ttm.ID ");
                    bldr.AppendLine("               FROM   TM_Task_Members ttm ");
                    bldr.AppendLine("               WHERE  ttm.TaskID = " + tDao.ID + " ");
                    bldr.AppendLine("                      AND ttm.StepId IN (SELECT id ");
                    bldr.AppendLine("                                         FROM   TM_Entitytasksteps te ");
                    bldr.AppendLine("                                         WHERE  te.PhaseId IN (SELECT id ");
                    bldr.AppendLine("                                                               FROM    ");
                    bldr.AppendLine("                                                                      TM_Entitytaskphases  ");
                    bldr.AppendLine("                                                                      te2 ");
                    bldr.AppendLine("                                                               WHERE  te2.EntityID =  ");
                    bldr.AppendLine("                                                                      " + tDao.ID + ") ");
                    bldr.AppendLine("                                                AND te.id IN (SELECT tetsi.MarcomStepId ");
                    bldr.AppendLine("                                                              FROM    ");
                    bldr.AppendLine("                                                                     TM_ExternalTaskStepInfo  ");
                    bldr.AppendLine("                                                                     tetsi ");
                    bldr.AppendLine("                                                              WHERE  tetsi.ExternalId =  ");
                    bldr.AppendLine("                                                                     " + dalimstepId + "))) ");
                    bldr.AppendLine("        AND TaskID = " + tDao.ID + " ");
                    tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery(bldr.ToString());
                    tx.Commit();

                    //update the task status step by step
                    _dalim_MarcomTaskStatus(proxy, tDao.ID, dalimstepId, stepStatus);
                    //get the marcom StepID
                    int marcomStepID = _dalim_MarcomGetstepID(proxy, tDao.ID, dalimstepId);
                    proxy.MarcomManager.TaskManager.UpdateTaskNotification(tDao.ID, stepStatus, tDao.EntityID, marcomStepID, 0, false, prevPhaseID, currentPhaseStepId);

                    return true;

                }
            }
            catch
            {
                return false;
            }

        }

        public bool _dalim_MarcomTaskStatus(ExternalTaskManagerProxy proxy, int taskid, int dalimStepid, int stepStatus)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID)) // initialize the transaction
                {
                    //find the task id from the dalim document Id
                    EntityTaskDao tDao = new EntityTaskDao();
                    tDao = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].Query<EntityTaskDao>().Where(a => a.ID == taskid).FirstOrDefault();

                    //find the marcomstepid from dalim step

                    StringBuilder qry = new StringBuilder();
                    qry.AppendLine(" SELECT  tetsi.MarcomStepId FROM TM_EntityTask tet INNER JOIN TM_Entitytaskphases te  ");
                    qry.AppendLine(" ON tet.ID = te.EntityID INNER JOIN TM_Entitytasksteps te2 ");
                    qry.AppendLine(" ON te.ID = te2.PhaseId INNER JOIN TM_ExternalTaskStepInfo tetsi ");
                    qry.AppendLine(" ON te2.ID = tetsi.MarcomStepId ");
                    qry.AppendLine("WHERE tet.id = " + taskid + " AND tetsi.ExternalId = " + dalimStepid + "");
                    qry.AppendLine("");

                    var stepDao = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery(qry.ToString());

                    int marcomStepID = 0;
                    if (stepDao != null)
                    {
                        marcomStepID = Convert.ToInt32(stepDao.Cast<Hashtable>().ToList()[0]["MarcomStepId"]);
                    }

                    tx.Commit();

                    UpdateApprovalFlowStatus(proxy, marcomStepID, tDao.ID, stepStatus);

                    UpdateTaskVersionActivePhase(proxy, tDao.ID);


                    proxy.MarcomManager.TaskManager.UpdateTaskVersionDetails(tDao.ID);

                    return true;

                }
            }
            catch
            {
                return false;
            }

        }

        public int UpdateApprovalFlowStatus(ExternalTaskManagerProxy proxy, int stepid, int taskID, int status)
        {
            try
            {

                EntityTaskDao EntityTaskDao = new EntityTaskDao();
                IList<EntityTaskDao> Itask = new List<EntityTaskDao>();
                TaskMembersDao TaskMembersDao = new TaskMembersDao();
                IList<TaskMembersDao> Itaskmember = new List<TaskMembersDao>();
                IList<EntitytaskphasesDao> itaskPhase = new List<EntitytaskphasesDao>();
                EntitytaskstepsDao stepdao = new EntitytaskstepsDao();

                using (ITransaction txinner = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    EntityTaskDao = (from item in txinner.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].Query<EntityTaskDao>() where item.ID == taskID select item).FirstOrDefault();
                    status = DecideApprovalFlowStatus(txinner, proxy, taskID);
                    EntityTaskDao.TaskStatus = status;
                    Itask.Add(EntityTaskDao);
                    txinner.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].Save<EntityTaskDao>(Itask);
                    txinner.Commit();
                }
                return status;

            }

            catch (Exception ex)
            {
                return status;
            }
        }

        public int DecideApprovalFlowStatus(ITransaction tx, ExternalTaskManagerProxy proxy, int taskId)
        {


            try
            {
                var taskdao = tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Query<EntityTaskDao>().Where(a => a.ID == taskId).Select(a => a).FirstOrDefault();
                IList<Entitytaskphases> iePhase = new List<Entitytaskphases>(); IList<Entitytasksteps> ieSteps = new List<Entitytasksteps>();
                IList<TaskMembersDao> ieMembers = new List<TaskMembersDao>(); IList<TaskMembersDao> ietmpMembers = new List<TaskMembersDao>(); IList<Entitytasksteps> ietmpSteps = new List<Entitytasksteps>();
                int memCount = 0;
                iePhase = tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Query<EntitytaskphasesDao>().Where(a => a.EntityID == taskId).Select(a => new Entitytaskphases { ID = a.ID }).ToList<Entitytaskphases>();
                ieSteps = (from t1 in iePhase
                           join t2 in tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Query<EntitytaskstepsDao>() on t1.ID equals t2.PhaseId
                           select new Entitytasksteps { ID = t2.ID, PhaseId = t2.PhaseId, MinApproval = t2.MinApproval }).ToList<Entitytasksteps>();
                ieMembers = (from t1 in ieSteps
                             join t2 in tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Query<TaskMembersDao>() on t1.ID equals t2.StepId
                             where t2.RoleID != 1
                             select t2).ToList<TaskMembersDao>();

                foreach (var val in ieSteps)
                {
                    ietmpMembers = ieMembers.Where(a => a.StepId == val.ID).Select(a => a).ToList<TaskMembersDao>();
                    if (ietmpMembers.Count > 0)
                    {
                        memCount = ietmpMembers.Where(a => a.ApprovalStatus == 5).Count();
                        if (memCount > 0)
                        {
                            val.Status = 5; //check for anyone rejected
                            continue;
                        }
                        memCount = ietmpMembers.Where(a => a.ApprovalStatus == 3).Count();
                        if (val.MinApproval > 0)
                        {
                            if (memCount >= val.MinApproval)
                            {
                                val.Status = 3;  //check for the step reached minimum approval count
                                continue;
                            }
                        }
                        memCount = ietmpMembers.Where(a => a.ApprovalStatus == 3).Count();
                        if (memCount == ietmpMembers.Count)
                        {
                            val.Status = 3; //check for all assignees approved
                            continue;
                        }
                        memCount = ietmpMembers.Where(a => a.ApprovalStatus == 4).Count();
                        if (memCount == ietmpMembers.Count)
                        {
                            val.Status = 4;//check for all assignees unable to complete
                            continue;
                        }
                        memCount = ietmpMembers.Where(a => a.ApprovalStatus == null).Count();
                        if (memCount == ietmpMembers.Count)
                        {
                            val.Status = 0;//check for whether nobody starts reply
                            continue;
                        }
                        memCount = ietmpMembers.Where(a => a.ApprovalStatus == 3).Count();
                        if (memCount == ietmpMembers.Count)
                        {
                            val.Status = 3;//check for if nobody rejected and  all people approved
                            continue;
                        }
                        else if (ietmpMembers.Count > 0)
                        {
                            val.Status = 1;
                            continue;
                        }
                    }
                    else
                        val.Status = 0;
                }

                foreach (var val in iePhase)
                {
                    ietmpSteps = ieSteps.Where(a => a.PhaseId == val.ID).Select(a => a).ToList<Entitytasksteps>();
                    if (ietmpSteps.Count > 0)
                    {
                        memCount = ietmpSteps.Where(a => a.Status == 5).Count();
                        if (memCount > 0)
                        {
                            val.Status = 5; //check for anyone rejected
                            continue;
                        }
                        memCount = ietmpSteps.Where(a => a.Status == 3).Count();
                        if (memCount == ietmpSteps.Count)
                        {
                            val.Status = 3; //check for all assignees approved
                            continue;
                        }
                        memCount = ietmpSteps.Where(a => a.Status == 4).Count();
                        if (memCount == ietmpSteps.Count)
                        {
                            val.Status = 4;//check for all assignees unable to complete
                            continue;
                        }
                        memCount = ietmpSteps.Where(a => a.Status == null).Count();
                        if (memCount == ietmpSteps.Count)
                        {
                            val.Status = 0;//check for whether nobody starts reply
                            continue;
                        }
                        else if (ietmpSteps.Count > 0)
                        {
                            val.Status = 1;
                            continue;
                        }

                    }
                    else
                        val.Status = 0;
                }


                memCount = iePhase.Where(a => a.Status == 5).Count();
                if (memCount > 0)
                    return 5; //check for anyone rejected
                memCount = iePhase.Where(a => a.Status == 3).Count();
                if (memCount == iePhase.Count)
                    return 3; //check for all assignees approved
                memCount = iePhase.Where(a => a.Status == 4).Count();
                if (memCount == iePhase.Count)
                    return 4;//check for all assignees unable to complete
                memCount = iePhase.Where(a => a.Status == null).Count();
                if (memCount == iePhase.Count)
                    return 0;//check for whether nobody starts reply
                else if (iePhase.Count > 0)
                    return 1;
                else return 0;

            }

            catch (Exception ex)
            {
                return 0;
            }
        }

        #endregion

        public int UpdateTaskVersionActivePhase(ExternalTaskManagerProxy proxy, int taskId)
        {


            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {

                    var taskdao = tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Query<EntityTaskDao>().Where(a => a.ID == taskId).Select(a => a).FirstOrDefault();
                    IList<Entitytaskphases> iePhase = new List<Entitytaskphases>(); IList<EntitytaskphasesDao> iPhaseDao = new List<EntitytaskphasesDao>(); IList<EntitytaskstepsDao> ieStepsDao = new List<EntitytaskstepsDao>(); IList<Entitytasksteps> ieSteps = new List<Entitytasksteps>();
                    IList<TaskMembersDao> ieMembers = new List<TaskMembersDao>(); IList<TaskMembersDao> ietmpMembers = new List<TaskMembersDao>(); IList<Entitytasksteps> ietmpSteps = new List<Entitytasksteps>(); TaskMembersDao memDao = new TaskMembersDao();
                    int memCount = 0;
                    iPhaseDao = tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Query<EntitytaskphasesDao>().Where(a => a.EntityID == taskId).ToList<EntitytaskphasesDao>();
                    iePhase = tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Query<EntitytaskphasesDao>().Where(a => a.EntityID == taskId).Select(a => new Entitytaskphases { ID = a.ID }).ToList<Entitytaskphases>();
                    ieStepsDao = (from t1 in iePhase
                                  join t2 in tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Query<EntitytaskstepsDao>() on t1.ID equals t2.PhaseId
                                  select t2).ToList<EntitytaskstepsDao>();
                    ieSteps = (from t1 in iePhase
                               join t2 in tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Query<EntitytaskstepsDao>() on t1.ID equals t2.PhaseId
                               select new Entitytasksteps { ID = t2.ID, PhaseId = t2.PhaseId, MinApproval = t2.MinApproval }).ToList<Entitytasksteps>();
                    ieMembers = (from t1 in ieSteps
                                 join t2 in tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Query<TaskMembersDao>() on t1.ID equals t2.StepId
                                 where t2.RoleID != 1
                                 select t2).ToList<TaskMembersDao>();


                    IList<EntitytaskphasesDao> iePhaseOutput = new List<EntitytaskphasesDao>(); EntitytaskphasesDao phaseObj = new EntitytaskphasesDao(); IList<EntitytaskstepsDao> ieStepsOutput = new List<EntitytaskstepsDao>(); EntitytaskstepsDao stepObj = new EntitytaskstepsDao();
                    IList<TaskMembersDao> ieMembersOutput = new List<TaskMembersDao>(); IList<TaskMembersDao> ietmpMembersOutput = new List<TaskMembersDao>();

                    foreach (var val in ieSteps)
                    {
                        stepObj = new EntitytaskstepsDao();
                        stepObj = ieStepsDao.Where(a => a.ID == val.ID).Select(a => a).FirstOrDefault();

                        ietmpMembers = ieMembers.Where(a => a.StepId == val.ID).Select(a => a).ToList<TaskMembersDao>();
                        if (ietmpMembers.Count > 0)
                        {
                            memCount = ietmpMembers.Where(a => a.ApprovalStatus == 5).Count();
                            if (memCount > 0)
                            {
                                val.Status = 5; //check for anyone rejected
                                stepObj.StatusCode = 5;
                                ieStepsOutput.Add(stepObj);
                                continue;
                            }
                            memCount = ietmpMembers.Where(a => a.ApprovalStatus == 3).Count();
                            if (val.MinApproval > 0)
                            {
                                if (memCount >= val.MinApproval)
                                {
                                    val.Status = 3;  //check for the step reached minimum approval count
                                    stepObj.StatusCode = 3;
                                    ieStepsOutput.Add(stepObj);
                                    continue;
                                }
                            }
                            memCount = ietmpMembers.Where(a => a.ApprovalStatus == 3).Count();
                            if (memCount == ietmpMembers.Count)
                            {
                                val.Status = 3; //check for all assignees approved
                                ieStepsOutput.Add(stepObj);
                                continue;
                            }
                            memCount = ietmpMembers.Where(a => a.ApprovalStatus == 4).Count();
                            if (memCount == ietmpMembers.Count)
                            {
                                val.Status = 4;//check for all assignees unable to complete
                                stepObj.StatusCode = 4;
                                ieStepsOutput.Add(stepObj);
                                continue;
                            }
                            memCount = ietmpMembers.Where(a => a.ApprovalStatus == null).Count();
                            if (memCount == ietmpMembers.Count)
                            {
                                val.Status = 0;//check for whether nobody starts reply
                                stepObj.StatusCode = 0;
                                ieStepsOutput.Add(stepObj);
                                continue;
                            }
                            memCount = ietmpMembers.Where(a => a.ApprovalStatus == 3).Count();
                            if (memCount == ietmpMembers.Count)
                            {
                                val.Status = 3;//check for if nobody rejected and  all people approved
                                stepObj.StatusCode = 3;
                                ieStepsOutput.Add(stepObj);
                                continue;
                            }
                            else if (ietmpMembers.Count > 0)
                            {
                                val.Status = 1;
                                stepObj.StatusCode = 1;
                                ieStepsOutput.Add(stepObj);
                                continue;
                            }
                        }
                        else
                        {
                            val.Status = 0;
                            stepObj.StatusCode = 0;
                            ieStepsOutput.Add(stepObj);
                            continue;
                        }


                    }

                    foreach (var val in iePhase)
                    {
                        phaseObj = new EntitytaskphasesDao();
                        phaseObj = iPhaseDao.Where(a => a.ID == val.ID).Select(a => a).FirstOrDefault();

                        ietmpSteps = ieSteps.Where(a => a.PhaseId == val.ID).Select(a => a).ToList<Entitytasksteps>();
                        if (ietmpSteps.Count > 0)
                        {
                            memCount = ietmpSteps.Where(a => a.Status == 5).Count();
                            if (memCount > 0)
                            {
                                val.Status = 5; //check for anyone rejected
                                phaseObj.StatusCode = 5;
                                iePhaseOutput.Add(phaseObj);
                                continue;
                            }
                            memCount = ietmpSteps.Where(a => a.Status == 3).Count();
                            if (memCount == ietmpSteps.Count)
                            {
                                val.Status = 3; //check for all assignees approved
                                phaseObj.StatusCode = 3;
                                iePhaseOutput.Add(phaseObj);
                                continue;
                            }
                            memCount = ietmpSteps.Where(a => a.Status == 4).Count();
                            if (memCount == ietmpSteps.Count)
                            {
                                val.Status = 4;//check for all assignees unable to complete
                                phaseObj.StatusCode = 4;
                                iePhaseOutput.Add(phaseObj);
                                continue;
                            }
                            memCount = ietmpSteps.Where(a => a.Status == null).Count();
                            if (memCount == ietmpSteps.Count)
                            {
                                val.Status = 0;//check for whether nobody starts reply
                                phaseObj.StatusCode = 0;
                                iePhaseOutput.Add(phaseObj);
                                continue;
                            }
                            else if (ietmpSteps.Count > 0)
                            {
                                val.Status = 1;
                                phaseObj.StatusCode = 1;
                                iePhaseOutput.Add(phaseObj);
                                continue;
                            }

                        }
                        else
                        {
                            val.Status = 0;
                            phaseObj.StatusCode = 0;
                            iePhaseOutput.Add(phaseObj);
                            continue;
                        }

                    }
                    if (iePhaseOutput.Count > 0)
                        tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Save<EntitytaskphasesDao>(iePhaseOutput);
                    if (ieStepsOutput.Count > 0)
                        tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Save<EntitytaskstepsDao>(ieStepsOutput);


                    if (iePhase.Count > 0)
                    {
                        bool fof = false; List<int> allowedArr = new List<int> { 0, 1, 5 };
                        foreach (var ephase in iePhase)
                        {
                            if (fof == false)
                            {
                                if (allowedArr.FindIndex(x => x == ephase.Status) > -1)
                                {
                                    fof = true;
                                    ephase.actionPermit = true;
                                }
                            }
                            else
                            {

                                ephase.actionPermit = false;

                            }

                        }
                    }

                    int activePhaseId = iePhase.Where(a => a.actionPermit == true).Select(a => a.ID).FirstOrDefault();


                    ieMembers = (from t1 in ieSteps
                                 join t2 in tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Query<TaskMembersDao>() on t1.ID equals t2.StepId
                                 where t2.RoleID != 1 && t1.PhaseId == activePhaseId
                                 select t2).ToList<TaskMembersDao>();

                    foreach (var mem in ieMembers)
                    {
                        memDao = new TaskMembersDao();
                        memDao = mem;
                        memDao.IsWorkStart = true;
                        ieMembersOutput.Add(memDao);

                    }

                    if (ieMembersOutput.Count > 0)
                        tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Save<TaskMembersDao>(ieMembersOutput);

                    taskdao.CurrentPhaseId = activePhaseId;
                    tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Save<EntityTaskDao>(taskdao);

                    tx.Commit();

                    return activePhaseId;

                }

            }

            catch (Exception ex)
            {
                return 0;
            }
        }


        public bool UpdateDalimDocumentFile(ExternalTaskManagerProxy proxy, int taskId, string filePath)
        {
            try
            {
                string dUserName = "", dPassword = "";

                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    //-----------> GET USER INFO
                    string str = "SELECT deu.UserName,deu.[Password] FROM DM_External_User deu WHERE deu.UserName = '" + proxy.MarcomManager.User.Email.ToLower() + "' ";
                    var usrlist = tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery(str.ToString());
                    if (usrlist.Count > 0)
                    {
                        dUserName = Convert.ToString(usrlist.Cast<Hashtable>().ToList()[0]["UserName"]);
                        dPassword = Convert.ToString(usrlist.Cast<Hashtable>().ToList()[0]["Password"]);
                    }

                    if (dUserName != "")
                    {
                        XDocument adminXmlDoc = MarcomCache<XDocument>.ReadXDocument(xmlType.Admin, proxy.MarcomManager.User.TenantID);
                        XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, proxy.MarcomManager.User.TenantID);

                        string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString(), retFilePath = baseDir + "//" + "dalimlog.txt";
                        bool bReturnLog = false; ErrorLog.LogFilePath = retFilePath;

                        string xelementName = "DalimDefaultSettings_Table";

                        EntityTaskDao tDao = new EntityTaskDao();
                        tDao = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].Query<EntityTaskDao>().Where(a => a.ID == taskId).FirstOrDefault();

                        StringBuilder jsonparameter = new StringBuilder();

                        jsonparameter.Append("{'id':1,'method':'admin.login'},{'id':2,'method':'document.get','params':{ ");
                        jsonparameter.Append("'ID': " + tDao.DalimID + ",'jobPath': '' }}, ");
                        jsonparameter.Append(" {'id':3,'method':'admin.logout'} ");

                        string response1 = "", JobID = "", documentName = "";
                        //----------------> GET JOBID FROM DALIM
                        response1 = RawRpcConnector.MakeGetRequest("[" + jsonparameter + "]", xelementFilepath.Element(xelementName).Element("DalimSettings").Element("DomainUrl").Value.ToString(), xelementFilepath.Element(xelementName).Element("DalimSettings").Element("UserName").Value.ToString(), xelementFilepath.Element(xelementName).Element("DalimSettings").Element("Password").Value.ToString());
                        JArray json = (JArray)JsonConvert.DeserializeObject(response1);
                        var objval = json[1]["result"];
                        if (objval != null)
                        {
                            JobID = json[1]["result"]["jobID"].ToString();
                            documentName = json[1]["result"]["name"].ToString();
                        }

                        if (JobID != "")
                        {
                            //string protocol = xelementFilepath.Element(xelementName).Element("DalimSettings").Element("Protocol").Value.ToString();
                            //string fileURL = protocol + proxy.MarcomManager.User.TenantHost + "/" + proxy.MarcomManager.User.TenantPath.Replace("\\", "/") + "DAMFiles/Original/" + filePath;


                            string amazonurl = proxy.MarcomManager.User.AwsStorage.Uploaderurl + "/" + proxy.MarcomManager.User.AwsStorage.BucketName;
                            string fileURL = amazonurl + "/" + proxy.MarcomManager.User.TenantPath.Replace("\\", "/") + "DAMFiles/Original/" + filePath;
                            jsonparameter.Clear();
                            jsonparameter.Append("{'id':1,'method':'admin.login'},{'id':2,'method':'document.register','params':{ ");
                            jsonparameter.Append("'jobID': '" + JobID + "','jobPath': '' , ");
                            jsonparameter.Append("'URL': '" + fileURL + "', ");
                            jsonparameter.Append("'documentName': '" + documentName + "','moveFile': true }}, ");
                            jsonparameter.Append(" {'id':3,'method':'admin.logout'} ");

                            string response = "";
                            //--------------> REGISTER DOCUMNET IN DALIM
                            response = RawRpcConnector.MakeGetRequest("[" + jsonparameter + "]", xelementFilepath.Element(xelementName).Element("DalimSettings").Element("DomainUrl").Value.ToString(), dUserName, dPassword);

                            bReturnLog = ErrorLog.CustomErrorRoutine(false, "DodumentRegister=============================", DateTime.Now);
                            bReturnLog = ErrorLog.CustomErrorRoutine(false, "Request===========================================" + Environment.NewLine, DateTime.Now);
                            bReturnLog = ErrorLog.CustomErrorRoutine(false, "{'id':1,'method':'admin.login'},{'id':2,'method':'production.workflows'},{'id':3,'method':'admin.logout'}" + Environment.NewLine, DateTime.Now);
                            bReturnLog = ErrorLog.CustomErrorRoutine(false, "Responce===========================================" + Environment.NewLine, DateTime.Now);
                            bReturnLog = ErrorLog.CustomErrorRoutine(false, response + Environment.NewLine, DateTime.Now);
                        }
                    }
                    tx.Commit();
                }
            }
            catch { }
            return false;
        }

        public int _dalim_MarcomGetstepID(ExternalTaskManagerProxy proxy, int taskid, int dalimStepid)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID)) // initialize the transaction
                {
                    //find the task id from the dalim document Id
                    EntityTaskDao tDao = new EntityTaskDao();
                    tDao = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].Query<EntityTaskDao>().Where(a => a.ID == taskid).FirstOrDefault();

                    //find the marcomstepid from dalim step

                    StringBuilder qry = new StringBuilder();
                    qry.AppendLine(" SELECT  tetsi.MarcomStepId FROM TM_EntityTask tet INNER JOIN TM_Entitytaskphases te  ");
                    qry.AppendLine(" ON tet.ID = te.EntityID INNER JOIN TM_Entitytasksteps te2 ");
                    qry.AppendLine(" ON te.ID = te2.PhaseId INNER JOIN TM_ExternalTaskStepInfo tetsi ");
                    qry.AppendLine(" ON te2.ID = tetsi.MarcomStepId ");
                    qry.AppendLine("WHERE tet.id = " + taskid + " AND tetsi.ExternalId = " + dalimStepid + "");
                    qry.AppendLine("");

                    var stepDao = tx.PersistenceManager.DamRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery(qry.ToString());

                    int marcomStepID = 0;
                    if (stepDao != null)
                    {
                        marcomStepID = Convert.ToInt32(stepDao.Cast<Hashtable>().ToList()[0]["MarcomStepId"]);
                    }

                    tx.Commit();



                    return marcomStepID;

                }
            }
            catch
            {
                return 0;
            }

        }
    }
}
