﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Metadata.Interface
{
    public interface IEntityTypeAttributeRelation
    {
        /// <summary>
        /// IAttributeType interface for table 'MM_EntityTypeAttributeRelation'.
        /// </summary>
        #region Public Properties

        int ID
        {
            get;
            set;
        }

        int EntityTypeID
        {
            get;
            set;

        }
        string EntityTypeCaption
        {
            get;
            set;

        }
        int AttributeID
        {
            get;
            set;

        }

        string AttributeCaption
        {
            get;
            set;

        }
        int AttributeTypeID
        {
            get;
            set;
        }
        string ValidationID
        {
            get;
            set;

        }

        int SortOrder
        {
            get;
            set;

        }
        string DefaultValue
        {
            get;
            set;

        }

        bool InheritFromParent
        {
            get;
            set;

        }

        bool IsReadOnly
        {
            get;
            set;

        }

        bool IsSpecial
        {
            get;
            set;

        }

        bool ChooseFromParentOnly
        {
            get;
            set;

        }

        bool IsValidationNeeded
        {
            get;
            set;

        }
        string Caption
        {
            get;
            set;

        }

        bool IsSystemDefined
        {
            get;
            set;

        }
        string PlaceHolderValue
        {
            get;
            set;

        }
        int MinValue
        {
            get;
            set;

        }
        int MaxValue
        {
            get;
            set;

        }
        bool IsHelptextEnabled
        {
            get;
            set;
        }

        string HelptextDecsription
        {
            get;
            set;
        }
        dynamic ParentValue { get; set; }
        dynamic ParentTreeLevelValueCaption { get; set; }
        dynamic Lable { get; set; }
        string strAttributeID { get; set; }
        #endregion
    }
    public interface IEntityTypeAttributeRelationwithLevels : IEntityTypeAttributeRelation
    {
        IList<ITreeLevel> Levels { get; set; }
        IList<IOption> Options { get; set; }
        IList<ITagOption> TagOptions { get; set; }
        string tree { get; set; }
        IList<DropDownTreePricing> DropDownPricing { get; set; }
    }

    public interface IUIDropDownPricing
    {
        int NodeId { get; set; }
        string caption { get; set; }
        int level { get; set; }
        int LevelParent { get; set; }
        string LevelKey { get; set; }
        string value { get; set; }
    }

    public interface ITreeDropDownLabel
    {
        int Level { get; set; }
        string Label { get; set; }
    }

    public interface ITreeDropDownCaption
    {
        int Level { get; set; }
        string Caption { get; set; }
    }
}
