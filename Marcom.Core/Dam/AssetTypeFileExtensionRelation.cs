﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandSystems.Marcom.Core.Dam.Interface;

namespace BrandSystems.Marcom.Core.Dam
{
    public partial class AssetTypeFileExtensionRelation : IAssetTypeFileExtensionRelation
    {
        #region "Member variables"

        protected int _id;
        protected int _assettypeid;
        protected string _extension;
        protected int _sortorder;
        protected string _assetcategory;

        #endregion

          #region "Constructor"

        public AssetTypeFileExtensionRelation() { }

        public AssetTypeFileExtensionRelation(int pid, int passettypeid, string pextension, int psortorder, string passetcategory) 
        {
            this._id = pid;
            this._assettypeid = passettypeid;
            this._extension = pextension;
            this._sortorder = psortorder;
            this._assetcategory = passetcategory;
        }

        #endregion


        #region "Public propertiers"

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int AssetTypeID
        {
            get { return _assettypeid; }
            set { _assettypeid = value; }

        }

        public string Extension
        {
            get { return _extension; }
            set { _extension = value; }

        }

        public int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }

        }

        public string AssetCategory
        {
            get { return _assetcategory; }
            set { _assetcategory = value; }

        }

        #endregion

    }
}
