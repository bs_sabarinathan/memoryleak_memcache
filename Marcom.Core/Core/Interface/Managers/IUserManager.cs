﻿using System;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.User.Interface;
using System.Collections;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Dal.User.Model;
using Newtonsoft.Json.Linq;

namespace BrandSystems.Marcom.Core.Interface.Managers
{
    public interface IUserManager
    {
        /// <summary>
        /// Initializes the I user.
        /// </summary>
        /// <param name="strbody">The strbody.</param>
        /// <returns>IUser</returns>
        IUser initializeIUser(string strbody);

        /// <summary>
        /// insert Users.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>in<t/returns>
        int User_Insert(IUser user);

        /// <summary>
        /// insert Users.
        /// <param name="Email">The email.</param>
        /// <param name="FirstName">The first name.</param>
        /// <param name="LastName">The last name.</param>
        /// <param name="Image">The image.</param>
        /// <param name="Language">The language.</param>
        /// <param name="Password">The password.</param>
        /// <param name="StartPage">The start page.</param>
        /// <param name="timezone">The timezone.</param>
        /// <param name="UserName">Name of the user.</param>
        /// <returns>int</returns>
        int User_Insert(string Email, string FirstName, string LastName, string Image, string Language, string Password, int? StartPage, string timezone, string UserName, int DashboardTemplateID, bool IsApiUser, IList<IAttributeData> entityattributedata, int AssetAccessID);

        /// <summary>
        /// Update users.
        /// </summary>
        /// <param name="usermgr">The usermgr.</param>
        /// <returns>bool</returns>
        bool User_Update(IUser usermgr);

        /// <summary>
        /// Update users.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="Email">The email.</param>
        /// <param name="FirstName">The first name.</param>
        /// <param name="LastName">The last name.</param>
        /// <param name="Image">The image.</param>
        /// <param name="Language">The language.</param>
        /// <param name="StartPage">The start page.</param>
        /// <param name="timezone">The timezone.</param>
        /// <returns>bool</returns>
        bool User_Update(int id, string Email, string FirstName, string LastName, string Image, string Language, int? StartPage, string timezone, int DashboardTemplateID, bool IsSSOUser, bool IsApiUser, IList<IAttributeData> entityattributedata, int AssetAccessID);

        /// <summary>
        /// Delete Users by ID.
        /// </summary>
        /// <param name="userid">The userid.</param>
        /// <returns>bool</returns>
        bool User_DeleteByID(int userid);

        /// <summary>
        /// Check User Involvement in entity by ID.
        /// </summary>
        /// <param name="userid">The userid.</param>
        /// <returns>bool</returns>
        bool User_CheckUserInvolvementByID(int userid);

        /// <summary>
        /// Select Users by ID.
        /// </summary>
        /// <param name="userid">The userid.</param>
        /// <returns>IUser</returns>
        IUser User_SelectByID(int userid);

        Tuple<List<int>, List<string>> GetStartpages();
        /// <summary>
        /// Valids the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <returns>IUser</returns>
        IUser valid_User(string userName, string password);

        /// <summary>
        /// Users select name by ID.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns>string</returns>
        string User_SelectNameByID(int userId);

        /// <summary>
        /// Returning user selection feeds for Entity.
        /// </summary>
        /// <param name="UseriD">The user ID</param>
        /// <returns>string</returns>
        string UserEntityselections(int userId);

        /// <summary>
        /// Updating user selection feeds for Entity.
        /// </summary>
        /// <param name="UseriD">The user ID</param>
        /// <param name="Feedselection">User selection Entitytpes as comma separated</param>
        /// <returns>bool</returns>
        bool UserFeedselectionUpdate(int userId, string feedSelection);

        /// <summary>
        /// User select.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <returns>IList</returns>
        IList<IUser> GetUsers();

        IList GetAPIusersDetails();
        bool GenerateGuidforSelectedAPI(int[] userIds);
        Tuple<IList<IPendingUser>, IList<IPendingUser>> GetPendingUsers();
        bool UpdateUsersToRegister(string selectedusers, bool status, int AssetAccess);
        IList<IUser> GetMemberList(string queryString, int GroupID);
        string GetAutoCompleteMemberList(string queryString);
        IList<IUser> GetUserByEntityID(int EntityID);
        IList<IEntityUsers> GetMembersByEntityID(int EntityID);

        /// <summary>
        /// Get UserDateTime as per user's time zone
        /// </summary>
        /// <returns></returns>
        string UserDateTime();

        DateTimeOffset FeedInitialRequestedTime { get; set; }


        DateTimeOffset FeedRecentlyUpdatedTime { get; set; }

        DateTimeOffset TaskFeedInitialRequestedTime { get; set; }


        DateTimeOffset TaskFeedRecentlyUpdatedTime { get; set; }

        DateTimeOffset AssetFeedInitialRequestedTime { get; set; }


        DateTimeOffset AssetFeedRecentlyUpdatedTime { get; set; }

        Boolean TaskFeedLock { get; set; }

        Boolean OverviewFeedLock { get; set; }

        Boolean AssetFeedLock { get; set; }

        /// <summary>
        /// update Users details.
        /// <param name="userId">The userId.</param>
        /// <param name="ColumnName">The ColumnName.</param>
        /// <param name="ColumnValue">The ColumnValue.</param>
        /// <returns>bool</returns>
        bool Userinfo_UpdateByColumn(string ColumnName, string ColumnValue);

        /// <summary>
        /// save users image.
        /// </summary>
        /// <param name="source img">The source img.</param>
        /// <param name="destination img">The destination img.</param>
        /// <param name="imgwidth img">The imgwidth img.</param>
        /// <param name="imgheight img">The imgheight img.</param>
        /// <param name="imgX img">The imgX img.</param>
        /// <param name="imgY img">The imgY img.</param>
        /// <returns>bool</returns>
        bool SaveUserImage(string sourcepath, int imgwidth, int imgheight, int imgX, int imgY, string filename, string fileExt);

        /// save users image.
        /// </summary>
        /// <param name="source img">The source img.</param>
        /// <param name="destination img">The destination img.</param>
        /// <param name="imgwidth img">The imgwidth img.</param>
        /// <param name="imgheight img">The imgheight img.</param>
        /// <param name="imgX img">The imgX img.</param>
        /// <param name="imgY img">The imgY img.</param>
        /// <returns>bool</returns>
        bool insetlogin(int UserID, string IPAddress, string Browser, string Version, int MajorVersion, string MinorVersion, string OS, bool IIsSSO);

        /// <summary>
        /// Getting Objective Users.
        /// </summary>
        /// <param name="typeId">The EntityTypeId.</param>
        /// <returns>IList of Users</returns>
        IList<IUser> GetAllObjectiveMembers(int entityTypeId);

        bool UpdateNewLanguageType(int UserID, int langtypeid);

        int IsSSOUser();
        Tuple<object> getdimensionunit();
        IUser GetDimensionunitsettings();
        string GetTenantFilePath(int TenantID);
        IList<PasswordSetting> GetTenantXMLFile(int TenantID);

        bool InvalidLoginAttemptDetails(string Email, string IPAddress, bool notifyadmi);

        bool ValidLoginAfterInvalid(string Email, string query = "");

        bool SendPasswordResetMail(string Email);

        IList GetLockeduserDetails();

        IList<dynamic> CreateSession(int UserID, string tenanturl, int tenantID, string Email);
        //bool[] LockResetpassword(string Guid, string Password, string OldPassword, bool Flag,int Tenantid);

        bool UserNotificationMail(string Email, string mailtype);

        bool SendAdminNotification(string Email = "");

        bool GetusermailbyGuid(Guid guid);

        JObject CheckNotifyUser(bool Notifyuser, string UserEmail, bool NotifyAdmin = false);

        IList<IApprovalRole> GetApprovalrole();

        int InsertApprovalRoleUser(int approvalroleid, int userid);

        bool DeleteApprovalRoleUser(int userid);

        int[] GetApprovalRoleUserByID(int userid);

        IList GetDalimUser();

        int SaveUpdateDalimUser(string email, string pwd, int id = 0);

        int DeleteDalimUser(int id);
        
        bool Updateadditionalsettingsstartpage(int ID);
        
        PasswordSetting GettingAccountLocalXMLValues();

        JObject SoftLockNotify(JObject usersdata);
    }
}
