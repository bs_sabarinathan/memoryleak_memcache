﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Report.Model
{
   
    public partial class CustomViewsDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _name;
        protected string _description;
        protected string _query;
        protected DateTime? _UpdatedOn;
        protected int _UpdaterID;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public CustomViewsDao() { }

        public CustomViewsDao(string Pname, string pdescription, string Pquery,int pUpdaterID, DateTime? pUpdatedOn)
        {
            this._name = Pname;
            this._description = pdescription;
            this._query = Pquery;
            this._UpdatedOn = pUpdatedOn;
            this._UpdaterID = pUpdaterID;
          
        }
        public CustomViewsDao(string Pname)
        {
            this._name = Pname;
        }


        public CustomViewsDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 100 characters");
                _bIsChanged |= (_name != value);
                _name = value;
            }

        }
        

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 250 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }

        public virtual string Query
        {
            get { return _query; }
            set
            {
                if (value == null)
                    throw new ArgumentOutOfRangeException("Query", "Query value,cannot contain null values");
                _bIsChanged |= (_query != value);
                _query = value;
            }

        }

        public virtual int UpdaterID
        {
            get { return UpdaterID; }
            set { UpdaterID = value; }

        }


        public virtual DateTime? UpdatedOn
        {
            get { return _UpdatedOn; }
            set { _UpdatedOn = value; }

        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string CustomViewsDao = "CustomViewsDao";
            public const string Id = "Id";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string Query = "Query";
            public const string UpdaterID = "UpdaterID";
            public const string UpdatedOn = "UpdatedOn";
           
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string CustomViewsDao = "CustomViewsDao";
            public const string Id = "Id";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string Query = "Query";
            public const string UpdaterID = "UpdaterID";
            public const string UpdatedOn = "UpdatedOn";
        }

        #endregion


    }
}
