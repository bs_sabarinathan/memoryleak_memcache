﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task.Interface;

namespace BrandSystems.Marcom.Core.Task
{
    /// <summary>
    /// Entity object for table 'PM_Entity'.
    /// </summary>
    internal class TaskTemplate : ITaskTemplate
    {
        #region Member Variables

        protected int _id;
        protected string _Name;
        protected string _description;
        #endregion

        #region Constructors
        public TaskTemplate() { }

        public TaskTemplate(string pName, string pDescription)
        {
     
            this._Name = pName;
            this._description = pDescription;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }



        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
            }

        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }

        }

        public IList<ITaskList> TaskListDetails
        {
            get;
            set;
        }

        public IList<ITasktemplateCondition> TempCondDetails
        {
            get;
            set;
        }

       public string TemplateCriteria
        {
            get;
            set;

        }
       
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            TaskTemplate castObj = null;
            try
            {
                castObj = (TaskTemplate)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}


