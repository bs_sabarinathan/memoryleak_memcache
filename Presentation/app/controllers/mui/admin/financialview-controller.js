﻿(function (ng, app) {
    "use strict";

    function financialviewctrl($scope, $location, $resource, $timeout, $cookies, $translate, AdminService) {
        var view_NS = {}, action_NS = {}, viewResponsedata = {}, viewSel = {}, sel_chck = {}, viewArr = [], del_rec = [], del_obj = {}, def_view = {}, sel_columns = [], htmlTable = '', arrayConstructor = [].constructor,
           objectConstructor = {}.constructor;
        $scope.newfinancialViewColumns = [
            { "IsChecked": false, "tooltipid": 2, "Alias": $translate.instant('LanguageContents.Res_1715.Caption'), "Caption": $translate.instant('LanguageContents.Res_1715.Caption'), Id: 1, CatId: 1, ViewId: 0, StateID: 1 }, { "IsChecked": false, "tooltipid": 3, "Alias": $translate.instant('LanguageContents.Res_1721.Caption'), "Caption": $translate.instant('LanguageContents.Res_1721.Caption'), Id: 2, ViewId: 0, StateID: 2, CatId: 1 }, { "IsChecked": false, "tooltipid": 4, "Alias": $translate.instant('LanguageContents.Res_1722.Caption'), "Caption": $translate.instant('LanguageContents.Res_1722.Caption'), ViewId: 0, StateID: 3, Id: 3, CatId: 1 }, { "IsChecked": false, "tooltipid": 5, "Alias": $translate.instant('LanguageContents.Res_1104.Caption'), "Caption": $translate.instant('LanguageContents.Res_1104.Caption'), ViewId: 0, StateID: 4, Id: 4, CatId: 2 }, { "IsChecked": false, "tooltipid": 6, "Alias": $translate.instant('LanguageContents.Res_1718.Caption'), "Caption": $translate.instant('LanguageContents.Res_1718.Caption'), ViewId: 0, StateID: 5, Id: 5, CatId: 2 },
            { "IsChecked": false, "tooltipid": 7, "Alias": $translate.instant('LanguageContents.Res_1704.Caption'), "Caption": $translate.instant('LanguageContents.Res_1704.Caption'), ViewId: 0, StateID: 6, Id: 6, CatId: 3 }, { "IsChecked": false, "tooltipid": 3, "Alias": $translate.instant('LanguageContents.Res_1721.Caption'), "Caption": $translate.instant('LanguageContents.Res_1721.Caption'), ViewId: 0, StateID: 7, Id: 7, CatId: 3 }, { "IsChecked": false, "tooltipid": 4, "Alias": $translate.instant('LanguageContents.Res_1722.Caption'), "Caption": $translate.instant('LanguageContents.Res_1722.Caption'), ViewId: 0, StateID: 8, Id: 8, CatId: 3 }, { "IsChecked": false, "tooltipid": 8, "Alias": $translate.instant('LanguageContents.Res_1164.Caption'), "Caption": $translate.instant('LanguageContents.Res_1164.Caption'), ViewId: 0, StateID: 9, Id: 9, CatId: 4 }, { "IsChecked": false, "tooltipid": 9, "Alias": $translate.instant('LanguageContents.Res_1725.Caption'), "Caption": $translate.instant('LanguageContents.Res_1725.Caption'), ViewId: 0, StateID: 10, Id: 10, CatId: 4 }, { "IsChecked": false, "tooltipid": 10, "Alias": $translate.instant('LanguageContents.Res_1726.Caption'), "Caption": $translate.instant('LanguageContents.Res_1726.Caption'), ViewId: 0, StateID: 11, Id: 11, CatId: 4 },
            { "IsChecked": false, "tooltipid": 0, "Alias": $translate.instant('LanguageContents.Res_5704.Caption'), "Caption": $translate.instant('LanguageContents.Res_5704.Caption'), ViewId: 0, StateID: 12, Id: 12, CatId: 4 }, { "IsChecked": false, "tooltipid": 0, "Alias": $translate.instant('LanguageContents.Res_5705.Caption'), "Caption": $translate.instant('LanguageContents.Res_5705.Caption'), ViewId: 0, StateID: 13, Id: 13, CatId: 4 }, { "IsChecked": false, "tooltipid": 0, "Alias": $translate.instant('LanguageContents.Res_5710.Caption'), "Caption": $translate.instant('LanguageContents.Res_5710.Caption'), ViewId: 0, StateID: 14, Id: 14, CatId: 4 }
        ];
        $scope.financialViewData = { list: [], viewname: "", financialviewSrc: [], viewid: 0 };
        view_NS.initializeobject = function () { AdminService.GetFinancialViews().then(function (res) { viewResponsedata = res.Response; $scope.financialViewData.list = res.Response; }); }
        action_NS.addFinancialView = function () {
            $scope.financialViewData.viewid = 0;
            $scope.financialViewData.viewname = "";
            for (var i = 0, loop = {}; loop = $scope.newfinancialViewColumns[i++];) {
                loop["IsChecked"] = false;
                loop["Alias"] = loop.Caption;
            }
            $scope.financialViewData.financialviewSrc = $scope.newfinancialViewColumns;
            $("#AddFinancialViewsdata").modal('show');
        }
        action_NS.selectDefault = function (view) {
            viewSel = $.grep($scope.financialViewData.list, function (e) { return e.Id == view.Id })[0];
            if (viewSel != null) { viewSel.IsDefault = true; } viewArr = $.grep($scope.financialViewData.list, function (e) { return e.Id !== view.Id });
            if (viewArr != undefined && viewArr.length > 0) for (var i = 0, sel = {}; sel = viewArr[i++];) { sel.IsDefault = false; }
        }
        action_NS.updateIsSelectable = function (view, $event) { view.IsSelectable = !view.IsSelectable; }
        action_NS.updatefinanacialviews = function () {

            if ($('#updatefinancialviews').hasClass('disabled')) {
                return;
            }
            $('#updatefinancialviews').addClass('disabled');

            var views = { "views": $scope.financialViewData.list };
            AdminService.UpdateFinancialViews(views).then(function (res) {
                if (res.StatusCode == 200) {
                    $('#updatefinancialviews').removeClass('disabled');
                    NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                    view_NS.initializeobject();
                }
                else {
                    $('#updatefinancialviews').removeClass('disabled');
                    NotifyError($translate.instant('LanguageContents.Res_5711.Caption'));
                }
            });
        }
        action_NS.addFinancialViewwithColumns = function () {

            if ($('#addFinancialViewwithColumns').hasClass('disabled')) {
                return;
            }
            $('#addFinancialViewwithColumns').addClass('disabled');

            sel_columns = $.grep($scope.financialViewData.financialviewSrc, function (e) { return e.IsChecked == true; });
            if (sel_columns.length > 0) {
                if (sel_columns.length <= 11) {
                    var template = action_NS.formFinancialHeaderTemplate(sel_columns);
                    console.log("Template string", template);
                    var views = { "viewId": $scope.financialViewData.viewid, "ViewCaption": $scope.financialViewData.viewname, "Columns": sel_columns, "Template": template };
                    AdminService.InsertFinacialColumns(views).then(function (res) {
                        if (res.StatusCode == 200) {
                            $('#addFinancialViewwithColumns').removeClass('disabled');
                            $scope.financialViewData.viewid; $("#AddFinancialViewsdata").modal('hide');
                            NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                            view_NS.initializeobject();
                        }
                        else {
                            $('#addFinancialViewwithColumns').removeClass('disabled');
                            NotifyError($translate.instant('LanguageContents.Res_5711.Caption'));
                        }
                    });
                }
                else {
                    $('#addFinancialViewwithColumns').removeClass('disabled');
                    bootbox.alert($translate.instant('LanguageContents.Res_5714.Caption'));
                }
            }
            else {
                $('#addFinancialViewwithColumns').removeClass('disabled');
                bootbox.alert($translate.instant('LanguageContents.Res_5713.Caption'));
            }
        }
        action_NS.DeleteFinancialViews = function () {

            if ($('#DeleteFinancialViews').hasClass('disabled')) {
                return;
            }
            $('#DeleteFinancialViews').addClass('disabled');

            del_rec = $.grep($scope.financialViewData.list, function (e) { return e.IsChecked == true });
            if (del_rec.length > 0) {

                def_view = $.grep(del_rec, function (e) { return e.IsDefault == true })[0];

                if (def_view != undefined) {
                    $('#DeleteFinancialViews').removeClass('disabled');
                    bootbox.alert($translate.instant('LanguageContents.Res_5715.Caption'));
                }
                else {
                    bootbox.confirm($translate.instant('LanguageContents.Res_2013.Caption'), function (result) {
                        if (result) {
                            if (del_rec.length > 0) {
                                AdminService.DeleteFinancialViews({ "views": del_rec }).then(function (res) {
                                    if (res.StatusCode == 200) {
                                        $('#DeleteFinancialViews').removeClass('disabled');
                                        for (var j = 0, del_obj = {}; del_obj = del_rec[j++];) {
                                            $scope.financialViewData.list.splice($.inArray(del_obj, $scope.financialViewData.list), 1);
                                        }
                                        view_NS.initializeobject();
                                        NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                                    }
                                    else {
                                        $('#DeleteFinancialViews').removeClass('disabled');
                                        NotifyError($translate.instant('LanguageContents.Res_5711.Caption'));
                                    }
                                });
                            }
                        }
                        else {
                            $('#DeleteFinancialViews').removeClass('disabled');
                        }
                    });
                }
            }

            else {
                $('#DeleteFinancialViews').removeClass('disabled');
                bootbox.alert($translate.instant('LanguageContents.Res_5712.Caption'));
            }


        }
        action_NS.EditFinancialView = function (view) {
            AdminService.EditFinancialView(view.Id).then(function (res) {
                if (res.StatusCode == 200) {
                    $scope.financialViewData.financialviewSrc = $scope.newfinancialViewColumns;
                    for (var i = 0, loop = {}; loop = $scope.financialViewData.financialviewSrc[i++];) {
                        sel_chck = $.grep(res.Response.viewColumns, function (e) { return e.StateID == loop.StateID })[0];
                        if (sel_chck != null) {
                            loop["IsChecked"] = true;
                            loop.Alias = sel_chck.Caption;
                        }
                        else {
                            loop["IsChecked"] = false;
                        }
                    }
                    $scope.financialViewData.viewname = res.Response.Caption;
                    $scope.financialViewData.viewid = res.Response.Id;
                }
                else {
                }
            });
        }
        action_NS.formFinancialHeaderTemplate = function (columns) {
            var planColSpan = [], budgetSpan = [], apprSpan = [], commitSpan = [];
            planColSpan = $.grep(columns, function (e) { return e.CatId == 1 });
            budgetSpan = $.grep(columns, function (e) { return e.CatId == 2 });
            apprSpan = $.grep(columns, function (e) { return e.CatId == 3 });
            commitSpan = $.grep(columns, function (e) { return e.CatId == 4 });

            htmlTable = "";
            htmlTable += "<table class='table table-fin'>";
            htmlTable += "<thead>";

            //main Header Row formation
            htmlTable += "<tr class='main'>";
            htmlTable += "<th colspan='3'><span>" + $translate.instant('LanguageContents.Res_589.Caption') + "</span></th>";
            if (planColSpan.length > 0)
                htmlTable += "<th colspan='" + planColSpan.length + "' id='planHeading'><span>" + $translate.instant('LanguageContents.Res_1714.Caption') + "</span></th>";
            if (budgetSpan.length > 0)
                htmlTable += "<th colspan='" + budgetSpan.length + "'><span>" + $translate.instant('LanguageContents.Res_1716.Caption') + "</span></th>";
            if (apprSpan.length > 0)
                htmlTable += "<th colspan='" + apprSpan.length + "' id='ApprovedHeading'><span>" + $translate.instant('LanguageContents.Res_1719.Caption') + "</span></th>";
            if (commitSpan.length > 0)
                htmlTable += "<th colspan='" + commitSpan.length + "'><span>" + $translate.instant('LanguageContents.Res_1724.Caption') + "</span></th>";
            htmlTable += "</tr>";

            //sub Header Row formation
            htmlTable += "<tr class='sub'>";
            htmlTable += "<th colspan='3'><span myqtooltip data-tooltipid='attr_1'>" + $translate.instant('LanguageContents.Res_261.Caption') + "</span></th>";
            for (var r = 0, col = {}; col = columns[r++];) {
                htmlTable += "<th><span myqtooltip data-tooltipid='attr_" + col.tooltipid + "'>" + col.Alias + "</span></th>";
            }
            htmlTable += "</tr>";
            htmlTable += "</thead>";
            htmlTable += "</table>";
            return htmlTable;
        }

        view_NS.initializeobject();
        $scope.ActionHandler = action_NS;

    }
    app.controller("mui.admin.financialviewctrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$translate', 'AdminService', financialviewctrl]);
})(angular, app);