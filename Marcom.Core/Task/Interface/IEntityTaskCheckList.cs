﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Task.Interface
{
  public  interface IEntityTaskCheckList
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int TaskId
        {
            get;
            set;
        }

        string Name
        {
            get;
            set;
        }

        int SortOrder
        {
            get;
            set;
        }

        int UserId
        {
            get;
            set;
        }

        int OwnerId
        {
            get;
            set;
        }

        DateTime? CompletedOn
        {
            get;
            set;
        }

        bool Status
        {
            get;
            set;
        }

        string UserName { get; set; }
        string OwnerName { get; set; }
        string CompletedOnValue { get; set; }

        bool IsExisting
        {
            get;
            set;
        }
        #endregion
    }
}

