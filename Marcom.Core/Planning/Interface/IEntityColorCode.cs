using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IEntityColorCode interface for table 'PM_EntityColorCode'.
    /// </summary>
    public interface IEntityColorCode
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int EntityTypeid
        {
            get;
            set;

        }

        int Attributeid
        {
            get;
            set;

        }

        int Optionid
        {
            get;
            set;

        }

        string ColorCode
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
