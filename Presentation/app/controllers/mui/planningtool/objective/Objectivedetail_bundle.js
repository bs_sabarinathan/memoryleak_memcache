///#source 1 1 /app/controllers/mui/planningtool/objective/detail-controller.js
(function (ng, app) {
    "use strict"; function muiplanningtoolobjectivedetailCtrl($scope, $timeout, $http, $compile, $resource, $stateParams, $window, $location, $templateCache, $cookies, $translate, ObjectivedetailService) {
        var model; $scope.Calanderopen = function ($event, model1) { $event.preventDefault(); $event.stopPropagation(); $scope.calanderopened = true; $scope.Ganttenddate1 = false; model = model1; }; $scope.dynCalanderopen = function ($event, model) { $event.preventDefault(); $event.stopPropagation(); $scope.calanderopened = false; $scope.Ganttenddate1 = true; }; $scope.ContextEntityID = 0; $scope.Newsfilter = { FeedgroupMulitipleFilterStatus: [], Feedgrouplistvalues: '-1' }; var CollectionIds = ""; if ($.cookie('ListofEntityID') == undefined || $.cookie('ListofEntityID') == null || $.cookie('ListofEntityID') == "") { $.cookie('ListofEntityID', GetGuidValue($stateParams.TrackID)); }
        CollectionIds = $.cookie('ListofEntityID').split(","); $scope.CurrentTrackingUrl = $stateParams.TrackID; $window.ListofEntityID = CollectionIds; $scope.ExpandingEntityID = '0'; $scope.objectidforreport = '0'; $scope.Level = "0"; $scope.CurrentObjectiveID = 0; $("#plandetailfilterdiv").css('display', 'block')
        $scope.$on("$destroy", function () { RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.objective.detailCtrl']")); }); $("#objectivedetailfilter").on('loadfiltersettings', function (event, TypeID) { $("#objectivedetailfiltersettings").trigger('loaddetailfiltersettings', 10); }); $("#objectivedetailfilter").on('applyplandetailfilter', function (event, filterid, filtername) { $("#objectivedetailfiltersettings").trigger('applyplandetailfilter', [filterid, filtername]); }); $("#objectivedetailfilter").on('EditFilterSettingsByFilterID', function (event, filterid, typeid) { $("#objectivedetailfiltersettings").trigger('EditFilterSettingsByFilterID', [filterid, typeid]); }); $("#objectivedetailfilter").on('ClearScope', function (event, filterid) { $("#objectivedetailfiltersettings").trigger('ClearScope', [filterid]); }); $("#objectivedetailfilter").on('ClearAndReApply', function (event, filterid) { $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption'); $scope.SelectedFilterID = 0; $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption'); }); var FilterObj = []; var FilterID = 0; $scope.ObjectiveFilterID = 0; $scope.ListViewDetails = [{ data: "", PageIndex: 0, UniqueID: "" }]; $scope.ListTemplate = ""; $scope.GanttViewDataPageTemplate = ''; $scope.GanttViewBlockPageTemplate = ''; $scope.CurrentData = null; $('#TreeCtrlCss').remove(); var GlobalChildNode = new Array(); $scope.EntityHighlight = false; if ($stateParams.ID != undefined && $stateParams.ID != 0) { $scope.ExpandingEntityID = $stateParams.ID; GlobalChildNode.push($stateParams.ID); $scope.Level = "100"; $scope.EntityHighlight = true; } else { GlobalChildNode = $window.ListofEntityID; GetTreeCount(); }
        $scope.loadobjectivefromsearchByID = function (id, level) {
            $scope.Level = level; $scope.ObjectiveFilterID = 0; FilterObj = []; $scope.ListViewDetails = [{ data: "", PageIndex: 0, UniqueID: "" }]; $scope.ListTemplate = ""; $scope.GanttViewDataPageTemplate = ''; $scope.GanttViewBlockPageTemplate = ''; GlobalChildNode = new Array(); GlobalChildNode.push(id)
            $scope.ExpandingEntityID = '0'; $scope.EntityHighlight = true; $('#TreeCtrlCss').remove(); GetTreeCount();
        }
        $scope.$on('OnFullfillmentChange', function (event, objectiveID) {
            if ($.cookie('ListofEntityID') == undefined || $.cookie('ListofEntityID') == null || $.cookie('ListofEntityID') == "") { $.cookie('ListofEntityID', GetGuidValue($stateParams.TrackID)); }
            CollectionIds = $.cookie('ListofEntityID').split(","); $scope.CurrentTrackingUrl = $stateParams.TrackID; $window.ListofEntityID = CollectionIds; $scope.ListViewDetails = [{ data: "", PageIndex: 0, UniqueID: "" }]; GlobalChildNode = $window.ListofEntityID; GlobalChildNode.push(0); GetTreeCount();
        }); function GetTreeCount() {
            $('#EntitiesTree').scrollTop(0); $scope.TreeClassList = []; var Node = {}; Node.StartRowNo = 0; Node.MaxNoofRow = 20; Node.FilterID = $scope.ObjectiveFilterID; Node.SortOrderColumn = "null"; Node.IsDesc = false; Node.IncludeChildren = true; Node.EntityID = '0'; Node.Level = $scope.Level; Node.ExpandingEntityID = $scope.ExpandingEntityID; Node.IDArr = GlobalChildNode; Node.FilterAttributes = FilterObj; ObjectivedetailService.ObjectiveDetail(Node).then(function (NodeCnt) {
                if (NodeCnt.Response != null && NodeCnt.Response.Data != null) {
                    fnPageTemplate(parseInt(NodeCnt.Response.DataCount)); $scope.ParenttreeList = NodeCnt.Response.Data; if ($scope.ParenttreeList != null) {
                        if ($scope.ListViewDetails[0].data == "") {
                            $scope.ListViewDetails[0].data = NodeCnt; $scope.ListViewDetails[0].PageIndex = 0
                            $scope.ListViewDetails[0].UniqueID = ""
                        } else { $scope.ListViewDetails.push({ data: NodeCnt, PageIndex: 0, UniqueID: "" }); }
                        var TreeItem = ""; var CostCentreID = 0; var Constructunique = ''; for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                            var UniqueId = ''; var Isobjective = false; var ExtraSpace = "<i class='icon-'></i>"; CostCentreID = $scope.ParenttreeList[i]["ObjectveID"]; if ($scope.ParenttreeList[i]["TypeID"] == 10) { ExtraSpace = ""; Isobjective = true; Constructunique = $scope.ParenttreeList[i]["Id"] + '.'; $scope.ParenttreeList[i]["UniqueKey"] = $scope.ParenttreeList[i]["Id"].toString(); UniqueId = UniqueKEY($scope.ParenttreeList[i]["Id"].toString()); } else { UniqueId = UniqueKEY(Constructunique + $scope.ParenttreeList[i]["UniqueKey"]); }
                            var GeneralUniquekey = UniqueKEY($scope.ParenttreeList[i]["class"].toString()); var ClassName = GenateClass(GeneralUniquekey); ClassName += " mo" + GeneralUniquekey; TreeItem += "<li data-ObjectiveID=" + CostCentreID + " data-over='true' class='" + ClassName + "'  data-uniqueKey='" + GeneralUniquekey + "'><a data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-role='Activity' data-isobjective='" + Isobjective + "' data-entityID=" + $scope.ParenttreeList[i]["Id"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> "; TreeItem += ExtraSpace + Space($scope.ParenttreeList[i]["UniqueKey"], $scope.ParenttreeList[i]["TypeID"]); if ($scope.ParenttreeList[i]["TotalChildrenCount"] > 0) { var level = ($scope.Level == 0 ? 2 : $scope.Level); if (GeneralUniquekey.split("-").length <= level) { TreeItem += "<i data-role='Arrow' data-IsExpanded='true' data-CostCentreID=" + CostCentreID + " data-IsConnected='true' data-icon='" + GeneralUniquekey + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-UniqueId=\"" + GeneralUniquekey + "\"></i>"; } else { TreeItem += "<i data-role='Arrow' data-IsExpanded='false' data-CostCentreID=" + CostCentreID + " data-IsConnected='false' data-icon='" + GeneralUniquekey + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-UniqueId=\"" + GeneralUniquekey + "\"></i>"; } } else { TreeItem += "<i class='icon-'></i>"; }
                            TreeItem += " <span style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>"; TreeItem += "<span class='treeItemName' data-name='text' data-Objectidforreport=" + CostCentreID + " >" + $scope.ParenttreeList[i]["Name"] + "</span>"; TreeItem += "</a> </li>";
                        }
                        var LoadedPage = 0; $scope.$broadcast('onTreePageCreation', [NodeCnt, 0, ""]); $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').attr("data-items", i); $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').append($compile(TreeItem)($scope)); $('#EntitiesTree li[data-page=' + LoadedPage + ']').removeClass('pending').addClass('done'); if (FilterID > 0 || FilterObj.length > 0) {
                            if ($('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').children().length == 0) { $('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').parent().css('display', 'none'); $('#EntitiesTree li[data-page=' + LoadedPage + ']').css('display', 'none'); clearTimeout(timer); timer = setTimeout(function () { StartPageLoad(false) }, 1); }
                            if (($('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').children().length > 0) && ($('#EntitiesTree li[data-page=' + LoadedPage + '] > ul').children().length < 10)) { clearTimeout(timer); timer = setTimeout(function () { StartPageLoad(false) }, 1); }
                        }
                        if ($scope.EntityHighlight == true) {
                            $("#EntitiesTree li.active").removeClass('active')
                            $("#EntitiesTree  li[data-ObjectiveID=" + $scope.CurrentObjectiveID + "] a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active'); $scope.EntityHighlight = false;
                        }
                        if (SearchEntityID != 0) { SearchEntityID = 0; }
                    }
                } else { $("#GanttDataContainer").html(""); $("#GanttBlockContainer").html(""); $("#EntitiesTree").html(""); $("#ListHolder").html(""); if (FilterID > 0 || FilterObj.length > 0) { $(window).AdjustHeightWidth(); } }
                $scope.disableShowAll = false;
            }); $(window).AdjustHeightWidth();
        }
        var TreePageLoadTimer = undefined; function fnPageTemplate(ItemCnt) {
            $("#EntitiesTree").html(""); $scope.GanttViewDataPageTemplate = ""; $scope.GanttViewBlockPageTemplate = ""; var PageSize = 1; var itemsPerPage = 20; if (ItemCnt > itemsPerPage) { PageSize = ItemCnt / itemsPerPage; }
            var TreeTemplate = ''; var ListTemplate = ''; var height = 34 * 20; for (var i = 0; i < PageSize; i++) {
                TreeTemplate += "<li class='pending listviewBGDiv' data-page='" + i + "'><ul class='nav nav-list nav-list-menu'></ul></li>"
                ListTemplate += "<tbody class='listviewBGDiv pending' style='min-height: " + height + "px;' data-page='" + i + "'></tbody>"; $scope.GanttViewDataPageTemplate += '<tbody class="widthcent"  data-page="' + i + '"><tr><td style="height: ' + (height - 14) + 'px;border-color: #fff;"></td></tr></tbody>'; $scope.GanttViewBlockPageTemplate += '<div data-page="' + i + '" style="min-height: ' + height + 'px;" class="ganttview-data-blocks-page widthInherit"></div>';
            }
            if (TreeTemplate.length > 0) { $scope.GanttViewDataPageTemplate += '<tbody class="blankTreeNode"></tbody>'; $scope.ListTemplate = ListTemplate; $("#EntitiesTree").trigger('onListViewTemplateCreation', [ListTemplate]); $("#EntitiesTree").html($compile(TreeTemplate + '<li class="blankTreeNode"></li>')($scope)); }
        }
        var timer; var pageQueue = []; $('#EntitiesTree').scroll(function () { $("#ListHolder").scrollTop($(this).scrollTop()); $("#GanttDataContainer").scrollTop($(this).scrollTop()); clearTimeout(timer); timer = setTimeout(function () { StartPageLoad(false) }, 20); }); $(window).scroll(function () { clearTimeout(timer); timer = setTimeout(function () { StartPageLoad(true) }, 20); }); function StartPageLoad(windowScroll) {
            var obj = $('#EntitiesTree'); var areaHeight = 0; var areaTop = 0; if (windowScroll) { areaHeight = $(window).height(); areaTop = $(window).scrollTop(); } else { areaHeight = $(obj).height(); areaTop = $(obj).position().top; }
            $('li.pending', obj).each(function () { var top = $(this).position().top - areaTop; var height = $(this).height(); if (top + height + height < 0) { } else if (top > areaHeight) { } else { $(this).removeClass('pending').addClass('inprogress'); pageQueue.push($(this).attr('data-page')); } });
        }
        setTimeout(LoadPageContent, 20); function LoadPageContent() { var PageNo = pageQueue.pop(); if (PageNo == undefined) { setTimeout(LoadPageContent, 20); } else { var StartRowNo = PageNo * 20; var MaxNoofRow = 20; TreeLoadPage(PageNo, StartRowNo, MaxNoofRow); } }
        function UniqueKEY(UniKey) {
            var substr = UniKey.split('.'); var id = ""; var row = substr.length; if (row > 1) {
                for (var i = 0; i < row; i++) { id += substr[i]; if (i != row - 1) { id += "-"; } }
                return id;
            }
            return UniKey;
        }
        $scope.TreeClassList = []; function GenateClass(UniKey) {
            var ToLength = UniKey.lastIndexOf('-'); if (ToLength == -1) { return ""; }
            var id = UniKey.substring(0, ToLength); var afterSplit = id.split('-'); var result = ""; for (var i = 0; i < afterSplit.length; i++) { result += SplitClss(id, i + 1); }
            return result;
        }
        function SplitClss(NewId, lengthofdata) {
            var afterSplit = NewId.split('-'); var finalresult = " p"; for (var i = 0; i < lengthofdata; i++) { finalresult += afterSplit[i]; if (i != lengthofdata - 1) { finalresult += "-" } }
            return finalresult;
        }
        function Space(UniKey, typeID) {
            var substr = UniKey.split('.'); var itag = ""; var row = substr.length; if (row > 0) { for (var i = 1; i < row; i++) { itag += "<i class='icon-'></i>"; } }
            if (typeID != 10) return itag; else return "";
        }
        function LoadChildTreeNodes(data, ChildCount, UniqueId, CostCenterId) {
            if (ChildCount > 0) {
                if ($('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isExpanded") == "true") { $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("class", "icon-caret-right"); Collapse(UniqueId); $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isExpanded", "false"); $('#EntitiesTree').scroll(); } else { $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("class", "icon-caret-down"); Expand(UniqueId); $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isExpanded", "true"); }
                if ($('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isChildrenPresent") == "true") { $scope.ExpandingEntityID = data; TreeLoad(true, UniqueId, CostCenterId); $('#treeSettings li i[data-icon=' + UniqueId + ']').attr("data-isChildrenPresent", "false"); }
            }
        }
        function TreeLoad(ParentOrChild, IdUnique, CostCenterId) {
            var StartRowNo = 0; var MaxNoofRow = 20; var TreeItem = ""; var Constructunique = IdUnique.substr(0, IdUnique.indexOf("-")) + "."; var parentNode = {}; parentNode.StartRowNo = StartRowNo; parentNode.MaxNoofRow = MaxNoofRow; parentNode.FilterID = 0; parentNode.SortOrderColumn = "null"; parentNode.IsDesc = false; parentNode.IncludeChildren = false; parentNode.EntityID = CostCenterId; parentNode.Level = $scope.Level; parentNode.ExpandingEntityID = $scope.ExpandingEntityID; parentNode.IDArr = GlobalChildNode; parentNode.FilterAttributes = FilterObj; ObjectivedetailService.ObjectiveDetail(parentNode).then(function (ParentNode) {
                var LoadedPage = 0; $scope.ParenttreeList = ParentNode.Response.Data; if ($scope.ParenttreeList != null) {
                    $scope.ListViewDetails.push({ data: ParentNode, PageIndex: LoadedPage, UniqueID: IdUnique }); var CostCentreID = '0'; for (var i = 0; i < $scope.ParenttreeList.length; i++) {
                        var UniqueId = ''; UniqueId = UniqueKEY(Constructunique + $scope.ParenttreeList[i]["UniqueKey"]); CostCentreID = $scope.ParenttreeList[i]["ObjectveID"]; var GeneralUniquekey = UniqueKEY($scope.ParenttreeList[i]["class"].toString()); var ClassName = GenateClass(GeneralUniquekey); ClassName += " mo" + GeneralUniquekey; var ExtraSpace = "<i class='icon-'></i>"; TreeItem += "<li data-ObjectiveID=" + CostCentreID + "  data-over='true' class='" + ClassName + "'  data-uniqueKey='" + GeneralUniquekey + "'><a data-EntityLevel='" + $scope.ParenttreeList[i]["Level"] + "' data-role='Activity' data-isobjective='false' data-entityID=" + $scope.ParenttreeList[i]["Id"] + "  data-colorcode=" + $scope.ParenttreeList[i]["ColorCode"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-shorttext=" + $scope.ParenttreeList[i]["ShortDescription"] + " data-entityname=\"" + $scope.ParenttreeList[i]["Name"] + "\"> "; TreeItem += Space($scope.ParenttreeList[i]["UniqueKey"], $scope.ParenttreeList[i]["TypeID"]); if ($scope.ParenttreeList[i]["TotalChildrenCount"] > 0) { var level = ($scope.Level == 0 ? 2 : $scope.Level); if (GeneralUniquekey.split("-").length <= level) { TreeItem += "<i data-role='Arrow' data-IsExpanded='true' data-CostCentreID=" + CostCentreID + " data-IsConnected='true' data-icon='" + GeneralUniquekey + "' class='icon-caret-down' data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-UniqueId=\"" + GeneralUniquekey + "\"></i>"; } else { TreeItem += "<i data-role='Arrow' data-IsExpanded='false' data-CostCentreID=" + CostCentreID + " data-IsConnected='false' data-icon='" + GeneralUniquekey + "' class='icon-caret-right' data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeList[i]["Id"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-TotalChildrenCount=" + $scope.ParenttreeList[i]["TotalChildrenCount"] + " data-UniqueId=\"" + GeneralUniquekey + "\"></i>"; } } else { TreeItem += "<i class='icon-'></i>"; }
                        TreeItem += " <span style='background-color: #" + $scope.ParenttreeList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeList[i]["ShortDescription"] + "</span>"; TreeItem += "<span class='treeItemName' data-name='text' data-Objectidforreport=" + CostCentreID + ">" + $scope.ParenttreeList[i]["Name"] + "</span>"; TreeItem += "</a> </li>";
                    }
                    $('#treeSettings li[data-uniqueKey=' + IdUnique + ']').after($compile(TreeItem)($scope)); if ($scope.EntityHighlight == true) {
                        $("#EntitiesTree li.active").removeClass('active')
                        $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active'); $scope.EntityHighlight = false;
                    }
                    $scope.$broadcast('onTreePageCreation', [ParentNode, 0, IdUnique]);
                }
            }); $(window).AdjustHeightWidth();
        }
        $("#treeSettings").click(function (e) {
            if ($scope.disableShowAll == false) {
                $(window).AdjustHeightWidth();
                var TargetControl = $(e.target); $scope.muiiscalender.Iscalender = false; $scope.muiiscalender.CalenderID = 0; var col = ""; if (TargetControl[0].tagName == "I" && TargetControl.attr('data-role') == 'Arrow') { LoadChildTreeNodes(TargetControl.attr('data-ParentID'), TargetControl.attr('data-TotalChildrenCount'), TargetControl.attr('data-UniqueId'), TargetControl.attr('data-costcentreid')) } else if (TargetControl[0].tagName == "I" && TargetControl.attr('data-role') == 'Overview') { $scope.LoadEntityTitle(TargetControl.attr('data-typeID')); $window.SubscriptionEntityTypeID = TargetControl.find('i[data-role="Overview"]').attr('data-typeid'); $window.GlobalUniquekey = TargetControl.parent().attr('data-uniquekey'); }
                else if (TargetControl[0].tagName == "A" && TargetControl.attr('data-role') == 'Activity') {
                    if ($("#EntitiesTree li").hasClass('active')) $("#EntitiesTree li.active").removeClass('active'); $(TargetControl).parents('li').addClass("active"); $scope.CurrentObjectiveID = $(TargetControl).parents('li').attr('data-ObjectiveID'); $scope.objectidforreport = $(TargetControl).children("span").eq(1).attr('data-Objectidforreport'); $scope.EntityShortText = TargetControl.attr('data-shorttext'); $scope.EntityColorcode = TargetControl.attr('data-colorcode'); $scope.RootLevelEntityName = TargetControl.attr('data-entityname').toString(); e.preventDefault(); var localScope = $(e.target).scope(); localScope.$apply(function () {
                        $scope.ContextEntityID = TargetControl.attr('data-entityID'); var id = TargetControl.attr('data-entityID'); $window.EntityTypeID = id; $window.SubscriptionEntityTypeID = TargetControl.find('i[data-role="Overview"]').attr('data-typeid'); $window.GlobalUniquekey = TargetControl.parent().attr('data-uniquekey');
                        if (TargetControl.attr('data-isobjective') == "true")
                            $location.path('/mui/objective/detail/section/' + TargetControl.attr('data-entityID') + '/overview');

                        else
                            //$location.path('/mui/objective/detail/sectionentity/' + TargetControl.attr('data-entityID'));
                            if (!localStorage.lasttab)
                                $location.path('/mui/objective/detail/sectionentity/' + TargetControl.attr('data-entityID') + '/overview');
                            else
                                $location.path('/mui/objective/detail/sectionentity/' + TargetControl.attr('data-entityID') + '/' + localStorage.getItem('lasttab'));
                    });
                } else if (TargetControl[0].tagName == "SPAN" && TargetControl.attr('data-name') == 'text') { if ($("#EntitiesTree li").hasClass('active')) $("#EntitiesTree li.active").removeClass('active'); $(TargetControl).parents('li').addClass("active"); $scope.CurrentObjectiveID = $(TargetControl).parents('li').attr('data-ObjectiveID'); $scope.EntityShortText = TargetControl.parent().attr('data-shorttext'); $scope.EntityColorcode = TargetControl.parent().attr('data-colorcode'); $scope.RootLevelEntityName = TargetControl.parent().attr('data-entityname').toString(); $scope.objectidforreport = $(TargetControl).attr('data-Objectidforreport'); e.preventDefault(); var localScope = $(e.target).scope(); localScope.$apply(function () { var id = TargetControl.parent().attr('data-entityID'); $window.EntityTypeID = id; $window.SubscriptionEntityTypeID = TargetControl.next().attr('data-typeid'); $window.GlobalUniquekey = TargetControl.parent().parent().attr('data-uniquekey'); if (TargetControl.parent().attr('data-isobjective') == "true") $location.path('/mui/objective/detail/section/' + TargetControl.parent().attr('data-entityID') + '/overview'); else $location.path('/mui/objective/detail/sectionentity/' + TargetControl.parent().attr('data-entityID') + '/overview'); }); }
                $(window).AdjustHeightWidth();
            }
        }); function GenerateCss() {
            var row = $scope.TreeClassList.length; var cssStyle = '<style type="text/css" id="TreeCtrlCss">'; $('#TreeCtrlCss').remove()
            for (var i = 0; i < row; i++) { cssStyle += '.' + $scope.TreeClassList[i].trim(); cssStyle += "{display: none;}"; }
            $('head').append(cssStyle + "</style>");
        }
        function Collapse(value) {
            if ($scope.TreeClassList.indexOf("p" + value) == -1) { $scope.TreeClassList.push("p" + value); }
            GenerateCss();
        }
        function Expand(value) {
            var index = $scope.TreeClassList.indexOf("p" + value)
            if (index != -1) { $scope.TreeClassList.splice(index, 1); }
            GenerateCss();
        }
        $("#EntitiesTree").on("loadobjectivefromfilter", function (event, filterid, ApplyFilterobj) {
            $scope.ListViewDetails = [{ data: "", PageIndex: 0, UniqueID: "" }]; $scope.ListTemplate = ""; $scope.GanttViewDataPageTemplate = ''; $scope.GanttViewBlockPageTemplate = ''; if (filterid != undefined) { $scope.ObjectiveFilterID = filterid; } else { $scope.ObjectiveFilterID = 0; }
            FilterObj = ApplyFilterobj; if ($stateParams.ID != undefined && $stateParams.ID != 0) GlobalChildNode.push($stateParams.ID)
            else GlobalChildNode = $window.ListofEntityID; $('#TreeCtrlCss').remove(); GetTreeCount();
        }); function TreeLoadPage(PageNo, StartRowNo, MaxNoofRow) {
            var TreeItem = ""; var ChildInclude = true; var Constructunique = ''; var parentNode = {}; parentNode.StartRowNo = StartRowNo; parentNode.MaxNoofRow = MaxNoofRow; parentNode.FilterID = 0; parentNode.SortOrderColumn = "null"; parentNode.IsDesc = false; parentNode.IncludeChildren = true; parentNode.EntityID = '0'; parentNode.Level = $scope.Level; parentNode.ExpandingEntityID = $scope.ExpandingEntityID; parentNode.IDArr = GlobalChildNode; parentNode.FilterAttributes = FilterObj; ObjectivedetailService.ObjectiveDetail(parentNode).then(function (ParentNode) {
                if (ParentNode.Response != null) {
                    $scope.ParenttreeNewList = ParentNode.Response.Data; if ($scope.ParenttreeNewList != null) {
                        $scope.ListViewDetails.push({ data: ParentNode, PageIndex: PageNo, UniqueID: "" }); var CostCentreID = '0'; for (var i = 0; i < $scope.ParenttreeNewList.length; i++) {
                            var UniqueId = ''; var Isobjective = false; var ExtraSpace = "<i class='icon-'></i>"; CostCentreID = $scope.ParenttreeNewList[i]["ObjectveID"]; if ($scope.ParenttreeNewList[i]["TypeID"] == 10) { ExtraSpace = ""; Isobjective = true; Constructunique = $scope.ParenttreeNewList[i]["Id"] + '.'; $scope.ParenttreeList[i]["UniqueKey"] = $scope.ParenttreeNewList[i]["Id"].toString(); UniqueId = UniqueKEY($scope.ParenttreeNewList[i]["Id"].toString()); } else { UniqueId = UniqueKEY(Constructunique + $scope.ParenttreeNewList[i]["UniqueKey"]); }
                            var GeneralUniquekey = UniqueKEY($scope.ParenttreeNewList[i]["class"].toString()); var ClassName = GenateClass(GeneralUniquekey); ClassName += " mo" + GeneralUniquekey; TreeItem += "<li data-ObjectiveID=" + CostCentreID + "  data-over='true' class='" + ClassName + "'  data-uniqueKey='" + GeneralUniquekey + "'><a data-EntityLevel='" + $scope.ParenttreeNewList[i]["Level"] + "' data-role='Activity' data-isobjective='" + Isobjective + "' data-entityID=" + $scope.ParenttreeNewList[i]["Id"] + " data-colorcode=" + $scope.ParenttreeNewList[i]["ColorCode"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-shorttext=" + $scope.ParenttreeNewList[i]["ShortDescription"] + " data-entityname=\"" + $scope.ParenttreeNewList[i]["Name"] + "\"> "; TreeItem += ExtraSpace + Space($scope.ParenttreeNewList[i]["UniqueKey"]); if ($scope.ParenttreeNewList[i]["TotalChildrenCount"] > 0) { var level = ($scope.Level == 0 ? 2 : $scope.Level); if (GeneralUniquekey.split("-").length <= level) { TreeItem += "<i data-role='Arrow' data-icon='" + GeneralUniquekey + "' class='icon-caret-down' data-CostCentreID=" + CostCentreID + " data-isExpanded=true  data-isChildrenPresent=false data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeNewList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeNewList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + GeneralUniquekey + "\"></i>"; } else { TreeItem += "<i data-role='Arrow' data-icon='" + GeneralUniquekey + "' class='icon-caret-right' data-CostCentreID=" + CostCentreID + " data-isExpanded=false data-isChildrenPresent=true data-isChildLoaded=true data-ParentID=" + $scope.ParenttreeNewList[i]["Id"] + " data-TotalChildrenCount=" + $scope.ParenttreeNewList[i]["TotalChildrenCount"] + " data-Permission=" + $scope.ParenttreeList[i].Permission + " data-UniqueId=\"" + GeneralUniquekey + "\"></i>"; } } else { TreeItem += "<i class='icon-'></i>"; }
                            TreeItem += " <span style='background-color: #" + $scope.ParenttreeNewList[i]["ColorCode"].trim() + ";'  class='eicon-s margin-right5x'>" + $scope.ParenttreeNewList[i]["ShortDescription"] + "</span>"; TreeItem += " <span class='treeItemName' data-name='text' data-Objectidforreport=" + CostCentreID + ">" + $scope.ParenttreeNewList[i]["Name"] + "</span>"; TreeItem += "</a> </li>";
                        }
                        if (TreeItem.length > 0) {
                            $('#EntitiesTree li[data-page=' + PageNo + '] > ul').html($compile(TreeItem)($scope)); $('#EntitiesTree li[data-page=' + PageNo + ']').removeClass('inprogress').addClass('done').removeAttr('style'); if ($('#EntitiesTree li[data-page=' + PageNo + '] > ul').attr("data-items") == undefined) { $('#EntitiesTree li[data-page=' + PageNo + '] > ul').attr("data-items", i); } else { var items = parseInt($('#EntitiesTree li[data-page=' + PageNo + '] > ul').attr("data-items")) * i; $('#EntitiesTree li[data-page=' + PageNo + '] > ul').attr("data-items", items); }
                            if ($scope.EntityHighlight == true) {
                                $("#EntitiesTree li.active").removeClass('active')
                                $("#EntitiesTree  li a[data-entityid=" + $stateParams.ID + "]").parent('li').addClass('active'); $scope.EntityHighlight = false;
                            }
                            $scope.CurrentData = ParentNode; $scope.$broadcast('onTreePageCreation', [ParentNode, PageNo, ""]); setTimeout(LoadPageContent, 0);
                        }
                    }
                }
                $(window).AdjustHeightWidth();
            });
        }; $scope.listofreportEntityID = ''; $scope.ObjectiveListofReportRecords = function () { $('#ReportPageModel').modal('show'); $scope.ReportGenerate = false; $scope.ReportGanttGenerate = false; $scope.ReportEdit = false; $scope.SelectedReport = ""; $scope.GanttReportDiv = false; $scope.CustomReportDiv = false; $scope.ReportImageURL = "NoImage.png"; $scope.reportDescription = "-"; $scope.showCustomEntityCheck = false; $scope.showCustomSubLevels = false; $scope.CustomEntityCheck = ""; $scope.CustomSubLevelsCheck = ""; ObjectivedetailService.ShowReports(0, true).then(function (saveviewscomment) { if (saveviewscomment.Response != null) { $scope.ReportTypes = saveviewscomment.Response; } else { } }); }
        $scope.changeReportType = function () {
            var eidlist = $scope.listofreportEntityID; $scope.Reportreset = []; $scope.selectvalue = 0; $scope.selectvalue = $scope.SelectedReport; $scope.ReportImageURL = "NoImage.png"; $scope.reportDescription = "-"; $scope.GanttReportDiv = false; $scope.CustomReportDiv = false; $scope.showCustomEntityCheck = false; $scope.showCustomSubLevels = false; $scope.CustomEntityCheck = ""; $scope.CustomSubLevelsCheck = ""; if ($scope.selectvalue > 0) {
                $scope.ReportEdit = true; $scope.ReportGenerate = true; $scope.ReportGanttGenerate = false; if ($stateParams.ID != undefined && $stateParams.ID != 0) { $scope.CustomReportDiv = true; } else { $scope.CustomReportDiv = false; }
                ObjectivedetailService.ShowReports($scope.selectvalue, true).then(function (BindReports) { if (BindReports.Response != null) { $scope.Reportreset = BindReports.Response; var count = BindReports.Response.length; $scope.showCustomEntityCheck = $scope.Reportreset[count - 1].EntityLevel; $scope.showCustomSubLevels = $scope.Reportreset[count - 1].SubLevel; $scope.reportDescription = $scope.Reportreset[count - 1].Description; $scope.ReportValue = $scope.Reportreset[count - 1].OID; $scope.ReportImageURL = $scope.Reportreset[count - 1].Preview; $scope.ReportEdit = "devexpress.reportserver:open?reportId=" + $scope.ReportValue + "&revisionId=0&preview=0"; } else { NotifyError($translate.instant('LanguageContents.Res_4523.Caption')); } });
            } else if ($scope.selectvalue == -100 || $scope.selectvalue == -101) {
                $scope.ReportEdit = false; $scope.ReportGenerate = false; $scope.ReportGanttGenerate = true; $scope.reportDescription = "Gantt View report is generated for the selected entities"; $scope.ReportImageURL = "GanttReport.png"; $scope.GanttReportDiv = true; $scope.Ganttoption = ''; $scope.GanttEntityType = ''; $scope.GanttstartDate = ""; $scope.Ganttenddate = ""; $scope.GanttSelectedEntityCheck = ""; $scope.GanttAllSubLevelsCheck = ""; $scope.CustomReportDiv = false; if ($stateParams.ID != undefined && $stateParams.ID != 0) { $scope.SelectedEntityCheckDiv = true; $scope.SubLevelsCheckDiv = true; } else { $scope.SelectedEntityCheckDiv = false; $scope.SubLevelsCheckDiv = false; }
                ObjectivedetailService.GetEntityTypefromDB().then(function (getEntityTypes) { $scope.GanttEntityTypes = getEntityTypes.Response; }); ObjectivedetailService.GetAdminSettingselemntnode('ReportSettings', 'GanttViewReport', 0).then(function (getattribtuesResult) { var getattribtues = getattribtuesResult.Response; if ((getattribtues != undefined || getattribtues != null) && (getattribtues.GanttViewReport != undefined || getattribtues.GanttViewReport != null)) { if (getattribtues.GanttViewReport.Attributes.Attribute.length > 0) { $scope.Ganttoptions = getattribtues.GanttViewReport.Attributes.Attribute; } else if (getattribtues.GanttViewReport.Attributes.Attribute.Id != "") { $scope.Ganttoptionsdata = []; $scope.Ganttoptionsdata.push({ "Id": getattribtues.GanttViewReport.Attributes.Attribute.Id, "Field": getattribtues.GanttViewReport.Attributes.Attribute.Field, "DisplayName": getattribtues.GanttViewReport.Attributes.Attribute.DisplayName }); $scope.Ganttoptions = $scope.Ganttoptionsdata; } } });
            } else { $scope.ReportEdit = false; $scope.ReportGenerate = false; $scope.GanttReportDiv = false; }
        }
        $scope.changeEntityType = function () { if ($scope.GanttEntityType.length > 0) { $scope.SelectedEntityCheckDiv = false; $scope.SubLevelsCheckDiv = false; $scope.GanttSelectedEntityCheck = ""; $scope.GanttAllSubLevelsCheck = ""; } else if ($stateParams.ID != undefined && $stateParams.ID != 0) { $scope.SelectedEntityCheckDiv = true; $scope.SubLevelsCheckDiv = true; } }; $scope.ChangeEntitylevelCheck = function () { if ($scope.GanttSelectedEntityCheck == true) { $scope.GanttAllSubLevelsCheck = false; } }; $scope.ChangeSubLevelsCheck = function () { if ($scope.GanttAllSubLevelsCheck == true) { $scope.GanttSelectedEntityCheck = false; } }; $scope.AddDefaultEndDate = function (startdate) { $("#EntityMetadata").addClass('notvalidate'); var startDate = new Date.create(startdate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")); if (startDate == null) { $scope.Ganttenddate = null } else { $scope.Ganttenddate = startDate.addDays(7); } }; $scope.CustomChangeEntitylevelCheck = function () { if ($scope.CustomEntityCheck == true) { $scope.CustomSubLevelsCheck = false; } }; $scope.CustomChangeSubLevelsCheck = function () { if ($scope.CustomSubLevelsCheck == true) { $scope.CustomEntityCheck = false; } }; $scope.hideReportPopup = function () {
            if ($scope.selectvalue == -100 || $scope.selectvalue == -101) {
                if ($scope.GanttstartDate.toString().length > 0 && $scope.Ganttenddate.toString().length > 0) { var sdate = new Date.create($scope.GanttstartDate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd'); var edate = new Date.create($scope.Ganttenddate.toString().replace(/(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3")).toString('yyyy-MM-dd'); var diffval = ((parseInt(new Date.create(edate.toString('dd/MM/yyyy')).getTime()) - parseInt((new Date.create(sdate.toString('dd/MM/yyyy')).getTime())))); if (diffval < 0) { $scope.GanttstartDate = ""; $scope.Ganttenddate = ""; bootbox.alert($translate.instant('LanguageContents.Res_4767.Caption')); return false; } } else if ($scope.GanttstartDate.toString().length > 0 || $scope.Ganttenddate.toString().length > 0) { $scope.GanttstartDate = ""; $scope.Ganttenddate = ""; bootbox.alert($translate.instant('LanguageContents.Res_4578.Caption')); return false; }
                var ExpandingEntityID = 0; var IncludeChildren = true; var IsMonthlyGantt = $scope.selectvalue == -100 ? false : true; if ($stateParams.ID != undefined && $stateParams.ID != 0) { ExpandingEntityID = $stateParams.ID; }
                if (($scope.ObjectiveFilterID == 0) && (ExpandingEntityID > 0) && ($scope.GanttSelectedEntityCheck == "") && ($scope.GanttAllSubLevelsCheck == "")) { ExpandingEntityID = 0; IncludeChildren = true; }
                if (($scope.ObjectiveFilterID > 0) && (ExpandingEntityID > 0) && ($scope.GanttSelectedEntityCheck == "") && ($scope.GanttAllSubLevelsCheck == "")) { ExpandingEntityID = 0; IncludeChildren = false; } else if ($scope.ObjectiveFilterID > 0 || ($scope.GanttSelectedEntityCheck == true)) { IncludeChildren = false; }
                $("#ReportPageModel").modal("hide"); $('#loadingReportPageModel').modal("show"); var GetObjectiveListreposes = []; var Node4 = {}; Node4.FilterID = $scope.ObjectiveFilterID; Node4.SortOrderColumn = "null"; Node4.IsDesc = false; Node4.IncludeChildren = IncludeChildren; Node4.EntityID = $scope.objectidforreport; Node4.Level = 100; Node4.ExpandingEntityID = ExpandingEntityID; Node4.IsMonthly = IsMonthlyGantt; Node4.IDArr = GlobalChildNode; Node4.FilterAttributes = FilterObj; Node4.EntityTypeArr = $scope.GanttEntityType; Node4.OptionAttributeArr = $scope.Ganttoption; Node4.GanttstartDate = $scope.GanttstartDate; Node4.Ganttenddate = $scope.Ganttenddate; ObjectivedetailService.ObjectiveDetailReportSystem(Node4).then(function (Node4) { if (Node4.Response != null) { GetObjectiveListreposes = Node4.Response; var a = document.createElement('a'), fileid = GetObjectiveListreposes, extn = '.xlsx'; var datetimetdy = ConvertDateToString(new Date.create(), $scope.DefaultSettings.DateFormat); var filename = (IsMonthlyGantt == false ? 'Gantt-view-(Quarterly)-' : 'Gantt-view-(Monthly)-') + datetimetdy + extn; a.href = 'DownloadReport.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + ''; a.download = (IsMonthlyGantt == false ? 'Gantt-view-(Quarterly)-' : 'Gantt-view-(Monthly)-') + datetimetdy; document.body.appendChild(a); $('#loadingReportPageModel').modal("hide"); a.click(); } });
            } else if ($scope.selectvalue > 0) {
                var IDLIST = new Array(); var ExpandingEntityID = 0; var IncludeChildren = true; if ($stateParams.ID != undefined && $stateParams.ID != 0) { ExpandingEntityID = $stateParams.ID; }
                if (($scope.ObjectiveFilterID == 0) && (ExpandingEntityID > 0) && ($scope.CustomEntityCheck == "") && ($scope.CustomSubLevelsCheck == "")) { ExpandingEntityID = 0; IncludeChildren = true; }
                if (($scope.ObjectiveFilterID > 0) && (ExpandingEntityID > 0) && ($scope.CustomEntityCheck == "") && ($scope.CustomSubLevelsCheck == "")) { ExpandingEntityID = 0; IncludeChildren = false; } else if ($scope.ObjectiveFilterID > 0 || ($scope.CustomEntityCheck == true)) { IncludeChildren = false; }
                $scope.listofreportEntityID = ''; var Node1 = {}; Node1.FilterID = $scope.ObjectiveFilterID; Node1.SortOrderColumn = "null"; Node1.IsDesc = false; Node1.IncludeChildren = IncludeChildren; Node1.EntityID = $scope.objectidforreport; Node1.Level = 100; Node1.ExpandingEntityID = ExpandingEntityID; Node1.IDArr = GlobalChildNode; Node1.FilterAttributes = FilterObj; ObjectivedetailService.ObjectiveDetailReport(Node1).then(function (GetCostCenterActivityReportList) {
                    if (GetCostCenterActivityReportList.Response != null) {
                        if (GetCostCenterActivityReportList.Response.length > 0) { for (var i = 0; i < GetCostCenterActivityReportList.Response.length; i++) { IDLIST.push(GetCostCenterActivityReportList.Response[i]); } }
                        $scope.listofreportEntityID = IDLIST.join(","); var param = { 'ID': $scope.ReportValue, 'SID': $scope.listofreportEntityID }; var w = 1200; var h = 800
                        var winprops = 'height=' + h + ',width=' + w + ',top=0,left=50,scrollbars=yes,resizable'
                        OpenWindowWithPost("reportviewer.aspx", winprops, "NewFile", param);
                    }
                });
            }
        }
        function OpenWindowWithPost(url, windowoption, name, params) {
            var form = document.createElement("form"); form.setAttribute("method", "post"); form.setAttribute("action", url); form.setAttribute("target", name); for (var i in params) { if (params.hasOwnProperty(i)) { var input = document.createElement('input'); input.type = 'hidden'; input.name = i; input.value = params[i]; form.appendChild(input); } }
            document.body.appendChild(form); window.open("report.aspx", name, windowoption); form.submit(); document.body.removeChild(form);
        }
        $scope.ObjectiveExpandtext = $translate.instant('LanguageContents.Res_4939.Caption'); $scope.ObjectiveExpndClass = 'icon-caret-right'; $scope.disableShowAll = false; $scope.ExpandAll = function () {
            if ($scope.disableShowAll == false) {
                $scope.disableShowAll = true; $scope.ListViewDetails = [{ data: "", PageIndex: 0, UniqueID: "" }]; $scope.ListTemplate = ""; $scope.GanttViewDataPageTemplate = ''; $scope.GanttViewBlockPageTemplate = ''; $('#TreeCtrlCss').remove(); if ($scope.ObjectiveExpandtext == $translate.instant('LanguageContents.Res_4939.Caption')) { $scope.ObjectiveExpandtext = $translate.instant('LanguageContents.Res_4940.Caption'); $scope.ObjectiveExpndClass = 'icon-caret-down'; $scope.Level = "100"; $scope.ExpandingEntityID = '0'; GetTreeCount(); } else { $scope.ObjectiveExpandtext = $translate.instant('LanguageContents.Res_4939.Caption'); $scope.ObjectiveExpndClass = 'icon-caret-right'; $scope.Level = "0"; $scope.ExpandingEntityID = '0'; GetTreeCount(); }
                $('.ganttview-data').scrollTop(0); $('.list_rightblock').scrollTop(0);
            }
        }
        $scope.SelectedFilterID = 0; $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption'); $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption'); ObjectivedetailService.GetFilterSettingsForDetail(10).then(function (filterSettings) { $scope.filterValues = filterSettings.Response; }); $scope.DetailFilterCreation = function (event) {
            if ($scope.SelectedFilterID == 0) {
                $('.FilterHolder').slideDown("slow")
                $("#objectivedetailfilter").trigger('ClearScope', $scope.SelectedFilterID);
            } else { $("#objectivedetailfilter").trigger('EditFilterSettingsByFilterID', [$scope.SelectedFilterID, 10]); $('.FilterHolder').slideDown("slow"); $scope.showSave = false; $scope.showUpdate = true; }
        }; $scope.ApplyFilter = function (filterid, filtername) {
            $scope.SelectedFilterID = filterid; if (filterid != 0) {
                $("#Filtersettingdiv").removeClass("btn-group open"); $("#Filtersettingdiv").addClass("btn-group"); $('.FilterHolder').slideUp("slow")
                $scope.AddEditFilter = 'Edit filter'; $scope.appliedfilter = filtername;
            } else { $scope.appliedfilter = filtername; $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption'); $('.FilterHolder').slideUp("slow"); $("#objectivedetailfilter").trigger('ClearScope', $scope.SelectedFilterID); }
            $("#objectivedetailfilter").trigger('applyplandetailfilter', [filterid, filtername]);
        }
        $("#objectivedetailfilter").on('reloaddetailfilter', function (event, typeId, filtername, filterid) {
            $scope.SelectedFilterID = filterid; if (filtername != '') { $("#objectivedetailfilter").trigger('EditFilterSettingsByFilterID', [$scope.SelectedFilterID, 10]); $scope.appliedfilter = filtername; $scope.AddEditFilter = 'Edit filter'; }
            if (filtername == $translate.instant('LanguageContents.Res_401.Caption')) { $scope.appliedfilter = $translate.instant('LanguageContents.Res_401.Caption'); $scope.AddEditFilter = $translate.instant('LanguageContents.Res_402.Caption'); $scope.SelectedFilterID = 0; $('.FilterHolder').slideUp("slow") }
            ObjectivedetailService.GetFilterSettingsForDetail(typeId).then(function (filterSettings) { $scope.filterValues = filterSettings.Response; $scope.atributesRelationList = []; });
        }); $scope.$on("$destroy", function () { RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.objective.detailCtrl']")); var model, CollectionIds, FilterObj, FilterID, GlobalChildNode, Node, TreeItem, CostCentreID, Constructunique, UniqueId, Isobjective, ExtraSpace, i, GeneralUniquekey, ClassName, level, LoadedPage, PageSize, itemsPerPage, TreeTemplate, ListTemplate, height, timer, pageQueue, obj, areaHeight, areaTop, top, PageNo, StartRowNo, MaxNoofRow, substr, id, row, ToLength, afterSplit, result, finalresult, itag, parentNode, TargetControl, col, localScope, cssStyle, index, items, eidlist, count, getattribtues, startDate, sdate, edate, diffval, ExpandingEntityID, IncludeChildren, IsMonthlyGantt, GetObjectiveListreposes, Node4, IDLIST, Node1, param, w, h, winprops, form, input, modalInstance = null; });
    }
    app.controller("mui.planningtool.objective.detailCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$stateParams', '$window', '$location', '$templateCache', '$cookies', '$translate', 'ObjectivedetailService', muiplanningtoolobjectivedetailCtrl]);
})(angular, app);
///#source 1 1 /app/controllers/mui/planningtool/objective/detail/detailfilter-controller.js
(function(ng,app){"use strict";function muiplanningtoolobjectivedetaildetailfilterCtrl($scope,$location,$resource,$timeout,$cookies,$compile,$window,$translate,ObjectivedetailfilterService){var model;$scope.Calanderopen=function($event,model1){$event.preventDefault();$event.stopPropagation();$scope.calanderopened=true;model=model1;};$scope.treeNodeSelectedHolderValues=[];var IsSave=0;var IsUpdate=0;$scope.deletefiltershow=false;$scope.showSave=true;$scope.showUpdate=false;$scope.EntityTypeID=0;$scope.appliedfilter="No filter applied";$scope.FilterFields={};$scope.PeriodOptionValue={};var DateValidate=dateFormat('1990-01-01',$scope.DefaultSettings.DateFormat);var elementNode='DetailFilter';var selectedfilterid=0;$scope.items=[];$scope.PercentageVisibleSettings={};$scope.DropDownFilterTreePricing={};$scope.TreePricing=[];$scope.FilterDataXML=[];if($window.ObjectiveFilterName!=''&&$window.ObjectiveFilterName!='No filter applied'){}else{$window.ObjectiveFilterName='';}
var TypeID=10;$scope.visible=false;$scope.dynamicEntityValuesHolder={};$scope.dyn_Cont='';$scope.fieldKeys=[];$scope.options={};$scope.setFieldKeys=function(){var keys=[];angular.forEach($scope.FilterFields,function(key){keys.push(key);$scope.fieldKeys=keys;});}
$scope.OptionObj={};$scope.fieldoptions=[];$scope.setoptions=function(){var keys=[];angular.forEach($scope.OptionObj,function(key){keys.push(key);$scope.fieldoptions=keys;});}
$scope.filterValues=[];$scope.atributesRelationList=[];$scope.atributesRelationListWithTree=[];$scope.filterSettingValues=[];$scope.DetailFilterCreation=function(event){$scope.ClearScopeModle($scope.FilterFields);$scope.FilterSettingsLoad();$scope.Deletefilter=false;$scope.Applyfilter=true;$scope.Updatefilter=false;$scope.Savefilter=true;$scope.ngsaveasfilter='';$scope.ngKeywordtext='';IsSave=0;IsUpdate=0;};$("#DetailFilterCreation").on("onDetailFilterCreation",function(event){$scope.ClearScopeModle();});$("#objectivedetailfiltersettings").on("ClearScope",function(event,filterid){$scope.showSave=true;$scope.showUpdate=false;$scope.deletefiltershow=false;$scope.ClearScopeModle();});$scope.ClearFilterAttributes=function(){$scope.ClearFieldsAndReApply();}
$scope.ClearScopeModle=function(){$scope.items=[];$scope.clearscope=!$scope.clearscope;$scope.ngKeywordtext='';$scope.ngsaveasfilter='';$scope.ddlParententitytypeId=[];$scope.ddlEntitymember=[];for(var variable in $scope.FilterFields){if(typeof $scope.FilterFields[variable]==="string"){if(variable!=="ListSingleSelection_69"){$scope.FilterFields[variable]="";}}else if(typeof $scope.FilterFields[variable]==="number"){$scope.FilterFields[variable]=null;}else if(Array.isArray($scope.FilterFields[variable])){$scope.FilterFields[variable]=[];}else if(typeof $scope.FilterFields[variable]==="object"){$scope.FilterFields[variable]={};}}
$scope.treePreviewObj={};$scope.treeNodeSelectedHolderValues=[];var treecount=$.grep($scope.atributesRelationList,function(e){return e.AttributeTypeId==7});for(var i=0;i<treecount.length;i++){ClearSavedTreeNode(treecount[i].AttributeId);}
$scope.costcemtreObject=[];$scope.MemberLists=[];var ApplyFilterobj=[];}
$scope.ClearFieldsAndReApply=function(){IsSave=0;$scope.items=[];$scope.clearscope=!$scope.clearscope;$scope.ngKeywordtext='';$scope.ngsaveasfilter='';$scope.ddlParententitytypeId=[];$scope.ddlEntitymember=[];for(var variable in $scope.FilterFields){if(typeof $scope.FilterFields[variable]==="string"){if(variable!=="ListSingleSelection_69"){$scope.FilterFields[variable]="";}}else if(typeof $scope.FilterFields[variable]==="number"){$scope.FilterFields[variable]=null;}else if(Array.isArray($scope.FilterFields[variable])){$scope.FilterFields[variable]=[];}else if(typeof $scope.FilterFields[variable]==="object"){$scope.FilterFields[variable]={};}}
$scope.treePreviewObj={};$scope.treeNodeSelectedHolderValues=[];var treecount=$.grep($scope.atributesRelationList,function(e){return e.AttributeTypeId==7});for(var i=0;i<treecount.length;i++){ClearSavedTreeNode(treecount[i].AttributeId);}
var pricingObj=$.grep($scope.atributesRelationList,function(e){return e.AttributeTypeId==13});if(pricingObj!=undefined)if(pricingObj.length>0)for(var t=0,obj;obj=pricingObj[t++];){for(var z=0,price;price=$scope.DropDownFilterTreePricing["AttributeId_Levels_"+obj.AttributeId+""][z++];){if(price.selection!=null)if(price.selection.length>0)price.selection=[];}}
$scope.ShowPeriodOptions=false;$scope.costcemtreObject=[];$scope.MemberLists=[];var ApplyFilterobj=[];$scope.showSave=true;$scope.showUpdate=false;$scope.deletefiltershow=false;$("#EntitiesTree").trigger("loadobjectivefromfilter",[0,ApplyFilterobj]);$("#objectivedetailfilter").trigger('ClearAndReApply');}
function KeepAllStepsMarkedComplete(){$("#MyWizard ul.steps").find("li").addClass("complete");$("#MyWizard ul.steps").find("span.badge").addClass("badge-success");window["tid_wizard_steps_all_complete_count"]++;if(window["tid_wizard_steps_all_complete_count"]>=20){clearInterval(window["tid_wizard_steps_all_complete"]);}}
ObjectivedetailfilterService.GetFilterSettingsForDetail(TypeID).then(function(filterSettings){$scope.filterValues=filterSettings.Response;if($window.ObjectiveFilterName==''){}else if($window.ObjectiveFilterName==undefined){}else{$("#Filtersettingdiv").find(":button")[0].firstChild.data=$window.ObjectiveFilterName;}});$scope.hideFilterSettings=function(){$('.FilterHolder').slideUp("slow")
$timeout(function(){$(window).AdjustHeightWidth();},500);}
$("#objectivedetailfiltersettings").on('loaddetailfiltersettings',function(event,TypeID){$scope.FilterSettingsLoad();});$scope.OntimeStatusLists=[{Id:0,Name:'On time',BtnBgColor:'btn-success',AbgColor:'background-image: linear-gradient(to bottom, #62C462, #51A351); color: #fff;',SelectedClass:'btn-success'},{Id:1,Name:'Delayed',BtnBgColor:'btn-warning',AbgColor:'background-image: linear-gradient(to bottom, #FBB450, #F89406); color: #fff;',SelectedClass:'btn-warning'},{Id:2,Name:'On hold',BtnBgColor:'btn-danger',AbgColor:'background-image: linear-gradient(to bottom, #EE5F5B, #BD362F); color: #fff;',SelectedClass:'btn-danger'}]
$scope.FilterSettingsLoad=function(){$scope.treesrcdirec={};$scope.treePreviewObj={};$scope.EntityHierarchyTypesResult=[];if($window.ObjectiveFilterName==''){$window.ObjectiveFilterName='';}
$scope.dyn_Cont='';$scope.Updatefilter=false;$scope.Applyfilter=true;$scope.Deletefilter=false;$scope.Savefilter=false;$("#dynamic_Controls").html('');$scope.atributesRelationList=[];$scope.atributesRelationListWithTree=[];$scope.ngsaveasfilter='';$scope.ngKeywordtext='';var OptionFrom=0;var IsKeyword="false";var IsEntityType="false";var IsEntityMember="false";var CurrentActiveVersion=0;$scope.PeriodOptions=[{Id:1,FilterPeriod:'Between'},{Id:2,FilterPeriod:'Within'}];var KeywordOptionsresponse;ObjectivedetailfilterService.GetOptionsFromXML(elementNode,TypeID).then(function(KeywordOptionsResult){KeywordOptionsresponse=KeywordOptionsResult.Response
OptionFrom=KeywordOptionsresponse.split(',')[0];IsKeyword=KeywordOptionsresponse.split(',')[1];IsEntityType=KeywordOptionsresponse.split(',')[3];IsEntityMember=KeywordOptionsresponse.split(',')[4];CurrentActiveVersion=KeywordOptionsresponse.split(',')[2];$scope.items=[];ObjectivedetailfilterService.GettingEntityTypeHierarchyForAdminTree(6,0).then(function(GerParentEntityData){$scope.entitytpesdata=GerParentEntityData.Response;$scope.tagAllOptions.data=[];if(GerParentEntityData.Response!=null){$.each(GerParentEntityData.Response,function(i,el){$scope.tagAllOptions.data.push({"id":el.Id,"text":el.Caption,"ShortDescription":el.ShortDescription,"ColorCode":el.ColorCode});});}
$scope.ddlParententitytypeId=$scope.EntityHierarchyTypesResult;var IDList1=new Array();var filterattidsmeber={};filterattidsmeber.IDList=$window.ListofEntityID;;filterattidsmeber.TypeID=TypeID;filterattidsmeber.FilterType='DetailFilter';filterattidsmeber.OptionFrom=OptionFrom;filterattidsmeber.IsEntityMember=IsEntityMember
$scope.tagmemberOptions=[];ObjectivedetailfilterService.GettingFilterEntityMember(filterattidsmeber).then(function(entityyMemberRelation){$scope.entityMemberRelationdata=entityyMemberRelation.Response;if(entityyMemberRelation.Response!=null){$.each(entityyMemberRelation.Response,function(i,el){$scope.tagmemberOptions.push({"id":el.Id,"Name":el.FirstName+' '+el.LastName});});}
var filterattids={};filterattids.TypeID=TypeID;filterattids.FilterType='DetailFilter';filterattids.OptionFrom=OptionFrom;filterattids.IDList=$window.ListofEntityID;filterattids.IsEntityMember=IsEntityMember
ObjectivedetailfilterService.GettingFilterAttribute(filterattids).then(function(entityAttributesRelation){$scope.atributesRelationList=entityAttributesRelation.Response;for(var i=0;i<$scope.atributesRelationList.length;i++){if($scope.atributesRelationList[i].AttributeTypeId==6){$scope.dyn_Cont+='<div class="control-group"><span>'+$scope.atributesRelationList[i].DisplayName+' : </span>';$scope.dyn_Cont+='<div class="controls"><select multiple="multiple"  class=\"multiselect\" multiselect-dropdown  data-placeholder="Select '+$scope.atributesRelationList[i].DisplayName+' options" ng-model="FilterFields.DropDown_'+$scope.atributesRelationList[i].AttributeId+'_'+$scope.atributesRelationList[i].TreeLevel+'"\  ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList['+i+'].LevelTreeNodes \">';$scope.dyn_Cont+='</select></div></div>';$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel]=[];}else if($scope.atributesRelationList[i].AttributeTypeId==12){$scope.dyn_Cont+='<div class="control-group"><span>'+$scope.atributesRelationList[i].DisplayName+' : </span>';$scope.dyn_Cont+='<div class="controls"><select multiple="multiple"  class=\"multiselect\" multiselect-dropdown  data-placeholder="Select '+$scope.atributesRelationList[i].DisplayName+' options" ng-model="FilterFields.DropDown_'+$scope.atributesRelationList[i].AttributeId+'_'+$scope.atributesRelationList[i].TreeLevel+'"\ ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList['+i+'].LevelTreeNodes \">';$scope.dyn_Cont+='</select></div></div>';$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel]=[];}else if($scope.atributesRelationList[i].AttributeTypeId==13){var k=$scope.TreePricing.length;var treecount12=$.grep($scope.FilterDataXML,function(e){return e.AttributeId==$scope.atributesRelationList[i].AttributeId});if(treecount12.length==0){var mm=$scope.atributesRelationList[i].AttributeId;$scope.PercentageVisibleSettings["AttributeId_Levels_"+$scope.atributesRelationList[i].AttributeId.toString()+""]=false;$scope.DropDownFilterTreePricing["AttributeId_Levels_"+$scope.atributesRelationList[i].AttributeId.toString()+""]=$scope.atributesRelationList[i].DropdowntreePricingAttr;$scope.dyn_Cont+="<div drowdowntreepercentagemultiselectionfilter  data-purpose='entity' data-attributeid="+$scope.atributesRelationList[i].AttributeId.toString()+"></div>";$scope.FilterDataXML.push({"AttributeId":parseInt($scope.atributesRelationList[i].AttributeId)});}}else if($scope.atributesRelationList[i].AttributeTypeId==3){if($scope.atributesRelationList[i].AttributeId==SystemDefiendAttributes.Owner){$scope.dyn_Cont+="<div class=\"control-group\"><span>"+$scope.atributesRelationList[i].DisplayName+"</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_"+$scope.atributesRelationList[i].AttributeId+"\"  id=\"DropDown_"+$scope.atributesRelationList[i].AttributeId+"\"     ng-options=\"ndata.Id as (ndata.FirstName+' '+ndata.LastName)  for ndata in  atributesRelationList["+i+"].Users \"></select></div>";$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]=[];}else if($scope.atributesRelationList[i].AttributeId==SystemDefiendAttributes.EntityStatus){var EntityStatusOptions=$scope.atributesRelationList[i].EntityStatusOptionValues
$scope.tagAllOptionsEntityStatus.data=[];if(EntityStatusOptions!=null){$.each(EntityStatusOptions,function(i,el){$scope.tagAllOptionsEntityStatus.data.push({"id":el.ID,"text":el.StatusOptions,"ShortDescription":el.ShortDesc,"ColorCode":el.ColorCode});});}
$scope.dyn_Cont+="<div class='control-group'>";$scope.dyn_Cont+="<span>"+$scope.atributesRelationList[i].DisplayName+" : </span><div class=\"controls\">";$scope.dyn_Cont+="<input class='width2x' id='ddlEntityStatus' placeholder='Select Entity Status Options' type='hidden'  ui-select2='tagAllOptionsEntityStatus' ng-model=\"FilterFields.DropDown_"+$scope.atributesRelationList[i].AttributeId+"\" />";$scope.dyn_Cont+="</div></div>";}else if($scope.atributesRelationList[i].AttributeId==SystemDefiendAttributes.EntityOnTimeStatus){$scope.dyn_Cont+='<div class=\"control-group\"><span>'+$scope.atributesRelationList[i].DisplayName+' : </span>';$scope.dyn_Cont+="<select  class=\"multiselect\" multiselect-dropdown   multiple=\"multiple\" ";$scope.dyn_Cont+="data-placeholder=\"Select Entity OnTime Status\" ng-model=\"FilterFields.DropDown_"+$scope.atributesRelationList[i].AttributeId+"\"";$scope.dyn_Cont+="id=\"DropDown_"+$scope.atributesRelationList[i].AttributeId+"\" ng-options=\"opt.Id as opt.Name for opt in  OntimeStatusLists \">";$scope.dyn_Cont+="</select>";$scope.dyn_Cont+="</div>";}else{$scope.dyn_Cont+='<div class=\"control-group\"><span>'+$scope.atributesRelationList[i].DisplayName+' : </span>';$scope.dyn_Cont+='<div class=\"controls\"> <select multiple="multiple"  class=\"multiselect\" multiselect-dropdown  data-placeholder="Select '+$scope.atributesRelationList[i].DisplayName+' options" ng-model=\"FilterFields.DropDown_'+$scope.atributesRelationList[i].AttributeId+'"\" ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList['+i+'].OptionValues \">';$scope.dyn_Cont+='</select></div></div>';$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]=[];}}else if($scope.atributesRelationList[i].AttributeTypeId==4){if($scope.atributesRelationList[i].AttributeId==107){$scope.dyn_Cont+="<div class=\"control-group\">";$scope.dyn_Cont+="<span>"+$translate.instant('LanguageContents.Res_517.Caption')+" : </span>";$scope.dyn_Cont+="<select  class=\"multiselect\" multiselect-dropdown  multiple=\"multiple\" data-placeholder=\"Select Objective type\" ng-model=\"FilterFields.DropDown_"+$scope.atributesRelationList[i].AttributeId+"\">";$scope.dyn_Cont+="<option value=\"1\">Numeric(Quantitative)</option>";$scope.dyn_Cont+="<option value=\"2\">Numeric(Non Quantitative)</option>";$scope.dyn_Cont+="<option value=\"3\">Qualitative</option>";$scope.dyn_Cont+="<option value=\"4\">Rating</option>";$scope.dyn_Cont+="</select>";$scope.dyn_Cont+="</div>";}else if($scope.atributesRelationList[i].AttributeId==108){$scope.dyn_Cont+="<div class=\"control-group\">";$scope.dyn_Cont+="<span>"+$translate.instant('LanguageContents.Res_24.Caption')+" : </span>";$scope.dyn_Cont+="<select  class=\"multiselect\" multiselect-dropdown  multiple=\"multiple\" data-placeholder=\"Select status\" ng-model=\"FilterFields.DropDown_"+$scope.atributesRelationList[i].AttributeId+"\">";$scope.dyn_Cont+="<option value=\"1\">Active</option>";$scope.dyn_Cont+="<option value=\"0\">Deactivated</option>";$scope.dyn_Cont+="</select>";$scope.dyn_Cont+="</div>";}else{$scope.dyn_Cont+="<div class=\"control-group\"><span>"+$scope.atributesRelationList[i].DisplayName+" : </span>";$scope.dyn_Cont+="<div class=\"controls\"><select  class=\"multiselect\" multiselect-dropdown   multiple=\"multiple\"";$scope.dyn_Cont+="data-placeholder=\"Select "+$scope.atributesRelationList[i].DisplayName+"\"";$scope.dyn_Cont+="ng-model=\"FilterFields.DropDown_"+$scope.atributesRelationList[i].AttributeId+"\"";$scope.dyn_Cont+="id=\"DropDown_"+$scope.atributesRelationList[i].AttributeId+"\" ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList["+i+"].OptionValues \">";$scope.dyn_Cont+="</select>";$scope.dyn_Cont+="</div></div>";$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]=[];}}else if($scope.atributesRelationList[i].AttributeTypeId==10){$scope.setoptions();$scope.items=[];$scope.items.push({startDate:'',endDate:''});$scope.dyn_Cont+="<div class=\"control-group\"><span>"+$scope.atributesRelationList[i].DisplayName+" : </span><div class=\"controls\"><div class=\"period nomargin\" id=\"periodcontrols\"  ng-form=\"subForm\">";$scope.dyn_Cont+="<div class=\"row-fluid\">";$scope.dyn_Cont+="<input class=\"sdate\" data-date-format='"+$scope.DefaultSettings.DateFormat+"' id=\"items.startDate\"type=\"text\" value=\"\" name=\"startDate\"  ng-click=\"Calanderopen($event,"+$scope.FilterFields["DatePart_Calander_Open"+"items.startDate"]+")\"  datepicker-popup=\"{{format}}\"  is-open=\"FilterFields.DatePart_Calander_Open"+"items.startDate"+"\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  ng-model=\"items.startDate\" placeholder=\"-- Start date --\" ng-change=\"OpenOptionsForPeriod()\"/><input class=\"edate\" type=\"text\" data-date-format='"+$scope.DefaultSettings.DateFormat+"' value=\"\"  ng-click=\"Calanderopen($event,"+$scope.FilterFields["DatePart_Calander_Open"+"items.endDate"]+")\"  datepicker-popup=\"{{format}}\"  is-open=\"FilterFields.DatePart_Calander_Open"+"items.endDate"+"\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" name=\"enddate\" id=\"items.endDate\" ng-model=\"items.endDate\" placeholder=\"-- End date --\" ng-change=\"OpenOptionsForPeriod()\"/>";$scope.dyn_Cont+="";$scope.dyn_Cont+="</div></div></div></div>";$scope.FilterFields["Period_"+$scope.atributesRelationList[i].AttributeId]=[];$scope.setFieldKeys();$scope.dyn_Cont+="<div ng-show=\"ShowPeriodOptions\" class=\"control-group\" id=\"ShowPeriodOptions\" data-attrid=\""+$scope.atributesRelationList[i].AttributeId+"\"><span>Period range : </span><div class=\"controls\">";$scope.dyn_Cont+="<select class=\"nomargin\" ui-select2 ng-model=\"PeriodOptionValue\"><option ng-repeat=\"ndata in PeriodOptions\" value=\"{{ndata.Id}}\">{{ndata.FilterPeriod}}</option></select></div></div>";$scope.ShowPeriodOptions=false;$scope.FilterFields["FilterFields.DatePart_Calander_Open"+"item.startDate"]=false;$scope.FilterFields["FilterFields.DatePart_Calander_Open"+"item.endDate"]=false;}}
if(IsEntityType=="True"){$scope.dyn_Cont+="<div class='control-group'>";$scope.dyn_Cont+="<span>EntityType : </span><div class=\"controls\">";$scope.dyn_Cont+="<input class='width2x' id='ddlChildren' placeholder='Select EntityType Options' type='hidden' ui-select2='tagAllOptions' ng-model='ddlParententitytypeId' />";$scope.dyn_Cont+="</div></div>";}
$scope.atributesRelationListWithTree=$.grep(entityAttributesRelation.Response,function(e){return e.AttributeTypeId==7})
for(var i=0;i<$scope.atributesRelationListWithTree.length;i++){if($scope.atributesRelationListWithTree[i].AttributeTypeId==7){$scope.treePreviewObj={};$scope.treesrcdirec["Attr_"+$scope.atributesRelationListWithTree[i].AttributeId]=JSON.parse($scope.atributesRelationListWithTree[i].tree).Children;if($scope.treesrcdirec["Attr_"+$scope.atributesRelationListWithTree[i].AttributeId].length>0){if(IsNotEmptyTree($scope.treesrcdirec["Attr_"+$scope.atributesRelationListWithTree[i].AttributeId])){$scope.treePreviewObj["Attr_"+$scope.atributesRelationListWithTree[i].AttributeId]=true;}else $scope.treePreviewObj["Attr_"+$scope.atributesRelationListWithTree[i].AttributeId]=false;}else{$scope.treePreviewObj["Attr_"+$scope.atributesRelationListWithTree[i].AttributeId]=false;}
$scope.dyn_Cont+='<div class="control-group treeNode-control-group">';$scope.dyn_Cont+='<span>'+$scope.atributesRelationListWithTree[i].DisplayName+'</span>';$scope.dyn_Cont+='<div class="controls treeNode-controls">';$scope.dyn_Cont+='<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_'+$scope.atributesRelationListWithTree[i].AttributeId+'" placeholder="Search" treecontext="treeNodeSearchDropdowns_Attr_'+$scope.atributesRelationListWithTree[i].AttributeId+'"></div>';$scope.dyn_Cont+='<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdowns_Attr_'+$scope.atributesRelationListWithTree[i].AttributeId+'">';$scope.dyn_Cont+='<span ng-if="doing_async">...loading...</span>';$scope.dyn_Cont+='<abn-tree tree-filter="filterValue_'+$scope.atributesRelationListWithTree[i].AttributeId+'" tree-data=\"treesrcdirec.Attr_'+$scope.atributesRelationListWithTree[i].AttributeId+'\" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';$scope.dyn_Cont+='</div></div>';$scope.dyn_Cont+='<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_'+$scope.atributesRelationListWithTree[i].AttributeId+'\">';$scope.dyn_Cont+='<div class="controls">';$scope.dyn_Cont+='<eu-tree tree-data=\"treesrcdirec.Attr_'+$scope.atributesRelationListWithTree[i].AttributeId+'\" node-attributeid="'+$scope.atributesRelationListWithTree[i].AttributeId+'" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';$scope.dyn_Cont+='</div></div></div>';}}
if(IsKeyword=="True"){$scope.dyn_Cont+="<div class=\"control-group\"><span>Keyword : </span><input type='text' ng-model='ngKeywordtext' id='ngKeywordtext' placeholder='Filter Keyword'></div>";}
if(IsEntityMember=="True"){$scope.dyn_Cont+="<div class=\"control-group\"><span>Members </span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model='ddlEntitymember'  id= 'ddlChildrenmember'     ng-options=\"ndata.id as (ndata.Name)  for ndata in  tagmemberOptions \"></select></div>";}
$("#dynamic_Controls").html($compile($scope.dyn_Cont)($scope));$scope.Applyfilter=true;$scope.Updatefilter=false;$scope.Deletefilter=false;$scope.Savefilter=true;});});});});}
$timeout($scope.FilterSettingsLoad(),100);$scope.OpenOptionsForPeriod=function(){$scope.PeriodOptionValue={};$scope.PeriodOptionValue=1;var startdate=$scope.items.startDate;var enddate=$scope.items.endDate;if(startdate!=null&&enddate!=null){if(startdate!=undefined&&enddate!=undefined){$scope.ShowPeriodOptions=true;}else $scope.ShowPeriodOptions=false;}else $scope.ShowPeriodOptions=false;}
$scope.FilterSave=function(){if(IsSave==1){return false;}
IsSave=1;var ApplyFilterobj=[];if($scope.ngsaveasfilter==''||$scope.ngsaveasfilter==undefined){bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));IsSave=0;return false;}
var FilterData={};var whereConditionData=[];var multiSelectVal=[];var usersVal=[];var orgLevel=[];var fiscalyear=[];for(var i=0;i<$scope.atributesRelationList.length;i++){if($scope.atributesRelationList[i].AttributeTypeId==6){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel]!=undefined){orgLevel=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel];for(var k=0;k<orgLevel.length;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel][k],'Level':$scope.atributesRelationList[i].TreeLevel,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId});}}}else if($scope.atributesRelationList[i].AttributeTypeId==12){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel]!=undefined){orgLevel=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel];for(var k=0;k<orgLevel.length;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel][k],'Level':$scope.atributesRelationList[i].TreeLevel,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId});}}}else if($scope.atributesRelationList[i].AttributeTypeId==13){var Array=$scope.atributesRelationList[i].DropdowntreePricingAttr.length;for(var ii=0;ii<Array;ii++){if($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection!=null){var j=$scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;for(var k=0;k<j;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],'Level':ii+1,'AttributeTypeId':13,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':''});}}}}else if($scope.atributesRelationList[i].AttributeTypeId==3&&$scope.atributesRelationList[i].AttributeId==SystemDefiendAttributes.Owner){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]!=undefined){usersVal=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId];for(var k=0;k<usersVal.length;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':usersVal[k],'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId});}}}else if($scope.atributesRelationList[i].AttributeTypeId==3||$scope.atributesRelationList[i].AttributeTypeId==4){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]!=undefined){fiscalyear=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]
for(var k=0;k<fiscalyear.length;k++){if(fiscalyear[k].Id!=undefined){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k].Id,'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId});}else{if($scope.atributesRelationList[i].AttributeId==71){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k].id,'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId});}else{whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k],'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId});}}}}}else if($scope.atributesRelationList[i].AttributeTypeId==7){var treenodes=[];treenodes=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==$scope.atributesRelationList[i].AttributeId;});for(var x=0,nodeval;nodeval=treenodes[x++];){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':parseInt(nodeval.id,10),'Level':parseInt(nodeval.Level,10),'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId});}}}
var entitytypeIdforFilter='';if($scope.ddlParententitytypeId!=undefined){if($scope.ddlParententitytypeId[0]!=undefined){for(var l=0;l<$scope.ddlParententitytypeId.length;l++){if(entitytypeIdforFilter==''){entitytypeIdforFilter=$scope.ddlParententitytypeId[l].id;}else{entitytypeIdforFilter=entitytypeIdforFilter+","+$scope.ddlParententitytypeId[l].id;}}}}
var entitymemberIdforFilter='';if($scope.ddlEntitymember!=undefined){if($scope.ddlEntitymember[0]!=undefined){for(var x=0;x<$scope.ddlEntitymember.length;x++){if(entitymemberIdforFilter==''){entitymemberIdforFilter=$scope.ddlEntitymember[x];}else{entitymemberIdforFilter=entitymemberIdforFilter+","+$scope.ddlEntitymember[x];}}}}
FilterData.FilterId=0;FilterData.FilterName=$scope.ngsaveasfilter;FilterData.Keyword=$scope.ngKeywordtext;FilterData.UserId=1;FilterData.TypeID=TypeID;if(entitytypeIdforFilter!=''){FilterData.entityTypeId=entitytypeIdforFilter+',10';}else{FilterData.entityTypeId='';}
FilterData.IsDetailFilter=1;FilterData.EntitymemberId=entitymemberIdforFilter
if($scope.items.startDate!=undefined&&$scope.items.endDate!=undefined){FilterData.StarDate=dateFormat($scope.items.startDate,'yyyy/mm/dd');FilterData.EndDate=dateFormat($scope.items.endDate,'yyyy/mm/dd');whereConditionData.push({'AttributeID':$("#ShowPeriodOptions").attr("data-attrid"),'SelectedValue':$scope.PeriodOptionValue,'Level':0,'AttributeTypeId':10});}else{FilterData.EndDate='';FilterData.StarDate='';}
FilterData.WhereConditon=whereConditionData;ObjectivedetailfilterService.InsertFilterSettings(FilterData).then(function(filterSettingsInsertresult){$('#FilterSettingsModal').modal('hide');if(filterSettingsInsertresult.StatusCode==405){NotifyError($translate.instant('LanguageContents.Res_4279.Caption'));IsSave=0;}else{NotifySuccess($translate.instant('LanguageContents.Res_4397.Caption'));IsSave=0;$("#objectivedetailfilter").trigger('reloaddetailfilter',[TypeID,$scope.ngsaveasfilter,filterSettingsInsertresult.Response]);$("#EntitiesTree").trigger("loadobjectivefromfilter",[filterSettingsInsertresult.Response,ApplyFilterobj]);}});var multiSelectVal=[];var usersVal=[];var orgLevel=[];};$("#objectivedetailfiltersettings").on('EditFilterSettingsByFilterID',function(event,filterId,typeId){$scope.showSave=false;$scope.showUpdate=true;$scope.deletefiltershow=true;$scope.LoadFilterSettingsByFilterID(event,filterId,typeId);});$scope.LoadFilterSettingsByFilterID=function(event,filterId,typeId){$scope.ClearScopeModle();$scope.Updatefilter=false;$scope.Applyfilter=false;$scope.Deletefilter=false;$scope.Savefilter=false;$scope.filterSettingValues=[];selectedfilterid=filterId;ObjectivedetailfilterService.GetFilterSettingValuesByFilertId(filterId).then(function(filterSettingsValues){$scope.filterSettingValues=filterSettingsValues.Response;if($scope.filterSettingValues!=null){for(var i=0;i<$scope.filterSettingValues.FilterValues.length;i++){if($scope.filterSettingValues.FilterValues[i].AttributeTypeId==3&&$scope.filterSettingValues.FilterValues[i].AttributeId==SystemDefiendAttributes.Owner){if($scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId]!=undefined)$scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId].push($scope.filterSettingValues.FilterValues[i].Value);}else if($scope.filterSettingValues.FilterValues[i].AttributeTypeId==3||$scope.filterSettingValues.FilterValues[i].AttributeTypeId==4){if($scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId]!=undefined){if($scope.filterSettingValues.FilterValues[i].AttributeId==71){var statusselectedoptions=$.grep($scope.atributesRelationList,function(e){return e.AttributeId==parseInt(71);})[0].EntityStatusOptionValues;var seletedoptionvalue=[];for(var sts=0;sts<$.grep($scope.filterSettingValues.FilterValues,function(e){return e.AttributeId==parseInt(71);}).length;sts++){seletedoptionvalue.push($.grep($scope.tagAllOptionsEntityStatus.data,function(e){return e.id==parseInt($scope.filterSettingValues.FilterValues[sts].Value);})[0]);if(seletedoptionvalue!=null){$.each(seletedoptionvalue,function(i,el){$scope.tagAllOptionsEntityStatus.data.push({"id":el.ID,"text":el.StatusOptions,"ShortDescription":el.ShortDesc,"ColorCode":el.ColorCode});});}
$scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId]=seletedoptionvalue;}}else{$scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId].push($scope.filterSettingValues.FilterValues[i].Value);}}}else if($scope.filterSettingValues.FilterValues[i].Level!=0&&$scope.filterSettingValues.FilterValues[i].AttributeTypeId==6){if($scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId+"_"+$scope.filterSettingValues.FilterValues[i].Level]!=undefined){$scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId+"_"+$scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);}}else if($scope.filterSettingValues.FilterValues[i].AttributeTypeId==7){$scope.treesrcdirec["Attr_"+$scope.filterSettingValues.FilterValues[i].AttributeId]=JSON.parse($scope.filterSettingValues.FilterValues[i].TreeValues).Children;GetTreeObjecttoSave($scope.filterSettingValues.FilterValues[i].AttributeId);if(IsNotEmptyTree($scope.treesrcdirec["Attr_"+$scope.filterSettingValues.FilterValues[i].AttributeId])){$scope.treePreviewObj["Attr_"+$scope.filterSettingValues.FilterValues[i].AttributeId]=true;}else $scope.treePreviewObj["Attr_"+$scope.filterSettingValues.FilterValues[i].AttributeId]=false;}else if($scope.filterSettingValues.FilterValues[i].Level!=0&&$scope.filterSettingValues.FilterValues[i].AttributeTypeId==12){if($scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId+"_"+$scope.filterSettingValues.FilterValues[i].Level]!=undefined){$scope.FilterFields["DropDown_"+$scope.filterSettingValues.FilterValues[i].AttributeId+"_"+$scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);}}else if($scope.filterSettingValues.FilterValues[i].AttributeTypeId==13){$scope.DropDownFilterTreePricing["AttributeId_Levels_"+$scope.filterSettingValues.FilterValues[i].AttributeId]=$scope.filterSettingValues.FilterValues[i].DropdowntreePricingData;}else if($scope.filterSettingValues.FilterValues[i].AttributeTypeId==10){if($scope.filterSettingValues.StartDate!=null){$scope.ShowPeriodOptions=true;$scope.PeriodOptionValue=$scope.filterSettingValues.FilterValues[i].Value;}}}
$scope.items.push({startDate:$scope.filterSettingValues.StartDate,endDate:$scope.filterSettingValues.EndDate});ObjectivedetailfilterService.GettingEntityTypeHierarchyForAdminTree(6,0).then(function(GerParentEntityData){$scope.entitytpesdata=GerParentEntityData.Response;$scope.tagAllOptions.data=[];if(GerParentEntityData.Response!=null){$.each(GerParentEntityData.Response,function(i,el){$scope.tagAllOptions.data.push({"id":el.Id,"text":el.Caption,"ShortDescription":el.ShortDescription,"ColorCode":el.ColorCode});});}});$scope.EntityHierarchyTypesResult=[];for(var l=0;l<filterSettingsValues.Response.EntityTypeID.split(',').length;l++){if(filterSettingsValues.Response.EntityTypeID.split(',')[l]!='0'){if(filterSettingsValues.Response.EntityTypeID.split(',')[l]!="10")$scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data,function(e){return e.id==filterSettingsValues.Response.EntityTypeID.split(',')[l]})[0]);}}
$scope.ddlParententitytypeId=$scope.EntityHierarchyTypesResult;if(filterSettingsValues.Response.EntityMemberID!=""&&filterSettingsValues.Response.EntityMemberID!=null){for(var l=0;l<filterSettingsValues.Response.EntityMemberID.split(',').length;l++){if(filterSettingsValues.Response.EntityMemberID.split(',')[l]!='0'){if($.grep($scope.tagmemberOptions,function(e){return e.id==filterSettingsValues.Response.EntityMemberID.split(',')[l]})[0]!=undefined){var mval=filterSettingsValues.Response.EntityMemberID.split(',')[l];if(mval!=""&&mval!=undefined)$scope.ddlEntitymember.push(parseInt(mval));}}}}
var startdateupdate=$scope.filterSettingValues.StartDate;var enddateupdate=$scope.filterSettingValues.EndDate;var editstartdate=startdateupdate.split('/');var editenddate=enddateupdate.split('/');startdateupdate=dateFormat(startdateupdate,$scope.DefaultSettings.DateFormat);enddateupdate=dateFormat(enddateupdate,$scope.DefaultSettings.DateFormat);if($scope.filterSettingValues.StartDate==""||$scope.filterSettingValues.EndDate==""){$scope.items.push({startDate:"",endDate:""});}else if($scope.filterSettingValues.StartDate!=DateValidate||$scope.filterSettingValues.EndDate!=DateValidate){$scope.items.startDate=startdateupdate;$scope.items.endDate=enddateupdate;}else{$scope.items.push({startDate:DateValidate,endDate:DateValidate});}
$scope.ngKeywordtext=$scope.filterSettingValues.Keyword;$scope.ngsaveasfilter=$scope.filterSettingValues.FilterName;$scope.showUpdate=true;$scope.showSave=false;$scope.Applyfilter=false;$scope.deletefiltershow=true;$scope.Savefilter=false;}});}
$scope.FilterUpdate=function(){if(IsUpdate==1){return false;}
IsUpdate=1;var ApplyFilterobj=[];var FilterData={};$scope.filterSettingValues.Keyword='';$scope.filterSettingValues.FilterName='';var whereConditionData=[];var multiSelectVal=[];var usersVal=[];var orgLevel=[];var fiscalyear=[];var entitytypeIdforFilter='';if($scope.ddlParententitytypeId!=undefined){if($scope.ddlParententitytypeId[0]!=undefined){for(var l=0;l<$scope.ddlParententitytypeId.length;l++){if(entitytypeIdforFilter==''){entitytypeIdforFilter=$scope.ddlParententitytypeId[l].id;}else{entitytypeIdforFilter=entitytypeIdforFilter+","+$scope.ddlParententitytypeId[l].id;}}}}
var entitymemberIdforFilter='';if($scope.ddlEntitymember!=undefined){if($scope.ddlEntitymember[0]!=undefined){for(var x=0;x<$scope.ddlEntitymember.length;x++){if(entitymemberIdforFilter==''){entitymemberIdforFilter=$scope.ddlEntitymember[x];}else{entitymemberIdforFilter=entitymemberIdforFilter+","+$scope.ddlEntitymember[x];}}}}
for(var i=0;i<$scope.atributesRelationList.length;i++){if($scope.atributesRelationList[i].AttributeTypeId==3&&$scope.atributesRelationList[i].AttributeId==SystemDefiendAttributes.Owner){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]!=undefined){usersVal=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId];for(var k=0;k<usersVal.length;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':usersVal[k],'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'EntityMemberIDs':entitymemberIdforFilter});}}}else if($scope.atributesRelationList[i].AttributeTypeId==3||$scope.atributesRelationList[i].AttributeTypeId==4){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]!=undefined){fiscalyear=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]
for(var k=0;k<fiscalyear.length;k++){if(fiscalyear[k].Id!=undefined){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k].Id,'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'EntityMemberIDs':entitymemberIdforFilter});}else{if($scope.atributesRelationList[i].AttributeId==71){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k].id,'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'EntityMemberIDs':entitymemberIdforFilter});}else{whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k],'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'EntityMemberIDs':entitymemberIdforFilter});}}}}}else if($scope.atributesRelationList[i].AttributeTypeId==7){var treenodes=[];treenodes=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==$scope.atributesRelationList[i].AttributeId;});for(var x=0,nodeval;nodeval=treenodes[x++];){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':parseInt(nodeval.id,10),'Level':parseInt(nodeval.Level,10),'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'EntityMemberIDs':entitymemberIdforFilter});}}else if($scope.atributesRelationList[i].AttributeTypeId==6){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel]!=undefined){orgLevel=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel];for(var k=0;k<orgLevel.length;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel][k],'Level':$scope.atributesRelationList[i].TreeLevel,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'EntityMemberIDs':entitymemberIdforFilter});}}}else if($scope.atributesRelationList[i].AttributeTypeId==12){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel]!=undefined){orgLevel=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel];for(var k=0;k<orgLevel.length;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel][k],'Level':$scope.atributesRelationList[i].TreeLevel,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'EntityMemberIDs':entitymemberIdforFilter});}}}else if($scope.atributesRelationList[i].AttributeTypeId==13){var Array=$scope.atributesRelationList[i].DropdowntreePricingAttr.length;for(var ii=0;ii<Array;ii++){if($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection!=null){var j=$scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;for(var k=0;k<j;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],'Level':ii+1,'AttributeTypeId':13,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':''});}}}}}
FilterData.FilterId=selectedfilterid;FilterData.TypeID=TypeID;FilterData.FilterName=$scope.ngsaveasfilter;FilterData.Keyword=$scope.ngKeywordtext;FilterData.UserId=1;FilterData.entityTypeId=entitytypeIdforFilter;FilterData.IsDetailFilter=1;FilterData.EntitymemberId=entitymemberIdforFilter
if($scope.items.startDate!=undefined&&$scope.items.endDate!=undefined){FilterData.StarDate=dateFormat($scope.items.startDate,'yyyy/mm/dd');FilterData.EndDate=dateFormat($scope.items.endDate,'yyyy/mm/dd');whereConditionData.push({'AttributeID':$("#ShowPeriodOptions").attr("data-attrid"),'SelectedValue':$scope.PeriodOptionValue,'Level':0,'AttributeTypeId':10,'EntityTypeIDs':entitytypeIdforFilter,'EntityMemberIDs':entitymemberIdforFilter});}else{FilterData.EndDate='';FilterData.StarDate='';}
if(whereConditionData.length==0&&entitymemberIdforFilter!=''){whereConditionData.push({'AttributeID':0,'SelectedValue':0,'Level':0,'AttributeTypeId':0,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}
FilterData.WhereConditon=whereConditionData;ObjectivedetailfilterService.InsertFilterSettings(FilterData).then(function(filterSettingsInsertresult){if(filterSettingsInsertresult.StatusCode==405){NotifyError($translate.instant('LanguageContents.Res_4358.Caption'));IsUpdate=0;}else{NotifySuccess($translate.instant('LanguageContents.Res_4400.Caption'));IsUpdate=0;$scope.appliedfilter=$scope.ngsaveasfilter;$("#objectivedetailfilter").trigger('reloaddetailfilter',[TypeID,$scope.ngsaveasfilter,filterSettingsInsertresult.Response]);$("#EntitiesTree").trigger("loadobjectivefromfilter",[filterSettingsInsertresult.Response,ApplyFilterobj]);}});var multiSelectVal=[];var usersVal=[];var orgLevel=[];};$scope.FilterID=0;$("#objectivedetailfiltersettings").on('applyplandetailfilter',function(event,FilterID,FilterName){$scope.ApplyFilter(FilterID,FilterName);});$scope.ApplyFilter=function(FilterID,FilterName){$('#FilterSettingsModal').modal('hide');var StartRowNo=0;var MaxNoofRow=20;var PageIndex=0;if(FilterID!=undefined){selectedfilterid=FilterID;$scope.FilterID=FilterID;}
$scope.appliedfilter=FilterName;$window.ObjectiveFilterName=FilterName;if(FilterID==0){$scope.appliedfilter="No filter applied";}
var whereConditionData=[];var multiSelectVal=[];var usersVal=[];var orgLevel=[];var fiscalyear=[];var entitytypeIdforFilter='';var entitymemberIdforFilter='';if($scope.ddlParententitytypeId!=undefined){if($scope.ddlParententitytypeId[0]!=undefined){for(var l=0;l<$scope.ddlParententitytypeId.length;l++){if(entitytypeIdforFilter==''){entitytypeIdforFilter=$scope.ddlParententitytypeId[l].id;}else{entitytypeIdforFilter=entitytypeIdforFilter+","+$scope.ddlParententitytypeId[l].id;}}}}
if($scope.ddlEntitymember!=undefined){if($scope.ddlEntitymember[0]!=undefined){for(var x=0;x<$scope.ddlEntitymember.length;x++){if(entitymemberIdforFilter==''){entitymemberIdforFilter=$scope.ddlEntitymember[x];}else{entitymemberIdforFilter=entitymemberIdforFilter+","+$scope.ddlEntitymember[x];}}}}
for(var i=0;i<$scope.atributesRelationList.length;i++){if($scope.atributesRelationList[i].AttributeTypeId==6){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel]!=undefined){orgLevel=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel];for(var k=0;k<orgLevel.length;k++){if($scope.ngKeywordtext!=''){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel][k],'Level':$scope.atributesRelationList[i].TreeLevel,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}else{whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel][k],'Level':$scope.atributesRelationList[i].TreeLevel,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}}}}else if($scope.atributesRelationList[i].AttributeTypeId==12){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel]!=undefined){orgLevel=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel];for(var k=0;k<orgLevel.length;k++){if($scope.ngKeywordtext!=''){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel][k],'Level':$scope.atributesRelationList[i].TreeLevel,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}else{whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId+"_"+$scope.atributesRelationList[i].TreeLevel][k],'Level':$scope.atributesRelationList[i].TreeLevel,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}}}}else if($scope.atributesRelationList[i].AttributeTypeId==13){var Array=$scope.atributesRelationList[i].DropdowntreePricingAttr.length;for(var ii=0;ii<Array;ii++){if($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection!=null){var j=$scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;for(var k=0;k<j;k++){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':$scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],'Level':ii+1,'AttributeTypeId':13,'EntityTypeIDs':'','Keyword':'','EntityMemberIDs':''});}}}}else if($scope.atributesRelationList[i].AttributeTypeId==7){var treenodes=[];treenodes=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==$scope.atributesRelationList[i].AttributeId;});for(var x=0,nodeval;nodeval=treenodes[x++];){if($scope.ngKeywordtext!='')whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':parseInt(nodeval.id,10),'Level':parseInt(nodeval.Level,10),'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':$scope.ngKeywordtext,'EntityMemberIDs':entitymemberIdforFilter});else whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':parseInt(nodeval.id,10),'Level':parseInt(nodeval.Level,10),'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','EntityMemberIDs':entitymemberIdforFilter});}}else if($scope.atributesRelationList[i].AttributeTypeId==3&&$scope.atributesRelationList[i].AttributeId==SystemDefiendAttributes.Owner){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]!=undefined){usersVal=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId];for(var k=0;k<usersVal.length;k++){if($scope.ngKeywordtext!=''){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':usersVal[k],'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}else{whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':usersVal[k],'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}}}}else if($scope.atributesRelationList[i].AttributeTypeId==3||$scope.atributesRelationList[i].AttributeTypeId==4){if($scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]!=undefined){fiscalyear=$scope.FilterFields["DropDown_"+$scope.atributesRelationList[i].AttributeId]
for(var k=0;k<fiscalyear.length;k++){if(fiscalyear[k].Id!=undefined){if($scope.ngKeywordtext!=''){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k].Id,'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':'10','Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}else{whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k].Id,'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':'10','Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}}else{if($scope.ngKeywordtext!=''){if($scope.atributesRelationList[i].AttributeId==71){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k].id,'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':'10','Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}else{whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k],'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':'10','Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}}else{if($scope.atributesRelationList[i].AttributeId==71){whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k].id,'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}else{whereConditionData.push({'AttributeID':$scope.atributesRelationList[i].AttributeId,'SelectedValue':fiscalyear[k],'Level':0,'AttributeTypeId':$scope.atributesRelationList[i].AttributeTypeId,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}}}}}}}
if(entitytypeIdforFilter!=''){if($scope.ngKeywordtext!=''){whereConditionData.push({'AttributeID':0,'SelectedValue':0,'Level':0,'AttributeTypeId':0,'EntityTypeIDs':entitytypeIdforFilter+',10','Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}else{whereConditionData.push({'AttributeID':0,'SelectedValue':0,'Level':0,'AttributeTypeId':0,'EntityTypeIDs':entitytypeIdforFilter+',10','Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}}
if($scope.ngKeywordtext!=''&&whereConditionData.length==0){whereConditionData.push({'AttributeID':0,'SelectedValue':0,'Level':0,'AttributeTypeId':0,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}
if(whereConditionData.length==0&&($scope.items.startDate!=undefined||$scope.items.endDate!=undefined)){whereConditionData.push({'AttributeID':0,'SelectedValue':0,'Level':0,'AttributeTypeId':0,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}else if(entitymemberIdforFilter==''&&whereConditionData.length==0){whereConditionData.push({'AttributeID':0,'SelectedValue':0,'Level':0,'AttributeTypeId':0,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':'','StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});}
var StartDate;var EndDate;var optionperiod=$scope.PeriodOptionValue;if($scope.items.startDate==undefined)optionperiod=0;if($scope.items.endDate==undefined)optionperiod=0;if($scope.items.startDate!=undefined&&$scope.items.endDate!=undefined)optionperiod=$scope.PeriodOptionValue;if($scope.items.startDate!=undefined)StartDate=dateFormat($scope.items.startDate,'yyyy/mm/dd');if($scope.items.endDate!=undefined)EndDate=dateFormat($scope.items.endDate,'yyyy/mm/dd');if($scope.items.startDate!=undefined||$scope.items.endDate!=undefined)whereConditionData.push({'AttributeID':$("#ShowPeriodOptions").attr("data-attrid"),'SelectedValue':optionperiod,'Level':0,'AttributeTypeId':10,'EntityTypeIDs':entitytypeIdforFilter,'Keyword':$scope.ngKeywordtext,'StartDate':$scope.items.startDate==undefined?"":ConvertDateToString($scope.items.startDate).replace('-','/').replace('-','/'),'EndDate':$scope.items.endDate==undefined?"":ConvertDateToString($scope.items.endDate).replace('-','/').replace('-','/'),'EntityMemberIDs':entitymemberIdforFilter});var ApplyFilterobj=[];if(FilterID!=0){ApplyFilterobj=whereConditionData;}else{ApplyFilterobj=[];}
$("#EntitiesTree").trigger("loadobjectivefromfilter",[FilterID,ApplyFilterobj])};$scope.filtersettingsreset=function(){var StartRowNo=0;var MaxNoofRow=20;var PageIndex=0;selectedfilterid=0;};$scope.DeleteFilter=function DeleteFilterSettingsValue(){var ApplyFilterobj=[];bootbox.confirm($translate.instant('LanguageContents.Res_2026.Caption'),function(result){if(result){$timeout(function(){var ID=selectedfilterid;ObjectivedetailfilterService.DeleteFilterSettings(ID).then(function(deletefilterbyFilterId){$('#FilterSettingsModal').modal('hide');if(deletefilterbyFilterId.StatusCode==405){NotifyError($translate.instant('LanguageContents.Res_4299.Caption'));}else{NotifySuccess($translate.instant('LanguageContents.Res_4398.Caption'));$("#objectivedetailfilter").trigger('reloaddetailfilter',[TypeID,'No filter applied',0]);$("#EntitiesTree").trigger("loadobjectivefromfilter",[0,ApplyFilterobj]);}});},100);}});};$scope.$on("$destroy",function(){RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.objective.detail.detailfilterCtrl']"));});$scope.tagAllOptionsEntityStatus={multiple:true,allowClear:true,data:$scope.ReassignMembersDataEntityStatus,formatResult:$scope.formatResultEntityStatus,formatSelection:$scope.formatSelection,dropdownCssClass:"bigdrop",escapeMarkup:function(m){return m;}};$scope.tagAllOptions={multiple:true,allowClear:true,data:$scope.ReassignMembersData,formatResult:$scope.formatResult,formatSelection:$scope.formatSelection,dropdownCssClass:"bigdrop",escapeMarkup:function(m){return m;}};$scope.ReassignMembersData=[];$scope.ReassignMembersDataEntityStatus=[];$scope.formatResult=function(item){var markup='<table class="user-result">';markup+='<tbody>';markup+='<tr>';markup+='<td class="user-image">';markup+='<span class="eicon" style="background-color: #'+item.ColorCode+'">'+item.ShortDescription+'</span>';markup+='</td>';markup+='<td class="user-info">';markup+='<div class="user-title">'+item.text+'</div>';markup+='</td>';markup+='</tr>';markup+='</tbody>';markup+='</table>';return markup;};$scope.formatResultEntityStatus=function(item){var markup='<table class="user-result">';markup+='<tbody>';markup+='<tr>';markup+='<td class="user-image">';markup+='<span class="eicon" style="background-color: #'+item.ColorCode+'">'+item.ShortDescription+'</span>';markup+='</td>';markup+='<td class="user-info">';markup+='<div class="user-title">'+item.text+'</div>';markup+='</td>';markup+='</tr>';markup+='</tbody>';markup+='</table>';return markup;};$scope.formatSelection=function(item){var markup='<table class="user-result">';markup+='<tbody>';markup+='<tr>';markup+='<td class="user-image">';markup+='<span class="eicon" style="background-color: #'+item.ColorCode+'">'+item.ShortDescription+'</span>';markup+='</td>';markup+='<td class="user-info">';markup+='<div class="user-title">'+item.text+'</div>';markup+='</td>';markup+='</tr>';markup+='</tbody>';markup+='</table>';return markup;};$scope.formatSelectionForEntityStauts=function(item){var markup='<table class="user-result">';markup+='<tbody>';markup+='<tr>';markup+='<td class="user-image">';markup+='<span class="eicon" style="background-color: #'+item.ColorCode+'">'+item.ShortDescription+'</span>';markup+='</td>';markup+='<td class="user-info">';markup+='<div class="user-title">'+item.text+'</div>';markup+='</td>';markup+='</tr>';markup+='</tbody>';markup+='</table>';return markup;};$scope.tagAllOptions={multiple:true,allowClear:true,data:$scope.ReassignMembersData,formatResult:$scope.formatResult,formatSelection:$scope.formatSelection,dropdownCssClass:"bigdrop",escapeMarkup:function(m){return m;}};$scope.tagAllOptionsEntityStatus={multiple:true,allowClear:true,data:$scope.ReassignMembersDataEntityStatus,formatResult:$scope.formatResultEntityStatus,formatSelection:$scope.formatSelection,dropdownCssClass:"bigdrop",escapeMarkup:function(m){return m;}};function IsNotEmptyTree(treeObj){var flag=false;for(var i=0,node;node=treeObj[i++];){if(node.ischecked==true){flag=true;return flag;}}
return flag;}
function ClearSavedTreeNode(attributeid){for(var i=0,branch;branch=$scope.treesrcdirec["Attr_"+attributeid][i++];){branch.ischecked=false;if(branch.Children.length>0){ClearRecursiveChildTreenode(branch.Children);}}}
function ClearRecursiveChildTreenode(children){for(var j=0,child;child=children[j++];){child.ischecked=false;if(child.Children.length>0){ClearRecursiveChildTreenode(child.Children);}}}
function GetTreeObjecttoSave(attributeid){$scope.treeNodeSelectedHolderValues=[];for(var i=0,branch;branch=$scope.treesrcdirec["Attr_"+attributeid][i++];){if(branch.ischecked==true){var remainRecord=[];remainRecord=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==branch.AttributeId&&e.id==branch.id;});if(remainRecord.length==0){$scope.treeNodeSelectedHolderValues.push(branch);}
if(branch.Children.length>0){FormRecursiveChildTreenode(branch.Children);}}}}
function FormRecursiveChildTreenode(children){for(var j=0,child;child=children[j++];){if(child.ischecked==true){var remainRecord=[];remainRecord=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==child.AttributeId&&e.id==child.id;});if(remainRecord.length==0){$scope.treeNodeSelectedHolderValues.push(child);if(child.Children.length>0){FormRecursiveChildTreenode(child.Children);}}}}}
var apple_selected,tree,treedata_avm,treedata_geography;$scope.my_tree_handler=function(branch,parentArr){var _ref;$scope.output="You selected: "+branch.Caption;if((_ref=branch.data)!=null?_ref.description:void 0){return $scope.output+='('+branch.data.description+')';}
if(branch.ischecked==true){var remainRecord=[];remainRecord=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==branch.AttributeId&&e.id==branch.id;});if(remainRecord.length==0){$scope.treeNodeSelectedHolderValues.push(branch);}
for(var i=0,parent;parent=parentArr[i++];){var remainRecord=[];remainRecord=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==parent.AttributeId&&e.id==parent.id;});if(remainRecord.length==0){$scope.treeNodeSelectedHolderValues.push(parent);}}}else{var remainRecord=[];remainRecord=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==branch.AttributeId&&e.id==branch.id;});if(remainRecord.length>0){$scope.treeNodeSelectedHolderValues.splice($scope.treeNodeSelectedHolderValues.indexOf(branch),1);if(branch.Children.length>0){RemoveRecursiveChildTreenode(branch.Children);}}}
if($scope.treesrcdirec["Attr_"+branch.AttributeId].length>0){if(IsNotEmptyTree($scope.treesrcdirec["Attr_"+branch.AttributeId])){$scope.treePreviewObj["Attr_"+branch.AttributeId]=true;}else $scope.treePreviewObj["Attr_"+branch.AttributeId]=false;}else{$scope.treePreviewObj["Attr_"+branch.AttributeId]=false;}
function RemoveRecursiveChildTreenode(children){for(var j=0,child;child=children[j++];){var remainRecord=[];remainRecord=$.grep($scope.treeNodeSelectedHolderValues,function(e){return e.AttributeId==child.AttributeId&&e.id==child.id;});if(remainRecord.length>0){$scope.treeNodeSelectedHolderValues.splice($.inArray(child,$scope.treeNodeSelectedHolderValues),1);if(child.Children.length>0){RemoveRecursiveChildTreenode(child.Children);}}}}};}
app.controller("mui.planningtool.objective.detail.detailfilterCtrl",['$scope','$location','$resource','$timeout','$cookies','$compile','$window','$translate','ObjectivedetailfilterService',muiplanningtoolobjectivedetaildetailfilterCtrl]);})(angular,app);
///#source 1 1 /app/controllers/mui/planningtool/SubEntityCreation-controller.js

(function (ng, app) {
    "use strict";
    //app.controller(
    //	"mui.planningtool.SubEntityCreationCtrl",
    function muiplanningtoolSubEntityCreationCtrl($scope, $location, $resource, $http, $cookies, $compile, $window, $translate, SubentitycreationService, $modalInstance, params) {
        // --- Define Controller Methods. ------------------- //
        var SubentityTypeCreationTimeout = {};
        var model;
        $scope.GetNonBusinessDaysforDatePicker();
        $scope.Calanderopen = function ($event, object, Call_from) {
            $event.preventDefault();
            $event.stopPropagation();
            if (Call_from == "DueDate") {
                object.StartDate = false; object.EndDate = false; object.DateAction = false; object.DueDate = true;
            }
            else if (Call_from == "StartDate") {
                object.EndDate = false; object.DueDate = false; object.DateAction = false; object.StartDate = true;
            }
            else if (Call_from == "EndDate") {
                object.StartDate = false; object.DueDate = false; object.DateAction = false; object.EndDate = true;
            }
            else {
                object.StartDate = false; object.EndDate = false; object.DueDate = false; object.DateAction = true;
            }
        };

        $scope.PeriodCalanderopen = function ($event, item, place) {
            $event.preventDefault();
            $event.stopPropagation();
            if (place == "start") {
                item.calstartopen = true;
                item.calendopen = false;
            }
            else if (place == "end") {
                item.calstartopen = false;
                item.calendopen = true;
            }
        };

        $scope.WizardSteps = [{ "StepId": 1, "StepName": $translate.instant('LanguageContents.Res_251.Caption'), "StepDOMID": "#step1" }];

        $scope.listofValidations = [];
        var remValtimer = "";
        $scope.DynamicAddValidation = function (attrID) {
            var IsValExist = $.grep($scope.listValidationResult, function (e) {
                return parseInt(e[0].split('_')[1]) == attrID;
            });

            if (IsValExist == null || IsValExist.length == 0) {
                var getVal = $.grep($scope.listofValidations, function (e) {
                    return parseInt(e[0].split('_')[1]) == attrID;
                });
                if (getVal != null && getVal.length > 0) {
                    $scope.listValidationResult.push(getVal[0]);

                    SubentityTypeCreationTimeout.btnclick = setTimeout(function () { $("#btnTempSub").click(); }, 100);
                    $("#EntityMetadata").removeClass('notvalidate');
                    $("#EntityMetadata").nod().destroy();
                    if (remValtimer)
                        SubentityTypeCreationTimeout.cancelto = setTimeout.cancel(remValtimer);
                    remValtimer = setTimeout(function () {
                        $("#EntityMetadata").nod($scope.listValidationResult, {
                            'delay': 200,
                            'submitBtnSelector': '#btnTempSub',
                            'silentSubmit': 'true'
                        });

                        SubentityTypeCreationTimeout.btnclick = setTimeout(function () { $("#btnTempSub").click(); }, 100);
                    }, 100);
                }
            }
        }

        $scope.DynamicRemoveValidation = function (attrID) {
            if ($scope.listValidationResult != undefined) {
                var IsValExist = $.grep($scope.listValidationResult, function (e) {
                    return parseInt(e[0].split('_')[1]) == attrID;
                });

                if (IsValExist != null && IsValExist.length > 0) {
                    $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                        return parseInt(e[0].split('_')[1]) != attrID;
                    });

                    SubentityTypeCreationTimeout.btnclick = setTimeout(function () { $("#btnTempSub").click(); }, 100);
                    $("#EntityMetadata").removeClass('notvalidate');
                    $("#EntityMetadata").nod().destroy();
                    if (remValtimer)
                        SubentityTypeCreationTimeout.cancelto = setTimeout.cancel(remValtimer);
                    remValtimer = setTimeout(function () {

                        $("#EntityMetadata").nod($scope.listValidationResult, {
                            'delay': 200,
                            'submitBtnSelector': '#btnTempSub',
                            'silentSubmit': 'true'
                        });

                        SubentityTypeCreationTimeout.btnclick = setTimeout(function () { $("#btnTempSub").click(); }, 100);
                    }, 10);
                }
            }
        }

        $scope.ShowOrHideAttributeToAttributeRelation = function (attrID, attrTypeID) {
            if ($scope.listAttriToAttriResult.length > 0) {
                var Attributetypename = '';
                var relationobj = $.grep($scope.listAttriToAttriResult, function (rel) {
                    return $.inArray(attrID, rel.AttributeRelationID.split(',')) != -1;
                });
                var ID = attrID.split("_");
                if (relationobj != undefined) {
                    if (relationobj.length > 0) {
                        for (var i = 0; i <= relationobj.length - 1; i++) {
                            Attributetypename = '';
                            if (relationobj[i].AttributeTypeID == 3) {
                                Attributetypename = 'ListSingleSelection_' + relationobj[i].AttributeID;
                            }
                            else if (relationobj[i].AttributeTypeID == 4) {
                                Attributetypename = 'ListMultiSelection_' + relationobj[i].AttributeID;
                            }
                            else if (relationobj[i].AttributeTypeID == 6) {
                                Attributetypename = 'DropDown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                            }
                            else if (relationobj[i].AttributeTypeID == 7) {
                                Attributetypename = 'Tree_' + relationobj[i].AttributeID;
                            }
                            else if (relationobj[i].AttributeTypeID == 12) {
                                Attributetypename = 'MultiSelectDropDown_' + relationobj[i].AttributeID + "_" + relationobj[i].AttributeLevel;
                            }

                            if ($scope.fields[Attributetypename] == undefined) {
                                if (attrTypeID == 1) {
                                    $scope.fields["TextSingleLine_" + ID[0]] = "";
                                }
                                else if (attrTypeID == 2) {
                                    $scope.fields["TextMultiLine_" + ID[0]] = "";
                                }
                                else if (attrTypeID == 3) {
                                    $scope.fields["ListSingleSelection_" + ID[0]] = "";
                                }
                                else if (attrTypeID == 4) {
                                    $scope.fields["ListMultiSelection_" + ID[0]] = "";
                                }
                                else if (attrTypeID == 5) {
                                    $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                                }
                                else if (attrTypeID == 6) {
                                    $scope.fields["DropDown_" + ID[0] + "_" + ID[1]] = "";
                                }
                                else if (attrTypeID == 12) {
                                    $scope.fields["MultiSelectDropDown_" + ID[0] + "_" + ID[1]] = "";
                                }
                                else if (attrTypeID == 17) {
                                    $scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
                                }
                                continue;
                                //return false;
                            }
                            if (relationobj[i].AttributeTypeID == 4) {
                                if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
                                    try {
                                        $scope.DynamicAddValidation(parseInt(ID[0]));
                                    }
                                    catch (e) { };
                                    return true;
                                }
                            }
                            else if (relationobj[i].AttributeTypeID == 6 || relationobj[i].AttributeTypeID == 12) {
                                if ($scope.fields[Attributetypename].id == relationobj[i].AttributeOptionID) {
                                    try {
                                        $scope.DynamicAddValidation(parseInt(ID[0]));
                                    }
                                    catch (e) { };
                                    return true;
                                }
                            }
                            else if (relationobj[i].AttributeTypeID == 7) {
                                if ($.inArray(relationobj[i].AttributeOptionID, $scope.fields[Attributetypename]) != -1) {
                                    try {
                                        $scope.DynamicAddValidation(parseInt(ID[0]));
                                    }
                                    catch (e) { };
                                    return true;
                                }
                            }
                            else {
                                if ($scope.fields[Attributetypename] == relationobj[i].AttributeOptionID) {
                                    try {
                                        $scope.DynamicAddValidation(parseInt(ID[0]));
                                    }
                                    catch (e) { };
                                    return true;
                                }
                            }
                            //return false;
                        }
                        if (attrTypeID == 1) {
                            $scope.fields["TextSingleLine_" + ID[0]] = "";
                        }
                        else if (attrTypeID == 2) {
                            $scope.fields["TextMultiLine_" + ID[0]] = "";
                        }
                        else if (attrTypeID == 3) {
                            $scope.fields["ListSingleSelection_" + ID[0]] = "";
                        }
                        else if (attrTypeID == 4) {
                            $scope.fields["ListMultiSelection_" + ID[0]] = "";
                        }
                        else if (attrTypeID == 5) {
                            $scope.fields["DatePart_" + ID[0]] = null;
                        }
                        else if (attrTypeID == 6) {
                            $scope.fields["DropDown_" + ID[0] + "_" + ID[1]] = "";
                        }
                        else if (attrTypeID == 12) {
                            $scope.fields["MultiSelectDropDown_" + ID[0] + "_" + ID[1]] = "";
                        }
                        else if (attrTypeID == 17) {
                            $scope.fields["ListTagwords_" + ID[0]] = [];
                        }
                        try {
                            $scope.DynamicRemoveValidation(parseInt(ID[0]));
                        }
                        catch (e) { };
                        return false;
                    }
                    else
                        return true;
                }
                else
                    return true;
            }
            else
                return true;
        };

        //Gets all the tabs list in the systems..
        function GetEntityTypeTabCollections(Id, Caption, CurrentEntityId) {
            SubentitycreationService.GetPlantabsettings().then(function (gettabresult) {
                if (gettabresult.Response != null) {
                    $scope.ShowFinancial = gettabresult.Response.Financials;
                    $scope.ShowObjective = gettabresult.Response.Objectives;
                    $scope.ShowAttachments = gettabresult.Response.Attachments;


                    if ($scope.ShowFinancial == "false" && $scope.ShowObjective == "false" && $scope.ShowAttachments == "false") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "true" && $scope.ShowObjective == "true" && $scope.ShowAttachments == "true") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_35.Caption'), "StepDOMID": "#step5" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_1796.Caption'), "StepDOMID": "#step2" });
                        $scope.WizardSteps.push({ "StepId": 4, "StepName": $translate.instant('LanguageContents.Res_701.Caption'), "StepDOMID": "#step3" });
                        $scope.WizardSteps.push({ "StepId": 5, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "true" && $scope.ShowObjective == "true" && $scope.ShowAttachments == "false") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_1796.Caption'), "StepDOMID": "#step2" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_701.Caption'), "StepDOMID": "#step3" });
                        $scope.WizardSteps.push({ "StepId": 4, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "true" && $scope.ShowObjective == "false" && $scope.ShowAttachments == "false") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_1796.Caption'), "StepDOMID": "#step2" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "false" && $scope.ShowObjective == "true" && $scope.ShowAttachments == "false") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_701.Caption'), "StepDOMID": "#step3" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "false" && $scope.ShowObjective == "true" && $scope.ShowAttachments == "true") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_35.Caption'), "StepDOMID": "#step5" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_701.Caption'), "StepDOMID": "#step3" });
                        $scope.WizardSteps.push({ "StepId": 4, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "false" && $scope.ShowObjective == "false" && $scope.ShowAttachments == "true") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_35.Caption'), "StepDOMID": "#step5" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }
                    else if ($scope.ShowFinancial == "true" && $scope.ShowObjective == "false" && $scope.ShowAttachments == "true") {
                        $scope.WizardSteps.push({ "StepId": 2, "StepName": $translate.instant('LanguageContents.Res_35.Caption'), "StepDOMID": "#step5" });
                        $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_1796.Caption'), "StepDOMID": "#step2" });
                        $scope.WizardSteps.push({ "StepId": 4, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    }



                    GenerateTabNavigations(Id, Caption, CurrentEntityId);
                }
                else {
                    $scope.WizardSteps.push({ "StepId": 3, "StepName": $translate.instant('LanguageContents.Res_11.Caption'), "StepDOMID": "#step4" });
                    GenerateTabNavigations(Id, Caption, CurrentEntityId);
                }

            });
        }

        function GenerateTabNavigations(Id, Caption, CurrentEntityId) {
            var tabHtml = '';
            $("#SubentitywizardStepClass").html(
                                $compile(tabHtml)($scope));
            for (var t = 0, tab; tab = $scope.WizardSteps[t++];) {
                tabHtml += '<li data-target=\"' + tab.StepDOMID + '\"  ng-click=\"changeSubentityTab2($event)\"><span class="badge badge-info">' + tab.StepId + '</span>' + tab.StepName + '<span class="chevron"></span></li>';
            }
            $("#SubentitywizardStepClass").html(
                                $compile(tabHtml)($scope));

            SubentityTypeCreationTimeout.createdynwiz = setTimeout(function () {
                $('#SubentityMyWizard').wizard();
                $('#SubentityMyWizard').wizard('stepLoaded');
                window["tid_wizard_steps_all_complete_count"] = 0;
                window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
                $('#btnWizardNext').show();     //intially wizard Next button is enalbed
                $('#btnWizardPrev').hide();     //intially wizard Previous button is DISABLED
                $scope.CreateDynamicControls(Id, Caption, CurrentEntityId);
            }, 20);
        }

        //Wizard enable/disable setup
        $scope.Dropdown = [];

        // handling wizard tab click event in the wizard
        $scope.listAttriToAttriResult = [];
        $scope.ShowHideAttributeOnRelation = {};
        $scope.changeTab = function () {

            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
            var currentWizardStep = $('#SubentityMyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#SubentityMyWizard').wizard('totnumsteps').totstep;

            if (currentWizardStep === 1) {
                $('#btnWizardNext').show();
                $('#btnWizardPrev').hide();
            }
            else if (currentWizardStep === totalWizardSteps) {
                $('#btnWizardNext').hide();
                $('#btnWizardPrev').show();
            }

            else {
                $('#btnWizardNext').show();
                $('#btnWizardPrev').show();
            }
            if (currentWizardStep === 3) {
                SubentityTypeCreationTimeout.loadmetadata = setTimeout(function () { EntityMetadata(); }, 100);
            }

        }
        $scope.changeSubentityTab2 = function (e) {
            SubentityTypeCreationTimeout.btnclick = setTimeout(function () { $("#btnTempSub").click(); }, 100);
            $("#EntityMetadata").removeClass('notvalidate');
            if ($("#EntityMetadata .error").length > 0) {
                e.stopImmediatePropagation();
                e.stopPropagation();
                return false;
            }
        }

        function KeepAllStepsMarkedComplete() {
            $("#SubentityMyWizard ul.steps").find("li").addClass("complete");
            $("#SubentityMyWizard ul.steps").find("span.badge").addClass("badge-success");
            window["tid_wizard_steps_all_complete_count"]++;
            if (window["tid_wizard_steps_all_complete_count"] >= 4) {
                clearInterval(window["tid_wizard_steps_all_complete"]);
            }
        }

        // change previous tab in the wizard

        $scope.changePrevTab = function () {
            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
            $('#SubentityMyWizard').wizard('previous');
            var currentWizardStep = $('#SubentityMyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#SubentityMyWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep === 1) {
                $('#btnWizardPrev').hide();
            }
            if (currentWizardStep < totalWizardSteps) {
                //$('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
            }
            else {
                //$('#btnWizardFinish').show();
                $('#btnWizardNext').hide();
            }
        }

        // change next tab in the wizard

        $scope.changenexttab = function () {
            EntityMetadata();
            $("#EntityMetadata").removeClass('notvalidate');
            SubentityTypeCreationTimeout.btnclick = setTimeout(function () { $("#btnTempSub").click(); }, 100);
            if ($("#EntityMetadata .error").length > 0) {
                return false;
            }

            window["tid_wizard_steps_all_complete_count"] = 0;
            window["tid_wizard_steps_all_complete"] = setInterval(function () { KeepAllStepsMarkedComplete(); }, 25);
            $('#SubentityMyWizard').wizard('next', '');
            var currentWizardStep = $('#SubentityMyWizard').wizard('selectedItem').step;
            var totalWizardSteps = $('#SubentityMyWizard').wizard('totnumsteps').totstep;
            if (currentWizardStep > 1) {
                $('#btnWizardPrev').show();
            }
            if (currentWizardStep === totalWizardSteps) {
                //$('#btnWizardFinish').show();
                $('#btnWizardNext').hide();
            }
            else {
                //$('#btnWizardFinish').hide();
                $('#btnWizardNext').show();
            }
        }

        //InheritFrom Parent Functionality Start
        $scope.Inherritingtreelevels = {};
        $scope.InheritingLevelsitems = [];
        $scope.treeTexts = {};
        $scope.multiselecttreeTexts = {};
        $scope.treeSources = {};
        $scope.treeSourcesObj = [];
        $scope.UploadAttributeData = [];
        $scope.settreeSources = function () {
            var keys = [];
            angular.forEach($scope.treeSources, function (key) {
                keys.push(key);
                $scope.treeSourcesObj = keys;
            });
        }
        $scope.treeTextsObj = [];
        $scope.settreeTexts = function () {
            var keys2 = [];
            angular.forEach($scope.treeTexts, function (key) {
                keys2.push(key);
                $scope.treeTextsObj = keys2;
            });
        }
        $scope.treelevelsObj = [];
        $scope.settreelevels = function () {
            var keys1 = [];
            angular.forEach($scope.Inherritingtreelevels, function (key) {
                keys1.push(key);
                $scope.treelevelsObj = keys1;
            });
        }
        //InheritFrom Parent Functionality End


        //Tree multiselection part STARTED

        $scope.treeNodeSelectedHolder = new Array();
        $scope.PercentageVisibleSettings = {};
        $scope.DropDownTreePricing = {};
        //var apple_selected, treedata_avm, treedata_geography;
        var tree;
        $scope.my_tree_handler = function (branch, parentArr) {
            $scope.fields["Tree_" + branch.AttributeId].splice(0, $scope.fields["Tree_" + branch.AttributeId].length);
            var _ref;
            $scope.output = "You selected: " + branch.Caption;


            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }

            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolder.push(branch);
                }

            }
            else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == branch.AttributeId && e.id == branch.id; });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(branch), 1);

                }
            }

            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                treeTextVisbileflag = false;
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                }
                else
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }
            else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }

            if ($scope.treeNodeSelectedHolder.length > 0) {
                for (var j = 0; j < $scope.treeNodeSelectedHolder.length; j++) {
                    $scope.fields["Tree_" + branch.AttributeId].push($scope.treeNodeSelectedHolder[j].id);
                }
            }

            //----------------> Calling Attribute to Attribute relation on selecting
            //$scope.ShowHideAttributeToAttributeRelations(branch.AttributeId, 0, 0, 7);

        };

        function RemoveRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == child.AttributeId && e.id == child.id; });
                if (remainRecord.length > 0) {

                    $scope.treeNodeSelectedHolder.splice($scope.treeNodeSelectedHolder.indexOf(child), 1);
                    if (child.Children.length > 0) {
                        RemoveRecursiveChildTreenode(child.Children);
                    }
                }

            }
        }

        $scope.treesrcdirec = {};

        $scope.my_tree = tree = {};

        //Tree  multiselection part ended

        $scope.Parentid = 0;
        //getting cost center
        $scope.costCenterList = [];
        $scope.MemberLists = [];
        $scope.costcemtreObject = [];
        $scope.count = 1;
        $scope.createCostCentre = function (CurrentEntityId) {
            if ($scope.EntitytypeID != undefined) {
                SubentitycreationService.GetCostcentreforEntityCreation($scope.EntitytypeID, 2, parseInt(CurrentEntityId, 10)).then(function (costCenterList) {
                    $scope.costCenterList = costCenterList.Response;
                    $scope.costcemtreObject = [];
                    $scope.MemberLists = [];
                    for (var i = 0; i < $scope.costCenterList.length; i++) {
                        $scope.costcemtreObject.push({
                            "CostcenterId": $scope.costCenterList[i].Id, "costcentername": $scope.costCenterList[i].costcentername, "Sortorder": 1, "Isassociate": 1, "Isactive": 1, "OwnerName": $scope.costCenterList[i].username, "OwnerID": $scope.costCenterList[i].UserID
                        });
                        $scope.count = $scope.count + 1;
                    }
                });
            }

            $scope.treeCategory = [];
            if ($scope.EntitytypeID != undefined) {
                SubentitycreationService.GetCostcentreTreeforPlanCreation($scope.EntitytypeID, 0, parseInt(CurrentEntityId, 10)).then(function (costCenterList1) {
                    $("#dynamicCostCentreTreeOnSubEntity").html = "";
                    if (costCenterList1.Response != null && costCenterList1.Response != false) {
                        $scope.treeCategory = [];
                        $scope.treeCategory = JSON.parse(costCenterList1.Response);

                        $scope.dyn_Cont = '';
                        $scope.dyn_Cont += ' <input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_SubEntity" placeholder="Search" treecontext="treeNodeSearchDropdown_SubEntity"> ';
                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_SubEntity">';
                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                        $scope.dyn_Cont += '<costcentre-tree tree-filter="filterValue_SubEntity" tree-data=\"treeCategory\" accessable="false" tree-control="my_tree" on-select="OnCostCentreTreesSelection(branch,parent)" expand-level=\"100\"></costcentre-tree>';
                        $scope.dyn_Cont += '</div>';
                        $("#dynamicCostCentreTreeOnSubEntity").html($compile($scope.dyn_Cont)($scope));
                    }
                    else {
                        $scope.treeCategory = [];
                        $scope.dyn_Cont = '';
                        $scope.dyn_Cont += ' <input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_SubEntity" placeholder="Search" treecontext="treeNodeSearchDropdown_SubEntity"> ';
                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_SubEntity">';
                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                        $scope.dyn_Cont += '<costcentre-tree tree-filter="filterValue_SubEntity" tree-data=\"treeCategory\" accessable="false" tree-control="my_tree" on-select="OnCostCentreTreesSelection(branch,parent)" expand-level=\"100\"></costcentre-tree>';
                        $scope.dyn_Cont += '</div>';
                        $("#dynamicCostCentreTreeOnSubEntity").html($compile($scope.dyn_Cont)($scope));
                    }
                });
            }

        }
        $scope.GlobalMembersList = [];
        $scope.createGlobalMembers = function (CurrentEntityId) {
            SubentitycreationService.GetGlobalMembers(parseInt(CurrentEntityId, 10)).then(function (GlobalMembersList) {
                $scope.GlobalMembersList = GlobalMembersList.Response;
                for (var i = 0; i < $scope.GlobalMembersList.length; i++) {
                    if (parseInt($scope.GlobalMembersList[i].RoleID, 10) == 1)
                        $scope.MemberLists.push({ "TID": $scope.count, "UserEmail": $scope.GlobalMembersList[i].Email, "DepartmentName": "-", "Title": "-", "Roleid": 2, "RoleName": "Editor", "Userid": parseInt($scope.GlobalMembersList[i].UserID, 10), "UserName": $scope.GlobalMembersList[i].UserName, "IsInherited": $scope.GlobalMembersList[i].IsInherited, "InheritedFromEntityid": $scope.GlobalMembersList[i].InheritedFromEntityID, "FromGlobal": 1, "CostCentreID": 0, "InheritedFromEntityName": $scope.GlobalMembersList[i].InheritedFromEntityName });
                    else
                        $scope.MemberLists.push({ "TID": $scope.count, "UserEmail": $scope.GlobalMembersList[i].Email, "DepartmentName": "-", "Title": "-", "Roleid": parseInt($scope.GlobalMembersList[i].RoleID, 10), "RoleName": $scope.GlobalMembersList[i].Caption, "Userid": parseInt($scope.GlobalMembersList[i].UserID, 10), "UserName": $scope.GlobalMembersList[i].UserName, "IsInherited": $scope.GlobalMembersList[i].IsInherited, "InheritedFromEntityid": $scope.GlobalMembersList[i].InheritedFromEntityID, "FromGlobal": 1, "CostCentreID": 0, "InheritedFromEntityName": $scope.GlobalMembersList[i].InheritedFromEntityName });
                    $scope.count = $scope.count + 1;
                }
            });
        }

        $scope.addcostCentre = function () {
            if ($scope.costcentreid != undefined && $scope.costcentreid != null && $scope.costcentreid.length > 0) {
                $.each($scope.costcentreid, function (val, item) {
                    var result = $.grep($scope.costcemtreObject, function (e) { return e.CostcenterId == item; });
                    if (result.length == 0) {
                        var costCentrevalues = $.grep($scope.costCenterList, function (e) { return e.Id == parseInt(item) })[0];
                        $scope.costcemtreObject.push({
                            "CostcenterId": item, "costcentername": costCentrevalues.costcentername, "Sortorder": 1, "Isassociate": 1, "Isactive": 1, "OwnerName": costCentrevalues.username, "OwnerID": costCentrevalues.UserID
                        });
                        var memberObj = $.grep($scope.MemberLists, function (e) { return e.Roleid == 8 && e.Userid == item; });
                        if (memberObj.length == 0) {
                            $scope.MemberLists.push({ "TID": $scope.count, "UserEmail": costCentrevalues.usermail, "DepartmentName": costCentrevalues.Designation, "Title": costCentrevalues.Title, "Roleid": 8, "RoleName": 'BudgetApprover', "Userid": costCentrevalues.UserID, "UserName": costCentrevalues.username, "IsInherited": '0', "InheritedFromEntityid": '0', "FromGlobal": 1, "CostCentreID": item });
                            $scope.count = $scope.count + 1;
                        }
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1985.Caption'));
                    }
                });
            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_1920.Caption'));
            }
            SubentityTypeCreationTimeout.spliceto = setTimeout(function () {
                $scope.costcentreid.splice(0, $scope.costcentreid.length);
            }, 10);

            if ($scope.treeCategory.length > 0)
                $scope.RecursiveCostCentreTreeClearChecked($scope.treeCategory);
        }

        $scope.RecursiveCostCentreTreeClearChecked = function (Treeval) {
            $.each(Treeval, function (val, item) {
                item.ischecked = false;
                if (item.Children.length > 0) {
                    $scope.RecursiveCostCentreTreeClearChecked(item.Children);
                }
            });
        }

        $scope.deleteCostCentre = function (item) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2033.Caption'), function (result) {
                if (result) {
                    SubentityTypeCreationTimeout.memberlisto = setTimeout(function () {
                        $scope.costcemtreObject.splice($.inArray(item, $scope.costcemtreObject), 1);
                        var memberToRempve = $.grep($scope.MemberLists, function (e) { return e.CostCentreID == item.CostcenterId });
                        if (memberToRempve.length > 0)
                            $scope.MemberLists.splice($.inArray(memberToRempve[0], $scope.MemberLists), 1);
                    }, 100);
                }
            });
        };

        $scope.subEntityName = "";
        $scope.EntitytypeID = 0;
        var ownername = $cookies['Username'];
        var ownerid = $cookies['UserId'];
        $scope.ownerEmail = $cookies['UserEmail'];
        $scope.OwnerName = ownername;
        $scope.OwnerID = ownerid;
        $scope.AutoCompleteSelectedObj = [];
        $scope.OwnerList = [];
        $scope.treePreviewObj = {};
        $scope.owner = {};
        SubentitycreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
            $scope.owner = Getowner.Response;
            $scope.QuickInfo1AttributeCaption = $scope.owner.QuickInfo1AttributeCaption;
            $scope.QuickInfo2AttributeCaption = $scope.owner.QuickInfo2AttributeCaption;
            $scope.OwnerList.push({ "Roleid": 1, "RoleName": "Owner", "UserEmail": $scope.ownerEmail, "DepartmentName": $scope.owner.Designation, "Title": $scope.owner.Title, "Userid": parseInt($scope.OwnerID, 10), "UserName": $scope.OwnerName, "IsInherited": '0', "InheritedFromEntityid": '0', "QuickInfo1": $scope.owner.QuickInfo1, "QuickInfo2": $scope.owner.QuickInfo2 });
        });
        //User Autocomplete End

        // --- Define Controller Variables. ----------------- //
        $scope.contrls = '';
        function GetEntityTypeRoleAccess(rootID) {
            SubentitycreationService.GetEntityTypeRoleAccess(rootID).then(function (role) {
                $scope.Roles = role.Response;
            });
        }

        // Get the all the module to bind ng-grid
        $scope.wizard = {
            newval: ''
        };
        $scope.OptionObj = {

        };
        $scope.UserimageNewTime = new Date.create().getTime().toString();

        // option holder part for dynamic controls START
        $scope.fieldoptions = [];
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });

        }
        // option holder part for dynamic controls END

        $scope.dyn_Cont = '';
        //$scope.dyn_ContSpecial = '';

        /// dynamic value holding scope is start
        $scope.fields = {
            usersID: ''
        };
        $scope.fieldKeys = [];
        $scope.setFieldKeys = function () {
            var keys = [];

            angular.forEach($scope.fields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        /// dynamic value holding scopes end

        $scope.optionsLists = [];
        $scope.EnableDisableControlsHolder = {};
        $scope.AttributeData = [];
        $scope.entityName = "";
        $scope.EntityMemberData = [];
        $scope.addUsers = function () {
            var result = $.grep($scope.MemberLists, function (e) { return e.Userid == parseInt($scope.AutoCompleteSelectedObj[0].Id, 10) && e.Roleid == parseInt($scope.fields['userRoles'], 10) && e.FromGlobal == 0; });
            if (result.length == 0) {
                var membervalues = $.grep($scope.Roles, function (e) { return e.ID == parseInt($scope.fields['userRoles']) })[0];
                if (membervalues != undefined) {
                    if ($scope.AutoCompleteSelectedObj.length > 0) {
                        $scope.MemberLists.push({ "TID": $scope.count, "UserEmail": $scope.AutoCompleteSelectedObj[0].Email, "DepartmentName": $scope.AutoCompleteSelectedObj[0].Designation, "Title": $scope.AutoCompleteSelectedObj[0].Title, "Roleid": parseInt($scope.fields['userRoles'], 10), "RoleName": membervalues.Caption, "Userid": parseInt($scope.AutoCompleteSelectedObj[0].Id, 10), "UserName": $scope.AutoCompleteSelectedObj[0].FirstName + ' ' + $scope.AutoCompleteSelectedObj[0].LastName, "IsInherited": '0', "InheritedFromEntityid": '0', "FromGlobal": 0, "InheritedFromEntityName": $scope.AutoCompleteSelectedObj[0].InheritedFromEntityName, "QuickInfo1": $scope.AutoCompleteSelectedObj[0].QuickInfo1, "QuickInfo2": $scope.AutoCompleteSelectedObj[0].QuickInfo2 });
                        $scope.EntityMemberData.push({ "TID": $scope.count, "UserEmail": $scope.AutoCompleteSelectedObj[0].Email, "DepartmentName": $scope.AutoCompleteSelectedObj[0].Designation, "Title": $scope.AutoCompleteSelectedObj[0].Title, "Roleid": parseInt($scope.fields['userRoles'], 10), "RoleName": membervalues.Caption, "Userid": parseInt($scope.AutoCompleteSelectedObj[0].Id, 10), "UserName": $scope.AutoCompleteSelectedObj[0].FirstName + ' ' + $scope.AutoCompleteSelectedObj[0].LastName, "IsInherited": '0', "InheritedFromEntityid": '0', "FromGlobal": 0, "InheritedFromEntityName": $scope.AutoCompleteSelectedObj[0].InheritedFromEntityName, "QuickInfo1": $scope.AutoCompleteSelectedObj[0].QuickInfo1, "QuickInfo2": $scope.AutoCompleteSelectedObj[0].QuickInfo2 });
                        $scope.fields.usersID = '';
                        $scope.count = $scope.count + 1;
                        $scope.AutoCompleteSelectedObj = [];
                    }
                }
            }
            else {
                bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption'));
            }
        };

        $scope.deleteOptions = function (users) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2025.Caption'), function (result) {
                if (result) {
                    SubentityTypeCreationTimeout.memberlistsplice = setTimeout(function () {
                        $scope.MemberLists.splice($.inArray(users, $scope.MemberLists), 1);
                        $scope.EntityMemberData.splice($.inArray(users, $scope.EntityMemberData), 1);
                    }, 100);
                }
            });
        };
        $scope.Entityamountcurrencytypeitem = [];
        //add period block
        $scope.items = [];
        $scope.subentityperiods = [];
        $scope.lastSubmit = [];
        $scope.addNew = function () {
            var ItemCnt = $scope.items.length;
            if (ItemCnt > 0) {
                if ($scope.items[ItemCnt - 1].startDate == null || $scope.items[ItemCnt - 1].startDate.length == 0 || $scope.items[ItemCnt - 1].endDate.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1986.Caption'));
                    return false;
                }
                $scope.items.push({ startDate: null, endDate: null, comment: '', sortorder: 0 });
            }
        };
        $scope.submitOne = function (item) {
            $scope.lastSubmit = angular.copy(item);
        };
        $scope.deleteOne = function (item) {
            $scope.lastSubmit.splice($.inArray(item, $scope.lastSubmit), 1);
            $scope.items.splice($.inArray(item, $scope.items), 1);
        };
        $scope.submitAll = function () {
            $scope.lastSubmit = angular.copy($scope.items);
        }
        $scope.FinancialRequestObj = [];
        $scope.ImageFileName = '';
        $scope.nonMandatoryIDList = [];

        $scope.saveEntity = function () {
            var percentageflag = false;
            $('div[data-role="formpercentagetotalcontainer"]').children().find('span[data-role="percentageerror"]').each(function (index, value) {

                if (($(this).attr('data-selection') != undefined) && ($(this).attr('data-ispercentage') != undefined) && ($(this).attr('data-isnotfilter') != undefined)) {
                    if (((parseInt($(this).attr('data-selection')) > 1) && ($(this).attr('data-ispercentage') == "true") && ($(this).attr('data-isnotfilter') == "true")) && ($(this).hasClass("result lapse"))) {
                        percentageflag = true;
                    }
                }
            });
            if (percentageflag) {
                return false;
            }
            $("#EntityMetadata").removeClass('notvalidate');
            SubentityTypeCreationTimeout.btnclick = setTimeout(function () { $("#btnTempSub").click(); }, 100);
            if ($("#EntityMetadata .error").length > 0) {
                return false;
            }
            $("#btnWizardFinish").attr('disabled', 'disabled');
            $scope.AttributeData = [];
            for (var i = 0 ; i < $scope.nonMandatoryIDList.length; i++) {
                $scope.IDList.push($scope.nonMandatoryIDList[i]);
            }
            if ($scope.IsObjectiveFired == false) {
                var entityMetadataObj = {};
                var metadata = [];
                metadata = FormObjectiveMetadata();
                entityMetadataObj.AttributeData = metadata;
                entityMetadataObj.Periods = [];
                GetObjectivePeriods();
                entityMetadataObj.Periods.push($scope.subentityperiods);
                entityMetadataObj.EntityTypeID = parseInt($scope.EntitytypeID, 10);
                SubentitycreationService.GettingPredefineObjectivesForEntityMetadata(entityMetadataObj).then(function (entityAttributeDataObjData) {
                    try {

                        UpdateObjectiveScope(entityAttributeDataObjData.Response);

                        SubentityTypeCreationTimeout.processto = setTimeout(function () {
                            ProcessEntityCreation();
                        }, 100);


                    }
                    catch (ex) {
                        SubentityTypeCreationTimeout.processto = setTimeout(function () {
                            ProcessEntityCreation();
                        }, 100);

                    }

                });
            }
            else {
                SubentityTypeCreationTimeout.processtimeoout = setTimeout(function () {
                    ProcessEntityCreation();
                }, 100);
            }

        };


        function ProcessEntityCreation() {
            var SaveEntity = {};
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id != undefined) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                    "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
                                    "Value": "-1"
                                });
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {

                        var attributeLevelOptions = [];
                        attributeLevelOptions = ($.grep($scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID + ""], function (e) { return e.level == (j + 1); }));
                        if (attributeLevelOptions[0] != undefined) {
                            if (attributeLevelOptions[0].selection != undefined) {
                                for (var o = 0, opt; opt = attributeLevelOptions[0].selection[o++];) {
                                    var valueMatches = [];
                                    if (attributeLevelOptions[0].selection.length > 1)
                                        valueMatches = jQuery.grep(attributeLevelOptions[0].LevelOptions, function (relation) {
                                            return relation.NodeId.toString() === opt;
                                        });
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [opt],
                                        "Level": (j + 1),
                                        "Value": valueMatches[0] != undefined ? (valueMatches[0].value == "" ? "-1" : parseInt(valueMatches[0].value)) : "-1"
                                    });
                                }
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        var levelCount = $scope.atributesRelationList[i].Levels.length;
                        if (levelCount == 1) {
                            for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                    "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level,
                                    "Value": "-1"
                                });
                            }
                        }
                        else {
                            if ($scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "" && $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined) {
                                if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                    for (var k = 0; k < $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].length; k++) {
                                        $scope.AttributeData.push({
                                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                            "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                            "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].id],
                                            "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)][k].Level,
                                            "Value": "-1"
                                        });
                                    }
                                }
                                else {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": [$scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id],
                                        "Level": $scope.fields['MultiSelectDropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level,
                                        "Value": "-1"
                                    });
                                }
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) {
                        if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.OwnerList[0].Userid, 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                    else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                var value = $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined ? $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] : 0;

                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt(value, 10),
                                    "Level": 0,
                                    "Value": "-1"
                                });

                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name)
                        $scope.entityName = $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID];
                    else {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": $scope.fields['TextSingleLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                            "Level": 0,
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields['TextMultiLine_' + $scope.atributesRelationList[i].AttributeID].toString(),
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime) {
                    var MyDate = new Date.create();
                    var MyDateString;

                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        MyDateString = $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID];
                    }
                    else {
                        MyDateString = "";
                    }
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": MyDateString,
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == $scope.atributesRelationList[i].AttributeID; });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        $scope.AttributeData.push({
                            "AttributeID": $scope.atributesRelationList[i].AttributeID,
                            "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                            "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                            "NodeID": [parseInt(nodeval.id, 10)],
                            "Level": parseInt(nodeval.Level, 10),
                            "Value": "-1"
                        });
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields["TextMoney_" + $scope.atributesRelationList[i].AttributeID],
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": ($scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] == "" || $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] == false) ? 0 : 1,
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0 ; k < multiselectiObject.length; k++) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": parseInt(multiselectiObject[k], 10),
                                    "Level": 0,
                                    "Value": "-1"
                                });
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.ImageFileName,
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                    if ($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                        for (var j = 0; j < $scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID].length; j++) {
                            $scope.AttributeData.push({
                                "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                "NodeID": parseInt($scope.fields['ListTagwords_' + $scope.atributesRelationList[i].AttributeID][j], 10),
                                "Level": 0,
                                "Value": "-1"
                            });
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                    // $scope.Entityamountcurrencytypeitem.push({ amount: parseInt($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID].replace(/ /g, '')), currencytype: $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID], Attributeid: $scope.atributesRelationList[i].AttributeID });
                    if ($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] == "") {
                        $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = "0";
                    }
                    $scope.Entityamountcurrencytypeitem.push({ amount: parseFloat($scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID].replace(/ /g, '')), currencytype: $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID], Attributeid: $scope.atributesRelationList[i].AttributeID });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID],
                        "Level": 0,
                        "Value": "-1"
                    });
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 18) {

                    $scope.AttributeData.push({
                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                        "NodeID": "",
                        "Level": 0,
                        "Value": "-1"
                    });

                }
            }
            SaveEntity.ParentId = parseInt($scope.Parentid, 10);
            SaveEntity.Typeid = parseInt($scope.EntitytypeID, 10);
            SaveEntity.Active = true;
            SaveEntity.IsLock = false;
            SaveEntity.Name = $scope.entityName;
            SaveEntity.EntityMembers = [];
            SaveEntity.EntityCostRelations = $scope.costcemtreObject;
            SaveEntity.Periods = [];
            $scope.savesubentityperiods = [];
            SaveEntity.Entityamountcurrencytype = [];
            for (var m = 0; m < $scope.items.length; m++) {
                if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                    if ($scope.items[m].startDate.length != 0 && $scope.items[m].endDate.length != 0) {
                        $scope.savesubentityperiods.push({ startDate: ($scope.items[m].startDate.length != 0 ? ConvertDateToString($scope.items[m].startDate) : ''), endDate: ($scope.items[m].endDate.length != 0 ? ConvertDateToString($scope.items[m].endDate) : ''), comment: ($scope.items[m].comment.trim().length != 0 ? $scope.items[m].comment : ''), sortorder: 0 });
                    }
                }
            }
            SaveEntity.Periods.push($scope.savesubentityperiods);
            SaveEntity.AttributeData = $scope.AttributeData;
            SaveEntity.EntityMembers = $scope.EntityMemberData;
            SaveEntity.IObjectiveEntityValue = $scope.IDList;
            SaveEntity.FinancialRequestObj = $scope.FinancialRequestObj;

            if ($scope.AssetSelectionFiles.length == 0) {
                var res = [];
                for (var i = 0; i < $scope.SelectedAssetIdFolderID.SelectedAssetIDs.length; i++) {
                    res.push($scope.SelectedAssetIdFolderID.SelectedAssetIDs[i].AssetId);
                }
                SaveEntity.AssetArr = res;
            }
            else
                SaveEntity.AssetArr = $scope.AssetSelectionFiles;
            $scope.curram = [];
            for (var m = 0; m < $scope.Entityamountcurrencytypeitem.length; m++) {
                $scope.curram.push({ amount: $scope.Entityamountcurrencytypeitem[m].amount, currencytype: $scope.Entityamountcurrencytypeitem[m].currencytype, Attributeid: $scope.Entityamountcurrencytypeitem[m].Attributeid });
            }
            SaveEntity.Entityamountcurrencytype.push($scope.curram);
            SaveEntity.SelectedAssetIdFolderID = $scope.SelectedAssetIdFolderID.SelectedAssetIDs;
            SubentitycreationService.CreateEntity(SaveEntity).then(function (EntityList) {
                if (EntityList.Response > 0) {
                    $scope.EntityMemberData = [];
                    $scope.MemberLists = [];
                    $scope.fields = [];
                    $scope.entityObjectiveList = {};
                    $scope.entityAllObjectives = {};
                    $scope.MandatoryEntityObjectives = {};
                    $scope.MandatoryObjList = [];
                    $scope.NonMandatoryObjList = [];
                    $scope.IDList = [];
                    $scope.nonMandatoryIDList = [];

                    $('#dynamicControls').modal('hide');
                    $('#moduleContextmenu').modal('hide');
                    $scope.Activity.IsActivitySectionLoad = true;
                    SubentityTypeCreationTimeout.refreshentitylist = setTimeout(function () { $("#EntitiesTree").trigger("ActivityLoadEntity", [EntityList.Response]); }, 50);
                    $location.path('/mui/planningtool/default/detail/section/' + EntityList.Response + '/overview');
                    $scope.OwnerList = [];
                    $scope.treePreviewObj = {};
                    $scope.Entityamountcurrencytypeitem = [];
                    $scope.OwnerList.push({ "Roleid": 1, "RoleName": "Owner", "UserEmail": $scope.ownerEmail, "DepartmentName": "-", "Title": "-", "Userid": parseInt($scope.OwnerID, 10), "UserName": $scope.OwnerName, "IsInherited": '0', "InheritedFromEntityid": '0', "InheritedFromEntityName": "-" });
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4341.Caption'));
                    $("#btnWizardFinish").attr('disabled', false);
                }
                SubentityTypeCreationTimeout.closepopup = setTimeout(function () { $modalInstance.dismiss('cancel') }, 100);
                $("#EntityMetadata").html('');
            });
        }
        //$(window).on("GloblaUniqueKeyAccess", function (event, UniqueKey) {
        //    $scope.UniqueKey = UniqueKey;
        //});
        $scope.addRootEntity = function () {
            $scope.EnableAdd = true;
            $scope.EnableUpdate = false;
            $scope.step = 0;
            $scope.EnableOptionUpdate = false;
            $scope.EnableOptionAdd = true;
        };
        $scope.treelevels = [];
        $scope.tree = {};
        $scope.MultiSelecttree = {};
        $scope.Options = {};
        function SubentityTypeCreation(Id, Caption, CurrentEntityId) {
            if (Id != undefined) {
                GetEntityTypeRoleAccess(Id);
            }
            $scope.OwnerList = [];
            $scope.treeNodeSelectedHolder = [];
            $scope.treesrcdirec = {};
            $scope.owner = {};
            //SubentitycreationService.GetUserById(parseInt($scope.OwnerID, 10)).then(function (Getowner) {
            //    $scope.owner = Getowner.Response;
            //    $scope.OwnerList.push({ "Roleid": 1, "RoleName": "Owner", "UserEmail": $scope.ownerEmail, "DepartmentName": $scope.owner.Designation, "Title": $scope.owner.Title, "Userid": parseInt($scope.OwnerID, 10), "UserName": $scope.OwnerName, "IsInherited": '0', "InheritedFromEntityid": '0', "QuickInfo1": $scope.owner.QuickInfo1, "QuickInfo2": $scope.owner.QuickInfo2 });
            //});
            $scope.Parentid = CurrentEntityId;
            $scope.createCostCentre(CurrentEntityId);
            $scope.createGlobalMembers(CurrentEntityId);
            $scope.WizardSteps = [{ "StepId": 1, "StepName": $translate.instant('LanguageContents.Res_251.Caption'), "StepDOMID": "#step1" }];
            var tabHtml = '';
            $("#SubentitywizardStepClass").html(
                                $compile(tabHtml)($scope));
            GetEntityTypeTabCollections(Id, Caption, CurrentEntityId);
        }

        $scope.CreateDynamicControls = function (Id, Caption, CurrentEntityId) {
            $("#btnWizardFinish").removeAttr('disabled');
            $("#EntityMetadata").html('');
            $("#EntityMetadata").scrollTop(0);
            $("#EntityMetadata").html('<div class="loadingSubPopup"><img src="assets/img/loading.gif"> Loading...</div>');
            $scope.subEntityName = Caption;
            $scope.EntitytypeID = Id;
            $scope.items = [];
            $scope.DateObject = {};
            //--------------------------> GET ATTRIBUTE TO ATTRIBUTE RELATIONS <------------------------------
            SubentitycreationService.GetAttributeToAttributeRelationsByIDForEntity(Id).then(function (entityAttrToAttrRelation) {
                if (entityAttrToAttrRelation.Response != null) {
                    if (entityAttrToAttrRelation.Response.length > 0) {
                        $scope.listAttriToAttriResult = entityAttrToAttrRelation.Response;
                    }
                }
            });
            SubentitycreationService.GetEntityTypeAttributeRelationWithLevelsByID(Id, CurrentEntityId).then(function (entityAttributesRelation) {
                $scope.atributesRelationList = entityAttributesRelation.Response;
                $scope.dyn_Cont = '';
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                        var totLevelCnt = $scope.atributesRelationList[i].Levels.length;
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                            $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                            $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                            if (j == 0) {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.treeSources["dropdown_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree);
                                $scope.EnableDisableControlsHolder["Treedropdown_" + $scope.atributesRelationList[i].AttributeID] = false;

                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Treedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                $scope.settreeSources();
                            }
                            else {
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                $scope.dyn_Cont += "<input ui-select2=\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.Treedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt + ",6 )\" ng-model=\"fields.DropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"DropDownTree_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                            }

                            $scope.setFieldKeys();
                        }
                        try {
                            var CaptionObj = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0].split(",");
                            var LabelObject = $scope.atributesRelationList[i].Lable[0];
                            for (var j = 0; j < LabelObject.length; j++) {
                                if (j == 0) {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = CaptionObj[j].trim();
                                        $scope.settreeTexts();
                                    }
                                    else {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                }
                                else {
                                    if (CaptionObj[j] != undefined) {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = CaptionObj[j].trim();
                                        $scope.settreeTexts();
                                    }
                                    else {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.treeTexts["dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                }
                            }
                            $scope.Inherritingtreelevels["dropdown_levels_" + $scope.atributesRelationList[i].ID] = $scope.InheritingLevelsitems;
                            $scope.settreelevels();
                            $scope.InheritingLevelsitems = [];
                            $scope.settreeTexts();
                            $scope.settreelevels();
                        }
                        catch (ex) {
                        }
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 7) {
                        $scope.fields["Tree_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree).Children;
                        GetTreeObjecttoSave($scope.atributesRelationList[i].AttributeID);
                        if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            treeTextVisbileflag = false;
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationList[i].AttributeID])) {
                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = true;
                            }
                            else
                                $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                        }
                        else {
                            $scope.treePreviewObj["Attr_" + $scope.atributesRelationList[i].AttributeID] = false;
                        }
                        $scope.dyn_Cont += '<div ng-show=\"ShowOrHideAttributeToAttributeRelation(\'' + $scope.atributesRelationList[i].AttributeID + '_0\')\" class="control-group treeNode-control-group">';
                        $scope.dyn_Cont += '<label class="control-label">' + $scope.atributesRelationList[i].AttributeCaption + '</label>';
                        $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                        $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" placeholder="Search" treecontext="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '"></div>';
                        $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdown_Attr_' + $scope.atributesRelationList[i].AttributeID + '">';
                        $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                        $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationList[i].AttributeID + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" accessable="' + $scope.atributesRelationList[i].IsReadOnly + '" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                        $scope.dyn_Cont += '</div></div></div>';
                        $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationList[i].AttributeID + '\">';
                        $scope.dyn_Cont += '<div class="controls">';
                        $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationList[i].AttributeID + '\" node-attributeid="' + $scope.atributesRelationList[i].AttributeID + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                        $scope.dyn_Cont += '</div></div>';
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                        var totLevelCnt1 = $scope.atributesRelationList[i].Levels.length;
                        for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {

                            if (totLevelCnt1 == 1) {

                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;

                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.Selection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                $scope.setFieldKeys();
                            }

                            else {
                                $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = true;
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)] = {};
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatResult = function (item) { return item.Caption };
                                $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].formatSelection = function (item) { return item.Caption };
                                if (j == 0) {
                                    $scope.treeSources["multiselectdropdown_" + $scope.atributesRelationList[i].AttributeID] = JSON.parse($scope.atributesRelationList[i].tree);
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = JSON.parse($scope.atributesRelationList[i].tree).Children;
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;

                                    $scope.EnableDisableControlsHolder["MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID] = false;
                                    $scope.dyn_Cont += "<div  ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                    $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                    $scope.settreeSources();
                                }
                                else {
                                    $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].data = [];

                                    if (j == ($scope.atributesRelationList[i].Levels.length - 1)) {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = true;
                                        $scope.dyn_Cont += "<div  ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\"> ";
                                        $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                    }
                                    else {
                                        $scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1)].multiple = false;
                                        $scope.dyn_Cont += "<div  ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.MultiSelectDropDown_ " + $scope.atributesRelationList[i].ID + "\">" + $scope.atributesRelationList[i].Levels[j].LevelName + " </label><div class=\"controls\">";
                                        $scope.dyn_Cont += "<input ui-select2 =\"Dropdown.OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" ng-disabled=\"EnableDisableControlsHolder.MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID + "\" ng-change=\"LoadSubLevels(" + $scope.atributesRelationList[i].AttributeID + ", " + (j + 1) + ", " + totLevelCnt1 + ",12 )\" ng-model=\"fields.MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\"  id=\"TreeMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "_" + (j + 1) + "\" type=\"hidden\" /></div></div>";
                                    }
                                }
                                $scope.setFieldKeys();
                            }
                        }
                        try {
                            var CaptionObj = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0].toString().split(",");
                            var LabelObject = $scope.atributesRelationList[i].Lable[0];
                            for (var j = 0; j < LabelObject.length; j++) {
                                if (j == 0) {
                                    if ($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][j] != undefined) {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][j].trim();
                                        $scope.settreeTexts();
                                    }
                                    else {
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                        $scope.settreeTexts();
                                    }
                                }
                                else {
                                    if (j == (LabelObject.length - 1)) {
                                        var k = j;
                                        $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = [];
                                        $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                        for (k; k < ($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0].length) ; k++) {
                                            if ($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][k] != undefined) {
                                                $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)].push($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][k].trim());
                                            }
                                            else {
                                                $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                            }
                                            $scope.settreeTexts();
                                        }
                                    }
                                    else {
                                        if ($scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][j] != undefined) {
                                            $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                            $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = $scope.atributesRelationList[i].ParentTreeLevelValueCaption[0][j].trim();
                                            $scope.settreeTexts();
                                        }
                                        else {
                                            $scope.InheritingLevelsitems.push({ caption: LabelObject[j].Label, level: j + 1 });
                                            $scope.multiselecttreeTexts["multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + (j + 1)] = "-";
                                            $scope.settreeTexts();
                                        }
                                    }
                                }
                            }
                            $scope.Inherritingtreelevels["multiselectdropdown_levels_" + $scope.atributesRelationList[i].ID] = $scope.InheritingLevelsitems;
                            $scope.settreelevels();
                            $scope.InheritingLevelsitems = [];
                            $scope.settreeTexts();
                            $scope.settreelevels();
                        }
                        catch (ex) { }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        if ($scope.atributesRelationList[i].IsSpecial == true) {
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Owner) {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"> <input ng-disabled=\"EnableDisableControlsHolder.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" dirownernameautopopulate placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                                $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.OwnerList[0].UserName;
                                $scope.setFieldKeys();
                            }
                        }
                        else if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.EntityStatus) {
                        }
                        else if ($scope.atributesRelationList[i].IsSpecial == false) {
                            $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                            $scope.setoptions();
                            $scope.EnableDisableControlsHolder["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                            $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListSingleSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> <select ui-select2  ng-disabled=\"EnableDisableControlsHolder.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" > <option value=\"\">Select " + $scope.atributesRelationList[i].AttributeCaption + "</option><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \"value=\"{{ndata.Id}}\">{{ndata.Caption}}</option> </select></div></div>";
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                            $scope.setFieldKeys();
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        if ($scope.atributesRelationList[i].AttributeID != 70) {
                            $scope.EnableDisableControlsHolder["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                            if ($scope.atributesRelationList[i].AttributeID == SystemDefiendAttributes.Name) {
                                $scope.dyn_Cont += "<div class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                            } else {
                                $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\" ng-disabled=\"EnableDisableControlsHolder.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"></div></div>";
                            }
                            $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                            $scope.setFieldKeys();
                        }
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group control-group-textarea\"><label class=\"control-label\" for=\"fields.TextMultiLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><textarea class=\"small-textarea\" ng-disabled=\"EnableDisableControlsHolder.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" name=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMultiLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\" rows=\"3\"></textarea></div></div>";
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        $scope.EnableDisableControlsHolder["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.ListMultiSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label><div class=\"controls\"> ";
                        if ($scope.atributesRelationList[i].IsReadOnly)
                            $scope.dyn_Cont += "<select class=\"multiselect\"   data-placeholder=\"Select filter\" multiselect-dropdown disabled ng-disabled=\"EnableDisableControlsHolder.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\" ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\"></select></div></div>";
                        else
                            $scope.dyn_Cont += "<select class=\"multiselect\"   data-placeholder=\"Select filter\" multiselect-dropdown  ng-disabled=\"EnableDisableControlsHolder.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\" multiple=\"multiple\" ng-model=\"fields.ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-options=\"ndata.Id as ndata.Caption for ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + "\" ></select></div></div>";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 5 && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.ApproveTime && $scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.CreationDate) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        if ($scope.atributesRelationList[i].Caption == "Due Date") {
                            $scope.IsOpend = "DueDate";
                            $scope.DateObject.DueDate = false;
                            var isopenedhtmlstr = "DateObject.DueDate";
                        } else if ($scope.atributesRelationList[i].Caption == "StartDate") {
                            $scope.IsOpend = "StartDate";
                            var isopenedhtmlstr = "DateObject.StartDate";
                            $scope.DateObject.StartDate = false;
                        }
                        else {
                            $scope.IsOpend = "EndDate";
                            $scope.DateObject.EndDate = false;
                            var isopenedhtmlstr = "DateObject.EndDate";
                        }
                        $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.fields["DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + $scope.MinValue);
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {

                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + $scope.MaxValue);
                        }
                        else {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }

                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.DateTime_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"text\" ng-change=\"setTimeout(changeduedate_changed(fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "," + $scope.atributesRelationList[i].AttributeID + "),3000)\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event,DateObject,'" + $scope.IsOpend + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"" + isopenedhtmlstr + "\" min=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        var param1 = new Date.create();
                        var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        $scope.setFieldKeys();
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextMoney_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"text\" ng-model=\"fields.TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"TextMoney_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        $scope.fields["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 13) {
                        $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = true;
                        $scope.DropDownTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeID.toString() + ""] = $scope.atributesRelationList[i].DropDownPricing;
                        if ($scope.atributesRelationList[i].IsReadOnly == true)
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        else
                            $scope.EnableDisableControlsHolder["Selection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div drowdowntreepercentagemultiselection data-purpose='entity' data-attributeid=" + $scope.atributesRelationList[i].AttributeID.toString() + "></div>";


                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.CheckBoxSelection_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  type=\"checkbox\" ng-model=\"fields.CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        $scope.fields["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        $scope.EnableDisableControlsHolder["Period_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        try {
                            if ($scope.atributesRelationList[i].InheritFromParent) {
                                if ($scope.atributesRelationList[i].ParentValue == "-") {
                                    $scope.items.push({ startDate: null, endDate: null, comment: '', sortorder: 0, calstartopen: false, calendopen: false });
                                }
                                else {
                                    if ($scope.atributesRelationList[i].ParentValue.length > 0) {
                                        for (var j = 0 ; j < $scope.atributesRelationList[i].ParentValue[0].length; j++) {
                                            var datStartUTCval = "";
                                            var datstartval = "";
                                            var datEndUTCval = "";
                                            var datendval = "";
                                            datStartUTCval = $scope.atributesRelationList[i].ParentValue[0][j].Startdate.substr(0, 10);
                                            datstartval = new Date.create((datStartUTCval));
                                            datEndUTCval = $scope.atributesRelationList[i].ParentValue[0][j].EndDate.substr(0, 10);
                                            datendval = new Date.create((datEndUTCval));
                                            $scope.items.push({ startDate: (ConvertDateToString(datstartval)), endDate: (ConvertDateToString(datendval)), comment: $scope.atributesRelationList[i].ParentValue[0][j].Description, sortorder: 0, calstartopen: false, calendopen: false });
                                        }
                                    }
                                    else {
                                        $scope.items.push({ startDate: null, endDate: null, comment: '', sortorder: 0, calstartopen: false, calendopen: false });
                                    }
                                }
                            }
                            else {
                                $scope.items.push({ startDate: null, endDate: null, comment: '', sortorder: 0, calstartopen: false, calendopen: false });
                            }
                        }
                        catch (e) { }
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                        }
                        else {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                        }
                        else {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }
                        var temp = $scope.HolidayListCalculation($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID], $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID]);
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MinDate);
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = (temp.MaxDate);
                        $scope.dyn_Cont += "    <div class=\"control-group\"><label for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\" class=\"control-label\">" + $scope.atributesRelationList[i].Caption + "</label><div class=\"controls\"><div class=\"period\" ng-repeat=\"item in items\" ng-form=\"subForm\">";
                        $scope.dyn_Cont += "<div class=\"row-fluid\"><div class=\"inputHolder span5\">";
                        $scope.dyn_Cont += "<input class=\"sdate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Period_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" name=\"startDate\" ng-click=\"PeriodCalanderopen($event,item,'start')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calstartopen\" min=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  ng-change=\"changeperioddate_changed(item.startDate,'StartDate')\" ng-model=\"item.startDate\" placeholder=\"-- Start date --\"/>";
                        $scope.dyn_Cont += "<input class=\"edate Period_" + $scope.atributesRelationList[i].AttributeID + "\" ng-disabled=\"EnableDisableControlsHolder.Period_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" name=\"enddate\" ng-change=\"changeperioddate_changed(item.endDate,'EndDate')\" ng-model=\"item.endDate\"  ng-click=\"PeriodCalanderopen($event,item,'end')\"  datepicker-popup=\"{{format}}\"  is-open=\"item.calendopen\" min=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- End date --\"/>";
                        $scope.dyn_Cont += "<input class=\"dateComment\" ng-disabled=\"EnableDisableControlsHolder.Period_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\"  name=\"comment\" ng-model=\"item.comment\" placeholder=\"-- " + $scope.atributesRelationList[i].Caption + " Comment --\" />";
                        $scope.dyn_Cont += "</div><div class=\"buttonHolder span1\">";
                        $scope.dyn_Cont += "<a ng-show=\"$first==false\" ng-click=\"deleteOne(item)\"><i class=\"icon-remove\"></i></a></div></div></div><a ng-click=\"addNew()\">[Add " + $scope.atributesRelationList[i].Caption + "]</a></div></div>";
                        $scope.fields["Period_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.fields["DatePart_Calander_Open" + "item.startDate"] = false;
                        $scope.fields["DatePart_Calander_Open" + "item.endDate"] = false;
                        $scope.setFieldKeys();
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                        StrartUpload_UploaderAttrSubEntity();
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].Options;
                        $scope.setoptions();
                        $scope.dyn_Cont += '<div ng-show=\"ShowOrHideAttributeToAttributeRelation(\'' + $scope.atributesRelationList[i].AttributeID + '_0\',' + $scope.atributesRelationList[i].AttributeTypeID + ')\" class="control-group"><label class="control-label"';
                        $scope.dyn_Cont += 'for="fields.Uploader_ ' + $scope.atributesRelationList[i].AttributeID + '">' + $scope.atributesRelationList[i].AttributeCaption + ': </label>';
                        $scope.dyn_Cont += '<div id="Uploader" class="controls">';
                        $scope.dyn_Cont += '<img id="UploaderPreview_' + $scope.atributesRelationList[i].AttributeID + '" class="ng-pristine ng-valid entityImgPreview"';
                        $scope.dyn_Cont += ' ng-model="fields.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" id="UploaderImageControl" src="" alt="No thumbnail present">';
                        $scope.dyn_Cont += '<br><a ng-show="EnableDisableControlsHolder.Uploader_' + $scope.atributesRelationList[i].AttributeID + '" ng-model="UploadImage" ng-click="UploadImagefile(' + $scope.atributesRelationList[i].AttributeID + ')" class="ng-pristine ng-valid">Select Image</a>';
                        $scope.dyn_Cont += '</div></div>';
                        $scope.fields["Uploader_" + $scope.atributesRelationList[i].AttributeID] = "";
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 17) {
                        $scope.fields["ListTagwords_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.OptionObj["tagoption_" + $scope.atributesRelationList[i].AttributeID] = [];
                        $scope.setoptions();
                        $scope.tempscope = [];
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\">";
                        $scope.dyn_Cont += "<label class=\"control-label\" for=\"fields.ListTagwords_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].AttributeCaption + " </label>";
                        $scope.dyn_Cont += "<div class=\"controls\">";
                        $scope.dyn_Cont += "<directive-tagwords item-attrid = \"" + $scope.atributesRelationList[i].AttributeID + "\" item-show-hide-progress =\"null\" item-tagword-id=\"fields.ListTagwords_" + $scope.atributesRelationList[i].AttributeID + "\" item-tagword-list=\"tempscope\"></directive-tagwords>";
                        $scope.dyn_Cont += "</div></div>";

                        if ($scope.atributesRelationList[i].InheritFromParent) {
                        }
                        else {
                        }
                        $scope.setFieldKeys();
                    } else if ($scope.atributesRelationList[i].AttributeTypeID == 16) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        $scope.MinValue = $scope.atributesRelationList[i].MinValue;
                        $scope.MaxValue = $scope.atributesRelationList[i].MaxValue;
                        $scope.IsOpend = "DateAction";
                        var isopenedhtmlstr = "DateObject.DateAction";
                        $scope.DateObject = { "DateAction": false };
                        $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        if ($scope.MinValue < 0) {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue + 1));
                        }
                        else {
                            $scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MinValue));
                        }
                        if ($scope.MaxValue >= 0 && $scope.MinValue < $scope.MaxValue) {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + ($scope.MaxValue - 1));
                        }
                        else {
                            $scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].setDate($scope.fields["DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID].getDate() + 100000);
                        }
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0')\" class=\"control-group\"><label class=\"control-label\" for=\"fields.DatePart_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input ng-disabled=\"EnableDisableControlsHolder.DateTime_" + $scope.atributesRelationList[i].AttributeID + "\" type=\"text\" ng-model=\"fields.DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"DatePart_" + $scope.atributesRelationList[i].AttributeID + "\" ng-click=\"Calanderopen($event,DateObject,'" + $scope.IsOpend + "')\"  datepicker-popup=\"{{format}}\"  is-open=\"" + isopenedhtmlstr + "\" min=\"fields.DatePartMinDate_" + $scope.atributesRelationList[i].AttributeID + "\" max=\"fields.DatePartMaxDate_" + $scope.atributesRelationList[i].AttributeID + "\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"" + $scope.atributesRelationList[i].Caption + "\"></div></div>";
                        var param1 = new Date.create();
                        var param2 = param1.getDate() + '/' + param1.getMonth() + '/' + param1.getFullYear();
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = new Date.create();
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = null;
                        $scope.fields["fields.DatePart_Calander_Open" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.setFieldKeys();
                    }

                    else if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                        $scope.ShowHideAttributeOnRelation["Attribute_" + $scope.atributesRelationList[i].AttributeID] = true;
                        $scope.EnableDisableControlsHolder["dListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.EnableDisableControlsHolder["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = false;
                        $scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID] = $scope.CurrencyFormatsList;
                        $scope['origninalamountvalue_' + $scope.atributesRelationList[i].AttributeID] = '0';
                        $scope['currRate_' + $scope.atributesRelationList[i].AttributeID] = 1;
                        $scope.setoptions();
                        $scope.dyn_Cont += "<div ng-show=\"ShowOrHideAttributeToAttributeRelation('" + $scope.atributesRelationList[i].AttributeID + "_0'," + $scope.atributesRelationList[i].AttributeTypeID + ")\" class=\"control-group\"><label class=\"control-label\" for=\"fields.TextSingleLine_ " + $scope.atributesRelationList[i].AttributeID + "\">" + $scope.atributesRelationList[i].Caption + " </label><div class=\"controls\"><input type=\"text\"  class=\"currencyTextbox   margin-right5x\" ng-change=\"Getamountentered(" + $scope.atributesRelationList[i].AttributeID + ")\" ng-disabled=\"EnableDisableControlsHolder.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-model=\"fields.dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" id=\"dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID + "\" placeholder=\"" + $scope.atributesRelationList[i].PlaceHolderValue + "\"> <select ui-select2 ng-disabled=\"EnableDisableControlsHolder.dListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\" ng-model=\"fields.ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  id=\"ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID + "\"  ng-change=\"GetCostCentreCurrencyRateById(" + $scope.atributesRelationList[i].AttributeID + ")\" class=\"currencySelector\" ><option ng-repeat=\"ndata in OptionObj.option_" + $scope.atributesRelationList[i].AttributeID + " \" value=\"{{ndata.Id}}\">{{ndata.Name}}</option></select></div></div>";
                        $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = '0';
                        $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.DefaultSettings.CurrencyFormat.Id;
                        $scope.setFieldKeys();

                    }

                    $scope.setFieldKeys();
                    $scope.setoptions();
                }
                $scope.dyn_Cont += '<input style="visibility:hidden" type="submit" id="btnTempSub" class="ng-scope" invisible>';
                $scope.setFieldKeys();
                $("#EntityMetadata").html('<div class="row-fluid"><div data-col="1" class="span4"></div><div data-col="2" class="span8"></div></div> ');
                $("#EntityMetadata").html(
                    $compile($scope.dyn_Cont)($scope));
                //$("[id^='dTextSingleLine_']").autoNumeric('init',$scope.DecimalSettings['FinancialAutoNumeric']);
                var mar = ($scope.DecimalSettings.FinancialAutoNumeric.vMax).substr(($scope.DecimalSettings.FinancialAutoNumeric.vMax).indexOf(".") + 1);
                $scope.mindec = "";
                if (mar.length == 1) {
                    $("[id^='dTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: "0", mDec: "1" });
                }
                if (mar.length != 1) {
                    if (mar.length < 5) {
                        $scope.mindec = "0.";

                        for (i = 0; i < mar.length; i++) {
                            $scope.mindec = $scope.mindec + "0";
                        }
                        $("[id^='dTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: $scope.mindec });
                    }
                    else {
                        $("[id^='dTextSingleLine_']").autoNumeric('init', { aSep: ' ', vMin: "0", mDec: "0" });
                    }
                }
                $("[id^='MTextMoney_']").autoNumeric('init', { aSep: ' ', vMin: "0", mDec: "0" });
                $("[id^='MTextMoney_']").keydown(function (event) {
                    if (event.which != 8 && isNaN(String.fromCharCode(event.which)) && (event.keyCode < 96 || event.keyCode > 105) && event.which != 46) {
                        bootbox.alert($translate.instant('Please enter only Number'));
                        event.preventDefault(); //stop character from entering input
                    }
                });
                $("#EntityMetadata").scrollTop(0);
                refreshAssetobjects();
                $scope.$broadcast('callbackforEntitycreation', 0, CurrentEntityId, 4);
                GetValidationList(Id);
                $("#EntityMetadata").addClass('notvalidate');

                SubentityTypeCreationTimeout.updateto = setTimeout(function () {
                    //--------> HIDE ALL THE ATTRIBUTE RELAIONS AFTER LOADING <--------------------
                    //HideAttributeToAttributeRelationsOnPageLoad();
                    UpdateInheritFromParentScopes();
                });
            });
        };

        var treeTextVisbileflag = false;
        function IsNotEmptyTree(treeObj) {

            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    treeTextVisbileflag = true;
                    return treeTextVisbileflag;
                }
                else {
                    IsNotEmptyTree(node.Children);
                }
            }
            return treeTextVisbileflag;
        }

        function GetTreeObjecttoSave(attributeid) {
            GenerateTreeStructure($scope.treesrcdirec["Attr_" + attributeid]);
        }
        var treeformflag = false;
        function GenerateTreeStructure(treeobj) {
            if (treeobj.length > 0) {
                for (var i = 0, node; node = treeobj[i++];) {

                    if (node.ischecked == true) {
                        var remainRecord = [];
                        remainRecord = $.grep($scope.treeNodeSelectedHolder, function (e) { return e.AttributeId == node.AttributeId && e.id == node.id; });
                        if (remainRecord.length == 0) {
                            $scope.treeNodeSelectedHolder.push(node);
                        }
                        treeformflag = false;
                        if (ischildSelected(node.Children)) {
                            GenerateTreeStructure(node.Children);
                        }
                        else {
                            GenerateTreeStructure(node.Children);
                        }
                    }
                    else
                        GenerateTreeStructure(node.Children);
                }

            }
        }


        function ischildSelected(children) {

            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    treeformflag = true;
                    return treeformflag
                }
            }
            return treeformflag;
        }

        function UpdateInheritFromParentScopes() {
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) {
                    } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                            var attrInheritval = ($.grep($scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID], function (e) { return e.Id == $scope.atributesRelationList[i].ParentValue[0] }));
                            if (attrInheritval != undefined) {
                                if (attrInheritval.length > 0) {
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = attrInheritval[0].Id;
                                }
                            }
                        }
                        else {
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                        }

                        if ($scope.atributesRelationList[i].IsReadOnly) {
                            $scope.EnableDisableControlsHolder["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        }
                    }
                }
                if ($scope.atributesRelationList[i].AttributeTypeID == 19) {
                    if ($scope.atributesRelationList[i].IsSpecial == true) {
                    } else if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                            var attrInheritval = ($.grep($scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID], function (e) { return e.Id == $scope.atributesRelationList[i].ParentValue[0]["Currencytypeid"] }));
                            if (attrInheritval != undefined) {
                                if (attrInheritval.length > 0) {
                                    $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = attrInheritval[0]["Id"];
                                    $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = ($scope.atributesRelationList[i].ParentValue[0]["Amount"]).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ').toString();
                                }
                            }
                        }
                        else {
                            $scope.fields["ListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = $scope.DefaultSettings.CurrencyFormat.Id;
                            $scope.fields["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = '0';
                        }

                        if ($scope.atributesRelationList[i].IsReadOnly) {
                            $scope.EnableDisableControlsHolder["dListSingleSelection_" + $scope.atributesRelationList[i].AttributeID] = true;
                            $scope.EnableDisableControlsHolder["dTextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = true;
                        }
                    }
                }

                else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.atributesRelationList[i].IsSpecial == false) {
                        if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                            if ($scope.atributesRelationList[i].ParentValue[0] != undefined) {
                                $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                                for (var j = 0; j < $scope.atributesRelationList[i].ParentValue[0].length; j++) {
                                    var attrInheritval = ($.grep($scope.OptionObj["option_" + $scope.atributesRelationList[i].AttributeID], function (e) { return e.Id == $scope.atributesRelationList[i].ParentValue[0][j] }));
                                    if (attrInheritval != undefined) {
                                        if (attrInheritval.length > 0) {
                                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID].push(attrInheritval[0].Id);
                                        }
                                    }

                                }
                            }
                        }
                        else {
                            $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = [];
                            var defaultmultiselectvalue = $scope.atributesRelationList[i].DefaultValue.split(',');
                            if ($scope.atributesRelationList[i].DefaultValue != "") {
                                for (var j = 0; j < defaultmultiselectvalue.length; j++) {
                                    $scope.fields["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID].push($.grep($scope.atributesRelationList[i].Options, function (e) { return e.Id == defaultmultiselectvalue[j]; })[0].Id);
                                }
                            }
                        }
                        if ($scope.atributesRelationList[i].IsReadOnly) {
                            $scope.EnableDisableControlsHolder["ListMultiSelection_" + $scope.atributesRelationList[i].AttributeID] = true;
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                        var levelCount = $scope.Inherritingtreelevels["dropdown_levels_" + $scope.atributesRelationList[i].ID].length
                        for (var j = 1; j <= levelCount; j++) {
                            var dropdown_text = "dropdown_text_" + $scope.atributesRelationList[i].ID + "_" + j;
                            if (j == 1) {
                                if ($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j)].data.length > 0) {
                                    $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.treeSources["dropdown_" + $scope.atributesRelationList[i].AttributeID].Children, function (e) { return e.Caption == $scope.treeTexts[dropdown_text] }))[0];
                                }
                            }
                            else {
                                if ($scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)] != null)
                                    if ($scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children != undefined)
                                        $scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.fields["DropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) { return e.Caption == $scope.treeTexts[dropdown_text] }))[0];
                            }
                        }
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Treedropdown_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 12) {
                    if (($scope.atributesRelationList[i].InheritFromParent) || ($scope.atributesRelationList[i].ChooseFromParentOnly == true && $scope.atributesRelationList[i].IsReadOnly == true)) {
                        var levelCount = $scope.Inherritingtreelevels["multiselectdropdown_levels_" + $scope.atributesRelationList[i].ID].length
                        for (var j = 1; j <= levelCount; j++) {
                            var dropdown_text = "multiselectdropdown_text_" + $scope.atributesRelationList[i].ID + "_" + j;
                            if (j == 1) {
                                if ($scope.atributesRelationList[i].InheritFromParent) {
                                    if ($scope.Dropdown["OptionValues" + $scope.atributesRelationList[i].AttributeID + "_" + (j)].data.length > 0) {
                                        $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.treeSources["multiselectdropdown_" + $scope.atributesRelationList[i].AttributeID].Children, function (e) { return e.Caption == $scope.multiselecttreeTexts[dropdown_text] }))[0];
                                    }
                                }
                            }
                            else {
                                if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)] != null) {
                                    if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children != undefined) {
                                        if (j == levelCount) {
                                            var k = j;
                                            if ($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children.length > 0) {
                                                $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = [];
                                                for (var l = 0; l < $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children.length; l++) {
                                                    $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j].push(($.grep($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) {
                                                        return e.Caption.toString().trim() == $scope.multiselecttreeTexts[dropdown_text][l].toString().trim();
                                                    }))[0]);
                                                }
                                            }
                                        }
                                        else {
                                            $scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + j] = ($.grep($scope.fields["MultiSelectDropDown_" + $scope.atributesRelationList[i].AttributeID + '_' + (j - 1)].Children, function (e) { return e.Caption == $scope.multiselecttreeTexts[dropdown_text] }))[0];
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["MultiSelectTreedropdown_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 1) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue;
                    }
                    else {
                        $scope.fields["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["TextSingleLine_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 2) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].ParentValue;
                    }
                    else {
                        $scope.fields["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = $scope.atributesRelationList[i].DefaultValue;
                    }
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["TextMultiLine_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 5) {
                    if ($scope.atributesRelationList[i].InheritFromParent) {
                        $scope.fields["DatePart_" + $scope.atributesRelationList[i].AttributeID] = ConvertDateFromStringToString($scope.atributesRelationList[i].ParentValue.toString(), $scope.DefaultSettings.DateFormat);
                    }

                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["DateTime_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 8) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["TextMoney_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 9) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["CheckBoxSelection_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 10) {

                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Period_" + $scope.atributesRelationList[i].AttributeID] = true;
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 11) {
                    if ($scope.atributesRelationList[i].IsReadOnly) {
                        $scope.EnableDisableControlsHolder["Uploader_" + $scope.atributesRelationList[i].AttributeID] = false;
                    }
                }
            }
        }

        //Validation functionality--call this for validation
        function GetValidationList(EntitypeId) {
            SubentitycreationService.GetValidationDationByEntitytype(EntitypeId).then(function (GetValidationresult) {
                if (GetValidationresult.Response != null) {
                    $scope.listValidationResult = GetValidationresult.Response;
                    $scope.listofValidations = GetValidationresult.Response;
                    if ($scope.listAttriToAttriResult != null) {
                        for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                            var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                            if (attrRelIDs != undefined) {
                                for (var j = 0; j < attrRelIDs.length; j++) {
                                    if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString());
                                        });
                                    }
                                    else {
                                        $scope.listValidationResult = $.grep($scope.listValidationResult, function (e) {
                                            return parseInt(e[0].substring((e.toString().lastIndexOf("_") + 1))) != parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                        });
                                    }
                                }
                            }
                        }
                    }
                    $("#EntityMetadata").nod($scope.listValidationResult, {
                        'delay': 200,
                        'submitBtnSelector': '#btnTempSub',
                        'silentSubmit': 'true'
                    });
                }
            });
        }

        $scope.entityObjectiveList = {};
        $scope.entityAllObjectives = {};
        $scope.MandatoryEntityObjectives = {};
        $scope.MandatoryObjList = [];
        $scope.NonMandatoryObjList = [];
        $scope.IDList = [];
        $scope.nonMandatoryIDList = [];
        $scope.IsObjectiveFired = false;
        function EntityMetadata() {
            $scope.AttributeData = [];
            var entityMetadataObj = {};
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "") {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id != undefined) {
                                $scope.AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id,
                                    "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                });
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.Owner) {
                        if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                if (isNaN(parseFloat(($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10)) == false) {
                                    var value = parseInt(($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10);
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": value,
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0 ; k < multiselectiObject.length; k++) {
                                if (multiselectiObject[k] != undefined) {
                                    $scope.AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Caption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": parseInt(multiselectiObject[k], 10),
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                }
            }
            entityMetadataObj.AttributeData = $scope.AttributeData;
            entityMetadataObj.Periods = [];
            $scope.subentityperiods.splice(0, $scope.subentityperiods.length);
            for (var m = 0; m < $scope.items.length; m++) {
                if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                    if ($scope.items[m].startDate.length != 0 && $scope.items[m].endDate.length != 0) {
                        $scope.subentityperiods.push({ startDate: ConvertDateToString($scope.items[m].startDate), endDate: ConvertDateToString($scope.items[m].endDate), comment: '', sortorder: 0 });
                    }
                }
            }
            entityMetadataObj.Periods.push($scope.subentityperiods);
            entityMetadataObj.EntityTypeID = parseInt($scope.EntitytypeID, 10);
            SubentitycreationService.GettingPredefineObjectivesForEntityMetadata(entityMetadataObj).then(function (entityAttributeDataObjData) {
                $scope.IsObjectiveFired = true;
                if (entityAttributeDataObjData.Response != null && entityAttributeDataObjData.Response.length > 0) {
                    $scope.entityAllObjectives = entityAttributeDataObjData.Response;
                    $scope.MandatoryObjList = [];
                    $scope.NonMandatoryObjList = [];
                    $scope.IDList = [];
                    for (var i = 0 ; i < $scope.entityAllObjectives.length ; i++) {
                        if ($scope.entityAllObjectives[i].ObjectiveMandatoryStatus == true) {
                            $scope.MandatoryObjList.push($scope.entityAllObjectives[i]);
                            $scope.IDList.push(parseInt($scope.entityAllObjectives[i].ObjectiveId, 10));
                        }
                        else {
                            $scope.NonMandatoryObjList.push($scope.entityAllObjectives[i]);
                        }
                    }
                    $scope.entityObjectiveList = $scope.NonMandatoryObjList;
                    $scope.MandatoryEntityObjectives = $scope.MandatoryObjList;
                    $scope.ngNoObjectivediv = false;
                    $scope.ngObjectiveDivHolder = true;
                }
                else {
                    $scope.ngObjectiveDivHolder = false;
                    $scope.ngNoObjectivediv = true;
                }
            });
        }


        function FormObjectiveMetadata() {

            var AttributeData = [];


            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeID == 6) {
                    for (var j = 0; j < $scope.atributesRelationList[i].Levels.length; j++) {
                        if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != undefined && $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)] != "") {
                            if ($scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id != undefined) {
                                AttributeData.push({
                                    "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                    "AttributeCaption": $scope.atributesRelationList[i].Levels[j].LevelName,
                                    "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                    "NodeID": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].id,
                                    "Level": $scope.fields['DropDown_' + $scope.atributesRelationList[i].AttributeID + '_' + (j + 1)].Level
                                });
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 3) {
                    if ($scope.atributesRelationList[i].AttributeID != SystemDefiendAttributes.Owner) {
                        if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != "" && $scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                            if ($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined) {
                                if (isNaN(parseFloat(($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10)) == false) {
                                    var value = parseInt(($scope.fields['ListSingleSelection_' + $scope.atributesRelationList[i].AttributeID]), 10);
                                    AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].AttributeCaption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": value,
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                }
                else if ($scope.atributesRelationList[i].AttributeTypeID == 4) {
                    if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != undefined && $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID] != "") {
                        if ($scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID].length > 0) {
                            var multiselectiObject = $scope.fields['ListMultiSelection_' + $scope.atributesRelationList[i].AttributeID];
                            for (var k = 0 ; k < multiselectiObject.length; k++) {
                                if (multiselectiObject[k] != undefined) {
                                    AttributeData.push({
                                        "AttributeID": $scope.atributesRelationList[i].AttributeID,
                                        "AttributeCaption": $scope.atributesRelationList[i].Caption,
                                        "AttributeTypeID": $scope.atributesRelationList[i].AttributeTypeID,
                                        "NodeID": parseInt(multiselectiObject[k], 10),
                                        "Level": 0
                                    });
                                }
                            }
                        }
                    }
                }
            }
            return AttributeData;
        }


        function GetObjectivePeriods() {

            $scope.subentityperiods.splice(0, $scope.subentityperiods.length);
            for (var m = 0; m < $scope.items.length; m++) {
                if ($scope.items[m].startDate != null && $scope.items[m].endDate != null) {
                    if ($scope.items[m].startDate.length != 0 && $scope.items[m].endDate.length != 0) {
                        $scope.subentityperiods.push({ startDate: ConvertDateToString($scope.items[m].startDate), endDate: ConvertDateToString($scope.items[m].endDate), comment: '', sortorder: 0 });
                    }
                }
            }
        }

        function UpdateObjectiveScope(objResponse) {

            if (objResponse != null) {


                if (objResponse != null && objResponse.length > 0) {
                    $scope.entityAllObjectives = objResponse;
                    $scope.MandatoryObjList = [];
                    $scope.NonMandatoryObjList = [];
                    $scope.IDList = [];
                    for (var i = 0 ; i < $scope.entityAllObjectives.length ; i++) {
                        if ($scope.entityAllObjectives[i].ObjectiveMandatoryStatus == true) {
                            $scope.MandatoryObjList.push($scope.entityAllObjectives[i]);
                            $scope.IDList.push(parseInt($scope.entityAllObjectives[i].ObjectiveId, 10));
                        }
                        else {
                            $scope.NonMandatoryObjList.push($scope.entityAllObjectives[i]);
                        }
                    }
                    $scope.entityObjectiveList = $scope.NonMandatoryObjList;
                    $scope.MandatoryEntityObjectives = $scope.MandatoryObjList;
                    $scope.ngNoObjectivediv = false;
                    $scope.ngObjectiveDivHolder = true;
                }
                else {
                    $scope.ngObjectiveDivHolder = false;
                    $scope.ngNoObjectivediv = true;
                }
            }
            else {
                $scope.IDList = [];
            }

        }
        $scope.UploadImagefile = function (attributeId) {
            $scope.UploadAttributeId = attributeId;
            $("#pickfilesUploaderAttrSub").click();
        }

        function StrartUpload_UploaderAttrSubEntity() {
            $('.moxie-shim').remove();
            var uploader_AttrSub = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                browse_button: 'pickfilesUploaderAttrSub',
                container: 'filescontainerooSub',
                max_file_size: '10000mb',
                flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                url: 'Handlers/UploadHandler.ashx?Type=Attachment',
                chunk_size: '64Kb',
                multi_selection: false,
                multipart_params: {}

            });
            uploader_AttrSub.bind('Init', function (up, params) {
            });

            uploader_AttrSub.init();
            uploader_AttrSub.bind('FilesAdded', function (up, files) {
                up.refresh();
                uploader_AttrSub.start();
            });
            uploader_AttrSub.bind('UploadProgress', function (up, file) {

            });
            uploader_AttrSub.bind('Error', function (up, err) {
                bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                up.refresh(); // Reposition Flash/Silverlight
            });
            uploader_AttrSub.bind('FileUploaded', function (up, file, response) {
                UpdateFileDetails(file, response.response);
            });
            uploader_AttrSub.bind('BeforeUpload', function (up, file) {
                $.extend(up.settings.multipart_params, { id: file.id, size: file.size });
            });
        }
        function UpdateFileDetails(file, response) {
            var extension = file.name.substring(file.name.lastIndexOf("."))
            var resultArr = response.split(",");
            $scope.ImageFileName = resultArr[0] + extension;
            var PreviewID = "UploaderPreview_" + $scope.UploadAttributeId;
            $('#' + PreviewID).attr('src', 'UploadedImages/' + $scope.ImageFileName);
        }
        $scope.Clear = function () {
            $modalInstance.dismiss('cancel');
        }
        $scope.UploadAttributeData = [];
        $scope.EntityObjSelect = function (objectiveId) {
            var IsExist = $.grep($scope.nonMandatoryIDList, function (e) {
                if (e == objectiveId) {
                    return e;
                }
            })
            if (IsExist.length == 0) {
                $scope.nonMandatoryIDList.push(objectiveId);
            }
        }

        $(document).on('click', '.checkbox-custom > input[id=SelectAllObjectives]', function (e) {
            var status = this.checked;
            $('#nonMandatoryObjectiveBody input:checkbox').each(function () {
                this.checked = status;
                if (status) {
                    $(this).next('i').addClass('checked');
                } else {
                    $(this).next('i').removeClass('checked');
                }
            });
            for (var i = 0 ; i < $scope.entityObjectiveList.length; i++) {
                $scope.nonMandatoryIDList.push($scope.entityObjectiveList[i].ObjectiveId);
            }
            for (var i = 0 ; i < $scope.MandatoryEntityObjectives.length; i++) {
                $scope.IDList.push($scope.MandatoryEntityObjectives[i].ObjectiveId);
            }
        });

        //------------> RECURSIVE FUNCTION TO HIDE ALL THE ATTRIBUTE RELATIONS FOR THE SELECTED ATTRIBUTE <--------------
        function RecursiveAttributeToAttributeHide(res, attrID, attrLevel, attrType) {
            var recursiveAttrID = '';
            var attributesToShow = [];

            //----------> CHECK THE ATTRIBUTE ON ATTRIBUTE ID AND LEVEL AND GET THE ATTRIBUTE INFO TO HIDE <--------------
            if (attrLevel > 0) {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID && e.AttributeLevel >= attrLevel);
                }));
            }
            else {
                attributesToShow.push($.grep(res, function (e) {
                    return (e.AttributeID == attrID);
                }));
            }

            if (attributesToShow[0] != undefined) {
                for (var i = 0; i < attributesToShow[0].length; i++) {
                    var attrRelIDs = attributesToShow[0][i].AttributeRelationID.toString().split(',');
                    if (attrRelIDs != undefined) {
                        for (var j = 0; j < attrRelIDs.length; j++) {
                            if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                //----------> HIDE THE ATTRIBUTE AND CLEAR THE SCOPE <----------
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                //----------> FIND FOR NEXT ATTRIBUTE IF THE ATTRIBUTE RELATIONS EXISTS <-------------
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return (e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))) && e.AttributeLevel == attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].length));
                                }));
                                //---------> IF THE ATTRIBUTE HAS RELATION WITH OTHER ATTRIBUTE START RECURSIVE AGAIN
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType);
                                    }
                                }
                            }
                            else {
                                //----------> HIDE THE ATTRIBUTE AND CLEAR THE SCOPE <----------
                                $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                $scope.fields['ListSingleSelection_' + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = "";

                                //----------> FIND FOR NEXT ATTRIBUTE IF THE ATTRIBUTE RELATIONS EXISTS <-------------
                                recursiveAttrID = ($.grep($scope.listAttriToAttriResult, function (e) {
                                    return e.AttributeID == parseInt(attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_")));
                                }));

                                //---------> IF THE ATTRIBUTE HAS RELATION WITH OTHER ATTRIBUTE START RECURSIVE AGAIN
                                if (recursiveAttrID != undefined) {
                                    for (var m = 0; m < recursiveAttrID.length; m++) {
                                        RecursiveAttributeToAttributeHide(res, recursiveAttrID[m].AttributeID, recursiveAttrID[m].AttributeLevel, attrType)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $scope.LoadSubLevels = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                //---------> 
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    //-----------> CLEAR THE SUB LEVEL ON SELECTING THE PARENT LEVEL
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        }
                        else if (attrType == 12) {
                            if (j == levelcnt)
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }

                    //-----------------> LOAD THE SUB LEVELS ON SELECTING PARENT LEVEL <----------------
                    if (attrType == 6) {
                        if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                    else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
                //----------------------------
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    }
                    catch (e) { }
                }

            }
            catch (e) { }
        }

        //--------------------> SHOW OR HIDE ATTRIBUTES FOR DROPDOWN ON SINGLE SELECTION <-----------------------
        $scope.ShowHideAttributeToAttributeRelations = function (attrID, attributeLevel, levelcnt, attrType) {
            try {
                var recursiveAttrID = '';
                var optionValue = '';
                var attributesToShow = [];
                var hideAttributeOtherThanSelected = [];
                //---------> 
                if (levelcnt > 0) {
                    var currntlevel = attributeLevel + 1;
                    //-----------> CLEAR THE SUB LEVEL ON SELECTING THE PARENT LEVEL
                    for (var j = currntlevel; j <= levelcnt; j++) {
                        $scope.Dropdown["OptionValues" + attrID + "_" + j].data.splice(0, $scope.Dropdown["OptionValues" + attrID + "_" + j].data.length);
                        if (attrType == 6) {
                            $scope.fields["DropDown_" + attrID + "_" + j] = "";
                        }
                        else if (attrType == 12) {
                            if (j == levelcnt)
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = [];
                            else
                                $scope.fields["MultiSelectDropDown_" + attrID + "_" + j] = "";
                        }
                    }

                    //-----------------> LOAD THE SUB LEVELS ON SELECTING PARENT LEVEL <----------------
                    if (attrType == 6) {
                        if ($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["DropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                    else if (attrType == 12) {
                        if ($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'] != undefined) {
                            $.each($scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel]['Children'], function (i, el) {
                                $scope.Dropdown["OptionValues" + attrID + "_" + (attributeLevel + 1)].data.push(el);
                            });
                        }
                    }
                }
                //----------------------------
                if (attrID == SystemDefiendAttributes.FiscalYear) {
                    try {
                        $scope.changeCostCenterSource();
                    }
                    catch (e) { }
                }
                //--------------> IF THERE IS NO ATTRIBUTE TO ATTRIBUTE RELATATIONS THEN RETURN BACK <--------------------------
                if ($scope.listAttriToAttriResult == null && $scope.listAttriToAttriResult == undefined) {
                    return false;
                }

                //------------> RECURSIVE FUNCTION TO HIDE ATTRIBUTE ON RELATIONS
                RecursiveAttributeToAttributeHide($scope.listAttriToAttriResult, attrID, attributeLevel, attrType);

                //-----> IF SINGLE SELECTION DROPDOWN SELECTED
                if (attrType == 3) {
                    if ($scope.fields['ListSingleSelection_' + attrID] != null) {
                        optionValue = parseInt(($scope.fields['ListSingleSelection_' + attrID]), 10);
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && e.AttributeOptionID == optionValue;
                    })[0]);
                }
                    //-----> IF MULTI SELECTION DROPDOWN SELECTED
                else if (attrType == 4) {
                    if ($scope.fields['ListMultiSelection_' + attrID] != null) {
                        optionValue = $scope.fields['ListMultiSelection_' + attrID];
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                    //-----> IF DROPDOWN TREE SELECTED
                else if (attrType == 6) {
                    var attrval = $scope.fields["DropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }
                    //-----> IF TREE SELECTED
                else if (attrType == 7) {
                    if ($scope.fields['Tree_' + attrID] != null) {
                        optionValue = $scope.fields['Tree_' + attrID];
                    }
                    else {
                        optionValue = 0;
                    }
                    attributesToShow = ($.grep($scope.listAttriToAttriResult, function (e) {
                        return e.AttributeID == attrID && (optionValue.indexOf(e.AttributeOptionID) != -1);
                    }));
                }
                else if (attrType == 12) {
                    var attrval = $scope.fields["MultiSelectDropDown_" + attrID + "_" + attributeLevel];
                    if (attrval != null) {
                        attributesToShow = [];
                        attributesToShow.push($.grep($scope.listAttriToAttriResult, function (e) {
                            return ((e.AttributeID == attrID) && e.AttributeOptionID == ((attrval != null) ? parseInt(attrval.id, 10) : 0) && e.AttributeLevel == ((attrval != null) ? parseInt(attrval.Level, 10) : 0));
                        })[0]);
                    }
                }

                //----------> IF OPTION AVAILABLE FOR THE ATTRIBUTE SHOW THE ATTRIBUTE
                if (attributesToShow[0] != undefined) {
                    for (var i = 0; i < attributesToShow.length; i++) {
                        var attrRelIDs = attributesToShow[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                //----------------> CHECK IF THE SELECTED ATTRIBUTE IS SINGLE SELECTION DROPDOWN OR DROPDOWN TREE <--------------------
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = true;
                                }
                                else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (e) { }
        }

        //--------> HIDE ALL THE ATTRIBUTE RELAIONS AFTER LOADING <--------------------
        function HideAttributeToAttributeRelationsOnPageLoad() {
            try {
                if ($scope.listAttriToAttriResult != undefined) {
                    for (var i = 0; i < $scope.listAttriToAttriResult.length; i++) {
                        var attrRelIDs = $scope.listAttriToAttriResult[i].AttributeRelationID.toString().split(',');
                        if (attrRelIDs != undefined) {
                            for (var j = 0; j < attrRelIDs.length; j++) {
                                if (parseInt(attrRelIDs[j].toString().substring((attrRelIDs[j].toString().lastIndexOf("_") + 1), attrRelIDs[j].toString().length)) > 0) {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString()] = false;
                                }
                                else {
                                    $scope.ShowHideAttributeOnRelation["Attribute_" + attrRelIDs[j].toString().substring(0, attrRelIDs[j].toString().lastIndexOf("_"))] = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (e) { }
        }

        $scope.AddDefaultEndDate = function (objdateval) {
            $("#EntityMetadata").addClass('notvalidate');
            if (objdateval.startDate == null) {
                objdateval.endDate = null
            }
            else {
                objdateval.endDate = new Date.create(objdateval.startDate);
                objdateval.endDate = objdateval.endDate.addDays(7);
            }
        };

        $scope.CheckPreviousEndDate = function (enddate, startdate, currentindex) {
            var enddate1 = null;
            if (currentindex != 0) {
                enddate1 = $scope.items[currentindex - 1].endDate;
            }
            if (enddate1 != null && enddate1 >= startdate) {
                bootbox.alert($translate.instant('LanguageContents.Res_1987.Caption'));
                $scope.items[currentindex].startDate = null;
                $scope.items[currentindex].endDate = null;
            }
            else {
                $("#EntityMetadata").addClass('notvalidate');
                if (startdate == null) {
                    $scope.items[currentindex].endDate = null
                }
                else {
                    $scope.items[currentindex].endDate = (new Date.create(startdate)).addDays(7);
                }
            }
        };

        $scope.CheckPreviousStartDate = function (enddate, startdate, currentindex) {
            var enddate1 = null;

            if (currentindex != 0 || currentindex == 0) {
                enddate1 = $scope.items[currentindex].endDate;
            }
            var edate = ConvertDateToString(enddate1);

            var sdate = ConvertDateToString(startdate);
            if (enddate1 != null) {
                if (edate < sdate) {
                    bootbox.alert($translate.instant('LanguageContents.Res_4240.Caption'));
                    $scope.items[currentindex].endDate = null;
                }
            }
        };


        //dam file selection part
        function refreshAssetobjects() {
            $scope.PageNoobj = { pageno: 1 };
            $scope.TaskStatusObj = [{ "Name": "Thumbnail", ID: 1 }, { "Name": "Summary", ID: 2 }, { "Name": "List", ID: 3 }];
            $scope.SettingsDamAttributes = {};
            $scope.OrderbyObj = [{ "Name": "Name (Ascending)", ID: 1 }, { "Name": "Name (Descending)", ID: 2 }, { "Name": "Creation date (Ascending)", ID: 3 }, { "Name": "Creation date (Descending)", ID: 4 }];
            $scope.FilterStatus = { filterstatus: 1 };
            $scope.DamViewName = { viewName: "Thumbnail" };
            $scope.OrderbyName = { orderbyname: "Creation date (Descending)" };
            $scope.OrderBy = { order: 4 };
            $scope.AssetSelectionFiles = [];
            $scope.SelectedAssetIdFolderID.SelectedAssetIDs = [];
        }
        refreshAssetobjects();

        $scope.costcentreid = [];

        $scope.OnCostCentreTreesSelection = function (branch, parentArr) {
            if (branch.ischecked == true) {
                $scope.costcentreid.push(branch.id);
            }
            else {
                $scope.costcentreid.splice($scope.costcentreid.indexOf(branch.id), 1);
            }
        };
        $scope.origninalamount = 0;
        $scope.Getamountentered = function (atrid) {

            if (1 == $scope.fields["ListSingleSelection_" + atrid])
                $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '');
            else
                $scope['origninalamountvalue_' + atrid] = $scope.fields["dTextSingleLine_" + atrid].replace(/ /g, '') / 1 / $scope['currRate_' + atrid];
        }
        $scope.GetCostCentreCurrencyRateById = function (atrid) {
            SubentitycreationService.GetCostCentreCurrencyRateById(0, $scope.fields["ListSingleSelection_" + atrid], true).then(function (resCurrencyRate) {
                if (resCurrencyRate.Response != null) {
                    $scope['currRate_' + atrid] = parseFloat(resCurrencyRate.Response[1]);
                    if ($scope['origninalamountvalue_' + atrid] != 0) {

                        //$scope.fields["dTextSingleLine_" + atrid] = parseInt((parseFloat($scope['origninalamountvalue_' + atrid]) * $scope['currRate_' + atrid])).formatMoney(0, ' ', ' ');
                        $scope.fields["dTextSingleLine_" + atrid] = (parseFloat($scope['origninalamountvalue_' + atrid]) * $scope['currRate_' + atrid]).formatMoney($scope.DecimalSettings['FormatMoney'].Financial_FormatMoney, '.', ' ');
                    }
                    //$scope.AvailableAssignAmount = $scope.AvailableAssignAmount * $scope.SelectedCostCentreCurrency.Rate;
                }
            });
        }



        SubentitycreationService.GetCurrencyListFFsettings().then(function (CurrencyListResult) {
            if (CurrencyListResult.Response != null)
                $scope.CurrencyFormatsList = CurrencyListResult.Response;
        });

        $scope.OnPageClose = function () {
            $scope.listAttriToAttriResult = [];
            $("#EntityMetadata").html('');
            $modalInstance.dismiss('cancel');
        }



        $scope.$on('callbackstoptimer', function (event) {
            $scope.$broadcast('callbackaftercreation');
        });

        $scope.$on('callbackacivatetimer', function (event) {
            $scope.$broadcast('callbackacivatetimercall');
        });

        $('#LightBoxPreview').on('hidden.bs.modal', function (event) {
            $scope.$broadcast('callbackaftercreation');
        });

        //change event for duedate
        $scope.changeduedate_changed = function (duedate, ID) {
            if (duedate != null) {
                var test = isValidDate(duedate.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(duedate, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert("Selected date should not same as non businessday");
                            $scope.fields["DatePart_" + ID] = "";
                        }
                    }
                }
                else {
                    $scope.fields["DatePart_" + ID] = "";
                    bootbox.alert("Select valid duedate");
                }
            }
        }
        //change event for period date
        $scope.changeperioddate_changed = function (date, datetype) {
            if (date != null) {
                var test = isValidDate(date.toString(), $scope.format.toString());
                if (test) {
                    var a = $.grep($scope.tempholidays, function (e) {
                        return e == dateFormat(date, $scope.format);
                    });
                    if (a != null) {
                        if (a.length > 0) {
                            bootbox.alert("Selected date should not same as non businessday");
                            if (datetype == "StartDate")
                                $scope.items[0].startDate = "";
                            else
                                $scope.item[0].endDate = "";
                        }
                    }
                }
                else {
                    if (datetype == "StartDate")
                        $scope.items[0].startDate = "";
                    else
                        $scope.item[0].endDate = "";
                    bootbox.alert("Select valid duedate");
                }
            }
        }

        function isValidDate(dateval, dateformat) {
            var formatlen;
            var defaultdateVal = [];
            defaultdateVal = dateval.length;
            formatlen = dateformat.length;
            if (formatlen == defaultdateVal || defaultdateVal > formatlen)
                return true;
            else
                return false;
        };
        SubentityTypeCreation(params.ID, params.Caption, params.CurrentEntityId);

        $scope.$on("$destroy", function () {
            setTimeout.cancel(SubentityTypeCreationTimeout);
            setTimeout.cancel(remValtimer);
            model, tree, ownername, ownerid, treeTextVisbileflag, treeformflag = null;
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.SubEntityCreationCtrl']"));
            //var model, remValtimer, IsValExist, getVal, Attributetypename, relationobj, ID, i, j, o, x, k, m, t, tabHtml, currentWizardStep, totalWizardSteps, keys, keys1, keys2, apple_selected, tree, treedata_avm, treedata_geography, _ref, remainRecord, result, costCentrevalues, memberObj, memberToRempve, ownername, ownerid, membervalues, ItemCnt, percentageflag, entityMetadataObj, metadata, SaveEntity, attributeLevelOptions, levelCount, value, MyDate, MyDateString, treenodes, multiselectiObject, res, totLevelCnt, CaptionObj, LabelObject, totLevelCnt1, temp, param1, param2, datStartUTCval, datstartval, datEndUTCval, datendval, mar, treeTextVisbileflag, treeformflag, remainRecord, attrInheritval, defaultmultiselectvalue, attrRelIDs, multiselectiObject, AttributeData, extension, resultArr, PreviewID, IsExist, status, recursiveAttrID, attributesToShow, currntlevel, optionValue, hideAttributeOtherThanSelected, attrval, enddate1, edate, sdate, test, a, formatlen, defaultdateVal = null;
            $scope.$broadcast('callbackaftercreation');
        });
        // --- Initialize. ---------------------------------- //

    }
    //);
    app.controller("mui.planningtool.SubEntityCreationCtrl", ['$scope', '$location', '$resource', '$http', '$cookies', '$compile', '$window', '$translate', 'SubentitycreationService', '$modalInstance', 'params', muiplanningtoolSubEntityCreationCtrl]);
})(angular, app);
///#source 1 1 /app/services/objectivedetail-service.js
(function(ng,app){function ObjectivedetailService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({ObjectiveDetail:ObjectiveDetail,GetEntityTypefromDB:GetEntityTypefromDB,ObjectiveDetailReport:ObjectiveDetailReport,GetAdminSettingselemntnode:GetAdminSettingselemntnode,ShowReports:ShowReports,ObjectiveDetailReportSystem:ObjectiveDetailReportSystem,GetFilterSettingsForDetail:GetFilterSettingsForDetail});function ObjectiveDetail(formobj){var request=$http({method:"post",url:"api/Metadata/ObjectiveDetail/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetEntityTypefromDB(){var request=$http({method:"get",url:"api/Metadata/GetEntityTypefromDB/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function ObjectiveDetailReport(formobj){var request=$http({method:"post",url:"api/Metadata/ObjectiveDetailReport/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetAdminSettingselemntnode(LogoSettings,elemntnode,typeid){var request=$http({method:"get",url:"api/common/GetAdminSettingselemntnode/"+LogoSettings+"/"+elemntnode+"/"+typeid,params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function ShowReports(OID,Show){var request=$http({method:"get",url:"api/Report/ShowReports/"+OID+"/"+Show,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function ObjectiveDetailReportSystem(formobj){var request=$http({method:"post",url:"api/Report/ObjectiveDetailReportSystem/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetFilterSettingsForDetail(TypeID){var request=$http({method:"get",url:"api/Planning/GetFilterSettingsForDetail/"+TypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("ObjectivedetailService",['$http','$q',ObjectivedetailService]);
})(angular, app);
///#source 1 1 /app/services/objectivedetailfilter-service.js
(function (ng, app) {
    function ObjectivedetailfilterService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetFilterSettingsForDetail: GetFilterSettingsForDetail,
            InsertFilterSettings: InsertFilterSettings,
            GetFilterSettingValuesByFilertId: GetFilterSettingValuesByFilertId,
            DeleteFilterSettings: DeleteFilterSettings,
            GetOptionsFromXML: GetOptionsFromXML,
            GettingEntityTypeHierarchyForAdminTree: GettingEntityTypeHierarchyForAdminTree,
            GettingFilterEntityMember: GettingFilterEntityMember,
            GettingFilterAttribute: GettingFilterAttribute
        });
        function GetFilterSettingsForDetail(TypeID) { var request = $http({ method: "get", url: "api/Planning/GetFilterSettingsForDetail/" + TypeID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function InsertFilterSettings(formobj) { var request = $http({ method: "post", url: "api/Planning/InsertFilterSettings/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GetFilterSettingValuesByFilertId(FilterID) { var request = $http({ method: "get", url: "api/Planning/GetFilterSettingValuesByFilertId/" + FilterID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function DeleteFilterSettings(FilterId) { var request = $http({ method: "delete", url: "api/Planning/DeleteFilterSettings/" + FilterId, params: { action: "delete" } }); return (request.then(handleSuccess, handleError)); }
        function GetOptionsFromXML(elementNode, typeid) { var request = $http({ method: "get", url: "api/Metadata/GetOptionsFromXML/" + elementNode + "/" + typeid, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GettingEntityTypeHierarchyForAdminTree(EntityTypeID, ModuleID) { var request = $http({ method: "get", url: "api/Metadata/GettingEntityTypeHierarchyForAdminTree/" + EntityTypeID + "/" + ModuleID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function GettingFilterEntityMember(formobj) { var request = $http({ method: "post", url: "api/Metadata/GettingFilterEntityMember/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function GettingFilterAttribute(formobj) { var request = $http({ method: "post", url: "api/Metadata/GettingFilterAttribute/", params: { action: "add" }, data: formobj }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("ObjectivedetailfilterService", ['$http', '$q', ObjectivedetailfilterService]);
})(angular, app);
///#source 1 1 /app/services/SubEntityCreation-service.js
(function(ng,app){function SubentitycreationService($http,$q){$http.defaults.headers.common.sessioncookie=$.cookie('Session');return({GetCostcentreforEntityCreation:GetCostcentreforEntityCreation,GetCostcentreTreeforPlanCreation:GetCostcentreTreeforPlanCreation,GetGlobalMembers:GetGlobalMembers,GettingPredefineObjectivesForEntityMetadata:GettingPredefineObjectivesForEntityMetadata,CreateEntity:CreateEntity,GetCostCentreCurrencyRateById:GetCostCentreCurrencyRateById,GetCurrencyListFFsettings:GetCurrencyListFFsettings,GetEntityTypeRoleAccess:GetEntityTypeRoleAccess,GetUserById:GetUserById,GetAttributeToAttributeRelationsByIDForEntity:GetAttributeToAttributeRelationsByIDForEntity,GetEntityTypeAttributeRelationWithLevelsByID:GetEntityTypeAttributeRelationWithLevelsByID,GetValidationDationByEntitytype:GetValidationDationByEntitytype,GetPlantabsettings:GetPlantabsettings});function GetCostcentreforEntityCreation(EntityTypeID,FiscalYear,EntityID){var request=$http({method:"get",url:"api/Planning/GetCostcentreforEntityCreation/"+EntityTypeID+"/"+FiscalYear+"/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCostcentreTreeforPlanCreation(EntityTypeID,FiscalYear,EntityID){var request=$http({method:"get",url:"api/Planning/GetCostcentreTreeforPlanCreation/"+EntityTypeID+"/"+FiscalYear+"/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetGlobalMembers(EntityID){var request=$http({method:"get",url:"api/Planning/GetGlobalMembers/"+EntityID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GettingPredefineObjectivesForEntityMetadata(formobj){var request=$http({method:"post",url:"api/Planning/GettingPredefineObjectivesForEntityMetadata/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function CreateEntity(formobj){var request=$http({method:"post",url:"api/Planning/CreateEntity/",params:{action:"add"},data:formobj});return(request.then(handleSuccess,handleError));}
function GetCostCentreCurrencyRateById(EntityId,CurrencyId,IsCostCentreCreation){var request=$http({method:"get",url:"api/Planning/GetCostCentreCurrencyRateById/"+EntityId+"/"+CurrencyId+"/"+IsCostCentreCreation,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetCurrencyListFFsettings(){var request=$http({method:"get",url:"api/Planning/GetCurrencyListFFsettings/",params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeRoleAccess(EntityTypeID){var request=$http({method:"get",url:"api/access/GetEntityTypeRoleAccess/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetUserById(ID){var request=$http({method:"get",url:"api/user/GetUserById/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetAttributeToAttributeRelationsByIDForEntity(ID){var request=$http({method:"get",url:"api/Metadata/GetAttributeToAttributeRelationsByIDForEntity/"+ID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetEntityTypeAttributeRelationWithLevelsByID(ID,ParentID){var request=$http({method:"get",url:"api/Metadata/GetEntityTypeAttributeRelationWithLevelsByID/"+ID+"/"+ParentID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetValidationDationByEntitytype(EntityTypeID){var request=$http({method:"get",url:"api/Metadata/GetValidationDationByEntitytype/"+EntityTypeID,params:{action:"get"}});return(request.then(handleSuccess,handleError));}
function GetPlantabsettings(){var request=$http({method:"get",url:"api/common/GetPlantabsettings/",params:{action:"get",}});return(request.then(handleSuccess,handleError));}
function handleError(response){if(!angular.isObject(response.data)||!response.data.message){return($q.reject("An unknown error occurred."));}
return($q.reject(response.data.message));}
function handleSuccess(response){return(response.data);}}
app.service("SubentitycreationService",['$http','$q',SubentitycreationService]);})(angular,app);
