﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// ICostCenterApprovedBudget interface for table 'PM_CostCenterApprovedBudget'.
    /// </summary>
    public interface ICostCenterApprovedBudget
    {
        #region Public Properties

        int ID
        {
            get;
            set;

        }

        int CostCentID
        {
            get;
            set;

        }

        int UserID
        {
            get;
            set;

        }


        DateTime ApproveTime
        {
            get;
            set;

        }



        #endregion
    }
}
