﻿(function (ng, app) {
    "use strict";

    function muiadminfinancialmetadatactrl($scope, $location, $resource, $timeout, $cookies, $translate, AdminService) {
        $scope.wizard = {
            attributeType: 0,
            AttributeId: 0,
            Description: '',
            Caption: '',
            newOption: '',
            EditOption: '',
            OptionID: 0,
            TreeLevelNumberByNode: [],
            TreeLevels: [],
            IsDisableAttributeType: false
        };
        $scope.FinAttrType = $scope.financialAttrType;
        AdminService.GetAttributetype().then(function (getattributeTypes) {
            $scope.listdata = getattributeTypes.Response;
            $scope.attributeTypes = $scope.listdata;
        });
        GetFinancialAttribute();

        function GetFinancialAttribute() {
            AdminService.GetFinancialAttribute().then(function (getattributes) {
                $scope.Finattributes = getattributes.Response;
                $scope.FinType = $scope.FinAttrType;
                if ($scope.FinType == 1) {
                    $scope.tooltipColumnHeading = 'Tooltip on Committed Column';
                } else if ($scope.FinType == 2) {
                    $scope.tooltipColumnHeading = 'Tooltip on Spent Column';
                }
            });
        }
        $scope.OnAttributeSelect = function () {
            $scope.optionsLists = [];
            if ($scope.wizard.attributeType == "3" || $scope.wizard.attributeType == "4") {
                $scope.EnableOptionUpdate = false;
                $scope.EnableOptionAdd = true;
                $scope.AttrType = "Option";
            } else {
                $scope.AttrType = "";
            }
        }
        $scope.addAttribute = function () {
            $("#AddFinancialMetadata").modal('show');
            $scope.showadd = true;
            $scope.showupdate = false;
            $scope.wizard.IsDisableAttributeType = false;
            $scope.wizard.Description = "";
            $scope.wizard.Caption = "";
            $scope.EnableAdd = true;
            $scope.wizard.AttributeId = 0;
            $scope.wizard.attributeType = "0";
            $scope.optionsLists = [];
            $scope.EnableUpdate = false;
            $scope.AttrType = "";
            $scope.EnableOptionUpdate = false;
            $scope.EnableOptionAdd = false;
            $timeout(function () {
                $('#AttributeCaption').focus();
            }, 1000);
        }
        $scope.count = 1;
        $scope.addOptions = function () {
            if ($scope.wizard.newOption == '') {
                bootbox.alert($translate.instant('LanguageContents.Res_1809.Caption'));
                return true;
            }
            if ($scope.wizard.OptionID != 0) {
                ($.grep($scope.optionsLists, function (e) {
                    return e.ID == $scope.wizard.AttributeId;
                }))[0].Caption = $scope.wizard.newOption;
            } else {
                $scope.optionsLists.push({
                    "PId": $scope.count,
                    "ID": 0,
                    "Caption": $scope.wizard.newOption,
                    "SortOrder": ($scope.optionsLists.length + 1)
                });
            }
            $scope.wizard.newOption = '';
            $scope.count = $scope.count + 1;
        };
        $scope.EditOptions = function () {
            if ($scope.wizard.OptionID != 0) {
                var UpdateOption = $.grep($scope.optionsLists, function (e) {
                    return e.PId == $scope.wizard.PrimaryID;
                });
                UpdateOption[0].Caption = $scope.wizard.newOption;
            } else if ($scope.wizard.OptionID == 0) {
                var UpdateOption = $.grep($scope.optionsLists, function (e) {
                    return e.PId == $scope.wizard.PrimaryID;
                });
                UpdateOption[0].Caption = $scope.wizard.newOption;
            }
            $scope.wizard.EditOption = '';
            $scope.wizard.newOption = '';
            $scope.wizard.OptionID = 0;
            $scope.EnableOptionUpdate = false;
            $scope.EnableOptionAdd = true;
        };
        $scope.GetOptionbyID = function GetOptionbyID(row) {
            $scope.wizard.PrimaryID = row.PId;
            $scope.wizard.OptionID = row.ID;
            $scope.wizard.EditOption = row.Caption;
            $scope.wizard.newOption = row.Caption;
            $scope.EnableOptionUpdate = true;
            $scope.EnableOptionAdd = false;
        };
        $scope.deleteOptions = function deleteOptions(row) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2050.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        try {
                            if (row.ID != 0) {
                                var ID = row.ID;
                                AdminService.DeleteFinancialOption(ID).then(function (deleteAttributeById) {
                                    if (deleteAttributeById.StatusCode == 405) {
                                        NotifyError($translate.instant('LanguageContents.Res_1128.Caption'));
                                    } else {
                                        var index = $scope.optionsLists.indexOf(row)
                                        $scope.optionsLists.splice(index, 1);
                                        NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                                        $scope.wizard.newOption = '';
                                        $scope.EnableOptionUpdate = false;
                                        $scope.EnableOptionAdd = true;
                                    }
                                });
                            } else {
                                var index = $scope.optionsLists.indexOf(row)
                                $scope.optionsLists.splice(index, 1);
                                NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                                $scope.wizard.newOption = '';
                                $scope.EnableOptionUpdate = false;
                                $scope.EnableOptionAdd = true;
                            }
                        } catch (e) {
                            NotifyError($translate.instant('LanguageContents.Res_4524.Caption'));
                        }
                    }, 100);
                }
            });
        };
        $scope.deleteAttribute = function deleteAttribute() {
            if ($(".findchkbox >td .checked").length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4944.Caption'));
                $('#FinMetadataDelete').next('i').removeClass('checked');
                return true;
            }
            bootbox.confirm($translate.instant('LanguageContents.Res_2050.Caption'), function (result) {
                if (result) {
                    var IDList = new Array();
                    $(".findchkbox td:first-child .checked").each(function () {
                        if ($(this).parents('td').attr('data-id') != undefined) IDList.push($(this).parents('td').attr('data-id'));
                    });
                    if (IDList.length != 0) {
                        var object = {};
                        object.ID = IDList;
                        AdminService.DeleteFinancialAttribute(object).then(function (result) {
                            if (result.Response == 1) {
                                NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                                $(".findchkbox td:first-child .checked").each(function () {
                                    var ID = parseInt($(this).parents('td').attr('data-id'));
                                    var attrDelObj = $.grep($scope.Finattributes, function (e) {
                                        return e.ID == ID;
                                    });
                                    $scope.Finattributes.splice($.inArray(attrDelObj[0], $scope.Finattributes), 1);
                                });
                                $('#RootLevelSelectAll').next('i').removeClass('checked');
                            }
                        });
                    }
                }
            });
        };

        function GetSelectChkBoxID() {
            var IDList = new Array();
            $(".findchkbox >td .findchkboxes").each(function () {
                if ($(this).parents('td').attr('data-id') != undefined) IDList.push($(this).parents('td').attr('data-id'));
            });
            return IDList;
        }
        $scope.IsSelectAllChecked = false;
        $(document).on('click', '.checkbox-custom > input[id=FinMetadataDelete]', function (e) {
            var status = this.checked;
            $(".findchkbox >td .findchkboxes").each(function () {
                this.checked = status;
                if (status) {
                    $(this).find('i').addClass('checked');
                } else {
                    $(this).find('i').removeClass('checked');
                }
            });
        });
        $scope.AddtreeOptionsNewChild = function (data) {
            var post = data.Children.length + 1;
            var newName = data.Caption + '-' + post;
            newName = "";
            var child_node = {
                Caption: newName,
                Level: data.Level + 1,
                id: 0,
                IsDeleted: false,
                Children: []
            };
            if (data.Caption != 'Root') {
                child_node.Level = data.Level + 1;
            }
            if (data.Children) {
                data.Children.push(child_node);
                var newLevel = $.inArray(child_node.Level, $scope.wizard.TreeLevelNumberByNode);
                $scope.wizard.TreeLevelNumberByNode.push(child_node.Level);
                if (newLevel == -1) {
                    var level = {
                        Level: child_node.Level,
                        Caption: '',
                        ID: 0
                    }
                    $scope.wizard.TreeLevels.push(level);
                }
            } else {
                data.Children = [child_node];
            }
        };
        $scope.addFinancialMetadata = function () {
            if ($scope.wizard.Caption == '' || $scope.wizard.Caption == undefined || $scope.wizard.Description == '' || $scope.wizard.Description == undefined || $scope.wizard.attributeType == '0') {
                bootbox.alert($translate.instant('LanguageContents.Res_1811.Caption'));
                return true;
            }
            if ($scope.wizard.attributeType === "3" || $scope.wizard.attributeType === "4") {
                if ($scope.optionsLists == "") {
                    bootbox.alert($translate.instant('LanguageContents.Res_1812.Caption'));
                    return true;
                }
            }
            var addFinancialAttribute = {};
            addFinancialAttribute.AttributeTypeID = $scope.wizard.attributeType;
            addFinancialAttribute.Caption = $scope.wizard.Caption;
            addFinancialAttribute.Description = $scope.wizard.Description;
            addFinancialAttribute.IsSystemDefined = 0;
            addFinancialAttribute.IsSpecial = 0;
            addFinancialAttribute.ID = $scope.wizard.AttributeId;
            addFinancialAttribute.FinAttType = $scope.FinType;
            $scope.getattrResponse = addFinancialAttribute.Response;
            AdminService.addFinancialAttribute(addFinancialAttribute).then(function (getattribute) {
                $scope.Finattributes.push({
                    "ID": getattribute.Response.m_Item1,
                    "Caption": $scope.wizard.Caption,
                    "AttributeTypeID": $scope.wizard.attributeType,
                    "Description": $scope.wizard.Description,
                    "IsSystemDefined": false,
                    "IsSpecial": false,
                    "FinTypeID": $scope.FinType,
                    "IsColumn": false,
                    "IsTooltip": false,
                    "IsCommitTooltip": false,
                    "SortOrder": getattribute.Response.m_Item2
                });
                if ($scope.wizard.attributeType > 0) {
                    if ($scope.wizard.attributeType === "3" || $scope.wizard.attributeType === "4") {
                        var addfinoptions = {};
                        addfinoptions.Options = $scope.optionsLists;
                        addfinoptions.AttributeID = getattribute.Response.m_Item1;
                        AdminService.addFinancialOption(addfinoptions).then(function (addoptionsResponse) {
                            $scope.optionsLists = [];
                        });
                    }
                }
                $scope.wizard.AttributeId = 0;
            });
            NotifySuccess($translate.instant('LanguageContents.Res_4116.Caption'));
            $('#AddFinancialMetadata').modal('hide');
        };
        $scope.ID = 0;
        $scope.EditMetadata = function EditMetadata(row) {
            $timeout(function () {
                $('#AttributeCaption').focus();
            }, 1000);
            $scope.AttrType = "";
            $scope.showadd = false;
            $scope.showupdate = true;
            $scope.ID = row.ID;
            $scope.wizard.IsDisableAttributeType = true;
            $scope.wizard.Caption = row.Caption;
            $scope.wizard.Description = row.Description;
            $scope.wizard.attributeType = row.AttributeTypeID;
            $scope.wizard.FinTypeID = row.FinTypeID;
            $scope.wizard.IsColumn = row.IsColumn;
            $scope.wizard.IsToolTip = row.IsTooltip;
            $scope.wizard.IsCommitToolTip = row.IsCommitTooltip;
            $scope.wizard.SortOrder = row.SortOrder;
            $scope.wizard.IsSystemDefined = row.IsSystemDefined;
            if (row.AttributeTypeID == 3 || row.AttributeTypeID == 4) {
                $scope.AttrType = "Option";
                $scope.EnableOptionUpdate = false;
                $scope.EnableOptionAdd = true;
                AdminService.GetFinancialAttributeOptions($scope.ID).then(function (getattributesOptions) {
                    $scope.optionListSer = getattributesOptions.Response;
                    $scope.count = 1;
                    for (var i = 0; i < $scope.optionListSer.length; i++) {
                        $scope.optionsLists.push({
                            "PId": $scope.count,
                            "ID": $scope.optionListSer[i].ID,
                            "Caption": $scope.optionListSer[i].Caption,
                            "SortOrder": $scope.optionListSer[i].SortOrder,
                            "FinAttributeID": $scope.optionListSer[i].FinAttributeID
                        });
                        $scope.count = $scope.count + 1;
                    }
                });
            } else $scope.AttrType = "";
        }
        $scope.updateFinancialMetadata = function updateFinancialMetadata() {
            var updateFinMetadata = {};
            updateFinMetadata.ID = $scope.ID;
            updateFinMetadata.Caption = $scope.wizard.Caption;
            updateFinMetadata.Description = $scope.wizard.Description;
            updateFinMetadata.FinTypeID = $scope.wizard.FinTypeID;
            updateFinMetadata.AttributeTypeID = $scope.wizard.attributeType;
            updateFinMetadata.IsSystemDefined = $scope.wizard.IsSystemDefined;
            updateFinMetadata.IsColumn = $scope.wizard.IsColumn;
            updateFinMetadata.IsToolTip = $scope.wizard.IsToolTip;
            updateFinMetadata.IsCommitToolTip = $scope.wizard.IsCommitToolTip;
            updateFinMetadata.SortOrder = $scope.wizard.SortOrder;
            AdminService.UpdateFinancialMetadata(updateFinMetadata).then(function (getattribute) {
                if (getattribute.Response == true) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4805.Caption'));
                    $('#AddFinancialMetadata').modal('hide');
                    var updatevalue = $.grep($scope.Finattributes, function (e) {
                        return e.ID == $scope.ID;
                    });
                    updatevalue[0].AttributeTypeID = $scope.wizard.attributeType;
                    updatevalue[0].Caption = $scope.wizard.Caption;
                    updatevalue[0].Description = $scope.wizard.Description;
                    updatevalue[0].IsSystemDefined = $scope.wizard.IsSystemDefined;
                    updatevalue[0].IsSpecial = "False";
                    updatevalue[0].ID = $scope.ID;
                    updatevalue[0].IsCommitTooltip = $scope.wizard.IsCommitToolTip;
                    updatevalue[0].IsColumn = $scope.wizard.IsColumn;
                    updatevalue[0].IsTooltip = $scope.wizard.IsToolTip;
                    updatevalue[0].SortOrder = $scope.wizard.SortOrder
                    if ($scope.optionsLists.length > 0) {
                        if ($scope.wizard.attributeType == "3" || $scope.wizard.attributeType == "4") {
                            var addfinoptionsupdate = {};
                            addfinoptionsupdate.Options = $scope.optionsLists;
                            addfinoptionsupdate.AttributeID = $scope.ID;
                            AdminService.addFinancialOption(addfinoptionsupdate).then(function (addfinoptionsupdateresponse) {
                                $scope.optionsLists = [];
                            });
                        }
                    }
                    $scope.wizard.AttributeId = 0;
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4941.Caption'));
                }
            });
        }
        $("#financialmetadatalist tbody").sortable({
            axis: "y",
            scroll: true,
            cursor: "move",
            update: function (event, ui) {
                var listItems = $("tr", this);
                var uiArray = new Array();
                listItems.length;
                var sortorderval = 0;
                listItems.each(function (idx, li) {
                    sortorderval = sortorderval + 1;
                    var finid = parseInt($(li).find('td:first').attr('data-id'));
                    uiArray.push($(li).find('td:first').attr('data-id'));
                    $.grep($scope.Finattributes, function (e) {
                        return e.ID == finid;
                    })[0].SortOrder = sortorderval;
                    var updatesortorderfin = {};
                    updatesortorderfin.ID = finid;
                    updatesortorderfin.SortOrder = sortorderval;
                    AdminService.UpdateFinMetadataSortOrder(updatesortorderfin).then(function (updatesort) { });
                });
                $scope.Finattributes
            }
        });
        $scope.GetChckedFinancialMetadata = function (row, chckboxtype) {
            $scope.ID = row.ID;
            $scope.wizard.IsDisableAttributeType = true;
            $scope.wizard.Caption = row.Caption;
            $scope.wizard.Description = row.Description;
            $scope.wizard.attributeType = row.AttributeTypeID;
            $scope.wizard.FinTypeID = row.FinTypeID;
            $scope.wizard.SortOrder = row.SortOrder;
            $scope.wizard.IsSystemDefined = row.IsSystemDefined;
            if (chckboxtype == "IsColumn" && row.IsColumn == true) {
                $scope.wizard.IsColumn = false;
                $scope.wizard.IsToolTip = row.IsTooltip;
                $scope.wizard.IsCommitToolTip = row.IsCommitTooltip;
            } else if (chckboxtype == "IsColumn" && row.IsColumn == false) {
                $scope.wizard.IsColumn = true;
                $scope.wizard.IsToolTip = row.IsTooltip;
                $scope.wizard.IsCommitToolTip = row.IsCommitTooltip;
            } else if (chckboxtype == "IsTooltip" && row.IsTooltip == false) {
                $scope.wizard.IsColumn = row.IsColumn;
                $scope.wizard.IsToolTip = true;
                $scope.wizard.IsCommitToolTip = row.IsCommitTooltip;
            } else if (chckboxtype == "IsTooltip" && row.IsTooltip == true) {
                $scope.wizard.IsToolTip = false;
                $scope.wizard.IsColumn = row.IsColumn;
                $scope.wizard.IsCommitToolTip = row.IsCommitTooltip;
            } else if (chckboxtype == "IsCommitTooltip" && row.IsCommitTooltip == true) {
                $scope.wizard.IsCommitToolTip = false;
                $scope.wizard.IsColumn = row.IsColumn;
                $scope.wizard.IsToolTip = row.IsTooltip;
            } else if (chckboxtype == "IsCommitTooltip" && row.IsCommitTooltip == false) {
                $scope.wizard.IsCommitToolTip = true;
                $scope.wizard.IsColumn = row.IsColumn;
                $scope.wizard.IsToolTip = row.IsTooltip;
            }
            $scope.updateFinancialMetadata();
        };
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.financialmetadatactrl']"));
        });
        $scope.set_color = function (clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            };
            else return '';
        }
    }
    app.controller("mui.admin.financialmetadatactrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$translate', 'AdminService', muiadminfinancialmetadatactrl]);
})(angular, app);