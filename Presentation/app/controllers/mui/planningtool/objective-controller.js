﻿(function (ng, app) {
    "use strict";
    //app.controller(
    //	"mui.planningtool.objectiveCtrl",
    function muiplanningtoolobjectiveCtrl($scope) {
        // --- Define Controller Methods. ------------------- //
        // ...

        // --- Define Scope Methods. ------------------------ //
        // ...


        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.objectiveCtrl']"));
        });
        // --- Initialize. ---------------------------------- //
        // ...
    }
    //);
    app.controller("mui.planningtool.objectiveCtrl", ['$scope', muiplanningtoolobjectiveCtrl]);
})(angular, app);