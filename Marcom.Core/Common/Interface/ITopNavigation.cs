﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    public class ITopNavigation
    {
        #region Public Properties

        IList<INavigation> TopMenu
        {
            get;
            set;

        }

        IList<INavigation> ChildMenu
        {
            get;
            set;

        }

        #endregion
    }
}
