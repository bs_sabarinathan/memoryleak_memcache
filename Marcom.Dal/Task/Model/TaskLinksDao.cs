﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Task.Model
{
    /// <summary>
    /// TaskLinksDao object for table 'TM_Links'.
    /// </summary>
    public partial class TaskLinksDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected string _name;
        protected string _url;
        protected int _activeversionno;
        protected int _typeid;
        protected string _createdon;
        protected int _ownerid;
        protected int _moduleid;
        protected Guid _linkguid;
        protected string _description;
        protected int _linkType;

        #endregion

        #region Constructors
        public TaskLinksDao() { }

        public TaskLinksDao(int pEntityid, string pName, string pURL, int pActiveVersionNo, int pTypeID, string pCreatedOn, int pOwnerID, int pModuleID, Guid pLinkGuid, string pDescription,int pLinkType)
        {
            this._entityid = pEntityid;
            this._name = pName;
            this._url = pURL;
            this._activeversionno = pActiveVersionNo;
            this._typeid = pTypeID;
            this.CreatedOn = pCreatedOn;
            this._ownerid = pOwnerID;
            this._moduleid = pModuleID;
            this._linkguid = pLinkGuid;
            this._description = pDescription;
            this._linkType = pLinkType;
        }

        public TaskLinksDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties
        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int EntityID
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }

        }

        public virtual string URL
        {
            get { return _url; }
            set { _url = value; }

        }

        public virtual int LinkType
        {
            get { return _linkType; }
            set { _linkType = value; }

        }

        public virtual int ActiveVersionNo
        {
            get { return _activeversionno; }
            set { _activeversionno = value; }

        }

        public virtual int TypeID
        {
            get { return _typeid; }
            set { _typeid = value; }

        }




        public virtual string CreatedOn
        {
            get { return _createdon; }
            set { _createdon = value; }

        }



        public virtual int OwnerID
        {
            get { return _ownerid; }
            set { _ownerid = value; }

        }

        public virtual int ModuleID
        {
            get { return _moduleid; }
            set { _moduleid = value; }
        }

        public virtual Guid LinkGuid
        {
            get { return _linkguid; }
            set { _linkguid = value; }
        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string TaskLinksDao = "TaskLinksDao";
            public const string ID = "ID";
            public const string EntityID = "EntityID";
            public const string Name = "Name";
            public const string URL = "URL";
            public const string ActiveVersionNo = "ActiveVersionNo";
            public const string TypeID = "TypeID";
            public const string CreatedOn = "CreatedOn";
            public const string OwnerID = "OwnerID";
            public const string ModuleID = "ModuleID";
            public const string LinkGuid = "LinkGuid";
            public const string Description = "Description";
            public const string LinkType = "LinkType";

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string TaskLinksDao = "TaskLinksDao";
            public const string ID = "ID";
            public const string EntityID = "EntityID";
            public const string Name = "Name";
            public const string URL = "URL";
            public const string ActiveVersionNo = "ActiveVersionNo";
            public const string TypeID = "TypeID";
            public const string CreatedOn = "CreatedOn";
            public const string OwnerID = "OwnerID";
            public const string ModuleID = "ModuleID";
            public const string LinkGuid = "LinkGuid";
            public const string Description = "Description";
            public const string LinkType = "LinkType";
        }

        #endregion


    }
}
