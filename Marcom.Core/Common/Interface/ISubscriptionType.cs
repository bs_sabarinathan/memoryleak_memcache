using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// ISubscriptionType interface for table 'CM_SubscriptionType'.
    /// </summary>
    public interface ISubscriptionType
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        string Caption
        {
            get;
            set;

        }

       
        bool isAppDefault
        {
            get;
            set;

        }
        bool isAppMandatory
        {
            get;
            set;

        }
        bool isMailDefault
        {
            get;
            set;

        }
        bool isMailMandatory
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
