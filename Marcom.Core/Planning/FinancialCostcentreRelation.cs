﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

    /// <summary>
    /// Attachments object for table 'PM_Attachments'.
    /// </summary>

    internal class FinancialCostcentreRelation : IFinancialCostcentreRelation
    {

        #region Public Properties

        public int EntityID
        {
            get;
            set;

        }

        public int Costcentreid
        {
            get;
            set;

        }



        public int Status
        {
            get;
            set;

        }

        #endregion
    }

    public class EntityFinancialOrder 
    {
        public int EntityID { get; set; }
        public string uniqueKey { get; set; }
    }

}
