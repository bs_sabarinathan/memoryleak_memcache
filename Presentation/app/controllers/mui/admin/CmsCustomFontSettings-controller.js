﻿(function (ng, app) {
    "use strict"; function muiadmincmscustomfontsettingsCtrl($scope, AdminService, $timeout, $translate) {
        $scope.ListOfCustomFonts = []; $scope.InvalidKit = false; $scope.IsValidated = false; $scope.ListOfFontsFromPlugin = []; $scope.ProviderId = 1; $scope.GetListOfCmsCustomFontFromDB = function () {
            AdminService.GetListOfCmsCustomFont().then(function (ResCustomFonts) {
                if (ResCustomFonts.Response != null)
                    $scope.ListOfCustomFonts = ResCustomFonts.Response;
            });
        }; $scope.GetListOfCmsCustomFontFromDB(); $scope.GetTypeKitFontsFromPlugin = function () { $scope.ListOfFontsFromPlugin.length = 0; $.getJSON("https://typekit.com/api/v1/json/kits/" + $scope.kitid + "/published?callback=?", function (data) { if (data.errors) { $timeout(function () { $scope.InvalidKit = true; $scope.IsValidated = false; }, 10); } else { $("#availableFonts").html(''); $timeout(function () { $scope.InvalidKit = false; $scope.IsValidated = true; }, 10); var itm = ""; $.each(data.kit.families, function (i, family) { itm += "<li><a>" + family.css_names[0] + "</a>"; itm += "<span class=\"sampleFont\" style=\"font-family:" + family.css_names[0] + "\">Aa Bb Cc Dd Ee Ff Gg Hh Ii Jj Kk Ll Mm Nn Oo Pp Qq Rr Ss Tt Uu Vv Ww Xx Yy Zz</span></li>"; $scope.ListOfFontsFromPlugin.push(family.css_names[0]); }); $("#availableFonts").append(itm); } }); }
        $scope.InsertUpdateCmsCustomFont = function () {
            var customfonts = { customfonts: $scope.ListOfFontsFromPlugin.join(","), kitId: $scope.kitid, providerId: $scope.ProviderId }; AdminService.InsertUpdateCmsCustomFont(customfonts).then(function (Res) {
                if (Res.Response == true) { $scope.GetListOfCmsCustomFontFromDB(); NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption')); }
                else
                    NotifyError($translate.instant('LanguageContents.Res_1144.Caption'));
            }); $("#customFontEditPopup").modal('hide');
        }
        $scope.EditCustomFont = function (objfnt) {
            $scope.Clear(); $scope.kitid = objfnt.KitID; for (var j = 0; j < $scope.ListOfCustomFonts.length; j++) { var fontlist = $.grep($scope.ListOfCustomFonts[j], function (e) { return e.ProviderID == objfnt.ProviderID; }); }
            $("#availableFonts").html(''); var itm = ""; for (var i = 0; i < fontlist.length; i++) { itm += "<li><a>" + fontlist[i].Name + "</a>"; itm += "<span class=\"sampleFont\" style=\"font-family:" + fontlist[i].Name + "\">Aa Bb Cc Dd Ee Ff Gg Hh Ii Jj Kk Ll Mm Nn Oo Pp Qq Rr Ss Tt Uu Vv Ww Xx Yy Zz</span></li>"; $scope.ListOfFontsFromPlugin.push(fontlist[i].Name); }
            $("#availableFonts").append(itm);
        }; $scope.DeleteCustomFont = function (providerid) {
            AdminService.DeleteCmsCustomFont(providerid).then(function (resDel) {
                if (resDel.Response == true) { $scope.GetListOfCmsCustomFontFromDB(); NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption')); }
                else
                    NotifyError($translate.instant('LanguageContents.Res_1144.Caption'));
            });
        }; $scope.Clear = function () { $scope.InvalidKit = false; $scope.IsValidated = false; $scope.kitid = ''; $scope.ListOfFontsFromPlugin.length = 0; $("#availableFonts").html(''); }
        $scope.$on("$destroy", function () { RecursiveUnbindAndRemove($("[ng-controller='mui.admin.cmscustomfontsettingsCtrl']")); });
    }
    app.controller('mui.admin.cmscustomfontsettingsCtrl', ['$scope', 'AdminService', '$timeout', '$translate', muiadmincmscustomfontsettingsCtrl]);
})(angular, app);