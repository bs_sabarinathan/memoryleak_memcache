﻿using BrandSystems.Marcom.Core.Planning.Interface;
using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// PurchaseOrderDao object for table 'PM_PurchaseOrder'.
    /// </summary>

    public partial class PurchaseOrderDetail : IPurchaseOrderDetail
    {
        #region Member Variables


        protected int _id;
        protected int _costcentreid;


        protected int _poid;
        protected string _description;

        protected int _currencytype;
      
        protected decimal _amount;

        #endregion

        #region Constructors
        public PurchaseOrderDetail() { }

        public PurchaseOrderDetail(int pCostCentreid, int pPOID, int pCurrencyType, int PCurrencyType, decimal pAmount)
        {
            this._costcentreid = pCostCentreid;
           
            this._poid = pPOID;
          
            this._currencytype = pCurrencyType;
            this._amount = pAmount;


        }

        public PurchaseOrderDetail(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set {  _id = value; }

        }

        public virtual int CostCentreID
        {
            get { return _costcentreid; }
            set {_costcentreid = value; }

        }

        public virtual decimal Amount
        {
            get { return _amount; }
            set {  _amount = value; }

        }

        public virtual int POID
        {
            get { return _poid; }
            set {  _poid = value; }

        }

        public virtual int CurrencyType
        {
            get { return _currencytype; }
            set {  _currencytype = value; }

        }

       

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            PurchaseOrderDetail castObj = null;
            try
            {
                castObj = (PurchaseOrderDetail)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion


    }

}
