﻿(function (ng, app) {
    "use strict";
    //app.controller('gridsterCtrl',
    function gridsterCtrl($window, $scope, $element, $attrs, $resource, $timeout, DashboardService, $translate) {
        var self = this;

        self.$el = $($element);
        self.gridster = null;

        self.updateModel = function (event, ui) {
            var serializedGrid = self.gridster.serialize();
            angular.forEach($scope.widgets, function (widget, idx) {
                widget.grid = serializedGrid[idx];
            });
            $scope.$apply();

            var newDimensions = self.calculateNewDimensions();

            $('#dynamicWidgets li[data-widget-id]').each(function () {
                var rows = parseInt($(this).attr('data-sizey'));
                $('.box-content', this).height((newDimensions[0][0] * rows) - 32);
                $('.graph', this).height($('.box-content', this).height() - 20);
            });
        };
        self.OnDragEnd = function (event, ui) {
            var serializedGrid = self.gridster.serialize();
            var widgetData = [];
            angular.forEach($scope.widgets, function (widget, idx) {
                widget.grid = serializedGrid[idx];
                widgetData.push({ widgtid: widget.WidgetRealid.toString().trim(), templateid: $window.TemplateID, grid: widget.grid });
            });

            var newDimensions = self.calculateNewDimensions();

            //karthi bug fix box-content not update the height
            $('#dynamicWidgets li[data-widget-id]').each(function () {
                var rows = parseInt($(this).attr('data-sizey'));
                $('.box-content', this).height((newDimensions[0][0] * rows) - 32);
                $('.graph', this).height($('.box-content', this).height() - 20);
            });

            //call service here and pass widgetData

            //var EditWidgetOnDrag = $resource('common/WidgetDragEditing/');
            //var editwidgetdrag = new EditWidgetOnDrag();
            var editwidgetdrag = {};

            editwidgetdrag.widgetdatalist = widgetData;

            if ($window.ISTemplate == true) {
                editwidgetdrag.IsAdminPage = true;
            }
            else {
                editwidgetdrag.IsAdminPage = false;
            }

            //  var saveusercomment = EditWidgetOnDrag.save(editwidgetdrag, function () {

            DashboardService.WidgetDragEditing(editwidgetdrag).then(function (saveusercomment) {

            });

            $scope.$apply();
        };
        self.defaultOptions = {
            shift_larger_widgets_down: false,
            max_size_x: 6, // @todo: ensure that max_size_x match number of cols
            // options used by widget_resize_dimensions extension
            cols: 6,
            margin_ratio: 0.1,
            resize_time: 500,
            max_size_y: 2,
            avoid_overlapped_widgets: true,
            serialize_params: function ($w, wgd) {
                return {
                    col: wgd.col,
                    row: wgd.row,
                    sizex: wgd.size_x,
                    sizey: wgd.size_y
                };
            },
            draggable: {
                stop: self.OnDragEnd,
                handle: '.move'
            },
            widget_margins: [10, 10],
            widget_base_dimensions: [400, 225]
        };

        self.options = angular.extend(self.defaultOptions, $scope.$eval($attrs.options));

        self.attachElementToGridster = function (li) {
            //attaches a new element to gridster
            var $w = li.addClass('gs_w').appendTo(self.gridster.$el);
            self.gridster.$widgets = self.gridster.$widgets.add($w);
            self.gridster.register_widget($w).add_faux_rows(1).set_dom_grid_height();
            //            $w.fadeIn();
            $w.css('opacity', 1);
        };

        self.calculateNewDimensions = function () {
            var containerWidth = self.$el.innerWidth();
            var newMargin = Math.round(containerWidth * self.options.margin_ratio / (self.options.cols * 2));
            var newSize = Math.round(containerWidth * (1 - self.options.margin_ratio) / self.options.cols);
            return [[newSize, newSize], [newMargin, newMargin]];
        }

        self.resizeWidgetDimensions = function (isPageLoad) {
            // Calculate widget dimension proportional to parent dimension.
            var newDimensions = self.calculateNewDimensions();

            var $ul = $('#dashboardBlock');
            var MaxWidth = (newDimensions[0][0] + (newDimensions[1][0] * 2)) * self.defaultOptions.cols;
            $ul.width(MaxWidth - 4);
            $ul.height($(window).height() + 150);

            $('#dynamicWidgets li[data-widget-id]').each(function () {
                var rows = parseInt($(this).attr('data-sizey'));
                $('.box-content', this).height(newDimensions[0][0] - 32);
                $('.graph', this).width($('.box-content', this).width() - 20);

                var chartid = $(this).find('.graph').attr("id");
                if (chartid != undefined) {
                    $(document).trigger("OnChartLoad", chartid);
                }

            });

            // Set new "fluid" widget dimensions
            self.gridster.resize_widget_dimensions({
                widget_base_dimensions: newDimensions[0],
                widget_margins: newDimensions[1]
            });

            self.updateModel();
        };

        self.hookWidgetResizer = function () {

            // Debounce
            $($window).on('resize', function (evt) {
                evt.preventDefault();
                $window.clearTimeout(self.resizeTimer);
                self.resizeTimer = $window.setTimeout(function () {
                    self.resizeWidgetDimensions(true);
                }, self.options.resize_time);
            });
        };

        $scope.$watch('widgets.length', function (newValue, oldValue) {
            if (newValue === oldValue) return;
            // Added a widget
            if (newValue === oldValue + 1) {
                $timeout(function () {
                    var li = self.$el.find('#dashboardBlock').find('li.box').last();
                    self.attachElementToGridster(li);
                });
            } else {
                // Removed a widget
            }
        });

        $scope.removeWidget = function (widget) {
            var id = widget.id;

            var $li = self.$el.find('[data-widget-id="' + id + '"]');

            $li.fadeOut('slow', function () {
                self.gridster.remove_widget($li, function () {
                    $scope.onremove({ widget: widget });
                });
            });
        };

        $scope.editWidget = function (widget) {
            $scope.onedit({ widget: widget });
        };

    }
    //);
    app.controller("gridsterCtrl", ['$window', '$scope', '$element', '$attrs', '$resource', '$timeout', 'DashboardService', '$translate', gridsterCtrl]);
    //app.controller(
    //   "mui.dashboardCtrl",
    function muidashboardCtrl($scope, $resource, $cookies, $compile, $timeout, $window, DashboardService, $translate) {
        $scope.wigetvar = {
            caption: '',
            description: '',
            attributeId: 0,
            widgetid: '',
            widgetType: 0,
            dimensionid: '',
            matrixid: [],
            noofitem: '',
            col: 1,
            row: 1,
            sizex: 2,
            sizey: 1,
            AddOrEdit: '',
            showcntrlforDynamic: false,
            NoOfItems: false,
            addbtn: false,
            filterID: 0,
            listofEntityID: '',
            listofSelectEntityID: '',
            visualtypeid: '',
            showstaticcntrlforDynamic: false,
            showcntrlforyear: false,
            noofYear: '',
            noofMonth: ''
        };

        $scope.GlobalTemplateID = 0;
        $scope.step = 0;

        $scope.MatrixData = [
    { matrixName: 'Assigned Amount', id: '1', dimensionid: '1' },
    { matrixName: 'In requests', id: '2', dimensionid: '1' },
    { matrixName: 'Approved budgets', id: '3', dimensionid: '1' },
    { matrixName: 'Approved allocated amount', id: '4', dimensionid: '1' },
    { matrixName: 'Total spent', id: '5', dimensionid: '1' },
    { matrixName: 'Available to spent', id: '6', dimensionid: '1' },
    { matrixName: 'In requests', id: '1', dimensionid: '2' },
    { matrixName: 'Approved budgets', id: '2', dimensionid: '2' },
    { matrixName: 'Approved allocated amount', id: '3', dimensionid: '2' },
    { matrixName: 'Total spent', id: '4', dimensionid: '2' },
    { matrixName: 'Available to spent', id: '5', dimensionid: '2' },
    { matrixName: 'Count', id: '1', dimensionid: '3' },
    { matrixName: 'Count', id: '1', dimensionid: '4' },
    { matrixName: 'Count', id: '1', dimensionid: '5' },
    { matrixName: 'Count', id: '1', dimensionid: '6' }
        ];

        $scope.MatrixDataForDimension = [];

        $scope.VisualTypeData = [
   { visualtypeName: 'Bar Chart', id: '1' },
   { visualtypeName: 'Pie Chart', id: '2' }
        ];

        var yeardata = [];
        for (var i = 2010; i <= 2030; i++) {

            yeardata.push({ "ID": i, "Value": i });
        }
        $scope.listofYear = yeardata;
        var Monthdata = [];
        Monthdata.push({ "ID": 0, "Value": 'All' });
        Monthdata.push({ "ID": 1, "Value": 'January' });
        Monthdata.push({ "ID": 2, "Value": 'February' });
        Monthdata.push({ "ID": 3, "Value": 'March' });
        Monthdata.push({ "ID": 4, "Value": 'April' });
        Monthdata.push({ "ID": 5, "Value": 'May' });
        Monthdata.push({ "ID": 6, "Value": 'June' });
        Monthdata.push({ "ID": 7, "Value": 'July' });
        Monthdata.push({ "ID": 8, "Value": 'August' });
        Monthdata.push({ "ID": 9, "Value": 'September' });
        Monthdata.push({ "ID": 10, "Value": 'October' });
        Monthdata.push({ "ID": 11, "Value": 'November' });
        Monthdata.push({ "ID": 12, "Value": 'December' });
        $scope.listofMonth = Monthdata;

        $scope.wigetvar.noofYear = (new Date.create()).getFullYear();
        $scope.wigetvar.noofMonth = (new Date.create()).getMonth() + 1;

        var SystemsDefinedAttributes = { "NewsFeed": 1, "Milestone": 2, "Financial": 3, };

        // --- Define Controller Methods. ------------------- //
        // ...
        $scope.templistofEntityOrCostChecked = [];
        $scope.listfilteredEntityOrCostvalues = [];
        $scope.widgets = [];
        $scope.dyn_Cont = "";
        var IsforAdmin = ($scope.ISTemplate == undefined ? false : $scope.ISTemplate);
        var TemplateID = ($scope.DashboardTemplateId == undefined ? 0 : $scope.DashboardTemplateId);
        $scope.wigetvar.addbtn = false;

        $scope.Iscompile = false;

        $scope.getwidgetSource = [];
        //var GetwidgetServ = $resource('common/GetWidgetDetailsByUserID/:UserID/:IsAdmin/:GlobalTemplateID', { UserID: parseInt($cookies['UserId'], 10), IsAdmin: IsforAdmin, GlobalTemplateID: TemplateID }, { get: { method: 'GET' } });
        //var widgetListList = GetwidgetServ.get({ UserID: parseInt($cookies['UserId'], 10), IsAdmin: IsforAdmin, GlobalTemplateID: TemplateID }, function () {
        DashboardService.GetWidgetDetailsByUserID(parseInt($cookies['UserId'], 10), IsforAdmin, TemplateID).then(function (widgetListList) {
            if (widgetListList.Response[0].WidgetTypeID == 0 && widgetListList.Response != null) {
                 $scope.dyn_Cont = "<div class=\"row-fluid gridster\" widgets='widgets'  onremove='removeWidget(widget)' onedit ='editWidget(widget)' > </div>";
                 $("#dynamicWidgets").html($compile($scope.dyn_Cont)($scope));
            }
            if (widgetListList.Response != null && widgetListList.Response[0].WidgetTypeID != 0) {
           // if (widgetListList.Response != null) {
                $scope.getwidgetSource = widgetListList.Response;

                if (widgetListList.Response != null || widgetListList.Response.length > 0) {
                    $scope.GlobalTemplateID = $scope.getwidgetSource[0].TemplateID;
                    if ($scope.ISTemplate == true) {
                        $window.TemplateID = $scope.DashboardTemplateId;
                        $window.ISTemplate = true;
                    }
                    else {
                        $scope.DashboardTemplateId = $scope.GlobalTemplateID;
                        $window.TemplateID = $scope.GlobalTemplateID;
                        $window.ISTemplate = false;
                    }

                    for (var i = 0; i < $scope.getwidgetSource.length; i++) {
                        $scope.widgets.push({
                            title: $scope.getwidgetSource[i].Caption,
                            WidgetRealid: $scope.getwidgetSource[i].Id.toString().trim(),
                            Typeid: $scope.getwidgetSource[i].WidgetTypeID,
                            MatrixID: $scope.getwidgetSource[i].MatrixID,
                            DimensionID: $scope.getwidgetSource[i].DimensionID,
                            VisualType: $scope.getwidgetSource[i].VisualType,
                            NoOfItem: $scope.getwidgetSource[i].NoOfItem,
                            NoOfYear: $scope.getwidgetSource[i].NoOfYear,
                            NoOfMonth: $scope.getwidgetSource[i].NoOfMonth,
                            grid: { row: $scope.getwidgetSource[i].Row, col: $scope.getwidgetSource[i].Column, sizex: $scope.getwidgetSource[i].SizeX == 0 ? 2 : $scope.getwidgetSource[i].SizeX, sizey: $scope.getwidgetSource[i].SizeY == 0 ? 1 : $scope.getwidgetSource[i].SizeY },

                        });
                    }
                    $scope.dyn_Cont = "<div class=\"row-fluid gridster\" widgets='widgets'  onremove='removeWidget(widget)' onedit ='editWidget(widget)' > </div>";
                    $("#dynamicWidgets").html(
                                              $compile($scope.dyn_Cont)($scope));



                    $timeout(function () { checkcontent(); }, 500);
                }
            }
        });

        function checkcontent() {
            $('#dynamicWidgets>div>ul>li[data-class="pending"]:eq(0)').each(function (j) {
                $(this).attr('data-class', 'Finished');
                var sizey = $(this).attr("data-sizey");// This is your sizey value
                var WizardID = $(this).attr("data-widget-id");
                var WizardRealId = $(this).attr("data-widget-Realid");
                var WizardTypeID = $(this).attr("data-widget-Typeid");
                var MatrixID = $(this).attr("data-widget-MatrixID");
                var DimensionID = $(this).attr("data-widget-DimensionID");
                var visualType = $(this).attr("data-widget-VisualType");
                var WizardNoOfItem = $(this).attr("data-widget-NoOfItem");
                var WizardNoOfYear = $(this).attr("data-widget-NoOfYear");
                var WizardNoOfMonth = $(this).attr("data-widget-NoOfMonth");
                $(this).find('.box-content-margin').each(function (k) {
                    if (WizardTypeID == 1) {
                        var template = '<activity-item type=\"NewsFeed\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                           $compile(template)($scope));
                    }
                    else if (WizardTypeID == 2) {
                        var template = '<activity-item type=\"Milestone\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                           $compile(template)($scope));
                    }
                    else if (WizardTypeID == 3) {
                        if (DimensionID == 1) {
                            var template = '<activity-item type=\"CostCenterFinancial\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                            $(this).html(
                               $compile(template)($scope));
                        }
                        else if (DimensionID == 2) {
                            var template = '<activity-item type=\"ActivityFinancial\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                            $(this).html(
                               $compile(template)($scope));
                        }
                        else if (DimensionID == 3) {
                            var template = '<activity-item type=\"Browser\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                            $(this).html(
                               $compile(template)($scope));
                        }
                        else if (DimensionID == 4) {
                            var template = '<activity-item type=\"BrowserVersion\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                            $(this).html(
                               $compile(template)($scope));
                        }
                        else if (DimensionID == 5) {
                            var template = '<activity-item type=\"OS\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                            $(this).html(
                               $compile(template)($scope));
                        }
                        else if (DimensionID == 6) {
                            var template = '<activity-item type=\"CountryName\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                            $(this).html(
                               $compile(template)($scope));
                        }
                    }
                    else if (WizardTypeID == 4) {
                        var template = '<activity-item type=\"Workspace\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" NoOfItem=\"' + WizardNoOfItem + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                           $compile(template)($scope));
                    }
                    else if (WizardTypeID == 5) {
                        var template = '<activity-item type=\"topxactivity\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" NoOfItem=\"' + WizardNoOfItem + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                           $compile(template)($scope));
                    }
                    else if (WizardTypeID == 6) {
                        var template = '<activity-item type=\"Notification\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                           $compile(template)($scope));
                    }
                    else if (WizardTypeID == 7) {
                        var template = '<activity-item type=\"MyTask\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" NoOfItem=\"' + WizardNoOfItem + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                          $compile(template)($scope));
                    }
                    else if (WizardTypeID == 8) {
                        var template = '<activity-item type=\"FundingRequest\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" NoOfItem=\"' + WizardNoOfItem + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                          $compile(template)($scope));
                    }
                    else if (WizardTypeID == 9) {
                        var template = '<activity-item type=\"PublishedAsset\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" NoOfItem=\"' + WizardNoOfItem + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                          $compile(template)($scope));
                    }
                    else if (WizardTypeID == 10) {
                        var template = '<activity-item type=\"DownloadedAsset\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" NoOfItem=\"' + WizardNoOfItem + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                          $compile(template)($scope));
                    }
                    else if (WizardTypeID == 11) {
                        var template = '<activity-item type=\"UploadAsset\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" NoOfYear=\"' + WizardNoOfYear + '\" NoOfMonth=\"' + WizardNoOfMonth + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                          $compile(template)($scope));
                    }
                });
                $timeout(function () { checkcontent(); }, 500);
            });
        }

        function ReloadContentInEdit(widgetid) {


            $('#dynamicWidgets>div>ul>li[data-widget-Realid= ' + widgetid + ']:eq(0)').each(function (ninja) {

                var temparr = $.grep($scope.widgets, function (val) {
                    return val.WidgetRealid.toString().trim() == widgetid;
                });

                $(this).attr('data-class', 'Finished');
                var sizey = $(this).attr("data-sizey");// This is your sizey value
                var WizardID = $(this).attr("data-widget-id");
                var WizardRealId = $(this).attr("data-widget-Realid");
                var WizardTypeID = $(this).attr("data-widget-Typeid");
                var MatrixID = temparr[0].MatrixID;
                var DimensionID = $(this).attr("data-widget-DimensionID");
                var visualType = temparr[0].VisualType;
                var WizardNoOfItem = temparr[0].NoOfItem;
                var WizardNoOfYear = temparr[0].NoOfYear;
                var WizardNoOfMonth = temparr[0].NoOfMonth;

                $(this).find('.box-content-margin').each(function (k) {

                    if (WizardTypeID == 1) {

                        var template = '<activity-item type=\"NewsFeed\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                           $compile(template)($scope));
                    }
                    else if (WizardTypeID == 2) {
                        var template = '<activity-item type=\"Milestone\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                           $compile(template)($scope));
                    }
                    else if (WizardTypeID == 3) {
                        if (DimensionID == 1) {
                            var template = '<activity-item type=\"CostCenterFinancial\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                            $(this).html(
                               $compile(template)($scope));
                        }
                        else if (DimensionID == 2) {
                            var template = '<activity-item type=\"ActivityFinancial\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                            $(this).html(
                               $compile(template)($scope));
                        }
                        else if (DimensionID == 3) {
                            var template = '<activity-item type=\"Browser\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                            $(this).html(
                               $compile(template)($scope));
                        }
                        else if (DimensionID == 4) {
                            var template = '<activity-item type=\"BrowserVersion\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                            $(this).html(
                               $compile(template)($scope));
                        }
                        else if (DimensionID == 5) {
                            var template = '<activity-item type=\"OS\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                            $(this).html(
                               $compile(template)($scope));
                        }
                        else if (DimensionID == 6) {
                            var template = '<activity-item type=\"CountryName\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                            $(this).html(
                               $compile(template)($scope));
                        }

                    }
                    else if (WizardTypeID == 4) {
                        var template = '<activity-item type=\"Workspace\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" NoOfItem=\"' + WizardNoOfItem + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                           $compile(template)($scope));
                    }
                    else if (WizardTypeID == 5) {
                        var template = '<activity-item type=\"topxactivity\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" NoOfItem=\"' + WizardNoOfItem + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                           $compile(template)($scope));
                    }
                    else if (WizardTypeID == 6) {
                        var template = '<activity-item type=\"Notification\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                           $compile(template)($scope));
                    }
                    else if (WizardTypeID == 7) {
                        var template = '<activity-item type=\"MyTask\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" NoOfItem=\"' + WizardNoOfItem + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                          $compile(template)($scope));
                    }
                    else if (WizardTypeID == 8) {
                        var template = '<activity-item type=\"FundingRequest\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" NoOfItem=\"' + WizardNoOfItem + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                          $compile(template)($scope));
                    }
                    else if (WizardTypeID == 9) {
                        var template = '<activity-item type=\"PublishedAsset\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" NoOfItem=\"' + WizardNoOfItem + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                          $compile(template)($scope));
                    }
                    else if (WizardTypeID == 10) {
                        var template = '<activity-item type=\"DownloadedAsset\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" NoOfItem=\"' + WizardNoOfItem + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                          $compile(template)($scope));
                    }
                    else if (WizardTypeID == 11) {
                        var template = '<activity-item type=\"UploadAsset\" WizardRealID=\"' + WizardRealId + '\" WizardTypeID=\"' + WizardTypeID + '\" MatrixID=\"' + MatrixID + '\" DimensionID=\"' + DimensionID + '\" visualType=\"' + visualType + '\" NoOfYear=\"' + WizardNoOfYear + '\" NoOfMonth=\"' + WizardNoOfMonth + '\" WizardID=\"' + WizardID + '\" ></activity-item>';
                        $(this).html(
                          $compile(template)($scope));
                    }

                });
                $timeout(function () { checkcontent(); }, 500);
            });
        }

        function RefreshWidgets() {
            if ($scope.widgets.length === 1 && $scope.Iscompile == false) {
                $("#dynamicWidgets").html('');
                $scope.dyn_Cont = "<div class=\"row-fluid gridster\" widgets='widgets' onremove='removeWidget(widget)' onedit ='editWidget(widget)'> </div>";
                $("#dynamicWidgets").html(
                                          $compile($scope.dyn_Cont)($scope));
            }
            $timeout(function () { checkcontent(); }, 500);
        }

        // --- Define Scope Methods. ------------------------ //

        //var GetWidgetTypelist = $resource('common/GetWidgetTypesByID/:UserID/:IsAdmin/:TypeID', { UserID: parseInt($cookies['UserId'], 10), IsAdmin: IsforAdmin, TypeID: 0 });
        //var getwidgettypes = GetWidgetTypelist.query(function () {
        DashboardService.GetWidgetTypesByID(parseInt($cookies['UserId'], 10), IsforAdmin, 0).then(function (getwidgettypes) {
            $scope.listofWidgetTypes = getwidgettypes.Response;
            if (getwidgettypes.Response.length == 0) {
                $scope.wigetvar.addbtn = false;
            }
            else { $scope.wigetvar.addbtn = true; }
        });


        $scope.Newwidget = function () {
            $('#widgetModal').modal('show');
            $scope.templistofEntityOrCostChecked = [];
            $scope.listfilteredEntityOrCostvalues = [];
            RefreshCntrlModel();
            $scope.wigetvar.AddOrEdit = 'ADD';
            $scope.step = 0;
            $scope.showfilter = false;
            $timeout(function () { $('#Caption').focus(); }, 1000);
        };

        $scope.addWidget = function () {

            //var AddWidget = $resource('common/Widget/');
            //var addnewwidget = new AddWidget();
            var addnewwidget = {};
            addnewwidget.templateid = $scope.DashboardTemplateId;
            if ($scope.wigetvar.AddOrEdit == 'EDIT') {
                addnewwidget.widgetid = $scope.wigetvar.widgetid;
            }
            addnewwidget.caption = $scope.wigetvar.caption;
            addnewwidget.description = $scope.wigetvar.description;
            addnewwidget.widgettypeid = $scope.wigetvar.widgetType;
            addnewwidget.filterid = $scope.wigetvar.filterID;
            addnewwidget.attributeid = 0;
            addnewwidget.widgetQuery = "";
            addnewwidget.listofEntityID = $scope.wigetvar.listofEntityID;
            addnewwidget.listofSelectEntityID = $scope.wigetvar.listofSelectEntityID;

            if ($scope.wigetvar.widgetType == '4' || $scope.wigetvar.widgetType == "5" || $scope.wigetvar.widgetType == "7" || $scope.wigetvar.widgetType == '9') {
                addnewwidget.noofitem = $scope.wigetvar.noofitem;
            }
            else {
                addnewwidget.noofitem = 0;
            }
            if ($scope.wigetvar.widgetType == '11') {
                addnewwidget.noofYear = $scope.wigetvar.noofYear;
                addnewwidget.noofMonth = $scope.wigetvar.noofMonth;
            }
            else {
                addnewwidget.noofYear = 0;
                addnewwidget.noofMonth = 0;
            }
            if ($scope.wigetvar.dimensionid == undefined) {
                addnewwidget.dimensionid = "";
            }
            else {
                addnewwidget.dimensionid = $scope.wigetvar.dimensionid;
            }

            if ($scope.wigetvar.matrixid != "") {
                addnewwidget.matrixid = $scope.wigetvar.matrixid.join(",");
            }
            else {
                addnewwidget.matrixid = "";
            }
            addnewwidget.columnval = $scope.wigetvar.col;
            addnewwidget.rowval = $scope.wigetvar.row;
            addnewwidget.sizeXval = $scope.wigetvar.sizex;
            addnewwidget.sizeYval = $scope.wigetvar.sizey;

            if ($scope.ISTemplate == true) {
                addnewwidget.isadminpage = true;
                addnewwidget.isstatic = true;
            }
            else {
                addnewwidget.isadminpage = false;
                addnewwidget.isstatic = false;
            }

            if ($scope.wigetvar.visualtypeid == undefined) {
                addnewwidget.visualtypeid = "";
            }
            else {
                addnewwidget.visualtypeid = $scope.wigetvar.visualtypeid;
            }

            // var saveusercomment = AddWidget.save(addnewwidget, function () {
            DashboardService.Widget(addnewwidget).then(function (saveusercomment) {
                if (saveusercomment.StatusCode == 200) {
                    if ($scope.wigetvar.AddOrEdit == 'ADD') {
                        $scope.widgets.push({
                            title: addnewwidget.caption,
                            WidgetRealid: saveusercomment.Response.toString(),
                            Typeid: addnewwidget.widgettypeid,
                            MatrixID: addnewwidget.matrixid,
                            DimensionID: addnewwidget.dimensionid,
                            VisualType: addnewwidget.visualtypeid,
                            NoOfItem: addnewwidget.noofitem,
                            NoOfYear: addnewwidget.noofYear,
                            NoOfMonth: addnewwidget.noofMonth,
                            grid: { row: 1, col: 1, sizex: 2, sizey: 1 },
                        });

                        if ($scope.GlobalTemplateID == 0 || $scope.GlobalTemplateID == null) {
                            //var GetwidgetServ = $resource('common/GetWidgetDetailsByUserID/:UserID/:IsAdmin/:GlobalTemplateID', { UserID: parseInt($cookies['UserId'], 10), IsAdmin: false, GlobalTemplateID: 0 }, { get: { method: 'GET' } });
                            //var widgetListList = GetwidgetServ.get({ UserID: parseInt($cookies['UserId'], 10), IsAdmin: IsforAdmin, GlobalTemplateID: TemplateID }, function () {
                            DashboardService.GetWidgetDetailsByUserID(parseInt($cookies['UserId'], 10), IsforAdmin, TemplateID).then(function (widgetListList) {
                                $scope.getwidgetSource = widgetListList.Response;
                                if (widgetListList.Response != null || widgetListList.Response.length > 0) {
                                    $scope.GlobalTemplateID = $scope.getwidgetSource[0].TemplateID;
                                    if ($scope.GlobalTemplateID > 0) {
                                        $scope.DashboardTemplateId = $scope.GlobalTemplateID;
                                    }
                                }
                            });
                        }

                        RefreshWidgets();
                    }
                    else if ($scope.wigetvar.AddOrEdit == 'EDIT') {
                        $($scope.widgets).each(function () {
                            if (this.WidgetRealid.toString().trim() == saveusercomment.Response.toString()) {
                                this.title = addnewwidget.caption;
                                this.DimensionID = addnewwidget.dimensionid;
                                this.MatrixID = addnewwidget.matrixid;
                                this.Typeid = addnewwidget.widgettypeid;
                                this.VisualType = addnewwidget.visualtypeid;
                                this.NoOfItem = addnewwidget.noofitem;
                                this.NoOfYear = addnewwidget.noofYear;
                                this.NoOfMonth = addnewwidget.noofMonth;
                            }
                        });

                        ReloadContentInEdit(saveusercomment.Response.toString());
                    }
                    $scope.wigetvar.AddOrEdit = '';
                }
            });
            RefreshCntrlModel();
            $('#widgetModal').modal('hide');
        };

        function RefreshCntrlModel() {
            $scope.wigetvar.widgetid = '';
            $scope.wigetvar.caption = '';
            $scope.wigetvar.description = '';
            $scope.wigetvar.widgetType = 0;
            $scope.wigetvar.dimensionid = '';
            $scope.wigetvar.noofitem = '';
            $scope.wigetvar.matrixid = 0;
            $scope.wigetvar.visualtypeid = '';
            $scope.wigetvar.col = 0;
            $scope.wigetvar.row = 0;
            $scope.wigetvar.sizex = 0;
            $scope.wigetvar.sizey = 0;
            $scope.wigetvar.showcntrlforDynamic = false;
            $scope.wigetvar.NoOfItems = false;
            $scope.wigetvar.widgetTypereadonly = false;
            $scope.wigetvar.wigetdimensionreadonly = false;
            $scope.wigetvar.filterID = 0;
            $scope.wigetvar.listofEntityID = '';
            $scope.wigetvar.listofSelectEntityID = '';
            $scope.wigetvar.showstaticcntrlforDynamic = false;
            $scope.wigetvar.showcntrlforyear = false;
            $scope.wigetvar.noofYear = '';
            $scope.wigetvar.noofMonth = '';
        }

        $scope.removeWidget = function (widget) {
            var IsAdminPage;
            if ($scope.ISTemplate == true) {
                // var DeleteWidgetByID = $resource('common/DeleteWidget/:TemplateID/:WidgetID/:IsAdminPage', { TemplateID: $scope.DashboardTemplateId, WidgetID: widget.WidgetRealid.toString().trim(), IsAdminPage: true }, { delete: { method: 'DELETE' } });
                IsAdminPage = true;
            }
            else {
                // var DeleteWidgetByID = $resource('common/DeleteWidget/:TemplateID/:WidgetID/:IsAdminPage', { TemplateID: $scope.DashboardTemplateId, WidgetID: widget.WidgetRealid.toString().trim(), IsAdminPage: false }, { delete: { method: 'DELETE' } });
                IsAdminPage = false;
            }

            // var deletewidgetid = DeleteWidgetByID.delete(function () {         
            DashboardService.DeleteWidget($scope.DashboardTemplateId, widget.WidgetRealid.toString().trim(), IsAdminPage).then(function () {

                $scope.widgets = $.grep($scope.widgets, function (val) {
                    return val.WidgetRealid.toString().trim() != widget.WidgetRealid.toString().trim();
                });
                $scope.Iscompile = true;
                NotifySuccess($translate.instant('LanguageContents.Res_4906.Caption'));
            });
        };

        $scope.editWidget = function (widget) {
            $scope.step = 0;
            $scope.templistofEntityOrCostChecked = [];
            $scope.listfilteredEntityOrCostvalues = [];
            var IsAdminPage;
            if ($scope.ISTemplate == true) {
                // var GetWidgetDetail = $resource('common/GetWidgetByID/:WidgetID/:TemplateID/:IsAdminPage', { WidgetID: widget.WidgetRealid.toString().trim(), TemplateID: $scope.DashboardTemplateId, IsAdminPage: true });
                IsAdminPage = true;

            }
            else {
                //var GetWidgetDetail = $resource('common/GetWidgetByID/:WidgetID/:TemplateID/:IsAdminPage', { WidgetID: widget.WidgetRealid.toString().trim(), TemplateID: $scope.DashboardTemplateId, IsAdminPage: false });
                IsAdminPage = false;
            }
            //var getwidgetlist = GetWidgetDetail.get(function () {
            //    DashboardService.GetWidgetByID(widget.WidgetRealid.toString().trim(), $scope.DashboardTemplateId, IsAdminPage).then(function (getwidgetlist) {
            DashboardService.GetWidgetByID((widget.WidgetRealid.toString().trim()) == "" ? '0' : widget.WidgetRealid.toString().trim(), $scope.DashboardTemplateId, IsAdminPage).then(function (getwidgetlist) {
                if (getwidgetlist.Response != null) {
                    if (getwidgetlist.Response.length > 0) {
                        $scope.listdata = getwidgetlist.Response;

                        $scope.wigetvar.widgetid = widget.WidgetRealid.toString().trim();
                        $scope.wigetvar.caption = $scope.listdata[0].Caption.trim();
                        $scope.wigetvar.description = $scope.listdata[0].Description.trim();
                        $scope.wigetvar.widgetType = $scope.listdata[0].WidgetTypeID;
                        $scope.wigetvar.dimensionid = $scope.listdata[0].DimensionID;
                        $scope.wigetvar.matrixid = $scope.listdata[0].MatrixID.trim().split(",");
                        $scope.wigetvar.visualtypeid = $scope.listdata[0].VisualType;
                        $scope.wigetvar.noofitem = $scope.listdata[0].NoOfItem;
                        $scope.wigetvar.col = widget.grid.col;
                        $scope.wigetvar.row = widget.grid.row;
                        $scope.wigetvar.sizex = widget.grid.sizex;
                        $scope.wigetvar.sizey = widget.grid.sizey;
                        $scope.wigetvar.filterID = $scope.listdata[0].FilterID;
                        $scope.wigetvar.listofEntityID = $scope.listdata[0].ListOfEntityIDs;
                        $scope.wigetvar.listofSelectEntityID = $scope.listdata[0].ListofSelectEntityIDs;
                        $scope.wigetvar.noofYear = $scope.listdata[0].NoOfYear;
                        $scope.wigetvar.noofMonth = $scope.listdata[0].NoOfMonth;

                        $scope.GetDimensionforTypeId();
                        $scope.wigetvar.wigetdimensionreadonly = true;
                        $scope.wigetvar.widgetTypereadonly = true;
                        $scope.wigetvar.AddOrEdit = 'EDIT';
                        $scope.wigetvar.showstaticcntrlforDynamic = false;
                        if ($scope.listdata[0].DimensionID == 1 || $scope.listdata[0].DimensionID == 2) {
                            $scope.showfilter = false;
                        }
                        if ($scope.wigetvar.widgetType == 11) {
                            $scope.wigetvar.showcntrlforyear = true;
                        }
                        else { $scope.wigetvar.showcntrlforyear = false; }
                    }
                }
            });

            $('#widgetModal').modal();
        };

        $scope.GetDimensionforTypeId = function () {
            //var GetDimensionlist = $resource('common/GetWidgetTypeDimensionByID/:WidgetTypeID', { WidgetTypeID: $scope.wigetvar.widgetType });
            //var getdimension = GetDimensionlist.query(function () {
            if ($scope.wigetvar.widgetType == undefined) return false;
            DashboardService.GetWidgetTypeDimensionByID($scope.wigetvar.widgetType).then(function (getdimension) {
                if (getdimension.Response != null) {
                    $scope.listofdimensiontypes = getdimension.Response;
                    if ($scope.wigetvar.widgetType == "1" || $scope.wigetvar.widgetType == "2" || $scope.wigetvar.widgetType == "6" || $scope.wigetvar.widgetType == "7" || $scope.wigetvar.widgetType == "8" || $scope.wigetvar.widgetType == "10") {
                        // var GetDimensionlist = $resource('common/GetWidgetTypeDimensionByID/:WidgetTypeID', { WidgetTypeID: $scope.wigetvar.widgetType });


                        $scope.wigetvar.showcntrlforDynamic = false;
                        $scope.wigetvar.NoOfItems = false;
                        $scope.wigetvar.showcntrlforyear = false;
                    }
                    else if ($scope.wigetvar.widgetType == "3") {
                        $scope.wigetvar.showcntrlforDynamic = true;
                        $scope.wigetvar.NoOfItems = false;
                        $scope.wigetvar.showcntrlforyear = false;
                    }
                    else if ($scope.wigetvar.widgetType == "4" || $scope.wigetvar.widgetType == "5" || $scope.wigetvar.widgetType == "9") {
                        $scope.wigetvar.showcntrlforDynamic = false;
                        $scope.wigetvar.NoOfItems = true;
                        $scope.wigetvar.showcntrlforyear = false;
                    }
                    else if ($scope.wigetvar.widgetType == "11") {
                        $scope.wigetvar.showcntrlforDynamic = false;
                        $scope.wigetvar.NoOfItems = false;
                        $scope.wigetvar.showcntrlforyear = true;
                    }
                }
            });
        };

        $scope.GetMatrixfordimensionId = function () {
            if ($scope.wigetvar.dimensionid != undefined) {
                $scope.MatrixDataForDimension = jQuery.grep($scope.MatrixData, function (a) {
                    return a.dimensionid == $scope.wigetvar.dimensionid;
                });

                if ($scope.wigetvar.dimensionid == "1" || $scope.wigetvar.dimensionid == "2") {
                    $scope.wigetvar.showstaticcntrlforDynamic = false;
                }
                else {
                    $scope.wigetvar.showstaticcntrlforDynamic = true;
                }
            }
            else {
                $scope.wigetvar.showstaticcntrlforDynamic = false;
            }
        };

        function ControlReset() {
        }

        // ------------------> LOAD DEFAULT FILTER DATA IN PAGE LOAD <----------------
        function LoadDashboardDefaultActivityData() {
            $scope.IsDuplicate = false;
            var GlobalChildNode = new Array();
            if ($scope.FilterType == undefined)
                $scope.FilterType = 1;

            //var NodeList = $resource('metadata/ActivityRootLevel/:StartRowNo/:MaxNoofRow/:FilterID/:SortOrderColumn/:IsDesc/:IncludeChildren/:EntityID/:UserID/:Level/:IsDuplicate', { StartRowNo: 0, MaxNoofRow: 9999902, FilterID: 0, SortOrderColumn: "null", IsDesc: false, IncludeChildren: false, EntityID: '0', UserID: parseInt($cookies['UserId'], 10), Level: 0, IsDuplicate: $scope.IsDuplicate });

            //var Node = new NodeList();
            var Node = {};
            Node.StartRowNo = 0;
            Node.MaxNoofRow = 9999902;
            Node.FilterID = 0;
            Node.SortOrderColumn = "null";
            Node.IsDesc = false;
            Node.IncludeChildren = false;
            Node.EntityID = '0';
            Node.UserID = parseInt($cookies['UserId'], 10);
            Node.Level = 0;
            Node.IsDuplicate = $scope.IsDuplicate;
            Node.IDArr = [];
            //var getactivitylist = NodeList.save(Node, function () {
            DashboardService.ActivityRootLevel(Node).then(function (getactivitylist) {
                if (getactivitylist.Response.Data != null) {
                    var listContent = getactivitylist.Response.Data;
                    for (var j = 0; j < listContent.length; j++) {
                        GlobalChildNode.push(listContent[j]["Id"])
                    }

                    //var NodeEntityList = $resource('metadata/ActivityDetail/:StartRowNo/:MaxNoofRow/:FilterID/:SortOrderColumn/:IsDesc/:IncludeChildren/:EntityID/:Level/:ExpandingEntityID/:IsSingleID', { StartRowNo: 0, MaxNoofRow: 9999902, FilterID: 0, SortOrderColumn: "null", IsDesc: false, IncludeChildren: true, EntityID: '0', Level: 0, ExpandingEntityID: 0, IsSingleID: false });
                    //var NodeEnty = new NodeEntityList();
                    var NodeEnty = {};
                    NodeEnty.StartRowNo = 0;
                    NodeEnty.MaxNoofRow = 9999902;
                    NodeEnty.FilterID = 0;
                    NodeEnty.SortOrderColumn = "null";
                    NodeEnty.IsDesc = false;
                    NodeEnty.IncludeChildren = true;
                    NodeEnty.EntityID = '0';
                    NodeEnty.Level = 0;
                    NodeEnty.ExpandingEntityID = 0;
                    NodeEnty.IsSingleID = false;
                    NodeEnty.IDArr = GlobalChildNode;
                    NodeEnty.FilterType = $scope.FilterType;
                    // var GetActivityDetailList = NodeEntityList.save(NodeEnty, function () {
                    DashboardService.ActivityDetail(NodeEnty).then(function (GetActivityDetailList) {
                        if (GetActivityDetailList.Response.Data != null) {
                            var ListofEntity = GetActivityDetailList.Response.Data;

                            for (var i = 0 ; i < ListofEntity.length; i++) {
                                var resultfileredvalues = {};
                                resultfileredvalues["EntityId"] = ListofEntity[i].Id;
                                resultfileredvalues["EntityName"] = ListofEntity[i].Name;
                                resultfileredvalues["ShortDescription"] = ListofEntity[i].ShortDescription;

                                resultfileredvalues["Checked"] = false;
                                resultfileredvalues["ColorCode"] = ListofEntity[i].ColorCode;
                                $scope.listfilteredEntityOrCostvalues.push(resultfileredvalues);
                            }
                        }
                    });

                }
            });
        }

        function LoadDashboardDefaultCostCenterData() {
            // var NodeList = $resource('metadata/CostCentreRootLevel/:StartRowNo/:MaxNoofRow/:FilterID/:SortOrderColumn/:IsDesc/:IncludeChildren/:EntityID/:UserID/:Level/', { StartRowNo: 0, MaxNoofRow: 9999902, FilterID: 0, SortOrderColumn: "null", IsDesc: false, IncludeChildren: false, EntityID: '0', UserID: parseInt($cookies['UserId'], 10), Level: 0 });
            //var Node = new NodeList();
            var Node = {};
            Node.StartRowNo = 0;
            Node.MaxNoofRow = 9999902;
            Node.FilterID = 0;
            Node.SortOrderColumn = "null";
            Node.IsDesc = false;
            Node.IncludeChildren = false;
            Node.EntityID = '0';
            Node.UserID = parseInt($cookies['UserId'], 10);
            Node.Level = 0;
            Node.IDArr = [];

            // var getactivitylist = NodeList.save(Node, function () {
            DashboardService.CostCentreRootLevel(Node).then(function (getactivitylist) {
                if (getactivitylist.Response.Data != null) {
                    $scope.listfilteredEntityOrCostvalues = [];
                    var SelectedEntitys = $scope.wigetvar.listofEntityID.toString().trim().split(",");
                    for (var i = 0 ; i < getactivitylist.Response.Data.length; i++) {
                        var resultfileredvalues = {};
                        if ($scope.wigetvar.AddOrEdit == "EDIT") {
                            if ($.inArray(getactivitylist.Response.Data[i].Id.toString(), SelectedEntitys) != -1)
                                resultfileredvalues["Checked"] = true;
                        }
                        else
                            resultfileredvalues["Checked"] = false;

                        resultfileredvalues["EntityId"] = getactivitylist.Response.Data[i].Id;
                        resultfileredvalues["EntityName"] = getactivitylist.Response.Data[i].Name;
                        resultfileredvalues["ShortDescription"] = getactivitylist.Response.Data[i].ShortDescription;

                        resultfileredvalues["ColorCode"] = getactivitylist.Response.Data[i].ColorCode;
                        $scope.listfilteredEntityOrCostvalues.push(resultfileredvalues);
                    }
                }
            });
        }

        //-----------------> ENDS DEFAULT FILTER <-------------------------------------

        //---------------> Filter part starts here <----------------------------

        $scope.LoadDashboardFilter = function () {
            $('#FilterTaskModal').modal('show');
            $scope.EntityTypeID = 0;
            $scope.appliedfilter = "No filter applied";
            $scope.FilterFields = {};
            var elementNode = 'DetailFilter';

            $scope.OptionObj = {};

            $scope.fieldoptions = [];
            $scope.setoptions = function () {
                var keys = [];

                angular.forEach($scope.OptionObj, function (key) {
                    keys.push(key);
                    $scope.fieldoptions = keys;
                });
            }

            var selectedfilterid = 0;

            var TypeID = 6;
            $scope.visible = false;

            $scope.dynamicEntityValuesHolder = {

            };
            $scope.dyn_Cont = '';
            $scope.fieldKeys = [];

            $scope.options = {};

            $scope.setFieldKeys = function () {
                var keys = [];

                angular.forEach($scope.FilterFields, function (key) {
                    keys.push(key);
                    $scope.fieldKeys = keys;
                });
            }
            $scope.filterValues = [];
            $scope.atributesRelationList = [];
            $scope.filterSettingValues = [];

            var TypeID = 6;
            if ($scope.wigetvar.dimensionid == "1") {
                TypeID = 5
            }
            else if ($scope.wigetvar.dimensionid == "2") {
                TypeID = 6
            }

            $scope.visible = false;

            $scope.dynamicEntityValuesHolder = {};
            $scope.dyn_Cont = '';
            $scope.fieldKeys = [];

            $scope.options = {};

            $scope.setFieldKeys = function () {
                var keys = [];

                angular.forEach($scope.FilterFields, function (key) {
                    keys.push(key);
                    $scope.fieldKeys = keys;
                });

            }
            $scope.filterValues = [];
            $scope.atributesRelationList = [];
            $scope.filterSettingValues = [];

            //Getting All Filters for while loading time of All Filters
            //var GettingFilters = $resource('planning/GetFilterSettingsForDetail/:TypeID', { TypeID: TypeID }, { get: { method: 'GET' } });
            //var filterSettings = GettingFilters.get(function () {
            DashboardService.GetFilterSettingsForDetail(TypeID).then(function (filterSettings) {
                $scope.filterValues = filterSettings.Response;
            });

            //Loading Attributes and options based on Admin settings => ActivityRoot Level =>isFilter = true 

            $scope.FilterSettingsLoad = function () {
                $scope.attributegroupTypeid = 0;
                $scope.EntityHierarchyTypesResult = [];
                $scope.treesrcdirec = {};
                $scope.treePreviewObj = {};
                if ($window.ActivityFilterName == '') {
                    $window.ActivityFilterName = '';
                }
                $scope.dyn_Cont = '';
                $scope.Updatefilter = false;
                $scope.Applyfilter = true;
                $scope.Deletefilter = false;
                $scope.Savefilter = false;
                $("#dynamic_Controls").html('');
                $scope.atributesRelationList = [];
                $scope.atributesRelationListWithTree = [];
                $scope.ngsaveasfilter = '';
                $scope.ngKeywordtext = '';
                $scope.atributeGroupList = [];

                var OptionFrom = 0;
                var IsKeyword = "false";
                var IsEntityType = "false";
                var CurrentActiveVersion = 0;
                $scope.PeriodOptions = [{ Id: 1, FilterPeriod: 'Between' }, { Id: 2, FilterPeriod: 'Within' }];
                var KeywordOptionsresponse;
                //var KeywordOptions = $resource('metadata/GetOptionsFromXML/:elementNode/:typeid', { elementNode: elementNode, typeid: TypeID }, { get: { method: 'GET' } });
                //var KeywordOptionsResult = KeywordOptions.get(function () {
                DashboardService.GetOptionsFromXML(elementNode, TypeID).then(function (KeywordOptionsResult) {
                    KeywordOptionsresponse = KeywordOptionsResult.Response;
                    OptionFrom = KeywordOptionsresponse.split(',')[0];
                    IsKeyword = KeywordOptionsresponse.split(',')[1];
                    IsEntityType = KeywordOptionsresponse.split(',')[3];
                    CurrentActiveVersion = KeywordOptionsresponse.split(',')[2];
                    //var GetParentEntityId = $resource('metadata/GettingEntityTypeHierarchyForAdminTree/:EntityTypeID/:ModuleID', { EntityTypeID: 6, ModuleID: 3 }, { get: { method: 'GET' } });
                    //var GerParentEntityData = GetParentEntityId.get({ EntityTypeID: 6 }, function () {
                    DashboardService.GettingEntityTypeHierarchyForAdminTree(6, 3).then(function (GerParentEntityData) {
                        $scope.entitytpesdata = GerParentEntityData.Response;
                        $scope.tagAllOptions.data = [];
                        if (GerParentEntityData.Response != null) {
                            $.each(GerParentEntityData.Response, function (i, el) {
                                $scope.tagAllOptions.data.push({
                                    "id": el.Id,
                                    "text": el.Caption,
                                    "ShortDescription": el.ShortDescription,
                                    "ColorCode": el.ColorCode
                                });
                            });
                        }
                        $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                        //var resourceEntityTypeAttributeRelationByID = $resource('metadata/GettingFilterAttribute/:TypeID/:FilterType/:OptionFrom', { TypeID: TypeID, FilterType: 'DetailFilter', OptionFrom: OptionFrom }, { get: { method: 'GET' } });
                        //var filterattids = new resourceEntityTypeAttributeRelationByID();
                        var filterattids = {};
                        filterattids.IDList = $.map($scope.listfilteredEntityOrCostvalues, function (e) { return e.EntityId });
                        //  var entityAttributesRelation = resourceEntityTypeAttributeRelationByID.save(filterattids, function () {
                        filterattids.TypeID = TypeID;
                        filterattids.FilterType = 'DetailFilter';
                        filterattids.OptionFrom = OptionFrom;
                        DashboardService.GettingFilterAttribute(filterattids).then(function (entityAttributesRelation) {
                            //var entityAttributesRelation = resourceEntityTypeAttributeRelationByID.get(function () {
                            $scope.atributesRelationList = entityAttributesRelation.Response;

                            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                                if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                                    $scope.dyn_Cont += '<div class="control-group"><span>' + $scope.atributesRelationList[i].DisplayName + ' : </span>';

                                    $scope.dyn_Cont += '<div class="controls"><select class=\"multiselect\" multiple="multiple" multiselect-dropdown data-placeholder="Select ' + $scope.atributesRelationList[i].DisplayName + ' options" ng-model="FilterFields.DropDown_' + $scope.atributesRelationList[i].AttributeId + '_' + $scope.atributesRelationList[i].TreeLevel + '"\ ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList[' + i + '].LevelTreeNodes \">';
                                    //$scope.dyn_Cont += '<option ng-repeat="opt in atributesRelationList[' + i + '].LevelTreeNodes" value="{{opt.Id}}">{{opt.Caption}}</option>';
                                    $scope.dyn_Cont += '</select></div></div>';

                                    $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] = [];
                                }
                                else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                                    $scope.dyn_Cont += '<div class="control-group"><span>' + $scope.atributesRelationList[i].DisplayName + ' : </span>';

                                    $scope.dyn_Cont += '<div class="controls"><select class=\"multiselect\" multiple="multiple" multiselect-dropdown data-placeholder="Select ' + $scope.atributesRelationList[i].DisplayName + ' options" ng-model="FilterFields.DropDown_' + $scope.atributesRelationList[i].AttributeId + '_' + $scope.atributesRelationList[i].TreeLevel + '"\ ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList[' + i + '].LevelTreeNodes \">';
                                    //$scope.dyn_Cont += '<option ng-repeat="opt in atributesRelationList[' + i + '].LevelTreeNodes" value="{{opt.Id}}">{{opt.Caption}}</option>';
                                    $scope.dyn_Cont += '</select></div></div>';

                                    $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] = [];
                                }
                                else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                                    var treecount12 = $.grep($scope.FilterDataXML, function (e) { return e.AttributeId == $scope.atributesRelationList[i].AttributeId });
                                    if (treecount12.length == 0) {
                                        var mm = $scope.atributesRelationList[i].AttributeId;
                                        $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeId.toString() + ""] = false;
                                        $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeId.toString() + ""] = $scope.atributesRelationList[i].DropdowntreePricingAttr;
                                        $scope.dyn_Cont += "<div drowdowntreepercentagemultiselectionfilter  data-purpose='entity' data-attributeid=" + $scope.atributesRelationList[i].AttributeId.toString() + "></div>";
                                        $scope.FilterDataXML.push({ "AttributeId": parseInt($scope.atributesRelationList[i].AttributeId) });
                                    }
                                }
                                else if ($scope.atributesRelationList[i].AttributeTypeId == 3) {
                                    if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                                        $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as (ndata.FirstName+' '+ndata.LastName)  for ndata in  atributesRelationList[" + i + "].Users \"></select></div>";
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                    }
                                    else if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.EntityStatus) {
                                        var EntityStatusOptions = $scope.atributesRelationList[i].EntityStatusOptionValues
                                        $scope.tagAllOptionsEntityStatus.data = [];
                                        if (EntityStatusOptions != null) {
                                            $.each(EntityStatusOptions, function (i, el) {
                                                $scope.tagAllOptionsEntityStatus.data.push({
                                                    "id": el.ID,
                                                    "text": el.StatusOptions,
                                                    "ShortDescription": el.ShortDesc,
                                                    "ColorCode": el.ColorCode
                                                });
                                            });
                                        }

                                        $scope.dyn_Cont += "<div class='control-group'>";
                                        $scope.dyn_Cont += "<span>" + $scope.atributesRelationList[i].DisplayName + " : </span><div class=\"controls\">";
                                        $scope.dyn_Cont += "<input class=\"width2x\" id='ddlEntityStatus' placeholder='Select Entity Status Options' type='hidden' ui-select2='tagAllOptionsEntityStatus' ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\" />";
                                        $scope.dyn_Cont += "</div></div>";
                                    }
                                    else if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.EntityOnTimeStatus) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><span>' + $scope.atributesRelationList[i].DisplayName + ' : </span>';
                                        $scope.dyn_Cont += "<div class=\"controls\"><select class=\"multiselect\" multiselect-dropdown  multiple=\"multiple\" ";
                                        $scope.dyn_Cont += "data-placeholder=\"Select Entity OnTime Status\" ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\" ng-options=\"opt.Id as opt.Name for opt in  OntimeStatusLists \"";
                                        $scope.dyn_Cont += "id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\">";
                                        //$scope.dyn_Cont += '<option ng-repeat="opt in OntimeStatusLists" value="{{opt.Id}}">{{opt.Name}}</option>';
                                        $scope.dyn_Cont += "</select>";
                                        $scope.dyn_Cont += "</div></div>";
                                    }
                                    else {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><span>' + $scope.atributesRelationList[i].DisplayName + ' : </span>';
                                        $scope.dyn_Cont += '<div class=\"controls\"> <select class=\"multiselect\" multiple="multiple" multiselect-dropdown ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList[' + i + '].OptionValues \" data-placeholder="Select ' + $scope.atributesRelationList[i].DisplayName + ' options" ng-model=\"FilterFields.DropDown_' + $scope.atributesRelationList[i].AttributeId + '"\">';
                                        //$scope.dyn_Cont += '<option ng-repeat="opt in atributesRelationList[' + i + '].OptionValues" value="{{opt.Id}}">{{opt.Caption}}</option>';
                                        $scope.dyn_Cont += '</select></div></div>';
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                    }
                                }
                                else if ($scope.atributesRelationList[i].AttributeTypeId == 4) {
                                    $scope.dyn_Cont += "<div class=\"control-group\"><span> " + $scope.atributesRelationList[i].DisplayName + " : </span>";
                                    $scope.dyn_Cont += "<div class=\"controls\"><select class=\"multiselect\" multiselect-dropdown  multiple=\"multiple\"";
                                    $scope.dyn_Cont += "data-placeholder=\"Select " + $scope.atributesRelationList[i].DisplayName + "\" ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList[" + i + "].OptionValues \"";
                                    $scope.dyn_Cont += "ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"";
                                    $scope.dyn_Cont += "id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\">";
                                    //$scope.dyn_Cont += '<option ng-repeat="opt in atributesRelationList[' + i + '].OptionValues" value="{{opt.Id}}">{{opt.Caption}}</option>';
                                    $scope.dyn_Cont += "</select>";
                                    $scope.dyn_Cont += "</div></div>";
                                    $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                }
                                else if ($scope.atributesRelationList[i].AttributeTypeId == 10) {
                                    $scope.setoptions();
                                    $scope.items = [];
                                    $scope.items.push({ startDate: '', endDate: '' });
                                    $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + " : </span><div class=\"controls\"><div class=\"period nomargin\" id=\"periodcontrols\"  ng-form=\"subForm\">";
                                    $scope.dyn_Cont += "<div class=\"row-fluid\">";
                                    $scope.dyn_Cont += "<input class=\"sdate\" data-date-format='" + $scope.DefaultSettings.DateFormat + "' id=\"items.startDate\"type=\"text\" value=\"\" name=\"startDate\" ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + "items.startDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open" + "items.startDate" + "\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  ng-model=\"items.startDate\" placeholder=\"-- Start date --\" ng-change=\"OpenOptionsForPeriod()\"/><input class=\"edate\" type=\"text\" data-date-format='" + $scope.DefaultSettings.DateFormat + "' ng-click=\"Calanderopen($event," + $scope.fields["DatePart_Calander_Open" + "items.endDate"] + ")\"  datepicker-popup=\"{{format}}\"  is-open=\"DatePart_Calander_Open" + "items.endDate" + "\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" value=\"\" name=\"enddate\" id=\"items.endDate\" ng-model=\"items.endDate\" placeholder=\"-- End date --\"  ng-change=\"OpenOptionsForPeriod()\"/>";
                                    $scope.dyn_Cont += "";
                                    $scope.dyn_Cont += "</div></div></div></div>";
                                    $scope.FilterFields["Period_" + $scope.atributesRelationList[i].AttributeId] = [];
                                    $scope.setFieldKeys();
                                    $scope.dyn_Cont += "<div ng-show=\"ShowPeriodOptions\" class=\"control-group\" id=\"ShowPeriodOptions\" data-attrid=\"" + $scope.atributesRelationList[i].AttributeId + "\"><span>Period range : </span><div class=\"controls\">";
                                    $scope.dyn_Cont += "<select class=\"nomargin\" ui-select2 ng-model=\"PeriodOptionValue\"><option ng-repeat=\"ndata in PeriodOptions\" value=\"{{ndata.Id}}\">{{ndata.FilterPeriod}}</option></select></div></div>";
                                    $scope.ShowPeriodOptions = false
                                    $scope.fields["DatePart_Calander_Open" + "items.startDate"] = false;
                                    $scope.fields["DatePart_Calander_Open" + "items.endDate"] = false;
                                }

                            }
                            if ($scope.atributesRelationList.length > 0)
                                $scope.atributeGroupList = $.grep($scope.atributesRelationList, function (e) { return e.AttributeTypeId == 3 || e.AttributeTypeId == 4 || e.AttributeTypeId == 6 || e.AttributeTypeId == 7 || e.AttributeTypeId == 12 });

                            if (IsEntityType == "True") {
                                $scope.dyn_Cont += "<div class='control-group'>";
                                $scope.dyn_Cont += "<span>EntityType : </span><div class=\"controls\"> ";
                                $scope.dyn_Cont += "<input class='width2x' id='ddlChildren' placeholder='Select EntityType Options' type='hidden' ui-select2='tagAllOptions' ng-model='ddlParententitytypeId' />";
                                $scope.dyn_Cont += "</div></div>";
                            }
                            $scope.atributesRelationListWithTree = $.grep(entityAttributesRelation.Response, function (e) { return e.AttributeTypeId == 7 })
                            for (var i = 0; i < $scope.atributesRelationListWithTree.length; i++) {
                                if ($scope.atributesRelationListWithTree[i].AttributeTypeId == 7) {
                                    $scope.treePreviewObj = {};
                                    $scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = JSON.parse($scope.atributesRelationListWithTree[i].tree).Children;
                                    if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId].length > 0) {
                                        if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId])) {
                                            $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = true;
                                        }
                                        else
                                            $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = false;
                                    }
                                    else {
                                        $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = false;
                                    }
                                    $scope.dyn_Cont += '<div class="control-group treeNode-control-group">';
                                    $scope.dyn_Cont += '<span>' + $scope.atributesRelationListWithTree[i].DisplayName + '</span>';
                                    $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                                    $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationListWithTree[i].AttributeId + '" placeholder="Search" treecontext="treeNodeSearchDropdowns_Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '"></div>';
                                    $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdowns_Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '">';
                                    $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                                    $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationListWithTree[i].AttributeId + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                                    $scope.dyn_Cont += '</div>';
                                    $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\">';
                                    $scope.dyn_Cont += '<div class="controls">';
                                    $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\" node-attributeid="' + $scope.atributesRelationListWithTree[i].AttributeId + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                    $scope.dyn_Cont += '</div></div></div></div>';
                                }
                            }
                            $("#dynamic_Controls").html(
                                         $compile($scope.dyn_Cont)($scope));
                            $scope.Applyfilter = true;
                            $scope.Updatefilter = false;
                            $scope.Deletefilter = false;
                            $scope.Savefilter = true;
                        });
                    });
                });
            }

            $timeout(function () {
                $scope.FilterSettingsLoad();
            }, 200);

            //Saving Filter settings and Selected Attributes
            $scope.FilterSave = function () {
                $("#activitydetailfilter").attr('disabled', 'disabled');

                //var InsertFilterSettings = $resource('planning/InsertFilterSettings');
                //var FilterData = new InsertFilterSettings();
                var FilterData = {};

                $scope.filterSettingValues.Keyword = '';
                $scope.filterSettingValues.FilterName = '';
                var whereConditionData = [];
                var multiSelectVal = [];
                var usersVal = [];
                var orgLevel = [];
                var fiscalyear = [];
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            for (var k = 0 ; k < orgLevel.length; k++) {
                                whereConditionData.push({ 'AttributeID': $scope.atributesRelationList[i].AttributeId, 'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k], 'Level': $scope.atributesRelationList[i].TreeLevel, 'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId });
                            }
                        }
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeId == 3 && $scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                            usersVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                            for (var k = 0 ; k < usersVal.length; k++) {
                                whereConditionData.push({ 'AttributeID': $scope.atributesRelationList[i].AttributeId, 'SelectedValue': usersVal[k], 'Level': 0, 'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId });
                            }
                        }
                    }
                    else if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                            fiscalyear = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId]
                            for (var k = 0 ; k < fiscalyear.length; k++) {
                                if (fiscalyear[k].Id != undefined) {
                                    whereConditionData.push({ 'AttributeID': $scope.atributesRelationList[i].AttributeId, 'SelectedValue': fiscalyear[k].Id, 'Level': 0, 'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId });
                                }
                                else {
                                    whereConditionData.push({ 'AttributeID': $scope.atributesRelationList[i].AttributeId, 'SelectedValue': fiscalyear[k], 'Level': 0, 'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId });
                                }
                            }
                        }
                    }
                }

                var entitytypeIdforFilter = '';
                if (TypeID != 5) {
                    for (var l = 0; l < $scope.FilterFields["DropDown_EntityType"].length; l++) {
                        if (entitytypeIdforFilter == '') {
                            entitytypeIdforFilter = $scope.FilterFields["DropDown_EntityType"][l];
                        }
                        else {
                            entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.FilterFields["DropDown_EntityType"][l];
                        }
                    }
                }
                else if (TypeID == 5) { entitytypeIdforFilter = 5; }
                FilterData.FilterId = 0;
                FilterData.FilterName = $scope.ngsaveasfilter;
                FilterData.Keyword = $scope.ngKeywordtext;
                FilterData.UserId = 1;
                FilterData.TypeID = TypeID;
                FilterData.entityTypeId = entitytypeIdforFilter;
                if (TypeID == 5) { FilterData.IsDetailFilter = 0; } else { FilterData.IsDetailFilter = 1; }
                if ($scope.items[0] != undefined) {
                    if ($scope.items[0].startDate == "") {
                        FilterData.StarDate = '';
                    }
                    else {
                        FilterData.StarDate = $scope.items[0].startDate;
                    }
                    if ($scope.items[0].endDate == "") {
                        FilterData.EndDate = '';
                    }
                    else {
                        FilterData.EndDate = $scope.items[0].endDate;
                    }
                }
                else {
                    FilterData.StarDate = '';
                    FilterData.EndDate = '';
                }

                FilterData.WhereConditon = [];
                FilterData.WhereConditon = whereConditionData;

                // var filterSettingsInsertresult = InsertFilterSettings.save(FilterData, function () {
                DashboardServices.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                    if (filterSettingsInsertresult.StatusCode == 200) {
                        $scope.wigetvar.filterID = filterSettingsInsertresult.Response;
                        $scope.LoadActivityFilter(filterSettingsInsertresult.Response);
                    }
                });


                var multiSelectVal = [];
                var usersVal = [];
                var orgLevel = [];
            };

            //Loading selected Attributes and Options based FilterID and TypeID
            $scope.LoadFilterSettingsByFilterID = function (filterId, typeId) {
                $scope.Updatefilter = false;
                $scope.Applyfilter = false;
                $scope.Deletefilter = false;
                $scope.Savefilter = false;
                $scope.filterSettingValues = [];
                selectedfilterid = filterId;
                //var GettingFilterValuesByFilterID = $resource('planning/GetFilterSettingValuesByFilertId/:FilterID', { FilterID: filterId }, { get: { method: 'GET' } });
                //var filterSettingsValues = GettingFilterValuesByFilterID.get(function () {
                DashboardService.GetFilterSettingValuesByFilertId(filterId).then(function (filterSettingsValues) {
                    $scope.filterSettingValues = filterSettingsValues.Response;
                    for (var i = 0 ; i < $scope.filterSettingValues.FilterValues.length ; i++) {
                        if ($scope.filterSettingValues.FilterValues[i].Level != 0 && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 6) {
                            $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);
                        }

                        else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] != undefined)
                                if ($scope.OptionObj["option_" + $scope.filterSettingValues.FilterValues[i].AttributeId] != undefined) {
                                    $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = ($.grep($scope.OptionObj["option_" + $scope.filterSettingValues.FilterValues[i].AttributeId], function (e) { return e.AttributeID == $scope.filterSettingValues.FilterValues[i].AttributeId }))[0];
                                }

                        }
                        else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 10) {

                        }
                    }
                    $scope.ngKeywordtext = $scope.filterSettingValues.Keyword;
                    $scope.ngsaveasfilter = $scope.filterSettingValues.FilterName;
                    $scope.Updatefilter = true;
                    $scope.Applyfilter = true;
                    $scope.Deletefilter = true;
                    $scope.Savefilter = false;
                });
            }

            //Updating FilterSettings
            $scope.FilterUpdate = function () {
                //var InsertFilterSettings = $resource('planning/InsertFilterSettings');
                //var FilterData = new InsertFilterSettings();
                var FilterData = {};
                $scope.filterSettingValues.Keyword = '';
                $scope.filterSettingValues.FilterName = '';
                var whereConditionData = [];
                var multiSelectVal = [];
                var usersVal = [];
                var orgLevel = [];
                var fiscalyear = [];
                for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                    if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                            orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                            for (var k = 0 ; k < orgLevel.length; k++) {
                                whereConditionData.push({ 'AttributeID': $scope.atributesRelationList[i].AttributeId, 'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k], 'Level': $scope.atributesRelationList[i].TreeLevel, 'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId });
                            }
                        }
                    }
                    else if ($scope.atributesRelationList[i].DisplayName.toLowerCase() == "owner") {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                            usersVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                            for (var k = 0 ; k < usersVal.length; k++) {
                                whereConditionData.push({ 'AttributeID': $scope.atributesRelationList[i].AttributeId, 'SelectedValue': usersVal[k], 'Level': 0, 'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId });
                            }
                        }
                    }
                    else if ($scope.atributesRelationList[i].AttributeId == 1 && $scope.atributesRelationList[i].AttributeTypeId == 3) {
                        if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                            fiscalyear = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId]
                            for (var k = 0 ; k < fiscalyear.length; k++) {
                                whereConditionData.push({ 'AttributeID': $scope.atributesRelationList[i].AttributeId, 'SelectedValue': fiscalyear[k].Id, 'Level': 0, 'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId });
                            }
                        }
                    }
                }

                FilterData.FilterId = selectedfilterid;
                FilterData.TypeID = TypeID;
                FilterData.FilterName = $scope.ngsaveasfilter;
                FilterData.Keyword = $scope.ngKeywordtext;
                FilterData.UserId = 1;
                FilterData.entityTypeId = '';
                FilterData.StarDate = $scope.items[0].startDate;
                FilterData.EndDate = $scope.items[0].endDate;
                FilterData.WhereConditon = whereConditionData;
                FilterData.$save();
                // var filterSettingsInsertresult = InsertFilterSettings.save(FilterData, function () {
                DashboardService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {

                    //var GettingFilters = $resource('planning/GetFilterSettingsForDetail/:TypeID', { TypeID: TypeID }, { get: { method: 'GET' } });
                    //var filterSettings = GettingFilters.get(function () {
                    DashboardService.GetFilterSettingsForDetail(TypeID).then(function (filterSettings) {
                        $scope.filterValues = filterSettings.Response;
                        $scope.atributesRelationList = [];
                    });
                });

                FilterData.WhereConditon = [];
                var multiSelectVal = [];
                var usersVal = [];
                var orgLevel = [];
                //var GettingFilters = $resource('planning/GetFilterSettingsForDetail/:TypeID', { TypeID: TypeID }, { get: { method: 'GET' } });
                //var filterSettings = GettingFilters.get(function () {
                DashboardService.GetFilterSettingsForDetail(TypeID).then(function (filterSettings) {
                    $scope.filterValues = filterSettings.Response;
                });
            };

            //Applying FilterSettings
            $scope.LoadActivityFilter = function (filterid) {
                $(window).AdjustHeightWidth();
                if ($scope.FilterType == undefined)
                    $scope.FilterType = 1;
                var getactivities = {};
                getactivities.startrowno = 0;
                getactivities.maxnoofrow = 20;
                getactivities.filterid = filterid;
                getactivities.sortordercolumn = "null";
                getactivities.isdesc = false;
                getactivities.includechildren = false;
                getactivities.entityid = '0';
                getactivities.userid = parseint($cookies['userid'], 10);
                getactivities.level = 0;
                var dimenID = $scope.wigetvar.dimensionid;
                var GetActivityList = '';
                if (dimenid == "1") {
                    // getactivitylist = $resource('metadata/costcentrerootlevel/:startrowno/:maxnoofrow/:filterid/:sortordercolumn/:isdesc/:includechildren/:entityid/:userid/:level/', { startrowno: 0, maxnoofrow: 20, filterid: filterid, sortordercolumn: "null", isdesc: false, includechildren: false, entityid: '0', userid: parseint($cookies['userid'], 10), level: 0 });

                    getactivities.IDArr = [];
                    DashboardService.costcentrerootlevel(getactivities).then(function (getactivitylist) {
                        // var getactivitylist = GetActivityList.save(getactivities, function () {
                        //    DashboardService.ActivityFilter(dimenID, StartRowNo, MaxNoofRow, FilterID, SortOrderColumn, IsDesc, IncludeChildren, EntityID, UserID, Level, ExpandingEntityID, IsSingleID, getactivities).then(function (getactivitylist) {
                        if (getactivitylist.Response != null && getactivitylist.Response.Data != null) {
                            if (getactivitylist.Response.ColumnDefs.length > 0) {
                                $scope.listfilteredEntityOrCostvalues = [];

                                for (var i = 0 ; i < getactivitylist.Response.Data.length; i++) {
                                    var resultfileredvalues = {};
                                    resultfileredvalues["EntityId"] = getactivitylist.Response.Data[i].Id;
                                    resultfileredvalues["EntityName"] = getactivitylist.Response.Data[i].Name;
                                    resultfileredvalues["ShortDescription"] = getactivitylist.Response.Data[i].ShortDescription;

                                    resultfileredvalues["Checked"] = false;

                                    $scope.listfilteredEntityOrCostvalues.push(resultfileredvalues);
                                }
                            }
                            $('#FilterTaskModal').modal('hide');
                        }
                        else {
                            bootbox.alert($translate.instant('LanguageContents.Res_1805.Caption'));
                            return false;
                        }
                    });

                }
                else if (dimenid == "2") {
                    // getactivitylist = $resource('metadata/activitydetail/:startrowno/:maxnoofrow/:filterid/:sortordercolumn/:isdesc/:includechildren/:entityid/:level/:expandingentityid/:issingleid', { startrowno: 0, maxnoofrow: 20, filterid: filterid, sortordercolumn: "null", isdesc: false, includechildren: true, entityid: '0', level: 0, expandingentityid: 0, issingleid: false });
                    var getactivities = {};
                    getactivities.startrowno = 0;
                    getactivities.maxnoofrow = 20;
                    getactivities.filterid = filterid;
                    getactivities.sortordercolumn = "null";
                    getactivities.isdesc = false;
                    getactivities.includechildren = true;
                    getactivities.entityid = '0';
                    getactivities.level = 0;
                    getactivities.expandingentityid = 0;
                    getactivities.issingleid = false;
                    getactivities.IDArr = [];
                    getactivities.FilterType = $scope.FilterType;
                    // var getactivitylist = GetActivityList.save(getactivities, function () {
                    DashboardService.ActivityFilter(getactivities).then(function (getactivitylist) {
                        if (getactivitylist.Response != null && getactivitylist.Response.Data != null) {
                            if (getactivitylist.Response.ColumnDefs.length > 0) {
                                $scope.listfilteredEntityOrCostvalues = [];

                                for (var i = 0 ; i < getactivitylist.Response.Data.length; i++) {
                                    var resultfileredvalues = {};
                                    resultfileredvalues["EntityId"] = getactivitylist.Response.Data[i].Id;
                                    resultfileredvalues["EntityName"] = getactivitylist.Response.Data[i].Name;
                                    resultfileredvalues["ShortDescription"] = getactivitylist.Response.Data[i].ShortDescription;

                                    resultfileredvalues["Checked"] = false;

                                    $scope.listfilteredEntityOrCostvalues.push(resultfileredvalues);
                                }
                            }
                            $('#FilterTaskModal').modal('hide');
                        }
                        else {
                            bootbox.alert($translate.instant('LanguageContents.Res_1805.Caption'));
                            return false;
                        }
                    });
                }

                // var getactivities = new GetActivityList();

            }


            $scope.tagAllOptionsEntityStatus = {
                multiple: true,
                allowClear: true,
                data: $scope.ReassignMembersDataEntityStatus,
                formatResult: $scope.formatResultEntityStatus,
                formatSelection: $scope.formatSelection,
                dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
                escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
            };

            $scope.tagAllOptions = {
                multiple: true,
                allowClear: true,
                data: $scope.ReassignMembersData,
                formatResult: $scope.formatResult,
                formatSelection: $scope.formatSelection,
                dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
                escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
            };
            //---------------> Bind Children with color 

            $scope.ReassignMembersData = [];
            $scope.ReassignMembersDataEntityStatus = [];
            $scope.formatResult = function (item) {
                var markup = '<table class="user-result">';
                markup += '<tbody>';
                markup += '<tr>';
                markup += '<td class="user-image">';
                markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
                markup += '</td>';
                markup += '<td class="user-info">';
                markup += '<div class="user-title">' + item.text + '</div>';
                markup += '</td>';
                markup += '</tr>';
                markup += '</tbody>';
                markup += '</table>';
                return markup;
            };
            $scope.formatResultEntityStatus = function (item) {
                var markup = '<table class="user-result">';
                markup += '<tbody>';
                markup += '<tr>';
                markup += '<td class="user-image">';
                markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
                markup += '</td>';
                markup += '<td class="user-info">';
                markup += '<div class="user-title">' + item.text + '</div>';
                markup += '</td>';
                markup += '</tr>';
                markup += '</tbody>';
                markup += '</table>';
                return markup;
            };
            $scope.formatSelection = function (item) {
                var markup = '<table class="user-result">';
                markup += '<tbody>';
                markup += '<tr>';
                markup += '<td class="user-image">';
                markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
                markup += '</td>';
                markup += '<td class="user-info">';
                markup += '<div class="user-title">' + item.text + '</div>';
                markup += '</td>';
                markup += '</tr>';
                markup += '</tbody>';
                markup += '</table>';
                return markup;
            };
            $scope.formatSelectionForEntityStauts = function (item) {
                var markup = '<table class="user-result">';
                markup += '<tbody>';
                markup += '<tr>';
                markup += '<td class="user-image">';
                markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
                markup += '</td>';
                markup += '<td class="user-info">';
                markup += '<div class="user-title">' + item.text + '</div>';
                markup += '</td>';
                markup += '</tr>';
                markup += '</tbody>';
                markup += '</table>';
                return markup;
            };
            $scope.tagAllOptions = {
                multiple: true,
                allowClear: true,
                data: $scope.ReassignMembersData,
                formatResult: $scope.formatResult,
                formatSelection: $scope.formatSelection,
                dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
                escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
            };
            $scope.tagAllOptionsEntityStatus = {
                multiple: true,
                allowClear: true,
                data: $scope.ReassignMembersDataEntityStatus,
                formatResult: $scope.formatResultEntityStatus,
                formatSelection: $scope.formatSelection,
                dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
                escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
            };

        }


        //----------------> Filter Ends <------------------------------------------

        //------------ Tab concept -----------

        $scope.handleNext = function () {


            if ($scope.wigetvar.caption == undefined) {
                bootbox.alert($translate.instant('LanguageContents.Res_1853.Caption'));
                return false;
            }
            if ($scope.wigetvar.caption.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_1853.Caption'));
                return false;
            }
            if ($scope.wigetvar.widgetType.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_1854.Caption'));
                return false;
            }
            if ($scope.wigetvar.NoOfItems == true) {
                if ($scope.wigetvar.noofitem.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1855.Caption'));
                    return false;
                }
            }
            if ($scope.wigetvar.showcntrlforDynamic == true && $scope.wigetvar.dimensionid != 1 && $scope.wigetvar.dimensionid != 2) {

                if ($scope.wigetvar.dimensionid == undefined) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1856.Caption'));
                    return false;
                }
                if ($scope.wigetvar.matrixid == "") {
                    bootbox.alert($translate.instant('LanguageContents.Res_1857.Caption'));
                    return false;
                }

                if ($scope.wigetvar.visualtypeid != 1 && $scope.wigetvar.visualtypeid != 2) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1858.Caption'));
                    return false;
                }

            }
            if ($scope.isLastStep()) {


                //timeing  not  removeing  pervious  list  for   edite
                var templist = [];
                $scope.templistofEntityOrCostChecked = [];


                if ($scope.wigetvar.showcntrlforDynamic == true && $scope.wigetvar.matrixid == "") {
                    bootbox.alert($translate.instant('LanguageContents.Res_1857.Caption'));
                    return false;
                }

                if ($scope.wigetvar.showcntrlforDynamic == true && $scope.wigetvar.visualtypeid != 1 && $scope.wigetvar.visualtypeid != 2) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1858.Caption'));
                    return false;
                }

                if ($scope.listfilteredEntityOrCostvalues != undefined && $scope.listfilteredEntityOrCostvalues.length > 0) {

                    templist = jQuery.grep($scope.listfilteredEntityOrCostvalues, function (n, i) {
                        return (n.Checked != false);
                    });

                    if (templist.length > 0) {
                        for (var i = 0 ; i < templist.length; i++) {
                            $scope.templistofEntityOrCostChecked.push(templist[i].EntityId);
                        }
                        $scope.step = 0;
                    }
                    else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1859.Caption'));
                        return false;
                    }
                    $scope.wigetvar.listofEntityID = '';
                    $scope.wigetvar.listofEntityID = $scope.templistofEntityOrCostChecked.join(",");

                }

                $scope.listfilteredEntityOrCostvalues = [];

                $scope.addWidget();
            }
            else {
                $scope.step += 1;
                $scope.listfilteredEntityOrCostvalues = [];
                if ($scope.getCurrentStep() == 'DashboardFilterPage') {
                    $scope.showfilter = true;
                    if ($scope.wigetvar.dimensionid == "1") {
                        LoadDashboardDefaultCostCenterData();
                    }
                    else if ($scope.wigetvar.dimensionid == "2") {
                        LoadDashboardDefaultActivityData();
                    }
                }
            }
        };

        //Ng-swithch tab functionality
        $scope.WidgetDynamicStep = ['DashboardMainPage', 'DashboardFilterPage'];
        $scope.WidgetStaticStep = ['DashboardMainPage'];

        $scope.step = 0;

        $scope.isCurrentStep = function (step) {
            return $scope.step === step;
        };

        $scope.setCurrentStep = function (step) {
            $scope.step = step;
        };

        $scope.getCurrentStep = function () {
            //if ($scope.wigetvar.AddOrEdit != 'EDIT') {
            if (($scope.wigetvar.widgetType == "3") && ($scope.wigetvar.dimensionid == "1" || $scope.wigetvar.dimensionid == "2")) {
                $scope.steps = $scope.WidgetDynamicStep;
                return $scope.WidgetDynamicStep[$scope.step];
            }
            else {
                $scope.steps = $scope.WidgetStaticStep;
                return $scope.WidgetStaticStep[$scope.step];
            }
            //}
            //else {
            //    $scope.steps = $scope.WidgetStaticStep;
            //    return $scope.WidgetStaticStep[$scope.step];
            //}
        };

        $scope.isFirstStep = function () {
            return $scope.step === 0;
        };

        $scope.isLastStep = function () {
            return $scope.step === ($scope.steps.length - 1);
        };

        $scope.getNextLabel = function () {
            return ($scope.isLastStep()) ? 'Submit' : 'Next';
        };


        $scope.handlePrevious = function () {
            $scope.showfilter = false;
            $scope.step -= ($scope.isFirstStep()) ? 0 : 1;
        };

        // ...

        // --- Define Controller Variables. ----------------- //




        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.dashboardCtrl']"));
        });
        // --- Initialize. ---------------------------------- //

        var self = this;
        // ...

        //For Setting Color Code - By Madhur 22 Dec 2014
        $scope.set_color = function (clr) {
            if (clr != null)
                return { 'background-color': "#" + clr.toString().trim() };
            else
                return '';
        }
    }
    //);
    app.controller("mui.dashboardCtrl", ['$scope', '$resource', '$cookies', '$compile', '$timeout', '$window', 'DashboardService', '$translate', muidashboardCtrl]);
})(angular, app);