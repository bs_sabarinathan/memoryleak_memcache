﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UpgradeTool
{
    [Serializable]
    public class Version
    {
        public int Id { get; set; }
        public string versionname { get; set; }
        public string details { get; set; }
        public string datereleased { get; set; }
        public string features { get; set; }
        public string updateZipPath { get; set; }
        public string SQLUpdatePath { get; set; }
        public string Status { get; set; }
        public string XMLUpdatePath { get; set; }
        public string DocPath { get; set; }
    }
}