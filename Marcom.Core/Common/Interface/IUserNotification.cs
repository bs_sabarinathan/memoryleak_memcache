using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Common.Interface
{
    /// <summary>
    /// IUserNotification interface for table 'CM_User_Notification'.
    /// </summary>
    public interface IUserNotification
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        int Userid
        {
            get;
            set;

        }

        int Entityid
        {
            get;
            set;

        }

        int Actorid
        {
            get;
            set;

        }

        DateTimeOffset CreatedOn
        {
            get;
            set;

        }

        int Typeid
        {
            get;
            set;

        }

        bool IsViewed
        {
            get;
            set;

        }

        bool IsSentInMail
        {
            get;
            set;

        }

        string TypeName
        {

            get;
            set;

        }

        string AttributeName
        {

            get;
            set;

        }

        string FromValue
        {

            get;
            set;

        }

        string ToValue
        {
            get;
            set;

        }
        int AssocitedEntityID
        {
            get;
            set;

        }
        int PhaseID
        {
            get;
            set;

        }
        string  StepID
        {
            get;
            set;

        }


        #endregion
    }
}
