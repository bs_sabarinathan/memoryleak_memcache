﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IAttachments interface for table 'PM_Attachments'.
    /// </summary>
    public interface IFinancialCostcentreRelation
    {
        #region Public Properties

        int EntityID
        {
            get;
            set;

        }

        int Costcentreid
        {
            get;
            set;

        }

      

        int Status
        {
            get;
            set;

        }

        #endregion
    }
}
