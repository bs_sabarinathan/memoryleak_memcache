﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Dal.Metadata.Model;
using BrandSystems.Marcom.Dal.Task.Model;
using BrandSystems.Marcom.Core.Metadata.Interface;
using Mail;
using Newtonsoft.Json.Linq;
using BrandSystems.Marcom.Dal.DAM.Model;

namespace BrandSystems.Marcom.Core.Common
{
    internal class NotificationFeedObjects : INotificationFeedObjects
    {
        public string action { get; set; }
        public int EntityId { get; set; }
        public String TypeName { get; set; }
        public String AttributeName { get; set; }
        public String FromValue { get; set; }
        public String ToValue { get; set; }
        public int Userid { get; set; }
        // public Object obj1 { get; set; }
        public IList<object> obj2 { get; set; }
        public IList obj3 { get; set; }
        public IList<ITreeValue> obj4 { get; set; }
        public JArray obj5 { get; set; }
        public JArray obj6 { get; set; }
        public int FinancialPlannedAmount { get; set; }
        public int AttributeId { get; set; }
        public int Attributetypeid { get; set; }
        public IList<AttributeDao> AttributeDetails { get; set; }
        public int Actorid { get; set; }
        public DateTimeOffset CreatedOn { get; set; }
        public int EntityTypeId { get; set; }
        public int AssociatedEntityId { get; set; }
        public int ParentId { get; set; }
        public bool memberisinherited { get; set; }
        public string attachmenttype { get; set; }
        public string OldActorName { get; set; }
        public IList<TreeValueDao> treeValues { get; set; }
        public IList<DAMTreeValueDao> damtreeValues { get; set; }
        public IList<TreeNodeDao> treeNode { get; set; }
        public int objectiveId{ get; set; }
        public int objectiveName { get; set; }
        public string AttributeRecordName { get; set; }
        public int Version { get; set; }
        public int PhaseID { get; set; }
        public string StepID { get; set; }
        public int PhaseCount { get; set; }
        public int NotifitypeID { get; set; }
        public bool IsPersonalized { get; set; }
        public IList<BrandSystems.Marcom.Core.Metadata.Interface.IEntityTypeAttributeGroupRelationwithLevels> attrgrprel { get; set; }
        public IList<BrandSystems.Marcom.Core.Planning.Interface.IAttributeData> attrdata { get; set; }
    }
    internal class WorkFlowNotifyHolder : NotificationFeedObjects
    {
        public IList<BrandSystems.Marcom.Dal.Planning.Model.TaskAttachmentsDao> taskattachmentdao { get; set; }
        public IList<BrandSystems.Marcom.Dal.Task.Model.NewTaskAttachmentsDao> newTaskattachmentdao { get; set; }
        public IList<BrandSystems.Marcom.Dal.Access.Model.TaskMemberDao> ientityRole { get; set; }
        public IList<TaskMembersDao> ientityRoles { get; set; }
        public IList<BrandSystems.Marcom.Dal.Task.Model.TaskMembersDao> ientityTaskRole { get; set; }
        public IList<BrandSystems.Marcom.Dal.Planning.Model.TaskDao> iTask { get; set; }
        public IList<BrandSystems.Marcom.Dal.Task.Model.EntityTaskDao> iEntityTask { get; set; }
        public string Tasktatus;
        public bool ismemberadded;
        public bool isattachment;
        public bool isfile;
        public List<int> MultiTask { get; set; }
        public bool NoneedNotification;
        
        public BrandSystems.Marcom.Dal.Planning.Model.TaskDao taskdao { get; set; }

    }
    public class MailHolder
    {
        public List<string> PrimaryTo { get; set; }
        public List<string> SecondaryTo { get; set; }
        public string Body { get; set; }
        public Dictionary<string, string> MailTemplateDictionary { get; set; }
        public int MailType { get; set; }
        public string Subject { get; set; }
        public string Id { get; set; }
        public bool isrecap { get; set; }
    }
    
}
