using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// FileDefaultInfo object for table 'CM_FileDefaultInfo'.
	/// </summary>
	
	internal class FileDefaultInfo : IFileDefaultInfo 
	{
		#region Member Variables

		protected int _id;
		protected string _property;
		protected string _value;
		
		
		#endregion
		
		#region Constructors
		public FileDefaultInfo() {}
			
		public FileDefaultInfo(int pId, string pProperty, string pValue)
		{
			this._id = pId; 
			this._property = pProperty; 
			this._value = pValue; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public string Property
		{
			get { return _property; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Property", "Property value, cannot contain more than 50 characters");			  
			  _property = value; 
			}
			
		}
		
		public string Value
		{
			get { return _value; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("Value", "Value value, cannot contain more than 250 characters");			  
			  _value = value; 
			}
			
		}
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			FileDefaultInfo castObj = null;
			try
			{
				castObj = (FileDefaultInfo)obj;
			} catch(Exception) { return false; } 
			return castObj.GetHashCode() == this.GetHashCode();
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			return this.GetType().FullName.GetHashCode();
				
		}
		#endregion
		
	}
	
}
