﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{

    public interface IWorkFlowOverView
    {
        #region Public Properties

         int StepID { get; set; }
         int ActiveTasks { get; set; }
         int UnassignedTasks { get; set; }
         int OverdueTasks { get; set; }
         string CurrentStepName { get; set; }
         dynamic OptionList { get; set; }

        #endregion
    }
    public interface IStepOptions
    {
         int StepID { get; set; }
         string StepName { get; set; }
    }

}
