﻿(function (ng, app) {
    "use strict";

    function muiadmincmspagetypeattributerelationCtrl($scope, $location, $resource, $timeout, $cookies, $window, $compile, $translate, MetadataService, AccessService, CommonService) {
        var IsSaveClose = false;
        $scope.IsDisabled = false;
        $scope.DuplicateEntityType = {
            EntityTypeID: 0,
            EntityCaption: "",
            EntityShortDescription: "",
            EntityTypeColorcode: "ffffff",
            Description: ""
        }
        var attributeObj = [];
        $scope.Attrs = [];
        $scope.enabledisableCmsattributes = false;
        $scope.MinValue = false;
        $scope.MaxValue = false;
        $scope.AddorEdit = 0;
        $scope.tempAttributeGrpID = 0;
        $scope.attributegroupGridData = [];
        $scope.attributegroupList = [];
        $scope.Category = 0;
        $scope.AdminTaskCheckList = [{
            ID: 0,
            StatusOptions: "",
            IsDeleted: false,
            SortOrder: 1,
            IsExisting: true
        }];
        $scope.IsAssociate = 0;
        $scope.AttributeIDList = [];
        $scope.Fetureattribues = [];
        $scope.EntityHietemp = [];
        $scope.ColorCodeGlobal = 'ffffff';
        $scope.ColorCodeGlobalObj = {};
        $scope.ColorCodeGlobalObj.colorcode = 'ffffff';
        $scope.ColorOptions = {
            preferredFormat: "hex",
            showInput: true,
            showAlpha: false,
            allowEmpty: true,
            showPalette: true,
            showPaletteOnly: false,
            togglePaletteOnly: true,
            togglePaletteMoreText: 'more',
            togglePaletteLessText: 'less',
            showSelectionPalette: true,
            chooseText: "Choose",
            cancelText: "Cancel",
            showButtons: true,
            clickoutFiresChange: true,
            palette: [
				["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
				["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)", "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
				["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)", "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)", "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)", "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
            ]
        };
         if ($scope.CurrentMetadataVersionInfo == 0)
            $scope.IsOlderMetadataVersion.IsOlderVersion = false;
        else
            $scope.IsOlderMetadataVersion.IsOlderVersion = true;
        $scope.colorchange = function (color) { }
        $(".pick-a-color").pickAColor({
            showSpectrum: true,
            showSavedColors: false,
            saveColorsPerElement: true,
            fadeMenuToggle: true,
            showAdvanced: true,
            showHexInput: true,
            showBasicColors: true
        });
        $scope.DragDropvalues = '';
        $scope.DragDropAttribteGroupvalues = '';
        $scope.attributeObjCaption = '';
        $scope.attributeObjAttributeTypeId = 0;
        $scope.EntityHierarchyTypesResult = [];
        $scope.ParententityDropdowndata = [];
        $scope.AttributeOptionvalues = {};
        $scope.entitytypeattributrearr = [];
        $scope.FulfillmentAttributes = [];
        $scope.entitytypefeaturearr = [];
        $scope.modules = {};
        $scope.entityattributes = {};
        $scope.attributes = {};
        $scope.Features = [];
        $scope.entitytpesdata = [];
        $scope.entitytpesdatasystemdefined = [];
        $scope.entitytpesdatarootlevel = [];
        $scope.entitytpesdatachildren = [];
        $scope.AttributeOptionvalues = [];
        $scope.ressdfwe = {};
        $scope.entitytypefeatures = {};
        $scope.entitytypeattributrearr = [];
        $scope.EntityID = 0;
        $scope.EntType = {
            EntityTypeCaption: "",
            EntityTypeDescription: "",
            ddlModuleID: 0,
            EntitytperelCaption: "",
            ddlAttributeID: 0,
            ddlSortorder: "",
            Defaultvalue: "",
            Inheritfromparent: false,
            Isreadonly: false,
            Choosefromparentonly: false,
            Isvalidationneeded: false,
            ddlEntityFeatureID: '',
            AttributeMultiselectDefaultValue: '',
            Shortdescription: '',
            Colourcode: '',
            ShowHideRootLevel: true,
            ShowHideInheritFromParent: true,
            ddlAttributeGroupID: 0,
            AttributeGroupCaption: "",
            locationOptions: 1,
            EnableAttributeGroupUpdate: true,
            EnableAttributeGroupAdd: true,
            representationOptions: 0,
            AttrGroupGlobalRolevalue: [],
            LocationType: 0,
            RepresentationType: "",
            MinValue: 0,
            MaxValue: 0,
            IsHelptextEnabled: 0,
            HelptextDecsription: "",
            PageSize: 0,
            IsTabInEntityCreation: false
        };
        $scope.EntityRoleResultCollection = [{}];
        $scope.DefaultEntityRoleCollection = [{}];
        $scope.SelctedEntityRoledIDs = [{}];
        GetAllEntityRole(false);

        function GetAllEntityRole(IsShow) {
            $scope.EntityRoleResultCollection = [{}];
            $scope.DefaultEntityRoleCollection = [{}];
            $scope.SelctedEntityRoledIDs = [{}];
            $scope.EntityRoleResultCollection.splice(0, 1);
            $scope.DefaultEntityRoleCollection.splice(0, 1);
            $scope.SelctedEntityRoledIDs.splice(0, 1);
            AccessService.GetAllEntityRole().then(function (GetEntityRolesResult) {
                if (GetEntityRolesResult.Response != null) {
                    for (var i = 0; i < GetEntityRolesResult.Response.length; i++) {
                        if (GetEntityRolesResult.Response[i].Id == EntityRoles.Owner || GetEntityRolesResult.Response[i].Id == EntityRoles.BudgerApprover) {
                            if (IsShow == false || GetEntityRolesResult.Response[i].Id != EntityRoles.BudgerApprover) {
                                $scope.DefaultEntityRoleCollection.push({
                                    EntityRoleID: GetEntityRolesResult.Response[i].Id,
                                    Caption: GetEntityRolesResult.Response[i].Caption
                                });
                            }
                        } else {
                            $scope.SelctedEntityRoledIDs.push({
                                EntityRoleID: GetEntityRolesResult.Response[i].Id,
                                Caption: GetEntityRolesResult.Response[i].Caption
                            });
                            $scope.EntityRoleResultCollection.push(GetEntityRolesResult.Response[i]);
                        }
                    }
                }
            });
        }
        $scope.AddEntityRole = function (Index, ID) {
            if (ValidateEntityRoleAcc() == false) {
                return false
            }
            $scope.SelctedEntityRoledIDs.splice(Index + 1, 0, {
                EntityRoleID: $scope.EntityRoleResultCollection[0].Id,
                Caption: ""
            });
        }
        $scope.DeleteEntityRole = function (Index, ID) {
            bootbox.confirm($translate.instant('LanguageContents.Res_1126.Caption'), function (result) {
                if (result) {
                    if (ID == undefined) {
                        $timeout(function () {
                            $scope.SelctedEntityRoledIDs.splice(Index, 1);
                        }, 200);
                        NotifySuccess($translate.instant('LanguageContents.Res_4248.Caption'));
                    } else {
                        MetadataService.DeleteEntityTypeRoleAcl(ID).then(function (Result) {
                            if (Result.Response == true) {
                                $timeout(function () {
                                    $scope.SelctedEntityRoledIDs.splice(Index, 1);
                                }, 200);
                                NotifySuccess($translate.instant('LanguageContents.Res_4248.Caption'));
                            } else if (Result.Response == "Exist") {
                                bootbox.alert($translate.instant('LanguageContents.Res_4854.Caption'));
                            }
                        });
                    }
                }
            });
        }

        function ValidEntitytypeattribute() {
            var entitytypeValues = [
				['#EntityTypeCaption', 'presence', 'Please Enter the Entity type caption'],
				['#Shortdescription', 'presence', 'Please Enter the Description'],
				['#ddlModuleID', 'presence', 'Please select module']
            ];
            $("#entitytypeformPage1").nod(entitytypeValues, {
                'delay': 200,
                'submitBtnSelector': '#btnTemp',
                'disableSubmitBtn': 'false',
                'silentSubmit': 'true'
            });
            sentValidation = true;
        }

        function ValidEntitytypeattributePage2() {
            var entitytypeValues1 = [
				['#EntitytpeattrrelCaption', 'presence', 'Please Enter the Attribute Caption'],
				['#ddlAttributeID', 'presence', 'Please Enter the Attribute Type']
            ];
            $("#entitytypeformPage2").nod(entitytypeValues1, {
                'delay': 200,
                'submitBtnSelector': '#btnTemp1',
                'disableSubmitBtn': 'false',
                'silentSubmit': 'true'
            });
        }

        function ValidEntitytypeattributePage4() {
            var entitytypeValues1 = [
				['#AttributeGroupCaption', 'presence', 'Please enter the Attribute Group Caption'],
				['#dropdownAttributeGrpID', 'presence', 'Please select Attribute group yype']
            ];
            $("#entitytypeformPage4").nod(entitytypeValues1, {
                'delay': 200,
                'submitBtnSelector': '#btnTemp4',
                'disableSubmitBtn': 'false',
                'silentSubmit': 'true'
            });
            $("#entitytypeformPage4").addClass('notvalidate');
        }
        AccessService.GetGlobalRole().then(function (getglobalroles) {
            $scope.globalrolesData = getglobalroles.Response;
        });
        $scope.EnityType = ['EnityType', 'EntityTypeAttribute', 'EntityTypeAttributeGroup'];
        $scope.step = 0;
        $scope.isCurrentStep = function (step) {
            return $scope.step === step;
        };
        $scope.setCurrentStep = function (step) {
            $scope.step = step;
        };
        $scope.getCurrentStep = function () {
            $scope.steps = $scope.EnityType;
            return $scope.EnityType[$scope.step];
        };
        $scope.isFirstStep = function () {
            return $scope.step === 0;
        };
        $scope.isLastStep = function () {
            return $scope.step === ($scope.steps.length - 1);
        };
        $scope.visibleLastStep = true;
        $scope.getNextLabel = function () {
            $scope.visibleLastStep = true;
            if ($scope.IsOlderMetadataVersion.IsOlderVersion == false && $scope.isLastStep()) $scope.visibleLastStep = false;
            if ($scope.isLastStep() == true) $scope.visiblesaveclose = false;
            return ($scope.isLastStep()) ? 'Submit' : 'Next';
        };
        $scope.handlePrevious = function () {
            $scope.enabledisableCmsattributes = false;
            if ($scope.AddorEdit == 1) $scope.visiblesaveclose = true;
            else $scope.visiblesaveclose = false;
            if ($scope.getCurrentStep() == "EntityTypeAttributeGroup") {
                var thisobj = this;
                $scope.EntType.ddlAttributeID = 0;
                $scope.EntType.EntitytperelCaption = '';
                $scope.EnablePlaceHolder = false;
                $scope.EnableMultiselectddl = false;
                $scope.EnableSingleSelection = false;
                $scope.Enableinputtxt = false;
                $scope.EntType.ShowHideInheritFromParent = false;
                $scope.EnableMultiselectFromParent = false;
                $scope.EnableEntitytypeAttributeAdd = true;
                $scope.EnableEntitytypeAttributeUpdate = false;
                $timeout(function () { $scope.closevalidationpopup(thisobj); }, 100);
            }
            $timeout(function () {
                $(".pick-a-color").pickAColor({
                    showSpectrum: true,
                    showSavedColors: false,
                    saveColorsPerElement: true,
                    fadeMenuToggle: true,
                    showAdvanced: true,
                    showHexInput: true,
                    showBasicColors: true
                });
                $("#appendedPrependedDropdownButton input").val($scope.ColorCodeGlobal);
                $(".current-color").css('background-color', '#' + $scope.ColorCodeGlobal);
            }, 200);
            $timeout(function () {
                applyAlternateColor();
            }, 100);
            $scope.step -= ($scope.isFirstStep()) ? 0 : 1;
        };
        $scope.workflowList = [];
        $scope.attributegroupdata = [];
        MetadataService.GetAttributeGroup().then(function (listAttributeGroups) {
            $scope.attributegroupList = listAttributeGroups.Response;
        });
        MetadataService.GetEntityType(6).then(function (entitytypes) {
            $scope.entitytpesdata = entitytypes.Response;
            $scope.entitytpesdatarootlevel = entitytypes.Response;
            $scope.BindChildEntities();
        });
        $scope.BindChildEntities = function () {
            MetadataService.GettingChildEntityTypes(-1).then(function (childEntitytypes) {
                $scope.childrenEntitytpesdata = childEntitytypes.Response;
                if (childEntitytypes.Response != null) {
                    if ($scope.tagAllOptions.data.length > 0) $scope.tagAllOptions.data.splice(0, $scope.tagAllOptions.data.length);
                    $.each(childEntitytypes.Response, function (i, el) {
                        $scope.tagAllOptions.data.push({
                            "id": el.Id,
                            "text": el.Caption,
                            "ShortDescription": el.ShortDescription,
                            "ColorCode": el.ColorCode
                        });
                    });
                }
            });
        }
        var sentValidation = false;
        $scope.addEntityTypeAttribute = function () {
            $("#entitytypeattributerelationModal").modal('show');
            IsSaveClose = false;
            $scope.IsDisabled = false;
            $scope.ShowEntityTypeRoleAcl = true;
            GetAllEntityRole(false);
            $scope.AddorEdit = 0;
            $scope.visiblesaveclose = false;
            $timeout(function () {
                $(".pick-a-color").pickAColor({
                    showSpectrum: false,
                    showSavedColors: false,
                    saveColorsPerElement: false,
                    fadeMenuToggle: true,
                    showAdvanced: true,
                    showHexInput: true,
                    showBasicColors: true
                });
            }, 50);
            $timeout(function () {
                $(".current-color").css('background-color', '#ffffff');
                $("#appendedPrependedDropdownButton input").val('ffffff');
            }, 500);
            $scope.attributegroupGridData = [];
            $scope.EntType.EnableAttributeGroupAdd = true;
            $scope.EntType.EnableAttributeGroupUpdate = false;
            $timeout(function () {
                $scope.EntType.ddlModuleID = 6
            }, 200);
            $scope.Category = 2;
            categoryid = 2;
            $scope.showStatusOptions = true;
            $scope.AdminTaskCheckList = [{
                ID: 0,
                StatusOptions: "",
                IsDeleted: false,
                SortOrder: 1,
                IsExisting: false
            }];
            $scope.attributerelationarr = [];
            $scope.step = 0;
            $scope.ColorCodeGlobal = '';
            $scope.ColorCodeGlobalObj.colorcode = "ffffff";
            $scope.EntityHierarchyTypesResult = [];
            $scope.Fetureattribues = [];
            $scope.ParententityDropdowndata = [];
            $scope.entitytypeattributrearr = [];
            $scope.EnableEntitytypeAttributeUpdate = false;
            $scope.Enableinputtxt = false;
            $scope.EnablePlaceHolder = false;
            $scope.EnableSingleSelection = false;
            $scope.EnableMultiselectddl = false;
            $scope.EnableEntityfeatureAdd = true;
            $scope.EnableMultiselectFromParent = false;
            $scope.EnableEntitytypeAttributeAdd = true;
            $scope.entitytypefeaturearr = [];
            $scope.EntityID = 0;
            $scope.EntType.EntitytperelCaption = '', $scope.EntType.ddlAttributeID = 0, $scope.EntType.Defaultvalue = '', $scope.EntType.MultiselectDefaultvalue = '', $scope.EntType.TextDefaultvalue = '', $scope.EntType.PlaceHolderTextValue = '', $scope.EntType.Inheritfromparent = false, $scope.EntType.Isreadonly = false, $scope.EntType.Choosefromparentonly = false, $scope.EntType.Isvalidationneeded = false, $scope.EntType = {
                EntityTypeCaption: "",
                EntityTypeDescription: "",
                ddlModuleID: 0,
                EntitytperelCaption: "",
                ddlAttributeID: 0,
                ddlSortorder: "",
                Defaultvalue: '',
                Inheritfromparent: false,
                Isreadonly: false,
                Choosefromparentonly: false,
                Isvalidationneeded: false,
                ddlEntityFeatureID: '',
                Shortdescription: '',
                Colourcode: '',
                ShowHideRootLevel: true,
                ShowHideInheritFromParent: true,
                MinValue: 0,
                MaxValue: 0,
                IsHelptextEnabled: 0,
            HelptextDecsription: ""
            };
            $scope.entitytypeattributrearr = [];
            var attributeObj = [];
            attributeObj = $.grep($scope.attributes, function (e) {
                return e.Id == parseInt(68);
            });
            $scope.entitytypeattributrearr.push({
                "Id": $scope.count,
                "AttributeID": 68,
                "ValidationID": 1,
                "SortOrder": 1,
                "DefaultValue": "",
                "InheritFromParent": false,
                "IsReadOnly": false,
                "ChooseFromParentOnly": false,
                "IsValidationNeeded": true,
                "Caption": attributeObj[0].Caption,
                "IsSystemDefined": true,
                "ID": 0,
                "EntityTypeName": $scope.EntType.EntityTypeCaption,
                "AttributeTypeID": 1,
                "AttributeCaption": attributeObj[0].Caption,
                "PlaceHolderValue": "",
                "MinValue": 0,
                "MaxValue": 0,
                "isvalidationset": 0,
                "IsHelptextEnabled": 0,
                "HelptextDecsription": ""
            });
            $scope.count += 1;
            attributeObj = [];
            attributeObj = $.grep($scope.attributes, function (e) {
                return e.Id == parseInt(69);
            });
            $scope.entitytypeattributrearr.push({
                "Id": $scope.count,
                "AttributeID": 69,
                "ValidationID": 1,
                "SortOrder": 2,
                "DefaultValue": "",
                "InheritFromParent": false,
                "IsReadOnly": false,
                "ChooseFromParentOnly": false,
                "IsValidationNeeded": true,
                "Caption": attributeObj[0].Caption,
                "IsSystemDefined": true,
                "ID": 0,
                "EntityTypeName": $scope.EntType.EntityTypeCaption,
                "AttributeTypeID": 3,
                "AttributeCaption": attributeObj[0].Caption,
                "PlaceHolderValue": "",
                "MinValue": 0,
                "MaxValue": 0,
                "isvalidationset": 0,
                "IsHelptextEnabled": 0,
                "HelptextDecsription": ""
            });
            $scope.count += 1;
            attributeObj = [];
            attributeObj = $.grep($scope.attributes, function (e) {
                return e.Id == parseInt(109);
            });
            $scope.entitytypeattributrearr.push({
                "Id": $scope.count,
                "AttributeID": 109,
                "ValidationID": 1,
                "SortOrder": 3,
                "DefaultValue": "",
                "InheritFromParent": false,
                "IsReadOnly": false,
                "ChooseFromParentOnly": false,
                "IsValidationNeeded": true,
                "Caption": attributeObj[0].Caption,
                "IsSystemDefined": true,
                "ID": 0,
                "EntityTypeName": $scope.EntType.EntityTypeCaption,
                "AttributeTypeID": 5,
                "AttributeCaption": attributeObj[0].Caption,
                "PlaceHolderValue": "",
                "MinValue": 0,
                "MaxValue": 0,
                "isvalidationset": 0,
                "IsHelptextEnabled": 0,
                "HelptextDecsription": ""
            });
            $scope.count += 1;
            attributeObj = [];
            attributeObj = $.grep($scope.attributes, function (e) {
                return e.Id == parseInt(71);
            });
            $scope.BindChildEntities();
            if (sentValidation == false) ValidEntitytypeattribute();
            $("#entitytypeformPage1").addClass('notvalidate');
            $timeout(function () {
                $('#EntityTypeCaption').focus();
            }, 1000);
        };
        $scope.LoadControl = function () {
            $scope.EnableEntitytypeAttributeUpdate = false;
            $scope.EnableEntitytypeAttributeAdd = true;
            $scope.EntType.EntitytperelCaption = '';
            $scope.EntType.Defaultvalue = '';
            $scope.EntType.TextDefaultvalue = '';
            $scope.EntType.PlaceHolderTextValue = '';
            $scope.EntType.Inheritfromparent = false;
            $scope.EntType.Isreadonly = false;
            $scope.EntType.entityattrelID = 0;
            $("#entitytypeformPage2").addClass('notvalidate');
            $scope.Enableinputtxt = false;
            $scope.EnablePlaceHolder = false;
            $scope.EnableSingleSelection = false;
            $scope.EnableMultiselectddl = false;
            $scope.EnableMultiselectFromParent = false;
            $scope.AttributeOptionvalues = [];
            var ID = parseInt($scope.EntType.ddlAttributeID);
            MetadataService.GetAdminOptionListID(ID).then(function (GetAttributeOptions) {
                $scope.AttributeOptionvalues = GetAttributeOptions.Response;
                var attributeObj = $.grep($scope.attributes, function (e) {
                    return e.Id == parseInt($scope.EntType.ddlAttributeID);
                });
                $scope.attributeObjCaption = attributeObj[0].Caption;
                $scope.attributeObjAttributeTypeId = attributeObj[0].AttributeTypeID;
                $scope.EntType.EntitytperelCaption = attributeObj[0].Caption;
                if (attributeObj[0].IsSpecial == true) {
                    $scope.EnableMultiselectddl = false;
                    $scope.EnableSingleSelection = false;
                    $scope.Enableinputtxt = false;
                    $scope.EnablePlaceHolder = false;
                    return false;
                }
                if (attributeObj[0].AttributeTypeID == 4) {
                    $scope.EnableMultiselectddl = true;
                    $scope.EnableMultiselectFromParent = true;
                }
                if (attributeObj[0].AttributeTypeID == 3) {
                    $scope.EnableMultiselectFromParent = true;
                    $scope.EnableSingleSelection = true;
                }
                if (attributeObj[0].AttributeTypeID == 5 || attributeObj[0].AttributeTypeID == 16) {
                    $scope.EnableMultiselectFromParent = false;
                    $scope.MinValue = true;
                    $scope.MaxValue = true;
                }
                if (attributeObj[0].AttributeTypeID == 6 || attributeObj[0].AttributeTypeID == 7 || attributeObj[0].AttributeTypeID == 12) {
                    $scope.EnableMultiselectFromParent = true;
                }
                if (attributeObj[0].AttributeTypeID == 1 || attributeObj[0].AttributeTypeID == 2) {
                    $scope.Enableinputtxt = true;
                    $scope.EnablePlaceHolder = true;
                }
            });
        }
        $scope.editentitytypetableInPopup = '<a class="iconLink" data-toggle="modal" ng-click="GetEntityTypeByID(row)" ng-show="row.entity.Category==2 || row.entity.Category==3 ||row.entity.Id == 5" data-toggle="modal" data-target="#entitytypeattributerelationModal"><i class="icon-edit"></i></a> ';
        $scope.entitytypedelete = '<a class="iconLink" data-toggle="modal" ng-click="DeleteEntityTypeByID(row)" ng-show="(row.entity.Category==2 && row.entity.Id!=5 && IsOlderMetadataVersion.IsOlderVersion==true)" data-toggle="modal"><i class="icon-remove"></i></a> ';
        $scope.entitytypeduplicate = '<a class="iconLink" data-toggle="modal" ng-click="DuplicatePopUpEntityTypeByID(row)" ng-show="row.entity.Category==2 || row.entity.Category==3 ||row.entity.Id == 5" data-toggle="modal" data-target="#DuplicateEntityTypeModel"><i class="icon-copy"></i></a> ';
        $scope.filterRootlevel = {
            filterText: ''
        };
        $scope.gridentityattributerelationrootlevel = {
            data: 'entitytpesdatarootlevel',
            enablePinning: false,
            filterOptions: $scope.filterRootlevel,
            columnDefs: [{
                field: "Id",
                displayName: 'Page type id',
                width: 100
            }, {
                field: "Caption",
                displayName: 'Page type name',
                width: 180
            }, {
                field: "Description",
                displayName: 'Description',
                width: 180
            }, {
                field: "ModuleCaption",
                displayName: 'Module name',
                width: 180
            }, {
                field: "IsRootLevel",
                displayName: 'Root level',
                width: 100
            }, {
                field: "IsAssociate",
                displayName: 'Associate',
                width: 100
            }, {
                field: "WorkFlowName",
                displayName: 'WorkFlow',
                width: 100,
                visible: false
            }, {
                field: "WorkFlowID",
                displayName: 'WorkFlowID',
                width: 100,
                visible: false
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.entitytypeduplicate,
                width: 30
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.editentitytypetableInPopup,
                width: 30
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.entitytypedelete,
                width: 40
            }]
        };
        var categoryid = '';
        $scope.GetEntityTypeByID = function GetEntityTypeByID(row) {
            $scope.IsDisabled = false;
            IsSaveClose = false;
            $scope.ShowEntityTypeRoleAcl = !row.entity.IsAssociate;
            var EntityTypeID = row.entity.Id;
            if (row.entity.IsAssociate == false) {
                MetadataService.GetEntityTypeRoleAcl(EntityTypeID).then(function (GetEntityRolesobjResult) {
                    if (GetEntityRolesobjResult.Response != null) {
                        $scope.SelctedEntityRoledIDs = {};
                        $scope.DefaultEntityRoleCollection = {};
                        $scope.SelctedEntityRoledIDs = GetEntityRolesobjResult.Response.m_Item2;
                        $scope.DefaultEntityRoleCollection = GetEntityRolesobjResult.Response.m_Item1;
                    } else {
                        if (EntityTypeID == SystemDefinedEntityTypes.CostCentre || EntityTypeID == SystemDefinedEntityTypes.Objective) {
                            GetAllEntityRole(true);
                        } else {
                            GetAllEntityRole(false);
                        }
                    }
                });
            }
            $scope.AddorEdit = 1;
            $scope.visiblesaveclose = true;
            $scope.IsAssociate = row.entity.IsAssociate;
            $scope.Category = 0;
            $scope.EntityID = row.entity.Id;
            if (row.entity.Category == 3) {
                $scope.showStatusOptions = false;
                $scope.Categorformatey = 0;
            } else {
                $scope.showStatusOptions = true;
                $scope.Category = 2;
            }
            if (row.entity.Id == 5) {
                $scope.showStatusOptions = true;
                $scope.Category = 2;
            }
            categoryid = row.entity.Category;
            $scope.step = 0;
            $scope.EntType.ddlParententitytypeId = [];
            $scope.EntType.ddlEntityFeatureID = '';
            $scope.ParententityDropdowndata = [];
            $scope.EntityHierarchyTypesResult = [];
            $scope.Fetureattribues = [];
            $scope.AttributeOptionvalues = [];
            $scope.EnableEntitytypeAttributeUpdate = false;
            $scope.entitytypeattributrearr = [];
            $scope.EntType.EntitytperelCaption = '';
            $scope.EntType.ddlAttributeID = '';
            $scope.EntType.ddlWorkFlow = '';
            $scope.EntType.Defaultvalue = '';
            $scope.EntType.MultiselectDefaultvalue = '';
            $scope.EntType.TextDefaultvalue = '';
            $scope.EntType.PlaceHolderTextValue = '';
            $scope.EntType.MinValue = 0;
            $scope.EntType.MaxValue = 0;
            $scope.EntType.Inheritfromparent = false;
            $scope.EntType.Isreadonly = false;
            $scope.EntType.Choosefromparentonly = false;
            $scope.EntType.Isvalidationneeded = false;
            $scope.EntType.IsRootLevel = false;
            $scope.EnableMultiselectFromParent = false;
            $scope.EntityHierarchyTypesResult = [];
            $scope.Fetureattribues = [];
            $timeout(function () {
                $(".pick-a-color").pickAColor({
                    showSpectrum: true,
                    showSavedColors: false,
                    saveColorsPerElement: true,
                    fadeMenuToggle: true,
                    showAdvanced: true,
                    showHexInput: true,
                    showBasicColors: true
                });
            }, 100);
            $scope.AdminTaskCheckList = [{
                ID: 0,
                StatusOptions: "",
                IsDeleted: false,
                SortOrder: 1,
                IsExisting: false
            }];
            $scope.AdminTaskCheckList = [{
                ID: 0,
                StatusOptions: "",
                IsDeleted: false,
                SortOrder: 1,
                IsExisting: false
            }];
            MetadataService.GetEntityStatusOptions(row.entity.Id).then(function (entitytypestatus) {
                $scope.AdminTaskCheckList = entitytypestatus.Response;
                if (entitytypestatus.Response == null) {
                    $scope.AdminTaskCheckList = [{
                        ID: 0,
                        StatusOptions: "",
                        IsDeleted: false,
                        SortOrder: 1,
                        IsExisting: false
                    }];
                }
            });
            MetadataService.GettingEntityTypeHierarchyForChildActivityType(row.entity.Id).then(function (GetEntityHierarchyForChildActivityIDResult) {
                if (GetEntityHierarchyForChildActivityIDResult.Response.length > 0) {
                    $scope.EntType.ShowHideRootLevel = false;
                } else {
                    $scope.EntType.ShowHideRootLevel = true;
                }
            });
            $scope.Features = [];
            MetadataService.GettingChildEntityTypes($scope.EntityID).then(function (GerParentEntityData) {
                $scope.childrenEntitytpesdata = GerParentEntityData.Response;
                $scope.tagAllOptions.data.splice(0, $scope.tagAllOptions.data.length);
                if (GerParentEntityData.Response != null) {
                    $.each(GerParentEntityData.Response, function (i, el) {
                        $scope.tagAllOptions.data.push({
                            "id": el.Id,
                            "text": el.Caption,
                            "ShortDescription": el.ShortDescription,
                            "ColorCode": el.ColorCode
                        });
                    });
                }
                MetadataService.GettingEntityTypeHierarchy($scope.EntityID).then(function (GetEntityHierarchyByIDResult) {
                    $scope.EntityHietemp = GetEntityHierarchyByIDResult.Response;
                    if ($scope.EntityHietemp != null && $scope.EntityHietemp.length > 0) {
                        for (var uti = 0; uti < $scope.EntityHietemp.length; uti++) {
                            for (var k = 0; k < $scope.tagAllOptions.data.length; k++) {
                                if ($scope.tagAllOptions.data[k].id == $scope.EntityHietemp[uti].ChildActivityTypeID) {
                                    $scope.EntityHierarchyTypesResult.push($scope.tagAllOptions.data[k]);
                                }
                            }
                        }
                        $scope.EntType.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                    }
                });
                MetadataService.GetFeature().then(function (FeatureData) {
                    $scope.Features = FeatureData.Response;
                    MetadataService.GetEntityTypefeatureByID($scope.EntityID).then(function (entityTypefeature) {
                        var ase = entityTypefeature.Response;
                        if (ase != undefined) for (var i = 0; i < ase.length; i++) {
                            $scope.Fetureattribues.push(ase[i].FeatureID);
                        }
                        $scope.EntType.ddlEntityFeatureID = $scope.Fetureattribues;
                    });
                    MetadataService.GetEntityTypeAttributeRelationByID($scope.EntityID).then(function (entityTypeAttributeRelation) {
                        if (entityTypeAttributeRelation.Response != null) {
                            $scope.entityattributes = entityTypeAttributeRelation.Response;
                            $scope.count = 1;
                            $scope.validationcolorid = "";
                            for (var i = 0; i < $scope.entityattributes.length; i++) {
                                if ($scope.AtributeValueList.length != 0) {
                                    $scope.validationcolorid = $.grep($scope.AtributeValueList, function (e) {
                                        return e.AttributeID == parseInt($scope.entityattributes[i].AttributeID);
                                    });
                                }
                                if ($scope.validationcolorid.length != 0) {
                                    $scope.entitytypeattributrearr.push({
                                        "Id": $scope.entityattributes[i].ID,
                                        "AttributeID": parseInt($scope.entityattributes[i].AttributeID),
                                        "ValidationID": 1,
                                        "SortOrder": $scope.entityattributes[i].SortOrder,
                                        "DefaultValue": $scope.entityattributes[i].DefaultValue,
                                        "InheritFromParent": $scope.entityattributes[i].InheritFromParent,
                                        "IsReadOnly": $scope.entityattributes[i].IsReadOnly,
                                        "ChooseFromParentOnly": $scope.entityattributes[i].ChooseFromParentOnly,
                                        "IsValidationNeeded": $scope.entityattributes[i].IsValidationNeeded,
                                        "Caption": $scope.entityattributes[i].Caption,
                                        "IsSystemDefined": $scope.entityattributes[i].IsSystemDefined,
                                        "ID": $scope.entityattributes[i].ID,
                                        "EntityTypeName": $scope.entityattributes[i].EntityTypeCaption,
                                        "AttributeTypeID": $scope.entityattributes[i].AttributeTypeID,
                                        "AttributeCaption": $scope.entityattributes[i].AttributeCaption,
                                        "PlaceHolderValue": $scope.entityattributes[i].PlaceHolderValue,
                                        "MinValue": $scope.entityattributes[i].MinValue,
                                        "MaxValue": $scope.entityattributes[i].MaxValue,
                                        "isvalidationset": 1,
                                        "IsHelptextEnabled": $scope.entityattributes[i].IsHelptextEnabled,
                                        "HelptextDecsription": $scope.entityattributes[i].HelptextDecsription
                                    });
                                } else $scope.entitytypeattributrearr.push({
                                    "Id": $scope.entityattributes[i].ID,
                                    "AttributeID": parseInt($scope.entityattributes[i].AttributeID),
                                    "ValidationID": 1,
                                    "SortOrder": $scope.entityattributes[i].SortOrder,
                                    "DefaultValue": $scope.entityattributes[i].DefaultValue,
                                    "InheritFromParent": $scope.entityattributes[i].InheritFromParent,
                                    "IsReadOnly": $scope.entityattributes[i].IsReadOnly,
                                    "ChooseFromParentOnly": $scope.entityattributes[i].ChooseFromParentOnly,
                                    "IsValidationNeeded": $scope.entityattributes[i].IsValidationNeeded,
                                    "Caption": $scope.entityattributes[i].Caption,
                                    "IsSystemDefined": $scope.entityattributes[i].IsSystemDefined,
                                    "ID": $scope.entityattributes[i].ID,
                                    "EntityTypeName": $scope.entityattributes[i].EntityTypeCaption,
                                    "AttributeTypeID": $scope.entityattributes[i].AttributeTypeID,
                                    "AttributeCaption": $scope.entityattributes[i].AttributeCaption,
                                    "PlaceHolderValue": $scope.entityattributes[i].PlaceHolderValue,
                                    "MinValue": $scope.entityattributes[i].MinValue,
                                    "MaxValue": $scope.entityattributes[i].MaxValue,
                                    "isvalidationset": 0,
                                    "IsHelptextEnabled": $scope.entityattributes[i].IsHelptextEnabled,
                                    "HelptextDecsription": $scope.entityattributes[i].HelptextDecsription
                                });
                                $scope.globalentitytyperelation.entitytyperel.push({
                                    "Id": $scope.entityattributes[i].ID,
                                    "AttributeID": parseInt($scope.entityattributes[i].AttributeID),
                                    "ValidationID": 1,
                                    "SortOrder": $scope.entityattributes[i].SortOrder,
                                    "DefaultValue": $scope.entityattributes[i].DefaultValue,
                                    "InheritFromParent": $scope.entityattributes[i].InheritFromParent,
                                    "IsReadOnly": $scope.entityattributes[i].IsReadOnly,
                                    "ChooseFromParentOnly": $scope.entityattributes[i].ChooseFromParentOnly,
                                    "IsValidationNeeded": $scope.entityattributes[i].IsValidationNeeded,
                                    "Caption": $scope.entityattributes[i].Caption,
                                    "IsSystemDefined": $scope.entityattributes[i].IsSystemDefined,
                                    "ID": $scope.entityattributes[i].ID,
                                    "EntityTypeName": $scope.entityattributes[i].EntityTypeCaption,
                                    "AttributeTypeID": $scope.entityattributes[i].AttributeTypeID,
                                    "AttributeCaption": $scope.entityattributes[i].AttributeCaption,
                                    "PlaceHolderValue": $scope.entityattributes[i].PlaceHolderValue,
                                    "MinValue": $scope.entityattributes[i].MinValue,
                                    "MaxValue": $scope.entityattributes[i].MaxValue,
                                    "IsHelptextEnabled": $scope.entityattributes[i].IsHelptextEnabled,
                                    "HelptextDecsription": $scope.entityattributes[i].HelptextDecsription
                                });
                                $scope.count = $scope.entityattributes[i].ID + 1;
                            }
                        }
                    });
                });
            });
            try {
                MetadataService.GetEntityTypeAttributeGroupRelation($scope.EntityID, 0, 0).then(function (attributeGrpRelation) {
                    if (attributeGrpRelation.Response != null) {
                        $scope.attributegroupGridData = [];
                        var LocationTypeCaption = "";
                        for (var i = 0; i < attributeGrpRelation.Response.length; i++) {
                            var attributeCaption = $.grep($scope.attributegroupList, function (e) {
                                return e.Id == parseInt(attributeGrpRelation.Response[i].AttributeGroupID);
                            })[0].Caption;
                            if (attributeGrpRelation.Response[i].LocationType == 1) LocationTypeCaption = "Append with detail block";
                            else if (attributeGrpRelation.Response[i].LocationType == 2) LocationTypeCaption = "Represent as separate block";
                            else if (attributeGrpRelation.Response[i].LocationType == 3) LocationTypeCaption = "Represent as Tab";
                            $scope.attributegroupGridData.push({
                                "ID": attributeGrpRelation.Response[i].ID,
                                "AttributeGroupID": attributeGrpRelation.Response[i].AttributeGroupID,
                                "AttributeGroupCaption": attributeCaption,
                                "Caption": attributeGrpRelation.Response[i].Caption,
                                "LocationType": attributeGrpRelation.Response[i].LocationType,
                                "RepresentationType": attributeGrpRelation.Response[i].RepresentationType,
                                "EntityTypeID": $scope.EntityID,
                                "SortOrder": 0,
                                "AttributeGroupGlobalAccess": attributeGrpRelation.Response[i].GlobalAccess,
                                "LocationTypeCaption": LocationTypeCaption,
                                "RepresentationTypeCaption": attributeGrpRelation.Response[i].RepresentationType == false ? "Key Value Pair" : "Populate as List",
                                "PageSize": attributeGrpRelation.Response[i].PageSize,
                                "IsTabInEntityCreation": attributeGrpRelation.Response[i].IsTabInEntityCreation
                            });
                        }
                    }
                });
            } catch (e) { }
            $scope.EntType.EntityID = row.entity.Id;
            $scope.EntType.EntityID = row.entity.Id;
            $scope.EntType.EntityTypeCaption = row.entity.Caption;
            $scope.EntType.EntityTypeDescription = row.entity.Description;
            $scope.EntType.Shortdescription = row.entity.ShortDescription;
            $scope.EntType.IsRootLevel = row.entity.IsRootLevel;
            if (row.entity.ColorCode != "Null") {
                $scope.ColorCodeGlobal = row.entity.ColorCode;
                $scope.ColorCodeGlobalObj.colorcode = $scope.ColorCodeGlobal;
            }
            $timeout(function () {
                $("#appendedPrependedDropdownButton input").val($scope.ColorCodeGlobal);
                $(".current-color").css('background-color', '#' + $scope.ColorCodeGlobal);
            }, 500);
            var entityModule = $.grep($scope.modules, function (e) {
                return e.Id == row.entity.ModuleID;
            });
            $scope.EntType.ddlModuleID = entityModule[0].Id;
            var entityWorkFlowname = $.grep($scope.modules, function (e) {
                return e.Id == row.entity.WorkFlowID;
            });
            $scope.EntType.ddlWorkFlow = row.entity.WorkFlowID;
            $scope.EnableUpdate = true;
            $scope.EnableAdd = false;
            $scope.EnableEntitytypeAttributeAdd = true;
            $scope.EnableEntityfeatureAdd = true;
            row = '';
            $timeout(function () {
                $('#EntityTypeCaption').focus().select();
            }, 1500);
            ValidationGet();
        };
        $scope.globalentitytyperelation = {
            entitytyperel: []
        };
        $scope.DeleteEntityTypeByID = function DeleteEntityTypeByID(row) {
            if (row.entity.IsAssociate == true) {
                bootbox.alert($translate.instant('LanguageContents.Res_1818.Caption'));
                return true;
            }
            bootbox.confirm($translate.instant('LanguageContents.Res_5523.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var ID = row.entity.Id;
                        MetadataService.DeleteEntityType(ID).then(function (deleteentitytypebyId) {
                            if (deleteentitytypebyId.Response == 2) {
                                bootbox.alert($translate.instant('LanguageContents.Res_5520.Caption'));
                            } else if (deleteentitytypebyId.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_5521.Caption'));
                            } else if (deleteentitytypebyId.StatusCode == 401) { } else {
                                var index = $scope.entitytpesdatarootlevel.indexOf(row.entity)
                                $scope.entitytpesdatarootlevel.splice(index, 1);
                                NotifySuccess($translate.instant('LanguageContents.Res_5522.Caption'));
                            }
                        });
                    }, 100);
                }
            });
        };
        $scope.GetAttributeByEntityTypeID = function GetEntityTypeByID(row) {
            $scope.enabledisableCmsattributes = false;
            //if ($scope.IsAssociate == true && row.AttributeID != 56) {
            //    bootbox.alert($translate.instant('LanguageContents.Res_1819.Caption'));
            //    return true;
            //}
            if ((row.AttributeID == 68 || row.AttributeID == 69 || row.AttributeID == 109) || ($scope.IsAssociate == true && row.AttributeID != 56)) {
                $scope.enabledisableCmsattributes = true;
            }
            $scope.RowIndex = row.Id;
            $scope.sortorder = row.SortOrder;
            $scope.AttributeOptionvalues = [];
            $scope.Enableinputtxt = false;
            $scope.EnablePlaceHolder = false;
            $scope.EnableSingleSelection = false;
            $scope.EnableMultiselectddl = false;
            $scope.EnableEntitytypeAttributeUpdate = true;
            $scope.EnableEntitytypeAttributeAdd = false;
            $scope.EnableMultiselectFromParent = false;
            $scope.EntType.EntitytperelCaption = '';
            $scope.EntType.ddlAttributeID = 0;
            $scope.AttributeOptionvalues = [];
            $scope.EntType.Inheritfromparent = row.InheritFromParent;
            $scope.EntType.Isreadonly = row.IsReadOnly;
            $scope.EntType.Choosefromparentonly = row.ChooseFromParentOnly;
            $scope.EntType.Isvalidationneeded = false;
            $scope.EntityAttribureRowID = 0;
            $scope.attributeObjCaption = '';
            $scope.EntityAttribureRowID = row.ID;
            var ID = row.AttributeID;
            $scope.EntType.MinValue = row.MinValue;
            $scope.EntType.MaxValue = row.MaxValue;
            $scope.EntType.IsHelptextEnabled = row.IsHelptextEnabled;
            $scope.EnableMultiselectFromParent = row.HelptextDecsription;
            var entityAttribute = $.grep($scope.attributes, function (e) {
                return e.Id == row.AttributeID;
            });
            $scope.EntType.ddlAttributeID = entityAttribute[0].Id;
            $scope.attributeObjAttributeTypeId = entityAttribute[0].AttributeTypeID;
            MetadataService.GetAdminOptionListID(ID).then(function (GetAttributeOptions) {
                $scope.AttributeOptionvalues = GetAttributeOptions.Response;
                if (entityAttribute[0].AttributeTypeID == 4) {
                    $scope.MinValue = false;
                    $scope.MaxValue = false;
                    if (entityAttribute[0].IsSpecial == true) {
                        $scope.EnableMultiselectddl = false;
                        $scope.EnableSingleSelection = false;
                        $scope.Enableinputtxt = false;
                        $scope.EnablePlaceHolder = false;
                    } else {
                        $scope.EnableMultiselectddl = true;
                        $scope.EnableSingleSelection = false;
                        $scope.Enableinputtxt = false;
                        $scope.EnablePlaceHolder = false;
                    }
                    var multiselectarr = [];
                    $scope.EntType.MultiselectDefaultvalue = [];
                    var defaultmultiselectvalue = row.DefaultValue.split(',');
                    if (row.DefaultValue != "") {
                        for (var j = 0; j < defaultmultiselectvalue.length; j++) {
                            $scope.EntType.MultiselectDefaultvalue.push(defaultmultiselectvalue[j]);
                        }
                    }
                } else if (entityAttribute[0].AttributeTypeID == 3) {
                    $scope.MinValue = false;
                    $scope.MaxValue = false;
                    var AttributeSingleselectionVal = $.grep($scope.AttributeOptionvalues, function (e) {
                        return e.Id == parseInt(row.DefaultValue);
                    });
                    if (entityAttribute[0].IsSpecial == true) {
                        $scope.EnableMultiselectddl = false;
                        $scope.EnableSingleSelection = false;
                        $scope.Enableinputtxt = false;
                        $scope.EnablePlaceHolder = false;
                    } else {
                        $scope.EnableSingleSelection = true;
                    }
                    $scope.EntType.Defaultvalue = '';
                    if (AttributeSingleselectionVal[0] != undefined) $scope.EntType.Defaultvalue = AttributeSingleselectionVal[0].Id;
                } else if (entityAttribute[0].AttributeTypeID == 1 || row.AttributeTypeID == 2) {
                    $scope.MinValue = false;
                    $scope.MaxValue = false;
                    if (entityAttribute[0].IsSpecial == true) {
                        $scope.EnableMultiselectddl = false;
                        $scope.EnableSingleSelection = false;
                        $scope.Enableinputtxt = false;
                        $scope.EnablePlaceHolder = false;
                    } else {
                        $scope.Enableinputtxt = true;
                        $scope.EnablePlaceHolder = true;
                    }
                    if (entityAttribute[0].Id == 68) $scope.Enableinputtxt = true;
                    $scope.EntType.TextDefaultvalue = '';
                    $scope.txtDefaultvalue = row.DefaultValue;
                    $scope.EntType.TextDefaultvalue = row.DefaultValue;
                    $scope.txtPlaceHolder = row.PlaceHolderValue;
                    $scope.EntType.PlaceHolderTextValue = row.PlaceHolderValue;
                }
            });
            if (entityAttribute[0].AttributeTypeID == 1 || entityAttribute[0].AttributeTypeID == 2) {
                $scope.MinValue = false;
                $scope.MaxValue = false;
                if (entityAttribute[0].IsSpecial == true) {
                    $scope.EnableMultiselectddl = false;
                    $scope.EnableSingleSelection = false;
                    $scope.Enableinputtxt = false;
                    $scope.EnablePlaceHolder = false;
                } else {
                    $scope.Enableinputtxt = true;
                    $scope.EnablePlaceHolder = true;
                }
            }
            if (entityAttribute[0].AttributeTypeID == 3 || entityAttribute[0].AttributeTypeID == 4 || entityAttribute[0].AttributeTypeID == 6 || entityAttribute[0].AttributeTypeID == 7 || entityAttribute[0].AttributeTypeID == 12) {
                $scope.MinValue = false;
                $scope.MaxValue = false;
                if (entityAttribute[0].IsSpecial == true) {
                    $scope.EnableMultiselectddl = false;
                    $scope.EnableSingleSelection = false;
                    $scope.Enableinputtxt = false;
                    $scope.EnablePlaceHolder = false;
                } else {
                    $scope.EnableMultiselectFromParent = true;
                }
            }
            if (entityAttribute[0].AttributeTypeID == 5 || entityAttribute[0].AttributeTypeID == 16) {
                $scope.EnableMultiselectddl = false;
                $scope.EnableSingleSelection = false;
                $scope.Enableinputtxt = false;
                $scope.EnablePlaceHolder = false;
                $scope.MinValue = true;
                $scope.MaxValue = true;
            }
            if (row.AttributeTypeID == 1 || row.AttributeTypeID == 2) {
                $scope.EntType.TextDefaultvalue = row.DefaultValue;
                $scope.EntType.PlaceHolderTextValue = row.PlaceHolderValue;
            } else if (row.AttributeTypeID == 3) {
                $scope.EntType.Defaultvalue = row.DefaultValue;
            } else if (row.AttributeTypeID == 4) {
                $scope.EntType.MultiselectDefaultvalue = [];
                var arryDefaultVal = row.DefaultValue.split(",");
                for (var j = 0; j < arryDefaultVal.length; j++) {
                    $scope.EntType.MultiselectDefaultvalue.push(arryDefaultVal[j].toString());
                }
            } else if (row.AttributeTypeID == 5 || row.AttributeTypeID == 16) {
                $scope.EntType.MinValue = row.MinValue;
                $scope.EntType.MaxValue = row.MaxValue;
            } else {
                $scope.EntType.TextDefaultvalue = row.DefaultValue;
            }
            $scope.EntType.entityattrelID = row.AttributeID;
            $scope.EntType.EntitytperelCaption = row.Caption;
            $scope.EntType.Colourcode = row.ColourCode;
            $scope.EntType.Inheritfromparent = row.InheritFromParent;
            $scope.EntType.Isreadonly = row.IsReadOnly;
            $scope.EntType.Choosefromparentonly = row.ChooseFromParentOnly;
        };
        MetadataService.GetModule().then(function (module) {
            $scope.modules = $.grep(module.Response, function (e) {
                return e.Id == 6;
            });
            MetadataService.GetAttribute().then(function (attribute) {
                var EntityStatusattribute = attribute.Response;
                $scope.attributes = attribute.Response;
                $scope.attributeslist = $.grep(attribute.Response, function (e) {
                    return (e.Id != 71 && e.Id != 74 && e.Id != 75 && e.Id != 77);
                });
                $.each($scope.attributeslist, function (i, el) {
                    $scope.Attrs.push({
                        "id": el.Id,
                        "text": el.Caption,
                    });
                });
                $scope.optionsSrc = $scope.Attrs;
                $scope.formatAttribute = function (item) {
                    return "<span style=\"background-color: #ccc\" class=\"eicon-s select2-result-eicon\">" + item.id + "</span><span class=\"select2-result-labelTxt\">" + item.text + "</span>";
                };
                $scope.formatAttributeSelection = function (item) {
                    if ($scope.EntType.ddlAttributeID != "") return "<span style=\"background-color: #ccc\" class=\"eicon-s select2-result-eicon\">" + item.id + "</span><span class=\"select2-result-labelTxt\">" + item.text + "</span>";
                };
                $scope.attributeOptionsConfig = {
                    formatResult: $scope.formatAttribute,
                    formatSelection: $scope.formatAttributeSelection
                };
                MetadataService.GetFeature().then(function (FeatureData) {
                    $scope.Features = FeatureData.Response;
                });
            });
        });
        $scope.count = 1;
        $scope.entitytypeattributerel = function () {
            $scope.enabledisableCmsattributes = false;
            $("#btnTemp1").click();
            $("#entitytypeformPage2").removeClass('notvalidate');
            if ($("#entitytypeformPage2 .error").length > 0) {
                return false;
            }
            $timeout(function () {
                applyAlternateColor();
            }, 100);
            var MinDefaultvalue = 0;
            var MaxDefaultvalue = 0;
            var Defaultvalue = '';
            var Placeholder = '';
            if ($scope.attributeObjAttributeTypeId == 1 || $scope.attributeObjAttributeTypeId == 2) {
                Defaultvalue = $scope.EntType.TextDefaultvalue == null ? "" : $scope.EntType.TextDefaultvalue;
                Placeholder = $scope.EntType.PlaceHolderTextValue == null ? "" : $scope.EntType.PlaceHolderTextValue;
            } else if ($scope.attributeObjAttributeTypeId == 3) {
                Defaultvalue = $scope.EntType.Defaultvalue == null ? "" : $scope.EntType.Defaultvalue.toString();
            } else if ($scope.attributeObjAttributeTypeId == 4) {
                Defaultvalue = $scope.EntType.MultiselectDefaultvalue == null ? "" : $scope.EntType.MultiselectDefaultvalue.toString();
            } else if ($scope.attributeObjAttributeTypeId == 5 || $scope.attributeObjAttributeTypeId == 16) {
                if ($scope.EntType.MaxValue != 0) {
                    if ($scope.EntType.MaxValue > $scope.EntType.MinValue) {
                        MinDefaultvalue = $scope.EntType.MinValue == null ? 0 : $scope.EntType.MinValue;
                        MaxDefaultvalue = $scope.EntType.MaxValue == null ? 0 : $scope.EntType.MaxValue;
                    } else {
                        bootbox.alert($translate.instant('LanguageContents.Res_5726.Caption'));
                        return false;
                    }
                } else {
                    MinDefaultvalue = $scope.EntType.MinValue == null ? 0 : $scope.EntType.MinValue;
                    MaxDefaultvalue = $scope.EntType.MaxValue == null ? 0 : $scope.EntType.MaxValue;
                }
            } else {
                Defaultvalue = $scope.EntType.TextDefaultvalue == null ? "" : $scope.EntType.TextDefaultvalue;
            }
            if ($scope.EntType.EntitytperelCaption == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_1820.Caption'));
                return false;
            }
            if ($scope.EntType.ddlAttributeID == null) {
                bootbox.alert($translate.instant('LanguageContents.Res_1821.Caption'));
                return false;
            }
            if ($scope.EntType.entityattrelID != 0 && $scope.EntType.entityattrelID != undefined) {
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].AttributeID = $scope.EntType.ddlAttributeID;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].ValidationID = 1;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].SortOrder = $scope.sortorder;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].DefaultValue = Defaultvalue;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].PlaceHolderValue = Placeholder;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].MinValue = MinDefaultvalue;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].MaxValue = MaxDefaultvalue;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].InheritFromParent = $scope.EntType.Inheritfromparent;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].IsReadOnly = $scope.EntType.Isreadonly;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].ChooseFromParentOnly = $scope.EntType.Choosefromparentonly;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].IsValidationNeeded = $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].IsValidationNeeded, $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].Caption = $scope.EntType.EntitytperelCaption;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].IsSystemDefined = $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].IsSystemDefined, $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].ID = $scope.EntityAttribureRowID;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].EntityTypeName = $scope.EntType.EntityTypeCaption;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].AttributeName = $.grep($scope.attributes, function (e) {
                    return e.Id == $scope.EntType.entityattrelID;
                })[0].Caption;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].AttributeTypeID = $scope.attributeObjAttributeTypeId;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].AttributeCaption = $.grep($scope.attributes, function (e) {
                    return e.Id == $scope.EntType.entityattrelID;
                })[0].Caption;
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].IsHelptextEnabled = $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].IsHelptextEnabled,
                $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].HelptextDecsription = $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.Id == parseInt($scope.RowIndex);
                })[0].HelptextDecsription,
                $scope.EnableEntitytypeAttributeUpdate = false;
                $scope.EnableEntitytypeAttributeAdd = true;
                $scope.EntType.entityattrelID = 0;
            } else {
                if ($.grep($scope.entitytypeattributrearr, function (e) {
					return e.AttributeID == $scope.EntType.ddlAttributeID;
                }).length > 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1822.Caption'));
                    $scope.AttributeMultiselectDefaultValue = '';
                    $scope.Enableinputtxt = false;
                    $scope.EnablePlaceHolder = false;
                    $scope.EnableSingleSelection = false;
                    $scope.EnableMultiselectddl = false;
                    $scope.EntType.EntitytperelCaption = '', $scope.EntType.ddlAttributeID = 0, $scope.EntType.Defaultvalue = '', $scope.EntType.MultiselectDefaultvalue = '', $scope.EntType.TextDefaultvalue = '', $scope.EntType.PlaceHolderTextValue = '', $scope.EntType.Inheritfromparent = false, $scope.EntType.Isreadonly = false, $scope.EntType.Choosefromparentonly = false, $scope.EntType.Isvalidationneeded = false, $scope.attributeObjAttributeTypeId = 0;
                    $scope.attributeObjCaption = '';
                    $scope.EnableMultiselectFromParent = false;
                    $scope.MinValue = false;
                    $scope.MaxValue = false;
                    $scope.EntType.HelptextDecsription == null ? "" : $scope.EntType.HelptextDecsription;
                    $scope.EntType.IsHelptextEnabled == null ? "" : $scope.EntType.IsHelptextEnabled;
                    return false;
                }
                var sortorder = 0;
                for (var i = 0; i < $scope.entitytypeattributrearr.length; i++) {
                    if ($scope.entitytypeattributrearr[i].SortOrder > sortorder) {
                        sortorder = $scope.entitytypeattributrearr[i].SortOrder;
                    }
                }
                $scope.entitytypeattributrearr.push({
                    "Id": $scope.count,
                    "AttributeID": parseInt($scope.EntType.ddlAttributeID),
                    "ValidationID": 1,
                    "SortOrder": sortorder + 1,
                    "DefaultValue": Defaultvalue,
                    "InheritFromParent": $scope.EntType.Inheritfromparent,
                    "IsReadOnly": $scope.EntType.Isreadonly,
                    "ChooseFromParentOnly": $scope.EntType.Choosefromparentonly,
                    "IsValidationNeeded": true,
                    "Caption": $scope.EntType.EntitytperelCaption,
                    "IsSystemDefined": false,
                    "ID": 0,
                    "EntityTypeName": $scope.EntType.EntityTypeCaption,
                    "AttributeTypeID": $scope.attributeObjAttributeTypeId,
                    "AttributeCaption": $scope.attributeObjCaption,
                    "PlaceHolderValue": Placeholder,
                    "MinValue": $scope.EntType.MinValue,
                    "MaxValue": $scope.EntType.MaxValue,
                    "isvalidationset": 0,
                    "IsHelptextEnabled": $scope.EntType.IsHelptextEnabled,
                    "HelptextDecsription": $scope.EntType.HelptextDecsription
                });
                $scope.AttributeIDList.push($scope.EntType.ddlAttributeID);
            }
            $scope.AttributeMultiselectDefaultValue = '';
            $scope.Enableinputtxt = false;
            $scope.EnablePlaceHolder = false;
            $scope.EnableSingleSelection = false;
            $scope.EnableMultiselectddl = false;
            $scope.EntType.EntitytperelCaption = '', $scope.EntType.ddlAttributeID = 0, $scope.EntType.Defaultvalue = '', $scope.EntType.MultiselectDefaultvalue = '', $scope.EntType.TextDefaultvalue = '', $scope.EntType.PlaceHolderTextValue = '', $scope.EntType.Inheritfromparent = false, $scope.EntType.Isreadonly = false, $scope.EntType.Choosefromparentonly = false, $scope.EntType.Isvalidationneeded = false, $scope.attributeObjAttributeTypeId = 0;
            $scope.attributeObjCaption = '';
            $scope.EnableMultiselectFromParent = false;
            $scope.count = $scope.count + 1;
            $scope.MinValue = false;
            $scope.MaxValue = false;
            $scope.EntType.HelptextDecsription == null ? "" : $scope.EntType.HelptextDecsription;
            $scope.EntType.IsHelptextEnabled == null ? "" : $scope.EntType.IsHelptextEnabled;
        };
        $scope.DeleteAttributeByEntityTypeID = function DeleteAttributeByEntityTypeID(row) {
            if ($scope.IsAssociate == true) {
                bootbox.alert($translate.instant('LanguageContents.Res_1823.Caption'));
                return true;
            }
            bootbox.confirm($translate.instant('LanguageContents.Res_1824.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var ID = row.attribs.Id;
                        var attrDelObj = $.grep($scope.entitytypeattributrearr, function (e) {
                            return e.Id == ID;
                        });
                        $scope.entitytypeattributrearr.splice($.inArray(attrDelObj[0], $scope.entitytypeattributrearr), 1);
                        $timeout(function () {
                            applyAlternateColor();
                        }, 100);
                        MetadataService.DeleteEntityAttributeRelation(ID).then(function (deleteentityAttributeByTypeId) {
                            if (deleteentityAttributeByTypeId.SourceCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4294.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4792.Caption'));
                            }
                        });
                        $scope.AttributeMultiselectDefaultValue = '';
                        $scope.Enableinputtxt = false;
                        $scope.EnablePlaceHolder = false;
                        $scope.EnableSingleSelection = false;
                        $scope.EnableMultiselectddl = false;
                        $scope.EntType.EntitytperelCaption = '', $scope.EntType.ddlAttributeID = 0, $scope.EntType.Defaultvalue = '', $scope.EntType.PlaceHolderTextValue = '', $scope.EntType.MinValue = 0, $scope.EntType.MaxValue = 0, $scope.EntType.MultiselectDefaultvalue = '', $scope.EntType.TextDefaultvalue = '', $scope.EntType.Inheritfromparent = false, $scope.EntType.Isreadonly = false, $scope.EntType.Choosefromparentonly = false, $scope.EntType.Isvalidationneeded = false, $scope.attributeObjAttributeTypeId = 0;
                        $scope.attributeObjCaption = '';
                        $scope.EnableEntitytypeAttributeUpdate = false;
                        $scope.EnableEntitytypeAttributeAdd = true;
                        $scope.EntType.entityattrelID = 0;
                        $scope.EnableMultiselectFromParent = false;
                        $scope.EntType.HelptextDecsription = "";
                        $scope.EntType.IsHelptextEnabled = 0;
                    }, 100);
                }
            });
        };
        $scope.ConditionDataArr = [];

        function collectionObjectiveCondition() {
            var EntttyTypeArr = new Array();
            var attriArreArr = new Array();
            var optionArr = [];
            $scope.ConditionDataArr = [];
            $('#AttributeToAttributeRelationsBody div[data-Holder="holder"]').each(function (index) {
                var tempattributeid = '';
                var tempattributelevel = '';
                var uniquekey = $(this).attr('data-id');
                if (($('#ObjAttributes' + uniquekey).val() != "") && ($('#ObjAttributes' + uniquekey).val() != undefined) && ($('#ObjAttributes' + uniquekey).val() != "0")) {
                    var tempid = $('#ObjAttributes' + uniquekey).val();
                    tempattributeid = tempid.substring(0, tempid.lastIndexOf('_'));
                    tempattributelevel = tempid.substring((tempid.lastIndexOf('_') + 1), tempid.length);
                    var collectionarr = {
                        'AttributeID': tempattributeid,
                        'AttributeLevelID': tempattributelevel,
                        'AttributeOptionID': $('#ObjAttributeOptions' + uniquekey).val(),
                        'AttributeToAttributeRelationID': ($('#ObjAttributesRelations' + uniquekey).val()).toString().split(',').toString()
                    };
                    $scope.ConditionDataArr.push(collectionarr);
                }
            });
        }
        $scope.handleNext = function (isForceful) {
            $scope.enabledisableCmsattributes = false;
            var entitystatusavailability = $.grep($scope.entitytypeattributrearr, function (e) {
                return e.AttributeID == 71;
            });
            var thisobjj = this;
            if ($scope.getCurrentStep() == "EnityType") {
                $timeout(function () { $scope.closevalidationpopup(thisobjj); }, 100);
            }
            $timeout(function () {
                applyAlternateColor();
            }, 100);
            if ($scope.isLastStep() || isForceful) {
                $scope.IsDisabled = true;
                if ($scope.EntityID == 5 || $scope.Category == 2) {
                    if (entitystatusavailability.length == 0) {
                        var attributeObj = [];
                        attributeObj = $.grep($scope.attributes, function (e) {
                            return e.Id == parseInt(71);
                        });
                        $scope.entitytypeattributrearr.push({
                            "Id": $scope.count,
                            "AttributeID": 71,
                            "ValidationID": 1,
                            "SortOrder": $scope.entitytypeattributrearr.length + 1,
                            "DefaultValue": "",
                            "InheritFromParent": false,
                            "IsReadOnly": false,
                            "ChooseFromParentOnly": false,
                            "IsValidationNeeded": true,
                            "Caption": attributeObj[0].Caption,
                            "IsSystemDefined": true,
                            "ID": 0,
                            "EntityTypeName": $scope.EntType.EntityTypeCaption,
                            "AttributeTypeID": 3,
                            "AttributeCaption": attributeObj[0].Caption,
                            "PlaceHolderValue": "",
                            "MinValue": 0,
                            "MaxValue": 0,
                            "IsHelptextEnabled": 0,
                            "HelptextDecsription": ""
                        });
                    }
                }
                var entityworkspaceAttr = $.grep($scope.entitytypeattributrearr, function (e) {
                    return e.AttributeID == 74 || e.AttributeID == 75;
                });
                if (entityworkspaceAttr.length == 0) {
                    $scope.entitytypeattributrearr.push({
                        "Id": $scope.count,
                        "AttributeID": 74,
                        "ValidationID": 1,
                        "SortOrder": $scope.entitytypeattributrearr.length + 1,
                        "DefaultValue": "",
                        "InheritFromParent": false,
                        "IsReadOnly": false,
                        "ChooseFromParentOnly": false,
                        "IsValidationNeeded": false,
                        "Caption": "MyRoleGlobalAccess",
                        "IsSystemDefined": true,
                        "ID": 0,
                        "EntityTypeName": $scope.EntType.EntityTypeCaption,
                        "AttributeTypeID": 4,
                        "AttributeCaption": "MyRoleGlobalAccess",
                        "PlaceHolderValue": "",
                        "MinValue": 0,
                        "MaxValue": 0,
                        "IsHelptextEnabled": 0,
                        "HelptextDecsription": ""

                    });
                    $scope.entitytypeattributrearr.push({
                        "Id": $scope.count,
                        "AttributeID": 75,
                        "ValidationID": 1,
                        "SortOrder": $scope.entitytypeattributrearr.length + 1,
                        "DefaultValue": "",
                        "InheritFromParent": false,
                        "IsReadOnly": false,
                        "ChooseFromParentOnly": false,
                        "IsValidationNeeded": false,
                        "Caption": "MyRoleEntityAccess",
                        "IsSystemDefined": true,
                        "ID": 0,
                        "EntityTypeName": $scope.EntType.EntityTypeCaption,
                        "AttributeTypeID": 4,
                        "AttributeCaption": "MyRoleEntityAccess",
                        "PlaceHolderValue": "",
                        "MinValue": 0,
                        "MaxValue": 0,
                        "IsHelptextEnabled": 0,
                        "HelptextDecsription": ""
                    });
                }
                var addEntityType = {};
                if ($scope.EntityID == 0 || $scope.EntityID == undefined) {
                    addEntityType.ID = 0;
                } else {
                    addEntityType.ID = $scope.EntityID;
                }
                if ($scope.ColorCodeGlobalObj.colorcode == undefined || $scope.ColorCodeGlobalObj.colorcode == "") {
                    bootbox.alert($translate.instant('LanguageContents.Res_1825.Caption'));
                    return false;
                }
                addEntityType.Caption = $scope.EntType.EntityTypeCaption;
                addEntityType.Description = $scope.EntType.EntityTypeDescription;
                addEntityType.ModuleID = $scope.EntType.ddlModuleID;
                addEntityType.Category = categoryid == 3 ? 3 : 2;
                addEntityType.WorkFlowID = $scope.EntType.ddlWorkFlow == "" ? 0 : $scope.EntType.ddlWorkFlow;
                addEntityType.WorkFlowID = addEntityType.WorkFlowID == undefined ? 0 : addEntityType.WorkFlowID;
                addEntityType.IsSystemDefined = 0;
                addEntityType.ShortDescription = $scope.EntType.Shortdescription;
                addEntityType.ColourCode = $scope.ColorCodeGlobalObj.colorcode.replace("#", "");
                addEntityType.IsAssociate = 0;
                addEntityType.IsRootLevel = $scope.EntType.IsRootLevel == undefined ? false : $scope.EntType.IsRootLevel;
                MetadataService.EntityType(addEntityType).then(function (getentityresult) {
                    if (getentityresult.StatusCode == 405) {
                        NotifyError($translate.instant('LanguageContents.Res_4337.Caption'));
                        return false;
                    }
                    var entitytypeId = getentityresult.Response;
                    if ($scope.EntityID != 0 && $scope.EntityHierarchyTypesResult != null) {
                        if ($scope.EntityID != 0 && $scope.EntityHierarchyTypesResult != null) {
                            MetadataService.DeleteEntityTypeHierarchy($scope.EntityID).then(function (entityHierarchyDelteResult) {
                                if ($scope.EntityID != 0 && $scope.Features != null) {
                                    MetadataService.DeleteEntityTypeFeature($scope.EntityID).then(function (entityFeatureDelteResult) {
                                        MetadataService.DeleteAttributeToAttributeRelation($scope.EntityID).then(function (deleteAttributeToAttributeRelationsResult) {
                                            MetadataService.DeleteEntitytypeAttributeGrpAccessRole($scope.EntityID).then(function (deleteEntityTypeAttrGrpAccessResult) {
                                                var saveAttribute = {};
                                                saveAttribute.EntityTypeID = $scope.EntityID;
                                                saveAttribute.AttributeData = $scope.entitytypeattributrearr;
                                                MetadataService.EntityTypeAttributeRelation(saveAttribute).then(function (EntityAttributeResponse) {
                                                    var saveEntityTyperel = {};
                                                    saveEntityTyperel.ParentActivityTypeID = $scope.EntityID;
                                                    if ($scope.EntType.ddlParententitytypeId != undefined) {
                                                        saveEntityTyperel.ChildAtctivityTypeIds = $.map($scope.EntType.ddlParententitytypeId, function (element) {
                                                            return element.id.toString();
                                                        });
                                                    } else {
                                                        saveEntityTyperel.ChildAtctivityTypeIds = [];
                                                    }
                                                    saveEntityTyperel.SortOrder = 1;
                                                    saveEntityTyperel.ID = 0;
                                                    MetadataService.InsertEntityTypeHierarchy(saveEntityTyperel).then(function (EntityTypeHeirarchy) {
                                                        $scope.entitytypefeaturearr = $scope.EntType.ddlEntityFeatureID;
                                                        var entitytypefeature = {};
                                                        entitytypefeature.TypeID = $scope.EntityID;
                                                        entitytypefeature.EntityFeatureData = $scope.entitytypefeaturearr;
                                                        entitytypefeature.ID = 0;
                                                        MetadataService.InsertEntityTypeFeature(entitytypefeature).then(function (EntityFeatureResponse) {
                                                            if (IsSaveClose == false) collectionObjectiveCondition();
                                                            var saveAttrToAttrRel = {};
                                                            saveAttrToAttrRel.EntityTypeID = $scope.EntityID;
                                                            saveAttrToAttrRel.AttributeData = $scope.ConditionDataArr;
                                                            MetadataService.InsertUpdateAttributeToAttributeRelations(saveAttrToAttrRel).then(function (EntityAttributeResponse) {
                                                                var addoptions = {};
                                                                addoptions.Options = $scope.AdminTaskCheckList;
                                                                addoptions.EntityID = $scope.EntityID;
                                                                MetadataService.EntityTypeStatusOptions(addoptions).then(function (addoptionsResponse) {
                                                                    $scope.optionsLists = [];
                                                                    if (!$scope.IsAssociate) {
                                                                        if (ValidateEntityRoleAcc() == false) {
                                                                            return false
                                                                        }
                                                                        $scope.FinalCollections = [{
                                                                            ID: 0,
                                                                            Caption: "",
                                                                            EntityTypeID: $scope.EntityID,
                                                                            EntityRoleID: 0,
                                                                            ModuleID: 6
                                                                        }];
                                                                        $scope.FinalCollections.splice(0, 1);
                                                                        for (var i = 0; i < $scope.DefaultEntityRoleCollection.length; i++) {
                                                                            $scope.FinalCollections.push({
                                                                                ID: ($scope.SelctedEntityRoledIDs[i].ID == undefined ? 0 : $scope.SelctedEntityRoledIDs[i].ID),
                                                                                Caption: $scope.DefaultEntityRoleCollection[i].Caption,
                                                                                EntityTypeID: $scope.EntityID,
                                                                                EntityRoleID: $scope.DefaultEntityRoleCollection[i].EntityRoleID,
                                                                                ModuleID: 6
                                                                            });
                                                                        }
                                                                        for (var i = 0; i < $scope.SelctedEntityRoledIDs.length; i++) {
                                                                            $scope.FinalCollections.push({
                                                                                ID: ($scope.SelctedEntityRoledIDs[i].ID == undefined ? 0 : $scope.SelctedEntityRoledIDs[i].ID),
                                                                                Caption: $scope.SelctedEntityRoledIDs[i].Caption,
                                                                                EntityTypeID: $scope.EntityID,
                                                                                EntityRoleID: $scope.SelctedEntityRoledIDs[i].EntityRoleID,
                                                                                ModuleID: 6
                                                                            });
                                                                        }
                                                                        var EntityTypeRoleAclparam = {};
                                                                        EntityTypeRoleAclparam.Options = $scope.FinalCollections;
                                                                        MetadataService.InsertUpdateEntityTypeRoleAccess(EntityTypeRoleAclparam).then(function (EntityTypeRoleResult) {
                                                                            if (EntityTypeRoleResult.Response != 0) {
                                                                                var saveAttributegrprelData = {};
                                                                                var value = 1;
                                                                                if ($scope.DragDropAttribteGroupvalues != '') {
                                                                                    for (var i = 0; i < $scope.DragDropAttribteGroupvalues.length; i++) {
                                                                                        if (parseInt($scope.DragDropAttribteGroupvalues[i].id) != 0) {
                                                                                            $.grep($scope.attributegroupGridData, function (e) {
                                                                                                return e.AttributeGroupID == parseInt($scope.DragDropAttribteGroupvalues[i].id);
                                                                                            })[0].SortOrder = value;
                                                                                            value++;
                                                                                        }
                                                                                    }
                                                                                }
                                                                                saveAttributegrprelData.AttributeData = $scope.attributegroupGridData;
                                                                                saveAttributegrprelData.EntityTypeID = $scope.EntityID;
                                                                                MetadataService.InsertUpdateEntityTypeAttributeGroup(saveAttributegrprelData).then(function (EntityAttributeResponse) {
                                                                                    if ($scope.AttributeID != undefined) {
                                                                                        var attrVal = {};
                                                                                        attrVal.AttributeValidationList = $scope.AtributeValueList;
                                                                                        attrVal.EntityTypeID = $scope.EntityID;
                                                                                        attrVal.AttributeID = $scope.AttributeID;
                                                                                        attrVal.AttributeTypeID = $scope.AttributeTypeID;
                                                                                        MetadataService.PostValidation(attrVal).then(function (AttrValditeResult) {
                                                                                            if (AttrValditeResult.Response != null) {
                                                                                                ClearAttributeValidation();
                                                                                                $scope.AtributeValueList = [];
                                                                                                $('#entitytypeattributerelationModal').modal('hide');
                                                                                            } else {
                                                                                                $('#entitytypeattributerelationModal').modal('hide');
                                                                                            }
                                                                                            MetadataService.GetEntityType(6).then(function (entitytypes) {
                                                                                                $scope.entitytpesdata = entitytypes.Response;
                                                                                                $scope.entitytpesdatarootlevel = entitytypes.Response;
                                                                                            });
                                                                                        });
                                                                                    } else {
                                                                                        $('#entitytypeattributerelationModal').modal('hide');
                                                                                        MetadataService.GetEntityType(6).then(function (entitytypes) {
                                                                                            $scope.entitytpesdata = entitytypes.Response;
                                                                                            $scope.entitytpesdatarootlevel = entitytypes.Response;
                                                                                        });
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    } else {
                                                                        var saveAttributegrprelData = {};
                                                                        var value = 1;
                                                                        if ($scope.DragDropAttribteGroupvalues != '') {
                                                                            for (var i = 0; i < $scope.DragDropAttribteGroupvalues.length; i++) {
                                                                                if (parseInt($scope.DragDropAttribteGroupvalues[i].id) != 0) {
                                                                                    $.grep($scope.attributegroupGridData, function (e) {
                                                                                        return e.AttributeGroupID == parseInt($scope.DragDropAttribteGroupvalues[i].id);
                                                                                    })[0].SortOrder = value;
                                                                                    value++;
                                                                                }
                                                                            }
                                                                        }
                                                                        saveAttributegrprelData.AttributeData = $scope.attributegroupGridData;
                                                                        saveAttributegrprelData.EntityTypeID = $scope.EntityID;
                                                                        MetadataService.InsertUpdateEntityTypeAttributeGroup(saveAttributegrprelData).then(function (EntityAttributeResponse) {
                                                                            if ($scope.AttributeID != undefined) {
                                                                                var attrVal = {};
                                                                                attrVal.EntityTypeID = $scope.EntityID;
                                                                                attrVal.AttributeID = $scope.AttributeID;
                                                                                attrVal.AttributeTypeID = $scope.AttributeTypeID;
                                                                                attrVal.AttributeValidationList = $scope.AtributeValueList;
                                                                                MetadataService.PostValidation(attrVal).then(function (AttrValditeResult) {
                                                                                    if (AttrValditeResult.Response != null) {
                                                                                        ClearAttributeValidation();
                                                                                        $scope.AtributeValueList = [];
                                                                                        $('#entitytypeattributerelationModal').modal('hide');
                                                                                    } else {
                                                                                        $('#entitytypeattributerelationModal').modal('hide');
                                                                                    }
                                                                                    MetadataService.GetEntityType(6).then(function (entitytypes) {
                                                                                        $scope.entitytpesdata = entitytypes.Response;
                                                                                        $scope.entitytpesdatarootlevel = $.grep($scope.entitytpesdata, function (e) {
                                                                                            return e.IsRootLevel == true
                                                                                        });
                                                                                    });
                                                                                });
                                                                            } else {
                                                                                $('#entitytypeattributerelationModal').modal('hide');
                                                                                MetadataService.GetEntityType(6).then(function (entitytypes) {
                                                                                    $scope.entitytpesdata = entitytypes.Response;
                                                                                    $scope.entitytpesdatarootlevel = $.grep($scope.entitytpesdata, function (e) {
                                                                                        return e.IsRootLevel == true
                                                                                    });
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                }
                            });
                        }
                    } else {
                        var saveAttribute = {};
                        saveAttribute.EntityTypeID = entitytypeId;
                        saveAttribute.AttributeData = $scope.entitytypeattributrearr;
                        MetadataService.EntityTypeAttributeRelation(saveAttribute).then(function (EntityAttributeResponse) {
                            var saveEntityTyperel = {};
                            saveEntityTyperel.ParentActivityTypeID = entitytypeId;
                            if ($scope.EntType.ddlParententitytypeId != undefined) {
                                saveEntityTyperel.ChildAtctivityTypeIds = $.map($scope.EntType.ddlParententitytypeId, function (element) {
                                    return element.id.toString();
                                });
                            } else {
                                saveEntityTyperel.ChildAtctivityTypeIds = [];
                            }
                            saveEntityTyperel.SortOrder = 1;
                            saveEntityTyperel.ID = 0;
                            MetadataService.InsertEntityTypeHierarchy(saveEntityTyperel).then(function (EntityTypeHeirarchy) {
                                $scope.entitytypefeaturearr = $scope.EntType.ddlEntityFeatureID;
                                var entitytypefeature = {};
                                entitytypefeature.TypeID = entitytypeId;
                                entitytypefeature.EntityFeatureData = $scope.entitytypefeaturearr;
                                entitytypefeature.ID = 0;
                                MetadataService.InsertEntityTypeFeature(entitytypefeature).then(function (EntityFeatureResponse) {
                                    var addoptions = {};
                                    addoptions.Options = $scope.AdminTaskCheckList;
                                    addoptions.EntityID = entitytypeId;
                                    MetadataService.EntityTypeStatusOptions(addoptions).then(function (addoptionsResponse) {
                                        $scope.optionsLists = [];
                                        if (IsSaveClose == false) collectionObjectiveCondition();
                                        if (!$scope.IsAssociate) {
                                            $scope.FinalCollections = [{
                                                ID: 0,
                                                Caption: "",
                                                EntityTypeID: entitytypeId,
                                                EntityRoleID: 0,
                                                ModuleID: 6
                                            }];
                                            $scope.FinalCollections.splice(0, 1);
                                            for (var i = 0; i < $scope.DefaultEntityRoleCollection.length; i++) {
                                                $scope.FinalCollections.push({
                                                    ID: ($scope.SelctedEntityRoledIDs[i].ID == undefined ? 0 : $scope.SelctedEntityRoledIDs[i].ID),
                                                    Caption: $scope.DefaultEntityRoleCollection[i].Caption,
                                                    EntityTypeID: entitytypeId,
                                                    EntityRoleID: $scope.DefaultEntityRoleCollection[i].EntityRoleID,
                                                    ModuleID: 6
                                                });
                                            }
                                            for (var i = 0; i < $scope.SelctedEntityRoledIDs.length; i++) {
                                                $scope.FinalCollections.push({
                                                    ID: ($scope.SelctedEntityRoledIDs[i].ID == undefined ? 0 : $scope.SelctedEntityRoledIDs[i].ID),
                                                    Caption: $scope.SelctedEntityRoledIDs[i].Caption,
                                                    EntityTypeID: entitytypeId,
                                                    EntityRoleID: $scope.SelctedEntityRoledIDs[i].EntityRoleID,
                                                    ModuleID: 6
                                                });
                                            }
                                            var EntityTypeRoleAclparam = {};
                                            EntityTypeRoleAclparam.Options = $scope.FinalCollections;
                                            MetadataService.InsertUpdateEntityTypeRoleAccess(EntityTypeRoleAclparam).then(function (EntityTypeRoleResult) {
                                                if (EntityTypeRoleResult.Response != 0) {
                                                    var saveAttributegrprelData = {};
                                                    var value = 1;
                                                    if ($scope.DragDropAttribteGroupvalues != '') {
                                                        for (var i = 0; i < $scope.DragDropAttribteGroupvalues.length; i++) {
                                                            if (parseInt($scope.DragDropAttribteGroupvalues[i].id) != 0) {
                                                                $.grep($scope.attributegroupGridData, function (e) {
                                                                    return e.AttributeGroupID == parseInt($scope.DragDropAttribteGroupvalues[i].id);
                                                                })[0].SortOrder = value;
                                                                value++;
                                                            }
                                                        }
                                                    }
                                                    saveAttributegrprelData.AttributeData = $scope.attributegroupGridData;
                                                    saveAttributegrprelData.EntityTypeID = entitytypeId;
                                                    MetadataService.InsertUpdateEntityTypeAttributeGroup(saveAttributegrprelData).then(function (EntityAttributeResponse) {
                                                        if ($scope.ConditionDataArr != undefined && $scope.ConditionDataArr.length > 0) {
                                                            var saveAttrToAttrRel = {};
                                                            saveAttrToAttrRel.EntityTypeID = entitytypeId;
                                                            saveAttrToAttrRel.AttributeData = $scope.ConditionDataArr;
                                                            MetadataService.InsertUpdateAttributeToAttributeRelations(saveAttrToAttrRel).then(function (EntityAttributeResponse) {
                                                                if ($scope.AttributeID != undefined) {
                                                                    var attrVal = {};
                                                                    attrVal.AttributeValidationList = $scope.AtributeValueList;
                                                                    attrVal.EntityTypeID = entitytypeId;
                                                                    attrVal.AttributeID = $scope.AttributeID;
                                                                    attrVal.AttributeTypeID = $scope.AttributeTypeID;
                                                                    MetadataService.PostValidation(attrVal).then(function (AttrValditeResult) {
                                                                        if (AttrValditeResult.Response != null) {
                                                                            ClearAttributeValidation();
                                                                            $scope.AtributeValueList = [];
                                                                            $('#entitytypeattributerelationModal').modal('hide');
                                                                        } else {
                                                                            $('#entitytypeattributerelationModal').modal('hide');
                                                                        }
                                                                        MetadataService.GetEntityType(6).then(function (entitytypes) {
                                                                            $scope.entitytpesdata = entitytypes.Response;
                                                                            $scope.entitytpesdatarootlevel = entitytypes.Response;
                                                                        });
                                                                    });
                                                                } else {
                                                                    $('#entitytypeattributerelationModal').modal('hide');
                                                                    MetadataService.GetEntityType(6).then(function (entitytypes) {
                                                                        $scope.entitytpesdata = entitytypes.Response;
                                                                        $scope.entitytpesdatarootlevel = entitytypes.Response;
                                                                    });
                                                                }
                                                            });
                                                        } else {
                                                            if ($scope.AttributeID != undefined) {
                                                                var attrVal = {};
                                                                attrVal.AttributeValidationList = $scope.AtributeValueList;
                                                                attrVal.EntityTypeID = entitytypeId;
                                                                attrVal.AttributeID = $scope.AttributeID;
                                                                attrVal.AttributeTypeID = $scope.AttributeTypeID;
                                                                MetadataService.PostValidation(attrVal).then(function (AttrValditeResult) {
                                                                    if (AttrValditeResult.Response != null) {
                                                                        ClearAttributeValidation();
                                                                        $scope.AtributeValueList = [];
                                                                        $('#entitytypeattributerelationModal').modal('hide');
                                                                    } else {
                                                                        $('#entitytypeattributerelationModal').modal('hide');
                                                                    }
                                                                    MetadataService.GetEntityType(6).then(function (entitytypes) {
                                                                        $scope.entitytpesdata = entitytypes.Response;
                                                                        $scope.entitytpesdatarootlevel = entitytypes.Response;
                                                                    });
                                                                });
                                                            } else {
                                                                $('#entitytypeattributerelationModal').modal('hide');
                                                                MetadataService.GetEntityType(6).then(function (entitytypes) {
                                                                    $scope.entitytpesdata = entitytypes.Response;
                                                                    $scope.entitytpesdatarootlevel = entitytypes.Response;
                                                                });
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                        } else {
                                            var saveAttributegrprelData = {};
                                            var value = 1;
                                            if ($scope.DragDropAttribteGroupvalues != '') {
                                                for (var i = 0; i < $scope.DragDropAttribteGroupvalues.length; i++) {
                                                    if (parseInt($scope.DragDropAttribteGroupvalues[i].id) != 0) {
                                                        $.grep($scope.attributegroupGridData, function (e) {
                                                            return e.AttributeGroupID == parseInt($scope.DragDropAttribteGroupvalues[i].id);
                                                        })[0].SortOrder = value;
                                                        value++;
                                                    }
                                                }
                                            }
                                            saveAttributegrprelData.AttributeData = $scope.attributegroupGridData;
                                            saveAttributegrprelData.EntityTypeID = entitytypeId;
                                            MetadataService.InsertUpdateEntityTypeAttributeGroup(saveAttributegrprelData).then(function (EntityAttributeResponse) {
                                                if ($scope.ConditionDataArr != undefined && $scope.ConditionDataArr.length > 0) {
                                                    var saveAttrToAttrRel = {};
                                                    saveAttrToAttrRel.EntityTypeID = entitytypeId;
                                                    saveAttrToAttrRel.AttributeData = $scope.ConditionDataArr;
                                                    MetadataService.InsertUpdateAttributeToAttributeRelations(saveAttrToAttrRel).then(function (EntityAttributeResponse) {
                                                        if ($scope.AttributeID != undefined) {
                                                            var attrVal = {};
                                                            attrVal.AttributeValidationList = $scope.AtributeValueList;
                                                            attrVal.EntityTypeID = entitytypeId;
                                                            attrVal.AttributeID = $scope.AttributeID;
                                                            attrVal.AttributeTypeID = $scope.AttributeTypeID;
                                                            MetadataService.PostValidation(attrVal).then(function (AttrValditeResult) {
                                                                if (AttrValditeResult.Response != null) {
                                                                    ClearAttributeValidation();
                                                                    $scope.AtributeValueList = [];
                                                                    $('#entitytypeattributerelationModal').modal('hide');
                                                                } else {
                                                                    $('#entitytypeattributerelationModal').modal('hide');
                                                                }
                                                                MetadataService.GetEntityType(6).then(function (entitytypes) {
                                                                    $scope.entitytpesdata = entitytypes.Response;
                                                                    $scope.entitytpesdatarootlevel = entitytypes.Response;
                                                                });
                                                            });
                                                        } else {
                                                            MetadataService.GetEntityType(6).then(function (entitytypes) {
                                                                $scope.entitytpesdata = entitytypes.Response;
                                                                $scope.entitytpesdatarootlevel = entitytypes.Response;
                                                            });
                                                        }
                                                    });
                                                } else {
                                                    if ($scope.AttributeID != undefined) {
                                                        var attrVal = {};
                                                        attrVal.AttributeValidationList = $scope.AtributeValueList;
                                                        attrVal.EntityTypeID = entitytypeId;
                                                        attrVal.AttributeID = $scope.AttributeID;
                                                        attrVal.AttributeTypeID = $scope.AttributeTypeID;
                                                        MetadataService.PostValidation(attrVal).then(function (AttrValditeResult) {
                                                            if (AttrValditeResult.Response != null) {
                                                                ClearAttributeValidation();
                                                                $scope.AtributeValueList = [];
                                                                $('#entitytypeattributerelationModal').modal('hide');
                                                            } else {
                                                                $('#entitytypeattributerelationModal').modal('hide');
                                                            }
                                                            MetadataService.GetEntityType(6).then(function (entitytypes) {
                                                                $scope.entitytpesdata = entitytypes.Response;
                                                                $scope.entitytpesdatarootlevel = entitytypes.Response;
                                                            });
                                                        });
                                                    } else {
                                                        $('#entitytypeattributerelationModal').modal('hide');
                                                        MetadataService.GetEntityType(6).then(function (entitytypes) {
                                                            $scope.entitytpesdata = entitytypes.Response;
                                                            $scope.entitytpesdatarootlevel = entitytypes.Response;
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    });
                                });
                            });
                        });
                    }
                    NotifySuccess($translate.instant('LanguageContents.Res_4801.Caption'));
                });
            } else {
                $timeout(function () { $("#btnTemp").click() }, 100);
                $("#entitytypeformPage1").removeClass('notvalidate');
                if ($("#entitytypeformPage1 .error").length > 0) {
                    return false;
                }
                if (ValidateEntityRoleAcc() == false) {
                    return false
                }
                if ($scope.getCurrentStep() == "EntityTypeAttribute") {
                    if ($scope.EntType.ddlAttributeID != "" || $scope.EntType.ddlAttributeID != 0 || $scope.EntType.EntitytperelCaption != "") {
                        bootbox.confirm($translate.instant('LanguageContents.Res_2110.Caption'), function (result) {
                            if (result) {
                                $scope.funloadnexttabinfo();
                            } else {
                                return true;
                            }
                        });
                    } else {
                        $scope.funloadnexttabinfo();
                    }
                } else {
                    $scope.funloadnexttabinfo();
                }
            }
        };

        function ValidateEntityRoleAcc() {
            var Isempty = false;
            $.grep($scope.SelctedEntityRoledIDs, function (item, i) {
                if (item.Caption.length == 0) {
                    Isempty = true;
                }
            });
            if (Isempty == true) {
                bootbox.alert($translate.instant('LanguageContents.Res_4574.Caption'));
                return false;
            }
            return true;
        }
        $scope.funloadnexttabinfo = function () {
            if ($scope.getCurrentStep() == 'EnityType') {
                $scope.ColorCodeGlobal = $('#appendedPrependedDropdownButton input').val();
            }
            $scope.step += 1;
            $scope.EnableEntitytypeAttributeAdd = true;
            $scope.EntType.EnableAttributeGroupAdd = true;
            $scope.EntType.EnableAttributeGroupUpdate = false;
            if ($scope.getCurrentStep() == 'EntityTypeAttribute') {
                if ($scope.ColorCodeGlobalObj.colorcode == undefined || $scope.ColorCodeGlobalObj.colorcode == "") {
                    bootbox.alert($translate.instant('LanguageContents.Res_1825.Caption'));
                    $scope.step -= 1;
                    return false;
                }
                if ($scope.EntType.IsRootLevel == true) {
                    $scope.EntType.ShowHideInheritFromParent = false;
                } else {
                    $scope.EntType.ShowHideInheritFromParent = true;
                }
                $timeout(function () {
                    ValidEntitytypeattributePage2();
                }, 100);
                $("#entitytypeformPage2").addClass('notvalidate');
            } else if ($scope.getCurrentStep() == 'AttributeToAttibuteRelations') {
                if ($scope.EntityID != SystemDefinedEntityTypes.Milestone) {
                    var lstName = $.grep($scope.entitytypeattributrearr, function (data) {
                        return parseInt(data.AttributeID) == SystemDefiendAttributes.Name
                    });
                    if (lstName == null || lstName.length == 0) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1827.Caption'));
                        $scope.step -= 1;
                        return false;
                    }
                    var lstOwner = $.grep($scope.entitytypeattributrearr, function (data) {
                        return parseInt(data.AttributeID) == SystemDefiendAttributes.Owner
                    });
                    if (lstOwner == null || lstOwner.length == 0) {
                        bootbox.alert($translate.instant('LanguageContents.Res_1828.Caption'));
                        $scope.step -= 1;
                        return false;
                    }
                }
                if ($scope.ColorCodeGlobalObj.colorcode == undefined || $scope.ColorCodeGlobalObj.colorcode == "") {
                    bootbox.alert($translate.instant('LanguageContents.Res_1825.Caption'));
                    $scope.step -= 1;
                    return false;
                }
                $scope.attributerelationarr = [];
                $scope.attributerelationarr = $.grep($scope.entitytypeattributrearr, function (e) {
                    return ((e.AttributeID != SystemDefiendAttributes.Owner) && (e.AttributeID != SystemDefiendAttributes.Status) && e.AttributeTypeID == parseInt(3) && (e.AttributeID != parseInt(71)) && (e.AttributeID != parseInt(74)) && (e.AttributeID != parseInt(75)) || e.AttributeTypeID == parseInt(4) || e.AttributeTypeID == parseInt(7) || e.AttributeTypeID == parseInt(6) || e.AttributeTypeID == parseInt(12));
                });
                $scope.FulfillmentAttributes = [];
                GetAttributesOnSelectedAttributes();
                $timeout(function () {
                    AddAttributeRelationsOnPageLoad();
                }, 300);
            } else if ($scope.getCurrentStep() == 'EntityTypeAttributeGroup') {
                $timeout(function () {
                    DragDropAttributeGroup();
                }, 200);
                $timeout(function () {
                    ValidEntitytypeattributePage4();
                }, 100);
            }
        }
        $scope.SaveClose = function () {
            $scope.enabledisableCmsattributes = false;
            IsSaveClose = true;
            $scope.IsDisabled = true;
            if ($('#appendedPrependedDropdownButton input').val() != undefined) $scope.ColorCodeGlobal = $('#appendedPrependedDropdownButton input').val();
            if ($scope.getCurrentStep() == "EntityTypeAttribute") {
                if ($scope.EntType.ddlAttributeID != "" || $scope.EntType.ddlAttributeID != 0 || $scope.EntType.EntitytperelCaption != "") {
                    bootbox.confirm($translate.instant('LanguageContents.Res_2110.Caption'), function (result) {
                        if (result) {
                            saveclosewithattrrelation();
                        } else {
                            return true;
                        }
                    });
                } else {
                    saveclosewithattrrelation();
                }
            } else {
                saveclosewithattrrelation();
            }
            $scope.ClearAttributeGroupCtrls();
        }

        function saveclosewithattrrelation() {
            MetadataService.GetAttributeToAttributeRelationsByID($scope.EntityID).then(function (entityAttrToAttrRelation) {
                var entityattributetoattribtuerelation = entityAttrToAttrRelation.Response;
                $scope.ConditionDataArr = [];
                try {
                    for (var i = 0; i < entityattributetoattribtuerelation.length; i++) {
                        var collectionarr = {
                            'AttributeID': entityattributetoattribtuerelation[i].AttributeID,
                            'AttributeLevelID': entityattributetoattribtuerelation[i].AttributeLevel,
                            'AttributeOptionID': entityattributetoattribtuerelation[i].AttributeOptionID,
                            'AttributeToAttributeRelationID': entityattributetoattribtuerelation[i].AttributeRelationID
                        };
                        $scope.ConditionDataArr.push(collectionarr);
                    }
                } catch (e) { }
                $timeout(function () {
                    $scope.handleNext(true);
                }, 50);
            });
        }
        $scope.$watch("entitytypeattributrearr", function (value) {
            $timeout(function () {
                applyAlternateColor();
            }, 100);
        }, true);

        function applyAlternateColor() {
            var flag = true;
            var backgroundcolor = "#F3F3F3";
            $(".entypeAttrbtTableContent > li").each(function (i, val) {
                if (parseInt($(this).data('attributeid')) !== 71 && parseInt($(this).data('attributeid')) !== 74 && parseInt($(this).data('attributeid')) !== 75) {
                    if (flag) {
                        backgroundcolor = "#F3F3F3";
                        flag = false;
                    } else {
                        backgroundcolor = "#FDFDFD";
                        flag = true;
                    }
                    $(this).css("background", backgroundcolor);
                }
            });
        }
        var DragDropAttributeGroup = function () {
            $('#attributegrouptable').tableDnD({
                onDrop: function (table, row) {
                    $scope.DragDropAttribteGroupvalues = table.tBodies[0].rows;;
                }
            });
        }

        function ControlReset() {
            $scope.ParententityDropdowndata = [];
            $scope.EntityHierarchyTypesResult = [];
            $scope.Fetureattribues = [];
            $scope.EnableEntitytypeAttributeUpdate = false;
            $scope.entitytypeattributrearr = [];
            $scope.EntType.EntitytperelCaption = '';
            $scope.EntType.ddlAttributeID = 0;
            $scope.EntType.ddlWorkFlow = 0;
            $scope.EntType.Defaultvalue = '';
            $scope.EntType.MultiselectDefaultvalue = '';
            $scope.EntType.TextDefaultvalue = '';
            $scope.EntType.PlaceHolderTextValue = '';
            $scope.EntType.Inheritfromparent = false;
            $scope.EntType.Isreadonly = false;
            $scope.EntType.Choosefromparentonly = false;
            $scope.EntType.Isvalidationneeded = false;
            $scope.EnableMultiselectFromParent = false;
            $scope.EntityHierarchyTypesResult = [];
            $scope.entitytpesdata = [];
            $scope.Fetureattribues = [];
            $(".pick-a-color")[0].attributes[0].value = '';
            $(".pick-a-color").pickAColor({
                showSpectrum: true,
                showSavedColors: false,
                saveColorsPerElement: true,
                fadeMenuToggle: true,
                showAdvanced: true,
                showHexInput: true,
                showBasicColors: true
            });
            $scope.Features = [];
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.entitytypeattributerelationCtrl']"));
        });
        $scope.ValiadtionOptions = [{
            key: "presence",
            Value: "Mandatory"
        }, {
            key: "max-length",
            Value: "Max-Length"
        }, {
            key: "min-length",
            Value: "min-length:Number"
        }, {
            key: "between",
            Value: "Between:Number"
        }, {
            key: "max-num",
            Value: "Max-num:Number"
        }, {
            key: "min-num",
            Value: "Min-num:Number"
        }, {
            key: "between-num",
            Value: "Between-num:Number:Number"
        }, {
            key: "integer",
            Value: "Integer"
        }, {
            key: "float",
            Value: "Float"
        }, {
            key: "same-as:Selector",
            Value: "Same-as:Selector"
        }, {
            key: "email",
            Value: "Email"
        }];
        $scope.childValidation = [];
        $scope.temvalidtaion = [];
        var RelationID = '';
        $scope.Validate = function AttributeValidate(attrbs) {
            $("#ValidateAttribute").modal('show');
            $("#ValidationType").select2("enable", true)
            ClearAttributeValidation();
            $scope.EnableValidationSave = true;
            $scope.EnableValidationUpdate = false;
            $scope.AttributeID = attrbs.AttributeID;
            $scope.AttributeTypeID = attrbs.AttributeTypeID;
            $scope.AtributeValidationList = [];
            $scope.childValidation = [];
            if (attrbs.ID != 0) $scope.childValidation = $.grep($scope.AtributeValueList, function (n, i) {
                return ($scope.AtributeValueList[i].RelationShipID == attrbs.ID);
            });
            else $scope.childValidation = $.grep($scope.AtributeValueList, function (n, i) {
                return ($scope.AtributeValueList[i].AttributeID == attrbs.AttributeID);
            });
            $scope.ValidCount = 0;
            $scope.temvalidtaion = [];
            for (var i = 0; i < $scope.childValidation.length; i++) {
                $scope.temvalidtaion.push($scope.childValidation[i]);
                $scope.temvalidtaion[i].ValidCount = i + 1;
                $scope.ValidCount = i + 1;
            }
            RelationID = '';
            RelationID = attrbs.ID;
            var tempList = $scope.childValidation;
        };
        $scope.AtributeValueList = [];
        $scope.ValidationAdd = function AttributeValidationAdd() {
            if ($scope.ValidationType.length > 0 && $scope.validationErorr.length > 0) {
                var validcheck = $.grep($scope.childValidation, function (e) {
                    return e.ValueType == $scope.ValidationType
                });
                if (validcheck.length > 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_4145.Caption'));
                    return false;
                }
                $scope.ValidCount++;
                $scope.AtributeValueList.push({
                    Id: 0,
                    Name: "",
                    EntityTypeID: 0,
                    RelationShipID: RelationID,
                    ValueType: $scope.ValidationType,
                    Value: $scope.ValidationValue,
                    ErrorMessage: $scope.validationErorr,
                    AttributeID: $scope.AttributeID
                });
                $scope.childValidation.push({
                    Id: 0,
                    Name: "",
                    EntityTypeID: 0,
                    RelationShipID: RelationID,
                    ValueType: $scope.ValidationType,
                    Value: $scope.ValidationValue,
                    ErrorMessage: $scope.validationErorr,
                    AttributeID: $scope.AttributeID,
                    ValidCount: $scope.ValidCount
                });
                $scope.temvalidtaion.push({
                    Id: 0,
                    Name: "",
                    EntityTypeID: 0,
                    RelationShipID: RelationID,
                    ValueType: $scope.ValidationType,
                    Value: $scope.ValidationValue,
                    ErrorMessage: $scope.validationErorr,
                    AttributeID: $scope.AttributeID,
                    ValidCount: $scope.ValidCount
                });
                ClearAttributeValidation();
            }
        };
        $scope.ValidationUpdate = function ValidationUpdate() {
            var relationshipid = $.grep($scope.temvalidtaion, function (e) {
                return e.ValidCount == parseInt($scope.Currentindex);
            })[0].RelationShipID;
            var ValueType = $.grep($scope.temvalidtaion, function (e) {
                return e.ValidCount == parseInt($scope.Currentindex);
            })[0].ValueType;
            $.grep($scope.AtributeValueList, function (e) {
                return e.RelationShipID == parseInt(relationshipid) && e.ValueType == ValueType;
            })[0].ErrorMessage = $scope.validationErorr;
            $.grep($scope.AtributeValueList, function (e) {
                return e.RelationShipID == parseInt(relationshipid) && e.ValueType == ValueType;
            })[0].Value = $scope.ValidationValue;
            $.grep($scope.AtributeValueList, function (e) {
                return e.RelationShipID == parseInt(relationshipid) && e.ValueType == ValueType;
            })[0].ValueType = $scope.ValidationType;
            $.grep($scope.childValidation, function (e) {
                return e.ValidCount == parseInt($scope.Currentindex);
            })[0].ErrorMessage = $scope.validationErorr;
            $.grep($scope.childValidation, function (e) {
                return e.ValidCount == parseInt($scope.Currentindex);
            })[0].Value = $scope.ValidationValue;
            $.grep($scope.childValidation, function (e) {
                return e.ValidCount == parseInt($scope.Currentindex);
            })[0].ValueType = $scope.ValidationType;
            $.grep($scope.temvalidtaion, function (e) {
                return e.ValidCount == parseInt($scope.Currentindex);
            })[0].ErrorMessage = $scope.validationErorr;
            $.grep($scope.temvalidtaion, function (e) {
                return e.ValidCount == parseInt($scope.Currentindex);
            })[0].Value = $scope.ValidationValue;
            $.grep($scope.temvalidtaion, function (e) {
                return e.ValidCount == parseInt($scope.Currentindex);
            })[0].ValueType = $scope.ValidationType;
            $scope.EnableValidationSave = true;
            $scope.EnableValidationUpdate = false;
            $("#ValidationType").select2("enable", true)
            ClearAttributeValidation();
        }
        $scope.EnableValidationUpdate = false;
        $scope.DeleteValidationAttributeByID = function (Index) {
            $scope.tempIndex = Index;
            if ($scope.childValidation[Index].Id > 0) {
                bootbox.confirm($translate.instant('LanguageContents.Res_1824.Caption'), function (result) {
                    if (result) {
                        $timeout(function () {
                            MetadataService.DeleteAttributeValidation($scope.childValidation[Index].Id).then(function (deleteByID) {
                                if (deleteByID.Response != null && deleteByID.Response == true) {
                                    var objecttoremove = $.grep($scope.AtributeValueList, function (e) {
                                        return e.RelationShipID == $scope.childValidation[$scope.tempIndex].RelationShipID && e.ValueType == $scope.childValidation[$scope.tempIndex].ValueType
                                    });
                                    // var uncommon = _.difference($scope.AtributeValueList, objecttoremove);
                                    var uncommon = $.grep($scope.AtributeValueList, function (e) { return e.Id != objecttoremove[0].Id });
                                    $scope.AtributeValueList = [];
                                    $scope.AtributeValueList = uncommon;
                                    $scope.childValidation.splice($scope.tempIndex, 1);
                                    $scope.temvalidtaion.splice($scope.tempIndex, 1);
                                    NotifySuccess($translate.instant('LanguageContents.Res_4790.Caption'));
                                } else {
                                    NotifyError($translate.instant('LanguageContents.Res_4284.Caption'));
                                }
                            });
                        }, 100);
                    }
                });
            } else {
                $scope.tempIndex = Index;
                var objecttoremove = $.grep($scope.AtributeValueList, function (e) {
                    return e.RelationShipID == $scope.childValidation[$scope.tempIndex].RelationShipID && e.ValueType == $scope.childValidation[$scope.tempIndex].ValueType
                });
                //var uncommon = _.difference($scope.AtributeValueList, objecttoremove);
                var uncommon = $.grep($scope.AtributeValueList, function (e) { return e.Id != objecttoremove[0].Id });
                $scope.AtributeValueList = [];
                $scope.AtributeValueList = uncommon;
                $scope.childValidation.splice($scope.tempIndex, 1);
                $scope.temvalidtaion.splice($scope.tempIndex, 1);
            }
        };

        function ClearAttributeValidation() {
            $scope.ValidationType = "";
            $scope.ValidationValue = "";
            $scope.validationErorr = "";
        }

        function ValidationGet() {
            MetadataService.GetAttributeValidationByEntityTypeId($scope.EntityID, 0).then(function (AttrValditeResult) {
                $scope.AtributeValueList = [];
                if (AttrValditeResult.Response != null) {
                    jQuery.each(AttrValditeResult.Response, function (index, Val) {
                        $scope.AtributeValueList.push({
                            Id: Val.Id,
                            Name: Val.Name,
                            EntityTypeID: Val.EntityTypeID,
                            RelationShipID: Val.RelationShipID,
                            ValueType: Val.ValueType,
                            Value: Val.Value,
                            ErrorMessage: Val.ErrorMessage,
                            AttributeID: Val.AttributeID
                        });
                    });
                }
            });
        }
        $scope.GetEntityAttributeByIndex = function (row) {
            $("#ValidationType").select2("enable", false)
            $scope.EnableValidationSave = false;
            $scope.EnableValidationUpdate = true;
            $scope.ValidationType = row.ValueType;
            $scope.ValidationValue = row.Value;
            $scope.validationErorr = row.ErrorMessage;
            if (row.ValidCount == undefined) {
                $scope.Currentindex = $.grep($scope.temvalidtaion, function (e) {
                    return e.Id == parseInt(row.Id);
                })[0].ValidCount
            } else $scope.Currentindex = row.ValidCount;
            $scope.ValidationValueCheck();
        }
        $scope.AddAttributeRelationsNewRow = function (event) {
            var TargetControl = $(event.target);
            var currentUniqueId = TargetControl.parents('div').attr('data-id');
            if (TargetControl.attr('data-role') == 'EntityType') { } else if (TargetControl.attr('data-role') == 'Attributes') {
                ResetDropDown(currentUniqueId, 1);
                var AttributeData = TargetControl.val().split("_");
                var entityAttribtueId = parseInt(AttributeData[0], 10);
                var entityAttributeLevel = parseInt(AttributeData[1], 10);
                MetadataService.GetAttributeOptionsInAttrToAttrRelations(entityAttribtueId, entityAttributeLevel).then(function (fulfillmentAttributeOptionsObj) {
                    if (fulfillmentAttributeOptionsObj.Response.length > 0) {
                        FillAttributeOption("ObjAttributeOptions" + currentUniqueId, fulfillmentAttributeOptionsObj.Response)
                    }
                });
            } else if (TargetControl.attr('data-role') == 'Options') {
                ResetDropDown(currentUniqueId, 2);
                FillAttributesRelations("ObjAttributesRelations" + currentUniqueId, $('#ObjAttributes' + currentUniqueId).val())
            } else if (TargetControl.attr('data-role') == 'Add') {
                if (TargetControl.parents().find('#ObjAttributes' + currentUniqueId).val() == "0" || TargetControl.parents().find('#ObjAttributes' + currentUniqueId).val() == null) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1829.Caption'));
                    return false;
                }
                if (TargetControl.parents().find('#ObjAttributeOptions' + currentUniqueId).val() == "0" || TargetControl.parents().find('#ObjAttributeOptions' + currentUniqueId).val() == null) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1830.Caption'));
                    return false;
                }
                if (TargetControl.parents().find('#ObjAttributesRelations' + currentUniqueId).val() == "0" || TargetControl.parents().find('#ObjAttributesRelations' + currentUniqueId).val() == null) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1831.Caption'));
                    return false;
                }
                var UniqueId = parseInt(TargetControl.parents('div').attr('data-id')) + 1;
                var html = '';
                html += "<div  data-control='main' data-Holder='holder' id='container" + UniqueId + "' data-uniquekey='container" + UniqueId + "' data-id='" + UniqueId + "'>";
                html += "<ul class='repeter' >";
                html += "<li class='form-inline'>";
                html += "<label>Attribute</label>";
                html += "<select ui-select2 id='ObjAttributes" + UniqueId + "' ng-model='dummymodule_attribute_" + UniqueId + "'   data-role='Attributes'>";
                html += "<option value='0'>-- Select --</option>";
                html += "";
                html += "</select>";
                html += "<label>Options</label>";
                html += "<select ui-select2  id='ObjAttributeOptions" + UniqueId + "' ng-model='dummymodule_options_" + UniqueId + "' data-role='Options'>";
                html += "<option  value='0'>-- Select --</option>";
                html += "";
                html += "</select>";
                html += "<label>Relations</label>";
                html += "<select ui-select2 multiple='multiple'  id='ObjAttributesRelations" + UniqueId + "' ng-model='dummymodule_relations_" + UniqueId + "' data-role='EntityType'>";
                html += "<option value='0'>-- Select --</option>";
                html += "";
                html += "</select>";
                html += "<button class='btn' data-role='Add' ><i class='icon-plus' data-role='Add'></i></button>&nbsp;<button class='btn' data-role='Remove' ><i class='icon-remove' data-role='Remove'></i></button>";
                html += "";
                html += "</li>";
                html += "</ul>";
                html += "</div>";
                $("#container" + currentUniqueId).after($compile(html)($scope));
                FillAttributes("ObjAttributes" + UniqueId);
            } else if (TargetControl.attr('data-role') == 'Remove') {
                if (currentUniqueId == 0) {
                    ResetDropDown(currentUniqueId, 1);
                    ResetDropDown(currentUniqueId, 2);
                    return false;
                }
                $("#container" + currentUniqueId).remove();
            }
        }

        function GetAttributesOnSelectedAttributes() {
            var temp = [];
            angular.forEach($scope.attributerelationarr, function (key) {
                temp.push(key.AttributeID);
            });
            try {
                var saveAttrToAttrRel = {};
                saveAttrToAttrRel.ID = temp.toString().split(",").toString();
                MetadataService.GetAttributeRelationByIDs(saveAttrToAttrRel).then(function (entityAttributesRelation) {
                    if (entityAttributesRelation.Response != null) {
                        $scope.FulfillmentAttributes = entityAttributesRelation.Response;
                        var val = "";
                        $timeout(function () {
                            for (var i = 0; i < $scope.FulfillmentAttributes.length; i++) {
                                val = "";
                                val = $.grep($scope.attributerelationarr, function (e) {
                                    return e.AttributeID == parseInt($scope.FulfillmentAttributes[i].Id);
                                })[0].Caption
                                $.grep($scope.FulfillmentAttributes, function (e) {
                                    return e.Id == parseInt($scope.FulfillmentAttributes[i].Id);
                                })[0].Caption = val;
                            }
                        }, 100);
                    }
                });
            } catch (e) { }
        }

        function FillAttributeOption(ControllerID, Response) {
            if (ControllerID != undefined) {
                $('#' + ControllerID).html("");
                $('#' + ControllerID).html("<option value='0'>-- Select --</option>");
                $.each(Response, function (val, item) {
                    if (item.Level != undefined) {
                        var currentId = item.Id.toString() + "_" + item.Level;
                        $('#' + ControllerID).append($("<option></option>").val(currentId).html(item.Caption));
                    } else {
                        $('#' + ControllerID).append($('<option ></option>').val(item.Id).html(item.Caption));
                    }
                });
            }
        }

        function FillAttributesRelations(ControllerID, attrID) {
            var tempAttributes = [];
            var tempattributeOfTypeDropdowntree = '';
            var tempAttrubteValuesForDropdowntree = [];
            var tempAttributesRelations = [];
            tempAttributes = ($.grep($scope.entitytypeattributrearr, function (e) {
                return (e.AttributeID != SystemDefiendAttributes.EntityStatus && e.AttributeID != SystemDefiendAttributes.MyRoleGlobalAccess && e.AttributeID != SystemDefiendAttributes.MyRoleEntityAccess && e.AttributeID != SystemDefiendAttributes.Owner && e.AttributeID != SystemDefiendAttributes.Name && e.AttributeTypeID != parseInt(10) && e.AttributeTypeID != parseInt(6) && e.AttributeTypeID != parseInt(12));
            }));
            tempattributeOfTypeDropdowntree = ($.grep($scope.entitytypeattributrearr, function (e) {
                return (e.AttributeTypeID == parseInt(6) || e.AttributeTypeID == parseInt(12));
            }));
            tempAttrubteValuesForDropdowntree = ($.grep($scope.FulfillmentAttributes, function (e) {
                return (e.AttributeTypeID == parseInt(6) || e.AttributeTypeID == parseInt(12));
            }));
            if (tempAttrubteValuesForDropdowntree != undefined) {
                for (var j = 0; j < tempAttrubteValuesForDropdowntree.length; j++) {
                    tempAttributes.push(tempAttrubteValuesForDropdowntree[j]);
                }
            }
            tempAttributes = ($.grep(tempAttributes, function (e) {
                return (e.Id != parseInt(attrID.substring(0, attrID.lastIndexOf('_'))) || e.Level != parseInt(attrID.substring((attrID.lastIndexOf('_') + 1), attrID.length)));
            }));
            if (ControllerID != undefined) {
                $('#' + ControllerID).html("");
                $('#' + ControllerID).html("<option value='0'>-- Select --</option>");
                $.each(tempAttributes, function (val, item) {
                    if (item.AttributeTypeID == 6) {
                        $('#' + ControllerID).append($('<option ></option>').val(item.Id + "_" + item.Level).html(item.Caption));
                    } else if (item.AttributeTypeID == 12) {
                        $('#' + ControllerID).append($('<option ></option>').val(item.Id + "_" + item.Level).html(item.Caption));
                    } else {
                        $('#' + ControllerID).append($('<option ></option>').val(item.AttributeID + "_0").html(item.Caption));
                    }
                });
            }
        }

        function FillAttributes(ControllerID) {
            if (ControllerID != undefined) {
                var objentities = $scope.FulfillmentAttributes;
                $('#' + ControllerID).html("");
                $('#' + ControllerID).html("<option value='0'>-- Select --</option>");
                $.each(objentities, function (val, item) {
                    if (item.Level != undefined) {
                        var currentId = item.Id.toString() + "_" + item.Level.toString();
                        $('#' + ControllerID).append($("<option></option>").val(currentId.toString()).html(item.Caption));
                    } else {
                        $('#' + ControllerID).append($('<option ></option>').val(item.Id).html(item.Caption));
                    }
                });
            }
        }

        function ResetDropDown(currentUniqueId, ControlStep) {
            if (ControlStep == 1) {
                $('#ObjAttributeOptions' + currentUniqueId).html("");
                $('#ObjAttributeOptions' + currentUniqueId).html("<option value='0'>-- Select --</option>");
                $('#ObjAttributeOptions' + currentUniqueId).select2("val", "");
                $('#ObjAttributesRelations' + currentUniqueId).html("");
                $('#ObjAttributesRelations' + currentUniqueId).html("<option value='0'>-- Select --</option>");
                $('#ObjAttributesRelations' + currentUniqueId).select2("val", "");
            } else if (ControlStep == 2) {
                $('#ObjAttributesRelations' + currentUniqueId).html("");
                $('#ObjAttributesRelations' + currentUniqueId).html("<option value='0'>-- Select --</option>");
                $('#ObjAttributesRelations' + currentUniqueId).select2("val", "");
            }
        }

        function AddAttributeRelationsOnDefault() {
            var UniqueId = 0;
            var html = '';
            html += "<div  data-control='main'  data-Holder='holder' id='container" + UniqueId + "' data-uniquekey='container" + UniqueId + "' data-id='" + UniqueId + "'>";
            html += "<ul class='repeter' >";
            html += "<li class='form-inline'>";
            html += "<label>Attribute</label>";
            html += "<select ui-select2 ng-model=dummymodule_attribute_" + UniqueId + " id='ObjAttributes" + UniqueId + "'   data-role='Attributes'>";
            html += "<option value='0'>-- Select --</option>";
            html += "";
            html += "</select>";
            html += "<label>Options</label>";
            html += "<select ui-select2 ng-model=dummymodule_options_" + UniqueId + "  id='ObjAttributeOptions" + UniqueId + "' data-role='Options'>";
            html += "<option  value='0'>-- Select --</option>";
            html += "";
            html += "</select>";
            html += "<label>Relations</label>";
            html += "<select ui-select2 ng-model=dummymodule_relations_" + UniqueId + " multiple='multiple' id='ObjAttributesRelations" + UniqueId + "' data-role='EntityType'>";
            html += "<option value='0'>-- Select --</option>";
            html += "";
            html += "</select>";
            html += "<button class='btn' data-role='Add' ><i class='icon-plus' data-role='Add'></i></button>&nbsp;<button class='btn' data-role='Remove' ><i class='icon-remove' data-role='Remove'></i></button>";
            html += "";
            html += "</li>";
            html += "</ul>";
            html += "</div>";
            $('#AttributeToAttributeRelationsBody').html($compile(html)($scope));
            $timeout(function () {
                FillAttributes("ObjAttributes0");
            }, 200);
        }

        function LoadFillAttributesRelations(ControllerID1, attrID1, relationIDs1) {
            var tempAttributes1 = [];
            var tempattributeOfTypeDropdowntree1 = [];
            var tempAttrubteValuesForDropdowntree1 = '';
            tempAttributes1 = ($.grep($scope.entitytypeattributrearr, function (e) {
                return (e.AttributeID != parseInt(attrID1.substring(0, attrID1.lastIndexOf("_"))) && e.AttributeID != SystemDefiendAttributes.Owner && e.AttributeID != SystemDefiendAttributes.EntityStatus && e.AttributeID != SystemDefiendAttributes.MyRoleGlobalAccess && e.AttributeID != SystemDefiendAttributes.MyRoleEntityAccess && e.AttributeID != SystemDefiendAttributes.Name && e.AttributeTypeID != parseInt(10) && e.AttributeTypeID != parseInt(6) && e.AttributeTypeID != parseInt(12));
            }));
            tempattributeOfTypeDropdowntree1.push(($.grep($scope.entitytypeattributrearr, function (e) {
                return (e.AttributeTypeID == parseInt(6) || e.AttributeTypeID == parseInt(12));
            }))[0]);
            tempAttrubteValuesForDropdowntree1 = ($.grep($scope.FulfillmentAttributes, function (e) {
                return (e.AttributeTypeID == parseInt(6) || e.AttributeTypeID == parseInt(12));
            }));
            if (parseInt(attrID1.substring((attrID1.lastIndexOf("_") + 1), attrID1.length)) > 0) {
                if (tempAttrubteValuesForDropdowntree1 != undefined) {
                    for (var j = 0; j < tempAttrubteValuesForDropdowntree1.length; j++) {
                        if (tempAttrubteValuesForDropdowntree1[j].Level != (parseInt(attrID1.substring((attrID1.lastIndexOf("_") + 1), attrID1.length)))) {
                            tempAttributes1.push(tempAttrubteValuesForDropdowntree1[j]);
                        }
                    }
                }
            } else {
                if (tempAttrubteValuesForDropdowntree1 != undefined) {
                    for (var j = 0; j < tempAttrubteValuesForDropdowntree1.length; j++) {
                        tempAttributes1.push(tempAttrubteValuesForDropdowntree1[j]);
                    }
                }
            }
            if (ControllerID1 != undefined) {
                $('#' + ControllerID1).html("");
                $('#' + ControllerID1).html("<option value='0'>-- Select --</option>");
                $.each(tempAttributes1, function (val, item1) {
                    if (item1.AttributeTypeID == 6) {
                        $('#' + ControllerID1).append($('<option ></option>').val(item1.Id.toString() + "_" + item1.Level).html(item1.Caption));
                    } else if (item1.AttributeTypeID == 12) {
                        $('#' + ControllerID1).append($('<option ></option>').val(item1.Id.toString() + "_" + item1.Level).html(item1.Caption));
                    } else {
                        $('#' + ControllerID1).append($('<option ></option>').val(item1.AttributeID.toString() + "_0").html(item1.Caption));
                    }
                });
                $timeout(function () {
                    if (relationIDs1 != undefined) {
                        var Values = relationIDs1.split(",");
                        $('#' + ControllerID1).select2("val", Values);
                    }
                }, 10);
            }
        }

        function LoadAttributeToAttributeRelationsHtmlOnUpadate(Result) {
            if (Result != null && Result.length > 0) {
                var html = '';
                $.each(Result, function (val, item) {
                    var UniqueId = val;
                    html += "<div  data-control='main' data-Holder='holder' id='container" + UniqueId + "' data-uniquekey='container" + UniqueId + "' data-id='" + UniqueId + "'>";
                    html += "<ul class='repeter' >";
                    html += "<li class='form-inline'>";
                    html += "<label>Attribute</label>";
                    html += "<select ui-select2 ng-model=dummymodule_attribute_" + UniqueId + " id='ObjAttributes" + UniqueId + "'   data-role='Attributes'>";
                    html += "<option value='0'>-- Select --</option>";
                    html += "";
                    html += "</select>";
                    html += "<label>Options</label>";
                    html += "<select ui-select2 ng-model=dummymodule_options_" + UniqueId + " id='ObjAttributeOptions" + UniqueId + "' data-role='Options'>";
                    html += "<option  value='0'>-- Select --</option>";
                    html += "";
                    html += "</select>";
                    html += "<label>Relations</label>";
                    html += "<select ui-select2 multiple='multiple' ng-model=dummymodule_relations_" + UniqueId + " id='ObjAttributesRelations" + UniqueId + "' data-role='EntityType'>";
                    html += "<option value='0'>-- Select --</option>";
                    html += "";
                    html += "</select>";
                    html += "<button class='btn' data-role='Add' ><i class='icon-plus' data-role='Add'></i></button>&nbsp;<button class='btn' data-role='Remove' ><i class='icon-remove' data-role='Remove'></i></button>";
                    html += "";
                    html += "</li>";
                    html += "</ul>";
                    html += "</div>";
                });
                $('#AttributeToAttributeRelationsBody').html($compile(html)($scope));
                $.each(Result, function (indx, item) {
                    FillAttributes("ObjAttributes" + indx);
                    var selectVal = item.AttributeID + "_" + item.AttributeLevel;
                    $timeout(function () {
                        $("#ObjAttributes" + indx).select2("val", selectVal.toString());
                    }, 10);
                    LoadFillAttributeOptions("ObjAttributeOptions" + indx, item.AttributeID, item.AttributeLevel, item.AttributeOptionID)
                    LoadFillAttributesRelations("ObjAttributesRelations" + indx, item.AttributeID + "_" + item.AttributeLevel, item.AttributeRelationID);
                });
            } else {
                AddAttributeRelationsOnDefault();
            }
        }

        function AddAttributeRelationsOnPageLoad() {
            if ($scope.EntityID == 0) {
                AddAttributeRelationsOnDefault();
            } else {
                MetadataService.GetAttributeToAttributeRelationsByID($scope.EntityID).then(function (entityAttrToAttrRelation) {
                    LoadAttributeToAttributeRelationsHtmlOnUpadate(entityAttrToAttrRelation.Response);
                });
            }
        }

        function LoadFillAttributeOptions(ControllerID, entityAttribtueId, entityAttributeLevel, selectedAttributeOption) {
            GetAttributeOptionsInAttrToAttrRelations(entityAttribtueId, entityAttributeLevel).then(function (fulfillmentAttributeOptionsObj) {
                if (fulfillmentAttributeOptionsObj.Response.length > 0) {
                    if (ControllerID != undefined) {
                        $('#' + ControllerID).html("");
                        $('#' + ControllerID).html("<option value='0'>-- Select --</option>");
                        if (fulfillmentAttributeOptionsObj.Response != null) {
                            $.each(fulfillmentAttributeOptionsObj.Response, function (val, item) {
                                $('#' + ControllerID).append($("<option></option>").val(item.Id).html(item.Caption));
                            });
                            $timeout(function () {
                                $('#' + ControllerID).select2("val", selectedAttributeOption);
                            }, 10);
                        }
                    }
                }
            });
        }
        $scope.ValidationValueCheck = function () {
            var selectvalue = $scope.ValidationType;
            if ($scope.ValidationType == "presence" || $scope.ValidationType == "integer" || $scope.ValidationType == "float" || $scope.ValidationType == "same-as:Selector" || $scope.ValidationType == "email") {
                $scope.ValidationValue = '';
                $scope.ShowValue = false;
            } else {
                $scope.ShowValue = true;
            }
        }
        $scope.AddCheckList = function (Index) {
            Index = Index + 1;
            if ($scope.AdminTaskCheckList.length > 0) {
                if (Index == $scope.AdminTaskCheckList.length) {
                    $scope.AdminTaskCheckList.push({
                        ID: 0,
                        StatusOptions: "",
                        IsDeleted: false,
                        SortOrder: $scope.AdminTaskCheckList.length + 1
                    });
                } else if (Index < $scope.AdminTaskCheckList.length) {
                    $scope.AdminTaskCheckList.push({
                        ID: 0,
                        StatusOptions: "",
                        IsDeleted: false,
                        SortOrder: Index + 1
                    });
                    var add = parseInt($scope.AdminTaskCheckList.length) - parseInt(Index);
                    for (var j = Index; j < add; j++) {
                        $scope.AdminTaskCheckList[j].SortOrder = add + 1;
                        add++;
                    }
                }
            }
        };
        $scope.contextCheckListIndex = 0;
        $scope.AddCheckListNext = function (index) {
            if ($scope.contextCheckListIndex == -1) {
                $scope.AdminTaskCheckList.push({
                    ID: 0,
                    StatusOptions: '',
                    SortOrder: 1,
                    IsExisting: true
                });
            } else {
                $scope.contextCheckListIndex = parseInt(index) + 1;
                $scope.AdminTaskCheckList.splice(parseInt($scope.contextCheckListIndex), 0, {
                    ID: 0,
                    StatusOptions: '',
                    SortOrder: $scope.contextCheckListIndex,
                    IsExisting: true
                });
            }
        }
        $scope.DeleteEntityChecklists = function (index, ID) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2003.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        $scope.AdminTaskCheckList.splice(index, 1);
                    }, 100);
                }
            });
        }
        $scope.RemoveCheckList = function (Index, ID) {
            if ($scope.AdminTaskCheckList.length == 1) {
                bootbox.alert($translate.instant('LanguageContents.Res_4919.Caption'));
            } else {
                bootbox.confirm($translate.instant('LanguageContents.Res_2004.Caption'), function (result) {
                    if (result) {
                        $timeout(function () {
                            if ($scope.AdminTaskCheckList.length > 1) {
                                if (ID != 0) {
                                    MetadataService.DeleteEntityTypeStatusOptions(ID).then(function (deleteentitystatus) { });
                                }
                                $timeout(function () {
                                    $scope.AdminTaskCheckList.splice(Index, 1);
                                }, 10);
                            }
                        }, 100);
                    }
                });
            }
        };
        $scope.ReassignMembersData = [];
        $scope.formatResult = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelection = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.addnewAttributeGroupToGrid = function () {
            $("#btnTemp4").click();
            $("#entitytypeformPage4").removeClass('notvalidate');
            if ($("#entitytypeformPage4 .error").length > 0) {
                return false;
            }
            if ($scope.attributegroupGridData != undefined && $scope.attributegroupGridData != "") {
                if ($.grep($scope.attributegroupGridData, function (e) {
					return e.AttributeGroupID == $scope.EntType.ddlAttributeGroupID;
                }).length > 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_2058.Caption'));
                    return false;
                }
            }
            if ($scope.EntType.LocationType == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4622.Caption'));
                return false;
            }
            if ($scope.EntType.RepresentationType == "") {
                bootbox.alert($translate.instant('LanguageContents.Res_4626.Caption'));
                return false;
            }
            var attributerelationCaption = $.grep($scope.attributegroupList, function (e) {
                return e.Id == parseInt($scope.EntType.ddlAttributeGroupID);
            })[0].Caption;
            var LocationTypeCaption = "";
            if ($scope.EntType.LocationType == 1) LocationTypeCaption = "Append with detail block";
            else if ($scope.EntType.LocationType == 2) LocationTypeCaption = "Represent as separate block";
            else if ($scope.EntType.LocationType == 3) LocationTypeCaption = "Represent as Tab";
            $scope.attributegroupGridData.push({
                "ID": 0,
                "AttributeGroupID": $scope.EntType.ddlAttributeGroupID,
                "AttributeGroupCaption": attributerelationCaption,
                "Caption": $scope.EntType.AttributeGroupCaption,
                "LocationType": $scope.EntType.LocationType,
                "RepresentationType": $scope.EntType.RepresentationType,
                "EntityTypeID": $scope.EntityID,
                "SortOrder": 0,
                "AttributeGroupGlobalAccess": $scope.EntType.AttrGroupGlobalRolevalue,
                "LocationTypeCaption": LocationTypeCaption,
                "RepresentationTypeCaption": $scope.EntType.RepresentationType == false || $scope.EntType.RepresentationType == "false" ? "Key Value Pair" : "Populate as List",
                "PageSize": $scope.EntType.PageSize,
                "IsTabInEntityCreation": $scope.EntType.IsTabInEntityCreation
            });
            $scope.EntType.ddlAttributeGroupID = 0;
            $scope.EntType.AttributeGroupCaption = '';
            $scope.EntType.LocationType = 0;
            $scope.EntType.RepresentationType = "";
            $scope.EntType.EnableAttributeGroupAdd = true;
            $scope.EntType.EnableAttributeGroupUpdate = false;
            $scope.EntType.AttrGroupGlobalRolevalue = [];
            $scope.EntType.PageSize = 0;
            $scope.EntType.IsTabInEntityCreation = false;
            $timeout(function () {
                DragDropAttributeGroup();
            }, 200);
        };
        $scope.UpdateAttributeGroupAndLoadToGrid = function () {
            $("#btnTemp4").click();
            $("#entitytypeformPage4").removeClass('notvalidate');
            if ($("#entitytypeformPage4 .error").length > 0) {
                return false;
            }
            if ($scope.attributegroupGridData != undefined && $scope.attributegroupGridData != "") {
                if ($scope.tempAttributeGrpID != $scope.EntType.ddlAttributeGroupID) {
                    if ($.grep($scope.attributegroupGridData, function (e) {
						return e.AttributeGroupID == $scope.EntType.ddlAttributeGroupID;
                    }).length > 0) {
                        bootbox.alert($translate.instant('LanguageContents.Res_2058.Caption'));
                        return false;
                    }
                }
            }
            var attributeGroupCaption = $.grep($scope.attributegroupList, function (e) {
                return e.Id == parseInt($scope.EntType.ddlAttributeGroupID);
            })[0].Caption;
            var LocationTypeCaption = "";
            if ($scope.EntType.LocationType == 1) LocationTypeCaption = "Append with detail block";
            else if ($scope.EntType.LocationType == 2) LocationTypeCaption = "Represent as separate block";
            else if ($scope.EntType.LocationType == 3) LocationTypeCaption = "Represent as Tab";
            $.grep($scope.attributegroupGridData, function (e) {
                return e.AttributeGroupID == parseInt($scope.tempAttributeGrpID);
            })[0].AttributeGroupCaption = attributeGroupCaption;
            $.grep($scope.attributegroupGridData, function (e) {
                return e.AttributeGroupID == parseInt($scope.tempAttributeGrpID);
            })[0].Caption = $scope.EntType.AttributeGroupCaption;
            $.grep($scope.attributegroupGridData, function (e) {
                return e.AttributeGroupID == parseInt($scope.tempAttributeGrpID);
            })[0].LocationType = $scope.EntType.LocationType;
            $.grep($scope.attributegroupGridData, function (e) {
                return e.AttributeGroupID == parseInt($scope.tempAttributeGrpID);
            })[0].RepresentationType = $scope.EntType.RepresentationType;
            $.grep($scope.attributegroupGridData, function (e) {
                return e.AttributeGroupID == parseInt($scope.tempAttributeGrpID);
            })[0].LocationTypeCaption = LocationTypeCaption;
            $.grep($scope.attributegroupGridData, function (e) {
                return e.AttributeGroupID == parseInt($scope.tempAttributeGrpID);
            })[0].RepresentationTypeCaption = $scope.EntType.RepresentationType == false || $scope.EntType.RepresentationType == "false" ? "Key Value Pair" : "Populate as List";
            $.grep($scope.attributegroupGridData, function (e) {
                return e.AttributeGroupID == parseInt($scope.tempAttributeGrpID);
            })[0].AttributeGroupID = $scope.EntType.ddlAttributeGroupID;
            $.grep($scope.attributegroupGridData, function (e) {
                return e.AttributeGroupID == parseInt($scope.tempAttributeGrpID);
            })[0].AttributeGroupGlobalAccess = $scope.EntType.AttrGroupGlobalRolevalue;
            $.grep($scope.attributegroupGridData, function (e) { return e.AttributeGroupID == parseInt($scope.tempAttributeGrpID); })[0].PageSize = $scope.EntType.PageSize;
            $.grep($scope.attributegroupGridData, function (e) { return e.AttributeGroupID == parseInt($scope.tempAttributeGrpID); })[0].IsTabInEntityCreation = $scope.EntType.IsTabInEntityCreation;
            $scope.EntType.ddlAttributeGroupID = 0;
            $scope.EntType.AttributeGroupCaption = '';
            $scope.EntType.LocationType = 0;
            $scope.EntType.RepresentationType = "";
            $scope.tempAttributeGrpID = 0;
            $scope.EntType.EnableAttributeGroupAdd = true;
            $scope.EntType.EnableAttributeGroupUpdate = false;
            $scope.EntType.AttrGroupGlobalRolevalue = [];
            $scope.EntType.PageSize = 0;
            $scope.EntType.IsTabInEntityCreation = false;
            $timeout(function () {
                DragDropAttributeGroup();
            }, 200);
        };
        $scope.LoadAttributeGrpToCtrlOnGridClick = function (row) {
            var attributeGrp = $.grep($scope.attributegroupGridData, function (e) {
                return e.AttributeGroupID == parseInt(row.AttributeGroupID);
            })[0];
            $scope.EntType.ddlAttributeGroupID = attributeGrp.AttributeGroupID;
            $scope.EntType.AttributeGroupCaption = attributeGrp.Caption;
            $scope.EntType.RepresentationType = attributeGrp.RepresentationType.toString();
            $scope.EntType.LocationType = attributeGrp.LocationType;
            $scope.EntType.AttrGroupGlobalRolevalue = attributeGrp.AttributeGroupGlobalAccess;
            $scope.tempAttributeGrpID = row.AttributeGroupID;
            $scope.EntType.EnableAttributeGroupAdd = false;
            $scope.EntType.EnableAttributeGroupUpdate = true;
        };
        $scope.ChangeAttributeGroup = function () {
            $("#entitytypeformPage4").addClass('notvalidate');
            $scope.EntType.AttributeGroupCaption = $.grep($scope.attributegroupList, function (e) {
                return e.Id == parseInt($scope.EntType.ddlAttributeGroupID);
            })[0].Caption;
            $scope.EntType.PageSize = 10;
        };
        $scope.DeleteAttributeGroupByID = function (row) {
            bootbox.confirm($translate.instant('LanguageContents.Res_1824.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        if (row.ID != 0) {
                            CommonService.DeleteCustomtabByID(0, $scope.attributegroupGridData[0].AttributeGroupID, $scope.attributegroupGridData[0].EntityTypeID).then(function (DeleteAttributeGroupTab) { });
                            MetadataService.DeleteEntityTypeAttributeGroupRelation(row.ID).then(function (res) {
                                if (res.Response == 1) {
                                    $scope.attributegroupGridData = $.grep($scope.attributegroupGridData, function (e) {
                                        return parseInt(e.AttributeGroupID) != parseInt(row.AttributeGroupID);
                                    });
                                    NotifySuccess($translate.instant('LanguageContents.Res_4114.Caption'));
                                } else {
                                    NotifyError($translate.instant('LanguageContents.Res_4289.Caption'));
                                }
                            });
                        } else {
                            $scope.attributegroupGridData = $.grep($scope.attributegroupGridData, function (e) {
                                return parseInt(e.AttributeGroupID) != parseInt(row.AttributeGroupID);
                            });
                        }
                    }, 50);
                    $scope.ClearAttributeGroupCtrls();
                }
            });
        };
        $scope.ClearAttributeGroupCtrls = function () {
            $scope.EntType.ddlAttributeGroupID = null;
            $scope.EntType.AttributeGroupCaption = "";
            $scope.EntType.LocationType = 0;
            $scope.EntType.RepresentationType = "";
            $scope.EntType.EnableAttributeGroupAdd = true;
            $scope.EntType.EnableAttributeGroupUpdate = false;
            $scope.EntType.AttrGroupGlobalRolevalue = [];
            $scope.EntType.PageSize = 0;
            $scope.EntType.IsTabInEntityCreation = false;
        }
        $scope.RestrictShortDesc = function (event) {
            if ($scope.EntType.Shortdescription != undefined) {
                if ($scope.EntType.Shortdescription.length >= 3) {
                    event.preventDefault();
                    return false;
                }
            }
        };
        $scope.DuplicatePopUpEntityTypeByID = function (row) {
            $timeout(function () {
                $scope.DuplicateEntityType.EntityTypeID = row.entity.Id;
                $scope.DuplicateEntityType.EntityCaption = row.entity.Caption;
                $scope.DuplicateEntityType.EntityShortDescription = row.entity.ShortDescription;
                $scope.DuplicateEntityType.EntityTypeColorcode = row.entity.ColorCode;
                $scope.DuplicateEntityType.Description = row.entity.Description;
            }, 500);
            $timeout(function () {
                $("#Duplicateborder-color input").val($scope.ColorCodeGlobal);
                $(".current-color").css('background-color', '#' + $scope.DuplicateEntityType.EntityTypeColorcode);
            }, 500);
            $timeout(function () {
                $('#duplentitypeCaption').focus().select();
            }, 1000);
        }
        $scope.CreateDuplicateEntityType = function () {
            var SaveDuplicateEntityType = {};
            SaveDuplicateEntityType.EntityTypeID = $scope.DuplicateEntityType.EntityTypeID;
            SaveDuplicateEntityType.EntityCaption = $scope.DuplicateEntityType.EntityCaption;
            SaveDuplicateEntityType.EntityShortDescription = $scope.DuplicateEntityType.EntityShortDescription;
            SaveDuplicateEntityType.Description = $scope.DuplicateEntityType.Description;
            SaveDuplicateEntityType.EntityTypeColorcode = $scope.DuplicateEntityType.EntityTypeColorcode.replace("#", "");;
            MetadataService.DuplicateEntityType(SaveDuplicateEntityType).then(function (EntityAttributeResponse) {
                if (EntityAttributeResponse.Response != null) {
                    $scope.entitytpesdata = EntityAttributeResponse.Response;
                    $scope.entitytpesdatarootlevel = EntityAttributeResponse.Response;
                    NotifySuccess($translate.instant('LanguageContents.Res_4252.Caption'));
                } else {
                    MetadataService.GetEntityType(6).then(function (entitytypes) {
                        $scope.entitytpesdata = entitytypes.Response;
                        $scope.entitytpesdatarootlevel = entitytypes.Response;
                    });
                    NotifyError($translate.instant('LanguageContents.Res_4319.Caption'));
                }
                $timeout(function () {
                    $('#DuplicateEntityTypeModel').modal('hide');
                }, 500);
            });
        }
        $scope.closevalidationpopup = function (attr) {
            var validationset = $.grep($scope.AtributeValueList, function (e) {
                return e.AttributeID == parseInt(attr.AttributeID);
            });
            if (validationset.length != 0) {
                if ($('#att' + attr.AttributeID).find('.icon-ok').hasClass('ng-hide') == true) {
                    $('#att' + attr.AttributeID).find('.icon-ok').removeClass('ng-hide');
                    $('#att' + attr.AttributeID).find('.color-primary').addClass('ng-hide');
                } else $('#validation' + attr.AttributeID).removeClass('color-primary');
            } else {
                if ($('#att' + attr.AttributeID).find('.color-primary').hasClass('ng-hide') == true) {
                    $('#att' + attr.AttributeID).find('.icon-ok').addClass('ng-hide');
                    $('#att' + attr.AttributeID).find('.color-primary').removeClass('ng-hide');
                } else $('#validation' + attr.AttributeID).addClass('color-primary');
            }
        }
        $scope.set_color = function (clr) {
            return {
                'background-color': "#" + clr.toString().trim()
            }
        }
    }
    app.controller("mui.admin.cmspagetypeattributerelationCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$window', '$compile', '$translate', 'MetadataService', 'AccessService', 'CommonService', muiadmincmspagetypeattributerelationCtrl]);
})(angular, app);