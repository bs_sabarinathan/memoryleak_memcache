﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Task.Interface;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;
using Newtonsoft.Json.Linq;
using System.Collections;
using BrandSystems.Marcom.Core.Access.Interface;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Dal.Task.Model;

namespace BrandSystems.Marcom.Core.Interface.Managers
{
    public interface ITaskManager
    {

        void test();

        /// <summary>
        /// Returns Task class.
        /// </summary>
        IAdminTask TasksService();

        IEntityTask EntityTasksService();
        ITaskMembers EntityTaskMembersService();
        /// <summary>
        /// Returns TaskAttachment class.
        /// </summary>
        INewTaskAttachments TasksAttachmentService();

        Tuple<int, ITaskList> InsertUpdateTaskList(int id, string caption, string description, int sortorder);


        int AddUpdateTaskFlag(string caption, string colorcode, string description, int sortorderid, int id = 0);
        bool DeleteSystemTaskList(int id);

        IList<ITaskLibraryTemplateHolder> GetTaskList();

        Object GetTaskTypes();

        bool UpdateTaskListSortOrder(int id, int sortorder);

        Tuple<int, IAdminTask> InsertTaskWithAttachments(int taskTypeID, int typeid, string TaskName, IList<IAdminTask> TaskList, IList<INewTaskAttachments> TaskAttachments, IList<IFile> TaskFiles, IList<IAttributeData> entityattributedata, IList<IAdminTaskCheckList> AdminTaskChkLst, JArray arrAttchObj, IList<IEntityPeriod> entityPeriods, IList<IEntityAmountCurrencyType> listentityamountcurrencytype, JArray EntityPhaseStepDetails);



        int InsertUpdateTaskTemplateCondition(int id, int tasktemplateID, int typeID, int sortorder, int attributeID, string value, int attributeLevel, int conditionType);

        int InsertTempTaskList(int templateId, int tasklistId, int SortOrder);

        int InsertUpdateTemplate(int ID, string caption, string description);

        IList<ITaskTemplate> GetTaskTemplateDetails();

        bool DeleteTaskTemplateListById(int templateID);
        Tuple<bool, IAdminTask> UpdateAdminTask(int milestoneTypeID, string Name, string description, int tasktype, IList<IAttributeData> milestoneObj, int entityId, IList<IAdminTaskCheckList> adminChkLst);
        IList<ITaskFile> GetTaskAttachmentFile(int taskID);

        IAttachments EntityTasksAttachmentService();
        IList<IFile> GetEntityTaskAttachmentFile(int taskID);
        IList<IFile> ViewAllFilesByEntityID(int taskID, int VersionFileID);
        bool UpdateAttachmentVersionNo(int TaskID, int SelectedVersion, int VersioningFileId);
        bool InsertTaskAttachments(int TaskID, IList<INewTaskAttachments> TaskAttachments, IList<IFile> TaskFiles);
        bool InsertEntityTaskAttachments(int TaskID, IList<IAttachments> TaskAttachments, IList<IFile> TaskFiles);
        int InsertEntityTaskAttachmentsVersion(int TaskID, IList<IAttachments> TaskAttachments, IList<IFile> TaskFiles, int FileID, int VersioningFileId);
        IList<ITaskFlag> GetTaskFlags();

        bool DeleteTaskFlagById(int flagid);
        bool UpdateTemplateTaskListSortOrder(int TemplateId, int taskListid, int sortorder);
        bool DeleteAttachments(int ActiveFileid);
        IList<ITaskLibraryTemplateHolder> GetEntityTaskList(int entityID);

        IList<IEntityTaskList> GetOverViewEntityTaskList(int entityID);
        Tuple<int, IEntityTask> InsertEntityTaskWithAttachments(int parentEntityID, int taskTypeID, int entitytypeid, string TaskName, IList<IEntityTask> TaskList, IList<INewTaskAttachments> TaskAttachments, IList<IFile> TaskFiles, IList<ITaskMembers> taskMembers, IList<IAttributeData> entityattributedata, JArray attachFiles, IList<IEntityTaskCheckList> entityCheckList, IList<IEntityPeriod> entityPeriods, int TaskcreatorID, IList<IEntityAmountCurrencyType> listentityamountcurrencytype, JArray listentityphasedetails, JObject externalObjects = null);
        bool UpdateOverviewEntityTaskList(int OnTimeStatus, string OnTimeComment, int ID);

        Tuple<bool, IList<ITaskMembers>> InsertTaskMembers(int EntityID, int taskID, IList<ITaskMembers> TaskMembers, IList<ITaskMembers> TaskGlobalMembers, string CurrentStepName = "");
        Tuple<bool, int, string> UpdateTaskStatus(int taskID, int status, int entityID = 0, int stepId = 0, int userid = 0, bool isExternalTask = false);
        bool updateTaskMemberFlag(int taskid, string colorCode);
        Tuple<int, IEntityTaskList> InsertUpdateEntityTaskList(int id, string caption, string description, int sortorder, int entityID, int TaskListcreator);
        bool UpdateEntityTaskListSortOrder(JArray SortOrderObject);
        Tuple<int, IEntityTask> InsertUnassignedEntityTaskWithAttachments(int parentEntityID, int taskTypeID, int entitytypeid, string TaskName, IEntityTask TaskList, IList<INewTaskAttachments> TaskAttachments, IList<IFile> TaskFiles, IList<ITaskMembers> taskMembers, IList<IAttributeData> entityattributedata, IList<IEntityTaskCheckList> AdminTaskChkLst, JArray arrAttchObj);
        ITaskLibraryTemplateHolder DuplicateEntityTaskList(int id, int entityID);

        IList GetMytasks(int FilterByentityID, int[] FilterStatusID, int pageNo, int AssignRole, int[] CustomFilterID);

        IList<IMyTaskCollection> GetMytasksAPI(int FilterByentityID, int[] FilterStatusID, int StartRowno, int MaxRowNo, int AssignRole, int userID);

        bool DeleteEntityTaskLis(int taskListID, int entityID);

        Tuple<int, IEntityTask> DuplicateEntityTask(int taskId, int entityID = 0);

        int DeleteEntityTask(int taskID, int entityID);

        Tuple<int, IEntityTask> CompleteUnnassignedEntityTask(int taskId);

        Tuple<bool, int, string> UpdatetasktoNotApplicableandUnassigned(int taskID);

        bool DeleteAdminTask(int taskID);

        bool DeleteTemplateConditionById(int templateCondID);

        bool DeleteAdminTemplateTaskRelationById(int TaskListId, int TemplateId);
        bool DeleteTaskMemberById(int id, int taskID);

        bool CopyFileFromTaskToEntityAttachments(int entityID, int ActiveFileID);

        void UpdateTaskTemplateCriteria(string TemplateCriteriaText, int TemplateID);

        bool UpdatetaskAttachmentDescription(int id, string friendlyName, string description);

        bool UpdatetaskLinkDescription(int id, string friendlyName, string description, string URL, int linktype);

        bool DeleteFileByID(int ID);

        int InsertLink(int EntityID, string Name, string URL, int linkType, string Description, int ActiveVersionNo, int TypeID, string CreatedOn, int OwnerID, int ModuleID);

        int InsertLinkInAdminTasks(int EntityID, string Name, string URL, int linkType, string Description, int ActiveVersionNo, int TypeID, string CreatedOn, int OwnerID, int ModuleID);

        bool DeleteLinkByID(int ID);


        bool SendReminderNotification(int taskmemberid, int taskid);
        bool UpdatetaskEntityTaskDetails(int TaskID, string taskName, string description, string note, string Duedate, string taskction, int entityID = 0);
        bool UpdatetaskEntityTaskDueDate(int TaskID, string Duedate);

        IEntityTask GetEntityTaskDetails(int EntityTaskID);

        IAdminTaskCheckList AdminTaskCheckListService();
        IEntityTaskCheckList EntityTaskCheckListService();

        bool DeleteAdminTaskCheckListByID(int chkLstID);

        bool UpdateTaskSortOrder(int TaskId, int taskListID, int sortorder);

        bool UpdateEntityTaskSortOrder(JArray SortOrderObject);

        IList<IEntityTaskCheckList> getTaskchecklist(int TaskId);

        bool InsertTaskCheckList(int taskID, string CheckListName, int sortOrder);

        /// <summary>
        /// Check the task checklist
        /// </summary>
        /// <param name="proxy">ID</param>
        /// <returns>bool</returns>
        bool ChecksTaskCheckList(int Id, bool Status);

        bool DeleteEntityCheckListByID(int chkLstID);

        IList<IEntitySublevelTaskHolder> GetSublevelTaskList(int entityID);

        IList<IMyTaskCollection> GetEntityUpcomingTaskList(int FilterByentityID, int FilterStatusID, int EntityID, bool IsChildren);

        bool CopyAttachmentsfromtask(int[] fileids, int taskid);

        bool CopyAttachmentsfromtask(int[] fileids, int taskid, int[] linkids);


        bool CopyAttachmentsfromtaskToExistingTasks(int[] TaskIDList, int FileID, string fileType);
        bool EnableDisable(bool status);
        bool gettaskflagstatus();

        bool copytogeneralattachment(int[] taskid, int fileid);

        bool copytogeneralattachment(int taskid, int fileid, int linkids);

        bool DeleteTaskFileByid(int ID, int EntityId);
        //   bool DeleteTaskLinkByid(int ID);

        bool DeleteTaskLinkByid(int ID, int EntityId);

        /// <summary>
        /// DeleteLinkByID.
        /// </summary>
        /// <param name="proxy">ID Parameter</param>
        /// <returns>bool</returns>
        bool DeleteAdminTaskLinkByID(int ID);


        int InsertUpdateEntityTaskCheckList(int Id, int taskId, String CheckListName, bool ChkListStatus, bool ISowner, int sortOrder, bool IsNew, int entityID = 0);

        IList<ITaskLibraryTemplateHolder> GetEntityTaskListWithoutTasks(int entityID);

        IList<IEntityTask> GetEntityTaskListDetails(int entityID, int taskListID);

        int GettaskCountByStatus(int tasklistID, int EntityID, int[] status);

        Tuple<bool, IList<ITaskMembers>> InsertUnAssignedTaskMembers(int EntityID, int taskID, IList<ITaskMembers> TaskMembers, ITaskMembers TaskOwner, IList<ITaskMembers> GlobalTaskMembers);

        bool UpdatetaskAdminAttachmentDescription(int id, string friendlyName, string description);
        bool UpdatetaskAdminLinkDescription(int id, string friendlyName, string description, string URL, int LinkType);
        IList<ITaskLibraryTemplateHolder> GetExistingEntityTasksByEntityID(int entityID);

        IList GetMyFundingRequests(int[] FilterStatusID, int AssignRole);

        IList FetchUnassignedTaskforReassign(int entityId, int taskListId);

        IList<IEntityTask> AssignUnassignTasktoMembers(int entityId, int taskListId, int[] taskCollection, int[] memberCollection, DateTime? dueDate);

        IList GetAdminTasksById(string[] tasklistArr);

        IList ExportTaskList(int tasklistArr, string NewGuids, string TaskLibName);

        string InsertTaskLibImport(string FileImport, int Taskid);
     
        IList<IAdminTaskCheckList> getAdminTaskchecklist(int TaskId);

        IList<IMilestoneMetadata> GetAdminTaskMetadatabyTaskID(int taskID);
        IList<ITasktemplateCondition> GetTaskTemplateConditionByTaskTempId(int TaskTempID);
        IList<ITaskLibraryTemplateHolder> GetFiltertaskCountByStatus(Dictionary<int, int> Maintask, int[] filter);

        IList<ITaskLibraryTemplateHolder> GetTemplateAdminTaskList();

        bool UpdateEntityTask(params object[] CollectionIds);

        Tuple<IList<SourceDestinationMember>, IList<SourceDestinationMember>, bool> GetSourceToDestinationmembers(int taskID, int sourceEntityID, int destinationEntityId);

        IList<IEntityTypeRoleAcl> GetDestinationEntityIdRoleAccess(int EntityID);

        bool InsertUpdateDragTaskMembers(IList<SourceDestinationMember> TaskMemberslst, IList<SourceDestinationMember> EntitytaskMemberLst, int SourceTaskID, int TargetTasklistID, int TargetEntityID);

        bool UpdateDragEntityTaskListByTask(int TaskID, int SrcTaskListID, int TargetTaskListID);

        /// <summary>
        /// Gets the attributes details by entityID.
        /// </summary>
        /// <param name="Id">The entityId.</param>
        /// <returns>
        /// Ilist
        /// </returns>
        IList<IAttributeData> GetEntityAttributesDetails(int entityId);
        IList GetEntityTaskAttachmentinfo(int TaskID);

        /// <summary>
        /// Get member.
        /// </summary>
        /// <param name="ID">The ID.</param>
        /// <returns>IList of IEntityRoleUser</returns>
        IList<ITaskMembers> GetTaskMember(int taskID);

        /// Updating proof Task status 
        /// </summary>
        ///  <param name="proxy"></param>
        /// <param name="entityId">The TaskID</param>
        /// <param name="status">The Status</param>
        /// <returns>True or False</returns>
        Tuple<bool, int, string> UpdateProofTaskStatus(int taskID, int status, int userid);

        /// <summary>
        /// Getting Task details
        /// </summary>
        /// <param name="proxy"></param>
        /// <param name="entityID">The entityID</param>
        /// <param name="taskListID">The taskListID</param>
        /// <param name="pageNo">The pageNo</param>
        /// <returns>IList</returns>
        IList GetEntityTaskCollection(int entityID, int taskListID, int pageNo);

	    #region task approval flow
        IList GetAllApprovalFlowTemplates();
        IList GetAllApprovalFlowPhases(int templateid);
        IList GetAllApprovalFlowPhasesSteps(int templateid);
        int InsertUpdateTaskApprovalTemplate(JObject tasktempdetails);
        bool DeleteApprovalTemplate(int TempId);
        int InsertUpdateTaskTemplateStep(JObject taskstepdetails);
        int[] InsertUpdateTaskTemplatePhase(JArray taskphasedetails);
        bool UpdatetaskTemplateSortOrder(JObject Tasksortorderdetails);
        bool UpdateTemplatePhaseSortOrder(JObject Taskphasedetails);
        bool DeleteApprovalTaskStep(int Stepid);
        bool InsertUpdateAdminTaskTemplate(JObject PhaseStepDetails, int EntityID);
        bool DeleteAdminApprovalTaskStep(int Stepid);
        IList GetAllAdminApprovalFlowPhasesSteps(int EntityId);
        bool UpdateAdminTasklistSortOrder(JObject Tasksortorderdetails);
        bool UpdateAdmintaskPhaseSortOrder(JObject Taskphasedetails);
        #endregion

        bool HighResPdfDownload(string HiResPdf);

	    List<int> getAllTemplateTask();

        int InsertUpdateVersion(JObject taskversion);

        string GetTakUIData(int taskId);

        int ReinitiateRejectedTask(JObject jObj);

        Tuple<string, string> GetTaskDetails(int taskId, int versionNo = 0);

        string CreateNewVersion(int taskId, int SrcVesrionId = 0, JObject jobj = null);

        bool AddNewPhase(JObject jobj);

        bool RemovePhase(int phaseID, int EntityID);

        int AddPhaseStep(JObject jObj);

        bool AddMembersToPhaseStep(JObject jObj);

        bool DeletePhaseStep(int stepID, int EntityID);

        bool RemovePhaseStepApprovar(JObject jObj);

        bool sendReminderNotificationforTaskPhase(JObject jObj);

        List<int> getAllMemberIDbyStepID(int stepID);

        void UpdateTaskVersionDetails(int taskId, int versionNo = 0);

        #region DALIM BY RAMESH

        string GetListOfDocumentWorkFlowFromDalim();

        string GetListOfWorkFlowFromDalim(int Id, string workflowname);

        #endregion

        string GetApprovalFlowLibraryList(int typeid, bool IsAdminSide);

        string CollectApprovalFlowSteps(int typeid, int templateId, string name);

        List<string> GetDalimUserFromDB(string username);

        bool ReorderOverviewStructure(JObject jobj);

        IList getTaskApprovalRolesUsers(JObject jobj);

        IList getEntityApprovalRoleUsers(JObject jobj);

        bool UpdateTaskNotification(int taskID, int status, int entityID = 0, int stepId = 0, int userid = 0, bool externalConnector = false, int curPhaseId = 0, int currentstatus = 0);

        bool UpdateImageNameforTask(JObject jobj);

        bool IsApprovalReportEnables();

        bool DuplicateEntityTask(int taskId, int entityID, bool attachmentDuplicate, bool checklistDuplicate, int newentityID, int taskListID);
    }
}
