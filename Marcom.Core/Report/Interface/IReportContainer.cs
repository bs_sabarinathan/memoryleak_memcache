﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Report.Interface
{
   public interface IReportContainer
    {
         int ID { get; set; }
         int TypeID { get; set; }
         int Level { get; set; }
         string Name { get; set; }
         string ColorCode { get; set; }
         string ShortDescription { get; set; }
         Hashtable MetadataCollections { get; set; }
         List<Hashtable> MemberCollections { get; set; }
         Hashtable TaskOverviewSummary { get; set; }
         List<Hashtable> TaskCollections { get; set; }
         List<Hashtable> FinancialCollections { get; set; }
         List<Hashtable> TaskListCollections { get; set; }
         Dictionary<string, string> MetadataColumnCollection { get; set; }


         List<Hashtable> CostcentreCollections { get; set; }
         List<Hashtable> ObjectiveCollections { get; set; }
    }
}



