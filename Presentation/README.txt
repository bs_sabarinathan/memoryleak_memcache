List of third party library

//JQuery 
http://jquery.com/download/
//Current version used 2.0.3


//PlUpload
http://www.plupload.com/
//Current version used v1.5.7


//CanvasJS
http://canvasjs.com/download-html5-charting-graphing-library/
//Current version used v1.3.0 Beta

//Select2
https://github.com/ivaynberg/select2/archive/3.4.2.zip#
//Current version used v3.4.2


//Pick-a-color
https://github.com/lauren/pick-a-color
//Current version used v1.1.4

