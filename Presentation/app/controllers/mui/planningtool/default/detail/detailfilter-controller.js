﻿(function (ng, app) {
    "use strict";

    function muiplanningtooldefaultdetaildetailfilterCtrl($scope, $location, $resource, $timeout, $cookies, $compile, $window, DetailfilterService, $translate) {
        var model;
        $scope.Calanderopen = function ($event, model1) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calanderopened = true;
            $scope[model] = false;
            $scope[model1] = true;
            model = model1;
        };
        $scope.attributegroupTypeid = 0;
        $scope.treeNodeSelectedHolderValues = [];
        var IsSave = 0;
        var IsUpdate = 0;
        $scope.deletefiltershow = false;
        $scope.showSave = true;
        $scope.showUpdate = false;
        $scope.EntityTypeID = 0;
        $scope.appliedfilter = "No filter applied";
        $scope.FilterFields = {};
        $scope.PeriodOptionValue = {};
        var d = new Date.create();
        d = "1990-01-01";
        var DateValidate = dateFormat(d, $scope.DefaultSettings.DateFormat);
        var elementNode = 'DetailFilter';
        var selectedfilterid = 0;
        $scope.PercentageVisibleSettings = {};
        $scope.DropDownFilterTreePricing = {};
        $scope.TreePricing = [];
        $scope.items = [];
        $scope.FilterDataXML = [];
        if ($window.ActivityFilterName != '' && $window.ActivityFilterName != 'No filter applied') { } else {
            $window.ActivityFilterName = '';
        }
        var TypeID = 6;
        $scope.visible = false;
        $scope.dynamicEntityValuesHolder = {};
        $scope.dyn_Cont = '';
        $scope.fieldKeys = [];
        $scope.options = {};
        $scope.setFieldKeys = function () {
            var keys = [];
            angular.forEach($scope.FilterFields, function (key) {
                keys.push(key);
                $scope.fieldKeys = keys;
            });
        }
        $scope.OptionObj = {};
        $scope.fieldoptions = [];
        $scope.setoptions = function () {
            var keys = [];
            angular.forEach($scope.OptionObj, function (key) {
                keys.push(key);
                $scope.fieldoptions = keys;
            });
        }
        $scope.filterValues = [];
        $scope.atributesRelationList = [];
        $scope.atributesRelationListWithTree = [];
        $scope.filterSettingValues = [];
        $scope.atributeGroupList = [];
        $scope.DetailFilterCreation = function (event) {
            $scope.ClearScopeModle($scope.FilterFields);
            $scope.FilterSettingsLoad();
            $scope.Deletefilter = false;
            $scope.Applyfilter = true;
            $scope.Updatefilter = false;
            $scope.Savefilter = true;
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            IsSave = 0;
            IsUpdate = 0;
        };
        $("#DetailFilterCreation").on("onDetailFilterCreation", function (event) {
            $scope.ClearScopeModle();
        });
        $("#plandetailfiltersettings").on("ClearScope", function (event, filterid) {
            $scope.showSave = true;
            $scope.showUpdate = false;
            $scope.deletefiltershow = false;
            $scope.ClearScopeModle();
        });
        $scope.ClearFilterAttributes = function () {
            $scope.ClearFieldsAndReApply();
        }
        $scope.ClearScopeModle = function () {
            $scope.items = [];
            $scope.clearscope = !$scope.clearscope;
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            $scope.ddlParententitytypeId = [];
            $scope.ddlEntitymember = [];
            $("#activitydetailfilter").removeAttr('disabled');
            for (var variable in $scope.FilterFields) {
                if (typeof $scope.FilterFields[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        $scope.FilterFields[variable] = "";
                    }
                } else if (typeof $scope.FilterFields[variable] === "number") {
                    $scope.FilterFields[variable] = null;
                } else if (Array.isArray($scope.FilterFields[variable])) {
                    $scope.FilterFields[variable] = [];
                } else if (typeof $scope.FilterFields[variable] === "object") {
                    $scope.FilterFields[variable] = {};
                }
            }
            $scope.treePreviewObj = {};
            $scope.treeNodeSelectedHolderValues = [];
            var treecount = $.grep($scope.atributesRelationList, function (e) {
                return e.AttributeTypeId == 7
            });
            for (var i = 0; i < treecount.length; i++) {
                ClearSavedTreeNode(treecount[i].AttributeId);
            }
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
        }
        $scope.ClearFieldsAndReApply = function () {
            $scope.attributegroupTypeid = 0;
            IsSave = 0;
            $scope.items = [];
            $scope.clearscope = !$scope.clearscope;
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            $scope.ddlParententitytypeId = [];
            $scope.ddlEntitymember = [];
            $("#activitydetailfilter").removeAttr('disabled');
            for (var variable in $scope.FilterFields) {
                if (typeof $scope.FilterFields[variable] === "string") {
                    if (variable !== "ListSingleSelection_69") {
                        $scope.FilterFields[variable] = "";
                    }
                } else if (typeof $scope.FilterFields[variable] === "number") {
                    $scope.FilterFields[variable] = null;
                } else if (Array.isArray($scope.FilterFields[variable])) {
                    $scope.FilterFields[variable] = [];
                } else if (typeof $scope.FilterFields[variable] === "object") {
                    $scope.FilterFields[variable] = {};
                }
            }
            $scope.treePreviewObj = {};
            $scope.treeNodeSelectedHolderValues = [];
            var treecount = $.grep($scope.atributesRelationList, function (e) {
                return e.AttributeTypeId == 7
            });
            for (var i = 0; i < treecount.length; i++) {
                ClearSavedTreeNode(treecount[i].AttributeId);
            }
            var pricingObj = $.grep($scope.atributesRelationList, function (e) {
                return e.AttributeTypeId == 13
            });
            if (pricingObj != undefined) if (pricingObj.length > 0) for (var t = 0, obj; obj = pricingObj[t++];) {
                for (var z = 0, price; price = $scope.DropDownFilterTreePricing["AttributeId_Levels_" + obj.AttributeId + ""][z++];) {
                    if (price.selection != null) if (price.selection.length > 0) price.selection = [];
                }
            }
            $scope.ExpandingEntityID.EntityID = '0'   
            $scope.ShowPeriodOptions = false;
            $scope.costcemtreObject = [];
            $scope.MemberLists = [];
            var ApplyFilterobj = [];
            $scope.showSave = true;
            $scope.showUpdate = false;
            $scope.deletefiltershow = false;
            $("#EntitiesTree").trigger("loadactivityfromfilter", [0, ApplyFilterobj, 1]);
            $("#plandetailfilter").trigger('ClearAndReApply');
        }

        function KeepAllStepsMarkedComplete() {
            $("#MyWizard ul.steps").find("li").addClass("complete");
            $("#MyWizard ul.steps").find("span.badge").addClass("badge-success");
            window["tid_wizard_steps_all_complete_count"]++;
            if (window["tid_wizard_steps_all_complete_count"] >= 20) {
                clearInterval(window["tid_wizard_steps_all_complete"]);
            }
        }
        $scope.hideFilterSettings = function () {
            $('.FilterHolder').slideUp("slow")
            $timeout(function () {
                $(window).AdjustHeightWidth();
            }, 500);
        }
        $("#plandetailfiltersettings").on('loaddetailfiltersettings', function (event, TypeID) {
            $scope.FilterSettingsLoad();
        });
        $scope.OntimeStatusLists = [{
            Id: 0,
            Name: 'On time',
            BtnBgColor: 'btn-success',
            AbgColor: 'background-image: linear-gradient(to bottom, #62C462, #51A351); color: #fff;',
            SelectedClass: 'btn-success'
        }, {
            Id: 1,
            Name: 'Delayed',
            BtnBgColor: 'btn-warning',
            AbgColor: 'background-image: linear-gradient(to bottom, #FBB450, #F89406); color: #fff;',
            SelectedClass: 'btn-warning'
        }, {
            Id: 2,
            Name: 'On hold',
            BtnBgColor: 'btn-danger',
            AbgColor: 'background-image: linear-gradient(to bottom, #EE5F5B, #BD362F); color: #fff;',
            SelectedClass: 'btn-danger'
        }]
        $scope.FilterSettingsLoad = function () {
            $scope.attributegroupTypeid = 0;
            $scope.EntityHierarchyTypesResult = [];
            $scope.treesrcdirec = {};
            $scope.treePreviewObj = {};
            if ($window.ActivityFilterName == '') {
                $window.ActivityFilterName = '';
            }
            $scope.dyn_Cont = '';
            $scope.Updatefilter = false;
            $scope.Applyfilter = true;
            $scope.Deletefilter = false;
            $scope.Savefilter = false;
            $("#dynamic_Controls").html('');
            $scope.atributesRelationList = [];
            $scope.atributesRelationListWithTree = [];
            $scope.ngsaveasfilter = '';
            $scope.ngKeywordtext = '';
            $scope.atributeGroupList = [];
            var OptionFrom = 0;
            var IsKeyword = "false";
            var IsEntityType = "false";
            var IsEntityMember = "false";
            var CurrentActiveVersion = 0;
            $scope.PeriodOptions = [{
                Id: 1,
                FilterPeriod: 'Between'
            }, {
                Id: 2,
                FilterPeriod: 'Within'
            }];
            var KeywordOptionsresponse;
            DetailfilterService.GetOptionsFromXML(elementNode, TypeID).then(function (KeywordOptionsResult) {
                KeywordOptionsresponse = KeywordOptionsResult.Response
                OptionFrom = KeywordOptionsresponse.split(',')[0];
                IsKeyword = KeywordOptionsresponse.split(',')[1];
                IsEntityType = KeywordOptionsresponse.split(',')[3];
                IsEntityMember = KeywordOptionsresponse.split(',')[4];
                CurrentActiveVersion = KeywordOptionsresponse.split(',')[2];
                DetailfilterService.GettingEntityTypeHierarchyForAdminTree(6, 3).then(function (GerParentEntityData) {
                    $scope.entitytpesdata = GerParentEntityData.Response;
                    $scope.tagAllOptions.data = [];
                    if (GerParentEntityData.Response != null) {
                        $.each(GerParentEntityData.Response, function (i, el) {
                            $scope.tagAllOptions.data.push({
                                "id": el.Id,
                                "text": el.Caption,
                                "ShortDescription": el.ShortDescription,
                                "ColorCode": el.ColorCode
                            });
                        });
                    }
                    $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult;
                    var IDList1 = new Array();
                    var filterattidsmeber = {};
                    filterattidsmeber.IDList = $window.ListofEntityID;;
                    filterattidsmeber.TypeID = TypeID;
                    filterattidsmeber.FilterType = 'DetailFilter';
                    filterattidsmeber.OptionFrom = OptionFrom;
                    filterattidsmeber.IsEntityMember = IsEntityMember
                    $scope.tagmemberOptions = [];
                    DetailfilterService.GettingFilterEntityMember(filterattidsmeber).then(function (entityyMemberRelation) {
                        $scope.entityMemberRelationdata = entityyMemberRelation.Response;
                        if (entityyMemberRelation.Response != null) {
                            $.each(entityyMemberRelation.Response, function (i, el) {
                                $scope.tagmemberOptions.push({
                                    "id": el.Id,
                                    "Name": el.FirstName + ' ' + el.LastName
                                });
                            });
                        }
                        var filterattids = {};
                        filterattids.TypeID = TypeID;
                        filterattids.FilterType = 'DetailFilter';
                        filterattids.OptionFrom = OptionFrom;
                        filterattids.IDList = $window.ListofEntityID;
                        filterattids.IsEntityMember = IsEntityMember
                        DetailfilterService.GettingFilterAttribute(filterattids).then(function (entityAttributesRelation) {
                            if (entityAttributesRelation.Response != null) {
                                $scope.atributesRelationList = entityAttributesRelation.Response;
                            }
                            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                                if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                                    $scope.dyn_Cont += '<div class="control-group"><span>' + $scope.atributesRelationList[i].DisplayName + ' : </span>';
                                    $scope.dyn_Cont += '<div class="controls"><select class=\"multiselect\" multiple="multiple" multiselect-dropdown data-placeholder="Select ' + $scope.atributesRelationList[i].DisplayName + ' options" ng-model="FilterFields.DropDown_' + $scope.atributesRelationList[i].AttributeId + '_' + $scope.atributesRelationList[i].TreeLevel + '"\ ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList[' + i + '].LevelTreeNodes \">';
                                    $scope.dyn_Cont += '</select></div></div>';
                                    $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] = [];
                                } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                                    $scope.dyn_Cont += '<div class="control-group"><span>' + $scope.atributesRelationList[i].DisplayName + ' : </span>';
                                    $scope.dyn_Cont += '<div class="controls"><select class=\"multiselect\" multiple="multiple" multiselect-dropdown data-placeholder="Select ' + $scope.atributesRelationList[i].DisplayName + ' options" ng-model="FilterFields.DropDown_' + $scope.atributesRelationList[i].AttributeId + '_' + $scope.atributesRelationList[i].TreeLevel + '"\ ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList[' + i + '].LevelTreeNodes \">';
                                    $scope.dyn_Cont += '</select></div></div>';
                                    $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] = [];
                                } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                                    var treecount12 = $.grep($scope.FilterDataXML, function (e) {
                                        return e.AttributeId == $scope.atributesRelationList[i].AttributeId
                                    });
                                    if (treecount12.length == 0) {
                                        var mm = $scope.atributesRelationList[i].AttributeId;
                                        $scope.PercentageVisibleSettings["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeId.toString() + ""] = false;
                                        $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.atributesRelationList[i].AttributeId.toString() + ""] = $scope.atributesRelationList[i].DropdowntreePricingAttr;
                                        $scope.dyn_Cont += "<div drowdowntreepercentagemultiselectionfilter  data-purpose='entity' data-attributeid=" + $scope.atributesRelationList[i].AttributeId.toString() + "></div>";
                                        $scope.FilterDataXML.push({
                                            "AttributeId": parseInt($scope.atributesRelationList[i].AttributeId)
                                        });
                                    }
                                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3) {
                                    if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                                        $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + "</span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"  id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"     ng-options=\"ndata.Id as (ndata.FirstName+' '+ndata.LastName)  for ndata in  atributesRelationList[" + i + "].Users \"></select></div>";
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                    } else if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.EntityStatus) {
                                        var EntityStatusOptions = $scope.atributesRelationList[i].EntityStatusOptionValues
                                        $scope.tagAllOptionsEntityStatus.data = [];
                                        if (EntityStatusOptions != null) {
                                            $.each(EntityStatusOptions, function (i, el) {
                                                $scope.tagAllOptionsEntityStatus.data.push({
                                                    "id": el.ID,
                                                    "text": el.StatusOptions,
                                                    "ShortDescription": el.ShortDesc,
                                                    "ColorCode": el.ColorCode
                                                });
                                            });
                                        }
                                        $scope.dyn_Cont += "<div class='control-group'>";
                                        $scope.dyn_Cont += "<span>" + $scope.atributesRelationList[i].DisplayName + " : </span><div class=\"controls\">";
                                        $scope.dyn_Cont += "<input class=\"width2x\" id='ddlEntityStatus' placeholder='Select Entity Status Options' type='hidden' ui-select2='tagAllOptionsEntityStatus' ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\" />";
                                        $scope.dyn_Cont += "</div></div>";
                                    } else if ($scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.EntityOnTimeStatus) {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><span>' + $scope.atributesRelationList[i].DisplayName + ' : </span>';
                                        $scope.dyn_Cont += "<div class=\"controls\"><select class=\"multiselect\" multiselect-dropdown  multiple=\"multiple\" ";
                                        $scope.dyn_Cont += "data-placeholder=\"Select Entity OnTime Status\" ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\" ng-options=\"opt.Id as opt.Name for opt in  OntimeStatusLists \"";
                                        $scope.dyn_Cont += "id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\">";
                                        $scope.dyn_Cont += "</select>";
                                        $scope.dyn_Cont += "</div></div>";
                                    } else {
                                        $scope.dyn_Cont += '<div class=\"control-group\"><span>' + $scope.atributesRelationList[i].DisplayName + ' : </span>';
                                        $scope.dyn_Cont += '<div class=\"controls\"> <select class=\"multiselect\" multiple="multiple" multiselect-dropdown ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList[' + i + '].OptionValues \" data-placeholder="Select ' + $scope.atributesRelationList[i].DisplayName + ' options" ng-model=\"FilterFields.DropDown_' + $scope.atributesRelationList[i].AttributeId + '"\">';
                                        $scope.dyn_Cont += '</select></div></div>';
                                        $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                    }
                                } else if ($scope.atributesRelationList[i].AttributeTypeId == 4) {
                                    $scope.dyn_Cont += "<div class=\"control-group\"><span> " + $scope.atributesRelationList[i].DisplayName + " : </span>";
                                    $scope.dyn_Cont += "<div class=\"controls\"><select class=\"multiselect\" multiselect-dropdown  multiple=\"multiple\"";
                                    $scope.dyn_Cont += "data-placeholder=\"Select " + $scope.atributesRelationList[i].DisplayName + "\" ng-options=\"opt.Id as opt.Caption for opt in  atributesRelationList[" + i + "].OptionValues \"";
                                    $scope.dyn_Cont += "ng-model=\"FilterFields.DropDown_" + $scope.atributesRelationList[i].AttributeId + "\"";
                                    $scope.dyn_Cont += "id=\"DropDown_" + $scope.atributesRelationList[i].AttributeId + "\">";
                                    $scope.dyn_Cont += "</select>";
                                    $scope.dyn_Cont += "</div></div>";
                                    $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] = [];
                                } else if ($scope.atributesRelationList[i].AttributeTypeId == 10) {
                                    $scope.setoptions();
                                    $scope.items = [];
                                    $scope.items.push({
                                        startDate: '',
                                        endDate: ''
                                    });
                                    $scope.dyn_Cont += "<div class=\"control-group\"><span>" + $scope.atributesRelationList[i].DisplayName + " : </span><div class=\"controls\"><div class=\"period nomargin\" id=\"periodcontrols\"  ng-form=\"subForm\">";
                                    $scope.dyn_Cont += "<div class=\"row-fluid\">";
                                    $scope.dyn_Cont += "<input class=\"sdate\" data-date-format='" + $scope.DefaultSettings.DateFormat + "' id=\"items.startDate\"type=\"text\" value=\"\" name=\"startDate\" ng-click=\"Calanderopen($event,'startDateopened')\"  datepicker-popup=\"{{format}}\"  is-open=\"startDateopened\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\"  ng-model=\"items.startDate\" placeholder=\"-- Start date --\" ng-change=\"OpenOptionsForPeriod()\"/><input class=\"edate\" type=\"text\" data-date-format='" + $scope.DefaultSettings.DateFormat + "' value=\"\" name=\"enddate\" id=\"items.endDate\" ng-model=\"items.endDate\" ng-click=\"Calanderopen($event,'endDateopened')\"  datepicker-popup=\"{{format}}\"  is-open=\"endDateopened\" min-date=\"minDate\" datepicker-options=\"dateOptions\" date-disabled=\"disabled(date, mode)\"  close-text=\"Close\" placeholder=\"-- End date --\"  ng-change=\"OpenOptionsForPeriod()\"/>";
                                    $scope.dyn_Cont += "";
                                    $scope.dyn_Cont += "</div></div></div></div>";
                                    $scope.FilterFields["Period_" + $scope.atributesRelationList[i].AttributeId] = [];
                                    $scope.setFieldKeys();
                                    $scope.dyn_Cont += "<div ng-show=\"ShowPeriodOptions\" class=\"control-group\" id=\"ShowPeriodOptions\" data-attrid=\"" + $scope.atributesRelationList[i].AttributeId + "\"><span>Period range : </span><div class=\"controls\">";
                                    $scope.dyn_Cont += "<select class=\"nomargin\" ui-select2 ng-model=\"PeriodOptionValue\"><option ng-repeat=\"ndata in PeriodOptions\" value=\"{{ndata.Id}}\">{{ndata.FilterPeriod}}</option></select></div></div>";
                                    $scope.ShowPeriodOptions = false;
                                    $scope.FilterFields["FilterFields.DatePart_Calander_Open" + "items.startDate"] = false;
                                    $scope.FilterFields["FilterFields.DatePart_Calander_Open" + "items.endDate"] = false;
                                }
                            }
                            if ($scope.atributesRelationList.length > 0) $scope.atributeGroupList = $.grep($scope.atributesRelationList, function (e) {
                                return e.AttributeTypeId == 3 || e.AttributeTypeId == 4 || e.AttributeTypeId == 6 || e.AttributeTypeId == 7 || e.AttributeTypeId == 12
                            });
                            if (IsEntityType == "True") {
                                $scope.dyn_Cont += "<div class='control-group'>";
                                $scope.dyn_Cont += "<span>EntityType : </span><div class=\"controls\"> ";
                                $scope.dyn_Cont += "<input class='width2x' id='ddlChildren' placeholder='Select EntityType Options' type='hidden' ui-select2='tagAllOptions' ng-model='ddlParententitytypeId' />";
                                $scope.dyn_Cont += "</div></div>";
                            }
                            $scope.atributesRelationListWithTree = $.grep(entityAttributesRelation.Response, function (e) {
                                return e.AttributeTypeId == 7
                            })
                            for (var i = 0; i < $scope.atributesRelationListWithTree.length; i++) {
                                if ($scope.atributesRelationListWithTree[i].AttributeTypeId == 7) {
                                    $scope.treePreviewObj = {};
                                    $scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = JSON.parse($scope.atributesRelationListWithTree[i].tree).Children;
                                    if ($scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId].length > 0) {
                                        if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId])) {
                                            $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = true;
                                        } else $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = false;
                                    } else {
                                        $scope.treePreviewObj["Attr_" + $scope.atributesRelationListWithTree[i].AttributeId] = false;
                                    }
                                    $scope.dyn_Cont += '<div class="control-group treeNode-control-group">';
                                    $scope.dyn_Cont += '<span>' + $scope.atributesRelationListWithTree[i].DisplayName + '</span>';
                                    $scope.dyn_Cont += '<div class="controls treeNode-controls">';
                                    $scope.dyn_Cont += '<div class="input-group treeNodeSearchBox"><span class="input-group-addon"><i class="icon-search"></i></span><input class="form-control multiselect-search nomargin" type="text" ng-model="filterValue_' + $scope.atributesRelationListWithTree[i].AttributeId + '" placeholder="Search" treecontext="treeNodeSearchDropdowns_Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '"></div>';
                                    $scope.dyn_Cont += '<div class="treeNodeSearchDropdown dropdown-menu contexMenu" style="display: none;" id="treeNodeSearchDropdowns_Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '">';
                                    $scope.dyn_Cont += '<span ng-if="doing_async">...loading...</span>';
                                    $scope.dyn_Cont += '<abn-tree tree-filter="filterValue_' + $scope.atributesRelationListWithTree[i].AttributeId + '" tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\" tree-control="my_tree" on-select="my_tree_handler(branch,parent)" expand-level=\"100\"></abn-tree>';
                                    $scope.dyn_Cont += '</div></div>';
                                    $scope.dyn_Cont += '<div class="control-group staticTreeGroup" ng-show=\"treePreviewObj.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\">';
                                    $scope.dyn_Cont += '<div class="controls">';
                                    $scope.dyn_Cont += '<eu-tree tree-data=\"treesrcdirec.Attr_' + $scope.atributesRelationListWithTree[i].AttributeId + '\" node-attributeid="' + $scope.atributesRelationListWithTree[i].AttributeId + '" node-id="id" node-name="Caption" node-state="expand" expand-icon="assets/img/treeExpandIcon.png" collapse-icon="assets/img/treeCollapseIcon.png" child-icon="assets/img/emptySpace.png" ></eu-tree>';
                                    $scope.dyn_Cont += '</div></div></div>';
                                }
                            }
                            if (IsKeyword == "True") {
                                $scope.dyn_Cont += "<div class=\"control-group\"><span>Keyword : </span><input type='text' ng-model='ngKeywordtext' id='ngKeywordtext' placeholder='Filter Keyword'></div>";
                            }
                            if (IsEntityMember == "True") {
                                $scope.dyn_Cont += "<div class=\"control-group\"><span>Members </span> <select  class=\"multiselect\"   data-placeholder=\"Select filter\"  multiple=\"multiple\"  multiselect-dropdown ng-model='ddlEntitymember'  id= 'ddlChildrenmember'     ng-options=\"ndata.id as (ndata.Name)  for ndata in  tagmemberOptions \"></select></div>";
                            }
                            $("#dynamic_Controls").html($compile($scope.dyn_Cont)($scope));
                            $scope.Applyfilter = true;
                            $scope.Updatefilter = false;
                            $scope.Deletefilter = false;
                            $scope.Savefilter = true;
                        });
                    });
                });
            });
        }
        $timeout(function () { $scope.FilterSettingsLoad() }, 100);
        $scope.OpenOptionsForPeriod = function () {
            $scope.PeriodOptionValue = {};
            $scope.PeriodOptionValue = 1;
            var startdate = $scope.items.startDate;
            var enddate = $scope.items.endDate;
            if (startdate != null && enddate != null) {
                if (startdate != undefined && enddate != undefined) {
                    $scope.ShowPeriodOptions = true;
                } else $scope.ShowPeriodOptions = false;
            } else $scope.ShowPeriodOptions = false;
        }
        $scope.FilterSave = function () {
            if (IsSave == 1) {
                return false;
            }
            IsSave = 1;
            if ($scope.ngsaveasfilter == '' || $scope.ngsaveasfilter == undefined) {
                bootbox.alert($translate.instant('LanguageContents.Res_1902.Caption'));
                IsSave = 0;
                return false;
            }
            var ApplyFilterobj = [];
            var FilterData = {};
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            var fiscalyear = [];
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                    var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                    for (var ii = 0; ii < Array; ii++) {
                        if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                            var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                            for (var k = 0; k < j; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                    'Level': ii + 1,
                                    'AttributeTypeId': 13,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': ''
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3 && $scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        usersVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        for (var k = 0; k < usersVal.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': usersVal[k],
                                'Level': 0,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        fiscalyear = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId]
                        for (var k = 0; k < fiscalyear.length; k++) {
                            if (fiscalyear[k].Id != undefined) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': fiscalyear[k].Id,
                                    'Level': 0,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                });
                            } else {
                                if ($scope.atributesRelationList[i].AttributeId == 71) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k].id,
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k],
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId
                                    });
                                }
                            }
                        }
                    }
                }
            }
            var entitytypeIdforFilter = '';
            if ($scope.ddlParententitytypeId != undefined) {
                if ($scope.ddlParententitytypeId[0] != undefined) {
                    for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                        if (entitytypeIdforFilter == '') {
                            entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                        } else {
                            entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                        }
                    }
                }
            }
            var entitymemberIdforFilter = '';
            if ($scope.ddlEntitymember != undefined) {
                if ($scope.ddlEntitymember[0] != undefined) {
                    for (var x = 0; x < $scope.ddlEntitymember.length; x++) {
                        if (entitymemberIdforFilter == '') {
                            entitymemberIdforFilter = $scope.ddlEntitymember[x];
                        } else {
                            entitymemberIdforFilter = entitymemberIdforFilter + "," + $scope.ddlEntitymember[x];
                        }
                    }
                }
            }
            var filtertype = 1;
            if ($scope.attributegroupTypeid != 0 && $scope.attributegroupTypeid != undefined) {
                whereConditionData.splice(0, whereConditionData.length);
                var res = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeId == parseInt($scope.attributegroupTypeid, 10)
                })[0];
                if (res.AttributeId == 71) {
                    for (var k = 0; k < res.EntityStatusOptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.EntityStatusOptionValues[k].ID,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 6 || res.AttributeTypeId == 12) {
                    for (var k = 0; k < res.LevelTreeNodes.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.LevelTreeNodes[k].Id,
                            'Level': res.LevelTreeNodes[k].Level,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 7) {
                    GetAllTreeObject(res.AttributeId);
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == res.AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else {
                    for (var k = 0; k < res.OptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.OptionValues[k].Id,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                }
                FilterData.IsDetailFilter = 5;
                filtertype = 5;
            } else FilterData.IsDetailFilter = 1;
            FilterData.FilterId = 0;
            FilterData.FilterName = $scope.ngsaveasfilter;
            FilterData.Keyword = $scope.ngKeywordtext;
            FilterData.UserId = 1;
            FilterData.TypeID = TypeID;
            FilterData.entityTypeId = entitytypeIdforFilter;
            FilterData.EntitymemberId = entitymemberIdforFilter
            if ($scope.items.startDate != undefined && $scope.items.endDate != undefined) {
                FilterData.StarDate = dateFormat($scope.items.startDate, 'yyyy/mm/dd');
                FilterData.EndDate = dateFormat($scope.items.endDate, 'yyyy/mm/dd');
                whereConditionData.push({
                    'AttributeID': $("#ShowPeriodOptions").attr("data-attrid"),
                    'SelectedValue': $scope.PeriodOptionValue,
                    'Level': 0,
                    'AttributeTypeId': 10
                });
            } else {
                FilterData.EndDate = '';
                FilterData.StarDate = '';
            }
            FilterData.WhereConditon = whereConditionData;
            DetailfilterService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                $('#FilterSettingsModal').modal('hide');
                if (filterSettingsInsertresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4279.Caption'));
                    IsSave = 0;
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4397.Caption'));
                    IsSave = 0;
                    $("#EntitiesTree").trigger("loadactivityfromfilter", [filterSettingsInsertresult.Response, ApplyFilterobj, filtertype]);
                    $("#plandetailfilter").trigger('reloaddetailfilter', [TypeID, $scope.ngsaveasfilter, filterSettingsInsertresult.Response, filtertype]);
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $("#plandetailfiltersettings").on('EditFilterSettingsByFilterID', function (event, filterId, typeId, filtertypeval) {
            $scope.showSave = false;
            $scope.showUpdate = true;
            $scope.deletefiltershow = true;
            $scope.LoadFilterSettingsByFilterID(event, filterId, typeId, filtertypeval);
        });
        $scope.LoadFilterSettingsByFilterID = function (event, filterId, typeId, filtertypeval) {
            $scope.ClearScopeModle();
            $scope.showSave = false;
            $scope.showUpdate = true;
            $scope.deletefiltershow = true;
            $scope.filterSettingValues = [];
            selectedfilterid = filterId;
            DetailfilterService.GetFilterSettingValuesByFilertId(filterId).then(function (filterSettingsValues) {
                $scope.filterSettingValues = filterSettingsValues.Response;
                if (filtertypeval != 5) {
                    for (var i = 0; i < $scope.filterSettingValues.FilterValues.length; i++) {
                        if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3 && $scope.filterSettingValues.FilterValues[i].AttributeId == SystemDefiendAttributes.Owner) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] != undefined) $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId].push($scope.filterSettingValues.FilterValues[i].Value);
                        } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 3 || $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 4) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] != undefined) {
                                if ($scope.filterSettingValues.FilterValues[i].AttributeId == 71) {
                                    var statusselectedoptions = $.grep($scope.atributesRelationList, function (e) {
                                        return e.AttributeId == parseInt(71);
                                    })[0].EntityStatusOptionValues;
                                    var seletedoptionvalue = [];
                                    for (var sts = 0; sts < $.grep($scope.filterSettingValues.FilterValues, function (e) {
										return e.AttributeId == parseInt(71);
                                    }).length; sts++) {
                                        seletedoptionvalue.push($.grep($scope.tagAllOptionsEntityStatus.data, function (e) {
                                            return e.id == parseInt($scope.filterSettingValues.FilterValues[sts].Value);
                                        })[0]);
                                        if (seletedoptionvalue != null) {
                                            $.each(seletedoptionvalue, function (i, el) {
                                                $scope.tagAllOptionsEntityStatus.data.push({
                                                    "id": el.ID,
                                                    "text": el.StatusOptions,
                                                    "ShortDescription": el.ShortDesc,
                                                    "ColorCode": el.ColorCode
                                                });
                                            });
                                        }
                                        $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = seletedoptionvalue;
                                    }
                                } else {
                                    $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId].push($scope.filterSettingValues.FilterValues[i].Value);
                                }
                            }
                        } else if ($scope.filterSettingValues.FilterValues[i].Level != 0 && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 6) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level] != undefined) {
                                $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);
                            }
                        } else if ($scope.filterSettingValues.FilterValues[i].Level != 0 && $scope.filterSettingValues.FilterValues[i].AttributeTypeId == 12) {
                            if ($scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level] != undefined) {
                                $scope.FilterFields["DropDown_" + $scope.filterSettingValues.FilterValues[i].AttributeId + "_" + $scope.filterSettingValues.FilterValues[i].Level].push($scope.filterSettingValues.FilterValues[i].Value);
                            }
                        } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 13) {
                            $scope.DropDownFilterTreePricing["AttributeId_Levels_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = $scope.filterSettingValues.FilterValues[i].DropdowntreePricingData;
                        } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 7) {
                            $scope.treesrcdirec["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = JSON.parse($scope.filterSettingValues.FilterValues[i].TreeValues).Children;
                            GetTreeObjecttoSave($scope.filterSettingValues.FilterValues[i].AttributeId);
                            if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId])) {
                                $scope.treePreviewObj["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = true;
                            } else $scope.treePreviewObj["Attr_" + $scope.filterSettingValues.FilterValues[i].AttributeId] = false;
                        } else if ($scope.filterSettingValues.FilterValues[i].AttributeTypeId == 10) {
                            if ($scope.filterSettingValues.StartDate != null) {
                                $scope.ShowPeriodOptions = true;
                                $scope.PeriodOptionValue = $scope.filterSettingValues.FilterValues[i].Value;
                            }
                        }
                    }
                } else $scope.attributegroupTypeid = $scope.filterSettingValues.FilterValues[0].AttributeId;
                $scope.items.push({
                    startDate: $scope.filterSettingValues.StartDate,
                    endDate: $scope.filterSettingValues.EndDate
                });
                DetailfilterService.GettingEntityTypeHierarchyForAdminTree(6, 3).then(function (GerParentEntityData) {
                    $scope.entitytpesdata = GerParentEntityData.Response;
                    $scope.tagAllOptions.data = [];
                    if (GerParentEntityData.Response != null) {
                        $.each(GerParentEntityData.Response, function (i, el) {
                            $scope.tagAllOptions.data.push({
                                "id": el.Id,
                                "text": el.Caption,
                                "ShortDescription": el.ShortDescription,
                                "ColorCode": el.ColorCode
                            });
                        });
                    }
                });
                $scope.EntityHierarchyTypesResult = [];
                for (var l = 0; l < filterSettingsValues.Response.EntityTypeID.split(',').length; l++) {
                    if (filterSettingsValues.Response.EntityTypeID.split(',')[l] != '0') {
                        $scope.EntityHierarchyTypesResult.push($.grep($scope.tagAllOptions.data, function (e) {
                            return e.id == filterSettingsValues.Response.EntityTypeID.split(',')[l]
                        })[0]);
                    }
                }
                $scope.ddlParententitytypeId = $scope.EntityHierarchyTypesResult == undefined ? 0 : $scope.EntityHierarchyTypesResult[0] == undefined ? 0 : $scope.EntityHierarchyTypesResult;
                if (filterSettingsValues.Response.EntityMemberID != "" && filterSettingsValues.Response.EntityMemberID != null) {
                    for (var l = 0; l < filterSettingsValues.Response.EntityMemberID.split(',').length; l++) {
                        if (filterSettingsValues.Response.EntityMemberID.split(',')[l] != '0') {
                            if ($.grep($scope.tagmemberOptions, function (e) {
								return e.id == filterSettingsValues.Response.EntityMemberID.split(',')[l]
                            })[0] != undefined) {
                                var mval = filterSettingsValues.Response.EntityMemberID.split(',')[l];
                                if (mval != "" && mval != undefined) $scope.ddlEntitymember.push(parseInt(mval));
                            }
                        }
                    }
                }
                var startdateupdate = $scope.filterSettingValues.StartDate;
                var enddateupdate = $scope.filterSettingValues.EndDate;
                var editstartdate = startdateupdate.split('/');
                var editenddate = enddateupdate.split('/');
                startdateupdate = dateFormat(startdateupdate, $scope.DefaultSettings.DateFormat);
                enddateupdate = dateFormat(enddateupdate, $scope.DefaultSettings.DateFormat);
                if ($scope.filterSettingValues.StartDate == "" || $scope.filterSettingValues.EndDate == "") {
                    $scope.items.push({
                        startDate: "",
                        endDate: ""
                    });
                } else if ($scope.filterSettingValues.StartDate != DateValidate || $scope.filterSettingValues.EndDate != DateValidate) {
                    $scope.items.startDate = startdateupdate;
                    $scope.items.endDate = enddateupdate;
                } else {
                    $scope.items.push({
                        startDate: DateValidate,
                        endDate: DateValidate
                    });
                }
                $scope.ngKeywordtext = $scope.filterSettingValues.Keyword;
                $scope.ngsaveasfilter = $scope.filterSettingValues.FilterName;
            });
        }
        $scope.FilterUpdate = function () {
            if (IsUpdate == 1) {
                return false;
            }
            IsUpdate = 1;
            var ApplyFilterobj = [];
            var FilterData = {};
            $scope.filterSettingValues.Keyword = '';
            $scope.filterSettingValues.FilterName = '';
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            var fiscalyear = [];
            var entitytypeIdforFilter = '';
            if ($scope.ddlParententitytypeId != undefined) {
                if ($scope.ddlParententitytypeId[0] != undefined) {
                    for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                        if (entitytypeIdforFilter == '') {
                            entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                        } else {
                            entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                        }
                    }
                }
            }
            var entitymemberIdforFilter = '';
            if ($scope.ddlEntitymember != undefined) {
                if ($scope.ddlEntitymember[0] != undefined) {
                    for (var x = 0; x < $scope.ddlEntitymember.length; x++) {
                        if (entitymemberIdforFilter == '') {
                            entitymemberIdforFilter = $scope.ddlEntitymember[x];
                        } else {
                            entitymemberIdforFilter = entitymemberIdforFilter + "," + $scope.ddlEntitymember[x];
                        }
                    }
                }
            }
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeId == 3 && $scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        usersVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        for (var k = 0; k < usersVal.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': usersVal[k],
                                'Level': 0,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'EntityMemberIDs': entitymemberIdforFilter
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        fiscalyear = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId]
                        for (var k = 0; k < fiscalyear.length; k++) {
                            if (fiscalyear[k].Id != undefined) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': fiscalyear[k].Id,
                                    'Level': 0,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            } else {
                                if ($scope.atributesRelationList[i].AttributeId == 71) {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k].id,
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'EntityMemberIDs': entitymemberIdforFilter
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k],
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'EntityMemberIDs': entitymemberIdforFilter
                                    });
                                }
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'EntityMemberIDs': entitymemberIdforFilter
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            whereConditionData.push({
                                'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                'Level': $scope.atributesRelationList[i].TreeLevel,
                                'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                'EntityTypeIDs': entitytypeIdforFilter,
                                'EntityMemberIDs': entitymemberIdforFilter
                            });
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                    var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                    for (var ii = 0; ii < Array; ii++) {
                        if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                            var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                            for (var k = 0; k < j; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                    'Level': ii + 1,
                                    'AttributeTypeId': 13,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': '',
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            }
                        }
                    }
                }
            }
            FilterData.FilterId = selectedfilterid;
            FilterData.TypeID = TypeID;
            FilterData.FilterName = $scope.ngsaveasfilter;
            FilterData.Keyword = $scope.ngKeywordtext;
            FilterData.UserId = 1;
            FilterData.entityTypeId = entitytypeIdforFilter;
            FilterData.EntitymemberId = entitymemberIdforFilter
            if ($scope.items.startDate != undefined && $scope.items.endDate != undefined) {
                FilterData.StarDate = dateFormat($scope.items.startDate, 'yyyy/mm/dd');
                FilterData.EndDate = dateFormat($scope.items.endDate, 'yyyy/mm/dd');
                whereConditionData.push({
                    'AttributeID': $("#ShowPeriodOptions").attr("data-attrid"),
                    'SelectedValue': $scope.PeriodOptionValue,
                    'Level': 0,
                    'AttributeTypeId': 10,
                    'EntityTypeIDs': entitytypeIdforFilter,
                    'EntityMemberIDs': entitymemberIdforFilter
                });
            } else {
                FilterData.EndDate = '';
                FilterData.StarDate = '';
            }
            var filtertype = 1;
            if ($scope.attributegroupTypeid != 0 && $scope.attributegroupTypeid != undefined) {
                whereConditionData.splice(0, whereConditionData.length);
                var res = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeId == parseInt($scope.attributegroupTypeid, 10)
                })[0];
                if (res.AttributeId == 71) {
                    for (var k = 0; k < res.EntityStatusOptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.EntityStatusOptionValues[k].ID,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 6 || res.AttributeTypeId == 12) {
                    for (var k = 0; k < res.LevelTreeNodes.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.LevelTreeNodes[k].Id,
                            'Level': res.LevelTreeNodes[k].Level,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 7) {
                    GetAllTreeObject(res.AttributeId);
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == res.AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else {
                    for (var k = 0; k < res.OptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.OptionValues[k].Id,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                }
                filtertype = 5;
                FilterData.IsDetailFilter = 5;
            } else FilterData.IsDetailFilter = 1;
            if (whereConditionData.length == 0 && entitymemberIdforFilter != '') {
                whereConditionData.push({
                    'AttributeID': 0,
                    'SelectedValue': 0,
                    'Level': 0,
                    'AttributeTypeId': 0,
                    'EntityTypeIDs': entitytypeIdforFilter,
                    'Keyword': '',
                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                    'EntityMemberIDs': entitymemberIdforFilter
                });
            }
            FilterData.WhereConditon = whereConditionData;
            if (selectedfilterid != 0) {
                ApplyFilterobj = whereConditionData;
            } else {
                ApplyFilterobj = [];
            }
            DetailfilterService.InsertFilterSettings(FilterData).then(function (filterSettingsInsertresult) {
                if (filterSettingsInsertresult.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4358.Caption'));
                    IsUpdate = 0;
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4400.Caption'));
                    IsUpdate = 0;
                    $scope.appliedfilter = $scope.ngsaveasfilter;
                    $("#plandetailfilter").trigger('reloaddetailfilter', [TypeID, $scope.ngsaveasfilter, filterSettingsInsertresult.Response, filtertype]);
                    $("#EntitiesTree").trigger("loadactivityfromfilter", [filterSettingsInsertresult.Response, ApplyFilterobj, filtertype]);
                }
            });
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
        };
        $scope.FilterID = 0;
        $("#plandetailfiltersettings").on('applyplandetailfilter', function (event, FilterID, FilterName, filtertypeval) {
            $scope.ApplyFilter(FilterID, FilterName, filtertypeval);
        });
        $scope.ApplyFilter = function (FilterID, FilterName, filtertypeval) {
            var filtertype = 1;
            if (FilterID != undefined && FilterID != 0) {
                $scope.FilterSettingsLoad();
                filtertype = filtertypeval;
            }
            $('#FilterSettingsModal').modal('hide');
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            if (FilterID != undefined) {
                selectedfilterid = FilterID;
                $scope.FilterID = FilterID;
            }
            $scope.appliedfilter = FilterName;
            $window.ActivityFilterName = FilterName;
            if (FilterID == 0) {
                $scope.appliedfilter = "No filter applied";
            }
            var whereConditionData = [];
            var multiSelectVal = [];
            var usersVal = [];
            var orgLevel = [];
            var fiscalyear = [];
            var entitytypeIdforFilter = '';
            if ($scope.ddlParententitytypeId != undefined) {
                if ($scope.ddlParententitytypeId[0] != undefined) {
                    for (var l = 0; l < $scope.ddlParententitytypeId.length; l++) {
                        if (entitytypeIdforFilter == '') {
                            entitytypeIdforFilter = $scope.ddlParententitytypeId[l].id;
                        } else {
                            entitytypeIdforFilter = entitytypeIdforFilter + "," + $scope.ddlParententitytypeId[l].id;
                        }
                    }
                }
            }
            var entitymemberIdforFilter = '';
            if ($scope.ddlEntitymember != undefined) {
                if ($scope.ddlEntitymember[0] != undefined) {
                    for (var x = 0; x < $scope.ddlEntitymember.length; x++) {
                        if (entitymemberIdforFilter == '') {
                            entitymemberIdforFilter = $scope.ddlEntitymember[x];
                        } else {
                            entitymemberIdforFilter = entitymemberIdforFilter + "," + $scope.ddlEntitymember[x];
                        }
                    }
                }
            }
            for (var i = 0; i < $scope.atributesRelationList.length; i++) {
                if ($scope.atributesRelationList[i].AttributeTypeId == 6) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            if ($scope.ngKeywordtext != '') {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                    'Level': $scope.atributesRelationList[i].TreeLevel,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': $scope.ngKeywordtext,
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            } else {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                    'Level': $scope.atributesRelationList[i].TreeLevel,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': '',
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 12) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel] != undefined) {
                        orgLevel = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel];
                        for (var k = 0; k < orgLevel.length; k++) {
                            if ($scope.ngKeywordtext != '') {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                    'Level': $scope.atributesRelationList[i].TreeLevel,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': $scope.ngKeywordtext,
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            } else {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId + "_" + $scope.atributesRelationList[i].TreeLevel][k],
                                    'Level': $scope.atributesRelationList[i].TreeLevel,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': '',
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 13) {
                    var Array = $scope.atributesRelationList[i].DropdowntreePricingAttr.length;
                    for (var ii = 0; ii < Array; ii++) {
                        if ($scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection != null) {
                            var j = $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection.length;
                            for (var k = 0; k < j; k++) {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': $scope.atributesRelationList[i].DropdowntreePricingAttr[ii].selection[k],
                                    'Level': ii + 1,
                                    'AttributeTypeId': 13,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': '',
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3 && $scope.atributesRelationList[i].AttributeId == SystemDefiendAttributes.Owner) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        usersVal = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId];
                        for (var k = 0; k < usersVal.length; k++) {
                            if ($scope.ngKeywordtext != '') {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': usersVal[k],
                                    'Level': 0,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': $scope.ngKeywordtext,
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            } else {
                                whereConditionData.push({
                                    'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                    'SelectedValue': usersVal[k],
                                    'Level': 0,
                                    'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                    'EntityTypeIDs': entitytypeIdforFilter,
                                    'Keyword': '',
                                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                    'EntityMemberIDs': entitymemberIdforFilter
                                });
                            }
                        }
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 7) {
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == $scope.atributesRelationList[i].AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        if ($scope.ngKeywordtext != '') whereConditionData.push({
                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': $scope.ngKeywordtext,
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                        else whereConditionData.push({
                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if ($scope.atributesRelationList[i].AttributeTypeId == 3 || $scope.atributesRelationList[i].AttributeTypeId == 4) {
                    if ($scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId] != undefined) {
                        fiscalyear = $scope.FilterFields["DropDown_" + $scope.atributesRelationList[i].AttributeId]
                        for (var k = 0; k < fiscalyear.length; k++) {
                            if (fiscalyear[k].Id != undefined) {
                                if ($scope.ngKeywordtext != '') {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k].Id,
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': $scope.ngKeywordtext,
                                        'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                        'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                        'EntityMemberIDs': entitymemberIdforFilter
                                    });
                                } else {
                                    whereConditionData.push({
                                        'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                        'SelectedValue': fiscalyear[k].Id,
                                        'Level': 0,
                                        'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                        'EntityTypeIDs': entitytypeIdforFilter,
                                        'Keyword': '',
                                        'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                        'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                        'EntityMemberIDs': entitymemberIdforFilter
                                    });
                                }
                            } else {
                                if ($scope.ngKeywordtext != '') {
                                    if ($scope.atributesRelationList[i].AttributeId == 71) {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': fiscalyear[k].id,
                                            'Level': 0,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': $scope.ngKeywordtext,
                                            'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                            'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                            'EntityMemberIDs': entitymemberIdforFilter
                                        });
                                    } else {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': fiscalyear[k],
                                            'Level': 0,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': $scope.ngKeywordtext,
                                            'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                            'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                            'EntityMemberIDs': entitymemberIdforFilter
                                        });
                                    }
                                } else {
                                    if ($scope.atributesRelationList[i].AttributeId == 71) {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': fiscalyear[k].id,
                                            'Level': 0,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': '',
                                            'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                            'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                            'EntityMemberIDs': entitymemberIdforFilter
                                        });
                                    } else {
                                        whereConditionData.push({
                                            'AttributeID': $scope.atributesRelationList[i].AttributeId,
                                            'SelectedValue': fiscalyear[k],
                                            'Level': 0,
                                            'AttributeTypeId': $scope.atributesRelationList[i].AttributeTypeId,
                                            'EntityTypeIDs': entitytypeIdforFilter,
                                            'Keyword': '',
                                            'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                                            'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                                            'EntityMemberIDs': entitymemberIdforFilter
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (entitytypeIdforFilter != '') {
                if ($scope.ngKeywordtext != '') {
                    whereConditionData.push({
                        'AttributeID': 0,
                        'SelectedValue': 0,
                        'Level': 0,
                        'AttributeTypeId': 0,
                        'EntityTypeIDs': entitytypeIdforFilter,
                        'Keyword': $scope.ngKeywordtext,
                        'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                        'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                        'EntityMemberIDs': entitymemberIdforFilter
                    });
                } else {
                    whereConditionData.push({
                        'AttributeID': 0,
                        'SelectedValue': 0,
                        'Level': 0,
                        'AttributeTypeId': 0,
                        'EntityTypeIDs': entitytypeIdforFilter,
                        'Keyword': '',
                        'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                        'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                        'EntityMemberIDs': entitymemberIdforFilter
                    });
                }
            }
            if (whereConditionData.length == 0 && $scope.ngKeywordtext != '') {
                whereConditionData.push({
                    'AttributeID': 0,
                    'SelectedValue': 0,
                    'Level': 0,
                    'AttributeTypeId': 0,
                    'EntityTypeIDs': entitytypeIdforFilter,
                    'Keyword': $scope.ngKeywordtext,
                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                    'EntityMemberIDs': entitymemberIdforFilter
                });
            }
            if (whereConditionData.length == 0 && ($scope.items.startDate != undefined || $scope.items.endDate != undefined)) {
                whereConditionData.push({
                    'AttributeID': 0,
                    'SelectedValue': 0,
                    'Level': 0,
                    'AttributeTypeId': 0,
                    'EntityTypeIDs': entitytypeIdforFilter,
                    'Keyword': $scope.ngKeywordtext,
                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                    'EntityMemberIDs': entitymemberIdforFilter
                });
            }
            if (whereConditionData.length == 0 && entitymemberIdforFilter != '') {
                whereConditionData.push({
                    'AttributeID': 0,
                    'SelectedValue': 0,
                    'Level': 0,
                    'AttributeTypeId': 0,
                    'EntityTypeIDs': entitytypeIdforFilter,
                    'Keyword': '',
                    'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                    'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                    'EntityMemberIDs': entitymemberIdforFilter
                });
            }
            var StartDate;
            var EndDate;
            var ApplyFilterobj = [];
            var optionperiod = $scope.PeriodOptionValue;
            if ($scope.items.startDate == undefined) optionperiod = 0;
            if ($scope.items.endDate == undefined) optionperiod = 0;
            if ($scope.items.startDate != undefined && $scope.items.endDate != undefined) optionperiod = $scope.PeriodOptionValue;
            if ($scope.items.startDate != undefined) StartDate = dateFormat($scope.items.startDate, 'yyyy/mm/dd');
            if ($scope.items.endDate != undefined) EndDate = dateFormat($scope.items.endDate, 'yyyy/mm/dd');
            if ($scope.items.startDate != undefined || $scope.items.endDate != undefined) whereConditionData.push({
                'AttributeID': $("#ShowPeriodOptions").attr("data-attrid"),
                'SelectedValue': optionperiod,
                'Level': 0,
                'AttributeTypeId': 10,
                'EntityTypeIDs': entitytypeIdforFilter,
                'Keyword': $scope.ngKeywordtext,
                'StartDate': $scope.items.startDate == undefined ? "" : ConvertDateToString($scope.items.startDate).replace('-', '/').replace('-', '/'),
                'EndDate': $scope.items.endDate == undefined ? "" : ConvertDateToString($scope.items.endDate).replace('-', '/').replace('-', '/'),
                'EntityMemberIDs': entitymemberIdforFilter
            });
            if ($scope.attributegroupTypeid != 0 && $scope.attributegroupTypeid != undefined) {
                whereConditionData.splice(0, whereConditionData.length);
                var res = $.grep($scope.atributesRelationList, function (e) {
                    return e.AttributeId == parseInt($scope.attributegroupTypeid, 10)
                })[0];
                if (res.AttributeId == 71) {
                    for (var k = 0; k < res.EntityStatusOptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.EntityStatusOptionValues[k].ID,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 6 || res.AttributeTypeId == 12) {
                    for (var k = 0; k < res.LevelTreeNodes.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.LevelTreeNodes[k].Id,
                            'Level': res.LevelTreeNodes[k].Level,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 7) {
                    GetAllTreeObject(res.AttributeId);
                    var treenodes = [];
                    treenodes = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == res.AttributeId;
                    });
                    for (var x = 0, nodeval; nodeval = treenodes[x++];) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': parseInt(nodeval.id, 10),
                            'Level': parseInt(nodeval.Level, 10),
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else if (res.AttributeTypeId == 3 && res.AttributeId == SystemDefiendAttributes.Owner) {
                    for (var k = 0; k < res.Users.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.Users[k].Id,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                } else {
                    for (var k = 0; k < res.OptionValues.length; k++) {
                        whereConditionData.push({
                            'AttributeID': res.AttributeId,
                            'SelectedValue': res.OptionValues[k].Id,
                            'Level': 0,
                            'AttributeTypeId': res.AttributeTypeId,
                            'EntityTypeIDs': entitytypeIdforFilter,
                            'Keyword': '',
                            'StartDate': "",
                            'EndDate': "",
                            'EntityMemberIDs': entitymemberIdforFilter
                        });
                    }
                }
                ApplyFilterobj = whereConditionData;
                filtertype = 5;
            } else {
                if (FilterID != 0) {
                    ApplyFilterobj = whereConditionData;
                } else {
                    ApplyFilterobj = [];
                }
            }
            $("#EntitiesTree").trigger("loadactivityfromfilter", [FilterID, ApplyFilterobj, filtertype]);
            $(window).AdjustHeightWidth();
        };
        $scope.filtersettingsreset = function () {
            var StartRowNo = 0;
            var MaxNoofRow = 20;
            var PageIndex = 0;
            selectedfilterid = 0;
        };
        $scope.DeleteFilter = function DeleteFilterSettingsValue() {
            var ApplyFilterobj = [];
            bootbox.confirm($translate.instant('LanguageContents.Res_155.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var ID = selectedfilterid;
                        DetailfilterService.DeleteFilterSettings(ID).then(function (deletefilterbyFilterId) {
                            if (deletefilterbyFilterId.StatusCode == 405) {
                                NotifyError($translate.instant('LanguageContents.Res_4299.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4398.Caption'));
                                $("#EntitiesTree").trigger("loadactivityfromfilter", [0, ApplyFilterobj, 1]);
                                $("#plandetailfilter").trigger('reloaddetailfilter', [TypeID, 'No filter applied', 0, 1]);
                            }
                        });
                    }, 100);
                }
            });
        };
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.default.detail.detailfilterCtrl']"));
        });
        $scope.tagAllOptionsEntityStatus = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersDataEntityStatus,
            formatResult: $scope.formatResultEntityStatus,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.ReassignMembersData = [];
        $scope.ReassignMembersDataEntityStatus = [];
        $scope.formatResult = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatResultEntityStatus = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelection = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.formatSelectionForEntityStauts = function (item) {
            var markup = '<table class="user-result">';
            markup += '<tbody>';
            markup += '<tr>';
            markup += '<td class="user-image">';
            markup += '<span class="eicon" style="background-color: #' + item.ColorCode + '">' + item.ShortDescription + '</span>';
            markup += '</td>';
            markup += '<td class="user-info">';
            markup += '<div class="user-title">' + item.text + '</div>';
            markup += '</td>';
            markup += '</tr>';
            markup += '</tbody>';
            markup += '</table>';
            return markup;
        };
        $scope.tagAllOptions = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersData,
            formatResult: $scope.formatResult,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };
        $scope.tagAllOptionsEntityStatus = {
            multiple: true,
            allowClear: true,
            data: $scope.ReassignMembersDataEntityStatus,
            formatResult: $scope.formatResultEntityStatus,
            formatSelection: $scope.formatSelection,
            dropdownCssClass: "bigdrop",
            escapeMarkup: function (m) {
                return m;
            }
        };

        function IsNotEmptyTree(treeObj) {
            var flag = false;
            for (var i = 0, node; node = treeObj[i++];) {
                if (node.ischecked == true) {
                    flag = true;
                    return flag;
                }
            }
            return flag;
        }

        function ClearSavedTreeNode(attributeid) {
            for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                branch.ischecked = false;
                if (branch.Children.length > 0) {
                    ClearRecursiveChildTreenode(branch.Children);
                }
            }
        }

        function ClearRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                child.ischecked = false;
                if (child.Children.length > 0) {
                    ClearRecursiveChildTreenode(child.Children);
                }
            }
        }

        function GetTreeObjecttoSave(attributeid) {
            $scope.treeNodeSelectedHolderValues = [];
            for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                if (branch.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == branch.AttributeId && e.id == branch.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(branch);
                    }
                    if (branch.Children.length > 0) {
                        FormRecursiveChildTreenode(branch.Children);
                    }
                }
            }
        }

        function GetAllTreeObject(attributeid) {
            $scope.treeNodeSelectedHolderValues = [];
            for (var i = 0, branch; branch = $scope.treesrcdirec["Attr_" + attributeid][i++];) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolderValues.push(branch);
                }
                if (branch.Children.length > 0) {
                    FormRecursiveChildTreenode(branch.Children);
                }
            }
        }

        function FormRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                if (child.ischecked == true) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == child.AttributeId && e.id == child.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(child);
                        if (child.Children.length > 0) {
                            FormRecursiveChildTreenode(child.Children);
                        }
                    }
                }
            }
        }
        var apple_selected, tree, treedata_avm, treedata_geography;
        $scope.my_tree_handler = function (branch, parentArr) {
            var _ref;
            $scope.output = "You selected: " + branch.Caption;
            if ((_ref = branch.data) != null ? _ref.description : void 0) {
                return $scope.output += '(' + branch.data.description + ')';
            }
            if (branch.ischecked == true) {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length == 0) {
                    $scope.treeNodeSelectedHolderValues.push(branch);
                }
                for (var i = 0, parent; parent = parentArr[i++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == parent.AttributeId && e.id == parent.id;
                    });
                    if (remainRecord.length == 0) {
                        $scope.treeNodeSelectedHolderValues.push(parent);
                    }
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                    return e.AttributeId == branch.AttributeId && e.id == branch.id;
                });
                if (remainRecord.length > 0) {
                    $scope.treeNodeSelectedHolderValues.splice($scope.treeNodeSelectedHolderValues.indexOf(branch), 1);
                    if (branch.Children.length > 0) {
                        RemoveRecursiveChildTreenode(branch.Children);
                    }
                }
            }
            if ($scope.treesrcdirec["Attr_" + branch.AttributeId].length > 0) {
                if (IsNotEmptyTree($scope.treesrcdirec["Attr_" + branch.AttributeId])) {
                    $scope.treePreviewObj["Attr_" + branch.AttributeId] = true;
                } else $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            } else {
                $scope.treePreviewObj["Attr_" + branch.AttributeId] = false;
            }

            function RemoveRecursiveChildTreenode(children) {
                for (var j = 0, child; child = children[j++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.treeNodeSelectedHolderValues, function (e) {
                        return e.AttributeId == child.AttributeId && e.id == child.id;
                    });
                    if (remainRecord.length > 0) {
                        $scope.treeNodeSelectedHolderValues.splice($.inArray(child, $scope.treeNodeSelectedHolderValues), 1);
                        if (child.Children.length > 0) {
                            RemoveRecursiveChildTreenode(child.Children);
                        }
                    }
                }
            }
        }
    }
    app.controller("mui.planningtool.default.detail.detailfilterCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$compile', '$window', 'DetailfilterService', '$translate', muiplanningtooldefaultdetaildetailfilterCtrl]);
})(angular, app);