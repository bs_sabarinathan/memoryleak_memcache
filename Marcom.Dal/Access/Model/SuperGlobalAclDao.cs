﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Access.Model
{
    public partial class SuperGlobalAclDao : BaseDao, ICloneable 
    {
        #region Member Variables

        protected int _id;
        protected int _globalroleid;
        protected int _menuid;
      //  protected int _entitytypeid;
        protected int _featureid;
        protected bool _accesspermission;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

       #region Constructors
		public SuperGlobalAclDao() {}

        public SuperGlobalAclDao(int pId, int pGlobalRoleid, int pMenuid, int pFeatureid, bool pAccessPermission)
		{
            this._id = pId;
			this._globalroleid = pGlobalRoleid; 
			this._menuid = pMenuid;
            //this._entitytypeid = pEntityTypeid;  int pEntityTypeid,
			this._featureid = pFeatureid; 
			this._accesspermission = pAccessPermission; 
		}
		
		#endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int GlobalRoleid
        {
            get { return _globalroleid; }
            set { _bIsChanged |= (_globalroleid != value); _globalroleid = value; }

        }

        public virtual int Menuid
        {
            get { return _menuid; }
            set { _bIsChanged |= (_menuid != value); _menuid = value; }

        }

        //public virtual int EntityTypeid
        //{
        //    get { return _entitytypeid; }
        //    set { _bIsChanged |= (_entitytypeid != value); _entitytypeid = value; }

        //}

        public virtual int Featureid
        {
            get { return _featureid; }
            set { _bIsChanged |= (_featureid != value); _featureid = value; }

        }

        public virtual bool AccessPermission
        {
            get { return _accesspermission; }
            set { _bIsChanged |= (_accesspermission != value); _accesspermission = value; }

        }


        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion 

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string Id = "Id";
            public const string SuperGlobalAclDao = "SuperGlobalAclDao";
            public const string GlobalRoleid = "GlobalRoleid";
            public const string Menuid = "Menuid";
            //public const string EntityTypeid = "EntityTypeid";
            public const string Featureid = "Featureid";
            public const string AccessPermission = "AccessPermission";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string Id = "Id";
            public const string SuperGlobalAclDao = "SuperGlobalAclDao";
            public const string GlobalRoleid = "GlobalRoleid";
            public const string Menuid = "Menuid";
            //public const string EntityTypeid = "EntityTypeid";
            public const string Featureid = "Featureid";
            public const string AccessPermission = "AccessPermission";
        }

        #endregion
    }
}
