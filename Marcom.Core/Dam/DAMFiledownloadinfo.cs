﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.DAM
{
   public class DAMFiledownloadinfo
    {
        #region Public Properties

        public double Height
        {
            get;
            set;

        }
        public double Width
        {
            get;
            set;

        }
        public double XDpi
        {
            get;
            set;
        }
        public double YDpi
        {
            get;
            set;
        }
        public string FileName
        {
            get;
            set;
        }

        public string FileFomat
        {
            get;
            set;
        }
        public string FilePath
        {
            get;
            set;
        }
        public string Errorinfo
        {
            get;
            set;
        }
        public bool Status
        {
            get;
            set;

        }
        #endregion
    }
}
