﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace BrandSystems.Marcom.Utility
{
  public class TranslatetoXml
    {
      public string ObjectToXML<t>(t arg)
      {

          XmlSerializer xs = new XmlSerializer(arg.GetType());

          MemoryStream memStream = default(MemoryStream);
          memStream = new MemoryStream();

          XmlTextWriter xmlWriter = default(XmlTextWriter);
          xmlWriter = new XmlTextWriter(memStream, Encoding.UTF8);

          xs.Serialize(xmlWriter, arg);

          xmlWriter.Close();
          memStream.Close();

          string xml = null;
          xml = Encoding.UTF8.GetString(memStream.GetBuffer());
          xml = xml.Substring(xml.IndexOf(Convert.ToChar(60)));
          xml = xml.Substring(0, (xml.LastIndexOf(Convert.ToChar(62)) + 1));

          return xml;

      }
    }
}
