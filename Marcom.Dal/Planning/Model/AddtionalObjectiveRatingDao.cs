﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{
    public partial class AddtionalObjectiveRatingDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _ratings;
        protected int _obectiveid;
        protected int _sortorder;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;

        #endregion

        #region Constructors
        public AddtionalObjectiveRatingDao() { }

        public AddtionalObjectiveRatingDao(int pId, string pRatings, int pObjectiveId, int pSortorder)
        {
            this._id = pId;
            this._ratings = pRatings;
            this._obectiveid = pObjectiveId;
            this._sortorder = pSortorder;
        }

        public AddtionalObjectiveRatingDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }
        public virtual string Ratings
        {
            get { return _ratings; }
            set { _ratings = value; }
        }
        public virtual int ObjectiveId
        {
            get { return _obectiveid; }
            set { _obectiveid = value; }

        }
        public virtual int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }
        #endregion
        
        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AddtionalObjectiveRatingDao = "AddtionalObjectiveRatingDao";
            public const string Id = "Id";
            public const string Ratings = "Ratings";
            public const string ObjectiveId = "ObjectiveId";
            public const string Sortorder = "Sortorder";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AddtionalObjectiveRatingDao = "AddtionalObjectiveRatingDao";
            public const string Id = "Id";
            public const string Ratings = "Ratings";
            public const string ObjectiveId = "ObjectiveId";
            public const string Sortorder = "Sortorder";
        }

        #endregion
    }
}
