﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using DevExpress.ReportServer.ServiceModel.Client;
using DevExpress.ReportServer.ServiceModel.DataContracts;
using System.Web.Services;
using BrandSystems.Marcom.Utility;
using BrandSystems.Marcom.Core.Managers.Proxy;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using System.Collections;
using System.ServiceModel;
using Presentation.Utility;
using System.Web.UI.WebControls;
using BrandSystems.Marcom.Core.Utility;
namespace Presentation
{
    public partial class ReportViewer : System.Web.UI.Page
    {
        protected string InitParams { get; set; }
        string username1;
        string userpassword1;
        string ReportUrl;
        protected void Page_Load(object sender, EventArgs e)
        {
            string baseDir = AppDomain.CurrentDomain.BaseDirectory;

            // search the file below the current directory
            string retFilePath = baseDir + "//" + "LogFile.txt";

            bool bReturnLog = false;
            ErrorLog.LogFilePath = retFilePath;
            try
            {

                TenantResolver tenantres = new TenantResolver(System.Web.HttpContext.Current.Request.Url.Host.ToString());
                Guid userSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);

                IMarcomManager managers = MarcomManagerFactory.GetMarcomManager(null, userSession);
                IList listresultreslit = null;
                listresultreslit = managers.ReportManager.ReportCredential_Select(0);


                for (int m = 0; m < listresultreslit.Count; m++)
                {
                    var item2 = listresultreslit[m];
                    username1 = ((string)((System.Collections.Hashtable)(item2))["ViewerUsername"]);
                    userpassword1 = ((string)((System.Collections.Hashtable)(item2))["ViewerPassword"]);
                    ReportUrl = ((string)((System.Collections.Hashtable)(item2))["ReportUrl"]);
                }
                var item3 = listresultreslit[0];
                // Read the authentication cookie and attach it to the response,
                // so that the embedded Silverlight viewer can authenticate on the Report Server


                if (Request.Form["ID"] != null)
                {
                    ReportId.Value = Request.Form["ID"];
                    if (Request.Form["SID"] != null)
                    {
                        SelectedIDs.Value = Request.Form["SID"].Replace(",", "&");
                    }
                    else
                    {
                        SelectedIDs.Value = "0";
                    }
                }

                ASPxDocumentViewer1.SettingsRemoteSource.ServerUri = ReportUrl;
                ASPxDocumentViewer1.SettingsRemoteSource.ReportId = int.Parse(ReportId.Value);
                ASPxDocumentViewer1.Height = new Unit(800);
                ASPxDocumentViewer1.Width = new Unit(1198);
            }
            catch (Exception ex)
            {
                bReturnLog = ErrorLog.CustomErrorRoutine(false, "Report Cache" + ex.Message.ToString(), DateTime.Now);
            }
        }

        protected void documentViewer_RequestParameters(object sender, DevExpress.XtraReports.Web.DocumentViewer.RequestParametersEventArgs e)
        {
            if (e.Parameters["SelectedIDs"] != null)
                e.Parameters["SelectedIDs"].Value = SelectedIDs.Value;
        }

        protected void documentViewer_RequestCredentials(object sender, DevExpress.XtraReports.Web.DocumentViewer.WebAuthenticatorLoginEventArgs e)
        {
            e.Credential = new DevExpress.XtraReports.Web.DocumentViewer.WebCredential(username1, userpassword1);
            e.Handled = true;
        }
       

    }
}
