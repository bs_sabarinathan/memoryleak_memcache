﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.DAM.Interface
    {
    /// <summary>
    /// IDamFileProfile interface for table 'Dam_File_Profile'.
    /// </summary>
    public interface IDamFileProfile
        {
        #region Public Properties

        int ID
            {
            get;
            set;
            }
        string Name
            {
            get;
            set;

            }

        double Height
            {

            get;
            set;
            }
        double Width
            {
            get;
            set;
            }

        string Extension
            {
            get;
            set;
            }
        int DPI
            {
            get;
            set;
            }
        string IsAssetAccess
            {
            get;
            set;
            }




        #endregion
        }
    }

