﻿using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Metadata;
using BrandSystems.Marcom.Metadata.Interface;
using System;

namespace BrandSystems.Marcom.Core.Metadata
{
    internal class FinancialMultiSelect : IFinancialMultiSelect
    {
        #region member variables

        public int _finID { get; set; }
        public int _finTypeID { get; set; }
        public int _finAttributeID { get; set; }
        public int _finOptionID { get; set; }
        public int _id { get; set; }
        #endregion


        #region Constructors
        public FinancialMultiSelect() { }
        public FinancialMultiSelect(int pid, int pfinid, int pfintypeid, int pfinattributeid, int pfinoptionid)
        {
            this._id = pid;
            this._finTypeID = pfintypeid;
            this._finAttributeID = pfinattributeid;
            this._finOptionID = pfinoptionid;
            this._finTypeID = pfintypeid;
        }
        public FinancialMultiSelect(int pid)
        {
            this._id = pid;
        }

        #endregion


        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int FinID
        {
            get { return _finID; }
            set { _finID = value; }

        }

        public virtual int FinTypeID
        {
            get { return _finTypeID; }
            set { _finTypeID = value; }

        }

        public virtual int FinAttributeID
        {
            get { return _finAttributeID; }
            set { _finAttributeID = value; }

        }

        public virtual int FinOptionID
        {
            get { return _finOptionID; }
            set { _finOptionID = value; }

        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            FinancialMultiSelect castObj = null;
            try
            {
                castObj = (FinancialMultiSelect)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.ID);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion


    }
}
