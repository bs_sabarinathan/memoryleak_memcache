﻿(function (ng, app) {
    "use strict";

    function financialhelptextctrl($scope, $location, $resource, $timeout, $cookies, $translate, AdminService) {

        GetFinancialAttribute();

        $scope.financialhelptextobj = { "selectedfinancialattr": [] };

        function GetFinancialAttribute() {

            AdminService.GetAllFinancialAttribute().then(function (getattributes) {
                $scope.Finattributes = getattributes.Response;

                for (var key in $scope.Finattributes) {
                    if ($scope.Finattributes[key].HelptextDecsription != null || key.HelptextDecsription != "") {
                        $scope.Finattributes[key].HelptextDecsription = $('<div />').html($scope.Finattributes[key].HelptextDecsription).text();
                        $scope.Finattributes[key]._HelptextDecsription = $('<div />').html($scope.Finattributes[key]._HelptextDecsription).text();
                    }
                }
            });
        }

        $scope.GetChckedFinancialhelptext = function (event, item, helptext) {

            var checkbox = event.target;
            var res1 = $.grep($scope.Finattributes, function (e) {
                return e.ID == item.ID;
            });

            if (res1.length > 0) {

                var tempres = $.grep($scope.financialhelptextobj.selectedfinancialattr, function (e) {
                    return e.ID == item.ID;
                });
                if (res1[0].IsHelptextEnabled == true) {
                    $timeout(function () {
                        res1[0].IsHelptextEnabled = false;
                        res1[0]._IsHelptextEnabled = false;
                    }, 10);

                    if (tempres.length > 0) {
                        tempres[0].IsHelptextEnabled = false;
                        tempres[0].HelptextDecsription = item.HelptextDecsription.trim();
                    }
                    else
                        $scope.financialhelptextobj.selectedfinancialattr.push({ IsHelptextEnabled: false, ID: item.ID, HelptextDecsription: item.HelptextDecsription.trim(), isfromtable: item.isfromtable });
                }
                else {
                    $timeout(function () {
                        res1[0].IsHelptextEnabled = true;
                        res1[0]._IsHelptextEnabled = true;
                    }, 10);
                    if (tempres.length > 0) {
                        tempres[0].IsHelptextEnabled = true;
                        tempres[0].HelptextDecsription = item.HelptextDecsription.trim();
                    }
                    else
                        $scope.financialhelptextobj.selectedfinancialattr.push({ IsHelptextEnabled: true, ID: item.ID, HelptextDecsription: item.HelptextDecsription.trim(), isfromtable: item.isfromtable });
                }
            }
        }

        $scope.UpdateFinancialHelptextDescription = function (ID, newvalue) {
            var attr_res = $.grep($scope.Finattributes, function (e) {
                return e.ID == ID;
            });
            if (attr_res.length > 0) {

                var tempres = $.grep($scope.financialhelptextobj.selectedfinancialattr, function (e) {
                    return e.ID == ID;
                });

                if (attr_res[0].HelptextDecsription.trim() == attr_res[0]._HelptextDecsription.trim()) {
                    attr_res[0].HelptextDecsription = attr_res[0].HelptextDecsription.trim();
                    attr_res[0]._HelptextDecsription = attr_res[0]._HelptextDecsription.trim();

                    if (tempres.length > 0) {
                        tempres[0].IsHelptextEnabled = attr_res[0].IsHelptextEnabled;
                        tempres[0].HelptextDecsription = newvalue.trim();
                    }
                    else
                        $scope.financialhelptextobj.selectedfinancialattr.push({ IsHelptextEnabled: attr_res[0].IsHelptextEnabled, ID: ID, HelptextDecsription: newvalue.trim(), isfromtable: attr_res[0].isfromtable });
                }
                else {
                    attr_res[0].HelptextDecsription = attr_res[0].HelptextDecsription.trim();
                    if (tempres.length > 0) {
                        tempres[0].IsHelptextEnabled = attr_res[0].IsHelptextEnabled;
                        tempres[0].HelptextDecsription = newvalue.trim();
                    }
                    else
                        $scope.financialhelptextobj.selectedfinancialattr.push({ IsHelptextEnabled: attr_res[0].IsHelptextEnabled, ID: ID, HelptextDecsription: newvalue.trim(), isfromtable: attr_res[0].isfromtable });
                }
            }
        }

        $scope.updatefinanacialhelptextobj = function () {
            if ($scope.financialhelptextobj.selectedfinancialattr.length > 0) {
                var updatefinancialhelptext = $scope.financialhelptextobj;
                AdminService.UpdateFinancialHelptext(updatefinancialhelptext).then(function (getresult) {
                    if (getresult.Response == true) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4805.Caption'));
                        for (var i = 0; i < $scope.financialhelptextobj.selectedfinancialattr.length; i++) {
                            var res = $.grep($scope.Finattributes, function (e) {
                                return e.ID == $scope.financialhelptextobj.selectedfinancialattr[i].ID;
                            });
                            if (res.length > 0) {
                                res[0]._HelptextDecsription = $scope.financialhelptextobj.selectedfinancialattr[i].HelptextDecsription;                                
                            }
                        }

                        $scope.financialhelptextobj.selectedfinancialattr = [];
                    }
                    else {
                        NotifyError($translate.instant('LanguageContents.Res_4941.Caption'));
                        $scope.financialhelptextobj.selectedfinancialattr = [];
                    }
                });
            }
        }
    }

    app.directive('nghelpblur', [function () {
        return {
            link: function (scope, element, attrs, ngModel) {
                element.bind('blur', function () {
                    var oldvalue = [];
                    var iterater = attrs.iterater;
                    var res = $.grep(scope.Finattributes, function (e) { return e.ID == iterater; });
                    if (res.length > 0) {
                        oldvalue = res[0]._HelptextDecsription;
                    }
                    else
                        return false;
                    var newvalue = element[0].value;
                    if (oldvalue != newvalue) {
                        $(element).addClass('success');
                        scope.UpdateFinancialHelptextDescription(attrs.iterater, newvalue);
                    }
                });

                scope.$on("$destroy", function () {
                    $(window).off("resize.Viewport");
                });
            }
        };
    }]);

    app.controller("mui.admin.financialhelptextctrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$translate', 'AdminService', financialhelptextctrl]);
})(angular, app);