﻿using BrandSystems.Marcom.Core.Planning.Interface;
using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.DAM.Interface
{
    /// <summary>
    /// IAttachments interface for table 'DM_Assets'.
    /// </summary>
    public interface IAssets
    {
        #region Public Properties

        int ID
        {
            get;
            set;

        }

        int EntityID
        {
            get;
            set;

        }

        string Name
        {
            get;
            set;

        }

        int ActiveFileID
        {
            get;
            set;
        }


        int FolderID
        {
            get;
            set;

        }
        DateTime Createdon
        {
            get;
            set;

        }
        int CreatedBy
        {
            get;
            set;
        }

        int AssetTypeid
        {
            get;
            set;
        }
        int Status
        {
            get;
            set;
        }
        bool Active
        {
            get;
            set;
        }
        DateTime UpdatedOn
        {
            get;
            set;

        }
        IList<IAttributeData> AttributeData { get; set; }
        IList<IDAMFile> Files { get; set; }
        dynamic MediaGeneratorData { get; set; }

        string AssetAccess
        { get; set; }

        int Category
        { get; set; }

        bool IsPublish
        { get; set; }
        string Url
        {
            get;
            set;

        }
        int LinkedAssetID { get; set; }
        string StrCreatedDate { get; set; }
        string StrUpdatedOn { get; set; }
        string assetTypeName { get; set; }

        int PublisherID
        {
            get;
            set;

        }

        DateTime? PublishedOn
        {
            get;
            set;

        }

        string[] TargetEntityID
        {
            get;
            set;
        }
        int ProductionEntityID
        {
            get;
            set;
        }
        string[] AssetRoles { get; set; }

        string folderName { get; set; }

        IList PublishLinkDetails { get; set; }

        string StrPublishedOn { get; set; }

        string StrExpiryDate { get; set; }


        string searchtype { get; set; }

        #endregion
    }
}
