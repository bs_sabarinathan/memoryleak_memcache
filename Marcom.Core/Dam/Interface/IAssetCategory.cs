﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Dam.Interface
{
    public interface IAssetCategory
    {
        int id { get; set; }
        string Caption { get; set; }
        string ParentKey { get; set; }
        int Level { get; set; }
        int ParentID { get; set; }
        bool IsChecked { get; set; }
        bool IsDeleted { get; set; }
        List<AssetCategory> Children { get; set; }
    }
}
