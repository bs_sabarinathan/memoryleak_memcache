﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Marcom.API.RequestHeaderDetails
{
    public class RequestHeaders
    {
        public Dictionary<string, string> RequestHeaderValues = new Dictionary<string, string>();
        public bool AuthorizationTokenPresent;
        public bool AuthTokenAuthenticate;
        public string AuthorizationToken;
        public int APIUserID;
        public Guid RequestHeaderSession;
        private static Regex isGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);
        public int ImpersonateUserID;
        public RequestHeaders()
        {
            GetRequestHeaders();
            AuthTokenPresent();
            AuthTokenValid();
            ReturnRequestSessionID();
            SetImpersonateUserID();
        }

        public Dictionary<string, string> GetRequestHeaders()
        {
            foreach (var key in HttpContext.Current.Request.Headers.AllKeys)
                RequestHeaderValues.Add(key, HttpContext.Current.Request.Headers[key].ToString());
            return RequestHeaderValues;
        }

        public bool AuthTokenPresent()
        {
            if (!RequestHeaderValues.ContainsKey("X-AuthorizationToken"))
            { AuthorizationTokenPresent = false; AuthorizationToken = string.Empty; }
            else
            { AuthorizationTokenPresent = true; AuthorizationToken = RequestHeaderValues["X-AuthorizationToken"].ToString(); }
            return AuthorizationTokenPresent;
        }

        public bool AuthTokenValid()
        {
            //write logic to validate the authentication token and get the respective user id.
            AuthTokenAuthenticate = true;
            APIUserID = 12771;
            return AuthTokenAuthenticate;
        }

        public void ReturnRequestSessionID()
        {
            if (RequestHeaderValues.ContainsKey("X-SessionID"))
            {
                bool IsSessionGuid = false;
                IsSessionGuid = IsGuid(RequestHeaderValues["X-SessionID"].ToString());
                if (IsSessionGuid == true) { RequestHeaderSession = new Guid(RequestHeaderValues["X-SessionID"]); }
            }
        }

        public void SetImpersonateUserID()
        {
            if (RequestHeaderValues.ContainsKey("X-ImpersonateUID") == true)
            {
                ImpersonateUserID = Convert.ToInt32((RequestHeaderValues["X-ImpersonateUID"]));
            }
            else
            {
                ImpersonateUserID = 0;
            }
        }

        internal static bool IsGuid(string reqSession)
        {
            bool isValid = false;
            if (reqSession != null)
            {
                if (isGuid.IsMatch(reqSession))
                {
                    isValid = true;
                }
            }

            return isValid;
        }

    }
}