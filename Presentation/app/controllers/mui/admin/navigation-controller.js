﻿(function (ng, app) {
    "use strict";

    function muiadminnavigationCtrl($scope, $location, $resource, $timeout, $cookies, $translate, AdminService) {
        var Parentid = 1;
        var NavigationID = 1;
        $scope.features = [];
        AdminService.GetNavigationByIDandParentid(1, parseInt($cookies['UserId']), 0).then(function (getnavigation) {
            $scope.listdata = getnavigation.Response;
            $scope.parentNavigationList = $scope.listdata;
            $timeout(function () { DragDrop(); }, 200)
            $(window).AdjustHeightWidth();
        });
        var DragDrop = function () {
            $('#MainNavigationTbl').tableDnD({
                onDrop: function (table, row) {
                    $scope.DragDropvalues = $.tableDnD.serialize();
                    saveSortOrder($scope.DragDropvalues);
                }
            });
        }

        function saveSortOrder(dragdata) {
            var id;
            var value = 0;
            var AttributeArray = [];
            var MainNavigationValues = [];
            var array = dragdata.split('&');
            for (var i = 0; i < array.length; i++) {
                if (array[i] != 'MainNavigationTbl[]=') {
                    id = array[i].replace('MainNavigationTbl[]=', '');
                    MainNavigationValues[value] = $.grep($scope.parentNavigationList, function (e) {
                        return e.Id == parseInt(id);
                    });
                    $.grep($scope.parentNavigationList, function (e) {
                        return e.Id == parseInt(id);
                    })[0].SortOrder = value + 1;
                    MainNavigationValues[value][0].SortOrder = value + 1;
                    value = value + 1;
                    AttributeArray.push({
                        "Id": parseInt(id),
                        "SortOrder": $.grep($scope.parentNavigationList, function (e) {
                            return e.Id == parseInt(id);
                        })[0].SortOrder
                    });
                }
            }
            var UpdateNavigationSortOrder = {};
            UpdateNavigationSortOrder.AttributeArray = AttributeArray;
            AdminService.UpdateNavigationSortOrder(UpdateNavigationSortOrder).then(function (updatenavicationsortorder) { });
        }
        AdminService.GetNavigationByIDandParentid(0, parseInt($cookies['UserId']), 0).then(function (getnavigationforsubmenu) {
            $scope.listdata1 = getnavigationforsubmenu.Response;
            $scope.childNavigationList = $scope.listdata1;
            $timeout(function () { DragDropSubNavig(); }, 200)
            $(window).AdjustHeightWidth();
        });
        var DragDropSubNavig = function () {
            $('#SubNavigationTbl').tableDnD({
                onDrop: function (table, row) {
                    $scope.DragDropvaluesSubNavig = $.tableDnD.serialize();
                    saveSortOrderSubNavig($scope.DragDropvaluesSubNavig);
                }
            });
        }

        function saveSortOrderSubNavig(dragdatasub) {
            var idsub;
            var valuesub = 0;
            var AttributeArraySub = [];
            var MainNavigationValuesSub = [];
            var arraysub = dragdatasub.split('&');
            for (var i = 0; i < arraysub.length; i++) {
                if (arraysub[i] != 'SubNavigationTbl[]=') {
                    idsub = arraysub[i].replace('SubNavigationTbl[]=', '');
                    MainNavigationValuesSub[valuesub] = $.grep($scope.childNavigationList, function (e) {
                        return e.Id == parseInt(idsub);
                    });
                    $.grep($scope.childNavigationList, function (e) {
                        return e.Id == parseInt(idsub);
                    })[0].SortOrder = valuesub + 1;
                    MainNavigationValuesSub[valuesub][0].SortOrder = valuesub + 1;
                    valuesub = valuesub + 1;
                    AttributeArraySub.push({
                        "Id": parseInt(idsub),
                        "SortOrder": $.grep($scope.childNavigationList, function (e) {
                            return e.Id == parseInt(idsub);
                        })[0].SortOrder
                    });
                }
            }
            var updatenavicationsortorder = {};
            updatenavicationsortorder.AttributeArray = AttributeArraySub;
            AdminService.UpdateNavigationSortOrder(updatenavicationsortorder).then(function (updatenavicationsortorder) { });
        }
        $scope.editableInPopup = '<a class="iconLink" data-toggle="modal" ng-click="GetModuleByID(row)" data-target="#moduleModal"><i class="icon-edit"></i></a> '
        $scope.delete = '<a class="iconLink" data-toggle="modal" ng-click="deleteNavigation(row)" data-toggle="modal"><i class="icon-remove"></i></a> '
        $scope.editableInsubmenuPopup = '<a class="iconLink" data-toggle="modal" ng-click="GetsubModuleByID(row)" data-toggle="modal" data-target="#modulesubModal"><i class="icon-edit"></i></a> '
        $scope.deletesubmenu = '<a class="iconLink" data-toggle="modal" ng-click="deleteNavigation(row)" data-toggle="modal"><i class="icon-remove"></i></a> '
        $scope.GetMainNavigationById = function GetMainNavigationById(row) {
            $('#moduleModal').modal('show');
            $scope.features = [];
            AdminService.GetModuleFeaturesForNavigation(row.Moduleid).then(function (getallfeatures) {
                $scope.features = getallfeatures.Response;
                $scope.NavigationID = row.Id;
                $scope.ddlModule = row.Moduleid;
                $scope.inputRoutes = row.Url;
                $scope.SortOrder = row.SortOrder;
                $scope.inputExternalurl = row.ExternalUrl;
                $scope.ddlFeature = row.Featureid;
                $scope.inputCaption = row.Caption;
                $scope.inputDescription = row.Description;
                $scope.inlineCheckboxIsDefault = 0;
                Boolean(row.IsDefault.toString().toLowerCase() == "true" ? 1 : 0);
                $scope.inlineCheckboxIsExternal = Boolean(row.IsExternal.toString().toLowerCase() == "true" ? 1 : 0);
                $scope.inlineCheckboxUserName = Boolean(row.AddUserName.toString().toLowerCase() == "true" ? 1 : 0);
                $scope.inlineCheckboxUserEmail = Boolean(row.AddUserEmail.toString().toLowerCase() == "true" ? 1 : 0);
                $scope.inlineCheckboxAddUserID = Boolean(row.AddUserID.toString().toLowerCase() == "true" ? 1 : 0);
                $scope.inlineCheckboxAddLanguageCode = Boolean(row.AddLanguageCode.toString().toLowerCase() == "true" ? 1 : 0);
                $scope.EnableUpdate = true;
                $scope.EnableAdd = false;
                $scope.ddlFeature = row.Featureid;
                $scope.ssltype = row.SearchType;
            });
            ValidateNavigation();
            $timeout(function () {
                $('#inputCaption').focus().select();
            }, 1000);
        };
        $scope.GetSubNavigationByID = function GetSubNavigationByID(row) {
            $('#modulesubModal').modal('show');
            $scope.subfeatures = [];
            AdminService.GetModuleFeaturesForNavigation(row.Moduleid).then(function (getallfeatures) {
                $scope.subfeatures = getallfeatures.Response;
                $scope.NavigationID = row.Id;
                $scope.ddlsubModule = row.Moduleid;
                $scope.inputSubRoutes = row.Url;
                if (row.Typeid == 17) {
                    $scope.inputExternalurl = row.ExternalUrl;
                } else {
                    $scope.inputExternalurl = '';
                }
                $scope.ddlsubFeature = row.Featureid;
                $scope.ddlParent = row.Parentid;
                $scope.inputsubCaption = row.Caption;
                $scope.inputsubDescription = row.Description;
                $scope.inlineCheckboxsubIsDefault = 0;
                $scope.inlineCheckboxIsExternal = Boolean(row.IsExternal.toString().toLowerCase() == "true" ? 1 : 0);
                $scope.inlineCheckboxsubUserName = Boolean(row.AddUserName.toString().toLowerCase() == "true" ? 1 : 0);;
                $scope.inlineCheckboxsubUserEmail = Boolean(row.AddUserEmail.toString().toLowerCase() == "true" ? 1 : 0);;
                $scope.inlineCheckboxsubAddUserID = Boolean(row.AddUserID.toString().toLowerCase() == "true" ? 1 : 0);;
                $scope.inlineCheckboxsubAddLanguageCode = Boolean(row.AddLanguageCode.toString().toLowerCase() == "true" ? 1 : 0);;
                $scope.EnablesubUpdate = true;
                $scope.EnablesubAdd = false;
                $scope.SortOrder = row.SortOrder;
                $scope.ddlFeature = row.Featureid;
                ValidatesubNavigation();
                $timeout(function () {
                    $('#inputsubCaption').focus().select();
                }, 1000);
                if (row.Featureid == 17) {
                    $scope.inputSubExternalurl = row.ExternalUrl;
                }
            });
        };
        var Version = 1;
        AdminService.GetModule().then(function (ModuleList) {
            $scope.Modules = ModuleList.Response;
            $scope.ModulesSelect = $scope.Modules;
        });
        $scope.navigationtypes = [{
            FeatureID: 1,
            FeatureName: 'Activity List'
        }, {
            FeatureID: 2,
            FeatureName: 'Cost center'
        }, {
            FeatureID: 3,
            FeatureName: 'Objective'
        }, {
            FeatureID: 6,
            FeatureName: 'DashBoard'
        }, {
            FeatureID: 11,
            FeatureName: 'ExternalLink'
        }, {
            FeatureID: 4,
            FeatureName: 'MyPage'
        }, {
            FeatureID: 5,
            FeatureName: 'Support'
        }, {
            FeatureID: 7,
            FeatureName: 'Task'
        }, {
            FeatureID: 8,
            FeatureName: 'Workspace'
        }, {
            FeatureID: 9,
            FeatureName: 'AdminSetting'
        }, {
            FeatureID: 10,
            FeatureName: 'Reports'
        }, {
            FeatureID: 12,
            FeatureName: 'MetadataSetting'
        }];
        $scope.getFeatures = function () {
            $scope.features = [];
            AdminService.GetModuleFeaturesForNavigation($scope.ddlModule).then(function (GetFeatures) {
                $scope.features = GetFeatures.Response;
            });
        }
        $scope.getSubFeatures = function () {
            $scope.subfeatures = [];
            AdminService.GetModuleFeaturesForNavigation($scope.ddlsubModule).then(function (getallfeatures) {
                $scope.subfeatures = getallfeatures.Response;
            });
        }
        $scope.slno = '<span class="slno">{{row.rowIndex+1}}</span>';
        $scope.gridOptions = {
            data: 'parentNavigationList',
            enablePinning: false,
            enableColumnReordering: false,
            columnDefs: [{
                field: "SlNo",
                displayName: 'Sl.No',
                cellTemplate: $scope.slno,
                width: 60
            }, {
                field: "Id",
                displayName: 'ID',
                width: 60,
                visible: false
            }, {
                field: "Typeid",
                displayName: 'Typeid',
                width: 60,
                visible: false
            }, {
                field: "Caption",
                displayName: 'Caption',
                width: 120
            }, {
                field: "Url",
                displayName: 'Url',
                width: 120
            }, {
                field: "ExternalUrl",
                displayName: 'ExternalUrl',
                width: 120
            }, {
                field: "Description",
                displayName: 'Description',
                width: 120
            }, {
                field: "IsDefault",
                displayName: 'Default',
                width: 80,
                visible: false
            }, {
                field: "AddUserName",
                displayName: 'User Name',
                width: 80
            }, {
                field: "AddUserEmail",
                displayName: 'User Email',
                width: 80
            }, {
                field: "AddUserID",
                displayName: 'UserID',
                width: 80
            }, {
                field: "AddLanguageCode",
                displayName: 'Lang Code',
                width: 80
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.editableInPopup,
                width: 30
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.delete,
                width: 30
            }]
        };
        $scope.gridOptionssubmenu = {
            data: 'childNavigationList',
            enablePinning: false,
            columnDefs: [{
                field: "SlNo",
                displayName: 'Sl.No',
                cellTemplate: $scope.slno,
                width: 60
            }, {
                field: "Id",
                displayName: 'ID',
                width: 60,
                visible: false
            }, {
                field: "Caption",
                displayName: 'Caption',
                width: 120
            }, {
                field: "Url",
                displayName: 'Url',
                width: 120
            }, {
                field: "ExternalUrl",
                displayName: 'ExternalUrl',
                width: 120
            }, {
                field: "Description",
                displayName: 'Description',
                width: 120
            }, {
                field: "IsDefault",
                displayName: 'Default',
                width: 80,
                visible: false
            }, {
                field: "AddUserName",
                displayName: 'User Name',
                width: 80
            }, {
                field: "AddUserEmail",
                displayName: 'User Email',
                width: 80
            }, {
                field: "AddUserID",
                displayName: 'UserID',
                width: 80
            }, {
                field: "AddLanguageCode",
                displayName: 'Lang Code',
                width: 80
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.editableInsubmenuPopup,
                width: 30
            }, {
                field: "",
                displayName: '',
                cellTemplate: $scope.deletesubmenu,
                width: 30
            }]
        };
        $scope.funfeatured = function () {
            if ($scope.ddlFeature == 17) {
                $scope.inlineCheckboxIsExternal = true;
            } else {
                $scope.inlineCheckboxIsExternal = false;
            }
        }
        $scope.subExternal = function () {
            if ($scope.ddlFeature == 17) {
                $scope.inlinesubCheckboxIsExternal = true;
            } else {
                $scope.inlinesubCheckboxIsExternal = false;
            }
        }
        $scope.update = function () {
            $scope.ddlNavigationType = 0;
            var updatenavication = {};
            updatenavication.ID = $scope.NavigationID;
            updatenavication.TypeID = $scope.ddlNavigationType;
            updatenavication.ModuleID = parseInt($scope.ddlModule);
            updatenavication.FeatureID = parseInt($scope.ddlFeature);
            updatenavication.SelectedSearchType = $scope.ssltype;
            updatenavication.ParentId = 0;
            updatenavication.Caption = $scope.inputCaption;
            updatenavication.Description = $scope.inputDescription;
            updatenavication.Url = $scope.inputRoutes;
            if ($scope.ddlFeature == 17) {
                updatenavication.ExternalUrl = $scope.inputExternalurl;
            } else {
                updatenavication.ExternalUrl = '';
            }
            updatenavication.IsExternal = $scope.inlineCheckboxIsExternal == false ? 0 : 1;
            updatenavication.IsDefault = 0;
            updatenavication.AddUserName = $scope.inlineCheckboxUserName == false ? 0 : 1;
            updatenavication.AddUserEmail = $scope.inlineCheckboxUserEmail == false ? 0 : 1;
            updatenavication.AddUserID = $scope.inlineCheckboxAddUserID == false ? 0 : 1;
            updatenavication.AddLanguageCode = $scope.inlineCheckboxAddLanguageCode == false ? 0 : 1;
            updatenavication.IsEnable = 0;
            updatenavication.SortOrder = $scope.SortOrder;
            AdminService.UpdateNavigationByID(updatenavication).then(function (updateNavigation) {
                if (updateNavigation.Response == false) {
                    NotifyError($translate.instant('LanguageContents.Res_4364.Caption'));
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                }
                var updateNavigationList = $.grep($scope.listdata, function (e) {
                    return e.Id == $scope.NavigationID;
                });
                updateNavigationList[0].Typeid = 0;
                updateNavigationList[0].ModuleID = $scope.ddlModule;
                updateNavigationList[0].ParentId = 0;
                updateNavigationList[0].Featureid = $scope.ddlFeature;
                updateNavigationList[0].Caption = $scope.inputCaption;
                updateNavigationList[0].Description = $scope.inputDescription;
                updateNavigationList[0].Url = $scope.inputRoutes;
                if ($scope.ddlFeature == 17) {
                    updateNavigationList[0].ExternalUrl = $scope.inputExternalurl;
                } else {
                    updateNavigationList[0].ExternalUrl = '';
                }
                updateNavigationList[0].IsExternal = $scope.inlineCheckboxIsExternal;
                updateNavigationList[0].IsEnable = 0;
                updateNavigationList[0].IsDefault = $scope.inlineCheckboxIsDefault;
                updateNavigationList[0].AddUserName = $scope.inlineCheckboxUserName;
                updateNavigationList[0].AddUserEmail = $scope.inlineCheckboxUserEmail;
                updateNavigationList[0].AddUserID = $scope.inlineCheckboxAddUserID;
                updateNavigationList[0].AddLanguageCode = $scope.inlineCheckboxAddLanguageCode;
                updateNavigationList[0].SearchType = $scope.ssltype;
            });
            $('#moduleModal').modal('hide');
        };
        $scope.updatesubmodule = function () {
            var updatenavication1 = {};
            updatenavication1.ID = $scope.NavigationID;
            updatenavication1.TypeID = 0;
            updatenavication1.ModuleID = parseInt($scope.ddlsubModule);
            updatenavication1.FeatureID = parseInt($scope.ddlsubFeature == undefined ? 0 : $scope.ddlsubFeature);
            updatenavication1.ParentId = parseInt($scope.ddlParent);
            updatenavication1.Caption = $scope.inputsubCaption;
            updatenavication1.Description = $scope.inputsubDescription;
            updatenavication1.Url = $scope.inputSubRoutes;
            if ($scope.ddlFeature == 17) {
                updatenavication1.ExternalUrl = $scope.inputSubExternalurl;
            } else {
                updatenavication1.ExternalUrl = '';
            }
            updatenavication1.IsExternal = $scope.inlineCheckboxIsExternal == false ? 0 : 1;
            if ($scope.inlineCheckboxsubIsDefault != undefined) updatenavication1.IsEnable = $scope.inlineCheckboxsubIsDefault;
            else updatenavication1.IsEnable = 0;
            if ($scope.inlineCheckboxsubIsDefault != undefined) updatenavication1.IsDefault = $scope.inlineCheckboxsubIsDefault;
            else updatenavication1.IsDefault = 0;
            updatenavication1.AddUserName = $scope.inlineCheckboxsubUserName == false ? 0 : 1;
            updatenavication1.AddUserEmail = $scope.inlineCheckboxsubUserEmail == false ? 0 : 1;
            updatenavication1.AddUserID = $scope.inlineCheckboxsubAddUserID == false ? 0 : 1;
            updatenavication1.AddLanguageCode = $scope.inlineCheckboxsubAddLanguageCode == false ? 0 : 1;
            updatenavication1.SortOrder = $scope.SortOrder;
            updatenavication1.SelectedSearchType = $scope.ssltype;
            AdminService.UpdateNavigationByID(updatenavication1).then(function (updateChildNavigation) {
                if (updateChildNavigation.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4364.Caption'));
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4875.Caption'));
                }
                var updateChildNavigationList = $.grep($scope.listdata1, function (e) {
                    return e.Id == $scope.NavigationID;
                });
                updateChildNavigationList[0].TypeID = 0;
                updateChildNavigationList[0].ModuleID = $scope.ddlsubModule;
                updateChildNavigationList[0].ParentId = $scope.ddlParent;
                updateChildNavigationList[0].Caption = $scope.inputsubCaption;
                updateChildNavigationList[0].Description = $scope.inputsubDescription;
                updateChildNavigationList[0].Url = $scope.inputSubRoutes;
                updateChildNavigationList[0].ExternalUrl = $scope.inputSubExternalurl;
                updateChildNavigationList[0].IsExternal = $scope.inlineCheckboxIsExternal;
                updateChildNavigationList[0].IsEnable = 0;
                updateChildNavigationList[0].FeatureID = $scope.ddlFeature;
                updateChildNavigationList[0].IsDefault = 0;
                updateChildNavigationList[0].AddUserName = $scope.inlineCheckboxsubUserName;
                updateChildNavigationList[0].AddUserEmail = $scope.inlineCheckboxsubUserEmail;
                updateChildNavigationList[0].AddUserID = $scope.inlineCheckboxsubAddUserID;
                updateChildNavigationList[0].AddLanguageCode = $scope.inlineCheckboxsubAddLanguageCode;
                AdminService.GetNavigationByIDandParentid(0, parseInt($cookies['UserId']), 0).then(function (getnavigationforsubmenu) {
                    $scope.listdata1 = getnavigationforsubmenu.Response;
                    $scope.childNavigationList = $scope.listdata1;
                    $timeout(function () { DragDrop(); }, 200);
                    $timeout(function () { DragDropSubNavig(); }, 200);
                    $(window).AdjustHeightWidth();
                });
            });
            $('#modulesubModal').modal('hide');
        };
        $scope.addModule = function () {
            $('#moduleModal').modal('show');
            $scope.inputExternalurl = "";
            $scope.features = [];
            $scope.inputDescription = "";
            $scope.inputRoutes = "";
            $scope.inputCaption = "";
            $scope.inlineCheckboxIsDefault = "";
            $scope.inlineCheckboxIsExternal = "";
            $scope.EnableAdd = true;
            $scope.ddlFeature = '';
            $scope.ddlModule = '';
            $scope.EnableUpdate = false;
            $scope.inputCaption = "";
            $scope.inlineCheckboxUserName = "";
            $scope.inlineCheckboxUserEmail = "";
            $scope.inlineCheckboxAddUserID = "";
            $scope.inlineCheckboxAddLanguageCode = "";
            $scope.ssltype = 0;
            ValidateNavigation();
            $timeout(function () {
                $('#inputCaption').focus().select();
            }, 1000);
        };
        $scope.addsubModule = function () {
            $('#modulesubModal').modal('show');
            $scope.inputSubExternalurl = "";
            $scope.inputsubDescription = "";
            $scope.inputSubRoutes = "";
            $scope.inlineCheckboxsubIsDefault = "";
            $scope.inputCaption = "";
            $scope.inlineCheckboxIsExternal = "";
            $scope.EnablesubAdd = true;
            $scope.ddlsubFeature = 0;
            $scope.ddlParent = 0;
            $scope.ddlsubModule = 0;
            $scope.subddlFeature = 0;
            $scope.EnablesubUpdate = false;
            $scope.inputsubCaption = "";
            $scope.inlineCheckboxsubUserName = "";
            $scope.inlineCheckboxsubUserEmail = "";
            $scope.inlineCheckboxsubAddUserID = "";
            $scope.inlineCheckboxsubAddLanguageCode = "";
            $scope.inputSubExternalurl = "";
            ValidatesubNavigation();
            $timeout(function () {
                $('#inputsubCaption').focus().select();
            }, 1000);
        };
        $scope.save = function () {
            $("#btnTempMainNav").click();
            $("#frmMainNavigation").removeClass('notvalidate');
            if ($("#frmMainNavigation .error").length > 0) {
                return false;
            }
            if ($scope.inputRoutes[0] == "/" || $scope.inputRoutes[$scope.inputRoutes.length - 1] == "/" || $scope.inputRoutes[0] == "\\" || $scope.inputRoutes[$scope.inputRoutes.length - 1] == "\\") {
                bootbox.alert($translate.instant('LanguageContents.Res_4746.Caption'));
                return false;
            }
            var addmodule = {};
            addmodule.SearchType = $scope.ssltype;
            addmodule.ID = 0;
            addmodule.TypeID = 0;
            addmodule.ParentID = 0;
            addmodule.ModuleID = parseInt($scope.ddlModule);
            addmodule.FeatureID = parseInt($scope.ddlFeature);
            addmodule.Caption = $scope.inputCaption;
            if ($scope.inputExternalurl == undefined) {
                $scope.inputExternalurl = '';
            }
            addmodule.ExternalUrl = $scope.inputExternalurl;
            addmodule.Description = $scope.inputDescription;
            addmodule.Url = $scope.inputRoutes;
            addmodule.JavaScript = $scope.inputDescription;
            if ($scope.inlineCheckboxIsDefault == undefined || $scope.inlineCheckboxIsDefault == "") {
                $scope.inlineCheckboxIsDefault = 0;
            }
            if ($scope.inlineCheckboxIsDefault != undefined) {
                addmodule.IsActive = $scope.inlineCheckboxIsDefault;
            } else {
                addmodule.IsActive = 0;
            }
            addmodule.IsPopup = addmodule.IsActive;
            addmodule.IsIframe = addmodule.IsActive;
            addmodule.IsDynamicPage = addmodule.IsActive;
            addmodule.IsDefault = addmodule.IsActive;
            addmodule.IsExternal = 0;
            if ($scope.inlineCheckboxUserName != 0) addmodule.AddUserName = $scope.inlineCheckboxUserName;
            else addmodule.AddUserName = 0;
            if ($scope.inlineCheckboxUserEmail != 0) addmodule.AddUserEmail = $scope.inlineCheckboxUserEmail;
            else addmodule.AddUserEmail = 0;
            if ($scope.inlineCheckboxAddUserID != 0) addmodule.AddUserID = $scope.inlineCheckboxAddUserID;
            else addmodule.AddUserID = 0;
            if ($scope.inlineCheckboxAddLanguageCode != 0) addmodule.AddLanguageCode = $scope.inlineCheckboxAddLanguageCode;
            else addmodule.AddLanguageCode = 0;
            addmodule.Imageurl = $scope.inputDescription;
            addmodule.GlobalRoleid = 1;
            AdminService.InsertNavigation(addmodule).then(function (savenavigation) {
                if (savenavigation.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4340.Caption'));
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                    $scope.listdata.push({
                        "Id": $scope.navigationResultID,
                        "Parentid": 0,
                        "Typeid": $scope.ddlFeature,
                        "Moduleid": $scope.ddlModule,
                        "Caption": $scope.inputCaption,
                        "Url": $scope.inputRoutes,
                        "ExternalUrl": $scope.inputExternalurl,
                        "IsExternal": "false",
                        "IsDefault": $scope.inlineCheckboxIsDefault == 0 ? "False" : "True",
                        "Description": $scope.inputDescription,
                        "Featureid": "",
                        "JavaScript": "",
                        "IsActive": $scope.inlineCheckboxIsDefault == 0 ? "False" : "True",
                        "IsPopup": $scope.inlineCheckboxIsDefault == 0 ? "False" : "True",
                        "IsIframe": $scope.inlineCheckboxIsDefault == 0 ? "False" : "True",
                        "IsDynamicPage": $scope.inlineCheckboxIsDefault == 0 ? "False" : "True",
                        "AddUserName": $scope.inlineCheckboxUserName == 0 ? "False" : "True",
                        "AddUserEmail": $scope.inlineCheckboxUserEmail == 0 ? "False" : "True",
                        "AddUserID": $scope.inlineCheckboxAddUserID == 0 ? "False" : "True",
                        "AddLanguageCode": $scope.inlineCheckboxAddLanguageCode == 0 ? "False" : "True",
                        "Imageurl": "",
                        "SearchType": $scope.ssltype
                    });
                    AdminService.GetNavigationByIDandParentid(1, parseInt($cookies['UserId']), 0).then(function (getnavigation) {
                        $scope.listdata = getnavigation.Response;
                        $scope.parentNavigationList = $scope.listdata;
                        $timeout(function () { DragDrop(); }, 300);
                        $timeout(function () { DragDropSubNavig(); }, 300);
                        $(window).AdjustHeightWidth();
                    });
                }
                $scope.navigationResultID = savenavigation.Response;
            });
            $('#moduleModal').modal('hide');
        };
        $scope.savesubmenu = function () {
            $("#btnTempsubNav").click();
            $("#frmsubNavigation").removeClass('notvalidate');
            if ($("#frmsubNavigation .error").length > 0) {
                return false;
            }
            if ($scope.inputSubRoutes[0] == "/" || $scope.inputSubRoutes[$scope.inputSubRoutes.length - 1] == "/" || $scope.inputSubRoutes[0] == "\\" || $scope.inputSubRoutes[$scope.inputSubRoutes.length - 1] == "\\") {
                bootbox.alert($translate.instant('LanguageContents.Res_4746.Caption'));
                return false;
            }
            var addmodule = {};
            addmodule.ID = 0;
            addmodule.TypeID = 0;
            addmodule.ParentID = $scope.ddlParent;
            addmodule.ModuleID = parseInt($scope.ddlsubModule);
            addmodule.FeatureID = parseInt($scope.ddlsubFeature);
            addmodule.Caption = $scope.inputsubCaption;
            if ($scope.inputSubExternalurl == undefined) {
                $scope.inputSubExternalurl = '';
            }
            addmodule.ExternalUrl = $scope.inputSubExternalurl;
            addmodule.Description = $scope.inputsubDescription;
            addmodule.Url = $scope.inputSubRoutes;
            addmodule.JavaScript = $scope.inputsubDescription;
            if ($scope.inlineCheckboxsubIsDefault == '') {
                $scope.inlineCheckboxsubIsDefault = 0;
            }
            addmodule.IsActive = 0;
            addmodule.IsPopup = 0;
            addmodule.IsIframe = 0;
            addmodule.IsDynamicPage = 0;
            addmodule.IsDefault = 0;
            addmodule.IsExternal = 0;
            addmodule.AddUserName = $scope.inlineCheckboxsubUserName;
            addmodule.AddUserEmail = $scope.inlineCheckboxsubUserEmail;
            addmodule.Imageurl = $scope.inputsubDescription;
            addmodule.IsDefault = 0;
            addmodule.SearchType = 0;
            if ($scope.inlineCheckboxsubUserName != '') addmodule.AddUserName = $scope.inlineCheckboxsubUserName;
            else addmodule.AddUserName = 0;
            if ($scope.inlineCheckboxsubUserEmail != '') addmodule.AddUserEmail = $scope.inlineCheckboxsubUserEmail;
            else addmodule.AddUserEmail = 0;
            if ($scope.inlineCheckboxsubAddUserID != '') addmodule.AddUserID = $scope.inlineCheckboxsubAddUserID;
            else addmodule.AddUserID = 0;
            if ($scope.inlineCheckboxsubAddLanguageCode != '') addmodule.AddLanguageCode = $scope.inlineCheckboxsubAddLanguageCode;
            else addmodule.AddLanguageCode = 0;
            addmodule.GlobalRoleid = 1;
            AdminService.InsertNavigation(addmodule).then(function (saveChildnavigation) {
                if (saveChildnavigation.StatusCode == 405) {
                    NotifyError($translate.instant('LanguageContents.Res_4376.Caption'));
                } else {
                    NotifySuccess($translate.instant('LanguageContents.Res_4717.Caption'));
                    $scope.listdata1.push({
                        "Id": $scope.childnavigationResultID,
                        "Parentid": $scope.ddlParent,
                        "Typeid": $scope.ddlFeature,
                        "Moduleid": $scope.ddlsubModule,
                        "Caption": $scope.inputsubCaption,
                        "Url": $scope.inputSubRoutes,
                        "ExternalUrl": $scope.inputSubExternalurl,
                        "IsExternal": "false",
                        "IsDefault": $scope.inlineCheckboxsubIsDefault == 0 ? "False" : "True",
                        "Description": $scope.inputsubDescription,
                        "Featureid": "",
                        "JavaScript": "",
                        "IsActive": $scope.inlineCheckboxsubIsDefault == 0 ? "False" : "True",
                        "IsPopup": $scope.inlineCheckboxsubIsDefault == 0 ? "False" : "True",
                        "IsIframe": $scope.inlineCheckboxsubIsDefault == 0 ? "False" : "True",
                        "IsDynamicPage": $scope.inlineCheckboxsubIsDefault == 0 ? "False" : "True",
                        "AddUserName": $scope.inlineCheckboxsubUserName == 0 ? "False" : "True",
                        "AddUserEmail": $scope.inlineCheckboxsubUserEmail == 0 ? "False" : "True",
                        "AddUserID": $scope.inlineCheckboxsubAddUserID == 0 ? "False" : "True",
                        "AddLanguageCode": $scope.inlineCheckboxsubAddLanguageCode == 0 ? "False" : "True",
                        "Imageurl": ""
                    });
                    AdminService.GetNavigationByIDandParentid(0, parseInt($cookies['UserId']), 0).then(function (getnavigationforsubmenu) {
                        $scope.listdata1 = getnavigationforsubmenu.Response;
                        $scope.childNavigationList = $scope.listdata1;
                        $timeout(function () { DragDrop(); }, 300);
                        $timeout(function () { DragDropSubNavig(); }, 300);
                        $(window).AdjustHeightWidth();
                    });
                }
                $scope.childnavigationResultID = saveChildnavigation.Response;
            });
            $('#modulesubModal').modal('hide');
        };
        $scope.deleteNavigation = function deleteNavigation(row) {
            bootbox.confirm($translate.instant('LanguageContents.Res_2050.Caption'), function (result) {
                if (result) {
                    $timeout(function () {
                        var ID = row.Id;
                        AdminService.DeleteNavigation(ID).then(function (deleteAttributeById) {
                            var index = $scope.listdata.indexOf(row);
                            if (index < 0) {
                                index = $scope.listdata1.indexOf(row);
                                $scope.listdata1.splice(index, 1);
                            } else $scope.listdata.splice(index, 1);
                            NotifySuccess($translate.instant('LanguageContents.Res_4206.Caption'));
                            AdminService.GetNavigationByIDandParentid(1, parseInt($cookies['UserId']), 0).then(function (getnavigation) {
                                $scope.listdata = getnavigation.Response;
                                $scope.parentNavigationList = $scope.listdata;
                                $timeout(function () { DragDrop(); }, 300)
                                $timeout(function () { DragDropSubNavig(); }, 300);
                                $(window).AdjustHeightWidth();
                            });
                        });
                    }, 100);
                }
            });
        };

        function ValidateNavigation() {
            var entitytypeValues = [
				['#inputCaption', 'presence', 'Please enter the Caption'],
				['#ddlModule', 'presence', 'Please select Moudle'],
				['#Feature', 'presence', 'Please select Feature'],
				['#inputRoutes', 'presence', 'Please enter URL']
            ];
            $("#frmMainNavigation").nod(entitytypeValues, {
                'delay': 200,
                'submitBtnSelector': '#btnTempMainNav',
                'silentSubmit': 'true'
            });
            $("#frmMainNavigation").addClass('notvalidate');
        }
        $scope.SearchOptions = [{
            Lable: "All",
            id: 0,
            isSelected: true,
            "Actual_Type": "All"
        }, {
            Lable: "Productions",
            id: 1,
            isSelected: true,
            "Actual_Type": "Productions"
        }, {
            Lable: "Tasks",
            id: 2,
            isSelected: true,
            "Actual_Type": "Task"
        }, {
            Lable: "Attachments",
            id: 3,
            isSelected: true,
            "Actual_Type": "Attachments"
        }, {
            Lable: "Asset library",
            id: 4,
            isSelected: true,
            "Actual_Type": "Assetlibrary"
        }, {
            Lable: "Pages",
            id: 5,
            isSelected: true,
            "Actual_Type": "CmsNavigation"
        }, {
            Lable: "Tags",
            id: 6,
            isSelected: true,
            "Actual_Type": "Tags"
        }]
        $scope.ssltype = 0;

        function ValidatesubNavigation() {
            var entitytypeValuessub = [
				['#inputsubCaption', 'presence', 'Please enter the Caption'],
				['#ddlsubModule', 'presence', 'Please select Moudle'],
				['#ddlsubFeature', 'presence', 'Please select Feature'],
				['#inputSubRoutes', 'presence', 'Please enter URL'],
				['#ddlParent', 'presence', 'Please enter Parent Naviagation']
            ];
            $("#frmsubNavigation").nod(entitytypeValuessub, {
                'delay': 200,
                'submitBtnSelector': '#btnTempsubNav',
                'silentSubmit': 'true'
            });
            $("#frmsubNavigation").addClass('notvalidate');
        }
        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.admin.navigationCtrl']"));
        });
    }
    app.controller("mui.admin.navigationCtrl", ['$scope', '$location', '$resource', '$timeout', '$cookies', '$translate', 'AdminService', muiadminnavigationCtrl]);
})(angular, app);