﻿using System;
using System.Collections.Generic;


namespace BrandSystems.Marcom.Dal.Task.Model
{
   public class EntityTaskCheckListDao : BaseDao, ICloneable
    {
        #region Member Variables

       
        protected int _id;
        protected int _taskId;
        protected string _name;
        protected int _sortOrder;
        protected int _userId;
        protected int _ownerId;
        protected DateTime? _completedOn;
        protected bool _status;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;

        #endregion


        #region Constructors
        public EntityTaskCheckListDao() { }

        public EntityTaskCheckListDao(int tId, int tTaskId, string tName, int tSortOrder ,int tUserId, int tOwnerId, DateTime? tCompletedOn,bool pstatus)
        {
            this._id = tId;
            this._taskId = tTaskId;
            this._name = tName;
            this._sortOrder = tSortOrder;
            this._userId= tUserId;
            this._ownerId= tOwnerId;
            this._completedOn= tCompletedOn;
            this._status = pstatus;
        }

        public EntityTaskCheckListDao(int tId)
        {
            this._id = tId;
        }

        #endregion

        #region public properties


        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int TaskId
        {
            get { return _taskId; }
            set { _bIsChanged |= (_taskId != value); _taskId = value; }
        }

        public virtual string Name
        {
            get { return _name; }
            set { _bIsChanged |= (_name != value); _name = value; }
        }

        public virtual int SortOrder
        {
            get { return _sortOrder; }
            set { _bIsChanged |= (_sortOrder != value); _sortOrder = value; }
        }

        public virtual int UserId
        {
            get { return _userId; }
            set { _bIsChanged |= (_userId != value); _userId = value; }
        }

        public virtual int OwnerId
        {
            get { return _ownerId; }
            set { _bIsChanged |= (_ownerId != value); _ownerId = value; }
        }

        public virtual DateTime? CompletedOn
        {
            get { return _completedOn; }
            set { _bIsChanged |= (_completedOn != value); _completedOn = value; }
        }

        public virtual bool Status
        {
            get { return _status; }
            set { _bIsChanged |= (_status != value); _status = value; }
        }



        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }


        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion


        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string EntityTaskCheckListDao = "EntityTaskCheckListDao";
            public const string Id = "Id";
            public const string TaskId = "TaskId";
            public const string Name = "Name";
            public const string SortOrder = "SortOrder";
            public const string UserId = "UserId";
            public const string OwnerId = "OwnerId";
            public const string CompletedOn = "CompletedOn";
            public const string Status = "Status";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string EntityTaskCheckListDao = "EntityTaskCheckListDao";
            public const string Id = "Id";
            public const string TaskId = "TaskId";
            public const string Name = "Name";
            public const string SortOrder = "SortOrder";
            public const string UserId = "UserId";
            public const string OwnerId = "OwnerId";
            public const string CompletedOn = "CompletedOn";
            public const string Status = "Status";
        }

        #endregion






    }
}
