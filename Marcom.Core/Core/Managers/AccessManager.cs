﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Core.Interface.Managers;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Managers.Proxy;
using BrandSystems.Marcom.Core.Access.Interface;
using BrandSystems.Marcom.Dal.Access.Model;
using BrandSystems.Marcom.Core.Access;
using BrandSystems.Marcom.Dal.Base;
using System.Collections;
using BrandSystems.Marcom.Core.Metadata;
using Newtonsoft.Json.Linq;
using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Dal.Planning.Model;
using BrandSystems.Marcom.Dal.User.Model;

namespace BrandSystems.Marcom.Core.Managers
{
    internal partial class AccessManager : IManager
    {
        /// <summary>
        /// The instance
        /// </summary>
        private static AccessManager instance = new AccessManager();

        /// <summary>
        /// Initializes the specified marcom manager.
        /// </summary>
        /// <param name="marcomManager">The marcom manager.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Initialize(IMarcomManager marcomManager)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>
        /// The instance.
        /// </value>
        internal static AccessManager Instance
        {
            get { return instance; }
        }

        /// <summary>
        /// Commits the caches.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void CommitCaches()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Rollbacks the caches.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void RollbackCaches()
        {
            throw new NotImplementedException();
        }

        #region Instance of Classes In ServiceLayer reference
        /// <summary>
        /// Returns TaskMember class.
        /// </summary>
        public ITaskMember TaskMemberservice()
        {
            return new TaskMember();
        }


        #endregion


        /// <summary>
        /// Creates the entity user role.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="entityId">The entity id.</param>
        /// <param name="Roleid">The roleid.</param>
        /// <param name="Userid">The userid.</param>
        /// <param name="IsInherited">if set to <c>true</c> [is inherited].</param>
        /// <param name="InheritedFromEntityid">The inherited from entityid.</param>
        /// <returns>IEntityRoleUser</returns>
        public int CreateEntityUserRole(AccessManagerProxy proxy, int entityId, int Roleid, int Userid, bool IsInherited, int InheritedFromEntityid)
        {
            //Check access 
            //Start Transaction

            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    // Business logic of EntityRoleUser
                    EntityRoleUserDao dao = new EntityRoleUserDao();
                    dao.Entityid = entityId;
                    dao.Roleid = Roleid;
                    dao.Userid = Userid;
                    dao.IsInherited = IsInherited;
                    dao.InheritedFromEntityid = InheritedFromEntityid;
                    tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Save<EntityRoleUserDao>(dao);
                    tx.Commit();
                    IEntityRoleUser EntityRoleUser = new EntityRoleUser();
                    EntityRoleUser.Id = dao.Id;
                    EntityRoleUser.Entityid = dao.Entityid;
                    EntityRoleUser.Roleid = dao.Roleid;
                    EntityRoleUser.Userid = dao.Userid;
                    EntityRoleUser.IsInherited = dao.IsInherited;
                    EntityRoleUser.InheritedFromEntityid = dao.InheritedFromEntityid;

                    return EntityRoleUser.Id;
                }

            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        /// <summary>
        /// Deletes the entity user role.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="entityId">The entity id.</param>
        /// <param name="Roleid">The roleid.</param>
        /// <param name="Userid">The userid.</param>
        /// <returns>IEntityRoleUser</returns>
        public bool DeleteEntityUserRole(AccessManagerProxy proxy, int ID)
        {
            try
            {
                IEntityRoleUser _DeleteEntityUserRole = new EntityRoleUser();
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    EntityRoleUserDao dao = new EntityRoleUserDao();
                    dao.Id = ID;
                    tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Delete<EntityRoleUserDao>(dao);
                    tx.Commit();
                    _DeleteEntityUserRole.Entityid = dao.Entityid;
                    _DeleteEntityUserRole.Roleid = dao.Roleid;
                    _DeleteEntityUserRole.Userid = dao.Userid;
                }
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;

        }

        /// <summary>
        /// get the role.
        /// </summary>
        /// <returns>Ilist of IRole</returns>
        public IList<IRole> GetRole(AccessManagerProxy proxy)
        {
            try
            {
                IList<IRole> irole = new List<IRole>();
                IList<RoleDao> roledao = new List<RoleDao>();
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    roledao = tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].GetAll<RoleDao>();
                    int[] EntityRoleIds = { 2, 3, 5 };
                    var result = roledao.Where(at => EntityRoleIds.Contains(at.Id));
                    foreach (var item in result)
                    {
                        IRole role = new Role();
                        role.Id = item.Id;
                        role.Caption = item.Caption;
                        role.Description = item.Description;
                        irole.Add(role);
                    }
                }
                return irole;
            }
            catch
            {

            }
            return null;
        }

        /// <summary>
        /// Saves/Update role.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="Caption">The caption.</param>
        /// <param name="Description">The description.</param>
        /// <param name="Id">The id.</param>
        /// <returns>String</returns>
        public int SaveUpdateRole(AccessManagerProxy proxy, String Caption, String Description, int Id)
        {
            try
            {
                RoleDao Role = new RoleDao();
                IList<RoleDao> RoleList;
                if (Id == 0)
                {
                    Role.Caption = Caption;
                    Role.Description = Description;
                    using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                    {
                        RoleList = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].GetAll<RoleDao>();
                        tx.Commit();
                    }
                    var x = from t in RoleList where t.Caption == Caption && t.Description == Description select t;
                    if (x.Count() > 0)
                    {
                        Role.Id = x.ElementAt(0).Id;
                    }
                }
                else
                {
                    Role.Id = Id;
                    Role.Caption = Caption;
                    Role.Description = Description;
                }
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Save<RoleDao>(Role);
                    tx.Commit();
                }
                return Role.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Deletes the role.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="Id">The id.</param>
        /// <returns>String</returns>
        public bool DeleteRole(AccessManagerProxy proxy, int Id)
        {
            try
            {
                RoleDao Role = new RoleDao();
                Role.Id = Id;
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].DeleteByID<RoleDao>(Role.Id);
                    tx.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Entities enum val.
        /// </summary>
        /// <param name="Access">The access.</param>
        /// <returns>int</returns>
        public int EntityEnumVal(string Access)
        {
            int sum = 0;
            string[] words = Access.Split(',');
            foreach (string word in words)
            {
                if (word == "All")
                    sum = 0;
                else if (word == "View")
                    sum = sum + (int)OperationId.View;
                else if (word == "Edit")
                    sum = sum + (int)OperationId.Edit;
                else if (word == "Delete")
                    sum = sum + (int)OperationId.Delete;
                else if (word == "Create")
                    sum = sum + (int)OperationId.Create;
                else if (word == "Self")
                    sum = sum + (int)OperationId.Self;
                else if (word == "Simulate")
                    sum = sum + (int)OperationId.Simulate;
                else if (word == "Allow")
                    sum = sum + (int)OperationId.Allow;
            }
            return sum;
        }

        /// <summary>
        /// Entities enum val.
        /// </summary>
        /// <param name="Access">The access.</param>
        /// <returns>int</returns>
        public string EntityEnumCaption(int Access)
        {
            string sum = "";
            if (Access == 0)
                sum = "All";
            else if (Access == 1)
                sum = "View";
            else if (Access == 2)
                sum = "Edit";
            else if (Access == 4)
                sum = "Delete";
            else if (Access == 8)
                sum = "Create";
            else if (Access == 16)
                sum = "Self";
            else if (Access == 32)
                sum = "Simulate";
            else if (Access == 64)
                sum = "Allow";

            return sum;
        }

        /// <summary>
        /// Saves the update entity ACL.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="RoleId">The role id.</param>
        /// <param name="ModuleId">The module id.</param>
        /// <param name="EntityTypeId">The entity type id.</param>
        /// <param name="FeatureId">The feature id.</param>
        /// <param name="AccessRights">The access rights.</param>
        /// <returns>int</returns>
        public int SaveUpdateEntityACL(AccessManagerProxy proxy, int RoleId, int ModuleId, int EntityTypeId, int FeatureId, string AccessRights)
        {
            IEntityAcl EntityAcl = new EntityAcl();
            try
            {
                int AccessPermission = 0;
                AccessPermission = EntityEnumVal(AccessRights);
                EntityAclDao EntityAclDao = new EntityAclDao();
                //EntityAclDao.Id = EntityAclDao.Id;
                EntityAclDao.Roleid = RoleId;
                EntityAclDao.EntityTypeid = EntityTypeId;
                EntityAclDao.Moduleid = ModuleId;
                EntityAclDao.Featureid = FeatureId;
                EntityAclDao.AccessPermission = AccessPermission;
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Save<EntityAclDao>(EntityAclDao);
                    tx.Commit();
                }
                EntityAcl.Roleid = RoleId;
                EntityAclDao.Id = EntityAclDao.Id;
                return EntityAcl.Id;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// get the global access.
        /// </summary>
        /// <param name="caption">The GlobalRoleid.</param>
        /// <param name="description">The Moduleid.</param>
        /// <param name="roleid">The EntityTypeid.</param>
        /// /// <param name="description">The Featureid.</param>
        /// <param name="roleid">The AccessPermission.</param>
        /// <returns>Ilist of IGlobalAcl</returns>
        public IList<object> GetGlobalAcl(AccessManagerProxy proxy, int roleID)
        {
            try
            {
                IList<object> result = new List<object>();
                //IList<IGlobalAcl> _iiGlobalAcl = new List<IGlobalAcl>();
                //IList<GlobalAclDao> _GlobalAclDao = new List<GlobalAclDao>();
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    var distinctroles = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery("SELECT aga.GlobalRoleID,aga.ModuleID,aga.FeatureID FROM AM_GlobalAcl aga WHERE aga.GlobalRoleID = " + roleID + " GROUP BY aga.GlobalRoleID ,aga.ModuleID,aga.FeatureID");
                    foreach (var item in distinctroles)
                    {
                        IList roles = null;
                        IList roleaccess = null;
                        StringBuilder getGlobalAcl = new StringBuilder();
                        //getGlobalAcl.Append(" SELECT agr.Caption AS GlobalRoleCaption,mm.Caption AS ModuleCaption,met.Caption AS EntityTypeCaption,mf.Caption AS FeatureCaption, aga.AccessPermission As AccessPermission,aga.GlobalRoleID,aga.ModuleID,aga.EntityTypeID,aga.FeatureID, aga.ID FROM   AM_GlobalAcl aga INNER JOIN AM_GlobalRole agr ON  agr.ID = aga.GlobalRoleID INNER JOIN MM_Module mm ON  mm.ID = aga.ModuleID INNER JOIN MM_EntityType met ON  met.ID = aga.EntityTypeID INNER JOIN MM_Feature mf ON  mf.ID = aga.FeatureID ");
                        getGlobalAcl.Append("SELECT ");
                        getGlobalAcl.Append("ISNULL(mm.Caption,'All')            AS ModuleCaption, ");
                        getGlobalAcl.Append("ISNULL(met.Caption ,'All')          AS EntityTypeCaption, ");
                        getGlobalAcl.Append("ISNULL(mf.Caption ,'All')           AS FeatureCaption, ");
                        getGlobalAcl.Append("AccessPermission   AS AccessPermission, ");
                        getGlobalAcl.Append("aga.GlobalRoleID, ");
                        getGlobalAcl.Append("aga.ModuleID, ");
                        getGlobalAcl.Append("aga.EntityTypeID, ");
                        getGlobalAcl.Append("aga.FeatureID, ");
                        getGlobalAcl.Append("aga.ID ");
                        getGlobalAcl.Append("FROM   AM_GlobalAcl aga ");
                        getGlobalAcl.Append("LEFT OUTER JOIN AM_GlobalRole agr ");
                        getGlobalAcl.Append("ON  agr.ID = aga.GlobalRoleID ");
                        getGlobalAcl.Append("LEFT OUTER JOIN MM_Module mm ");
                        getGlobalAcl.Append("ON  mm.ID = aga.ModuleID ");
                        getGlobalAcl.Append("LEFT OUTER JOIN MM_EntityType met ");
                        getGlobalAcl.Append("ON  met.ID = aga.EntityTypeID ");
                        getGlobalAcl.Append("LEFT OUTER JOIN MM_Feature mf ");
                        getGlobalAcl.Append("ON  mf.ID = aga.FeatureID");
                        getGlobalAcl.Append(" WHERE aga.GlobalRoleID= ? AND aga.ModuleID= ? AND aga.FeatureID= ?");

                        roles = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].ExecuteQuerywithMinParam(getGlobalAcl.ToString(), (int)((System.Collections.Hashtable)(item))["GlobalRoleID"], (int)((System.Collections.Hashtable)(item))["ModuleID"], (int)((System.Collections.Hashtable)(item))["FeatureID"]);
                        StringBuilder access = new StringBuilder();
                        StringBuilder ids = new StringBuilder();
                        int i = 1;
                        string AccessPermission = "";

                        for (int m = 0; m < roles.Count; m++)
                        {
                            var item2 = roles[m];
                            AccessPermission = EntityEnumCaption((int)((System.Collections.Hashtable)(item2))["AccessPermission"]);
                            ids.Append((int)((System.Collections.Hashtable)(item2))["ID"]);
                            access.Append(AccessPermission);
                            if (roles.Count > i)
                            {
                                access.Append(", ");
                                ids.Append(",");
                            }
                            i++;
                        }

                        int l = 1;
                        int rolescount = roles.Count;
                        for (int m = 0; m < rolescount; m++)
                        {
                            if (rolescount == l)
                            {

                            }
                            else
                            {
                                roles.RemoveAt(roles.Count - 1);
                                l++;
                            }
                        }


                        //int rolescount = roles.Count;
                        //if (roles.Count > 1)
                        //{
                        //    for (int j = 0; j < rolescount; j++)
                        //    {
                        //        if (j < roles.Count)
                        //        {
                        //            roles.RemoveAt(j);
                        //        }
                        //    }

                        //}
                        foreach (var itemss in roles)
                        {
                            ((System.Collections.Hashtable)(itemss))["AccessPermission"] = access.ToString();
                            ((System.Collections.Hashtable)(itemss))["ID"] = ids.ToString();
                        }
                        foreach (var item123 in roles)
                        {
                            result.Add(item123);
                        }
                        //result.Add(roles);
                    }

                    return result;
                }

            }
            catch { return null; }
        }

        public bool CheckGlobalAccessAvaiilability(AccessManagerProxy proxy, int GlobalRoleID, int ModuleID, int FeatureID)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {

                    IList<GlobalAclDao> globalrole;
                    IList<MultiProperty> prplst = new List<MultiProperty>();
                    prplst.Add(new MultiProperty { propertyName = GlobalAclDao.PropertyNames.GlobalRoleid, propertyValue = GlobalRoleID });
                    prplst.Add(new MultiProperty { propertyName = GlobalAclDao.PropertyNames.Moduleid, propertyValue = ModuleID });
                    prplst.Add(new MultiProperty { propertyName = GlobalAclDao.PropertyNames.Featureid, propertyValue = FeatureID });
                    globalrole = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].GetEquals<GlobalAclDao>(prplst);
                    if (globalrole.Count() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {

                return true;
            }

        }

        /// <summary>
        /// Saves/Update global ACL.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="GlobalRoleId">The global role id.</param>
        /// <param name="ModuleId">The module id.</param>
        /// <param name="EntityTypeId">The entity type id.</param>
        /// <param name="FeatureId">The feature id.</param>
        /// <param name="AccessRights">The access rights.</param>
        /// <returns>IGlobalAcl</returns>
        public bool SaveUpdateGlobalACL(AccessManagerProxy proxy, int GlobalRoleId, int ModuleId, int EntityTypeId, int FeatureId, string AccessRights, int ID = 0)
        {
            IGlobalAcl GlobalAcl = new GlobalAcl();
            try
            {

                int AccessPermission = 0;
                AccessPermission = EntityEnumVal(AccessRights);
                GlobalAclDao GlobalAclDao = new GlobalAclDao();
                GlobalAclDao.Id = ID;
                GlobalAclDao.GlobalRoleid = GlobalRoleId;
                GlobalAclDao.Moduleid = ModuleId;
                GlobalAclDao.EntityTypeid = EntityTypeId;
                GlobalAclDao.Featureid = FeatureId;
                GlobalAclDao.AccessPermission = true;
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Save<GlobalAclDao>(GlobalAclDao);
                    tx.Commit();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteOldGlobalAccess(AccessManagerProxy proxy, int GlobalRoleID, int ModuleID, int FeatureID)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].ExecuteQuerywithMinParam("delete from AM_GlobalAcl where GlobalRoleID = ? and ModuleID=  ? and FeatureID =  ? ", GlobalRoleID, ModuleID, FeatureID);
                    tx.Commit();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Delete global ACL.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="ID">ID</param>
        /// <returns>IGlobalAcl</returns>
        public bool DeleteGlobalACL(AccessManagerProxy proxy, int ID)
        {
            IGlobalAcl GlobalAcl = new GlobalAcl();
            try
            {

                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].DeleteByID<GlobalAclDao>(GlobalAclDao.PropertyNames.Id, ID);
                    tx.Commit();
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        //GetGlobalRole
        /// <summary>
        /// get the global role.
        /// </summary>
        /// <param name="caption">The caption.</param>
        /// <param name="description">The description.</param>
        /// <param name="roleid">The roleid.</param>
        /// <returns>Ilist of GlobalRole</returns>
        public IList<IGlobalRole> GetGlobalRole(AccessManagerProxy proxy)
        {
            try
            {
                IList<IGlobalRole> _iiGlobalRole = new List<IGlobalRole>();
                IList<GlobalRoleDao> _GlobalRoleDao = new List<GlobalRoleDao>();
                var userglobal = 0;

                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    _GlobalRoleDao = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].GetAll<GlobalRoleDao>();
                    userglobal = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Query<GlobalRoleUserDao>().Where(a => a.Userid == proxy.MarcomManager.User.Id).Select(a => a.GlobalRoleId).Min();

                    if (userglobal != -1) // if Person Not super admin should not display the super admin option
                    {
                        int[] SuperAdminId = { -1 };
                        var result = _GlobalRoleDao.Where(at => !SuperAdminId.Contains(at.Id) && at.Id != -2);

                        foreach (var item in result)
                        {
                            IGlobalRole _iglobalrole = new GlobalRole();
                            _iglobalrole.Id = item.Id;
                            _iglobalrole.Caption = item.Caption;
                            _iglobalrole.Description = item.Description;
                            _iglobalrole.IsAssetAccess = item.IsAssetAccess;
                            _iiGlobalRole.Add(_iglobalrole);
                        }
                    }
                    if (userglobal == -1)
                    {
                        foreach (var item in _GlobalRoleDao)
                        {
                            IGlobalRole _iglobalrole = new GlobalRole();
                            _iglobalrole.Id = item.Id;
                            _iglobalrole.Caption = item.Caption;
                            _iglobalrole.Description = item.Description;
                            _iglobalrole.IsAssetAccess = item.IsAssetAccess;
                            _iiGlobalRole.Add(_iglobalrole);
                        }
                    }
                }
                return _iiGlobalRole;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //GetGlobalRole By ID
        /// <summary>
        /// get the global role by id.
        /// </summary>
        /// <param name="caption">The caption.</param>
        /// <param name="description">The description.</param>
        /// <param name="roleid">The roleid.</param>
        /// <returns>Ilist of GlobalRole</returns>
        public IList<IGlobalRole> GetGlobalRoleByID(AccessManagerProxy proxy, int ID)
        {
            try
            {
                GlobalRoleDao GlobalRoleDao = new GlobalRoleDao();
                GlobalRoleDao.Id = ID;
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    GlobalRoleDao = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Get<GlobalRoleDao>(GlobalRoleDao);
                }
                IList<IGlobalRole> _iiglobalrole = new List<IGlobalRole>();
                IGlobalRole _iGlobalRole = new GlobalRole();
                _iGlobalRole.Id = GlobalRoleDao.Id;
                _iGlobalRole.Caption = GlobalRoleDao.Caption;
                _iGlobalRole.Description = GlobalRoleDao.Description;
                _iiglobalrole.Add(_iGlobalRole);
                return _iiglobalrole;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        //insert/update global role 
        /// <summary>
        /// Inserts/Update global role.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="caption">The caption.</param>
        /// <param name="description">The description.</param>
        /// <param name="roleid">The roleid.</param>
        /// <returns>int</returns>
        public bool InsertUpdateGlobalRole(AccessManagerProxy proxy, string caption, string description, int ID = 0)
        {
            try
            {
                GlobalRoleDao dao = new GlobalRoleDao();
                dao.Caption = caption;
                dao.Description = description;
                if (ID != 0)
                {
                    dao.Id = ID;
                }
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Save<GlobalRoleDao>(dao);
                    tx.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes the global role.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="roleid">The roleid.</param>
        /// <returns>string</returns>
        public int DeleteGlobalRole(AccessManagerProxy proxy, int roleid)
        {
            try
            {
                GlobalRoleDao dao = new GlobalRoleDao();
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    var ischeckGlobalAccess = (from item in tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Query<GlobalAclDao>() where item.GlobalRoleid == roleid select item);
                    if (ischeckGlobalAccess.Count() > 0)
                        return 2;

                    //dao = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Get<GlobalRoleDao>(roleid);
                    //tx.Commit();
                }
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].DeleteByID<GlobalRoleDao>(GlobalRoleDao.PropertyNames.Id, roleid);
                    tx.Commit();
                }
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Inserts the global role user.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="globalroleid">The globalroleid.</param>
        /// <param name="userid">The userid.</param>
        /// <returns>int</returns>
        public int InsertGlobalRoleUser(AccessManagerProxy proxy, int globalroleid, int userid)
        {
            //return null;
            try
            {
                if ((CheckAccess(proxy, Modules.Admin, 4, FeatureID.ListView, OperationId.Self)) == true)
                {
                    GlobalRoleUserDao dao = new GlobalRoleUserDao();
                    dao.GlobalRoleId = globalroleid;
                    dao.Userid = userid;
                    using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                    {
                        tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Save<GlobalRoleUserDao>(dao);
                        tx.Commit();
                    }

                    return dao.Id;
                }
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Delete the global role user.
        /// </summary>
        /// <param name="userid">The userid.</param>
        /// <returns>bool</returns>
        public bool DeleteGlobalRoleUser(AccessManagerProxy proxy, int userid)
        {
            //return null;
            try
            {
                if ((CheckAccess(proxy, Modules.Admin, 4, FeatureID.ListView, OperationId.Self)) == true)
                {

                    using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                    {
                        tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].DeleteByID<GlobalRoleUserDao>(GlobalRoleUserDao.PropertyNames.Userid, userid);
                        tx.Commit();
                        return true;
                    }


                }

            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }



        /// <summary>
        /// Get the global role user By UserID.
        /// </summary>
        /// <param name="userid">The userid.</param>
        /// <returns>List<int></returns>
        public int[] GetGlobalRoleUserByID(AccessManagerProxy proxy, int userid)
        {
            //return null;
            try
            {
                if ((CheckAccess(proxy, Modules.Admin, 4, FeatureID.ListView, OperationId.Self)) == true)
                {

                    using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                    {
                        var GlobalRolelist = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Query<GlobalRoleUserDao>();
                        var roleList = from t in GlobalRolelist where t.Userid == userid select t;
                        int[] GlobalRoleId = new int[roleList.ToList().Count()];
                        for (var i = 0; i < roleList.ToList().Count(); i++)
                        {
                            GlobalRoleId[i] = roleList.ToList().ElementAt(i).GlobalRoleId;
                        }
                        tx.Commit();
                        return GlobalRoleId;

                    }


                }

            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }



        public bool CheckUserIsAdmin(AccessManagerProxy proxy)
        {
            //return null;
            try
            {

                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    IList<MultiProperty> prpList = new List<MultiProperty>();
                    prpList.Add(new MultiProperty { propertyName = GlobalRoleUserDao.PropertyNames.Userid, propertyValue = proxy.MarcomManager.User.Id });
                    prpList.Add(new MultiProperty { propertyName = GlobalRoleUserDao.PropertyNames.GlobalRoleId, propertyValue = 1 });
                    var GlobalRolelist = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].GetEquals<GlobalRoleUserDao>(prpList);



                    tx.Commit();
                    if (GlobalRolelist.ToList().Count() > 0)
                        return true;

                }




            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }


        // Check access for a global type
        /// <summary>
        /// Checks the access.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="moduleId">The module id.</param>
        /// <param name="EntitytypeID">The entitytype ID.</param>
        /// <param name="featureId">The feature id.</param>
        /// <param name="AccessPermissionId">The access permission id.</param>
        /// <returns>bool</returns>
        public bool CheckAccess(AccessManagerProxy proxy, Modules moduleId, int EntitytypeID, FeatureID featureId, OperationId AccessPermissionId)
        {
            try
            {
                IList<GlobalRoleUserDao> globalRoleUser;
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    globalRoleUser = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].GetEquals<GlobalRoleUserDao>(GlobalRoleUserDao.PropertyNames.Userid, proxy.MarcomManager.User.Id);
                    IList<GlobalAclDao> globalAcl;
                    globalAcl = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].GetAll<GlobalAclDao>();
                    var newx = from newt in globalRoleUser.ToList()
                               join newz in globalAcl on newt.GlobalRoleId equals newz.GlobalRoleid into leftjoin
                               from fnresult in leftjoin.DefaultIfEmpty()
                               select fnresult;
                    //foreach (var sval in newx.ToList())
                    //{
                    //    if (sval.Moduleid == (int)moduleId && sval.EntityTypeid == EntitytypeID && (sval.Featureid == (int)featureId) && ((sval.AccessPermission & (int)AccessPermissionId) != 0))
                    //    {
                    //        return true;
                    //    }
                    //}

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {

                return true;

            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="proxy"></param>
        /// <param name="moduleId"></param>
        /// <param name="featureId"></param>
        /// <param name="AccessPermissionId"></param>
        /// <returns></returns>
        public bool CheckUserAccess(AccessManagerProxy proxy, Modules moduleId, FeatureID featureId, OperationId AccessPermissionId)
        {
            try
            {
                bool stat = proxy.MarcomManager.User.ListOfUserGlobalRoles.Any(tt => tt.Moduleid == (int)moduleId && tt.Featureid == (int)featureId && (tt.AccessPermission == true));
                return stat;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="proxy"></param>
        /// <param name="moduleId"></param>
        /// <param name="featureId"></param>
        /// <returns></returns>
        public bool CheckUserAccess(AccessManagerProxy proxy, Modules moduleId, FeatureID featureId)
        {
            try
            {
                bool stat = proxy.MarcomManager.User.ListOfUserGlobalRoles.Any(tt => tt.Moduleid == (int)moduleId && tt.Featureid == (int)featureId && tt.AccessPermission == true);
                return stat;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public void GetApplicationLevelSettings(AccessManagerProxy metadataManagerProxy)
        {
            //IMarcomManager metadataManagerProxy = MarcomManagerFactory.GetMarcomManager(GetSystemSession());
            using (ITransaction tx = metadataManagerProxy.MarcomManager.GetTransaction(metadataManagerProxy.MarcomManager.User.TenantID))
            {
                string qry = "select mm.ID as 'moduleid',mm.Caption as 'modulecaption',mf.Caption as 'featurecaption',mf.ID as 'featureid' from MM_Module mm inner join MM_Feature mf on  mm.ID=mf.ModuleID where mm.IsEnable=1 and mf.IsEnable=1";
                var result = tx.PersistenceManager.MetadataRepository[metadataManagerProxy.MarcomManager.User.TenantID].ExecuteQuery(qry).Cast<Hashtable>().ToList();
                MarcomManagerFactory.ListOfModuleFeature = new List<ModuleFeature>();
                foreach (var c in result)
                {
                    ModuleFeature mf = new ModuleFeature();
                    mf.Featureid = Convert.ToInt32(c["featureid"]);
                    mf.ModuleId = Convert.ToInt32(c["moduleid"]);
                    mf.ModuleCaption = Convert.ToString(c["modulecaption"]);
                    mf.FeatureCaption = Convert.ToString(c["featurecaption"]);
                    MarcomManagerFactory.ListOfModuleFeature.Add(mf);
                }
            }
        }

        // Try access for a global type
        /// <summary>
        /// Tries the access.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="moduleId">The module id.</param>
        /// <param name="EntitytypeID">The entitytype ID.</param>
        /// <param name="featureId">The feature id.</param>
        /// <param name="AccessPermissionId">The access permission id.</param>
        /// <exception cref="System.Exception">Invalid operation</exception>
        public void TryAccess(AccessManagerProxy proxy, Modules moduleId, int EntitytypeID, FeatureID featureId, OperationId AccessPermissionId)
        {
            if (CheckAccess(proxy, moduleId, EntitytypeID, featureId, AccessPermissionId) == false)
            {
                throw new Exception("Invalid operation");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="proxy"></param>
        /// <param name="moduleId"></param>
        /// <param name="featureId"></param>
        /// <param name="AccessPermissionId"></param>
        public void TryAccess(AccessManagerProxy proxy, Modules moduleId, FeatureID featureId, OperationId AccessPermissionId)
        {
            if (CheckUserAccess(proxy, moduleId, featureId, AccessPermissionId) == false)
            {
                throw new MarcomAccessDeniedException("Access denied");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="proxy"></param>
        /// <param name="moduleId"></param>
        /// <param name="featureId"></param>
        public void TryAccess(AccessManagerProxy proxy, Modules moduleId, FeatureID featureId)
        {
            if (CheckUserAccess(proxy, moduleId, featureId) == false)
            {
                throw new MarcomAccessDeniedException("Access denied");
            }
        }

        // Check access for a local type
        /// <summary>
        /// Checks the access.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="moduleId">The module id.</param>
        /// <param name="EntitytypeID">The entitytype ID.</param>
        /// <param name="featureId">The feature id.</param>
        /// <param name="AccessPermissionId">The access permission id.</param>
        /// <param name="EntityId">The entity id.</param>
        /// <returns>bool</returns>
        public bool CheckAccess(AccessManagerProxy proxy, Modules moduleId, int EntitytypeID, FeatureID featureId, OperationId AccessPermissionId, int EntityId)
        {
            try
            {
                IList<EntityRoleUserDao> entityRoleUser;
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    IList<MultiProperty> prplst = new List<MultiProperty>();
                    prplst.Add(new MultiProperty { propertyName = EntityRoleUserDao.PropertyNames.Entityid, propertyValue = EntityId });
                    prplst.Add(new MultiProperty { propertyName = EntityRoleUserDao.PropertyNames.Userid, propertyValue = proxy.MarcomManager.User.Id });
                    entityRoleUser = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].GetEquals<EntityRoleUserDao>(prplst);
                    IList<EntityAclDao> entityAcl;
                    entityAcl = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].GetAll<EntityAclDao>();
                    var newx = from newt in entityRoleUser.ToList()
                               join newz in entityAcl on newt.Roleid equals newz.Roleid into leftjoin
                               from fnresult in leftjoin.DefaultIfEmpty()
                               select fnresult;
                    foreach (var sval in newx.ToList())
                    {
                        if (sval.Moduleid == (int)moduleId && sval.EntityTypeid == EntitytypeID && ((sval.Featureid & (int)featureId) != 0) && ((sval.AccessPermission & (int)AccessPermissionId) != 0))
                        {
                            return true;
                        }
                    }
                }
            }
            catch
            {
                return false;
            }

            return false;
        }

        // Try access for a local type
        /// <summary>
        /// Tries the access.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        /// <param name="moduleId">The module id.</param>
        /// <param name="EntitytypeID">The entitytype ID.</param>
        /// <param name="featureId">The feature id.</param>
        /// <param name="AccessPermissionId">The access permission id.</param>
        /// <param name="EntityId">The entity id.</param>
        /// <exception cref="System.Exception">Invalid operation</exception>
        public void TryAccess(AccessManagerProxy proxy, Modules moduleId, int EntitytypeID, FeatureID featureId, OperationId AccessPermissionId, int EntityId)
        {
            if (CheckAccess(proxy, moduleId, EntitytypeID, featureId, AccessPermissionId, EntityId) == false)
            {
                throw new Exception("Invalid operation");
            }
        }

        public bool InsertUpdateGlobalEntitTypeACL(AccessManagerProxy proxy, JArray entitytypeAccessObj)
        {
            try
            {
                bool Isdeleted = false;
                IList<GlobalEntityTypeAccessDao> iiglobalentitytypeaccess = new List<GlobalEntityTypeAccessDao>();
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    int globalroleid = 0, moduleid = 0;
                    foreach (var obj in entitytypeAccessObj)
                    {
                        if (!Isdeleted)
                        {
                            globalroleid = (int)obj["GlobalRoleID"];
                            moduleid = (int)obj["ModuleID"];
                            tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].ExecuteQuerywithMinParam("delete from AM_GlobalEntityTypeAcl where GlobalRoleID = ? and ModuleID=  ? ", globalroleid, moduleid);
                            Isdeleted = true;
                        }
                        if ((bool)obj["AccessPermission"])
                        {
                            GlobalEntityTypeAccessDao GlobalAclDao = new GlobalEntityTypeAccessDao();
                            GlobalAclDao.Id = 0;
                            GlobalAclDao.GlobalRoleid = (int)obj["GlobalRoleID"];
                            GlobalAclDao.Moduleid = (int)obj["ModuleID"];
                            GlobalAclDao.EntityTypeid = (int)obj["EntityTypeID"];
                            GlobalAclDao.AccessPermission = (bool)obj["AccessPermission"];
                            iiglobalentitytypeaccess.Add(GlobalAclDao);
                        }
                    }

                    tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Save<GlobalEntityTypeAccessDao>(iiglobalentitytypeaccess);
                    tx.Commit();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public IList<object> GetGlobalEntityTypeAcl(AccessManagerProxy proxy, int roleID, int moduleID = 3)
        {
            try
            {
                IList<object> result = new List<object>();
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {

                    IList<IEntityType> ientitytype = new List<IEntityType>();
                    ientitytype = proxy.MarcomManager.MetadataManager.GetEntityTypeIsAssociate();

                    string entitytypeIDs = "";

                    foreach (var obj in ientitytype)
                    {
                        entitytypeIDs += obj.Id.ToString() + ",";
                    }

                    IList roles = null;
                    StringBuilder getGlobalAcl = new StringBuilder();

                    getGlobalAcl.Append("SELECT tempentitytype.Caption, ISNULL(ageta.AccessPermission,0)AccessPermission, ");
                    getGlobalAcl.Append("ISNULL(ageta.GlobalRoleID, " + roleID + ")GlobalRoleID, ISNULL(ageta.ModuleID, " + moduleID + ")ModuleID, tempentitytype.ID AS EntityTypeID, ");
                    getGlobalAcl.Append("tempentitytype.ShortDescription, tempentitytype.ColorCode, ISNULL(ageta.ID,0)ID ");
                    getGlobalAcl.Append("FROM ( ");
                    getGlobalAcl.Append("SELECT * FROM MM_EntityType met ");
                    getGlobalAcl.Append("WHERE met.ID IN (" + entitytypeIDs.TrimEnd(',') + "))tempentitytype ");
                    getGlobalAcl.Append("LEFT JOIN  AM_GlobalEntityTypeAcl ageta ");
                    getGlobalAcl.Append("ON ageta.EntityTypeID = tempentitytype.ID ");

                    getGlobalAcl.Append("AND ageta.ModuleID = " + moduleID + " AND ageta.GlobalRoleID = " + roleID + " ");
                    getGlobalAcl.Append("LEFT JOIN AM_GlobalRole agr ON ageta.GlobalRoleID = agr.ID ");
                    getGlobalAcl.Append("LEFT JOIN MM_Module mm ON ageta.ModuleID = mm.ID ");

                    roles = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].ExecuteQuerywithMinParam(getGlobalAcl.ToString());

                    foreach (var item123 in roles)
                    {
                        result.Add(item123);
                    }

                    return result;
                }

            }
            catch { return null; }
        }

        public bool DeleteGlobalEntityTypeACL(AccessManagerProxy proxy, int ID)
        {
            IGlobalAcl GlobalAcl = new GlobalAcl();
            try
            {

                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].DeleteByID<GlobalEntityTypeAccessDao>(GlobalEntityTypeAccessDao.PropertyNames.Id, ID);
                    tx.Commit();
                    return true;
                }
            }
            catch
            {
            }
            return false;
        }

        /// <summary>
        /// get the role.
        /// </summary>
        /// <returns>Ilist of IRole</returns>
        public IList<IRole> GetAllEntityRole(AccessManagerProxy proxy)
        {
            try
            {
                IList<IRole> irole = new List<IRole>();
                IList<RoleDao> roledao = new List<RoleDao>();
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    roledao = tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].Query<RoleDao>().ToList();
                    int[] EntityRoleIds = { (int)EntityRoles.Owner, (int)EntityRoles.Editer, (int)EntityRoles.Viewer, (int)EntityRoles.ExternalParticipate, (int)EntityRoles.BudgerApprover, (int)EntityRoles.ProofInitiator, (int)EntityRoles.Corporate };

                    var result = roledao.Where(at => EntityRoleIds.Contains(at.Id));
                    foreach (var item in result)
                    {
                        IRole role = new Role();
                        role.Id = item.Id;
                        role.Caption = item.Caption;
                        role.Description = item.Description;
                        irole.Add(role);
                    }
                }
                return irole;
            }
            catch
            {

            }
            return null;
        }

        public IList<IEntityTypeRoleAcl> GetEntityTypeRoleAccess(AccessManagerProxy proxy, int EntityTypeID)
        {
            try
            {

                IList<IEntityTypeRoleAcl> _iientityTyperoleobj = new List<IEntityTypeRoleAcl>();
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    IList<EntityTypeRoleAclDao> dao = new List<EntityTypeRoleAclDao>();
                    int[] FilterIds = { (int)EntityRoles.Owner, (int)EntityRoles.BudgerApprover };
                    var entityroleobj = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Query<EntityTypeRoleAclDao>().Where(a => a.EntityTypeID == EntityTypeID && !FilterIds.Contains(a.EntityRoleID)).OrderBy(a => a.Sortorder);
                    foreach (var item in entityroleobj)
                    {
                        EntityTypeRoleAcl entitytypeRoleAclObj = new EntityTypeRoleAcl();
                        entitytypeRoleAclObj.ID = item.ID;
                        entitytypeRoleAclObj.EntityTypeID = item.EntityTypeID;
                        entitytypeRoleAclObj.EntityRoleID = item.EntityRoleID;
                        entitytypeRoleAclObj.ModuleID = item.ModuleID;
                        entitytypeRoleAclObj.Sortorder = item.Sortorder;
                        entitytypeRoleAclObj.Caption = item.Caption;
                        _iientityTyperoleobj.Add(entitytypeRoleAclObj);
                    }
                }
                return _iientityTyperoleobj;

            }
            catch
            {


            }
            return null;
        }

        /// <summary>
        /// CHECK THE ENTITY TYPE ACCESS
        /// </summary>
        /// <param name="moduleID"></param>
        /// <param name="EntityTypeID"></param>
        public void TryEntityTypeAccess(AccessManagerProxy proxy, Modules moduleID, int EntityTypeID)
        {
            if (!CheckEntityTypeAccessForRootEntityType(proxy, moduleID, EntityTypeID))
            {
                throw new MarcomAccessDeniedException("Access denied");
            }
        }

        /// <summary>
        /// ENTITY TYPE ACCESS
        /// </summary>
        /// <param name="moduleId"></param>
        /// <param name="EntitytypeID"></param>
        /// <returns></returns>
        public bool CheckEntityTypeAccessForRootEntityType(AccessManagerProxy proxy, Modules moduleId, int EntitytypeID)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    StringBuilder str = new StringBuilder();

                    str.Append("SELECT COUNT(*) AS AccessExist ");
                    str.Append("FROM   AM_GlobalEntityTypeAcl ageta INNER JOIN MM_Module mm ON  mm.ID = ageta.ModuleID  ");
                    str.Append("AND mm.IsEnable = 1 WHERE mm.ID = " + (int)moduleId + " AND ageta.EntityTypeID = " + EntitytypeID + "  AND  ageta.GlobalRoleID IN (SELECT GlobalRoleID FROM  ");
                    str.Append("AM_GlobalRole_User WHERE  UserId = " + proxy.MarcomManager.User.Id + ") ");

                    var result = tx.PersistenceManager.MetadataRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery(str.ToString()).Cast<Hashtable>().ToList().FirstOrDefault();
                    tx.Commit();

                    if (result["AccessExist"] != null && (int)result["AccessExist"] > 0)
                        return true;


                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// ENTITY TYPE AND ENTITY ACCESS
        /// </summary>
        /// <param name="moduleID"></param>
        /// <param name="parententityID"></param>
        /// <param name="EntityTypeID"></param>
        public void TryEntityTypeAccess(AccessManagerProxy proxy, Modules moduleID, int parententityID, int EntityTypeID)
        {
            if (!CheckEntityTypeAndEntityRoleAccess(proxy, moduleID, parententityID, EntityTypeID))
            {
                throw new MarcomAccessDeniedException("Access denied");
            }
        }

        public bool CheckEntityTypeAndEntityRoleAccess(AccessManagerProxy proxy, Modules moduleId, int parententityID, int EntitytypeID)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    StringBuilder str = new StringBuilder();

                    str.Append(" SELECT MIN(aetra.EntityRoleID) as Permission  FROM AM_Entity_Role_User aeru ");
                    str.Append("INNER JOIN AM_EntityTypeRoleAcl aetra ON aeru.RoleID=aetra.ID ");
                    str.Append("WHERE aeru.EntityID= " + parententityID + " AND aeru.UserID = " + proxy.MarcomManager.User.Id + " ");

                    var res = tx.PersistenceManager.MetadataRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery(str.ToString()).Cast<Hashtable>().ToList();
                    if (res[0]["Permission"] != null && Convert.ToInt32(res[0]["Permission"]) <= 2)
                        return true;
                    else
                    {
                        str.Clear();
                        str.Append("SELECT COUNT(*) AS AccessExist ");
                        str.Append("FROM   AM_GlobalEntityTypeAcl ageta INNER JOIN MM_Module mm ON  mm.ID = ageta.ModuleID ");
                        str.Append("AND mm.IsEnable = 1 INNER JOIN MM_EntityType met ON  met.ID = ageta.EntityTypeID ");
                        str.Append("WHERE ageta.EntityTypeID = " + EntitytypeID + " AND mm.ID = " + (int)moduleId + "  AND  ageta.GlobalRoleID IN (SELECT GlobalRoleID FROM   AM_GlobalRole_User WHERE  UserId = " + proxy.MarcomManager.User.Id + ") ");

                        var result = tx.PersistenceManager.MetadataRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery(str.ToString()).Cast<Hashtable>().ToList().FirstOrDefault();

                        if ((int)result["AccessExist"] > 0)
                            return true;
                    }
                    tx.Commit();
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// ENTITY ACCESSs
        /// </summary>
        /// <param name="currententityID"></param>
        /// <param name="moduleID"></param>
        public void TryEntityTypeAccess(AccessManagerProxy proxy, int currententityID, Modules moduleID)
        {


            if (!CheckEntityRoleAccess(proxy, moduleID, currententityID))
            {
                throw new MarcomAccessDeniedException("Access denied");
            }

        }

        public bool CheckEntityRoleAccess(AccessManagerProxy proxy, Modules moduleId, int currententityID)
        {
            try
            {
                int isGlobalAdmin = proxy.MarcomManager.User.ListOfUserGlobalRoles.Where(role => role.Id == 1).Count();
                if (isGlobalAdmin > 0)
                    return true;
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    var entitytasktype = tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery("select TaskTypeId from MM_EntityTaskType where EntitytypeId=(SELECT pe.TypeID FROM PM_Entity pe WHERE pe.ID =" + currententityID + ")");
                    var FeatureID = tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery("select ID from MM_Feature where Caption='View/Edit all' and ModuleID =" + (int)moduleId);
                    var SharedEntityAccess_Permission = tx.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery("select AccessPermission from  AM_GlobalAcl where FeatureID=" + ((System.Collections.Hashtable)FeatureID[0])["ID"] + "and AccessPermission = 1 and GlobalRoleID  in (select GlobalRoleId from AM_GlobalRole_User where UserId=" + proxy.MarcomManager.User.Id + ")");
                    if (SharedEntityAccess_Permission != null)
                    {
                        if (SharedEntityAccess_Permission.Count > 0)
                        {
                            return true;
                        }
                        else
                        {
                            StringBuilder str = new StringBuilder();
                            str.Append(" DECLARE @TestVal int  ");
                            str.Append(" SET @TestVal = (SELECT pe.TypeID FROM PM_Entity pe WHERE pe.ID = ?)  ");
                            if (entitytasktype.Count != 0)
                                str.Append(" SET @TestVal = 30  ");
                            str.Append(" SELECT   ");
                            str.Append(" CASE @TestVal  ");
                            str.Append(" WHEN 7 THEN  ");
                            str.Append(" (SELECT 1 AS Permission )    ");// FundingRequestDao request button itself we are hiding are hiding
                            // str.Append(" FROM PM_Task_Members ptm WHERE ptm.TaskID = ? AND ptm.UserID = ?)  ");
                            str.Append(" WHEN 30 THEN  ");
                            str.Append(" (SELECT MIN(ttm.RoleID) as Permission   ");
                            str.Append(" FROM TM_Task_Members ttm WHERE ttm.TaskID = ? AND ttm.UserID = ?)  ");
                            str.Append(" ELSE        ");
                            str.Append(" (SELECT MIN(aetra.EntityRoleID) as Permission   FROM AM_Entity_Role_User aeru  ");
                            str.Append(" INNER JOIN AM_EntityTypeRoleAcl aetra ON aeru.RoleID=aetra.ID   ");
                            str.Append(" WHERE aeru.EntityID= ? AND aeru.UserID = ?)  ");
                            str.Append(" END   AS 'Permission' , @TestVal etype  ");
                            var res = tx.PersistenceManager.MetadataRepository[proxy.MarcomManager.User.TenantID].ExecuteQuerywithMinParam(str.ToString(), currententityID, currententityID, proxy.MarcomManager.User.Id, currententityID, proxy.MarcomManager.User.Id).Cast<Hashtable>().ToList();
                            tx.Commit();
                            if ((int)res[0]["etype"] != 7 && (int)res[0]["etype"] != 30)
                            {
                                if (res[0]["Permission"] != null && Convert.ToInt32(res[0]["Permission"]) <= 2)
                                    return true;
                                else if (res[0]["Permission"] != null && Convert.ToInt32(res[0]["Permission"]) == 10)
                                    return true;
                            }
                            else
                            {
                                if (res[0]["Permission"] != null && Convert.ToInt32(res[0]["Permission"]) <= 4)
                                    return true;
                                else if (res[0]["Permission"] != null && Convert.ToInt32(res[0]["Permission"]) == 10)
                                    return true;
                            }
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public int InsertAssetAccess(AccessManagerProxy proxy, int AssetAccessID, int userid)
        {
            //return null;
            try
            {
                if ((CheckAccess(proxy, Modules.Admin, 4, FeatureID.ListView, OperationId.Self)) == true)
                {
                    AssetAccessRoleDao dao = new AssetAccessRoleDao();
                    dao.AssetAccessID = AssetAccessID;
                    dao.Userid = userid;
                    using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                    {
                        tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Save<AssetAccessRoleDao>(dao);
                        tx.Commit();
                    }

                    return dao.Id;
                }
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int[] GetAssetAccessByID(AccessManagerProxy proxy, int userid)
        {
            //return null;
            try
            {
                if ((CheckAccess(proxy, Modules.Admin, 4, FeatureID.ListView, OperationId.Self)) == true)
                {

                    using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                    {
                        var AssetAccesslist = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Query<AssetAccessRoleDao>();
                        var roleList = from t in AssetAccesslist where t.Userid == userid select t;
                        int[] AssetAccessID = new int[roleList.ToList().Count()];
                        for (var i = 0; i < roleList.ToList().Count(); i++)
                        {
                            AssetAccessID[i] = roleList.ToList().ElementAt(i).AssetAccessID;
                        }


                        tx.Commit();
                        return AssetAccessID;

                    }


                }

            }
            catch (Exception ex)
            {
                return null;
            }
            return null;
        }

        public bool DeleteAssetAccessByID(AccessManagerProxy proxy, int userid)
        {
            //return null;
            try
            {
                if ((CheckAccess(proxy, Modules.Admin, 4, FeatureID.ListView, OperationId.Self)) == true)
                {
                    using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                    {
                        tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].DeleteByID<AssetAccessRoleDao>(AssetAccessRoleDao.PropertyNames.Userid, userid);
                        tx.Commit();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return false;
        }

        public IList<ISuperAdminDetails> GetSuperAdminModule(AccessManagerProxy proxy, int Id)
        {
            try
            {
                IList<ISuperAdminDetails> _iisuperadmin = new List<ISuperAdminDetails>();
                IList<SuperAdminDetailsDao> dao = new List<SuperAdminDetailsDao>();

                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    var res = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Query<SuperGlobalAclDao>().Where(a => a.AccessPermission == true && a.GlobalRoleid == Id);
                    if (res != null)
                    {
                        dao = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].GetAll<SuperAdminDetailsDao>();
                        tx.Commit();
                        foreach (var item in dao)
                        {
                            ISuperAdminDetails _isuperadmin = new BrandSystems.Marcom.Core.Access.SuperAdminDetails();
                            var custmtab = res.Where(a => a.Featureid == item.Id).ToList();
                            bool permission = false;
                            if (custmtab.Count() == 1)
                            {
                                permission = true;

                            }
                            _isuperadmin.IsTopnavigation = item.IsTopNavigation;
                            _isuperadmin.Id = item.Id;
                            _isuperadmin.Name = item.Name;
                            _isuperadmin.MenuID = item.MenuID;
                            _isuperadmin.AccessPermission = permission;
                            _isuperadmin.FeatureID = item.FeatureID;
                            _iisuperadmin.Add(_isuperadmin);

                        }
                    }
                    else
                    {
                        dao = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].GetAll<SuperAdminDetailsDao>();
                        tx.Commit();
                        foreach (var item in dao)
                        {
                            ISuperAdminDetails _isuperadmin = new BrandSystems.Marcom.Core.Access.SuperAdminDetails();

                            _isuperadmin.Id = item.Id;
                            _isuperadmin.Name = item.Name;
                            _isuperadmin.IsTopnavigation = item.IsTopNavigation;
                            _isuperadmin.MenuID = item.MenuID;
                            _iisuperadmin.Add(_isuperadmin);
                        }
                    }
                }
                return _iisuperadmin;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool SaveUpdateSuperAdminRoleFeatures(AccessManagerProxy proxy, int GlobalRoleID, int MenuID, int FeatureID, bool IsChecked, int GlobalAclId)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    if (MenuID > 0)
                    {
                        SuperGlobalAclDao daos = new SuperGlobalAclDao();
                        daos.GlobalRoleid = GlobalRoleID;
                        daos.Menuid = MenuID;
                        daos.Featureid = FeatureID;
                        daos.AccessPermission = IsChecked;

                        tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Save<SuperGlobalAclDao>(daos);
                    }
                    tx.Commit();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public int DeleteGlobalByID(AccessManagerProxy proxy, int roleid, int Menu)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    string result;
                    if (Menu >= 100)
                    {
                        result = "delete from AM_SuperGlobalAcl where GlobalRoleID =" + roleid + "and MenuID >=100";
                        tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery(result);
                    }
                    else
                    {
                        result = "delete from AM_SuperGlobalAcl where GlobalRoleID =" + roleid;
                        tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery(result);
                    }
                    tx.Commit();
                }
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        //---------------- FOR LOADING THE MENUS BASED ON SUPER ADMIN SETTINGS--------------------//
        public IList<ISuperGlobalAcl> GetSuperglobalacl(AccessManagerProxy proxy)
        {
            try
            {

                IList<ISuperGlobalAcl> _iisuperadmin = new List<ISuperGlobalAcl>();
                IList<SuperGlobalAclDao> dao = new List<SuperGlobalAclDao>();
                IList<SuperAdminDetailsDao> daosuper = new List<SuperAdminDetailsDao>();
                ISuperGlobalAcl _isuperadmin = new BrandSystems.Marcom.Core.Access.SuperGlobalAcl();
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    var userglobal = 0;
                    dao = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].GetAll<SuperGlobalAclDao>();
                    daosuper = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].GetAll<SuperAdminDetailsDao>();
                    userglobal = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Query<GlobalRoleUserDao>().Where(a => a.Userid == proxy.MarcomManager.User.Id).Select(a => a.GlobalRoleId).Min();
                    tx.Commit();
                    //---------------USER ROLE IS NOT SUPER AMIN GET ALL DATA WHICH IS SET BY SUPER ADMIN---------------------
                    if (userglobal != -1)
                    {
                        foreach (var item in dao)
                        {
                            _isuperadmin = new BrandSystems.Marcom.Core.Access.SuperGlobalAcl();
                            _isuperadmin.Id = item.Id;
                            _isuperadmin.Menuid = item.Menuid;
                            _isuperadmin.GlobalRoleid = item.GlobalRoleid;
                            _isuperadmin.Featureid = item.Featureid;
                            _isuperadmin.AccessPermission = item.AccessPermission;
                            _iisuperadmin.Add(_isuperadmin);
                        }
                    }
                    //---------------USER ROLE IS SUPER AMIN LOAD ALL THE MENUS---------------------
                    else
                    {
                        foreach (var item in daosuper)
                        {
                            _isuperadmin = new BrandSystems.Marcom.Core.Access.SuperGlobalAcl();
                            _isuperadmin.Id = item.Id;
                            _isuperadmin.Featureid = item.FeatureID;
                            _isuperadmin.AccessPermission = true;
                            _iisuperadmin.Add(_isuperadmin);
                        }
                    }
                }
                return _iisuperadmin;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //Assigning user Razor features
        public Dictionary<string, bool> getUserGlobalRoleList(IMarcomManager marcommanager)
        {
            Dictionary<string, bool> listofglobalaccess = new Dictionary<string, bool>();
            try
            {

                listofglobalaccess.Add("addEntityPermission", marcommanager.AccessManager.CheckUserAccess((int)UserAccessModule.Module.Planning, (int)UserAccessModule.Feature.Plan, (int)UserAccessModule.Operations.Create));
                listofglobalaccess.Add("addDuplicatePermission", marcommanager.AccessManager.CheckUserAccess((int)UserAccessModule.Module.Planning, (int)UserAccessModule.Feature.Duplicate, (int)UserAccessModule.Operations.Create));
                listofglobalaccess.Add("addDeleteEntityPermission", marcommanager.AccessManager.CheckUserAccess((int)UserAccessModule.Module.Planning, (int)UserAccessModule.Feature.Plan, (int)UserAccessModule.Operations.Delete));
                listofglobalaccess.Add("addGanttViewPermission", marcommanager.AccessManager.CheckUserAccess((int)UserAccessModule.Module.Planning, (int)UserAccessModule.Feature.GanttView, (int)UserAccessModule.Operations.Allow));
                listofglobalaccess.Add("addEditAllPermission", marcommanager.AccessManager.CheckUserAccess((int)UserAccessModule.Module.Planning, (int)UserAccessModule.Feature.ViewEditAll, (int)UserAccessModule.Operations.Allow));
                listofglobalaccess.Add("addViewAllPermission", marcommanager.AccessManager.CheckUserAccess((int)UserAccessModule.Module.Planning, (int)UserAccessModule.Feature.ViewAll, (int)UserAccessModule.Operations.Allow));

                return listofglobalaccess;
            }
            catch (Exception ex)
            {
                return listofglobalaccess;
            }
        }

        //  FOR LOADING GLOBAL ROLE USERS BASED ON TASKSTEP ID
        public IList GetGlobalRoleUserByRoleID(AccessManagerProxy proxy, int GlobalRoleId)
        {
            IList selectedroleuserdetails;
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    StringBuilder query = new StringBuilder();
                    query.Append("select roleuser.FirstName as FirstName,roleuser.LastName as LastName,");
                    query.Append("roleuser.ID as Userid,roleuser.UserName as UserName,roleuser.Email as UserEmail,");
                    query.Append("roleuser.Title as Title,roleuser.Designation as Designation,");
                    query.Append("(select Caption from AM_Role where ID=" + GlobalRoleId + ") as Role,");
                    query.Append("(select Id from AM_Role where ID=" + GlobalRoleId + ") as Roleid");
                    query.Append(" from UM_User roleuser where roleuser.ID in");
                    query.Append(" (select UserId from AM_GlobalRole_User where GlobalRoleId=" + GlobalRoleId + ")");
                    selectedroleuserdetails = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].ExecuteQuerywithMinParam(query.ToString());
                    tx.Commit();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return selectedroleuserdetails;
        }
        public bool SaveUpdateNewsfeedRoleFeatures(AccessManagerProxy proxy, int GlobalRoleID, JObject Newsfeedrempdata)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    GlobalRoleAccess_NewsFeedDao dao = new GlobalRoleAccess_NewsFeedDao();
                    dao.GlobalRoleID = GlobalRoleID;
                    dao.NewsFeedGroupID = (int)Newsfeedrempdata["Id"];
                    tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Save<GlobalRoleAccess_NewsFeedDao>(dao);
                    tx.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool DeleteByGlobalRoleID(AccessManagerProxy proxy, int GlobalRoleID)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    IList<GlobalRoleAccess_NewsFeedDao> roledao = tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].GetAll<GlobalRoleAccess_NewsFeedDao>();
                    if (roledao.Count > 0)
                    {
                        tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].DeleteByGlobalRoleID<GlobalRoleAccess_NewsFeedDao>(GlobalRoleID);
                        tx.Commit();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool DeleteByGlobalRoleID_Notification(AccessManagerProxy proxy, int GlobalRoleID)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    IList<GlobalRoleAccess_NotificationDao> roledao = tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].GetAll<GlobalRoleAccess_NotificationDao>();
                    if (roledao.Count > 0)
                    {
                        tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].DeleteByGlobalRoleID<GlobalRoleAccess_NotificationDao>(GlobalRoleID);
                        tx.Commit();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool SaveUpdateNotificationRoleFeatures(AccessManagerProxy proxy, int GlobalRoleID, JObject Notificationdata)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    GlobalRoleAccess_NotificationDao dao = new GlobalRoleAccess_NotificationDao();
                    dao.GlobalRoleID = GlobalRoleID;
                    dao.NotificationTempID = (int)Notificationdata["Id"];
                    tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Save<GlobalRoleAccess_NotificationDao>(dao);
                    tx.Commit();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public IList<GlobalRoleAccess_NewsFeedDao> GetSelectedFeedFilter(AccessManagerProxy proxy, int GlobalRoleID)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    IList<GlobalRoleAccess_NewsFeedDao> selectedfeedtemps = new List<GlobalRoleAccess_NewsFeedDao>();
                    GlobalRoleAccess_NewsFeedDao dao = new GlobalRoleAccess_NewsFeedDao();
                    dao.GlobalRoleID = GlobalRoleID;
                    IList<GlobalRoleAccess_NewsFeedDao> roledao = tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].GetAll<GlobalRoleAccess_NewsFeedDao>();
                    tx.Commit();
                    var temp_dao = from t in roledao where t.GlobalRoleID == GlobalRoleID select t;
                    foreach (var i in temp_dao)
                    {
                        GlobalRoleAccess_NewsFeedDao temp = new GlobalRoleAccess_NewsFeedDao();
                        temp.GlobalRoleID = i.GlobalRoleID;
                        temp.NewsFeedGroupID = i.NewsFeedGroupID;
                        temp.ID = i.ID;
                        selectedfeedtemps.Add(temp);
                    }
                    return selectedfeedtemps;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IList<GlobalRoleAccess_NotificationDao> GetSelectedNotificationFilter(AccessManagerProxy proxy, int GlobalRoleID)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    IList<GlobalRoleAccess_NotificationDao> selectednotificationtemps = new List<GlobalRoleAccess_NotificationDao>();
                    GlobalRoleAccess_NotificationDao dao = new GlobalRoleAccess_NotificationDao();
                    dao.GlobalRoleID = GlobalRoleID;
                    IList<GlobalRoleAccess_NotificationDao> roledao = tx.PersistenceManager.PlanningRepository[proxy.MarcomManager.User.TenantID].GetAll<GlobalRoleAccess_NotificationDao>();
                    tx.Commit();
                    var temp_dao = from t in roledao where t.GlobalRoleID == GlobalRoleID select t;
                    foreach (var i in temp_dao)
                    {
                        GlobalRoleAccess_NotificationDao temp = new GlobalRoleAccess_NotificationDao();
                        temp.GlobalRoleID = i.GlobalRoleID;
                        temp.NotificationTempID = i.NotificationTempID;
                        temp.ID = i.ID;
                        selectednotificationtemps.Add(temp);
                    }
                    return selectednotificationtemps;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool Updatepublishaccess(AccessManagerProxy proxy, int Role, bool AccessPermission)
        {
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                     int accessenable = 0;
                    IList<PublishAccessDao> selectednotificationtemps = new List<PublishAccessDao>();
                    PublishAccessDao dao = new PublishAccessDao();
                    dao.Role = Role;
                    dao.AccessPermission = AccessPermission;
                    if (AccessPermission == true)                    
                          accessenable = 1;
                    else
                         accessenable = 0;
                   
                   var res = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Query<PublishAccessDao>().Where(a => a.Role == dao.Role).ToList();
                   if (res.Count > 0)
                   {
                       tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery("update [AM_Publishaccess] set AccessPermission=" + accessenable + "  where Role =" + dao.Role);
                   }
                   else
                   {
                       tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].Save<PublishAccessDao>(dao);                      
                   }
                   tx.Commit();
                }                            
                return true;
            }
            catch
            {
                return false;
            }
        }
       
        public IList Getpublishaccess(AccessManagerProxy proxy)
        {
            IList selectedroleuserdetails;
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    StringBuilder query = new StringBuilder();
                    query.Append("select b.ID,Role,AccessPermission,Caption,Description from [AM_Publishaccess] A full  join  AM_GlobalRole b on a.Role=b.ID  where b.ID not in(-1,-2)");                
                    selectedroleuserdetails = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].ExecuteQuery(query.ToString());
                    tx.Commit();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return selectedroleuserdetails;
        }



        public bool userpublishaccess(AccessManagerProxy proxy, int entityid)
        {

            int userid = (int)proxy.MarcomManager.User.Id;
            bool publishaccess = false;
            try
            {
                using (ITransaction tx = proxy.MarcomManager.GetTransaction(proxy.MarcomManager.User.TenantID))
                {
                    var entityuserroles = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].GetAll<EntityRoleUserDao>().Where(x => x.Entityid == entityid && x.Userid == userid).Select(s=>s.Roleid).ToList();
                    List<int> entityRoles = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].GetAll<EntityTypeRoleAclDao>().Where(x => x.EntityRoleID == (int)StaticEntityRoles.Owner || x.EntityRoleID == (int)StaticEntityRoles.Editor).Select(s => s.ID).ToList();

                    List<int> userRoles = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].GetAll<GlobalRoleUserDao>().Where(x => x.Userid == userid).Select(s => s.GlobalRoleId).ToList();
                    List<int> publishaccessRoles = tx.PersistenceManager.AccessRepository[proxy.MarcomManager.User.TenantID].GetAll<PublishAccessDao>().Where(x => x.AccessPermission == true).Select(s => s.Role).ToList();
                    if (userRoles.Contains((int)staticglobalRole.admin))
                    {
                        publishaccess = true;
                        return publishaccess; 
                    }
                    foreach (var entRol in entityuserroles)
                    {
                        if (entityRoles.Contains(entRol))
                        {
                            foreach (var role in publishaccessRoles)
                            {
                                if (userRoles.Contains(role))
                                {
                                    publishaccess = true;
                                    return publishaccess;
                                }
                            }
 
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return publishaccess;
        }

    }
}
