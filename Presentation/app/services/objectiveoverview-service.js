﻿(function (ng, app) {
    "use strict";

    function ObjectiveoverviewService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            GetFulfillmentEntityTypes: GetFulfillmentEntityTypes,
            GetFulfillmentAttribute: GetFulfillmentAttribute,
            GetFulfillmentAttributeOptions: GetFulfillmentAttributeOptions,
            GetObjectiveRelatedDataOnLoad: GetObjectiveRelatedDataOnLoad,
            GetObjectiveRelatedDataOnLoadSet2: GetObjectiveRelatedDataOnLoadSet2,
            GetMember: GetMember,
            UpdateObjectiveSummaryBlockData: UpdateObjectiveSummaryBlockData,
            GettingObjectiveSummaryBlockDetails: GettingObjectiveSummaryBlockDetails,
            GettingEditObjectiveFulfillmentDetails: GettingEditObjectiveFulfillmentDetails,
            UpdatingObjectiveOverDetails: UpdatingObjectiveOverDetails,
            UpdateObjectiveFulfillmentCondition: UpdateObjectiveFulfillmentCondition,
            GettingObjectiveFulfillmentBlockDetails: GettingObjectiveFulfillmentBlockDetails,
            UpdateObjectiveOwner: UpdateObjectiveOwner,
            UpdateObjectivestatus: UpdateObjectivestatus,
            GetFeedFilter: GetFeedFilter,
            GetEnityFeeds: GetEnityFeeds,
            GetLastEntityFeeds: GetLastEntityFeeds,
            InsertFeedComment: InsertFeedComment,
            PostFeed: PostFeed,
            IsActiveEntity: IsActiveEntity,
            GetEntityTypeAttributeGroupRelation: GetEntityTypeAttributeGroupRelation,
            SaveDetailBlockForLevels: SaveDetailBlockForLevels,
            copyuploadedImage: copyuploadedImage,
            UpdateImageName: UpdateImageName,
            SaveDetailBlockForTreeLevels: SaveDetailBlockForTreeLevels,
            GetAttributeTreeNodeByEntityID: GetAttributeTreeNodeByEntityID
        });

        function GetFulfillmentEntityTypes() {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentEntityTypes/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentAttribute(EntityTypeID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentAttribute/" + EntityTypeID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFulfillmentAttributeOptions(AttributeID, AttributeLevel) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetFulfillmentAttributeOptions/" + AttributeID + "/" + AttributeLevel,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetObjectiveRelatedDataOnLoad(ObjectiveID) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetObjectiveRelatedDataOnLoad/" + ObjectiveID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetObjectiveRelatedDataOnLoadSet2(ObjectiveID) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetObjectiveRelatedDataOnLoadSet2/" + ObjectiveID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetMember(EntityID) {
            var request = $http({
                method: "get",
                url: "api/Planning/GetMember/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateObjectiveSummaryBlockData(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/UpdateObjectiveSummaryBlockData/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingObjectiveSummaryBlockDetails(ObjectiveId) {
            var request = $http({
                method: "get",
                url: "api/Planning/GettingObjectiveSummaryBlockDetails/" + ObjectiveId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingEditObjectiveFulfillmentDetails(ObjectiveID) {
            var request = $http({
                method: "get",
                url: "api/Planning/GettingEditObjectiveFulfillmentDetails/" + ObjectiveID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdatingObjectiveOverDetails(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/UpdatingObjectiveOverDetails/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateObjectiveFulfillmentCondition(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/UpdateObjectiveFulfillmentCondition/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GettingObjectiveFulfillmentBlockDetails(ObjectiveId) {
            var request = $http({
                method: "get",
                url: "api/Planning/GettingObjectiveFulfillmentBlockDetails/" + ObjectiveId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateObjectiveOwner(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/UpdateObjectiveOwner/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateObjectivestatus(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/UpdateObjectivestatus/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetFeedFilter() {
            var request = $http({
                method: "get",
                url: "api/common/GetFeedFilter/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEnityFeeds(EntityID, pageNo, Feedsgroupid) {
            var request = $http({
                method: "get",
                url: "api/common/GetEnityFeeds/" + EntityID + "/" + pageNo + "/" + Feedsgroupid,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetLastEntityFeeds(EntityID, Feedsgroupid) {
            var request = $http({
                method: "get",
                ignoreLoadingBar: true,
                url: "api/common/GetLastEntityFeeds/" + EntityID + "/" + Feedsgroupid,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function InsertFeedComment(addfeedcomment) {
            var request = $http({
                method: "post",
                url: "api/common/InsertFeedComment/",
                params: {
                    action: "add",
                },
                data: addfeedcomment
            });
            return (request.then(handleSuccess, handleError));
        }

        function PostFeed(addnewsfeed) {
            var request = $http({
                method: "post",
                url: "api/common/PostFeed/",
                params: {
                    action: "add",
                },
                data: addnewsfeed
            });
            return (request.then(handleSuccess, handleError));
        }

        function IsActiveEntity(EntityID) {
            var request = $http({
                method: "get",
                url: "api/common/IsActiveEntity/" + EntityID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveDetailBlockForLevels(jobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SaveDetailBlockForLevels/",
                params: {
                    action: "add"
                },
                data: jobj

            });
            return (request.then(handleSuccess, handleError));
        }

        function copyuploadedImage(filename) {
            var request = $http({
                method: "post",
                url: "api/Planning/copyuploadedImage/",
                params: {
                    action: "save"
                },
                data: {
                    filename: filename
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateImageName(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/UpdateImageName/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetEntityTypeAttributeGroupRelation(entitytypeId, EntityID, AttributeGroupId) {

            var request = $http({
                method: "get",
                url: "api/Metadata/GetEntityTypeAttributeGroupRelation/" + entitytypeId + "/" + EntityID + "/" + AttributeGroupId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function SaveDetailBlockForTreeLevels(formobj) {
            var request = $http({
                method: "post",
                url: "api/Metadata/SaveDetailBlockForTreeLevels/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttributeTreeNodeByEntityID(AttributeID, EntityID) {
            var request = $http({
                method: "get",
                url: "api/Metadata/GetAttributeTreeNodeByEntityID/" + AttributeID + "/" + EntityID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            response.data.StatusCode = response.status;
            return (response.data);
        }
    }
    app.service("ObjectiveoverviewService", ['$http', '$q', ObjectiveoverviewService]);
})(angular, app);