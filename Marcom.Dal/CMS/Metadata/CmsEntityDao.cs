﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.CMS.Model
{
    public partial class CmsEntityDao : BaseDao, ICloneable
    {
        #region Member variables

        protected int _id;
        protected int _parentid;
        protected bool _active;
        protected string _name;
        protected int _level;
        protected int _templateid;
        protected int _navid;
        protected string _publisheddate;
        protected string _publishedtime;
        protected string _description;
        protected int _contentVersionid;
        protected bool _publishedstatus;
        protected string _uniquekey;

        #endregion

        #region Constructors

        public CmsEntityDao()
        {

        }

        public CmsEntityDao(int pid)
        {
            this._id = pid;
        }

        public CmsEntityDao(int pid, int pparentid, bool pactive, string pname, int plevel, int ptemplateid, int pnavid, string ppublisheddate, string pdescription, int pcontenversionid, bool ppublishedstatus, string puniquekey, string ppublishedtime)
        {
            this._id = pid;
            this._parentid = pparentid;
            this._active = pactive;
            this._contentVersionid = pcontenversionid;
            this._description = pdescription;
            this._level = plevel;
            this._name = pname;
            this._navid = pnavid;
            this._publisheddate = ppublisheddate;
            this._publishedstatus = ppublishedstatus;
            this._templateid = ptemplateid;
            this._uniquekey = puniquekey;
            this._publishedtime = ppublishedtime;
        }

        #endregion

        #region Public properties

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public virtual int ParentID
        {
            get { return _parentid; }
            set { _parentid = value; }
        }


        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public virtual string UniqueKey
        {
            get { return _uniquekey; }
            set { _uniquekey = value; }
        }

        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }
        }


        public virtual int Level
        {
            get { return _level; }
            set { _level = value; }
        }

        public virtual int TemplateID
        {
            get { return _templateid; }
            set { _templateid = value; }
        }

        public virtual int NavID
        {
            get { return _navid; }
            set { _navid = value; }
        }


        public virtual string PublishedDate
        {
            get { return _publisheddate; }
            set { _publisheddate = value; }
        }

        public virtual string PublishedTime
        {
            get { return _publishedtime; }
            set { _publishedtime = value; }
        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public virtual int Version
        {
            get { return _contentVersionid; }
            set { _contentVersionid = value; }
        }


        public virtual bool PublishedStatus
        {
            get { return _publishedstatus; }
            set { _publishedstatus = value; }
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string CmsEntityDao = "CmsEntityDao";
            public const string ID = "ID";
            public const string ParentID = "ParentID";
            public const string Active = "Active";
            public const string UniqueKey = "UniqueKey";
            public const string Name = "Name";
            public const string Level = "Level";
            public const string TemplateID = "TemplateID";
            public const string NavID = "NavID";
            public const string PublishedDate = "PublishedDate";
            public const string PublishedTime = "PublishedTime";
            public const string Description = "Description";
            public const string Version = "Version";
            public const string PublishedStatus = "PublishedStatus";

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string CmsEntityDao = "CmsEntityDao";
            public const string ID = "ID";
            public const string ParentID = "ParentID";
            public const string Active = "Active";
            public const string UniqueKey = "UniqueKey";
            public const string Name = "Name";
            public const string Level = "Level";
            public const string TemplateID = "TemplateID";
            public const string NavID = "NavID";
            public const string PublishedDate = "PublishedDate";
            public const string PublishedTime = "PublishedTime";
            public const string Description = "Description";
            public const string Version = "Version";
            public const string PublishedStatus = "PublishedStatus";
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

    }
}
