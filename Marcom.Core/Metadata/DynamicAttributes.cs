﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{
    class DynamicAttributes : IDynamicAttributes 
    {
        #region Member Variables

		protected Guid _id;
        protected int _entityId;
      //  protected Iesi.Collections.Generic.ISet<DynamicnewAttributes> _attributes;
		protected List<KeyValuePair<String, String>> _attributes;

		#endregion
		
		#region Constructors
		public DynamicAttributes() {}

        public DynamicAttributes(int pEntityId, List<KeyValuePair<String, String>> pAttributes)
		{
		    this._entityId = pEntityId;
			this._attributes = pAttributes; 
		}

        public DynamicAttributes(Guid pId)
		{
			this._id = pId; 
		}
		
       
		
		#endregion
		
		#region Public Properties

        public Guid Id
        {
            get { return _id; }
            set { Id = value; }
        }
        public int EntityId
        {
            get { return _entityId; }
            set { _entityId = value; }
        }
        public List<KeyValuePair<String, String>> Attributes
        {
            get{ return _attributes;}
            set { _attributes = value; }
           
        }
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            DynamicAttributes castObj = null;
			try
			{
                castObj = (DynamicAttributes)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
    }
}
