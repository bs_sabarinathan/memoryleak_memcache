﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task.Interface;

namespace BrandSystems.Marcom.Core.Task
{
    /// <summary>
    /// Entity object for table 'PM_Entity'.
    /// </summary>
    internal class TempTaskList : ITempTaskList
    {
        #region Member Variables

        protected int _id;
        protected int _TempID;
        protected int _TaskListID;
        protected int _Sortorder;
        #endregion

        #region Constructors
        public TempTaskList() { }

        public TempTaskList(int pTempID, int pTaskListID, int pAttributeID, bool pConditionType, int pSortorder)
        {
            this._TempID = pTempID;
            this._TaskListID = pTaskListID;
            this._Sortorder = pSortorder;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }


        public int TempID
        {
            get { return _TempID; }
            set { _TempID = value; }

        }

        public int TaskListID
        {
            get { return _TaskListID; }
            set { _TaskListID = value; }

        }

      
        public int SortOder
        {
            get { return _Sortorder; }
            set { _Sortorder = value; }

        }



        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            TempTaskList castObj = null;
            try
            {
                castObj = (TempTaskList)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}


