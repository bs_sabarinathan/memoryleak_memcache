﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Core.ImportXml
{
    internal class AttributeToAttributeRelationsImport
    {
        #region Member Variables
        protected int _id;
        protected int _newid;
        protected int _entitytypeid;
        protected int _attributetypeid;
        protected int _attributeid;
        protected int _attributeoptionid;
        protected int _attibutelevel;
        protected string _attributerelationid;
        #endregion

        #region Constructors

        public AttributeToAttributeRelationsImport() { }

        public AttributeToAttributeRelationsImport(int pid, int pentitytypeid, int pattributetypeid, int pattributeid, int pattributeoptionid, int pattibutelevel, string pattributerelationid, int pNewId)
        {
            this._id = pid;
            this._entitytypeid = pentitytypeid;
            this._attributetypeid = pattributetypeid;
            this._attributeid = pattributeid;
            this._attributeoptionid = pattributeoptionid;
            this._attibutelevel = pattibutelevel;
            this._attributerelationid = pattributerelationid;
            this._newid = pNewId;
        }

        public AttributeToAttributeRelationsImport(int pid)
        {
            this._id = pid;
        }

        #endregion

        #region Public Properties

        public int ID
        {
            get { return _id; }
            set { _id = value; }

        }

        public int NewId
        {
            get { return _newid; }
            set { _newid = value; }

        }

        public int EntityTypeID
        {
            get { return _entitytypeid; }
            set { _entitytypeid = value; }

        }

        public int AttributeTypeID
        {
            get { return _attributetypeid; }
            set { _attributetypeid = value; }

        }

        public int AttributeID
        {
            get { return _attributeid; }
            set { _attributeid = value; }

        }

        public int AttributeOptionID
        {
            get { return _attributeoptionid; }
            set { _attributeoptionid = value; }

        }

        public int AttributeLevel
        {
            get { return _attibutelevel; }
            set { _attibutelevel = value; }

        }

        public string AttributeRelationID
        {
            get { return _attributerelationid; }
            set { _attributerelationid = value; }

        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            AttributeToAttributeRelationsImport castObj = null;
            try
            {
                castObj = (AttributeToAttributeRelationsImport)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.ID);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {
            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }
}
