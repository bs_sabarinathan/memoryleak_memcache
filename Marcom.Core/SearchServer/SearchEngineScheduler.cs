﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Threading;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using Mail;
using Preview;
using BrandSystems.Marcom.Core.Utility;
using BrandSystems.Marcom.Core.DAM.Interface;


/// <summary>
/// This is the manager class that handles running the Preview operation on a background thread
/// </summary>
/// 
namespace BrandSystems.Marcom.Core.SearchServer
{
    public class SearchEngineScheduler : IDisposable
    {


        private bool _Cancelled = false;
        /// <summary>
        /// Determines the status fo the Scheduler
        /// </summary>        
        /// 
        public bool Cancelled
        {
            get { return _Cancelled; }
            set { _Cancelled = value; }
        }


        private Dictionary<string, string> ApplicationSettings = new Dictionary<string, string>();

        public static bool isIteminloop = false;
        public SearchEngineScheduler(Dictionary<string, string> AppSettings)
            : base()
        {

            ApplicationSettings = AppSettings;
            isIteminloop = false;
            SearchEngineServer ps = new SearchEngineServer();
            ps.ApplicationSettings = AppSettings;
        }

        /// <summary>
        /// The frequency of checks for mails present in database are 
        /// performed in Seconds.
        /// </summary>

        private int CheckFrequency = 60;

        private AutoResetEvent WaitHandle = new AutoResetEvent(false);

        private object SyncLock = new Object();

        public SearchEngineScheduler()
        {
        }

        /// <summary>
        /// Starts the background thread processing       
        /// </summary>
        /// <param name="CheckFrequency">Frequency that checks are performed in seconds</param>
        public void Start(int checkFrequency, int TenantID)
        {
            // *** Ensure that any waiting instances are shut down
            //this.WaitHandle.Set();

            this.CheckFrequency = checkFrequency;
            this.Cancelled = false;

            if (isIteminloop == false)
            {

                var t = new Thread(
                        () => Run(TenantID));

                //Thread t = new Thread();
                t.Start();

            }
        }

        /// <summary>
        /// Causes the processing to stop. If the operation is still
        /// active it will stop after the current email processing
        /// completes
        /// </summary>
        public void Stop()
        {
            lock (this.SyncLock)
            {
                if (Cancelled)
                {
                    return;
                }

                this.Cancelled = true;
                this.WaitHandle.Set();
            }
        }

        /// <summary>
        /// Runs the actual processing loop by checking the mail box
        /// </summary>

        private void Run(int TenantID)
        {
            // *** Start out  waiting
            this.WaitHandle.WaitOne(this.CheckFrequency * 1000, true);


            while (!Cancelled)
            {



                MarcomManagerFactory.EnsureInitialized();

                if (isIteminloop == false)
                {
                    isIteminloop = true;
                    SearchEngineServer ps1 = new SearchEngineServer();
                    ps1.UpdateLuceneIndex(TenantID);
                }



                // *** Http Ping to force the server to stay alive 
                this.PingServer();
                // *** Put in 

                this.WaitHandle.WaitOne(this.CheckFrequency * 1000, true);
            }

        }

        public void PingServer()
        {
            try
            {
                WebClient http = new WebClient();
                //   string Result = http.DownloadString(System.Configuration.ConfigurationManager.AppSettings("AppUrl"));
            }
            catch (Exception ex)
            {
                string Message = ex.Message;
            }
        }


        #region "IDisposable Members"

        public void Dispose()
        {
            this.Stop();
        }

        #endregion

    }
}