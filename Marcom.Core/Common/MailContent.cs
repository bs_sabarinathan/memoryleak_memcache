using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
    /// MailContent object for table 'CM_MailContent'.
	/// </summary>
	
	internal class MailContent : IMailContent 
	{
		#region Member Variables

		protected int _id;
        protected string _subject;
        protected string _body;
        protected string _description;
        protected int _mailsubscriptiontypeid;
		
		
		#endregion
		
		#region Constructors
		public MailContent() {}

        public MailContent(int pid, string pSubject, string pBody, string pDescription, int pMailSubscriptionTypeID)
		{
            this._id = pid;
            this._subject = pSubject;
            this._body = pBody;
            this._description = pDescription;
            this._mailsubscriptiontypeid = pMailSubscriptionTypeID; 
 
		}

        public MailContent(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}

        public string Subject
		{
			get { return _subject; }            
			set 
			{
			  if (value != null && value.Length > 50)
                  throw new ArgumentOutOfRangeException("Subject", "Subject value, cannot contain more than 50 characters");
              _subject = value; 
			}
		}

        public string Body
		{
			get { return _body; }
            set { _body = value; }
			
		}

        public string description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 4000)
                  throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 100 characters");
              _description = value; 
			}
			
		}
        public virtual int MailSubscriptionTypeID
        {
            get { return _mailsubscriptiontypeid; }
            set {  _mailsubscriptiontypeid = value; }

        }
		

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			FeedComment castObj = null;
			try
			{
				castObj = (FeedComment)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
