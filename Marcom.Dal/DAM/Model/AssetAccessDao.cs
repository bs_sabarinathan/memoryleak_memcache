﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
    public partial class AssetAccessDao : BaseDao, ICloneable
    {
        #region "Member variables"

        protected int _id;
        protected string _role;        

        #endregion

        #region "Constructor"

        public AssetAccessDao() { }


        public AssetAccessDao(int pid, string pRole) 
        {
            this._id = pid;
            this._role = pRole;
        }

        #endregion

        #region "Public propertiers"

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }       


        public virtual string Role
        {
            get { return _role; }
            set { _role = value; }

        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string AssetAccessDao = "AssetAccessDao";
            public const string ID = "ID";
            public const string Role = "Role";
            
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string AssetAccessDao = "AssetAccessDao";
            public const string ID = "ID";
            public const string Role = "Role";
            
        }

        #endregion

    }
}
