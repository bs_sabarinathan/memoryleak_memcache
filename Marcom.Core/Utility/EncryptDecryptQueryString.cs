﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using BrandSystems.Cryptography;

namespace BrandSystems.Marcom.Core.Utility
{
    public class EncryptDecryptQueryString
    {


        public static string EncryptCustomTabUrlQueryString(string stringToEncrypt, string EncryKey, string EncryIV, string Algorithm, string PaddingMode, string CipherMode)
        {
            try
            {

                

                List<byte> Key = new List<byte>();

                BrandSystems.Marcom.Core.Common.SSO ssodata = new BrandSystems.Marcom.Core.Common.SSO();

                ssodata.key = EncryKey;
                ssodata.IV = EncryIV;
                ssodata.AlgorithmValue = Algorithm;
                ssodata.PaddingModeValue = PaddingMode;
                ssodata.CipherModeValue = CipherMode;

                //foreach (string id in Application.Item("Key").ToString().Split(","))
                // foreach (string id in "98,82,78,68,115,121,115,70,111,114,50,48,49,49,33,33".Split(','))
                foreach (string id in ssodata.key.ToString().Split(','))
                {
                    Key.Add(byte.Parse(id));
                }

                BrandSystems.Cryptography.Security.Key = Key.ToArray();

                List<byte> IV = new List<byte>();


                //foreach (string id in "98,82,78,68,115,121,115,70,111,114,50,48,49,49,33,33".Split(','))
                foreach (string id in ssodata.IV.ToString().Split(','))
                {
                    IV.Add(byte.Parse(id));
                }
                BrandSystems.Cryptography.Security.IV = IV.ToArray();
                

                BrandSystems.Cryptography.Security.Algo = (BrandSystems.Cryptography.Security.AlgoType)Enum.Parse(typeof(BrandSystems.Cryptography.Security.AlgoType), ssodata.AlgorithmValue.ToString()); //Security.AlgoType.TripleDES; //(BrandSystems.Cryptography.Security.AlgoType)Enum.Parse(typeof(BrandSystems.Cryptography.Security.AlgoType), Application.Item("AlgoType").ToString());

                if (ssodata.PaddingModeValue != null)
                {
                    BrandSystems.Cryptography.Security.PaddingMode = (System.Security.Cryptography.PaddingMode)Enum.Parse(typeof(System.Security.Cryptography.PaddingMode), ssodata.PaddingModeValue.ToString());
                }

                if (ssodata.CipherModeValue != null)
                {
                    BrandSystems.Cryptography.Security.CipherMode = (System.Security.Cryptography.CipherMode)Enum.Parse(typeof(System.Security.Cryptography.CipherMode), ssodata.CipherModeValue.ToString());
                }

                // BrandSystems.Cryptography.Security.PaddingMode = PaddingMode.ANSIX923;
                //  BrandSystems.Cryptography.Security.CipherMode = CipherMode.CBC;

                //string PlainText = BrandSystems.Cryptography.Security.DecryptString(token).Replace("\0", "");
                //string PlainText = BrandSystems.Cryptography.Security.EncryptString(stringToEncrypt);

                byte[] EncryptByte = BrandSystems.Cryptography.Security.Encrypt(stringToEncrypt);

                //Validate the token date and time
                //PlainText = HttpContext.Current.Server.UrlDecode(PlainText);
                return HttpContext.Current.Server.UrlEncode(Convert.ToBase64String(EncryptByte));
            }
            catch (Exception ex)
            {

            }
            return null;
        }



    }
}
