﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Common;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Metadata.Interface;
using Marcom.API.Authorization;
using Marcom.API.RequestHeaderDetails;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Core.Report.Interface;
using Marcom.API.Models;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.Helpers;
using BrandSystems.Marcom.Dal.Base;
using System.Text;
using BrandSystems.Marcom.Core.Task.Interface;
using BrandSystems.Marcom.Core.DAM.Interface;
using BrandSystems.Marcom.Core.Dam.Interface;
using BrandSystems.Marcom.Core.Metadata.Interface;
namespace Marcom.API.Services.DigitalAssetLibrary
{
    [AuthenticateTokenAttribute]
    public class DigitalAssetRepositoryV1
    {
        // public IList<IMyTaskCollection> GetMytasks(TaskManagerProxy proxy, int FilterByentityID, int[] FilterStatusID, int StartRowno, int MaxRowNo, int AssignRole)
        public List<object> GetDAMViewSettings()
        {

            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.DigitalAssetManager.GetDAMViewSettings().ToList();
        }

        /// <summary>
        /// Gets the Entity Dam Folder.
        /// </summary>
        /// <param name="attributeID">The EntityID.</param>
        /// <returns>string</returns>
        public string GetEntityDamFolder(int entityID)
        {

            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.DigitalAssetManager.GetEntityDamFolder(entityID);
        }


        public List<object> GetAssets(int folderid, int entityID, int viewType, int orderbyid, int pageNo, bool ViewAll)
        {

            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.DigitalAssetManager.GetAssets(folderid, entityID, viewType, orderbyid, pageNo, ViewAll);
        }

        public string CheckPreviewGenerator(string Assetids)
        {

            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.DigitalAssetManager.CheckPreviewGenerator(Assetids);
        }

        public IAssets GetAssetAttributesDetails(int assetId)
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.DigitalAssetManager.GetAssetAttributesDetails(assetId);
        }

        public int PostEntityPeriod(JObject jobj)
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            JArray typeObj = (JArray)jobj["assetIdArr"];

            int[] assetIDArr = typeObj.Select(x => (int)x).ToArray();
            return marcomManager.DigitalAssetManager.AttachAssetsEntityCreation((int)jobj["BriefID"], assetIDArr);
        }
        public List<object> GetDAMToolTipSettings()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.DigitalAssetManager.GetDAMToolTipSettings();
        }
        public int CreateAsset(int parentId, int TypeId, string Name, IList<IAttributeData> listattributevalues, IList<IDamPeriod> listperiodvalues, string FileName, int VersionNo, string MimeType, string Extension, long Size, int EntityID, String FileGuid, string Description, bool IsforDuplicate = false, int Duplicatefilestatus = 0, int LinkedAssetID = 0, string fileAdditionalinfo = null, string strAssetAccess = null, int SourceAssetID = 0, bool IsforAdmin = false, int assetactioncode = 0, bool blnAttach = false, IList<IDAMFile> SourceAssetDAMFile = null, bool fromcrop = false, IList<IAssetAmountCurrencyType> listentityamountcurrencytype = null, bool IsTemplate = false)
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.DigitalAssetManager.CreateAsset(parentId, TypeId, Name, listattributevalues, listperiodvalues, FileName, VersionNo, MimeType, Extension, Size, EntityID, FileGuid, Description, IsforDuplicate, Duplicatefilestatus, LinkedAssetID, fileAdditionalinfo, strAssetAccess, SourceAssetID, IsforAdmin, assetactioncode, blnAttach, SourceAssetDAMFile, fromcrop, listentityamountcurrencytype, IsTemplate);
        }
        public bool CreateNewAssetWithFolder(int EntityID, JArray assetobj, int ImpersonateUID)
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.DigitalAssetManager.CreateNewAssetWithFolder(EntityID, assetobj, ImpersonateUID);
        }
        public Tuple<IList, IList> GetDAMExtensions()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.DigitalAssetManager.GetDAMExtensions();
        }
        public string S3Settings()
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.DigitalAssetManager.S3Settings();
        }
        public string GetAssetCategoryTree(int AsetTypeID)
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.DigitalAssetManager.GetAssetCategoryTree(AsetTypeID);
        }
        public List<object> GetAssetCategoryPathInAssetEdit(int AssetTypeID)
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.DigitalAssetManager.GetAssetCategoryPathInAssetEdit(AssetTypeID);
        }
        public IList<IEntityTypeAttributeRelationwithLevels> GetDamAttributeRelation(int damID)
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.DigitalAssetManager.GetDamAttributeRelation(damID);
        }
        public int CreateBlankAsset(int parentId, int TypeId, string Name, IList<IAttributeData> listattributevalues, int EntityID, int Category, IList<IDamPeriod> listperiodvalues, string url = null, bool IsforDuplicate = false, int LinkedAssetID = 0, string strAssetAccess = null, int SourceAssetID = 0, IList<IAssetAmountCurrencyType> listentityamountcurrencytype = null)
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.DigitalAssetManager.CreateBlankAsset(parentId, TypeId, Name, listattributevalues, EntityID, Category, listperiodvalues, url, IsforDuplicate, LinkedAssetID, strAssetAccess, SourceAssetID, listentityamountcurrencytype);
        }
        public List<Tuple<string, string>> savemultipleUploadedImg(JObject ImgArr)
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.PlanningManager.savemultipleUploadedImg(ImgArr);
        }
        public bool writeUploadErrorlog(JObject errorloginfo)
        {
            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.DigitalAssetManager.writeUploadErrorlog(errorloginfo);
        }
    }
}