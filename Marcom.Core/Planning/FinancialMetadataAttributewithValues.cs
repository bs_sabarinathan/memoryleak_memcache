﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    class FinancialMetadataAttributewithValues : IFinancialMetadataAttributewithValues
    {

        #region Public Properties

        public int ID { get; set; }
        public int EntityID { get; set; }
        public int CostcenterID { get; set; }
        public int FinID { get; set; }
        public int FinTypeID { get; set; }
        public string Caption { get; set; }
        public int AttributeTypeID { get; set; }
        public bool IsSystemDefined { get; set; }
        public string Description { get; set; }
        public bool IsColumn { get; set; }
        public bool IsTooltip { get; set; }
        public bool IsCommitTooltip { get; set; }
        public int SortOrder { get; set; }
        public IList<IFinancialOption> Options { get; set; }
        public dynamic values { get; set; }
        public string ValueCaption { get; set; }
        public bool IsHelptextEnabled { get; set; }
        public string HelptextDecsription { get; set; }
        #endregion

    }
}
