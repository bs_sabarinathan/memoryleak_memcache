﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using Presentation.com.proofhq.www;
using Presentation.Utility;

namespace Presentation
{
    public partial class proofstatusupdate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var TaskID = Request.QueryString["tid"];

            TenantResolver tenantres = new TenantResolver(System.Web.HttpContext.Current.Request.Url.Host.ToString());
            Guid userSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);

            IMarcomManager managers = MarcomManagerFactory.GetMarcomManager(null, userSession);


            var a = "<?xml version=1.0?> " +
                      "<callback type='decision'>" +
                      "<recipient_id>400</recipient_id>" +
                      " <file_id>242</file_id>" +
                      "  <name>Irek Kubicki</name>" +
                      " <decision>changes required</decision>" +
                      "  <proof_name>MyFile.pdf</proof_name>  " +
                      "   <folder_id>2343</folder_id>" +
                      " <folder_name>My Folder</folder_name>" +
                      " <recipient_id>34543</recipient_id>" +
                      " <author_email>irek@proofhq.com</author_email>   " +
                      "</callback>";


            var str = (new System.IO.StreamReader(Request.InputStream)).ReadToEnd();

            XDocument doc = XDocument.Parse(str);
            var usermail = doc.Descendants("author_email").ToList<XElement>()[0].Value;
            var status = doc.Descendants("decision").ToList<XElement>()[0].Value;

            var UserID = managers.DigitalAssetManager.GetUserIDByEmail(usermail);

            if (status == "Approved")
            {
                var val = managers.TaskManager.UpdateProofTaskStatus(Convert.ToInt32(TaskID), 3, Convert.ToInt32(UserID));
            }


        }
    }
}