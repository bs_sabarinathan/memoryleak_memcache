﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    /// <summary>
    /// AdditionalObjectiveEntityValues object for table 'PM_AdditionalObjectiveRating'.
    /// </summary>
   internal class AddtionalObjectiveRating : IAddtionalObjectiveRating
    {
        #region Member Variables

        protected int _id;
        protected string _ratings;
        protected int _obectiveid;
        protected int _sortorder;

        #endregion

        #region Constructors
        public AddtionalObjectiveRating() { }

        public AddtionalObjectiveRating(int pId, string pRatings, int pObjectiveId, int pSortorder)
        {
            this._id = pId;
            this._ratings = pRatings;
            this._obectiveid = pObjectiveId;
            this._sortorder = pSortorder;
        }

        public AddtionalObjectiveRating(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }
        public string Ratings
        {
            get { return _ratings; }
            set { _ratings = value; }
        }
        public int ObjectiveId
        {
            get { return _obectiveid; }
            set { _obectiveid = value; }

        }
        public int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }

        }
        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            AddtionalObjectiveRating castObj = null;
            try
            {
                castObj = (AddtionalObjectiveRating)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }
}
