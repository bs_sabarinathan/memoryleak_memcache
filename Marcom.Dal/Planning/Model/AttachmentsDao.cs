using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// AttachmentsDao object for table 'PM_Attachments'.
	/// </summary>
	
    public partial class AttachmentsDao : BaseDao, ICloneable
	{
		#region Member Variables

		protected int _id;
		protected int _entityid;
		protected string _name;
		protected int _activeversionno;
		protected int _activefileid;
        protected int _typeid;
        protected DateTime _createdon;
        protected Guid _fileguid;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
        protected int _activeFileVersionID;
        protected int _versioningfileid;
		#endregion
		
		#region Constructors
		public AttachmentsDao() {}

        public AttachmentsDao(int pEntityid, string pName, int pActiveVersionNo, int pActiveFileid, int pTypeid, DateTime pCreatedon, int pVersioningFileId)
		{
			this._entityid = pEntityid; 
			this._name = pName; 
			this._activeversionno = pActiveVersionNo; 
			this._activefileid = pActiveFileid;
            this._typeid = pTypeid;
            this._createdon = pCreatedon;
            this._versioningfileid = pVersioningFileId;
            
		}
				
		public AttachmentsDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual int Entityid
		{
			get { return _entityid; }
			set { _bIsChanged |= (_entityid != value); _entityid = value; }
			
		}
		
		public virtual string Name
		{
			get { return _name; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
			  _bIsChanged |= (_name != value); 
			  _name = value; 
			}
			
		}
		
		public virtual int ActiveVersionNo
		{
			get { return _activeversionno; }
			set { _bIsChanged |= (_activeversionno != value); _activeversionno = value; }
			
		}
		
		public virtual int ActiveFileid
		{
			get { return _activefileid; }
			set { _bIsChanged |= (_activefileid != value); _activefileid = value; }
			
		}

        public virtual int Typeid
        {
            get { return _typeid; }
            set { _bIsChanged |= (_typeid != value); _typeid = value; }

        }



        public virtual int ActiveFileVersionID
        {
            get { return _activeFileVersionID; }
            set { _bIsChanged |= (_activeFileVersionID != value); _activeFileVersionID = value; }

        }

        public virtual int VersioningFileId
        {
            get { return _versioningfileid; }
            set { _bIsChanged |= (_versioningfileid != value); _versioningfileid = value; }

        }

        public virtual DateTime Createdon
        {
            get { return _createdon; }
            set { _bIsChanged |= (_createdon != value); _createdon = value; }

        }

       

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }
		




		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string AttachmentsDao = "AttachmentsDao";			
			public const string Id = "Id";			
			public const string Entityid = "Entityid";			
			public const string Name = "Name";			
			public const string ActiveVersionNo = "ActiveVersionNo";			
			public const string ActiveFileid = "ActiveFileid";
            public const string Typeid = "Typeid";
            public const string Createdon = "Createdon";
            public const string ActiveFileVersionID = "ActiveFileVersionID";
            public const string VersioningFileId = "VersioningFileId";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string AttachmentsDao = "AttachmentsDao";			
			public const string Id = "Id";			
			public const string Entityid = "Entityid";			
			public const string Name = "Name";			
			public const string ActiveVersionNo = "ActiveVersionNo";			
			public const string ActiveFileid = "ActiveFileid";
            public const string Typeid = "Typeid";
            public const string Createdon = "Createdon";
            public const string ActiveFileVersionID = "ActiveFileVersionID";
            public const string VersioningFileId = "VersioningFileId";

		}

		#endregion
		
		
	}
	
}
