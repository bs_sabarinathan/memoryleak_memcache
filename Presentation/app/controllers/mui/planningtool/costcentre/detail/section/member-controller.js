﻿(function (ng, app) {
    "use strict"; function muiplanningtoolcostcentredetailmemberCtrl($scope, $timeout, $http, $compile, $resource, $stateParams, $window, $translate, MemberService) {
        function sortOn(collection, name) {
            collection.sort(function (a, b) {
                if (a[name] <= b[name]) { return (-1); }
                return (1);
            });
        }
        $scope.UserimageNewTime = new Date().getTime().toString(); $scope.groupBy = function (attribute, ISglobal) {
            $scope.groups = []; sortOn($scope.memberList, attribute); var groupValue = "_INVALID_GROUP_VALUE_"; for (var i = 0, friend; friend = $scope.memberList[i++];) {
                if (ISglobal == false) {
                    if (friend["IsInherited"] === false) {
                        if (friend[attribute] !== groupValue) { var group = { label: friend[attribute], friends: [], rolename: friend.Role }; groupValue = group.label; $scope.groups.push(group); }
                        group.friends.push(friend);
                    }
                }
                else if (ISglobal == true) {
                    if (friend[attribute] !== groupValue) { var group = { label: friend[attribute], friends: [], rolename: friend.Role }; groupValue = group.label; $scope.groups.push(group); }
                    group.friends.push(friend);
                }
            }
        }; $scope.UserimageNewTime = new Date().getTime().toString(); $scope.memberList = []; $scope.groups = []; $scope.$on('LoadMembersDetailCC', function (event, ID) { $scope.EntityID = ID; $scope.load(); $scope.GlobalAccessStatus = { GlobalText: "Show global access", GlobalStatus: 0 }; }); $scope.AutoCompleteSelectedObj = []; $scope.EntityID = $stateParams.ID; $scope.IsInherited = true
        $scope.InheritedFromEntityid = $stateParams.ID; var levelid = GetLevelfromUniqueKey($window.GlobalUniquekey); $scope.ShowGlobalAccess = levelid > 1 ? true : false; $scope.UserLists = {}; $scope.Roles = {}; $scope.addMember = function () {
            $("#memberModal").modal('show');

            $timeout(function () { $('#ddluser').focus(); }, 1000)
        };
        $scope.AutoCompleteSelectedUserObj = { "UserSelection": [] };
        $scope.load = function (parameters) {
            if ($scope.IsLock == true) { $scope.MemberAddIsLock = false; }
            else
                $scope.MemberAddIsLock = true; MemberService.GetMember($scope.EntityID).then(function (member) { var resp = member.Response; $scope.memberList = member.Response; $scope.groupBy('Roleid', false); $scope.QuickInfo1AttributeCaption = $scope.memberList[0].QuickInfo1AttributeCaption; $scope.QuickInfo2AttributeCaption = $scope.memberList[0].QuickInfo2AttributeCaption; MemberService.GetUsers().then(function (UserbyNamelist) { $scope.UserLists = UserbyNamelist.Response; MemberService.GetEntityTypeRoleAccess(5).then(function (role) { $scope.Roles = role.Response; }); }); });
        }; $scope.ddlrole = ''; $scope.addEntityMember = function () {
            try {
                if ($scope.ddlrole == '' || $scope.ddlrole.length == 0) { bootbox.alert($translate.instant('LanguageContents.Res_1906.Caption')); $('#addcoscentermemberID').removeClass('disabled'); return false; } else { $('#addcoscentermemberID').addClass('disabled'); }
                var userval = $('#ddluser').val(); if (userval.length == 0) { bootbox.alert($translate.instant('LanguageContents.Res_5749.Caption')); $('#addcoscentermemberID').removeClass('disabled'); return false; }
                var memberdata = {}; memberdata.EntityID = $scope.EntityID; memberdata.RoleID = $scope.ddlrole; memberdata.Assignee = parseInt($scope.AutoCompleteSelectedObj[0].Id, 10); memberdata.IsInherited = false; memberdata.InheritedFromEntityid = 0; var result = $.grep($scope.memberList, function (e) { return (e.Userid == parseInt($scope.AutoCompleteSelectedObj[0].Id, 10) && e.Roleid == $scope.ddlrole) && (e.IsInherited == false); }); if (result.length == 0) {
                    MemberService.PostMember(memberdata).then(function (InsertMember) {
                        MemberService.GetMember($scope.EntityID).then(function (member) {
                            var resp = member.Response; $scope.memberList = member.Response; var GlobalAccessToshow = ($scope.GlobalAccessStatus.GlobalStatus == 1 ? true : false)
                            $scope.groupBy('Roleid', GlobalAccessToshow);
                        }); $('#CCddlrole').select2('val', ''); $scope.ddlrole = ''; $('#addcoscentermemberID').removeClass('disabled'); $scope.ddlUser = ''; $scope.AutoCompleteSelectedObj = []; NotifySuccess($translate.instant('LanguageContents.Res_4479.Caption'));
                    });
                } else { $('#CCddlrole').select2('val', ''); $scope.ddlUser = ''; $scope.ddlrole = ''; $scope.AutoCompleteSelectedObj = []; $('#addcoscentermemberID').removeClass('disabled'); bootbox.alert($translate.instant('LanguageContents.Res_1907.Caption')); }
            }
            catch (e)
            { $('#addcoscentermemberID').removeClass('disabled'); }
        }; $scope.updateEntityMember = function (ID) { var data = {}; data.ID = ID; data.EntityID = $scope.EntityID; data.RoleID = $scope.ddlrole; data.Assignee = parseInt($scope.AutoCompleteSelectedObj[0].Id, 10); data.IsInherited = $scope.IsInherited; data.InheritedFromEntityid = $scope.InheritedFromEntityid; MemberService.PutMember(data).then(function (UpdateMember) { }); }; $scope.deleteEntityMember = function (ID, name) {
            if ($scope.IsLock == true) { return false; }
            bootbox.confirm($translate.instant('LanguageContents.Res_1851.Caption') + name + " ?", function (result) {
                if (result) {
                    $timeout(function () {
                        MemberService.Member(ID).then(function () {
                            MemberService.GetMember($scope.EntityID).then(function (member) {
                                var resp = member.Response; $scope.memberList = member.Response; var GlobalAccessToshow = ($scope.GlobalAccessStatus.GlobalStatus == 1 ? true : false)
                                $scope.groupBy('Roleid', GlobalAccessToshow);
                            });
                        });
                    }, 100);
                }
            });
        }; function GetLevelfromUniqueKey(UniqueKey) {
            if (UniqueKey != null && UniqueKey != undefined) { var substr = UniqueKey.split('.'); return substr.length; }
            else { return '' }
        }
        $scope.GlobalAccessStatus = { GlobalText: "Show global access", GlobalStatus: 0 }; $scope.toggleAutoUserAccess = function () {
            if ($scope.GlobalAccessStatus.GlobalStatus == 0) { $scope.GlobalAccessStatus.GlobalStatus = 1; $scope.GlobalAccessStatus.GlobalText = "Hide global access", $scope.groupBy('Roleid', true); }
            else { $scope.GlobalAccessStatus.GlobalStatus = 0; $scope.GlobalAccessStatus.GlobalText = "Show global access", $scope.groupBy('Roleid', false); }
        }
        $scope.CloseAddmember = function () { $scope.ddlrole = ''; $('#CCddlrole').select2('val', ''); $('#addcoscentermemberID').removeClass('disabled'); $scope.ddlUser = ''; $scope.AutoCompleteSelectedObj = []; }
        $scope.$on("$destroy", function () { RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.costcentre.detail.memberCtrl']")); }); $scope.MemberAddIsLock = true; $timeout(function () { $scope.load(); }, 0);
    }
    app.controller("mui.planningtool.costcentre.detail.memberCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$stateParams', '$window', '$translate', 'MemberService', muiplanningtoolcostcentredetailmemberCtrl]);
})(angular, app);