﻿var app = angular.module("app", ['ui.select2', 'ngRoute', "ngResource", 'ngCookies', 'ngGrid', '$strap.directives', 'dndLists', 'angucomplete-alt', 'ngTagsInput', 'pascalprecht.translate', 'ui.bootstrap', 'pasvaz.bindonce', 'angularUtils.directives.dirPagination']);
app.config(['$compileProvider', function ($compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|mailto|devexpress.reportserver):/);
}]);
app.directive('a', function () {
    return {
        restrict: 'E',
        link: function (scope, elem, attrs) {
            var attr = elem.attr('data-toggle');
            if (typeof attr !== 'undefined' && attr !== false) {
                elem.on('click', function (e) {
                    e.preventDefault();
                });
            }
        }
    };
});
var defaultlang = "na";
$.ajaxSetup({
    beforeSend: function (xhr) {
        xhr.setRequestHeader('sessioncookie', $.cookie('Session'));
    }
});
var GlobalIsWorkspaces = false;
var NewsFeedUniqueTimer = null;
var NewsFeedUniqueTimerForTask = null;
var AdditionalSettingsObj = [];
var RememberPlanFilterID = 0;
var RememberPlanFilterAttributes = [];
var RemeberPlanFilterName = "No filter applied";
var RememberCCFilterID = 0;
var RememberCCFilterAttributes = [];
var RemeberCCFilterName = "No filter applied";
var RememberObjFilterID = 0;
var RememberObjFilterAttributes = [];
var RemeberObjFilterName = "No filter applied";
var PreviewGeneratorTimer = null;
var EditPreviewGeneratorTimer = null;
var GlobalAssetEditID = 0;
var TenantFilePath = "";
var TenantID = 0;
var ReportCurrency = {
    CurrencyRate: 0,
    Name: ''
};
var appRecacheLastTime = Date.create().format('{12hr}:{mm}:{ss}');
var amazonURL = "";
var tempLightboxguid = [];
$.ajax({
    type: "GET",
    url: "api/Common/GetAdditionalSettings",
    contentType: "application/json",
    async: false,
    success: function (tempdata) {
        if (tempdata.Response != null) {
            Defaultdate = tempdata.Response[0].SettingValue;
            AdditionalSettingsObj = tempdata.Response;
            app.value('$strapConfig', {
                datepicker: {
                    format: Defaultdate
                }
            });
            var objlen = tempdata.Response.length;
            TenantFilePath = tempdata.Response[objlen - 1].SettingValue;
            TenantID = tempdata.Response[objlen - 1].Id;
        }
    },
    error: function (data) { }
});
$.fn.editable.defaults.mode = 'Popup';
var data;
var IsGlobalAdmin = false;
var GlobalUserDateFormat = '';
$.ajax({
    type: "GET",
    url: "api/Common/GetNavigationConfig",
    contentType: "application/json",
    async: false,
    success: function (tempdata) {
        if (tempdata.Response != null) {

            data = tempdata.Response.m_Item1;
            defaultlang = tempdata.Response.m_Item2;
            eval(defaultlang);
            setTimeout(eval(data), 50);
        }
    },
    error: function (data) { }
});
$.ajax({
    type: "GET",
    url: "api/Access/CheckUserIsAdmin",
    contentType: "application/json",
    async: false,
    success: function (tempdata) {
        if (tempdata.Response != null && tempdata.Response != false) {
            IsGlobalAdmin = true;
        }
    },
    error: function (data) { }
});
var Notifystatementsdisplaytime = '3';
$.ajax({
    type: "GET",
    url: "api/common/Getnotifytimesetbyuser",
    contentType: "application/json",
    async: false,
    success: function (tempdata) {
        if (tempdata.Response != null && tempdata.Response != "false") {
            Notifystatementsdisplaytime = tempdata.Response;
        }
    },
    error: function (data) { }
});
var Notifycyclestatementsdisplaytime = 30000;
$.ajax({
    type: "GET",
    url: "api/common/Getnotifyrecycletimesetbyuser",
    contentType: "application/json",
    async: false,
    success: function (tempdata) {
        if (tempdata.Response != null && tempdata.Response != false) {
            Notifycyclestatementsdisplaytime = parseInt(tempdata.Response);
        }
    },
    error: function (data) { }
});
var UserTime;

function NotifySuccess(msg) {
    $('.top-right').notify({
        message: {
            text: msg
        },
        type: 'success',
        fadeOut: {
            enabled: true,
            delay: parseInt(Notifystatementsdisplaytime) * 1000
        }
    }).show();
}

function NotifyError(msg) {
    $('.top-right').notify({
        message: {
            text: msg
        },
        type: 'danger',
        fadeOut: {
            enabled: true,
            delay: parseInt(Notifystatementsdisplaytime) * 1000
        }
    }).show();
}
var IsLock = false;
var SearchEntityID = 0;
var EntityTypeID = 0;
var SearchEntityLevel = "0";
Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
		c = isNaN(c = Math.abs(c)) ? 2 : c,
		d = d == undefined ? "." : d,
		t = t == undefined ? "," : t,
		s = n < 0 ? "-" : "",
		i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
		j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
Number.prototype.formatMoneyObj = function (c, d, t) {
    var n = this,
		c = isNaN(c) ? 2 : c,
		d = d == undefined ? "." : d,
		t = t == undefined ? "," : t,
		s = n < 0 ? "-" : "",
		i = n = Math.abs(+n || 0).toFixed(c) + "",
		j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
String.prototype.contains = function (chr) {
    var str = this;
    if (str.indexOf(chr) == -1) return false;
    else return true;
};
if (!String.prototype.startsWith) {
    Object.defineProperty(String.prototype, 'startsWith', {
        enumerable: false,
        configurable: false,
        writable: false,
        value: function (searchString, position) {
            position = position || 0;
            return this.indexOf(searchString, position) === position;
        }
    });
}
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (needle) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] === needle) {
                return i;
            }
        }
        return -1;
    };
}
Array.prototype.contains = function (elem) {
    for (var i in this) {
        if (this[i] == elem) return true;
    }
    return false;
}
if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str) {
        return this.indexOf(str) === 0;
    };
}
var SystemDefiendAttributes = {
    FiscalYear: 1,
    Name: 68,
    Owner: 69,
    MilestoneStatus: 67,
    MilestoneEntityID: 66,
    ObjectiveType: 107,
    AssignedAmount: 59,
    Status: 70,
    EntityStatus: 71,
    ApproveTime: 73,
    ObjectiveStatus: 108,
    MyRoleGlobalAccess: 74,
    MyRoleEntityAccess: 75,
    EntityOnTimeStatus: 77,
    TakeDescription: 83,
    TaskNotes: 81,
    TaskDueDate: 84,
    ParentEntityName: -10,
    Description: 3,
    TagOption: 87,
    SubAssignedAmount: 88,
    AvailableAssignedAmount: 89,
    CreationDate: 90
}
var FinancialTransactionType = {
    Planned: 1,
    ApprovedAllocation: 2,
    Commited: 3,
    Spent: 4,
    ForeCasting: 5,
    TotalAssignedAmount: 6,
    SubAssignedAmount: 7,
    AvailableAssignAmount: 8,
    RequestedAmount: 9
};
var taskType = {
    Work_Task: 2,
    Approval_Task: 3,
    Reviewal_Task: 31,
    Asset_Approval_Task: 32,
    Proof_approval_task: 36,
    dalim_approval_task: 37
};
var clientFileStoragetype = {
    local: 0,
    Amazon: 1
};
var finAttributesEnum = (function () {
    var finAttributesEnum = {
        1: "Planned",
        2: "In requests",
        3: "Approved allocations",
        4: "Name"
    };
    return {
        get: function (value) {
            return finAttributesEnum[value];
        }
    }
})();
var finPurchaseOrderAttributesEnum = (function () {
    var finPurchaseOrderAttributesEnum = {
        5: {
            Name: "PONumber",
            Language: "Res_1769"
        },
        6: {
            Name: "strAmount",
            Language: "Res_1746"
        },
        7: {
            Name: "strCreatedDate",
            Language: "Res_31"
        },
        8: {
            Name: "SupplierName",
            Language: "Res_22"
        },
        9: {
            Name: "Description",
            Language: "Res_1736"
        },
        10: {
            Name: "RequesterName",
            Language: "Res_1770"
        },
        11: {
            Name: "strAppprovalDate",
            Language: "Res_1771"
        },
        12: {
            Name: "strSendDate",
            Language: "Res_1772"
        },
        13: {
            Name: "ApproverName",
            Language: "Res_1770"
        }
    };
    return {
        get: function (value) {
            return finPurchaseOrderAttributesEnum[value];
        }
    }
})();
var finSpentAttributesEnum = (function () {
    var finSpentAttributesEnum = {
        15: {
            Name: "InvoiceNumber",
            Language: "Res_1769"
        },
        16: {
            Name: "strCreatedDate",
            Language: "Res_1746"
        },
        17: {
            Name: "POID",
            Language: "Res_31"
        },
        18: {
            Name: "SupplierName",
            Language: "Res_22"
        },
        19: {
            Name: "Description",
            Language: "Res_1736"
        },
        20: {
            Name: "Amount",
            Language: "Res_1736"
        }
    };
    return {
        get: function (value) {
            return finSpentAttributesEnum[value];
        }
    }
})();
var assetAction = (function () {
    var assetAction = {
        1: "AddtoLightBox",
        2: "Task",
        3: "ApprovalTask",
        4: "PublishDAM",
        5: "DuplicateAsset",
        6: "Delete",
        7: "Download",
        8: "Cut",
        9: "Copy",
        10: "ViewLightBox",
        11: "DownloadAll",
        12: "UnpublishAssets",
        13: "Reformat"
    };
    return {
        get: function (value) {
            return assetAction[value];
        }
    }
})();
var taskTypes = (function () {
    var taskTypes = {
        2: "Work Task",
        3: "Approval Task",
        31: "Reviewal Task",
        32: "Asset Approval Task",
        36: "Proof approval task"
    };
    return {
        get: function (value) {
            return taskTypes[value];
        }
    }
})();
var TaskStatus = (function () {
    var TaskStatus = {
        0: "Unassigned",
        1: "In progress",
        2: "Completed",
        3: "Approved",
        4: "Unable to complete",
        5: "Rejected",
        6: "Withdrawn",
        7: "Not Applicable",
        8: "ForcefulComplete",
        9: "RevokeTask"
    };
    return {
        get: function (value) {
            return TaskStatus[value];
        }
    }
})();
var TaskStatusEnum = {
    "Unassigned": 0,
    "In progress": 1,
    "Completed": 2,
    "Approved": 3,
    "Unable to complete": 4,
    "Rejected": 5,
    "Withdrawn": 6,
    "Not Applicable": 7,
    "ForcefulComplete": 8,
    "RevokeTask": 9
};
var taskTypesEnum = {
    "Work Task": 2,
    "Approval Task": 3,
    "Reviewal Task": 31,
    "Asset Approval Task": 32,
    "Proof approval task": 36
};

function generateUniqueTracker() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
    });
    return uuid;
};
var SystemDefiendFinancialMetadata = {
    FundingCostcenter: 3,
    PO: 1,
    Spent: 2
}
var SystemDefinedEntityTypes = {
    Milestone: 1,
    Objective: 10,
    CostCentre: 5,
    AdditionalObjective: 11,
    Task: 30,
    FundingRequest: 7,
    Calendar: 35
}
var AttributeTypes = {
    Owner: 3,
    DateTime: 5,
    ObjectiveType: 4,
    Period: 10
}
var ShowAllRemember = {
    Activity: true,
    CostCentre: true,
    Objective: true
}
var ForecstDivisonIds = {
    Yearly: 1,
    Monthly: 2,
    Quaterly: 3,
    Half_yearly: 4
}
var EntityRoles = {
    Owner: 1,
    Editer: 2,
    Viewer: 3,
    ExternalParticipate: 5,
    BudgerApprover: 8,
    ProofInitiator: 9,
    Corporate: 10
}
var ForecstBasisIds = {
    Planned_Budget: 1,
    Approved_alloc: 2,
    Non_res_appr_alloc: 3
}
var Defaultdate = '';

function Mimer(Extenstion) {
    switch (Extenstion) {
        case "gif":
        case "png":
        case "jpeg":
        case "bmp":
        case "tiff":
        case "emf":
        case "eps":
        case "jpg":
        case "psd":
        case "tif":
            return "Image"
            break;
        case "pdf":
        case "doc":
        case "docx":
        case "ppt":
        case "pptx":
        case "xls":
        case "xlsx":
        case "txt":
        case "xml":
            return "Document"
            break;
        case "aac":
        case "mka":
        case "mp3":
        case "ogg":
        case "wav":
            return "Audio"
            break;
        case "mov":
        case "wmv":
        case "mp4":
        case "flv":
        case "webm":
        case "avi":
        case "mkv":
        case "mkv":
        case "m4v":
            return "Video"
            break;
        case "zip":
        case "rar":
        case "7z":
            return "Zip"
            break;
        default:
            return "Other";
    }
}
var _systemMimeTypes = {
    "ai": "application/postscript",
    "aif": "audio/x-aiff",
    "aifc": "audio/x-aiff",
    "aiff": "audio/x-aiff",
    "asc": "text/plain",
    "atom": "application/atom+xml",
    "au": "audio/basic",
    "avi": "video/x-msvideo",
    "bcpio": "application/x-bcpio",
    "bin": "application/octet-stream",
    "bmp": "image/bmp",
    "cdf": "application/x-netcdf",
    "cgm": "image/cgm",
    "class": "application/octet-stream",
    "cpio": "application/x-cpio",
    "cpt": "application/mac-compactpro",
    "csh": "application/x-csh",
    "css": "text/css",
    "dcr": "application/x-director",
    "dif": "video/x-dv",
    "dir": "application/x-director",
    "djv": "image/vnd.djvu",
    "djvu": "image/vnd.djvu",
    "dll": "application/octet-stream",
    "dmg": "application/octet-stream",
    "dms": "application/octet-stream",
    "doc": "application/msword",
    "docx": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "dotx": "application/vnd.openxmlformats-officedocument.wordprocessingml.template",
    "docm": "application/vnd.ms-word.document.macroEnabled.12",
    "dotm": "application/vnd.ms-word.template.macroEnabled.12",
    "dtd": "application/xml-dtd",
    "dv": "video/x-dv",
    "dvi": "application/x-dvi",
    "dxr": "application/x-director",
    "eps": "application/postscript",
    "etx": "text/x-setext",
    "exe": "application/octet-stream",
    "ez": "application/andrew-inset",
    "gif": "image/gif",
    "gram": "application/srgs",
    "grxml": "application/srgs+xml",
    "gtar": "application/x-gtar",
    "hdf": "application/x-hdf",
    "hqx": "application/mac-binhex40",
    "htm": "text/html",
    "html": "text/html",
    "ice": "x-conference/x-cooltalk",
    "ico": "image/x-icon",
    "ics": "text/calendar",
    "ief": "image/ief",
    "ifb": "text/calendar",
    "iges": "model/iges",
    "igs": "model/iges",
    "jnlp": "application/x-java-jnlp-file",
    "jp2": "image/jp2",
    "jpe": "image/jpeg",
    "jpeg": "image/jpeg",
    "jpg": "image/jpeg",
    "js": "application/x-javascript",
    "kar": "audio/midi",
    "latex": "application/x-latex",
    "lha": "application/octet-stream",
    "lzh": "application/octet-stream",
    "m3u": "audio/x-mpegurl",
    "m4a": "audio/mp4a-latm",
    "m4b": "audio/mp4a-latm",
    "m4p": "audio/mp4a-latm",
    "m4u": "video/vnd.mpegurl",
    "m4v": "video/x-m4v",
    "mac": "image/x-macpaint",
    "man": "application/x-troff-man",
    "mathml": "application/mathml+xml",
    "me": "application/x-troff-me",
    "mesh": "model/mesh",
    "mid": "audio/midi",
    "midi": "audio/midi",
    "mif": "application/vnd.mif",
    "mov": "video/quicktime",
    "movie": "video/x-sgi-movie",
    "mp2": "audio/mpeg",
    "mp3": "audio/mpeg",
    "mp4": "video/mp4",
    "mpe": "video/mpeg",
    "mpeg": "video/mpeg",
    "mpg": "video/mpeg",
    "mpga": "audio/mpeg",
    "ms": "application/x-troff-ms",
    "msh": "model/mesh",
    "mxu": "video/vnd.mpegurl",
    "nc": "application/x-netcdf",
    "oda": "application/oda",
    "ogg": "application/ogg",
    "pbm": "image/x-portable-bitmap",
    "pct": "image/pict",
    "pdb": "chemical/x-pdb",
    "pdf": "application/pdf",
    "pgm": "image/x-portable-graymap",
    "pgn": "application/x-chess-pgn",
    "pic": "image/pict",
    "pict": "image/pict",
    "png": "image/png",
    "pnm": "image/x-portable-anymap",
    "pnt": "image/x-macpaint",
    "pntg": "image/x-macpaint",
    "ppm": "image/x-portable-pixmap",
    "ppt": "application/vnd.ms-powerpoint",
    "pptx": "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    "potx": "application/vnd.openxmlformats-officedocument.presentationml.template",
    "ppsx": "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
    "ppam": "application/vnd.ms-powerpoint.addin.macroEnabled.12",
    "pptm": "application/vnd.ms-powerpoint.presentation.macroEnabled.12",
    "potm": "application/vnd.ms-powerpoint.template.macroEnabled.12",
    "ppsm": "application/vnd.ms-powerpoint.slideshow.macroEnabled.12",
    "ps": "application/postscript",
    "qt": "video/quicktime",
    "qti": "image/x-quicktime",
    "qtif": "image/x-quicktime",
    "ra": "audio/x-pn-realaudio",
    "ram": "audio/x-pn-realaudio",
    "ras": "image/x-cmu-raster",
    "rdf": "application/rdf+xml",
    "rgb": "image/x-rgb",
    "rm": "application/vnd.rn-realmedia",
    "roff": "application/x-troff",
    "rtf": "text/rtf",
    "rtx": "text/richtext",
    "sgm": "text/sgml",
    "sgml": "text/sgml",
    "sh": "application/x-sh",
    "shar": "application/x-shar",
    "silo": "model/mesh",
    "sit": "application/x-stuffit",
    "skd": "application/x-koan",
    "skm": "application/x-koan",
    "skp": "application/x-koan",
    "skt": "application/x-koan",
    "smi": "application/smil",
    "smil": "application/smil",
    "snd": "audio/basic",
    "so": "application/octet-stream",
    "spl": "application/x-futuresplash",
    "src": "application/x-wais-source",
    "sv4cpio": "application/x-sv4cpio",
    "sv4crc": "application/x-sv4crc",
    "svg": "image/svg+xml",
    "swf": "application/x-shockwave-flash",
    "t": "application/x-troff",
    "tar": "application/x-tar",
    "tcl": "application/x-tcl",
    "tex": "application/x-tex",
    "texi": "application/x-texinfo",
    "texinfo": "application/x-texinfo",
    "tif": "image/tiff",
    "tiff": "image/tiff",
    "tr": "application/x-troff",
    "tsv": "text/tab-separated-values",
    "txt": "text/plain",
    "ustar": "application/x-ustar",
    "vcd": "application/x-cdlink",
    "vrml": "model/vrml",
    "vxml": "application/voicexml+xml",
    "wav": "audio/x-wav",
    "wbmp": "image/vnd.wap.wbmp",
    "wbmxl": "application/vnd.wap.wbxml",
    "wml": "text/vnd.wap.wml",
    "wmlc": "application/vnd.wap.wmlc",
    "wmls": "text/vnd.wap.wmlscript",
    "wmlsc": "application/vnd.wap.wmlscriptc",
    "wrl": "model/vrml",
    "xbm": "image/x-xbitmap",
    "xht": "application/xhtml+xml",
    "xhtml": "application/xhtml+xml",
    "xls": "application/vnd.ms-excel",
    "xml": "application/xml",
    "xpm": "image/x-xpixmap",
    "xsl": "application/xml",
    "xlsx": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    "xltx": "application/vnd.openxmlformats-officedocument.spreadsheetml.template",
    "xlsm": "application/vnd.ms-excel.sheet.macroEnabled.12",
    "xltm": "application/vnd.ms-excel.template.macroEnabled.12",
    "xlam": "application/vnd.ms-excel.addin.macroEnabled.12",
    "xlsb": "application/vnd.ms-excel.sheet.binary.macroEnabled.12",
    "xslt": "application/xslt+xml",
    "xul": "application/vnd.mozilla.xul+xml",
    "xwd": "image/x-xwindowdump",
    "xyz": "chemical/x-xyz",
    "zip": "application/zip"
}

function GetMIMEType(fileName) {
    if (fileName != null) {
        var fileext = fileName.split('.').pop();
        var extenstion = fileext.toLowerCase();
        if (extenstion.length > 0 && _systemMimeTypes[extenstion] != undefined) {
            return _systemMimeTypes[extenstion];
        }
        return "application/unknown";
    } else return "application/unknown";
}

function returnformatstring(format) {
    var replacematter = "",
        formatparts = [],
        formatpattern = "";
    if (format.contains('/')) formatparts = format.split('/');
    else formatparts = format.split('-');
    for (var p = 0; p <= formatparts.length - 1; p++) {
        formatparts[p] = "{" + formatparts[p].toUpperCase() + "}";
    }
    replacematter = formatparts.join("-");
    return replacematter;
}

function formatteddateFormat(date, format) {
    if (format.toString() == "MM-dd-yyyy" || format.toString() == "MM/dd/yyyy" || format.toString() == "M-d-yyyy" || format.toString() == "M/d/yyyy") {
        date = new Date.create(date);
    } else {
        format = format.toUpperCase();
        var dateSample = date;
        var replacematter = "",
            formatparts = [],
            formatpattern = "";
        if (format.contains('/')) {
            formatparts = format.split('/');
            formatpattern = "/";
        } else {
            formatparts = format.split('-');
            formatpattern = "-";
        }
        for (var p = 0; p <= formatparts.length - 1; p++) {
            formatparts[p] = "{" + formatparts[p].toUpperCase() + "}";
        }
        replacematter = formatparts.join(formatpattern);
        date = new Date.create(date, replacematter);
    }
    return date;
}

function dateFormat(date, format) {
    if (format == undefined) {
        format = GlobalUserDateFormat;
    }
    format = format.toUpperCase();
    var dateSample = date;
    var replacematter = "",
        formatparts = [];
    if (format.contains('/')) formatparts = format.split('/');
    else formatparts = format.split('-');
    for (var p = 0; p <= formatparts.length - 1; p++) {
        formatparts[p] = "{" + formatparts[p].toUpperCase() + "}";
    }
    replacematter = formatparts.join("-");
    date = new Date.create(date);
    if (date.getDate() == "1" && date.getMonth() == "0" && date.getFullYear() == '1970') {
        date = new Date(dateSample);
    }
    format = format.replace("DD", (date.getDate() < 10 ? '0' : '') + date.getDate());
    format = format.replace("D", (date.getDate() < 10 ? '' : '') + date.getDate());
    format = format.replace("MM", (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1));
    format = format.replace("M", (date.getMonth() < 9 ? '' : '') + (date.getMonth() + 1));
    format = format.replace("YYYY", date.getFullYear());
    return format;
}

function ConvertStringToDate(date) {
    var dateVal;
    if (date.contains('/')) dateVal = date.split('/');
    else dateVal = date.split('-');
    return date = new Date.create(Date.create((parseInt(dateVal[0]), parseInt(dateVal[1] - 1), parseInt(dateVal[2])).toString()).utc(true))
}

function ConvertStringToDateObj(date) {
    var dateVal;
    if (date.contains('/')) dateVal = date.split('/');
    else dateVal = date.split('-');
    return date = new Date.create(Date.create((parseInt(dateVal[0]), parseInt(dateVal[1] - 1), parseInt(dateVal[2]))))
}

function ConvertStringToDateByFormat(date, inputformat) {
    var dateSample = date;
    var dateVal;
    if (date.contains('/')) {
        dateVal = date.split('/');
    } else {
        dateVal = date.split('-');
    }
    var defaultFormat = inputformat.toUpperCase();
    if (defaultFormat.startsWith('D')) {
        date = new Date.create(Date.create(parseInt(dateVal[2]), parseInt(dateVal[1] - 1), parseInt(dateVal[0])).utc(true));
    } else if (defaultFormat.startsWith('M')) {
        date = new Date.create(Date.create(parseInt(dateVal[2]), parseInt(dateVal[0] - 1), parseInt(dateVal[1])).utc(true));
    } else {
        date = new Date.create(Date.create(parseInt(dateVal[0]), parseInt(dateVal[1] - 1), parseInt(dateVal[2])).utc(true));
    }
    return date;
}

function ConvertDateToString(date) {
    if (typeof date == "string") date = new Date.create(date);
    return (date.getFullYear() + "-" + ((date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1)) + "-" + ((date.getDate() < 10 ? '0' : '') + date.getDate()));
}

function ConvertDateFromStringToString(date) {
    var dateParts;
    if (date.contains('/')) dateParts = date.split('/');
    else dateParts = date.split('-');
    var dateObj = new Date.create(Date.create(parseInt(dateParts[0]), parseInt(dateParts[1] - 1), parseInt(dateParts[2])).utc(true));
    var format = GlobalUserDateFormat;
    format = format.replace("DD", (dateObj.getDate() < 10 ? '0' : '') + dateObj.getDate());
    format = format.replace("D", (dateObj.getDate() < 10 ? '' : '') + dateObj.getDate());
    format = format.replace("MM", (dateObj.getMonth() < 9 ? '0' : '') + (dateObj.getMonth() + 1));
    format = format.replace("M", (dateObj.getMonth() < 9 ? '' : '') + (dateObj.getMonth() + 1));
    format = format.replace("YYYY", dateObj.getFullYear());
    return format;
}

function ConvertDateFromStringToStringByFormat(date) {
    var dateSample = date;
    var dateVal;
    if (date.contains('/')) {
        dateVal = date.split('/');
    } else {
        dateVal = date.split('-');
    }
    var defaultFormat = GlobalUserDateFormat;
    if (defaultFormat.startsWith('D')) {
        dateSample = dateVal[2] + "-" + dateVal[1] + "-" + dateVal[0];
    } else if (defaultFormat.startsWith('M')) {
        dateSample = dateVal[2] + "-" + dateVal[0] + "-" + dateVal[1];
    } else {
        dateSample = dateVal[0] + "-" + dateVal[1] + "-" + dateVal[2];
    }
    return dateSample;
}

function FormatStandard(date, format) {
    format = format.toUpperCase();
    var dateSample = date;
    var dateVal;
    if (date.contains('/')) dateVal = date.split('/');
    else dateVal = date.split('-');
    date = new Date.create(Date.create(parseInt(dateVal[0]), parseInt(dateVal[1] - 1), parseInt(dateVal[2])).utc(true))
    format = format.replace("DD", (date.getDate() < 10 ? '0' : '') + date.getDate());
    format = format.replace("D", (date.getDate() < 10 ? '' : '') + date.getDate());
    format = format.replace("MM", (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1));
    format = format.replace("M", (date.getMonth() < 9 ? '' : '') + (date.getMonth() + 1));
    format = format.replace("YYYY", date.getFullYear());
    return format;
}

function ReturnFormattedTodayDate() {
    var today = Date.create(new Date())
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var today = yyyy + "-" + mm + "-" + dd;
    return today;
}

function dstrToUTC(ds) {
    var dsarr = ds.split("-");
    var mm = parseInt(dsarr[1], 10);
    var dd = parseInt(dsarr[2], 10);
    var yy = parseInt(dsarr[0], 10);
    var dt = new Date('' + yy.toString() + '-' + mm.toString() + '-' + dd.toString() + '');
    return dt;
}

function datediff(ds1, ds2) {
    var date1 = new Date.create(ds1);
    var date2 = new Date.create(ds2);
    var timeDiff = Math.floor(date1.getTime() - date2.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    return diffDays;
}

function GetCurrentCacheTime() {
    return Date.create().format('{12hr}:{mm}:{ss}');
}

function TrackTimeDifference(currentCacheTime, lastcachetime) {
    var srchms = currentCacheTime,
        trgthms = lastcachetime;
    var a = srchms.split(':'),
        b = lastcachetime.split(':');
    var srcseconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]),
        trgtseconds = (+b[0]) * 60 * 60 + (+b[1]) * 60 + (+b[2]);
    var mdiff = Math.floor((srcseconds - trgtseconds) / 60);
    return mdiff;
}

function CreateHisory(IDList) {
    var trackValue = IDList[0];
    for (ti = 1; ti < IDList.length; ti++) {
        trackValue += ',' + IDList[ti];
    }
    $.cookie('ListofEntityID', IDList);
    var Guid = GenerateGUID();
    InsertGUID(Guid, trackValue);
    return Guid;
}

function GenerateGUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
    });
    return uuid;
};

function InsertGUID(Guid, Value) {
    var ApplicationUrlTrack = {
        "TrackID": "" + Guid + "",
        "TrackValue": "" + Value + ""
    };
    var TrackData = JSON.stringify(ApplicationUrlTrack);
    $.ajax({
        type: "POST",
        url: "api/Common/InsertUpdateApplicationUrlTrack",
        contentType: "application/json",
        data: TrackData,
        async: true,
        success: function (tempdata) {
            if (tempdata.Response != false) { }
        },
        error: function (data) { }
    });
}

function GetGuidValue(trackid) {
    var guid = "";
    $.ajax({
        type: "GET",
        url: "api/Common/GetApplicationUrlTrackByID/" + trackid,
        contentType: "application/json",
        data: {
            TrackID: trackid
        },
        async: false,
        success: function (tempdata) {
            if (tempdata.Response != null) {
                guid = tempdata.Response;
            }
        },
        error: function (data) { }
    });
    return guid;
}
$(document).on("change", "#EntityMetadata select", function () {
    var ActualAttributeID = $(this).attr('id');
    if (ActualAttributeID != undefined) {
        var AttrID = $(this).attr('id').replace('ListSingleSelection_', '').replace('DropDownTree_', '');
        var OptionVal = $('option[value="' + $(this).val() + '"]', this).text().toString().trim();
    }
});

function FixPace() {
    if (window.oldProgress == undefined) {
        window.oldProgress = $('.pace-progress').attr('data-progress-text').replace('%', '');
    } else {
        var newProgress = $('.pace-progress').attr('data-progress-text').replace('%', '');
        if (parseFloat(newProgress) < parseFloat(window.oldProgress)) {
            window.oldProgress = newProgress;
        } else if ((parseFloat(newProgress) - parseFloat(window.oldProgress)) < 1 && parseFloat(newProgress) > 98) {
            $('body').removeClass('pace-running').addClass('pace-done');
            $('body .pace').removeClass('pace-active').addClass('pace-inactive');
        } else {
            window.oldProgress = newProgress;
        }
    }
}
setInterval('FixPace();', 500);

function isIE() {
    var myNav = navigator.userAgent.toLowerCase();
    return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
}
$(document).on("mouseover", "#separator", function () {
    if (!$(this).hasClass('ui-draggable')) {
        $("#separator").draggable({
            axis: "x",
            grid: [20, 20],
            stop: function (event, ui) {
                if (event.clientX > 599) {
                    $('#content').css('margin-left', 600 + 'px');
                    $('#separator').css('left', '0');
                    $('#left').css('width', 600 + 'px');
                } else if (event.clientX < 229) {
                    $('#content').css('margin-left', 230 + 'px');
                    $('#separator').css('left', '0');
                    $('#left').css('width', 230 + 'px');
                    if ($('#TreeOpenClose').find('i').hasClass('icon-chevron-left')) {
                        TreeOpenCloseClick($('#TreeOpenClose'));
                    } else {
                        $('#content').css('margin-left', 62 + 'px');
                        $('#separator').css('left', '0');
                        $('#left').css('width', 62 + 'px');
                    }
                } else {
                    if ($('#TreeOpenClose').find('i').hasClass('icon-chevron-right')) {
                        TreeOpenCloseClick($('#TreeOpenClose'));
                    }
                    $('#content').css('margin-left', (event.clientX + 1) + 'px');
                    $('#separator').css('left', '0');
                    $('#left').css('width', (event.clientX + 1) + 'px');
                }
            }
        });
    }
});
$(document).on("mouseover", "#attachmentseparator", function () {
    if (!$(this).hasClass('ui-draggable')) {
        $("#attachmentseparator").draggable({
            axis: "x",
            grid: [20, 20],
            stop: function (event, ui) {
                var ClientX = event.clientX - ($('#left').width() + 38);
                if (ClientX > 599) {
                    $('#right_DamView').css('margin-left', 610 + 'px');
                    $('#left_folderTree').css('width', 600 + 'px');
                    $('#attachmentseparator').attr('style', 'right: -1px;');
                    $('.expandedLeftSection').show();
                    $('.collapsedLeftSection').hide();
                } else if (ClientX < 229) {
                    $('#right_DamView').css('margin-left', 30 + 'px');
                    $('#left_folderTree').css('width', 20 + 'px');
                    $('#attachmentseparator').attr('style', 'right: -1px;');
                    $('.expandedLeftSection').hide();
                    $('.collapsedLeftSection').show();
                } else {
                    $('#right_DamView').css('margin-left', (ClientX + 1 + 10) + 'px');
                    $('#left_folderTree').css('width', (ClientX + 1) + 'px');
                    $('#attachmentseparator').attr('style', 'right: -1px;');
                    $('.expandedLeftSection').show();
                    $('.collapsedLeftSection').hide();
                }
            }
        });
    }
});
$(document).on("click", ".collapsedLeftSection", function () {
    $('.expandedLeftSection').show();
    $('.collapsedLeftSection').hide();
    $('#right_DamView').css('margin-left', 240 + 'px');
    $('#left_folderTree').css('width', 230 + 'px');
    $('#attachmentseparator').attr('style', 'right: -1px;');
});

function TreeOpenCloseClick(obj) {
    if ($(obj).find('i').hasClass('icon-chevron-left')) {
        $(obj).attr('data-width', $('#left').width());
        $('#left .ActionBtnArea').next().hide();
        $('#treeSettings').hide();
        $(obj).find('i').removeClass('icon-chevron-left').addClass('icon-chevron-right');
        $('#content').css('margin-left', 62 + 'px');
        $('#separator').css('left', '0');
        $('#left').css('width', 62 + 'px');
    } else {
        $('#left .ActionBtnArea').next().show();
        $('#treeSettings').show();
        $(obj).find('i').removeClass('icon-chevron-right').addClass('icon-chevron-left');
        $('#content').css('margin-left', $(obj).attr('data-width') + 'px');
        $('#separator').css('left', '0');
        $('#left').css('width', $(obj).attr('data-width') + 'px');
    }
}
$(document).on("click", "#TreeOpenClose", function () {
    TreeOpenCloseClick($(this));
});

function countwatches() {
    (function () {
        var root = angular.element(document.getElementsByTagName('body'));
        var watchers = [];
        var f = function (element) {
            angular.forEach(['$scope', '$isolateScope'], function (scopeProperty) {
                if (element.data() && element.data().hasOwnProperty(scopeProperty)) {
                    angular.forEach(element.data()[scopeProperty].$$watchers, function (watcher) {
                        watchers.push(watcher);
                    });
                }
            });
            angular.forEach(element.children(), function (childElement) {
                f(angular.element(childElement));
            });
        };
        f(root);
        var watchersWithoutDuplicates = [];
        angular.forEach(watchers, function (item) {
            if (watchersWithoutDuplicates.indexOf(item) < 0) {
                watchersWithoutDuplicates.push(item);
            }
        });
        console.log(watchersWithoutDuplicates.length);
    })();
}

function RecursiveUnbindAndRemove($jElement) {
    $jElement.children().each(function () {
        RecursiveUnbindAndRemove($(this));
    });
    dealoc($jElement);
}

function dealoc(obj) {
    var jqCache = angular.element.cache;
    if (obj) {
        if (angular.isElement(obj)) {
            cleanup(angular.element(obj));
        } else if (!window.jQuery) {
            for (var key in jqCache) {
                var value = jqCache[key];
                if (value.data && value.data.$scope == obj) {
                    delete jqCache[key];
                }
            }
        }
    }

    function cleanup(element) {
        element.off().removeData();
        if (window.jQuery) {
            jQuery.cleanData([element]);
        }
        var children = element[0].childNodes || [];
        for (var i = 0; i < children.length; i++) {
            cleanup(angular.element(children[i]));
        }
    }
}
window.CreateDOCGeneratedAsset = "", window.CreateversionDOCGeneratedAsset = "";
$.ajax({
    type: "GET",
    url: "api/Common/GetOptimakerAddresspoints",
    contentType: "application/json",
    async: false,
    success: function (tempdata) {
        if (tempdata.Response != null) {
            var strArr = tempdata.Response.split(",");
            if (strArr.length > 0) {
                window.CreateDOCGeneratedAsset = strArr[0];
                window.CreateversionDOCGeneratedAsset = strArr[1];
            }
        }
    },
    error: function (data) { }
});
window.entityId = 0, window.folderid = 0, window.isword = false, window.createdbyid = 0, window.assetId = 0;

function setGlobalOptimakerSettings(eid, fid, isword, createdby) {
    window.entityId = eid, window.folderid = fid, window.isword = isword, window.createdbyid = createdby;
}

function setAssetIdfrGlobalOptimakerSettings(assetid, eid, createdBy) {
    window.assetId = assetid;
    window.entityId = eid;
    window.createdbyid = createdBy;
}
var jsonObject = {
    "Searchterm": "a1"
};
var cloudsetup = {};
$.ajax({
    type: "GET",
    url: "api/dam/S3Settings",
    contentType: "application/json",
    async: false,
    success: function (data) {
        if (data.Response != null && data.Response != "") {
            var res = JSON.parse(data.Response);
            cloudsetup.BucketName = res.BucketName;
            cloudsetup.AWSAccessKeyID = res.AWSAccessKeyID;
            cloudsetup.AWSSecretAccessKey = res.AWSSecretAccessKey;
            cloudsetup.RequestEndpoint = res.RequestEndpoint;
            cloudsetup.PolicyDocument = res.PolicyDocument;
            cloudsetup.PolicyDocumentSignature = res.PolicyDocumentSignature;
            cloudsetup.Uploaderurl = res.UploaderUrl;
            cloudsetup.storageType = res.FileStorageType;

            amazonURL = cloudsetup.Uploaderurl + "/";
        }
    },
    error: function (data) { }
});