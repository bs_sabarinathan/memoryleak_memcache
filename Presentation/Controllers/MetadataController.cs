﻿using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Core.Planning;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Presentation.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Xml.Linq;

namespace Presentation.Controllers
{
    public class MetadataController : ApiController
    {
        ServiceResponse result;

        [Route("api/Metadata/EntityType")]
        [HttpPost]
        public ServiceResponse EntityType([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertUpdateEntityType((string)jobj["Caption"], (string)jobj["Description"], (int)jobj["ModuleID"], (int)jobj["Category"], (string)jobj["ShortDescription"], (string)jobj["ColourCode"], ((int)jobj["IsAssociate"]) == 1 ? true : false, (int)jobj["WorkFlowID"], (bool)jobj["IsRootLevel"], (int)jobj["ID"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/EntityType method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/EntityType method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/PutEntityType")]
        [HttpPut]
        public ServiceResponse PutEntityType([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                int ID = (int)jobj["ID"];
                result.Response = (marcomManager.MetadataManager.InsertUpdateEntityType((string)jobj["Caption"], (string)jobj["Description"], (int)jobj["ModuleID"], (int)jobj["Category"], (string)jobj["ShortDescription"], (string)jobj["ColourCode"], (bool)jobj["IsAssociate"], (int)jobj["WorkFlowID"], (bool)jobj["IsRootLevel"], ID) == 0 ? false : true);

            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/DeleteEntityType/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteEntityType(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteEntityType(ID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/InsertEntityTypeFeature")]
        [HttpPost]
        public ServiceResponse InsertEntityTypeFeature([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                //System.Threading.Thread.Sleep(1000);
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jEntityFeatureData = (JArray)jobj["EntityFeatureData"];
                foreach (var entobj in jEntityFeatureData)
                {
                    // System.Threading.Thread.Sleep(1000);
                    result.Response = marcomManager.MetadataManager.InsertEntityTypefeature((int)jobj["TypeID"], (int)entobj, (int)jobj["ID"]);
                }
                //result.Response = marcomManager.MetadataManager.InsertEntityTypefeature((int)jobj["TypeID"], (int)jobj["FeatureID"],(int)jobj["ID"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/InsertEntityTypeFeature method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch (Exception e)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/InsertEntityTypeFeature method hits exception as " + e, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/DeleteEntityTypeFeature/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteEntityTypeFeature(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteEntityTypeFeature(ID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/DeleteEntityTypeFeature method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/DeleteEntityTypeFeature method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/Attribute")]
        [HttpPost]
        public ServiceResponse Attribute([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertUpdateAttribute((string)jobj["Caption"], (string)jobj["Description"], (int)jobj["AttributeTypeID"], ((int)jobj["IsSystemDefined"] == 0 ? false : true), ((int)jobj["IsSpecial"] == 0 ? false : true), (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/PutAttribute")]
        [HttpPut]
        //method name changed PutAttribute
        public ServiceResponse PutAttribute([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = (marcomManager.MetadataManager.InsertUpdateAttribute((string)jobj["Caption"], (string)jobj["Caption"], (int)jobj["AttributeTypeID"], ((int)jobj["IsSystemDefined"] == 0 ? false : true), ((int)jobj["IsSpecial"] == 0 ? false : true), (int)jobj["ID"]) == 0 ? false : true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/DeleteAttribute/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteAttribute(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteAttribute(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/Module")]
        [HttpPost]
        public ServiceResponse Module([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertUpdateModule((string)jobj["Caption"], (string)jobj["Description"], ((int)jobj["IsEnable"] == 0 ? false : true), (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/PutModule")]
        [HttpPut]
        //method name changed PutModule
        public ServiceResponse PutModule([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                int ID = (int)jobj["ID"];
                result.Response = (marcomManager.MetadataManager.InsertUpdateModule((string)jobj["Caption"], (string)jobj["Description"], (bool)jobj["IsEnable"], ID) == 0 ? false : true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetModuleByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetModuleByID(int ID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetModuleByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/Attributetype")]
        [HttpPost]
        public ServiceResponse Attributetype([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertUpadateAttributetype((string)jobj["Caption"], (string)jobj["ClassName"], (bool)jobj["IsSelectable"], (string)jobj["DataType"], (string)jobj["SqlType"], (int)jobj["Length"], (bool)jobj["IsNullable"], (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/PutAttributetype")]
        [HttpPut]

        public ServiceResponse PutAttributetype([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = (marcomManager.MetadataManager.InsertUpadateAttributetype((string)jobj["Caption"], (string)jobj["ClassName"], (bool)jobj["IsSelectable"], (string)jobj["DataType"], (string)jobj["SqlType"], (int)jobj["Length"], (bool)jobj["IsNullable"], (int)jobj["ID"]) == 0 ? false : true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/DeleteAttributetype/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteAttributetype(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteAttributetype(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/DeleteModule/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteModule(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteModule(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/Modulefeature")]
        [HttpPost]
        public ServiceResponse Modulefeature([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertUpdateModulefeature((int)jobj["ModuleID"], (int)jobj["FeatureID"], (bool)jobj["IsEnable"], (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/PutModulefeature")]
        [HttpPut]
        //method name changed PutModulefeature
        public ServiceResponse PutModulefeature([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = (marcomManager.MetadataManager.InsertUpdateModulefeature((int)jobj["ModuleID"], (int)jobj["FeatureID"], (bool)jobj["IsEnable"], (int)jobj["ID"]) == 0 ? false : true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityType/{ModuleID}")]
        [HttpGet]
        public ServiceResponse GetEntityType(int ModuleID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityType(ModuleID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GetEntityTypeIsAssociate")]
        [HttpGet]
        public ServiceResponse GetEntityTypeIsAssociate()
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityTypeIsAssociate();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GetEntityTypefromDB")]
        [HttpGet]
        public ServiceResponse GetEntityTypefromDB()
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityTypefromDB();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetFulfillmentFinicalAttribute/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetFulfillmentFinicalAttribute(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetFulfillmentFinicalAttribute(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityTypeByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetEntityTypeByID(int ID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityTypeByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GetAttributetype")]
        [HttpGet]
        public ServiceResponse GetAttributetype()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributetype();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetAttributeTypeByEntityTypeID/{EntityTypeID}/{IsAdmin}")]
        [HttpGet]
        public ServiceResponse GetAttributeTypeByEntityTypeID(int EntityTypeID, bool IsAdmin)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributeTypeByEntityTypeID(EntityTypeID, IsAdmin);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetAttributeByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetAttributeByID(int ID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributeByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/DeleteModuleFeature/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteModuleFeature(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteModuleFeature(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/GetMultiSelect/{EntityID}/{AttributeID}/{OptionID}")]
        [HttpGet]
        public ServiceResponse GetMultiSelect(int EntityID, int AttributeID, string OptionID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertMultiSelect(EntityID, AttributeID, OptionID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/Option")]
        [HttpPost]
        public ServiceResponse Option([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jr = (JArray)jobj["Options"];
                var len = jr.Count();
                for (int i = 0; i < len; i++)
                {
                    result.Response = marcomManager.MetadataManager.InsertUpdateOption((string)jr[i]["Caption"], (int)jobj["AttributeID"], (int)jr[i]["SortOrder"], (int)jr[i]["ID"]);
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/PutOption")]
        [HttpPut]

        public ServiceResponse PutOption([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = (marcomManager.MetadataManager.InsertUpdateOption((string)jobj["Caption"], (int)jobj["AttributeID"], (int)jobj["SortOrder"], (int)jobj["ID"]) == 0 ? false : true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/DeleteOption/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteOption(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteOption(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/TreeLevel")]
        [HttpPost]
        public ServiceResponse TreeLevel([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertUpdateTreelevel((int)jobj["Level"], (string)jobj["LevelName"], (int)jobj["AttributeID"], (bool)jobj["IsPercentage"], (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetTreelevelByAttributeID/{AttributeID}")]
        [HttpGet]
        public ServiceResponse GetTreelevelByAttributeID(int AttributeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetTreelevelByAttributeID(AttributeID, false);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetAdminTreelevelByAttributeID/{AttributeID}")]
        [HttpGet]
        public ServiceResponse GetAdminTreelevelByAttributeID(int AttributeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetTreelevelByAttributeID(AttributeID, true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        //method renamed

        [Route("api/Metadata/PutTreeLevel")]
        [HttpPut]
        public ServiceResponse PutTreeLevel([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = (marcomManager.MetadataManager.InsertUpdateTreelevel((int)jobj["Level"], (string)jobj["LevelName"], (int)jobj["AttributeID"], (bool)jobj["IsPercentage"], (int)jobj["ID"]) == 0 ? false : true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/DeleteTreeLevel/{TreeLevelID}")]
        [HttpDelete]
        public ServiceResponse DeleteTreeLevel(int TreeLevelID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteTreeLevel(TreeLevelID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/InsertTreeNode")]
        [HttpPost]
        public ServiceResponse InsertTreeNode([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //result.Response = marcomManager.MetadataManager.InsertTreeNode((int)jobj["NodeID"], (int)jobj["ParentNodeID"], (int)jobj["Level"], (string)jobj["KEY"], (int)jobj["AttributeID"], (string)jobj["Caption"],(int)jobj["SortOrder"], (int)jobj["ID"]);
                result.Response = marcomManager.MetadataManager.InsertUpdateTree((JObject)jobj["Tree"], (JArray)jobj["TreeLevels"], (int)jobj["AttributeID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/InsertAttributeSequencePattern")]
        [HttpPost]
        public ServiceResponse InsertAttributeSequencePattern([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertAttributeSequencePattern(jobj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/DeleteTreeNode/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteTreeNode(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteTreeNode(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/TreeValue")]
        [HttpPost]
        public ServiceResponse TreeValue([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertUpdateTreeValue((int)jobj["AttributeID"], (int)jobj["EntityID"], (int)jobj["NodeID"], (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        //method renamed
        [Route("api/Metadata/PutTreeValue")]
        [HttpPut]
        public ServiceResponse PutTreeValue([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = (marcomManager.MetadataManager.InsertUpdateTreeValue((int)jobj["AttributeID"], (int)jobj["NodeID"], (int)jobj["ID"]) == 0 ? false : true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/DeleteTreeValue/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteTreeValue(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteTreeValue(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/Validation")]
        [HttpPost]
        public ServiceResponse Validation()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = 0;//marcomManager.MetadataManager.InsertUpdateValidation(0,);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        //Method Renamed

        [Route("api/Metadata/PostValidation")]
        [HttpPost]
        public ServiceResponse PostValidation([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray jarr = (JArray)jobj["AttributeValidationList"];
                result.StatusCode = (int)HttpStatusCode.OK;
                foreach (var objew in jarr)
                {
                    IList<IValidation> listValidation = new List<IValidation>();
                    IValidation validation = marcomManager.MetadataManager.CreateValidationInstace();
                    validation.EntityTypeID = (int)jobj["EntityTypeID"];
                    validation.ErrorMessage = (string)objew["ErrorMessage"];
                    validation.Value = (string)objew["Value"];
                    validation.ValueType = (string)objew["ValueType"];
                    validation.Id = (int)objew["Id"];
                    validation.AttributeID = (int)objew["AttributeID"];
                    listValidation.Add(validation);
                    result.StatusCode = (int)HttpStatusCode.OK;
                    result.Response = marcomManager.MetadataManager.InsertUpdateValidation(listValidation, validation.AttributeID, validation.EntityTypeID, (int)jobj["AttributeTypeID"], validation.Id);
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/GetAttributeValidationByEntityTypeId/{EntityTypeID}/{AttributeID}")]
        [HttpGet]
        public ServiceResponse GetAttributeValidationByEntityTypeId(int EntityTypeID, int AttributeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributeValidationByEntityTypeId(EntityTypeID, AttributeID);
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/GetAttributeValidationByEntityTypeId method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GetValidationDationByEntitytype/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetValidationDationByEntitytype(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetValidationDationByEntitytype(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/DeleteAttributeValidation/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteAttributeValidation(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteValidation(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/DeleteValidation")]
        [HttpDelete]
        public ServiceResponse DeleteValidation([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteValidation((int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/GetAttribute")]
        [HttpGet]
        public ServiceResponse GetAttribute()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttribute();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetAttributeWithLevels")]
        [HttpGet]
        public ServiceResponse GetAttributeWithLevels()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributeWithLevels();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetNavigationType")]
        [HttpGet]
        public ServiceResponse GetNavigationType()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetUserEnabledNavigations();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;

            }
            return result;
        }
        [Route("api/Metadata/GetModule")]
        [HttpGet]
        public ServiceResponse GetModule()
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetModule();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetModulefeature/{Version}")]
        [HttpGet]
        public ServiceResponse GetModulefeature(int Version)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetModulefeature(Version);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        //Method renamed
        [Route("api/Metadata/GetMultiSelect1/{Version}")]
        [HttpGet]
        public ServiceResponse GetMultiSelect1(int Version)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetMultiSelect(Version);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetOption/{Version}")]
        [HttpGet]
        public ServiceResponse GetOption(int Version)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetOption(Version);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetOptionListID/{ID}")]
        [HttpGet]
        public ServiceResponse GetOptionListID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetOptionListByID(ID, false);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetAdminOptionListID/{ID}")]
        [HttpGet]
        public ServiceResponse GetAdminOptionListID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetOptionListByID(ID, true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetOptionDetailListByID/{ID}/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetOptionDetailListByID(int ID, int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetOptionDetailListByID(ID, EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetTreelevel/{Version}")]
        [HttpGet]
        public ServiceResponse GetTreelevel(int Version)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetTreelevel(Version);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetTreeNode/{AttributeID}")]
        [HttpGet]
        public ServiceResponse GetTreeNode(int AttributeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetTreeNode(AttributeID, false);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetAttributeTreeNode/{AttributeID}/{EntityID}/{Ischoosefromparent}")]
        [HttpGet]
        public ServiceResponse GetAttributeTreeNode(int AttributeID, int EntityID, bool Ischoosefromparent)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                if (!Ischoosefromparent)
                    result.Response = marcomManager.MetadataManager.GetAttributeTreeNode(AttributeID, EntityID);
                else
                    result.Response = marcomManager.MetadataManager.GetDetailAttributeTreeNodeFromParent(AttributeID, EntityID, true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetAdminTreeNode/{AttributeID}")]
        [HttpGet]
        public ServiceResponse GetAdminTreeNode(int AttributeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetTreeNode(AttributeID, true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetTreeValue/{Version}")]
        [HttpGet]
        public ServiceResponse GetTreeValue(int Version)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetTreeValue(Version);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetValidation/{Version}")]
        [HttpGet]
        public ServiceResponse GetValidation(int Version)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetValidation(Version);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/ListSetting/{elementNode}/{entitytype}")]
        [HttpGet]
        public ServiceResponse ListSetting(string elementNode, int entitytype)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = XmlSetting.ListSettings(elementNode, entitytype, tenantres.tenantID);// marcomManager.MetadataManager.ListSetting((string)metadata.elementNode);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/EntityTypeAttributeRelation")]
        [HttpPost]
        public ServiceResponse EntityTypeAttributeRelation([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                // System.Threading.Thread.Sleep(1500);
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jEntityAttributedata = (JArray)jobj["AttributeData"];

                for (int i = 0; i < jEntityAttributedata.Count(); i++)
                {
                    //result.Response = marcomManager.MetadataManager.InsertUpdateEntityTypeAttributeRelation((int)jobj["EntityTypeID"], (int)jEntityAttributedata[i]["AttributeID"], jEntityAttributedata[i]["ValidationID"].ToString(), (i + 1), (string)jEntityAttributedata[i]["DefaultValue"], ((bool)jEntityAttributedata[i]["InheritFromParent"]), ((bool)jEntityAttributedata[i]["IsReadOnly"]), ((bool)jEntityAttributedata[i]["ChooseFromParentOnly"]), ((bool)jEntityAttributedata[i]["IsValidationNeeded"]), (string)jEntityAttributedata[i]["Caption"], (bool)jEntityAttributedata[i]["IsSystemDefined"], (string)jEntityAttributedata[i]["PlaceHolderValue"], (int)jEntityAttributedata[i]["MinValue"], (int)jEntityAttributedata[i]["MaxValue"], (int)jEntityAttributedata[i]["ID"]);
                    result.Response = marcomManager.MetadataManager.InsertUpdateEntityTypeAttributeRelation((int)jobj["EntityTypeID"], (int)jEntityAttributedata[i]["AttributeID"], jEntityAttributedata[i]["ValidationID"].ToString(), (i + 1), (string)jEntityAttributedata[i]["DefaultValue"], ((bool)jEntityAttributedata[i]["InheritFromParent"]), ((bool)jEntityAttributedata[i]["IsReadOnly"]), ((bool)jEntityAttributedata[i]["ChooseFromParentOnly"]), ((bool)jEntityAttributedata[i]["IsValidationNeeded"]), (string)jEntityAttributedata[i]["Caption"], (bool)jEntityAttributedata[i]["IsSystemDefined"], (string)jEntityAttributedata[i]["PlaceHolderValue"], (int)jEntityAttributedata[i]["MinValue"], (int)jEntityAttributedata[i]["MaxValue"], (int)jEntityAttributedata[i]["ID"], (bool)jEntityAttributedata[i]["IsHelptextEnabled"], (string)jEntityAttributedata[i]["HelptextDecsription"]);

                }
            }
            catch (MarcomAccessDeniedException ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/EntityTypeAttributeRelation method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/EntityTypeAttributeRelation method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityTypeAttributeRelation/{Version}")]
        [HttpGet]
        public ServiceResponse GetEntityTypeAttributeRelation(int Version)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntitytyperelation(Version);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityTypeAttributeRelationByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetEntityTypeAttributeRelationByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityTypeAttributeRelationByID(ID);
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/GetEntityTypeAttributeRelationByID method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityTypeAttributeRelationWithLevelsByID/{ID}/{ParentID}")]
        [HttpGet]
        public ServiceResponse GetEntityTypeAttributeRelationWithLevelsByID(int ID, int ParentID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityTypeAttributeRelationWithLevelsByID(ID, ParentID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        //--------------------

        [Route("api/Metadata/GetJSONEntityTypeAttributeRelationWithLevelsByID/{ID}/{ParentID}")]
        [HttpGet]
        public ServiceResponse GetSubEntityTypeAttributeRelationWithLevelsByID(int ID, int ParentID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetJSONEntityTypeAttributeRelationWithLevelsByID(ID, ParentID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }


        [Route("api/Metadata/DeleteEntityAttributeRelation/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteEntityAttributeRelation(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //int id = (int)metadata["ID"];
                result.Response = marcomManager.MetadataManager.DeleteEntityAttributeRelation(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/SyncToDb")]
        [HttpPost]
        public ServiceResponse SyncToDb()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.SyncToDb();
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetFeature")]
        [HttpGet]
        public ServiceResponse GetFeature()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetFeature();
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/GetFeature method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityTypefeature/{Version}")]
        [HttpGet]
        public ServiceResponse GetEntityTypefeature(int Version)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityTypefeature(Version);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityTypefeatureByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetEntityTypefeatureByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityTypefeatureByID(ID);
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/GetEntityTypefeatureByID method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetVersionsCountAndCurrentVersion/{key}")]
        [HttpGet]
        public ServiceResponse GetVersionsCountAndCurrentVersion(string key)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetVersionsCountAndCurrentVersion(key);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;

        }
        [Route("api/Metadata/UpdateActiveVersion")]
        [HttpPost]
        public ServiceResponse UpdateActiveVersion([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.UpdateActiveVersion((int)jobj["version"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/UpdateWorkingVersion")]
        [HttpPost]
        public ServiceResponse UpdateWorkingVersion([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.UpdateWorkingVersion((int)jobj["version"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetTreeNodeByLevel/{AttributeID}/{Level}")]
        [HttpGet]
        public ServiceResponse GetTreeNodeByLevel(int AttributeID, int Level)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetTreeNodeByLevel(AttributeID, Level);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GettingFilterAttribute")]
        [HttpPost]
        public ServiceResponse GettingFilterAttribute([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray jEntityID = (JArray)jobj["IDList"];
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GettingFilterAttribute((int)jobj["TypeID"], (String)jobj["FilterType"], (int)jobj["OptionFrom"], jEntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GettingChildEntityTypes/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GettingChildEntityTypes(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GettingChildEntityTypes(EntityTypeID);
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/GettingChildEntityTypes method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/InsertEntityTypeHierarchy")]
        [HttpPost]
        public ServiceResponse InsertEntityTypeHierarchy([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                //System.Threading.Thread.Sleep(1500);
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jEntityChildActvityIds = (JArray)jobj["ChildAtctivityTypeIds"];

                foreach (var entattrobj in jEntityChildActvityIds)
                {
                    // System.Threading.Thread.Sleep(1500);
                    result.Response = marcomManager.MetadataManager.InsertEntityTypeHierarchy((int)jobj["ParentActivityTypeID"], (int)entattrobj, (int)jobj["SortOrder"], (int)jobj["ID"]);
                }
            }
            catch (MarcomAccessDeniedException ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/InsertEntityTypeHierarchy method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/InsertEntityTypeHierarchy method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetAttributesForDetailBlock/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetAttributesForDetailBlock(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributesForDetailBlock(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/SaveDetailBlock")]
        [HttpPost]
        public ServiceResponse SaveDetailBlock([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.SaveDetailBlock((int)jobj["ID"], (int)jobj["EntityID"], (string)jobj["NewValue"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/SaveDetailBlockForLevels")]
        [HttpPost]
        public ServiceResponse SaveDetailBlockForLevels([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray jrnewValue = (JArray)jobj["NewValue"];
                //dynamic valueList = jrnewValue.Select(jv => (dynamic)jv).ToArray();
                List<object> obj1 = new List<object>();
                foreach (var obj in jrnewValue)
                {
                    if (obj.Type == JTokenType.Boolean)
                    {
                        obj1.Add(Convert.ToBoolean(obj.ToString()));
                    }
                    else if (obj.Type == JTokenType.Date)
                    {
                        obj1.Add(obj.ToString());

                    }
                    else if (obj.Type == JTokenType.Integer)
                    {

                        obj1.Add(int.Parse(obj.ToString()));

                    }
                    else
                        obj1.Add(obj.ToString());
                }


                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.SaveDetailBlockForLevels((int)jobj["EntityID"], (int)jobj["AttributetypeID"], (int)jobj["AttributeID"], obj1, (int)jobj["Level"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/UpdateDropDownTreePricing")]
        [HttpPost]
        public ServiceResponse UpdateDropDownTreePricing([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray jrnewValue = (JArray)jobj["NewValue"];

                IList<ITreeValue> iitree = new List<ITreeValue>();

                foreach (var obj in jrnewValue)
                {
                    ITreeValue itree = new TreeValue();
                    itree.Nodeid = (int)obj["NodeId"];
                    itree.Level = (int)obj["Level"];
                    itree.Value = (string)obj["value"] != "-1" ? (string)obj["value"] : "";
                    iitree.Add(itree);
                }


                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.UpdateDropDownTreePricing((int)jobj["EntityID"], (int)jobj["AttributetypeID"], (int)jobj["AttributeID"], iitree);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/SaveDetailBlockForTreeLevels")]
        [HttpPost]
        public ServiceResponse SaveDetailBlockForTreeLevels([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray jrnewValue = (JArray)jobj["NewValue"];

                JArray jrnewtree = (JArray)jobj["newTree"];
                JArray jroldTree = (JArray)jobj["oldTree"];

                IList<ITreeValue> iitree = new List<ITreeValue>();

                foreach (var obj in jrnewValue)
                {
                    ITreeValue itree = new TreeValue();
                    itree.Nodeid = (int)obj["id"];
                    itree.Level = (int)obj["Level"];
                    iitree.Add(itree);
                }


                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.SaveDetailBlockForTreeLevels((int)jobj["EntityID"], (int)jobj["AttributetypeID"], (int)jobj["AttributeID"], iitree, (JArray)jobj["oldTree"], (JArray)jobj["newTree"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GettingEntityTypeHierarchy/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GettingEntityTypeHierarchy(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GettingEntityTypeHierarchy(EntityTypeID);
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/GettingEntityTypeHierarchy method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GettingEntityTypeHierarchyForRootLevel/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GettingEntityTypeHierarchyForRootLevel(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GettingEntityTypeHierarchyForRootLevel(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetOwnerForEntity/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetOwnerForEntity(int EntityID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetOwnerForEntity(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GetOptionToBind")]
        [HttpGet]
        public String GetOptionToBind()
        {
            string testing = string.Empty;
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                testing = "[{value: 0, text: 'Admin'}, {value: 1, text: 'Operator'}]";
                result.Response = null;

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return testing;
        }
        [Route("api/Metadata/DeleteEntityTypeHierarchy/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteEntityTypeHierarchy(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteEntityTypeHierarchy(ID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/DeleteEntityTypeHierarchy method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/DeleteEntityTypeHierarchy method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/GetOptionsFromXML/{elementNode}/{typeid}")]
        [HttpGet]
        public ServiceResponse GetOptionsFromXML(string elementNode, int typeid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetOptionsFromXML(elementNode, typeid, marcomManager.User.TenantID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GetFulfillmentEntityTypes")]
        [HttpGet]
        public ServiceResponse GetFulfillmentEntityTypes()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetFulfillmentEntityTypes();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GetFulfillmentEntityTypesfrCal")]
        [HttpGet]
        public ServiceResponse GetFulfillmentEntityTypesfrCal()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetFulfillmentEntityTypesfrCal();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetFulfillmentAttribute/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetFulfillmentAttribute(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetFulfillmentAttribute(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetFulfillmentAttributeOptions/{AttributeID}/{AttributeLevel}")]
        [HttpGet]
        public ServiceResponse GetFulfillmentAttributeOptions(int AttributeID, int AttributeLevel)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetFulfillmentAttributeOptions(AttributeID, AttributeLevel);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetAllEntityIds/")]
        [HttpGet]
        public ServiceResponse GetAllEntityIds()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                if (marcomManager.EntitySortorderIdColle != null)
                {
                    var IdCollections = marcomManager.EntitySortorderIdColle.Select(a => a.EntityIds).ToArray();


                    result.Response = IdCollections;
                }
                else
                {
                    result.Response = null;
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/ActivityRootLevel")]
        [HttpPost]
        public ServiceResponse ActivityRootLevel([FromBody]JObject jobj)
        {
            //  StringBuilder strStringText = new StringBuilder();
            //  var ActivityRoot1 = Stopwatch.StartNew();
            //   strStringText.Append("--------------service root level start ActivityRoot1 -------").Append("\n");
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = jr[i]["EntityTypeIDs"].ToString();
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        if (jr[i]["EntityMemberIDs"] != null)
                            filtervalues.EntityMemberIDs = jr[i]["EntityMemberIDs"].ToString();
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                List<int> Typeids = marcomManager.MetadataManager.GetAllEntityTypes();
                var lstSettings = XmlSetting.MultiRootLevelSettings("RootView", 6, Typeids, tenantres.tenantID);

                if ((bool)jobj["IsDuplicate"])
                {
                    marcomManager.EntitySortorderIdColle = null;
                }
                // ActivityRoot1.Stop();
                //   strStringText.Append("--------------service root level end ActivityRoot1 (" + ActivityRoot1.Elapsed.TotalMilliseconds + ") -------").Append("\n");
                //   var ActivityRoot2 = Stopwatch.StartNew();
                //   strStringText.Append("--------------core function call start -------").Append("\n");

                result.Response = marcomManager.MetadataManager.ListofRecords((int)jobj["StartRowNo"], (int)jobj["MaxNoofRow"], (int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], 1, (int)jobj["EntityID"], false, (int)jobj["UserID"], (int)jobj["Level"], false, 0);
                //  ActivityRoot2.Stop();
                //  strStringText.Append("--------------core function call end  (" + ActivityRoot2.Elapsed.TotalMilliseconds + ") -------").Append("\n");
                //  XmlSetting.TextFileWriter(strStringText);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/ActivityDetail")]
        [HttpPost]
        public ServiceResponse ActivityDetail([FromBody]JObject jobj)
        {
            //   StringBuilder strStringText = new StringBuilder();
            //   var ActivityRoot1 = Stopwatch.StartNew();
            //   strStringText.Append("--------------service entity detail start ActivityRoot1 -------").Append("\n");
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                ListSettings lstSettings = XmlSetting.ListSettings("Tree", 6, tenantres.tenantID);
                lstSettings.Attributes = XmlSetting.ListGanttAttributes(6, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = (string)jr[i]["EntityTypeIDs"];
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        filtervalues.StartDate = (string)jr[i]["StartDate"];
                        filtervalues.EndDate = (string)jr[i]["EndDate"];
                        if (jr[i]["EntityMemberIDs"] != null)
                            filtervalues.EntityMemberIDs = (string)jr[i]["EntityMemberIDs"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                //       ActivityRoot1.Stop();
                //       strStringText.Append("--------------service entity detail end ActivityRoot1 (" + ActivityRoot1.Elapsed.TotalMilliseconds + ") -------").Append("\n");
                //       var ActivityRoot2 = Stopwatch.StartNew();
                //       strStringText.Append("--------------core entity detail call start -------").Append("\n");
                result.Response = marcomManager.MetadataManager.ListofRecords((int)jobj["StartRowNo"], (int)jobj["MaxNoofRow"], (int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], (int)jobj["FilterType"], (int)jobj["EntityID"], (bool)jobj["IsSingleID"], 0, (int)jobj["Level"], false, (int)jobj["ExpandingEntityID"], false, 6, false);
                //       ActivityRoot2.Stop();
                //       strStringText.Append("--------------core entity detail call end  (" + ActivityRoot2.Elapsed.TotalMilliseconds + ") -------").Append("\n");
                //       XmlSetting.TextFileWriter(strStringText);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/ActivityDetailReport")]
        [HttpPost]
        public ServiceResponse ActivityDetailReport([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                ListSettings lstSettings = XmlSetting.ListSettings("Tree", 6, tenantres.tenantID);
                lstSettings.Attributes = XmlSetting.ListGanttAttributes(6, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = (string)jr[i]["EntityTypeIDs"];
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        if (jr[i]["EntityMemberIDs"] != null)
                            filtervalues.EntityMemberIDs = (string)jr[i]["EntityMemberIDs"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.Response = marcomManager.MetadataManager.ListofReportRecords((int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], 1, (int)jobj["EntityID"], false, 0, (int)jobj["Level"], false, (int)jobj["ExpandingEntityID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/CostCentreDetailReport")]
        [HttpPost]
        public ServiceResponse CostCentreDetailReport([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                ListSettings lstSettings = XmlSetting.ListSettings("Tree", 5, tenantres.tenantID);
                lstSettings.Attributes = XmlSetting.ListGanttAttributes(5, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = (string)jr[i]["EntityTypeIDs"];
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        filtervalues.StartDate = (string)jr[i]["StartDate"];
                        filtervalues.EndDate = (string)jr[i]["EndDate"];
                        if (jr[i]["EntityMemberIDs"] != null)
                            filtervalues.EntityMemberIDs = jr[i]["EntityMemberIDs"].ToString();
                        iFiltervalues.Add(filtervalues);
                    }
                }

                //result.Response = marcomManager.MetadataManager.ListofReportRecords((int)metadata.StartRowNo, (int)metadata.MaxNoofRow, (int)metadata.FilterID, iFiltervalues, Idarr, ((string)metadata.SortOrderColumn == "null" ? "" : (string)metadata.SortOrderColumn), (bool)metadata.IsDesc, lstSettings, (bool)metadata.IncludeChildren, 2, (int)metadata.EntityID, false, 0, (int)metadata.Level);
                result.Response = marcomManager.MetadataManager.ListofReportRecords((int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], 2, (int)jobj["EntityID"], false, 0, (int)jobj["Level"], false, (int)jobj["ExpandingEntityID"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/ObjectiveDetailReport")]
        [HttpPost]
        public ServiceResponse ObjectiveDetailReport([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                ListSettings lstSettings = XmlSetting.ListSettings("Tree", 10, tenantres.tenantID);
                lstSettings.Attributes = XmlSetting.ListGanttAttributes(10, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = (string)jr[i]["EntityTypeIDs"];
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        filtervalues.StartDate = (string)jr[i]["StartDate"];
                        filtervalues.EndDate = (string)jr[i]["EndDate"];
                        if (jr[i]["EntityMemberIDs"] != null)
                            filtervalues.EntityMemberIDs = (string)jr[i]["EntityMemberIDs"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.Response = marcomManager.MetadataManager.ListofReportRecords((int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], 3, (int)jobj["EntityID"], false, 0, (int)jobj["Level"], false, (int)jobj["ExpandingEntityID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/CostCentreRootLevel")]
        [HttpPost]
        public ServiceResponse CostCentreRootLevel([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                ListSettings lstSettings = XmlSetting.ListSettings("RootView", 5, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = "";
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.Response = marcomManager.MetadataManager.ListofRecords((int)jobj["StartRowNo"], (int)jobj["MaxNoofRow"], (int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], 1, (int)jobj["EntityID"], false, (int)jobj["UserID"], (int)jobj["Level"], false, 0, false, 5, true);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/CostCentreDetail")]
        [HttpPost]
        public ServiceResponse CostCentreDetail([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                ListSettings lstSettings = XmlSetting.ListSettings("Tree", 5, tenantres.tenantID);
                lstSettings.Attributes = XmlSetting.ListGanttAttributes(5, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = (string)jr[i]["EntityTypeIDs"];
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        filtervalues.StartDate = (string)jr[i]["StartDate"];
                        filtervalues.EndDate = (string)jr[i]["EndDate"];
                        if (jr[i]["EntityMemberIDs"] != null)
                            filtervalues.EntityMemberIDs = (string)jr[i]["EntityMemberIDs"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.Response = marcomManager.MetadataManager.ListofRecords((int)jobj["StartRowNo"], (int)jobj["MaxNoofRow"], (int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], (int)jobj["FilterType"], (int)jobj["EntityID"], false, 0, (int)jobj["Level"], false, (int)jobj["ExpandingEntityID"], false, 5, false);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetActivityByID")]
        [HttpPost]
        public ServiceResponse GetActivityByID([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                ListSettings lstSettings = XmlSetting.ListSettings("Tree", 6, tenantres.tenantID);
                lstSettings.Attributes = XmlSetting.ListGanttAttributes(6, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = "";
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.Response = marcomManager.MetadataManager.ListofRecords((int)jobj["StartRowNo"], (int)jobj["MaxNoofRow"], (int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], 1, (int)jobj["EntityID"], true, 0, (int)jobj["Level"], false, (int)jobj["ExpandingEntityID"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/ObjectiveRootLevel")]
        [HttpPost]
        public ServiceResponse ObjectiveRootLevel([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;

                var objTypeids = marcomManager.MetadataManager.GetObjectiveEntityType(false);
                List<int> Typeids = new List<int>();
                if (objTypeids.Count > 0)
                {
                    foreach (var val in objTypeids)
                    {
                        Typeids.Add(Convert.ToInt32(val.GetType().GetProperty("Id").GetValue(val, null)));
                    }
                }
                var lstSettings = XmlSetting.MultiRootLevelSettings("RootView", 10, Typeids, tenantres.tenantID);

                //ListSettings lstSettings = XmlSetting.ListSettings("RootView", 10, tenantres.tenantID);

                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = "";
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.Response = marcomManager.MetadataManager.ListofRecords((int)jobj["StartRowNo"], (int)jobj["MaxNoofRow"], (int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], 1, (int)jobj["EntityID"], false, (int)jobj["UserID"], (int)jobj["Level"], true, 0);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/ObjectiveDetail")]
        [HttpPost]
        public ServiceResponse ObjectiveDetail([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                ListSettings lstSettings = XmlSetting.ListSettings("Tree", 10, tenantres.tenantID);
                lstSettings.Attributes = XmlSetting.ListGanttAttributes(10, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = (string)jr[i]["EntityTypeIDs"];
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        filtervalues.StartDate = (string)jr[i]["StartDate"];
                        filtervalues.EndDate = (string)jr[i]["EndDate"];
                        if (jr[i]["EntityMemberIDs"] != null)
                            filtervalues.EntityMemberIDs = (string)jr[i]["EntityMemberIDs"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.Response = marcomManager.MetadataManager.ListofRecords((int)jobj["StartRowNo"], (int)jobj["MaxNoofRow"], (int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], 3, (int)jobj["EntityID"], false, 0, (int)jobj["Level"], false, (int)jobj["ExpandingEntityID"], false, 10, (bool)jobj["IsPredefinedObj"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetPath/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetPath(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetPath(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/DeleteOptionByAttributeID/{attributeid}")]
        [HttpDelete]
        public ServiceResponse DeleteOptionByAttributeID(int attributeid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteOptionByAttributeID(attributeid);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/DeleteSequencByeAttributeID/{attributeid}")]
        [HttpDelete]
        public ServiceResponse DeleteSequencByeAttributeID(int attributeid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteSequencByeAttributeID(attributeid);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/InsertEntityHistory/{EntityID}/{UserID}")]
        [HttpPost]
        public ServiceResponse InsertEntityHistory(int EntityID, int UserID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //result.Response = marcomManager.MetadataManager.InsertEntityHistory((int)jobj["EntityID"],(int)jobj["UserID"]);
                result.Response = marcomManager.MetadataManager.InsertEntityHistory(EntityID, UserID);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityHistoryByID/{UserID}/{Topx}")]
        [HttpGet]
        public ServiceResponse GetEntityHistoryByID(int UserID, int Topx)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                //return marcomManager.CommonManager.Navigation_Select(((int)common.IsParentID == 1) ? true : false);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.EntityHistory_Select(UserID, Topx);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetTopActivityByID/{UserID}/{Topx}")]
        [HttpGet]
        public ServiceResponse GetTopActivityByID(int UserID, int Topx)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                //return marcomManager.CommonManager.Navigation_Select(((int)common.IsParentID == 1) ? true : false);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.TopxActivity_Select(UserID, Topx);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetTopMyTaskByID/{UserID}/{Topx}")]
        [HttpGet]
        public ServiceResponse GetTopMyTaskByID(int UserID, int Topx)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                //return marcomManager.CommonManager.Navigation_Select(((int)common.IsParentID == 1) ? true : false);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.TopxMyTask_Select(UserID, Topx);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GetWorkFlowDetails")]
        [HttpGet]
        public ServiceResponse GetWorkFlowDetails()
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetWorkFlowDetails();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/CreateWorkFlow")]
        [HttpPost]
        public ServiceResponse CreateWorkFlow([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray WorkFlowSteps = (JArray)jobj["WorkFlowSteps"];
                JArray WorkFlowStepsTasks = (JArray)jobj["WorkFlowStepsTasks"];

                IList<IWorkFlowSteps> listWorkflowSteps = new List<IWorkFlowSteps>();
                if (WorkFlowSteps != null)
                {
                    foreach (var entityPeriodObj in WorkFlowSteps)
                    {

                        //if (entityPeriodObj.Count() > 0)
                        //{
                        //foreach (var arrp in entityPeriodObj)
                        //{
                        IWorkFlowSteps steps = marcomManager.MetadataManager.CreateWorkFlowStepsInstace();
                        IList<IWorkFlowStepPredefinedTasks> listWorkflowTasks = new List<IWorkFlowStepPredefinedTasks>();

                        steps.Id = (int)entityPeriodObj["Id"];
                        steps.WorkFlowID = (int)entityPeriodObj["WorkFlowStepID"];
                        steps.SortOrder = (int)entityPeriodObj["SortOrder"];
                        steps.HelpText = (string)entityPeriodObj["HelpText"];
                        steps.Name = (string)entityPeriodObj["Name"];
                        JArray stepsTasks = (JArray)entityPeriodObj["Tasks"];
                        if (stepsTasks != null)
                        {
                            foreach (var stepVal in stepsTasks)
                            {
                                IWorkFlowStepPredefinedTasks itasks = marcomManager.MetadataManager.CreateWorkFlowStepPredefinedTasksInstace();
                                itasks.TaskID = (int)stepVal;
                                listWorkflowTasks.Add(itasks);
                            }

                        }
                        steps.WorkFlowTasks = listWorkflowTasks;
                        listWorkflowSteps.Add(steps);
                        //}
                        //}
                    }
                }
                else if (WorkFlowSteps == null)
                {
                    listWorkflowSteps = null;
                }
                IList<IWorkFlowStepPredefinedTasks> listWorkflowStepstasks = new List<IWorkFlowStepPredefinedTasks>();
                if (WorkFlowStepsTasks != null)
                {
                    foreach (var arrc in WorkFlowStepsTasks)
                    {
                        IWorkFlowStepPredefinedTasks tasks = marcomManager.MetadataManager.CreateWorkFlowStepPredefinedTasksInstace();
                        tasks.StepID = (int)arrc["StepId"];
                        tasks.TaskID = (int)arrc["TaskID"];
                        listWorkflowStepstasks.Add(tasks);
                    }
                }
                else if (WorkFlowStepsTasks == null)
                {
                    listWorkflowStepstasks = null;
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertWorkFlow((string)jobj["Name"], (string)jobj["Description"], listWorkflowSteps, listWorkflowStepstasks, (int)jobj["WorkFlowID"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }
        [Route("api/Metadata/GetAttributefromDB")]
        [HttpGet]
        public ServiceResponse GetAttributefromDB()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributefromDB();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GetAttributesforDetailFilter")]
        [HttpGet]
        public ServiceResponse GetAttributesforDetailFilter()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributesforDetailFilter();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GettingEntityTypeHierarchyForAdminTree/{EntityTypeID}/{ModuleID}")]
        [HttpGet]
        public ServiceResponse GettingEntityTypeHierarchyForAdminTree(int EntityTypeID, int ModuleID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GettingEntityTypeHierarchyForAdminTree(EntityTypeID, ModuleID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/InsertUpdatePredefinedWorkFlow")]
        [HttpPost]
        public ServiceResponse InsertUpdatePredefinedWorkFlow([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrAttchObj = (JArray)jobj["FileAttachments"];
                result.StatusCode = (int)HttpStatusCode.OK;

                IList<IPredefinedWorflowFileAttachement> listTaskattachment = new List<IPredefinedWorflowFileAttachement>();
                if (arrAttchObj != null)
                {
                    string tempdate = "";
                    DateTime tempd = new DateTime();

                    foreach (var arm in arrAttchObj)
                    {
                        IPredefinedWorflowFileAttachement attachval = marcomManager.MetadataManager.PredefinedWrkflwFileAttachmentService();
                        attachval.Name = (string)arm["Name"];
                        attachval.Fileguid = Guid.NewGuid();
                        attachval.Extension = (string)arm["Extension"];
                        attachval.Size = (long)arm["Size"];
                        attachval.MimeType = (string)arm["MimeType"];
                        attachval.OwnerID = (int)arm["OwnerID"];
                        attachval.VersionNo = (int)arm["VersionNo"];

                        tempdate = (string)arm["Createdon"];


                        tempd = DateTime.ParseExact(tempdate.ToString(), "dd-MM-yyyy", null);
                        string formatted = tempd.ToString("yyyy-MM-dd");

                        tempd = DateTime.ParseExact(formatted.ToString(), "yyyy-MM-dd", null);
                        attachval.Createdon = tempd;
                        listTaskattachment.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listTaskattachment = null;
                }

                result.Response = marcomManager.MetadataManager.InsertUpdatePredefinedWorkTask((string)jobj["Caption"], (string)jobj["Description"], listTaskattachment, (int)jobj["WorkflowType"], (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetPredefinedWorkflowByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetPredefinedWorkflowByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.Response = marcomManager.MetadataManager.GetPredefinedWorkflowByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetWorkFlowDetailsByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetWorkFlowDetailsByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.Response = marcomManager.MetadataManager.GetWorkFlowDetailsByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetWorkFlowTStepByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetWorkFlowTStepByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.Response = marcomManager.MetadataManager.GetWorkFlowTStepByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetWorkFlowStepPredefinedTaskByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetWorkFlowStepPredefinedTaskByID(string ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.Response = marcomManager.MetadataManager.GetWorkFlowStepPredefinedTaskByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/DeleteWorkflowByID/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteWorkflowByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                //IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager((Guid)Session["ID"]);
                //reader = new StreamReader(Request.Body);
                //string strBody = reader.ReadToEnd();
                //JObject jobj = JObject.Parse(strBody);
                //result.StatusCode = (int)HttpStatusCode.OK;
                //result.Response = marcomManager.MetadataManager.DeleteValidation((int)jobj["ID"]);

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.Response = marcomManager.MetadataManager.DeleteWorkflowByID(ID);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/GetPredefinedWorkflowFilesAttchedByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetPredefinedWorkflowFilesAttchedByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.Response = marcomManager.MetadataManager.GetPredefinedWorkflowFilesAttchedByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/DeletePredWorkflowFileByID/{ID}")]
        [HttpDelete]
        public ServiceResponse DeletePredWorkflowFileByID(string ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeletePredWorkflowFileByID(ID);
                //return marcomManager.MetadataManager.DeletePredWorkflowFileByID((string)metadata.ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/DeletePredefinedWorkflowByID/{ID}/{Name}/{Description}/{WorkflowType}")]
        [HttpDelete]
        public ServiceResponse DeletePredefinedWorkflowByID(int ID, string Name, string Description, int WorkflowType)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.Response = marcomManager.MetadataManager.DeletePredefinedWorkflowByID(ID, Name, Description, WorkflowType);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/GetModuleFeatures/{moduleID}")]
        [HttpGet]
        public ServiceResponse GetModuleFeatures(int moduleID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetModuleFeatures(moduleID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetModuleFeaturesForNavigation/{moduleID}")]
        [HttpGet]
        public ServiceResponse GetModuleFeaturesForNavigation(int moduleID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetModuleFeaturesForNavigation(moduleID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/UpdateFeature")]
        [HttpPut]
        public ServiceResponse UpdateFeature([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertUpdateFeature((string)jobj["Caption"], (string)jobj["Description"], (int)jobj["moduleID"], ((int)jobj["IsEnable"] == 0 ? false : true), (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/GetTreeNodeByAttributeID/{AttributeID}")]
        [HttpGet]
        public ServiceResponse GetTreeNodeByAttributeID(int AttributeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetTreeNodeByAttributeID(AttributeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/SetWorkingVersionFlag")]
        [HttpPost]
        public ServiceResponse SetWorkingVersionFlag([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                result.StatusCode = (int)HttpStatusCode.OK;
                MarcomManagerFactory.IsCurrentWorkingXml[tenantres.tenantID] = false;
                if ((int)jobj.First == 0)
                {
                    MarcomManagerFactory.viewOldMetadataVersion[tenantres.tenantID] = true;
                    MarcomManagerFactory.oldMetadataVersionNumber[tenantres.tenantID] = (string)jobj["VersionNo"];
                }
                else if ((int)jobj.First == 1)
                {
                    MarcomManagerFactory.IsWorkingWithCurrentWorkingVersion[tenantres.tenantID] = true;
                    MarcomManagerFactory.viewOldMetadataVersion[tenantres.tenantID] = false;
                }
                else if ((int)jobj.First == 2)
                {
                    MarcomManagerFactory.IsWorkingWithCurrentWorkingVersion[tenantres.tenantID] = false;
                    MarcomManagerFactory.viewOldMetadataVersion[tenantres.tenantID] = false;
                }
                result.Response = "done";
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
                MarcomCache<bool>.SetApplyChangesLock(false);
            }
            return result;
        }
        [Route("api/Metadata/GetMetadataVersion")]
        [HttpGet]
        public ServiceResponse GetMetadataVersion()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetMetadataVersion();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/InsertMetadataVersion")]
        [HttpPost]
        public ServiceResponse InsertMetadataVersion([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertMetadataVersion((int)jobj["ID"], (string)jobj["Name"], (string)jobj["Description"], (int)jobj["selectedMetadataVer"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetTaskFulfillmentEntityTypes")]
        [HttpGet]
        public ServiceResponse GetTaskFulfillmentEntityTypes()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetTaskFulfillmentEntityTypes();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetXmlNodes_CheckIfValueExistsOrNot")]
        [HttpGet]
        public ServiceResponse GetXmlNodes_CheckIfValueExistsOrNot()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetXmlNodes_CheckIfValueExistsOrNot();
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GettingEntityForRootLevel/{IsRootLevel}")]
        [HttpGet]
        public ServiceResponse GettingEntityForRootLevel(bool IsRootLevel)
        {
            result = new ServiceResponse();
            try
            {
                Guid sessionapi = new Guid(((string[])(Request.Headers.GetValues("sessioncookie")))[0]);
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, sessionapi);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GettingEntityForRootLevel(IsRootLevel);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GettingEntityTypeHierarchyForChildActivityType/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GettingEntityTypeHierarchyForChildActivityType(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GettingEntityTypeHierarchyForChildActivityType(EntityTypeID);
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/GettingEntityTypeHierarchyForChildActivityType method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetAttributeRelationByIDs")]
        [HttpPost]
        public ServiceResponse GetAttributeRelationByIDs([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                //IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                //result.StatusCode = (int)HttpStatusCode.OK;
                //result.Response = marcomManager.MetadataManager.GetAttributeRelationByIDs(ID);
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributeRelationByIDs((string)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/InsertUpdateAttributeToAttributeRelations")]
        [HttpPost]
        public ServiceResponse InsertUpdateAttributeToAttributeRelations([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jEntityAttributedata = (JArray)jobj["AttributeData"];

                foreach (var entattrobj in jEntityAttributedata)
                {
                    result.Response = marcomManager.MetadataManager.InsertUpdateAttributeToAttributeRelations((int)jobj["EntityTypeID"], (int)entattrobj["AttributeTypeID"], (int)entattrobj["AttributeID"], (int)entattrobj["AttributeOptionID"], (int)entattrobj["AttributeLevelID"], (string)entattrobj["AttributeToAttributeRelationID"], 0);
                }
            }
            catch (MarcomAccessDeniedException ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/InsertUpdateAttributeToAttributeRelations method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch (Exception ex1)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/InsertUpdateAttributeToAttributeRelations method hits exception as " + ex1, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/DeleteAttributeToAttributeRelation/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteAttributeToAttributeRelation(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //int id = (int)metadata["ID"];
                result.Response = marcomManager.MetadataManager.DeleteAttributeToAttributeRelation(ID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/DeleteAttributeToAttributeRelation method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/DeleteAttributeToAttributeRelation method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/GetAttributeToAttributeRelationsByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetAttributeToAttributeRelationsByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributeToAttributeRelationsByID(ID);
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/GetAttributeToAttributeRelationsByID method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetTreeNodeByEntityID/{AttributeID}/{ParentEntityID}")]
        [HttpGet]
        public ServiceResponse GetTreeNodeByEntityID(int AttributeID, int ParentEntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetTreeNode(AttributeID, false, ParentEntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetAttributeToAttributeRelationsByIDForEntity/{ID}")]
        [HttpGet]
        public ServiceResponse GetAttributeToAttributeRelationsByIDForEntity(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributeToAttributeRelationsByIDForEntity(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetAttributeToAttributeRelationsByIDForEntityInOverView/{EntityTypeID}/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetAttributeToAttributeRelationsByIDForEntityInOverView(int EntityTypeID, int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributeToAttributeRelationsByIDForEntity(EntityTypeID, EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GetAttributeOptionsInAttrToAttrRelations")]
        [HttpPost]
        public ServiceResponse GetAttributeOptionsInAttrToAttrRelations([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                string[] jAttrIDs = Newtonsoft.Json.JsonConvert.DeserializeObject<string[]>(jobj["AttrIDs"].ToString());
                result.Response = marcomManager.MetadataManager.GetAttributeOptionsInAttrToAttrRelations(jAttrIDs);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/EntityTypeStatusOptions")]
        [HttpPost]
        public ServiceResponse EntityTypeStatusOptions([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jr = (JArray)jobj["Options"];
                for (int i = 0; i < jr.Count(); i++)
                {
                    int sortorder = i;
                    result.Response = marcomManager.MetadataManager.InsertUpdateEntityTypeStatusOption((int)jobj["EntityID"], (string)jr[i]["StatusOptions"], sortorder + 1, (int)jr[i]["ID"], (string)jr[i]["ColorCode"]);
                }
            }
            catch (MarcomAccessDeniedException ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/EntityTypeStatusOptions method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/EntityTypeStatusOptions method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/DamTypeFileExtensionOptions")]
        [HttpPost]
        public ServiceResponse DamTypeFileExtensionOptions([FromBody]JObject jobj)
        {

            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jr = (JArray)jobj["Options"];
                for (int i = 0; i < jr.Count(); i++)
                {
                    int sortorder = i;
                    result.Response = marcomManager.MetadataManager.InsertUpdateDamTypeFileExtensionOption((int)jobj["EntityID"], (string)jr[i]["ExtensionOptions"], sortorder + 1, (int)jr[i]["ID"]);
                }
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityStatusOptions/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetEntityStatusOptions(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityStatusOptions(EntityTypeID);
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/GetEntityStatusOptions method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetDamTypeFileExtensionOptions/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetDamTypeFileExtensionOptions(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetDamTypeFileExtensionOptions(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetAllDamTypeFileExtensionOptions")]
        [HttpGet]
        public ServiceResponse GetAllDamTypeFileExtensionOptions()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAllDamTypeFileExtensionOptions();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/DeleteEntityTypeStatusOptions/{EntityTypeID}")]
        [HttpDelete]
        public ServiceResponse DeleteEntityTypeStatusOptions(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //int id = (int)metadata["EntityTypeID"];
                result.Response = marcomManager.MetadataManager.DeleteEntityTypeStatusOptions(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/DeleteDamTypeFileExtensionOptions/{EntityTypeID}")]
        [HttpDelete]
        public ServiceResponse DeleteDamTypeFileExtensionOptions(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                // int id = (int)metadata["EntityTypeID"];
                result.Response = marcomManager.MetadataManager.DeleteDamTypeFileExtensionOptions(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityStatus/{entityTypeID}/{isAdmin}/{entityId}")]
        [HttpGet]
        public ServiceResponse GetEntityStatus(int entityTypeID, bool isAdmin, int entityId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityStatus(entityTypeID, isAdmin, entityId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/RootLevelEntityTypeHierarchy")]
        [HttpGet]
        public ServiceResponse RootLevelEntityTypeHierarchy()
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.RootLevelEntityTypeHierarchy();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/ChildEntityTypeHierarchy")]
        [HttpGet]
        public ServiceResponse ChildEntityTypeHierarchy()
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(GetCookie(Request, "session")));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.ChildEntityTypeHierarchy();
            }
            catch (Exception ex)
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        /// <summary>
        /// Retrieves an individual cookie from the cookies collection
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cookieName"></param>
        /// <returns></returns>
        public string GetCookie(HttpRequestMessage request, string cookieName)
        {
            CookieHeaderValue cookie = request.Headers.GetCookies(cookieName).FirstOrDefault();
            if (cookie != null)
                return cookie[cookieName].Value;

            return null;
        }

        [Route("api/Metadata/MyworkSpaceDetails")]
        [HttpPost]
        public ServiceResponse MyworkSpaceDetails([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                ListSettings lstSettings = XmlSetting.MyWorkSpaceSettingsread("Tree", "MyWorkspaces", 6, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = (string)jr[i]["EntityTypeIDs"];
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        filtervalues.StartDate = (string)jr[i]["StartDate"];
                        filtervalues.EndDate = (string)jr[i]["EndDate"];
                        if (jr[i]["EntityMemberIDs"] != null)
                            filtervalues.EntityMemberIDs = (string)jr[i]["EntityMemberIDs"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.Response = marcomManager.MetadataManager.ListofRecords((int)jobj["StartRowNo"], (int)jobj["MaxNoofRow"], (int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], 1, (int)jobj["EntityID"], true, 0, (int)jobj["Level"], false, (int)jobj["ExpandingEntityID"], true);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetOptionDetailListByIDOptimised/{EntityTypeID}/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetOptionDetailListByIDOptimised(int EntityTypeID, int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetOptionDetailListByIDOptimised(EntityTypeID, EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetWorkspacePath/{EntityID}/{IsWorkspace}")]
        [HttpGet]
        public ServiceResponse GetWorkspacePath(int EntityID, bool IsWorkspace)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetPath(EntityID, IsWorkspace);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/ReportViewCreationAndPushSchema")]
        [HttpGet]
        public ServiceResponse ReportViewCreationAndPushSchema()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.ReportViewCreationAndPushSchema();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetAllModuleFeatures/{moduleID}")]
        [HttpGet]
        public ServiceResponse GetAllModuleFeatures(int moduleID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAllModuleFeatures(moduleID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/InsertUpdateAttributeGroupAndAttributeRelation")]
        [HttpPost]
        public ServiceResponse InsertUpdateAttributeGroupAndAttributeRelation([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                // System.Threading.Thread.Sleep(1500);
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jGroupAttributedata = (JArray)jobj["AttributeData"];
                IList<IAttributeGroupAttributeRelation> listattributerelation = new List<IAttributeGroupAttributeRelation>();
                foreach (var objew in jGroupAttributedata)
                {
                    IAttributeGroupAttributeRelation Objattributerelation = marcomManager.MetadataManager.CreateAttributeRelationInstace();
                    Objattributerelation.ID = (int)objew["ID"];
                    Objattributerelation.AttributeID = (int)objew["AttributeID"];
                    Objattributerelation.Caption = (string)objew["AttributeRelationCaption"];
                    Objattributerelation.SortOrder = (int)objew["SortOrder"];
                    Objattributerelation.ShowAsColumn = (bool)objew["ShowAsColumn"];
                    Objattributerelation.IsSearchable = (bool)objew["IsSearchable"];
                    listattributerelation.Add(Objattributerelation);
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertUpdateAttributeGroupAndAttributeRelation((int)jobj["AttributeGroupID"], (string)jobj["Caption"], (string)jobj["Description"], listattributerelation, (bool)jobj["IsPredefined"]);

            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetAttributeGroup")]
        [HttpGet]
        public ServiceResponse GetAttributeGroup()
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributeGroup();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetAttributeGroupAttributeRelation/{AttributeGroupID}")]
        [HttpGet]
        public ServiceResponse GetAttributeGroupAttributeRelation(int AttributeGroupID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributeGroupAttributeRelation(AttributeGroupID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/DeleteAttributeGroup/{attributegroupid}")]
        [HttpDelete]
        public ServiceResponse DeleteAttributeGroup(int attributegroupid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteAttributeGroup(attributegroupid);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/DeleteAttributeGroupAttributeRelation/{attributeRelationId}")]
        [HttpDelete]
        public ServiceResponse DeleteAttributeGroupAttributeRelation(int attributeRelationId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteAttributeGroupAttributeRelation(attributeRelationId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/InsertUpdateEntityTypeAttributeGroup")]
        [HttpPost]
        public ServiceResponse InsertUpdateEntityTypeAttributeGroup([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jGroupAttributedata = (JArray)jobj["AttributeData"];
                IList<IEntityTypeAttributeGroupRelation> listattributerelation = new List<IEntityTypeAttributeGroupRelation>();
                IList<IAttributeGroupRoleAccess> listattrgrpaccess = new List<IAttributeGroupRoleAccess>();

                foreach (var objew in jGroupAttributedata)
                {
                    IEntityTypeAttributeGroupRelation Objattributerelation = marcomManager.MetadataManager.CreateAttributeGroupRelationInstace();
                    Objattributerelation.ID = (int)objew["ID"];
                    Objattributerelation.EntityTypeID = (int)objew["EntityTypeID"];
                    Objattributerelation.Caption = (string)objew["Caption"];
                    Objattributerelation.AttributeGroupID = (int)objew["AttributeGroupID"];
                    Objattributerelation.LocationType = (int)objew["LocationType"];
                    Objattributerelation.RepresentationType = (bool)objew["RepresentationType"];
                    Objattributerelation.SortOrder = (int)objew["SortOrder"];
                    Objattributerelation.PageSize = (int)objew["PageSize"];
                    Objattributerelation.IsTabInEntityCreation = (bool)objew["IsTabInEntityCreation"];
                    Objattributerelation.IsAttrGrpInheritFromParent = (bool)objew["IsAttrGrpInheritFromParent"];
                    listattributerelation.Add(Objattributerelation);

                    JArray jGroupAccess = (JArray)objew["AttributeGroupGlobalAccess"];

                    if (jGroupAccess != null)
                    {
                        foreach (var itm in jGroupAccess)
                        {
                            IAttributeGroupRoleAccess ObjGrpAccess = marcomManager.MetadataManager.CreateAttributeGroupRoleAccessInstace();
                            ObjGrpAccess.AttributeGroupID = (int)objew["AttributeGroupID"];
                            ObjGrpAccess.EntityTypeID = (int)jobj["EntityTypeID"];
                            ObjGrpAccess.GlobalRoleID = (int)itm;
                            listattrgrpaccess.Add(ObjGrpAccess);
                        }
                    }
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertUpdateEntityTypeAttributeGroup(listattributerelation, listattrgrpaccess, (int)jobj["EntityTypeID"], (string)jobj["GlobalAccessids"]);

            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityTypeAttributeGroupRelation/{entitytypeId}/{EntityID}/{AttributeGroupId}")]
        [HttpGet]
        public ServiceResponse GetEntityTypeAttributeGroupRelation(int entitytypeId, int EntityID, int AttributeGroupId)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityTypeAttributeGroupRelation(entitytypeId, EntityID, AttributeGroupId);
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/GetEntityTypeAttributeGroupRelation method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/DeleteEntityTypeAttributeGroupRelation/{attributegroupId}")]
        [HttpDelete]
        public ServiceResponse DeleteEntityTypeAttributeGroupRelation(int attributegroupId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteEntityTypeAttributeGroupRelation(attributegroupId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetAttributeGroupAttributeOptions/{ID}/{AttributeRecordID}/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetAttributeGroupAttributeOptions(int ID, int AttributeRecordID, int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributeGroupAttributeOptions(ID, EntityID, AttributeRecordID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityAttributesGroupValues/{EntityID}/{EntityTypeID}/{GroupID}/{IsCmsContent}/{pageNo}/{PageSize}")]
        [HttpGet]
        public ServiceResponse GetEntityAttributesGroupValues(int EntityID, int EntityTypeID, int GroupID, bool IsCmsContent, int pageNo = 0, int PageSize = 0)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityAttributesGroupValues(EntityID, EntityTypeID, GroupID, IsCmsContent, pageNo, PageSize);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/DuplicateEntityType")]
        [HttpPost]
        public ServiceResponse DuplicateEntityType([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DuplicateEntityType((int)jobj["EntityTypeID"], (string)jobj["EntityCaption"], (string)jobj["EntityShortDescription"], (string)jobj["Description"], (string)jobj["EntityTypeColorcode"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = null;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityTypeAttributeRelationWithLevelsByIDForUserDetails/{ID}/{ParentID}")]
        [HttpGet]
        public ServiceResponse GetEntityTypeAttributeRelationWithLevelsByIDForUserDetails(int ID, int ParentID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityTypeAttributeRelationWithLevelsByIDForUserDetails(ID, ParentID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetUserDetailsAttributes/{TypeID}/{UserID}")]
        [HttpGet]
        public ServiceResponse GetUserDetailsAttributes(int TypeID, int UserID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetUserDetailsAttributes(TypeID, UserID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetOptionDetailListByIDForMyPage/{ID}/{UserID}")]
        [HttpGet]
        public ServiceResponse GetOptionDetailListByIDForMyPage(int ID, int UserID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetOptionDetailListByIDForMyPage(ID, UserID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetUserRegistrationAttributes")]
        [HttpGet]
        public ServiceResponse GetUserRegistrationAttributes()
        {
            result = new ServiceResponse();
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                BrandSystems.Marcom.Utility.ClsDb objdb = new BrandSystems.Marcom.Utility.ClsDb(tenantres.tenanturl);
                result.StatusCode = (int)HttpStatusCode.OK;
                DataSet ds = new DataSet();
                ds = objdb.GetUserRegistrationAttributes("SELECT mmv.ID FROM MM_MetadataVersion mmv WHERE mmv.[State] = 1", CommandType.Text);


                string mappingfilesPath = System.Configuration.ConfigurationSettings.AppSettings["MarcomPresentation"].ToString() + XmlSetting.GetTenantFilePath(tenantres.tenantID);
                mappingfilesPath = mappingfilesPath + "MetadataXML" + @"\MetadataVersion_V" + ds.Tables[0].Rows[0][0] + ".xml";
                XDocument docx = XDocument.Load(mappingfilesPath);

                var query = (from c in docx.Root.Elements("Attribute_Table").Elements("Attribute")
                             join attr in docx.Root.Elements("EntityTypeAttributeRelation_Table").Elements("EntityTypeAttributeRelation") on Convert.ToInt32(c.Element("ID").Value) equals Convert.ToInt32(attr.Element("AttributeID").Value)
                             where Convert.ToInt32(attr.Element("EntityTypeID").Value) == 12
                             select new
                             {
                                 Caption = c.Element("Caption").Value,
                                 AttributeID = Convert.ToInt16(attr.Element("AttributeID").Value),
                                 AttributeTypeID = Convert.ToInt16(c.Element("AttributeTypeID").Value)
                             });

                JObject jobj = JObject.FromObject(new
                {
                    Table = query
                });

                var json1 = JsonConvert.SerializeObject(jobj);
                result.Response = json1;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;

        }
        [Route("api/Metadata/GetOptionValuesForUserRegistration")]
        [HttpGet]
        public ServiceResponse GetOptionValuesForUserRegistration()
        {
            result = new ServiceResponse();
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                BrandSystems.Marcom.Utility.ClsDb objdb = new BrandSystems.Marcom.Utility.ClsDb(tenantres.tenanturl);
                result.StatusCode = (int)HttpStatusCode.OK;
                DataSet ds = new DataSet();
                ds = objdb.GetUserRegistrationAttributes("SELECT * FROM MM_Option mo", CommandType.Text);

                var query = from row in ds.Tables[0].AsEnumerable()
                            select new
                            {
                                ID = (int)row["ID"],
                                Caption = (string)row["Caption"],
                                AttributeID = (int)row["AttributeID"],
                                SortOrder = (int)row["SortOrder"]
                            };

                JObject jobj = JObject.FromObject(new
                {
                    OptionTable = query
                });

                var json1 = JsonConvert.SerializeObject(jobj);
                result.Response = json1;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;

        }
        [Route("api/Metadata/GetDynamicAttrValidationDetails")]
        [HttpGet]
        public ServiceResponse GetDynamicAttrValidationDetails()
        {
            result = new ServiceResponse();
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                BrandSystems.Marcom.Utility.ClsDb objdb = new BrandSystems.Marcom.Utility.ClsDb(tenantres.tenanturl);
                result.StatusCode = (int)HttpStatusCode.OK;
                DataSet ds = new DataSet();
                ds = objdb.GetUserRegistrationAttributes("SELECT mv.Name,mv.ValueType,mv.ErrorMessage FROM MM_Validation mv WHERE mv.EntityTypeID = 12", CommandType.Text);

                var query = from row in ds.Tables[0].AsEnumerable()
                            select new
                            {
                                Name = (string)row["Name"],
                                ValueType = (string)row["ValueType"],
                                ErrorMessage = (string)row["ErrorMessage"]
                            };

                JObject jobj = JObject.FromObject(new
                {
                    Table = query
                });

                var json1 = JsonConvert.SerializeObject(jobj);
                result.Response = json1;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;

        }
        [Route("api/Metadata/SaveDetailBlockForLevelsFromMyPage")]
        [HttpPost]
        public ServiceResponse SaveDetailBlockForLevelsFromMyPage([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                List<object> obj1 = new List<object>();
                if (jobj["NewValue"].Type == JTokenType.Date)
                {
                    obj1.Add(jobj["NewValue"].ToString());
                }
                else
                {
                    JArray jrnewValue = (JArray)jobj["NewValue"];
                    //dynamic valueList = jrnewValue.Select(jv => (dynamic)jv).ToArray();

                    foreach (var obj in jrnewValue)
                    {
                        if (obj.Type == JTokenType.Boolean)
                        {
                            obj1.Add(Convert.ToBoolean(obj.ToString()));
                        }
                        else if (obj.Type == JTokenType.Date)
                        {
                            obj1.Add(obj.ToString());

                        }
                        else if (obj.Type == JTokenType.Integer)
                        {

                            obj1.Add(int.Parse(obj.ToString()));

                        }
                        else
                            obj1.Add(obj.ToString());
                    }

                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.SaveDetailBlockForLevelsFromMyPage((int)jobj["UserID"], (int)jobj["AttributetypeID"], (int)jobj["AttributeID"], obj1, (int)jobj["Level"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetUserVisiblity")]
        [HttpGet]
        public ServiceResponse GetUserVisiblity()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetUserVisiblity();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/InsertUserVisibleInfo")]
        [HttpPost]
        public ServiceResponse InsertUserVisibleInfo([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                // System.Threading.Thread.Sleep(1500);
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jEntityAttributedata = (JArray)jobj["AttributeData"];

                for (int i = 0; i < jEntityAttributedata.Count(); i++)
                {
                    result.Response = marcomManager.MetadataManager.InsertUpdateEntityTypeAttributeRelation((int)jobj["EntityTypeID"], (int)jEntityAttributedata[i]["AttributeID"], (string)jEntityAttributedata[i]["ValidationID"], (i + 1), (string)jEntityAttributedata[i]["DefaultValue"], ((bool)jEntityAttributedata[i]["InheritFromParent"]), ((bool)jEntityAttributedata[i]["IsReadOnly"]), ((bool)jEntityAttributedata[i]["ChooseFromParentOnly"]), ((bool)jEntityAttributedata[i]["IsValidationNeeded"]), (string)jEntityAttributedata[i]["Caption"], (bool)jEntityAttributedata[i]["IsSystemDefined"], (string)jEntityAttributedata[i]["PlaceHolderValue"], (int)jEntityAttributedata[i]["MinValue"], (int)jEntityAttributedata[i]["MaxValue"], (int)jEntityAttributedata[i]["ID"]);

                }
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/InsertUpdateUserVisibleInfo")]
        [HttpPost]
        public ServiceResponse InsertUpdateUserVisibleInfo([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertUpdateUserVisibleInfo((int)jobj["AttributeId"], (int)jobj["IsEnable"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [HttpPost]
        public ServiceResponse InsertSetUpToolAttributes([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                //---------------> READ ALL THE JSON NODES RELATED TO ATTRIBUTES, ENTITY TYPES AND COSTCENTRE <------------
                JArray arrattributes = (JArray)jobj["metadata"]["attributes"];
                JArray arrenittytypes = (JArray)jobj["metadata"]["worklflowphases"];
                JArray arrcostcentre = (JArray)jobj["metadata"]["costcentres"];
                System.Collections.Hashtable attributeIds = new System.Collections.Hashtable();

                int attid = 0, cntoption = 1, parententitytypeid = 0, childentitytypeid = 0, loopcnt = 1, HierarchyOrder = 1;

                //-------------------> GET TREE SETTINGS FOR DYNAMIC ENTITY <---------------
                var resultTreeobj = marcomManager.CommonManager.GetAdminSettings("Tree", 6);
                JObject jsonTreeobj = JObject.Parse(resultTreeobj.ToString());
                JArray arrTreesettings = new JArray();
                bool treesetting = false;

                if (jsonTreeobj["Tree"]["EntityTypes"].Count() != 0)
                {
                    arrTreesettings = (JArray)jsonTreeobj["Tree"]["EntityTypes"]["EntityType"];
                    treesetting = true;
                }

                //-------------------> GET TREE SETTINGS FOR COST CENTRE <---------------
                var resultCstCntrebj = marcomManager.CommonManager.GetAdminSettings("Tree", 5);
                JObject jsonCstCntreobj = JObject.Parse(resultCstCntrebj.ToString());
                JArray arrCstCntre = new JArray();
                bool treeCstCntre = false;


                // if (jsonCstCntreobj["Tree"]["EntityTypes"].Count() != 0)
                //{
                //   arrCstCntre = (JArray)jsonCstCntreobj["Tree"]["EntityTypes"]["EntityType"];
                //  treeCstCntre = true;
                //}


                //-------------------> GET TREE SETTINGS FOR OBJECTIVE <---------------
                var resultobjective = marcomManager.CommonManager.GetAdminSettings("Tree", 10);
                JObject jsonobjtive = JObject.Parse(resultobjective.ToString());
                JArray arrobjtive = new JArray();
                bool treeobjtive = false;


                // if (jsonobjtive["Tree"]["EntityTypes"].Count() != 0)
                //{
                //   arrobjtive = (JArray)jsonobjtive["Tree"]["EntityTypes"]["EntityType"];
                //  treeobjtive = true;
                //}


                //----------------> CHECK IF ATTRIBUTE EXISTS <-------------
                if (arrattributes.Count > 0)
                {
                    foreach (var objattr in arrattributes)
                    {
                        attid = 0;
                        //---------------------> INSERT ATTRIBUTES <---------------
                        attid = marcomManager.MetadataManager.InsertUpdateAttribute((string)objattr["AttributeName"], (string)objattr["AttributeName"], 3, false, false, 0);
                        attributeIds.Add(attid, (string)objattr["AttributeName"]);
                        if (objattr["AttributeOptions"].Count() > 0)
                        {
                            cntoption = 1;
                            foreach (var objattroption in objattr["AttributeOptions"])
                            {
                                //---------------> INSERT ATTRIBUTE OPTIONS <------------
                                result.Response = marcomManager.MetadataManager.InsertUpdateOption((string)objattroption["AttOptionName"], attid, cntoption, 0);
                                cntoption++;
                            }
                        }
                    }
                }
                //----------> ADD DEFAULT ATTRIBUTES <----------
                attributeIds.Add(75, "MyRoleEntityAccess");
                attributeIds.Add(74, "MyRoleGlobalAccess");
                attributeIds.Add(71, "EntityStatus");
                attributeIds.Add(1, "Fiscal Year");


                if (arrenittytypes.Count > 0)
                {
                    foreach (var objentity in arrenittytypes)
                    {
                        loopcnt = 3;
                        //------------------------> INSERT ENTITY TYPE <---------------
                        parententitytypeid = marcomManager.MetadataManager.InsertUpdateEntityType((string)objentity["workflow"], (string)objentity["workflow"], 3, 2, (string)objentity["shortdescription"], (string)objentity["colorcode"], false, 0, true, 0);
                        arrTreesettings.Add(JObject.Parse("{\"@ID\":\"" + parententitytypeid + "\"}"));

                        //------------------------> INSERT ENTITY OVERALL STATUS <-------------
                        result.Response = marcomManager.MetadataManager.InsertUpdateEntityTypeStatusOption(parententitytypeid, "Active", 1, 0);

                        result.Response = marcomManager.MetadataManager.InsertUpdateEntityTypeAttributeRelation(parententitytypeid, 68, "0", 1, "", false, false, false, false, "Name", false, "", 0, 0, 0);
                        result.Response = marcomManager.MetadataManager.InsertUpdateEntityTypeAttributeRelation(parententitytypeid, 69, "0", 2, "", false, false, false, false, "Owner", false, "", 0, 0, 0);

                        foreach (int attrid in attributeIds.Keys)
                        {
                            //---------------------> INSERT ENTITYTYPE ATTRIBUTE RELATIONS <--------------
                            result.Response = marcomManager.MetadataManager.InsertUpdateEntityTypeAttributeRelation(parententitytypeid, (int)attrid, "0", loopcnt, "", false, false, false, false, (string)attributeIds[attrid], false, "", 0, 0, 0);
                            loopcnt++;
                        }
                        if (objentity["workflowsteps"].Count() > 0)
                        {
                            foreach (var objsubentity in objentity["workflowsteps"])
                            {
                                loopcnt = 3;
                                HierarchyOrder = 1;
                                //------------------------> INSERT CHILD ENTITY TYPE <--------------
                                childentitytypeid = marcomManager.MetadataManager.InsertUpdateEntityType((string)objsubentity["stepname"], (string)objsubentity["stepname"], 3, 2, (string)objsubentity["shortdescription"], (string)objsubentity["GnattColorCode"], false, 0, false, 0);
                                arrTreesettings.Add(JObject.Parse("{\"@ID\":\"" + childentitytypeid + "\"}"));

                                //------------------------> INSERT HIERARCHY OF CHILD ENTITY TYPE <----------------
                                result.Response = marcomManager.MetadataManager.InsertEntityTypeHierarchy((int)parententitytypeid, (int)childentitytypeid, HierarchyOrder, 0);

                                //------------------------> INSERT ENTITY OVERALL STATUS <-------------
                                result.Response = marcomManager.MetadataManager.InsertUpdateEntityTypeStatusOption(childentitytypeid, "Active", 1, 0);

                                result.Response = marcomManager.MetadataManager.InsertUpdateEntityTypeAttributeRelation(childentitytypeid, 68, "0", 1, "", false, false, false, false, "Name", false, "", 0, 0, 0);
                                result.Response = marcomManager.MetadataManager.InsertUpdateEntityTypeAttributeRelation(childentitytypeid, 69, "0", 2, "", false, false, false, false, "Owner", false, "", 0, 0, 0);

                                foreach (int attrid in attributeIds.Keys)
                                {
                                    //---------------------> INSERT ENTITYTYPE ATTRIBUTE RELATIONS <--------------
                                    result.Response = marcomManager.MetadataManager.InsertUpdateEntityTypeAttributeRelation(childentitytypeid, (int)attrid, "0", loopcnt, "", false, false, false, false, (string)attributeIds[attrid], false, "", 0, 0, 0);
                                    loopcnt++;
                                }
                                HierarchyOrder++;
                            }
                        }
                    }
                }



                if (arrattributes.Count > 0 || arrenittytypes.Count > 0)
                {
                    //------------------> MAKE SYSNC TO DB <------------

                    MarcomManagerFactory.IsWorkingWithCurrentWorkingVersion[tenantres.tenantID] = true;
                    result.Response = marcomManager.MetadataManager.SyncToDb();
                }

                //----------------> SAVE TREESETTINGS FOR DYNAMIC ENTITY, COST CENTRE AND OBJECTIVE <-----------------
                if (!treesetting)
                {
                    JArray objarr = new JArray();
                    objarr.Add(JObject.Parse("{\"EntityType\":\"[]\"}"));
                    objarr[0]["EntityType"] = arrTreesettings;
                    jsonTreeobj["Tree"]["EntityTypes"] = objarr[0];
                }
                result.Response = marcomManager.CommonManager.AdminSettingsforRootLevelInsertUpdate(Convert.ToString(jsonTreeobj), "Tree", 6);

                if (!treeCstCntre)
                {
                    JArray objarr = new JArray();
                    objarr.Add(JObject.Parse("{\"EntityType\":\"[]\"}"));
                    arrTreesettings.Add(JObject.Parse("{\"@ID\":\"5\"}"));
                    objarr[0]["EntityType"] = arrTreesettings;
                    jsonCstCntreobj["Tree"]["EntityTypes"] = objarr[0];
                }
                result.Response = marcomManager.CommonManager.AdminSettingsforRootLevelInsertUpdate(Convert.ToString(jsonCstCntreobj), "Tree", 5);

                if (!treeobjtive)
                {
                    JArray objarr = new JArray();
                    objarr.Add(JObject.Parse("{\"EntityType\":\"[]\"}"));
                    arrTreesettings.RemoveAt(arrTreesettings.Count - 1);
                    arrTreesettings.Add(JObject.Parse("{\"@ID\":\"10\"}"));
                    objarr[0]["EntityType"] = arrTreesettings;
                    jsonobjtive["Tree"]["EntityTypes"] = objarr[0];
                }
                result.Response = marcomManager.CommonManager.AdminSettingsforRootLevelInsertUpdate(Convert.ToString(jsonobjtive), "Tree", 10);


                bool rootview = false, ganttview = false, listview = false, workspace = false;
                //-------------------> GET ROOT LEVEL ENTITY ATTRIBUTES <---------------
                var resultRootobj = marcomManager.CommonManager.GetAdminSettings("RootView", 6);
                JObject jsonRootobj = JObject.Parse(resultRootobj.ToString());
                JArray arrRootview = new JArray();
                if (jsonRootobj["RootView"]["Attributes"].Count() != 0)
                {
                    arrRootview = (JArray)jsonRootobj["RootView"]["Attributes"]["Attribute"];
                    rootview = true;
                }

                //-------------------> GET GANTT VIEW ENTITY ATTRIBUTES <---------------
                var resultGanttViewobj = marcomManager.CommonManager.GetAdminSettings("GanttView", 6);
                JObject jsonGanttViewobj = JObject.Parse(resultGanttViewobj.ToString());
                JArray arrGanttView = new JArray();
                if (jsonGanttViewobj["GanttView"]["Attributes"].Count() != 0)
                {
                    arrGanttView = (JArray)jsonGanttViewobj["GanttView"]["Attributes"]["Attribute"];
                    ganttview = true;
                }

                //-------------------> GET LIST VIEW ATTRIBUTES <---------------
                var resultListViewobj = marcomManager.CommonManager.GetAdminSettings("ListView", 6);
                JObject jsonListViewobj = JObject.Parse(resultListViewobj.ToString());
                JArray arrListView = new JArray();
                if (jsonListViewobj["ListView"]["Attributes"].Count() != 0)
                {
                    arrListView = (JArray)jsonListViewobj["ListView"]["Attributes"]["Attribute"];
                    listview = true;
                }

                attributeIds.Add(69, "Owner");

                //-------------------> GET MY WORKSPACES ATTRIBUTES <----------
                var resultMyWorkspacesobj = marcomManager.CommonManager.GetAdminSettings("MyWorkspaces", 6);
                JObject jsonMyWorkspacesobj = JObject.Parse(resultMyWorkspacesobj.ToString());
                JArray arrMyWorkspaces = new JArray();
                if (jsonMyWorkspacesobj["MyWorkspaces"]["Attributes"]["Attribute"].Count() != 0)
                {
                    arrMyWorkspaces = (JArray)jsonMyWorkspacesobj["MyWorkspaces"]["Attributes"]["Attribute"];
                    workspace = true;
                }

                foreach (int attrid in attributeIds.Keys)
                {
                    if (attrid != 68 && attrid != 74 && attrid != 75)
                    {
                        if (!rootview)
                        {
                            if (attrid == 69 || attrid == 71)
                            {
                                arrRootview.Add(JObject.Parse("{\"Id\":\"" + attrid + "\",\"IsOrderBy\":\"false\",\"IsSelect\":\"true\",\"IsFilter\":\"true\",\"Level\":\"0\",\"Type\":\"3\",\"WhereCondition\":\"null\",\"Field\":\"" + attrid + "\",\"DisplayName\":\"" + (string)attributeIds[attrid] + "\",\"IsSpecial\":\"true\"}"));
                            }
                            else
                            {
                                arrRootview.Add(JObject.Parse("{\"Id\":\"" + attrid + "\",\"IsOrderBy\":\"false\",\"IsSelect\":\"true\",\"IsFilter\":\"true\",\"Level\":\"0\",\"Type\":\"3\",\"WhereCondition\":\"null\",\"Field\":\"" + attrid + "\",\"DisplayName\":\"" + (string)attributeIds[attrid] + "\",\"IsSpecial\":\"false\"}"));
                            }
                        }

                        if (!ganttview && attrid == 71)
                        {
                            arrGanttView.Add(JObject.Parse("{\"Id\":\"" + attrid + "\",\"IsOrderBy\":\"false\",\"IsSelect\":\"true\",\"IsFilter\":\"true\",\"Level\":\"0\",\"Type\":\"3\",\"WhereCondition\":\"null\",\"Field\":\"" + attrid + "\",\"DisplayName\":\"" + (string)attributeIds[attrid] + "\",\"IsSpecial\":\"true\"}"));
                        }

                        if (!listview)
                        {
                            if (attrid == 69 || attrid == 71)
                            {
                                arrListView.Add(JObject.Parse("{\"Id\":\"" + attrid + "\",\"IsOrderBy\":\"false\",\"IsSelect\":\"true\",\"IsFilter\":\"true\",\"Level\":\"0\",\"Type\":\"3\",\"WhereCondition\":\"null\",\"Field\":\"" + attrid + "\",\"DisplayName\":\"" + (string)attributeIds[attrid] + "\",\"IsSpecial\":\"true\"}"));
                            }
                            else
                            {
                                arrListView.Add(JObject.Parse("{\"Id\":\"" + attrid + "\",\"IsOrderBy\":\"false\",\"IsSelect\":\"true\",\"IsFilter\":\"true\",\"Level\":\"0\",\"Type\":\"3\",\"WhereCondition\":\"null\",\"Field\":\"" + attrid + "\",\"DisplayName\":\"" + (string)attributeIds[attrid] + "\",\"IsSpecial\":\"false\"}"));
                            }

                        }

                        if (!workspace)
                        {
                            if (attrid == 69 || attrid == 71)
                            {
                                arrMyWorkspaces.Add(JObject.Parse("{\"Id\":\"" + attrid + "\",\"IsOrderBy\":\"false\",\"IsSelect\":\"true\",\"IsFilter\":\"true\",\"Level\":\"0\",\"Type\":\"3\",\"WhereCondition\":\"null\",\"Field\":\"" + attrid + "\",\"DisplayName\":\"" + (string)attributeIds[attrid] + "\",\"IsSpecial\":\"true\"}"));
                            }
                            else
                            {
                                arrMyWorkspaces.Add(JObject.Parse("{\"Id\":\"" + attrid + "\",\"IsOrderBy\":\"false\",\"IsSelect\":\"true\",\"IsFilter\":\"true\",\"Level\":\"0\",\"Type\":\"3\",\"WhereCondition\":\"null\",\"Field\":\"" + attrid + "\",\"DisplayName\":\"" + (string)attributeIds[attrid] + "\",\"IsSpecial\":\"false\"}"));
                            }
                        }
                    }
                }

                //----------------> SAVE ROOTVIEW SETTINGS <-----------------
                if (!rootview)
                {
                    JArray objarr = new JArray();
                    objarr.Add(JObject.Parse("{\"Attribute\":\"[]\"}"));
                    objarr[0]["Attribute"] = arrRootview;
                    jsonRootobj["RootView"]["Attributes"] = objarr[0];

                    result.Response = marcomManager.CommonManager.AdminSettingsforRootLevelInsertUpdate(Convert.ToString(jsonRootobj), "RootView", 6);
                    result.Response = marcomManager.CommonManager.AdminSettingsforRootLevelInsertUpdate(Convert.ToString(jsonRootobj), "RootView", 5);
                }
                //----------------> SAVE GANTTVIEW SETTINGS <-----------------
                if (!ganttview)
                {
                    JArray objarr = new JArray();
                    objarr.Add(JObject.Parse("{\"Attribute\":\"[]\"}"));
                    objarr[0]["Attribute"] = arrGanttView;
                    jsonGanttViewobj["GanttView"]["Attributes"] = objarr[0];

                    result.Response = marcomManager.CommonManager.AdminSettingsforRootLevelInsertUpdate(Convert.ToString(jsonGanttViewobj), "GanttView", 6);
                    result.Response = marcomManager.CommonManager.AdminSettingsforRootLevelInsertUpdate(Convert.ToString(jsonGanttViewobj), "GanttView", 5);
                    result.Response = marcomManager.CommonManager.AdminSettingsforRootLevelInsertUpdate(Convert.ToString(jsonGanttViewobj), "GanttView", 10);
                }
                //----------------> SAVE LISTVIEW SETTINGS <-----------------
                if (!listview)
                {
                    JArray objarr = new JArray();
                    objarr.Add(JObject.Parse("{\"Attribute\":\"[]\"}"));
                    objarr[0]["Attribute"] = arrListView;
                    jsonListViewobj["ListView"]["Attributes"] = objarr[0];
                    result.Response = marcomManager.CommonManager.AdminSettingsforRootLevelInsertUpdate(Convert.ToString(jsonListViewobj), "ListView", 6);
                }
                //----------------> SAVE MY WORKSPACES SETTINGS <-----------------
                if (!workspace)
                {
                    jsonMyWorkspacesobj["MyWorkspaces"]["Attributes"]["Attribute"] = arrMyWorkspaces;
                    result.Response = marcomManager.CommonManager.AdminSettingsforRootLevelInsertUpdate(Convert.ToString(jsonMyWorkspacesobj), "MyWorkspaces", 6);
                }

                if (arrcostcentre.Count > 0)
                {
                    IList<IOption> _iioption = new List<IOption>();
                    _iioption = marcomManager.MetadataManager.GetOption(0);
                    var tempobj = from itm in _iioption where itm.AttributeID == 1 select itm;

                    //-----------> CREATE A LIST OF ATTRIBUTES DATA <---------------
                    foreach (var objcostcntr in arrcostcentre)
                    {
                        IList<IAttributeData> listattributevalues = new List<IAttributeData>();

                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = 69;
                        entityAttr.Caption = "Owner";
                        entityAttr.TypeID = 3;
                        entityAttr.Level = 0;
                        entityAttr.Value = (int)objcostcntr["Owner"];

                        IAttributeData entityAttr1 = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr1.ID = 1;
                        entityAttr1.Caption = "Fiscal Year";
                        entityAttr1.TypeID = 3;
                        entityAttr1.Level = 0;
                        var objfiscal = (from fis in tempobj where fis.Caption == (string)objcostcntr["FiscalYear"] select fis.Id).ToList();
                        entityAttr1.Value = Convert.ToInt32(objfiscal[0].ToString());

                        IAttributeData entityAttr2 = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr2.ID = 59;
                        entityAttr2.Caption = "AssignedAmount";
                        entityAttr2.TypeID = 8;
                        entityAttr2.Level = 0;
                        entityAttr2.Value = (int)objcostcntr["amount"];

                        listattributevalues.Add(entityAttr);
                        listattributevalues.Add(entityAttr1);
                        listattributevalues.Add(entityAttr2);

                        //--------------------------------> INSERT A NEW COST CENTRE <--------------------------
                        result.Response = marcomManager.PlanningManager.CreateCostcentre(5, (string)objcostcntr["CostCentreName"], (int)objcostcntr["amount"], listattributevalues, null, 0, 1, null, null);
                    }
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = 1;
            }
            catch (Exception e)
            {
                result.StatusCode = (int)HttpStatusCode.InternalServerError;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/addFinancialAttribute")]
        [HttpPost]
        public ServiceResponse addFinancialAttribute([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertUpdateFinancialAttribute((string)jobj["Caption"], (string)jobj["Description"], (int)jobj["AttributeTypeID"], ((int)jobj["IsSystemDefined"] == 0 ? false : true), ((int)jobj["IsSpecial"] == 0 ? false : true), (int)jobj["FinAttType"], (int)jobj["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/addFinancialOption")]
        [HttpPost]
        public ServiceResponse addFinancialOption([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jr = (JArray)jobj["Options"];
                var len = jr.Count();
                for (int i = 0; i < len; i++)
                {
                    result.Response = marcomManager.MetadataManager.InsertUpdateFinancialAttrOptions((string)jr[i]["Caption"], (int)jobj["AttributeID"], (int)jr[i]["SortOrder"], (int)jr[i]["ID"]);
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetFinancialAttribute")]
        [HttpGet]
        public ServiceResponse GetFinancialAttribute()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetFinancialAttribute();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/DeleteFinancialAttribute")]
        [HttpPost]
        public ServiceResponse DeleteFinancialAttribute([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jEntityID = (JArray)jobj["ID"];
                foreach (var entityId in jEntityID)
                {
                    result.Response = marcomManager.MetadataManager.DeleteFinancialAttribute((int)entityId);
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/DeleteFinancialOptionByAttributeID/{attributeid}")]
        [HttpDelete]
        public ServiceResponse DeleteFinancialOptionByAttributeID(int attributeid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteFinancialOptionByAttributeID(attributeid);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/UpdateFinancialMetadata")]
        [HttpPost]
        public ServiceResponse UpdateFinancialMetadata([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.UpdateFinancialMetadata((int)jobj["ID"], (string)jobj["Caption"], (string)jobj["Description"], (int)jobj["FinTypeID"], (int)jobj["AttributeTypeID"], ((int)jobj["IsSystemDefined"] == 0 ? false : true), ((int)jobj["IsColumn"] == 0 ? false : true), ((int)jobj["IsToolTip"] == 0 ? false : true), ((int)jobj["IsCommitToolTip"] == 0 ? false : true), (int)jobj["SortOrder"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/DeleteFinMetadata/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteFinMetadata(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                //result.Response = marcomManager.MetadataManager.DeleteFinMetadata((int)metadata.ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/UpdateFinMetadataSortOrder")]
        [HttpPost]
        public ServiceResponse UpdateFinMetadataSortOrder([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.UpdateFinMetadataSortOrder((int)jobj["ID"], (int)jobj["SortOrder"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetFinancialAttributeOptions/{ID}")]
        [HttpGet]
        public ServiceResponse GetFinancialAttributeOptions(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetFinancialAttributeOptions(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/DeleteFinancialOption/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteFinancialOption(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteFinancialOption(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/GettingEntityTypeTreeStructure/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GettingEntityTypeTreeStructure(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GettingEntityTypeTreeStructure(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetSubEntityTypeAccessPermission/{EntityID}/{EntityTypeID}/{moduleID}")]
        [HttpGet]
        public ServiceResponse GetSubEntityTypeAccessPermission(int EntityID, string EntityTypeID, int moduleID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;

                result.Response = marcomManager.MetadataManager.GetSubEntityTypeAccessPermission(EntityID, EntityTypeID, moduleID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/InsertUpdateEntityTypeRoleAccess")]
        [HttpPost]
        public ServiceResponse InsertUpdateEntityTypeRoleAccess([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jr = (JArray)jobj["Options"];
                for (int i = 0; i < jr.Count(); i++)
                {
                    int sortorder = i;
                    result.Response = marcomManager.MetadataManager.InsertUpdateEntityTypeRoleAccess((string)jr[i]["Caption"], (int)jr[i]["EntityTypeID"], (int)jr[i]["EntityRoleID"], (int)jr[i]["ModuleID"], sortorder + 1, (int)jr[i]["ID"]);
                }
            }
            catch (MarcomAccessDeniedException ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/InsertUpdateEntityTypeRoleAccess method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityTypeRoleAcl/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetEntityTypeRoleAcl(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;

                result.Response = marcomManager.MetadataManager.GetEntityTypeRoleAcl(EntityTypeID);
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("commonManager/GetEntityTypeRoleAcl method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/DeleteEntityTypeRoleAcl/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteEntityTypeRoleAcl(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                if (marcomManager.MetadataManager.IsRoleExist(ID) == false)
                {
                    result.Response = marcomManager.MetadataManager.DeleteEntityTypeRoleAcl(ID);
                }
                else
                {
                    result.StatusCode = (int)HttpStatusCode.OK;
                    result.Response = "Exist";
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/GetRoleFeatures/{GlobalRoleID}")]
        [HttpGet]
        public ServiceResponse GetRoleFeatures(int GlobalRoleID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;

                result.Response = marcomManager.MetadataManager.GetRoleFeatures(GlobalRoleID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/SaveUpdateRoleFeatures")]
        [HttpPost]
        public ServiceResponse SaveUpdateRoleFeatures([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jr = (JArray)jobj["GlobalAccessFeatures"];
                for (int i = 0; i < jr.Count(); i++)
                {
                    result.Response = marcomManager.MetadataManager.SaveUpdateRoleFeatures((int)jobj["GlobalRoleID"], (int)jr[i]["ModuleID"], (int)jr[i]["Id"], (bool)jr[i]["IsChecked"] == true ? true : false, (int)jr[i]["GlobalAclID"]);
                }
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetAttributeTreeNodeByEntityID/{AttributeID}/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetAttributeTreeNodeByEntityID(int AttributeID, int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributeTreeNode(AttributeID, EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetDAMEntityTypes")]
        [HttpGet]
        public ServiceResponse GetDAMEntityTypes()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetDAMEntityTypes();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetWorkspacePermission")]
        [HttpGet]
        public ServiceResponse GetWorkspacePermission()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetWorkspacePermission();
            }
            catch (Exception ex)
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetDropDownTreePricingObject/{AttributeID}/{IsInheritFromParent}/{Isfromparent}/{Entityid}/{ParentId}")]
        [HttpGet]
        public ServiceResponse GetDropDownTreePricingObject(int AttributeID, bool IsInheritFromParent, bool Isfromparent, int Entityid, int ParentId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetDropDownTreePricingObjectDetail(AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetDropDownTreePricingObjectFromParentDetail/{AttributeID}/{IsInheritFromParent}/{Isfromparent}/{Entityid}/{ParentId}")]
        [HttpGet]
        public ServiceResponse GetDropDownTreePricingObjectFromParentDetail(int AttributeID, bool IsInheritFromParent, bool Isfromparent, int Entityid, int ParentId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetDropDownTreePricingObjectFromParentDetail(AttributeID, IsInheritFromParent, Isfromparent, Entityid, ParentId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/SaveDetailBlockForLink")]
        [HttpPost]
        public ServiceResponse SaveDetailBlockForLink([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                List<object> obj1 = new List<object>();
                obj1.Add((string)jobj["linkname"].ToString());

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.SaveDetailBlockForLink((int)jobj["EntityID"], (int)jobj["AttributetypeID"], (int)jobj["AttributeID"], (string)jobj["NewUrl"], obj1, (int)jobj["linkType"], (int)jobj["moduleType"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/DamDeleteEntityAttributeRelation/{ID}/{AttributeID}/{EntitytypeID}")]
        [HttpDelete]
        public ServiceResponse DamDeleteEntityAttributeRelation(int ID, int AttributeID, int EntitytypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //int id = (int)metadata["ID"];
                result.Response = marcomManager.MetadataManager.DamDeleteEntityAttributeRelation(ID, AttributeID, EntitytypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/DeleteEntitytypeAttributeGrpAccessRole/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteEntitytypeAttributeGrpAccessRole(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //int id = (int)metadata["ID"];
                result.Response = marcomManager.MetadataManager.DeleteEntitytypeAttributeGrpAccessRole(ID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/DeleteEntitytypeAttributeGrpAccessRole method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/DeleteEntitytypeAttributeGrpAccessRole method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/GetAttributeGroupImportedFileColumnName")]
        [HttpPost]
        public ServiceResponse GetAttributeGroupImportedFileColumnName([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                string FileID = (string)jobj["FileID"];
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributeGroupImportedFileColumnName(FileID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/InsertImportedAttributeGroupData")]
        [HttpPost]
        public ServiceResponse InsertImportedAttributeGroupData([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray jrnewValue = (JArray)jobj["BindedLabelAndColumn"];

                List<object> obj1 = new List<object>();
                for (int i = 0; i < jrnewValue.Count; i++)
                {
                    obj1.Add(new { AttributeID = (string)jrnewValue[i]["AttributeID"], AttributeType = (string)jrnewValue[i]["AttributeType"], Level = (int)jrnewValue[i]["Level"], AttributeCaption = (string)jrnewValue[i]["AttributeCaption"], ColumnName = (string)jrnewValue[i]["ColumnName"], Value = (string)jrnewValue[i]["Value"] });
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertImportedAttributeGroupData(obj1, (int)jobj["EntityID"], (string)jobj["AttributeGrpName"], (string)jobj["FileName"], (int)jobj["AttributeGroupID"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/FetchEntityStatusTree/{EntityID}")]
        [HttpGet]
        public ServiceResponse FetchEntityStatusTree(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.FetchEntityStatusTree(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetTaskEntityType")]
        [HttpGet]
        public ServiceResponse GetTaskEntityType()
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetTaskEntityType();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetTaskTypes")]
        [HttpGet]
        public ServiceResponse GetTaskTypes()
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetTaskTypes();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityTaskType/{EntityTypeID}")]
        [HttpGet]
        public ServiceResponse GetEntityTaskType(int EntityTypeID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityTaskType(EntityTypeID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                // result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/InsertUpdateEntityTaskType")]
        [HttpPost]
        public ServiceResponse InsertUpdateEntityTaskType([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertUpdateEntityTaskType((int)jobj["EntityTypeID"], (int)jobj["taskType"], (int)jobj["ID"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityAttributesGroupLabelNames/{EntityID}/{GroupID}")]
        [HttpGet]
        public ServiceResponse GetEntityAttributesGroupLabelNames(int EntityID, int GroupID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityAttributesGroupLabelNames(EntityID, GroupID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/CalenderRootLevel")]
        [HttpPost]
        public ServiceResponse CalenderRootLevel([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                ListSettings lstSettings = XmlSetting.ListSettings("RootView", 35, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = "";
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.Response = marcomManager.MetadataManager.ListofRecords((int)jobj["StartRowNo"], (int)jobj["MaxNoofRow"], (int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], 1, (int)jobj["EntityID"], false, (int)jobj["UserID"], (int)jobj["Level"], false, 0);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/CalenderDetail")]
        [HttpPost]
        public ServiceResponse CalenderDetail([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                ListSettings lstSettings = XmlSetting.ListSettings("Tree", 35, tenantres.tenantID);
                lstSettings.Attributes = XmlSetting.ListGanttAttributes(35, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = (string)jr[i]["EntityTypeIDs"];
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        filtervalues.StartDate = (string)jr[i]["StartDate"];
                        filtervalues.EndDate = (string)jr[i]["EndDate"];
                        if (jr[i]["EntityMemberIDs"] != null)
                            filtervalues.EntityMemberIDs = (string)jr[i]["EntityMemberIDs"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.Response = marcomManager.MetadataManager.ListofRecords((int)jobj["StartRowNo"], (int)jobj["MaxNoofRow"], (int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], 7, (int)jobj["EntityID"], false, 0, (int)jobj["Level"], false, (int)jobj["ExpandingEntityID"], false, 35, false);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/GetTagOptionList/{IsAdmin}")]
        [HttpGet]
        public ServiceResponse GetTagOptionList(bool IsAdmin)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetTagOptionList(IsAdmin);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/InsertUpdateTagOption")]
        [HttpPost]
        public ServiceResponse InsertUpdateTagOption([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jr = (JArray)jobj["TagOptions"];
                var len = jr.Count();
                for (int i = 0; i < len; i++)
                {
                    result.Response = marcomManager.MetadataManager.InsertUpdateTagOption((string)jr[i]["Caption"], (int)jobj["AttributeID"], (int)jr[i]["SortOrder"], (int)jr[i]["ID"], (bool)jobj["IsforAdmin"], (int)jobj["TotalPopulartags"]);
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/DeleteTagOption/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteTagOption(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteTagOption(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/AssetAccess/")]
        [HttpPost]
        public ServiceResponse AssetAccess([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //JArray jr = (JArray)jobj["Options"];
                //var len = jr.Count();
                result.Response = marcomManager.MetadataManager.InsertAssetAccessOption((string)jobj["Caption"], 1, (int)jobj["Id"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/DeleteAssetAccessOption/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteAssetAccessOption(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteAssetAccessOption(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/GetAssestAccessSaved")]
        [HttpGet]
        public ServiceResponse GetAssestAccessSaved()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAssestAccessSaved();

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GetAssignedAssetRoleIDs/{AssetId}")]
        [HttpGet]
        public ServiceResponse GetAssignedAssetRoleIDs(int AssetId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAssignedAssetRoleIDs(AssetId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GetAssignedAssetRoleName/{RoleId}")]
        [HttpGet]
        public ServiceResponse GetAssignedAssetRoleName(int RoleId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAssignedAssetRoleName(RoleId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GetAssetAccess")]
        [HttpGet]
        public ServiceResponse GetAssetAccess()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAssetAccess();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;

            }
            return result;
        }
        [Route("api/Metadata/ExportMetadataSettingsXml")]
        [HttpPost]
        public ServiceResponse ExportMetadataSettingsXml([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.ExportMetadataSettingsXml((int)jobj["MetadataVerId"], (string)jobj["VersionName"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/ImportMetadataSettingsXml")]
        [HttpPost]
        public ServiceResponse ImportMetadataSettingsXml([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                Dictionary<string, bool> listofimporttypes = new Dictionary<string, bool>();
                JObject entitytypestodo = (JObject)jobj["ImportTypes"];
                if (entitytypestodo != null)
                {
                    foreach (var obj in entitytypestodo)
                    {
                        listofimporttypes.Add(obj.Key, (bool)obj.Value);
                    }
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.ImportMetadataSettingsXml((int)jobj["MetadataVerId"], listofimporttypes, (string)jobj["ImportFileID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/CostCentreTreeDetail")]
        [HttpPost]
        public ServiceResponse CostCentreTreeDetail([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                ListSettings lstSettings = XmlSetting.ListSettings("Tree", 5, tenantres.tenantID);
                lstSettings.Attributes = XmlSetting.ListGanttAttributes(5, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = (string)jr[i]["EntityTypeIDs"];
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        filtervalues.StartDate = (string)jr[i]["StartDate"];
                        filtervalues.EndDate = (string)jr[i]["EndDate"];
                        if (jr[i]["EntityMemberIDs"] != null)
                            filtervalues.EntityMemberIDs = (string)jr[i]["EntityMemberIDs"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.Response = marcomManager.MetadataManager.ListofRecords((int)jobj["StartRowNo"], (int)jobj["MaxNoofRow"], (int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], (int)jobj["FilterType"], (int)jobj["EntityID"], true, 0, (int)jobj["Level"], false, (int)jobj["ExpandingEntityID"], false, 5, false);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/SetIsCurrentWorkingXml")]
        [HttpPost]
        public ServiceResponse SetIsCurrentWorkingXml()
        {
            result = new ServiceResponse();
            try
            {
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                MarcomManagerFactory.IsCurrentWorkingXml[tenantres.tenantID] = true;
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = "true";
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/GetOwnerName/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetOwnerName(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetOwnerName(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/getTreeCount/{ID}")]
        [HttpGet]
        public ServiceResponse getTreeCount(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.getTree(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/CalenderDetailReport")]
        [HttpPost]
        public ServiceResponse CalenderDetailReport([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["IDArr"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                ListSettings lstSettings = XmlSetting.ListSettings("Tree", 35, tenantres.tenantID);
                lstSettings.Attributes = XmlSetting.ListGanttAttributes(35, tenantres.tenantID);
                IList<IFiltersettingsValues> iFiltervalues = new List<IFiltersettingsValues>();
                if (jobj["FilterAttributes"] != null)
                {
                    //var FilterAttr = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IFiltersettingsValues>>(jobj["FilterAttributes"].ToString());
                    JArray jr = (JArray)jobj["FilterAttributes"];
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        int AttrId = (int)jr[i]["AttributeID"];
                        FiltersettingsValues filtervalues = new FiltersettingsValues();
                        filtervalues.AttributeId = (int)jr[i]["AttributeID"];
                        filtervalues.AttributeTypeId = (int)jr[i]["AttributeTypeId"];
                        filtervalues.EntityTypeIDs = (string)jr[i]["EntityTypeIDs"];
                        filtervalues.FilterId = 0;
                        filtervalues.Id = 0;
                        filtervalues.Level = (int)jr[i]["Level"];
                        filtervalues.Value = (int)jr[i]["SelectedValue"];
                        filtervalues.Keyword = (string)jr[i]["Keyword"];
                        filtervalues.StartDate = (string)jr[i]["StartDate"];
                        filtervalues.EndDate = (string)jr[i]["EndDate"];
                        if (jr[i]["EntityMemberIDs"] != null)
                            filtervalues.EntityMemberIDs = (string)jr[i]["EntityMemberIDs"];
                        iFiltervalues.Add(filtervalues);
                    }
                }
                result.Response = marcomManager.MetadataManager.ListofReportRecords((int)jobj["FilterID"], iFiltervalues, Idarr, ((string)jobj["SortOrderColumn"] == "null" ? "" : (string)jobj["SortOrderColumn"]), (bool)jobj["IsDesc"], lstSettings, (bool)jobj["IncludeChildren"], 7, (int)jobj["EntityID"], false, 0, (int)jobj["Level"], false, (int)jobj["ExpandingEntityID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/metadata/getParentID/{entityID}")]
        [HttpGet]
        public ServiceResponse getParentID(int entityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.getParentID(entityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;

        }

        [Route("api/Metadata/GetAttributeSequenceByID/{ID}")]
        [HttpGet]
        public ServiceResponse GetAttributeSequenceByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttributeSequenceByID(ID, true);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GetEntityGlobalAccessInDetail/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetEntityGlobalAccessInDetail(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityGlobalAccessInDetail(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/InsertEntityGlobalAccessInDetail/")]
        [HttpPost]
        public ServiceResponse InsertEntityGlobalAccessInDetail([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray jr = (JArray)jobj["GlobalRoleIDs"];
                var len = jr.Count();
                int[] GlobalRoleID = new int[len];

                for (int i = 0; i < jr.Count; i++)
                {
                    GlobalRoleID[i] = (int)jr[i];
                }

                result.Response = marcomManager.MetadataManager.InsertEntityGlobalAccessInDetail((int)jobj["EntityID"], GlobalRoleID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/GettingFilterEntityMember")]
        [HttpPost]
        public ServiceResponse GettingFilterEntityMember([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                JArray jEntityID = (JArray)jobj["IDList"];
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GettingFilterEntityMember((int)jobj["TypeID"], (String)jobj["FilterType"], (int)jobj["OptionFrom"], jEntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/GetTaskLinkedToEntity/{ID}")]
        [HttpGet]
        public ServiceResponse GetTaskLinkedToEntity(int ID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetTaskLinkedToEntity(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/GetListOfCmsCustomFont")]
        [HttpGet]
        public ServiceResponse GetListOfCmsCustomFont()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetListOfCmsCustomFont();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/InsertUpdateCmsCustomFont")]
        [HttpPost]
        public ServiceResponse InsertUpdateCmsCustomFont([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                result.Response = marcomManager.MetadataManager.InsertUpdateCmsCustomFont((string)jobj["customfonts"], (string)jobj["kitId"], (int)jobj["providerId"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/DeleteCmsCustomFont/{ProviderId}")]
        [HttpDelete]
        public ServiceResponse DeleteCmsCustomFont(int ProviderId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteCmsCustomFont(ProviderId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/InsertUpdateCmsCustomStyle")]
        [HttpPost]
        public ServiceResponse InsertUpdateCmsCustomStyle([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                result.Response = marcomManager.MetadataManager.InsertUpdateCmsCustomStyle((int)jobj["Id"], (string)jobj["ClassName"], (string)jobj["CssCode"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/GetListOfCmsCustomStyle")]
        [HttpGet]
        public ServiceResponse GetListOfCmsCustomStyle()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetListOfCmsCustomStyle();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/DeleteCmsCustomStyle/{Id}")]
        [HttpDelete]
        public ServiceResponse DeleteCmsCustomStyle(int Id)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteCmsCustomStyle(Id);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/InsertUpdateCmsTreeStyleSettings")]
        [HttpPost]
        public ServiceResponse InsertUpdateCmsTreeStyleSettings([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                result.Response = marcomManager.MetadataManager.InsertUpdateCmsTreeStyleSettings((string)jobj["BackgroundClr"], (string)jobj["ForegroundClr"], (string)jobj["BorderClr"], (string)jobj["LeftborderHlghtClr"], (string)jobj["SelBackgroundClr"], (string)jobj["MouseOvrBackgroundClr"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/GetCmsTreeStyle")]
        [HttpGet]
        public ServiceResponse GetCmsTreeStyle()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetCmsTreeStyle();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = "";
            }
            return result;
        }

        [HttpPost]
        public ServiceResponse SaveImageFromBaseFormat([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.SaveImageFromBaseFormat((string)jobj["imageinbase"]);
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/EntityType method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/CheckBeforeRemovechecklist/{ID}")]
        [HttpGet]
        public ServiceResponse CheckBeforeRemovechecklist(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.MetadataManager.CheckBeforeRemovechecklist(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/GetApprovalRoles")]
        [HttpGet]
        public ServiceResponse GetApprovalRoles()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetApprovalRoles();

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/InsertApprovalRole/")]
        [HttpPost]
        public ServiceResponse InsertApprovalRole([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                //JArray jr = (JArray)jobj["Options"];
                //var len = jr.Count();
                result.Response = marcomManager.MetadataManager.InsertApprovalRole((int)jobj["Id"], (string)jobj["Caption"], (string)jobj["Description"]);
            }

            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/DeleteApprovalRole/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteApprovalRole(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.DeleteApprovalRole(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Metadata/GetEntityTypeAttributes")]
        [HttpGet]
        public ServiceResponse GetEntityTypeAttributes()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityTypeAttributes();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/updateentitytypehelptext")]
        [HttpPost]
        public ServiceResponse updateentitytypehelptext([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;

                JArray selectedentitytypes = (JArray)jobj["selectedentitytypes"];
                IList<object> listassetidfolderid = new List<object>();
                IList<object> AssetidlistObj = new List<object>();
                if (selectedentitytypes != null)
                {
                    foreach (var objAssetIdFolderId in selectedentitytypes)
                    {
                        int EntityTypeId = (int)objAssetIdFolderId["EntityTypeId"];
                        int AttributeID = (int)objAssetIdFolderId["AttributeID"];
                        string HelptextDecsription = (string)objAssetIdFolderId["HelptextDecsription"];
                        bool IsHelptextEnabled = (bool)objAssetIdFolderId["IsHelptextEnabled"];
                        result.Response = marcomManager.MetadataManager.updateentitytypehelptext(EntityTypeId, AttributeID, HelptextDecsription, IsHelptextEnabled);
                    }
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/GetAllFinancialAttribute")]
        [HttpGet]
        public ServiceResponse GetAllFinancialAttribute()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcommanager.MetadataManager.GetAllFinancialAttribute();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Metadata/UpdateFinancialHelptext")]
        [HttpPost]
        public ServiceResponse UpdateFinancialHelptext([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                JArray selectedfinancialattr = (JArray)jobj["selectedfinancialattr"];

                if (selectedfinancialattr != null)
                {
                    foreach (var selectedfinAttr in selectedfinancialattr)
                    {
                        int Id = 0;

                        string attrID = "";

                        attrID = (string)selectedfinAttr["ID"];
                        if ((bool)selectedfinAttr["isfromtable"])
                        {
                            Id = (int)selectedfinAttr["ID"];
                        }

                        string HelptextDescription = (string)selectedfinAttr["HelptextDecsription"];

                        bool isfromtable = (bool)selectedfinAttr["isfromtable"];

                        result.Response = marcomManager.MetadataManager.UpdateFinancialHelptext(Id, ((int)selectedfinAttr["IsHelptextEnabled"] == 0 ? false : true), HelptextDescription, isfromtable, attrID);

                    }
                }
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }


        [Route("api/Metadata/ExportEntityHelpTextListtoExcel")]
        [HttpGet]
        //public ServiceResponse ExportTaskListtoExcel(int TaskListID, string NewGuid, string TaskLibName)
        public ServiceResponse ExportEntityHelpTextListtoExcel()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                TenantResolver tenantres = new TenantResolver(Request.Headers.Referrer.Host);
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.ExportEntityHelpTextListtoExcel(tenantres.tenantID);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Metadata/GetPreDefinedAttributeGroupAttributeOptions")]
        [HttpPost]
        public ServiceResponse GetPreDefinedAttributeGroupAttributeOptions([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {

                JArray jr = (JArray)jobj["searchattributes"];
                int[] Idarr = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["SelectedIds"].ToString());
                IList<object> searchvalues = new List<object>();
                bool IsSelectAll = Convert.ToBoolean(jobj["SelectAll"]);
                bool Isfromsave = Convert.ToBoolean(jobj["IsFromSave"]);
                if (jr.Count() > 0)
                {
                    for (int i = 0; i < jr.Count(); i++)
                    {
                        searchvalues.Add(new
                        {
                            AttributeId = (int)jr[i]["AttributeID"],
                            AttributeTypeId = (int)jr[i]["AttributeTypeId"],
                            Level = (int)jr[i]["Level"],
                            Value = (string)jr[i]["SelectedValue"],
                            SelectedText = (string)jr[i]["SelectedText"]
                        });
                    }
                }
                else
                    searchvalues = null;

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetPreDefinedAttributeGroupAttributeOptions((int)jobj["ID"], (int)jobj["entityId"], (int)jobj["EntityTypeID"], searchvalues, Isfromsave, Idarr, jobj["pageNo"] == null ? 0 : (int)jobj["pageNo"], jobj["PageSize"] == null ? 0 : (int)jobj["PageSize"], (string)jobj["sortColumnBy"], IsSelectAll);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GetAttrGroupSearchAttributes/{GroupID}")]
        [HttpGet]
        public ServiceResponse GetAttrGroupSearchAttributes(int GroupID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetAttrGroupSearchAttributes(GroupID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/GetEntityAttributesGroupHeaders/{EntityID}/{EntityTypeID}/{GroupID}")]
        [HttpGet]
        public ServiceResponse GetEntityAttributesGroupHeaders(int EntityID, int EntityTypeID, int GroupID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetEntityAttributesGroupHeaders(EntityID, EntityTypeID, GroupID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/InsertEntityHelpTextImport")]
        [HttpPost]
        public ServiceResponse InsertEntityHelpTextImport([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertEntityHelpTextImport((string)jobj["FileImport"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/getEntityToSetOverallStauts/{ModuleID}")]
        [HttpGet]
        public ServiceResponse getEntityToSetOverallStauts(int ModuleID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.getEntityToSetOverallStauts(ModuleID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;

        }
        [Route("api/Metadata/getCurrentEntityStatus/{EntityID}")]
        [HttpGet]
        public ServiceResponse getCurrentEntityStatus(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.getCurrentEntityStatus(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }
        [Route("api/Metadata/updateEntityOverviewStatus/{EntityTypeID}/{EntityID}")]
        [HttpGet]
        public ServiceResponse updateEntityOverviewStatus(int EntityTypeID, int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.updateEntityOverviewStatus(EntityTypeID, EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }

        [Route("api/Metadata/GetObjectiveEntityType/{IsAdmin}")]
        [HttpGet]
        public ServiceResponse GetObjectiveEntityType(bool IsAdmin)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetObjectiveEntityType(IsAdmin);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/InsertUpdateEntityObjectiveType")]
        [HttpPost]
        public ServiceResponse InsertUpdateEntityObjectiveType([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InsertUpdateEntityObjectiveType((int)jobj["EntityTypeID"], (int)jobj["ObjectiveTypeId"], (int)jobj["ID"], (int)jobj["objectiveBaseTypeId"], (int)jobj["unitId"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Metadata/GetObjectiveTypeAttributeByEntityType/{EntityTypeID}/{IsAdmin}")]
        [HttpGet]
        public ServiceResponse GetObjectiveTypeAttributeByEntityType(int EntityTypeID, bool IsAdmin)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetObjectiveTypeAttributeByEntityType(EntityTypeID, IsAdmin);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/getPlanandObjTypes/")]
        [HttpGet]
        public ServiceResponse getPlanandObjTypes()
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.getPlanandObjTypes();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/Metadata/InheritParentAttributeGroupRecords/{parentId}/{EntityTypeID}/{AttributeGroupId}")]
        [HttpGet]
        public ServiceResponse InheritParentAttributeGroupRecords(int parentId, int EntityTypeID, int AttributeGroupId)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.InheritParentAttributeGroupRecords(parentId, EntityTypeID, AttributeGroupId);
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/InheritParentAttributeGroupRecords method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Metadata/AttributeDetailsbyID/{AttrID}")]
        [HttpGet]
        public ServiceResponse AttributeDetailsbyID(int AttrID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.AttributeDetailsbyID(AttrID);
            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("MetadataManager/AttributeDetailsbyID method hits exception as " + ex, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/metadata/GetMetadataLockStatus")]
        [HttpGet]
        public ServiceResponse GetMetadataLockStatus()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.GetMetadataLockStatus();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/metadata/SetMetadataLockStatus/{status}")]
        [HttpGet]
        public ServiceResponse SetMetadataLockStatus(bool status)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.SetMetadataLockStatus(status);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }

        [Route("api/metadata/ClearCache")]
        [HttpGet]
        public ServiceResponse ClearCache()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.MetadataManager.ClearCache();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
    }

}