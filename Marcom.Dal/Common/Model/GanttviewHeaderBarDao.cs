﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Common.Model
{

    /// <summary>
    /// GanttviewHeaderBarDao object for table 'CM_GanttviewHeaderBar'.
    /// </summary>
    public partial class GanttviewHeaderBarDao : BaseDao, ICloneable
    {

        #region Member Variables

        protected int _id;
        protected string _name;
        protected DateTime _startdate;
        protected DateTime _enddate;
        protected string _description;
        protected string _colorcode;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public GanttviewHeaderBarDao() { }

        public GanttviewHeaderBarDao(string pName, DateTime pStartdate, DateTime pEndDate, string pDescription, string pcolorcode)
        {
            this._name = pName;
            this._startdate = pStartdate;
            this._enddate = pEndDate;
            this._description = pDescription;
            this._colorcode = pcolorcode;
        }
        #endregion

        #region Public Properties


        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual string Name
        {
            get { return _name; }
            set { _bIsChanged |= (_name != value); _name = value; }

        }

        public virtual DateTime Startdate
        {
            get { return _startdate; }
            set { _bIsChanged |= (_startdate != value); _startdate = value; }

        }

        public virtual DateTime EndDate
        {
            get { return _enddate; }
            set { _bIsChanged |= (_enddate != value); _enddate = value; }

        }

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }

        public virtual string ColorCode
        {
            get { return _colorcode; }
            set { _bIsChanged |= (_colorcode != value); _colorcode = value; }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string Id = "Id";
            public const string GanttviewHeaderBarDao = "GanttviewHeaderBarDao";
            public const string Name = "Name";
            public const string Startdate = "Startdate";
            public const string EndDate = "EndDate";
            public const string Description = "Description";
            public const string ColorCode = "ColorCode";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string Id = "Id";
            public const string GanttviewHeaderBarDao = "GanttviewHeaderBarDao";
            public const string Name = "Name";
            public const string Startdate = "Startdate";
            public const string EndDate = "EndDate";
            public const string Description = "Description";
            public const string ColorCode = "ColorCode";
        }

        #endregion

    }
}
