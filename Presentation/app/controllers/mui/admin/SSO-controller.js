﻿(function (ng, app) {
    "use strict";

    function muiadminSSOCntrl($scope, $location, $resource, $timeout, $translate, AdminService) {
        $scope.select2Options = {
            tags: [],
            multiple: true,
            simple_tags: false,
            minimumInputLength: 1,
            formatResult: function (item) {
                return item.text;
            },
            formatSelection: function (item) {
                return item.text;
            }
        }
        $scope.Algorithms = [];
        $scope.PaddingModes = [];
        $scope.CipherModes = [];
        $scope.SSOTokenModes = [];
        $scope.usergroup = [];
        $scope.gobalaccess = [];
        loadssodetails();

        function loadssodetails() {
            AdminService.GetSSODetails().then(function (res) {
                var details = res.Response;
                $scope.key = res.Response[0].key;
                $scope.IV = res.Response[0].IV;
                $scope.Algorithm = res.Response[0].AlgorithmValue;
                $scope.PaddingMode = res.Response[0].PaddingModeValue;
                $scope.CipherMode = res.Response[0].CipherModeValue;
                $scope.SSOTokenMode = res.Response[0].SSOTokenModeValue;
                var groupvals = res.Response[0].UserGroupsvalue.split(',');
                $scope.usergroup = groupvals;
                $scope.ClientIntranetUrl = res.Response[0].ClientIntranetUrl;
                $scope.Usergroups = res.Response[0].UserGroupsoption;
                $scope.Algorithms = res.Response[0].Algorithmoption;
                $scope.PaddingModes = res.Response[0].PaddingModeoption;
                $scope.CipherModes = res.Response[0].CipherModeoption;
                $scope.SSOTokenModes = res.Response[0].SSOTokenModeoption;
                $scope.SSOTimeDifference = res.Response[0].SSOTimeDifference;
                $scope.SSOaccessRoles = res.Response[0].SSOAccessRoles;
                if (res.Response[0].globalaccessRole != null) {
                    $scope.globalaccess = res.Response[0].globalaccessRole.split(',');
                } else $scope.globalaccess = res.Response[0].globalaccessRole
                $scope.SAMLroles = [];
                var samlroles = res.Response[0].SAMLroles;
                if (samlroles != null) {
                    for (var i = 0; i < samlroles.length; i++) {
                        $scope.SAMLroles.push({ id: samlroles[i], text: samlroles[i] })
                    }
                }
                
            });
        }
        $scope.UpdateSSoSettings = function () {
            var updatesso = {};
            updatesso.key = $scope.key;
            updatesso.iv = $scope.IV;
            updatesso.algo = $('#Algorithm option:selected').val();
            updatesso.paddingmode = $('#PaddingMode option:selected').val();
            updatesso.ciphermode = $('#CipherMode option:selected').val();
            updatesso.tokenmode = $('#SSOTokenMode option:selected').val();
            updatesso.ClientIntranetUrl = $scope.ClientIntranetUrl;
            var str = '';
            var glAccess = '';
            for (var i = 0; i < $scope.usergroup.length; i++) {
                str = str + $scope.usergroup[i] + ',';
            }
            for (var i = 0; i < $scope.globalaccess.length; i++) {
                glAccess = glAccess + $scope.globalaccess[i] + ',';
            }
            str = str.substring(0, str.length - 1);
            glAccess = glAccess.substring(0, glAccess.length - 1);
            updatesso.UserGroupsvalue = str;
            updatesso.globalAccessRoles = glAccess;
            updatesso.SAMLroles = $scope.SAMLroles;
            updatesso.SSOTimeDifference = $scope.SSOTimeDifference;
            AdminService.UpdateSSOSettings(updatesso).then(function (updatess) {
                if (updatess.Response == true) {
                    NotifySuccess($translate.instant('LanguageContents.Res_4763.Caption'));
                    loadssodetails();
                }
            });
        }
    }
    app.controller("mui.admin.SSOCntrl", ['$scope', '$location', '$resource', '$timeout', '$translate', 'AdminService', muiadminSSOCntrl]);
})(angular, app);