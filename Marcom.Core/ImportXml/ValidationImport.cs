/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.ImportXml
{

	/// <summary>
    /// Validation object for table 'MM_Validation'.
	/// </summary>

    internal class ValidationImport
	{
		#region Member Variables

        protected int _newid;
		protected int _id;
        protected int _entitytypeid;
        protected int _relationshipid;
        protected string _name;
        protected string _valuetype;
        protected string _errormessage;
        protected string _value;
		#endregion
		
		#region Constructors
		public ValidationImport() {}

        public ValidationImport(int pId, int pentitytypeid, int prelationshipid, string pname, string pvaluetype, string perrormessage, string pvalue, int pNewId)
		{
            this._id = pId;
            this._name = pname;
            this._entitytypeid = pentitytypeid;
            this._relationshipid = prelationshipid;
            this._valuetype = pvaluetype;
            this._errormessage = perrormessage;
            this._value = pvalue;
            this._newid = pNewId;
		}

        public ValidationImport(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}

        public int NewId
        {
            get { return _newid; }
            set { _newid = value; }

        }

        public virtual int EntityTypeID
        {
            get { return _entitytypeid; }
            set
            {
                _entitytypeid = value;
            }

        }

        public virtual int RelationShipID
        {
            get { return _relationshipid; }
            set
            {
                _relationshipid = value;
            }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                _name = value;
            }

        }

        public virtual string ValueType
        {
            get { return _valuetype; }
            set
            {
                _valuetype = value;
            }

        }

        public virtual string ErrorMessage
        {
            get { return _errormessage; }
            set
            {
                _errormessage = value;
            }

        }

        public virtual string Value
        {
            get { return _value; }
            set
            {
                _value = value;
            }

        }


		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
            ValidationImport castObj = null;
			try
			{
                castObj = (ValidationImport)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
