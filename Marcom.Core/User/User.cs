using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.User.Interface;
using System.Security.Cryptography;
using BrandSystems.Marcom.Core.Access;
using Amazon.S3;

namespace BrandSystems.Marcom.Core.User
{

    /// <summary>
    /// User object for table 'UM_User'.
    /// </summary>
    public class User : IUser, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _firstname;
        protected string _lastname;
        protected string _username;
        protected byte[] _password;
        protected string _saltpassword;
        protected string _email;
        protected string _image;
        protected string _language;
        protected string _timezone;
        protected int? _startpage;
        protected string _feedselection;
        protected int _DashboardTemplateID;
        protected int _IsDashboardCustomized;

        protected bool _gender;
        //protected string _dob;
        protected string _phone;
        protected string _address;
        protected string _city;
        protected int _zipcode;
        protected string _website;

        protected string _designation;
        protected string _title;
        protected string _companyName;
        protected string _Partners;
        protected string _OldPassword;
        protected int _languagesettings;
        protected bool _IsssoUser;
        protected bool _Isapiuser;

        protected IList<GlobalAcl> _listofusergloablroles;
        protected string _quickinfo1;
        protected string _quickinfo2;
        protected string _quickinfo1attributecaption;
        protected string _quickinfo2attributecaption;
        protected int _DimensionUnit;
        protected int _AssetAccess;

        protected int _tenantid;
        protected string _tenanthost;
        protected string _tenantPath;
        protected string _tenantName;
        protected bool _memberrelated;

        #endregion

        #region Constructors
        public User() { }

        public User(string pFirstName, string pLastName, string pUserName, byte[] pPassword, string pEmail, string pImage, string pLanguage, string pTimeZone, int? pStartPage, string saltPassword, string pFeedSelection, int ptemplateID, int pDashboardTemplateID, int pIsDashboardCustomized, string pCompanyName, string pPartners, string pOldPassword, int pLanguageSettings, bool pIsssouser, bool pIsapiuser, string pquickInfo1, string pquickInfo2, string pquickInfo1attributecaption, string pquickInfo2attributecaption, int pDimensionUnit, int pAssetAccess, int ptenantid, string ptenanthost, string ptenantpath, string ptenantname, bool pMemberRelated)
        {
            this._firstname = pFirstName;
            this._lastname = pLastName;
            this._username = pUserName;
            this._password = pPassword;
            this._email = pEmail;
            this._image = pImage;
            this._language = pLanguage;
            this._timezone = pTimeZone;
            this._startpage = pStartPage;
            this._saltpassword = saltPassword;
            this._feedselection = pFeedSelection;
            this._DashboardTemplateID = pDashboardTemplateID;
            this._IsDashboardCustomized = pIsDashboardCustomized;
            this._companyName = pCompanyName;
            this._Partners = pPartners;
            this._OldPassword = pOldPassword;
            this._languagesettings = pLanguageSettings;
            this._IsssoUser = pIsssouser;
            this._Isapiuser = pIsapiuser;
            this._quickinfo1 = pquickInfo1;
            this._quickinfo2 = pquickInfo2;
            this._quickinfo1 = pquickInfo1attributecaption;
            this._quickinfo2 = pquickInfo2attributecaption;
            this._DimensionUnit = pDimensionUnit;
            this._AssetAccess = pAssetAccess;
            this._tenantid = ptenantid;
            this._tenanthost = ptenanthost;
            this._tenantPath = ptenantpath;
            this._memberrelated = pMemberRelated;
            this._tenantName = ptenantname;
        }

        public User(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public string FirstName
        {
            get { return _firstname; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("FirstName", "FirstName value, cannot contain more than 50 characters");
                _firstname = value;
            }

        }

        public string LastName
        {
            get { return _lastname; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("LastName", "LastName value, cannot contain more than 50 characters");
                _lastname = value;
            }

        }

        public string UserName
        {
            get { return _username; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("UserName", "UserName value, cannot contain more than 250 characters");
                _username = value;
            }

        }

        public byte[] Password
        {
            get { return _password; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Password", "Password value, cannot contain more than 50 characters");
                _password = value;
            }

        }

        public string SaltPassword
        {
            get { return _saltpassword; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Password", "Password value, cannot contain more than 50 characters");

                _saltpassword = value;
            }

        }

        public string Email
        {
            get { return _email; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Email", "Email value, cannot contain more than 250 characters");
                _email = value;
            }

        }

        public string Image
        {
            get { return _image; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Image", "Image value, cannot contain more than 250 characters");
                _image = value;
            }

        }

        public string Language
        {
            get { return _language; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Language", "Language value, cannot contain more than 50 characters");
                _language = value;
            }

        }

        public string TimeZone
        {
            get { return _timezone; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("TimeZone", "TimeZone value, cannot contain more than 50 characters");
                _timezone = value;
            }

        }

        public int? StartPage
        {
            get { return _startpage; }
            set { _startpage = value; }

        }

        public string Feedselection
        {
            get { return _feedselection; }
            set
            { _feedselection = value; }

        }

        public int DashboardTemplateID
        {
            get { return _DashboardTemplateID; }
            set { _DashboardTemplateID = value; }

        }

        public int IsDashboardCustomized
        {
            get { return _IsDashboardCustomized; }
            set { _IsDashboardCustomized = value; }

        }

        public bool Gender
        {
            get { return _gender; }
            set { _gender = value; }

        }

        public bool IsSSOUser
        {
            get { return _IsssoUser; }
            set { _IsssoUser = value; }

        }

        public bool IsAPIUser
        {
            get { return _Isapiuser; }
            set { _Isapiuser = value; }

        }


        //public string DOB
        //{
        //    get { return _dob; }
        //    set
        //    {
        //        _dob = value;
        //    }

        //}

        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }

        }

        public string Address
        {
            get { return _address; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Address", "Address value, cannot contain more than 250 characters");
                _address = value;
            }

        }

        public string City
        {
            get { return _city; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("City", "City value, cannot contain more than 50 characters");
                _city = value;
            }

        }

        public int ZipCode
        {
            get { return _zipcode; }
            set { _zipcode = value; }
        }

        public string Website
        {
            get { return _website; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Website", "Website value, cannot contain more than 50 characters");
                _website = value;
            }

        }

        public string Designation
        {
            get { return _designation; }
            set
            {
                if (value != null && value.Length > 75)
                    throw new ArgumentOutOfRangeException("Designation", "Designation value, cannot contain more than 75 characters");
                _designation = value;
            }
        }

        public string Title
        {
            get { return _title; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Title", "Title value, cannot contain more than 250 characters");
                _title = value;
            }
        }
        public virtual string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }

        }
        public virtual string Partners
        {
            get { return _Partners; }
            set { _Partners = value; }

        }

        public virtual string OldPassword
        {
            get { return _OldPassword; }
            set { _OldPassword = value; }

        }

        public int LanguageSettings
        {
            get { return _languagesettings; }
            set { _languagesettings = value; }
        }
        public IList<GlobalAcl> ListOfUserGlobalRoles
        {
            get { return _listofusergloablroles; }
            set { _listofusergloablroles = value; }
        }

        public Dictionary<string, bool> ListOfAccess { get; set; }


        public string QuickInfo1
        {
            get { return _quickinfo1; }
            set { _quickinfo1 = value; }
        }

        public string QuickInfo2
        {
            get { return _quickinfo2; }
            set { _quickinfo2 = value; }
        }
        public string QuickInfo1AttributeCaption
        {
            get { return _quickinfo1attributecaption; }
            set { _quickinfo1attributecaption = value; }
        }

        public string QuickInfo2AttributeCaption
        {
            get { return _quickinfo2attributecaption; }
            set { _quickinfo2attributecaption = value; }
        }
        public int DimensionUnit
        {
            get { return _DimensionUnit; }
            set { _DimensionUnit = value; }
        }

        public int AssetAccess
        {
            get { return _AssetAccess; }
            set { _AssetAccess = value; }

        }

        public int TenantID
        {
            get { return _tenantid; }
            set { _tenantid = value; }
        }

        public string TenantHost
        {
            get { return _tenanthost; }
            set { _tenanthost = value; }
        }

        public string TenantPath
        {
            get { return _tenantPath; }
            set { _tenantPath = value; }
        }

        public string TenantName
        {
            get { return _tenantName; }
            set { _tenantName = value; }
        }
        public Guid session { get; set; }
        public bool MemberRelated
        {
            get { return _memberrelated; }
            set { _memberrelated = value; }

        }




        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            User castObj = null;
            try
            {
                castObj = (User)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }


        public StorageBlock AwsStorage { get; set; }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

    }

    public class StorageBlock
    {
        public string AWSAccessKeyID { get; set; }
        public string AWSSecretAccessKey { get; set; }
        public string BucketName { get; set; }
        public string ServiceURL { get; set; }
        public string RegionEndpoint { get; set; }
        public AmazonS3Client S3 { get; set; }

        public int storageType { get; set; }

        public string Uploaderurl { get; set; }

    }
}
