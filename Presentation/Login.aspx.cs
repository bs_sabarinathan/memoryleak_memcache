﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrandSystems.Marcom.Core.User;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
//using Marcom.SAML.Library.Schema;
using MarcomSAMLLibrary.Schema;
using System.Xml;
using System.Xml.Serialization;
using System.Security.Cryptography.Xml;
using System.Security.Cryptography.X509Certificates;
using System.Configuration;
using BrandSystems.Marcom.Core.Utility;
using System.Xml.Linq;
using Newtonsoft.Json;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.AmazonStorageHelper;

namespace Presentation
{
    public partial class Login : System.Web.UI.Page
    {

        private static string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
        private static string retFilePath = baseDir + "//" + "SAMLSSO.txt";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                errorLabel.Visible = false;
                ErrorLog.LogFilePath = retFilePath;

                if (!Page.IsPostBack == true)
                {

                    switch (Request.HttpMethod)
                    {
                        case "POST":
                            // Checking to verify that a SAML Assertion is included

                            if (Request.Params.AllKeys != null)
                                foreach (var item in Request.Params.AllKeys)
                                {

                                    //ErrorLog.CustomExternalRoutine("Key is: " + item + " Value is :" + Request.Params[item], DateTime.Now);
                                }

                            if (Request.Params["SAMLResponse"] == null)
                            {

                                ErrorLog.CustomExternalRoutine("SAMLResponse not attached", DateTime.Now);
                                //throw invalid credential error
                                errorLabel.Visible = true;
                                break;
                            }

                            // pull Base 64 encoded XML saml assertion from Request and decode ist

                            XmlDocument SAMLXML = new XmlDocument();
                            String SAMLResponseString = System.Text.Encoding.UTF8.GetString(
                                Convert.FromBase64String(Request.Params["SAMLResponse"].ToString()));
                            SAMLXML.LoadXml(SAMLResponseString);

                            ErrorLog.CustomExternalRoutine("SAMLResponseString: " + SAMLResponseString , DateTime.Now);
                            //string value1 = File.ReadAllText(@"E:\TestApps\saml-http-post-reference-master\saml-http-post-reference-master - Working-Copy\CMMSAMLServiceProvider\App_Data\xml1.txt");
                            //string value1 = File.ReadAllText(@"D:\SAMIL_LOG\SamlStaticresponse.txt");
                            //XmlDocument SAMLXML = new XmlDocument();
                            //SAMLXML.LoadXml(value1);

                            // pull Base 64 encoded XML saml assertion from Request and decode it
                            //XmlDocument SAMLXML = new XmlDocument();
                            //String SAMLResponseString = System.Text.Encoding.UTF8.GetString(
                            //   Convert.FromBase64String(value1));
                            //SAMLXML.LoadXml(SAMLResponseString);

                            ////test with the hardcoded xml value


                            // Validate X509 Certificate Signature
                            if (!ValidateX509CertificateSignature(SAMLXML))
                            {
                                ErrorLog.CustomExternalRoutine("Response validation failed", DateTime.Now);
                                errorLabel.Visible = true;
                                break;

                            }

                            AssertionData SSOData = null;
                            AssertionRoles SSORoles = null;

                            // Finding 
                            AssertionType assertion = GetAssertionFromXMLDoc(SAMLXML);
                            if (assertion.Issuer.Value == ConfigurationManager.AppSettings["CertIssuer"])
                            {
                                SSOData = new AssertionData(assertion);
                                SSORoles = new AssertionRoles(assertion);

                            }
                            else
                            {
                                ErrorLog.CustomExternalRoutine("CertIssuer matching failed: " + assertion.Issuer.Value, DateTime.Now);
                            }

                            List<string> SAMLRolesInput = new List<string>();

                            try
                            {

                                List<string> SAMLRoles = new List<string>();
                                string TenantHost = HttpContext.Current.Request.Url.Host;
                                TenantSelection tfp = new TenantSelection();
                                string TenantFilePath = tfp.GetTenantFilePathByHostName(TenantHost);
                                int TenantID = tfp.GetTenantIDByHostName(TenantHost);
                                Guid systemSession = MarcomManagerFactory.GetSystemSession(TenantID);
                                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, (Guid)systemSession);
                                string xmlpath = "";
                                XElement xelementFilepath;
                                XDocument adminXmlDoc;

                                if (marcommanager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
                                {
                                    xmlpath = Path.Combine(TenantFilePath, "AdminSettings.xml");
                                    xelementFilepath = AWSHelper.ReadAdminXElement(marcommanager.User.AwsStorage.S3, marcommanager.User.AwsStorage.BucketName, xmlpath);
                                    adminXmlDoc = AWSHelper.ReadAdminXDocument(marcommanager.User.AwsStorage.S3, marcommanager.User.AwsStorage.BucketName, xmlpath);
                                }
                                else
                                {
                                    xmlpath = Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + TenantFilePath, "AdminSettings.xml");
                                    xelementFilepath = XElement.Load(xmlpath);
                                    adminXmlDoc = XDocument.Load(xmlpath);
                                }


                                string xelementName = "SSO";
                                var xmlElement = xelementFilepath.Element(xelementName);
                                var samlroles = xmlElement.Descendants("SAMLRoles");
                                foreach (XElement role in samlroles.Elements("Role"))
                                {
                                    SAMLRoles.Add(role.Value.ToLower().ToString());
                                }

                                ErrorLog.CustomExternalRoutine("Admin roles : " + string.Join(",", SAMLRoles), DateTime.Now);

                                foreach (string role in SSORoles.SAMLRoles)
                                {
                                    var isExist = SAMLRoles.Where(x => x.ToLower().Contains(role.ToLower())).FirstOrDefault();
                                    if (isExist != null)
                                    {
                                        SAMLRolesInput.Add(role.ToString());
                                    }
                                }


                                ErrorLog.CustomExternalRoutine("input roles : " + string.Join(",", SAMLRolesInput), DateTime.Now);

                                if (SAMLRoles.Count > 0 && SSORoles.SAMLRoles.Count == 0)
                                {
                                    //throw invalid credential error
                                    errorLabel.Visible = true;
                                    ErrorLog.CustomExternalRoutine("admin configured role but request don't have role", DateTime.Now);
                                    break;

                                }

                                if (SAMLRoles.Count > 0 && SSORoles.SAMLRoles.Count > 0)
                                {

                                    //check the SamlRolee arematching the admin defined roles
                                    if (isSubset(SAMLRoles, SSORoles.SAMLRoles, SAMLRoles.Count, SSORoles.SAMLRoles.Count))
                                    {
                                        //if matches then allow to proceed
                                        SAMLRolesInput = SSORoles.SAMLRoles;
                                    }
                                    else
                                    {
                                        //throw invalid credential error
                                        errorLabel.Visible = true;
                                        ErrorLog.CustomExternalRoutine("resuest role not even part of the configured role", DateTime.Now);
                                        break;

                                    }
                                }

                                if (SAMLRoles.Count == 0 && SSORoles.SAMLRoles.Count > 0)
                                {
                                    //allow to proceed
                                    SAMLRolesInput = SSORoles.SAMLRoles;
                                }


                            }
                            catch (Exception ex)
                            {
                                ErrorLog.ErrorRoutine(false, ex);
                            }


                            var userEmailID = SSOData.SAMLAttributes["useremail"];

                            var firstname = SSOData.SAMLAttributes["firstname"];

                            var lastname = SSOData.SAMLAttributes["lastname"];

                            ErrorLog.CustomExternalRoutine("NamedID is: " + userEmailID + " firstname is :" + firstname + " lastname is " + lastname, DateTime.Now);


                            logintoMarcom(userEmailID, firstname, lastname, SAMLRolesInput);

                            // At this point any specific work that needs to be done to establish user context with
                            // the SSOData should be executed before redirecting the user browser to the target
                            //Response.Redirect(Request.Params["RelayState"].ToString());


                            break;
                    }

                }
            }
            catch (Exception ex)
            {
                errorLabel.Visible = true;
                ErrorLog.LogFilePath = retFilePath;
                ErrorLog.ErrorRoutine(false, ex);

            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            //logintoMarcom();

        }



        /* Return 1 if arr2[] is a subset of arr1[] */
        bool isSubset(List<string> arr1, List<string> arr2, int m, int n)
        {
            try
            {
                int i = 0;
                int j = 0;
                for (i = 0; i < n; i++)
                {
                    for (j = 0; j < m; j++)
                    {
                        if (arr2[i].ToLower() == arr1[j].ToLower())
                            return true;
                    }

                    /* If the above inner loop was not broken at all then
                       arr2[i] is not present in arr1[] */
                    if (j == m)
                        return false;
                }

                /* If we reach here then all elements of arr2[] 
                  are present in arr1[] */
                return true;

            }
            catch (Exception ex)
            {
                ErrorLog.ErrorRoutine(false, ex);
                return false;
            }
        }
        public void logintoMarcom(string Email, string firtsName, string lastname, List<string> samlRolesModel)
        {
            ClientScriptManager script = Page.ClientScript;
            ErrorLog.LogFilePath = retFilePath;
            try
            {

                //Create input Json object
                string apploginserviceurl;
                apploginserviceurl = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.LastIndexOf("/")) + "/api/User/SecureSAMLSSO";
                StringBuilder jsonlogincredentials = new StringBuilder();
                //jsonlogincredentials.Append("{ \"Email\": \"" + Email + "\"}");


                jsonlogincredentials.Append("{ \"Email\": \"" + Email + "\", \"firstname\": \"" + firtsName + "\",\"samlroles\": \"" + string.Join("@", samlRolesModel) + "\", \"lastname\": \"" + lastname + "\"}");

                //jsonlogincredentials.Append("{ \"Email\": \"" + Email + "\", \"firstname\": \"" + firtsName + "\", \"lastname\": \"" + lastname + "\"}");


                ErrorLog.CustomExternalRoutine("Start validation", DateTime.Now);
                //Call marcom nancy service
                CallMarcomLoginService(apploginserviceurl, jsonlogincredentials.ToString());

                //Redirect to index.html

                if (!script.IsClientScriptBlockRegistered(this.GetType(), "Redirec"))
                {
                    ErrorLog.CustomExternalRoutine("User validated and granting access to Marcom", DateTime.Now);
                    script.RegisterClientScriptBlock(this.GetType(), "Redirec", "<script type=text/javascript>window.location.replace('index.html');</script>");
                }

            }

            catch (Exception ex)
            {
                if (!script.IsClientScriptBlockRegistered(this.GetType(), "Redirec"))
                {

                    ErrorLog.CustomExternalRoutine("User not validated due to " + ex.Message, DateTime.Now);
                    script.RegisterClientScriptBlock(this.GetType(), "Redirec", "<script type=text/javascript>alert('Invalid credentials')</script>");
                }
            }
        }
        public void CallMarcomLoginService(string apploginserviceurl, string jsonlogincredentials)
        {
            try
            {
                //MAKE A POST REQUEST WITH USER CREDENTIALS
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apploginserviceurl);
                request.ContentType = "application/json;charset=\"utf-8\"";
                request.Accept = "application/json";
                request.Method = "POST";

                using (Stream stm = request.GetRequestStream())
                {
                    using (StreamWriter stmw = new StreamWriter(stm))
                    {
                        stmw.Write(jsonlogincredentials.ToString());
                    }
                }


                WebResponse response = null;

                //WebResponse response = request.GetResponse();

                using (response = request.GetResponse())
                {



                    //READ THE RESPONSE
                    Response.Write(((HttpWebResponse)response).StatusDescription);
                    // Get the stream containing content returned by the server.
                    Stream dataStream = response.GetResponseStream();
                    // Open the stream using a StreamReader for easy access.
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.
                    string responseFromServer = reader.ReadToEnd();
                    responseFromServer = responseFromServer.Replace("\"StatusCode\":200,", "");


                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    dynamic responseobj = serializer.Deserialize<object>(responseFromServer);
                    System.Collections.Generic.Dictionary<string, object> responsedict = new System.Collections.Generic.Dictionary<string, object>();

                    //LOOP THROUGH TO ADD COOKIE VALUES
                    foreach (var kvp in responseobj)
                    {
                        System.Collections.Generic.KeyValuePair<string, object> s = kvp;
                        responsedict = (Dictionary<string, object>)s.Value;
                    }


                    //var sessionId = response.Headers["Set-Cookie"].Split(';').Where(s => s.StartsWith("_nc=")).SingleOrDefault().Replace("_nc=", "");

                    //create cookies
                    HttpCookie CookieUserName = new HttpCookie("Username"); CookieUserName.Value = responsedict["FirstName"].ToString() + " " + responsedict["LastName"].ToString();
                    HttpCookie CookieUserId = new HttpCookie("UserId"); CookieUserId.Value = responsedict["Id"].ToString();
                    HttpCookie CookieUserImage = new HttpCookie("UserImage"); CookieUserImage.Value = responsedict["Image"].ToString();
                    HttpCookie CookieUserEmail = new HttpCookie("UserEmail"); CookieUserEmail.Value = responsedict["Email"].ToString();
                    HttpCookie CookieStartPage = new HttpCookie("StartPage"); CookieStartPage.Value = responsedict["StartPage"].ToString();
                    HttpCookie Cookienc = new HttpCookie("Session");
                    Cookienc.Value = responsedict["session"].ToString();


                    //set cookie expiry date-time. Made it to last for next 12 hours.
                    CookieUserName.Expires = DateTime.Now.AddMinutes(30);
                    CookieUserId.Expires = DateTime.Now.AddMinutes(30);
                    CookieUserImage.Expires = DateTime.Now.AddMinutes(30);
                    CookieUserEmail.Expires = DateTime.Now.AddMinutes(30);
                    CookieStartPage.Expires = DateTime.Now.AddMinutes(30);
                    Cookienc.Expires = DateTime.Now.AddMinutes(120);


                    //Write the cookies to client.
                    Response.Cookies.Add(CookieUserName);
                    Response.Cookies.Add(CookieUserId);
                    Response.Cookies.Add(CookieUserImage);
                    Response.Cookies.Add(CookieUserEmail);
                    Response.Cookies.Add(CookieStartPage);
                    Response.Cookies.Add(Cookienc);

                    //request.AllowAutoRedirect = true;
                    //request.KeepAlive = true;

                    // Clean up the streams.
                    reader.Close();
                    dataStream.Close();
                    //response.Close();

                }

            }
            catch (WebException webex)
            {
                WebResponse errResp = webex.Response;
                using (Stream respStream = errResp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(respStream);
                    string text = reader.ReadToEnd();
                }
                throw webex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private AssertionType GetAssertionFromXMLDoc(XmlDocument SAMLXML)
        {
            ErrorLog.LogFilePath = retFilePath;
            ErrorLog.CustomExternalRoutine("GetAssertionFromXMLDoc started", DateTime.Now);
            AssertionType assertion = new AssertionType();
            try
            {
                XmlNamespaceManager ns = new XmlNamespaceManager(SAMLXML.NameTable);
                ns.AddNamespace("saml", "urn:oasis:names:tc:SAML:2.0:assertion");
                XmlElement xeAssertion = SAMLXML.DocumentElement.SelectSingleNode("saml:Assertion", ns) as XmlElement;

                XmlSerializer serializer = new XmlSerializer(typeof(AssertionType));

                assertion = (AssertionType)serializer.Deserialize(new XmlNodeReader(xeAssertion));

                return assertion;
            }
            catch (Exception ex)
            {
                ErrorLog.LogFilePath = retFilePath;
                ErrorLog.ErrorRoutine(false, ex);
                return assertion;
            }
        }

        private bool ValidateX509CertificateSignature(XmlDocument SAMLResponse)
        {
            try
            {

                XmlNodeList XMLSignatures = SAMLResponse.GetElementsByTagName("Signature", "http://www.w3.org/2000/09/xmldsig#");
                // Checking If the Response or the Assertion has been signed once and only once.
                if (XMLSignatures.Count != 1) return false;
                SignedXml SignedSAML = new SignedXml(SAMLResponse);
                SignedSAML.LoadXml((XmlElement)XMLSignatures[0]);
                String CertPath = System.Web.Hosting.HostingEnvironment.MapPath(@"~/SAMLCER/spgidsp_exportedCert.cer");

                X509Certificate2 SigningCert = new X509Certificate2(CertPath);

                return SignedSAML.CheckSignature(SigningCert, true);

            }
            catch (Exception ex)
            {
                ErrorLog.LogFilePath = retFilePath;
                ErrorLog.ErrorRoutine(false, ex);
                return false;
            }
        }
    }

    public class AssertionData
    {
        public Dictionary<string, string> SAMLAttributes;

        public AssertionData(AssertionType assertion)
        {
            // Find the attribute statement within the assertion
            AttributeStatementType ast = null;
            foreach (StatementAbstractType sat in assertion.Items)
            {
                if (sat.GetType().Equals(typeof(AttributeStatementType)))
                {
                    ast = (AttributeStatementType)sat;
                }
            }

            if (ast == null)
            {
                throw new ApplicationException("Invalid SAML Assertion: Missing Attribute Values");
            }

            SAMLAttributes = new Dictionary<string, string>();

            string UserID, UserFirstName, UserLastName, UserDisplayName, UserEmail, GroupID;

            // Do what needs to be done to pull specific attributes out for sending on
            // For now assuming this is a simple list of string key and string values
            foreach (AttributeType at in ast.Items)
            {

                var value = "";
                if (at.Name == "role")
                {
                    foreach (var val in at.AttributeValue)
                    {
                        value = ((System.Xml.XmlNode[])(val)).Length > 1 ? ((System.Xml.XmlNode[])(val))[1].Value : "";
                    }
                }


                if (((System.Xml.XmlNode[])(at.AttributeValue.FirstOrDefault())) != null)
                {
                    value = ((System.Xml.XmlNode[])(at.AttributeValue.FirstOrDefault())).Length > 1 ? ((System.Xml.XmlNode[])(at.AttributeValue.FirstOrDefault()))[1].Value : "";
                }

                SAMLAttributes.Add(at.Name, value);

                switch (at.Name)
                {
                    case "UserID":
                        if (at.AttributeValue.Length > 0) UserID = at.AttributeValue[0].ToString();
                        break;
                    case "firstname":
                        if (at.AttributeValue.Length > 0) UserFirstName = at.AttributeValue[0].ToString();
                        break;
                    case "lastname":
                        if (at.AttributeValue.Length > 0) UserLastName = at.AttributeValue[0].ToString();
                        break;
                    case "UserDisplayName":
                        if (at.AttributeValue.Length > 0) UserDisplayName = at.AttributeValue[0].ToString();
                        break;
                    case "useremail":
                        if (at.AttributeValue.Length > 0) UserEmail = at.AttributeValue[0].ToString();
                        break;
                    case "GroupID":
                        if (at.AttributeValue.Length > 0) GroupID = at.AttributeValue[0].ToString();
                        break;
                }
            }
        }



    }

    public class AssertionRoles
    {
        public List<string> SAMLRoles;


        private static string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
        private static string retFilePath = baseDir + "//" + "SAMLSSO.txt";
        public AssertionRoles(AssertionType assertion)
        {


            // Find the attribute statement within the assertion
            AttributeStatementType ast = null;
            foreach (StatementAbstractType sat in assertion.Items)
            {
                if (sat.GetType().Equals(typeof(AttributeStatementType)))
                {
                    ast = (AttributeStatementType)sat;
                }
            }

            if (ast == null)
            {
                throw new ApplicationException("Invalid SAML Assertion: Missing Attribute Values");
            }

            SAMLRoles = new List<string>();


            try
            {
                ErrorLog.CustomExternalRoutine("role retrival process started", DateTime.Now);
                // Do what needs to be done to pull specific attributes out for sending on
                // For now assuming this is a simple list of string key and string values
                foreach (AttributeType at in ast.Items)
                {

                    var value = "";
                    if (at.Name == "role")
                    {
                        foreach (var val in at.AttributeValue)
                        {
                            value = ((System.Xml.XmlNode[])(val)).Length > 1 ? ((System.Xml.XmlNode[])(val))[1].Value : "";
                            SAMLRoles.Add(value);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLog.LogFilePath = retFilePath;
                ErrorLog.ErrorRoutine(false, ex);
            }
        }


    }

    public class AssertionRoleInput
    {
        public List<string> Roles { get; set; }
    }


}