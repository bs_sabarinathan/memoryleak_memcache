﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Task.Model
{
    public class ApprovalFlowTemplateDao : BaseDao, ICloneable
    {
        protected int _id;
        protected string _name;
        protected string _description;
        protected int _tempid;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;

        #region Constructors
        public ApprovalFlowTemplateDao() { }
        public ApprovalFlowTemplateDao(int ID, string Name, string Description, int TempId)
        {
            this._id = ID;
            this._name = Name;
            this._description = Description;
            this._tempid = TempId;
        }
        public ApprovalFlowTemplateDao(int ID)
        {
            this._id = ID;
        }

        #endregion

        #region Public Properties
        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }
        }
        public virtual string Name
        {
            get { return _name; }
            set { _bIsChanged |= (_name != value); _name = value; }
        }
        public virtual string Description
        {
            get { return _description; }
            set { _bIsChanged |= (_description != value); _description = value; }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        public static class PropertyNames
        {
            public const string ApprovalFlowTemplateDao = "ApprovalFlowTemplateDao";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string TempId = "TempId";
        }


    }
}

