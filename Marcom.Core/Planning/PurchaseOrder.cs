﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

    /// <summary>
    /// PurchaseOrder object for table 'PM_PurchaseOrder'.
    /// </summary>

    public class PurchaseOrder : IPurchaseOrder
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected DateTime _createddate;
        protected string _ponumber;
        protected int _supplierid;
        protected string _description;
        protected DateTime? _expectespenddate;
        protected DateTime? _approvedate;
        protected int? _approverid;
        protected DateTime? _senddate;
        protected int _status;
        protected int _userid;
        protected bool _IsExternal;

        #endregion

        #region Constructors
        public PurchaseOrder() { }

        public PurchaseOrder(int pEntityid, DateTime pCreateddate, string pPonumber, int pSupplierid, string pDescription, DateTime? pExpectespenddate, DateTime? pApprovedate, int? pApproverId, int pStatus, int pUserID, DateTime? pSenddate, bool pIsExternal)
        {
            this._entityid = pEntityid;
            this._createddate = pCreateddate;
            this._ponumber = pPonumber;
            this._supplierid = pSupplierid;
            this._description = pDescription;
            this._expectespenddate = pExpectespenddate;
            this._approvedate = pApprovedate;
            this._senddate = pSenddate;
            this._approverid = pApproverId;
            this._status = pStatus;
            this._userid = pUserID;
            this._IsExternal = pIsExternal;

        }


        public PurchaseOrder(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int Entityid
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public virtual DateTime CreateDate
        {
            get { return _createddate; }
            set { _createddate = value; }

        }

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 250 characters");

                _description = value;
            }

        }

        public virtual string PONumber
        {
            get { return _ponumber; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("PONumber", "PONumber value, cannot contain more than 50 characters");

                _ponumber = value;
            }

        }


        public virtual int SupplierID
        {
            get { return _supplierid; }
            set { _supplierid = value; }

        }

        public virtual DateTime? ExpectedSpentDate
        {
            get { return _expectespenddate; }
            set { _expectespenddate = value; }

        }

        public virtual DateTime? ApprovedDate
        {
            get { return _approvedate; }
            set { _approvedate = value; }

        }

        public virtual int? ApproverID
        {
            get { return _approverid; }
            set { _approverid = value; }

        }

        public virtual DateTime? SentDate
        {
            get { return _senddate; }
            set { _senddate = value; }

        }


        public virtual int Status
        {
            get { return _status; }
            set { _status = value; }

        }

        public virtual int UserID
        {
            get { return _userid; }
            set { _userid = value; }

        }

        public virtual bool IsExternal
        {
            get { return _IsExternal; }
            set { _IsExternal = value; }
        }
        

        public string RequesterName { get; set; }
        public string SupplierName { get; set; }
        public string ApproverName { get; set; }
        public int CostCentreID { get; set; }
        public decimal amount { get; set; }
        public string currencytypeName { get; set; }
        public int currencytype { get; set; }
        public string strAppprovalDate { get; set; }
        public string strSendDate { get; set; }
        public string strCreatedDate { get; set; }
        public string strAmount { get; set; }
        public string strStatusName { get; set; }
        public dynamic purchaseOrderDetail { get; set; }
        public bool isApproveSendAuthority { get; set; }
        public IList<IFinancialMetadataAttributewithValues> DynamciData { get; set; }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            PurchaseOrder castObj = null;
            try
            {
                castObj = (PurchaseOrder)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
