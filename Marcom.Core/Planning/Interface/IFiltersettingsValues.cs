﻿using BrandSystems.Marcom.Core.Metadata;
using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IFilterSettings interface for table 'PM_FilterValues'.
    /// </summary>
   public interface IFiltersettingsValues
    {

        #region Public Properties

        int Id
        {
            get;
            set;
        }

        int FilterId
        {
            get;
            set;
        }

        int AttributeId
        {
            get;
            set;
        }
        int AttributeTypeId
        {
            get;
            set;
        }
        int Level
        {
            get;
            set;
        }
        int Value
        {
            get;
            set;
        }
        string EntityTypeIDs
        {
            get;
            set;
        }
        string Keyword
        {
            get;
            set;
        }

        string StartDate
        {
            get;
            set;
        }

        string EndDate
        {
            get;
            set;
        }

        string TreeValues
        { get; set; }

        IList<DropDownTreePricing> DropdowntreePricingData { get; set; }
        string EntityMemberIDs
        {
            get;
            set;
        }

        #endregion
    }
}
