﻿(function(ng, app) {
	"use strict";

	function muiadminsuperadminCtrl($scope, $location, $resource, $timeout, $compile, $translate, AdminService) {
		var IsEdit = 0;
		$scope.ModuleforEntityTypeAccess = [];
		$scope.EntityTypeAccessInPopup = '<a class="iconLink" data-toggle="modal" ng-click="GetEntityTypeAccessOnClick(row)" ng-hide="row.entity.Id==1001" data-toggle="modal" data-target="#EntitytypeAccessPopUp"><i class="icon-edit"></i></a> '
		$scope.FeatureAccessInPopup = '<a class="iconLink"  ng-click="GetFeatureAccessOnClick(row)" ng-hide="row.entity.Id==1001 || row.entity.Id==-1" ><i class="icon-edit"></i></a> '
		$scope.metadataFeatureAccessInPopup = '<a class="iconLink"  ng-click="GetmetadaFeatureAccessOnClick(row)" ng-hide="row.entity.Id==1001 || row.entity.Id==-1"><i class="icon-edit"></i></a> '
		$scope.delete = '<a class="iconLink" data-toggle="modal" ng-click="DeleteGlobalRolesByID(row)" ng-hide="row.entity.Id==10 || row.entity.Id==1 || row.entity.Id==1001" data-toggle="modal"><i class="icon-remove"></i></a> '
		$scope.ediglobalroleInPopup = '<div class="ngCellText"><a ng-click="GetGlobalRolesByID(row)">{{row.entity.Caption}}</a></div>'
		$scope.GetGlobalRolesByID = function GetGlobalRolesByID(row) {
		    $("#globalrolesModal").modal("show");
			$scope.Id = row.entity.Id;
			$scope.Caption = row.entity.Caption;
			$scope.Description = row.entity.Description;
			$scope.EnableGlobalRoleUpdate = true;
			$scope.EnableGloabalRoleAdd = false;
			$timeout(function() {
				$('#Caption').focus().select();
			}, 1000);
		};
		AdminService.GetGlobalRole().then(function(getglobalroles) {
			$scope.listglobalrolesdata = getglobalroles.Response;
			$scope.globalrolesData = $scope.listglobalrolesdata;
		});
		$scope.addGlobalRoles = function() {
			$scope.Caption = '';
			$scope.Description = '';
			$scope.EnableGloabalRoleAdd = true;
			$scope.EnableGlobalRoleUpdate = false;
			$timeout(function() {
				$('#Caption').focus();
			}, 1000);
		};
		$scope.slno = '<span class="slno">{{row.rowIndex+1}}</span>';
		$scope.filterOptions = {
			filterText: ''
		};
		var linkCellTemplate = '<div class="ngCellText">' + ' <a href="test">Visible text</a>' + '</div>';
		$scope.gridGlobalRole = {
			data: 'globalrolesData',
			enablePinning: false,
			columnDefs: [{
				field: "SlNo",
				displayName: $translate.instant('LanguageContents.Res_4753.Caption'),
				cellTemplate: $scope.slno,
				width: 60
			}, {
				field: "Id",
				displayName: $translate.instant('LanguageContents.Res_69.Caption'),
				visible: false,
				width: 60
			}, {
				field: "Caption",
				displayName: $translate.instant('LanguageContents.Res_44.Caption'),
				cellTemplate: $scope.ediglobalroleInPopup,
				width: 180
			}, {
				field: "Description",
				displayName: $translate.instant('LanguageContents.Res_22.Caption'),
				width: 250
			}, {
				field: "Entity Type",
				displayName: $translate.instant('LanguageContents.Res_42.Caption'),
				visible: false,
				cellTemplate: $scope.EntityTypeAccessInPopup,
				width: 80
			}, {
				field: "General settings",
				displayName: "General",
				cellTemplate: $scope.FeatureAccessInPopup,
				width: 80
			}, {
				field: "Metadata settings",
				displayName: "Metadata",
				cellTemplate: $scope.metadataFeatureAccessInPopup,
				width: 80
			}, {
				field: "",
				displayName: '',
				visible: false,
				cellTemplate: $scope.delete,
				width: 40
			}]
		};
		$scope.GlobalRoleAccess = {};
		$scope.globalaccessdata = {};
		$scope.globalEntityTypeaccessdata = {};
		AdminService.GetGlobalRole().then(function(getglobalrolforeaccess) {
			$scope.GlobalRoleforAccess = getglobalrolforeaccess.Response;
			$scope.globalaccessdata = $scope.GlobalRoleforAccess;
		});
		$scope.ModuleGlobalAccess = {};
		$scope.moduleglobalaccessdata = {};
		var Version = 1;
		AdminService.GetModule().then(function(getmoduleglobalaccess) {
			$scope.ModuleforEntityTypeAccess = getmoduleglobalaccess.Response;
			$scope.ModuleforEntityTypeAccess.push({
				Id: -1,
				Caption: "Custom Tab",
				Description: "Custom Tab",
				IsEnable: true
			});
		});
		$scope.EntityGlobalAccess = {};
		$scope.entityglobalaccessdata = {};
		$scope.FeatureGlobalAccess = {};
		$scope.featureglobalaccessdata = {};
		$scope.ID = 0;
		$scope.ShowChatOption = function(features) {
			var d = ($.cookie('UserEmail').contains('brandsystems') == true && IsGlobalAdmin == true && features.Caption == 'Live Chat') ? true : false;
			return d;
		};
		$scope.GetFeatureAccessOnClick = function (row) {
		    $("#globalaccessModal").modal("show");
			$scope.childData = [];
			$scope.GlobalRoleCaption = row.entity.Caption;
			$scope.ModuleforEntityTypeAccess = [];
			$scope.ParentName = [];
			$scope.GlobalRole = row.entity.Id;
			$scope.GlobalRoleCaption = row.entity.Caption;
			AdminService.GetSuperAdminModule(row.entity.Id).then(function(getmoduleglobalaccess) {
				for (var i = 0; i < getmoduleglobalaccess.Response.length; i++) {
					$scope.ParentName.push({
						MenuID: getmoduleglobalaccess.Response[i]["MenuID"],
						Name: getmoduleglobalaccess.Response[i]["Name"]
					})
					if (getmoduleglobalaccess.Response[i]["FeatureID"] == 0 && getmoduleglobalaccess.Response[i]["MenuID"] != 107) {
						$scope.ModuleforEntityTypeAccess.push({
							MenuID: getmoduleglobalaccess.Response[i]["MenuID"],
							Name: getmoduleglobalaccess.Response[i]["Name"]
						});
					}
					if (getmoduleglobalaccess.Response[i]["FeatureID"] != 0) {
						$scope.childData.push({
							MenuID: getmoduleglobalaccess.Response[i]["MenuID"],
							Name: getmoduleglobalaccess.Response[i]["Name"],
							Id: getmoduleglobalaccess.Response[i]["FeatureID"],
							GlobalAclID: getmoduleglobalaccess.Response[i]["IsTopnavigation"],
							IsChecked: getmoduleglobalaccess.Response[i]["AccessPermission"]
						});
					}
				}
				$scope.globalaccessData = $scope.childData;
			});
			
		};
		$scope.GetmetadaFeatureAccessOnClick = function (row) {
		    $("#globalaccessModalmetadata").modal("show");
			$scope.childData = [];
			$scope.GlobalRoleCaptionMetadata = "Super Admin";
			$scope.ModuleforEntityTypeAccess = [];
			$scope.ParentName = [];
			$scope.GlobalRole = row.entity.Id;
			$scope.GlobalRoleCaption = row.entity.Caption;
			AdminService.GetSuperAdminModule(row.entity.Id).then(function(getMetadataglobalaccess) {
				for (var i = 0; i < getMetadataglobalaccess.Response.length; i++) {
					if (getMetadataglobalaccess.Response[i]["MenuID"] >= 100 && getMetadataglobalaccess.Response[i]["MenuID"] <= 106 && getMetadataglobalaccess.Response[i]["FeatureID"] != 0) {
						$scope.childData.push({
							MenuID: getMetadataglobalaccess.Response[i]["MenuID"],
							Name: getMetadataglobalaccess.Response[i]["Name"],
							Id: getMetadataglobalaccess.Response[i]["FeatureID"],
							GlobalAclID: getMetadataglobalaccess.Response[i]["IsTopnavigation"],
							IsChecked: getMetadataglobalaccess.Response[i]["AccessPermission"]
						});
					}
				}
				$scope.globalaccessData = $scope.childData;
			});
			
		};
		$scope.SaveUpdateRoleFeatures = function SaveUpdateRoleFeatures() {
			$scope.globalaccessData;
			AdminService.DeleteGlobalByID($scope.GlobalRole, $scope.globalaccessData[0].MenuID).then(function(deleteglobalroles) {
				if (deleteglobalroles.Response != null) {
					$timeout(function() {
						var insertupdglobalfeatures = {};
						insertupdglobalfeatures.GlobalAccessFeatures = $scope.globalaccessData;
						insertupdglobalfeatures.GlobalRoleID = $scope.GlobalRole;
						AdminService.SaveUpdateSuperAdminRoleFeatures(insertupdglobalfeatures).then(function(result) {
							if (result.Response == true) {
								NotifySuccess($translate.instant('LanguageContents.Res_4799.Caption'));
								$("#globalaccessModal").modal('hide');
								$('#globalaccessModalmetadata').modal('hide');
							} else NotifyError($translate.instant('LanguageContents.Res_4329.Caption'));
						});
					}, 500);
					$("#globalaccessModal").modal('hide');
					$('#globalaccessModalmetadata').modal('hide');
				}
			});
		}
		$scope.set_color = function(clr) {
			if (clr != null) return {
				'background-color': "#" + clr.toString().trim()
			};
			else return '';
		}
		$scope.UpdateGlobalRole = function() {
			var updateglobalroles = {};
			updateglobalroles.ID = $scope.Id;
			updateglobalroles.Caption = $scope.Caption;
			updateglobalroles.Description = $scope.Description;
			AdminService.InsertUpdateGlobalRole(updateglobalroles).then(function() {
				NotifySuccess($translate.instant('LanguageContents.Res_4874.Caption'));
				AdminService.GetGlobalRole().then(function(getglobalroles) {
					$scope.listglobalrolesdata = getglobalroles.Response;
					$scope.globalrolesData = $scope.listglobalrolesdata;
				});
				$('#globalrolesModal').modal('hide');
				AdminService.GetGlobalRole().then(function(getglobalroles) {
					$scope.listglobalrolesdata = getglobalroles.Response;
					$scope.globalrolesData = $scope.listglobalrolesdata;
				});
			});
		};
	}
	app.controller("mui.admin.superadminCtrl", ['$scope', '$location', '$resource', '$timeout', '$compile', '$translate', 'AdminService', muiadminsuperadminCtrl]);
})(angular, app);