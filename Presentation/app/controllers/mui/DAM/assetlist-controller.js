﻿(function(ng, app) {
	"use strict";

	function muiDAMassetlistCtrl($window, $location, $timeout, $scope, $cookies, $resource,$stateParams, AssetListService, $compile) {
		var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
		var TenantFilePath1 = TenantFilePath.replace(/\\/g, "\/")
		var imagesrcpath = TenantFilePath;
		if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
			TenantFilePath1 = cloudpath;
			imagesrcpath = cloudpath;
		}
		var localfolderid = 0;
		if ($scope.selectedTree.id != undefined) {
		    if ($scope.selectedTree.id == 0) {
		        $('#assetlistview').removeClass('assetlistscroll');
		        $('#assetlistview').addClass('assetlistscroll');
		    }
		    else {
		        $('#assetlistview').removeClass('assetlistscroll');
		        localfolderid = $scope.selectedTree.id;
		    }
		}

		$scope.userpublishaccess = false;
		$scope.IsPublish = false;
		userpublishaccess($stateParams.ID);
	    /******** Current user publishacces ********/
		function userpublishaccess(entityID) {
		    AssetListService.userpublishaccess(entityID).then(function (result) {
		        $scope.userpublishaccess = result.Response;
		        $scope.IsPublish = result.Response;
		    });
		}
		if ($("#icon_selectassets").hasClass("icon-check")) {
		    $("#icon_selectassets").removeClass("icon-check");
		    $("#icon_selectassets").addClass("icon-unchecked");
		}
		$scope.$on('LoadListAssetView', function(event, folderID, EID, view) {
			var currentpath = $location.path().replace('/', '');
			var test = currentpath;
			test = test.split("/");
			if (test[test.length - 1] == "attachment") {
				if ($scope.SelectAllAssetsObj.checkselectall) {
					$scope.$emit('clickcheckselectall', 1);
				}
			}
			localfolderid = folderID;
			if (test[test.length - 1] == "task" || folderID == 0) {
			    $('#assetlistview').removeClass('assetlistscroll');
			    $('#assetlistview').addClass('assetlistscroll');
			}
			else {
			    $('#assetlistview').removeClass('assetlistscroll');
			    localfolderid = $scope.selectedTree.id;

			}

			AssetListService.GetEntityAsset(folderID, EID, view, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo, false).then(function(data) {
				var res = [];
				res = data.Response;
				LoadView(res);
				loadScrollSettings();
			});
		});
		if ($scope.selectedTree.id != undefined) {
			AssetListService.GetEntityAsset($scope.selectedTree.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo, false).then(function(data) {
				var res = [];
				res = data.Response;
				LoadView(res);
				loadScrollSettings();
			});
		}
		$scope.AssetListSelection.AssetSelection = {};
		$scope.AssetListSelection.AssetChkClass = {};
		$scope.AssetListSelectionClass.AssetSelectionClass = {};
		var DynamicData = [];

		function LoadView(res) {
			var html = '';
			if ($scope.AssetFilesObj.PageNo == 1) {
				$("#assetlistview").html(html);
				$scope.AssetSelection = {};
				$scope.AssetSelectionClass = {};
				$scope.DownloadFileObj.DownloadFilesArr = [];
				$scope.AssetFilesObj.AssetSelection = [];
				$scope.AssetFilesObj.AssetFiles = [];
				$scope.AssetFilesObj_Temp.AssetFiles = [];
				$scope.AssetFilesObj.AssetDynamicData = [];
			}
			if (res != null) {
			    var assets = [];
			    loading = false;
				assets = res[0].AssetFiles;
				for (var j = 0, asset; asset = assets[j++];) {
					$scope.AssetFilesObj.AssetFiles.push(asset);
					$scope.AssetFilesObj_Temp.AssetFiles.push(asset);
				}
				DynamicData = res[0].AssetDynData;
				for (var k = 0, dyn; dyn = DynamicData[k++];) {
					$scope.AssetFilesObj.AssetDynamicData.push(dyn);
				}
				if ($scope.AssetFilesObj.PageNo == 1) {
					html += '<div>';
					html += ' <table id="AssetListViewTable" class="table table-normal-both ListviewTable">';
					html += ' <thead>';
					html += GenerateHeader();
					html += ' </thead>';
					html += ' <tbody>';
					html += GenerateList(assets);
					html += ' </tbody>';
					html += ' </table>';
					html += '</div>';
					$("#assetlistview").append($compile(html)($scope));
					if ($('#assetview').css('display') == 'none') {
						$('#assetview').css('display', 'block');
					}
				} else {
					html += GenerateList(assets);
					$("#AssetListViewTable tbody").append($compile(html)($scope));
					if ($('#assetview').css('display') == 'none') {
						$('#assetview').css('display', 'block');
					}
				}
			} else {
				if ($scope.AssetFilesObj.PageNo == 1 && $scope.Dam_Directory.length > 0) {
					var emptyHtml = "";
					if ($scope.processingsrcobj.processingplace == "attachment") emptyHtml = '<div class="emptyFolder">This folder is empty.</div>';
					if ($scope.processingsrcobj.processingplace == "task") emptyHtml = '<div class="emptyFolder">There is no asset.</div>';
					$("#assetlistview").html(emptyHtml);
					if ($('#assetview').css('display') == 'none') {
						$('#assetview').css('display', 'block');
					}
				} else if ($scope.AssetFilesObj.PageNo == 1 && $scope.Dam_Directory.length == 0) {
					var emptyHtml = "";
					if ($scope.processingsrcobj.processingplace == "attachment") emptyHtml = '<div class="emptyFolder">There is no folder.</div>';
					$("#assetlistview").html(emptyHtml);
					if ($('#assetview').css('display') == 'none') {
						$('#assetview').css('display', 'block');
					}
				}
			}
		}
		$scope.selectedAllassetfiles = false;
		$scope.checkAllFile = function($event) {
			var checkbox = $event.target;
			if (checkbox.checked) {
				$scope.selectedAllunassignedtasks = true;
				$scope.SelectAllAssetsObj.checkselectall = true;
				$scope.$emit('clickcheckselectall', 1);
			} else {
				$scope.selectedAllunassignedtasks = false;
				$scope.SelectAllAssetsObj.checkselectall = false;
				$scope.$emit('clickDselectall');
			}
		}
		$scope.selectallListassets = function(checkbox) {
			if ($scope.DamViewName == "List") {
				for (var i = 0, asset; asset = $scope.AssetFilesObj.AssetFiles[i++];) {
					$scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = ($scope.selectedAllunassignedtasks == true) ? "checkbox checked" : "checkbox";
					$scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = $scope.selectedAllunassignedtasks;
					if ($scope.selectedAllunassignedtasks == true) {
						$('#chkBox_' + asset.AssetUniqueID + '').attr('checked', 'checked');
						$('#chkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox');
						$('#chkBoxIcon_' + asset.AssetUniqueID + '').addClass('checked');
					} else {
						$('#chkBox_' + asset.AssetUniqueID + '').removeAttr('checked');
						$('#chkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox checked');
						$('#chkBoxIcon_' + asset.AssetUniqueID + '').addClass('checkbox');
					}
					$scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = ($scope.selectedAllunassignedtasks == true) ? "li-Selection selected" : "li-Selection";
					updatescopesforactions(asset.AssetUniqueID, asset.ActiveFileID, $scope.selectedAllunassignedtasks, checkbox);
				}
			}
		}

		function updatescopesforactions(assetid, activefileid, ischecked, checkbox) {
			var res = [];
			res = $.grep($scope.AssetFilesObj.AssetFiles, function(e) {
				return e.AssetUniqueID == assetid;
			});
			if (res.length > 0) {
				DownloadAssets(checkbox, assetid, res[0].FileName, res[0].Extension, res[0].FileGuid, res[0].Category);
			}
			if (ischecked) {
				var remainRecord = [];
				remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function(e) {
					return e == assetid;
				});
				if (remainRecord.length == 0) {
					$scope.AssetFilesObj.AssetSelection.push(assetid);
				}
				if (res[0].Category == 0) {
					AddAttachmentsToTask(assetid, activefileid, true);
				}
			} else {
				var remainRecord = [];
				remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function(e) {
					return e == assetid;
				});
				if (remainRecord.length > 0) {
					$scope.AssetFilesObj.AssetSelection.splice($.inArray(assetid, $scope.AssetFilesObj.AssetSelection), 1);
				}
				if (res[0].Category == 0) {
					AddAttachmentsToTask(assetid, activefileid, false);
				}
			}
		}
		$scope.Listfullselection = "";

		function GenerateHeader() {
			$scope.Listfullselection = ""
			if ($scope.SelectAllAssetsObj.checkselectall) $scope.Listfullselection = "checkbox checked"
			else $scope.Listfullselection = "checkbox"
			var html = '';
			html += '<tr>';
			html += ' <th>';
			html += ' <label class="checkbox checkbox-custom">';
			html += ' <input ng-click="checkAllFile($event)" type="checkbox"/>';
			html += '<input type="checkbox" ng-model="selectedAllunassignedtasks" ng-click=\"checkAllFile($event)\" id="damlistviewselectall" />';
			html += '<i id="listselectheader" ng-class="Listfullselection"></i>';
			html += '       </label>';
			html += '   </th>';
			html += '   <th>';
			html += '       <span>Header</span>';
			html += '   </th>';
			if ($scope.SettingsAttributes["ListViewSettings"].length > 0) {
				for (var i = 0, attr; attr = $scope.SettingsAttributes["ListViewSettings"][i++];) {
					html += '   <th>';
					html += '       <span>' + attr.Caption + '</span>';
					html += '   </th>';
				}
			}
			html += '<th></th>';
			html += '</tr>';
			return html;
		}

		function GenerateList(assets) {
			var html = '';
			for (var j = 0, asset; asset = assets[j++];) {
				if (!$scope.SelectAllAssetsObj.checkselectall) {
					$scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = false;
					$scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox";
				} else {
					$scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = true;
					$scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = "checkbox checked";
				}
				var assetselection = "";
				assetselection = $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] == true ? "li-Selection selected" : "li-Selection";
				$scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = assetselection;
				html += '<tr id="divthlistboxid_' + asset.AssetUniqueID + '" ng-class=\"AssetListSelectionClass.AssetSelectionClass.asset_' + asset.AssetUniqueID + '\">';
				html += '   <td damlistviewpreviewtooltip  data-tooltiptype="assetlistview" data-entitytypeid=' + asset.AssetUniqueID + ' data-shortdesc=' + asset.ShortDescription + '>';
				html += '       <label class="checkbox checkbox-custom">';
				html += '           <input ng-click="checkSelection(' + asset.AssetUniqueID + ',' + asset.ActiveFileID + ', $event)" ng-model="AssetListSelection.AssetSelection.asset_' + asset.AssetUniqueID + '" id="chkBox_' + asset.AssetUniqueID + '" type="checkbox" />';
				html += '           <i id="chkBoxIcon_' + asset.AssetUniqueID + '" ng-class=\"AssetListSelection.AssetChkClass.asset_' + asset.AssetUniqueID + '\"></i>';
				html += '       </label>';
				html += '   </td>';
				html += '   <td damlistviewpreviewtooltip  data-tooltiptype="assetlistview"  ng-click=\"Editasset(' + asset.AssetUniqueID + ')\" data-entitytypeid=' + asset.AssetUniqueID + ' data-backcolor=' + asset.ColorCode + ' data-shortdesc=' + asset.ShortDescription + '>';
				html += '       <span class="thmbListviewImgSpan">';
				html += '           <div class="thmbListview-eIconContainer" > ';
				html += '               <span class="thmbListview-eIcon" ng-click="Editasset(' + asset.AssetUniqueID + ')" my-qtip2 qtip-content="' + asset.Caption + '" style="background-color: #' + asset.ColorCode + ';">' + asset.ShortDescription + '</span>';
				html += '           </div>';
				html += '           <span class="thmbListviewImgName">' + asset.AssetName + '</span>';
				html += '       </span>';
				html += '   </td>';
				if ($scope.SettingsAttributes["ListViewSettings"].length > 0) {
					for (var i = 0, attr; attr = $scope.SettingsAttributes["ListViewSettings"][i++];) {
						html += '<td damlistviewpreviewtooltip  data-tooltiptype="assetlistview" data-entitytypeid=' + asset.AssetUniqueID + ' data-shortdesc=' + asset.ShortDescription + '>';
						if ($scope.AssetFilesObj.AssetDynamicData.length > 0) {
							var data = [];
							data = $.grep($scope.AssetFilesObj.AssetDynamicData, function(e) {
								return e.ID == asset.AssetUniqueID;
							});
							if (data.length > 0) {
								if (data.length > 0) {
									if (data[0]["" + attr.ID + ""] != "" && data[0]["" + attr.ID + ""] != null) if (data[0]["" + attr.ID + ""] != '1900-01-01') html += '<span class="thmbListviewDescSpan">' + data[0]["" + attr.ID + ""] + '</span>';
									else html += '<span class="thmbListviewDescSpan">-</span>';
									else html += '<span class="thmbListviewDescSpan">-</span>';
								} else html += '<span class="thmbListviewDescSpan">-</span>';
							} else html += '<span class="thmbListviewDescSpan">-</span>';
						} else html += '<span class="thmbListviewDescSpan">-</span>';
						html += '</td>';
					}
				}
				html += '   <td><span class="thmbListview-options"><i id="assetlistoptionsIcon_' + j + '" ng-click="SetAssetActionId(' + asset.AssetUniqueID + ')" class="icon-reorder" data-toggle="dropdown" data-role="" contextmenuoptions="assetlistviewActionMenu"></i></span></td>';
				html += '</tr>';
			}
			return html;
		}
		$scope.checkSelection = function(assetid, activefileid, event) {
			var checkbox = event.target;
			var assetselection = "";
			assetselection = checkbox.checked == true ? "li-Selection selected" : "li-Selection";
			$scope.AssetListSelection.AssetChkClass["asset_" + assetid + ""] = true ? "checkbox checked" : "checkbox";
			$scope.AssetListSelectionClass.AssetSelectionClass["asset_" + assetid + ""] = assetselection;
			var res = [];
			res = $.grep($scope.AssetFilesObj.AssetFiles, function(e) {
				return e.AssetUniqueID == assetid;
			});
			if (res.length > 0) {
				var filename = (res[0].Name != undefined ? res[0].Name : res[0].FileName)
				DownloadAssets(checkbox, assetid, filename, res[0].Extension, res[0].FileGuid, res[0].Category);
			}
			if (checkbox.checked) {
				var remainRecord = [];
				remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function(e) {
					return e == assetid;
				});
				if (remainRecord.length == 0) {
					$scope.AssetFilesObj.AssetSelection.push(assetid);
				}
				if ($scope.AssetFilesObj.AssetSelection.length == $scope.AssetFilesObj.AssetFiles.length) {
				    if ($("#icon_selectassets").hasClass("icon-unchecked")) {
				        $("#icon_selectassets").removeClass("icon-unchecked");
				        $("#icon_selectassets").addClass("icon-check");
				        $("#listselectheader").addClass("checked");
				        $scope.selectallassets(0);
				    }
				}
				if (res[0].Category == 0) {
					AddAttachmentsToTask(assetid, activefileid, true);
				}
			} else {
				var remainRecord = [];
				remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function(e) {
					return e == assetid;
				});
				if (remainRecord.length > 0) {
					$scope.AssetFilesObj.AssetSelection.splice($.inArray(assetid, $scope.AssetFilesObj.AssetSelection), 1);
				}
				$("#listselectheader").removeClass("checked");
				if (res[0].Category == 0) {
					AddAttachmentsToTask(assetid, activefileid, false);
				}
			}
			if (!checkbox.checked) {
				$scope.$emit('checkselectall', false);
			}
		}

		function AddAttachmentsToTask(assetid, activefileid, ischecked) {
			if (ischecked == true) {
				var response = $.grep($scope.AssetFilesObj.AssetFiles, function(e) {
					return e.ActiveFileID == activefileid
				})[0];
				if (response != null) {
					data = $.grep($scope.AssetFilesObj.AssetDynamicData, function(e) {
						return e.ID == assetid;
					});
					var flagattTask = $.grep($scope.DamAssetTaskselectionFiles.AssetFiles, function(e) {
						return e.FileGuid == response.FileGuid
					})[0];
					if (flagattTask == null) {
						$scope.DamAssetTaskselectionFiles.AssetFiles.push(response);
						if (data[0] != undefined) {
							$scope.DamAssetTaskselectionFiles.AssetDynamicData.push(data[0]);
						}
					}
				}
			} else {
				var response = $.grep($scope.AssetFilesObj.AssetFiles, function(e) {
					return e.ActiveFileID == activefileid
				})[0];
				var flagattTask = $.grep($scope.DamAssetTaskselectionFiles.AssetFiles, function(e) {
					return e.FileGuid == response.FileGuid
				})[0];
				var flagdata = $.grep($scope.DamAssetTaskselectionFiles.AssetDynamicData, function(e) {
					return e.ID == assetid
				})[0];
				$scope.DamAssetTaskselectionFiles.AssetFiles.splice($.inArray(flagattTask, $scope.DamAssetTaskselectionFiles.AssetFiles), 1);
				$scope.DamAssetTaskselectionFiles.AssetDynamicData.splice($.inArray(flagdata, $scope.DamAssetTaskselectionFiles.AssetDynamicData), 1);
			}
		}

		function parseSize(bytes) {
			var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"];
			var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
			if (bytes == 0) return '0 Byte';
			var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
			return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
		}

		function DownloadAssets(checkbox, assetid, name, ext, guid, category) {
			if (checkbox.checked) {
				var remainRecord = [];
				remainRecord = $.grep($scope.DownloadFileObj.DownloadFilesArr, function(e) {
					return e.AssetId == assetid;
				});
				if (remainRecord.length == 0) {
					$scope.DownloadFileObj.DownloadFilesArr.push({
						AssetId: assetid,
						Name: name,
						Fileguid: guid,
						Extension: ext,
						Category: category
					});
				}
			} else {
				var remainRecord = [];
				remainRecord = $.grep($scope.DownloadFileObj.DownloadFilesArr, function(e) {
					return e.AssetId == assetid;
				});
				if (remainRecord.length > 0) {
					$scope.DownloadFileObj.DownloadFilesArr.splice($.inArray(remainRecord[0], $scope.DownloadFileObj.DownloadFilesArr), 1);
				}
			}
		}

		function getMoreListItems() {
			$scope.AssetFilesObj.PageNo = $scope.AssetFilesObj.PageNo + 1;
			AssetListService.GetEntityAsset($scope.selectedTree.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo, false).then(function(data) {
				var res = [];
				if (res != null) {
				    if (res.length > 0)
				        loading = false;
				}
				res = data.Response;
				LoadView(res);
			});
			var loader = $("#loader");
			loader.text("Loading New Items");
		}
		$scope.$on('UpdatePublishIcon', function(event, fileids) {
			for (var i = 0; i < fileids.length; i++) {
				$('#Publishicon_' + fileids[i]).removeClass('displayNone')
			}
		});
		var previewTimer = $timeout(function() {
			CheckPreviewGenerator()
		}, 5000);

		function CheckPreviewGenerator() {
			getPreviewIDs();
		}

		function getPreviewIDs() {
			var assetids = [];
			for (var i = 0; i < $('.loadingImg').length; i++) {
				assetids.push($('.loadingImg')[i].id);
			}
			if (assetids.length > 0) {
				AssetListService.CheckPreviewGeneratorID(assetids).then(function(result) {
					var generatedids = result.Response.split(',');
					for (var j = 0; j < generatedids.length; j++) {
						if (generatedids[j] != "") {
							var src = $('#' + generatedids[j]).attr('data-src');
							var extn = '.jpg';
							try {
								var upd = $.grep($scope.AssetFilesObj.AssetFiles, function(e) {
									return e.ActiveFileID == generatedids[j]
								});
								if (upd.length > 0) {
									$.grep($scope.AssetFilesObj.AssetFiles, function(e) {
										return e.ActiveFileID == generatedids[j]
									})[0].Status = 2;
								}
								var lightboxid = $.grep($scope.damlightbox.lightboxselection, function(e) {
									return e.AssetId == upd[0].AssetID
								});
								if (lightboxid.length > 0) {
									$.grep($scope.damlightbox.lightboxselection, function(e) {
										return e.AssetId == upd[0].AssetID
									})[0].Status = 2;
									$.grep($scope.damlightbox.lightboxselection, function(e) {
										return e.AssetId == upd[0].AssetID
									})[0].AssetStatus = 2;
								}
							} catch (e) {}
							$('#' + generatedids[j]).removeClass('loadingImg');
							$('#' + generatedids[j]).attr('src', imagesrcpath + 'DAMFiles/Preview/Small_' + src + '' + extn + '?' + generateUniqueTracker());
						}
					}
				});
			}
			PreviewGeneratorTimer = $timeout(function() {
				CheckPreviewGenerator()
			}, 3000);
		}
		$scope.LoadAssetListViewMetadata = function(qtip_id, assetid, backcolor, shortdesc) {
			$scope.tempEntityTypeTreeText = '';
			if (assetid != null) {
				AssetListService.GetAttributeDetails(assetid).then(function(attrList) {
					$scope.ActiveVersion = $.grep(attrList.Response.Files, function(e) {
						return e.ID == attrList.Response.ActiveFileID;
					})[0];
					var html = '';
					html += '       <div class="lv-ImgDetailedPreview">';
					html += '           <div class="idp-Box"> ';
					html += '               <div class="idp-Block"> ';
					html += '                   <div class="idp-leftSection"> ';
					html += '                       <div class="idp-ImgBlock"> ';
					html += '                           <div class="idp-ImgDiv"> ';
					html += '                               <div class="idp-ImgContainer"> ';
					if (attrList.Response.Category == 0) {
						if ($scope.ActiveVersion.ProcessType > 0) {
							if ($scope.ActiveVersion.Status == 2) html += '<img src="' + imagesrcpath + 'DAMFiles/Preview/Small_' + $scope.ActiveVersion.Fileguid + '.jpg?' + generateUniqueTracker() + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '\'" />';
							else if ($scope.ActiveVersion.Status == 1 || $scope.ActiveVersion.Status == 0) html += '<img class="loadingImg" id=' + $scope.ActiveVersion.ActiveFileID + ' data-extn=' + $scope.ActiveVersion.Extension + ' data-src=' + $scope.ActiveVersion.Fileguid + ' ng-click=\"Editasset(' + $scope.ActiveVersion.AssetUniqueID + ')\" src="assets/img/loading.gif" alt="Image"   onerror="this.onerror=null;this.src=\'' + TenantFilePath1 + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
							else if ($scope.ActiveVersion.Status == 3) {
								html += '<img src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  onerror="this.onerror=null;this.src=\'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
							}
						} else {
							html += '<img src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/' + $scope.ActiveVersion.Extension.substring(1, $scope.ActiveVersion.Extension.length).toUpperCase() + '.jpg?' + generateUniqueTracker() + '' + '" alt="Image"  onerror="this.onerror=null;this.src=\'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
						}
					} else if (attrList.Response.Category == 1) {
						html += '<img src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/BLANK.jpg?' + generateUniqueTracker() + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
					} else if (attrList.Response.Category == 2) {
						html += '<img src="' + imagesrcpath + 'DAMFiles/StaticPreview_small/LINK.jpg?' + generateUniqueTracker() + '" alt="Image"  onerror="this.onerror=null;this.src=\'' + imagesrcpath + 'DAMFiles/Original/NoPreview1.jpg?' + generateUniqueTracker() + '\'" />';
					}
					html += '                               </div> ';
					html += '                           </div> ';
					html += '                       </div> ';
					html += '                       <div class="idp-baiscDetailBlock"> ';
					html += '                           <div class="idp-ActionButtonContainer"> ';
					html += '                               <span> ';
					html += '                                   <i class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i> ';
					html += '                                   <i class="icon-ok displayNone" my-qtip2 qtip-content="Approved and Combined with ..."></i> ';
					if (attrList.Response.Category == 0) {
						if (attrList.Response.IsPublish == true) html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i> ';
						else html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
						if (attrList.Response.PublishLinkDetails != null && attrList.Response.PublishLinkDetails[0].LinkUserDetail != '' && attrList.Response.PublishLinkDetails[0].LinkUserDetail != undefined) html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
						else html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
					} else {
						if (attrList.Response.IsPublish == true) html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up" my-qtip2 qtip-content="Published"></i> ';
						else html += '                                   <i id="Publishicon_' + attrList.Response.ID + '"  class="icon-arrow-up displayNone" my-qtip2 qtip-content="Published"></i> ';
						if (attrList.Response.PublishLinkDetails[0].LinkUserDetail != '') html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link" my-qtip2 qtip-content="Linked"></i>';
						else html += '<i id="Linkicon_' + attrList.Response.ID + '" class="icon-link displayNone" my-qtip2 qtip-content="Linked"></i>';
					}
					html += '                               </span> ';
					html += '                           </div> ';
					html += '                           <div class="idp-detail-eIconContainer"> ';
					html += '                               <span class="idp-eIcon" style="background-color: #' + backcolor + ';">' + shortdesc + '</span>';
					html += '                           </div> ';
					html += '                       </div> ';
					html += '                   </div> ';
					html += '                   <div class="idp-rightSection"> ';
					html += '                       <div class="idp-expandedDetailBlock"> ';
					html += '                           <div class="idp-expandedDetails"> ';
					html += '                               <form class="form-horizontal"> ';
					try {
						html += DrawListBlock(attrList);
					} catch (e) {}
					html += '                               </form> ';
					html += '                           </div> ';
					html += '                       </div> ';
					html += '                   </div> ';
					html += '               </div> ';
					html += '           </div> ';
					html += '       </div>';
					$('#' + qtip_id).html(html);
				});
			}
		};

		function DrawListBlock(attrList) {
			var html = '';
			$scope.dyn_Cont = '';
			refreshdata();
			$scope.attributedata = [];
			if (attrList.Response.AttributeData != null) {
				if ($scope.SettingsAttributes["ListViewToolTipSettings"].length > 0) {
					var tooltipattrs = $.grep($scope.SettingsAttributes["ListViewToolTipSettings"], function(e) {
						return e.assetType == attrList.Response.AssetTypeid
					});
				}
				if (tooltipattrs.length > 0) {
					for (var l = 0; l < tooltipattrs.length; l++) {
						var assetlistdatares = $.grep(attrList.Response.AttributeData, function(e) {
							return e.ID == tooltipattrs[l].ID
						});
						if (assetlistdatares.length > 0) {
							$scope.attributedata.push(assetlistdatares[0]);
						}
					}
				}
				html += '<div class="control-group recordRow">';
				html += '    <label class="control-label">Asset name</label>';
				html += '    <div class="controls rytSide">';
				html += '        <label class="control-label">' + attrList.Response.Name + '</label>';
				html += '</div></div>';
				for (var i = 0; i < $scope.attributedata.length; i++) {
					if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == false) {
						$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
						if ($scope.attributedata[i].Caption == null) {
							$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html("-").text();
							$scope.attributedata[i].Caption = "-";
						} else if ($scope.attributedata[i].Caption != undefined) {
							$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
						}
						html += '<div  class=\"control-group recordRow\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls rytSide\"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
					} else if ($scope.attributedata[i].TypeID == 1 && $scope.attributedata[i].IsSpecial == true) {
						$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
						if ($scope.attributedata[i].Caption != undefined) {
							$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Value).text();
						}
						html += '<div class=\"control-group recordRow\"><label class=\"control-label\"\>' + 'Asset Name' + '</label><div class=\"controls rytSide\"><span class="editable">' + $scope.attributedata[i].Value + '</span></div></div>';
					} else if ($scope.attributedata[i].TypeID == 2) {
						$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = "-";
						if ($scope.attributedata[i].Caption == null) {
							$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html("-").text();
							$scope.attributedata[i].Caption = "-";
						} else if ($scope.attributedata[i].Caption != undefined) {
							$scope.fields["SingleLineTextValue_" + $scope.attributedata[i].ID] = $('<div />').html($scope.attributedata[i].Caption).text();
						}
						html += '<div class=\"control-group recordRow\"><label class=\"control-label\"\>' + $scope.attributedata[i].Lable + '</label><div class=\"controls rytSide\"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
					} else if ($scope.attributedata[i].TypeID == 3) {
						if ($scope.attributedata[i].ID == SystemDefiendAttributes.Owner && $scope.attributedata[i].IsSpecial == true) {
							if ($scope.attributedata[i].Caption[0] != undefined) {
								$scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
								$scope.setFieldKeys();
								$scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
								$scope.setNormalDropdownCaption();
								html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption[0] + '</span></div></div>';
							} else {
								$scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
								$scope.setFieldKeys();
								$scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
								$scope.setNormalDropdownCaption();
								html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
								html += '<div class="controls rytSide"><span>' + $scope.attributedata[i].Lable + '</span>';
								html += '</div></div>';
							}
						} else {
							if ($scope.attributedata[i].Caption[0] != undefined) {
								if ($scope.attributedata[i].Caption[0].length > 1) {
									$scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption[0];
									$scope.setFieldKeys();
									$scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
									$scope.setNormalDropdownCaption();
									html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption[0] + '</span></div></div>';
								}
							} else {
								$scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] = "-";
								$scope.setFieldKeys();
								$scope.NormalDropdownCaption["NormalDropDownCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
								$scope.setNormalDropdownCaption();
								html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.fields["NormalDropDown_" + $scope.attributedata[i].ID] + '</span></div></div>';
							}
						}
					} else if ($scope.attributedata[i].TypeID == 4) {
						if ($scope.attributedata[i].Caption[0] != undefined) {
							if ($scope.attributedata[i].Caption.length > 1) {
								$scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
								$scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
								$scope.setNormalMultiDropdownCaption();
								html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
							}
						} else {
							$scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] = "-";
							$scope.NormalMultiDropdownCaption["NormalMultiDropDown_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
							$scope.setNormalMultiDropdownCaption();
							html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.fields["NormalMultiDropDown_" + $scope.attributedata[i].ID] + '</span></div></div>';
						}
					} else if ($scope.attributedata[i].TypeID == 17) {
						if ($scope.attributedata[i].Caption[0] != undefined) {
							if ($scope.attributedata[i].Caption.length > 1) {
								$scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Caption;
								$scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
								$scope.setNormalMultiDropdownCaption();
								html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.attributedata[i].Caption + '</span></div></div>';
							}
						} else {
							$scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] = "-";
							$scope.NormalMultiDropdownCaption["TagWordsCaption_" + $scope.attributedata[i].ID] = $scope.attributedata[i].Lable;
							$scope.setNormalMultiDropdownCaption();
							html += '<div  class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.fields["TagWordsCaption_" + $scope.attributedata[i].ID] + '</span></div></div>';
						}
					} else if ($scope.attributedata[i].TypeID == 10) {
						var inlineEditabletitile = $scope.attributedata[i].Caption;
						if ($scope.attributedata[i].Value == "-") {
							$scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID] = '-';
							$scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] = "-";
							html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
							html += '<div class="controls rytSide">';
							html += '<span class="editable">' + $scope.fields["PeriodStartEndDate_" + $scope.attributedata[i].ID];
							html += '</span>';
							html += '</div>';
							html += '</div>';
							html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
							html += '<div class="controls rytSide">';
							html += '<span class="editable">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].ID] + '</span>';
							html += '</div>';
							html += '</div>';
						} else {
							for (var j = 0; j < $scope.attributedata[i].Value.length; j++) {
								var datStartUTCval = "";
								var datstartval = "";
								var datEndUTCval = "";
								var datendval = "";
								datstartval = new Date($scope.attributedata[i].Value[j].Startdate);
								datendval = new Date($scope.attributedata[i].Value[j].EndDate);
								$scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
								$scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] = ConvertDateFromStringToString(ConvertDateToString(datendval));
								if ($scope.attributedata[i].Value[j].Description == undefined) {
									$scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = "-";
								} else {
									$scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] = $scope.attributedata[i].Value[j].Description;
								}
								html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label>';
								html += '<div class="controls rytSide">';
								html += '<span class="editable">' + $scope.fields["PeriodStartDate_" + $scope.attributedata[i].Value[j].Id];
								html += ' to ' + $scope.fields["PeriodEndDate_" + $scope.attributedata[i].Value[j].Id] + '</span>';
								html += '</div>';
								html += '</div>';
								html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">Comment ' + inlineEditabletitile + '</label>';
								html += '<div class="controls rytSide">';
								html += '<span class="editable">' + $scope.fields["PeriodDateDesc_" + $scope.attributedata[i].Value[j].Id] + '</span>';
								html += '</div>';
								html += '</div>';
							}
						}
					} else if ($scope.attributedata[i].TypeID == 5 && $scope.attributedata[i].ID != SystemDefiendAttributes.ApproveTime) {
						var datStartUTCval = "";
						var datstartval = "";
						var inlineEditabletitile = $scope.attributedata[i].Caption;
						$scope.ShowHideAttributeOnRelation["Attribute_" + $scope.attributedata[i].ID] = true;
						if ($scope.attributedata[i].Value != null || $scope.attributedata[i].Value != undefined) {
							datstartval = new Date($scope.attributedata[i].Value);
							$scope.fields["DateTime_" + $scope.attributedata[i].ID] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
							$scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = ConvertDateFromStringToString(ConvertDateToString(datstartval));
						} else {
							$scope.fields["DateTime_" + $scope.attributedata[i].ID] = "-";
							$scope.fields["DateTime_Dir_" + $scope.attributedata[i].ID] = "-";
						}
						if ($scope.attributedata[i].ID != SystemDefiendAttributes.CreationDate) {
							if ($scope.attributedata[i].IsReadOnly == true) {
								html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide\"><span class="editable">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</span></div></div>';
							} else {
								html += '<div class=\"control-group recordRow\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls rytSide"><span class="editable">' + $scope.fields["DateTime_" + $scope.attributedata[i].ID] + '</span></div></div>';
							}
						} else {
							$scope.dyn_Cont += '<div ng-show= ShowHideAttributeOnRelation.Attribute_' + $scope.attributedata[i].ID + ' class=\"control-group\"><label class=\"control-label\" for=\"label\">' + $scope.attributedata[i].Lable + '</label> <div class="controls"><label class="control-label">{{fields.DateTime_' + $scope.attributedata[i].ID + '}}</label></div></div>';
						}
					}
				}
			}
			return html;
		}

		function refreshdata() {
			$scope.ShowHideAttributeOnRelation = {};
			$scope.treelevels = {};
			$scope.fieldoptions = [];
			$scope.OwnerList = [];
			$scope.owner = {};
			$scope.NormalDropdownCaption = {};
			$scope.NormalDropdownCaptionObj = [];
			$scope.normaltreeSources = {};
			$scope.setNormalDropdownCaption = function() {
				var keys1 = [];
				angular.forEach($scope.NormalDropdownCaption, function(key) {
					keys1.push(key);
					$scope.NormalDropdownCaptionObj = keys1;
				});
			}
			$scope.NormalMultiDropdownCaption = {};
			$scope.NormalMultiDropdownCaptionObj = [];
			$scope.setNormalMultiDropdownCaption = function() {
				var keys1 = [];
				angular.forEach($scope.NormalMultiDropdownCaption, function(key) {
					keys1.push(key);
					$scope.NormalMultiDropdownCaptionObj = keys1;
				});
			}
			$scope.setFieldKeys = function() {
				var keys = [];
				angular.forEach($scope.fields, function(key) {
					keys.push(key);
					$scope.fieldKeys = keys;
				});
			}
			$scope.fields = {
				usersID: ''
			};
			$scope.UserId = $cookies['UserId'];
		}

		function checkListItemContents() {
			getMoreListItems(function() {});
		}
		var loading = false;

		function loadScrollSettings() {
		    if (localfolderid != 0) {
		        $(window).scroll(function () {

		            if (loading) {
		                return;
		            }

		            if ($(window).scrollTop() > $(document).height() - $(window).height() - 200) {
		                loading = true;

		                checkListItemContents();


		            }

		        });
		    } else {
		        $(".assetlistscroll").bind("scroll", function (e) {

		            if ($(this)[0].scrollHeight - $(this).scrollTop() < 2000) {
		                //if (($(this)[0].scrollHeight - 150) < 2000) {
		                checkListItemContents();

		            }
		        });
		    }


		}
		$scope.$on("$destroy", function() {
			$(window).unbind('scroll');
			$timeout.cancel(previewTimer);
			$timeout.cancel(PreviewGeneratorTimer);
		});
		$scope.SetAssetActionId = function(assetid) {
			$scope.AssetFilesObj.selectedAssetId = assetid;
			var remainRecord = [];
			remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function(e) {
				return e == $scope.AssetFilesObj.selectedAssetId;
			});
			if (remainRecord.length == 0) {
				$scope.AssetFilesObj.AssetSelection.push($scope.AssetFilesObj.selectedAssetId);
			}
			var remdownloadrecord = [];
			remdownloadrecord = $.grep($scope.DownloadFileObj.DownloadFilesArr, function(e) {
				return e.AssetId == assetid;
			});
			if (remdownloadrecord.length == 0) {
				var res = $.grep($scope.AssetFilesObj.AssetFiles, function(e) {
					return e.AssetUniqueID == assetid
				});
				if (res.length > 0) $scope.DownloadFileObj.DownloadFilesArr.push({
					AssetId: assetid,
					FileName: res[0].FileName,
					Fileguid: res[0].FileGuid,
					Extension: res[0].Extension,
					Category: res[0].Category
				});
			}
			$('#chkBoxIcon_' + assetid + '').addClass('checked');
			var checkboxchecked = true;
			var assetselection = "";
			assetselection = checkboxchecked == true ? "li-Selection selected" : "li-Selection";
			$scope.AssetListSelection.AssetChkClass["asset_" + assetid + ""] = checkboxchecked == true ? "checkbox checked" : "checkbox";
			$scope.AssetListSelectionClass.AssetSelectionClass["asset_" + assetid + ""] = assetselection;
			$scope.AssetListSelection.AssetSelection["asset_'" + assetid + ""] = checkboxchecked;
			if ($scope.AssetFilesObj.AssetSelection.length == 0 || $scope.AssetFilesObj.AssetSelection.length == 1) {
				if ($scope.AssetFilesObj.AssetSelection[0] == assetid || $scope.AssetFilesObj.AssetSelection.length == 0) {
					var temparr = $.grep($scope.AssetFilesObj.AssetFiles, function(e) {
						return e.Category == 1 && e.AssetUniqueID == assetid;
					});
					if (temparr.length > 0) {
						$scope.IsPublish = false;
						$scope.UnPublish = false;
						$scope.showhidepackage = false;
						$scope.Isblankasset = false;
						return true;
					} else {
						$scope.showhidepackage = true;
						$scope.Isblankasset = true;
					}
				}
			}
			if ($scope.AssetFilesObj.AssetSelection.length == 0 || $scope.AssetFilesObj.AssetSelection.length == 1) {
				if ($scope.AssetFilesObj.AssetSelection[0] == assetid || $scope.AssetFilesObj.AssetSelection.length == 0) {
					if ($('#Publishicon_' + assetid)[0].className == "icon-arrow-up") {
						$scope.IsPublish = false;
						if ($scope.userpublishaccess == true)
						    $scope.UnPublish = true;
						else
						    $scope.UnPublish = false;
					} else {					    
						$scope.UnPublish = false;
					}
				} else {				  
					$scope.UnPublish = true;
					$scope.showhidepackage = true;
				}
			} else {			    
				$scope.UnPublish = true;
				$scope.showhidepackage = true;
				$scope.Isblankasset = true;
			}
		}
		$scope.CallBackAssetAction = function(assetid, istask, tasktype) {
			var actionName = assetAction.get(assetid);
			if (actionName != undefined) {
			    $scope.$emit('DamAssetAction', actionName, istask, tasktype);
			    userpublishaccess($stateParams.ID);
			}
		}
		$scope.Editasset = function(AssetID) {
			$timeout(function() {
				$scope.$emit('callBackAssetEditinMUI', AssetID, $scope.AssetFilesObj.IsLock, false, 'List');
			}, 100);
		}
		$scope.$on('loadAssetDetailSection_th_List', function(event, assetList) {
			AssetListService.GetAssetDetsforSelectedAssets($scope.selectedTree.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo, false, assetList).then(function(data) {
				var res = [];
				res = data.Response;
				LoadViewDetails(res);
			});
		});

		function LoadViewDetails(res) {
			if (res != null) {
			    var assets = [];
			    loading = false;
				assets = res[0].AssetFiles;
				for (var j = 0, asset; asset = assets[j++];) {
					var assetData = $.grep($scope.AssetFilesObj.AssetFiles, function(rel) {
						return rel.AssetUniqueID == asset.AssetUniqueID
					});
					var assettempData = $.grep($scope.AssetFilesObj.AssetFiles, function(rel) {
						return rel.AssetUniqueID == asset.AssetUniqueID
					});
					if (assetData != null) {
						if (assetData.length == 0) {
							$scope.AssetFilesObj.AssetFiles.push(asset);
							$scope.AssetFilesObj_Temp.AssetFiles.push(asset);
						} else {
							$scope.AssetFilesObj.AssetFiles.splice($.inArray(assetData[0], $scope.AssetFilesObj.AssetFiles), 1);
							$scope.AssetFilesObj.AssetFiles.push(asset);
							if (assettempData.length > 0) {
								var Indexofasset = $scope.AssetFilesObj_Temp.AssetFiles.indexOf(assettempData[0]);
								$scope.AssetFilesObj_Temp.AssetFiles.splice(Indexofasset, 1, assetData[0]);
							}
						}
					} else {
						$scope.AssetFilesObj.AssetFiles.push(asset);
						$scope.AssetFilesObj_Temp.AssetFiles.push(asset);
					}
				}
			}
		}

		$scope.assetMenuActions = function (action) {

		    var AssetObj = {};
		    AssetObj.IsLock = $scope.AssetFilesObj.IsLock,
            AssetObj.isNotify = false,
            AssetObj.viewtype = 'List'
		    $scope.$emit('callBacktoDam', action);
		}

	}
	app.controller("mui.DAM.assetlistCtrl", ['$window', '$location', '$timeout', '$scope', '$cookies', '$resource', '$stateParams', 'AssetListService', '$compile', muiDAMassetlistCtrl]);
})(angular, app);