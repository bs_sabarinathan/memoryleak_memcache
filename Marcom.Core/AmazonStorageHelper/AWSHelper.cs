﻿using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BrandSystems.Marcom.Core.AmazonStorageHelper
{
    public class AWSHelper
    {
        /// <summary>
        /// Upload the local file to the Amazon s3
        /// </summary>
        /// <param name="Amazon client"></param>
        /// <param name="BUCKET_NAME"></param> the amazon bucket name
        /// <param name="desireKey"></param> the amazon file key ( must be with the file extension. Example is foo.jpg )
        /// <param name="sysPath"></param> system file path (example: D://foo.jpg)
        /// <returns>System.Net.HttpStatusCode</returns>
        /// <remarks>This is just a wrapper for the default provider. For Amazon Error Code http://docs.aws.amazon.com/AmazonS3/latest/API/ErrorResponses.html </remarks>
        public static System.Net.HttpStatusCode uploadfile(AmazonS3Client client, string bucketname, string desireKey, string sysPath)
        {
            try
            {

                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = bucketname,
                    Key = desireKey.Replace("\\", "/"),
                    FilePath = sysPath,
                    CannedACL = S3CannedACL.PublicRead
                };
                PutObjectResponse response2 = client.PutObject(request);

                return System.Net.HttpStatusCode.OK;
            }

            catch (AmazonS3Exception amazonS3Exception)
            {
                string logfilepath = ConfigurationManager.AppSettings["MarcomPresentation"] + "Tenants/BucketUploadFailLog.txt";
                string logText = "\r\n Failed to upload File at " + DateTime.Now + "\r\n  SourcePath : " + sysPath + "\r\n  Target S3Key : " + desireKey + "\r\n  Exception Message : " + amazonS3Exception.Message.ToString() + "\r\n *********************************************************************************";
                System.IO.File.AppendAllText(logfilepath, logText + Environment.NewLine);

                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }
                else
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }





            }

            catch (Exception ex)
            {
                string logfilepath = ConfigurationManager.AppSettings["MarcomPresentation"] + "Tenants/BucketUploadFailLog.txt";
                string logText = "\r\n Failed to upload File at " + DateTime.Now + "\r\n  SourcePath : " + sysPath + "\r\n  Target S3Key : " + desireKey + "\r\n  Exception Message : " + ex.Message.ToString() + "\r\n *********************************************************************************";
                System.IO.File.AppendAllText(logfilepath, logText + Environment.NewLine);
                return System.Net.HttpStatusCode.InternalServerError;
            }
        }

        /// <summary>
        /// copy and delete the file in s3 - since the rename feature they don't allow
        /// </summary>
        /// <param name="Amazon client"></param>
        /// <param name="sourceBucket"></param> the amazon source bucket name
        /// <param name="sourceKey"></param> the amazon source file key ( must be with the file extension. Example is foo.jpg )
        /// <param name="destinationBucket"></param> the amazon destination bucket name
        /// <param name="destinationKey"></param> the amazon destination file key ( must be with the file extension. Example is foo.jpg )
        /// <returns>System.Net.HttpStatusCode</returns>
        /// <remarks>This method actually the replace for the rename object. since the rename feature they don't allow. For Amazon Error Code http://docs.aws.amazon.com/AmazonS3/latest/API/ErrorResponses.html </remarks>
        public static System.Net.HttpStatusCode CopyandDeleteS3Object(AmazonS3Client client, string sourceBucket, string sourceKey, string destinationBucket, string destinationKey)
        {

            try
            {
                //Copy the object

                CopyObjectRequest copyRequest = new CopyObjectRequest
                {
                    SourceBucket = sourceBucket,
                    SourceKey = sourceKey.Replace("\\", "/"),
                    DestinationBucket = destinationBucket,
                    DestinationKey = destinationKey.Replace("\\", "/"),
                    CannedACL = S3CannedACL.PublicRead
                };
                client.CopyObject(copyRequest);

                //Delete the original
                DeleteObjectRequest deleteRequest = new DeleteObjectRequest()
                {
                    BucketName = sourceBucket,
                    Key = sourceKey.Replace("\\", "/")
                };
                client.DeleteObject(deleteRequest);

                return System.Net.HttpStatusCode.OK;

            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }
                else
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }

            }
            catch (Exception ex)
            {

                return System.Net.HttpStatusCode.InternalServerError;
            }
        }

        /// <summary>
        /// copy the file in s3
        /// </summary>
        /// <param name="Amazon client"></param>
        /// <param name="sourceBucket"></param> the amazon source bucket name
        /// <param name="sourceKey"></param> the amazon source file key ( must be with the file extension. Example is foo.jpg )
        /// <param name="destinationBucket"></param> the amazon destination bucket name
        /// <param name="destinationKey"></param> the amazon destination file key ( must be with the file extension. Example is foo.jpg )
        /// <returns>System.Net.HttpStatusCode</returns>
        /// <remarks>Copy the s3 object in s3 bucket. For Amazon Error Code http://docs.aws.amazon.com/AmazonS3/latest/API/ErrorResponses.html </remarks>
        public static System.Net.HttpStatusCode CopyS3Object(AmazonS3Client client, string sourceBucket, string sourceKey, string destinationBucket, string destinationKey)
        {
            try
            {
                //Copy the object
                CopyObjectRequest copyRequest = new CopyObjectRequest
                {
                    SourceBucket = sourceBucket,
                    SourceKey = sourceKey.Replace("\\", "/"),
                    DestinationBucket = destinationBucket,
                    DestinationKey = destinationKey.Replace("\\", "/"),
                    CannedACL = S3CannedACL.PublicRead
                };
                client.CopyObject(copyRequest);
                return System.Net.HttpStatusCode.OK;

            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }
                else
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }

            }
            catch (Exception ex)
            {

                return System.Net.HttpStatusCode.InternalServerError;
            }
        }

        /// <summary>
        /// Delete the file from s3
        /// </summary>
        /// <param name="Amazon client"></param>
        /// <param name="sourceBucket"></param> the amazon source bucket name
        /// <param name="sourceKey"></param> the amazon source file key ( must be with the file extension. Example is foo.jpg )
        /// <returns>System.Net.HttpStatusCode</returns>
        /// <remarks>delete the s3 object from s3 bucket. For Amazon Error Code http://docs.aws.amazon.com/AmazonS3/latest/API/ErrorResponses.html </remarks>
        public static System.Net.HttpStatusCode DeleteS3Object(AmazonS3Client client, string sourceBucket, string sourceKey)
        {
            try
            {
                //Delete the original
                DeleteObjectRequest deleteRequest = new DeleteObjectRequest()
                {
                    BucketName = sourceBucket,
                    Key = sourceKey.Replace("\\", "/"),
                };
                client.DeleteObject(deleteRequest);

                return System.Net.HttpStatusCode.OK;

            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }
                else
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }

            }
            catch (Exception ex)
            {

                return System.Net.HttpStatusCode.InternalServerError;
            }
        }

        /// <summary>
        /// List out All the Files from the Amazon Bucket
        /// </summary>
        /// <param name="Amazon client"></param>
        /// <param name="bucketName"></param> the amazon source bucket name
        /// <returns>object</returns>
        /// <remarks>List out all the keys in the particular bucket. For Amazon Error Code http://docs.aws.amazon.com/AmazonS3/latest/API/ErrorResponses.html </remarks>
        public static object ListOutAllKeys(AmazonS3Client client, string bucketName)
        {

            try
            {
                List<Object> files = new List<object>();

                try
                {
                    ListObjectsRequest request = new ListObjectsRequest
                    {
                        BucketName = bucketName
                    };
                    do
                    {
                        ListObjectsResponse response = client.ListObjects(request);
                        // Process response.
                        foreach (S3Object entry in response.S3Objects)
                        {

                            if (entry.Size > 0)
                            {

                                string[] keySplit = entry.Key.Split('/');
                                string fileName = keySplit[keySplit.Length - 1];
                                string extension = System.IO.Path.GetExtension(fileName);
                                object flObj = new { fileName, entry.Size, entry.Key, extension };
                                files.Add(flObj);
                            }

                        }

                        // If response is truncated, set the marker to get the next 
                        // set of keys.
                        if (response.IsTruncated)
                        {
                            request.Marker = response.NextMarker;
                        }
                        else
                        {
                            request = null;
                        }
                    } while (request != null);


                }
                catch (AmazonS3Exception amazonS3Exception)
                {
                    if (amazonS3Exception.ErrorCode != null &&
                        (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                        ||
                        amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                    {
                        return null;
                    }
                    else
                    {
                        return null;
                    }

                }

                object objForm = new { files };
                return objForm;

            }
            catch (Exception ex)
            {

                return null;
            }
        }

        /// <summary>
        /// download the file from the s3 to local drive
        /// </summary>
        /// <param name="Amazon client"></param>
        /// <param name="bucketName"></param> the amazon source bucket name
        /// <param name="s3Key"></param> the amazon file key ( must be with the file extension and full path directory. Example is "Tenants/Client1/DamFiles/Original/foo.jpg" )
        /// <param name="destination"></param> the local drive destination folder path ( must be with the file extension and full path directory. Example is "D://c6eec3f6-3506-4ede-903b-28acdb95fdb7.jpg" ) Or can be System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Files", newcopyname)
        /// <returns>HttpStatusCode</returns>
        /// <remarks>download the file from the s3 to local drive. For Amazon Error Code http://docs.aws.amazon.com/AmazonS3/latest/API/ErrorResponses.html </remarks>
        public static HttpStatusCode downloadsfile(AmazonS3Client client, string bucketName, string s3Key, string extension, string destination)
        {

            try
            {

                string keyName = s3Key;
                string[] keySplit = keyName.Split('/');
                string fileName = keySplit[keySplit.Length - 1];
                string newcopyname = Guid.NewGuid().ToString() + extension;

                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = bucketName,
                    Key = s3Key.Replace("\\", "/"),
                };
                using (GetObjectResponse getObjectResponse = client.GetObject(request))
                {
                    if (!System.IO.File.Exists(destination))
                    {
                        using (System.IO.Stream s = getObjectResponse.ResponseStream)
                        {
                            using (System.IO.FileStream fs = new System.IO.FileStream(destination, System.IO.FileMode.Create, System.IO.FileAccess.Write))
                            {
                                byte[] data = new byte[32768];
                                int bytesRead = 0;
                                do
                                {
                                    bytesRead = s.Read(data, 0, data.Length);
                                    fs.Write(data, 0, bytesRead);
                                }
                                while (bytesRead > 0);
                                fs.Flush();
                            }
                        }
                    }
                }

                return System.Net.HttpStatusCode.OK;

            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }
                else
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }

            }
            catch (Exception ex)
            {
                return System.Net.HttpStatusCode.InternalServerError;
            }
        }

        /// <summary>
        /// download the file from the s3 to local drive
        /// </summary>
        /// <param name="Amazon client"></param>
        /// <param name="bucketName"></param> the amazon source bucket name
        /// <param name="s3Key"></param> the amazon file key ( must be with the file extension and full path directory. Example is "Tenants/Client1/DamFiles/Original/foo.jpg" )
        /// <param name="destination"></param> the local drive destination folder path ( must be with the file extension and full path directory. Example is "D://c6eec3f6-3506-4ede-903b-28acdb95fdb7.jpg" ) Or can be System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Files", newcopyname)
        /// <returns>HttpStatusCode</returns>
        /// <remarks>download the file from the s3 to local drive. For Amazon Error Code http://docs.aws.amazon.com/AmazonS3/latest/API/ErrorResponses.html </remarks>
        public static HttpStatusCode directDownloadfile(AmazonS3Client client, string bucketName, string s3Key, string extension, string destination)
        {

            try
            {
                S3FileInfo s3FileInfo = new Amazon.S3.IO.S3FileInfo(client, bucketName, s3Key.Replace("\\", "/"));
                if (s3FileInfo.Exists)
                {
                    s3FileInfo.CopyToLocal(destination);
                    // file exists
                }
                else
                {
                    // file does not exist
                }

                return System.Net.HttpStatusCode.OK;

            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }
                else
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }

            }
            catch (Exception ex)
            {
                return System.Net.HttpStatusCode.InternalServerError;
            }
        }

        /// <summary>
        ///  Reads object from S3.
        /// </summary>
        /// <param name="Amazon client"></param>
        /// <param name="bucketName"></param> the amazon source bucket name
        /// <param name="s3Key"></param> the amazon source file key ( must be with the file extension. Example is foo.jpg )
        /// <returns>string</returns>
        /// <remarks>Reads object from S3. For Amazon Error Code http://docs.aws.amazon.com/AmazonS3/latest/API/ErrorResponses.html </remarks>
        public static bool isKeyExists(AmazonS3Client client, string bucketName, string s3Key)
        {
            try
            {
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = bucketName,
                    Key = s3Key.Replace("\\", "/")
                };

                GetObjectResponse response = client.GetObject(request);
                StreamReader reader = new StreamReader(response.ResponseStream);
                String content = reader.ReadToEnd();
                if (content.Length > 0)
                    return true;
                else return false;

            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return false;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        ///  Determine if an object exists in a S3 bucket
        /// </summary>
        /// <param name="Amazon client"></param>
        /// <param name="bucketName"></param> the amazon source bucket name
        /// <param name="s3Key"></param> the amazon source file key ( must be with the file extension. Example is foo.jpg )
        /// <returns>bool</returns>
        /// <remarks>Determine if an object exists in a S3 bucket. For Amazon Error Code http://docs.aws.amazon.com/AmazonS3/latest/API/ErrorResponses.html </remarks>
        private static string ReadS3Object(AmazonS3Client client, string bucketName, string s3Key)
        {
            try
            {
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = bucketName,
                    Key = s3Key.Replace("\\", "/")
                };

                GetObjectResponse response = client.GetObject(request);
                StreamReader reader = new StreamReader(response.ResponseStream);
                String content = reader.ReadToEnd();
                return content;

            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return null;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Creates bucket if it is not exists.
        /// </summary>
        /// <param name="client"></param>
        private static bool CreateBucket(AmazonS3Client client, string bucketname)
        {

            try
            {
                ListBucketsResponse response = client.ListBuckets();

                bool found = false;
                foreach (S3Bucket bucket in response.Buckets)
                {
                    if (bucket.BucketName == bucketname)
                    {
                        //Bucket found will not create it.
                        found = true;
                        break;
                    }
                }

                if (found == false)
                {
                    //Bucket not found will create it.
                    PutBucketRequest request = new PutBucketRequest
                    {
                        BucketName = bucketname
                    };

                    client.PutBucket(request);

                    //Created S3 bucket with the given name

                    return true;
                }
                else return false;

            }

            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return false;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Writes sample S3 object.
        /// </summary>
        /// <param name="client"></param>
        private static bool WriteS3Object(AmazonS3Client client, string bucketname, string s3key)
        {
            try
            {

                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = bucketname,
                    Key = s3key.Replace("\\", "/"),
                    ContentBody = "This is body of S3 object.",
                    CannedACL = S3CannedACL.PublicRead
                };
                // You can also put a file as S3 Object content, but you must 
                // remove request.WithContentBody in this case.
                // request.WithFilePath(pathToFile);

                //Writing S3 object with key " + S3_KEY + " in bucket " + bucketname;

                client.PutObject(request);

                return true;

            }

            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return false;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Read S3 object metadata.
        /// </summary>
        /// <param name="client"></param>
        private static void ReadS3ObjectMetadata(AmazonS3Client client, string bucketName, string s3Key)
        {

            GetObjectMetadataRequest request = new GetObjectMetadataRequest
            {
                BucketName = bucketName,
                Key = s3Key.Replace("\\", "/")
            };
            GetObjectMetadataResponse response = client.GetObjectMetadata(request);
            foreach (string key in response.Metadata.Keys)
            {
                //Console.Out.WriteLine("   key: " + key + ", value: " + response.Metadata[key]);
            }
        }
        public static int ReadS3Objectfilesize(AmazonS3Client client, string bucketName, string s3Key)
        {

            GetObjectRequest request = new GetObjectRequest
            {
                BucketName = bucketName,
                Key = s3Key.Replace("\\", "/")
            };
            int filesize = 0;
            GetObjectResponse response = client.GetObject(request);
            StreamReader reader = new StreamReader(response.ResponseStream);
            String content = reader.ReadToEnd();
            filesize = content.Length;

            return filesize;


        }
        /// <summary>
        ///  Reads ACL for S3 object.
        /// </summary>
        /// <param name="client"></param>
        private static void GetS3ObjectAccessList(AmazonS3Client client, string bucketname, string s3key)
        {

            GetACLRequest request = new GetACLRequest
            {
                BucketName = bucketname,
                Key = s3key.Replace("\\", "/")
            };
            GetACLResponse response = client.GetACL(request);
            // Console.Out.WriteLine("Object owner is " + response.AccessControlList.Owner.DisplayName);
        }

        /// <summary>
        /// Generates URL to access S3 object for a week from now. 
        /// </summary>
        /// <param name="client"></param>
        private static void GenerateAccessURL(AmazonS3Client client, string bucketname, string s3key)
        {

            GetPreSignedUrlRequest request = new GetPreSignedUrlRequest
            {
                BucketName = bucketname,
                Key = s3key.Replace("\\", "/"),
                Expires = DateTime.Now.Add(new TimeSpan(7, 0, 0, 0))
            };
            Console.Out.WriteLine("Generated signed URL that will be valid for a week for object: " + client.GetPreSignedURL(request));

        }

        /// <summary>
        /// Checks if logging is enabled for this bucket.
        /// </summary>
        /// <param name="client"></param>
        private static void GetBucketLoggingRequest(AmazonS3Client client, string bucketname)
        {

            GetBucketLoggingRequest request = new GetBucketLoggingRequest
            {
                BucketName = bucketname
            };
            GetBucketLoggingResponse response = client.GetBucketLogging(request);
            bool loggingEnabled = string.IsNullOrEmpty(response.BucketLoggingConfig.TargetBucketName) == false;
            Console.Out.WriteLine("Bucket logging is " + loggingEnabled);
        }

        /// <summary>
        /// Enumerates all versions of object.
        /// </summary>
        /// <param name="client"></param>
        private static void GetObjectVersions(AmazonS3Client client, string bucketname, string s3key)
        {

            ListVersionsRequest request = new ListVersionsRequest
            {
                BucketName = bucketname,
                Prefix = s3key.Replace("\\", "/"),
            };
            ListVersionsResponse response = client.ListVersions(request);
            Console.Out.WriteLine("Found the following versions for prefix " + s3key);
            foreach (S3ObjectVersion version in response.Versions)
            {
                Console.Out.WriteLine("   version id: " + version.VersionId + ", last modified time: " + version.LastModified);
            }
        }

        /// <summary>
        /// CreatenewFolder.
        /// </summary>
        /// <param name="client"></param>
        public static bool CreatenewFolder(AmazonS3Client client, string bucketName, string folderName)
        {
            try
            {
                var folderKey = folderName.Replace("\\", "/") + "/"; //end the folder name with "/"
                PutObjectRequest request = new PutObjectRequest
                {
                    BucketName = bucketName,
                    StorageClass = S3StorageClass.Standard,
                    ServerSideEncryptionMethod = ServerSideEncryptionMethod.None,
                    CannedACL = S3CannedACL.PublicReadWrite,
                    Key = folderKey,
                    ContentBody = string.Empty
                };
                PutObjectResponse response = client.PutObject(request);
                return true;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return false;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        /// <summary>
        /// DoesFolderExists.
        /// </summary>
        /// <param name="client"></param>
        public static bool DoesFolderExists(AmazonS3Client client, string bucketName, string folderName)
        {

            try
            {
                ListObjectsRequest request = new ListObjectsRequest();
                request.BucketName = bucketName;
                request.Prefix = folderName.Replace("\\", "/") + "/";
                request.MaxKeys = 1;
                ListObjectsResponse response = client.ListObjects(request);
                return (response.S3Objects.Count > 0);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return false;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        /// <summary>
        /// DeleteExpiredFolders.
        /// </summary>
        /// <param name="client"></param>
        public static bool DeleteExpiredFolders(AmazonS3Client client, string bucketName, string folderName, int frequency)
        {

            DateTime expireCheck = DateTime.Now.AddDays(frequency);
            try
            {
                ListObjectsRequest request = new ListObjectsRequest();
                request.BucketName = "bucketName";
                request.Prefix = folderName + "/";
                ListObjectsResponse response = client.ListObjects(request);
                foreach (S3Object o in response.S3Objects)
                {
                    //Console.WriteLine("{0}\t{1}\t{2}", o.Key, o.Size, o.LastModified);
                    if (o.LastModified <= expireCheck)
                        DeleteFile(client, bucketName, o.Key);
                }
                return true;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return false;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        /// <summary>
        /// DeleteFile.
        /// </summary>
        /// <param name="client"></param>
        public static bool DeleteFile(AmazonS3Client client, string bucketName, string s3key)
        {

            try
            {
                DeleteObjectRequest request = new DeleteObjectRequest();
                request.BucketName = bucketName;
                request.Key = s3key.Replace("\\", "/");
                client.DeleteObject(request);

                return true;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return false;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        /// <summary>
        /// LISTING OWNED BUCKETS.
        /// </summary>
        /// <param name="client"></param>
        public static List<object> ListingOwnedBuckets(AmazonS3Client client)
        {
            List<object> ownBuckts = new List<object>();
            try
            {
                ListBucketsResponse response = client.ListBuckets();
                foreach (S3Bucket b in response.Buckets)
                {
                    ownBuckts.Add(new { BucketName = b.BucketName, CreationDate = b.CreationDate });
                    // Console.WriteLine("{0}\t{1}", b.BucketName, b.CreationDate);
                }

                return ownBuckts;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return ownBuckts;
                }
                else
                {
                    return ownBuckts;
                }

            }
            catch (Exception ex)
            {
                return ownBuckts;
            }

        }


        /// <summary>
        /// GetFileSize.
        /// </summary>
        /// <param name="client"></param>
        public static long GetFileSize(AmazonS3Client client, string bucketName, string folderName, string s3Key)
        {

            try
            {
                ListObjectsRequest listObjectsRequest = new ListObjectsRequest();
                listObjectsRequest.BucketName = bucketName;
                listObjectsRequest.Prefix = folderName + "/";
                ListObjectsResponse listObjectsResponse = client.ListObjects(listObjectsRequest);
                foreach (S3Object entry in listObjectsResponse.S3Objects)
                {
                    if (entry.Key == s3Key)
                        return entry.Size;
                    //Console.WriteLine("Found object with key {0}, size {1}, last modification date {2}", entry.Key, entry.Size, entry.LastModified);
                }

                return 0;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return 0;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        /// <summary>
        /// GetFileSize.
        /// </summary>
        /// <param name="client"></param>
        public static string DownloadUrl(AmazonS3Client client, string bucketName, string s3Key)
        {
            string url = "";
            try
            {
                GetPreSignedUrlRequest request = new GetPreSignedUrlRequest();
                request.BucketName = bucketName;
                request.Key = s3Key.Replace("\\", "/"); 
                request.Expires = DateTime.Now.AddHours(1);
                request.Protocol = Protocol.HTTP;
                url = client.GetPreSignedURL(request);
                return url;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return url;
                }
                else
                {
                    return url;
                }

            }
            catch (Exception ex)
            {
                return url;
            }

        }

        /// <summary>
        /// ReadAdminXml.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="bucketName"></param>
        /// <param name="s3Key"></param>
        public static XDocument ReadAdminXDocument(AmazonS3Client client, string bucketName, string s3Key)
        {
            try
            {
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = bucketName,
                    Key = s3Key.Replace("\\", "/"),
                };

                GetObjectResponse response = client.GetObject(request);
                StreamReader reader = new StreamReader(response.ResponseStream);
                String content = reader.ReadToEnd();
                return XDocument.Parse(content);

            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return null;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// ReadAdminXml.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="bucketName"></param>
        /// <param name="s3Key"></param>
        public static XElement ReadAdminXElement(AmazonS3Client client, string bucketName, string s3Key)
        {
            try
            {
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = bucketName,
                    Key = s3Key.Replace("\\", "/")
                };

                GetObjectResponse response = client.GetObject(request);
                StreamReader reader = new StreamReader(response.ResponseStream);
                String content = reader.ReadToEnd();
                return XElement.Parse(content);

            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return null;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Upload the local file to the Amazon s3
        /// </summary>
        /// <param name="Amazon client"></param>
        /// <param name="BUCKET_NAME"></param> the amazon bucket name
        /// <param name="desireKey"></param> the amazon file key ( must be with the file extension. Example is foo.jpg )
        /// <param name="content"></param> file content
        /// <returns>System.Net.HttpStatusCode</returns>
        /// <remarks>This is just a wrapper for the default provider. For Amazon Error Code http://docs.aws.amazon.com/AmazonS3/latest/API/ErrorResponses.html </remarks>
        public static System.Net.HttpStatusCode uploadXdocContent(AmazonS3Client client, string bucketname, string desireKey, XDocument xDoc)
        {
            try
            {

                string content = ToOuterXml(xDoc);

                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = bucketname,
                    Key = desireKey.Replace("\\", "/"),
                    ContentBody = content,
                    CannedACL = S3CannedACL.PublicRead
                };
                PutObjectResponse response2 = client.PutObject(request);

                return System.Net.HttpStatusCode.OK;
            }

            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }
                else
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }

            }

            catch (Exception ex)
            {
                return System.Net.HttpStatusCode.InternalServerError;
            }
        }

        /// <summary>
        /// Upload the local file to the Amazon s3
        /// </summary>
        /// <param name="Amazon client"></param>
        /// <param name="BUCKET_NAME"></param> the amazon bucket name
        /// <param name="desireKey"></param> the amazon file key ( must be with the file extension. Example is foo.jpg )
        /// <param name="content"></param> file content
        /// <returns>System.Net.HttpStatusCode</returns>
        /// <remarks>This is just a wrapper for the default provider. For Amazon Error Code http://docs.aws.amazon.com/AmazonS3/latest/API/ErrorResponses.html </remarks>
        public static System.Net.HttpStatusCode uploadXElemContent(AmazonS3Client client, string bucketname, string desireKey, XElement xDoc)
        {
            try
            {

                string content = ToOuterXml(xDoc);

                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = bucketname,
                    Key = desireKey.Replace("\\", "/"),
                    ContentBody = content,
                    CannedACL = S3CannedACL.PublicRead
                };
                PutObjectResponse response2 = client.PutObject(request);

                return System.Net.HttpStatusCode.OK;
            }

            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }
                else
                {
                    return System.Net.HttpStatusCode.BadRequest;
                }

            }

            catch (Exception ex)
            {
                return System.Net.HttpStatusCode.InternalServerError;
            }
        }

        public static string ToOuterXml(XDocument xmlDoc)
        {
            return xmlDoc.ToString();
        }

        public static string ToOuterXml(XElement xmlDoc)
        {
            return xmlDoc.ToString();
        }
    }
}
