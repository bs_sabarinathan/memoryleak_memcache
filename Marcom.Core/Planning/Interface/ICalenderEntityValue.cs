using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// ICalenderEntityValue interface for table 'PM_CalenderEntityValue'.
    /// </summary>
    public interface ICalenderEntityValue
    {
        #region Public Properties

        int Id
        {
            get;
            set;
        }
        int Calenderid
        {
            get;
            set;

        }

        int Entityid
        {
            get;
            set;

        }    
        #endregion
    }
}
