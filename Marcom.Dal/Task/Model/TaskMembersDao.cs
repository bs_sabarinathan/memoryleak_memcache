﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.Task.Model
{
    public partial class TaskMembersDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _RoleID;
        protected int _UserID;
        protected int _TaskID;
        protected string _FlagColorCode;
        protected int? _ApprovalStatus;
        protected int _ApprovalRount;
        protected int _StepId;
        protected DateTime? _OverdueMailSent;
        protected bool _IsTaskalertSent;
        protected int _phaseid;
        protected int _versionid;
        protected int _steproleid;
        protected Boolean _IsWorkStart;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public TaskMembersDao() { }

        public TaskMembersDao(int pApprovalRount, int? pApprovalStatus, int pTaskID, int pUserID, int pRoleiD, string pFlagColorCode, int pStepId, int PhaseID, int VersionID, int StepRoleID, bool pIsWorkStart)
        {
            this._ApprovalRount = pApprovalRount;
            this._ApprovalStatus = pApprovalStatus;
            this._TaskID = pTaskID;
            this._UserID = pUserID;
            this._RoleID = pRoleiD;
            this._FlagColorCode = pFlagColorCode;
            this._StepId = pStepId;
            this._phaseid = PhaseID;
            this._versionid = VersionID;
            this._steproleid = StepRoleID;
            this._IsWorkStart = pIsWorkStart;
        }

        public TaskMembersDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }


        public virtual int RoleID
        {
            get { return _RoleID; }
            set { _bIsChanged |= (_RoleID != value); _RoleID = value; }
        }

        public virtual int UserID
        {
            get { return _UserID; }
            set { _bIsChanged |= (_UserID != value); _UserID = value; }
        }

        public virtual int TaskID
        {
            get { return _TaskID; }
            set { _bIsChanged |= (_TaskID != value); _TaskID = value; }
        }

        public virtual int StepId
        {
            get { return _StepId; }
            set { _bIsChanged |= (_StepId != value); _StepId = value; }
        }
        public virtual int PhaseID
        {
            get { return _phaseid; }
            set { _bIsChanged |= (_phaseid != value); _phaseid = value; }
        }

        public virtual int VersionID
        {
            get { return _versionid; }
            set { _bIsChanged |= (_versionid != value); _versionid = value; }
        }

        public virtual int ApprovalRount
        {
            get { return _ApprovalRount; }
            set { _bIsChanged |= (_ApprovalRount != value); _ApprovalRount = value; }
        }

        public virtual int? ApprovalStatus
        {
            get { return _ApprovalStatus; }
            set { _bIsChanged |= (_ApprovalStatus != value); _ApprovalStatus = value; }
        }

        public virtual string FlagColorCode
        {
            get { return _FlagColorCode; }
            set { _bIsChanged |= (_FlagColorCode != value); _FlagColorCode = value; }
        }

        public virtual DateTime? OverdueMailSent
        {
            get { return _OverdueMailSent; }
            set { _bIsChanged |= (_OverdueMailSent != value); _OverdueMailSent = value; }
        }

        public virtual bool IsTaskalertSent
        {
            get { return _IsTaskalertSent; }
            set { _bIsChanged |= (_IsTaskalertSent != value); _IsTaskalertSent = value; }
        }
        public virtual int StepRoleID
        {
            get { return _steproleid; }
            set { _bIsChanged |= (_steproleid != value); _steproleid = value; }
        }

        public virtual bool IsWorkStart
        {
            get { return _IsWorkStart; }
            set { _bIsChanged |= (_IsWorkStart != value); _IsWorkStart = value; }
        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string ID = "ID";
            public const string RoleID = "RoleID";
            public const string UserID = "UserID";
            public const string TaskID = "TaskID";
            public const string ApprovalStatus = "ApprovalStatus";
            public const string ApprovalRount = "ApprovalRount";
            public const string FlagColorCode = "FlagColorCode";
            public const string OverdueMailSent = "OverdueMailSent";
            public const string IsTaskalertSent = "IsTaskalertSent";
            public const string StepId = "StepId";
            public const string PhaseID = "PhaseID";
            public const string VersionID = "VersionID";
            public const string StepRoleID = "StepRoleID";
            public const string IsWorkStart = "IsWorkStart";

        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string ID = "ID";
            public const string RoleID = "RoleID";
            public const string UserID = "UserID";
            public const string TaskID = "TaskID";
            public const string ApprovalStatus = "ApprovalStatus";
            public const string ApprovalRount = "ApprovalRount";
            public const string FlagColorCode = "FlagColorCode";
            public const string OverdueMailSent = "OverdueMailSent";
            public const string IsTaskalertSent = "IsTaskalertSent";
            public const string StepId = "StepId";
            public const string PhaseID = "PhaseID";
            public const string VersionID = "VersionID";
            public const string StepRoleID = "StepRoleID";
            public const string IsWorkStart = "IsWorkStart";
        }

        #endregion


    }
}
