using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

	/// <summary>
    /// EntityTypeFeatureDao object for table 'MM_MultiSelect'.
	/// </summary>
	
	public partial class MultiSelectDao : BaseDao, ICloneable 
	{
		#region Member Variables

        protected int _id;
        protected int _entityid;
        protected int _attributeid;
        protected int _optionid;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion

        //#region Extensibility Method Definitions

        //public override bool Equals(object obj)
        //{
        //    MultiSelectDao toCompare = obj as MultiSelectDao;
        //    if (toCompare == null)
        //    {
        //        return false;
        //    }

        //    if (!Object.Equals(this.Attributeid, toCompare.Attributeid))
        //        return false;
        //    if (!Object.Equals(this.Entityid, toCompare.Entityid))
        //        return false;
        //    if (!Object.Equals(this.Optionid, toCompare.Optionid))
        //        return false;         


        //    return true;
        //}

        //public override int GetHashCode()
        //{
        //    int hashCode = 13;
        //    hashCode = (hashCode * 7) + Entityid.GetHashCode();
        //    hashCode = (hashCode * 7) + Attributeid.GetHashCode();
        //    hashCode = (hashCode * 7) + Optionid.GetHashCode();
        //    return hashCode;
        //}

        //#endregion
		
		#region Constructors
		public MultiSelectDao() {}

        public MultiSelectDao(int pId, int pEntityid, int pAttributeid, int pOptionid)
		{
            this._id = pId;
            this._entityid = pEntityid;
            this._optionid = pOptionid;
            this._attributeid = pAttributeid;
		}
		
      
		#endregion
		
		#region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int Entityid
		{
            get { return _entityid; }
            set { _bIsChanged |= (_entityid != value); _entityid = value; }
			
		}
        public virtual int Attributeid
        {
            get { return _attributeid; }
            set { _bIsChanged |= (_attributeid != value); _attributeid = value; }

        }
        public virtual int Optionid
		{
            get { return _optionid; }
            set { _bIsChanged |= (_optionid != value); _optionid = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string Id = "Id";
            public const string MultiSelectDao = "MultiSelectDao";
            public const string Attributeid = "Attributeid";
            public const string Entityid = "Entityid";
            public const string Optionid = "Optionid";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string Id = "Id";
			public const string MultiSelectDao = "MultiSelectDao";
            public const string Attributeid = "Attributeid";
            public const string Entityid = "Entityid";
            public const string Optionid = "Optionid";			
		}

		#endregion
		
		
	}
	
}
