﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Cms.Interface
{
    public interface ICmsSnippetTemplate
    {
        int ID
        {
            get;
            set;
        }

        string SnippetContent
        {
            get;
            set;
        }

        bool Active
        {
            get;
            set;
        }
        string ThumbnailGuid
        {
            get;
            set;
        }
        string CreatedOn
        {
            get;
            set;
        }
        string DefaultFirstContent
        {
            get;
            set;
        }
        string DefaultLastContent
        {
            get;
            set;
        }
    }
}
