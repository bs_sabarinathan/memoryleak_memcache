﻿(function (ng, app) {
    "use strict";

    function muiDashBoardWidgetNewsFeedWidgetCtrl($scope, $timeout, $http, $compile, $resource, $cookies, NewsfeedwidgetService, $location, $window, $translate, $modal) {
        $scope.userimg = parseInt($cookies['UserId'])
        $scope.EntityFeedsdivID = "EntityFeedsdiv_" + $scope.WizardID;
        $scope.FeedContentdivID = "FeedContentdiv_" + $scope.WizardID
        $scope.WizardFeedsgroupid = '-1';
        $timeout(function () { LoadNewsFeed(); }, 100);

        function LoadNewsFeed() {
            try {
                $scope.dyn_Cont = '';
                $scope.FeedHtml = '';
                $scope.entityNewsFeeds = {};
                var PagenoforScroll = 0;
                $scope.EntityFeedsResultset = [];
                NewsfeedwidgetService.GetEnityFeeds(0, 0, $scope.WizardFeedsgroupid).then(function (getEntityNewsFeedResult) {
                    $scope.EntityFeedsResultset = getEntityNewsFeedResult.Response;
                    var feeddivHtml = '';
                    PagenoforScroll += 1;
                    $('#' + $scope.EntityFeedsdivID + '').html('');
                    $('#' + $scope.FeedContentdivID + '').html('');
                    $('#' + $scope.EntityFeedsdivID + '').parents(".pending").removeClass('pending').addClass('done').removeAttr("style");
                    if (getEntityNewsFeedResult.Response != null) {
                        for (var i = 0; i < getEntityNewsFeedResult.Response.length; i++) {
                            var feedcomCount = getEntityNewsFeedResult.Response[i].FeedComment != null ? getEntityNewsFeedResult.Response[i].FeedComment.length : 0;
                            feeddivHtml = '';
                            if (getEntityNewsFeedResult.Response[i].Actor == parseInt($cookies['UserId'])) {
                                $scope.userimgsrc = $scope.NewUserImgSrc;
                            } else {
                                $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].Actor;
                            }
                            feeddivHtml = feeddivHtml + '<li data-parent="NewsParent"  data-ID=' + getEntityNewsFeedResult.Response[i].FeedId + '>';
                            feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                            feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                            feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].UserEmail + '>' + getEntityNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedText + '</p></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                            feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="nfCommentHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResult.Response[i].FeedId + '" >Comment</a></span>';
                            if (feedcomCount > 1) {
                                feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="nfCommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                            } else {
                                feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="nfCommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                            }
                            feeddivHtml = feeddivHtml + '<ul class="subComment">';
                            if (getEntityNewsFeedResult.Response[i].FeedComment != '' && getEntityNewsFeedResult.Response[i].FeedComment != null) {
                                var j = 0;
                                if (getEntityNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                    $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                } else {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                }
                                for (var k = 0; k < getEntityNewsFeedResult.Response[i].FeedComment.length; k++) {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    if (k == 0) {
                                        feeddivHtml = feeddivHtml + '<li';
                                    } else {
                                        feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                    }
                                    feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                    feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                    feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                    feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                    feeddivHtml = feeddivHtml + '</div></div></li>';
                                }
                            }
                            feeddivHtml = feeddivHtml + '</ul></li>';
                            $('#' + $scope.EntityFeedsdivID + '').append(feeddivHtml);
                        }
                    }
                    $('#' + $scope.EntityFeedsdivID + '').parents('.box-content').find('.InProgress').remove();
                    $timeout(function () { TimerForLatestFeed(); }, 30000);
                });
            } catch (e) { }
            try {
                $('.scrollable').scroll(function () {
                    if ($('.scrollable').children().find('activity-item[wizardtypeid=1]').length >= 1) {
                        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                            PagenoforScroll += 1;
                            NewsfeedwidgetService.GetEnityFeeds(0, PagenoforScroll, $scope.WizardFeedsgroupid).then(function (getEntityNewsFeedResultForScroll) {
                                var feeddivHtml = '';
                                for (var i = 0; i < getEntityNewsFeedResultForScroll.Response.length; i++) {
                                    var feedcomCount = getEntityNewsFeedResultForScroll.Response[i].FeedComment != null ? getEntityNewsFeedResultForScroll.Response[i].FeedComment.length : 0;
                                    feeddivHtml = '';
                                    if (getEntityNewsFeedResultForScroll.Response[i].Actor == parseInt($cookies['UserId'])) {
                                        $scope.userimgsrc = $scope.NewUserImgSrc;
                                    } else {
                                        $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].Actor;
                                    }
                                    feeddivHtml = feeddivHtml + '<li data-parent="NewsParent"  data-ID=' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '>';
                                    feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                    feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                                    feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResultForScroll.Response[i].UserEmail + '>' + getEntityNewsFeedResultForScroll.Response[i].UserName + '</a></h5></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResultForScroll.Response[i].FeedText + '</p></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResultForScroll.Response[i].FeedHappendTime + '</span>';
                                    feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="nfCommentHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" >Comment</a></span>';
                                    if (feedcomCount > 1) {
                                        feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="nfCommentshowHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + ' more comment(s)</a></span></div></div></div>';
                                    } else {
                                        feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="nfCommentshowHTML"  data-commnetid="' + getEntityNewsFeedResultForScroll.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResultForScroll.Response[i].FeedId + '" ></a></span></div></div></div>';
                                    }
                                    feeddivHtml = feeddivHtml + '<ul class="subComment">';
                                    if (getEntityNewsFeedResultForScroll.Response[i].FeedComment != '' && getEntityNewsFeedResultForScroll.Response[i].FeedComment != null) {
                                        var j = 0;
                                        if (getEntityNewsFeedResultForScroll.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                            $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                        } else {
                                            $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                        }
                                        for (var k = 0; k < getEntityNewsFeedResultForScroll.Response[i].FeedComment.length; k++) {
                                            $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                            if (k == 0) {
                                                feeddivHtml = feeddivHtml + '<li';
                                            } else {
                                                feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                            }
                                            feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Id + '"">';
                                            feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                            feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                            feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                            feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                            feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].Comment + '</p></div>';
                                            feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResultForScroll.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                            feeddivHtml = feeddivHtml + '</div></div></li>';
                                        }
                                    }
                                    feeddivHtml = feeddivHtml + '</ul></li>';
                                    $('#' + $scope.EntityFeedsdivID + '').append(feeddivHtml);
                                }
                            });
                        }
                    }
                });
            } catch (e) { }
            try {
                var TimerForLatestFeed = function () {
                    $scope.FeedHtml = '';
                    $scope.entityNewsFeeds = {};
                    NewsfeedwidgetService.GetLastEntityFeeds(0, $scope.WizardFeedsgroupid).then(function (getEntityNewsFeedResult) {
                        var feeddivHtml = '';
                        for (var i = 0; i < getEntityNewsFeedResult.Response.length; i++) {
                            var feedcomCount = getEntityNewsFeedResult.Response[i].FeedComment != null ? getEntityNewsFeedResult.Response[i].FeedComment.length : 0;
                            feeddivHtml = '';
                            if (getEntityNewsFeedResult.Response[i].Actor == parseInt($cookies['UserId'])) {
                                $scope.userimgsrc = $scope.NewUserImgSrc;
                            } else {
                                $scope.userimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].Actor;
                            }
                            feeddivHtml = feeddivHtml + '<li data-parent="NewsParent" data-ID=' + getEntityNewsFeedResult.Response[i].FeedId + '>';
                            feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                            feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.userimgsrc + ' alt="Avatar"></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmnt" data-parent="parentforcomment">';
                            feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].UserEmail + '>' + getEntityNewsFeedResult.Response[i].UserName + '</a></h5></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedText + '</p></div>';
                            feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedHappendTime + '</span>';
                            feeddivHtml = feeddivHtml + '<span class="cmntActions" ><a  data-DynHTML="nfCommentHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedId + '" id="OverviewComment_' + getEntityNewsFeedResult.Response[i].FeedId + '" >Comment</a></span>';
                            if (feedcomCount > 1) {
                                feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="nfCommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" > View ' + (feedcomCount - 1) + 'more comment(s)</a></span></div></div></div>';
                            } else {
                                feeddivHtml = feeddivHtml + '<span class="cmntInfo"><a data-DynHTML="nfCommentshowHTML"  data-commnetid="' + getEntityNewsFeedResult.Response[i].FeedComment + '"   id="Overviewfeed_' + getEntityNewsFeedResult.Response[i].FeedId + '" ></a></span></div></div></div>';
                            }
                            feeddivHtml = feeddivHtml + '<ul class="subComment"';
                            if (getEntityNewsFeedResult.Response[i].FeedComment != '' && getEntityNewsFeedResult.Response[i].FeedComment != null) {
                                var j = 0;
                                if (getEntityNewsFeedResult.Response[i].FeedComment[j].Actor == parseInt($cookies['UserId'])) {
                                    $scope.commentuserimgsrc = $scope.NewUserImgSrc;
                                } else {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[j].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                }
                                for (var k = 0; k < getEntityNewsFeedResult.Response[i].FeedComment.length; k++) {
                                    $scope.commentuserimgsrc = "Handlers/UserImage.ashx?id=" + getEntityNewsFeedResult.Response[i].FeedComment[k].Actor + "&time=" + $scope.DefaultImageSettings.ImageSpan;
                                    if (k == 0) {
                                        feeddivHtml = feeddivHtml + '<li';
                                    } else {
                                        feeddivHtml = feeddivHtml + '<li style = "display:none;"';
                                    }
                                    feeddivHtml = feeddivHtml + ' id="feedcomment_' + getEntityNewsFeedResult.Response[i].FeedComment[k].Id + '"">';
                                    feeddivHtml = feeddivHtml + '<div class="newsFeed">';
                                    feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=' + $scope.commentuserimgsrc + ' alt="Avatar"></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmnt">';
                                    feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5><a href=mailto:' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserEmail + '>' + getEntityNewsFeedResult.Response[i].FeedComment[k].UserName + '</a></h5></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + getEntityNewsFeedResult.Response[i].FeedComment[k].Comment + '</p></div>';
                                    feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">' + getEntityNewsFeedResult.Response[i].FeedComment[k].CommentedOn + '</span></div>';
                                    feeddivHtml = feeddivHtml + '</div></div></li>';
                                }
                            }
                            feeddivHtml = feeddivHtml + '</ul></li>';
                            $('#' + $scope.EntityFeedsdivID + '').prepend(feeddivHtml);
                        }
                    });
                    PagenoforScroll = 0;
                    if ($(location).attr('href').contains('overview')) {
                        $timeout(function () { TimerForLatestFeed(); }, 30000);
                    } else {
                        $timeout.cancel(TimerForLatestFeed);
                    }
                }
            } catch (e) { }
        }
        $('body').on('click', 'a[data-DynHTML="nfCommentHTML"]', function (event) {
            var commentuniqueid = this.id;
            event.stopImmediatePropagation();
            var imgpath = "Handlers/UserImage.ashx?id=" + parseInt($cookies['UserId']) + "&time=" + $scope.DefaultImageSettings.ImageSpan;
            $(this).hide();
            var feeddivHtml = '';
            feeddivHtml = feeddivHtml + '<li class="writeNewComment">';
            feeddivHtml = feeddivHtml + '<div class="newComment"><div class="userAvatar"><img data-role="user-avatar" src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\'></div>';
            feeddivHtml = feeddivHtml + '<div class="textarea-wrapper" data-role="textarea"><div contenteditable="true" tabindex="0" class="textarea" id="feedcomment_' + commentuniqueid + '"></div></div>';
            feeddivHtml = feeddivHtml + '<button type="submit" class="btn btn-primary" data-dynCommentBtn="nfButtonHTML" >' + $translate.instant('LanguageContents.Res_1204.Caption') + '</button></div></li>';
            var currentobj = $(this).parents('li[data-parent="NewsParent"]').find('ul');
            if ($(this).parents('li[data-parent="NewsParent"]').find('ul').children().length > 0) $(this).parents('li[data-parent="NewsParent"]').find('ul li:first').before(feeddivHtml);
            else $(this).parents('li[data-parent="NewsParent"]').find('ul').html(feeddivHtml);
            $timeout(function () {
                $('#feedcomment_' + commentuniqueid).html('').focus();
            }, 10);
        });
        $('body').on('click', 'a[data-DynHTML="nfCommentshowHTML"]', function (event) {
            if ($(this).parents()[5].id == $scope.EntityFeedsdivID) {
                var feedid1 = event.target.attributes["id"].nodeValue;
                var feedid = feedid1.substring(13);
                var feedfilter = $.grep($scope.EntityFeedsResultset, function (e) {
                    return e.FeedId == parseInt(feedid);
                });
                $("#Overviewfeed_" + feedid).next('div').hide();
                for (var i = 0; i < feedfilter[0].FeedComment.length; i++) {
                    if ($('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '')[0].style.display != "none") {
                        $(this)[0].innerHTML = "View " + (feedfilter[0].FeedComment.length - 1) + " more comment(s)";
                        if (i != 0) $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideUp(1000, function () { });
                        else $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                    } else {
                        $('#feedcomment_' + feedfilter[0].FeedComment[i].Id + '').slideDown(1000, function () { });
                        $(this)[0].innerHTML = "Hide comment(s)";
                    }
                }
            }
        });
        $('body').on('click', 'a[data-id="feedpath"]', function (event) {
            event.stopImmediatePropagation();
            var entityID = $(this).attr('data-entityid');
            var IsObjective = false;
            IsObjective = $(event.target).attr('data-IsObjective');
            NewsfeedwidgetService.IsActiveEntity(entityID).then(function (result) {
                if (result.Response == true) {
                    NewsfeedwidgetService.getredirectPath(entityID).then(function (path) {
                        if (path.Response != null) if (path.Response.indexOf('cms') > -1) {
                            NewsfeedwidgetService.GetCmsEntitiesByID(entityID).then(function (resNavID) {
                                if (resNavID.Response != null) {
                                    $scope.GlobalNavigationID.NavigationID = resNavID.Response[0].NavID.toString();
                                    $cookies["GlobalNavigationID"] = resNavID.Response[0].NavID.toString();
                                    $location.path('/mui/cms/detail/list/' + entityID + '/entitycontent');
                                }
                            });
                        } else {
                            if (IsObjective == "True")
                                $location.path('/mui/objective/detail/section/' + entityID + '/overview');
                            else
                                $location.path(path.Response + entityID + '/overview');
                        }
                    });
                }
                else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1866.Caption'));
                }
            });
        });
        $('body').on('click', 'a[data-id="costcenterlink"]', function (event) {
            event.stopImmediatePropagation();
            var EntityID = $(this).attr('data-costcentreid');
            NewsfeedwidgetService.CheckUserPermissionForEntity(EntityID).then(function (CheckUserPermissionForEntity) {
                if (CheckUserPermissionForEntity.Response == true && $scope.Moduleid != 6)
                    $location.path('/mui/planningtool/costcentre/detail/section/' + EntityID + '/overview');
                if ($('.FundingRequestPopup').length != 0) {
                    $('.FundingRequestPopup').hide()
                }
            });
        });
        $('body').on('click', 'a[data-id="objectivelink"]', function (event) {
            event.stopImmediatePropagation();
            var ObjectiveID = $(this).attr('data-objectiveid');
            var EntityID = $(this).attr('data-entityid');
            NewsfeedwidgetService.CheckUserPermissionForEntity(EntityID).then(function (CheckUserPermissionForEntity) {
                if (CheckUserPermissionForEntity.Response == true && $scope.Moduleid != 6) {
                    $location.path('/mui/objective/detail/section/' + ObjectiveID + '/overview');
                }
                if ($('.FundingRequestPopup').length != 0) {
                    $('.FundingRequestPopup').hide()
                }
            });
        });
        $('body').on('click', 'a[data-id="taskpopupopen"]', function (event) {
            event.stopImmediatePropagation();
            var entityid = $(this).attr('data-entityid');
            var taskid = $(this).attr('data-taskid');
            var typeid = $(this).attr('data-typeid');
            NewsfeedwidgetService.IsActiveEntity(taskid).then(function (result) {
                if (result.Response == true) {
                    //$scope.$emit("pingMUITaskEdit", {
                    //	TaskId: taskid,
                    //	EntityId: entityid
                    //});
                    $scope.OpenTopNotificationTaskPopUp(taskid, entityid, typeid);
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });
        $('body').on('click', 'a[data-id="openfundrequestpopup"]', function (event) {
            var EntityID = $(this).attr('data-parentid');
            var fundreqid = $(this).attr('data-entityid');
            var typeid = $(this).attr('data-typeid');
            NewsfeedwidgetService.IsActiveEntity(fundreqid).then(function (result) {
                if (result.Response == true) {
                    $("#feedFundingRequestModal").modal("show");
                    $scope.$emit('onNewsfeedCostCentreFundingRequestsAction', fundreqid);
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });
        $('body').on('click', 'a[data-id="Assetpath"]', function (event) {
            event.stopImmediatePropagation();
            var entityid = $(this).attr('data-entityid');
            var AssetID = $(this).attr('data-Assetid');
            NewsfeedwidgetService.IsAvailableAsset($(this).attr('data-Assetid')).then(function (result) {
                if (result.Response == true) {
                    var modalInstance = $modal.open({
                        templateUrl: 'views/mui/DAM/assetedit.html',
                        controller: "mui.DAM.asseteditCtrl",
                        resolve: {
                            params: function () {
                                return {
                                    AssetID: AssetID,
                                    IsLock: false,
                                    isNotify: true,
                                    viewtype: 'ViewType'
                                };
                            }
                        },
                        scope: $scope,
                        windowClass: 'iv-Popup',
                        backdrop: "static"
                    });
                    modalInstance.result.then(function (selectedItem) {
                        $scope.selected = selectedItem;
                    }, function () { });
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_1955.Caption'));
                }
            });
        });
        $('body').on('click', 'button[data-dynCommentBtn="nfButtonHTML"]', function (event) {
            event.stopImmediatePropagation();
            if ($(this).prev().eq(0).text().toString().trim().length > 0) {
                var addfeedcomment = {};
                var addfeedcomment;
                var FeedidforComment = $(this).parents("li:eq(1)").attr('data-id');
                var myDate = new Date();
                addfeedcomment.FeedID = $(this).parents("li:eq(1)").attr('data-id');
                addfeedcomment.Actor = parseInt($cookies['UserId']);
                addfeedcomment.Comment = $(this).prev().eq(0).text();
                var d = new Date();
                var month = d.getMonth() + 1;
                var day = d.getDate();
                var hrs = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                var output = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day + ' ' + hrs;
                var _this = $(this);
                NewsfeedwidgetService.InsertFeedComment(addfeedcomment).then(function (saveusercomment) {
                    var feeddivHtml = '';
                    feeddivHtml = feeddivHtml + '<li>';
                    feeddivHtml = feeddivHtml + '<div class=\"newsFeed\">';
                    feeddivHtml = feeddivHtml + '<div class="userAvatar"><img src=\'Handlers/UserImage.ashx?id=' + parseInt($cookies['UserId']) + '&time=' + $scope.DefaultImageSettings.ImageSpan + '\' alt="Avatar"></div>';
                    feeddivHtml = feeddivHtml + '<div class="cmnt">';
                    feeddivHtml = feeddivHtml + '<div class="cmntHeader"><h5> <a href="undefined">' + $cookies['Username'] + '</a></h5></div>';
                    feeddivHtml = feeddivHtml + '<div class="cmntContent"><p>' + saveusercomment.Response + '</p></div>';
                    feeddivHtml = feeddivHtml + '<div class="cmntFooter"><span class="cmntTime">Few seconds ago</span></div>';
                    feeddivHtml = feeddivHtml + '</div></div></div></li>';
                    $('#' + $scope.EntityFeedsdivID + '').find('li[data-id=' + FeedidforComment + ']').first().find('li:first').before(feeddivHtml)
                    $(".writeNewComment").remove();;
                    $('#OverviewComment_' + addfeedcomment.FeedID).show();
                });
            }
        });

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.DashBoardWidget.NewsFeedWidgetCtrl']"));
        });
    }
    app.controller("mui.DashBoardWidget.NewsFeedWidgetCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', 'NewsfeedwidgetService', '$location', '$window', '$translate', '$modal', muiDashBoardWidgetNewsFeedWidgetCtrl]);
    function NewsfeedwidgetService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetEnityFeeds: GetEnityFeeds,
            GetLastEntityFeeds: GetLastEntityFeeds,
            getredirectPath: getredirectPath,
            InsertFeedComment: InsertFeedComment,
            IsAvailableAsset: IsAvailableAsset,
            IsActiveEntity: IsActiveEntity,
            GetCmsEntitiesByID: GetCmsEntitiesByID,
            CheckUserPermissionForEntity: CheckUserPermissionForEntity           
        });
        function GetEnityFeeds(EntityID, pageNo, Feedsgroupid) { var request = $http({ method: "get", url: "api/common/GetEnityFeeds/" + EntityID + "/" + pageNo + "/" + Feedsgroupid, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function GetLastEntityFeeds(EntityID, Feedsgroupid) { var request = $http({ method: "get", ignoreLoadingBar: true, url: "api/common/GetLastEntityFeeds/" + EntityID + "/" + Feedsgroupid, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function getredirectPath(entityID) { var request = $http({ method: "get", url: "api/Common/getredirectPath/" + entityID, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function InsertFeedComment(addfeedcomment) { var request = $http({ method: "post", url: "api/common/InsertFeedComment/", params: { action: "add", }, data: addfeedcomment }); return (request.then(handleSuccess, handleError)); }
        function IsAvailableAsset(AssetID) { var request = $http({ method: "get", url: "api/common/IsAvailableAsset/" + AssetID, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function IsActiveEntity(EntityID) { var request = $http({ method: "get", url: "api/common/IsActiveEntity/" + EntityID, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function GetCmsEntitiesByID(CmsEntityID) { var request = $http({ method: "get", url: "api/cms/GetCmsEntitiesByID/" + CmsEntityID, params: { action: "get" } }); return (request.then(handleSuccess, handleError)); }
        function CheckUserPermissionForEntity(EntitiyID) { var request = $http({ method: "get", url: "api/common/CheckUserPermissionForEntity/" + EntitiyID, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("NewsfeedwidgetService", ['$http', '$q', NewsfeedwidgetService]);

})(angular, app);