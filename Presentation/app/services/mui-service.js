﻿(function (ng, app) {
    "use strict";

    function MuiService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            SetIsCurrentWorkingXml: SetIsCurrentWorkingXml,
            GetDecimalSettingsValue: GetDecimalSettingsValue,
            GetAdditionalSettings: GetAdditionalSettings,
            GetNotification: GetNotification,
            GetTaskLIveUpdateRecords: GetTaskLIveUpdateRecords,
            updateBroadcastMessagesbyuser: updateBroadcastMessagesbyuser,
            IsActiveEntity: IsActiveEntity,
            CheckUserPermissionForEntity: CheckUserPermissionForEntity,
            UpdatetopIsviewedStatusNotification: UpdatetopIsviewedStatusNotification,
            UpdateIsviewedStatusNotification: UpdateIsviewedStatusNotification,
            GetTopNavigation: GetTopNavigation,
            GetSearchtype: GetSearchtype,
            GetassignedAcess: GetassignedAcess,
            GetThemeValues: GetThemeValues,
            GetThemeData: GetThemeData,
            GetHolidaysDetails: GetHolidaysDetails,
            GetNonBusinessDays: GetNonBusinessDays,
            GetAttachmentEditFeature: GetAttachmentEditFeature,
            QuickSearch: QuickSearch,
            DuplicateEntities: DuplicateEntities,
            GetDAMViewSettings: GetDAMViewSettings,
            GetGlobalRoleUserByID: GetGlobalRoleUserByID,
            GetSuperglobalacl: GetSuperglobalacl,
            GetModuleID: GetModuleID,
            GetCmsEntitiesByID: GetCmsEntitiesByID,
            GetAdminSettings: GetAdminSettings,
        });

        function SetIsCurrentWorkingXml() {
            var request = $http({
                method: "post",
                url: "api/Metadata/SetIsCurrentWorkingXml/",
                params: {
                    action: "add"
                },
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDecimalSettingsValue() {
            var request = $http({
                method: "get",
                url: "api/common/GetDecimalSettingsValue/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAdditionalSettings() {
            var request = $http({
                method: "get",
                url: "api/common/GetAdditionalSettings/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetNotification(flag) {
            var request = $http({
                method: "get",
                ignoreLoadingBar: true,
                url: "api/common/GetNotification/" + flag,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTaskLIveUpdateRecords() {
            var request = $http({
                method: "get",
                ignoreLoadingBar: true,
                url: "api/common/GetTaskLIveUpdateRecords/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function updateBroadcastMessagesbyuser() {
            var request = $http({
                method: "post",
                url: "api/common/updateBroadcastMessagesbyuser/",
                params: {
                    action: "add",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function IsActiveEntity(EntityID) {
            var request = $http({
                method: "get",
                url: "api/common/IsActiveEntity/" + EntityID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function CheckUserPermissionForEntity(EntitiyID) {
            var request = $http({
                method: "get",
                url: "api/common/CheckUserPermissionForEntity/" + EntitiyID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdatetopIsviewedStatusNotification(jsonparameter) {
            var request = $http({
                method: "post",
                ignoreLoadingBar: true,
                url: "api/common/UpdatetopIsviewedStatusNotification",
                params: {
                    action: "add"
                },
                data: jsonparameter
            });
            return (request.then(handleSuccess, handleError));
        }

        function UpdateIsviewedStatusNotification(updatestae) {
            var request = $http({
                method: "post",
                url: "api/common/UpdateIsviewedStatusNotification/",
                params: {
                    action: "add"
                },
                data: {
                    UserID: updatestae.UserID,
                    FirstFiveNotifications: updatestae.FirstFiveNotifications,
                    Flag: updatestae.Flag
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetTopNavigation() {
            var request = $http({
                method: "get",
                url: "api/common/GetTopNavigation/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetSearchtype(userid) {
            var request = $http({
                method: "get",
                url: "api/common/GetSearchtype/" + userid,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetassignedAcess(userid) {
            var request = $http({
                method: "get",
                url: "api/common/GetassignedAcess/" + userid,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetThemeValues() {
            var request = $http({
                method: "get",
                url: "api/common/GetThemeValues/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetThemeData(Theme) {
            var request = $http({
                method: "get",
                url: "api/common/GetThemeData/" + Theme,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetHolidaysDetails() {
            var request = $http({
                method: "get",
                url: "api/Common/GetHolidaysDetails/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetNonBusinessDays() {
            var request = $http({
                method: "get",
                url: "api/Common/GetNonBusinessDays/",
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetAttachmentEditFeature() {
            var request = $http({
                method: "get",
                url: "api/Planning/GetAttachmentEditFeature/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function QuickSearch(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/QuickSearch/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function QuickSearch1(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/QuickSearch1/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function DuplicateEntities(formobj) {
            var request = $http({
                method: "post",
                url: "api/Planning/DuplicateEntities/",
                params: {
                    action: "add"
                },
                data: formobj
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetDAMViewSettings() {
            var request = $http({
                method: "get",
                url: "api/dam/GetDAMViewSettings/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetGlobalRoleUserByID(ID) {
            var request = $http({
                method: "get",
                url: "api/access/GetGlobalRoleUserByID/" + ID,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function GetSuperglobalacl() {
            var request = $http({
                method: "get",
                url: "api/access/GetSuperglobalacl/",
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function GetModuleID(EntityId) {
            var request = $http({
                method: "get",
                url: "api/common/GetModuleID/" + EntityId,
                params: {
                    action: "get"
                }
            });
            return (request.then(handleSuccess, handleError));
        }
        function GetCmsEntitiesByID(CmsEntityID) {
            var request = $http({
                method: "get",
                url: "api/cms/GetCmsEntitiesByID/" + CmsEntityID,
                params: { action: "get" }
            });
            return (request.then(handleSuccess, handleError));
        }
        function GetAdminSettings(LogoSettings, typeid) {
            var request = $http({
                method: "get",
                url: "api/common/GetAdminSettings/" + LogoSettings + "/" + typeid,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            response.data.StatusCode = response.status;
            return (response.data);
        }
    }
    app.service("MuiService", ['$http', '$q', MuiService]);
})(angular, app);