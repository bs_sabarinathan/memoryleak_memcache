﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Configuration;

namespace Presentation.Utility
{
    public class TenantResolver
    {
        public int tenantID { get; set; }
        public string tenantName { get; set; }
        public string tenanturl { get; set; }
        public string tenantconnection { get; set; }
        public string tenantfilepath { get; set; }

        public TenantResolver()
        {
            //do nothing
        }

        public TenantResolver(string hostname)
        {
            string TenantInfoPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() ;
            TenantInfoPath = TenantInfoPath + "Tenants\\TenantsInfo.xml";
            XmlDocument xdcDocument = new XmlDocument();
            xdcDocument.Load(TenantInfoPath);
            XmlNode xn = xdcDocument.SelectSingleNode("/Tenants/Tenant[@TenantHost='" + hostname + "']");
            tenantID = Convert.ToInt32(xn.Attributes["ID"].Value);
            tenantName = xn["TenantName"].InnerText;
            tenanturl = hostname;
            tenantconnection = xn["conn"].InnerText;
            tenantfilepath = xn["FilePath"].InnerText;
        }
    }
}