﻿/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Core.DAM.Interface;

namespace BrandSystems.Marcom.Core.DAM
{

    /// <summary>
    /// Module object for table 'DM_Folder'.
    /// </summary>

    public class Folder : IFolder
    {
        #region Member Variables

        protected int _id;
        protected int _nodeid;
        protected int _parentpodeid;
        protected int _level;
        protected string _key;
        protected int _entityid;
        protected string _caption;
        protected string _description;
        protected int _sortOrder;
        public string _colorCode;

        #endregion

        #region Constructors
        public Folder() { }

        public Folder(int pId, int pNodeid, int pParentnodeid, int pLevel, string pKEY, int pEntityid, string pCaption, int pSortOrder,string pDescription,string pColorcode)
        {
            this._id = pId;
            this._nodeid = pNodeid;
            this._level = pLevel;
            this._key = pKEY;
            this._parentpodeid = pParentnodeid;
            this._entityid = pEntityid;
            this._caption = pCaption;
            this._description = pDescription;
            this._sortOrder = pSortOrder;
            this._colorCode = pColorcode;
        }

        public Folder(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int NodeID
        {
            get { return _nodeid; }
            set { _nodeid = value; }

        }
        public virtual int ParentNodeID
        {
            get { return _parentpodeid; }
            set { _parentpodeid = value; }

        }
        public virtual int Level
        {
            get { return _level; }
            set { _level = value; }

        }

        public virtual string KEY
        {
            get { return _key; }
            set { _key = value; }

        }

        public virtual int EntityID
        {
            get { return _entityid; }
            set { _entityid = value; }

        }

        public virtual string Caption
        {
            get { return _caption; }
            set { _caption = value; }

        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }

        }

        public virtual int SortOrder
        {
            get { return _sortOrder; }
            set { _sortOrder = value; }

        }

        public List<Folder> Children { get; set; }

        public virtual string Colorcode
        {
            get { return _colorCode; }
            set { _colorCode = value; }

        }


        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            Folder castObj = null;
            try
            {
                castObj = (Folder)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
