﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// PurchaseOrderDao object for table 'PM_PurchaseOrder'.
    /// </summary>

    public partial class ApprovedAllocatedDetailsDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected int _cosrcentreid;
        protected DateTime _date;
        protected string _description;
        protected decimal _Amount;
        protected int _currencytype;
        protected int _userid;

        protected bool _bIsDeleted;
        protected bool _bIsChanged;

        #endregion

        #region Constructors
        public ApprovedAllocatedDetailsDao() { }

        public ApprovedAllocatedDetailsDao(int pEntityid, int pCosrcentreId, DateTime pDate, string pDescription, int pCurrencyType, int pUserID, decimal pAmount)
        {
            this._entityid = pEntityid;
            this._cosrcentreid = pCosrcentreId;
            this._date = pDate;
            this._description = pDescription;
            this._currencytype = pCurrencyType;
            this._userid = pUserID;
            this._Amount = pAmount;

        }

        public ApprovedAllocatedDetailsDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int EntityID
        {
            get { return _entityid; }
            set { _bIsChanged |= (_entityid != value); _entityid = value; }

        }

        public virtual int CostCentreID
        {
            get { return _cosrcentreid; }
            set { _bIsChanged |= (_cosrcentreid != value); _cosrcentreid = value; }

        }

        public virtual DateTime Date
        {
            get { return _date; }
            set { _bIsChanged |= (_date != value); _date = value; }

        }

        public virtual decimal Amount
        {
            get { return _Amount; }
            set { _bIsChanged |= (_Amount != value); _Amount = value; }

        }

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 250 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }


        public virtual int CurrencyType
        {
            get { return _currencytype; }
            set { _bIsChanged |= (_currencytype != value); _currencytype = value; }

        }


        public virtual int UserID
        {
            get { return _userid; }
            set { _bIsChanged |= (_userid != value); _userid = value; }

        }


        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string ApprovedAllocatedDetailsDao = "ApprovedAllocatedDetailsDao";
            public const string Id = "Id";
            public const string EntityID = "EntityID";
            public const string CostCentreID = "CostCentreID";
            public const string Amount = "Amount";
            public const string CurrencyType = "CurrencyType";
            public const string Description = "Description";
            public const string Date = "Date";
            public const string UserID = "UserID";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string PlanningDao = "PlanningDao";
            public const string Id = "Id";
            public const string EntityID = "EntityID";
            public const string CostCentreID = "CostCentreID";
            public const string Amount = "Amount";
            public const string CurrencyType = "CurrencyType";
            public const string Description = "Description";
            public const string Date = "Date";
            public const string UserID = "UserID";
        }

        #endregion


    }

}
