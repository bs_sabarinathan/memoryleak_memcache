﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;
namespace BrandSystems.Marcom.Core.Planning
{
    class FundingRequestHolder:IFundingRequestHolder
    {
        public int CostCenterID { get; set; }
        public string DueDate { get; set; }
        public string Comment { get; set; }
        public string Amount { get; set; }
    }
}
