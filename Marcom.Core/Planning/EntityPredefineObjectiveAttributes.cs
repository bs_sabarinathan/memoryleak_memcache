﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{
    [DataContract(Namespace = "BrandSystems")]
   public class EntityPredefineObjectiveAttributes : IEntityPredefineObjectiveAttributes
    {
        [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public int AttributeId { get; set; }
        [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public string AttributeName { get; set; }
        [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public int AttributeTypeId { get; set; }
        [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public string AttributeValues { get; set; }
        [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public string StartDate { get; set; }
        [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public string EndDate { get; set; }
        [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public int ObjectiveId { get; set; }
         [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public int ObjectiveTypeId { get; set; }
         [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public string ObjectiveName { get; set; }
         [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public string ObjectiveOwner { get; set; }
         [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public string ObjectiveDescription { get; set; }
         [DataMember(IsRequired = false, Order = 1), XmlAttribute()]
        public bool ObjectiveMandatoryStatus { get; set; }
         public int ObjectiveEntitytypeID { get; set; }
    }
}
