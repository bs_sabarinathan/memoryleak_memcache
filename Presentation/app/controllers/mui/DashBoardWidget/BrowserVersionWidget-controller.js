﻿(function (ng, app) {
    "use strict";

    function muiDashBoardWidgetBrowserVersionWidgetCtrl($scope, $timeout, $http, $compile, $resource, $cookies, $location, $window, WidgetbrowserversionService) {
        $scope.browserVersionchartMetrics = {
            "Count": "1"
        };
        $scope.GlobalChartType = {
            1: "column",
            2: "pie"
        };
        $scope.userimg = parseInt($cookies['UserId']);
        $timeout(function () {
            drawdetail();
        }, 100);
        $scope.BrowserVersionWidgetContentID = $scope.WizardID;

        function drawdetail() {
            $scope.browserVersionchartpoints = [];
            var visualtype = $scope.visualType;
            WidgetbrowserversionService.GetDynamicwidgetContentUserID(parseInt($cookies['UserId'], 10), $scope.WizardTypeID, $scope.WizardRealID, $scope.DimensionID).then(function (GetDynamicwidgetContentUserID) {
                var browserVersionResult = GetDynamicwidgetContentUserID.Response;
                var data = [];
                for (var i = 0; i < browserVersionResult.length; i++) {
                    if ($scope.GlobalChartType[$scope.visualType] == "column") data.push({
                        y: browserVersionResult[i].VALUE,
                        label: browserVersionResult[i].NAME + browserVersionResult[i].VERSION,
                        name: browserVersionResult[i].NAME + browserVersionResult[i].VERSION
                    });
                    else data.push({
                        y: browserVersionResult[i].VALUE,
                        legendText: browserVersionResult[i].NAME + browserVersionResult[i].VERSION + ": " + browserVersionResult[i].VALUE,
                        indexLabel: "",
                        name: browserVersionResult[i].NAME + browserVersionResult[i].VERSION
                    });
                }
                $('#' + $scope.BrowserVersionWidgetContentID + '').attr('data-visualType', $scope.GlobalChartType[$scope.visualType]);
                $('#' + $scope.BrowserVersionWidgetContentID + '').attr('data-value', JSON.stringify(data));
                $('#' + $scope.BrowserVersionWidgetContentID + '').attr('data-charttitle', 'Browser Version Summary');
                RenderBarChart($scope.WizardID);
                $('#' + $scope.BrowserVersionWidgetContentID + '').parents('.box-content').find('.InProgress').remove();
            });
        };

        function RenderBarChart(WizardID) {
            $('#' + WizardID + '').height($('#' + WizardID + '').parents('.box-content').height() - 20);
            var data = JSON.parse($('#' + WizardID + '').attr('data-value'));
            var charttitle = $('#' + WizardID + '').attr('data-charttitle');
            CanvasJS.addColorSet("finColorSet1", ["#BDF2A1", "#9ECF6E", "#DDDD69", "#4DA3CE", "#B08BEB", "#3EA0DD", "#F5A52A", "#23BFAA", "#FAA586", "#EB8CC6"]);
            if ($('#' + WizardID + '').attr('data-visualType') == "column") {
                var chart = new CanvasJS.Chart(WizardID, {
                    title: {
                        text: charttitle,
                        verticalAlign: "top",
                        horizontalAlign: "center",
                        fontSize: 15,
                        fontFamily: "Calibri",
                        fontWeight: "normal",
                        fontColor: "black",
                        fontStyle: "normal",
                        borderThickness: 0,
                        borderColor: "black",
                        cornerRadius: 0,
                        backgroundColor: null,
                        margin: 0
                    },
                    colorSet: "finColorSet1",
                    height: $('#' + WizardID + '').parents('.box-content').height() - 10,
                    axisX: {
                        tickColor: "white",
                        labelAutoFit: false,
                        labelWrap: true,
                        tickLength: 5,
                        labelFontFamily: "sans-serif",
                        labelFontColor: "#7F7F7F",
                        labelFontSize: 10,
                        labelFontWeight: "normal"
                    },
                    axisY: {
                        tickLength: 0,
                        labelFontSize: 0,
                        lineThickness: 0,
                        gridThickness: 0,
                        includeZero: true,
                    },
                    data: [{
                        type: "column",
                        indexLabel: "{y}",
                        indexLabelFontSize: 12,
                        indexLabelFontWeight: "normal",
                        indexLabelPlacement: "outside",
                        indexLabelOrientation: "horizontal",
                        indexLabelFontFamily: "sans-serif",
                        indexLabelFontColor: "darkgrey",
                        legendMarkerColor: "gray",
                        showInLegend: false,
                        toolTipContent: "{name}:<strong>{y}</strong>",
                        dataPoints: data
                    }]
                });
            } else {
                var chart = new CanvasJS.Chart(WizardID, {
                    title: {
                        text: charttitle,
                        verticalAlign: "top",
                        horizontalAlign: "center",
                        fontSize: 15,
                        fontFamily: "Calibri",
                        fontWeight: "normal",
                        fontColor: "black",
                        fontStyle: "normal",
                        borderThickness: 0,
                        borderColor: "black",
                        cornerRadius: 0,
                        backgroundColor: null,
                        margin: 0
                    },
                    animationEnabled: true,
                    height: $('#' + WizardID + '').parents('.box-content').height() - 10,
                    legend: {
                        verticalAlign: "center",
                        horizontalAlign: "right",
                        fontSize: 12,
                        fontweight: "normal",
                        padding: 0,
                        fontColor: "darkgrey",
                        fontFamily: "sans-serif"
                    },
                    data: [{
                        indexLabelFontSize: 12,
                        indexLabelLineThickness: 0,
                        indexLabelFontStyle: "italic",
                        indexLabelFontWeight: "normal",
                        indexLabelFontFamily: "sans-serif",
                        indexLabelFontColor: "darkgrey",
                        indexLabelLineColor: "darkgrey",
                        indexLabelPlacement: "outside",
                        type: "pie",
                        showInLegend: true,
                        toolTipContent: "{name}:<strong>{y}</strong>",
                        dataPoints: data
                    }]
                });
            }
            chart.render();
        }
        $(document).on("OnChartLoad", function (event, setChartID) {
            if (setChartID != undefined) {
                $('#' + setChartID + '').html('');
                RenderBarChart(setChartID);
            }
        });

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.DashBoardWidget.BrowserVersionWidgetCtrl']"));
        });
    }
    app.controller("mui.DashBoardWidget.BrowserVersionWidgetCtrl", ['$scope', '$timeout', '$http', '$compile', '$resource', '$cookies', '$location', '$window', 'WidgetbrowserversionService', muiDashBoardWidgetBrowserVersionWidgetCtrl]);
    function WidgetbrowserversionService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session'); return ({
            GetDynamicwidgetContentUserID: GetDynamicwidgetContentUserID
        });
        function GetDynamicwidgetContentUserID(UserID, WidgetTypeID, WidgetID, DimensionID) { var request = $http({ method: "get", url: "api/common/GetDynamicwidgetContentUserID/" + UserID + "/" + WidgetTypeID + "/" + WidgetID + "/" + DimensionID, params: { action: "get", } }); return (request.then(handleSuccess, handleError)); }
        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) { return ($q.reject("An unknown error occurred.")); }
            return ($q.reject(response.data.message));
        }
        function handleSuccess(response) { return (response.data); }
    }
    app.service("WidgetbrowserversionService", ['$http', '$q', WidgetbrowserversionService]);
})(angular, app);