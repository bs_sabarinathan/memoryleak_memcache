﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IFilterSettings interface for table 'PM_Filter'.
    /// </summary>
    public interface IFilterSettings
    {
        #region Public Properties

        int FilterID
        {
            get;
            set;

        }

        string FilterName
        {
            get;
            set;
        }

        string Keyword
        {
            get;
            set;
        }

        int UserID
        {
            get;
            set;

        }

        int TypeID
        {
            get;
            set;
        }

        string EntityTypeID
        {
            get;
            set;
        }


        string StartDate
        {
            get;
            set;
        }

        string EndDate
        {
            get;
            set;
        }

        string WhereCondition
        {
            get;
            set;
        }
        IList<IFiltersettingsValues> FilterValues
        {
            get;
            set;
        }
        int IsDetailFilter { get; set; }
        string EntityMemberID
        {
            get;
            set;
        }

        #endregion
    }
}
