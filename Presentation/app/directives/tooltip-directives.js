﻿app
.directive("entitytypetreetooltip", ['$compile', '$parse', function ($compile, $parse) {
    return function (scope, element, attrs) {
        scope.qtipSkin = (attrs.skin ? "qtip-" + attrs.skin : "qtip-dark");
        scope.closeButton = (attrs.closeButton == null ? false : true)
        scope.qtipPointerPos = "top center";
        scope.qtipContentPos = "top center";
        var attrslst = attrs;
        $(element).qtip({
            content: {
                text: '<div><img position="right" style="width:15px; margin-right:5px;" src="assets/img/loading.gif"> Loading..</div>',
            },
            events: {
                show: function (event, api) {
                    if (attrslst.tooltiptype == "ENTITYTYPETREE") {
                        var qtipContent_id = api.elements.content.attr('id');
                        scope.LoadtQtipForEntityTypeTreeStructure(qtipContent_id, attrslst.entitytypeid);
                    }
                }
            },
            style: {
                classes: scope.qtipSkin + " qtip-shadow qtip-rounded",
                title: {
                    'display': 'none'
                }
            },
            show: {
                event: 'mouseenter click unfocus',
                solo: true
            },
            hide: {
                delay: 0,
                fixed: false,
                effect: function () {
                    $(this).fadeOut(0);
                },
                event: 'unfocus click mouseleave'
            },
            position: {
                my: 'bottom center',
                at: 'top center',
                viewport: $(window),
                adjust: {
                    method: 'shift',
                    mouse: true
                },
                corner: {
                    target: 'bottom center',
                    tooltip: 'bottom center',
                    mimic: 'top'
                },
                target: 'mouse'
            }
        });
        scope.$on("$destroy", function () {
            $(element).qtip('destroy', true);
            $(window).off("resize.Viewport");
        });
    }
}])
.directive("damlistviewpreviewtooltip", ['$compile', '$parse', '$rootScope', function ($compile, $parse, $rootScope) {
    return function (scope, element, attrs) {
        scope.qtipSkin = (attrs.skin ? "qtip-" + attrs.skin : "lv-DAM_AssetsQtip"); scope.closeButton = (attrs.closeButton == null ? false : true)
        scope.qtipPointerPos = "top center"; scope.qtipContentPos = "top center"; var attrslst = attrs; $(element).qtip({ content: { text: '<div class="InProgress"><span><img src="assets/img/loading.gif">Loading...</span></div>', }, events: { show: function (event, api) { if (attrslst.tooltiptype == "assetlistview") { var qtipContent_id = api.elements.content.attr('id'); scope.LoadAssetListViewMetadata(qtipContent_id, attrslst.entitytypeid, attrslst.backcolor, attrslst.shortdesc); } } }, style: { classes: scope.qtipSkin + " qtip-dark qtip-shadow qtip-rounded", title: { 'display': 'none' } }, show: { event: 'mouseenter click unfocus', solo: true }, hide: { delay: 0, fixed: false, effect: function () { $(this).fadeOut(0); }, event: 'unfocus click mouseleave' }, position: { my: 'left top', at: 'right top', viewport: $(window), corner: { target: 'top right', tooltip: 'top right', mimic: 'top' }, target: 'mouse' } }); scope.$on("$destroy", function () { $(element).qtip('destroy', true); $(window).off("resize.Viewport"); element.remove(); });
    }
}])
.directive("damthumbnailviewtooltip", ['$compile', '$parse', function ($compile, $parse) {
    return function (scope, element, attrs) {
        scope.qtipSkin = (attrs.skin ? "qtip-" + attrs.skin : "th-DAM_AssetsQtip"); scope.closeButton = (attrs.closeButton == null ? false : true)
        scope.qtipPointerPos = "top center"; scope.qtipContentPos = "top center"; var attrslst = attrs; $(element).qtip({ content: { text: '<div class="InProgress"><span><img src="assets/img/loading.gif">Loading...</span></div>', }, events: { show: function (event, api) { if (attrslst.tooltiptype == "assetthumbnailview") { var qtipContent_id = api.elements.content.attr('id'); scope.LoadAssetthumbnailViewMetadata(qtipContent_id, attrslst.entitytypeid, attrslst.backcolor, attrslst.shortdesc); } } }, style: { classes: scope.qtipSkin + " qtip-dark qtip-shadow qtip-rounded", title: { 'display': 'none' } }, show: { event: 'mouseenter click unfocus', solo: true }, hide: { delay: 0, fixed: false, effect: function () { $(this).fadeOut(0); }, event: 'unfocus click mouseleave' }, position: { my: 'left top', at: 'right top', viewport: $(window), corner: { target: 'top right', tooltip: 'top right', mimic: 'top' }, target: 'mouse' } }); scope.$on("$destroy", function () { $(element).qtip('destroy', true); $(window).off("resize.Viewport"); element.remove(); });
    }
}])
.directive("dampublisheddetailtooltip", ['$compile', '$parse', function ($compile, $parse) {
    return function (scope, element, attrs) {
        scope.qtipSkin = (attrs.skin ? "qtip-" + attrs.skin : "th-DAM_AssetsQtip"); scope.closeButton = (attrs.closeButton == null ? false : true)
        scope.qtipPointerPos = "top center"; scope.qtipContentPos = "top center"; var attrslst = attrs; $(element).qtip({
            content: { text: '<div class="InProgress"><span><img src="assets/img/loading.gif">Loading...</span></div>', }, events: {
                show: function (event, api) {
                    var qtipContent_id = api.elements.content.attr('id'), linkdetailarr = []; if (attrslst.location == "link")
                        linkdetailarr = attrslst.linkdetails.split("###"); var header = attrslst.location == "link" ? "Linked asset" : "Published asset", lable1 = attrslst.location == "link" ? "Source" : "Published by", lable2 = attrslst.location == "link" ? "Owner" : "Published on", value1 = attrslst.location == "link" ? (linkdetailarr.length > 0 ? linkdetailarr[0] : "") : attrslst.publishedby, value2 = attrslst.location == "link" ? (linkdetailarr.length > 1 ? linkdetailarr[1] : "") : attrslst.publishedon; value3 = attrslst.location == "link" ? (linkdetailarr.length > 2 ? linkdetailarr[2] : "") : ""; if ((attrslst.publishedby != null && !attrslst.publishedon.contains("1900")) || attrslst.location == "link") {
                            var html = ""; html += ' <div class="th-AssetInfoTooltip">'; html += ' <div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + lable1 + '</span><span class="th-AssetInfoValue">' + value1 + '</span></div>'; html += ' <div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">' + lable2 + '</span><span class="th-AssetInfoValue">' + value2 + '</span></div>'; if (attrslst.location == "link")
                                html += ' <div class="th-AssetInfoRow"><span class="th-AssetInfoLabel">Linked by</span><span class="th-AssetInfoValue">' + value3 + '</span></div>'; html += ' </div>'; $('#' + qtipContent_id).html(html); $('#' + qtipContent_id).qtip('show').show();
                        }
                        else { $('#' + qtipContent_id).html(''); $('#' + qtipContent_id).qtip('hide').hide(); }
                }
            }, style: { classes: scope.qtipSkin + " qtip-dark qtip-shadow qtip-rounded", title: { 'display': 'none' } }, show: { event: 'mouseenter click unfocus', solo: true }, hide: { delay: 0, fixed: false, effect: function () { $(this).fadeOut(0); }, event: 'unfocus click mouseleave' }, position: { my: 'left top', at: 'right top', viewport: $(window), corner: { target: 'top right', tooltip: 'top right', mimic: 'top' }, target: 'mouse' }
        }); scope.$on("$destroy", function () { $(element).qtip('destroy', true); $(window).off("resize.Viewport"); });
    }
}]);