﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Task.Interface
{
    interface ITempTaskList
    {
        #region Properties

        int Id
        {
            get;
            set;

        }


        int TempID
        {
            get;
            set;

        }

        int TaskListID
        {
            get;
            set;

        }


        int SortOder
        {
            get;
            set;

        }



        #endregion
    }
}
