﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace BrandSystems.Marcom.Dal.Planning.Model
{
  public class DynamicAttributesDao :BaseDao, ICloneable
    {
        #region Member Variables
		
        protected int _id;
      //  protected Iesi.Collections.Generic.ISet<DynamicnewAttributes> _attributes;
		//protected List<KeyValuePair<String, object>> _attributes;

        protected IDictionary _attributes;
       
		#endregion
		
		#region Constructors
		public DynamicAttributesDao() {}

        public DynamicAttributesDao(int pId, IDictionary pAttributes)
		{
			this._id = pId;
            this._attributes = new Hashtable();
            this._attributes = pAttributes;  
		}
	
		#endregion
		
		#region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual IDictionary Attributes
        {
            get { return _attributes; }
            set { _attributes = value; }

        }
		
		#endregion
        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string DynamicAttributesDao = "DynamicAttributesDao";			
			public const string Id = "Id";
            public const string Attributes = "Attributes";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string DynamicAttributesDao = "DynamicAttributesDao";
            public const string Id = "Id";
            public const string Attributes = "Attributes";					
		}

		#endregion
    }
}
