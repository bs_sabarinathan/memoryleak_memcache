using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Metadata.Model
{

	/// <summary>
	/// ModuleDao object for table 'MM_Module'.
	/// </summary>
	
	public partial class ModuleDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected string _caption;
		protected string _description;
		protected bool _isenable;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public ModuleDao() {}
			
		public ModuleDao(int pId, string pCaption, string pDescription, bool pIsEnable)
		{
			this._id = pId; 
			this._caption = pCaption; 
			this._description = pDescription; 
			this._isenable = pIsEnable; 
		}
				
		public ModuleDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual string Caption
		{
			get { return _caption; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
			  _bIsChanged |= (_caption != value); 
			  _caption = value; 
			}
			
		}
		
		public virtual string Description
		{
			get { return _description; }
			set 
			{
			  if (value != null && value.Length > 1073741823)
			    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
			  _bIsChanged |= (_description != value); 
			  _description = value; 
			}
			
		}
		
		public virtual bool IsEnable
		{
			get { return _isenable; }
			set { _bIsChanged |= (_isenable != value); _isenable = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string ModuleDao = "ModuleDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";			
			public const string Description = "Description";			
			public const string IsEnable = "IsEnable";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string ModuleDao = "ModuleDao";			
			public const string Id = "Id";			
			public const string Caption = "Caption";			
			public const string Description = "Description";			
			public const string IsEnable = "IsEnable";			
		}

		#endregion
		
		
	}
	
}
