﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task;
using BrandSystems.Marcom.Core.Access.Interface;

namespace BrandSystems.Marcom.Core.Task.Interface
{
    public interface IMyTaskCollection
    {
        #region Public Properties

        String FilterHeaderName
        {
            get;
            set;

        }

        IList<MyTask> TaskCollection
        {
            get;
            set;
        }

        string EntityColorCode
        {
            get;
            set;

        }

        string EntityShortDescription
        {
            get;
            set;

        }
        #endregion
    }
}
