﻿
(function (ng, app) {

    "use strict";

    function muiplanningtooliframeCtrl($scope, $cookies, $http, $stateParams, $compile, $window, $timeout, $location, IframService) {

        $('#myBriefContainer').css('min-height', window.innerHeight - 103);

        function setIFrameSrc(iFrameID) {


            IframService.GetNavigationExternalLinksByID($stateParams.ID).then(function (getmodule) {
                $scope.width = ($('#iframeHolder').width()) + 'px';
                $scope.height = (window.innerHeight - 132) + 'px';
                var myFrame = $('#' + iFrameID);
                var url = getmodule.Response;
                $(myFrame).attr('src', url);

                $('iframe#externallink').load(function () {
                    $('#divLoading').css('display', 'none');
                });

            });

        }


        $scope.$on('loadexternalurl', function (event, id) {
            $("#externallink").attr('src', "");
            $('#divLoading').css('display', 'block');
            $timeout(function () {
                setIFrameSrc("externallink");
            }, 50);

            
        });

        $timeout(function () {
            setIFrameSrc("externallink");
        }, 10);

        $scope.$on("$destroy", function () {
            RecursiveUnbindAndRemove($("[ng-controller='mui.planningtool.iframeCtrl']"));
        });
        $scope.set_color = function (clr) {
            if (clr != null)
                return { 'background-color': "#" + clr.toString().trim() };
            else
                return '';
        }
    }
    app.controller("mui.planningtool.iframeCtrl", ['$scope', '$cookies', '$http', '$stateParams', '$compile', '$window', '$timeout', '$location', 'IframService', muiplanningtooliframeCtrl]);

})(angular, app);

(function (ng, app) {
    "use strict";

    function IframService($http, $q) {
        $http.defaults.headers.common.sessioncookie = $.cookie('Session');
        return ({
            GetNavigationExternalLinksByID:GetNavigationExternalLinksByID
        });


        function GetNavigationExternalLinksByID(ID) {
            var request = $http({
                method: "get",
                url: "api/common/GetNavigationExternalLinksByID/" + ID,
                params: {
                    action: "get",
                }
            });
            return (request.then(handleSuccess, handleError));
        }

        function handleError(response) {
            if (!angular.isObject(response.data) || !response.data.message) {
                return ($q.reject("An unknown error occurred."));
            }
            return ($q.reject(response.data.message));
        }

        function handleSuccess(response) {
            return (response.data);
        }
    }
    app.service("IframService", ['$http', '$q', IframService]);
})(angular, app);