﻿using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Task.Interface;
using BrandSystems.Marcom.Dal.Task.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Presentation.Controllers
{
    public class TaskController : ApiController
    {
        ServiceResponse result;
        [Route("api/Task/InsertUpdateTaskList")]
        [HttpPost]
        public ServiceResponse InsertUpdateTaskList([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertUpdateTaskList((int)jobj["ID"], (string)jobj["Caption"], (string)jobj["Description"], (int)jobj["sortorder"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Task/AddUpdateTaskFlag")]
        [HttpPost]
        public ServiceResponse AddUpdateTaskFlag([FromBody]JObject jobject)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.AddUpdateTaskFlag((string)jobject["Name"], (string)jobject["ColoCode"], (string)jobject["Description"], (int)jobject["SortOder"], (int)jobject["ID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/DeleteTaskFlagByID/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteTaskFlagByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteTaskFlagById(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/DeleteSystemTaskList/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteSystemTaskList(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = marcomManager.TaskManager.DeleteSystemTaskList(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetTaskFlags")]
        [HttpGet]
        public ServiceResponse GetTaskFlags()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetTaskFlags();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetTaskList")]
        [HttpGet]
        public ServiceResponse GetTaskList()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetTaskList();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetTaskTypes")]
        [HttpGet]
        public ServiceResponse GetTaskTypes()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = marcomManager.TaskManager.GetTaskTypes();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/UpdateTaskListSortOrder/{TaskListID}/{SortOrderID}")]
        [HttpPut]
        public ServiceResponse UpdateTaskListSortOrder(int TaskListID, int SortOrderID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdateTaskListSortOrder(TaskListID, SortOrderID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/InsertTaskWithAttachments")]
        [HttpPost]
        public ServiceResponse InsertTaskWithAttachments([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrAttchObj = (JArray)jobj["TaskAttachments"];
                JArray arrFilesObj = (JArray)jobj["TaskFiles"];
                JArray arrperiods = (JArray)jobj["Periods"];
                JArray arrChkList = (JArray)jobj["AdminTaskCheckList"];
                JArray arrentityamountcurrencytype = (JArray)jobj["Entityamountcurrencytype"];
                IList<IAdminTask> listTask = new List<IAdminTask>();
                JArray EntityPhaseStepDetails = (JArray)jobj["PhaseStepDetails"];
                IAdminTask taskval = marcomManager.TaskManager.TasksService();
                taskval.Caption = (string)jobj["Name"];
                taskval.Description = (string)jobj["Description"];
                taskval.TaskType = (int)jobj["TaskType"];
                taskval.TaskListID = (int)jobj["TaskListID"];
                listTask.Add(taskval);

                IList<INewTaskAttachments> listTaskattachment = new List<INewTaskAttachments>();
                if (arrAttchObj != null)
                {
                    foreach (var arm in arrAttchObj)
                    {
                        INewTaskAttachments attachval = marcomManager.TaskManager.TasksAttachmentService();
                        attachval.Name = (string)arm["FileName"] + (string)arm["Extension"];
                        attachval.ActiveFileid = 1;
                        attachval.ActiveVersionNo = 1;
                        attachval.Createdon = DateTime.UtcNow;
                        attachval.Typeid = 4;
                        attachval.Description = (string)arm["FileDescription"];
                        listTaskattachment.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listTaskattachment = null;
                }

                IList<IFile> listFiles = new List<IFile>();
                if (arrFilesObj != null)
                {
                    foreach (var arm in arrFilesObj)
                    {
                        IFile attachval = marcomManager.CommonManager.Fileservice();
                        attachval.Checksum = (string)arm["Checksum"];
                        attachval.CreatedOn = (DateTime)arm["CreatedOn"];
                        attachval.Extension = (string)arm["Extension"];
                        attachval.MimeType = (string)arm["MimeType"];
                        attachval.Moduleid = (int)arm["ModuleID"];
                        attachval.Name = (string)arm["Name"];
                        attachval.Ownerid = (int)arm["OwnerID"];
                        attachval.Size = (long)arm["Size"];
                        attachval.VersionNo = (int)arm["VersionNo"];
                        attachval.strFileID = (string)arm["FileGuid"];
                        attachval.Description = (string)arm["FileDescription"];
                        listFiles.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listTaskattachment = null;
                }

                List<IEntityPeriod> listEntityperiods = new List<IEntityPeriod>();
                if (arrperiods != null)
                {
                    foreach (var entityPeriodObj in arrperiods)
                    {
                        if (entityPeriodObj.Count() > 0)
                        {
                            foreach (var arrp in entityPeriodObj)
                            {
                                DateTime startDate1 = new DateTime();
                                DateTime endDate = new DateTime();
                                string NewstartDate = (arrp["startDate"].ToString()) == "" ? null : ((DateTime.ParseExact(arrp["startDate"].ToString(), "dd-MM-yyyy", null)).ToString("yyyy-MM-dd") == "" ? null : (DateTime.ParseExact(arrp["startDate"].ToString(), "dd-MM-yyyy", null)).ToString("yyyy-MM-dd"));
                                string NewendDate = (arrp["endDate"].ToString()) == "" ? null : ((DateTime.ParseExact(arrp["endDate"].ToString(), "dd-MM-yyyy", null)).ToString("yyyy-MM-dd") == "" ? null : (DateTime.ParseExact(arrp["endDate"].ToString(), "dd-MM-yyyy", null)).ToString("yyyy-MM-dd"));

                                if (DateTime.TryParse(NewstartDate, out startDate1) && DateTime.TryParse(NewendDate, out endDate))
                                {

                                    IEntityPeriod enti = marcomManager.PlanningManager.Entityperiodservice();
                                    enti.Startdate = startDate1;
                                    enti.EndDate = endDate;
                                    enti.Description = (string)arrp["comment"];
                                    enti.SortOrder = (int)arrp["sortorder"];
                                    listEntityperiods.Add(enti);
                                }
                            }
                        }
                    }
                }
                else if (arrperiods == null)
                {
                    listEntityperiods = null;
                }
                IList<IEntityAmountCurrencyType> listentityamountcurrencytype = new List<IEntityAmountCurrencyType>();
                if (arrentityamountcurrencytype != null)
                {
                    if (arrentityamountcurrencytype.Count() > 0)
                    {
                        foreach (var item in arrentityamountcurrencytype)
                        {
                            foreach (var i in item)
                            {
                                IEntityAmountCurrencyType entamcur = marcomManager.PlanningManager.EntityAmountCurrencyTypeservice();
                                entamcur.Amount = (decimal)i["amount"];
                                entamcur.Currencytypeid = (int)i["currencytype"];
                                entamcur.Attributeid = (int)i["Attributeid"];
                                listentityamountcurrencytype.Add(entamcur);
                            }
                        }
                    }

                }
                else if (arrentityamountcurrencytype == null)
                {
                    arrentityamountcurrencytype = null;
                }


                JArray taskAttribue = (JArray)jobj["AttributeData"];
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (taskAttribue != null)
                {
                    foreach (var objAttr in taskAttribue)
                    {

                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                            if ((string)objAttr["Value"] != "-1")
                                entityAttr.SpecialValue = Convert.ToString(objAttr["Value"].ToString());
                            else
                                entityAttr.SpecialValue = "";
                        }
                        else
                        {
                            if (entityAttr.ID != (int)SystemDefinedAttributes.CreationDate)
                            {
                                if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                {
                                    entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Date)
                                {
                                    entityAttr.Value = (objAttr["NodeID"].ToString());//Newtonsoft.Json.JsonConvert.DeserializeObject<string>(obj["Value"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Integer)
                                {
                                    entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                }
                                else
                                    entityAttr.Value = objAttr["NodeID"].ToString();
                            }
                            else
                            {
                                entityAttr.Value = DateTime.UtcNow.ToString("yyyy-MM-dd");
                            }
                        }
                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (taskAttribue == null)
                {
                    listattributevalues = null;
                }
                IList<IAdminTaskCheckList> AdminTaskChkLst = new List<IAdminTaskCheckList>();


                if (arrChkList != null)
                {
                    foreach (var arm in arrChkList)
                    {
                        if ((string)arm["NAME"] != null)
                        {
                            IAdminTaskCheckList adminChklist = marcomManager.TaskManager.AdminTaskCheckListService();
                            adminChklist.NAME = (string)arm["NAME"];
                            adminChklist.SortOrder = 1;
                            AdminTaskChkLst.Add(adminChklist);
                        }
                    }
                }
                else if (arrChkList == null)
                {
                    AdminTaskChkLst = null;
                }



                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertTaskWithAttachments((int)jobj["TaskType"], (int)jobj["Typeid"], (string)jobj["Name"], listTask, listTaskattachment, listFiles, listattributevalues, AdminTaskChkLst, arrAttchObj, listEntityperiods, listentityamountcurrencytype, EntityPhaseStepDetails);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }
        [Route("api/Task/InsertUpdateTaskTemplateCondition")]
        [HttpPost]
        public ServiceResponse InsertUpdateTaskTemplateCondition([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                int lastInserted = 0;

                if (jobj != null)
                {
                    JArray jarr = (JArray)jobj["TaskTempCondition"];
                    result.StatusCode = (int)HttpStatusCode.OK;

                    for (int i = 0; i < jarr.Count; i++)
                    {
                        int id = 0;
                        var ID = jarr[i]["ID"];
                        if (jarr[i]["ID"] != null)
                        {
                            id = (int)jarr[i]["ID"];
                        }
                        string Attributevalue = "";
                        int attributeid = 0;
                        int attributelevel = 0;
                        string objentityattr = (string)jarr[i]["ObjEntityAttributes"];

                        if (objentityattr != null)
                        {
                            string[] attributeData = Convert.ToString(jarr[i]["ObjEntityAttributes"]).Split('_');
                            attributeid = Convert.ToInt32(attributeData[0]);
                            attributelevel = Convert.ToInt32(attributeData[1]);
                        }
                        if (objentityattr != null && objentityattr.Count() > 0)
                        {

                            string[] ArrattrVal = Newtonsoft.Json.JsonConvert.DeserializeObject<string[]>(jarr[i]["ObjAttributeOptionns"].ToString());
                            if (ArrattrVal != null)
                            {
                                Attributevalue = string.Join(",", ArrattrVal);
                            }
                            else
                            {
                                Attributevalue = Convert.ToString(ArrattrVal);
                            }
                        }
                        int ConditionType = 1;
                        if (jarr[i]["ConditionType"] != null && (string)jarr[i]["ConditionType"] != "")
                        {
                            ConditionType = (int)jarr[i]["ConditionType"];
                        }
                        lastInserted = marcomManager.TaskManager.InsertUpdateTaskTemplateCondition(id, (int)jobj["TasktemplateID"], (int)jarr[i]["ObjEntityType"], (int)jarr[i]["SortOrder"], attributeid, Attributevalue, attributelevel, ConditionType);
                    }
                }
                result.Response = lastInserted;

                marcomManager.TaskManager.UpdateTaskTemplateCriteria(jobj["TaskTempCriteria"].ToString(), (int)jobj["TasktemplateID"]);
                //jobj["TaskTempCriteria"].ToString()

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/GetTaskTemplateDetails")]
        [HttpGet]
        public ServiceResponse GetTaskTemplateDetails()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetTaskTemplateDetails();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/InsertTempTaskList")]
        [HttpPost]
        public ServiceResponse InsertTempTaskList([FromBody]JObject jobject)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertTempTaskList((int)jobject["templateId"], (int)jobject["taskListID"], (int)jobject["SortOrder"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/InsertUpdateTemplate")]
        [HttpPost]
        public ServiceResponse InsertUpdateTemplate([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertUpdateTemplate((int)jobj["ID"], (string)jobj["caption"], (string)jobj["description"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Task/UpdateAdminTask")]
        [HttpPost]
        public ServiceResponse UpdateAdminTask([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray milestoneAttribue = (JArray)jobj["AttributeData"];
                JArray arrChkList = (JArray)jobj["AdminTaskCheckList"];

                List<object> listObj = new List<object>();
                listObj = null;
                result.StatusCode = (int)HttpStatusCode.OK;
                IList<IAttributeData> listAttributes = new List<IAttributeData>();
                List<object> objval = new List<object>();
                IList<IAdminTaskCheckList> AdminTaskChkLst = new List<IAdminTaskCheckList>();
                if (arrChkList != null)
                {
                    foreach (var arm in arrChkList)
                    {
                        IAdminTaskCheckList adminChklist = marcomManager.TaskManager.AdminTaskCheckListService();
                        adminChklist.NAME = (string)arm["NAME"];
                        adminChklist.SortOrder = 1;
                        adminChklist.ID = (int)arm["ID"];
                        AdminTaskChkLst.Add(adminChklist);
                    }
                }
                else if (arrChkList == null)
                {
                    AdminTaskChkLst = null;
                }

                result.Response = marcomManager.TaskManager.UpdateAdminTask(30, (string)jobj["Name"], (string)jobj["Description"], (int)jobj["TaskType"], listAttributes, (int)jobj["TaskID"], AdminTaskChkLst);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Task/DeleteTaskTemplateListById/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteTaskTemplateListById(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteTaskTemplateListById(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetTaskAttachmentFile/{TaskID}")]
        [HttpGet]
        public ServiceResponse GetTaskAttachmentFile(int TaskID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetTaskAttachmentFile(TaskID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetEntityTaskAttachmentFile/{TaskID}")]
        [HttpGet]
        public ServiceResponse GetEntityTaskAttachmentFile(int TaskID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetEntityTaskAttachmentFile(TaskID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/ViewAllFilesByEntityID/{TaskID}/{VersionFileID}")]
        [HttpGet]
        public ServiceResponse ViewAllFilesByEntityID(int TaskID, int VersionFileID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.ViewAllFilesByEntityID(TaskID, VersionFileID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/UpdateAttachmentVersionNo/")]
        [HttpPost]
        public ServiceResponse UpdateAttachmentVersionNo([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdateAttachmentVersionNo((int)jobj["TaskID"], (int)jobj["SelectedVersion"], (int)jobj["VersioningFileId"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/InsertTaskAttachments/")]
        [HttpPost]
        public ServiceResponse InsertTaskAttachments([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrAttchObj = (JArray)jobj["TaskAttachments"];
                JArray arrFilesObj = (JArray)jobj["TaskFiles"];

                IList<INewTaskAttachments> listTaskattachment = new List<INewTaskAttachments>();
                if (arrAttchObj != null)
                {
                    foreach (var arm in arrAttchObj)
                    {
                        INewTaskAttachments attachval = marcomManager.TaskManager.TasksAttachmentService();
                        attachval.Name = (string)arm["FileName"] + (string)arm["Extension"];
                        attachval.ActiveFileid = 1;
                        attachval.ActiveVersionNo = 1;
                        attachval.Createdon = DateTime.UtcNow;
                        attachval.Typeid = 4;
                        attachval.Description = (string)arm["FileDescription"];
                        listTaskattachment.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listTaskattachment = null;
                }

                IList<IFile> listFiles = new List<IFile>();
                if (arrFilesObj != null)
                {
                    foreach (var arm in arrFilesObj)
                    {
                        IFile attachval = marcomManager.CommonManager.Fileservice();
                        attachval.Checksum = (string)arm["Checksum"];
                        attachval.CreatedOn = (DateTime)arm["CreatedOn"];
                        attachval.Extension = (string)arm["Extension"];
                        attachval.MimeType = (string)arm["MimeType"];
                        attachval.Moduleid = (int)arm["ModuleID"];
                        attachval.Name = (string)arm["Name"];
                        attachval.Ownerid = (int)arm["OwnerID"];
                        attachval.Size = (long)arm["Size"];
                        attachval.VersionNo = (int)arm["VersionNo"];
                        attachval.strFileID = (string)arm["FileGuid"];
                        attachval.Description = (string)arm["FileDescription"];
                        listFiles.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listTaskattachment = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertTaskAttachments((int)jobj["TaskID"], listTaskattachment, listFiles);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }
        [Route("api/Task/InsertEntityTaskAttachmentsVersion")]
        [HttpPost]
        public ServiceResponse InsertEntityTaskAttachmentsVersion([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrAttchObj = (JArray)jobj["TaskAttachments"];
                JArray arrFilesObj = (JArray)jobj["TaskFiles"];

                IList<IAttachments> listTaskattachment = new List<IAttachments>();
                if (arrAttchObj != null)
                {
                    foreach (var arm in arrAttchObj)
                    {
                        IAttachments attachval = marcomManager.TaskManager.EntityTasksAttachmentService();
                        attachval.Name = (string)arm["FileName"] + (string)arm["Extension"];
                        attachval.ActiveFileid = 1;
                        attachval.ActiveVersionNo = 1;
                        attachval.Createdon = DateTime.UtcNow;
                        attachval.Typeid = 4;
                        attachval.VersioningFileId = (int)jobj["VersioningFileId"];
                        //attachval.Description = (string)arm["FileDescription"];
                        listTaskattachment.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listTaskattachment = null;
                }

                IList<IFile> listFiles = new List<IFile>();
                if (arrFilesObj != null)
                {
                    foreach (var arm in arrFilesObj)
                    {
                        IFile attachval = marcomManager.CommonManager.Fileservice();
                        attachval.Checksum = (string)arm["Checksum"];
                        attachval.CreatedOn = (DateTime)arm["CreatedOn"];
                        attachval.Extension = (string)arm["Extension"];
                        attachval.MimeType = (string)arm["MimeType"];
                        attachval.Moduleid = (int)arm["ModuleID"];
                        attachval.Name = (string)arm["Name"];
                        attachval.Ownerid = (int)arm["OwnerID"];
                        attachval.Size = (long)arm["Size"];
                        attachval.VersionNo = (int)arm["VersionNo"];
                        attachval.strFileID = (string)arm["FileGuid"];
                        attachval.Description = (string)arm["FileDescription"];
                        listFiles.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listTaskattachment = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertEntityTaskAttachmentsVersion((int)jobj["TaskID"], listTaskattachment, listFiles, (int)jobj["FileId"], (int)jobj["VersioningFileId"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }
        [Route("api/Task/InsertEntityTaskAttachments")]
        [HttpPost]
        public ServiceResponse InsertEntityTaskAttachments([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {


                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrAttchObj = (JArray)jobj["TaskAttachments"];
                JArray arrFilesObj = (JArray)jobj["TaskFiles"];

                IList<IAttachments> listTaskattachment = new List<IAttachments>();
                if (arrAttchObj != null)
                {
                    foreach (var arm in arrAttchObj)
                    {
                        IAttachments attachval = marcomManager.TaskManager.EntityTasksAttachmentService();
                        attachval.Name = (string)arm["FileName"] + (string)arm["Extension"];
                        attachval.ActiveFileid = 1;
                        attachval.ActiveVersionNo = 1;
                        attachval.Createdon = DateTime.UtcNow;
                        attachval.Typeid = 4;
                        //attachval.Description = (string)arm["FileDescription"];
                        listTaskattachment.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listTaskattachment = null;
                }

                IList<IFile> listFiles = new List<IFile>();
                if (arrFilesObj != null)
                {
                    foreach (var arm in arrFilesObj)
                    {
                        IFile attachval = marcomManager.CommonManager.Fileservice();
                        attachval.Checksum = (string)arm["Checksum"];
                        attachval.CreatedOn = (DateTime)arm["CreatedOn"];
                        attachval.Extension = (string)arm["Extension"];
                        attachval.MimeType = (string)arm["MimeType"];
                        attachval.Moduleid = (int)arm["ModuleID"];
                        attachval.Name = (string)arm["Name"];
                        attachval.Ownerid = (int)arm["OwnerID"];
                        attachval.Size = (long)arm["Size"];
                        attachval.VersionNo = (int)arm["VersionNo"];
                        attachval.strFileID = (string)arm["FileGuid"];
                        attachval.Description = (string)arm["FileDescription"];
                        listFiles.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listTaskattachment = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertEntityTaskAttachments((int)jobj["TaskID"], listTaskattachment, listFiles);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }
        [Route("api/Task/UpdateTemplateTaskListSortOrder/{TempID}/{TaskListID}/{SortOrderID}")]
        [HttpPut]
        public ServiceResponse UpdateTemplateTaskListSortOrder(int TempID, int TaskListID, int SortOrderID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdateTemplateTaskListSortOrder(TempID, TaskListID, SortOrderID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/DeleteAttachments/{ActiveFileid}")]
        [HttpDelete]
        public ServiceResponse DeleteAttachments(int ActiveFileid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = marcomManager.TaskManager.DeleteAttachments(ActiveFileid);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetEntityTaskList/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetEntityTaskList(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetEntityTaskList(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/InsertEntityTaskWithAttachments")]
        [HttpPost]
        public ServiceResponse InsertEntityTaskWithAttachments([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrAttchObj = (JArray)jobj["TaskAttachments"];
                JArray arrFilesObj = (JArray)jobj["TaskFiles"];
                JArray arrobjMembers = (JArray)jobj["TaskMembers"];
                JArray arrperiods = (JArray)jobj["Periods"];
                JArray arrChkList = (JArray)jobj["AdminTaskCheckList"];
                JArray arrentityamountcurrencytype = (JArray)jobj["Entityamountcurrencytype"];
                JObject arrassetFilesObj = (JObject)jobj["AdditionalObjects"];

                //Used for getting Entity Task Phase,Step and Member Details


                IList<IEntityTask> listTask = new List<IEntityTask>();
                IEntityTask taskval = marcomManager.TaskManager.EntityTasksService();
                taskval.Name = (string)jobj["Name"];
                taskval.TaskListID = (int)jobj["TaskListID"];
                taskval.Description = (string)jobj["Description"];
                taskval.Note = (string)jobj["Note"];
                taskval.TaskType = (int)jobj["TaskType"];
                taskval.EntityTaskListID = 0;
                taskval.AssetId = (int)jobj["AssetID"];
                JArray EntityPhaseStepDetails = (JArray)jobj["phaseStepList"];

                DateTime startDate = new DateTime();
                if (DateTime.TryParse(jobj["DueDate"].ToString(), out startDate))
                    taskval.DueDate = startDate;
                else
                    taskval.DueDate = null;
                taskval.EntityID = Convert.ToInt32((string)jobj["ParentEntityID"]);

                listTask.Add(taskval);



                IList<ITaskMembers> listTaskMembers = new List<ITaskMembers>();

                if (arrobjMembers != null)
                {
                    foreach (var arm in arrobjMembers)
                    {
                        ITaskMembers entrole = marcomManager.TaskManager.EntityTaskMembersService();
                        entrole.RoleID = 4;
                        entrole.UserID = (int)arm["Userid"];
                        entrole.ApprovalStatus = null;
                        entrole.FlagColorCode = "f5f5f5";
                        entrole.ApprovalRount = 1;
                        entrole.EntityRoleID = arm["EntityRoleID"] == null ? 0 : (int)arm["EntityRoleID"];
                        entrole.IsGlobal = (bool)arm["IsGlobal"];
                        listTaskMembers.Add(entrole);
                    }
                }

                else if (arrobjMembers == null)
                {
                    // listTaskMembers = null;
                }

                IList<INewTaskAttachments> listTaskattachment = new List<INewTaskAttachments>();
                if (arrAttchObj != null)
                {
                    foreach (var arm in arrAttchObj)
                    {
                        INewTaskAttachments attachval = marcomManager.TaskManager.TasksAttachmentService();
                        attachval.Name = (string)arm["FileName"] + (string)arm["Extension"];
                        attachval.ActiveFileid = 1;
                        attachval.ActiveVersionNo = 1;
                        attachval.Createdon = DateTime.UtcNow;
                        attachval.Typeid = 4;
                        attachval.Description = (string)arm["FileDescription"];
                        listTaskattachment.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listTaskattachment = null;
                }


                IList<IFile> listFiles = new List<IFile>();
                if (arrFilesObj != null)
                {
                    foreach (var arm in arrFilesObj)
                    {
                        IFile attachval = marcomManager.CommonManager.Fileservice();
                        attachval.Checksum = (string)arm["Checksum"];
                        attachval.CreatedOn = (DateTime)arm["CreatedOn"];
                        attachval.Extension = (string)arm["Extension"];
                        attachval.MimeType = (string)arm["MimeType"];
                        attachval.Moduleid = (int)arm["ModuleID"];
                        attachval.Name = (string)arm["Name"];
                        attachval.Ownerid = (int)arm["OwnerID"];
                        attachval.Size = (long)arm["Size"];
                        attachval.VersionNo = (int)arm["VersionNo"];
                        attachval.strFileID = (string)arm["FileGuid"];
                        attachval.Description = (string)arm["FileDescription"];
                        attachval.IsExist = (bool)arm["IsExists"];
                        listFiles.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listTaskattachment = null;
                }

                JArray taskAttribue = (JArray)jobj["AttributeData"];
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (taskAttribue != null)
                {
                    foreach (var objAttr in taskAttribue)
                    {

                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if (objAttr["NodeID"] != null)
                            if ((dynamic)objAttr["NodeID"].Count() > 0)
                            {
                                foreach (var obj in (dynamic)objAttr["NodeID"])
                                {
                                    object valueObj = new object();
                                    valueObj = (dynamic)obj;
                                    listObj.Add(valueObj);
                                }
                                entityAttr.Value = listObj;
                                if ((string)objAttr["Value"] != "-1")
                                    entityAttr.SpecialValue = Convert.ToString(objAttr["Value"].ToString());
                                else
                                    entityAttr.SpecialValue = "";
                            }
                            else
                            {
                                if (entityAttr.ID != (int)SystemDefinedAttributes.CreationDate)
                                {
                                    if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                    {
                                        entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                    }
                                    if (objAttr["NodeID"].Type == JTokenType.Date)
                                    {
                                        entityAttr.Value = (objAttr["NodeID"].ToString());//Newtonsoft.Json.JsonConvert.DeserializeObject<string>(obj["Value"].ToString());
                                    }
                                    if (objAttr["NodeID"].Type == JTokenType.Integer)
                                    {
                                        entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                    }
                                    else
                                        entityAttr.Value = objAttr["NodeID"].ToString();
                                }
                                else
                                {
                                    entityAttr.Value = DateTime.UtcNow.ToString("yyyy-MM-dd");
                                }

                            }
                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (taskAttribue == null)
                {
                    listattributevalues = null;
                }
                IList<IEntityTaskCheckList> AdminTaskChkLst = new List<IEntityTaskCheckList>();


                if (arrChkList != null)
                {
                    foreach (var arm in arrChkList)
                    {
                        if ((string)arm["Name"] != null)
                        {
                            IEntityTaskCheckList adminChklist = marcomManager.TaskManager.EntityTaskCheckListService();
                            adminChklist.Name = (string)arm["Name"];
                            adminChklist.SortOrder = 1;
                            AdminTaskChkLst.Add(adminChklist);
                        }
                    }
                }
                else if (arrChkList == null)
                {
                    AdminTaskChkLst = null;
                }

                IList<IEntityPeriod> listEntityperiods = new List<IEntityPeriod>();
                if (arrperiods != null)
                {
                    foreach (var entityPeriodObj in arrperiods)
                    {
                        if (entityPeriodObj.Count() > 0)
                        {
                            foreach (var arrp in entityPeriodObj)
                            {                               
                                DateTime P_startDate = new DateTime();
                                DateTime p_endDate = new DateTime();
                                if (DateTime.TryParse(arrp["startDate"].ToString(), out P_startDate) && DateTime.TryParse(arrp["endDate"].ToString(), out p_endDate))
                                {
                                    IEntityPeriod enti = marcomManager.PlanningManager.Entityperiodservice();
                                    enti.Startdate = P_startDate;
                                    enti.EndDate = p_endDate;
                                    enti.Description = (string)arrp["comment"];
                                    enti.SortOrder = (int)arrp["sortorder"];
                                    listEntityperiods.Add(enti);
                                }
                            }
                        }
                    }
                }
                else if (arrperiods == null)
                {
                    listEntityperiods = null;
                }
                IList<IEntityAmountCurrencyType> listentityamountcurrencytype = new List<IEntityAmountCurrencyType>();
                if (arrentityamountcurrencytype != null)
                {
                    if (arrentityamountcurrencytype.Count() > 0)
                    {
                        foreach (var item in arrentityamountcurrencytype)
                        {
                            foreach (var i in item)
                            {
                                IEntityAmountCurrencyType entamcur = marcomManager.PlanningManager.EntityAmountCurrencyTypeservice();
                                entamcur.Amount = (decimal)i["amount"];
                                entamcur.Currencytypeid = (int)i["currencytype"];
                                entamcur.Attributeid = (int)i["Attributeid"];
                                listentityamountcurrencytype.Add(entamcur);
                            }
                        }
                    }

                }
                else if (arrentityamountcurrencytype == null)
                {
                    arrentityamountcurrencytype = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertEntityTaskWithAttachments((int)jobj["ParentEntityID"], (int)jobj["TaskType"], (int)jobj["Typeid"], (string)jobj["Name"], listTask, listTaskattachment, listFiles, listTaskMembers, listattributevalues, arrAttchObj, AdminTaskChkLst, listEntityperiods, 0, listentityamountcurrencytype, EntityPhaseStepDetails, arrassetFilesObj);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }
        [Route("api/Task/UpdateOverviewEntityTaskList/{OnTimeStatus}/{OnTimeComment}/{ID}")]
        [HttpPut]
        public ServiceResponse UpdateOverviewEntityTaskList(int OnTimeStatus, string OnTimeComment, int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdateOverviewEntityTaskList(OnTimeStatus, OnTimeComment, ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/InsertTaskMembers")]
        [HttpPost]

        public ServiceResponse InsertTaskMembers([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {


                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrobjMembers = (JArray)jobj["TaskMembers"];
                JArray arrobjGlobalMembers = (JArray)jobj["GlobalTaskMembers"];

                IList<ITaskMembers> listTaskMembers = new List<ITaskMembers>();
                IList<ITaskMembers> listGlobalTaskMembers = new List<ITaskMembers>();
                if (arrobjMembers != null)
                {
                    foreach (var arm in arrobjMembers)
                    {
                        ITaskMembers entrole = marcomManager.TaskManager.EntityTaskMembersService();
                        entrole.RoleID = 4;
                        entrole.UserID = (int)arm["Userid"];
                        entrole.ApprovalRount = 1;
                        entrole.ApprovalStatus = null;
                        entrole.PhaseID = ((int)arm["PhaseID"] == null) ? 0 : (int)arm["PhaseID"];
                        entrole.StepId = ((int)arm["StepId"] == null) ? 0 : (int)arm["StepId"];
                        entrole.VersionID = ((int)arm["VersionID"] == null) ? 0 : (int)arm["VersionID"];
                        entrole.StepRoleID = ((int)arm["StepRoleID"] == null) ? 0 : (int)arm["StepRoleID"];
                        listTaskMembers.Add(entrole);
                    }
                }
                else if (arrobjMembers == null)
                {
                    // listTaskMembers = null;
                }
                if (arrobjGlobalMembers != null)
                {
                    foreach (var arm in arrobjGlobalMembers)
                    {
                        ITaskMembers entrole = marcomManager.TaskManager.EntityTaskMembersService();
                        entrole.RoleID = 4;
                        entrole.UserID = (int)arm["Userid"];
                        entrole.ApprovalRount = 1;
                        entrole.ApprovalStatus = null;
                        entrole.PhaseID = ((int)arm["PhaseID"] == null) ? 0 : (int)arm["PhaseID"];
                        entrole.StepId = ((int)arm["StepId"] == null) ? 0 : (int)arm["StepId"];
                        entrole.VersionID = ((int)arm["VersionID"] == null) ? 0 : (int)arm["VersionID"];
                        entrole.StepRoleID = ((int)arm["StepRoleID"] == null) ? 0 : (int)arm["StepRoleID"];
                        listTaskMembers.Add(entrole);
                        entrole = marcomManager.TaskManager.EntityTaskMembersService();
                        entrole.RoleID = (int)arm["Roleid"] == 0 ? (int)arm["EntityRole"] : (int)arm["Roleid"];
                        entrole.UserID = (int)arm["Userid"];
                        listGlobalTaskMembers.Add(entrole);
                    }
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertTaskMembers((int)jobj["ParentId"], (int)jobj["TaskID"], listTaskMembers, listGlobalTaskMembers, (string)jobj["CurrentStepName"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }
        [Route("api/Task/InsertTaskOwner")]
        [HttpPost]
        public ServiceResponse InsertTaskOwner([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {


                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrobjMembers = (JArray)jobj["TaskMembers"];

                IList<ITaskMembers> listTaskMembers = new List<ITaskMembers>();
                IList<ITaskMembers> listGlobalTaskMembers = new List<ITaskMembers>();
                if (arrobjMembers != null)
                {
                    foreach (var arm in arrobjMembers)
                    {
                        ITaskMembers entrole = marcomManager.TaskManager.EntityTaskMembersService();
                        entrole.RoleID = 1;
                        entrole.UserID = (int)arm["Userid"];
                        entrole.ApprovalRount = 1;
                        entrole.ApprovalStatus = null;
                        listTaskMembers.Add(entrole);
                    }
                }
                else if (arrobjMembers == null)
                {
                    // listTaskMembers = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertTaskMembers((int)jobj["ParentId"], (int)jobj["TaskID"], listTaskMembers, listGlobalTaskMembers);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }
        [Route("api/Task/UpdateTaskStatus")]
        [HttpPost]
        public ServiceResponse UpdateTaskStatus([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdateTaskStatus((int)jobj["TaskID"], (int)jobj["Status"], (int)jobj["EntityID"], (int)jobj["Stepid"], (int)jobj["userId"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/UpdateTaskStatusInTaskTab/{TaskID}/{Status}/{EntityID}")]
        [HttpPut]
        public ServiceResponse UpdateTaskStatusInTaskTab(int TaskID, int Status, int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdateTaskStatus(TaskID, Status, EntityID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetOverViewEntityTaskList/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetOverViewEntityTaskList(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetOverViewEntityTaskList(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/updateTaskMemberFlag")]
        [HttpPost]
        public ServiceResponse updateTaskMemberFlag([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.updateTaskMemberFlag((int)jobj["TaskID"], (string)jobj["ColorCode"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/InsertUpdateEntityTaskList")]
        [HttpPost]
        public ServiceResponse InsertUpdateEntityTaskList([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertUpdateEntityTaskList((int)jobj["ID"], (string)jobj["Caption"], (string)jobj["Description"], (int)jobj["sortorder"], (int)jobj["EntityID"], 0);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/UpdateEntityTaskListSortOrder")]
        [HttpPost]
        public ServiceResponse UpdateEntityTaskListSortOrder([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdateEntityTaskListSortOrder((JArray)jobj["TaskListObj"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/InsertUnassignedEntityTaskWithAttachments")]
        [HttpPost]

        public ServiceResponse InsertUnassignedEntityTaskWithAttachments([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {


                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrAttchObj = (JArray)jobj["TaskAttachments"];
                JArray arrFilesObj = (JArray)jobj["TaskFiles"];
                JArray arrobjMembers = null;
                if (jobj["TaskMembers"].HasValues)
                    arrobjMembers = (JArray)jobj["TaskMembers"];
                JArray arrChkList = (JArray)jobj["AdminTaskCheckList"];

                IEntityTask taskval = marcomManager.TaskManager.EntityTasksService();
                taskval.Name = (string)jobj["Name"];
                taskval.TaskListID = (int)jobj["TaskListID"];
                taskval.Id = (int)jobj["TaskID"];
                taskval.Description = (string)jobj["Description"];
                taskval.TaskType = (int)jobj["TaskType"];
                taskval.Note = (string)jobj["Note"];
                taskval.EntityTaskListID = 0;
                DateTime startDate = new DateTime();
                if (DateTime.TryParse(jobj["DueDate"].ToString(), out startDate))
                    taskval.DueDate = startDate;
                else
                    taskval.DueDate = null;
                taskval.EntityID = (int)jobj["ParentEntityID"];

                IList<ITaskMembers> listTaskMembers = new List<ITaskMembers>();
                if (arrobjMembers != null)
                {
                    foreach (var arm in arrobjMembers)
                    {
                        ITaskMembers entrole = marcomManager.TaskManager.EntityTaskMembersService();
                        entrole.RoleID = 4;
                        entrole.UserID = (int)arm["Userid"];
                        entrole.ApprovalRount = 1;
                        entrole.ApprovalStatus = null;
                        listTaskMembers.Add(entrole);
                    }
                }
                else if (arrobjMembers == null)
                {
                    //    listTaskMembers = new List<ITaskMember>();
                }


                IList<INewTaskAttachments> listTaskattachment = new List<INewTaskAttachments>();
                if (arrAttchObj != null)
                {
                    foreach (var arm in arrAttchObj)
                    {
                        INewTaskAttachments attachval = marcomManager.TaskManager.TasksAttachmentService();
                        attachval.Name = (string)arm["FileName"] + (string)arm["Extension"];
                        attachval.ActiveFileid = 1;
                        attachval.ActiveVersionNo = 1;
                        attachval.Createdon = DateTime.UtcNow;
                        attachval.Typeid = 4;
                        attachval.Description = (string)arm["FileDescription"];
                        listTaskattachment.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listTaskattachment = null;
                }

                IList<IFile> listFiles = new List<IFile>();
                if (arrFilesObj != null)
                {
                    foreach (var arm in arrFilesObj)
                    {
                        IFile attachval = marcomManager.CommonManager.Fileservice();
                        attachval.Checksum = (string)arm["Checksum"];
                        attachval.CreatedOn = (DateTime)arm["CreatedOn"];
                        attachval.Extension = (string)arm["Extension"];
                        attachval.MimeType = (string)arm["MimeType"];
                        attachval.Moduleid = (int)arm["ModuleID"];
                        attachval.Name = (string)arm["Name"];
                        attachval.Ownerid = (int)arm["OwnerID"];
                        attachval.Size = (long)arm["Size"];
                        attachval.VersionNo = (int)arm["VersionNo"];
                        attachval.strFileID = (string)arm["FileGuid"];
                        attachval.Description = (string)arm["FileDescription"];
                        listFiles.Add(attachval);
                    }
                }
                else if (arrAttchObj == null)
                {
                    listTaskattachment = null;
                }

                JArray taskAttribue = (JArray)jobj["AttributeData"];
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (taskAttribue != null)
                {
                    foreach (var objAttr in taskAttribue)
                    {

                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                            if ((string)objAttr["Value"] != "-1")
                                entityAttr.SpecialValue = Convert.ToString(objAttr["Value"].ToString());
                            else
                                entityAttr.SpecialValue = "";
                        }
                        else
                        {
                            if (entityAttr.ID != (int)SystemDefinedAttributes.CreationDate)
                            {
                                if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                {
                                    entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Date)
                                {
                                    entityAttr.Value = (objAttr["NodeID"].ToString());//Newtonsoft.Json.JsonConvert.DeserializeObject<string>(obj["Value"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Integer)
                                {
                                    entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                }
                                else
                                    entityAttr.Value = objAttr["NodeID"].ToString();
                            }
                            else
                            {
                                entityAttr.Value = DateTime.UtcNow.ToString("yyyy-MM-dd");
                            }
                        }
                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (taskAttribue == null)
                {
                    listattributevalues = null;
                }
                IList<IEntityTaskCheckList> AdminTaskChkLst = new List<IEntityTaskCheckList>();


                if (arrChkList != null)
                {
                    foreach (var arm in arrChkList)
                    {
                        if ((string)arm["Name"] != null)
                        {
                            IEntityTaskCheckList adminChklist = marcomManager.TaskManager.EntityTaskCheckListService();
                            adminChklist.Name = (string)arm["Name"];
                            adminChklist.SortOrder = 1;
                            AdminTaskChkLst.Add(adminChklist);
                        }
                    }
                }
                else if (arrChkList == null)
                {
                    AdminTaskChkLst = null;
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertUnassignedEntityTaskWithAttachments((int)jobj["ParentEntityID"], (int)jobj["TaskType"], (int)jobj["Typeid"], (string)jobj["Name"], taskval, listTaskattachment, listFiles, listTaskMembers, listattributevalues, AdminTaskChkLst, arrAttchObj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }
        [Route("api/Task/DuplicateEntityTaskList")]
        [HttpPut]
        public ServiceResponse DuplicateEntityTaskList([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DuplicateEntityTaskList((int)jobj["TaskListID"], (int)jobj["EntityID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetMytasks")]
        [HttpPost]
        public ServiceResponse GetMytasks([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                int[] FilterStatusID = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["FilterStatusID"].ToString());
                int[] CustomFilterID = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["CustomFilterID"].ToString());
                //List<int> myDeserializedObjList = (List<int>)Newtonsoft.Json.JsonConvert.DeserializeObject(jobj["FilterStatusID"].ToString(), typeof(List<int>));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetMytasks((int)jobj["FilterByentityID"], FilterStatusID, (int)jobj["PageNo"], (int)jobj["AssignRole"],CustomFilterID);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/DeleteTaskEntityListID/{TaskListID}/{EntityID}")]
        [HttpDelete]
        public ServiceResponse DeleteTaskEntityListID(int TaskListID, int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteEntityTaskLis(TaskListID, EntityID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/DuplicateEntityTask/")]
        [HttpPut]
        public ServiceResponse DuplicateEntityTask([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DuplicateEntityTask((int)jobj["TaskID"], (int)jobj["EntityID"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/DeleteEntityTask/{TaskID}/{EntityID}")]
        [HttpDelete]
        public ServiceResponse DeleteEntityTask(int TaskID, int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteEntityTask(TaskID, EntityID);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/InsertTaskLibImport")]
        [HttpPost]
        public ServiceResponse InsertTaskLibImport([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertTaskLibImport((string)jobj["FileImport"], (int)jobj["LangTypeId"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/CompleteUnnassignedEntityTask/")]
        [HttpPost]
        public ServiceResponse CompleteUnnassignedEntityTask([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.CompleteUnnassignedEntityTask((int)jobj["TaskID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/UpdatetasktoNotApplicableandUnassigned/{TaskID}")]
        [HttpPut]
        public ServiceResponse UpdatetasktoNotApplicableandUnassigned(int TaskID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdatetasktoNotApplicableandUnassigned(TaskID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/DeleteAdminTask/{TaskID}")]
        [HttpDelete]
        public ServiceResponse DeleteAdminTask(int TaskID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteAdminTask(TaskID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/DeleteTemplateConditionById/{TemplateCondID}")]
        [HttpDelete]
        public ServiceResponse DeleteTemplateConditionById(int TemplateCondID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteTemplateConditionById(TemplateCondID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/DeleteAdminTemplateTaskRelationById/{TaskListID}/{TemplateID}")]
        [HttpDelete]
        public ServiceResponse DeleteAdminTemplateTaskRelationById(int TaskListID, int TemplateID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteAdminTemplateTaskRelationById(TaskListID, TemplateID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/DeleteTaskMemberById/{ID}/{TaskID}")]
        [HttpDelete]
        public ServiceResponse DeleteTaskMemberById(int ID, int TaskID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteTaskMemberById(ID, TaskID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/CopyFileFromTaskToEntityAttachments/{ActivFileID}/{EntityID}")]
        [HttpGet]
        public ServiceResponse CopyFileFromTaskToEntityAttachments(int ActivFileID, int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.CopyFileFromTaskToEntityAttachments(EntityID, ActivFileID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/UpdatetaskAttachmentDescription")]
        [HttpPut]
        public ServiceResponse UpdatetaskAttachmentDescription([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdatetaskAttachmentDescription((int)jobj["FileID"], (string)jobj["FileName"], (string)jobj["Description"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/UpdatetaskLinkDescription")]
        [HttpPut]
        public ServiceResponse UpdatetaskLinkDescription([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdatetaskLinkDescription((int)jobj["FileID"], (string)jobj["FileName"], (string)jobj["Description"], (string)jobj["URL"], (int)jobj["LinkType"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/DeleteFileByID/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteFileByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteFileByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/InsertLink")]
        [HttpPost]
        public ServiceResponse InsertLink([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertLink((int)jobj["EntityID"], (string)jobj["Name"], (string)jobj["URL"], (int)jobj["LinkType"], (string)jobj["Description"], (int)jobj["ActiveVersionNo"], (int)jobj["TypeID"], (string)jobj["CreatedOn"], (int)jobj["OwnerID"], (int)jobj["ModuleID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/InsertLinkInAdminTasks")]
        [HttpPost]
        public ServiceResponse InsertLinkInAdminTasks([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertLinkInAdminTasks((int)jobj["EntityID"], (string)jobj["Name"], (string)jobj["URL"], (int)jobj["LinkType"], (string)jobj["Description"], (int)jobj["ActiveVersionNo"], (int)jobj["TypeID"], (string)jobj["CreatedOn"], (int)jobj["OwnerID"], (int)jobj["ModuleID"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/SendReminderNotification/{TaskMemberID}/{TaskID}")]
        [HttpPost]
        public ServiceResponse SendReminderNotification(int TaskMemberID, int TaskID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.SendReminderNotification(TaskMemberID, TaskID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/DeleteLinkByID/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteLinkByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteLinkByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/UpdatetaskEntityTaskDetails")]
        [HttpPost]
        public ServiceResponse UpdatetaskEntityTaskDetails([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdatetaskEntityTaskDetails((int)jobj["TaskID"], (string)jobj["TaskName"], (string)jobj["Description"], (string)jobj["Note"], (string)jobj["DueDate"], (string)jobj["actionfor"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/UpdatetaskEntityTaskDetailsInTaskTab")]
        [HttpPost]
        public ServiceResponse UpdatetaskEntityTaskDetailsInTaskTab([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdatetaskEntityTaskDetails((int)jobj["TaskID"], (string)jobj["TaskName"], (string)jobj["Description"], (string)jobj["Note"], (string)jobj["DueDate"], (string)jobj["actionfor"], (int)jobj["entityID"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/UpdatetaskEntityTaskDueDate")]
        [HttpPost]
        public ServiceResponse UpdatetaskEntityTaskDueDate([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdatetaskEntityTaskDueDate((int)jobj["TaskID"], (string)jobj["DueDate"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetEntityTaskDetails/{EntityTaskID}")]
        [HttpGet]
        public ServiceResponse GetEntityTaskDetails(int EntityTaskID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetEntityTaskDetails(EntityTaskID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/UpdateTaskSortOrder/{TaskId}/{TaskListID}/{SortOrderID}")]
        [HttpPut]
        public ServiceResponse UpdateTaskSortOrder(int TaskId, int TaskListID, int SortOrderID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdateTaskSortOrder(TaskId, TaskListID, SortOrderID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/UpdateEntityTaskSortOrder")]
        [HttpPost]
        public ServiceResponse UpdateEntityTaskSortOrder([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdateEntityTaskSortOrder((JArray)jobj["TaskListObj"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/DeleteAdminTaskCheckListByID/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteAdminTaskCheckListByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteAdminTaskCheckListByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/getTaskchecklist/{TaskID}")]
        [HttpGet]
        public ServiceResponse getTaskchecklist(int TaskID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.getTaskchecklist(TaskID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/InsertTaskCheckList")]
        [HttpPost]
        public ServiceResponse InsertTaskCheckList([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertTaskCheckList((int)jobj["TaskID"], (string)jobj["CheckListName"], (int)jobj["SortOrder"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/ChecksTaskCheckList/{ID}/{Status}")]
        [HttpPut]
        public ServiceResponse ChecksTaskCheckList(int ID, bool Status)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.ChecksTaskCheckList(ID, Status);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/DeleteEntityCheckListByID/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteEntityCheckListByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteEntityCheckListByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetSublevelTaskList/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetSublevelTaskList(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetSublevelTaskList(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetEntityUpcomingTaskList/{FilterByentityID}/{FilterStatusID}/{EntityID}/{IsChildren}")]
        [HttpGet]
        public ServiceResponse GetEntityUpcomingTaskList(int FilterByentityID, int FilterStatusID, int EntityID, bool IsChildren)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetEntityUpcomingTaskList(FilterByentityID, FilterStatusID, EntityID, IsChildren);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetFilesandLinksByEntityID/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetFilesandLinksByEntityID(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.CommonManager.GetFilesandLinksByEntityID(EntityID);
            }
            catch
            {
                return null;
            }
            return result;
        }
        [Route("api/Task/CopyAttachmentsfromtask")]
        [HttpPut]
        public ServiceResponse CopyAttachmentsfromtask([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrFilesObj = (JArray)jobj["ActivFileIDs"];
                int[] fileIdList = arrFilesObj.Select(jv => (int)jv).ToArray();

                JArray arrlinksObj = (JArray)jobj["linkIDs"];
                int[] fileIdList1 = arrlinksObj.Select(jv => (int)jv).ToArray();

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.CopyAttachmentsfromtask(fileIdList, (int)jobj["EntityID"], fileIdList1);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/CopytoGeneralAttachments")]
        [HttpPut]
        public ServiceResponse CopytoGeneralAttachments([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.copytogeneralattachment((int)jobj["ActivFileIDs"], (int)jobj["EntityID"], (int)jobj["linkIDs"]);
            }
            catch
            {
                return null;
            }
            return result;
        }
        [Route("api/Task/CopyAttachmentsfromtaskToExistingTasks")]
        [HttpPut]
        public ServiceResponse CopyAttachmentsfromtaskToExistingTasks([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrTasksObj = (JArray)jobj["TaskIdsArr"];
                int[] TaskIDList = arrTasksObj.Select(jv => (int)jv["TaskID"]).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.CopyAttachmentsfromtaskToExistingTasks(TaskIDList, (int)jobj["FileID"], (string)jobj["FileType"]);
            }
            catch
            {
                return null;
            }
            return result;
        }
        [Route("api/Task/EnableDisableTaskflag/{status}")]
        [HttpPost]
        public ServiceResponse EnableDisableTaskflag(bool status)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.EnableDisable(status);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/gettaskflagstatus")]
        [HttpGet]
        public ServiceResponse gettaskflagstatus()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.gettaskflagstatus();
            }
            catch
            {
                return null;
            }
            return result;
        }
        [Route("api/Task/DeleteTaskFileByid/{ID}/{EntityId}")]
        [HttpDelete]
        public ServiceResponse DeleteTaskFileByid(int ID, int EntityId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteTaskFileByid(ID, EntityId);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/DeleteTaskLinkByid/{ID}/{EntityId}")]
        [HttpDelete]
        public ServiceResponse DeleteTaskLinkByid(int ID, int EntityId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteTaskLinkByid(ID, EntityId);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = false;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/DeleteAdminTaskLinkByID/{ID}")]
        [HttpDelete]
        public ServiceResponse DeleteAdminTaskLinkByID(int ID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteAdminTaskLinkByID(ID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/InsertUpdateEntityTaskCheckList")]
        [HttpPost]
        public ServiceResponse InsertUpdateEntityTaskCheckList([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertUpdateEntityTaskCheckList((int)jobj["Id"], (int)jobj["taskId"], (string)jobj["CheckListName"], (bool)jobj["ChkListStatus"], (bool)jobj["ISowner"], (int)jobj["SortOrder"], (bool)jobj["isnew"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }
        [Route("api/Task/InsertUpdateEntityTaskCheckListInTask")]
        [HttpPost]
        public ServiceResponse InsertUpdateEntityTaskCheckListInTask([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertUpdateEntityTaskCheckList((int)jobj["Id"], (int)jobj["taskId"], (string)jobj["CheckListName"], (bool)jobj["ChkListStatus"], (bool)jobj["ISowner"], (int)jobj["SortOrder"], (bool)jobj["isnew"], (int)jobj["EntityID"]);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/GetEntityTaskListWithoutTasks/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetEntityTaskListWithoutTasks(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetEntityTaskListWithoutTasks(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetEntityTaskListDetails/{EntityID}/{TaskListID}")]
        [HttpGet]
        public ServiceResponse GetEntityTaskListDetails(int EntityID, int TaskListID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetEntityTaskListDetails(EntityID, TaskListID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/GettaskCountByStatus")]
        [HttpPut]
        public ServiceResponse GettaskCountByStatus([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrStatusObj = (JArray)jobj["Status"];
                int[] statusIdList = arrStatusObj.Select(jv => (int)jv).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GettaskCountByStatus((int)jobj["TaskListID"], (int)jobj["EntityID"], statusIdList);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/InsertUnAssignedTaskMembers")]
        [HttpPost]

        public ServiceResponse InsertUnAssignedTaskMembers([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {


                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrobjMembers = (JArray)jobj["TaskMembers"];
                JArray arrobjGlobalMembers = (JArray)jobj["GlobalTaskMembers"];
                JArray arrobjOwner = null;
                if (jobj["ownerlist"].Count() > 0)
                {
                    arrobjOwner = (JArray)jobj["ownerlist"];
                }

                ITaskMembers entOwnerrole = marcomManager.TaskManager.EntityTaskMembersService();
                IList<ITaskMembers> listTaskMembers = new List<ITaskMembers>();
                IList<ITaskMembers> listGlobalTaskMembers = new List<ITaskMembers>();
                if (arrobjMembers != null)
                {
                    foreach (var arm in arrobjMembers)
                    {
                        ITaskMembers entrole = marcomManager.TaskManager.EntityTaskMembersService();
                        entrole.RoleID = 4;
                        entrole.UserID = (int)arm["Userid"];
                        entrole.ApprovalRount = 1;
                        entrole.ApprovalStatus = null;
                        listTaskMembers.Add(entrole);
                    }
                }
                else if (arrobjMembers == null)
                {
                    // listTaskMembers = null;
                }
                if (arrobjOwner != null && arrobjOwner.Count > 0)
                {
                    entOwnerrole = marcomManager.TaskManager.EntityTaskMembersService();
                    entOwnerrole.RoleID = 1;
                    entOwnerrole.UserID = (int)arrobjOwner[0]["Userid"];
                    entOwnerrole.ApprovalRount = 1;
                    entOwnerrole.ApprovalStatus = null;
                    //listTaskMembers.Add(entrole);

                }
                else
                {
                    entOwnerrole = null;
                }

                if (arrobjGlobalMembers != null)
                {
                    foreach (var arm in arrobjGlobalMembers)
                    {
                        ITaskMembers entrole = marcomManager.TaskManager.EntityTaskMembersService();
                        entrole.RoleID = 4;
                        entrole.UserID = (int)arm["Userid"];
                        entrole.ApprovalRount = 1;
                        entrole.ApprovalStatus = null;
                        listTaskMembers.Add(entrole);
                        entrole = marcomManager.TaskManager.EntityTaskMembersService();
                        entrole.RoleID = (int)arm["Roleid"];
                        entrole.UserID = (int)arm["Userid"];
                        listGlobalTaskMembers.Add(entrole);
                    }
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertUnAssignedTaskMembers((int)jobj["ParentId"], (int)jobj["TaskID"], listTaskMembers, entOwnerrole, listGlobalTaskMembers);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;

        }
        [Route("api/Task/UpdatetaskAdminLinkDescription")]
        [HttpPut]
        public ServiceResponse UpdatetaskAdminLinkDescription([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdatetaskAdminLinkDescription((int)jobj["LinkID"], (string)jobj["LinkName"], (string)jobj["Description"], (string)jobj["URL"], (int)jobj["LinkType"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/UpdatetaskAdminAttachmentDescription")]
        [HttpPut]
        public ServiceResponse UpdatetaskAdminAttachmentDescription([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdatetaskAdminAttachmentDescription((int)jobj["FileID"], (string)jobj["FileName"], (string)jobj["Description"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetExistingEntityTasksByEntityID/{EntityID}")]
        [HttpGet]
        public ServiceResponse GetExistingEntityTasksByEntityID(int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetExistingEntityTasksByEntityID(EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetMyFundingRequests")]
        [HttpPost]
        public ServiceResponse GetMyFundingRequests([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                int[] FilterStatusID = Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(jobj["FilterStatusID"].ToString());
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetMyFundingRequests(FilterStatusID, (int)jobj["AssignRole"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/FetchUnassignedTaskforReassign/{EntityID}/{TaskListID}")]
        [HttpGet]
        public ServiceResponse FetchUnassignedTaskforReassign(int EntityID, int TaskListID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.FetchUnassignedTaskforReassign(EntityID, TaskListID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/AssignUnassignTasktoMembers")]
        [HttpPost]
        public ServiceResponse AssignUnassignTasktoMembers([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrobjMembers = (JArray)jobj["TaskMembers"];
                JArray arrobjtasks = (JArray)jobj["Tasks"];
                int[] taskmemberArr = arrobjMembers.Select(jv => (int)jv["id"]).ToArray();
                int[] taskList = arrobjtasks.Select(jv => (int)jv["TaskID"]).ToArray();
                DateTime? duedate = new DateTime();
                if ((string)jobj["DueDate"] == "")
                    duedate = null;
                else
                    duedate = DateTime.Parse(jobj["DueDate"].ToString());

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.AssignUnassignTasktoMembers((int)jobj["EntityID"], (int)jobj["TaskListID"], taskList, taskmemberArr, duedate);
            }
            catch (MarcomAccessDeniedException ex)
            {
                result.StatusCode = (int)HttpStatusCode.Unauthorized;
                result.Response = 0;
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/GetAdminTasksByTaskListID/{TaskListID}")]
        [HttpGet]
        public ServiceResponse GetAdminTasksByTaskListID(string TaskListID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                string[] strIds = { TaskListID };
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetAdminTasksById(strIds);
            }

            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/GetAdminTasksById")]
        [HttpPost]
        public ServiceResponse GetAdminTasksById([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrobjTasksList = (JArray)jobj["libraryObj"];
                string[] taskList = arrobjTasksList.Select(jv => (string)jv["ID"]).ToArray();
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetAdminTasksById(taskList);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/getAdminTaskchecklist/{TaskID}")]
        [HttpGet]
        public ServiceResponse getAdminTaskchecklist(int TaskID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.getAdminTaskchecklist(TaskID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetAdminTaskMetadatabyTaskID/{TaskID}")]
        [HttpGet]
        public ServiceResponse GetAdminTaskMetadatabyTaskID(int TaskID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetAdminTaskMetadatabyTaskID(TaskID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetTaskTemplateConditionByTaskTempId/{TaskTempID}")]
        [HttpGet]
        public ServiceResponse GetTaskTemplateConditionByTaskTempId(int TaskTempID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetTaskTemplateConditionByTaskTempId(TaskTempID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetFiltertaskCountByStatus")]
        [HttpPost]
        public ServiceResponse GetFiltertaskCountByStatus([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray arrStatusObj = (JArray)jobj["Status"];
                int[] statusIdList = arrStatusObj.Select(jv => (int)jv).ToArray();
                JArray arrparentEntObj = (JArray)jobj["TaskObject"];
                Dictionary<int, int> taskDict = new Dictionary<int, int>();
                foreach (var val in arrparentEntObj)
                {
                    taskDict.Add((int)val["TaskListID"], (int)val["EntityID"]);
                }
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetFiltertaskCountByStatus(taskDict, statusIdList);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetTemplateAdminTaskList")]
        [HttpGet]
        public ServiceResponse GetTemplateAdminTaskList()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetTemplateAdminTaskList();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Task/UpdateEntityTask")]
        [HttpPost]
        public ServiceResponse UpdateEntityTask([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdateEntityTask(jobj["Parameter"]["Id"], jobj["Parameter"]["tasklistid"], jobj["Parameter"]["entityid"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetSourceToDestinationmembers/{taskID}/{sourceEntityID}/{destinationEntityId}")]
        [HttpGet]
        public ServiceResponse GetSourceToDestinationmembers(int taskID, int sourceEntityID, int destinationEntityId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetSourceToDestinationmembers(taskID, sourceEntityID, destinationEntityId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Task/GetDestinationEntityIdRoleAccess/{destinationEntityId}")]
        [HttpGet]
        public ServiceResponse GetDestinationEntityIdRoleAccess(int destinationEntityId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetDestinationEntityIdRoleAccess(destinationEntityId);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = null;
            }
            return result;
        }
        [Route("api/Task/InsertUpdateDragTaskMembers")]
        [HttpPost]
        public ServiceResponse InsertUpdateDragTaskMembers([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray jMarr = (JArray)jobj["Matchedmembers"];
                JArray jUnarr = (JArray)jobj["UnMatchedmembers"];
                IList<BrandSystems.Marcom.Core.Task.SourceDestinationMember> SourceMembers = new List<BrandSystems.Marcom.Core.Task.SourceDestinationMember>();
                if (jMarr != null)
                {
                    foreach (var item in jMarr)
                    {
                        BrandSystems.Marcom.Core.Task.SourceDestinationMember currentobj = new BrandSystems.Marcom.Core.Task.SourceDestinationMember();
                        currentobj.Id = (int)item["Id"];
                        currentobj.ActualRoleId = (int)item["ActualRoleId"];
                        currentobj.EntityID = (int)item["EntityID"];
                        currentobj.IstaskOwner = (bool)item["IstaskOwner"];
                        currentobj.RoleName = (string)item["RoleName"];
                        currentobj.Status = (bool)item["Status"];
                        currentobj.UserID = (int)item["UserID"];
                        currentobj.UserName = (string)item["UserName"];
                        currentobj.RoleID = (int)item["RoleID"];
                        SourceMembers.Add(currentobj);

                    }
                }


                IList<BrandSystems.Marcom.Core.Task.SourceDestinationMember> TargetMembers = new List<BrandSystems.Marcom.Core.Task.SourceDestinationMember>();
                if (jUnarr != null)
                {
                    foreach (var item in jUnarr)
                    {
                        BrandSystems.Marcom.Core.Task.SourceDestinationMember currentobj = new BrandSystems.Marcom.Core.Task.SourceDestinationMember();
                        currentobj.Id = (int)item["Id"];
                        currentobj.ActualRoleId = (int)item["ActualRoleId"];
                        currentobj.EntityID = (int)item["EntityID"];
                        currentobj.IstaskOwner = (bool)item["IstaskOwner"];
                        currentobj.RoleName = (string)item["RoleName"];
                        currentobj.Status = (bool)item["Status"];
                        currentobj.UserID = (int)item["UserID"];
                        currentobj.UserName = (string)item["UserName"];
                        currentobj.RoleID = (int)item["RoleID"];
                        TargetMembers.Add(currentobj);

                    }
                }

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertUpdateDragTaskMembers(SourceMembers, TargetMembers, (int)jobj["SourceTaskId"], (int)jobj["TargetTaskListID"], (int)jobj["TargetEntityID"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/UpdateDragEntityTaskListByTask")]
        [HttpPost]
        public ServiceResponse UpdateDragEntityTaskListByTask([FromBody] JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdateDragEntityTaskListByTask((int)jobj["TaskID"], (int)jobj["SrcTaskListID"], (int)jobj["TargetTaskListID"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetEntityAttributesDetails/{TaskID}")]
        [HttpGet]
        public ServiceResponse GetEntityAttributesDetails(int TaskID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetEntityAttributesDetails(TaskID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetEntityTaskAttachmentinfo/{TaskID}")]
        [HttpGet]
        public ServiceResponse GetEntityTaskAttachmentinfo(int TaskID)
        {
            result = new ServiceResponse();
            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetEntityTaskAttachmentinfo(TaskID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/GetEntityTaskCollection/{EntityID}/{TaskListID}/{pageno}")]
        [HttpGet]
        public ServiceResponse GetEntityTaskCollection(int EntityID, int TaskListID, int pageno)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetEntityTaskCollection(EntityID, TaskListID, pageno);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/InsertUpdateTaskList1")]
        [HttpPost]
        public ServiceResponse InsertUpdateTaskList1([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {


                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertUpdateTaskList((int)jobj["ID"], (string)jobj["Caption"], (string)jobj["Description"], (int)jobj["sortorder"]);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/ExportTaskList/{TaskListID}/{NewGuid}/{TaskLibName}")]
        [HttpGet]
        public ServiceResponse ExportTaskList(int TaskListID, string NewGuid, string TaskLibName)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.ExportTaskList(TaskListID, NewGuid, TaskLibName);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/GetAllApprovalFlowTemplates")]
        [HttpGet]
        public ServiceResponse GetAllApprovalFlowTemplates()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetAllApprovalFlowTemplates();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/GetAllApprovalFlowPhases/{templateid}")]
        [HttpGet]
        public ServiceResponse GetAllApprovalFlowPhases(int templateid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetAllApprovalFlowPhases(templateid);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/GetAllApprovalFlowPhasesSteps/{templateid}")]
        [HttpGet]
        public ServiceResponse GetAllApprovalFlowPhasesSteps(int templateid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetAllApprovalFlowPhasesSteps(templateid);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/InsertUpdateTaskTemplate")]
        [HttpPost]
        public ServiceResponse InsertUpdateTaskTemplate([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertUpdateTaskApprovalTemplate(jobj);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/DeleteApprovalTemplate/{TempID}")]
        [HttpDelete]
        public ServiceResponse DeleteApprovalTemplate(int TempID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));

                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteApprovalTemplate(TempID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/InsertUpdateTaskTemplateStep")]
        [HttpPost]
        public ServiceResponse InsertUpdateTaskTemplateStep([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertUpdateTaskTemplateStep(jobj);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/InsertUpdateTaskTemplatePhase")]
        [HttpPost]
        public ServiceResponse InsertUpdateTaskTemplatePhase([FromBody]JArray jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertUpdateTaskTemplatePhase(jobj);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/UpdatetaskTemplateSortOrder")]
        [HttpPost]
        public ServiceResponse UpdatetaskTemplateSortOrder([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdatetaskTemplateSortOrder(jobj);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/UpdateTemplatePhaseSortOrder")]
        [HttpPost]
        public ServiceResponse UpdateTemplatePhaseSortOrder([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdateTemplatePhaseSortOrder(jobj);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/DeleteApprovalTaskStep/{Stepid}")]
        [HttpDelete]
        public ServiceResponse DeleteApprovalTaskStep(int Stepid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteApprovalTaskStep(Stepid);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/HighResPdfDownload/{HiResPdf}")]
        [HttpGet]
        public ServiceResponse HighResPdfDownload(string HiResPdf)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.HighResPdfDownload(HiResPdf);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/getAllTemplateTask")]
        [HttpGet]
        public ServiceResponse getAllTemplateTask()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.getAllTemplateTask();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/InsertUpdateAdminTaskTemplate")]
        [HttpPost]
        public ServiceResponse InsertUpdateAdminTaskTemplate([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                JArray PhaseStepDetails = (JArray)jobj["PhaseStepDetails"];
                int EntityID = (int)jobj["EntityID"];
                result.StatusCode = (int)HttpStatusCode.OK;
                foreach (var i in PhaseStepDetails)
                {
                    result.Response = marcomManager.TaskManager.InsertUpdateAdminTaskTemplate((JObject)i, EntityID);
                }

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/DeleteAdminApprovalTaskStep/{Stepid}")]
        [HttpDelete]
        public ServiceResponse DeleteAdminApprovalTaskStep(int Stepid)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeleteAdminApprovalTaskStep(Stepid);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/GetAllAdminApprovalFlowPhasesSteps/{EntityId}")]
        [HttpGet]
        public ServiceResponse GetAllAdminApprovalFlowPhasesSteps(int EntityId)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetAllAdminApprovalFlowPhasesSteps(EntityId);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }
        [Route("api/Task/UpdateAdminTasklistSortOrder")]
        [HttpPost]
        public ServiceResponse UpdateAdminTasklistSortOrder([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdateAdminTasklistSortOrder(jobj);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/UpdateAdmintaskPhaseSortOrder")]
        [HttpPost]
        public ServiceResponse UpdateAdmintaskPhaseSortOrder([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdateAdmintaskPhaseSortOrder(jobj);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }
        [Route("api/Task/InsertUpdateVersion")]
        [HttpPost]
        public ServiceResponse InsertUpdateVersion([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.InsertUpdateVersion(jobj);

            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Task/GetTakUIData/{taskId}/{versionNo}")]
        [HttpGet]
        public ServiceResponse GetTakUIData(int taskId, int versionNo = 0)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetTaskDetails(taskId, versionNo);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/CreateNewVersion")]
        [HttpPost]
        public ServiceResponse CreateNewVersion([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.CreateNewVersion((int)jobj["taskid"], (int)jobj["versionNo"], (JObject)jobj["FileDetails"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/GetTaskDetails")]
        [HttpPost]
        public ServiceResponse GetTaskDetails([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetTaskDetails((int)jobj["taskid"], (int)jobj["versionNo"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/GetListOfDocumentWorkFlowFromDalim")]
        [HttpGet]
        public ServiceResponse GetListOfDocumentWorkFlowFromDalim()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetListOfDocumentWorkFlowFromDalim();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = "";
            }
            return result;
        }

        [Route("api/Task/AddNewPhase")]
        [HttpPost]
        public ServiceResponse AddNewPhase([FromBody]JObject jObj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.AddNewPhase(jObj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/RemovePhase/{PhaseID}/{EntityID}")]
        [HttpDelete]
        public ServiceResponse RemovePhase(int PhaseID, int EntityID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.RemovePhase(PhaseID, EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/AddPhaseStep/")]
        [HttpPost]
        public ServiceResponse AddPhaseStep([FromBody]JObject jObj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.AddPhaseStep(jObj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Task/AddMembersToPhaseStep")]
        [HttpPost]
        public ServiceResponse AddMembersToPhaseStep([FromBody]JObject jObj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.AddMembersToPhaseStep(jObj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/DeletePhaseStep/{stepID}/{EntityID}")]
        [HttpDelete]
        public ServiceResponse DeletePhaseStep(int stepID, int EntityID)
        {
            result = new ServiceResponse();

            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.DeletePhaseStep(stepID, EntityID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/RemovePhaseStepApprovar/")]
        [HttpPost]
        public ServiceResponse RemovePhaseStepApprovar([FromBody]JObject jObj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.RemovePhaseStepApprovar(jObj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/sendReminderNotificationforTaskPhase/")]
        [HttpPost]
        public ServiceResponse sendReminderNotificationforTaskPhase([FromBody]JObject jObj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.sendReminderNotificationforTaskPhase(jObj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/getAllMemberIDbyStepID/{stepID}")]
        [HttpGet]
        public ServiceResponse getAllMemberIDbyStepID(int stepID)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.getAllMemberIDbyStepID(stepID);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/GetListOfWorkFlowFromDalim")]
        [HttpPost]
        public ServiceResponse GetListOfWorkFlowFromDalim([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetListOfWorkFlowFromDalim((int)jobj["Id"], (string)jobj["workflowname"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = 0;
            }
            return result;
        }

        [Route("api/Task/GetDalimUserFromDB/{username}")]
        [HttpGet]
        public ServiceResponse GetDalimUserFromDB(string username)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetDalimUserFromDB(username);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        #region viniston's methods

        [Route("api/Task/GetApprovalFlowLibraryList/{librarytypeid}/{IsAdminSide}")]
        [HttpGet]
        public ServiceResponse GetApprovalFlowLibraryList(int librarytypeid, bool IsAdminSide)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.GetApprovalFlowLibraryList(librarytypeid, IsAdminSide);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = "";
            }
            return result;
        }

        [Route("api/Task/CollectApprovalFlowSteps")]
        [HttpPost]
        public ServiceResponse CollectApprovalFlowSteps([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.CollectApprovalFlowSteps((int)jobj["Typeid"], (int)jobj["TemplateId"], (string)jobj["TemplateName"]);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = "";
            }
            return result;
        }

        [Route("api/Task/ReorderOverviewStructure")]
        [HttpPost]
        public ServiceResponse ReorderOverviewStructure([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.ReorderOverviewStructure(jobj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = "";
            }
            return result;
        }


        [Route("api/Task/getTaskApprovalRolesUsers")]
        [HttpPost]
        public ServiceResponse getTaskApprovalRolesUsers([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.getTaskApprovalRolesUsers(jobj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = "";
            }
            return result;
        }


        [Route("api/Task/getEntityApprovalRoleUsers")]
        [HttpPost]
        public ServiceResponse getEntityApprovalRoleUsers([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.getEntityApprovalRoleUsers(jobj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = "";
            }
            return result;
        }

        #endregion

        [Route("api/Task/UpdateImageNameforTask")]
        [HttpPost]
        public ServiceResponse UpdateImageNameforTask([FromBody]JObject jobj)
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.UpdateImageNameforTask(jobj);
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

        [Route("api/Task/IsApprovalReportEnables")]
        [HttpGet]
        public ServiceResponse IsApprovalReportEnables()
        {
            result = new ServiceResponse();
            try
            {
                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(Request, Guid.Parse(((string[])(Request.Headers.GetValues("sessioncookie")))[0]));
                result.StatusCode = (int)HttpStatusCode.OK;
                result.Response = marcomManager.TaskManager.IsApprovalReportEnables();
            }
            catch
            {
                result.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
                result.Response = false;
            }
            return result;
        }

    }

}