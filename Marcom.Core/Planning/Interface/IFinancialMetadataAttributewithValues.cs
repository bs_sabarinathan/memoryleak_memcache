﻿using BrandSystems.Marcom.Core.Metadata.Interface;
using System;
using System.Collections;
using System.Collections.Generic;


namespace BrandSystems.Marcom.Core.Planning.Interface
{
    public interface IFinancialMetadataAttributewithValues
    {

        /// <summary>
        /// IFinancialMetadataAttributewithValues interface for table '[MM_Fin_Attribute]'.
        /// </summary>
        #region Public Properties

        int ID { get; set; }
        int EntityID { get; set; }
        int CostcenterID { get; set; }
        int FinID { get; set; }
        int FinTypeID { get; set; }
        string Caption { get; set; }
        int AttributeTypeID { get; set; }
        bool IsSystemDefined { get; set; }
        string Description { get; set; }
        bool IsColumn { get; set; }
        bool IsTooltip { get; set; }
        bool IsCommitTooltip { get; set; }
        int SortOrder { get; set; }
        IList<IFinancialOption> Options { get; set; }
        dynamic values { get; set; }
        string ValueCaption { get; set; }
        bool IsHelptextEnabled { get; set; }
        string HelptextDecsription { get; set; }
        #endregion
    }
}
