﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IFinancial interface for table 'PM_FinancialForecastSettings'.
    /// </summary>
    public interface IFinancialForecastSettings
    {
        #region Public Properties


        int ID
        {
            get;
            set;

        }

        bool IsFinancialForecast
        {
            get;
            set;

        }

        int ForecastDivision
        {
            get;
            set;

        }

        int ForecastBasis
        {
            get;
            set;

        }

        int ForecastLevel
        {
            get;
            set;

        }

        int ForecastDeadlines
        {
            get;
            set;

        }

   
        #endregion

    }
}
