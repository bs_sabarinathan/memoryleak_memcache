﻿using BrandSystems.Marcom.Dal.Task.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Task.Interface
{
    public interface IEntitytaskphases
    {
        int ID { get; set; }
        string TempID { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        string ColorCode { get; set; }
        int EntityID { get; set; }
        int SortOrder { get; set; }
        int Status { get; set; }
        bool actionPermit { get; set; }


        IList<EntitytaskstepsDao> StepsData { get; set; }
    }
}
