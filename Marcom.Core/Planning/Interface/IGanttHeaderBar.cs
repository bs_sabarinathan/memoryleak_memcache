using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IGanttHeaderBar interface for table 'PM_GanttHeaderBar'.
    /// </summary>
    public interface IGanttHeaderBar
    {
        #region Public Properties

        int Id
        {
            get;
            set;

        }

        DateTime StartDate
        {
            get;
            set;

        }

        DateTime EndDate
        {
            get;
            set;

        }

        string ColorCode
        {
            get;
            set;

        }

        string Description
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
