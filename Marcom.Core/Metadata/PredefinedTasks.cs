﻿/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

    /// <summary>
    /// Attribute object for table 'MM_Attribute'.
    /// </summary>

    internal class PredefinedTasks : IPredefinedTasks
    {
        #region Member Variables

        protected int _id;
        protected string _name;
        protected string _description;
        protected int _workflowtypeid;

        #endregion

        #region Constructors
        public PredefinedTasks() { }

        public PredefinedTasks(string pName, string pDescription)
        {
            this._name = pName;
            this._description = pDescription;
        }

        public PredefinedTasks(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

       

        public string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 50 characters");
                _name = value;
            }

        }

        public string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _description = value;
            }

        }

        public int WorkflowType
        {
            get { return _workflowtypeid; }
            set { _workflowtypeid = value; }
        }
      

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            PredefinedTasks castObj = null;
            try
            {
                castObj = (PredefinedTasks)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
