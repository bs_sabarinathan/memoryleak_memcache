﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task.Interface;

namespace BrandSystems.Marcom.Core.Task
{
    /// <summary>
    /// Entity object for table 'PM_Entity'.
    /// </summary>
    internal class EntityTask : IEntityTask
    {
        #region Member Variables

        protected int _id;
        protected int _EntityID;
        protected int _MemberID;
        protected string _Name;
        protected int _TaskType;
        protected string _Description;
        protected int _TaskStatus;
        protected DateTime? _DueDate;
        protected int _EntityTaskListID;
        protected int _sortOrder { get; set; }
        protected int _TaskListID;
        protected int _assetId;
        protected int? _taskFlowId;
        protected string _note;
        protected int _DalimID;

        #endregion

        #region Constructors
        public EntityTask() { }

        public EntityTask(string pName, string pDescription, int pTaskType, DateTime? pDueDate, int pTaskStatus, int pEntityTaskListID, int pTaskListID, int pSortOrder, int pAssetId, string pNote, int pDalimID, int? ptaskFlowId)
        {

            this._Name = pName;
            this._Description = pDescription;
            this._DueDate = pDueDate;
            this._TaskStatus = pTaskStatus;
            this._EntityTaskListID = pEntityTaskListID;
            this._TaskListID = pTaskListID;
            this._TaskType = pTaskType;
            this._sortOrder = pSortOrder;
            this._assetId = pAssetId;
            this._note = pNote;
            this._DalimID = pDalimID;
            this._taskFlowId = ptaskFlowId;

        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int EntityID
        {
            get { return _EntityID; }
            set { _EntityID = value; }

        }


        public int MemberID
        {
            get { return _MemberID; }
            set { _MemberID = value; }

        }

        public int TaskStatus
        {
            get { return _TaskStatus; }
            set { _TaskStatus = value; }

        }

        public DateTime? DueDate
        {
            get { return _DueDate; }
            set { _DueDate = value; }

        }

        public int TaskType
        {
            get { return _TaskType; }
            set { _TaskType = value; }

        }

        public int EntityTaskListID
        {
            get { return _EntityTaskListID; }
            set { _EntityTaskListID = value; }

        }

        public int TaskListID
        {
            get { return _TaskListID; }
            set { _TaskListID = value; }

        }

        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
            }

        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }

        }

        public string Note
        {
            get { return _note; }
            set { _note = value; }

        }

        public int SortOrder
        {
            get { return _sortOrder; }
            set { _sortOrder = value; }

        }


        public int AssetId
        {
            get { return _assetId; }
            set { _assetId = value; }

        }

        public int DalimID
        {
            get { return _DalimID; }
            set { _DalimID = value; }

        }

        public int? TaskFlowID
        {
            get { return _taskFlowId; }
            set { _taskFlowId = value; }

        }

        public bool IsWorkStart { get; set; }


        public string TaskTypeName { get; set; }
        public int TotalTaskAttachment { get; set; }
        public dynamic AttributeData { get; set; }
        public string StatusName { get; set; }
        public string strDate { get; set; }
        public dynamic taskAssigness { get; set; }
        public dynamic TotaltaskAssigness { get; set; }
        public dynamic taskAttachment { get; set; }
        public string AssigneeName { get; set; }
        public int AssigneeID { get; set; }
        public int totalDueDays { get; set; }
        public string MemberFlagColorCode { get; set; }
        public IList<ITaskMembers> taskMembers { get; set; }
        public string ProgressCount { get; set; }
        public dynamic TaskCheckList { get; set; }
        public int TaskTypeID { get; set; }
        public string colorcode { get; set; }
        public string shortdesc { get; set; }

        public IList<ITaskList> TaskListDetails
        {
            get;
            set;
        }

        public IList<ITasktemplateCondition> TempCondDetails
        {
            get;
            set;
        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityTask castObj = null;
            try
            {
                castObj = (EntityTask)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }

    public class TaskJsonwrapper
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string Description { get; set; }
        public int TaskStatus { get; set; }
        public int currentVersion { get; set; }
        public string DueDate { get; set; }
        public int EntityID { get; set; }
        public int EntityTaskListID { get; set; }
        public int Sortorder { get; set; }
        public int TaskType { get; set; }
        public string Note { get; set; }
        public DateTime CreatedOn { get; set; }
        public int TaskListID { get; set; }
        public int AssetId { get; set; }
        public string TypeDetails { get; set; }
        public string TotaTaskAssignees { get; set; }
        public string TaskPhases { get; set; }
        public string TaskSteps { get; set; }
        public string TaskPath { get; set; }
        public string TaskVersionDetails { get; set; }
        public string CurrentTaskVersionDetails { get; set; }
        public int checklists { get; set; }
        public int completedchecklists { get; set; }
        public int DalimID { get; set; }

    }
}