﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Task.Model
{

    /// <summary>
    /// AttachmentsDao object for table 'PM_Attachments'.
    /// </summary>

    public partial class NewTaskAttachmentsDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected int _entityid;
        protected string _name;
        protected int _activeversionno;
        protected int _activefileid;
        protected string _description;
        protected int _typeid;
        protected DateTime _createdon;
        protected Guid _fileguid;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        #endregion

        #region Constructors
        public NewTaskAttachmentsDao() { }

        public NewTaskAttachmentsDao(int pEntityid, string pName, int pActiveVersionNo, string pDescription, int pActiveFileid, int pTypeid, DateTime pCreatedon)
        {
            this._entityid = pEntityid;
            this._name = pName;
            this._activeversionno = pActiveVersionNo;
            this._activefileid = pActiveFileid;
            this._description = pDescription;
            this._typeid = pTypeid;
            this._createdon = pCreatedon;

        }

        public NewTaskAttachmentsDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int Entityid
        {
            get { return _entityid; }
            set { _bIsChanged |= (_entityid != value); _entityid = value; }

        }

        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 500)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 500 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }

        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
                _bIsChanged |= (_name != value);
                _name = value;
            }

        }

        public virtual int ActiveVersionNo
        {
            get { return _activeversionno; }
            set { _bIsChanged |= (_activeversionno != value); _activeversionno = value; }

        }

        public virtual int ActiveFileid
        {
            get { return _activefileid; }
            set { _bIsChanged |= (_activefileid != value); _activefileid = value; }

        }

        public virtual int Typeid
        {
            get { return _typeid; }
            set { _bIsChanged |= (_typeid != value); _typeid = value; }

        }

        public virtual DateTime Createdon
        {
            get { return _createdon; }
            set { _bIsChanged |= (_createdon != value); _createdon = value; }

        }



        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string NewTaskAttachmentsDao = "NewTaskAttachmentsDao";
            public const string Id = "Id";
            public const string Entityid = "Entityid";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string ActiveVersionNo = "ActiveVersionNo";
            public const string ActiveFileid = "ActiveFileid";
            public const string Typeid = "Typeid";
            public const string Createdon = "Createdon";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string NewTaskAttachmentsDao = "NewTaskAttachmentsDao";
            public const string Id = "Id";
            public const string Entityid = "Entityid";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string ActiveVersionNo = "ActiveVersionNo";
            public const string ActiveFileid = "ActiveFileid";
            public const string Typeid = "Typeid";
            public const string Createdon = "Createdon";
        }

        #endregion


    }

}
