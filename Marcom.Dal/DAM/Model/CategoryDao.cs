﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.DAM.Model
{
   public partial class CategoryDao:BaseDao,ICloneable
    {

        #region "Member variables"

        protected int _id;
        protected string _name;
        protected string _parentkey;
        protected int _level;
        protected int _parentid;

        #endregion

       #region "Constructor"

        public CategoryDao() { }


        public CategoryDao(int pid, string pname, string pparentkey,int plevel,int pparentid) 
        {
            this._id = pid;
            this._name = pname;
            this._parentkey = pparentkey;
            this._level = plevel;
            this._parentid = pparentid;
        }

        #endregion


         #region "Public propertiers"

        public virtual int ID
        {
            get { return _id; }
            set { _id = value; }

        }

       

        public virtual string Name
        {
            get { return _name; }
            set
            {
                _name = value;
            }

        }



        public virtual string ParentKey
        {
            get { return _parentkey; }
            set { _parentkey = value; }

        }

        public virtual int Level
        {
            get { return _level; }
            set { _level = value; }

        }

        public virtual int ParentID
        {
            get { return _parentid; }
            set { _parentid = value; }

        }

        #endregion


        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion


        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string CategoryDao = "CategoryDao";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string ParentKey = "ParentKey";
            public const string Level = "Level";
            public const string ParentID = "ParentID";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string CategoryDao = "CategoryDao";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string ParentKey = "ParentKey";
            public const string Level = "Level";
            public const string ParentID = "ParentID";
        }

        #endregion

    }
}
