﻿using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.User.Interface;
using System.Security.Cryptography;


namespace BrandSystems.Marcom.Core.User
{
    public class EntityUsers : IEntityUsers
    {
        #region Member Variables

		protected bool _isowner;
		protected string _ownername;
		protected string _email;
		#endregion
		
		#region Constructors
		public EntityUsers() {}
			
		public EntityUsers(bool pIsOwner, string pOwnerName, string pEmail)
		{
			this._isowner = pIsOwner; 
			this._ownername = pOwnerName; 
			this._email = pEmail; 
		}
				
		#endregion
		
		#region Public Properties
		
		public bool IsOwner
		{
            get { return _isowner; }
			set { _isowner = value; }
			
		}
		
		public string OwnerName
		{
            get { return _ownername; }
            set { _ownername = value; }
			
		}
		
		public string Email
		{
            get { return _email; }
			set { _email = value; }
			
		}
		
		#endregion 
		
		
    }
}
