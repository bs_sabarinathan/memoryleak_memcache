using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Planning.Interface;

namespace BrandSystems.Marcom.Core.Planning
{

	/// <summary>
	/// Objective object for table 'PM_Calender'.
	/// </summary>
	
	internal class Calenderclass : BaseEntity, ICalender 
	{
		#region Member Variables

        protected int _id;
        protected int _typeid;
        protected string _name;
        protected string _description;
        protected bool? _calenderstatus;
        protected DateTime? _publishedon;
        protected bool _isexternal;
        protected int _visibilityperiod;
        protected int _visibilitytype;
        protected int _ownerid;
        protected DateTime _createdon;
        protected string _publishedate;
		
		#endregion
		
		#region Constructors
		public Calenderclass() {}

        public Calenderclass(int pId, int pTypeid, string pName, string pDescription, bool? pCalenderStatus, bool pIsExternal, int pVisibilityPeriod, int pVisibilityType, int pOwnerId, DateTime pCreatedOn, string ppublisheddate)
        {
            this._id = pId;
            this._typeid = pTypeid;
            this._name = pName;
            this._description = pDescription;
            this._calenderstatus = pCalenderStatus;
            this._isexternal = pIsExternal;
            this._visibilityperiod = pVisibilityPeriod;
            this._visibilitytype = pVisibilityType;
            this._ownerid = pOwnerId;
            this._createdon = pCreatedOn;
            this._publishedate = ppublisheddate;
        }
		
		#endregion
		
		#region Public Properties

		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public int Typeid
		{
			get { return _typeid; }
			set { _typeid = value; }
			
		}
		
		public string Name
		{
			get { return _name; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("Name", "Name value, cannot contain more than 250 characters");
			  _name = value; 
			}
			
		}

        public string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _description = value;
            }

        }
        public virtual bool? CalenderStatus
        {
            get { return _calenderstatus; }
            set { _calenderstatus = value; }
        }

        public virtual DateTime? PublishedOn
        {
            get { return _publishedon; }
            set { _publishedon = value; }
        }

        public virtual bool IsExternal
        {
            get { return _isexternal; }
            set { _isexternal = value; }
        }
        public virtual int Visibilityperiod
        {
            get { return _visibilityperiod; }
            set { _visibilityperiod = value; }
        }
        public virtual int Visibilitytype
        {
            get { return _visibilitytype; }
            set { _visibilitytype = value; }
        }
        public virtual int OwnerID
        {
            get { return _ownerid; }
            set { _ownerid = value; }
        }

        public virtual DateTime CreatedOn
        {
            get { return _createdon; }
            set { _createdon = value; }
        }

        public virtual string PublishedData
        {
            get { return _publishedate; }
            set { _publishedate = value; }
        }
		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			Objective castObj = null;
			try
			{
				castObj = (Objective)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				this._id.Equals( castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
