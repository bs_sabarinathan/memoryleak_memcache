using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
    /// <summary>
    /// IObjectiveConditionValue interface for table 'PM_ObjectiveConditionValue'.
    /// </summary>
    public interface IObjectiveConditionValue
    {
        #region Public Properties

        int Id
        {
            get;
            set;
        }

        int Conditionid
        {
            get;
            set;

        }

        int Value
        {
            get;
            set;

        }

        
        

        #endregion
    }
}
