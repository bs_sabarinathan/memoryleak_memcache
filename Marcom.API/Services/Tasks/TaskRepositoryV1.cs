﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core.Common;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Metadata.Interface;
using Marcom.API.Authorization;
using Marcom.API.RequestHeaderDetails;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Core.Report.Interface;
using Marcom.API.Models;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.Helpers;
using BrandSystems.Marcom.Dal.Base;
using System.Text;
using BrandSystems.Marcom.Core.Task.Interface;



namespace Marcom.API.Services.Tasks
{
    public class TaskRepositoryV1 
    {
       // public IList<IMyTaskCollection> GetMytasks(TaskManagerProxy proxy, int FilterByentityID, int[] FilterStatusID, int StartRowno, int MaxRowNo, int AssignRole)
        public IList<IMyTaskCollection> GetMytasks(int FilterByentityID, int StartRowno, int MaxRowNo, int AssignRole,int UserID)
        {

            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            int[] arr = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            return marcomManager.TaskManager.GetMytasksAPI(FilterByentityID, arr, StartRowno, MaxRowNo, AssignRole, UserID).ToList();
        }
       
        public int ReinitiateRejectedTask(JObject jObj)
        {

            IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);
            return marcomManager.TaskManager.ReinitiateRejectedTask(jObj);
        }

    }
}