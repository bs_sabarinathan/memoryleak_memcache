/*
Created using Microdesk MyGeneration NHibernate Template v1.1
[based on MyGeneration/Template/NHibernate (c) by Sharp 1.4]
*/
using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Metadata.Interface;

namespace BrandSystems.Marcom.Core.Metadata
{

    /// <summary>
    /// EntityTypeFeature object for table 'MM_EntityType_Feature'.
    /// </summary>

    internal class EntityTypeFeature : IEntityTypeFeature
    {
        #region Member Variables

        protected int _id;
        protected int _typeid;
        protected int _featureid;
        protected string _featurename;


        #endregion

        #region Constructors
        public EntityTypeFeature() { }

        public EntityTypeFeature(int pId, int pTypeid, int pFeatureid)
        {
            this._id = pId;
            this._typeid = pTypeid;
            this._featureid = pFeatureid;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int TypeID
        {
            get { return _typeid; }
            set { _typeid = value; }

        }

        public int FeatureID
        {
            get { return _featureid; }
            set { _featureid = value; }

        }

        public string FeatureName
        {
            get { return _featurename; }
            set { _featurename = value; }
        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            EntityTypeFeature castObj = null;
            try
            {
                castObj = (EntityTypeFeature)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                this._id.Equals(castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion

    }

}
