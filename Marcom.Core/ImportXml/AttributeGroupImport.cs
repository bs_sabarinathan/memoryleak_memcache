﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.ImportXml
{
    internal class AttributeGroupImport 
    {
        #region Member Variables

        protected int _id;
        protected int _newid;
        protected string _caption;
        protected string _description;

        #endregion

        #region Constructors
        public AttributeGroupImport() { }

        public AttributeGroupImport(int pId, string pCaption, string pDescription, int pNewId)
        {
            this._id = pId;
            this._caption = pCaption;
            this._description = pDescription;
            this._newid = pNewId;
        }

        public AttributeGroupImport(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public int NewId
        {
            get { return _newid; }
            set { _newid = value; }

        }

        public string Caption
        {
            get { return _caption; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Caption", "Caption value, cannot contain more than 250 characters");
                _caption = value;
            }

        }
        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1073741823)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1073741823 characters");
                _description = value;
            }
        }

        #endregion

        #region Equals And HashCode Overrides
        /// <summary>
        /// local implementation of Equals based on unique value members
        /// </summary>
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            AttributeGroupImport castObj = null;
            try
            {
                castObj = (AttributeGroupImport)obj;
            }
            catch (Exception) { return false; }
            return (castObj != null) &&
                (this._id == castObj.Id);
        }
        /// <summary>
        /// local implementation of GetHashCode based on unique value members
        /// </summary>
        public override int GetHashCode()
        {


            int hash = 57;
            hash = 27 * hash * _id.GetHashCode();
            return hash;
        }
        #endregion
    }
}
