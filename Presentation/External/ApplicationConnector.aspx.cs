﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using DevExpress.ReportServer.ServiceModel.Client;
using DevExpress.ReportServer.ServiceModel.DataContracts;
using System.Web.Services;
using BrandSystems.Marcom.Utility;
using BrandSystems.Marcom.Core.Managers.Proxy;
using BrandSystems.Marcom.Core;
using BrandSystems.Marcom.Core.Interface;
using System.Collections;
using System.ServiceModel;
using Presentation.Utility;
using System.Configuration;
using BrandSystems.Marcom.Core.Utility;

namespace Presentation.External
{
    public partial class ApplicationConnector : System.Web.UI.Page
    {


        private static string baseDir = ConfigurationManager.AppSettings["MarcomPresentation"].ToString() + "";
        private static string retFilePath = baseDir + "//" + "External.txt";

        protected void Page_Load(object sender, EventArgs e)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            bool bReturnLog = false;
            try
            {
                if (Request.QueryString["id"] != null)
                {
                    TenantResolver tenantres = new TenantResolver(System.Web.HttpContext.Current.Request.Url.Host.ToString());
                    Guid userSession = MarcomManagerFactory.GetSystemSession(tenantres.tenantID);
                    IMarcomManager managers = MarcomManagerFactory.GetMarcomManager(null, userSession);
                    int documentID = Convert.ToInt32(Request.QueryString["id"]);
                    string stepName = Convert.ToString(Request.QueryString["step"]);
                    int dalimstepid = 0, status = 0;
                    switch (stepName)
                    {
                        case "BrandManagerApproved":
                            stepName = "Brand Manager Review";
                            status = 3;
                            break;
                        case "BrandManagerRejected":
                            stepName = "Brand Manager Review";
                            status = 5;
                            break;
                        case "BrandApproved":
                            stepName = "Brand Review";
                            status = 3;
                            break;
                        case "BrandRejected":
                            stepName = "Brand Review";
                            status = 5;
                            break;
                        case "ClientApproved":
                            stepName = "Client Review";
                            status = 3;
                            break;
                        case "ClientRejected":
                            stepName = "Client Review";
                            status = 5;
                            break;
                        case "Phase1approved":
                            stepName = "Phase 1 Review";
                            status = 3;
                            break;
                        case "Phase1rejected":
                            stepName = "Phase 1 Review";
                            status = 5;
                            break;
                        case "Phase2approved":
                            stepName = "Phase 2 Review";
                            status = 3;
                            break;
                        case "Phase2rejected":
                            stepName = "Phase 2 Review";
                            status = 5;
                            break;


                    }
                     
                    bool response = managers.ExternalTaskManager.UpdateDalimUserTaskStatus(documentID, dalimstepid, status, stepName);
                    if (response)
                    {
                        ErrorLog.LogFilePath = retFilePath;
                        string strLog = "url= >" + url;
                        bReturnLog = ErrorLog.CustomExternalRoutine(strLog + " Succusfully update status", DateTime.Now);
                        Response.Write("true");
                    }
                    else
                    {
                        ErrorLog.LogFilePath = retFilePath;
                        string strLog = "url= >" + url;
                        bReturnLog = ErrorLog.CustomExternalRoutine(strLog + " Failed to update status", DateTime.Now);
                        Response.Write("false");
                    }

                }
                else
                {
                    ErrorLog.LogFilePath = retFilePath;
                    string strLog = "url= >" + url;
                    bReturnLog = ErrorLog.CustomExternalRoutine(strLog + "Error: TaskId is null", DateTime.Now);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.LogFilePath = retFilePath;
                string strLog = "url= >" + url;
                bReturnLog = ErrorLog.CustomExternalRoutine(strLog + "Error: " + ex.Message.ToString(), DateTime.Now);
            }
        }

    }
}