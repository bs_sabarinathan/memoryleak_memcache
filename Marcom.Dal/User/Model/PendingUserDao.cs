using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.User.Model
{

	/// <summary>
	/// UserDao object for table 'UM_User'.
	/// </summary>
	public partial class PendingUserDao : BaseDao, ICloneable 
	{
		#region Member Variables

		protected int _id;
		protected string _firstname;
		protected string _lastname;
		protected string _username;
		protected byte[] _password;
        protected string _saltPassword;
		protected string _email;
		
		protected string _language;
		protected string _timezone;
        protected string _companyname;
        protected string _partners;
        protected string _title;
        protected string _department;
		
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
       

        protected bool _gender;
        protected DateTime _dob;
        protected string _phone;
        protected string _address;
        protected string _city;
        protected int _zipcode;
        protected string _website;

      
        protected string _activationstatus;


		#endregion
		
		#region Constructors
		public PendingUserDao() {}

        public PendingUserDao(string pFirstName, string pLastName, string pUserName, byte[] pPassword, string pEmail, string pLanguage, string pTimeZone, string psaltPassword, bool pGender, DateTime pDOB, string pPhone, string pAddress, string pCity, int pZipcode, string pWebsite, string pCompanyName, string pPartners, string pTitle, string pDepartment, string pActivationStatus)
		{
			this._firstname = pFirstName; 
			this._lastname = pLastName; 
			this._username = pUserName; 
			this._password = pPassword; 
			this._email = pEmail; 
			
			this._language = pLanguage; 
			this._timezone = pTimeZone;

            this._companyname = pCompanyName; 
			this._partners = pPartners;
            this._title = pTitle;
            this._department = pDepartment;
            this._saltPassword = psaltPassword;
            
            this._gender=pGender;           
            this._dob=pDOB;
            this._phone=pPhone;
            this._address=pAddress;
            this._city=pCity;
            this._zipcode=pZipcode;
            this._website=pWebsite;
      
            this._activationstatus=pActivationStatus;
		}

        public PendingUserDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public virtual int Id
		{
			get { return _id; }
			set { _bIsChanged |= (_id != value); _id = value; }
			
		}
		
		public virtual string FirstName
		{
			get { return _firstname; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("FirstName", "FirstName value, cannot contain more than 50 characters");
			  _bIsChanged |= (_firstname != value); 
			  _firstname = value; 
			}
			
		}
		
		public virtual string LastName
		{
			get { return _lastname; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("LastName", "LastName value, cannot contain more than 50 characters");
			  _bIsChanged |= (_lastname != value); 
			  _lastname = value; 
			}
			
		}
		
		public virtual string UserName
		{
			get { return _username; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("UserName", "UserName value, cannot contain more than 250 characters");
			  _bIsChanged |= (_username != value); 
			  _username = value; 
			}
			
		}

        public virtual byte[] Password
		{
			get { return _password; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Password", "Password value, cannot contain more than 50 characters");
			  _bIsChanged |= (_password != value); 
			  _password = value; 
			}
			
		}


        public virtual string SaltPassword
        {
            get { return _saltPassword; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Password", "Password value, cannot contain more than 50 characters");
                _bIsChanged |= (_saltPassword != value);
                _saltPassword = value;
            }

        }

		public virtual string Email
		{
			get { return _email; }
			set 
			{
			  if (value != null && value.Length > 250)
			    throw new ArgumentOutOfRangeException("Email", "Email value, cannot contain more than 250 characters");
			  _bIsChanged |= (_email != value); 
			  _email = value; 
			}
			
		}
		
	
		
		public virtual string Language
		{
			get { return _language; }
			set 
			{
			  if (value != null && value.Length > 50)
			    throw new ArgumentOutOfRangeException("Language", "Language value, cannot contain more than 50 characters");
			  _bIsChanged |= (_language != value); 
			  _language = value; 
			}
			
		}
		
		public virtual string TimeZone
		{
			get { return _timezone; }
			set 
			{
			  if (value != null && value.Length > 1000)
			    throw new ArgumentOutOfRangeException("TimeZone", "TimeZone value, cannot contain more than 50 characters");
			  _bIsChanged |= (_timezone != value); 
			  _timezone = value; 
			}
			
		}
        public virtual string CompanyName
        {
            get { return _companyname; }
            set
            {
                if (value != null && value.Length > 1000)
                    throw new ArgumentOutOfRangeException("CompanyName", "CompanyName value, cannot contain more than 500 characters");
                _bIsChanged |= (_companyname != value);
                _companyname = value;
            }

        }
        public virtual string Partners
        {
            get { return _partners; }
            set
            {
                if (value != null && value.Length > 1000)
                    throw new ArgumentOutOfRangeException("CompanyName", "TimeZone value, cannot contain more than 1000 characters");
                _bIsChanged |= (_partners != value);
                _partners = value;
            }

        }
        public virtual string Title
        {
            get { return _title; }
            set
            {
                if (value != null && value.Length > 1000)
                    throw new ArgumentOutOfRangeException("title", "title value, cannot contain more than 500 characters");
                _bIsChanged |= (_title != value);
                _title = value;
            }

        }
        public virtual string Department
        {
            get { return _department; }
            set
            {
                if (value != null && value.Length > 1000)
                    throw new ArgumentOutOfRangeException("department", "department value, cannot contain more than 1000 characters");
                _bIsChanged |= (_department != value);
                _department = value;
            }

        } 
            

        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}

        public virtual bool Gender
        {
            get { return _gender; }
            set { _gender = value; }

        }

        public virtual DateTime DOB
        {
            
               get { return _dob; }
			set { _bIsChanged |= (_dob != value); _dob = value; }

        }

        public virtual string Phone
        {
               get { return _phone; }
			set { _bIsChanged |= (_phone != value); _phone = value; }            

        }

        public virtual string Address
        {
            get { return _address; }
            set
            {
                if (value != null && value.Length > 250)
                    throw new ArgumentOutOfRangeException("Address", "Address value, cannot contain more than 250 characters");
                _bIsChanged |= (_address != value);
                _address = value;
            }

        }

        public virtual string City
        {
            get { return _city; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("City", "City value, cannot contain more than 50 characters");
                _bIsChanged |= (_city != value);
                _city = value;
            }

        }

        public virtual int ZipCode
        {
              get { return _zipcode; }
			set { _bIsChanged |= (_zipcode != value); _zipcode = value; }
           
        }

        public virtual string Website
        {
            get { return _website; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Website", "Website value, cannot contain more than 50 characters");
                _bIsChanged |= (_website != value);
                _website = value;
            }

        }      
      

         public virtual string ActivationStatus
        {
             get { return _activationstatus; }
			set { _bIsChanged |= (_activationstatus != value); _activationstatus = value; }
          

         }
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
            public const string PendingUserDao = "PendingUserDao";			
			public const string Id = "Id";			
			public const string FirstName = "FirstName";			
			public const string LastName = "LastName";			
			public const string UserName = "UserName";			
			public const string Password = "Password";
            public const string SaltPassword = "SaltPassword";
			public const string Email = "Email";			
						
			public const string Language = "Language";			
			public const string TimeZone = "TimeZone";			
			
            public const string Gender = "Gender";
            public const string DOB = "DOB";
            public const string Phone = "Phone";
            public const string Address = "Address";
            public const string City = "City";
            public const string ZipCode = "ZipCode";
            public const string Website = "Website";
            public const string CompanyName = "CompanyName";
            public const string Partners = "Partners";
            public const string Department = "Department";
            public const string Title = "Title"; 
            public const string ActivationStatus = "ActivationStatus";
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
            public const string PendingUserDao = "PendingUserDao";
            public const string Id = "Id";
            public const string FirstName = "FirstName";
            public const string LastName = "LastName";
            public const string UserName = "UserName";
            public const string Password = "Password";
            public const string SaltPassword = "SaltPassword";
            public const string Email = "Email";

            public const string Language = "Language";
            public const string TimeZone = "TimeZone";

            public const string Gender = "Gender";
            public const string DOB = "DOB";
            public const string Phone = "Phone";
            public const string Address = "Address";
            public const string City = "City";
            public const string ZipCode = "ZipCode";
            public const string Website = "Website";
            public const string CompanyName = "CompanyName";
            public const string Partners = "Partners";
            public const string Department = "Department";
            public const string Title = "Title";
            public const string ActivationStatus = "ActivationStatus";

		}

		#endregion
		
		
	}
	
}
