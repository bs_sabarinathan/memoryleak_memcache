﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandSystems.Marcom.Core.Cms.Interface
{
    public interface ICmsEntity
    {
        int ID
        {
            get;
            set;
        }

        int ParentID
        {
            get;
            set;
        }


        bool Active
        {
            get;
            set;
        }

        string UniqueKey
        {
            get;
            set;
        }

        string Name
        {
            get;
            set;
        }


        int Level
        {
            get;
            set;
        }

        int TemplateID
        {
            get;
            set;
        }

        int NavID
        {
            get;
            set;
        }


        string PublishedDate
        {
            get;
            set;
        }

        string PublishedTime
        {
            get;
            set;
        }

        string Description
        {
            get;
            set;
        }

        int Version
        {
            get;
            set;
        }


        bool PublishedStatus
        {
            get;
            set;
        }

        bool IsChildrenPresent
        {
            get;
            set;
        }

        string Tag
        {
            get;
            set;
        }
    }
}
