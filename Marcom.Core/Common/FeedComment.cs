using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Common.Interface;

namespace BrandSystems.Marcom.Core.Common
{

	/// <summary>
	/// FeedComment object for table 'CM_Feed_Comment'.
	/// </summary>
	
	internal class FeedComment : IFeedComment 
	{
		#region Member Variables

		protected int _id;
		protected int _feedid;
		protected int _actor;
		protected string _comment;
		protected string _commentedon;
        protected string _username;
        protected string _usermail;
        protected string _userimage;
		
		#endregion
		
		#region Constructors
		public FeedComment() {}
			
		public FeedComment(int pFeedid, int pActor, string pComment, string pCommentedOn)
		{
			this._feedid = pFeedid; 
			this._actor = pActor; 
			this._comment = pComment; 
			this._commentedon = pCommentedOn; 
		}
				
		public FeedComment(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties
		
		public int Id
		{
			get { return _id; }
			set { _id = value; }
			
		}
		
		public int Feedid
		{
			get { return _feedid; }
			set { _feedid = value; }
			
		}
		
		public int Actor
		{
			get { return _actor; }
			set { _actor = value; }
			
		}
		
		public string Comment
		{
			get { return _comment; }
			set 
			{
			  if (value != null && value.Length > 4000)
			    throw new ArgumentOutOfRangeException("Comment", "Comment value, cannot contain more than 4000 characters");
			  _comment = value; 
			}
			
		}
		
		public string CommentedOn
		{
			get { return _commentedon; }
			set { _commentedon = value; }
			
		}

        public string UserName
        {
            get { return _username; }
            set
            {
                _username = value;
            }

        }

        public string Usermail
        {
            get { return _usermail; }
            set { _usermail = value; }
        }

        public string Userimage
        {
            get { return _userimage; }
            set { _userimage = value; }
        }

		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			FeedComment castObj = null;
			try
			{
				castObj = (FeedComment)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
