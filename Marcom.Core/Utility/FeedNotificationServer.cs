﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using BrandSystems.Marcom.Core.Common;
using BrandSystems.Marcom.Core.Common.Interface;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Dal.Access.Model;
using BrandSystems.Marcom.Dal.Common.Model;
using BrandSystems.Marcom.Dal.Metadata.Model;
using BrandSystems.Marcom.Dal.Planning.Model;
using BrandSystems.Marcom.Dal.Task.Model;
using BrandSystems.Marcom.Dal.User.Model;
using Mail;
using Newtonsoft.Json.Linq;
using BrandSystems.Marcom.Dal.DAM.Model;
using System.Configuration;
using BrandSystems.Marcom.Core.AmazonStorageHelper;


namespace BrandSystems.Marcom.Core.Utility
{
    class FeedNotificationServer
    {
        IMarcomManager marcomManager;

        public FeedNotificationServer(int TenantID)
        {
            marcomManager = MarcomManagerFactory.GetMarcomManager(null, MarcomManagerFactory.GetSystemSession(TenantID));
        }

        public void AsynchronousNotify(NotificationFeedObjects obj)
        {
            System.Threading.Tasks.Task taskforfeed = new System.Threading.Tasks.Task(() => addNewsFeedAndNotification(obj));
            taskforfeed.Start();

        }
        public void ScheduledMailcheck(int TenantID)
        {
            MailServer ms = new MailServer();
            ms.HandleSendMail(TenantID);
        }
        public void TaskReminder(int TenantID)
        {
            MailServer ms = new MailServer();
            ms.InsertIntoMailNotification(TenantID);
        }
        public void EntityStatusReminder(int TenantID)
        {
            MailServer ms = new MailServer();
            ms.UpdateNotification(TenantID);
        }
        public void SendMailForNotifications(int userid)
        {
            //not in use
        }


        public void addNewsFeedAndNotification(NotificationFeedObjects obj)
        {
            int newsFeedTemplateId = 0;
            int MailTemplateID = 0;
            MailServer ms = new MailServer();

            try
            {
                //Get the tenant related file path
                TenantSelection tfp = new TenantSelection();
                string TenantFilePath = tfp.GetTenantFilePath(marcomManager.User.TenantID);
                XElement xelementFilepath = MarcomCache<XElement>.ReadXElement(xmlType.Admin, marcomManager.User.TenantID);

                //The Key is root node current Settings
                string xelementName = "ApplicationURL";

                var xmlElement = xelementFilepath.Element(xelementName);


                using (ITransaction txInnerloop = marcomManager.GetTransaction(marcomManager.User.TenantID))
                {

                    switch (obj.action)     //switch case for different actions performed.
                    {

                        case "create entity":
                            int notificationtypeid = 0;
                            MailTemplateID = 23;
                            if (obj.ParentId == 0)
                            {
                                newsFeedTemplateId = 18;
                                notificationtypeid = Convert.ToInt32(NotificationTypeID.RootLevelEntityCreated);
                            }
                            else
                            {
                                newsFeedTemplateId = 1;
                                notificationtypeid = Convert.ToInt32(NotificationTypeID.EntityCreated);
                            }
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue);
                            // marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, notificationtypeid, false, false, obj.TypeName, "", "", "", MailTemplateID);
                            txInnerloop.Commit();
                            break;
                        case "add attribute group":
                            newsFeedTemplateId = 51;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, 0, obj.AttributeRecordName);
                            txInnerloop.Commit();
                            break;
                        case "delete attribute group":
                            newsFeedTemplateId = 52;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, 0, obj.AttributeRecordName);
                            txInnerloop.Commit();
                            break;
                        case "Predefined Objective":
                            newsFeedTemplateId = 57;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, obj.AttributeRecordName);
                            txInnerloop.Commit();
                            break;
                        case "delete Predefined Objective":
                            newsFeedTemplateId = 19;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, obj.AttributeRecordName);
                            txInnerloop.Commit();
                            break;
                        case "Additional Objective":
                            newsFeedTemplateId = 49;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue);
                            txInnerloop.Commit();
                            break;
                        case "duplicate entity":
                            newsFeedTemplateId = 48;
                            obj.TypeName = (from item in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where item.Id == Convert.ToInt32(obj.EntityTypeId) select item.Caption).FirstOrDefault();
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;
                        case "delete entity":
                            newsFeedTemplateId = 19;
                            MailTemplateID = 25;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue,0,obj.AssociatedEntityId);
                            marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.EntityDeleted), false, false, obj.TypeName, "", "", "", MailTemplateID);
                            txInnerloop.Commit();
                            break;
                        case "del check list":
                            newsFeedTemplateId = 25;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);
                            IList<TaskMembersDao> taskmemdel = new List<TaskMembersDao>();
                            taskmemdel = (from mem in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<TaskMembersDao>() where mem.TaskID == obj.EntityId select mem).ToList<TaskMembersDao>();
                            var taskdetailsforchklstdel = (from entid in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityTaskDao>() where entid.ID == obj.EntityId select entid).FirstOrDefault();
                            var tasktypechkdel = (from ent in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where ent.Id == Convert.ToInt32(taskdetailsforchklstdel.TaskType) select ent).FirstOrDefault();
                            foreach (var item in taskmemdel)
                            {
                                if (item.RoleID != 1)
                                {
                                    if (item.UserID != obj.Actorid)
                                    {
                                        marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 41, false, false, tasktypechkdel.Caption, obj.AttributeName, obj.FromValue, obj.AssociatedEntityId.ToString(), 48, item.UserID);
                                    }
                                }
                            }
                            txInnerloop.Commit();
                            break;
                        case "add check list":
                            newsFeedTemplateId = 24;
                            var taskdetailsforchklstadd = (from entid in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityTaskDao>() where entid.ID == obj.EntityId select entid).FirstOrDefault();
                            var entitydetaitladd = (from ent in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityDao>() where ent.Id == Convert.ToInt32(taskdetailsforchklstadd.EntityID) select ent).FirstOrDefault();
                            IList<TaskMembersDao> taskmemadd = new List<TaskMembersDao>();
                            taskmemadd = (from mem in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<TaskMembersDao>() where mem.TaskID == obj.EntityId select mem).ToList<TaskMembersDao>();
                            obj.AssociatedEntityId = entitydetaitladd.Id;
                            var tasktypechkadd = (from ent in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where ent.Id == Convert.ToInt32(taskdetailsforchklstadd.TaskType) select ent).FirstOrDefault();
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);
                            foreach (var item in taskmemadd)
                            {
                                if (item.RoleID != 1)
                                {
                                    if (item.UserID != obj.Actorid)
                                    {
                                        marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 40, false, false, tasktypechkadd.Caption, obj.AttributeName, obj.FromValue, obj.AssociatedEntityId.ToString(), 47, item.UserID);

                                    }
                                }
                            }
                            txInnerloop.Commit();
                            break;
                        case "status change check list":
                            newsFeedTemplateId = 26;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);

                            IList<TaskMembersDao> taskmem = new List<TaskMembersDao>();
                            taskmem = (from mem in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<TaskMembersDao>() where mem.TaskID == obj.EntityId select mem).ToList<TaskMembersDao>();
                            var taskdetailsforchklst = (from entid in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityTaskDao>() where entid.ID == obj.EntityId select entid).FirstOrDefault();
                            var tasktypechk = (from ent in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where ent.Id == Convert.ToInt32(taskdetailsforchklst.TaskType) select ent).FirstOrDefault();
                            foreach (var item in taskmem)
                            {
                                if (item.RoleID != 1)
                                {
                                    if (item.UserID != obj.Actorid)
                                    {
                                        marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 42, false, false, tasktypechk.Caption, obj.AttributeName, obj.ToValue, obj.AssociatedEntityId.ToString(), 49, item.UserID);

                                    }
                                }
                            }
                            txInnerloop.Commit();
                            break;
                        case "Planned amount update":
                            newsFeedTemplateId = 7;
                            MailTemplateID = 25;
                            try
                            {
                                if (Convert.ToString(obj.FromValue) != Convert.ToString(obj.ToValue))
                                {
                                    var ccname = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<BaseEntityDao>() where data1.Id == Convert.ToInt32(obj.AttributeId) select data1.Name).FirstOrDefault().ToString();
                                    obj.AttributeName = ccname.ToString();
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, Convert.ToString(obj.FromValue), Convert.ToString(obj.ToValue), 0, obj.AssociatedEntityId);
                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.EntityPlanBudgetUpdated), false, false, obj.TypeName, obj.AttributeName, Convert.ToString(obj.FromValue), Convert.ToString(obj.ToValue), 0);
                                }

                            }
                            catch (Exception ex)
                            {

                            }
                            txInnerloop.Commit();
                            break;
                        case "Commited Amount":
                            newsFeedTemplateId = 20;
                            MailTemplateID = 30;
                            if (Convert.ToString(obj.FromValue) != Convert.ToString(obj.ToValue))
                            {
                                var ccnameforcommitedamount = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<BaseEntityDao>() where data1.Id == Convert.ToInt32(obj.AttributeId) select data1.Name).FirstOrDefault().ToString();
                                obj.AttributeName = ccnameforcommitedamount.ToString();
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, Convert.ToString(Convert.ToInt32(obj.FromValue)), Convert.ToString(Convert.ToInt32(obj.ToValue)), 0, obj.AssociatedEntityId);
                                //  marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, Convert.ToString(Convert.ToInt32(obj.FromValue)), Convert.ToString(Convert.ToInt32(obj.ToValue)));
                                marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.EntityCommitBudgetUpdated), false, false, obj.TypeName, obj.AttributeName, Convert.ToString(Convert.ToInt32(obj.FromValue)), Convert.ToString(Convert.ToInt32(obj.ToValue)), 0);
                            }
                            txInnerloop.Commit();
                            break;
                        case "Spent Amount":
                            newsFeedTemplateId = 21;
                            try
                            {
                                if (Convert.ToString(obj.FromValue) != Convert.ToString(obj.ToValue))
                                {
                                    var ccnameforspentamount = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<BaseEntityDao>() where data1.Id == Convert.ToInt32(obj.AttributeId) select data1.Name).FirstOrDefault().ToString();
                                    obj.AttributeName = ccnameforspentamount.ToString();
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, Convert.ToString(Convert.ToInt32(obj.FromValue)), Convert.ToString(Convert.ToInt32(obj.ToValue)), 0, obj.AssociatedEntityId);
                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.EntitySpentBudgetUpdated), false, false, obj.TypeName, obj.AttributeName, Convert.ToString(Convert.ToInt32(obj.FromValue)), Convert.ToString(Convert.ToInt32(obj.ToValue)));
                                }
                                txInnerloop.Commit();
                            }
                            catch (Exception e)
                            {
                            }
                            break;
                        case "cost center assigned amount update":
                            newsFeedTemplateId = 9;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, Convert.ToString(Convert.ToInt32(obj.ToValue)));
                            txInnerloop.Commit();
                            break;
                        case "Release funds":
                            newsFeedTemplateId = 58;
                            var ccname3 = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<BaseEntityDao>() where data1.Id == Convert.ToInt32(obj.AttributeName) select data1.Name).FirstOrDefault().ToString();
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, ccname3, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;
                        case "Forecast amount update":
                            newsFeedTemplateId = 116;
                            //var ccname3 = (from data1 in txInnerloop.PersistenceManager.PlanningRepository.Query<BaseEntityDao>() where data1.Id == Convert.ToInt32(obj.AttributeName) select data1.Name).FirstOrDefault().ToString();
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;
                        case "money transfer":
                            MailTemplateID = 32;
                            newsFeedTemplateId = 12;
                            var ccname1 = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<BaseEntityDao>() where data1.Id == Convert.ToInt32(obj.FromValue) select data1.Name).FirstOrDefault().ToString();
                            var ccname2 = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<BaseEntityDao>() where data1.Id == Convert.ToInt32(obj.ToValue) select data1.Name).FirstOrDefault().ToString();

                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                            marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.MoneyTransferred), false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0);
                            txInnerloop.Commit();
                            break;
                        case "Update costcentre Approved Budjet":
                            newsFeedTemplateId = 60;
                            var ccname4 = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<BaseEntityDao>() where data1.Id == Convert.ToInt32(obj.EntityId) select data1.Name).FirstOrDefault().ToString();
                            //marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid , newsFeedTemplateId , obj.EntityId , obj.action , ccname4 , obj.FromValue , obj.ToValue,0,obj.AssociatedEntityId);
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, ccname4, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;
                        case "Approved allocations":
                            newsFeedTemplateId = 59;
                            var ccname5 = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<BaseEntityDao>() where data1.Id == Convert.ToInt32(obj.AssociatedEntityId) select data1.Name).FirstOrDefault().ToString();
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, ccname5, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;

                        case "costcenter added":
                            newsFeedTemplateId = 8;
                            MailTemplateID = 33;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, obj.Userid, obj.AssociatedEntityId);
                            // marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.CostcenterAdded), false, false, obj.TypeName, "", "", "", 0);
                            txInnerloop.Commit();
                            break;
                        case "cost center deleted from financial":
                            newsFeedTemplateId = 44;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, obj.Userid, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;
                        case "fund request status changed":
                            newsFeedTemplateId = 42;
                            var costcentredetaoilsoffundreq = (from dd in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityDao>() where dd.Id == obj.AssociatedEntityId select dd).FirstOrDefault();
                            var budgetApprover = (from dd in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<TaskMemberDao>() where dd.TaskID == obj.EntityId && dd.Roleid == 1 select dd).FirstOrDefault();
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, costcentredetaoilsoffundreq.Name, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);
                            marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 26, false, false, obj.TypeName, costcentredetaoilsoffundreq.Name, "", obj.ToValue, 40, budgetApprover.Userid);
                            txInnerloop.Commit();
                            break;
                        case "funding request Deleted":
                            newsFeedTemplateId = 45;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, obj.Userid, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;
                        case "added start period":
                            newsFeedTemplateId = 46;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, obj.Userid, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;
                        case "add comment for period":
                            newsFeedTemplateId = 47;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, obj.Userid, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;
                        case "comment added on feed":
                            marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 17, false, false, obj.TypeName, "", obj.FromValue, obj.ToValue, 51, 0, obj.AssociatedEntityId, obj.PhaseID, obj.StepID, obj.IsPersonalized);
                            txInnerloop.Commit();
                            break;
                        case "comment added":
                            marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, (obj.NotifitypeID) > 0 ? obj.NotifitypeID : 46, false, false, obj.TypeName, "", obj.FromValue, obj.ToValue, (obj.NotifitypeID) > 0 ? 51 : 42, obj.Userid, obj.AssociatedEntityId, obj.PhaseID, obj.StepID, obj.IsPersonalized);
                            txInnerloop.Commit();

                            break;
                        case "fundingrequest":
                            newsFeedTemplateId = 6;
                            MailTemplateID = 28;
                            var costcentrename = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<BaseEntityDao>() where data1.Id == Convert.ToInt32(obj.AttributeName) select data1).FirstOrDefault();
                            var budgetapprover = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityRoleUserDao>()
                                                  join EntityTypeRole in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<EntityTypeRoleAclDao>() on data1.Roleid equals EntityTypeRole.ID
                                                  where data1.Entityid == Convert.ToInt32(costcentrename.Id) && EntityTypeRole.EntityRoleID == 1
                                                  select data1).FirstOrDefault();

                            obj.AttributeName = costcentrename.Name.ToString();
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, Convert.ToString(obj.FinancialPlannedAmount), 0, obj.AssociatedEntityId);

                            if (budgetapprover.Userid != obj.Actorid)
                            {
                                marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.FundRequestCreated), false, false, obj.TypeName, obj.AttributeName, obj.FromValue, Convert.ToString(obj.FinancialPlannedAmount), 28, budgetapprover.Userid);
                            }

                            txInnerloop.Commit();
                            break;
                        case "milestone delete":
                            newsFeedTemplateId = 11;
                            MailTemplateID = 34;
                            StringBuilder query = new StringBuilder();
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                            // marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.MilestoneDeleted), false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID);
                            txInnerloop.Commit();
                            break;
                        case "milestoneadded":
                            newsFeedTemplateId = 10;
                            MailTemplateID = 27;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue,0,obj.AssociatedEntityId);
                            //   marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.MilestoneCreated), false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID);
                            txInnerloop.Commit();
                            break;
                        case "milestoneupdated":
                            newsFeedTemplateId = 43;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                            txInnerloop.Commit();
                            break;
                        case "attachment added":
                            int notificationtemplate = 0;
                            var checktaskentitytype = (from ent in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityDao>() where ent.Id == Convert.ToInt32(obj.EntityId) select ent).FirstOrDefault();
                            if (checktaskentitytype.Typeid == 30 || checktaskentitytype.Typeid == 31 || checktaskentitytype.Typeid == 2 || checktaskentitytype.Typeid == 3)
                            {
                                if (obj.attachmenttype == "link")
                                {
                                    newsFeedTemplateId = 27;
                                    notificationtemplate = 48;
                                    MailTemplateID = 46;
                                }
                                else
                                {
                                    newsFeedTemplateId = 28;
                                    notificationtemplate = 34;
                                    MailTemplateID = 44;
                                }
                                var taskdetailsforattachmnt = (from entid in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityTaskDao>() where entid.ID == obj.EntityId select entid).FirstOrDefault();
                                var entitydetaitladdattchmnt = (from ent in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityDao>() where ent.Id == Convert.ToInt32(taskdetailsforattachmnt.EntityID) select ent).FirstOrDefault();
                                obj.AssociatedEntityId = entitydetaitladdattchmnt.Id;
                                var tasktypechkaattachmnt = (from ent in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where ent.Id == Convert.ToInt32(taskdetailsforattachmnt.TaskType) select ent).FirstOrDefault();

                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);
                                marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, notificationtemplate, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID);
                            }
                            else
                            {
                                if (obj.attachmenttype == "link")
                                    newsFeedTemplateId = 15;
                                else
                                    newsFeedTemplateId = 14;

                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                            }
                            txInnerloop.Commit();
                            break;
                        case "attachment deleted":
                            var checktaskentitytypefordelet = (from ent in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityDao>() where ent.Id == Convert.ToInt32(obj.EntityId) select ent).FirstOrDefault();
                            if (checktaskentitytypefordelet.Typeid == 30 || checktaskentitytypefordelet.Typeid == 31 || checktaskentitytypefordelet.Typeid == 2 || checktaskentitytypefordelet.Typeid == 3)
                            {
                                int mailtemplateid = 0;
                                if (obj.attachmenttype == "link")
                                {
                                    newsFeedTemplateId = 29;
                                    notificationtemplate = 49;
                                    mailtemplateid = 43;
                                }
                                else
                                {
                                    newsFeedTemplateId = 30;
                                    notificationtemplate = 50;
                                    mailtemplateid = 45;
                                }
                                var taskdetailsforattachmnt1 = (from entid in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityTaskDao>() where entid.ID == obj.EntityId select entid).FirstOrDefault();
                                var entitydetaitladdattchmnt1 = (from ent in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityDao>() where ent.Id == Convert.ToInt32(taskdetailsforattachmnt1.EntityID) select ent).FirstOrDefault();
                                obj.AssociatedEntityId = entitydetaitladdattchmnt1.Id;
                                var tasktypechkaattachmnt1 = (from ent in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where ent.Id == Convert.ToInt32(taskdetailsforattachmnt1.TaskType) select ent).FirstOrDefault();

                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);
                                marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, notificationtemplate, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, mailtemplateid);
                            }
                            else
                            {
                                if (obj.attachmenttype == "link")
                                    newsFeedTemplateId = 31;
                                else
                                    newsFeedTemplateId = 32;
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                            }
                            txInnerloop.Commit();
                            break;
                        case "Task Reinitialized":

                            if (obj.AttributeName == "link")
                                newsFeedTemplateId = 34;
                            else
                                newsFeedTemplateId = 33;


                            string memberseinitialize = "";
                            int entityid1reinitialize = 0;
                            String taskownerNamereinitialize = "";
                            String taskownerEmailreinitialize = "";
                            string taskImagePathreinitialize = "";
                            String tasklogopathreinitialize = "";
                            MailServer mailServerreinitialize = new MailServer();
                            MailHolder taskmailHolder1taskreinitialize = new MailHolder();
                            taskmailHolder1taskreinitialize.PrimaryTo = new List<string>();

                            var taskdetailsreinitialize = (from entid in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityTaskDao>() where entid.ID == obj.EntityId select entid).FirstOrDefault();
                            var entitydetaitlreinitialize = (from ent in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityDao>() where ent.Id == Convert.ToInt32(taskdetailsreinitialize.EntityID) select ent).FirstOrDefault();
                            obj.AssociatedEntityId = entitydetaitlreinitialize.Id;
                            var tasktypechkreinitialize = (from ent in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where ent.Id == Convert.ToInt32(taskdetailsreinitialize.TaskType) select ent).FirstOrDefault();
                            obj.TypeName = tasktypechkreinitialize.Caption;

                            var Userid1taskreinitialize = (from tt in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<UserDao>() where tt.Id == Convert.ToInt32(obj.Actorid) select tt).FirstOrDefault();

                            taskmailHolder1taskreinitialize.MailType = 35;
                            taskmailHolder1taskreinitialize.MailTemplateDictionary = new Dictionary<string, string>();

                            taskownerNamereinitialize = Userid1taskreinitialize.FirstName + " " + Userid1taskreinitialize.LastName;
                            taskownerEmailreinitialize = Userid1taskreinitialize.Email;
                            taskmailHolder1taskreinitialize.PrimaryTo = new List<string>();
                            if (marcomManager.User.AwsStorage.storageType == (int)StorageArea.Amazon)
                            {
                                string cloudUrl = (marcomManager.User.AwsStorage.Uploaderurl + "/" + marcomManager.User.AwsStorage.BucketName + "/" + GetTenantFilePath(marcomManager.User.TenantID)).Replace("\\", "/");
                                tasklogopathreinitialize = cloudUrl + "logo.png";
                                string userImageS3Key = GetTenantFilePath(marcomManager.User.TenantID) + "UserImages/" + Userid1taskreinitialize.Id + ".jpg";
                                taskImagePathreinitialize = cloudUrl + "UserImages/" + Userid1taskreinitialize.Id + ".jpg";

                                bool isexist = AWSHelper.isKeyExists(marcomManager.User.AwsStorage.S3, marcomManager.User.AwsStorage.BucketName, userImageS3Key);
                                if (!isexist)
                                {
                                    taskImagePathreinitialize = cloudUrl + "UserImages/noimage.jpg";
                                }

                            }
                            else
                            {
                                tasklogopathreinitialize = xmlElement.Value.ToString() + GetTenantFilePath(marcomManager.User.TenantID) + "/logo.png";
                                taskImagePathreinitialize = xmlElement.Value.ToString() + GetTenantFilePath(marcomManager.User.TenantID) + "/UserImages/" + Userid1taskreinitialize.Id + ".jpg";

                                if (System.IO.File.Exists(Path.Combine(Path.Combine(ConfigurationManager.AppSettings["MarcomPresentation"] + TenantFilePath + "/UserImages/", Userid1taskreinitialize.Id + ".jpg"))) == false)
                                    taskImagePathreinitialize = xmlElement.Value.ToString() + GetTenantFilePath(marcomManager.User.TenantID) + "/UserImages/noimage.jpg";
                            }


                            taskmailHolder1taskreinitialize.MailType = 37;
                            taskmailHolder1taskreinitialize.MailTemplateDictionary = new Dictionary<string, string>();

                            taskmailHolder1taskreinitialize.MailTemplateDictionary.Add("@AssingedBy@", taskownerNamereinitialize);
                            taskmailHolder1taskreinitialize.MailTemplateDictionary.Add("@Time@", DateTimeOffset.Now.ToString());
                            taskmailHolder1taskreinitialize.MailTemplateDictionary.Add("@AssingedByImage@", taskImagePathreinitialize);
                            taskmailHolder1taskreinitialize.MailTemplateDictionary.Add("@ApplicationURL@", "");
                            taskmailHolder1taskreinitialize.MailTemplateDictionary.Add("@AssingedByMailID@", taskownerEmailreinitialize);
                            taskmailHolder1taskreinitialize.MailTemplateDictionary.Add("@ImagePath@", tasklogopathreinitialize);

                            foreach (var mem in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityRoles)
                            {
                                if (mem.UserID != obj.Actorid)
                                {
                                    //------------notfication------------
                                    UserNotificationDao userNotifyfortask1reinitaize = new UserNotificationDao();
                                    userNotifyfortask1reinitaize.Entityid = obj.EntityId;
                                    userNotifyfortask1reinitaize.Actorid = obj.Actorid;
                                    userNotifyfortask1reinitaize.CreatedOn = DateTimeOffset.Now;
                                    userNotifyfortask1reinitaize.Typeid = 43;
                                    userNotifyfortask1reinitaize.IsViewed = false;
                                    userNotifyfortask1reinitaize.IsSentInMail = false;
                                    userNotifyfortask1reinitaize.TypeName = obj.TypeName;
                                    userNotifyfortask1reinitaize.AttributeName = obj.AttributeName;
                                    userNotifyfortask1reinitaize.FromValue = obj.FromValue;
                                    userNotifyfortask1reinitaize.ToValue = obj.ToValue;
                                    userNotifyfortask1reinitaize.Userid = mem.UserID;
                                    if (mem.UserID != obj.Actorid)
                                    {
                                        txInnerloop.PersistenceManager.CommonRepository[marcomManager.User.TenantID].Save<UserNotificationDao>(userNotifyfortask1reinitaize);
                                    }
                                    var actorDetail1taskreinitialze = (from act in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<UserDao>() where act.Id == Convert.ToInt32(mem.UserID) select act).FirstOrDefault();
                                    var actorName1taskreinitialze = actorDetail1taskreinitialze.FirstName + " " + actorDetail1taskreinitialze.LastName;


                                    taskmailHolder1taskreinitialize.PrimaryTo.Add(actorDetail1taskreinitialze.Email);

                                    if (taskmailHolder1taskreinitialize.MailTemplateDictionary.ContainsKey("@taskTypeName@"))
                                        taskmailHolder1taskreinitialize.MailTemplateDictionary["@taskTypeName@"] = tasktypechkreinitialize.Caption;
                                    else
                                        taskmailHolder1taskreinitialize.MailTemplateDictionary.Add("@taskTypeName@", tasktypechkreinitialize.Caption);


                                    if (taskmailHolder1taskreinitialize.MailTemplateDictionary.ContainsKey("@taskName@"))
                                        taskmailHolder1taskreinitialize.MailTemplateDictionary["@taskName@"] = taskdetailsreinitialize.Name.ToString();
                                    else
                                        taskmailHolder1taskreinitialize.MailTemplateDictionary.Add("@taskName@", taskdetailsreinitialize.Name.ToString());

                                    var mainenity = (from tt in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityDao>() where tt.Id == Convert.ToInt32(obj.AssociatedEntityId) select tt).FirstOrDefault();
                                    var maintype = (from tt in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where tt.Id == Convert.ToInt32(mainenity.Typeid) select tt).FirstOrDefault();
                                    if (taskmailHolder1taskreinitialize.MailTemplateDictionary.ContainsKey("@EntityTypeName@"))
                                        taskmailHolder1taskreinitialize.MailTemplateDictionary["@EntityTypeName@"] = maintype.Caption;
                                    else
                                        taskmailHolder1taskreinitialize.MailTemplateDictionary.Add("@EntityTypeName@", maintype.Caption);


                                    if (taskmailHolder1taskreinitialize.MailTemplateDictionary.ContainsKey("@EntityName@"))
                                        taskmailHolder1taskreinitialize.MailTemplateDictionary["@EntityName@"] = mainenity.Name.ToString();
                                    else
                                        taskmailHolder1taskreinitialize.MailTemplateDictionary.Add("@EntityName@", mainenity.Name.ToString());

                                    var file = (from uu in txInnerloop.PersistenceManager.CommonRepository[marcomManager.User.TenantID].Query<FileDao>() where uu.Id == Convert.ToInt32(obj.ToValue) select uu).FirstOrDefault();
                                    if (taskmailHolder1taskreinitialize.MailTemplateDictionary.ContainsKey("@filename@"))
                                        taskmailHolder1taskreinitialize.MailTemplateDictionary["@filename@"] = file.Name.ToString();
                                    else
                                        taskmailHolder1taskreinitialize.MailTemplateDictionary.Add("@filename@", file.Name.ToString());

                                    mailServerreinitialize.HandleUnScheduledMail(taskmailHolder1taskreinitialize, marcomManager.User.TenantID, marcomManager.User.TenantHost);
                                }

                            }
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;

                        case "delete task":
                            newsFeedTemplateId = 36;
                            //var taskdetailsdelete = (from entid in txInnerloop.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Query<EntityTaskDao>() where entid.ID == obj.EntityId select entid).FirstOrDefault();

                            //var entitydetaitldelete = (from ent in txInnerloop.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Query<EntityDao>() where ent.Id == Convert.ToInt32(taskdetailsdelete.EntityID) select ent).FirstOrDefault();
                            //obj.AssociatedEntityId = entitydetaitldelete.Id;
                            //var tasktypechkrdelete = (from ent in txInnerloop.PersistenceManager.TaskRepository[proxy.MarcomManager.User.TenantID].Query<EntityTypeDao>() where ent.Id == Convert.ToInt32(taskdetailsdelete.TaskType) select ent).FirstOrDefault();
                            //obj.TypeName = tasktypechkrdelete.Caption;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);

                            txInnerloop.Commit();
                            break;
                        case "forcefully completed":
                            newsFeedTemplateId = 37;
                            var taskdetailsComplete = (from entid in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityTaskDao>() where entid.ID == obj.EntityId select entid).FirstOrDefault();
                            var tasktypechkrComplete = (from ent in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where ent.Id == Convert.ToInt32(taskdetailsComplete.TaskType) select ent).FirstOrDefault();
                            obj.TypeName = tasktypechkrComplete.Caption;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);
                            marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 7, false, false, obj.action, "", obj.FromValue, obj.action, 36, 0, 0, obj.PhaseID, "0");
                            txInnerloop.Commit();
                            break;
                        case "Removed Task Member":
                            newsFeedTemplateId = 35;
                            string removedmembers = "";
                            var taskdetailsremoved = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityTaskDao>() where data1.ID == Convert.ToInt32(obj.EntityId) select data1.TaskType).FirstOrDefault().ToString();
                            var tasktype2removed = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where data1.Id == Convert.ToInt32(taskdetailsremoved) select data1.Caption).FirstOrDefault().ToString();
                            var Userid1removed = (from tt in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<UserDao>() where tt.Id == Convert.ToInt32(obj.Actorid) select tt).FirstOrDefault();

                            foreach (var mem in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityRoles)
                            {
                                if (mem.RoleID != 1)
                                {
                                    removedmembers = removedmembers + ',' + mem.UserID;

                                }
                                marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 45, false, false, tasktype2removed, "", obj.FromValue, "", 41, mem.UserID);

                            }

                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, tasktype2removed, obj.AttributeName, obj.FromValue, removedmembers);

                            txInnerloop.Commit();
                            break;
                        case "multitask task member added":
                            newsFeedTemplateId = 17;


                            foreach (var task in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).MultiTask)
                            {
                                string multimembers = "";
                                var multitaskdetails = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityTaskDao>() where data1.ID == Convert.ToInt32(task) select data1.TaskType).FirstOrDefault().ToString();
                                var multitasktype2 = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where data1.Id == Convert.ToInt32(multitaskdetails) select data1.Caption).FirstOrDefault().ToString();
                                var multiUserid1 = (from tt in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<UserDao>() where tt.Id == Convert.ToInt32(obj.Actorid) select tt).FirstOrDefault();

                                var taskmembers = ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityRoles.Where(a => a.TaskID == task && a.RoleID != 1).ToList();
                                foreach (var mem in taskmembers)
                                {
                                    if (mem.RoleID != 1)
                                    {
                                        multimembers = multimembers + ',' + mem.UserID;

                                    }
                                    if (mem.RoleID != 1 && mem.UserID != ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).Actorid)
                                    {
                                        marcomManager.CommonManager.UserNotification_Insert(task, obj.Actorid, DateTimeOffset.Now, 6, false, false, multitasktype2, "", obj.FromValue, "");

                                        var actorDetail1 = (from act in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<UserDao>() where act.Id == Convert.ToInt32(mem.UserID) select act).FirstOrDefault();
                                        var actorName1 = actorDetail1.FirstName + " " + actorDetail1.LastName;

                                    }

                                }
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, task, multitasktype2, obj.AttributeName, obj.FromValue, multimembers);
                            }
                            //written  to handle multi assigned task mails only.
                            marcomManager.CommonManager.InsertMultiAssignedTaskMail(59, obj.Actorid, ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).MultiTask, ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityRoles);
                            txInnerloop.Commit();

                            break;
                        case "task Version added":
                            newsFeedTemplateId = 71;
                            int vernotificationtypeidtemp1id = 61;
                            string versiontaskmembers = "";
                            var taskversionmembers = ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityRoles.Where(a => a.TaskID == obj.EntityId && a.RoleID != 1).ToList();
                            var taskversiondetails = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityTaskDao>() where data1.ID == Convert.ToInt32(obj.EntityId) select data1.TaskType).FirstOrDefault().ToString();
                            var taskversiontype2 = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where data1.Id == Convert.ToInt32(taskversiondetails) select data1.Caption).FirstOrDefault().ToString();
                            foreach (var mem in taskversionmembers)
                            {
                                if (versiontaskmembers.Contains(mem.UserID.ToString()))
                                { }
                                else if (mem.UserID != ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).Actorid)
                                {
                                    if(versiontaskmembers.Length >0)
                                        versiontaskmembers = versiontaskmembers + ',' + mem.UserID.ToString();
                                    else
                                        versiontaskmembers = mem.UserID.ToString();
                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, vernotificationtypeidtemp1id, false, false, taskversiontype2, "", obj.FromValue, obj.Version.ToString(),63, mem.UserID);
                                    //marcomManager.CommonManager.UserNotification_Insert(task.ID, obj.Actorid, DateTimeOffset.Now, notificationtypeidtemp1id, false, false, tasktype, "", obj.FromValue, taskmembers, 35, mem.UserID, 0, Phaselists, usersteps);
                                    var actorDetail1 = (from act in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<UserDao>() where act.Id == Convert.ToInt32(mem.UserID) select act).FirstOrDefault();
                                    var actorName1 = actorDetail1.FirstName + " " + actorDetail1.LastName;

                                }

                            }
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, taskversiontype2, obj.AttributeName, obj.FromValue, versiontaskmembers,0,0,null,obj.Version);
                            txInnerloop.Commit();
                            break;

                        case "task member added":
                            newsFeedTemplateId = 17;
                            int notificationtypeidtempid = 6;
                            string members = "";
                            int maPhaselists = 0;
                            string maSteplists = "";
                            var taskdetails = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityTaskDao>() where data1.ID == Convert.ToInt32(obj.EntityId) select data1.TaskType).FirstOrDefault().ToString();
                            var tasktype2 = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where data1.Id == Convert.ToInt32(taskdetails) select data1.Caption).FirstOrDefault().ToString();
                            var Userid1 = (from tt in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<UserDao>() where tt.Id == Convert.ToInt32(obj.Actorid) select tt).FirstOrDefault();
                            var taskinfo = (from data2 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityTaskDao>() where data2.ID == Convert.ToInt32(taskdetails) select data2).FirstOrDefault();
                            if (taskdetails == "3" || taskdetails == "32" || taskdetails == "36" || taskdetails == "37")
                            {
                                foreach (var mem in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityRoles)
                                {
                                    maPhaselists = mem.PhaseID;
                                    if (mem.StepId != null)
                                        if (maSteplists.Contains(mem.StepId.ToString()))
                                        { }
                                        else
                                            if (maSteplists.Length > 0)
                                                maSteplists = maSteplists + ',' + mem.StepId;
                                            else
                                                maSteplists = mem.StepId.ToString();
                                    if (mem.RoleID != 1)
                                    {
                                        if (members.Contains(mem.UserID.ToString()))
                                        { }
                                        else
                                        {

                                            if (members.Length > 0)
                                                members = members + ',' + mem.UserID;
                                            else
                                                members = mem.UserID.ToString();
                                            string usersteps = "";
                                            var linqsub = (from t in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityRoles where (t.UserID == mem.UserID && t.StepId > 0) select t.StepId).ToList();
                                            if (linqsub.Count() > 0)
                                                usersteps = string.Join(",", linqsub.Select(p => p));

                                            if (maPhaselists > 0 || ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).PhaseCount > 0 || obj.PhaseID > 0)
                                                notificationtypeidtempid = 56;
                                            else
                                                notificationtypeidtempid = 6;

                                            if (mem.RoleID != 1 && mem.UserID != ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).Actorid)
                                            {
                            
                                                marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, notificationtypeidtempid, false, false, tasktype2, "", obj.FromValue, "", 35, mem.UserID, 0, maPhaselists, usersteps);
                                                if (((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole != null)
                                                {
                                                    var listofotheruser = (from t in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole where (t.UserID == mem.UserID && t.RoleID != 1 && t.PhaseID == mem.PhaseID && t.StepId == mem.StepId) select t.UserID).ToList();
                                                    foreach (var tusers in listofotheruser)
                                                    {
                                                        marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, notificationtypeidtempid, false, false, tasktype2, "", obj.FromValue, "", 35, tusers, 0, maPhaselists, usersteps);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (var mem in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityRoles)
                                {
                                    if (mem.RoleID != 1)
                                    {
                                        members = members + ',' + mem.UserID;

                                    }
                                    if (maPhaselists > 0 || ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).PhaseCount > 0 || obj.PhaseID > 0)
                                        notificationtypeidtempid = 56;
                                    else
                                        notificationtypeidtempid = 6;
                                    if (mem.RoleID != 1 && mem.UserID != ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).Actorid)
                                    {
                                        marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, notificationtypeidtempid, false, false, tasktype2, "", obj.FromValue, "", 35, mem.UserID, 0, maPhaselists, maSteplists);
                                    }


                                }
                            }

                            if (maPhaselists > 0 || ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).PhaseCount > 0 || obj.PhaseID > 0)
                                newsFeedTemplateId = 66;
                            else
                                newsFeedTemplateId = 17;

                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, tasktype2, obj.AttributeName, obj.FromValue, members, 0, 0, null, 0, maPhaselists, maSteplists);
                            txInnerloop.Commit();

                            break;
                        case "Task Status change":
                            if ((((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).Tasktatus) == "RevokeTask")
                                newsFeedTemplateId = 41;
                            else
                                if ((obj.PhaseID > 0 || obj.PhaseCount > 0) && (((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).Tasktatus) == "Approved")
                                    newsFeedTemplateId = 67;
                                else
                                    newsFeedTemplateId = 22;
                            MailServer mailServer1stat = new MailServer();
                            IList<TaskMembersDao> TaskMembers = new List<TaskMembersDao>();
                            var ttypetasktype = (from ent in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where ent.Id == obj.AttributeId select ent.Caption).FirstOrDefault();

                            for (int i = 0; i < obj.obj2.Count(); i++)
                            {

                                TaskMembersDao dao = new TaskMembersDao();
                                dao.RoleID = ((TaskMembersDao)(obj.obj2[i])).RoleID;
                                dao.UserID = ((TaskMembersDao)(obj.obj2[i])).UserID;
                                dao.ID = ((TaskMembersDao)(obj.obj2[i])).ID;
                                dao.OverdueMailSent = ((TaskMembersDao)(obj.obj2[i])).OverdueMailSent;
                                TaskMembers.Add(dao);

                            }
                            int mailtemplateidfortask = 0;
                            int notificationtypeidfortask = 0;
                            if ((((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).Tasktatus) == "RevokeTask")
                            {
                                ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).Tasktatus = "In Progress";
                                notificationtypeidfortask = 47;
                                mailtemplateidfortask = 39;
                            }
                            else
                            {
                                if ((obj.PhaseID > 0 || obj.PhaseCount > 0) && (((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).Tasktatus) == "Approved")
                                    notificationtypeidfortask = 57;
                                else
                                    notificationtypeidfortask = 7;
                                mailtemplateidfortask = 36;
                            }

                            if ((((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).NoneedNotification) == false)
                            {
                                marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, notificationtypeidfortask, false, false, obj.action, "", obj.FromValue, ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).Tasktatus, mailtemplateidfortask, 0, 0, obj.PhaseID, obj.StepID.ToString());
                            }
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, ttypetasktype, obj.FromValue, ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).Tasktatus.ToString(), 0, 0, null, 0, obj.PhaseID, obj.StepID.ToString());
                            txInnerloop.Commit();
                            break;
                        case "Task Phase Status change":
                            int pentityid = 0;
                            int pnewfeedtempid = 0;
                            int pnotificationtypeidtemp1id = 4;
                            String ptaskownerName = "";
                            String ptaskownerEmail = "";
                            string ptaskImagePath = "";
                            String ptasklogopath = "";
                            MailServer pmailServer = new MailServer();
                            MailHolder ptaskmailHolder = new MailHolder();
                            ptaskmailHolder.PrimaryTo = new List<string>();

                            var pUserid = (from tt in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<UserDao>() where tt.Id == Convert.ToInt32(obj.Actorid) select tt).FirstOrDefault();
                            //var taskowner = (from tt in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole where tt.RoleID == 1 && tt.TaskID == Convert.ToInt32(obj.EntityId) select tt).FirstOrDefault();
                            IList<TaskMembersDao> taskowner = new List<TaskMembersDao>();
                            taskmemdel = (from mem in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<TaskMembersDao>() where mem.TaskID == obj.EntityId && mem.RoleID == 1 select mem).ToList<TaskMembersDao>();
                            int taskownerid = taskmemdel[0].UserID;
                            var ttypetasktypep = (from ent in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where ent.Id == obj.AttributeId select ent.Caption).FirstOrDefault();
                            UserNotificationDao puserNotify = new UserNotificationDao();

                            string taskmembersp = "";
                            int Phaselistsp = 0;
                            string Steplistsp = "";


                            if (((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole != null)
                            {
                                foreach (var mem in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole)
                                {
                                    Phaselistsp = mem.PhaseID;
                                    if (mem.StepId != null)
                                    {
                                        if (Steplistsp.Contains(mem.StepId.ToString()))
                                        { }
                                        else
                                            if (Steplistsp.Length > 0)
                                                Steplistsp = Steplistsp + ',' + mem.StepId;
                                            else
                                                Steplistsp = mem.StepId.ToString();
                                    }
                                    if (mem.RoleID != 1)
                                    {
                                        if (taskmembersp.Contains(mem.UserID.ToString()))
                                        { }
                                        else
                                        {
                                            if (taskmembersp.Length > 0)
                                                taskmembersp = taskmembersp + ',' + mem.UserID;
                                            else
                                                taskmembersp = mem.UserID.ToString();
                                            string usersteps = "";
                                            var linqsub = (from t in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole where (t.UserID == mem.UserID && t.StepId > 0) select t.StepId).ToList();
                                            if (linqsub.Count() > 0)
                                                usersteps = string.Join(",", linqsub.Select(p => p));

                                            if (Phaselistsp > 0 || obj.PhaseCount > 0 || obj.PhaseID > 0)
                                                pnotificationtypeidtemp1id = 58;
                                            else
                                                pnotificationtypeidtemp1id = 58;
                                            if (mem.RoleID != 1 && mem.UserID != ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).Actorid && mem.UserID != taskownerid)
                                            {
                                                marcomManager.CommonManager.UserNotification_Insert(((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).EntityId, obj.Actorid, DateTimeOffset.Now, pnotificationtypeidtemp1id, false, false, ttypetasktypep, "", obj.FromValue, taskmembersp, 35, mem.UserID, 0, Phaselistsp, "");
                                            }
                                        }
                                    }
                                }


                            }


                            if (ttypetasktypep != "Asset approval task")
                            {

                                if (taskmembersp == "" && taskmembersp.Length == 0 && ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole.Count == 0)
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, 38, ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).EntityId, ttypetasktypep, obj.AttributeName, obj.FromValue, taskmembersp, 0, 0, null, 0, Phaselistsp, Steplistsp);
                                else
                                {
                                    if (Phaselistsp > 0 || obj.PhaseCount > 0 || obj.PhaseID > 0)
                                        pnewfeedtempid = 68;
                                    else
                                        pnewfeedtempid = 68;
                                    if (((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).AttributeId != 37)
                                        marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, pnewfeedtempid, ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).EntityId, ttypetasktypep, obj.AttributeName, obj.FromValue, taskmembersp, 0, 0, null, 0, Phaselistsp, "");
                                }
                            }




                            txInnerloop.Commit();
                            break;
                        case "Task Phase Assigned":
                            int paentityid = 0;
                            int panewfeedtempid = 0;
                            int panotificationtypeidtemp1id = 4;
                            String pataskownerName = "";
                            String pataskownerEmail = "";
                            string pataskImagePath = "";
                            String patasklogopath = "";
                            MailServer pamailServer = new MailServer();
                            MailHolder pataskmailHolder = new MailHolder();
                            pataskmailHolder.PrimaryTo = new List<string>();
                            IList<TaskMembersDao> taskowner1 = new List<TaskMembersDao>();
                            taskmemdel = (from mem in txInnerloop.PersistenceManager.TaskRepository[marcomManager.User.TenantID].Query<TaskMembersDao>() where mem.TaskID == obj.EntityId && mem.RoleID == 1 select mem).ToList<TaskMembersDao>();
                            int taskownerid1 = taskmemdel[0].UserID;
                            // var taskownerids = (from tt in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole where tt.RoleID == 1 select tt.UserID).FirstOrDefault();
                            var paUserid = (from tt in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<UserDao>() where tt.Id == Convert.ToInt32(obj.Actorid) select tt).FirstOrDefault();
                            var tatypetasktypep = (from ent in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where ent.Id == obj.AttributeId select ent.Caption).FirstOrDefault();
                            UserNotificationDao pauserNotify = new UserNotificationDao();

                            string taskmemberspa = "";
                            int Phaselistspa = 0;
                            string Steplistspa = "";


                            if (((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole != null)
                            {
                                foreach (var mem in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole)
                                {
                                    Phaselistspa = mem.PhaseID;
                                    if (mem.StepId != null)
                                    {
                                        if (Steplistspa.Contains(mem.StepId.ToString()))
                                        { }
                                        else
                                            if (Steplistspa.Length > 0)
                                                Steplistspa = Steplistspa + ',' + mem.StepId;
                                            else
                                                Steplistspa = mem.StepId.ToString();
                                    }
                                    if (mem.RoleID != 1)
                                    {
                                        if (taskmemberspa.Contains(mem.UserID.ToString()))
                                        { }
                                        else
                                        {
                                            if (taskmemberspa.Length > 0)
                                                taskmemberspa = taskmemberspa + ',' + mem.UserID;
                                            else
                                                taskmemberspa = mem.UserID.ToString();
                                            string usersteps = "";
                                            var linqsub = (from t in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole where (t.UserID == mem.UserID && t.StepId > 0) select t.StepId).ToList();
                                            if (linqsub.Count() > 0)
                                                usersteps = string.Join(",", linqsub.Select(p => p));

                                            if (Phaselistspa > 0 || obj.PhaseCount > 0 || obj.PhaseID > 0)
                                                pnotificationtypeidtemp1id = 55;
                                            else
                                                pnotificationtypeidtemp1id = 4;
                                            if (mem.RoleID != 1 && mem.UserID != ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).Actorid && mem.UserID != taskownerid1)
                                            {
                                                marcomManager.CommonManager.UserNotification_Insert(((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).EntityId, obj.Actorid, DateTimeOffset.Now, pnotificationtypeidtemp1id, false, false, tatypetasktypep, "", obj.FromValue, taskmemberspa, 35, mem.UserID, 0, Phaselistspa, Steplistspa);
                                            }
                                        }
                                    }
                                }


                            }


                            if (tatypetasktypep != "Asset approval task")
                            {

                                if (taskmemberspa == "" && taskmemberspa.Length == 0 && ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole.Count == 0)
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, 38, ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).EntityId, tatypetasktypep, obj.AttributeName, obj.FromValue, taskmemberspa, 0, 0, null, 0, Phaselistspa, Steplistspa);
                                else
                                {
                                    if (Phaselistspa > 0 || obj.PhaseCount > 0 || obj.PhaseID > 0)
                                        pnewfeedtempid = 65;
                                    else
                                        pnewfeedtempid = 13;
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, pnewfeedtempid, ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).EntityId, tatypetasktypep, obj.AttributeName, obj.FromValue, taskmemberspa, 0, 0, null, 0, Phaselistspa, Steplistspa);
                                }
                            }




                            txInnerloop.Commit();
                            break;


                        case "Tasks":
                            int entityid = 0;
                            int newfeedtempid = 0;
                            int notificationtypeidtemp1id = 4;
                            String taskownerName = "";
                            String taskownerEmail = "";
                            string taskImagePath = "";
                            String tasklogopath = "";
                            MailServer mailServer = new MailServer();
                            MailHolder taskmailHolder = new MailHolder();
                            taskmailHolder.PrimaryTo = new List<string>();

                            var Userid = (from tt in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<UserDao>() where tt.Id == Convert.ToInt32(obj.Actorid) select tt).FirstOrDefault();
                            UserNotificationDao userNotify = new UserNotificationDao();
                            if (((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).iEntityTask != null)
                            {
                                foreach (var task in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).iEntityTask)
                                {
                                    string taskmembers = "";
                                    int Phaselists = 0;
                                    string Steplists = "";


                                    var tasktype = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where data1.Id == Convert.ToInt32(obj.TypeName) select data1.Caption).FirstOrDefault().ToString();
                                    if (task.TaskType == 3 || task.TaskType == 32 || task.TaskType == 36 || task.TaskType == 37)
                                    {
                                        if (((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole != null)
                                        {
                                            foreach (var mem in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole)
                                            {
                                                Phaselists = mem.PhaseID;
                                                if (mem.StepId != null)
                                                {
                                                    if (Steplists.Contains(mem.StepId.ToString()))
                                                    { }
                                                    else
                                                        if (Steplists.Length > 0)
                                                            Steplists = Steplists + ',' + mem.StepId;
                                                        else
                                                            Steplists = mem.StepId.ToString();
                                                }
                                                if (mem.RoleID != 1)
                                                {
                                                    if (taskmembers.Contains(mem.UserID.ToString()))
                                                    { }
                                                    else
                                                    {
                                                        if (taskmembers.Length > 0)
                                                            taskmembers = taskmembers + ',' + mem.UserID;
                                                        else
                                                            taskmembers = mem.UserID.ToString();
                                                        string usersteps = "";
                                                        var linqsub = (from t in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole where (t.UserID == mem.UserID && t.StepId > 0) select t.StepId).ToList();
                                                        if (linqsub.Count() > 0)
                                                            usersteps = string.Join(",", linqsub.Select(p => p));

                                                        if (Phaselists > 0 || obj.PhaseCount > 0 || obj.PhaseID > 0)
                                                            notificationtypeidtemp1id = 55;
                                                        else
                                                            notificationtypeidtemp1id = 4;
                                                        if (mem.RoleID != 1 && mem.UserID != ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).Actorid)
                                                        {
                                                            marcomManager.CommonManager.UserNotification_Insert(task.ID, obj.Actorid, DateTimeOffset.Now, notificationtypeidtemp1id, false, false, tasktype, "", obj.FromValue, taskmembers, 35, mem.UserID, 0, Phaselists, usersteps);
                                                        }
                                                    }
                                                }
                                            }


                                        }
                                    }
                                    else
                                    {
                                        if (((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole != null)
                                        {
                                            foreach (var mem in ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole)
                                            {
                                                if (mem.RoleID != 1)
                                                {
                                                    if (taskmembers.Length > 0)
                                                        taskmembers = taskmembers + ',' + mem.UserID;
                                                    else
                                                        taskmembers = mem.UserID.ToString();

                                                }
                                                if (Phaselists > 0 || obj.PhaseCount > 0 || obj.PhaseID > 0)
                                                    notificationtypeidtempid = 55;
                                                else
                                                    notificationtypeidtempid = 4;
                                                if (mem.RoleID != 1 && mem.UserID != ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).Actorid)
                                                {
                                                    marcomManager.CommonManager.UserNotification_Insert(task.ID, obj.Actorid, DateTimeOffset.Now, notificationtypeidtemp1id, false, false, tasktype, "", obj.FromValue, taskmembers, 35, mem.UserID, 0, Phaselists, Steplists);
                                                }
                                            }


                                        }
                                    }
                                    if (tasktype != "Asset approval task")
                                    {

                                        if (taskmembers == "" && taskmembers.Length == 0 && ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole.Where(s => s.RoleID != 1).Count() == 0)
                                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, 38, task.ID, tasktype, obj.AttributeName, obj.FromValue, taskmembers, 0, 0, null, 0, Phaselists, Steplists);
                                        else
                                        {
                                            if (Phaselists > 0 || obj.PhaseCount > 0 || obj.PhaseID > 0)
                                                newfeedtempid = 65;
                                            else
                                                newfeedtempid = 13;
                                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newfeedtempid, task.ID, tasktype, obj.AttributeName, obj.FromValue, taskmembers, 0, 0, null, 0, Phaselists, Steplists);
                                        }
                                    }
                                    else
                                    {

                                        if (taskmembers == "" && taskmembers.Length == 0 && ((BrandSystems.Marcom.Core.Common.WorkFlowNotifyHolder)(obj)).ientityTaskRole.Where(s => s.RoleID != 1).Count() == 0)
                                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, 110, task.ID, tasktype, obj.AttributeName, obj.FromValue, taskmembers, 0, obj.AssociatedEntityId, null, obj.Version, Phaselists, Steplists);
                                        else
                                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, 109, task.ID, tasktype, obj.AttributeName, obj.FromValue, taskmembers, 0, obj.AssociatedEntityId, null, obj.Version, Phaselists, Steplists);
                                    }
                                }
                            }

                            txInnerloop.Commit();
                            break;
                        case "entity member added":

                            newsFeedTemplateId = 3;
                            var temp = obj.obj2.ToList();
                            string entityMembers = "";
                            foreach (var x in temp)
                            {
                                UserNotificationDao userNotify1 = new UserNotificationDao();

                                if (Convert.ToInt32(((BrandSystems.Marcom.Dal.Access.Model.EntityRoleUserDao)(x)).Userid) != obj.Actorid)
                                {
                                    if (Convert.ToInt32(((BrandSystems.Marcom.Dal.Access.Model.EntityRoleUserDao)(x)).IsInherited) == 0)
                                    {
                                        entityMembers = entityMembers + "," + Convert.ToInt32(((BrandSystems.Marcom.Dal.Access.Model.EntityRoleUserDao)(x)).Userid);
                                        marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 14, false, false, "", "", "", Convert.ToString(((BrandSystems.Marcom.Dal.Access.Model.EntityRoleUserDao)(x)).Roleid), 8, Convert.ToInt32(((BrandSystems.Marcom.Dal.Access.Model.EntityRoleUserDao)(x)).Userid));
                                    }

                                }

                            }
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, entityMembers, obj.Userid);

                            txInnerloop.Commit();
                            break;
                        case "Changed the task details":
                            newsFeedTemplateId = 39;
                            var entdets = (from tt in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<EntityDao>() where tt.Id == obj.EntityId select tt.Typeid).FirstOrDefault();
                            var typedetails = (from tt in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<EntityTypeDao>() where tt.Id == entdets select tt.Caption).FirstOrDefault();
                            if (obj.FromValue != obj.ToValue)
                            {
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, typedetails.ToString(), obj.AttributeName, obj.FromValue, obj.ToValue,0,obj.AssociatedEntityId);
                                marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 5, false, false, typedetails.ToString(), obj.AttributeName, obj.FromValue, obj.ToValue, 50);
                            }
                            txInnerloop.Commit();

                            break;
                        case "member removed":
                            IList<UserNotificationDao> userNotifylist = new List<UserNotificationDao>();
                            // marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, 5, obj.EntityId, "", "", "", "", obj.Userid);
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, 5, obj.EntityId, obj.action, "", "", obj.ToValue, obj.Userid);
                            if (obj.Userid != obj.Actorid)
                            {
                                marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 16, false, false, "", "", "", obj.ToValue, 52, obj.Userid);
                            }
                            txInnerloop.Commit();
                            break;
                        case "member added":
                            newsFeedTemplateId = 3;

                            if (obj.Userid != obj.Actorid)
                            {
                                if (obj.memberisinherited == false)
                                {
                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 14, false, false, "", "", "", obj.ToValue, 8, obj.Userid);

                                }
                            }
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, obj.Userid);

                            txInnerloop.Commit();
                            break;
                        case "predefobjective update":
                            newsFeedTemplateId = 50;
                            if (obj.FromValue != obj.ToValue)
                            {
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.objectiveId);
                            }
                            txInnerloop.Commit();
                            break;


                        case "Additional objective update":
                            newsFeedTemplateId = 56;
                            if (obj.FromValue != obj.ToValue)
                            {
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.objectiveId);
                            }
                            txInnerloop.Commit();
                            break;
                        case "objective status update":
                            newsFeedTemplateId = 23;
                            if (obj.FromValue != obj.ToValue)
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                            txInnerloop.Commit();
                            break;
                        case "objective summary update":
                            newsFeedTemplateId = 4;
                            if (obj.FromValue != obj.ToValue)
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                            txInnerloop.Commit();
                            break;
                        case "calender summary update":
                            newsFeedTemplateId = 4;
                            if (obj.FromValue != obj.ToValue)
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                            txInnerloop.Commit();
                            break;

                        case "Update Member Status":
                            newsFeedTemplateId = 4;
                            if (obj.FromValue != obj.ToValue)
                            {
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                                txInnerloop.Commit();
                            }
                            break;

                        case "entity status update":
                            newsFeedTemplateId = 4;
                            if (obj.FromValue != obj.ToValue)
                            {
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                                marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 51, false, false, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 53);
                            }
                            txInnerloop.Commit();
                            break;
                        case "update attribute group":


                            for (int zz = 0; zz < obj.attrgrprel.Count; zz++)
                            {
                                newsFeedTemplateId = 53;
                                obj.AttributeName = obj.attrgrprel.ElementAt(zz).AttributeCaption;
                                if (Convert.ToInt32(obj.attrgrprel.ElementAt(zz).AttributeTypeID) == 6)
                                {
                                    try
                                    {
                                        IList<TreeLevelDao> attr = new List<TreeLevelDao>();


                                        IList<TreeNodeDao> newValues = new List<TreeNodeDao>();
                                        if (obj.attrdata.Count() > 0)
                                        {
                                            for (int i = 0; i < obj.attrdata.Count(); i++)
                                            {
                                                newValues.Add((from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeNodeDao>() where item.Id == Convert.ToInt32(obj.obj2[i]) select item).FirstOrDefault());


                                            }
                                        }

                                        for (int i = 0; i < newValues.Count(); i++)
                                        {
                                            if (newValues[i] != null)
                                            {
                                                attr.Add(txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].GetbyCriteria<TreeLevelDao>(TreeLevelDao.PropertyNames.AttributeID, TreeLevelDao.PropertyNames.Level, Convert.ToInt32(obj.AttributeId), Convert.ToInt32(newValues[i].Level)));
                                            }
                                        }


                                        IList<TreeNodeDao> oldValues = new List<TreeNodeDao>();
                                        if (obj.obj3.Count > 0)
                                        {
                                            foreach (var ele in obj.obj3)
                                            {
                                                oldValues.Add((from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeNodeDao>() where item.Id == Convert.ToInt32(((BrandSystems.Marcom.Dal.Metadata.Model.AttrGroupTreeValueDao)(ele)).Nodeid) select item).FirstOrDefault());
                                            }
                                        }

                                        for (int i = 0; i < newValues.Count(); i++)
                                        {
                                            if (newValues[i] != null)
                                            {
                                                obj.AttributeName = attr[i].LevelName;
                                                obj.ToValue = (newValues[i].Caption == null ? "-" : newValues[i].Caption.ToString().Trim());
                                                obj.FromValue = (i > oldValues.Count() - 1 || oldValues.Count() == 0 || oldValues[i] == null ? "-" : oldValues[i].Caption.ToString().Trim());
                                                if (obj.FromValue != obj.ToValue)
                                                {
                                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                                                }
                                            }
                                        }


                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }
                                else if (Convert.ToInt32(obj.attrgrprel.ElementAt(zz).AttributeTypeID) == 3)
                                {
                                    obj.FromValue = "";
                                    obj.ToValue = "";
                                    int optionIdforfeed = 0;
                                    optionIdforfeed = (obj.attrgrprel.ElementAt(zz).AttributeValue == null ? 0 : obj.attrgrprel.ElementAt(zz).AttributeValue);
                                    var optionoldvalue = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.OptionDao>(optionIdforfeed);

                                    obj.FromValue = (optionoldvalue == null ? "-" : Convert.ToString(optionoldvalue.Caption));
                                    var newvallist = (from item in obj.attrdata where item.ID == obj.attrgrprel.ElementAt(zz).AttributeID select item).FirstOrDefault();
                                    int optionid = newvallist.Value;
                                    var optionnewvalue = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.OptionDao>(Convert.ToInt32(optionid));
                                    obj.ToValue = Convert.ToString(optionnewvalue.Caption);

                                    if (obj.FromValue != obj.ToValue)
                                    {
                                        marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, 0, obj.AttributeRecordName);
                                        txInnerloop.Commit();
                                    }
                                }
                                else if (Convert.ToInt32(obj.attrgrprel.ElementAt(zz).AttributeTypeID) == 1 || Convert.ToInt32(obj.attrgrprel.ElementAt(zz).AttributeTypeID) == 2)
                                {
                                    obj.FromValue = "";
                                    obj.ToValue = "";
                                    if (obj.attrgrprel.ElementAt(zz).AttributeID == 76)
                                        newsFeedTemplateId = 55;
                                    else
                                        newsFeedTemplateId = 53;
                                    if (obj.attrgrprel.ElementAt(zz).AttributeValue != obj.attrdata[zz].Value)
                                    {
                                        obj.FromValue = obj.attrgrprel.ElementAt(zz).AttributeValue;
                                        obj.ToValue = obj.attrdata[zz].Value;
                                        marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, 0, obj.AttributeRecordName);
                                        txInnerloop.Commit();
                                    }

                                }

                                else if (Convert.ToInt32(obj.attrgrprel.ElementAt(zz).AttributeTypeID) == 4)
                                {
                                    obj.FromValue = "";
                                    obj.ToValue = "";
                                    int[] oldvalue = obj.attrgrprel.ElementAt(zz).AttributeValue;
                                    foreach (var ele in oldvalue)
                                    {
                                        var oldOptionName = (from item in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<BrandSystems.Marcom.Dal.Metadata.Model.OptionDao>() where item.Id == Convert.ToInt32(ele) select item.Caption).FirstOrDefault();
                                        obj.FromValue = obj.FromValue + ',' + oldOptionName;
                                    }
                                    var newval = (from item in obj.attrdata where item.ID == obj.attrgrprel.ElementAt(zz).AttributeID select item).ToList();
                                    foreach (var ele in newval.AsEnumerable())
                                    {
                                        int value = Convert.ToInt32(ele.Value);
                                        var optionName = (from item in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<BrandSystems.Marcom.Dal.Metadata.Model.OptionDao>() where item.Id == value select item.Caption).FirstOrDefault();
                                        obj.ToValue = obj.ToValue + ',' + optionName;
                                    }
                                    obj.FromValue = ((obj.FromValue == "" || obj.FromValue == null) ? "-" : obj.FromValue.Trim().TrimStart(','));
                                    obj.ToValue = ((obj.ToValue == "" || obj.ToValue == null) ? "-" : obj.ToValue.Trim().TrimStart(','));
                                    if (obj.FromValue != obj.ToValue)
                                    {
                                        marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, 0, obj.AttributeRecordName);
                                        txInnerloop.Commit();
                                    }
                                }
                                else if (Convert.ToInt32(obj.attrgrprel.ElementAt(zz).AttributeTypeID) == 11)    //when uploading an image template is different
                                {
                                    obj.FromValue = "";
                                    obj.ToValue = "";
                                    newsFeedTemplateId = 54;
                                    if (obj.attrgrprel.ElementAt(zz).AttributeValue != obj.attrdata[zz].Value)
                                    {
                                        marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, 0, obj.AttributeRecordName);
                                        txInnerloop.Commit();
                                    }
                                }
                                else if (Convert.ToInt32(obj.attrgrprel.ElementAt(zz).AttributeTypeID) == 10)
                                {
                                    if (obj.attrgrprel.ElementAt(zz).AttributeValue != obj.attrdata[zz].Value)
                                    {
                                        obj.FromValue = obj.attrgrprel.ElementAt(zz).AttributeValue;
                                        obj.ToValue = obj.attrdata[zz].Value;
                                        marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, 0, obj.AttributeRecordName);
                                        txInnerloop.Commit();
                                    }
                                }
                                else if (Convert.ToInt32(obj.attrgrprel.ElementAt(zz).AttributeTypeID) == 5)
                                {
                                    obj.FromValue = (obj.attrgrprel.ElementAt(zz).AttributeValue == null ? null : Convert.ToDateTime(obj.attrgrprel.ElementAt(zz).AttributeValue).ToString("MM/dd/yyyy"));
                                    obj.ToValue = (obj.attrdata[zz].Value == null ? null : Convert.ToDateTime(obj.attrdata[zz].Value).ToString("MM/dd/yyyy"));
                                    if (obj.FromValue != obj.ToValue)
                                    {

                                        marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, 0, obj.AttributeRecordName);
                                        txInnerloop.Commit();
                                    }
                                }
                                else if (Convert.ToInt32(obj.attrgrprel.ElementAt(zz).AttributeTypeID) == 12)
                                {
                                    IList<TreeLevelDao> attr = new List<TreeLevelDao>();
                                    IList<TreeNodeDao> oldValues = new List<TreeNodeDao>();
                                    IList<TreeNodeDao> newValues = new List<TreeNodeDao>();
                                    if (obj.attrgrprel.ElementAt(zz).AttributeValue.Count > 0)
                                    {
                                        for (int i = 0; i < obj.attrgrprel.ElementAt(zz).AttributeValue.Count; i++)
                                        {
                                            int treeNodeId = obj.attrgrprel.ElementAt(zz).AttributeValue[i];
                                            oldValues.Add((from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeNodeDao>() where item.Id == Convert.ToInt32(treeNodeId) select item).FirstOrDefault());

                                        }
                                    }
                                    var newval = (from item in obj.attrdata where item.ID == obj.attrgrprel.ElementAt(zz).AttributeID select item).ToList();
                                    if (newval.Count > 0)
                                    {
                                        foreach (var ele in newval)
                                        {
                                            int elementId = ele.Value[0];
                                            newValues.Add((from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeNodeDao>() where item.Id == Convert.ToInt32(elementId) select item).FirstOrDefault());
                                        }
                                    }
                                    for (int i = 0; i < newValues.Count(); i++)
                                    {

                                        if (newValues[i] != null)
                                            attr.Add(txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].GetbyCriteria<TreeLevelDao>(TreeLevelDao.PropertyNames.AttributeID, TreeLevelDao.PropertyNames.Level, Convert.ToInt32(obj.attrgrprel.ElementAt(zz).AttributeID), Convert.ToInt32(newValues[i].Level)));
                                        else
                                            if (oldValues.Count != 0)
                                                attr.Add(txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].GetbyCriteria<TreeLevelDao>(TreeLevelDao.PropertyNames.AttributeID, TreeLevelDao.PropertyNames.Level, Convert.ToInt32(obj.attrgrprel.ElementAt(zz).AttributeID), Convert.ToInt32(oldValues[i].Level)));
                                    }

                                    var maxLevelCount = obj.attrgrprel.ElementAt(zz).Levels.Count;
                                    IList<TreeNodeDao> multiSelectoldvalues = new List<TreeNodeDao>();
                                    IList<TreeNodeDao> multiSelectnewvalues = new List<TreeNodeDao>();
                                    for (int i = 0; i < newValues.Count(); i++)
                                    {
                                        if (newValues[i] != null)
                                        {
                                            if (i <= attr.Count - 1)
                                                obj.AttributeName = attr[i].LevelName;
                                            if (newValues[i].Level == maxLevelCount)
                                            {
                                                multiSelectnewvalues.Add(newValues[i]);
                                                if (oldValues.Count > 0)
                                                {
                                                    multiSelectoldvalues = (from item in oldValues where item.Level == maxLevelCount select item).ToList<TreeNodeDao>();
                                                }
                                            }
                                            else
                                            {
                                                obj.ToValue = (newValues[i].Caption == null ? "-" : newValues[i].Caption.ToString().Trim());
                                                obj.FromValue = (i > oldValues.Count() - 1 || oldValues.Count() == 0 || oldValues[i] == null ? "-" : oldValues[i].Caption.ToString().Trim());
                                                if (obj.FromValue != obj.ToValue)
                                                {
                                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, 0, obj.AttributeRecordName);

                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (i <= attr.Count - 1)
                                                obj.AttributeName = attr[i].LevelName;
                                            if (newValues[i] == null)
                                            {

                                                if (oldValues.Count > 0)
                                                {
                                                    if (oldValues[i].Level == maxLevelCount)
                                                        multiSelectoldvalues = (from item in oldValues where item.Level == maxLevelCount select item).ToList<TreeNodeDao>();
                                                    else
                                                    {
                                                        obj.ToValue = (newValues[i].Caption == null ? "-" : newValues[i].Caption.ToString().Trim());
                                                        obj.FromValue = (i > oldValues.Count() - 1 || oldValues.Count() == 0 || oldValues[i] == null ? "-" : oldValues[i].Caption.ToString().Trim());
                                                        if (obj.FromValue != obj.ToValue)
                                                        {
                                                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, 0, obj.AttributeRecordName);

                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (newValues[i].Level == maxLevelCount)
                                                {

                                                    if (oldValues.Count > 0)
                                                    {
                                                        multiSelectoldvalues = (from item in oldValues where item.Level == maxLevelCount select item).ToList<TreeNodeDao>();
                                                    }
                                                }
                                                else
                                                {
                                                    obj.ToValue = (newValues[i].Caption == null ? "-" : newValues[i].Caption.ToString().Trim());
                                                    obj.FromValue = (i > oldValues.Count() - 1 || oldValues.Count() == 0 || oldValues[i] == null ? "-" : oldValues[i].Caption.ToString().Trim());
                                                    if (obj.FromValue != obj.ToValue)
                                                    {
                                                        marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, 0, obj.AttributeRecordName);

                                                    }
                                                }
                                            }
                                        }
                                    }
                                    string multiselecttovalue = "";
                                    string multiselectfromvalue = "";
                                    for (int i = 0; i < multiSelectnewvalues.Count(); i++)
                                    {
                                        multiselecttovalue = (multiSelectnewvalues[i].Caption == null ? "-" : (multiselecttovalue + "," + multiSelectnewvalues[i].Caption.ToString().Trim()));
                                    }
                                    for (int i = 0; i < multiSelectoldvalues.Count(); i++)
                                    {
                                        multiselectfromvalue = (i > multiSelectoldvalues.Count() - 1 || multiSelectoldvalues.Count() == 0 ? "-" : (multiselectfromvalue + "," + multiSelectoldvalues[i].Caption.ToString().Trim()));
                                    }
                                    if (multiselecttovalue != multiselectfromvalue)
                                    {
                                        marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, multiselectfromvalue.Trim().TrimStart(','), multiselecttovalue.Trim().TrimStart(','), 0, 0, obj.AttributeRecordName);

                                    }

                                }
                                txInnerloop.Commit();
                            }
                            break;
                        case "metadata update":
                            newsFeedTemplateId = 4;
                            MailTemplateID = 24;

                            if (Convert.ToInt32(obj.Attributetypeid) == 6)
                            {
                                try
                                {
                                    IList<TreeLevelDao> attr = new List<TreeLevelDao>();


                                    IList<TreeNodeDao> newValues = new List<TreeNodeDao>();
                                    if (obj.obj2.Count() > 0)
                                    {
                                        for (int i = 0; i < obj.obj2.Count(); i++)
                                        {
                                            newValues.Add((from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeNodeDao>() where item.Id == Convert.ToInt32(obj.obj2[i]) select item).FirstOrDefault());


                                        }
                                    }

                                    for (int i = 0; i < newValues.Count(); i++)
                                    {
                                        if (newValues[i] != null)
                                        {
                                            attr.Add(txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].GetbyCriteria<TreeLevelDao>(TreeLevelDao.PropertyNames.AttributeID, TreeLevelDao.PropertyNames.Level, Convert.ToInt32(obj.AttributeId), Convert.ToInt32(newValues[i].Level)));
                                        }
                                    }


                                    IList<TreeNodeDao> oldValues = new List<TreeNodeDao>();
                                    if (obj.obj3.Count > 0)
                                    {
                                        foreach (var ele in obj.obj3)
                                        {
                                            oldValues.Add((from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeNodeDao>() where item.Id == Convert.ToInt32(((BrandSystems.Marcom.Dal.Metadata.Model.TreeValueDao)(ele)).Nodeid) select item).FirstOrDefault());
                                        }
                                    }

                                    for (int i = 0; i < newValues.Count(); i++)
                                    {
                                        if (newValues[i] != null)
                                        {
                                            obj.AttributeName = attr[i].LevelName;
                                            obj.ToValue = (newValues[i].Caption == null ? "-" : newValues[i].Caption.ToString().Trim());
                                            obj.FromValue = (i > oldValues.Count() - 1 || oldValues.Count() == 0 || oldValues[i] == null ? "-" : oldValues[i].Caption.ToString().Trim());
                                            if (obj.FromValue != obj.ToValue)
                                            {
                                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                                            }
                                        }
                                    }


                                }
                                catch (Exception ex)
                                {

                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 3)
                            {

                                obj.AttributeName = obj.AttributeDetails[0].Caption;
                                if (obj.AttributeDetails[0].IsSpecial == true && obj.AttributeDetails[0].Id == Convert.ToInt32(SystemDefinedAttributes.Owner))
                                {
                                    var role = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.User.Model.UserDao>(Convert.ToInt32(obj.obj2[0]));
                                    obj.ToValue = (role == null ? "-" : role.FirstName + " " + role.LastName);
                                    if (obj.obj3.Count != 0)
                                    {
                                        var oldrole = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.User.Model.UserDao>(Convert.ToInt32(((BrandSystems.Marcom.Dal.Access.Model.EntityRoleUserDao)(obj.obj3[0])).Userid));
                                        obj.FromValue = (oldrole == null ? "-" : oldrole.FirstName + " " + oldrole.LastName);
                                    }
                                    else
                                    {
                                        obj.FromValue = obj.OldActorName;
                                    }
                                }
                                else
                                {
                                    var list = obj.obj3.Cast<Hashtable>();
                                    int oldoption = 0;
                                    foreach (var item in list)
                                    {
                                        oldoption = Convert.ToInt32(item["Attr_" + obj.AttributeId]);
                                    }

                                    var optionoldvalue = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.OptionDao>(oldoption);
                                    obj.FromValue = (optionoldvalue == null ? "-" : Convert.ToString(optionoldvalue.Caption));
                                    var optionnewvalue = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.OptionDao>(Convert.ToInt32(obj.obj2[0]));
                                    obj.ToValue = Convert.ToString(optionnewvalue.Caption);
                                }
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                                    // marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.EntityUpdated), false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID);
                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 1)
                            {
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                if (obj.AttributeDetails[0].IsSpecial == true && obj.AttributeDetails[0].Id == Convert.ToInt32(SystemDefinedAttributes.Name))
                                {
                                    foreach (var item in obj.obj3.Cast<Hashtable>())
                                    {

                                        obj.FromValue = Convert.ToString(item["Name"]);
                                    }

                                }
                                else
                                {
                                    var list = obj.obj3.Cast<Hashtable>();
                                    string oldoption = "";
                                    foreach (var item in list)
                                    {
                                        oldoption = Convert.ToString(item["Attr_" + obj.AttributeId]);
                                        obj.FromValue = (oldoption == "" || oldoption == null) ? "-" : oldoption;
                                    }
                                }
                                obj.ToValue = (string)obj.obj2[0];
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                                    //  marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.EntityUpdated), false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID);
                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 2)
                            {
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                if (obj.AttributeDetails[0].IsSpecial == true && obj.AttributeDetails[0].Id == Convert.ToInt32(SystemDefinedAttributes.Name))
                                {
                                    var oldname = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Planning.Model.BaseEntityDao>(Convert.ToInt32(((BrandSystems.Marcom.Dal.Planning.Model.BaseEntityDao)(obj.obj3[0])).Id));
                                    obj.FromValue = oldname.Name;
                                }
                                else
                                {
                                    var list = obj.obj3.Cast<Hashtable>();
                                    string oldoption = "";
                                    foreach (var item in list)
                                    {
                                        oldoption = Convert.ToString(item["Attr_" + obj.AttributeId]);
                                        obj.FromValue = (oldoption == "" || oldoption == null) ? "-" : oldoption;
                                    }
                                }
                                obj.ToValue = (string)obj.obj2[0];
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                                    // marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.EntityUpdated), false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID);
                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 19)
                            {
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                if (obj.AttributeDetails[0].IsSpecial == true && obj.AttributeDetails[0].Id == Convert.ToInt32(SystemDefinedAttributes.Name))
                                {
                                    var oldname = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Planning.Model.BaseEntityDao>(Convert.ToInt32(((BrandSystems.Marcom.Dal.Planning.Model.BaseEntityDao)(obj.obj3[0])).Id));
                                    obj.FromValue = oldname.Name;
                                }
                                else
                                {
                                    var list = obj.obj3.Cast<Hashtable>();
                                    string oldoption = "";
                                    foreach (var item in list)
                                    {
                                        // oldoption = Convert.ToString(Convert.ToInt32(item["Amount"]))+" "+ Convert.ToString(item["ShortName"]);
                                        oldoption = Convert.ToString((item["Amount"])) + " " + Convert.ToString(item["ShortName"]);
                                        obj.FromValue = (oldoption == "" || oldoption == null) ? "-" : oldoption;
                                    }
                                }
                                obj.ToValue = (string)obj.obj2[0] + " " + (string)obj.obj2[2];
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                                    // marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.EntityUpdated), false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID);
                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 9)
                            {
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                if (obj.AttributeDetails[0].IsSpecial == true && obj.AttributeDetails[0].Id == Convert.ToInt32(SystemDefinedAttributes.Name))
                                {
                                    var oldname = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Planning.Model.BaseEntityDao>(Convert.ToInt32(((BrandSystems.Marcom.Dal.Planning.Model.BaseEntityDao)(obj.obj3[0])).Id));
                                    obj.FromValue = oldname.Name;
                                }
                                else
                                {
                                    var list = obj.obj3.Cast<Hashtable>();
                                    string oldoption = "";
                                    foreach (var item in list)
                                    {
                                        oldoption = Convert.ToString(item["Attr_" + obj.AttributeId]);
                                        obj.FromValue = oldoption;
                                    }
                                }
                                obj.ToValue = Convert.ToBoolean(obj.obj2[0]).ToString();
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 4)
                            {
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                foreach (var ele in obj.obj3)
                                {
                                    var oldOptionName = (from item in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<BrandSystems.Marcom.Dal.Metadata.Model.OptionDao>() where item.Id == Convert.ToInt32(ele) select item.Caption).FirstOrDefault();
                                    if (oldOptionName != null)
                                        obj.FromValue = obj.FromValue + ',' + oldOptionName;
                                }
                                foreach (var ele in obj.obj2)
                                {
                                    var optionName = (from item in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<BrandSystems.Marcom.Dal.Metadata.Model.OptionDao>() where item.Id == Convert.ToInt32(ele) select item.Caption).FirstOrDefault();
                                    if (optionName != null)
                                        obj.ToValue = obj.ToValue + ',' + optionName;
                                }
                                obj.FromValue = ((obj.FromValue == "" || obj.FromValue == null) ? "-" : obj.FromValue.Trim().TrimStart(','));
                                obj.ToValue = ((obj.ToValue == "" || obj.ToValue == null) ? "-" : obj.ToValue.Trim().TrimStart(','));
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                                    //  marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.EntityUpdated), false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID);
                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 5)
                            {
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                var list = obj.obj3.Cast<Hashtable>();
                                string oldoption = "-";
                                foreach (var item in list)
                                {

                                    oldoption = Convert.ToDateTime(item["Attr_" + obj.AttributeId]).ToString("yyyy-MM-dd");
                                }
                                obj.FromValue = oldoption == "1/1/1900 12:00:00 AM" ? "-" : oldoption;
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                                    // marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.EntityUpdated), false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID);
                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 10)
                            {

                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                                    // marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.EntityUpdated), false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID);
                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 8)
                            {
                                var tovalue = obj.obj3.Cast<Hashtable>();
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                string oldoption = "-";
                                foreach (var item in tovalue)
                                {
                                    oldoption = Convert.ToString(Convert.ToInt32(item["Attr_" + obj.AttributeId]));
                                    obj.FromValue = (oldoption == "" || oldoption == null) ? "-" : oldoption;
                                }
                                obj.ToValue = obj.ToValue;
                                if (obj.FromValue != obj.ToValue)
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                                  
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 12)
                            {

                                IList<TreeLevelDao> attr = new List<TreeLevelDao>();


                                IList<TreeNodeDao> newValues = new List<TreeNodeDao>();
                                if (obj.obj2.Count() > 0)
                                {
                                    for (int i = 0; i < obj.obj2.Count(); i++)
                                    {
                                        newValues.Add((from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeNodeDao>() where item.Id == Convert.ToInt32(obj.obj2[i]) select item).FirstOrDefault());

                                    }
                                }
                                IList<TreeNodeDao> oldValues = new List<TreeNodeDao>();
                                if (obj.obj3.Count > 0)
                                {
                                    foreach (var ele in obj.obj3)
                                    {
                                        oldValues.Add((from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeNodeDao>() where item.Id == Convert.ToInt32(((BrandSystems.Marcom.Dal.Metadata.Model.TreeValueDao)(ele)).Nodeid) select item).FirstOrDefault());
                                    }
                                }
                                for (int i = 0; i < newValues.Count(); i++)
                                {

                                    if (newValues[i] != null)
                                        attr.Add(txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].GetbyCriteria<TreeLevelDao>(TreeLevelDao.PropertyNames.AttributeID, TreeLevelDao.PropertyNames.Level, Convert.ToInt32(obj.AttributeId), Convert.ToInt32(newValues[i].Level)));
                                    else
                                        if (oldValues.Count != 0)
                                            attr.Add(txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].GetbyCriteria<TreeLevelDao>(TreeLevelDao.PropertyNames.AttributeID, TreeLevelDao.PropertyNames.Level, Convert.ToInt32(obj.AttributeId), Convert.ToInt32(oldValues[i].Level)));
                                }

                                var maxLevelCount = (from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeLevelDao>()
                                                     where item.AttributeID == obj.AttributeId
                                                     select item.Level).Max();
                                IList<TreeNodeDao> multiSelectoldvalues = new List<TreeNodeDao>();
                                IList<TreeNodeDao> multiSelectnewvalues = new List<TreeNodeDao>();
                                for (int i = 0; i < newValues.Count(); i++)
                                {
                                    if (newValues[i] != null)
                                    {
                                        if (i <= attr.Count - 1)
                                            obj.AttributeName = attr[i].LevelName;
                                        if (newValues[i].Level == maxLevelCount)
                                        {
                                            multiSelectnewvalues.Add(newValues[i]);
                                            if (oldValues.Count > 0)
                                            {
                                                multiSelectoldvalues = (from item in oldValues where item.Level == maxLevelCount select item).ToList<TreeNodeDao>();
                                            }
                                        }
                                        else
                                        {
                                            obj.ToValue = (newValues[i].Caption == null ? "-" : newValues[i].Caption.ToString().Trim());
                                            obj.FromValue = (i > oldValues.Count() - 1 || oldValues.Count() == 0 || oldValues[i] == null ? "-" : oldValues[i].Caption.ToString().Trim());
                                            if (obj.FromValue != obj.ToValue)
                                            {
                                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);

                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (i <= attr.Count - 1)
                                            obj.AttributeName = attr[i].LevelName;
                                        if (newValues[i] == null)
                                        {

                                            if (oldValues.Count > 0)
                                            {
                                                if (oldValues[i].Level == maxLevelCount)
                                                    multiSelectoldvalues = (from item in oldValues where item.Level == maxLevelCount select item).ToList<TreeNodeDao>();
                                                else
                                                {
                                                    obj.ToValue = (newValues[i].Caption == null ? "-" : newValues[i].Caption.ToString().Trim());
                                                    obj.FromValue = (i > oldValues.Count() - 1 || oldValues.Count() == 0 || oldValues[i] == null ? "-" : oldValues[i].Caption.ToString().Trim());
                                                    if (obj.FromValue != obj.ToValue)
                                                    {
                                                        marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);

                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (newValues[i].Level == maxLevelCount)
                                            {

                                                if (oldValues.Count > 0)
                                                {
                                                    multiSelectoldvalues = (from item in oldValues where item.Level == maxLevelCount select item).ToList<TreeNodeDao>();
                                                }
                                            }
                                            else
                                            {
                                                obj.ToValue = (newValues[i].Caption == null ? "-" : newValues[i].Caption.ToString().Trim());
                                                obj.FromValue = (i > oldValues.Count() - 1 || oldValues.Count() == 0 || oldValues[i] == null ? "-" : oldValues[i].Caption.ToString().Trim());
                                                if (obj.FromValue != obj.ToValue)
                                                {
                                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);

                                                }
                                            }
                                        }
                                    }
                                }
                                string multiselecttovalue = "";
                                string multiselectfromvalue = "";
                                for (int i = 0; i < multiSelectnewvalues.Count(); i++)
                                {
                                    multiselecttovalue = (multiSelectnewvalues[i].Caption == null ? "-" : (multiselecttovalue + "," + multiSelectnewvalues[i].Caption.ToString().Trim()));
                                }
                                for (int i = 0; i < multiSelectoldvalues.Count(); i++)
                                {
                                    multiselectfromvalue = (i > multiSelectoldvalues.Count() - 1 || multiSelectoldvalues.Count() == 0 ? "-" : (multiselectfromvalue + "," + multiSelectoldvalues[i].Caption.ToString().Trim()));
                                }
                                if (multiselecttovalue != multiselectfromvalue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, multiselectfromvalue.Trim().TrimStart(','), multiselecttovalue.Trim().TrimStart(','));

                                }

                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 13)
                            {



                                IList<TreeNodeDao> newValues = new List<TreeNodeDao>();
                                if (obj.obj2.Count() > 0)
                                {
                                    for (int i = 0; i < obj.obj2.Count(); i++)
                                    {
                                        newValues.Add((from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeNodeDao>() where item.Id == Convert.ToInt32(obj.obj2[i]) select item).FirstOrDefault());

                                    }
                                }
                                IList<TreeNodeDao> oldValues = new List<TreeNodeDao>();

                                if (obj.obj3.Count > 0)
                                {
                                    foreach (var ele in obj.obj3)
                                    {
                                        oldValues.Add((from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeNodeDao>() where item.Id == Convert.ToInt32(((BrandSystems.Marcom.Dal.Metadata.Model.TreeValueDao)(ele)).Nodeid) select item).FirstOrDefault());

                                    }
                                }

                                var listlevel = (from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeLevelDao>()
                                                 where item.AttributeID == obj.AttributeId
                                                 select item.Level).ToList().Distinct();

                                if (listlevel != null)
                                {

                                    foreach (var lev in listlevel)
                                    {
                                        IList<TreeNodeDao> multiSelectoldvalues = new List<TreeNodeDao>();
                                        IList<TreeNodeDao> multiSelectnewvalues = new List<TreeNodeDao>();
                                        IList<TreeLevelDao> attr = new List<TreeLevelDao>();

                                        attr.Add(txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].GetbyCriteria<TreeLevelDao>(TreeLevelDao.PropertyNames.AttributeID, TreeLevelDao.PropertyNames.Level, Convert.ToInt32(obj.AttributeId), Convert.ToInt32(lev)));
                                        obj.AttributeName = attr[0].LevelName;
                                        if (newValues.Count > 0)
                                        {
                                            multiSelectnewvalues = (from item in newValues where item.Level == lev select item).ToList<TreeNodeDao>();

                                        }
                                        if (oldValues.Count > 0)
                                        {
                                            multiSelectoldvalues = (from item in oldValues where item.Level == lev select item).ToList<TreeNodeDao>();
                                        }

                                        string multiselecttovalue = "";
                                        string multiselectfromvalue = "";

                                        for (int i = 0; i < multiSelectnewvalues.Count(); i++)
                                        {
                                            int nodeid = 0;
                                            nodeid = Convert.ToInt32(multiSelectnewvalues[i].Id.ToString().Trim());
                                            string toNodevalue = "";
                                            var treeValue = (from item2 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<TreeValueDao>() where item2.Entityid == obj.EntityId && item2.Attributeid == obj.AttributeId && item2.Nodeid == nodeid select item2).ToList<TreeValueDao>();
                                            if (treeValue[0].Value != null && treeValue[0].Value != "")
                                            {
                                                toNodevalue = "(" + treeValue[0].Value + "%)";
                                            }
                                            multiselecttovalue = (multiSelectnewvalues[i].Caption == null ? "-" : (multiselecttovalue + "," + multiSelectnewvalues[i].Caption.ToString().Trim() + toNodevalue));
                                        }
                                        for (int i = 0; i < multiSelectoldvalues.Count(); i++)
                                        {
                                            string fromNodevalue = "";
                                            int nodeid = 0;
                                            nodeid = Convert.ToInt32(multiSelectoldvalues[i].Id.ToString().Trim());
                                            if (obj.treeValues.Count > 0)
                                            {
                                                var treefromValue = (from item2 in obj.treeValues where item2.Entityid == obj.EntityId && item2.Attributeid == obj.AttributeId && item2.Nodeid == nodeid select item2.Value).FirstOrDefault();
                                                if (treefromValue != null && treefromValue != "")
                                                {
                                                    fromNodevalue = "(" + treefromValue + "%)";
                                                }
                                            }

                                            multiselectfromvalue = (i > multiSelectoldvalues.Count() - 1 || multiSelectoldvalues.Count() == 0 ? "-" : (multiselectfromvalue + "," + multiSelectoldvalues[i].Caption.ToString().Trim() + fromNodevalue));
                                        }
                                        if (multiselecttovalue != multiselectfromvalue)
                                        {
                                            txInnerloop.Commit();
                                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, multiselectfromvalue.Trim().TrimStart(','), multiselecttovalue.Trim().TrimStart(','));

                                        }
                                    }


                                }


                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 7)
                            {
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                StringBuilder TreeFromValue = new StringBuilder();
                                StringBuilder TreeTOValue = new StringBuilder();


                                //GetNewsFeedRecursionTreeNode(obj.obj5, obj.AttributeId, ref TreeFromValue);
                                //GetNewsFeedRecursionTreeNode(obj.obj6, obj.AttributeId, ref TreeTOValue);

                                GetNewsFeedRecursionTreeNodeNew(obj.obj5, obj.AttributeId, ref TreeFromValue);
                                GetNewsFeedRecursionTreeNodeNew(obj.obj6, obj.AttributeId, ref TreeTOValue);




                                var treeformflag = false;
                                ArrayList treenodetree = new ArrayList();

                                TreeFromValue.Replace("[]", "");
                                TreeTOValue.Replace("[]", "");
                                if (TreeFromValue.ToString() == null || TreeFromValue.ToString() == " ")
                                    TreeFromValue.Append("-");
                                if (TreeTOValue.ToString() == null || TreeTOValue.ToString() == " ")
                                    TreeTOValue.Append("-");

                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, TreeFromValue.ToString(), TreeTOValue.ToString());


                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 17)
                            {
                                string mappingfilesPath = ConfigurationManager.AppSettings["MarcomPresentation"].ToString();

                                mappingfilesPath = mappingfilesPath + TenantFilePath + "MetadataXML" + @"\MetadataVersion_V" + Convert.ToString(MarcomManagerFactory.ActiveMetadataVersionNumber[marcomManager.User.TenantID]) + ".xml";
                                string metadataVersion=Convert.ToString(MarcomManagerFactory.ActiveMetadataVersionNumber[marcomManager.User.TenantID]);
                                XDocument metadataXDoc = MarcomCache<XDocument>.ReadXDocument(xmlType.Metadata, marcomManager.User.TenantID, metadataVersion);

                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                foreach (var ele in obj.obj3)
                                {
                                    //var oldOptionName = (from item in txInnerloop.PersistenceManager.UserRepository[proxy.MarcomManager.User.TenantID].Query<BrandSystems.Marcom.Dal.Metadata.Model.TagOptionDao>() where item.Id == Convert.ToInt32(ele) select item.Caption).FirstOrDefault();
                                    var oldOptionName = (from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].GetObject<TagOptionDao>(metadataXDoc) where item.Id == Convert.ToInt32(ele) select item.Caption).FirstOrDefault();

                                    obj.FromValue = obj.FromValue + ',' + oldOptionName;
                                }
                                foreach (var ele in obj.obj2)
                                {
                                    var optionName = (from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].GetObject<TagOptionDao>(metadataXDoc) where item.Id == Convert.ToInt32(ele) select item.Caption).FirstOrDefault();
                                    obj.ToValue = obj.ToValue + ',' + optionName;
                                }
                                obj.FromValue = ((obj.FromValue == "" || obj.FromValue == null) ? "-" : obj.FromValue.Trim().TrimStart(','));
                                obj.ToValue = ((obj.ToValue == "" || obj.ToValue == null) ? "-" : obj.ToValue.Trim().TrimStart(','));
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                                    //  marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.EntityUpdated), false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID);
                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 16)
                            {
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                var list = obj.obj3.Cast<Hashtable>();
                                string oldoption = "-";
                                foreach (var item in list)
                                {
                                    if (item["Attr_" + obj.AttributeId] != null)
                                        oldoption = Convert.ToDateTime(item["Attr_" + obj.AttributeId]).ToString("yyyy-MM-dd");
                                    else
                                        oldoption = "-";
                                }
                                obj.FromValue = oldoption == "1/1/1900 12:00:00 AM" ? "-" : oldoption;
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                    // marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.EntityUpdated), false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID);
                                }
                            }

                            txInnerloop.Commit();
                            break;
                        case "create Asset":
                            newsFeedTemplateId = 101;
                            MailTemplateID = 60;
                            //if (obj.ParentId == 0)
                            //{

                            //    notificationtypeid = Convert.ToInt32(NotificationTypeID.RootLevelEntityCreated);
                            //}
                            //else
                            //{
                            //    newsFeedTemplateId = 1;
                            //    notificationtypeid = Convert.ToInt32(NotificationTypeID.EntityCreated);
                            //}
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                            marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 52, false, false, obj.TypeName, obj.AttributeName, "", "", MailTemplateID, 0, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;

                        case "delete Asset":
                            newsFeedTemplateId = 102;
                            MailTemplateID = 62;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                            marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 54, false, false, obj.TypeName, obj.AttributeName, "", "", MailTemplateID, 0, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;
                        case "Publish Asset":
                            newsFeedTemplateId = 105;
                            MailTemplateID = 61;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                            marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, "PublishStatus", "-", "Publish", MailTemplateID, 0, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;
                        case "UnPublish Asset":
                            newsFeedTemplateId = 119;
                            MailTemplateID = 61;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                            marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, "PublishStatus", "Publish", "UnPublish", MailTemplateID, 0, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;
                        case "Add AssetVersion":
                            newsFeedTemplateId = 106;
                            MailTemplateID = 61;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                            marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;
                        case "Delete AssetVersion":
                            newsFeedTemplateId = 107;
                            MailTemplateID = 61;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                            marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;

                        case "Download Asset":
                            newsFeedTemplateId = 111;
                            //MailTemplateID = 25;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                            //marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.EntityDeleted), false, false, obj.TypeName, "", "", "", MailTemplateID);
                            txInnerloop.Commit();
                            break;
                        case "Send Asset":
                            newsFeedTemplateId = 112;
                            //MailTemplateID = 25;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                            //marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, Convert.ToInt32(NotificationTypeID.EntityDeleted), false, false, obj.TypeName, "", "", "", MailTemplateID);
                            txInnerloop.Commit();
                            break;
                        case "Moved Asset":
                            newsFeedTemplateId = 113;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                            // marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, notificationtypeid, false, false, obj.TypeName, "", "", "", MailTemplateID);
                            txInnerloop.Commit();
                            break;
                        case "Copy Asset":
                            newsFeedTemplateId = 114;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                            // marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, notificationtypeid, false, false, obj.TypeName, "", "", "", MailTemplateID);
                            txInnerloop.Commit();
                            break;

                        case "Attach Asset":
                            newsFeedTemplateId = 115;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                            // marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, notificationtypeid, false, false, obj.TypeName, "", "", "", MailTemplateID);
                            txInnerloop.Commit();
                            break;
                        case "Asset AccessChanged":
                            newsFeedTemplateId = 108;
                            MailTemplateID = 61;
                            obj.FromValue = ((obj.FromValue == "" || obj.FromValue == null || obj.FromValue == "0") ? "-" : obj.FromValue.Trim().TrimStart(','));
                            obj.ToValue = ((obj.ToValue == "" || obj.ToValue == null || obj.ToValue == "0") ? "-" : obj.ToValue.Trim().TrimStart(','));
                            string listfromvalues = "-";
                            string listtovalues = "-";
                            if (obj.FromValue != "-")
                            {
                                listfromvalues = "";
                                string[] values = obj.FromValue.Split(',');
                                foreach (string val in values)
                                {
                                    var oldRolename = (from item in txInnerloop.PersistenceManager.AccessRepository[marcomManager.User.TenantID].Query<AssetAccessDao>() where item.ID == Convert.ToInt32(val) select item.Role).FirstOrDefault();
                                    if (listfromvalues.Length > 0)
                                        listfromvalues = listfromvalues + "," + oldRolename.ToString();
                                    else
                                        listfromvalues = oldRolename.ToString();

                                }
                            }

                            if (obj.ToValue != "-")
                            {
                                listtovalues = "";
                                string[] newvalues = obj.ToValue.Split(',');
                                foreach (string newval in newvalues)
                                {
                                    var newRolename = (from item in txInnerloop.PersistenceManager.AccessRepository[marcomManager.User.TenantID].Query<AssetAccessDao>() where item.ID == Convert.ToInt32(newval) select item.Role).FirstOrDefault();
                                    if (listtovalues.Length > 0)
                                        listtovalues = listtovalues + "," + newRolename.ToString();
                                    else
                                        listtovalues = newRolename.ToString();

                                }
                            }


                            if (listfromvalues != listtovalues)
                            {
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, listfromvalues, listtovalues, 0, obj.AssociatedEntityId, null, obj.Version);
                                marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, "Access", listfromvalues, listtovalues, MailTemplateID, 0, obj.AssociatedEntityId);
                            }
                            txInnerloop.Commit();
                            break;
                        case "Asset added start period":
                            newsFeedTemplateId = 117;
                            MailTemplateID = 61;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, obj.Userid, obj.AssociatedEntityId);
                            marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;
                        case "Asset add comment for period":
                            newsFeedTemplateId = 118;
                            MailTemplateID = 61;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, obj.Userid, obj.AssociatedEntityId);
                            marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;
                        case "Asset metadata update":
                            newsFeedTemplateId = 103;
                            MailTemplateID = 61;

                            if (obj.AttributeName == "AssetType")
                            {
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, "Asset Type", obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);
                                }
                            }


                            else if (Convert.ToInt32(obj.Attributetypeid) == -1)
                            {
                                if (obj.AttributeName == "AssetName")
                                {
                                    if (obj.FromValue != obj.ToValue)
                                    {
                                        marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, "Name", obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                        marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, "Name", obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);
                                    }
                                }
                            }

                            else if (Convert.ToInt32(obj.Attributetypeid) == 16)
                            {
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                var list = obj.obj3.Cast<Hashtable>();
                                string oldoption = "-";
                                foreach (var item in list)
                                {
                                    if (item["Attr_" + obj.AttributeId] != null)
                                        oldoption = Convert.ToDateTime(item["Attr_" + obj.AttributeId]).ToString("yyyy-MM-dd");
                                    else
                                        oldoption = "-";
                                }
                                obj.FromValue = oldoption == "1/1/1900 12:00:00 AM" ? "-" : oldoption;
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);
                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 6)
                            {
                                try
                                {
                                    IList<TreeLevelDao> attr = new List<TreeLevelDao>();


                                    IList<TreeNodeDao> newValues = new List<TreeNodeDao>();
                                    if (obj.obj2.Count() > 0)
                                    {
                                        for (int i = 0; i < obj.obj2.Count(); i++)
                                        {
                                            newValues.Add((from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeNodeDao>() where item.Id == Convert.ToInt32(obj.obj2[i]) select item).FirstOrDefault());


                                        }
                                    }

                                    for (int i = 0; i < newValues.Count(); i++)
                                    {
                                        if (newValues[i] != null)
                                        {
                                            attr.Add(txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].GetbyCriteria<TreeLevelDao>(TreeLevelDao.PropertyNames.AttributeID, TreeLevelDao.PropertyNames.Level, Convert.ToInt32(obj.AttributeId), Convert.ToInt32(newValues[i].Level)));
                                        }
                                    }


                                    IList<TreeNodeDao> oldValues = new List<TreeNodeDao>();
                                    if (obj.obj3.Count > 0)
                                    {
                                        foreach (var ele in obj.obj3)
                                        {
                                            oldValues.Add((from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeNodeDao>() where item.Id == Convert.ToInt32(((BrandSystems.Marcom.Dal.Metadata.Model.TreeValueDao)(ele)).Nodeid) select item).FirstOrDefault());
                                        }
                                    }

                                    for (int i = 0; i < newValues.Count(); i++)
                                    {
                                        if (newValues[i] != null)
                                        {
                                            obj.AttributeName = attr[i].LevelName;
                                            obj.ToValue = (newValues[i].Caption == null ? "-" : newValues[i].Caption.ToString().Trim());
                                            obj.FromValue = (i > oldValues.Count() - 1 || oldValues.Count() == 0 || oldValues[i] == null ? "-" : oldValues[i].Caption.ToString().Trim());
                                            if (obj.FromValue != obj.ToValue)
                                            {
                                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                                marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);
                                            }
                                        }
                                    }


                                }
                                catch (Exception ex)
                                {

                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 3)
                            {

                                obj.AttributeName = obj.AttributeDetails[0].Caption;
                                if (obj.AttributeDetails[0].IsSpecial == true && obj.AttributeDetails[0].Id == Convert.ToInt32(SystemDefinedAttributes.Owner))
                                {
                                    var role = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.User.Model.UserDao>(Convert.ToInt32(obj.obj2[0]));
                                    obj.ToValue = (role == null ? "-" : role.FirstName + " " + role.LastName);
                                    var oldrole = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.User.Model.UserDao>(Convert.ToInt32(((BrandSystems.Marcom.Dal.Access.Model.EntityRoleUserDao)(obj.obj3[0])).Userid));
                                    obj.FromValue = (oldrole == null ? "-" : oldrole.FirstName + " " + oldrole.LastName);
                                }
                                else
                                {
                                    var list = obj.obj3.Cast<Hashtable>();
                                    int oldoption = 0;
                                    foreach (var item in list)
                                    {
                                        oldoption = Convert.ToInt32(item["Attr_" + obj.AttributeId]);
                                    }

                                    var optionoldvalue = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.OptionDao>(oldoption);
                                    obj.FromValue = (optionoldvalue == null ? "-" : Convert.ToString(optionoldvalue.Caption));
                                    var optionnewvalue = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.OptionDao>(Convert.ToInt32(obj.obj2[0]));
                                    obj.ToValue = Convert.ToString(optionnewvalue.Caption);
                                }
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);
                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 14)
                            {

                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                if (obj.AttributeDetails[0].IsSpecial == true && obj.AttributeDetails[0].Id == 14)
                                {
                                    foreach (var item in obj.obj3.Cast<Hashtable>())
                                    {

                                        obj.FromValue = obj.FromValue = ((item["Name"] == "" || item["Name"] == null) ? "-" : Convert.ToString(item["Name"]));
                                    }
                                }
                                else
                                {
                                    var list = obj.obj3.Cast<Hashtable>();
                                    string oldoption = "";
                                    foreach (var item in list)
                                    {
                                        oldoption = Convert.ToString(item["Name"]);
                                        obj.FromValue = ((oldoption == "" || oldoption == null) ? "-" : Convert.ToString(oldoption));
                                    }
                                }
                                obj.ToValue = (string)obj.obj2[0];
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);
                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 1)
                            {
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                if (obj.AttributeDetails[0].IsSpecial == true && obj.AttributeDetails[0].Id == Convert.ToInt32(SystemDefinedAttributes.Name))
                                {
                                    foreach (var item in obj.obj3.Cast<Hashtable>())
                                    {

                                        obj.FromValue = obj.FromValue = ((item["Name"] == "" || item["Name"] == null) ? "-" : Convert.ToString(item["Name"]));
                                    }

                                }
                                else
                                {
                                    var list = obj.obj3.Cast<Hashtable>();
                                    string oldoption = "";
                                    foreach (var item in list)
                                    {
                                        oldoption = Convert.ToString(item["Attr_" + obj.AttributeId]);
                                        obj.FromValue = ((oldoption == "" || oldoption == null) ? "-" : Convert.ToString(oldoption));
                                    }
                                }
                                obj.ToValue = (string)obj.obj2[0];
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);
                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 2)
                            {
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                if (obj.AttributeDetails[0].IsSpecial == true && obj.AttributeDetails[0].Id == Convert.ToInt32(SystemDefinedAttributes.Name))
                                {
                                    var oldname = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Planning.Model.BaseEntityDao>(Convert.ToInt32(((BrandSystems.Marcom.Dal.Planning.Model.BaseEntityDao)(obj.obj3[0])).Id));
                                    obj.FromValue = (oldname == null ? "-" : oldname.Name);

                                }
                                else
                                {
                                    var list = obj.obj3.Cast<Hashtable>();
                                    string oldoption = "";
                                    foreach (var item in list)
                                    {
                                        oldoption = Convert.ToString(item["Attr_" + obj.AttributeId]);
                                        obj.FromValue = ((oldoption == "" || oldoption == null) ? "-" : oldoption);
                                    }
                                }
                                obj.ToValue = (string)obj.obj2[0];
                                obj.FromValue = ((obj.FromValue == "" || obj.FromValue == null) ? "-" : obj.FromValue);
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);
                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 4)
                            {
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                foreach (var ele in obj.obj3)
                                {
                                    var oldOptionName = (from item in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<BrandSystems.Marcom.Dal.Metadata.Model.OptionDao>() where item.Id == Convert.ToInt32(ele) select item.Caption).FirstOrDefault();
                                    obj.FromValue = obj.FromValue + ',' + oldOptionName;
                                }
                                foreach (var ele in obj.obj2)
                                {
                                    var optionName = (from item in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<BrandSystems.Marcom.Dal.Metadata.Model.OptionDao>() where item.Id == Convert.ToInt32(ele) select item.Caption).FirstOrDefault();
                                    obj.ToValue = obj.ToValue + ',' + optionName;
                                }
                                obj.FromValue = ((obj.FromValue == "" || obj.FromValue == null) ? "-" : obj.FromValue.Trim().TrimStart(','));
                                obj.ToValue = ((obj.ToValue == "" || obj.ToValue == null) ? "-" : obj.ToValue.Trim().TrimStart(','));
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);
                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 10)
                            {
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);
                                }
                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 12)
                            {

                                IList<TreeLevelDao> attr = new List<TreeLevelDao>();


                                IList<TreeNodeDao> newValues = new List<TreeNodeDao>();
                                if (obj.obj2.Count() > 0)
                                {
                                    for (int i = 0; i < obj.obj2.Count(); i++)
                                    {
                                        newValues.Add((from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeNodeDao>() where item.Id == Convert.ToInt32(obj.obj2[i]) select item).FirstOrDefault());

                                    }
                                }
                                IList<TreeNodeDao> oldValues = new List<TreeNodeDao>();
                                if (obj.obj3.Count > 0)
                                {
                                    foreach (var ele in obj.obj3)
                                    {
                                        oldValues.Add((from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeNodeDao>() where item.Id == Convert.ToInt32(((BrandSystems.Marcom.Dal.Metadata.Model.TreeValueDao)(ele)).Nodeid) select item).FirstOrDefault());
                                    }
                                }
                                for (int i = 0; i < newValues.Count(); i++)
                                {

                                    if (newValues[i] != null)
                                        attr.Add(txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].GetbyCriteria<TreeLevelDao>(TreeLevelDao.PropertyNames.AttributeID, TreeLevelDao.PropertyNames.Level, Convert.ToInt32(obj.AttributeId), Convert.ToInt32(newValues[i].Level)));
                                    else
                                        if (oldValues.Count != 0)
                                            attr.Add(txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].GetbyCriteria<TreeLevelDao>(TreeLevelDao.PropertyNames.AttributeID, TreeLevelDao.PropertyNames.Level, Convert.ToInt32(obj.AttributeId), Convert.ToInt32(oldValues[i].Level)));
                                }

                                var maxLevelCount = (from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeLevelDao>()
                                                     where item.AttributeID == obj.AttributeId
                                                     select item.Level).Max();
                                IList<TreeNodeDao> multiSelectoldvalues = new List<TreeNodeDao>();
                                IList<TreeNodeDao> multiSelectnewvalues = new List<TreeNodeDao>();
                                for (int i = 0; i < newValues.Count(); i++)
                                {
                                    if (newValues[i] != null)
                                    {
                                        if (i <= attr.Count - 1)
                                            obj.AttributeName = attr[i].LevelName;
                                        if (newValues[i].Level == maxLevelCount)
                                        {
                                            multiSelectnewvalues.Add(newValues[i]);
                                            if (oldValues.Count > 0)
                                            {
                                                multiSelectoldvalues = (from item in oldValues where item.Level == maxLevelCount select item).ToList<TreeNodeDao>();
                                            }
                                        }
                                        else
                                        {
                                            obj.ToValue = (newValues[i].Caption == null ? "-" : newValues[i].Caption.ToString().Trim());
                                            obj.FromValue = (i > oldValues.Count() - 1 || oldValues.Count() == 0 || oldValues[i] == null ? "-" : oldValues[i].Caption.ToString().Trim());
                                            if (obj.FromValue != obj.ToValue)
                                            {
                                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                                marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);

                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (i <= attr.Count - 1)
                                            obj.AttributeName = attr[i].LevelName;
                                        if (newValues[i] == null)
                                        {

                                            if (oldValues.Count > 0)
                                            {
                                                if (oldValues[i].Level == maxLevelCount)
                                                    multiSelectoldvalues = (from item in oldValues where item.Level == maxLevelCount select item).ToList<TreeNodeDao>();
                                                else
                                                {
                                                    obj.ToValue = (newValues[i].Caption == null ? "-" : newValues[i].Caption.ToString().Trim());
                                                    obj.FromValue = (i > oldValues.Count() - 1 || oldValues.Count() == 0 || oldValues[i] == null ? "-" : oldValues[i].Caption.ToString().Trim());
                                                    if (obj.FromValue != obj.ToValue)
                                                    {
                                                        marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                                        marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);

                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (newValues[i].Level == maxLevelCount)
                                            {

                                                if (oldValues.Count > 0)
                                                {
                                                    multiSelectoldvalues = (from item in oldValues where item.Level == maxLevelCount select item).ToList<TreeNodeDao>();
                                                }
                                            }
                                            else
                                            {
                                                obj.ToValue = (newValues[i].Caption == null ? "-" : newValues[i].Caption.ToString().Trim());
                                                obj.FromValue = (i > oldValues.Count() - 1 || oldValues.Count() == 0 || oldValues[i] == null ? "-" : oldValues[i].Caption.ToString().Trim());
                                                if (obj.FromValue != obj.ToValue)
                                                {
                                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);

                                                }
                                            }
                                        }
                                    }
                                }
                                string multiselecttovalue = "";
                                string multiselectfromvalue = "";
                                for (int i = 0; i < multiSelectnewvalues.Count(); i++)
                                {
                                    multiselecttovalue = (multiSelectnewvalues[i].Caption == null ? "-" : (multiselecttovalue + "," + multiSelectnewvalues[i].Caption.ToString().Trim()));
                                }
                                for (int i = 0; i < multiSelectoldvalues.Count(); i++)
                                {
                                    multiselectfromvalue = (i > multiSelectoldvalues.Count() - 1 || multiSelectoldvalues.Count() == 0 ? "-" : (multiselectfromvalue + "," + multiSelectoldvalues[i].Caption.ToString().Trim()));
                                }
                                if (multiselecttovalue != multiselectfromvalue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, multiselectfromvalue.Trim().TrimStart(','), multiselecttovalue.Trim().TrimStart(','), 0, obj.AssociatedEntityId, null, obj.Version);
                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);

                                }

                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 17)
                            {
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                foreach (var ele in obj.obj3)
                                {
                                    var oldOptionName = (from item in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<BrandSystems.Marcom.Dal.Metadata.Model.TagOptionDao>() where item.Id == Convert.ToInt32(ele) select item.Caption).FirstOrDefault();
                                    if (oldOptionName != null)
                                        obj.FromValue = obj.FromValue + ',' + oldOptionName;
                                }
                                foreach (var ele in obj.obj2)
                                {
                                    var optionName = (from item in txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Query<BrandSystems.Marcom.Dal.Metadata.Model.TagOptionDao>() where item.Id == Convert.ToInt32(ele) select item.Caption).FirstOrDefault();
                                    if (optionName != null)
                                        obj.ToValue = obj.ToValue + ',' + optionName;
                                }
                                obj.FromValue = ((obj.FromValue == "" || obj.FromValue == null) ? "-" : obj.FromValue.Trim().TrimStart(','));
                                obj.ToValue = ((obj.ToValue == "" || obj.ToValue == null) ? "-" : obj.ToValue.Trim().TrimStart(','));
                                if (obj.FromValue != obj.ToValue)
                                {
                                    obj.FromValue = obj.FromValue.Replace(",,", ",");
                                    obj.ToValue = obj.ToValue.Replace(",,", ",");
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);
                                }
                            }

                            else if (Convert.ToInt32(obj.Attributetypeid) == 5)
                            {
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                var list = obj.obj3.Cast<Hashtable>();
                                string oldoption = "-";
                                foreach (var item in list)
                                {
                                    if (item["Attr_" + obj.AttributeId] != null)
                                        oldoption = Convert.ToDateTime(item["Attr_" + obj.AttributeId]).ToString("yyyy-MM-dd");
                                    else
                                        oldoption = "-";
                                }
                                obj.FromValue = oldoption == "1/1/1900 12:00:00 AM" ? "-" : oldoption;
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);
                                }
                            }


                            else if (Convert.ToInt32(obj.Attributetypeid) == 13)
                            {
                                IList<TreeNodeDao> newValues = new List<TreeNodeDao>();
                                if (obj.obj2.Count() > 0)
                                {
                                    for (int i = 0; i < obj.obj2.Count(); i++)
                                    {
                                        newValues.Add((from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeNodeDao>() where item.Id == Convert.ToInt32(obj.obj2[i]) select item).FirstOrDefault());

                                    }
                                }
                                IList<TreeNodeDao> oldValues = new List<TreeNodeDao>();

                                if (obj.obj3.Count > 0)
                                {
                                    foreach (var ele in obj.obj3)
                                    {
                                        oldValues.Add((from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeNodeDao>() where item.Id == Convert.ToInt32(((DAMTreeValueDao)(ele)).Nodeid) select item).FirstOrDefault());

                                    }
                                }
                                var listlevel = (from item in txInnerloop.PersistenceManager.MetadataRepository[marcomManager.User.TenantID].Query<TreeLevelDao>()
                                                 where item.AttributeID == obj.AttributeId
                                                 select item.Level).ToList().Distinct();

                                if (listlevel != null)
                                {

                                    foreach (var lev in listlevel)
                                    {
                                        IList<TreeNodeDao> multiSelectoldvalues = new List<TreeNodeDao>();
                                        IList<TreeNodeDao> multiSelectnewvalues = new List<TreeNodeDao>();
                                        IList<TreeLevelDao> attr = new List<TreeLevelDao>();

                                        attr.Add(txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].GetbyCriteria<TreeLevelDao>(TreeLevelDao.PropertyNames.AttributeID, TreeLevelDao.PropertyNames.Level, Convert.ToInt32(obj.AttributeId), Convert.ToInt32(lev)));
                                        obj.AttributeName = attr[0].LevelName;
                                        if (newValues.Count > 0)
                                        {
                                            multiSelectnewvalues = (from item in newValues where item.Level == lev select item).ToList<TreeNodeDao>();

                                        }
                                        if (oldValues.Count > 0)
                                        {
                                            multiSelectoldvalues = (from item in oldValues where item.Level == lev select item).ToList<TreeNodeDao>();
                                        }

                                        string multiselecttovalue = "";
                                        string multiselectfromvalue = "";

                                        for (int i = 0; i < multiSelectnewvalues.Count(); i++)
                                        {
                                            int nodeid = 0;
                                            nodeid = Convert.ToInt32(multiSelectnewvalues[i].Id.ToString().Trim());
                                            string toNodevalue = "";
                                            var treeValue = (from item2 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<DAMTreeValueDao>() where item2.Entityid == obj.EntityId && item2.Attributeid == obj.AttributeId && item2.Nodeid == nodeid select item2).ToList<DAMTreeValueDao>();
                                            if (treeValue[0].Value != null && treeValue[0].Value != "")
                                            {
                                                toNodevalue = "(" + treeValue[0].Value + "%)";
                                            }
                                            multiselecttovalue = (multiSelectnewvalues[i].Caption == null ? "-" : (multiselecttovalue + "," + multiSelectnewvalues[i].Caption.ToString().Trim() + toNodevalue));
                                        }
                                        for (int i = 0; i < multiSelectoldvalues.Count(); i++)
                                        {
                                            string fromNodevalue = "";
                                            int nodeid = 0;
                                            nodeid = Convert.ToInt32(multiSelectoldvalues[i].Id.ToString().Trim());
                                            if (obj.treeValues.Count > 0)
                                            {
                                                var treefromValue = (from item2 in obj.treeValues where item2.Entityid == obj.EntityId && item2.Attributeid == obj.AttributeId && item2.Nodeid == nodeid select item2.Value).FirstOrDefault();
                                                if (treefromValue != null && treefromValue != "")
                                                {
                                                    fromNodevalue = "(" + treefromValue + "%)";
                                                }
                                            }

                                            multiselectfromvalue = (i > multiSelectoldvalues.Count() - 1 || multiSelectoldvalues.Count() == 0 ? "-" : (multiselectfromvalue + "," + multiSelectoldvalues[i].Caption.ToString().Trim() + fromNodevalue));
                                        }
                                        if (multiselecttovalue != multiselectfromvalue)
                                        {
                                            txInnerloop.Commit();
                                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, multiselectfromvalue.Trim().TrimStart(','), multiselecttovalue.Trim().TrimStart(','), 0, obj.AssociatedEntityId, null, obj.Version);
                                            marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);

                                        }
                                    }


                                }


                            }


                            else if (Convert.ToInt32(obj.Attributetypeid) == 7)
                            {
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                StringBuilder TreeFromValue = new StringBuilder();
                                StringBuilder TreeTOValue = new StringBuilder();


                                //GetNewsFeedRecursionTreeNode(obj.obj5, obj.AttributeId, ref TreeFromValue);
                                //GetNewsFeedRecursionTreeNode(obj.obj6, obj.AttributeId, ref TreeTOValue);

                                GetNewsFeedRecursionTreeNodeNew(obj.obj5, obj.AttributeId, ref TreeFromValue);
                                GetNewsFeedRecursionTreeNodeNew(obj.obj6, obj.AttributeId, ref TreeTOValue);




                                var treeformflag = false;
                                ArrayList treenodetree = new ArrayList();

                                TreeFromValue.Replace("[]", "");
                                TreeTOValue.Replace("[]", "");
                                if (TreeFromValue.ToString() == null || TreeFromValue.ToString() == " ")
                                    TreeFromValue.Append("-");
                                if (TreeTOValue.ToString() == null || TreeTOValue.ToString() == " ")
                                    TreeTOValue.Append("-");

                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, TreeFromValue.ToString(), TreeTOValue.ToString(), 0, obj.AssociatedEntityId, null, obj.Version);
                                marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);


                            }
                            else if (Convert.ToInt32(obj.Attributetypeid) == 19)
                            {
                                var attr = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Metadata.Model.AttributeDao>(obj.AttributeId);
                                obj.AttributeName = attr.Caption;
                                if (obj.AttributeDetails[0].IsSpecial == true && obj.AttributeDetails[0].Id == Convert.ToInt32(SystemDefinedAttributes.Name))
                                {
                                    var oldname = txInnerloop.PersistenceManager.UserRepository[marcomManager.User.TenantID].Get<BrandSystems.Marcom.Dal.Planning.Model.BaseEntityDao>(Convert.ToInt32(((BrandSystems.Marcom.Dal.Planning.Model.BaseEntityDao)(obj.obj3[0])).Id));
                                    obj.FromValue = oldname.Name;
                                }
                                else
                                {
                                    var list = obj.obj3.Cast<Hashtable>();
                                    string oldoption = "";
                                    foreach (var item in list)
                                    {
                                        // oldoption = Convert.ToString(Convert.ToInt32(item["Amount"]))+" "+ Convert.ToString(item["ShortName"]);
                                        oldoption = Convert.ToString((item["Amount"])) + " " + Convert.ToString(item["ShortName"]);
                                        obj.FromValue = (oldoption == "" || oldoption == null) ? "-" : oldoption;
                                    }
                                }
                                obj.ToValue = (string)obj.obj2[0] + " " + (string)obj.obj2[2];
                                if (obj.FromValue != obj.ToValue)
                                {
                                    marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId, null, obj.Version);
                                    marcomManager.CommonManager.UserNotification_Insert(obj.EntityId, obj.Actorid, DateTimeOffset.Now, 53, false, false, obj.TypeName, obj.AttributeName, obj.FromValue, obj.ToValue, MailTemplateID, 0, obj.AssociatedEntityId);

                                }
                            }

                            txInnerloop.Commit();
                            break;
                        case "MoveTask":
                            newsFeedTemplateId = 61;
                            var ObjMoveTask = (from data1 in txInnerloop.PersistenceManager.PlanningRepository[marcomManager.User.TenantID].Query<BaseEntityDao>() where data1.Id == Convert.ToInt32(obj.EntityId) select data1.Name).FirstOrDefault().ToString();
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Userid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);
                            txInnerloop.Commit();
                            break;
                        case "Calender status update":
                            newsFeedTemplateId = 23;
                            if (obj.FromValue != obj.ToValue)
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                            txInnerloop.Commit();
                            break;
                        case "Cms metadata update":
                            newsFeedTemplateId = 62;

                            if (obj.FromValue != obj.ToValue)
                            {
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue);
                            }

                            txInnerloop.Commit();
                            break;
                        case "cms entity content changed":
                            newsFeedTemplateId = 63;
                            marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, "", "", "");
                            txInnerloop.Commit();
                            break;
                        case "Folder creation":
                            newsFeedTemplateId = 121;

                            if (obj.FromValue != obj.ToValue)
                            {
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);
                            }

                            txInnerloop.Commit();
                            break;
                        case "Folder Updated":
                            newsFeedTemplateId = 122;

                            if (obj.FromValue != obj.ToValue)
                            {
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);
                            }

                            txInnerloop.Commit();
                            break;
                        case "Folder Deleted":
                            newsFeedTemplateId = 123;

                            if (obj.FromValue != obj.ToValue)
                            {
                                marcomManager.CommonManager.SaveUpdateFeed(obj.Actorid, newsFeedTemplateId, obj.EntityId, obj.action, obj.AttributeName, obj.FromValue, obj.ToValue, 0, obj.AssociatedEntityId);
                            }

                            txInnerloop.Commit();
                            break;
                        default:
                            txInnerloop.Commit();
                            break;

                    }

                    BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("Updated the Feeds", BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);

                }


            }
            catch (Exception ex)
            {
                BrandSystems.Marcom.Core.Metadata.LogHandler.LogInfo("error in feed" + " " + ex.Message, BrandSystems.Marcom.Core.Metadata.LogHandler.LogType.General);
            }
        }


        public void GetNewsFeedRecursionTreeNode(JArray jObject, int attributID, ref StringBuilder Captionvalues)
        {

            Captionvalues.Append(" [");
            bool isNotFirstItems = false;

            for (int i = 0; i < jObject.Count(); i++)
            {
                if ((bool)jObject[i]["ischecked"])
                {

                    if (!isNotFirstItems)
                        Captionvalues.Append((string)jObject[i]["Caption"]);
                    else
                        Captionvalues.Append(", " + (string)jObject[i]["Caption"]);
                    isNotFirstItems = true;

                    if (jObject[i]["Children"].Count() > 0)
                    {

                        JArray jarraylist = jObject[i]["Children"] as JArray;
                        GetNewsFeedRecursionTreeNode(jarraylist, attributID, ref Captionvalues);

                    }
                }


            }
            Captionvalues.Append("]");

        }

        public void GetNewsFeedRecursionTreeNodeNew(JArray treeobj, int attributID, ref StringBuilder Captionvalues)
        {
            ArrayList treenodetree = new ArrayList();
            treenodetree = new ArrayList();
            JArray jarraychildren = treeobj as JArray;
            processrecords(treeobj, treenodetree);

            Captionvalues.Append(string.Join("", treenodetree.ToArray()));



        }
        public void processrecords(JArray treeobj, ArrayList treenodetree)
        {


            bool treeformflag = false;


            for (int i = 0; i < treeobj.Count(); i++)
            {

                if ((bool)treeobj[i]["ischecked"])
                {
                    if (treenodetree.Count > 0)
                        if (treenodetree[treenodetree.Count - 1] != "[")
                            treenodetree.Add(",");
                    treenodetree.Add(treeobj[i]["Caption"]);
                    treeformflag = false;

                    JArray jarraylist = treeobj[i]["Children"] as JArray;
                    if (ischildSelected(jarraylist, ref treeformflag))
                    {
                        treenodetree.Add("[");
                        JArray jarraychildren = treeobj[i]["Children"] as JArray;
                        processrecords(jarraychildren, treenodetree);
                        treenodetree.Add("]");
                    }
                    else
                    {
                        JArray jarraychildren = treeobj[i]["Children"] as JArray;
                        processrecords(jarraychildren, treenodetree);
                    }

                }
                else
                {
                    JArray jarraychildren = treeobj[i]["Children"] as JArray;
                    processrecords(jarraychildren, treenodetree);
                }
            }

        }


        public bool ischildSelected(JArray children, ref  bool treeformflag)
        {

            for (int i = 0; i < children.Count(); i++)
            {
                if ((bool)children[i]["ischecked"])
                {
                    treeformflag = true;
                    return treeformflag;
                }
            }
            return treeformflag;
        }

        public string GetTenantFilePath(int TenantID)
        {
            //Get the tenant related file path
            TenantSelection tfp = new TenantSelection();
            return tfp.GetTenantFilePath(TenantID);
        }

    }


}
