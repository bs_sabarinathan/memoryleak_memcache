using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

	/// <summary>
	/// ObjectiveConditionValueDao object for table 'PM_ObjectiveConditionValue'.
	/// </summary>
	
	public partial class ObjectiveConditionValueDao : BaseDao, ICloneable 
	{
		#region Member Variables

        protected int _id;
		protected int _conditionid;
		protected int _value;
		protected bool _bIsDeleted;
		protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public ObjectiveConditionValueDao() {}
			
		public ObjectiveConditionValueDao(int pConditionid, int pValue)
		{
			this._conditionid = pConditionid; 
			this._value = pValue; 
		}
				
		public ObjectiveConditionValueDao(int pConditionid)
		{
			this._conditionid = pConditionid; 
		}
		
		#endregion
		
		#region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }
        }
		public virtual int Conditionid
		{
			get { return _conditionid; }
			set { _bIsChanged |= (_conditionid != value); _conditionid = value; }
			
		}
		
		public virtual int Value
		{
			get { return _value; }
			set { _bIsChanged |= (_value != value); _value = value; }
			
		}


        public virtual bool IsDeleted
		{
			get
			{
				return _bIsDeleted;
			}
			set
			{
				_bIsDeleted = value;
			}
		}

        public virtual bool IsChanged
		{
			get
			{
				return _bIsChanged;
			}
			set
			{
				_bIsChanged = value;
			}
		}
		
		#endregion 				
		
		#region ICloneable methods

        public virtual object Clone()
		{
			return this.MemberwiseClone();
		}
		
		#endregion
				
		#region Public Property and Mapping Constants

		//Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
		public static class PropertyNames
		{
			public const string ObjectiveConditionValueDao = "ObjectiveConditionValueDao";
            public const string Id = "Id";
			public const string Conditionid = "Conditionid";			
			public const string Value = "Value";			
		}
		
		//Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
		public static class MappingNames
		{
			public const string ObjectiveConditionValueDao = "ObjectiveConditionValueDao";
            public const string Id = "Id";
			public const string Conditionid = "Conditionid";			
			public const string Value = "Value";			
		}

		#endregion
		
		
	}
	
}
