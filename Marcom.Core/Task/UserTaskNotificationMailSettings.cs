using System;
using System.Collections;
using System.Collections.Generic;
using BrandSystems.Marcom.Core.Task.Interface;

namespace BrandSystems.Marcom.Core.Task
{

	/// <summary>
    /// UserNotificationMailSettings object for table 'TM_UserNotificationMailSettings'.
	/// </summary>

    internal class UserTaskNotificationMailSettings : IUserTaskNotificationMailSettings 
	{
		#region Member Variables

		protected int _id;
		protected int _userid;
		protected DateTimeOffset _lastsenton;
		protected DateTimeOffset _lastupdatedon;
        protected bool _isnotificationenable;
        protected int _noofdays;
        protected TimeSpan _notificationtiming;
        protected bool _isemailenable;
        protected TimeSpan _mailtiming;
        protected bool _isemailassignedenable;
		#endregion
		
		
		#region Constructors
		public UserTaskNotificationMailSettings() {}

        public UserTaskNotificationMailSettings(int pUserid, DateTimeOffset pLastSentOn, DateTimeOffset pLastUpdatedOn, bool pIsNotificationEnable, int pNoOfDays, TimeSpan pNotificationTiming, bool pIsEmailEnable, TimeSpan pMailTiming,bool pIsEmailAssignedEnable)
		{
            this._userid = pUserid;
            this._lastsenton = pLastSentOn;
            this._lastupdatedon = pLastUpdatedOn;
            this._isnotificationenable = pIsNotificationEnable;
            this._noofdays = pNoOfDays;
            this._notificationtiming = pNotificationTiming;
            this._isemailenable = pIsEmailEnable;
            this._mailtiming = pMailTiming;
            this._isemailassignedenable = pIsEmailAssignedEnable;
		}
				
		public UserTaskNotificationMailSettings(int pId)
		{
			this._id = pId; 
		}
		
		#endregion
		
		#region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }

        }

        public virtual int Userid
        {
            get { return _userid; }
            set {  _userid = value; }

        }

        public virtual DateTimeOffset LastSentOn
        {
            get { return _lastsenton; }
            set { _lastsenton = value; }

        }

        public virtual DateTimeOffset LastUpdatedOn
        {
            get { return _lastupdatedon; }
            set {  _lastupdatedon = value; }

        }

        public virtual bool IsNotificationEnable
        {
            get { return _isnotificationenable; }
            set {  _isnotificationenable = value; }

        }

        public virtual int NoOfDays
        {
            get { return _noofdays; }
            set { _noofdays = value; }

        }

        public virtual TimeSpan NotificationTiming
        {
            get { return _notificationtiming; }
            set { _notificationtiming = value; }

        }

        public virtual bool IsEmailEnable
        {
            get { return _isemailenable; }
            set { _isemailenable = value; }

        }

        public virtual TimeSpan MailTiming
        {
            get { return _mailtiming; }
            set { _mailtiming = value; }

        }

        public virtual bool IsEmailAssignedEnable
        { get { return _isemailassignedenable; }
            set { _isemailassignedenable = value; }
        }
		#endregion 
		
		#region Equals And HashCode Overrides
		/// <summary>
		/// local implementation of Equals based on unique value members
		/// </summary>
		public override bool Equals( object obj )
		{
			if( this == obj ) return true;
			UserTaskNotificationMailSettings castObj = null;
			try
			{
				castObj = (UserTaskNotificationMailSettings)obj;
			} catch(Exception) { return false; } 
			return ( castObj != null ) &&
				( this._id == castObj.Id );
		}
		/// <summary>
		/// local implementation of GetHashCode based on unique value members
		/// </summary>
		public override int GetHashCode()
		{
		  
			
			int hash = 57; 
			hash = 27 * hash * _id.GetHashCode();
			return hash; 
		}
		#endregion
		
	}
	
}
