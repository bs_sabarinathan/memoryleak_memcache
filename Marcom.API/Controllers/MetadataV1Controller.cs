﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BrandSystems.Marcom.Core.Report.Interface;
using BrandSystems.Marcom.Metadata.Interface;
using Marcom.API.Authorization;
using Marcom.API.Models;
using Marcom.API.Services;
using Newtonsoft.Json.Linq;
using BrandSystems.Marcom.Core.Metadata.Interface;
using BrandSystems.Marcom.Core.Planning.Interface;
using BrandSystems.Marcom.Core.Access.Interface;
using BrandSystems.Marcom.Core.Interface;
using BrandSystems.Marcom.Core;
using System.IO;
using BrandSystems.Marcom.Core.Metadata;
using BrandSystems.Marcom.Dal.Access.Model;
using BrandSystems.Marcom.Core.User.Interface;

namespace Marcom.API.Controllers
{
    [AuthenticateTokenAttribute]
    public class MetadataV1Controller : ApiController
    {

        // get the base directory
        public static string baseDir = AppDomain.CurrentDomain.BaseDirectory + "errorlog.txt";
        // search the file below the current directory
        private static string retFilePath = baseDir;
        private static bool bReturnLog = false;

        #region MemberVariables

        private MetadataOverviewRepositoryV1 meatadaRepository;

        #endregion

        #region ConstructorLogic

        /// <summary>
        /// Constructer logic
        /// </summary>
        public MetadataV1Controller()
        {
            this.meatadaRepository = new MetadataOverviewRepositoryV1();
        }

        #endregion

        #region  API-Actions

        /// <summary>
        /// This will fetch all the details for a given Entity ID
        /// </summary>
        [HttpGet]
        [Queryable]
        public IQueryable<dynamic> GetEntityByID(int ID)
        {
            try
            {
                return meatadaRepository.GetEntityByID(ID).AsQueryable();
            }
            catch (Exception e)
            {
                throw e;
            }
        }



        /// <summary>
        /// This will get details of all the entities. (Top 20)
        /// </summary>
        [HttpGet]
        [Queryable]
        public IQueryable<MetadataModelV1> GetAllRootLevelEntity()
        {
            try
            {
                return meatadaRepository.GetAllRootLevelEntity().AsQueryable();
            }
            catch (Exception e)
            {
                throw e;
            }
        }



        /// <summary>
        /// This will update the lock status of an entity.
        /// </summary>
        [HttpPut]
        public HttpResponseMessage UpdateEntityLock(int id, int lockstatus)
        {
            try
            {
                meatadaRepository.UpdateEntityLocks(id, lockstatus);
                var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.OK, "Updated Successfully");
                return response;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// This will duplicate the entities.
        /// </summary>
        [HttpPut]
        public HttpResponseMessage DuplicateEntities(JObject json)
        {
            try
            {
                meatadaRepository.DuplicateEntities(json);
                var response = Request.CreateResponse<string>(System.Net.HttpStatusCode.OK, "Updated Successfully");
                return response;
            }
            catch (Exception e)
            {
                throw e;
            }

            /* code to validate the model received as input
            if (ModelState.IsValid)
            {
               
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Model state is invalid");
            }
             */
        }


        /// <summary>
        /// This will get details of all the entities. (Top 20)
        /// </summary>
        [HttpGet]

        public dynamic GetUserInfo(int userid, int entityid = 0)
        {
            try
            {
                return meatadaRepository.GetUserInfo(userid, entityid);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        [HttpGet]
        [Queryable]
        public IListofRecord GetEntitiesByUserIDListView(int ID)
        {
            try
            {
                return meatadaRepository.GetEntitiesByUserID(ID);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        [HttpPost]
        public IListofRecord GetCustomEntitiesByUserIDListView(JObject jobj)
        {
            ErrorLog.LogFilePath = retFilePath;
            try
            {
                bReturnLog = ErrorLog.CustomErrorRoutine(false, "GetCustomEntitiesByUserIDListView started at " + DateTime.Now.ToString(), DateTime.Now);

                int UserId = (int)jobj["UserId"];
                JArray typeObj = (JArray)jobj["typeArr"];
                JArray attrObj = (JArray)jobj["attrArr"];
                List<int> typeArr = typeObj.Select(jv => (int)jv).ToList();
                List<int> attrArr = attrObj.Select(jv => (int)jv).ToList();

                bReturnLog = ErrorLog.CustomErrorRoutine(false, "GetCustomEntitiesByUserIDListView done at " + DateTime.Now.ToString(), DateTime.Now);

                return meatadaRepository.GetCustomEntitiesByUserID(UserId, typeArr, attrArr, (int)jobj["StartRow"], (int)jobj["EndRow"]);
            }
            catch (Exception e)
            {
                ErrorLog.LogFilePath = retFilePath;
                bReturnLog = ErrorLog.ErrorRoutine(false, e);
                throw e;
            }
        }

        [HttpGet]
        [Queryable]
        public IList GetUserInvolvedEntities(string UniqueKey, int Userid)
        {
            try
            {
                return meatadaRepository.GetUserInvolvedEntities(UniqueKey, Userid);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public IList<IAttributeToAttributeRelations> GetAttributeToAttributeRelationsByIDForEntity(JObject jobj)
        {
            try
            {
                int entityTypeID = (int)jobj["typeid"];
                int entityId = (int)jobj["entityId"];
                return meatadaRepository.GetAttributeToAttributeRelationsByIDForEntity(entityTypeID, entityId);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpGet]
        public IList GetEntitiesList(int ID, int ParentID, int TypeID)
        {
            try
            {
                return meatadaRepository.GetEntitiesList(ID, ParentID, TypeID);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public IList<IEntityTypeAttributeRelationwithLevels> GetEntityTypeAttributeRelationWithLevelsByID(JObject jobj)
        {
            try
            {
                int typeid = (int)jobj["typeid"];
                int ParentID = (int)jobj["ParentID"];
                int ImpersonateUID = (int)jobj["ImpersonateUID"];
                return meatadaRepository.GetEntityTypeAttributeRelationWithLevelsByID(typeid, ParentID,ImpersonateUID);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public List<IEntityPredefineObjectiveAttributes> GettingPredefineObjectivesForEntityMetadata([FromBody] JObject jobj)
        {

            try
            {

                IMarcomManager marcomManager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.sessionId);

                JArray arrperiods = (JArray)jobj["Periods"];
                JArray arrobjAttibutevalues = (JArray)jobj["AttributeData"];
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                DateTime startDate = new DateTime();
                DateTime endDate = new DateTime();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {

                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcomManager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                        }
                        else
                        {
                            if (objAttr["NodeID"].Type == JTokenType.Boolean)
                            {
                                entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                            }
                            if (objAttr["NodeID"].Type == JTokenType.Date)
                            {
                                entityAttr.Value = (objAttr["NodeID"].ToString());
                            }
                            if (objAttr["NodeID"].Type == JTokenType.Integer)
                            {
                                entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                            }
                            else
                                entityAttr.Value = objAttr["NodeID"].ToString();
                        }
                        listattributevalues.Add(entityAttr);
                    }
                }
                IList<IEntityPeriod> listEntityperiods = new List<IEntityPeriod>();
                if (arrperiods != null)
                {
                    foreach (var entityPeriodObj in arrperiods)
                    {
                        if (entityPeriodObj.Count() > 0)
                        {
                            for (int i = 0; i < entityPeriodObj.Count(); i++)
                            {
                                IEntityPeriod enti = marcomManager.PlanningManager.Entityperiodservice();
                                if (i == 0)
                                {
                                    startDate = (DateTime)entityPeriodObj[i]["startDate"];//.ToString("MM/dd/yyyy");
                                }
                                enti.Startdate = (DateTime)entityPeriodObj[i]["startDate"];
                                enti.EndDate = (DateTime)entityPeriodObj[i]["endDate"];
                                enti.Description = (string)entityPeriodObj[i]["comment"];
                                enti.SortOrder = (int)entityPeriodObj[i]["sortorder"];
                                if (entityPeriodObj.Count() - 1 == i)
                                {
                                    endDate = (DateTime)entityPeriodObj[i]["endDate"];
                                }
                                listEntityperiods.Add(enti);
                            }
                        }
                    }
                }

                return meatadaRepository.GettingPredefineObjectivesForEntityMetadata(listattributevalues, startDate, endDate, (int)jobj["EntityTypeID"]);



            }
            catch
            {
                return null;
            }
            return null;
        }


        [HttpPost]
        public List<List<string>> GetValidationDationByEntitytype(JObject jobj)
        {
            try
            {
                int entityTypeID = (int)jobj["EntityTypeID"];
                return meatadaRepository.GetValidationDationByEntitytype(entityTypeID);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public IList CheckCustomHierarchy(JObject jobj)
        {
            try
            {
                int parentEntitytypeID = (int)jobj["parentEntitytypeID"];
                JArray typeObj = (JArray)jobj["subtypeArr"];
                int[] subtypeArr = typeObj.Select(jv => (int)jv).ToArray();
                return meatadaRepository.CheckCustomHierarchy(parentEntitytypeID, subtypeArr, (int)jobj["userID"]);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public IList GetGlobalMembers(JObject jobj)
        {
            try
            {
                int parentEntitytypeID = (int)jobj["EntitytypeID"];
                JArray typeObj = (JArray)jobj["roleArr"];
                int[] roleidArr = typeObj.Select(jv => (int)jv).ToArray();
                return meatadaRepository.GetGlobalMembers(roleidArr, parentEntitytypeID);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        [HttpPost]
        public int CreateEntity(JObject jobj)
        {

            try
            {
                IMarcomManager marcommanager = MarcomManagerFactory.GetMarcomManager(null, AuthenticateTokenAttribute.ImpersonateSession[(int)jobj["ImpersonateUID"]]);


                JArray arrperiods = (JArray)jobj["Periods"];
                JArray arrmembers = (JArray)jobj["EntityMembers"];
                JArray arrentitycost = (JArray)jobj["EntityCostRelations"];
                JArray arrobjentvalues = (JArray)jobj["IObjectiveEntityValue"];
                JArray arrobjAttibutevalues = (JArray)jobj["AttributeData"];
                JArray arrFilesObj = (JArray)jobj["AssetArr"]; // link assets for sub entities
                int[] assetIdArr = arrFilesObj.Select(jv => (int)jv).ToArray();
                IList<IEntityRoleUser> listEntityMembers = new List<IEntityRoleUser>();
                if (arrmembers.Count > 0)
                {
                    foreach (var arm in arrmembers)
                    {
                        IEntityRoleUser entrole = marcommanager.PlanningManager.Entityrolesservice();
                        entrole.Roleid = (int)arm["Roleid"];
                        entrole.Userid = (int)arm["Userid"];
                        entrole.IsInherited = false;
                        entrole.InheritedFromEntityid = 0;
                        listEntityMembers.Add(entrole);
                    }
                }
                else if (arrmembers.Count == 0)
                {
                    listEntityMembers = null;
                }

                IList<IFundingRequest> listFundrequest = null;

                IList<IEntityPeriod> listEntityperiods = new List<IEntityPeriod>();
                if (arrperiods != null)
                {
                    foreach (var entityPeriodObj in arrperiods)
                    {
                        if (entityPeriodObj.Count() > 0)
                        {
                            foreach (var arrp in entityPeriodObj)
                            {
                                DateTime startDate = new DateTime();
                                DateTime endDate = new DateTime();
                                if (DateTime.TryParse(arrp["startDate"].ToString(), out startDate) && DateTime.TryParse(arrp["endDate"].ToString(), out endDate))
                                {

                                    IEntityPeriod enti = marcommanager.PlanningManager.Entityperiodservice();
                                    enti.Startdate = startDate;
                                    enti.EndDate = endDate;
                                    enti.Description = (string)arrp["comment"];
                                    enti.SortOrder = (int)arrp["sortorder"];
                                    listEntityperiods.Add(enti);
                                }
                            }
                        }
                    }
                }
                else if (arrperiods == null)
                {
                    listEntityperiods = null;
                }
                IList<IEntityCostReleations> listentitycostcentres = new List<IEntityCostReleations>();

                listentitycostcentres = null;
                IList<IObjectiveEntityValue> listobjentityvalues = new List<IObjectiveEntityValue>();
                if (arrobjentvalues != null)
                {
                    foreach (var arrentobj in arrobjentvalues)
                    {
                        IObjectiveEntityValue objentvalues = marcommanager.PlanningManager.ObjEnityvalservice();
                        objentvalues.Objectiveid = (int)arrentobj;
                        listobjentityvalues.Add(objentvalues);
                    }
                }
                else if (arrobjentvalues == null)
                {
                    listobjentityvalues = null;
                }
                IList<IAttributeData> listattributevalues = new List<IAttributeData>();
                if (arrobjAttibutevalues != null)
                {
                    foreach (var objAttr in arrobjAttibutevalues)
                    {

                        List<object> listObj = new List<object>();
                        IAttributeData entityAttr = marcommanager.PlanningManager.AttributeDataservice();
                        entityAttr.ID = (int)objAttr["AttributeID"];
                        entityAttr.Caption = (dynamic)objAttr["AttributeCaption"];
                        entityAttr.TypeID = (int)objAttr["AttributeTypeID"];
                        entityAttr.Level = (int)objAttr["Level"];
                        if ((dynamic)objAttr["NodeID"].Count() > 0)
                        {
                            foreach (var obj in (dynamic)objAttr["NodeID"])
                            {
                                object valueObj = new object();
                                valueObj = (dynamic)obj;
                                listObj.Add(valueObj);
                            }
                            entityAttr.Value = listObj;
                            if ((string)objAttr["Value"] != "-1")
                                entityAttr.SpecialValue = Convert.ToString(objAttr["Value"].ToString());
                            else
                                entityAttr.SpecialValue = "";
                        }
                        else
                        {
                            if (entityAttr.ID != (int)SystemDefinedAttributes.CreationDate)
                            {
                                if (objAttr["NodeID"].Type == JTokenType.Boolean)
                                {
                                    entityAttr.Value = Convert.ToBoolean(objAttr["NodeID"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Date)
                                {
                                    entityAttr.Value = (objAttr["NodeID"].ToString());//Newtonsoft.Json.JsonConvert.DeserializeObject<string>(obj["Value"].ToString());
                                }
                                if (objAttr["NodeID"].Type == JTokenType.Integer)
                                {
                                    entityAttr.Value = int.Parse(objAttr["NodeID"].ToString());
                                }
                                else
                                    entityAttr.Value = objAttr["NodeID"].ToString();
                            }
                            else
                            {
                                entityAttr.Value = DateTime.UtcNow.ToString("yyyy-MM-dd");
                            }
                        }
                        listattributevalues.Add(entityAttr);
                    }
                }
                else if (arrobjAttibutevalues == null)
                {
                    listattributevalues = null;
                }

                return meatadaRepository.CreateEntity((int)jobj["ImpersonateUID"], (int)jobj["ParentId"], (int)jobj["Typeid"], (bool)jobj["Active"], (bool)jobj["IsLock"], (String)jobj["Name"], listEntityMembers, listentitycostcentres, listEntityperiods, listFundrequest, listattributevalues, assetIdArr, listobjentityvalues);

            }
            catch (MarcomAccessDeniedException ex)
            {
                return 0;
            }
            catch
            {
                return 0;
            }
        }



        [HttpPost]
        public List<object> GetCurrentBriefRelatedData(JObject jObj)
        {
            try
            {
                List<object> BriefRelatedData = new List<object>();
                BriefRelatedData = meatadaRepository.GetCurrentBriefRelatedData(jObj);
                return BriefRelatedData;
            }
            catch
            {
                return null;
            }

        }

        [HttpPost]
        public string GetAttributeTreeNode(JObject jObj)
        {
            try
            {

                string AttributeTreeNode = meatadaRepository.GetAttributeTreeNode(jObj);
                return AttributeTreeNode;
            }
            catch
            {
                return null;
            }

        }

        [HttpPost]
        public IList<DropDownTreePricing> GetDropDownTreePricingObjectFromParentDetail(JObject jObj)
        {
            try
            {

                IList<DropDownTreePricing> DropDownTreePricingData = meatadaRepository.GetDropDownTreePricingObjectFromParentDetail(jObj);
                return DropDownTreePricingData;
            }
            catch
            {
                return null;
            }

        }

        [HttpPost]
        public IList<DropDownTreePricing> GetDropDownTreePricingObject(JObject jObj)
        {
            try
            {
                IList<DropDownTreePricing> DropDownTreeData = meatadaRepository.GetDropDownTreePricingObjectDetail(jObj);
                return DropDownTreeData;
            }
            catch
            {
                return null;
            }

        }

        [HttpPost]
        public IList<ICurrencyType> GetCurrencyListFFsettings(JObject jObj)
        {
            try
            {
                IList<ICurrencyType> getCurrencyList = meatadaRepository.GetCurrencyListFFsettings();
                return getCurrencyList;
            }
            catch
            {
                return null;
            }

        }

        [HttpPost]
        public string GetTreeNode(JObject jObj)
        {
            try
            {
                string treeNode = meatadaRepository.GetTreeNode(jObj);
                return treeNode;
            }
            catch
            {
                return null;
            }

        }

        [HttpPost]
        public string GetTreeNodeByEntityID(JObject jObj)
        {
            try
            {
                string treeNode = meatadaRepository.GetTreeNodeByEntityID(jObj);
                return treeNode;
            }
            catch
            {
                return null;
            }

        }

        [HttpPost]
        public IList<IOption> GetOptionDetailListByID(JObject jObj)
        {
            try
            {
                IList<IOption> OptionDetails = meatadaRepository.GetOptionDetailListByID(jObj);
                return OptionDetails;
            }
            catch
            {
                return null;
            }

        }

        [HttpPost]
        public string GetDamTreeNode(JObject jObj)
        {
            try
            {
                string DamTreeNode = meatadaRepository.GetDamTreeNode(jObj);
                return DamTreeNode;
            }
            catch
            {
                return null;
            }

        }

        [HttpPost]
        public bool SaveDetailBlockForLevels(JObject jObj)
        {
            try
            {
                bool result = meatadaRepository.SaveDetailBlockForLevels(jObj);
                return result;
            }
            catch
            {
                return false;
            }

        }

        [HttpPost]
        public int InsertEntityPeriod(JObject jObj)
        {
            try
            {
                int result = meatadaRepository.InsertEntityPeriod(jObj);
                return result;
            }
            catch
            {
                return 0;
            }

        }

        [HttpPost]
        public string GetEntitiPeriodByIdForGantt(JObject jObj)
        {
            try
            {
                string result = meatadaRepository.GetEntitiPeriodByIdForGantt(jObj);
                return result;
            }
            catch
            {
                return null;
            }

        }

        [HttpPost]
        public bool PostEntityPeriod(JObject jObj)
        {
            try
            {
                bool result = meatadaRepository.PostEntityPeriod(jObj);
                return result;
            }
            catch
            {
                return false;
            }

        }

        [HttpPost]
        public bool DeleteEntityPeriod(JObject jObj)
        {
            try
            {
                bool result = meatadaRepository.DeleteEntityPeriod((int)jObj["periodid"]);
                return result;
            }
            catch
            {
                return false;
            }

        }

        [HttpPost]
        public string GetAutoCompleteMemberList(JObject jObj)
        {
            try
            {
                return meatadaRepository.GetAutoCompleteMemberList((string)jObj["QueryString"]);
            }
            catch
            {
                return null;
            }

        }
        [HttpPost]
        public IList<HolidayDetailsDao> GetNonBusinessDays(JObject jObj)
        {
            try
            {
                FileInfo fi = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "errorlog.txt");

                using (TextWriter txtWriter = new StreamWriter(fi.Open(FileMode.Truncate)))
                {
                    txtWriter.Write("Service reached to GetNonBusinessDays" + Environment.NewLine);

                }
                IList<HolidayDetailsDao> OptionDetails = meatadaRepository.GetNonBusinessDays();
                return OptionDetails;
            }
            catch (Exception e)
            {
                FileInfo fi = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "errorlog.txt");

                using (TextWriter txtWriter = new StreamWriter(fi.Open(FileMode.Truncate)))
                {
                    txtWriter.Write("Service reached to GetNonBusinessDays catch" + e.ToString() + Environment.NewLine);

                }
                return null;
            }

        }
        [HttpPost]
        public IList<HolidayDetailsDao> GetHolidaysDetails(JObject jObj)
        {
            try
            {
                IList<HolidayDetailsDao> OptionDetails = meatadaRepository.GetHolidaysDetails();
                return OptionDetails;
            }
            catch
            {
                return null;
            }

        }

        [HttpPost]
        public JObject InsertUpdatePredefinedAttributeGroupData([FromBody] JObject jObj)
        {
            try
            {
                int AttrGrpId = Convert.ToInt32(jObj["AttributeGroupId"].ToString());
                return meatadaRepository.InsertUpdatePredefinedAttributeGroupData(jObj["AttributeData"].ToString(), jObj["AttributeHeader"].ToString(), AttrGrpId);
            }
            catch
            {
                return null;
            }
        }

        [HttpGet]
        public JObject GetListOfEntityTypeAttributeGroupValues(int EntityTypeID, int AttrGrpId)
        {
            try
            {
                return meatadaRepository.GetListOfEntityAttributesGroupValues(EntityTypeID, AttrGrpId);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        [HttpGet]
        public JObject GetEntityTypeAttributeDetailsList(int EntityTypeId, int ParentId)
        {
            try
            {
                return meatadaRepository.GetEntityTypeAttributeDetailsList(EntityTypeId, ParentId);

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpGet]
        public JObject GetEntityList(string EntityTypeId)
        {
            try
            {
                return meatadaRepository.GetEntityList(EntityTypeId);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpGet]
        public JObject GetObjectiveList(string EntityTypeId)
        {
            try
            {
                return meatadaRepository.GetObjectiveList(EntityTypeId);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPut]
        public bool UpdateObjectiveList(int EntityId, int ObjectiveID, decimal PlannedTrgt, decimal TrgtOutcome)
        {
            try
            {
                return meatadaRepository.UpdateObjectiveList(EntityId, ObjectiveID, PlannedTrgt, TrgtOutcome);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpGet]
        public JObject GetEntityFinancialList(string EntityTypeId)
        {
            try
            {
                return meatadaRepository.GetEntityFinancialList(EntityTypeId);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [HttpPost]
        public IList<IUser> GetUsers(JObject jObj)
        {
            try
            {
                IList<IUser> UserDetails = meatadaRepository.GetUsers();
                return UserDetails;
            }
            catch
            {
                return null;
            }

        }
        #endregion
    }
}