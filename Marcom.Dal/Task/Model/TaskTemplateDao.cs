﻿using System;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Dal.Planning.Model
{

    /// <summary>
    /// PurchaseOrderDao object for table 'PM_PurchaseOrder'.
    /// </summary>

    public partial class TaskTemplateDao : BaseDao, ICloneable
    {
        #region Member Variables

        protected int _id;
        protected string _Name;
        protected string _description;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
        protected string _templatecriteriatext;
        #endregion

        #region Constructors
        public TaskTemplateDao() { }

        public TaskTemplateDao(string pName, string pDescription)
        {

            this._Name = pName;
            this._description = pDescription;
        }

        public TaskTemplateDao(int pId)
        {
            this._id = pId;
        }

        #endregion

        #region Public Properties

        public virtual int ID
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }



        public virtual string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 1000000)
                    throw new ArgumentOutOfRangeException("Description", "Description value, cannot contain more than 1000000 characters");
                _bIsChanged |= (_description != value);
                _description = value;
            }

        }

        public virtual string TemplateCriteriaText
        {
            get { return _templatecriteriatext; }
            set
            {
                if (value != null && value.Length > 1000000)
                    throw new ArgumentOutOfRangeException("TemplateCriteriaText", "TemplateCriteriaText value, cannot contain more than 1000000 characters");
                _bIsChanged |= (_templatecriteriatext != value);
                _templatecriteriatext = value;
            }

        }

        public virtual string Name
        {
            get { return _Name; }
            set { _bIsChanged |= (_Name != value); _Name = value; }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion

        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {
            public const string TaskTemplateDao = "TaskTemplateDao";
            public const string ID = "ID";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string TemplateCriteriaText = "TemplateCriteriaText";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string ID = "ID";
            public const string Name = "Name";
            public const string Description = "Description";
            public const string TemplateCriteriaText = "TemplateCriteriaText";
        }

        #endregion


    }

}
