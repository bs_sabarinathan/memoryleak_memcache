﻿(function (ng, app) {
    "use strict";

    function muidamCtrl($window, $location, $timeout, $scope, $cookies, $resource, $stateParams, $translate, DamNewService, $compile, $modal) {

        var cloudpath = (cloudsetup.Uploaderurl + '\\' + cloudsetup.BucketName + '\\' + TenantFilePath).replace(/\\/g, "\/");
        var imageBaseUrlcloud = TenantFilePath;
        if (parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) {
            imageBaseUrlcloud = cloudpath;
        }
        $('#left_folderTree').css('display', 'none');
        $('#right_DamView').removeClass('cent');
        $scope.showhideall = $translate.instant('LanguageContents.Res_4939.Caption');
        $scope.SettingsAttributes = {};
        var bootboxconfirmTimer;
        $scope.AttachemtstoTask = [];
        $scope.AttachmentFilename = [];
        $scope.FilestoTask = [];
        $scope.FileList = [];
        $scope.ProgressMsgHeader = "";
        $scope.ProgressContent = $translate.instant('LanguageContents.Res_4219.Caption');
        $scope.contextFileObj = {
            node: {},
            parent: {}
        };
        $scope.select2Options = {
            tags: [],
            multiple: true,
            simple_tags: false,
            minimumInputLength: 1,
            formatResult: function (item) {
                return item.text;
            },
            formatSelection: function (item) {
                return item.text;
            }
        }
        $scope.userpublishaccess = false;

        var firstparentfolder = {};
        var timerObj_dam = {};

        /******** Current user publishacces ********/
        function userpublishaccess(entityID) {
            DamNewService.userpublishaccess(entityID).then(function (result) {
                $scope.userpublishaccess = result.Response;
            });
        }

        userpublishaccess($stateParams.ID);

        $scope.subview = "Thumbnail";
        $scope.AssetFilesObj = {
            AssetFiles: [],
            AssetTaskObj: [],
            AssetDynamicData: [],
            AssetSelection: [],
            PageNo: 1,
            OrderBy: 4,
            selectedAssetId: 0,
            "HideShowllMenus": true,
            "HideAddFolder": true,
            "HideAddAsset": true,
            "HideShowall": true,
            "HideCreateTask": true,
            "HideCut": true,
            IsLock: false,
            "HideAssetApproval": true,
            "HideproofApproval": true,
            "HidePublishDAM": true,
            "HideEntityAttachment": false
        };
        $scope.FilesToDAM = [];
        $scope.DownloadFileObj = {
            DownloadFilesArr: []
        };
        $scope.AssetListSelection = {
            AssetSelection: {},
            AssetChkClass: {}
        };
        $scope.AssetListSelectionClass = {
            AssetSelectionClass: {}
        };
        $scope.newsFeedinfo = [];
        $scope.FolderOption = {
            showAdd: false
        }
        $scope.userid = parseInt($.cookie('UserId'), 10);
        $scope.userisProofInitiator = false;
        $scope.EntityID = parseInt($stateParams.ID, 10);
        if ($scope.EntityID != null && $scope.EntityID != undefined && !isNaN($scope.EntityID)) {
            DamNewService.GetMember($scope.EntityID).then(function (member) {
                $scope.memberList = member.Response;
                var loggedinUserDets = [];
                if ($scope.memberList != null) {
                    loggedinUserDets = ($scope.memberList.length > 0) ? $.grep($scope.memberList, function (e) {
                        return e.Userid == $scope.userid
                    }) : [];
                }
                if (loggedinUserDets.length > 0) {
                    var loggedinUserRoles = $.grep(loggedinUserDets, function (e) {
                        return e.BackendRoleID == EntityRoles.ProofInitiator
                    });
                    if (loggedinUserRoles.length > 0) {
                        $scope.userisProofInitiator = true;
                    }
                }
            });
        }
        $scope.ColorCodeGlobalObj = {};
        $scope.ColorCodeGlobalObj.colorcode = 'ffd300';
        $scope.ColorOptions = {
            preferredFormat: "hex",
            showInput: true,
            showAlpha: false,
            allowEmpty: true,
            showPalette: true,
            showPaletteOnly: false,
            togglePaletteOnly: true,
            togglePaletteMoreText: 'more',
            togglePaletteLessText: 'less',
            showSelectionPalette: true,
            chooseText: "Choose",
            cancelText: "Cancel",
            showButtons: true,
            clickoutFiresChange: true,
            palette: [
				["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)", "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
				["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)", "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
				["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)", "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)", "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)", "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
            ]
        };
        $scope.colorchange = function (color) { }
        $scope.$on('editthumbnail', function (event, ID) {
            $scope.$broadcast('openassetpopup', ID);
        });
        $scope.$on('LoadUpdatedAsset', function (event, assetID, ActiveVersion) {
            $scope.$broadcast('Loadupdatedassetinthumnail', assetID, ActiveVersion);
        });
        $scope.$on('loadAssetDetailSection', function (event, arrlist) {
            if ($scope.DamViewName == "Thumbnail") {
                $scope.$broadcast('loadAssetDetailSection_th', arrlist);
            } else if ($scope.DamViewName == "Summary") {
                $scope.$broadcast('loadAssetDetailSection_th_Summary', arrlist);
            } else if ($scope.DamViewName == "List") {
                $scope.$broadcast('loadAssetDetailSection_th_List', arrlist);
            }
        });
        $scope.selectedTree = {};
        $scope.TaskStatusObj = [{
            "Name": "Thumbnail",
            ID: 1,
            "uilabel": $translate.instant('LanguageContents.Res_4857.Caption')
        }, {
            "Name": "Summary",
            ID: 2,
            "uilabel": $translate.instant('LanguageContents.Res_4811.Caption')
        }, {
            "Name": "List",
            ID: 3,
            "uilabel": $translate.instant('LanguageContents.Res_663.Caption')
        }];
        $scope.AssetStatusObj = [{
            "Name": "Prepared",
            ID: 1
        }, {
            "Name": "Approved",
            ID: 2
        }, {
            "Name": "Published",
            ID: 3
        }];
        $scope.OrderbyObj = [{
            "Name": "Name (Ascending)",
            ID: 1
        }, {
            "Name": "Name (Descending)",
            ID: 2
        }, {
            "Name": "Creation date (Ascending)",
            ID: 3
        }, {
            "Name": "Creation date (Descending)",
            ID: 4
        }];
        $scope.MulitipleFilterStatus = 1;
        $scope.DamViewName = "Thumbnail";
        $scope.damviewuilabel = $translate.instant('LanguageContents.Res_4857.Caption');
        $scope.OrderbyName = $translate.instant('LanguageContents.Res_4931.Caption');
        $scope.callbackid = 0;
        $scope.AssetFilesObj.HideCreateTask = true;
        $scope.AssetFilesObj.HideAssetApproval = true;
        $scope.AssetFilesObj.HideproofApproval = true;
        $scope.AssetFilesObj.HidePublishDAM = true;
        $scope.attachmentAddIsLock = !$scope.AssetFilesObj.IsLock;
        if ($scope.processingsrcobj.processingplace == "task") {
            $scope.AssetFilesObj.HideAddFolder = false;
            $scope.AssetFilesObj.HideCreateTask = false;
            $scope.AssetFilesObj.HideShowall = false;
            $scope.AssetFilesObj.HideAddAsset = true;
            $scope.AssetFilesObj.HideCut = false;
            $scope.AssetFilesObj.IsLock = $scope.processingsrcobj.processinglock;
            $scope.AssetFilesObj.HideAssetApproval = false;
            $scope.AssetFilesObj.HideproofApproval = false;
            $scope.AssetFilesObj.HidePublishDAM = false;
            $scope.AssetFilesObj.HideEntityAttachment = true;
            $scope.entityid
        }
        if ($scope.processingsrcobj.processingplace == "attachment") {
            $scope.AssetFilesObj.IsLock = $scope.processingsrcobj.processinglock;
        }
        $scope.FilterByOptionChange = function (id) {
            $scope.AssetFilesObj.PageNo = 1;
            var res = $.grep($scope.TaskStatusObj, function (e) {
                return e.ID == id;
            });
            $scope.DamViewName = res[0].Name;
            $scope.damviewuilabel = res[0].uilabel;
            if ($scope.IsShowallAssets.Fromshowallassets == 1) {
                $scope.IsShowallAssets.DamViewName = id;
            }
            if ($scope.showhideall == $translate.instant('LanguageContents.Res_4940.Caption')) {
                $('#assetview').css('display', 'none')
                $scope.showallassetview = "showallasset";
                if (id != $scope.MulitipleFilterStatus) {
                    $scope.MulitipleFilterStatus = id;
                }
                $scope.$broadcast('LoadShowallassets', id, $scope.AssetFilesObj.OrderBy);
            } else if (id != $scope.MulitipleFilterStatus) {
                $scope.MulitipleFilterStatus = id;
                $('#assetview').css('display', 'block')
                $scope.subview = res[0].Name;
                $scope.showallassetview = "";
            }
        }
        var apple_selected, tree;
        $scope.my_tree_handler = function (branch) {
            selectedAssetIDs = [];
            selectedAssetFiles = [];
            $scope.showhideall = $translate.instant('LanguageContents.Res_4939.Caption');
            $('#assetview').css('display', 'block')
            $scope.showallassetview = "";
            if ($scope.callbackid > 0) $scope.AssetFilesObj.PageNo = 1;
            if (branch.id == 0) $scope.FolderOption.showAdd = false;
            else $scope.FolderOption.showAdd = true;
            var res = $.grep($scope.TaskStatusObj, function (e) {
                return e.ID == $scope.MulitipleFilterStatus;
            });
            $scope.selectedTree = branch;
            $scope.Folderselect.ID = branch.id;
            $scope.Folderselect.foldername = branch.Caption;
            $scope.Folderselect.EntityID = $scope.processingsrcobj.processingid;
            DamNewService.GetBreadCrumFolderPath($scope.processingsrcobj.processingid, branch.id).then(function (data) {
                $scope.folderpath = [];
                $scope.temppath = [];
                $scope.temppath = data.Response;
                if ($scope.temppath.length > 0) {
                    for (var i = 0, attr; attr = $scope.temppath[i++];) {
                        $scope.folderpath.push(attr);
                    }
                }
            });
            if (res[0].ID == 1) {
                if ($scope.callbackid > 0) $timeout(function () {
                    $scope.$broadcast('LoadThumbnailAssetView', branch.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                }, 100);
            } else if (res[0].ID == 2) {
                if ($scope.callbackid > 0) $timeout(function () {
                    $scope.subview = "Summary";
                    $scope.$broadcast('LoadSummaryAssetView', branch.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                }, 100);
            } else if (res[0].ID == 3) {
                if ($scope.callbackid > 0) $timeout(function () {
                    $scope.subview = "List";
                    $scope.$broadcast('LoadListAssetView', branch.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                }, 100);
            }
            $scope.callbackid = $scope.callbackid + 1;
        };
        $scope.foldercontextclick = function (branch, parent) {
            if (branch.id == 0) $scope.FolderOption.showAdd = false;
            else $scope.FolderOption.showAdd = true;
            $scope.contextFileObj.node = branch;
            $scope.contextFileObj.parent = parent;
        };
        $scope.Dam_Directory = [];
        $scope.my_tree = tree = {};
        $scope.FolderExpandCollapseObj = {
            "Status": false
        };
        $scope.FolderExpandCollapseAction = function (flag) {
            if (flag == true) $scope.my_tree.expand_all();
            else $scope.my_tree.collapse_all();
        }
        $scope.try_async_load = function () {
            $scope.doing_async = true;
            $scope.showallassetview = '';
            return $timeout(function () {
                if ($scope.processingsrcobj.processingid != null && $scope.processingsrcobj.processingid != undefined && !isNaN($scope.processingsrcobj.processingid)) {
                    DamNewService.GetEntityDamFolders($scope.processingsrcobj.processingid).then(function (data) {
                        var res = [];
                        if ($scope.processingsrcobj.processingplace == "task") {
                            res.Children = [];
                            res.Children.push({
                                "Caption": "Root",
                                "Description": "Root",
                                "Level": 0,
                                id: 0,
                                SortOrder: 0,
                                IsDeleted: false,
                                Key: $scope.processingsrcobj.processingid.toString(),
                                Children: []
                            });
                        } else res = JSON.parse(data.Response);
                        var Children = [];
                        if (res.Children.length == 0) {
                            $('#left_folderTree').css('display', 'none');
                            $('#right_DamView').addClass('cent');
                            $timeout(function () {
                                var stateObj = $.grep($scope.TaskStatusObj, function (e) {
                                    return e.ID == $scope.MulitipleFilterStatus;
                                });
                                if (stateObj[0].ID == 1) $timeout(function () {
                                    $scope.$broadcast('LoadThumbnailAssetView', -100, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo);
                                }, 200);
                                else if (stateObj[0].ID == 2) $timeout(function () {
                                    $scope.subview = "Summary";
                                    $scope.$broadcast('LoadSummaryAssetView', -100, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                                }, 200);
                                else if (stateObj[0].ID == 3) $timeout(function () {
                                    $scope.subview = "List";
                                    $scope.$broadcast('LoadListAssetView', -100, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                                }, 200);
                            }, 20);
                        } else if (res != null) {
                            $('#left_folderTree').css('display', 'block');
                            $('#right_DamView').removeClass('cent');
                            for (var i = 0, child; child = res.Children[i++];) {
                                Children.push(child);
                            }
                            if (Children.length > 0) {
                                firstparentfolder.id = Children[0].id;
                            }
                            else {
                                firstparentfolder = null;
                            }
                            $timeout(function () {
                                var foldername = '';
                                if ($scope.MediaBankSettings.directfromMediaBank) {
                                    foldername = $scope.MediaBankSettings.FolderName;
                                } else {
                                    if ($scope.MediaBankSettings.folderName != '') foldername = $scope.MediaBankSettings.folderName;
                                    else foldername = Children[0].Caption;
                                }
                                $scope.initialTreeSelect = foldername;
                                $scope.MediaBankSettings.directfromMediaBank = false;
                                $scope.MediaBankSettings.FolderName = false;
                                $scope.Dam_Directory = Children;
                                $scope.doing_async = false;
                                $scope.MediaBankSettings.folderName = '';
                                var treefolder = '<dam-tree tree-data="Dam_Directory" tree-control="my_tree" on-contextclick="foldercontextclick(branch,parent)" on-select="my_tree_handler(branch)" expand-level="2" initial-selection="initialTreeSelect"></dam-tree>';
                                $("#DamFolder").html($compile(treefolder)($scope));
                                var stateObj = $.grep($scope.TaskStatusObj, function (e) {
                                    return e.ID == $scope.MulitipleFilterStatus;
                                });
                                $scope.doing_async = false;
                                $timeout(function () {
                                    var b;
                                    try {
                                        b = tree.get_selected_branch();
                                    }
                                    catch (ex) {
                                        //console.log(ex.message);
                                        b = firstparentfolder;
                                    }
                                    if (b == null || b == undefined) {
                                        b = firstparentfolder;
                                    }
                                    if (b != undefined) {
                                        if (stateObj[0].ID == 1) $timeout(function () {
                                            if (b != null) {
                                                $scope.$broadcast('LoadThumbnailAssetView', b.id, $scope.processingsrcobj.processingid, 1, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo);
                                            }
                                        }, 200);
                                        else if (stateObj[0].ID == 2) $timeout(function () {
                                            $scope.subview = "Summary";
                                            $scope.$broadcast('LoadSummaryAssetView', b.id, $scope.processingsrcobj.processingid, 2);
                                        }, 200);
                                        else if (stateObj[0].ID == 3) $timeout(function () {
                                            $scope.subview = "List";
                                            $scope.$broadcast('LoadListAssetView', b.id, $scope.processingsrcobj.processingid, 3);
                                        }, 200);
                                    }
                                }, 20);
                                if ($scope.processingsrcobj.processingplace == "task") {
                                    $('#left_folderTree').css('display', 'none');
                                    $('#right_DamView').addClass('cent');
                                }
                            }, 10);
                        } else {
                            $timeout(function () {
                                var stateObj = $.grep($scope.TaskStatusObj, function (e) {
                                    return e.ID == $scope.MulitipleFilterStatus;
                                });
                                if (stateObj[0].ID == 1) $timeout(function () {
                                    $scope.$broadcast('LoadThumbnailAssetView', -100, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus, $scope.AssetFilesObj.OrderBy, $scope.AssetFilesObj.PageNo);
                                }, 200);
                                else if (stateObj[0].ID == 2) $timeout(function () {
                                    $scope.subview = "Summary";
                                    $scope.$broadcast('LoadSummaryAssetView', -100, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                                }, 200);
                                else if (stateObj[0].ID == 3) $timeout(function () {
                                    $scope.subview = "List";
                                    $scope.$broadcast('LoadListAssetView', -100, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                                }, 200);
                            }, 20);
                        }
                    });
                }
            }, 10);
        };

        function PageLoad() {
            if (tree.length > 0) {
                tree.splice(0, tree.length);
            }
            $scope.subview = "";
            $scope.my_tree = tree;
            if ($scope.Dam_Directory.length > 0) {
                $scope.Dam_Directory.splice(0, $scope.Dam_Directory.length);
            }
            $scope.selectedTree = {};
            $scope.subview = "Thumbnail";
            if ($scope.entitylockstatus.isUpdatedfrmsection == false) {
                DamNewService.GetLockStatus($stateParams.ID).then(function (entitylockstatus) {
                    $scope.processingsrcobj.processinglock = entitylockstatus.Response.m_Item1;
                    $scope.AssetFilesObj.IsLock = $scope.processingsrcobj.processinglock;
                    GetDAMViewSettings();
                    GetDAMToolTipSettings();
                });
            }
            else {
                $scope.processingsrcobj.processinglock = $scope.IsLock;
                $scope.AssetFilesObj.IsLock = $scope.IsLock;
                GetDAMViewSettings();
                GetDAMToolTipSettings();
            }
        }

        function PageLoadFromTrigger() {
            $('#left_folderTree').css('display', 'none');
            var res = $.grep($scope.TaskStatusObj, function (e) {
                return e.ID == $scope.MulitipleFilterStatus;
            });
            if (res[0].ID == 1) {
                $('#assetthumbnailview').html('');
            } else if (res[0].ID == 2) {
                $('assetsummaryview').html('');
            } else if (res[0].ID == 3) {
                $('assetlistview').html('');
            }
            $scope.showhideall = $translate.instant('LanguageContents.Res_4939.Caption');
            $scope.subview = "";
            $('#assetview').css('display', 'block')
            $scope.showallassetview = '';
            if (tree.length > 0) {
                tree.splice(0, tree.length);
            }
            $scope.folderpath = [];
            $scope.my_tree = tree;
            if ($scope.Dam_Directory.length > 0) {
                $scope.Dam_Directory.splice(0, $scope.Dam_Directory.length);
            }
            $scope.selectedTree = {};
            $scope.subview = "Thumbnail";
            GetDAMViewSettings();
            GetDAMToolTipSettings();
        }

        function PageReLoadFromTrigger() {
            $scope.showhideall = $translate.instant('LanguageContents.Res_4939.Caption');
            $('#assetview').css('display', 'block')
            $scope.showallassetview = '';
            if (tree.length > 0) {
                tree.splice(0, tree.length);
            }
            $scope.folderpath = [];
            $scope.my_tree = tree;
            if ($scope.Dam_Directory.length > 0) {
                $scope.Dam_Directory.splice(0, $scope.Dam_Directory.length);
            }
            $scope.selectedTree = {};
            GetDAMViewSettings();
            GetDAMToolTipSettings();
        }

        function GetDAMViewSettings() {
            DamNewService.GetDAMViewSettings().then(function (data) {
                if (data.Response != null) {
                    $scope.SettingsAttributes["ThumbnailSettings"] = data.Response[0].ThumbnailSettings;
                    $scope.SettingsAttributes["SummaryViewSettings"] = data.Response[0].SummaryViewSettings;
                    $scope.SettingsAttributes["ListViewSettings"] = data.Response[0].ListViewSettings;
                    $scope.try_async_load();
                }
            });
        }

        function GetDAMToolTipSettings() {
            DamNewService.GetDAMToolTipSettings().then(function (data) {
                if (data.Response != null) {
                    $scope.SettingsAttributes["ThumbnailToolTipSettings"] = data.Response[0].ThumbnailSettings;
                    $scope.SettingsAttributes["ListViewToolTipSettings"] = data.Response[0].ListViewSettings;
                }
            });
        }
        PageLoad();
        $scope.FolderType = 0;
        $scope.addfolder = true;
        $scope.addRootfolder = true;
        $scope.parentFolderName = "";
        $scope.AddSublevel = true;
        $scope.Addsamelevel = false;
        $scope.CreateNewFolder = function () {
            var b;
            b = $scope.contextFileObj.node;
            $scope.AddSublevel = true;
            $scope.Addsamelevel = false;
            $scope.NewFolderCaption = "New Folder";
            $scope.NewFolderDescription = "";
            $scope.addfolder = true;
            $scope.parentFolderName = b.Caption;
            $timeout(function () {
                $('[id=dam-newfoldername]:enabled:visible:first').focus().select()
            }, 500);
            $("#addNewFolder").modal("show");
        }
        $scope.CreateSameLevelNewFolder = function () {
            var b;
            b = $scope.contextFileObj.node;
            $scope.AddSublevel = false;
            $scope.Addsamelevel = true;
            $scope.NewFolderCaption = "New Folder";
            $scope.NewFolderDescription = "";
            $scope.addfolder = true;
            $scope.parentFolderName = b.Caption;
            $timeout(function () {
                $('[id=dam-newfoldername]:enabled:visible:first').focus().select()
            }, 500);
            $("#addNewFolder").modal("show");
        }
        $scope.CreateNewRootFolder = function () {
            $scope.NewRootFolderCaption = "New Folder";
            $scope.NewRootFolderDescription = "";
            $scope.addRootfolder = true;
            $timeout(function () {
                $('[id=dam-newrootfoldername]:enabled:visible:first').focus().select()

                $('#addRootFolder').on('show.bs.modal', function () {

                    $(this).before($('.modal-backdrop'));
                    //Make sure the z-index is higher than the backdrop
                    $(this).css("z-index", parseInt($('.modal-backdrop').css('z-index')) + 1);
                });


            }, 500);


            $("#addRootFolder").modal("show");
        }




        $scope.try_adding_a_Root_branch = function () {
            if ($('#btnRootAddfolder').hasClass('disabled')) {
                return;
            }
            $('#btnRootAddfolder').addClass('disabled');
            if ($scope.NewRootFolderCaption == "" || $scope.NewRootFolderCaption == undefined) {
                $('#btnRootAddfolder').removeClass('disabled');
                bootbox.alert($translate.instant('LanguageContents.Res_4601.Caption'));
                return false;
            }
            var folderObj = {};
            folderObj.EntityId = $scope.processingsrcobj.processingid;
            folderObj.ParentNodeId = 0;
            folderObj.Caption = $scope.NewRootFolderCaption;
            folderObj.Description = $scope.NewRootFolderDescription;
            folderObj.Colorcode = $scope.ColorCodeGlobalObj.colorcode.replace("#", "");
            folderObj.Level = 0;
            folderObj.Key = $scope.processingsrcobj.processingid.toString();
            folderObj.Nodeid = 1;
            folderObj.SortOrder = $scope.Dam_Directory.length + 1;
            folderObj.id = 0;
            DamNewService.CreateNewFolder(folderObj).then(function (result) {
                if (result.Response == 0) {
                    $('#btnRootAddfolder').removeClass('disabled');
                    NotifyError($translate.instant('LanguageContents.Res_4281.Caption'));
                } else {
                    $('#btnRootAddfolder').removeClass('disabled');
                    $('#left_folderTree').css('display', 'block');
                    $('#right_DamView').removeClass('cent');
                    $scope.addRootfolder = true;
                    $scope.parentFolderName = "";
                    $scope.NewRootFolderCaption = "";
                    $scope.NewRootFolderDescription = "";
                    $scope.ColorCodeGlobalObj.colorcode = "ffd300";
                    NotifySuccess($translate.instant('LanguageContents.Res_4405.Caption'));
                    $(".addRootFolder").modal("hide");
                    $timeout(function () {
                        if ($scope.Dam_Directory.length > 0) {
                            return tree.add_root_branch({
                                Caption: folderObj.Caption,
                                Description: folderObj.Description,
                                ColorCode: folderObj.Colorcode.replace("#", ""),
                                Level: folderObj.Level,
                                Key: folderObj.Key,
                                id: result.Response,
                                SortOrder: folderObj.SortOrder,
                                IsDeleted: false,
                                Children: []
                            });
                        } else {
                            $scope.try_async_load();
                        }
                    }, 100);
                }
            });
        };
        $scope.try_adding_a_branch = function () {
            if ($('#btnAddfolder').hasClass('disabled')) {
                return;
            }
            $('#btnAddfolder').addClass('disabled');
            if ($scope.NewFolderCaption == "" || $scope.NewFolderCaption == undefined) {
                $('#btnAddfolder').removeClass('disabled');
                bootbox.alert($translate.instant('LanguageContents.Res_4601.Caption'));
                return false;
            }
            var b;
            b = $scope.contextFileObj.node;
            var folderObj = {};
            folderObj.EntityId = $scope.processingsrcobj.processingid;
            folderObj.ParentNodeId = b.id;
            folderObj.Caption = $scope.NewFolderCaption;
            folderObj.Colorcode = $scope.ColorCodeGlobalObj.colorcode.replace("#", "");
            folderObj.Description = $scope.NewFolderDescription;
            folderObj.Level = parseInt(b.Level) + 1;
            if (folderObj.id == 0) folderObj.Key = b.Key + "." + (b.Children.length + 1);
            else folderObj.Key = b.Key + '.' + (b.Children.length + 1);
            folderObj.Nodeid = (b.Children.length + 1);
            folderObj.SortOrder = (b.Children.length + 1);
            folderObj.id = 0;
            DamNewService.CreateNewFolder(folderObj).then(function (result) {
                if (result.Response == 0) {
                    $('#btnAddfolder').removeClass('disabled');
                    NotifyError($translate.instant('LanguageContents.Res_4281.Caption'));
                } else {
                    $('#btnAddfolder').removeClass('disabled');
                    $('#left_folderTree').css('display', 'block');
                    $('#right_DamView').removeClass('cent');
                    $scope.addfolder = true;
                    $scope.AddSublevel = true;
                    $scope.Addsamelevel = false;
                    $scope.parentFolderName = "";
                    $scope.NewFolderCaption = "";
                    $scope.NewFolderDescription = "";
                    $scope.ColorCodeGlobalObj.colorcode = "ffd300";
                    NotifySuccess($translate.instant('LanguageContents.Res_4405.Caption'));
                    $("#addNewFolder").modal("hide");
                    return tree.add_branch(b, {
                        Caption: folderObj.Caption,
                        Description: folderObj.Description,
                        ColorCode: folderObj.Colorcode.replace("#", ""),
                        Level: folderObj.Level,
                        Key: folderObj.Key,
                        id: result.Response,
                        SortOrder: folderObj.SortOrder,
                        IsDeleted: false,
                        Children: []
                    });
                }
            });
        };
        $scope.CreateNewSublevelfolder = function () {
            if ($scope.AddSublevel) $scope.try_adding_a_branch();
            else $scope.try_adding_SameLevel_branch();
        }
        $scope.try_adding_SameLevel_branch = function () {
            if ($('#btnAddfolder').hasClass('disabled')) {
                return;
            }
            $('#btnAddfolder').addClass('disabled');
            if ($scope.NewFolderCaption == "" || $scope.NewFolderCaption == undefined) {
                $('#btnAddfolder').removeClass('disabled');
                bootbox.alert($translate.instant('LanguageContents.Res_4601.Caption'));
                return false;
            }
            var b;
            b = $scope.contextFileObj.parent;
            var folderObj = {};
            folderObj.EntityId = $scope.processingsrcobj.processingid;
            folderObj.ParentNodeId = b != null ? b.id : 0;
            folderObj.Caption = $scope.NewFolderCaption;
            folderObj.Colorcode = $scope.ColorCodeGlobalObj.colorcode.replace("#", "");
            folderObj.Description = $scope.NewFolderDescription;
            folderObj.Level = b != null ? parseInt(b.Level) + 1 : 0;
            folderObj.Key = b != null ? (b.Key + "." + (b.Children.length + 1)) : $scope.processingsrcobj.processingid.toString();
            folderObj.Nodeid = b != null ? (b.Children.length + 1) : 1;
            folderObj.SortOrder = b != null ? (b.Children.length + 1) : $scope.Dam_Directory.length + 1;
            folderObj.id = 0;
            DamNewService.CreateNewFolder(folderObj).then(function (result) {
                if (result.Response == 0) {
                    $('#btnAddfolder').removeClass('disabled');
                    NotifyError($translate.instant('LanguageContents.Res_4281.Caption'));
                } else {
                    $('#btnAddfolder').removeClass('disabled');
                    $('#left_folderTree').css('display', 'block');
                    $('#right_DamView').removeClass('cent');
                    $scope.addfolder = true;
                    $scope.AddSublevel = false;
                    $scope.Addsamelevel = true;
                    $scope.parentFolderName = "";
                    $scope.NewFolderCaption = "";
                    $scope.NewFolderDescription = "";
                    $scope.ColorCodeGlobalObj.colorcode = "ffd300";
                    NotifySuccess($translate.instant('LanguageContents.Res_4405.Caption'));
                    $("#addNewFolder").modal("hide");
                    return tree.add_branch(b, {
                        Caption: folderObj.Caption,
                        Description: folderObj.Description,
                        ColorCode: folderObj.Colorcode.replace("#", ""),
                        Level: folderObj.Level,
                        Key: folderObj.Key,
                        id: result.Response,
                        SortOrder: folderObj.SortOrder,
                        IsDeleted: false,
                        Children: []
                    });
                }
            });
        };
        $scope.download = function () {
            if ($scope.DownloadFileObj.DownloadFilesArr.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4610.Caption'));
            }
            $scope.ProgressMsgHeader = "Downloading Assets";
            if ($scope.DownloadFileObj.DownloadFilesArr.length == 1) {
                var arr = $scope.DownloadFileObj.DownloadFilesArr;
                var ID = 0;
                var CategoryID = -1;
                CategoryID = $scope.DownloadFileObj.DownloadFilesArr[0].Category;
                if (CategoryID != 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_4127.Caption'));
                    refreshDownLoadScope(arr);
                    return fasle
                }
                if ($scope.DownloadFileObj.DownloadFilesArr[0].AssetId != undefined) {
                    ID = $scope.DownloadFileObj.DownloadFilesArr[0].AssetId;
                }
                var filename = ($scope.DownloadFileObj.DownloadFilesArr[0].FileName != undefined ? $scope.DownloadFileObj.DownloadFilesArr[0].FileName : $scope.DownloadFileObj.DownloadFilesArr[0].Name);
                var actualfilename = "";
                if (filename != undefined) {
                    for (var i = 0; i < filename.length; i++) {
                        if ((filename[i] >= '0' && filename[i] <= '9') || (filename[i] >= 'A' && filename[i] <= 'z' || (filename[i] == '.' || filename[i] == '_') || (filename[i] == ' '))) {
                            actualfilename = actualfilename + (filename[i]);
                        }
                    }
                }
                $timeout(function () {
                    var fileid = $scope.DownloadFileObj.DownloadFilesArr[0].Extension == '.zip' ? 'package' : $scope.DownloadFileObj.DownloadFilesArr[0].Fileguid,
						extn = $scope.DownloadFileObj.DownloadFilesArr[0].Extension == '.zip' ? 'zip' : $scope.DownloadFileObj.DownloadFilesArr[0].Extension;
                    var filename = ($scope.DownloadFileObj.DownloadFilesArr[0].FileName != undefined ? $scope.DownloadFileObj.DownloadFilesArr[0].FileName : $scope.DownloadFileObj.DownloadFilesArr[0].Name) + extn;
                    blockUIForDownload();
                    location.href = 'DAMDownload.aspx?FileID=' + $scope.DownloadFileObj.DownloadFilesArr[0].Fileguid + '&FileFriendlyName=' + actualfilename + '&Ext=' + extn + '&token=' + $('#download_token_value_id').val();
                    refreshDownLoadScope(arr);
                    NotifySuccess("1 " + $translate.instant('LanguageContents.Res_5808.Caption'));
                }, 100);
                $timeout(function () {
                    $scope.newsFeedinfo.push({
                        "AssetID": ID,
                        "action": "Download Asset",
                        "TypeName": "Asset Download"
                    });
                    DamNewService.CreateNewsFeedinfo($scope.newsFeedinfo).then(function (result) {
                        if (result.Response == null) { } else {
                            $scope.newsFeedinfo = [];
                            ID = 0;
                        }
                    });
                }, 100);
            } else if ($scope.DownloadFileObj.DownloadFilesArr.length > 1) {
                var arr = $scope.DownloadFileObj.DownloadFilesArr;
                var b;
                b = tree.get_selected_branch();
                var blank = $.grep($scope.DownloadFileObj.DownloadFilesArr, function (e) {
                    return e.Category > 0;
                });
                var fileasset = $.grep($scope.DownloadFileObj.DownloadFilesArr, function (e) {
                    return e.Category == 0;
                });
                var alerttext = $translate.instant('LanguageContents.Res_4848.Caption');
                if (blank.length > 0 && fileasset.length > 1) {
                    bootbox.confirm(alerttext, function (result) {
                        if (result) {
                            $timeout(function () {
                                DamNewService.DownloadFiles(arr).then(function (result) {
                                    var count = $scope.AssetFilesObj.AssetSelection.length;
                                    if (result.Response == null) {
                                        NotifyError($translate.instant('LanguageContents.Res_4317.Caption'));
                                        $('.loadingDownloadPageModel').modal("hide");
                                    } else {
                                        $scope.DownloadFileObj.DownloadFilesArr = [];
                                        $scope.AssetFilesObj.AssetTaskObj = [];
                                        $scope.AssetFilesObj.AssetSelection = [];
                                        var fileid = result.Response,
											extn = '.zip';
                                        var filename = 'package';
                                        blockUIForDownload();
                                        location.href = 'DAMDownload.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#download_token_value_id').val();
                                        refreshDownLoadScope(arr);
                                        var ht_Count = "";
                                        if (count > 0) ht_Count = "Assets";
                                        if (count == 1) ht_Count = "Asset";
                                        NotifySuccess(count + " " + ht_Count + " Successfully Downloaded");
                                    }
                                });
                            }, 100);
                        }
                    });
                } else if (blank.length > 0 && fileasset.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_4127.Caption'));
                    refreshDownLoadScope(arr);
                    return fasle
                } else if (blank.length > 0 && fileasset.length == 1) {
                    bootbox.confirm(alerttext, function (result) {
                        if (result) {
                            $timeout(function () {
                                var fileid = fileasset[0].Fileguid,
									extn = fileasset[0].Extension == '.zip' ? 'zip' : fileasset[0].Extension;
                                var filename = fileasset[0].FileName + extn;
                                blockUIForDownload();
                                location.href = 'DAMDownload.aspx?FileID=' + fileid + '&FileFriendlyName=' + fileasset[0].FileName + '&Ext=' + extn + '&token=' + $('#download_token_value_id').val();
                                refreshDownLoadScope(arr);
                            }, 100);
                        }
                    });
                    $timeout(function () {
                        $scope.newsFeedinfo.push({
                            "AssetID": fileasset[0].AssetId,
                            "action": "Download Asset",
                            "TypeName": "Asset Download"
                        });
                        DamNewService.CreateNewsFeedinfo($scope.newsFeedinfo).then(function (result) {
                            if (result.Response == null) { } else {
                                $scope.newsFeedinfo = [];
                                ID = 0;
                            }
                        });
                    }, 100);
                } else {
                    $timeout(function () {
                        blockUIForDownload();
                        DamNewService.DownloadFiles(arr).then(function (result) {
                            var count = $scope.DownloadFileObj.DownloadFilesArr.length;
                            if (result.Response == null) {
                                NotifyError($translate.instant('LanguageContents.Res_4317.Caption'));
                                $('.loadingDownloadPageModel').modal("hide");
                            } else {
                                $scope.DownloadFileObj.DownloadFilesArr = [];
                                $scope.AssetFilesObj.AssetTaskObj = [];
                                $scope.AssetFilesObj.AssetSelection = [];
                                var fileid = result.Response,
									extn = '.zip';
                                var filename = 'package';
                                location.href = 'DAMDownload.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#download_token_value_id').val();
                                refreshDownLoadScope(arr);
                                var ht_Count = "";
                                if (count > 0) ht_Count = "Assets";
                                if (count == 1) ht_Count = "Asset";
                                NotifySuccess(count + " " + ht_Count + " Successfully Downloaded");
                            }
                        });
                    }, 100);
                }
            }
        }


        $scope.$on('clearDamDownloadScope', function (event, assetIds) {
            refreshDownLoadScope(assetIds);
        });

        function refreshDownLoadScope(arr) {
            $("#damlistviewselectall").removeClass('checkbox checked');
            $('#damlistviewselectall').addClass('checkbox');
            $("#showalldamlistviewselectall").removeClass('checkbox checked');
            $('#showalldamlistviewselectall').addClass('checkbox');
            $("#listselectheader").removeClass("checked");
            $("#listselectheader").addClass("check");
            $("#showalllistselectheader").removeClass("checked");
            $("#showalllistselectheader").addClass("check");
            for (var i = 0, item; item = arr[i++];) {
                if ($scope.DamViewName == "Thumbnail") $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + item.AssetId] = "th-Selection";
                else if ($scope.DamViewName == "Summary") $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + item.AssetId] = "th-sum-Selection";
                else if ($scope.DamViewName == "List") $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + item.AssetId] = "li-Selection";
                $scope.AssetListSelection.AssetSelection["asset_" + item.AssetId] = false;
                $scope.AssetListSelection.AssetChkClass["asset_" + item.AssetId + ""] = "checkbox";
            }
            $scope.SelectAllAssetsObj.checkselectall = false;
            $("#icon_selectassets").removeClass("icon-check");
            $("#icon_selectassets").addClass("icon-unchecked");
            $scope.DownloadFileObj.DownloadFilesArr = [];
            $scope.AssetFilesObj.AssetTaskObj = [];
            $scope.AssetFilesObj.AssetSelection = [];
            $scope.DamAssetTaskselectionFiles.AssetFiles = [];
            $scope.DamAssetTaskselectionFiles.AssetDynamicData = [];
            $scope.AttachemtstoTask = [];
            $scope.FilestoTask = [];
            $scope.selectall = false;
        }

        function refreshDeleteableScope(arr) {
            $("#damlistviewselectall").removeClass('checkbox checked');
            $('#damlistviewselectall').addClass('checkbox');
            $("#showalldamlistviewselectall").removeClass('checkbox checked');
            $('#showalldamlistviewselectall').addClass('checkbox');
            $("#listselectheader").removeClass("checked");
            $("#listselectheader").addClass("check");
            $("#showalllistselectheader").removeClass("checked");
            $("#showalllistselectheader").addClass("check");
            for (var i = 0, item; item = arr[i++];) {
                if ($scope.DamViewName == "Thumbnail") $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + item] = "th-Selection";
                else if ($scope.DamViewName == "Summary") $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + item] = "th-sum-Selection";
                else if ($scope.DamViewName == "List") $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + item] = "li-Selection";
                $scope.AssetListSelection.AssetSelection["asset_" + item] = false;
                $scope.AssetListSelection.AssetChkClass["asset_" + item + ""] = "checkbox";
            }
            $scope.DownloadFileObj.DownloadFilesArr = [];
            $scope.AssetFilesObj.AssetTaskObj = [];
            $scope.AssetFilesObj.AssetSelection = [];
            $scope.DamAssetTaskselectionFiles.AssetFiles = [];
            $scope.DamAssetTaskselectionFiles.AssetDynamicData = [];
            $scope.selectall = false;
        }
        var fileDownloadCheckTimer;

        function blockUIForDownload() {
            var token = new Date().getTime();
            $('#download_token_value_id').val(token);
            $('#packagedownload_token_id').val(token);
            $scope.ProgressContent = "Downloading";
            $('#loadingDownloadPageModel').modal("show");
            fileDownloadCheckTimer = setInterval(function () {
                var cookieValue = $.cookie('fileDownloadToken');
                if (cookieValue == token) {
                    finishDownload();
                }
            }, 1000);
        }

        function finishDownload() {
            window.clearInterval(fileDownloadCheckTimer);
            $.removeCookie('fileDownloadToken');
            $('.loadingDownloadPageModel').modal("hide");
        }
        $scope.EditFolder = function () {
            var b, p;
            b = $scope.contextFileObj.node;
            if (b.Level != 0) {
                p = $scope.contextFileObj.parent;
                $scope.parentFolderName = p != undefined ? p.Caption : "";
                $scope.addfolder = false;
                $scope.NewFolderCaption = b.Caption;
                $scope.NewFolderDescription = b.Description;
                $scope.ColorCodeGlobalObj.colorcode = b.ColorCode;
                $timeout(function () {
                    $('[id=dam-newfoldername]:enabled:visible:first').focus().select()
                }, 500);
                $("#addNewFolder").modal("show");
            } else {
                $scope.addRootfolder = false;
                $scope.NewRootFolderCaption = b.Caption;
                $scope.NewRootFolderDescription = b.Description;
                $scope.ColorCodeGlobalObj.colorcode = b.ColorCode;
                $timeout(function () {
                    $('[id=dam-newrootfoldername]:enabled:visible:first').focus().select()
                }, 500);
                $("#addRootFolder").modal("show");
            }
        }
        $scope.folderflag = false;
        $scope.UpdateFolder = function () {
            if ($('#btnUpdatefolder').hasClass('disabled')) {
                return;
            }
            $('#btnUpdatefolder').addClass('disabled');
            $scope.folderflag = true;
            if ($scope.NewFolderCaption == "" || $scope.NewFolderCaption == undefined) {
                $('#btnUpdatefolder').removeClass('disabled');
                bootbox.alert($translate.instant('LanguageContents.Res_4601.Caption'));
                return false;
            }
            var b;
            b = $scope.contextFileObj.node;
            var folderObj = {};
            folderObj.Caption = $scope.NewFolderCaption;
            folderObj.Description = $scope.NewFolderDescription;
            folderObj.Colorcode = $scope.ColorCodeGlobalObj.colorcode.replace("#", "");
            folderObj.id = b.id;
            DamNewService.UpdateFolder(folderObj).then(function (result) {
                if (result.Response == 0) {
                    $scope.folderflag = false;
                    $('#btnUpdatefolder').removeClass('disabled');
                    NotifyError($translate.instant('LanguageContents.Res_4359.Caption'));
                } else {
                    DamNewService.GetBreadCrumFolderPath($scope.processingsrcobj.processingid, b.id).then(function (data) {
                        $scope.folderpath = [];
                        $scope.temppath = [];
                        $scope.temppath = data.Response;
                        if ($scope.temppath.length > 0) {
                            for (var i = 0; i < $scope.temppath.length; i++) {
                                $scope.folderpath.push($scope.temppath[i]);
                            }
                        }
                    });
                    $scope.parentFolderName = "";
                    b.Caption = $scope.NewFolderCaption;
                    b.Description = $scope.NewFolderDescription;
                    b.ColorCode = folderObj.Colorcode;
                    $scope.addfolder = true;
                    $scope.NewFolderCaption = "";
                    $scope.NewFolderDescription = "";
                    $scope.folderflag = false;
                    $('#btnUpdatefolder').removeClass('disabled');
                    NotifySuccess($translate.instant('LanguageContents.Res_4408.Caption'));
                    $("#addNewFolder").modal("hide");
                }
            });
        }
        $scope.UpdateRootFolder = function () {
            if ($('#btnRootUpdatefolder').hasClass('disabled')) {
                return;
            }
            $('#btnRootUpdatefolder').addClass('disabled');
            if ($scope.NewRootFolderCaption == "" || $scope.NewRootFolderCaption == undefined) {
                $('#btnRootUpdatefolder').removeClass('disabled');
                bootbox.alert($translate.instant('LanguageContents.Res_4601.Caption'));
                return false;
            }
            var b;
            b = $scope.contextFileObj.node;
            var folderObj = {};
            folderObj.Caption = $scope.NewRootFolderCaption;
            folderObj.Description = $scope.NewRootFolderDescription;
            folderObj.Colorcode = $scope.ColorCodeGlobalObj.colorcode.replace("#", "");;
            folderObj.id = b.id;
            DamNewService.UpdateFolder(folderObj).then(function (result) {
                if (result.Response == 0) {
                    $('#btnRootUpdatefolder').removeClass('disabled');
                    NotifyError($translate.instant('LanguageContents.Res_4359.Caption'));
                } else {
                    DamNewService.GetBreadCrumFolderPath($scope.processingsrcobj.processingid, b.id).then(function (data) {
                        $scope.folderpath = [];
                        $scope.temppath = [];
                        $scope.temppath = data.Response;
                        if ($scope.temppath.length > 0) {
                            for (var i = 0; i < $scope.temppath.length; i++) {
                                $scope.folderpath.push($scope.temppath[i]);
                            }
                        }
                    });
                    $('#btnRootUpdatefolder').removeClass('disabled');
                    $scope.parentFolderName = "";
                    b.Caption = $scope.NewRootFolderCaption;
                    b.Description = $scope.NewRootFolderDescription;
                    b.ColorCode = folderObj.Colorcode;
                    $scope.addRootfolder = true;
                    $scope.NewRootFolderCaption = "";
                    $scope.NewRootFolderDescription = "";
                    $scope.ColorCodeGlobalObj.colorcode = "ffd300";
                    NotifySuccess($translate.instant('LanguageContents.Res_4408.Caption'));
                    $(".addRootFolder").modal("hide");
                }
            });
        }
        var delfolderIdObj = new Array();
        $scope.deleteFolder = function () {
            var b, p;
            b = $scope.contextFileObj.node;
            p = $scope.contextFileObj.parent;
            var alerttext = ($translate.instant('LanguageContents.Res_4089.Caption')) + " " + b.Caption + (b.Children.length > 0 ? " and its sub folders also" : "") + " ?";
            bootbox.confirm(alerttext, function (result) {
                if (result) {
                    bootboxconfirmTimer = $timeout(function () {
                        delfolderIdObj.push(b.id);
                        FormRecursiveChildTreenode(b.Children);
                        DamNewService.deleteFolder(delfolderIdObj).then(function (result) {
                            if (result.Response == 0) {
                                NotifyError($translate.instant('LanguageContents.Res_4300.Caption'));
                            } else {
                                NotifySuccess($translate.instant('LanguageContents.Res_4406.Caption'));
                                tree.remove_branch(p, b);
                                if ($scope.selectedTree != null) if (b === $scope.selectedTree) if ($scope.Dam_Directory.length > 0) tree.select_prev_branch(b);
                                else $scope.try_async_load();
                                b = tree.get_selected_branch();
                                var res = $.grep($scope.TaskStatusObj, function (e) {
                                    return e.ID == $scope.MulitipleFilterStatus;
                                });
                                if ($scope.Dam_Directory.length == 0) {
                                    $('#left_folderTree').css('display', 'none');
                                    $('#right_DamView').addClass('cent');
                                    $scope.folderpath = [];
                                } else {
                                    $('#left_folderTree').css('display', 'block');
                                    $('#right_DamView').removeClass('cent');
                                }
                                $scope.selectedTree = b;
                                $scope.Folderselect.ID = b.id;
                                $scope.Folderselect.foldername = b.Caption;
                                $scope.Folderselect.EntityID = $scope.processingsrcobj.processingid;
                                if (res[0].ID == 1) $timeout(function () {
                                    $scope.$broadcast('LoadThumbnailAssetView', b.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                                }, 100);
                                else if (res[0].ID == 2) $timeout(function () {
                                    $scope.$broadcast('LoadSummaryAssetView', b.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                                }, 100);
                                else if (res[0].ID == 3) $timeout(function () {
                                    $scope.$broadcast('LoadListAssetView', b.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                                }, 100);
                            }
                        });
                    }, 100);
                }
            });
        }
        $scope.deleteAssets = function () {
            if ($('#icon_selectassets').attr('class') != 'icon-check') {
                if ($scope.selectall == false) {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                    selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                } else {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                }
            }
            if (selectedAssetIDs.length > 0) {
                $scope.ProgressMsgHeader = "Performing Delete action";
                $scope.ProgressContent = "Processing..";
                var AssetLinkID = []
                if (selectedAssetIDs.length > 0) {
                    $scope.linkedassetsID = [];
                    for (var i = 0; i < selectedAssetIDs.length; i++) {
                        $scope.linkedassetsID = ($.grep(selectedAssetFiles, function (e) {
                            return e.LinkUserDetail.length > 0 && e.AssetUniqueID == selectedAssetIDs[i];
                        }));
                        if ($scope.linkedassetsID.length > 0) {
                            AssetLinkID.push($scope.linkedassetsID[0].AssetUniqueID);
                        }
                    }
                    if (AssetLinkID.length == selectedAssetIDs.length) {
                        bootbox.confirm($translate.instant('LanguageContents.Res_5783.Caption'), function (result) {
                            if (result) {
                                deleteAssetsservice();
                            }
                        });
                    } else if (AssetLinkID.length > 0) {
                        bootbox.confirm($translate.instant('LanguageContents.Res_5784.Caption'), function (result) {
                            if (result) {
                                deleteAssetsservice();
                            }
                        });
                    } else {
                        deleteAssetsservice();
                    }
                }
            } else bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'))
        }

        function deleteAssetsservice() {
            if (selectedAssetIDs.length > 0) {
                var ht_Count = "";
                if (selectedAssetIDs.length > 0) ht_Count = "Assets";
                if (selectedAssetIDs.length == 1) ht_Count = "Asset";
                bootbox.confirm($translate.instant('LanguageContents.Res_4950.Caption') + selectedAssetIDs.length + " " + ht_Count + "?", function (result) {
                    if (result) {
                        $('#loadingDownloadPageModel').modal("show");
                        bootboxconfirmTimer = $timeout(function () {
                            var data = {};
                            data.FolderArr = $scope.AssetFilesObj.AssetSelection;
                            DamNewService.deleteAssets(data).then(function (result) {
                                var count = selectedAssetIDs.length;
                                var b;
                                b = tree.get_selected_branch();
                                if (result.Response == 0) {
                                    $('.loadingDownloadPageModel').modal("hide");
                                    NotifyError($translate.instant('LanguageContents.Res_4285.Caption'));
                                } else {
                                    refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                                    refreshDeleteableScope(selectedAssetIDs);
                                    $scope.AssetFilesObj.AssetSelection = [];
                                    $scope.AssetFilesObj.PageNo = 1;
                                    $('.loadingDownloadPageModel').modal("hide");
                                    NotifySuccess(count + " " + $translate.instant('LanguageContents.Res_4100.Caption'));
                                    for (var j = 0; j < selectedAssetIDs.length; j++) {
                                        var res = $.grep($scope.TaskStatusObj, function (e) {
                                            return e.ID == $scope.MulitipleFilterStatus;
                                        });
                                        if (res[0].ID == 1) {
                                            $('#divthboxid_' + selectedAssetIDs[j]).addClass('displayNone');
                                            $('#showalldivthboxid_' + selectedAssetIDs[j]).addClass('displayNone');
                                        } else if (res[0].ID == 2) {
                                            $('#divthsumboxid_' + selectedAssetIDs[j]).addClass('displayNone');
                                            $('#showalldivthsumboxid_' + selectedAssetIDs[j]).addClass('displayNone');
                                        } else if (res[0].ID == 3) {
                                            $('#divthlistboxid_' + selectedAssetIDs[j]).addClass('displayNone');
                                            $('#showalldivthlistboxid_' + selectedAssetIDs[j]).addClass('displayNone');
                                        }
                                        var DelAssets = [],
											data = [];
                                        DelAssets = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                                            return e.AssetUniqueID == selectedAssetIDs[j];
                                        });
                                        if (DelAssets.length > 0) {
                                            $scope.AssetFilesObj.AssetFiles.splice($.inArray(DelAssets[0], $scope.AssetFilesObj.AssetFiles), 1);
                                        }
                                        var Dyndata = $.grep($scope.AssetFilesObj.AssetDynamicData, function (e) {
                                            return e.ID == selectedAssetIDs[j]
                                        })[0];
                                        if (Dyndata != undefined) {
                                            $scope.AssetFilesObj.AssetDynamicData.splice($.inArray(Dyndata, $scope.AssetFilesObj.AssetDynamicData), 1);
                                        }
                                    }
                                    if ($scope.processingsrcobj.processingplace == "task") {
                                        $timeout(function () {
                                            $scope.$emit('Taskassetinfoupdate');
                                        }, 100);
                                    }
                                    selectedAssetIDs = [];

                                    $scope.$broadcast('closeasseteditpopup');
                                }
                            });
                        }, 100);
                    }
                });
            } else bootbox.alert($translate.instant('LanguageContents.Res_4609.Caption'));
        }

        function FormRecursiveChildTreenode(children) {
            for (var j = 0, child; child = children[j++];) {
                var remainRecord = [];
                remainRecord = $.grep(delfolderIdObj, function (e) {
                    return e.id == child.id;
                });
                if (remainRecord.length == 0) {
                    delfolderIdObj.push(child.id);
                    if (child.Children.length > 0) {
                        FormRecursiveChildTreenode(child.Children);
                    }
                }
            }
        }
        $scope.$on('CallBackThumbnailAssetView', function (event, ID) {
            if (ID == 0) {
                $timeout(function () {
                    $scope.$emit('Taskassetinfoupdate');
                }, 100);
            }
            var res = $.grep($scope.TaskStatusObj, function (e) {
                return e.ID == $scope.MulitipleFilterStatus;
            });
            var b;
            b = $scope.selectedTree;
            //b = folderId;
            $timeout(function () {
                if (res[0].ID == 1) $timeout(function () {
                    if (b != null)
                        $scope.$broadcast('LoadThumbnailAssetView', b.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                }, 200);
                else if (res[0].ID == 2) $timeout(function () {
                    if (b != null)
                        $scope.$broadcast('LoadSummaryAssetView', b.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                }, 200);
                else if (res[0].ID == 3) $timeout(function () {
                    if (b != null)
                        $scope.$broadcast('LoadListAssetView', b.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                }, 200);
            }, 50);
        });
        $scope.$on('ReloadAssetView', function (event, data) {
            if (data != undefined && data != null && data != 0) {
                $scope.AssetFilesObj = data.AssetFilesObj;
                $scope.processingsrcobj = data.processingsrcobj
                $scope.entityid = data.EntityID;
            }
            if ($scope.processingsrcobj.processingplace == "attachment") {
                $scope.AssetFilesObj.HideShowllMenus = true;
                $scope.AssetFilesObj.HideCreateTask = true;
                $scope.AssetFilesObj.HideAddFolder = true;
                $scope.AssetFilesObj.HideAddAsset = true;
                $scope.AssetFilesObj.HideCut = true;
                $scope.AssetFilesObj.HideAssetApproval = true;
                $scope.AssetFilesObj.HideproofApproval = true;
                $scope.AssetFilesObj.HidePublishDAM = true;
                $scope.AssetFilesObj.HideEntityAttachment = false;
                $scope.AssetFilesObj.IsLock = $scope.processingsrcobj.processinglock;
                PageLoadFromTrigger();
            } else if ($scope.processingsrcobj.processingplace == "task") {
                $scope.AssetFilesObj.HideAddFolder = false;
                $scope.AssetFilesObj.HideCreateTask = false;
                $scope.AssetFilesObj.HideShowllMenus = false;
                $scope.AssetFilesObj.HideAddAsset = true;
                $scope.AssetFilesObj.HideCut = false;
                $scope.AssetFilesObj.HideAssetApproval = false;
                $scope.AssetFilesObj.HideproofApproval = false;
                $scope.AssetFilesObj.HidePublishDAM = false;
                $scope.AssetFilesObj.HideEntityAttachment = true;
                $scope.AssetFilesObj.IsLock = $scope.processingsrcobj.processinglock;
                userpublishaccess($scope.entityid);
                PageReLoadFromTrigger();
            }
        });
        $scope.Process = function () {
            bootbox.alert($translate.instant('LanguageContents.Res_4910.Caption'));
        }
        $scope.OrderbyOption = function (orderid) {
            var branch;
            branch = tree.get_selected_branch();
            if ($scope.showhideall == $translate.instant('LanguageContents.Res_4940.Caption')) {
                if (orderid != $scope.AssetFilesObj.OrderBy) {
                    $scope.AssetFilesObj.PageNo = 1;
                    $scope.AssetFilesObj.OrderBy = orderid;
                    var orderobj = $.grep($scope.OrderbyObj, function (e) {
                        return e.ID == orderid;
                    });
                    $scope.OrderbyName = orderobj[0].Name;
                    $('#assetview').css('display', 'none')
                    $scope.showallassetview = "showallasset";
                    $scope.$broadcast('LoadShowallassets', $scope.MulitipleFilterStatus, $scope.AssetFilesObj.OrderBy);
                }
            } else if (orderid != $scope.AssetFilesObj.OrderBy) {
                $scope.AssetFilesObj.PageNo = 1;
                $scope.AssetFilesObj.OrderBy = orderid;
                var orderobj = $.grep($scope.OrderbyObj, function (e) {
                    return e.ID == orderid;
                });
                $('#assetview').css('display', 'block');
                $scope.showallassetview = "";
                $scope.OrderbyName = orderobj[0].Name;
                var res = $.grep($scope.TaskStatusObj, function (e) {
                    return e.ID == $scope.MulitipleFilterStatus;
                });
                if (res[0].ID == 1) {
                    $timeout(function () {
                        if (branch != undefined)
                            $scope.$broadcast('LoadThumbnailAssetView', branch.id, $scope.processingsrcobj.processingid, 1);
                    }, 200);
                } else if (res[0].ID == 2) {
                    $timeout(function () {
                        if (branch != undefined)
                            $scope.$broadcast('LoadSummaryAssetView', branch.id, $scope.processingsrcobj.processingid, 2);
                    }, 200);
                } else if (res[0].ID == 3) {
                    $timeout(function () {
                        if (branch != undefined)
                            $scope.$broadcast('LoadListAssetView', branch.id, $scope.processingsrcobj.processingid, 3);
                    }, 200);
                }
            }
        }
        $scope.DuplicateAssets = function () {
            if ($('#icon_selectassets').attr('class') != 'icon-check') {
                if ($scope.selectall == false) {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                    selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                } else {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                }
            }
            if (selectedAssetIDs.length > 0) {
                var Link = [],
					temparr = [];
                angular.forEach(selectedAssetIDs, function (item) {
                    temparr = ($.grep(selectedAssetFiles, function (e) {
                        return e.LinkUserDetail.length > 0 && e.AssetUniqueID == item;
                    }));
                    if (temparr != null) if (temparr.length > 0) Link.push(temparr[0].AssetID);
                });
                if (Link.length == selectedAssetIDs.length) {
                    var ht_Count = "";
                    if (Link.length > 0) ht_Count = "Assets";
                    if (Link.length == 1) ht_Count = "Asset";
                    bootbox.alert($translate.instant('LanguageContents.Res_5757.Caption') + ht_Count + $translate.instant('LanguageContents.Res_5758.Caption'));
                    $scope.AssetFilesObj.AssetSelection = [];
                    selectedAssetIDs = [];
                    $scope.DownloadFileObj.DownloadFilesArr = [];
                    refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                    refreshDeleteableScope(selectedAssetIDs);
                    $scope.AssetFilesObj.PageNo = 1;
                    return false;
                } else if (Link.length > 0) {
                    bootbox.confirm($translate.instant('LanguageContents.Res_5785.Caption'), function (result) {
                        if (result) {
                            selectedAssetIDs = $.grep(selectedAssetIDs, function (e) {
                                return $.inArray(e, Link) == -1;
                            });
                            ProcessDuplicateAssets(selectedAssetIDs);
                        }
                    });
                } else if (Link.length == 0) {
                    ProcessDuplicateAssets(selectedAssetIDs);
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4607.Caption'));
            }
        }

        function ProcessDuplicateAssets(assets) {
            if (assets.length > 0) {
                $scope.ProgressMsgHeader = "Creating Duplicate Assets";
                $scope.ProgressContent = "Processing";
                $('#loadingDownloadPageModel').modal("show");
                var b;
                b = tree.get_selected_branch();
                var arr = assets;
                $timeout(function () {
                    DamNewService.DuplicateAssetFiles(arr).then(function (result) {
                        var count = selectedAssetIDs.length;
                        if (result.Response == 0) {
                            $scope.AssetFilesObj.AssetSelection = [];
                            selectedAssetIDs = [];
                            $('.loadingDownloadPageModel').modal("hide");
                            NotifyError($translate.instant('LanguageContents.Res_4324.Caption'));
                        } else {
                            $scope.AssetFilesObj.PageNo = 1;
                            $scope.AssetFilesObj.AssetSelection = [];
                            selectedAssetIDs = [];
                            refreshDownLoadScope(arr);
                            refreshDeleteableScope(arr);
                            $scope.DownloadFileObj.DownloadFilesArr = [];
                            $('.loadingDownloadPageModel').modal("hide");
                            var ht_Count = "";
                            if (count > 0) ht_Count = "Assets";
                            if (count == 1) ht_Count = "Asset";
                            NotifySuccess(count + " " + "Duplicate " + ht_Count + " created successfully");
                            if ($scope.showhideall == $translate.instant('LanguageContents.Res_4940.Caption')) {
                                $('#assetview').css('display', 'none')
                                $scope.showallassetview = "showallasset";
                                $scope.$broadcast('LoadShowallassets', $scope.MulitipleFilterStatus, $scope.AssetFilesObj.OrderBy);
                            } else {
                                $('#assetview').css('display', 'block');
                                $scope.showallassetview = "";
                                var res = $.grep($scope.TaskStatusObj, function (e) {
                                    return e.ID == $scope.MulitipleFilterStatus;
                                });
                                $timeout(function () {
                                    if (res[0].ID == 1) {
                                        $timeout(function () {
                                            $scope.$broadcast('LoadThumbnailAssetView', b.id, $scope.processingsrcobj.processingid, 1);
                                        }, 200);
                                    } else if (res[0].ID == 2) {
                                        $timeout(function () {
                                            $scope.$broadcast('LoadSummaryAssetView', b.id, $scope.processingsrcobj.processingid, 2);
                                        }, 200);
                                    } else if (res[0].ID == 3) {
                                        $timeout(function () {
                                            $scope.$broadcast('LoadListAssetView', b.id, $scope.processingsrcobj.processingid, 3);
                                        }, 200);
                                    }
                                }, 100);
                            }
                        }
                    });
                }, 100);
            } else bootbox.alert($translate.instant('LanguageContents.Res_4607.Caption'));
        }
        $scope.Addtolightbox = function () {
            if ($('#icon_selectassets').attr('class') != 'icon-check') {
                if ($scope.selectall == false) {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                    selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                } else {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                }
            }
            var count = selectedAssetIDs.length;
            var imageassetcount = 0;
            var blankassetcount = 0;
            var imageasset = [];
            if (selectedAssetIDs.length > 0) {
                for (var i = 0, assetid; assetid = selectedAssetIDs[i++];) {
                    var remainRecord = [];
                    remainRecord = $.grep($scope.damlightbox.lightboxselection, function (e) {
                        return e.AssetId == assetid;
                    });
                    if (remainRecord.length == 0) {
                        var asset = [],
							data = [];
                        asset = $.grep(selectedAssetFiles, function (e) {
                            return e.AssetUniqueID == assetid;
                        });
                        data = $.grep($scope.AssetFilesObj.AssetDynamicData, function (e) {
                            return e.ID == assetid;
                        });
                        if (asset[0].FileGuid != null && asset[0].Category == 0) {
                            if (asset.length > 0) {
                                $scope.damlightbox.lightboxselection.push({
                                    "AssetRawData": asset[0],
                                    "EntityID": $scope.processingsrcobj.processingid,
                                    "ShortDesc": asset[0].ShortDescription,
                                    "ColorCode": asset[0].ColorCode,
                                    "AssetId": assetid,
                                    "AssetName": asset[0].Name,
                                    "Description": asset[0].Description != null ? asset[0].Description : "-",
                                    "Guid": asset[0].FileGuid,
                                    "Ext": asset[0].Extension,
                                    "AssetStatus": asset[0].Status,
                                    "FileSize": parseSize(asset[0].Size),
                                    "DynamicData": data,
                                    "MimeType": asset[0].MimeType,
                                    "Extension": asset[0].Extension.substring(1, asset[0].Extension.length).toLowerCase(),
                                    "Category": asset[0].Category,
                                    "Iscropped": false
                                });
                                imageassetcount = imageassetcount + 1;
                                imageasset.push(assetid);
                            }
                        } else {
                            blankassetcount = blankassetcount + 1;
                        }
                    }
                }
                if ($scope.damlightbox.lightboxselection.length > 0 && imageassetcount > 0) {
                    refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                    refreshDeleteableScope(selectedAssetIDs);
                    $timeout(function () {
                        selectedAssetIDs = [];
                    }, 200);
                    var ht_Count = "";
                    if (imageassetcount > 0) ht_Count = "Assets";
                    if (imageassetcount == 1) ht_Count = "Asset";
                    NotifySuccess(imageassetcount + " " + ht_Count + " " + $translate.instant('LanguageContents.Res_4066.Caption'));
                    $scope.DownloadFileObj.DownloadFilesArr = [];
                    $scope.AssetFilesObj.AssetTaskObj = [];
                    $scope.AssetFilesObj.AssetSelection = [];
                    selectedAssetIDs = [];
                } else {
                    refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                    refreshDeleteableScope(selectedAssetIDs);
                    $timeout(function () {
                        selectedAssetIDs = [];
                    }, 200);
                    $scope.DownloadFileObj.DownloadFilesArr = [];
                    $scope.AssetFilesObj.AssetTaskObj = [];
                    $scope.AssetFilesObj.AssetSelection = [];
                    selectedAssetIDs = [];
                    if (blankassetcount > 0) {
                        bootbox.alert($translate.instant('LanguageContents.Res_5759.Caption'));
                    }
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }
        }
        $scope.PerformAssetOperation = function (action) {
            if (action == 3) {
                if ($scope.Dam_Directory.length > 0) if ($scope.damclipboard.assetselection != 0) {
                    $scope.ProgressMsgHeader = "Performing  action";
                    $scope.ProgressContent = "Processing..";
                    $('#loadingDownloadPageModel').modal("show");
                    var b = tree.get_selected_branch();
                    DamNewService.MoveAssets($scope.damclipboard.assetselection, b.id, $scope.processingsrcobj.processingid, $scope.damclipboard.actioncode).then(function (result) {
                        var scount = $scope.damclipboard.assetselection.length;
                        if (result.Response == 0) {
                            $('.loadingDownloadPageModel').modal("hide");
                            NotifyError($translate.instant('LanguageContents.Res_4937.Caption'));
                            $scope.damclipboard.assetselection = [];
                            $scope.damclipboard.actioncode = 0;
                        } else {
                            $('.loadingDownloadPageModel').modal("hide");
                            $scope.damclipboard.assetselection = [];
                            $scope.damclipboard.actioncode = 0;
                            refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                            refreshDeleteableScope($scope.AssetFilesObj.AssetSelection);
                            $timeout(function () {
                                $scope.AssetFilesObj.AssetSelection = [];
                            }, 200);
                            $scope.DownloadFileObj.DownloadFilesArr = [];
                            $scope.AssetFilesObj.AssetTaskObj = [];
                            $scope.AssetFilesObj.AssetSelection = [];
                            var ht_Count = "";
                            if (scount > 0) ht_Count = "Assets";
                            if (scount == 1) ht_Count = "Asset";
                            NotifySuccess(scount + " " + ht_Count + " pasted sucessfully");
                            if (b.id == 0) {
                                $timeout(function () {
                                    $scope.$emit('Taskassetinfoupdate');
                                }, 100);
                            }
                            var res = $.grep($scope.TaskStatusObj, function (e) {
                                return e.ID == $scope.MulitipleFilterStatus;
                            });
                            $timeout(function () {
                                $scope.AssetFilesObj.PageNo = 1;
                                if (res[0].ID == 1) $timeout(function () {
                                    $scope.$broadcast('LoadThumbnailAssetView', b.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                                }, 200);
                                else if (res[0].ID == 2) $timeout(function () {
                                    $scope.$broadcast('LoadSummaryAssetView', b.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                                }, 200);
                                else if (res[0].ID == 3) $timeout(function () {
                                    $scope.$broadcast('LoadListAssetView', b.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                                }, 200);
                            }, 100);
                        }
                    });
                } else bootbox.alert($translate.instant('LanguageContents.Res_5760.Caption'));
            }
            if (action == 1 || action == 2) {
                if ($('#icon_selectassets').attr('class') != 'icon-check') {
                    if ($scope.selectall == false) {
                        selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                        selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                    } else {
                        selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                    }
                }
                var count = selectedAssetIDs.length;
                if (selectedAssetIDs.length > 0) {
                    $scope.damclipboard.actioncode = action;
                    for (var i = 0, assetid; assetid = selectedAssetIDs[i++];) {
                        var remainRecord = [];
                        remainRecord = $.grep($scope.damclipboard.assetselection, function (e) {
                            return e.AssetId == assetid;
                        });
                        if (remainRecord.length == 0) {
                            $scope.damclipboard.assetselection.push({
                                "AssetId": assetid
                            });
                        }
                        if (action == 1) $('#divthboxid_' + assetid).addClass('opaque2');
                        else if (action == 2 && $('#divthboxid_' + assetid).hasClass('opaque2')) $('#divthboxid_' + assetid).removeClass('opaque2');
                    }
                    if ($scope.damclipboard.assetselection.length > 0) {
                        refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                        refreshDeleteableScope(selectedAssetIDs);
                        $timeout(function () {
                            selectedAssetIDs = [];
                        }, 200);
                        NotifySuccess(count + " " + $translate.instant('LanguageContents.Res_4099.Caption'));
                        $scope.DownloadFileObj.DownloadFilesArr = [];
                        $scope.AssetFilesObj.AssetTaskObj = [];
                        $scope.AssetFilesObj.AssetSelection = [];
                        selectedAssetIDs = [];
                    }
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
                }
            }
            if (action == 4) {
                $scope.damclipboard.assetselection = [];
                $scope.damclipboard.actioncode = 0;
            }
        }

        function parseSize(bytes) {
            var suffix = ["bytes", "KB", "MB", "GB", "TB", "PB"];
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return '0 Byte';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        }
        $scope.Viewlightbox = function () {
            if ($('#icon_selectassets').attr('class') != 'icon-check') {
                if ($scope.selectall == false) {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                    selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                } else {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                }
            }
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/DAM/damlightbox.html',
                controller: "mui.DAM.lightboxCtrl",
                resolve: {
                    params: function () {
                        return {
                            isfromDAM: true
                        };
                    }
                },
                scope: $scope,
                windowClass: 'in LightBoxPreviewPopup popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
            refreshDownLoadScope(selectedAssetIDs);
            refreshDeleteableScope(selectedAssetIDs);
        }
        $scope.checkAllassets = function ($event) {
            var checkbox = $event.target;
            if (checkbox.checked) {
                $scope.selectedAllassets = true;
            } else {
                $scope.selectedAllassets = false;
            }
            angular.forEach($scope.damlightbox.lightboxselection, function (item) {
                item.Selected = $scope.selectedAllassets;
            });
        };
        $scope.RemoveCheckallAssetSelection = function (list, $event) {
            var checkbox = $event.target;
            list.Selected = checkbox.checked;
            var taskCollection = $.grep($scope.damlightbox.lightboxselection, function (e) {
                return e.Selected == true;
            }).length;
            if (taskCollection != $scope.damlightbox.lightboxselection.length) $scope.selectedAllassets = false;
            else $scope.selectedAllassets = true;
        }
        var selectedAssetIDs = [];
        var selectedAssetFiles = [];
        var selectedAssetDynData = [];
        $scope.PublishAssets = function () {
            if ($('#icon_selectassets').attr('class') != 'icon-check') {
                if ($scope.selectall == false) {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                    selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                } else {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                }
            }
            if (selectedAssetIDs.length > 0) {
                var Blank = [],
					Blanktemparr = [],
					NOAccesstemparr = [],
					NOAccess = [],
					temp = [];
                temp = selectedAssetIDs;
                for (var i = 0; i < selectedAssetIDs.length; i++) {
                    NOAccesstemparr = $.grep(selectedAssetFiles, function (e) {
                        return e.AssetAccesscount == 0 && e.AssetUniqueID == selectedAssetIDs[i];
                    });
                    if (NOAccesstemparr != null) {
                        if (NOAccesstemparr.length > 0) {
                            NOAccess.push(NOAccesstemparr[0].AssetUniqueID);
                        }
                    }
                }
                if (NOAccess.length == selectedAssetIDs.length) {
                    bootbox.alert($translate.instant('LanguageContents.Res_5752.Caption'));
                    refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                    refreshDeleteableScope(temp);
                    $scope.DownloadFileObj.DownloadFilesArr = [];
                    $scope.AssetFilesObj.AssetTaskObj = [];
                    $scope.AssetFilesObj.AssetSelection = [];
                    selectedAssetIDs = [];
                    selectedAssetFiles = [];
                    return false;
                } else if (NOAccess.length != selectedAssetIDs.length && NOAccess.length > 0) {
                    bootbox.confirm($translate.instant('LanguageContents.Res_5786.Caption'), function (result) {
                        if (result) {
                            selectedAssetIDs = $.grep(selectedAssetIDs, function (e) {
                                return $.inArray(e, NOAccess) == -1;
                            });
                            for (var i = 0; i < selectedAssetIDs.length; i++) {
                                Blanktemparr = $.grep(selectedAssetFiles, function (e) {
                                    return e.Category == 1 && e.AssetUniqueID == selectedAssetIDs[i];
                                });
                                if (Blanktemparr != null) {
                                    if (Blanktemparr.length > 0) {
                                        Blank.push(Blanktemparr[0].AssetUniqueID);
                                    }
                                }
                            }
                            publishaction(Blank);
                        }
                    });
                } else {
                    for (var i = 0; i < selectedAssetIDs.length; i++) {
                        Blanktemparr = $.grep(selectedAssetFiles, function (e) {
                            return e.Category == 1 && e.AssetUniqueID == selectedAssetIDs[i];
                        });
                        if (Blanktemparr != null) {
                            if (Blanktemparr.length > 0) {
                                Blank.push(Blanktemparr[0].AssetUniqueID);
                            }
                        }
                    }
                    publishaction(Blank);
                }
            } else bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'))
        }

        function publishaction(Blank) {
            if (Blank.length > 0) {
                if (Blank.length == selectedAssetIDs.length) {
                    bootbox.alert($translate.instant('LanguageContents.Res_5761.Caption'));
                    refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                    refreshDeleteableScope(selectedAssetIDs);
                    $scope.DownloadFileObj.DownloadFilesArr = [];
                    $scope.AssetFilesObj.AssetTaskObj = [];
                    $scope.AssetFilesObj.AssetSelection = [];
                    selectedAssetIDs = [];
                    return false;
                } else if (Blank.length > 0) {
                    bootbox.confirm($translate.instant('LanguageContents.Res_5787.Caption'), function (result) {
                        if (result) {
                            selectedAssetIDs = $.grep(selectedAssetIDs, function (e) {
                                return $.inArray(e, Blank) == -1;
                            });
                            if (selectedAssetIDs.length > 0) {
                                var Link = [],
									temparr = [];
                                angular.forEach(selectedAssetIDs, function (item) {
                                    temparr = ($.grep(selectedAssetFiles, function (e) {
                                        return e.LinkUserDetail.length > 0 && e.AssetUniqueID == item;
                                    }));
                                    if (temparr != null) if (temparr.length > 0) Link.push(temparr[0].AssetUniqueID);
                                });
                                if (Link.length == selectedAssetIDs.length) {
                                    var ht_Count = "";
                                    if (Link.length > 0) ht_Count = "Assets";
                                    if (Link.length == 1) ht_Count = "Asset";
                                    bootbox.alert($translate.instant('LanguageContents.Res_5757.Caption') + ht_Count + $translate.instant('LanguageContents.Res_5758.Caption'));
                                    refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                                    refreshDeleteableScope(selectedAssetIDs);
                                    $scope.DownloadFileObj.DownloadFilesArr = [];
                                    $scope.AssetFilesObj.AssetTaskObj = [];
                                    $scope.AssetFilesObj.AssetSelection = [];
                                    selectedAssetIDs = [];
                                    return false;
                                } else if (Link.length > 0) {
                                    bootbox.confirm($translate.instant('LanguageContents.Res_5762.Caption'), function (result) {
                                        if (result) {
                                            selectedAssetIDs = $.grep(selectedAssetIDs, function (e) {
                                                return $.inArray(e, Link) == -1;
                                            });
                                            PublishAssetsservice();
                                        }
                                    });
                                } else if (Link.length == 0) {
                                    PublishAssetsservice();
                                }
                            } else {
                                bootbox.alert($translate.instant('LanguageContents.Res_5752.Caption'));
                                refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                                refreshDeleteableScope(temp);
                                $scope.DownloadFileObj.DownloadFilesArr = [];
                                $scope.AssetFilesObj.AssetTaskObj = [];
                                $scope.AssetFilesObj.AssetSelection = [];
                                selectedAssetIDs = [];
                                return false;
                            }
                        }
                    });
                }
            } else {
                var Link = [],
					temparr = [];
                angular.forEach(selectedAssetIDs, function (item) {
                    temparr = ($.grep(selectedAssetFiles, function (e) {
                        return e.LinkUserDetail.length > 0 && e.AssetUniqueID == item;
                    }));
                    if (temparr != null) if (temparr.length > 0) Link.push(temparr[0].AssetID);
                });
                if (Link.length == selectedAssetIDs.length) {
                    var ht_Count = "";
                    if (Link.length > 0) ht_Count = "Assets";
                    if (Link.length == 1) ht_Count = "Asset";
                    bootbox.alert($translate.instant('LanguageContents.Res_5757.Caption') + ht_Count + $translate.instant('LanguageContents.Res_5758.Caption'));
                    refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                    refreshDeleteableScope(selectedAssetIDs);
                    $scope.DownloadFileObj.DownloadFilesArr = [];
                    $scope.AssetFilesObj.AssetTaskObj = [];
                    $scope.AssetFilesObj.AssetSelection = [];
                    selectedAssetIDs = [];
                    return false;
                } else if (Link.length > 0) {
                    bootbox.confirm($translate.instant('LanguageContents.Res_5762.Caption'), function (result) {
                        if (result) {
                            selectedAssetIDs = $.grep(selectedAssetIDs, function (e) {
                                return $.inArray(e, Link) == -1;
                            });
                            PublishAssetsservice();
                        }
                    });
                } else if (Link.length == 0) {
                    PublishAssetsservice();
                }
            }
        }

        function PublishAssetsservice() {
            $scope.ProgressMsgHeader = "Publishing Assets";
            $scope.ProgressContent = "Publishing";
            $('#loadingDownloadPageModel').modal("show");
            var arr = '';
            arr = selectedAssetIDs;
            $timeout(function () {
                DamNewService.PublishAssets(arr).then(function (result) {
                    if (result.Response == true) {
                        var count = arr.length;
                        var ht_Count = "";
                        if (count > 0) ht_Count = "Assets";
                        if (count == 1) ht_Count = "Asset";
                        NotifySuccess(count + " " + ht_Count + " Published successfully");
                        if ($scope.DamViewName == "Thumbnail") {
                            $scope.$broadcast('loadAssetDetailSection_th', arr);
                        } else if ($scope.DamViewName == "Summary") {
                            $scope.$broadcast('loadAssetDetailSection_th_Summary', arr);
                        } else if ($scope.DamViewName == "List") {
                            $scope.$broadcast('loadAssetDetailSection_th_List', arr);
                        }
                        if ($scope.showallassetview == "showallasset") {
                            $scope.$broadcast('loadShowallAssetDetailSectionfr_th', arr, 1);
                        } else {
                            $scope.$broadcast('UpdatePublishIcon', arr);
                        }
                        if ($('#icon_selectassets').attr('class') != 'icon-check') {
                            if ($scope.selectall == false) {
                                selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                                selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                            } else {
                                selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                            }
                        }
                        refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                        refreshDeleteableScope(selectedAssetIDs);
                        $timeout(function () {
                            selectedAssetIDs = [];
                        }, 200);
                        $scope.DownloadFileObj.DownloadFilesArr = [];
                        $scope.AssetFilesObj.AssetTaskObj = [];
                        $scope.AssetFilesObj.AssetSelection = [];
                        selectedAssetIDs = [];
                        for (var j = 0; j < arr.length; j++) {
                            for (var i = 0; i < $scope.AssetFilesObj.AssetFiles.length; i++) {
                                var temparr = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                                    return e.IsPublish == false && e.AssetUniqueID == arr[j];
                                });
                                if (temparr.length > 0) {
                                    temparr[0].IsPublish = true;
                                }
                            }
                        }
                        $('.loadingDownloadPageModel').modal("hide");
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_1900.Caption'));
                        $('.loadingDownloadPageModel').modal("hide");
                    }
                });
            }, 300);
        }

        function refreshblocks(assetIdArr) { }
        $scope.OpenWorkTask = function (tasktype) {
            if ($scope.DamAssetTaskselectionFiles.AssetFiles.length > 0) {
                if (tasktype == 32 || tasktype == 36) {
                    if ($scope.DamAssetTaskselectionFiles.AssetFiles.length > 1) {
                        bootbox.alert($translate.instant('LanguageContents.Res_4918.Caption'));
                        refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                        return false;
                    }
                }
                var assetid = 0;
                if (tasktype == 32 || tasktype == 36) assetid = $scope.DamAssetTaskselectionFiles.AssetFiles[0].AssetUniqueID;
                assetid = $scope.DamAssetTaskselectionFiles.AssetFiles[0].AssetUniqueID;
                var tempDamAssetSelected = $scope.DamAssetTaskselectionFiles.AssetFiles;
                var tempSelectedAssetDynData = $scope.DamAssetTaskselectionFiles.AssetDynamicData;
                if ($('#icon_selectassets').attr('class') != 'icon-check') {
                    if ($scope.selectall == false) {
                        selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                        selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                    } else {
                        selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                    }
                }
                refreshDownLoadScope(selectedAssetIDs);
                refreshDeleteableScope(selectedAssetIDs);
                $scope.DamAssetTaskselectionFiles.AssetFiles = tempDamAssetSelected;
                $scope.DamAssetTaskselectionFiles.AssetDynamicData = tempSelectedAssetDynData;
                var opentaskpopupdata = {};
                opentaskpopupdata.AssetID = assetid;
                opentaskpopupdata.IsTask = true;
                opentaskpopupdata.IsLock = false;
                opentaskpopupdata.TaskType = tasktype;
                opentaskpopupdata.Fromplace = "Task";
                opentaskpopupdata.Type = "addtaskfromasset";
                var modalInstance = $modal.open({
                    templateUrl: 'views/mui/task/TaskCreation.html',
                    controller: "mui.task.muitaskTaskCreationCtrl",
                    resolve: {
                        params: function () {
                            return {
                                data: opentaskpopupdata
                            };
                        }
                    },
                    scope: $scope,
                    windowClass: 'addTaskPopup popup-widthL',
                    backdrop: "static"
                });
                modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                }, function () { });
            } else bootbox.alert($translate.instant('LanguageContents.Res_4608.Caption'));
        }
        $scope.availableTags = [];
        $scope.TomailAddress = null;
        $scope.mailSubject = "";
        $scope.select2Options = {
            tags: [],
            multiple: true,
            simple_tags: false,
            minimumInputLength: 1,
            formatResult: function (item) {
                return item.text;
            },
            formatSelection: function (item) {
                return item.text;
            }
        }
        $scope.selectmetadata = function (e) {
            var chk = e.target;
            if (chk.checked) $scope.includemdata = true;
            else $scope.includemdata = false;
        }
        $scope.loadlinkassetcreation = function () {
            if ($scope.Dam_Directory.length > 0 || $scope.processingsrcobj.processingplace == "task") {
                $scope.addassetfromcomputer(1);
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4565.Caption'));
            }
        }
        $scope.ReturnImagePth = function (Category, Extension, Status, FileGuid, IsEdit) {
            if (Category == 0) {
                if (Extension != null) {
                    if (Extension.toLowerCase() == '.jpg' || Extension.toLowerCase() == '.jpeg' || Extension.toLowerCase() == '.png' || Extension.toLowerCase() == '.psd' || Extension.toLowerCase() == '.bmp' || Extension.toLowerCase() == '.tif' || Extension.toLowerCase() == '.tiff' || Extension.toLowerCase() == '.gif' || asset.Extension.toLowerCase() == '.emf' || asset.Extension.toLowerCase() == '.eps' || asset.Extension.toLowerCase() == '.pdf') {
                        if (Status == 2) {
                            if (IsEdit == false) return TenantFilePath + 'DAMFiles/Preview/Small_' + FileGuid + '.jpg';
                            else return TenantFilePath + 'DAMFiles/Preview/Big_' + FileGuid + '.jpg';
                        } else if (Status == 1 || Status == 0) {
                            return "assets/img/loading.gif";
                        } else if (Status == 3) {
                            return TenantFilePath + "DAMFiles/StaticPreview_small/" + Extension.substring(1, Extension.length).toUpperCase() + ".jpg";
                        }
                    } else {
                        return TenantFilePath + "DAMFiles/StaticPreview_small/" + Extension.substring(1, Extension.length).toUpperCase() + ".jpg";
                    }
                } else {
                    return TenantFilePath + "DAMFiles/Original/NoPreview1.jpg";
                }
            } else if (Category == 1) {
                return TenantFilePath + "DAMFiles/StaticPreview_small/BLANK.jpg"
            } else if (Category == 2) {
                return TenantFilePath + "DAMFiles/StaticPreview_small/LINK.jpg";
            }
        }
        $scope.SelectAllAssetsObj = {
            checkselectall: false
        };
        $scope.SelectAllAssetsObj.checkselectall = false;
        $scope.selectall = false;
        $scope.$on('checkselectall', function (event, flag) {
            $scope.SelectAllAssetsObj.checkselectall = flag;
            $("#icon_selectassets").removeClass("icon-check");
            $("#icon_selectassets").addClass("icon-unchecked");
            $("#damlistviewselectall").addClass("icon-unchecked");
            $("#damlistviewselectall").removeClass("icon-check");
            $("#showalldamlistviewselectall").addClass("icon-unchecked");
            $("#showalldamlistviewselectall").removeClass("icon-check");
            $scope.selectall = true;
        });
        $scope.$on('clickcheckselectall', function (event, frommenu) {
            $scope.selectallassets(frommenu);
        });
        $scope.$on('clickDselectall', function () {
            $scope.Dselectallassets();
        });
        $scope.Dselectallassets = function () {
            if ($scope.SelectAllAssetsObj.checkselectall == false) {
                $scope.SelectAllAssetsObj.checkselectall = true;
                $scope.selectallassets(0);
            } else {
                $scope.SelectAllAssetsObj.checkselectall = true;
                $scope.selectallassets(0);
            }
        }
        $scope.selectallassets = function (frommenu) {
            if (frommenu == 0) {
                $scope.SelectAllAssetsObj.checkselectall = ($scope.SelectAllAssetsObj.checkselectall) ? false : true;
            } else {
                $scope.SelectAllAssetsObj.checkselectall = true;
            }
            if ($scope.SelectAllAssetsObj.checkselectall) {
                $("#icon_selectassets").removeClass("icon-unchecked");
                $("#icon_selectassets").addClass("icon-check");
                $("#damlistviewselectall").removeClass("icon-unchecked");
                $("#damlistviewselectall").addClass("icon-check");
                $("#showalldamlistviewselectall").removeClass("icon-unchecked");
                $("#showalldamlistviewselectall").addClass("icon-check");
                $("#listselectheader").addClass("checked");
                $("#listselectheader").removeClass("check");
                $("#showalllistselectheader").addClass("checked");
                $("#showalllistselectheader").removeClass("check");
            } else {
                $("#icon_selectassets").removeClass("icon-check");
                $("#icon_selectassets").addClass("icon-unchecked");
                $("#damlistviewselectall").addClass("icon-unchecked");
                $("#damlistviewselectall").removeClass("icon-check");
                $("#showalldamlistviewselectall").addClass("icon-unchecked");
                $("#showalldamlistviewselectall").removeClass("icon-check");
                $("#listselectheader").removeClass("checked");
                $("#listselectheader").addClass("check");
                $("#showalllistselectheader").removeClass("checked");
                $("#showalllistselectheader").addClass("check");
            }
            if ($scope.AssetFilesObj.AssetFiles.length > 0) {
                if ($('#icon_selectassets').attr('class') == 'icon-check') {
                    var dynData = [];
                    var Assets = [];
                    var b;
                    b = tree.get_selected_branch();
                    selectedAssetFiles = [];
                    selectedAssetDynData = [];
                    selectedAssetIDs = [];
                    $scope.ProgressMsgHeader = "Performing select all action";
                    $scope.ProgressContent = "Processing..";
                    if ($scope.showallassetview == "") {
                        var ViewAll = false;
                    } else if ($scope.showallassetview == "showallasset") {
                        var ViewAll = true;
                    } else {
                        var ViewAll = false;
                    }
                    var selectAsset = {};
                    selectAsset.FolderID = b == null ? $scope.Folderselect.ID : b.id;
                    selectAsset.EntityID = $scope.processingsrcobj.processingid;
                    selectAsset.View = $scope.MulitipleFilterStatus;
                    selectAsset.Orderby = 4;
                    selectAsset.ViewAll = ViewAll;
                    DamNewService.GetAllAssetsBasedOnFolderandEntity(selectAsset).then(function (result) {
                        if (result.Response != null) {
                            if (result.Response[0].AssetFiles.length > 200) $('#loadingDownloadPageModel').modal("show");
                            $scope.DamAssetTaskselectionFiles.AssetFiles = [];
                            $scope.DamAssetTaskselectionFiles.AssetDynamicData = [];
                            selectedAssetFiles = [];
                            selectedAssetDynData = [];
                            selectedAssetIDs = [];
                            var AllAssets = result.Response[0].AssetFiles;
                            var AllAssetsDynamicData = result.Response[0].AssetDynData;
                            selectedAssetFiles = AllAssets;
                            selectedAssetDynData = AllAssetsDynamicData;
                            var temparr = [],
								temp;
                            for (var i = 0; i < selectedAssetFiles.length; i++) {
                                temparr[i] = selectedAssetFiles[i].AssetUniqueID;
                            }
                            selectedAssetIDs = temparr;
                            $scope.AssetFilesObj.AssetSelection = selectedAssetIDs;
                            if ($scope.DamViewName == "Thumbnail") {
                                for (var i = 0, asset; asset = selectedAssetFiles[i++];) {
                                    $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = ($scope.SelectAllAssetsObj.checkselectall == true) ? "checkbox checked" : "checkbox";
                                    $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = $scope.SelectAllAssetsObj.checkselectall;
                                    if ($scope.SelectAllAssetsObj.checkselectall == true) {
                                        $('#chkBox_' + asset.AssetUniqueID + '').attr('checked', 'checked');
                                        $('#chkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox');
                                        $('#chkBoxIcon_' + asset.AssetUniqueID + '').addClass('checked');
                                    } else {
                                        $('#chkBox_' + asset.AssetUniqueID + '').removeAttr('checked');
                                        $('#chkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox checked');
                                        $('#chkBoxIcon_' + asset.AssetUniqueID + '').addClass('checkbox');
                                    }
                                    $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = ($scope.SelectAllAssetsObj.checkselectall == true) ? "th-Selection selected" : "th-Selection";
                                    updatescopesforactions(asset.AssetUniqueID, asset.ActiveFileID, $scope.SelectAllAssetsObj.checkselectall);
                                }
                                $('.loadingDownloadPageModel').modal("hide");
                            } else if ($scope.DamViewName == "Summary") {
                                for (var i = 0, asset; asset = selectedAssetFiles[i++];) {
                                    $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = ($scope.SelectAllAssetsObj.checkselectall == true) ? "checkbox checked" : "checkbox";
                                    $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = $scope.SelectAllAssetsObj.checkselectall;
                                    if ($scope.SelectAllAssetsObj.checkselectall == true) {
                                        $('#sumchkBoxIcon_' + asset.AssetUniqueID + '').attr('checked', 'checked');
                                        $('#chkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox');
                                        $('#chkBoxIcon_' + asset.AssetUniqueID + '').addClass('checked');
                                    } else {
                                        $('#sumchkBoxIcon_' + asset.AssetUniqueID + '').removeAttr('checked');
                                        $('#chkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox checked');
                                        $('#chkBoxIcon_' + asset.AssetUniqueID + '').addClass('checkbox');
                                    }
                                    $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = ($scope.SelectAllAssetsObj.checkselectall == true) ? "th-sum-Selection selected" : "th-sum-Selection";
                                    updatescopesforactions(asset.AssetUniqueID, asset.ActiveFileID, $scope.SelectAllAssetsObj.checkselectall);
                                }
                                $('.loadingDownloadPageModel').modal("hide");
                            } else if ($scope.DamViewName == "List") {
                                for (var i = 0, asset; asset = selectedAssetFiles[i++];) {
                                    $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = ($scope.SelectAllAssetsObj.checkselectall == true) ? "checkbox checked" : "checkbox";
                                    $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = $scope.SelectAllAssetsObj.checkselectall;
                                    if ($scope.SelectAllAssetsObj.checkselectall == true) {
                                        $('#chkBox_' + asset.AssetUniqueID + '').attr('checked', 'checked');
                                        $('#chkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox');
                                        $('#chkBoxIcon_' + asset.AssetUniqueID + '').addClass('checked');
                                    } else {
                                        $('#chkBox_' + asset.AssetUniqueID + '').removeAttr('checked');
                                        $('#chkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox checked');
                                        $('#chkBoxIcon_' + asset.AssetUniqueID + '').addClass('checkbox');
                                    }
                                    $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = ($scope.SelectAllAssetsObj.checkselectall == true) ? "li-Selection selected" : "li-Selection";
                                    updatescopesforactions(asset.AssetUniqueID, asset.ActiveFileID, $scope.SelectAllAssetsObj.checkselectall);
                                }
                            }
                            $('.loadingDownloadPageModel').modal("hide");
                        } else {
                            $('.loadingDownloadPageModel').modal("hide");
                        }
                    });
                } else {
                    if (selectedAssetFiles.length == 0) selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                    if ($scope.DamViewName == "Thumbnail") {
                        for (var i = 0, asset; asset = selectedAssetFiles[i++];) {
                            $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = ($scope.SelectAllAssetsObj.checkselectall == true) ? "checkbox checked" : "checkbox";
                            $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = $scope.SelectAllAssetsObj.checkselectall;
                            if ($scope.SelectAllAssetsObj.checkselectall == true) {
                                $('#chkBox_' + asset.AssetUniqueID + '').attr('checked', 'checked');
                                $('#chkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox');
                                $('#chkBoxIcon_' + asset.AssetUniqueID + '').addClass('checked');
                            } else {
                                $('#chkBox_' + asset.AssetUniqueID + '').removeAttr('checked');
                                $('#chkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox checked');
                                $('#chkBoxIcon_' + asset.AssetUniqueID + '').addClass('checkbox');
                            }
                            $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = ($scope.SelectAllAssetsObj.checkselectall == true) ? "th-Selection selected" : "th-Selection";
                        }
                    } else if ($scope.DamViewName == "Summary") {
                        for (var i = 0, asset; asset = selectedAssetFiles[i++];) {
                            $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = ($scope.SelectAllAssetsObj.checkselectall == true) ? "checkbox checked" : "checkbox";
                            $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = $scope.SelectAllAssetsObj.checkselectall;
                            if ($scope.SelectAllAssetsObj.checkselectall == true) {
                                $('#sumchkBoxIcon_' + asset.AssetUniqueID + '').attr('checked', 'checked');
                                $('#chkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox');
                                $('#chkBoxIcon_' + asset.AssetUniqueID + '').addClass('checked');
                            } else {
                                $('#sumchkBoxIcon_' + asset.AssetUniqueID + '').removeAttr('checked');
                                $('#chkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox checked');
                                $('#chkBoxIcon_' + asset.AssetUniqueID + '').addClass('checkbox');
                            }
                            $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = ($scope.SelectAllAssetsObj.checkselectall == true) ? "th-sum-Selection selected" : "th-sum-Selection";
                        }
                    } else if ($scope.DamViewName == "List") {
                        for (var i = 0, asset; asset = selectedAssetFiles[i++];) {
                            $scope.AssetListSelection.AssetChkClass["asset_" + asset.AssetUniqueID + ""] = ($scope.SelectAllAssetsObj.checkselectall == true) ? "checkbox checked" : "checkbox";
                            $scope.AssetListSelection.AssetSelection["asset_'" + asset.AssetUniqueID + ""] = $scope.SelectAllAssetsObj.checkselectall;
                            if ($scope.SelectAllAssetsObj.checkselectall == true) {
                                $('#chkBox_' + asset.AssetUniqueID + '').attr('checked', 'checked');
                                $('#chkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox');
                                $('#chkBoxIcon_' + asset.AssetUniqueID + '').addClass('checked');
                            } else {
                                $('#chkBox_' + asset.AssetUniqueID + '').removeAttr('checked');
                                $('#chkBoxIcon_' + asset.AssetUniqueID + '').removeClass('checkbox checked');
                                $('#chkBoxIcon_' + asset.AssetUniqueID + '').addClass('checkbox');
                            }
                            $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + asset.AssetUniqueID + ""] = ($scope.SelectAllAssetsObj.checkselectall == true) ? "li-Selection selected" : "li-Selection";
                        }
                    }
                    for (var j = 0; j < selectedAssetFiles.length; j++) {
                        updatescopesforactions(selectedAssetFiles[j].AssetUniqueID, selectedAssetFiles[j].ActiveFileID, $scope.SelectAllAssetsObj.checkselectall);
                    }
                    selectedAssetIDs = [];
                    selectedAssetFiles = [];
                    $scope.AssetFilesObj.AssetSelection = [];
                }
            }
        }

        function updatescopesforactions(assetid, activefileid, ischecked) {
            if ($('#icon_selectassets').attr('class') != 'icon-check') {
                if ($scope.selectall == false) {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                    selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                    selectedAssetDynData = $scope.AssetFilesObj.AssetDynamicData;
                } else {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                }
            }
            var res = [];
            res = $.grep(selectedAssetFiles, function (e) {
                return e.AssetUniqueID == assetid;
            });
            if (res.length > 0) {
                DownloadAssets(ischecked, assetid, res[0].FileName, res[0].Extension, res[0].FileGuid, res[0].Category);
            }
            if (ischecked) {
                var remainRecord = [];
                remainRecord = $.grep(selectedAssetIDs, function (e) {
                    return e == assetid;
                });
                if (remainRecord.length == 0) {
                    selectedAssetIDs.push(assetid);
                }
                if (res[0].Category == 0) {
                    AddAttachmentsToTask(assetid, activefileid, true);
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep(selectedAssetIDs, function (e) {
                    return e == assetid;
                });
                if (remainRecord.length > 0) {
                    selectedAssetIDs.splice($.inArray(assetid, selectedAssetIDs), 1);
                }
                if (res[0].Category == 0) {
                    AddAttachmentsToTask(assetid, activefileid, false);
                }
            }
        }

        function DownloadAssets(ischecked, assetid, name, ext, guid, category) {
            if (ischecked) {
                var remainRecord = [];
                remainRecord = $.grep($scope.DownloadFileObj.DownloadFilesArr, function (e) {
                    return e.AssetId == assetid;
                });
                if (remainRecord.length == 0) {
                    $scope.DownloadFileObj.DownloadFilesArr.push({
                        AssetId: assetid,
                        FileName: name,
                        Fileguid: guid,
                        Extension: ext,
                        Category: category
                    });
                }
            } else {
                var remainRecord = [];
                remainRecord = $.grep($scope.DownloadFileObj.DownloadFilesArr, function (e) {
                    return e.AssetId == assetid;
                });
                if (remainRecord.length > 0) {
                    $scope.DownloadFileObj.DownloadFilesArr.splice($.inArray(remainRecord[0], $scope.DownloadFileObj.DownloadFilesArr), 1);
                }
            }
        }

        function AddAttachmentsToTask(assetid, activefileid, ischecked) {
            if ($('#icon_selectassets').attr('class') != 'icon-check') {
                if ($scope.selectall == false) {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                    selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                    selectedAssetDynData = $scope.AssetFilesObj.AssetDynamicData;
                } else {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                }
            }
            if (ischecked == true) {
                var response = $.grep(selectedAssetFiles, function (e) {
                    return e.ActiveFileID == activefileid
                })[0];
                if (response != null) {
                    data = $.grep(selectedAssetDynData, function (e) {
                        return e.ID == assetid;
                    });
                    var flagattTask = $.grep($scope.DamAssetTaskselectionFiles.AssetFiles, function (e) {
                        return e.FileGuid == response.FileGuid
                    })[0];
                    if (flagattTask == null) {
                        $scope.DamAssetTaskselectionFiles.AssetFiles.push(response);
                        if (data[0] != undefined) {
                            $scope.DamAssetTaskselectionFiles.AssetDynamicData.push(data[0]);
                        }
                    }
                }
            } else {
                var response = $.grep(selectedAssetFiles, function (e) {
                    return e.ActiveFileID == activefileid
                })[0];
                var flagattTask = $.grep($scope.DamAssetTaskselectionFiles.AssetFiles, function (e) {
                    return e.FileGuid == response.FileGuid
                })[0];
                $scope.DamAssetTaskselectionFiles.AssetFiles.splice($.inArray(flagattTask, $scope.DamAssetTaskselectionFiles.AssetFiles), 1);
                if ($scope.DamAssetTaskselectionFiles.AssetDynamicData != undefined) {
                    var flagdata = $.grep($scope.DamAssetTaskselectionFiles.AssetDynamicData, function (e) {
                        return e.ID == assetid
                    })[0];
                    $scope.DamAssetTaskselectionFiles.AssetDynamicData.splice($.inArray(flagdata, $scope.DamAssetTaskselectionFiles.AssetDynamicData), 1);
                }
            }
        }
        $scope.$on('RefreshAssetSelectionforTask', function (event, tasklistid) {
            refreshTaskAssetSelection();
            $scope.$emit('ReloadEntitytaskLibrary', tasklistid);
        });

        function refreshTaskAssetSelection() {
            for (var i = 0, item; item = $scope.AssetFilesObj.AssetTaskObj[i++];) {
                if ($scope.DamViewName == "Thumbnail") $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + item] = "th-Selection";
                else if ($scope.DamViewName == "Summary") $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + item] = "th-sum-Selection";
                else if ($scope.DamViewName == "List") $scope.AssetListSelectionClass.AssetSelectionClass["asset_" + item] = "li-Selection";
                $scope.AssetListSelection.AssetSelection["asset_" + item] = false;
                $scope.AssetListSelection.AssetChkClass["asset_" + item + ""] = "checkbox";
            }
            $scope.DownloadFileObj.DownloadFilesArr = [];
            $scope.AssetFilesObj.AssetTaskObj = [];
            $scope.AssetFilesObj.AssetSelection = [];
        }
        $scope.$on('DamAssetAction', function (event, action, istask, tasktype) {
            var res = [];
            res = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                return e.AssetUniqueID == $scope.AssetFilesObj.selectedAssetId;
            });
            switch (action) {
                case "DuplicateAsset":
                    DuplicateSelectedAsset([$scope.AssetFilesObj.selectedAssetId]);
                    $scope.AssetFilesObj.selectedAssetId = 0;
                    break;
                case "AddtoLightBox":
                    var remainRecord = [];
                    remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                        return e == $scope.AssetFilesObj.selectedAssetId;
                    });
                    if (remainRecord.length == 0) {
                        $scope.AssetFilesObj.AssetSelection.push($scope.AssetFilesObj.selectedAssetId);
                    }
                    $scope.Addtolightbox();
                    $scope.AssetFilesObj.selectedAssetId = 0;
                    break;
                case "ViewLightBox":
                    $scope.Viewlightbox();
                    $scope.AssetFilesObj.selectedAssetId = 0;
                    break;
                case "Task":
                case "ApprovalTask":
                    if (res[0].FileGuid != null) {
                        var remainRecord = [];
                        remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                            return e == $scope.AssetFilesObj.selectedAssetId;
                        });
                        if (remainRecord.length == 0) {
                            $scope.AssetFilesObj.AssetSelection.push($scope.AssetFilesObj.selectedAssetId);
                        }
                        AddAttachmentsToTask($scope.AssetFilesObj.selectedAssetId, res[0].ActiveFileID, true);
                    }
                    if ($scope.DamAssetTaskselectionFiles.AssetFiles.length == 0) {
                        bootbox.alert($translate.instant('LanguageContents.Res_4917.Caption'));
                    } else {
                        $scope.OpenWorkTask(tasktype);
                    }
                    $scope.AssetFilesObj.selectedAssetId = 0;
                    break;
                case "PublishDAM":
                    var remainRecord = [];
                    remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                        return e == $scope.AssetFilesObj.selectedAssetId;
                    });
                    if (remainRecord.length == 0) {
                        $scope.AssetFilesObj.AssetSelection.push($scope.AssetFilesObj.selectedAssetId);
                    }
                    $scope.PublishAssets();
                    $scope.AssetFilesObj.selectedAssetId = 0;
                    break;
                case "Delete":
                    deleteAsset([$scope.AssetFilesObj.selectedAssetId]);
                    $scope.AssetFilesObj.selectedAssetId = 0;
                    break;
                case "Download":
                    var remainRecord = [];
                    remainRecord = $.grep($scope.DownloadFileObj.DownloadFilesArr, function (e) {
                        return e.AssetId == res[0].AssetID;
                    });
                    if (remainRecord.length == 0) {
                        $scope.DownloadFileObj.DownloadFilesArr.push({
                            AssetId: res[0].AssetID,
                            FileName: res[0].FileName,
                            Fileguid: res[0].FileGuid,
                            Extension: res[0].Extension,
                            Category: res[0].Category
                        });
                    }
                    $scope.download();
                    $scope.AssetFilesObj.selectedAssetId = 0;
                    break;
                case "Cut":
                    var remainRecord = [];
                    remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                        return e == $scope.AssetFilesObj.selectedAssetId;
                    });
                    if (remainRecord.length == 0) {
                        $scope.AssetFilesObj.AssetSelection.push($scope.AssetFilesObj.selectedAssetId);
                    }
                    $scope.PerformAssetOperation(1);
                    $scope.AssetFilesObj.selectedAssetId = 0;
                    break;
                case "Copy":
                    var remainRecord = [];
                    remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                        return e == $scope.AssetFilesObj.selectedAssetId;
                    });
                    if (remainRecord.length == 0) {
                        $scope.AssetFilesObj.AssetSelection.push($scope.AssetFilesObj.selectedAssetId);
                    }
                    $scope.PerformAssetOperation(2);
                    $scope.AssetFilesObj.selectedAssetId = 0;
                    break;
                case "DownloadAll":
                    var remainRecord = [];
                    remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                        return e == $scope.AssetFilesObj.selectedAssetId;
                    });
                    if (remainRecord.length == 0) {
                        $scope.AssetFilesObj.AssetSelection.push($scope.AssetFilesObj.selectedAssetId);
                    }
                    $scope.DownloadWithMetadata(istask);
                    $scope.AssetFilesObj.selectedAssetId = 0;
                    break;
                case "UnpublishAssets":
                    var remainRecord = [];
                    remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                        return e == $scope.AssetFilesObj.selectedAssetId;
                    });
                    if (remainRecord.length == 0) {
                        $scope.AssetFilesObj.AssetSelection.push($scope.AssetFilesObj.selectedAssetId);
                    }
                    $scope.UnPublishAssets();
                    break;
                case "Reformat":
                    var remainRecord = [];
                    remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                        return e == $scope.AssetFilesObj.selectedAssetId;
                    });
                    if (remainRecord.length == 0) {
                        $scope.AssetFilesObj.AssetSelection.push($scope.AssetFilesObj.selectedAssetId);
                    }
                    $scope.Reformatproces();
                    $scope.AssetFilesObj.selectedAssetId = 0;
                    break;
                default:
                    bootbox.alert($translate.instant('LanguageContents.Res_4639.Caption'));
                    $scope.AssetFilesObj.selectedAssetId = 0;
                    break;
            }
        });

        function PerformAssetLevelAction(action, assetid) {
            if (action == 1 || action == 2) {
                $scope.damclipboard.actioncode = action;
                var remainRecord = [];
                remainRecord = $.grep($scope.damclipboard.assetselection, function (e) {
                    return e.AssetId == assetid;
                });
                if (remainRecord.length == 0) {
                    $scope.damclipboard.assetselection.push({
                        "AssetId": assetid
                    });
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }
        }

        function DuplicateSelectedAsset(arr) {
            var remainRecord = [];
            remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                return e == arr[0];
            });
            if (remainRecord.length == 0) {
                $scope.AssetFilesObj.AssetSelection.push(arr[0]);
            }
            $scope.DuplicateAssets();
        }

        function PublishAsset(asset) {
            if (asset.length > 0) {
                var arr = asset;
                DamNewService.PublishAssets(arr).then(function (result) {
                    if (result.Response == true) {
                        NotifySuccess($translate.instant('LanguageContents.Res_4782.Caption'));
                        if ($scope.showallassetview == "showallasset") {
                            $scope.$broadcast('ShowallUpdatePublishIcon', arr);
                        } else {
                            $scope.$broadcast('UpdatePublishIcon', arr);
                        }
                    } else {
                        NotifyError($translate.instant('LanguageContents.Res_1900.Caption'));
                    }
                });
            } else bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'))
        }

        function opentaskpopup(attachment, file, assetid, tasktype) {
            if (attachment.length > 0) {
                if (tasktype == 32) {
                    if ($scope.AttachemtstoTask.length > 1) {
                        bootbox.alert($translate.instant('LanguageContents.Res_4918.Caption'));
                        return false;
                    }
                }
                $scope.AttachmentFilename = attachment;
                $scope.FileList = file;
                var assetid = 0;
                if (tasktype == 32) assetid = assetid;
                var remainRecord = [];
                remainRecord = $.grep($scope.DownloadFileObj.DownloadFilesArr, function (e) {
                    return e.AssetId == assetid;
                });
                refreshDownLoadScope(remainRecord);
                $scope.$broadcast('Cleasetaskpopup', "Dam", tasktype, 0, assetid);
            } else bootbox.alert($translate.instant('LanguageContents.Res_4608.Caption'));
        }

        function AddAssettoLightbox(assetid) {
            var remainRecord = [];
            remainRecord = $.grep($scope.damlightbox.lightboxselection, function (e) {
                return e.AssetId == assetid;
            });
            if (remainRecord.length == 0) {
                var asset = [],
					data = [];
                asset = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                    return e.AssetUniqueID == assetid;
                });
                data = $.grep($scope.AssetFilesObj.AssetDynamicData, function (e) {
                    return e.ID == assetid;
                });
                if (asset[0].FileGuid != null) if (asset.length > 0) $scope.damlightbox.lightboxselection.push({
                    "EntityID": $scope.processingsrcobj.processingid,
                    "ShortDesc": asset[0].ShortDescription,
                    "ColorCode": asset[0].ColorCode,
                    "AssetId": assetid,
                    "AssetName": asset[0].Name,
                    "Description": asset[0].Description != null ? asset[0].Description : "-",
                    "Guid": asset[0].FileGuid,
                    "Ext": asset[0].Extension,
                    "AssetStatus": asset[0].Status,
                    "FileSize": parseSize(asset[0].Size),
                    "DynamicData": data,
                    "MimeType": asset[0].MimeType,
                    "Extension": asset[0].Extension.substring(1, asset[0].Extension.length).toLowerCase(),
                    "Category": asset[0].Category,
                    "Iscropped": false
                });
            }
            if ($scope.damlightbox.lightboxselection.length > 0) {
                var count = $scope.AssetFilesObj.AssetSelection.length;
                var ht_Count = "";
                if (count > 0) ht_Count = "Assets";
                if (count == 1) ht_Count = "Asset";
                NotifySuccess(count + " " + ht_Count + " " + $translate.instant('LanguageContents.Res_4066.Caption'));
            }
        }

        function deleteAsset(arr) {
            $scope.ProgressMsgHeader = "Downloading Assets";
            var remainRecord = [];
            remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                return e == arr[0];
            });
            if (remainRecord.length == 0) {
                $scope.AssetFilesObj.AssetSelection.push(arr[0]);
            }
            $scope.deleteAssets();
        }

        function downloadSingleAsset(asset) {
            var arr = asset;
            var remainRecord = [];
            remainRecord.push({
                AssetId: arr[0].AssetID,
                FileName: arr[0].FileName,
                Fileguid: arr[0].FileGuid,
                Extension: arr[0].Extension,
                Category: arr[0].Category
            });
            $timeout(function () {
                var fileid = arr[0].FileGuid,
					extn = arr[0].Extension;
                var filename = arr[0].FileName + extn;
                blockUIForDownload();
                location.href = 'DAMDownload.aspx?FileID=' + arr[0].FileGuid + '&FileFriendlyName=' + arr[0].FileName + '&Ext=' + arr[0].Extension + '&token=' + $('#download_token_value_id').val();
                refreshDownLoadScope(remainRecord);
            }, 100);
        }
        $scope.DAMStatusFilter = [];
        $scope.FilterbyStatusChange = function () {
            var d = $scope.DAMStatusFilter;
            var branch;
            branch = tree.get_selected_branch();
            var res = $.grep($scope.TaskStatusObj, function (e) {
                return e.ID == $scope.MulitipleFilterStatus;
            });
            if (res[0].ID == 1) {
                $timeout(function () {
                    $scope.$broadcast('LoadThumbnailAssetView', branch.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                }, 200);
            } else if (res[0].ID == 2) {
                $timeout(function () {
                    $scope.$broadcast('LoadSummaryAssetView', branch.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                }, 200);
            } else if (res[0].ID == 3) {
                $timeout(function () {
                    $scope.$broadcast('LoadListAssetView', branch.id, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                }, 200);
            }
        }
        $scope.showOptimakerTemplates = function () {
            if ($scope.Dam_Directory.length > 0 || $scope.processingsrcobj.processingplace == "task") {
                $timeout(function () {
                    var modalInstance = $modal.open({
                        templateUrl: 'views/mui/DAM/damoptimakertemplate.html',
                        controller: "mui.DAM.damoptimakertemplateCtrl",
                        resolve: {
                            params: function () {
                                return {
                                    id: 1
                                };
                            }
                        },
                        scope: $scope,
                        windowClass: 'optimakerTemplatePopup popup-widthL',
                        backdrop: "static"
                    });
                    modalInstance.result.then(function (selectedItem) {
                        $scope.selected = selectedItem;
                    }, function () { });
                }, 100);
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4565.Caption'));
            }
        }
        $scope.AddFilesFromDAM = function () {
            if ($scope.Dam_Directory.length > 0) {
                var branch;
                var Iscmspath = $location.path().remove('/detail/section/' + $scope.processingsrcobj.processingid + '/attachment');
                if (Iscmspath == '/mui/cms') {
                    $scope.IsfromassetlibrarytoCMS.Isfromassetlibrary = true;
                } else $scope.IsfromassetlibrarytoCMS.Isfromassetlibrary = false;
                branch = tree.get_selected_branch();
                $scope.MediaBankSettings.Redirectfromdam = true;
                $scope.MediaBankSettings.FolderId = $scope.selectedTree.id;
                $scope.MediaBankSettings.EntityId = $scope.processingsrcobj.processingid;
                $scope.MediaBankSettings.ViewType = $scope.MulitipleFilterStatus;
                $scope.MediaBankSettings.directfromMediaBank = false;
                $scope.MediaBankSettings.ProductionTypeID = $scope.EntityTypeID;
                $scope.MediaBankSettings.FolderName = (branch == undefined || branch == null ? $scope.folderpath[0].caption : branch.Caption);
                $location.path("/mui/mediabank");
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4565.Caption'));
            }
        }
        $scope.LoadAssets = function (folderid, foldername) {
            DamNewService.GetBreadCrumFolderPath($scope.processingsrcobj.processingid, folderid).then(function (data) {
                tree.breadcrum_selection_branch(folderid);
                $scope.folderpath = [];
                $scope.temppath = [];
                $scope.temppath = data.Response;
                if ($scope.temppath.length > 0) {
                    for (var i = 0; i < $scope.temppath.length; i++) {
                        $scope.folderpath.push($scope.temppath[i]);
                    }
                    $scope.initialTreeSelect = foldername;
                }
            });
            var res = $.grep($scope.TaskStatusObj, function (e) {
                return e.ID == $scope.MulitipleFilterStatus;
            });
            if (res[0].ID == 1) {
                if ($scope.callbackid > 0) $timeout(function () {
                    $scope.$broadcast('LoadThumbnailAssetView', folderid, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                }, 200);
            } else if (res[0].ID == 2) {
                if ($scope.callbackid > 0) $timeout(function () {
                    $scope.$broadcast('LoadSummaryAssetView', folderid, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                }, 200);
            } else if (res[0].ID == 3) {
                if ($scope.callbackid > 0) $timeout(function () {
                    $scope.$broadcast('LoadListAssetView', folderid, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                }, 200);
            }
        }
        $scope.Showallassets = function () {
            if ($scope.showhideall == $translate.instant('LanguageContents.Res_4939.Caption')) {
                $scope.showhideall = $translate.instant('LanguageContents.Res_4940.Caption');
                $('#assetview').css('display', 'none');
                $scope.showallassetview = "showallasset";
                $scope.IsShowallAssets.Fromshowallassets = 1;
                $scope.AssetFilesObj.HideShowllMenus = false;
                $scope.AssetFilesObj.HideAddAsset = false;
                $scope.AssetFilesObj.HideAddFolder = false;
                $scope.subview = "";
                if ($scope.DamViewName == "Thumbnail") {
                    $scope.MulitipleFilterStatus = 1;
                    $scope.IsShowallAssets.DamViewName = 1;
                } else if ($scope.DamViewName == "Summary") {
                    $scope.MulitipleFilterStatus = 2;
                    $scope.IsShowallAssets.DamViewName = 2;
                } else if ($scope.DamViewName == "List") {
                    $scope.MulitipleFilterStatus = 3;
                    $scope.IsShowallAssets.DamViewName = 3;
                }
            } else {
                $scope.showhideall = $translate.instant('LanguageContents.Res_4939.Caption');
                $scope.IsShowallAssets.Fromshowallassets = 0;
                $scope.AssetFilesObj.HideShowllMenus = true;
                $scope.AssetFilesObj.HideAddAsset = true;
                $scope.AssetFilesObj.HideAddFolder = true;
                $scope.showallassetview = "";
                $scope.AssetFilesObj.PageNo = 1;
                if ($scope.SelectAllAssetsObj.checkselectall == true) {
                    $scope.selectallassets(1);
                }
                if ($scope.DamViewName == "Thumbnail") {
                    $scope.subview = "Thumbnail";
                    $scope.MulitipleFilterStatus = 1;
                } else if ($scope.DamViewName == "Summary") {
                    $scope.subview = "Summary";
                    $scope.MulitipleFilterStatus = 2;
                } else if ($scope.DamViewName == "List") {
                    $scope.subview = "List";
                    $scope.MulitipleFilterStatus = 3;
                }
                $scope.FilterbyStatusChange();
            }
        }
        var jcrop_api;
        $scope.userimgWidth = 0;
        $scope.userimgHeight = 0;
        $scope.userimgX = 0;
        $scope.userimgY = 0;
        $scope.Cropedimgassetid = 0;
        $scope.loadcroppopup = function (fileguid) {
            $scope.Cropedimgassetid = fileguid.AssetId
            var path = fileguid.Guid;
            $('#UserProfilePic100').modal('show');
            $('#UserProfilePic100 #userpic100').attr('style', '');
            $('#UserProfilePic100 #userpic100').removeAttr('src').css('visibility', 'hidden');
            $('#UserProfilePic100 #userpic100').attr('src', TenantFilePath + 'DAMFiles/Original/' + fileguid.Guid + '.' + fileguid.Extension);
            $('#UserProfilePic100 #userpic100').css('visibility', 'visible');
            $timeout(function () {
                var w = document.getElementById('userpic100').naturalWidth;
                var h = document.getElementById('userpic100').naturalHeight;
                $('#UserProfilePic100 #userpic100').attr('data-width', w).attr('data-height', h);
                $scope.AssigneNewImage(w, h, path, fileguid.Extension);
            }, 400);
        }
        $scope.AssigneNewImage = function (w, h, path, extn) {
            $('#UserProfilePic100 #userpic100').attr('src', TenantFilePath + 'DAMFiles/Original/' + path + '.' + extn);
            $('#UserProfilePic100 #userpic100').attr('data-width', w).attr('data-height', h);
            if (w >= h) {
                $('#UserProfilePic100 #userpic100').css('width', '704px');
                var imgheight = Math.round((h / w) * 704);
                var margine = Math.round((528 - imgheight) / 2);
                if (margine < 0) {
                    margine = 0;
                }
                $('#UserProfilePic100 #userpic100').parent().css('margin-top', margine + 'px');
            } else {
                $('#UserProfilePic100 #userpic100').css('height', '528px');
                $('#UserProfilePic100 #userpic100').parent().css('margin-top', '0px');
            }
            $('#UserProfilePic100 #userpic100').css('visibility', 'visible');
            initJcrop();
        };

        function initJcrop() {
            if ($('#UserProfilePic100 .jcrop-holder').length > 0) {
                jcrop_api.destroy();
            }
            $('#userpic100').Jcrop({
                onSelect: showPreview,
                onRelease: releaseCheck
            }, function () {
                jcrop_api = this;
                jcrop_api.focus();
                var w = $('#UserProfilePic100 #userpic100').width();
                var h = $('#UserProfilePic100 #userpic100').height();
                $scope.OrgimgWidth = w;
                $scope.OrgimgHeight = h;
                $scope.userimgWidth = w;
                $scope.userimgHeight = h;
                jcrop_api.animateTo([0, 0, 0, 0]);
            });

            function showPreview(c) {
                var orgw = $('#UserProfilePic100 #userpic100').attr('data-width');
                var orgh = $('#UserProfilePic100 #userpic100').attr('data-height');
                $scope.userimgWidth = Math.round((c.w / $scope.OrgimgWidth) * orgw);
                $scope.userimgHeight = Math.round((c.h / $scope.OrgimgHeight) * orgh);
                $scope.userimgX = Math.round((c.x / $scope.OrgimgWidth) * orgw);
                $scope.userimgY = Math.round((c.y / $scope.OrgimgHeight) * orgh);
            };

            function releaseCheck() {
                jcrop_api.setOptions({
                    allowSelect: true
                });
            };
        };
        $scope.UpdateLightBoximage = function () {
            var saveobj = {};
            saveobj.imgsourcepath = $("#userpic100").attr('src');
            saveobj.imgwidth = $scope.userimgWidth;
            saveobj.imgheight = $scope.userimgHeight;
            saveobj.imgX = $scope.userimgX;
            saveobj.imgY = $scope.userimgY;
            var filePath = $("#userpic100").attr('src').toString().replace("Temp", "");
            DamNewService.SaveCropedImageForLightBox(saveobj).then(function (data) {
                $scope.OrgimgWidth = 0;
                $scope.OrgimgHeight = 0;
                if (data.Response == true) {
                    $.grep($scope.damlightbox.lightboxselection, function (e) {
                        return e.AssetId == $scope.Cropedimgassetid
                    })[0].Iscropped = true;
                    $timeout(function () {
                        $('#UserProfilePic100').modal('hide');
                        $scope.attrgrpImagefileName = $("#userpic100").attr('src').toString().split("/")[2];
                        $("#imgcropped_" + $scope.Cropedimgassetid).attr("src", TenantFilePath + 'DAMFiles/Temp/' + $scope.attrgrpImagefileName + '?' + Math.random() + '');
                        if ($.grep(tempLightboxguid, function (e) {
							e == $scope.attrgrpImagefileName
                        }).length == 0) tempLightboxguid.push($scope.attrgrpImagefileName);
                    }, 200);
                }
            });
        }
        $scope.DownloadWithMetadata = function (result) {
            if ($('#icon_selectassets').attr('class') != 'icon-check') {
                if ($scope.selectall == false) {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                    selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                } else {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                }
            }
            if (selectedAssetIDs.length > 0) {
                if (result) {
                    var Blank = [],
						Blanktemparr = [];
                    for (var i = 0; i < selectedAssetIDs.length; i++) {
                        Blanktemparr = $.grep(selectedAssetFiles, function (e) {
                            return e.Category == 1 && e.AssetUniqueID == selectedAssetIDs[i];
                        });
                        if (Blanktemparr != null) {
                            if (Blanktemparr.length > 0) {
                                Blank.push(Blanktemparr[0].AssetUniqueID);
                            }
                        }
                    }
                    if (Blank.length > 0) {
                        if (Blank.length == selectedAssetIDs.length) {
                            bootbox.alert($translate.instant('LanguageContents.Res_5763.Caption'));
                            refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                            refreshDeleteableScope(selectedAssetIDs);
                            $scope.DownloadFileObj.DownloadFilesArr = [];
                            $scope.AssetFilesObj.AssetTaskObj = [];
                            $scope.AssetFilesObj.AssetSelection = [];
                            selectedAssetIDs = [];
                            return false;
                        } else if (Blank.length > 0) {
                            bootbox.confirm($translate.instant('LanguageContents.Res_5764.Caption'), function (Blankresult) {
                                if (Blankresult) {
                                    var d = $.grep(selectedAssetIDs, function (e) {
                                        return $.inArray(e, Blank) == -1;
                                    });
                                    PerformDownloadPackage(d, result);
                                }
                            });
                        }
                    } else if (Blank.length == 0) {
                        PerformDownloadPackage(selectedAssetIDs, result);
                    }
                } else {
                    PerformDownloadPackage(selectedAssetIDs, result);
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4610.Caption'));
            }
        }

        function PerformDownloadPackage(temparr, result) {
            if (temparr.length > 0) {
                var arr = [];
                for (var i = 0; i < temparr.length; i++) {
                    arr[i] = temparr[i];
                }
                var include = result;
                if (include == 0) $scope.ProgressMsgHeader = "Downloading Metadata";
                if (include == 1) $scope.ProgressMsgHeader = "Downloading Package";
                blockUIForDownload();
                DamNewService.DownloadAssetWithMetadata(arr, result).then(function (result) {
                    if (result.Response == null) {
                        NotifyError($translate.instant('LanguageContents.Res_4317.Caption'));
                    } else {
                        if (arr.length == 1) {
                            if (include == false) {
                                var fileid = result.Response[1],
									extn = '.xml';
                                if (result.Response[0] == null || result.Response[0] == "") var filename = "MediabankAssets" + extn;
                                else var filename = result.Response[0];
                            } else {
                                var fileid = result.Response[1],
									extn = '.zip';
                                var filename = "package";
                            }
                        } else {
                            var fileid = result.Response[1],
								extn = '.zip';
                            var filename = "package";
                        }
                        if ((parseInt(clientFileStoragetype.Amazon) == parseInt(cloudsetup.storageType)) && include) {
                            location.href = 'DAMDownload.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#download_token_value_id').val();
                        } else {
                            location.href = 'DownloadAssetMetadata.aspx?FileID=' + fileid + '&FileFriendlyName=' + filename + '&Ext=' + extn + '&token=' + $('#packagedownload_token_id').val();
                        }
                        refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                        refreshDeleteableScope(selectedAssetIDs);
                        $timeout(function () {
                            selectedAssetIDs = [];
                        }, 200);
                        $scope.DownloadFileObj.DownloadFilesArr = [];
                        $scope.AssetFilesObj.AssetTaskObj = [];
                        $scope.AssetFilesObj.AssetSelection = [];
                        selectedAssetIDs = [];
                        if (include == true) NotifySuccess("$translate.instant('LanguageContents.Res_5805.Caption')");
                        else NotifySuccess($translate.instant('LanguageContents.Res_5806.Caption'));
                    }
                });
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_5763.Caption'));
                refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                refreshDeleteableScope(selectedAssetIDs);
                $timeout(function () {
                    selectedAssetIDs = [];
                }, 200);
                $scope.DownloadFileObj.DownloadFilesArr = [];
                $scope.AssetFilesObj.AssetTaskObj = [];
                $scope.AssetFilesObj.AssetSelection = [];
                selectedAssetIDs = [];
            }
        }
        $scope.UnPublishAssets = function () {
            if ($('#icon_selectassets').attr('class') != 'icon-check') {
                if ($scope.selectall == false) {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                    selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                } else {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                }
            }
            if (selectedAssetIDs.length > 0) {
                if ($('#Publishicon_' + selectedAssetIDs)[0].className == "icon-arrow-up") {
                    for (var i = 0; i < selectedAssetFiles.length; i++) {
                        var temparr = $.grep(selectedAssetFiles, function (e) {
                            return e.IsPublish == false && e.AssetUniqueID == selectedAssetIDs;
                        });
                        if (temparr.length > 0) {
                            selectedAssetFiles[i].IsPublish = true;
                        }
                    }
                }
            }
            var assetselection = selectedAssetIDs;
            $timeout(function () {
                if (selectedAssetIDs.length > 0) {
                    var AssetLink = [],
						Assettemparr = [];
                    angular.forEach(selectedAssetIDs, function (item) {
                        Assettemparr = ($.grep(selectedAssetFiles, function (e) {
                            return e.LinkUserDetail.length > 0 && e.AssetUniqueID == item;
                        }));
                        if (Assettemparr != null) if (Assettemparr.length > 0) AssetLink.push(Assettemparr[0].AssetUniqueID);
                    });
                    if (AssetLink.length == selectedAssetIDs.length) {
                        var ht_Count = "";
                        if (AssetLink.length > 0) ht_Count = "Assets";
                        if (AssetLink.length == 1) ht_Count = "Asset";
                        bootbox.alert($translate.instant('LanguageContents.Res_5757.Caption') + ht_Count + $translate.instant('LanguageContents.Res_5765.Caption'));
                        $('.loadingDownloadPageModel').modal("hide");
                        refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                        refreshDeleteableScope(selectedAssetIDs);
                        $scope.DownloadFileObj.DownloadFilesArr = [];
                        $scope.AssetFilesObj.AssetTaskObj = [];
                        $scope.AssetFilesObj.AssetSelection = [];
                        selectedAssetIDs = [];
                        return false;
                    } else if (AssetLink.length > 0) {
                        bootbox.confirm($translate.instant('LanguageContents.Res_5788.Caption'), function (result) {
                            if (result) {
                                selectedAssetIDs = $.grep(selectedAssetIDs, function (e) {
                                    return $.inArray(e, AssetLink) == -1;
                                });
                                var publishedAsset = [],
									temparr = [];
                                for (var i = 0; i < selectedAssetIDs.length; i++) {
                                    temparr = $.grep(selectedAssetFiles, function (e) {
                                        return e.IsPublish == true && e.AssetUniqueID == selectedAssetIDs[i];
                                    });
                                    if (temparr != null) {
                                        if (temparr.length > 0) {
                                            publishedAsset.push(temparr[0].AssetUniqueID);
                                        }
                                    }
                                }
                                UnpublishAction(publishedAsset);
                            }
                        });
                    } else if (AssetLink.length == 0) {
                        var publishedAsset = [],
							temparr = [];
                        for (var i = 0; i < selectedAssetIDs.length; i++) {
                            temparr = $.grep(selectedAssetFiles, function (e) {
                                return e.IsPublish == true && e.AssetUniqueID == selectedAssetIDs[i];
                            });
                            if (temparr != null) {
                                if (temparr.length > 0) {
                                    publishedAsset.push(temparr[0].AssetUniqueID);
                                }
                            }
                        }
                        UnpublishAction(publishedAsset);
                    }
                } else {
                    bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'))
                }
            }, 300);
        }

        function UnpublishAction(Link) {
            var publishedAssets = Link;
            if (publishedAssets.length > 0) {
                var AssetLink = [],
					Assettemparr = [];
                if (publishedAssets.length == selectedAssetIDs.length) {
                    PerformAssetUnpublish(publishedAssets);
                } else if (publishedAssets.length > 0) {
                    bootbox.confirm($translate.instant('LanguageContents.Res_5789.Caption'), function (result) {
                        if (result) {
                            PerformAssetUnpublish(publishedAssets);
                        }
                    });
                } else if (publishedAssets.length == 0) {
                    bootbox.alert($translate.instant('LanguageContents.Res_5766.Caption'));
                    refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                    refreshDeleteableScope(selectedAssetIDs);
                    $scope.DownloadFileObj.DownloadFilesArr = [];
                    $scope.AssetFilesObj.AssetTaskObj = [];
                    $scope.AssetFilesObj.AssetSelection = [];
                    selectedAssetIDs = [];
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_5766.Caption'));
                refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                refreshDeleteableScope(selectedAssetIDs);
                $scope.DownloadFileObj.DownloadFilesArr = [];
                $scope.AssetFilesObj.AssetTaskObj = [];
                $scope.AssetFilesObj.AssetSelection = [];
                selectedAssetIDs = [];
            }
        }

        function PerformAssetUnpublish(result) {
            var arr = [];
            if (result.length > 0) {
                $scope.ProgressMsgHeader = "Unpublishing Assets";
                $scope.ProgressContent = "Unpublishing";
                $('#loadingDownloadPageModel').modal("show");
                for (var i = 0; i < result.length; i++) {
                    arr[i] = result[i];
                }
                DamNewService.UnPublishAssets(arr).then(function (result) {
                    if (result.Response == true) {
                        if (arr.length == 1) {
                            NotifySuccess("" + arr.length + " " + $translate.instant('LanguageContents.Res_5753.Caption'));
                        } else {
                            NotifySuccess("" + arr.length + " " +$translate.instant('LanguageContents.Res_5753.Caption'));
                        }
                        for (var j = 0; j < arr.length; j++) {
                            for (var i = 0; i < $scope.AssetFilesObj.AssetFiles.length; i++) {
                                var temparr = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                                    return e.IsPublish == true && e.AssetUniqueID == arr[j];
                                });
                                if (temparr.length > 0) {
                                    temparr[0].IsPublish = false;
                                }
                            }
                        }
                        if ($scope.showallassetview == "showallasset") {
                            $scope.$broadcast('ShowallUpdateUnPublishIcon', arr);
                        } else {
                            if ($scope.DamViewName == "Thumbnail") {
                                $scope.$broadcast('loadAssetDetailSection_th', arr);
                            } else if ($scope.DamViewName == "Summary") {
                                $scope.$broadcast('loadAssetDetailSection_th_Summary', arr);
                            } else if ($scope.DamViewName == "List") {
                                $scope.$broadcast('loadAssetDetailSection_th_List', arr);
                            }
                        }
                        $('.loadingDownloadPageModel').modal("hide");
                        if ($('#icon_selectassets').attr('class') != 'icon-check') {
                            if ($scope.selectall == false) {
                                selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                                selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                            } else {
                                selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                            }
                        }
                        refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                        refreshDeleteableScope(selectedAssetIDs);
                        $scope.DownloadFileObj.DownloadFilesArr = [];
                        $scope.AssetFilesObj.AssetTaskObj = [];
                        $scope.AssetFilesObj.AssetSelection = [];
                        selectedAssetIDs = [];
                    } else {
                        $('.loadingDownloadPageModel').modal("hide");
                        NotifyError($translate.instant('LanguageContents.Res_1900.Caption'));
                    }
                });
            } else {
                $('.loadingDownloadPageModel').modal("hide");
                bootbox.alert($translate.instant('LanguageContents.Res_5766.Caption'));
                refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                refreshDeleteableScope(selectedAssetIDs);
                $scope.DownloadFileObj.DownloadFilesArr = [];
                $scope.AssetFilesObj.AssetTaskObj = [];
                $scope.AssetFilesObj.AssetSelection = [];
                selectedAssetIDs = [];
            }
        }
        $scope.Reformatproces = function () {
            if ($scope.AssetFilesObj.AssetSelection.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_4726.Caption'));
            }
            var reformate = [],
				temparr = [];
            for (var i = 0; i < $scope.AssetFilesObj.AssetSelection.length; i++) {
                temparr = $.grep($scope.AssetFilesObj.AssetFiles, function (e) {
                    return e.Category == 0 && e.ProcessType == 1 && e.Status == 2 && e.Extension != ".pdf" && e.AssetUniqueID == $scope.AssetFilesObj.AssetSelection[i];
                });
                if (temparr != null) {
                    if (temparr.length > 0) {
                        reformate.push(temparr[0]);
                    }
                }
            }
            if ($scope.AssetFilesObj.AssetSelection.length > 0 && reformate.length == 0) {
                bootbox.alert($translate.instant('LanguageContents.Res_1165.Caption'));
                refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                refreshDeleteableScope($scope.AssetFilesObj.AssetSelection);
                $scope.DownloadFileObj.DownloadFilesArr = [];
                $scope.AssetFilesObj.AssetTaskObj = [];
                $scope.AssetFilesObj.AssetSelection = [];
                return false
            } else if ($scope.AssetFilesObj.AssetSelection.length > 0 && reformate.length > 1) {
                bootbox.alert($translate.instant('LanguageContents.Res_1166.Caption'));
                refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                refreshDeleteableScope($scope.AssetFilesObj.AssetSelection);
                $scope.DownloadFileObj.DownloadFilesArr = [];
                $scope.AssetFilesObj.AssetTaskObj = [];
                $scope.AssetFilesObj.AssetSelection = [];
            } else if ($scope.AssetFilesObj.AssetSelection.length > 0 && reformate.length == 1) {
                $timeout(function () {
                    var folderid;
                    var EntityID;
                    if ($scope.Folderselect.ID != undefined) {
                        folderid = $scope.Folderselect.ID;
                        EntityID = $scope.processingsrcobj.processingid;
                    } else {
                        folderid = reformate[0].FolderID;
                        EntityID = reformate[0].EntityID;
                    }
                    var modalInstance = $modal.open({
                        templateUrl: 'views/mui/assetreformat.html',
                        controller: "mui.muiassetreformatCtrl",
                        resolve: {
                            params: function () {
                                return {
                                    thubmnailGuid: reformate[0].FileGuid,
                                    assetid: reformate[0].AssetUniqueID,
                                    folderid: folderid,
                                    EntityID: EntityID,
                                    fromisassetlibrary: 0
                                };
                            }
                        },
                        scope: $scope,
                        windowClass: 'reformatAssetPopup popup-widthXL',
                        backdrop: "static"
                    });
                    modalInstance.result.then(function (selectedItem) {
                        $scope.selected = selectedItem;
                    }, function () { });
                }, 100);
                refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                refreshDeleteableScope($scope.AssetFilesObj.AssetSelection);
                $scope.DownloadFileObj.DownloadFilesArr = [];
                $scope.AssetFilesObj.AssetTaskObj = [];
                $scope.AssetFilesObj.AssetSelection = [];
            }
        }
        $scope.setdamassetselection = function () {
            if ($scope.damclipboard.assetselection == 0) {
                $scope.IsPaste = false;
            } else {
                $scope.IsPaste = true;
            }
            if ($('#icon_selectassets').attr('class') != 'icon-check') {
                var remainRecord = [];
                remainRecord = $.grep($scope.AssetFilesObj.AssetSelection, function (e) {
                    return e == $scope.AssetFilesObj.selectedAssetId;
                });
                if (remainRecord.length == 0) {
                    if ($('#chkBoxIcon_' + $scope.AssetFilesObj.selectedAssetId + '').hasClass('checked')) {
                        $scope.AssetFilesObj.AssetSelection.push($scope.AssetFilesObj.selectedAssetId);
                    }
                }
            }
        }

        function PerformReformat(result) { }
        $scope.set_color = function (clr) {
            if (clr != null) return {
                'background-color': "#" + clr.toString().trim()
            };
            else return '';
        }
        $scope.openDamAssetSendpopup = function () {
            if ($('#icon_selectassets').attr('class') != 'icon-check') {
                if ($scope.selectall == false) {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                    selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                } else {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                }
            }
            if (selectedAssetIDs.length > 0) {
                $scope.sendDamMetadatMailSubject = "";
                $scope.AssetToMailAddress = [];
                $scope.includemdata = false;
                $scope.processMailFlag = false;
                $scope.includemdata = false;
                $('#sendDamAssetMailPopup').modal('show');
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'));
            }
        }
        $scope.includemdata = false;
        $scope.selectmetadata = function (e) {
            var chk = e.target;
            if (chk.checked) $scope.includemdata = true;
            else $scope.includemdata = false;
        }
        $scope.sendDamAssetMail = function () {
            if (selectedAssetIDs.length > 0) {
                $scope.MailprocessMailFlag = true;
                var assetSelection = [];
                var emailvalid = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                var assetSelection = [],
					toArray = new Array(),
					mailSubject = $scope.sendDamMetadatMailSubject != "" ? $scope.sendDamMetadatMailSubject : "Marcom Media Bank";
                for (var i = 0, mail; mail = $scope.AssetToMailAddress[i++];) {
                    if (mail.text.match(emailvalid)) {
                        toArray.push(mail.text);
                    } else {
                        $scope.MailprocessMailFlag = false;
                        bootbox.alert($translate.instant('LanguageContents.Res_1137.Caption'));
                        return false;
                    }
                }
                for (var i = 0; i < selectedAssetIDs.length; i++) {
                    assetSelection.push({
                        "AssetId": selectedAssetIDs[i]
                    });
                }
                if (assetSelection.length > 0 && toArray.length > 0) {
                    var srcObj = {};
                    srcObj.Toaddress = toArray.join(",");
                    srcObj.subject = mailSubject;
                    srcObj.AssetArr = assetSelection;
                    srcObj.metadata = $scope.includemdata;
                    $timeout(function () {
                        DamNewService.SendMailWithMetaData(srcObj).then(function (result) {
                            if (result.Response == null) {
                                $('#sendDamAssetMail').removeClass('disabled');
                                $scope.processMailFlag = false;
                                $scope.MailprocessMailFlag = false;
                                NotifyError($translate.instant('LanguageContents.Res_4349.Caption'));
                                $('#sendDamAssetMail').modal("hide");
                                $scope.sendDamMetadatMailSubject = "";
                                $scope.AssetToMailAddress = [];
                                refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                                refreshDeleteableScope(selectedAssetIDs);
                                selectedAssetIDs = [];
                                $scope.DownloadFileObj.DownloadFilesArr = [];
                                $scope.AssetFilesObj.AssetTaskObj = [];
                                $scope.AssetFilesObj.AssetSelection = [];
                            } else {
                                $('#sendDamAssetMail').removeClass('disabled');
                                $scope.processMailFlag = false;
                                $scope.MailprocessMailFlag = false;
                                NotifySuccess($translate.instant('LanguageContents.Res_4472.Caption'));
                                $('#sendDamAssetMailPopup').modal("hide");
                                $scope.sendDamMetadatMailSubject = "";
                                $scope.AssetToMailAddress = [];
                                refreshDownLoadScope($scope.DownloadFileObj.DownloadFilesArr);
                                refreshDeleteableScope(selectedAssetIDs);
                                selectedAssetIDs = [];
                                $scope.DownloadFileObj.DownloadFilesArr = [];
                                $scope.AssetFilesObj.AssetTaskObj = [];
                                $scope.AssetFilesObj.AssetSelection = [];
                            }
                        });
                    }, 100);
                } else {
                    $scope.MailprocessMailFlag = false;
                    if (assetSelection.length == 0) bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'));
                    else if (toArray.length == 0) bootbox.alert($translate.instant('LanguageContents.Res_1138.Caption'));
                }
            } else {
                bootbox.alert($translate.instant('LanguageContents.Res_4614.Caption'));
            }
        }
        $scope.$on('CallbackLoadAssets', function (event, folderid, foldername) {
            $scope.initialTreeSelect = foldername;
            var res = $.grep($scope.TaskStatusObj, function (e) {
                return e.ID == $scope.MulitipleFilterStatus;
            });
            $scope.AssetFilesObj.PageNo = 1;
            if (res[0].ID == 1) {
                $timeout(function () {
                    $scope.$broadcast('LoadThumbnailAssetView', folderid, $scope.processingsrcobj.processingid, 1);
                }, 200);
            } else if (res[0].ID == 2) {
                $timeout(function () {
                    $scope.$broadcast('LoadSummaryAssetView', folderid, $scope.processingsrcobj.processingid, 2);
                }, 200);
            } else if (res[0].ID == 3) {
                $timeout(function () {
                    $scope.$broadcast('LoadListAssetView', folderid, $scope.processingsrcobj.processingid, 3);
                }, 200);
            }
        });
        $scope.$on('ReloadAssetViewreformate', function (event) {
            if ($scope.processingsrcobj.processingplace == "attachment") {
                $scope.AssetFilesObj.HideShowllMenus = true;
                $scope.AssetFilesObj.HideCreateTask = true;
                $scope.AssetFilesObj.HideAddFolder = true;
                $scope.AssetFilesObj.HideAddAsset = true;
                $scope.AssetFilesObj.HideCut = true;
                $scope.AssetFilesObj.HideAssetApproval = true;
                $scope.AssetFilesObj.HideproofApproval = true;
                $scope.AssetFilesObj.HidePublishDAM = true;
                $scope.AssetFilesObj.HideEntityAttachment = false;
                $scope.AssetFilesObj.IsLock = $scope.processingsrcobj.processinglock;
                PageLoadFromTrigger();
            } else if ($scope.processingsrcobj.processingplace == "task") {
                $scope.AssetFilesObj.HideAddFolder = false;
                $scope.AssetFilesObj.HideCreateTask = false;
                $scope.AssetFilesObj.HideShowllMenus = false;
                $scope.AssetFilesObj.HideAddAsset = true;
                $scope.AssetFilesObj.HideCut = false;
                $scope.AssetFilesObj.HideAssetApproval = false;
                $scope.AssetFilesObj.HideproofApproval = false;
                $scope.AssetFilesObj.HidePublishDAM = false;
                $scope.AssetFilesObj.HideEntityAttachment = true;
                $scope.AssetFilesObj.IsLock = $scope.processingsrcobj.processinglock;
                PageReLoadFromTrigger();
                $timeout(function () {
                    $scope.$emit('Taskassetinfoupdate');
                }, 100);
                var res = $.grep($scope.TaskStatusObj, function (e) {
                    return e.ID == $scope.MulitipleFilterStatus;
                });
                var b;
                $scope.AssetFilesObj.PageNo = 1;
                $timeout(function () {
                    if (res[0].ID == 1) $timeout(function () {
                        $scope.$broadcast('LoadThumbnailAssetView', 0, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                    }, 100);
                    else if (res[0].ID == 2) $timeout(function () {
                        $scope.$broadcast('LoadSummaryAssetView', 0, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                    }, 100);
                    else if (res[0].ID == 3) $timeout(function () {
                        $scope.$broadcast('LoadListAssetView', 0, $scope.processingsrcobj.processingid, $scope.MulitipleFilterStatus);
                    }, 100);
                }, 50);
            }
        });
        $scope.$on('CallBackAttachtakdrawFromDamAsset', function (event, ID) {
            $timeout(function () {
                $scope.$broadcast('CallBackAttachtakdrawFromDam', ID);
            }, 100);
        });
        $scope.openpopup = function () {
            if ($('#icon_selectassets').attr('class') != 'icon-check') {
                if ($scope.selectall == false) {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                    selectedAssetFiles = $scope.AssetFilesObj.AssetFiles;
                } else {
                    selectedAssetIDs = $scope.AssetFilesObj.AssetSelection;
                }
            }
            var damtaskentityselectionParams = {};
            var fromPlace = "attachment";
            if ($scope.PageFullData.overview != null && $scope.PageFullData.overview != undefined) {
                damtaskentityselectionParams = $scope.PageFullData.overview;
                fromPlace = "task";
            }
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/DAM/damtaskentityselection.html',
                controller: "mui.DAM.taskentitydamselectionCtrl",
                resolve: {
                    params: function () {
                        return {
                            fromPlace: fromPlace,
                            data: damtaskentityselectionParams
                        };
                    }
                },
                scope: $scope,
                windowClass: 'drpD-Link popup-widthL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
            refreshDownLoadScope(selectedAssetIDs);
            refreshDeleteableScope(selectedAssetIDs);
        }
        $scope.addassetfromcomputer = function (option) {
            var obj = {
                directory: $scope.Dam_Directory,
                AssetFilesObj: $scope.AssetFilesObj,
                selectedTree: $scope.selectedTree,
                processingsrcobj: $scope.processingsrcobj
            }
            var modalInstance = $modal.open({
                templateUrl: 'views/mui/DAM/assetcreation.html',
                controller: "mui.DAM.assetcreateCtrl",
                resolve: {
                    params: function () {
                        return {
                            "Option": option,
                            "Object": obj
                        };
                    }
                },
                scope: $scope,
                windowClass: 'fileUploadPopup popup-widthXL',
                backdrop: "static"
            });
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () { });
        }

        $scope.$on('callBacktoDam', function (event, action, AssetObj) {

            if ($scope.AssetFilesObj.AssetSelection.length > 1) {
                bootbox.alert($translate.instant('LanguageContents.Res_5719.Caption'));
                return false;
            }

            switch (action) {
                case "viewlightbox":
                    $scope.Viewlightbox();
                    break;
                case "send":
                    $scope.openDamAssetSendpopup();
                    break;
                case "ManageVersion":

                    AssetObj.AssetID = $scope.AssetFilesObj.selectedAssetId;

                    getVersionDetails(AssetObj.AssetID);
                    InitializeManageVersion();
                    break;
                case "ReplaceThumpnail":

                    AssetObj.AssetID = $scope.AssetFilesObj.selectedAssetId;
                    getVersionDetails(AssetObj.AssetID);
                    ChangePreviewProcess();
                    break;

            }

        })      

        /******** ManageVersion  ********/

        function InitializeManageVersion() {
            $("#uploadaseetversionProgress").empty();
            $("#uploadaseetversionProgress").append('<span class="pull-left count">0 of 0 Uploaded</span><span class="size">0 B / 0 B</span>' + '<div class="progress progress-striped active">' + '<div style="width: 0%" class="bar"></div>' + '</div>' + '<div id="uploaded" style="position: absolute; top: 42%; right: -30%;"></div>');
            StrartUpload_Versioning();
            $('#uploadaseetversionProgress').hide();

            $("#DamAssetVersion").modal('show');
        };

        function StrartUpload_Versioning() {
            var uploader_Version = "";
            if (uploader_Version != '' && uploader_Version != null && uploader_Version != undefined) {
                uploader_Version.destroy();
            }
            $('.moxie-shim').remove();
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                var uploader_Version = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfiles_Version',
                    container: 'filescontainer_Version',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: amazonURL + cloudsetup.BucketName,
                    urlstream_upload: true,
                    multiple_queues: true,
                    file_data_name: 'file',
                    multipart: true,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }
                });
                uploader_Version.bind('Init', function (up, params) { });
                uploader_Version.init();
                $('#pickfiles_V').each(function () {
                    var input = new mOxie.FileInput({
                        browse_button: this,
                        multiple: false
                    });
                    input.onchange = function (event) {
                        uploader_Version.addFile(input.files);
                    };
                    input.init();
                });
                uploader_Version.bind('FilesAdded', function (up, files) {
                    TotalUploadProgress_V(files[0]);
                    up.refresh();
                    uploader_Version.start();
                    $('#uploadaseetversionProgress').show();
                    $('#uploadaseetversionProgress .bar').css('width', '0');
                });
                uploader_Version.bind('UploadProgress', function (up, file) {
                    $('#uploadaseetversionProgress .bar').css("width", file.percent + "%");
                    TotalUploadProgress_V(file);
                });
                uploader_Version.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    $("#uploadaseetversionProgress").empty();
                    $("#uploadaseetversionProgress").hide();
                    up.refresh();
                });
                uploader_Version.bind('FileUploaded', function (up, file, response) {

                    var fileDescription = $('#Form_' + file.id + '').find('input[name="DescVal_' + file.id + '"]').val();
                    var fileid = file.id.replace("_", "");
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;

                    TotalUploadProgress_V(file);
                    timerObj_dam.fileuploadtimer = $timeout(function () {
                        SaveFileDetails_V(file, response.response);
                    }, 50);
                });
                uploader_Version.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKeypath = TenantFilePath.replace(/\\/g, "\/");
                    var uniqueKey = (uniqueKeypath + "DAMFiles/Original/" + file.id + fileext);
                    uploader_Version.settings.multipart_params.key = uniqueKey;
                    uploader_Version.settings.multipart_params.Filename = uniqueKey;
                });
            }
            else {

                var uploader_Version = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'pickfiles_Version',
                    container: 'filescontainer_Version',
                    max_file_size: '10000mb',
                    flash_swf_url: 'assets/vendor/js/plupload/Moxie.swf',
                    silverlight_xap_url: 'assets/vendor/js/plupload/Moxie.xap',
                    url: 'Handlers/DamFileHandler.ashx?Type=Attachment',
                    chunk_size: '64Kb',
                    multi_selection: false,
                    multipart_params: {}
                });
                uploader_Version.bind('Init', function (up, params) { });
                uploader_Version.init();
                $('#pickfiles_V').each(function () {
                    var input = new mOxie.FileInput({
                        browse_button: this,
                        multiple: false
                    });
                    input.onchange = function (event) {
                        uploader_Version.addFile(input.files);
                    };
                    input.init();
                });
                uploader_Version.bind('FilesAdded', function (up, files) {
                    TotalUploadProgress_V(files[0]);
                    up.refresh();
                    uploader_Version.start();
                    $('#uploadaseetversionProgress').show();
                    $('#uploadaseetversionProgress .bar').css('width', '0');
                });
                uploader_Version.bind('UploadProgress', function (up, file) {
                    $('#uploadaseetversionProgress .bar').css("width", file.percent + "%");
                    TotalUploadProgress_V(file);
                });
                uploader_Version.bind('Error', function (up, err) {
                    bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    $("#uploadaseetversionProgress").empty();
                    $("#uploadaseetversionProgress").hide();
                    up.refresh();
                });
                uploader_Version.bind('FileUploaded', function (up, file, response) {
                    TotalUploadProgress_V(file);
                    timerObj_dam.fileuploadtimer = $timeout(function () {
                        SaveFileDetails_V(file, response.response);
                    }, 50);
                });
                uploader_Version.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id,
                        size: file.size
                    });
                });
            }

            function TotalUploadProgress_V(file) {
                var UploadedCount = 0;
                $('#filelistActivity .attachmentBox').each(function () {
                    if ($(this).attr('data-status') == 'true') {
                        UploadedCount += 1;
                    }
                });
                $('#uploadaseetversionProgress .count').html(UploadedCount + ' of ' + 1 + ' Uploaded');
                $('#uploadaseetversionProgress .size').html(plupload.formatSize(file.loaded) + ' / ' + plupload.formatSize(file.size));
                $('#uploadaseetversionProgress .bar').css("width", Math.round(((file.loaded / file.size) * 100)) + "%");
            };
        }

        function SaveFileDetails_V(file, response) {
            var version = 0;
            if ($scope.Category == 0) {
                if ($scope.ActiveVersionNo == null || $scope.ActiveVersionNo == '') {
                    version = 0;
                } else {
                    version = $scope.ActiveVersionNo;
                }
            }
            $scope.SaveFiletoAsset = [];
            var extension = file.name.substring(file.name.lastIndexOf("."));
            var resultArr = response.split(",");
            var ID = 0;
            if ($scope.manageAsset.AssetID != undefined) ID = $scope.manageAsset.AssetID;
            else ID = $scope.manageAsset.AssetID;
            $scope.SaveFiletoAsset.push({
                "AssetID": ID,
                "Status": 0,
                "MimeType": resultArr[1],
                "Size": file.size,
                "FileGuid": resultArr[0],
                "CreatedOn": Date.now(),
                "Extension": extension,
                "Name": file.name,
                "VersionNo": version,
                "Description": file.name,
                "OwnerID": $cookies['UserId']
            });
            DamNewService.SaveFiletoAsset($scope.SaveFiletoAsset).then(function (result) {
                if (result.Response != null && result.Response != undefined) {
                    getVersionDetails(ID);
                    $('#uploadaseetversionProgress').hide();
                    NotifySuccess($translate.instant('LanguageContents.Res_4787.Caption'));
                    var assetlist = [];
                    assetlist.push($scope.manageAsset.AssetID);
                    $scope.$emit('loadAssetDetailSectionfrThumbnail', assetlist);
                    $scope.$emit('CallBackThumbnailAssetViewfrmMUI', ID);
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4265.Caption'));
                    $('#uploadaseetversionProgress').hide();
                }
            });
        }

        $scope.SaveFileCheckList = function (CheckListID, VersionNo, ChklistName, ChkListStatus) {
            $scope.UpdateAssetVersion(VersionNo);
        }

        $scope.UpdateAssetVersion = function (VersionNo) {
            var fileid = $.grep($scope.assetfiles, function (e) {
                return e.VersionNo == VersionNo
            })[0].ID;
            var ID = 0;
            if ($scope.manageAsset.AssetID != undefined) ID = $scope.manageAsset.AssetID;
            else ID = $scope.manageAsset.AssetID;
            DamNewService.UpdateAssetVersion(ID, fileid).then(function (result) {
                if (result.Response != null && result.Response != '') {
                    getVersionDetails(ID);
                    var datetimetdy = ConvertDateToString(new Date.create());
                    var datetimetdy1 = FormatStandard(datetimetdy, $scope.DefaultSettings.DateFormat);
                    $scope.AssetlastUpdatedOn = datetimetdy1;
                    NotifySuccess($translate.instant('LanguageContents.Res_4807.Caption'));
                    timerObj_dam.thumpnailcallbackTimer = $timeout(function () {
                        var assetlist = [];
                        assetlist.push(ID);
                        $scope.$emit('loadAssetDetailSectionfrThumbnail', assetlist);
                        $scope.$emit('CallBackThumbnailAssetViewfrmMUI', ID);
                    }, 100);
                } else {
                    NotifyError($translate.instant('LanguageContents.Res_4381.Caption'));
                }
            });
        }

        /********************* ReplaceThumpnail  ********************/

        function ChangePreviewProcess() {
            StrartUploadReplaceThumpnail();
            $timeout(function () {
                $("#replaceThumpnailButton").click();
            }, 200);
        }


        var uploader_ReplaceThumpnail = "";

        function StrartUploadReplaceThumpnail() {
            if (uploader_ReplaceThumpnail != '') {
                uploader_ReplaceThumpnail.destroy();
            }
            $('.moxie-shim').remove();
            if (parseInt(cloudsetup.storageType) == parseInt(clientFileStoragetype.Amazon)) {
                uploader_ReplaceThumpnail = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'replaceThumpnailButton',
                    container: 'replaceThumpnailContainer',
                    max_file_size: '10mb',
                    flash_swf_url: 'js/plupload.flash.swf',
                    silverlight_xap_url: 'js/plupload.silverlight.xap',
                    url: amazonURL + cloudsetup.BucketName,
                    multi_selection: false,
                    filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png,jpeg"
                    }],
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'success_action_status': '201',
                        'AWSAccessKeyId': cloudsetup.AWSAccessKeyID,
                        'policy': cloudsetup.PolicyDocument,
                        'signature': cloudsetup.PolicyDocumentSignature,
                    }
                });
                uploader_ReplaceThumpnail.bind('Init', function (up, params) { });
                uploader_ReplaceThumpnail.init();
                uploader_ReplaceThumpnail.bind('FilesAdded', function (up, files) {
                    //$.each(files, function (i, file) { });
                    up.refresh();
                    uploader_ReplaceThumpnail.start();
                    $('#fileuploadprogress').show();
                    $('#fileuploadprogress .bar').css('width', '0');
                    $('#fileuploadprogress #fileuploadbarText').html('0 %');
                });
                uploader_ReplaceThumpnail.bind('fileuploadprogress', function (up, file) {
                    $('#fileuploadprogress .bar').css("width", file.percent + "%");
                    $('#fileuploadprogress #fileuploadbarText').html(file.percent + "%");
                });
                uploader_ReplaceThumpnail.bind('Error', function (up, err) {

                    if (err.code == "-601") {
                        bootbox.alert($translate.instant('LanguageContents.Res_5744.Caption') + err.file.name.split('.')[1] + $translate.instant('LanguageContents.Res_5745.Caption'))
                    }
                    else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    }

                    up.refresh();
                });
                uploader_ReplaceThumpnail.bind('FileUploaded', function (up, file, response) {
                    var img = new Image();
                    var fileext = "." + file.name.split('.').pop();
                    var providerresponse = response;
                    response.response = file.id + "," + GetMIMEType(file.name) + "," + fileext;
                    var physicalpath = TenantFilePath + 'DAMFiles/Cropped/Temp/' + file.id + fileext;
                    img.src = amazonURL + cloudsetup.BucketName + "/" + physicalpath;
                    $('#fileuploadprogress').hide();
                    $('#fileuploadCrop').modal('show');
                    $('#fileuploadCrop #fileuploadCropReportImg').attr('style', '');
                    $('#fileuploadCrop #fileuploadCropReportImg').removeAttr('src').css('visibility', 'hidden');
                    $('#fileuploadCrop #fileuploadCropReportImg').attr('src', img.src);
                    $("<img/>").attr("src", imageBaseUrlcloud + 'DAMFiles/Cropped/Temp/' + file.id + fileext).load(function () {
                        var w = this.width;
                        var h = this.height;
                        var path = file.id + fileext;
                        $scope.tempResponse = response.response;
                        $('#fileuploadCrop #fileuploadCropReportImg').attr('data-width', w).attr('data-height', h);
                        $('#fileuploadCrop #fileuploadCropReportImg').css('visibility', 'visible');
                        $timeout(function () {
                            AssigneNewImage(w, h, path);
                        }, 200);
                    });
                });
                uploader_ReplaceThumpnail.bind('BeforeUpload', function (up, file) {
                    $.extend(up.settings.multipart_params, {
                        id: file.id.replace("_", ""),
                        size: file.size
                    });
                    var keyName = file.name;
                    var keySplit = keyName.split('/');
                    var fileName = keySplit[keySplit.Length - 1];
                    var fileext = "." + file.name.split('.').pop();
                    var uniqueKeypath = TenantFilePath.replace(/\\/g, "\/");
                    var uniqueKey = (uniqueKeypath + "DAMFiles/Cropped/Temp/" + file.id + fileext);
                    uploader_ReplaceThumpnail.settings.multipart_params.key = uniqueKey;
                    uploader_ReplaceThumpnail.settings.multipart_params.Filename = uniqueKey;
                });
            }
            else {
                uploader_ReplaceThumpnail = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight,browserplus,html4',
                    browse_button: 'replaceThumpnailButton',
                    container: 'replaceThumpnailContainer',
                    max_file_size: '10mb',
                    flash_swf_url: 'js/plupload.flash.swf',
                    silverlight_xap_url: 'js/plupload.silverlight.xap',
                    url: 'Handlers/AssetCropHandler.ashx?Type=Attachment',
                    multi_selection: false,
                    filters: [{
                        title: "Image files",
                        extensions: "jpg,gif,png,jpeg"
                    }]
                });
                uploader_ReplaceThumpnail.bind('Init', function (up, params) { });
                uploader_ReplaceThumpnail.init();
                uploader_ReplaceThumpnail.bind('FilesAdded', function (up, files) {
                    $.each(files, function (i, file) { });
                    up.refresh();
                    uploader_ReplaceThumpnail.start();
                    $('#fileuploadprogress').show();
                    $('#fileuploadprogress .bar').css('width', '0');
                    $('#fileuploadprogress #fileuploadbarText').html('0 %');
                });
                uploader_ReplaceThumpnail.bind('fileuploadprogress', function (up, file) {
                    $('#fileuploadprogress .bar').css("width", file.percent + "%");
                    $('#fileuploadprogress #fileuploadbarText').html(file.percent + "%");
                });
                uploader_ReplaceThumpnail.bind('Error', function (up, err) {
                    if (err.code == "-601") {
                        bootbox.alert($translate.instant('LanguageContents.Res_5744.Caption') + err.file.name.split('.')[1] + $translate.instant('LanguageContents.Res_5745.Caption'))
                    }
                    else {
                        bootbox.alert($translate.instant('LanguageContents.Res_1840.Caption'));
                    }

                    up.refresh();
                });
                uploader_ReplaceThumpnail.bind('FileUploaded', function (up, file, response) {
                    $scope.tempFile = file;
                    $scope.tempResponse = response.response;
                    var img = new Image();
                    img.src = TenantFilePath + 'DAMFiles/Cropped/Temp/' + response.response;
                    $('#fileuploadprogress').hide();
                    $('#fileuploadCrop').modal('show');
                    $('#fileuploadCrop #fileuploadCropReportImg').removeAttr('src').css('visibility', 'hidden');
                    $('#fileuploadCrop #fileuploadCropReportImg').attr('style', '');
                    var w = parseInt(response.response.split(',')[1]);
                    var h = parseInt(response.response.split(',')[2]);
                    var path = response.response.split(',')[0];
                    timerObj_dam.assignNewImage = $timeout(function () {
                        AssigneNewImage(w, h, path);
                    }, 200);
                });
            }

        }

        function AssigneNewImage(w, h, path) {
            $('#fileuploadCrop #fileuploadCropReportImg').attr('src', imageBaseUrlcloud + 'DAMFiles/Cropped/Temp/' + path);
            $('#fileuploadCrop #fileuploadCropReportImg').attr('data-width', w).attr('data-height', h);
            if (w >= h) {
                $('#fileuploadCrop #fileuploadCropReportImg').css('width', '704px');
                var imgheight = Math.round((h / w) * 704);
                var margine = Math.round((528 - imgheight) / 2);
                if (margine < 0) {
                    margine = 0;
                }
                $('#fileuploadCrop #fileuploadCropReportImg').parent().css('margin-top', margine + 'px');
            } else {
                $('#fileuploadCrop #fileuploadCropReportImg').css('height', '528px');
                $('#fileuploadCrop #fileuploadCropReportImg').parent().css('margin-top', '0px');
            }
            $('#fileuploadCrop #fileuploadCropReportImg').css('visibility', 'visible');
            initJcrop();
        }

        function initJcrop() {
            if ($('#fileuploadCrop .jcrop-holder').length > 0) {
                jcrop_api.destroy();
            }
            $('#fileuploadCropReportImg').Jcrop({
                onChange: showPreview,
                onSelect: showPreview,
                onRelease: releaseCheck
            }, function () {
                jcrop_api = this;
                jcrop_api.setOptions({
                    aspectRatio: 4 / 5
                });
                jcrop_api.focus();
                var w = $('#fileuploadCrop #fileuploadCropReportImg').width();
                var h = $('#fileuploadCrop #fileuploadCropReportImg').height();
                $scope.OrgimgWidth = w;
                $scope.OrgimgHeight = h;
                var nw = 0;
                var nh = 0;
                if (w <= h) {
                    nw = w;
                    nh = Math.round((w / 4) * 5);
                } else {
                    nh = h;
                    nw = Math.round((h / 4) * 5);
                }
                jcrop_api.animateTo([0, 0, nw, nh]);
            });

            function showPreview(c) {
                var orgw = $('#fileuploadCrop #fileuploadCropReportImg').attr('data-width');
                var orgh = $('#fileuploadCrop #fileuploadCropReportImg').attr('data-height');
                $scope.userimgWidth = Math.round((c.w / $scope.OrgimgWidth) * orgw);
                $scope.userimgHeight = Math.round((c.h / $scope.OrgimgHeight) * orgh);
                $scope.userimgX = Math.round((c.x / $scope.OrgimgWidth) * orgw);
                $scope.userimgY = Math.round((c.y / $scope.OrgimgHeight) * orgh);
            };

            function releaseCheck() {
                jcrop_api.setOptions({
                    allowSelect: true
                });
            };
        };

        $scope.updateThumpnail = function () {
            var saveobj = {};
            saveobj.imgsourcepath = $("#fileuploadCropReportImg").attr('src');
            saveobj.imgwidth = $scope.userimgWidth;
            saveobj.imgheight = $scope.userimgHeight;
            saveobj.imgX = $scope.userimgX;
            saveobj.imgY = $scope.userimgY;
            saveobj.Preview = $scope.tempResponse.split(',')[0];
            saveobj.Fileguid = $scope.manageAsset.EditableAssetGuid;
            saveobj.AssetExt = $scope.manageAsset.EditableAssetExt;
            saveobj.AssetId = $scope.manageAsset.AssetID;

            DamNewService.UpdateAssetThumpnail(saveobj).then(function (result) {
                if (result.Response == null) {
                    NotifyError($translate.instant('LanguageContents.Res_1145.Caption'));
                } else {
                    $scope.OrgimgWidth = 0;
                    $scope.OrgimgHeight = 0;
                    $scope.$emit('ChangeAssetPreview', saveobj.AssetId);
                    if ($scope.DownloadFileObj != undefined && $scope.DownloadFileObj != null)
                        if ($scope.DownloadFileObj.DownloadFilesArr.length > 0)
                            $scope.$emit('refreshDamDownloadScope', $scope.DownloadFileObj.DownloadFilesArr);
                    timerObj_dam.previewTimer = $timeout(function () {
                        var assetlist = [];
                        assetlist.push(saveobj.AssetId);
                        $scope.$emit('loadAssetDetailSectionfrThumbnail', assetlist);
                        $scope.$emit('CallBackThumbnailAssetViewfrmMUI', saveobj.AssetId);
                    }, 500);

                }
            });
        }

        function getVersionDetails(assetID) {

            DamNewService.GetAttributeDetails(assetID).then(function (attrList) {
                $scope.assetfiles = attrList.Response.Files;
                $scope.ActiveVersion = $.grep(attrList.Response.Files, function (e) {
                    return e.ID == attrList.Response.ActiveFileID;
                })[0];
                $scope.Category = attrList.Response.Category;
                $scope.ActiveVersionNo = $scope.ActiveVersion.VersionNo;
                $scope.manageAsset = {};
                $scope.manageAsset.AssetID = assetID;
                $scope.manageAsset.EditableAssetGuid = $scope.ActiveVersion.Fileguid;
                $scope.manageAsset.EditableAssetExt = $scope.ActiveVersion.Extension.toLowerCase();
            });
        }
        $scope.closeVersionPopup = function () {
            $scope.assetfiles = [];
        }

        $scope.$on("$destroy", function () {
            $timeout.cancel(bootboxconfirmTimer);
            $scope.AssetFilesObj_Temp.AssetFiles.splice(0, $scope.AssetFilesObj_Temp.AssetFiles.length);
            RecursiveUnbindAndRemove($("[ng-controller='mui.damCtrl']"));
        });
    }
    app.controller('mui.damCtrl', ['$window', '$location', '$timeout', '$scope', '$cookies', '$resource', '$stateParams', '$translate', 'DamNewService', '$compile', '$modal', muidamCtrl]);
})(angular, app);