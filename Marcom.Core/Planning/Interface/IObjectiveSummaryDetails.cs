﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BrandSystems.Marcom.Core.Planning.Interface
{
   public interface IObjectiveSummaryDetails
    {
       int Id { get; set; }
       bool ObjectiveStatus { get; set; }
       int ObjectiveTypeId { get; set; }
       string ObjectiveTypeName { get; set; }
       string ObjectiveDescription { get; set; }
       decimal GlobalBaseLine { get; set; }
       decimal GlobalTarget { get; set; }
       decimal PlannedAmount { get; set; }
       decimal TargetOutcome { get; set; }
       bool EnableCommnets { get; set; }
       int Fulfilled { get; set; }
       int NotFulfilled { get; set; }
       int LevelsInvolved { get; set; }
       int ObjectiveUnitId { get; set; }
       int ObjectiveUnitValue { get; set; }
       string ObjectiveUnitCaption { get; set; }
       List<IObjectiveRatingSummary> ObjectiveRating { get; set; }
    }
   public interface IObjectiveRatingSummary
   {
       int? RatingId { get; set; }
       string RatingCaption { get; set; }
       int? RatinCount { get; set; }
       int? ObjectiveRatingID { get; set; }
   }

}
