﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrandSystems.Marcom.Dal.Base;

namespace BrandSystems.Marcom.Core.Interface
{
    public interface ITransaction : IDisposable
    {
        bool IsOpen { get; }

        void Commit();
        void Rollback();

        PersistenceManager PersistenceManager { get; }
    }
}
