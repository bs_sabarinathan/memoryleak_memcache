﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrandSystems.Marcom.Dal.ExpireHandler.Model
{
    public partial class EHActionInitiatorDao : BaseDao, ICloneable 
    {
         #region Member Variables
		protected int _id;
        protected int _attributeID;
        protected int _entityID;
        protected int _actionSrcID;
        protected int _actionType;
        protected int _timeDuration;            
        protected int _actionPlace;        
        protected DateTime _executionDate;
        protected string _actionSrcValue;
        protected int _status;
        protected int _actionownerId;
        protected DateTime _updatedon;
        protected bool _bIsDeleted;
        protected bool _bIsChanged;
		#endregion
		
		#region Constructors
		public EHActionInitiatorDao() {}

        public EHActionInitiatorDao(int attributeID, int pentityID, int pactionSrcID, DateTime pexecutionDate, int ptimeDuration, int pactionPlace, int pactionType, string pactionSrcValue, int pstatus, int pactionownerId, DateTime pUpdatedOn)
		{   this._attributeID = attributeID;
        this._entityID = pentityID;
            this._actionSrcID = pactionSrcID;
            this._executionDate=pexecutionDate;
            this._timeDuration=ptimeDuration;
            this._actionPlace=pactionPlace;
            this._actionType = pactionType;
            this._actionSrcValue=pactionSrcValue;
            this._status = pstatus;
            this._actionownerId = pactionownerId;
            this._updatedon = pUpdatedOn;      
           
		}

        public EHActionInitiatorDao(int pId)
		{
			this._id = pId; 
		}
		
		#endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _bIsChanged |= (_id != value); _id = value; }

        }

        public virtual int AttributeID
        {
            get { return _attributeID; }
            set { _attributeID = value; }

        }
        public virtual int ActionSrcID
        {
            get { return _actionSrcID; }
            set { _actionSrcID = value; }

        }
        public virtual int EntityID
        {
            get { return _entityID; }
            set { _entityID = value; }

        }    

        public virtual DateTime ExecutionDate
        {
            get { return _executionDate; }
            set { _executionDate = value; }
        }
        public virtual int TimeDuration
        {
            get { return _timeDuration; }
            set { _timeDuration = value; }

        }
        public virtual int ActionPlace
        {
            get { return _actionPlace; }
            set { _actionPlace = value; }

        }

        public virtual int ActionType
        {
            get { return _actionType; }
            set { _actionType = value; }

        }
        public virtual string ActionSrcValue
        {
            get { return _actionSrcValue; }
            set { _actionSrcValue = value; }
        }
        public virtual int Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public virtual int ActionownerId
        {
            get { return _actionownerId; }
            set { _actionownerId = value; }

        }
        public virtual DateTime UpdatedOn
        {
            get { return _updatedon; }
            set { _updatedon = value; }

        }

        public virtual bool IsDeleted
        {
            get
            {
                return _bIsDeleted;
            }
            set
            {
                _bIsDeleted = value;
            }
        }

        public virtual bool IsChanged
        {
            get
            {
                return _bIsChanged;
            }
            set
            {
                _bIsChanged = value;
            }
        }

        #endregion 				

        #region ICloneable methods

        public virtual object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
        #region Public Property and Mapping Constants

        //Nested class to provide strongly-typed access to property names (for .NET databinding, etc.)
        public static class PropertyNames
        {   public const string EHActionInitiatorDao = "EHActionInitiatorDao";
            public const string Id = "Id";
            public const string AttributeID = "AttributeID";
            public const string EntityID = "EntityID";
            public const string ActionSrcID = "ActionSrcID";
            public const string ExecutionDate = "ExecutionDate";
            public const string TimeDuration = "TimeDuration";
            public const string ActionPlace = "ActionPlace";
            public const string ActionType = "ActionType";
            public const string ActionSrcValue = "ActionSrcValue";
            public const string Status = "Status";
            public const string ActionownerId = "ActionownerId";
            public const string UpdatedOn = "UpdatedOn";
        }

        //Nested class to provide strongly-typed access to mapping names (for NHibernate Queries, etc.)
        public static class MappingNames
        {
            public const string EHActionInitiatorDao = "EHActionInitiatorDao";
            public const string Id = "Id";
            public const string AttributeID = "AttributeID";
            public const string EntityID = "EntityID";
            public const string ActionSrcID = "ActionSrcID";
            public const string ExecutionDate = "ExecutionDate";
            public const string TimeDuration = "TimeDuration";
            public const string ActionPlace = "ActionPlace";
            public const string ActionType = "ActionType";
            public const string ActionSrcValue = "ActionSrcValue";
            public const string Status = "Status";
            public const string ActionownerId = "ActionownerId";
            public const string UpdatedOn = "UpdatedOn";
        }

        #endregion

    }
}
